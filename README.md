# AutographTheWorld Mixed Reality Version

 This example is based off of the Mixed Reality Tutorial 213 found [here](https://docs.microsoft.com/en-us/windows/mixed-reality/mixed-reality-213)

![Dwight Was Here Drawing](https://i.imgur.com/fSWy0zk.jpg)
Drawing

![Speech Recognition](https://i.imgur.com/98OPDzV.png)
Speech Recognition example

![](https://i.imgur.com/axnRMhI.jpg)
March 31, 2018 - (Miami) Doral, FL @outside festival

 This example adds in a custom skybox, 3D Model of the earth, speech recognition and the ability to take a screenshot of the drawing. It's supposed to automatically upload to Twitter, however the code isn't quite finished for that feature yet.
 
 This sample is built upon Unity v2017.2.1p2 version. Also this sample works when the Mixed Reality headset is configured for a standing full experience, not seated.

 This sample includes the prebuild Visual Studio project in the IL2CPP folder for running with Visual Studio 2017. In order to run this, make sure you've installed the Unity Game development, UWP development, C++ desktop development and .Net Framework features for Visual Studio. Also make sure you have the latest Windows 10 SDK.

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).