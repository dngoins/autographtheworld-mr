﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Windows.Foundation.Collections.IIterator`1<System.TimeSpan>
struct IIterator_1_t3050270018;
// Windows.Foundation.Collections.IIterator`1<System.Type>
struct IIterator_1_t358088233;
// Windows.Foundation.Collections.IIterator`1<System.UInt16>
struct IIterator_1_t51868431;
// Windows.Foundation.Collections.IIterator`1<System.UInt32>
struct IIterator_1_t434205451;
// Windows.Foundation.Collections.IIterator`1<System.UInt64>
struct IIterator_1_t2008183565;
// Windows.Foundation.Collections.IIterator`1<System.Uri>
struct IIterator_1_t2269347093;
// Windows.Foundation.Collections.IIterator`1<Windows.Devices.Haptics.ISimpleHapticsControllerFeedback>
struct IIterator_1_t73560054;
// Windows.Foundation.Collections.IIterator`1<Windows.Devices.Haptics.SimpleHapticsControllerFeedback>
struct IIterator_1_t3082571852;
// Windows.Foundation.Collections.IIterator`1<Windows.Foundation.IWwwFormUrlDecoderEntry>
struct IIterator_1_t817443443;
// Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Point>
struct IIterator_1_t2039097012;
// Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Rect>
struct IIterator_1_t569256960;
// Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Size>
struct IIterator_1_t2720028407;
// Windows.Foundation.Collections.IIterator`1<Windows.Media.SpeechSynthesis.IVoiceInformation>
struct IIterator_1_t985199492;
// Windows.Foundation.Collections.IIterator`1<Windows.Media.SpeechSynthesis.VoiceInformation>
struct IIterator_1_t2140548105;
// Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState>
struct IIterator_1_t3527940572;
// Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState2>
struct IIterator_1_t851581404;
// Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.SpatialInteractionSourceState>
struct IIterator_1_t3384798832;
// System.Type
struct Type_t;
// System.String
struct String_t;

extern RuntimeClass* Il2CppComObject_il2cpp_TypeInfo_var;
extern const uint32_t IIterable_1_First_m1083239098_MetadataUsageId;
extern const uint32_t IIterable_1_First_m449735320_MetadataUsageId;
extern const uint32_t IIterable_1_First_m1090735028_MetadataUsageId;
extern const uint32_t IIterable_1_First_m3240740629_MetadataUsageId;
extern const uint32_t IIterable_1_First_m2773234478_MetadataUsageId;
extern const uint32_t IIterable_1_First_m323213694_MetadataUsageId;
extern const uint32_t IIterable_1_First_m1930551993_MetadataUsageId;
extern const uint32_t IIterable_1_First_m3408224386_MetadataUsageId;
extern const uint32_t IIterable_1_First_m725484364_MetadataUsageId;
extern const uint32_t IIterable_1_First_m1907809909_MetadataUsageId;
extern const uint32_t IIterable_1_First_m516406116_MetadataUsageId;
extern const uint32_t IIterable_1_First_m4003146197_MetadataUsageId;
extern const uint32_t IIterable_1_First_m1025496468_MetadataUsageId;
extern const uint32_t IIterable_1_First_m4104204176_MetadataUsageId;
extern const uint32_t IIterable_1_First_m200553690_MetadataUsageId;
extern const uint32_t IIterable_1_First_m2575532575_MetadataUsageId;
extern const uint32_t IIterable_1_First_m1657544952_MetadataUsageId;
struct IIterator_1_t2720028407;
struct IIterator_1_t569256960;
struct IVoiceInformation_t3111056019;
struct IIterator_1_t985199492;
struct ISimpleHapticsControllerFeedback_t2199416581;
struct IIterator_1_t2039097012;
struct IWwwFormUrlDecoderEntry_t2943299970;
struct ISpatialInteractionSourceState2_t2977437931;
struct IIterator_1_t851581404;
struct ISpatialInteractionSourceState_t1358829803;
struct IIterator_1_t3384798832;
struct IIterator_1_t2140548105;
struct IIterator_1_t3527940572;
struct IIterator_1_t434205451;
struct IIterator_1_t2008183565;
struct IIterator_1_t51868431;
struct IIterator_1_t3050270018;
struct IIterator_1_t817443443;
struct IIterator_1_t358088233;
struct IIterator_1_t2269347093;
struct IIterator_1_t73560054;
struct IUriRuntimeClass_t921050115;;
struct IIterator_1_t3082571852;
struct Point_t4164953539 ;
struct Rect_t2695113487 ;
struct Size_t550917638 ;
struct TimeSpan_t881159249 ;



// Windows.Foundation.Collections.IIterable`1<Windows.Foundation.Size>
struct NOVTABLE IIterable_1_t2467282394 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m4003146197(IIterator_1_t2720028407** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<Windows.Foundation.Rect>
struct NOVTABLE IIterable_1_t316510947 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m516406116(IIterator_1_t569256960** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.Media.SpeechSynthesis.IVoiceInformation>
struct NOVTABLE IIterator_1_t985199492 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m4261329911(IVoiceInformation_t3111056019** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2587194256(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1083425478(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1097615757(uint32_t ___items0ArraySize, IVoiceInformation_t3111056019** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<Windows.Media.SpeechSynthesis.IVoiceInformation>
struct NOVTABLE IIterable_1_t732453479 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1025496468(IIterator_1_t985199492** comReturnValue) = 0;
};
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
// Windows.Foundation.Collections.IIterator`1<Windows.Devices.Haptics.SimpleHapticsControllerFeedback>
struct NOVTABLE IIterator_1_t3082571852 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3794219604(ISimpleHapticsControllerFeedback_t2199416581** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3051985185(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m4247690978(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3370528625(uint32_t ___items0ArraySize, ISimpleHapticsControllerFeedback_t2199416581** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<Windows.Foundation.Point>
struct NOVTABLE IIterable_1_t1786350999 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1907809909(IIterator_1_t2039097012** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.Foundation.IWwwFormUrlDecoderEntry>
struct NOVTABLE IIterator_1_t817443443 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1946311790(IWwwFormUrlDecoderEntry_t2943299970** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m4030884216(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m564191597(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m402209304(uint32_t ___items0ArraySize, IWwwFormUrlDecoderEntry_t2943299970** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState2>
struct NOVTABLE IIterator_1_t851581404 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1548962596(ISpatialInteractionSourceState2_t2977437931** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m20766391(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2099795593(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2124254469(uint32_t ___items0ArraySize, ISpatialInteractionSourceState2_t2977437931** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState2>
struct NOVTABLE IIterable_1_t598835391 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m2575532575(IIterator_1_t851581404** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.SpatialInteractionSourceState>
struct NOVTABLE IIterator_1_t3384798832 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1757905955(ISpatialInteractionSourceState_t1358829803** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1287739422(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3375070988(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m852781585(uint32_t ___items0ArraySize, ISpatialInteractionSourceState_t1358829803** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<Windows.UI.Input.Spatial.SpatialInteractionSourceState>
struct NOVTABLE IIterable_1_t3132052819 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1657544952(IIterator_1_t3384798832** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.Media.SpeechSynthesis.VoiceInformation>
struct NOVTABLE IIterator_1_t2140548105 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m4213660447(IVoiceInformation_t3111056019** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1766660868(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m112683399(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1697528449(uint32_t ___items0ArraySize, IVoiceInformation_t3111056019** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<Windows.Media.SpeechSynthesis.VoiceInformation>
struct NOVTABLE IIterable_1_t1887802092 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m4104204176(IIterator_1_t2140548105** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState>
struct NOVTABLE IIterator_1_t3527940572 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1347393236(ISpatialInteractionSourceState_t1358829803** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1096701536(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1173429162(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3554813579(uint32_t ___items0ArraySize, ISpatialInteractionSourceState_t1358829803** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState>
struct NOVTABLE IIterable_1_t3275194559 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m200553690(IIterator_1_t3527940572** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.UInt32>
struct NOVTABLE IIterable_1_t181459438 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3240740629(IIterator_1_t434205451** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.UInt16>
struct NOVTABLE IIterator_1_t51868431 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2601848623(uint16_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1464880407(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3625227941(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m965407816(uint32_t ___items0ArraySize, uint16_t* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.UInt64>
struct NOVTABLE IIterable_1_t1755437552 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m2773234478(IIterator_1_t2008183565** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.UInt32>
struct NOVTABLE IIterator_1_t434205451 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3564117589(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1240475462(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3827789177(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1580073288(uint32_t ___items0ArraySize, uint32_t* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.UInt16>
struct NOVTABLE IIterable_1_t4094089714 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1090735028(IIterator_1_t51868431** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.TimeSpan>
struct NOVTABLE IIterable_1_t2797524005 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1083239098(IIterator_1_t3050270018** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<Windows.Foundation.IWwwFormUrlDecoderEntry>
struct NOVTABLE IIterable_1_t564697430 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m725484364(IIterator_1_t817443443** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Type>
struct NOVTABLE IIterator_1_t358088233 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1985419415(Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1810394002(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3491870477(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1714411637(uint32_t ___items0ArraySize, Type_t ** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Type>
struct NOVTABLE IIterable_1_t105342220 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m449735320(IIterator_1_t358088233** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Uri>
struct NOVTABLE IIterable_1_t2016601080 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m323213694(IIterator_1_t2269347093** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.Devices.Haptics.ISimpleHapticsControllerFeedback>
struct NOVTABLE IIterator_1_t73560054 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1134133026(ISimpleHapticsControllerFeedback_t2199416581** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m497198888(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m683426578(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m253268830(uint32_t ___items0ArraySize, ISimpleHapticsControllerFeedback_t2199416581** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<Windows.Devices.Haptics.ISimpleHapticsControllerFeedback>
struct NOVTABLE IIterable_1_t4115781337 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1930551993(IIterator_1_t73560054** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Uri>
struct NOVTABLE IIterator_1_t2269347093 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2319108837(IUriRuntimeClass_t921050115** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1671647974(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m978794311(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3359105184(uint32_t ___items0ArraySize, IUriRuntimeClass_t921050115** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.UInt64>
struct NOVTABLE IIterator_1_t2008183565 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m4214781887(uint64_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1386923519(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m282066159(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m188438814(uint32_t ___items0ArraySize, uint64_t* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<Windows.Devices.Haptics.SimpleHapticsControllerFeedback>
struct NOVTABLE IIterable_1_t2829825839 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3408224386(IIterator_1_t3082571852** comReturnValue) = 0;
};
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef POINT_T4164953539_H
#define POINT_T4164953539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Point
struct  Point_t4164953539 
{
public:
	// System.Single Windows.Foundation.Point::_x
	float ____x_0;
	// System.Single Windows.Foundation.Point::_y
	float ____y_1;

public:
	inline static int32_t get_offset_of__x_0() { return static_cast<int32_t>(offsetof(Point_t4164953539, ____x_0)); }
	inline float get__x_0() const { return ____x_0; }
	inline float* get_address_of__x_0() { return &____x_0; }
	inline void set__x_0(float value)
	{
		____x_0 = value;
	}

	inline static int32_t get_offset_of__y_1() { return static_cast<int32_t>(offsetof(Point_t4164953539, ____y_1)); }
	inline float get__y_1() const { return ____y_1; }
	inline float* get_address_of__y_1() { return &____y_1; }
	inline void set__y_1(float value)
	{
		____y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT_T4164953539_H
#ifndef RECT_T2695113487_H
#define RECT_T2695113487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Rect
struct  Rect_t2695113487 
{
public:
	// System.Single Windows.Foundation.Rect::_x
	float ____x_0;
	// System.Single Windows.Foundation.Rect::_y
	float ____y_1;
	// System.Single Windows.Foundation.Rect::_width
	float ____width_2;
	// System.Single Windows.Foundation.Rect::_height
	float ____height_3;

public:
	inline static int32_t get_offset_of__x_0() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____x_0)); }
	inline float get__x_0() const { return ____x_0; }
	inline float* get_address_of__x_0() { return &____x_0; }
	inline void set__x_0(float value)
	{
		____x_0 = value;
	}

	inline static int32_t get_offset_of__y_1() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____y_1)); }
	inline float get__y_1() const { return ____y_1; }
	inline float* get_address_of__y_1() { return &____y_1; }
	inline void set__y_1(float value)
	{
		____y_1 = value;
	}

	inline static int32_t get_offset_of__width_2() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____width_2)); }
	inline float get__width_2() const { return ____width_2; }
	inline float* get_address_of__width_2() { return &____width_2; }
	inline void set__width_2(float value)
	{
		____width_2 = value;
	}

	inline static int32_t get_offset_of__height_3() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____height_3)); }
	inline float get__height_3() const { return ____height_3; }
	inline float* get_address_of__height_3() { return &____height_3; }
	inline void set__height_3(float value)
	{
		____height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2695113487_H
#ifndef SIZE_T550917638_H
#define SIZE_T550917638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Size
struct  Size_t550917638 
{
public:
	// System.Single Windows.Foundation.Size::_width
	float ____width_0;
	// System.Single Windows.Foundation.Size::_height
	float ____height_1;

public:
	inline static int32_t get_offset_of__width_0() { return static_cast<int32_t>(offsetof(Size_t550917638, ____width_0)); }
	inline float get__width_0() const { return ____width_0; }
	inline float* get_address_of__width_0() { return &____width_0; }
	inline void set__width_0(float value)
	{
		____width_0 = value;
	}

	inline static int32_t get_offset_of__height_1() { return static_cast<int32_t>(offsetof(Size_t550917638, ____height_1)); }
	inline float get__height_1() const { return ____height_1; }
	inline float* get_address_of__height_1() { return &____height_1; }
	inline void set__height_1(float value)
	{
		____height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIZE_T550917638_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
// Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Point>
struct NOVTABLE IIterator_1_t2039097012 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3650755423(Point_t4164953539 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m941782850(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1050410819(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1958449181(uint32_t ___items0ArraySize, Point_t4164953539 * ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Rect>
struct NOVTABLE IIterator_1_t569256960 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1183023450(Rect_t2695113487 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1554425837(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m328760485(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m269641506(uint32_t ___items0ArraySize, Rect_t2695113487 * ___items0, uint32_t* comReturnValue) = 0;
};
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_19)); }
	inline TimeSpan_t881159249  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_t881159249 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_t881159249  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_t881159249  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_t881159249  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_21)); }
	inline TimeSpan_t881159249  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_t881159249  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
// Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Size>
struct NOVTABLE IIterator_1_t2720028407 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m573419872(Size_t550917638 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m4223115842(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2301142642(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2384573303(uint32_t ___items0ArraySize, Size_t550917638 * ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.TimeSpan>
struct NOVTABLE IIterator_1_t3050270018 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3779463845(TimeSpan_t881159249 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m575406462(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1603012770(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m4131394294(uint32_t ___items0ArraySize, TimeSpan_t881159249 * ___items0, uint32_t* comReturnValue) = 0;
};



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.TimeSpan>::First()
extern "C"  RuntimeObject* IIterable_1_First_m1083239098 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m1083239098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t2797524005* ____iiterable_1_t2797524005 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t2797524005::IID, reinterpret_cast<void**>(&____iiterable_1_t2797524005));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t3050270018* returnValue = NULL;
	hr = ____iiterable_1_t2797524005->IIterable_1_First_m1083239098(&returnValue);
	____iiterable_1_t2797524005->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Type>::First()
extern "C"  RuntimeObject* IIterable_1_First_m449735320 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m449735320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t105342220* ____iiterable_1_t105342220 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t105342220::IID, reinterpret_cast<void**>(&____iiterable_1_t105342220));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t358088233* returnValue = NULL;
	hr = ____iiterable_1_t105342220->IIterable_1_First_m449735320(&returnValue);
	____iiterable_1_t105342220->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.UInt16>::First()
extern "C"  RuntimeObject* IIterable_1_First_m1090735028 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m1090735028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t4094089714* ____iiterable_1_t4094089714 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t4094089714::IID, reinterpret_cast<void**>(&____iiterable_1_t4094089714));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t51868431* returnValue = NULL;
	hr = ____iiterable_1_t4094089714->IIterable_1_First_m1090735028(&returnValue);
	____iiterable_1_t4094089714->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.UInt32>::First()
extern "C"  RuntimeObject* IIterable_1_First_m3240740629 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m3240740629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t181459438* ____iiterable_1_t181459438 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t181459438::IID, reinterpret_cast<void**>(&____iiterable_1_t181459438));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t434205451* returnValue = NULL;
	hr = ____iiterable_1_t181459438->IIterable_1_First_m3240740629(&returnValue);
	____iiterable_1_t181459438->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.UInt64>::First()
extern "C"  RuntimeObject* IIterable_1_First_m2773234478 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m2773234478_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t1755437552* ____iiterable_1_t1755437552 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t1755437552::IID, reinterpret_cast<void**>(&____iiterable_1_t1755437552));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t2008183565* returnValue = NULL;
	hr = ____iiterable_1_t1755437552->IIterable_1_First_m2773234478(&returnValue);
	____iiterable_1_t1755437552->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Uri>::First()
extern "C"  RuntimeObject* IIterable_1_First_m323213694 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m323213694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t2016601080* ____iiterable_1_t2016601080 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t2016601080::IID, reinterpret_cast<void**>(&____iiterable_1_t2016601080));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t2269347093* returnValue = NULL;
	hr = ____iiterable_1_t2016601080->IIterable_1_First_m323213694(&returnValue);
	____iiterable_1_t2016601080->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Windows.Devices.Haptics.ISimpleHapticsControllerFeedback>::First()
extern "C"  RuntimeObject* IIterable_1_First_m1930551993 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m1930551993_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t4115781337* ____iiterable_1_t4115781337 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t4115781337::IID, reinterpret_cast<void**>(&____iiterable_1_t4115781337));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t73560054* returnValue = NULL;
	hr = ____iiterable_1_t4115781337->IIterable_1_First_m1930551993(&returnValue);
	____iiterable_1_t4115781337->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Windows.Devices.Haptics.SimpleHapticsControllerFeedback>::First()
extern "C"  RuntimeObject* IIterable_1_First_m3408224386 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m3408224386_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t2829825839* ____iiterable_1_t2829825839 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t2829825839::IID, reinterpret_cast<void**>(&____iiterable_1_t2829825839));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t3082571852* returnValue = NULL;
	hr = ____iiterable_1_t2829825839->IIterable_1_First_m3408224386(&returnValue);
	____iiterable_1_t2829825839->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Windows.Foundation.IWwwFormUrlDecoderEntry>::First()
extern "C"  RuntimeObject* IIterable_1_First_m725484364 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m725484364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t564697430* ____iiterable_1_t564697430 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t564697430::IID, reinterpret_cast<void**>(&____iiterable_1_t564697430));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t817443443* returnValue = NULL;
	hr = ____iiterable_1_t564697430->IIterable_1_First_m725484364(&returnValue);
	____iiterable_1_t564697430->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Windows.Foundation.Point>::First()
extern "C"  RuntimeObject* IIterable_1_First_m1907809909 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m1907809909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t1786350999* ____iiterable_1_t1786350999 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t1786350999::IID, reinterpret_cast<void**>(&____iiterable_1_t1786350999));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t2039097012* returnValue = NULL;
	hr = ____iiterable_1_t1786350999->IIterable_1_First_m1907809909(&returnValue);
	____iiterable_1_t1786350999->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Windows.Foundation.Rect>::First()
extern "C"  RuntimeObject* IIterable_1_First_m516406116 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m516406116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t316510947* ____iiterable_1_t316510947 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t316510947::IID, reinterpret_cast<void**>(&____iiterable_1_t316510947));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t569256960* returnValue = NULL;
	hr = ____iiterable_1_t316510947->IIterable_1_First_m516406116(&returnValue);
	____iiterable_1_t316510947->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Windows.Foundation.Size>::First()
extern "C"  RuntimeObject* IIterable_1_First_m4003146197 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m4003146197_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t2467282394* ____iiterable_1_t2467282394 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t2467282394::IID, reinterpret_cast<void**>(&____iiterable_1_t2467282394));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t2720028407* returnValue = NULL;
	hr = ____iiterable_1_t2467282394->IIterable_1_First_m4003146197(&returnValue);
	____iiterable_1_t2467282394->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Windows.Media.SpeechSynthesis.IVoiceInformation>::First()
extern "C"  RuntimeObject* IIterable_1_First_m1025496468 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m1025496468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t732453479* ____iiterable_1_t732453479 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t732453479::IID, reinterpret_cast<void**>(&____iiterable_1_t732453479));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t985199492* returnValue = NULL;
	hr = ____iiterable_1_t732453479->IIterable_1_First_m1025496468(&returnValue);
	____iiterable_1_t732453479->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Windows.Media.SpeechSynthesis.VoiceInformation>::First()
extern "C"  RuntimeObject* IIterable_1_First_m4104204176 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m4104204176_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t1887802092* ____iiterable_1_t1887802092 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t1887802092::IID, reinterpret_cast<void**>(&____iiterable_1_t1887802092));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t2140548105* returnValue = NULL;
	hr = ____iiterable_1_t1887802092->IIterable_1_First_m4104204176(&returnValue);
	____iiterable_1_t1887802092->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState>::First()
extern "C"  RuntimeObject* IIterable_1_First_m200553690 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m200553690_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t3275194559* ____iiterable_1_t3275194559 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t3275194559::IID, reinterpret_cast<void**>(&____iiterable_1_t3275194559));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t3527940572* returnValue = NULL;
	hr = ____iiterable_1_t3275194559->IIterable_1_First_m200553690(&returnValue);
	____iiterable_1_t3275194559->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState2>::First()
extern "C"  RuntimeObject* IIterable_1_First_m2575532575 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m2575532575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t598835391* ____iiterable_1_t598835391 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t598835391::IID, reinterpret_cast<void**>(&____iiterable_1_t598835391));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t851581404* returnValue = NULL;
	hr = ____iiterable_1_t598835391->IIterable_1_First_m2575532575(&returnValue);
	____iiterable_1_t598835391->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Windows.UI.Input.Spatial.SpatialInteractionSourceState>::First()
extern "C"  RuntimeObject* IIterable_1_First_m1657544952 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m1657544952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t3132052819* ____iiterable_1_t3132052819 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t3132052819::IID, reinterpret_cast<void**>(&____iiterable_1_t3132052819));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t3384798832* returnValue = NULL;
	hr = ____iiterable_1_t3132052819->IIterable_1_First_m1657544952(&returnValue);
	____iiterable_1_t3132052819->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
