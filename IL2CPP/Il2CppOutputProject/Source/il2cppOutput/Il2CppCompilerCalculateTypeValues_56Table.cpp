﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Generic.Stack`1<System.Int32>
struct Stack_1_t3794335208;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// UnityEngine.WSA.AppCallbackItem
struct AppCallbackItem_t4184449723;
// HoloToolkit.Unity.Solver
struct Solver_t4167981057;
// System.String
struct String_t;
// System.Comparison`1<HoloToolkit.Unity.RaycastResultHelper>
struct Comparison_1_t1782916195;
// UnityEngine.Collider
struct Collider_t1773347010;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Threading.Tasks.Task`1<System.Boolean>
struct Task_1_t1502828140;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_t923100567;
// System.Action
struct Action_t1264377477;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t2326897723;
// System.Threading.Tasks.Task
struct Task_t3187275312;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// Windows.ApplicationModel.Core.CoreApplicationView
struct CoreApplicationView_t1335682569;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// UnityEngine.U2D.SpriteAtlas
struct SpriteAtlas_t646931412;
// HoloToolkit.Unity.SolverHandler
struct SolverHandler_t1963548039;
// HoloToolkit.Unity.FadeManager
struct FadeManager_t1688783044;
// HoloToolkit.Unity.DebugPanel
struct DebugPanel_t2851753240;
// System.Collections.Concurrent.ConcurrentDictionary`2<System.Int32,System.Action`1<System.Object>>
struct ConcurrentDictionary_2_t1542073159;
// UnityEngine.BoxCollider
struct BoxCollider_t1640800422;
// HoloToolkit.Unity.Interpolator
struct Interpolator_t3604653897;
// UnityEngine.Plane[]
struct PlaneU5BU5D_t3656189108;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// HoloToolkit.Unity.AdaptiveQuality
struct AdaptiveQuality_t1304437679;
// HoloToolkit.Unity.SceneLauncher
struct SceneLauncher_t1552664439;
// HoloToolkit.Unity.InputModule.MotionControllerInfo
struct MotionControllerInfo_t938152223;
// System.Collections.Generic.List`1<HoloToolkit.Unity.SpatialUnderstandingDll/MeshData>
struct List_1_t538943048;
// UnityEngine.Camera
struct Camera_t4157153871;
// System.Collections.Generic.Dictionary`2<System.UInt32,HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState>
struct Dictionary_2_t1714548309;
// UnityEngine.UI.Text
struct Text_t1901882714;
// System.String[]
struct StringU5BU5D_t1281789340;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// HoloToolkit.Unity.AdaptiveQuality/QualityChangedEvent
struct QualityChangedEvent_t3536656938;
// System.Collections.Generic.Queue`1<System.Single>
struct Queue_1_t1243526268;
// HoloToolkit.Unity.SceneLauncher/SceneMapping[]
struct SceneMappingU5BU5D_t1612672394;
// HoloToolkit.Unity.SceneLauncherButton
struct SceneLauncherButton_t1133275438;
// System.Collections.Generic.Queue`1<System.String>
struct Queue_1_t1693710183;
// System.Collections.Generic.List`1<HoloToolkit.Unity.DebugPanel/GetLogLine>
struct List_1_t2468529069;
// System.Collections.Generic.List`1<HoloToolkit.Unity.Solver>
struct List_1_t1345088503;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef HASHCODES_T2825567999_H
#define HASHCODES_T2825567999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.HashCodes
struct  HashCodes_t2825567999  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHCODES_T2825567999_H
#ifndef BITMANIPULATOR_T1741666884_H
#define BITMANIPULATOR_T1741666884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.BitManipulator
struct  BitManipulator_t1741666884  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.BitManipulator::mask
	int32_t ___mask_0;
	// System.Int32 HoloToolkit.Unity.BitManipulator::shift
	int32_t ___shift_1;

public:
	inline static int32_t get_offset_of_mask_0() { return static_cast<int32_t>(offsetof(BitManipulator_t1741666884, ___mask_0)); }
	inline int32_t get_mask_0() const { return ___mask_0; }
	inline int32_t* get_address_of_mask_0() { return &___mask_0; }
	inline void set_mask_0(int32_t value)
	{
		___mask_0 = value;
	}

	inline static int32_t get_offset_of_shift_1() { return static_cast<int32_t>(offsetof(BitManipulator_t1741666884, ___shift_1)); }
	inline int32_t get_shift_1() const { return ___shift_1; }
	inline int32_t* get_address_of_shift_1() { return &___shift_1; }
	inline void set_shift_1(int32_t value)
	{
		___shift_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITMANIPULATOR_T1741666884_H
#ifndef GPUTIMING_T2571224352_H
#define GPUTIMING_T2571224352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.GpuTiming
struct  GpuTiming_t2571224352  : public RuntimeObject
{
public:

public:
};

struct GpuTiming_t2571224352_StaticFields
{
public:
	// System.Int32 HoloToolkit.Unity.GpuTiming::nextAvailableEventId
	int32_t ___nextAvailableEventId_2;
	// System.Collections.Generic.Stack`1<System.Int32> HoloToolkit.Unity.GpuTiming::currentEventId
	Stack_1_t3794335208 * ___currentEventId_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> HoloToolkit.Unity.GpuTiming::eventIds
	Dictionary_2_t2736202052 * ___eventIds_4;

public:
	inline static int32_t get_offset_of_nextAvailableEventId_2() { return static_cast<int32_t>(offsetof(GpuTiming_t2571224352_StaticFields, ___nextAvailableEventId_2)); }
	inline int32_t get_nextAvailableEventId_2() const { return ___nextAvailableEventId_2; }
	inline int32_t* get_address_of_nextAvailableEventId_2() { return &___nextAvailableEventId_2; }
	inline void set_nextAvailableEventId_2(int32_t value)
	{
		___nextAvailableEventId_2 = value;
	}

	inline static int32_t get_offset_of_currentEventId_3() { return static_cast<int32_t>(offsetof(GpuTiming_t2571224352_StaticFields, ___currentEventId_3)); }
	inline Stack_1_t3794335208 * get_currentEventId_3() const { return ___currentEventId_3; }
	inline Stack_1_t3794335208 ** get_address_of_currentEventId_3() { return &___currentEventId_3; }
	inline void set_currentEventId_3(Stack_1_t3794335208 * value)
	{
		___currentEventId_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentEventId_3), value);
	}

	inline static int32_t get_offset_of_eventIds_4() { return static_cast<int32_t>(offsetof(GpuTiming_t2571224352_StaticFields, ___eventIds_4)); }
	inline Dictionary_2_t2736202052 * get_eventIds_4() const { return ___eventIds_4; }
	inline Dictionary_2_t2736202052 ** get_address_of_eventIds_4() { return &___eventIds_4; }
	inline void set_eventIds_4(Dictionary_2_t2736202052 * value)
	{
		___eventIds_4 = value;
		Il2CppCodeGenWriteBarrier((&___eventIds_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GPUTIMING_T2571224352_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef CIRCULARBUFFER_T1956967364_H
#define CIRCULARBUFFER_T1956967364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.CircularBuffer
struct  CircularBuffer_t1956967364  : public RuntimeObject
{
public:
	// System.Byte[] HoloToolkit.Unity.CircularBuffer::data
	ByteU5BU5D_t4116647657* ___data_0;
	// System.Int32 HoloToolkit.Unity.CircularBuffer::writeOffset
	int32_t ___writeOffset_1;
	// System.Int32 HoloToolkit.Unity.CircularBuffer::readOffset
	int32_t ___readOffset_2;
	// System.Int32 HoloToolkit.Unity.CircularBuffer::readWritePadding
	int32_t ___readWritePadding_3;
	// System.Boolean HoloToolkit.Unity.CircularBuffer::allowOverwrite
	bool ___allowOverwrite_4;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(CircularBuffer_t1956967364, ___data_0)); }
	inline ByteU5BU5D_t4116647657* get_data_0() const { return ___data_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ByteU5BU5D_t4116647657* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_writeOffset_1() { return static_cast<int32_t>(offsetof(CircularBuffer_t1956967364, ___writeOffset_1)); }
	inline int32_t get_writeOffset_1() const { return ___writeOffset_1; }
	inline int32_t* get_address_of_writeOffset_1() { return &___writeOffset_1; }
	inline void set_writeOffset_1(int32_t value)
	{
		___writeOffset_1 = value;
	}

	inline static int32_t get_offset_of_readOffset_2() { return static_cast<int32_t>(offsetof(CircularBuffer_t1956967364, ___readOffset_2)); }
	inline int32_t get_readOffset_2() const { return ___readOffset_2; }
	inline int32_t* get_address_of_readOffset_2() { return &___readOffset_2; }
	inline void set_readOffset_2(int32_t value)
	{
		___readOffset_2 = value;
	}

	inline static int32_t get_offset_of_readWritePadding_3() { return static_cast<int32_t>(offsetof(CircularBuffer_t1956967364, ___readWritePadding_3)); }
	inline int32_t get_readWritePadding_3() const { return ___readWritePadding_3; }
	inline int32_t* get_address_of_readWritePadding_3() { return &___readWritePadding_3; }
	inline void set_readWritePadding_3(int32_t value)
	{
		___readWritePadding_3 = value;
	}

	inline static int32_t get_offset_of_allowOverwrite_4() { return static_cast<int32_t>(offsetof(CircularBuffer_t1956967364, ___allowOverwrite_4)); }
	inline bool get_allowOverwrite_4() const { return ___allowOverwrite_4; }
	inline bool* get_address_of_allowOverwrite_4() { return &___allowOverwrite_4; }
	inline void set_allowOverwrite_4(bool value)
	{
		___allowOverwrite_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIRCULARBUFFER_T1956967364_H
#ifndef U3CU3EC_T2509199788_H
#define U3CU3EC_T2509199788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ApplicationViewManager/<>c
struct  U3CU3Ec_t2509199788  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2509199788_StaticFields
{
public:
	// HoloToolkit.Unity.ApplicationViewManager/<>c HoloToolkit.Unity.ApplicationViewManager/<>c::<>9
	U3CU3Ec_t2509199788 * ___U3CU3E9_0;
	// UnityEngine.WSA.AppCallbackItem HoloToolkit.Unity.ApplicationViewManager/<>c::<>9__0_0
	AppCallbackItem_t4184449723 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2509199788_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2509199788 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2509199788 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2509199788 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2509199788_StaticFields, ___U3CU3E9__0_0_1)); }
	inline AppCallbackItem_t4184449723 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline AppCallbackItem_t4184449723 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(AppCallbackItem_t4184449723 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2509199788_H
#ifndef SPATIALUNDERSTANDINGDLLTOPOLOGY_T221813560_H
#define SPATIALUNDERSTANDINGDLLTOPOLOGY_T221813560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllTopology
struct  SpatialUnderstandingDllTopology_t221813560  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALUNDERSTANDINGDLLTOPOLOGY_T221813560_H
#ifndef U3CCOSTARTU3ED__15_T802829203_H
#define U3CCOSTARTU3ED__15_T802829203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Solver/<CoStart>d__15
struct  U3CCoStartU3Ed__15_t802829203  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.Solver/<CoStart>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.Solver/<CoStart>d__15::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.Solver HoloToolkit.Unity.Solver/<CoStart>d__15::<>4__this
	Solver_t4167981057 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCoStartU3Ed__15_t802829203, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCoStartU3Ed__15_t802829203, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCoStartU3Ed__15_t802829203, ___U3CU3E4__this_2)); }
	inline Solver_t4167981057 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Solver_t4167981057 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Solver_t4167981057 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOSTARTU3ED__15_T802829203_H
#ifndef READONLYHASHSETRELATEDEXTENSIONS_T2711452126_H
#define READONLYHASHSETRELATEDEXTENSIONS_T2711452126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ReadOnlyHashSetRelatedExtensions
struct  ReadOnlyHashSetRelatedExtensions_t2711452126  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYHASHSETRELATEDEXTENSIONS_T2711452126_H
#ifndef SCENEMAPPING_T92329067_H
#define SCENEMAPPING_T92329067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SceneLauncher/SceneMapping
struct  SceneMapping_t92329067  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.SceneLauncher/SceneMapping::ScenePath
	String_t* ___ScenePath_0;
	// System.Boolean HoloToolkit.Unity.SceneLauncher/SceneMapping::IsButtonEnabled
	bool ___IsButtonEnabled_1;

public:
	inline static int32_t get_offset_of_ScenePath_0() { return static_cast<int32_t>(offsetof(SceneMapping_t92329067, ___ScenePath_0)); }
	inline String_t* get_ScenePath_0() const { return ___ScenePath_0; }
	inline String_t** get_address_of_ScenePath_0() { return &___ScenePath_0; }
	inline void set_ScenePath_0(String_t* value)
	{
		___ScenePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___ScenePath_0), value);
	}

	inline static int32_t get_offset_of_IsButtonEnabled_1() { return static_cast<int32_t>(offsetof(SceneMapping_t92329067, ___IsButtonEnabled_1)); }
	inline bool get_IsButtonEnabled_1() const { return ___IsButtonEnabled_1; }
	inline bool* get_address_of_IsButtonEnabled_1() { return &___IsButtonEnabled_1; }
	inline void set_IsButtonEnabled_1(bool value)
	{
		___IsButtonEnabled_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEMAPPING_T92329067_H
#ifndef SPATIALUNDERSTANDINGDLLSHAPES_T2489144933_H
#define SPATIALUNDERSTANDINGDLLSHAPES_T2489144933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes
struct  SpatialUnderstandingDllShapes_t2489144933  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALUNDERSTANDINGDLLSHAPES_T2489144933_H
#ifndef U3CU3EC_T4080613386_H
#define U3CU3EC_T4080613386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RaycastHelper/<>c
struct  U3CU3Ec_t4080613386  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4080613386_StaticFields
{
public:
	// HoloToolkit.Unity.RaycastHelper/<>c HoloToolkit.Unity.RaycastHelper/<>c::<>9
	U3CU3Ec_t4080613386 * ___U3CU3E9_0;
	// System.Comparison`1<HoloToolkit.Unity.RaycastResultHelper> HoloToolkit.Unity.RaycastHelper/<>c::<>9__5_1
	Comparison_1_t1782916195 * ___U3CU3E9__5_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4080613386_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4080613386 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4080613386 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4080613386 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4080613386_StaticFields, ___U3CU3E9__5_1_1)); }
	inline Comparison_1_t1782916195 * get_U3CU3E9__5_1_1() const { return ___U3CU3E9__5_1_1; }
	inline Comparison_1_t1782916195 ** get_address_of_U3CU3E9__5_1_1() { return &___U3CU3E9__5_1_1; }
	inline void set_U3CU3E9__5_1_1(Comparison_1_t1782916195 * value)
	{
		___U3CU3E9__5_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4080613386_H
#ifndef RAYCASTHELPER_T3966188783_H
#define RAYCASTHELPER_T3966188783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RaycastHelper
struct  RaycastHelper_t3966188783  : public RuntimeObject
{
public:

public:
};

struct RaycastHelper_t3966188783_StaticFields
{
public:
	// System.Boolean HoloToolkit.Unity.RaycastHelper::DebugEnabled
	bool ___DebugEnabled_0;

public:
	inline static int32_t get_offset_of_DebugEnabled_0() { return static_cast<int32_t>(offsetof(RaycastHelper_t3966188783_StaticFields, ___DebugEnabled_0)); }
	inline bool get_DebugEnabled_0() const { return ___DebugEnabled_0; }
	inline bool* get_address_of_DebugEnabled_0() { return &___DebugEnabled_0; }
	inline void set_DebugEnabled_0(bool value)
	{
		___DebugEnabled_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHELPER_T3966188783_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T860465322_H
#define U3CU3EC__DISPLAYCLASS5_0_T860465322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RaycastHelper/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t860465322  : public RuntimeObject
{
public:
	// UnityEngine.Collider HoloToolkit.Unity.RaycastHelper/<>c__DisplayClass5_0::collider
	Collider_t1773347010 * ___collider_0;

public:
	inline static int32_t get_offset_of_collider_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t860465322, ___collider_0)); }
	inline Collider_t1773347010 * get_collider_0() const { return ___collider_0; }
	inline Collider_t1773347010 ** get_address_of_collider_0() { return &___collider_0; }
	inline void set_collider_0(Collider_t1773347010 * value)
	{
		___collider_0 = value;
		Il2CppCodeGenWriteBarrier((&___collider_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T860465322_H
#ifndef INPUTMAPPINGAXISUTILITY_T387181254_H
#define INPUTMAPPINGAXISUTILITY_T387181254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputMappingAxisUtility
struct  InputMappingAxisUtility_t387181254  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMAPPINGAXISUTILITY_T387181254_H
#ifndef SETINDENTATTRIBUTE_T768412030_H
#define SETINDENTATTRIBUTE_T768412030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SetIndentAttribute
struct  SetIndentAttribute_t768412030  : public Attribute_t861562559
{
public:
	// System.Int32 HoloToolkit.Unity.SetIndentAttribute::<Indent>k__BackingField
	int32_t ___U3CIndentU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIndentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SetIndentAttribute_t768412030, ___U3CIndentU3Ek__BackingField_0)); }
	inline int32_t get_U3CIndentU3Ek__BackingField_0() const { return ___U3CIndentU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIndentU3Ek__BackingField_0() { return &___U3CIndentU3Ek__BackingField_0; }
	inline void set_U3CIndentU3Ek__BackingField_0(int32_t value)
	{
		___U3CIndentU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETINDENTATTRIBUTE_T768412030_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef SHOWIFATTRIBUTE_T4062566328_H
#define SHOWIFATTRIBUTE_T4062566328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ShowIfAttribute
struct  ShowIfAttribute_t4062566328  : public Attribute_t861562559
{
public:
	// System.String HoloToolkit.Unity.ShowIfAttribute::<MemberName>k__BackingField
	String_t* ___U3CMemberNameU3Ek__BackingField_0;
	// System.Boolean HoloToolkit.Unity.ShowIfAttribute::<ShowIfConditionMet>k__BackingField
	bool ___U3CShowIfConditionMetU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMemberNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ShowIfAttribute_t4062566328, ___U3CMemberNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CMemberNameU3Ek__BackingField_0() const { return ___U3CMemberNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMemberNameU3Ek__BackingField_0() { return &___U3CMemberNameU3Ek__BackingField_0; }
	inline void set_U3CMemberNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CMemberNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CShowIfConditionMetU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ShowIfAttribute_t4062566328, ___U3CShowIfConditionMetU3Ek__BackingField_1)); }
	inline bool get_U3CShowIfConditionMetU3Ek__BackingField_1() const { return ___U3CShowIfConditionMetU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CShowIfConditionMetU3Ek__BackingField_1() { return &___U3CShowIfConditionMetU3Ek__BackingField_1; }
	inline void set_U3CShowIfConditionMetU3Ek__BackingField_1(bool value)
	{
		___U3CShowIfConditionMetU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWIFATTRIBUTE_T4062566328_H
#ifndef TUTORIALATTRIBUTE_T3869340965_H
#define TUTORIALATTRIBUTE_T3869340965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TutorialAttribute
struct  TutorialAttribute_t3869340965  : public Attribute_t861562559
{
public:
	// System.String HoloToolkit.Unity.TutorialAttribute::<TutorialURL>k__BackingField
	String_t* ___U3CTutorialURLU3Ek__BackingField_0;
	// System.String HoloToolkit.Unity.TutorialAttribute::<Description>k__BackingField
	String_t* ___U3CDescriptionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTutorialURLU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TutorialAttribute_t3869340965, ___U3CTutorialURLU3Ek__BackingField_0)); }
	inline String_t* get_U3CTutorialURLU3Ek__BackingField_0() const { return ___U3CTutorialURLU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTutorialURLU3Ek__BackingField_0() { return &___U3CTutorialURLU3Ek__BackingField_0; }
	inline void set_U3CTutorialURLU3Ek__BackingField_0(String_t* value)
	{
		___U3CTutorialURLU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTutorialURLU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CDescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TutorialAttribute_t3869340965, ___U3CDescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CDescriptionU3Ek__BackingField_1() const { return ___U3CDescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CDescriptionU3Ek__BackingField_1() { return &___U3CDescriptionU3Ek__BackingField_1; }
	inline void set_U3CDescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CDescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDescriptionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALATTRIBUTE_T3869340965_H
#ifndef USEWITHATTRIBUTE_T4112209736_H
#define USEWITHATTRIBUTE_T4112209736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UseWithAttribute
struct  UseWithAttribute_t4112209736  : public Attribute_t861562559
{
public:
	// System.Type[] HoloToolkit.Unity.UseWithAttribute::<UseWithTypes>k__BackingField
	TypeU5BU5D_t3940880105* ___U3CUseWithTypesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CUseWithTypesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UseWithAttribute_t4112209736, ___U3CUseWithTypesU3Ek__BackingField_0)); }
	inline TypeU5BU5D_t3940880105* get_U3CUseWithTypesU3Ek__BackingField_0() const { return ___U3CUseWithTypesU3Ek__BackingField_0; }
	inline TypeU5BU5D_t3940880105** get_address_of_U3CUseWithTypesU3Ek__BackingField_0() { return &___U3CUseWithTypesU3Ek__BackingField_0; }
	inline void set_U3CUseWithTypesU3Ek__BackingField_0(TypeU5BU5D_t3940880105* value)
	{
		___U3CUseWithTypesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUseWithTypesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USEWITHATTRIBUTE_T4112209736_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef KEYFRAME_T4206410242_H
#define KEYFRAME_T4206410242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Keyframe
struct  Keyframe_t4206410242 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYFRAME_T4206410242_H
#ifndef TASKAWAITER_1_T2891770396_H
#define TASKAWAITER_1_T2891770396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter`1<System.Boolean>
struct  TaskAwaiter_1_t2891770396 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.TaskAwaiter`1::m_task
	Task_1_t1502828140 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_1_t2891770396, ___m_task_0)); }
	inline Task_1_t1502828140 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t1502828140 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t1502828140 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKAWAITER_1_T2891770396_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef ASYNCMETHODBUILDERCORE_T2955600131_H
#define ASYNCMETHODBUILDERCORE_T2955600131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t2955600131 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t1264377477 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2955600131, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_stateMachine_0), value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2955600131, ___m_defaultContextAction_1)); }
	inline Action_t1264377477 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t1264377477 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t1264377477 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultContextAction_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2955600131_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2955600131_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
#endif // ASYNCMETHODBUILDERCORE_T2955600131_H
#ifndef FEATUREINPROGRESSATTRIBUTE_T1665528438_H
#define FEATUREINPROGRESSATTRIBUTE_T1665528438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.FeatureInProgressAttribute
struct  FeatureInProgressAttribute_t1665528438  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATUREINPROGRESSATTRIBUTE_T1665528438_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef DRAWOVERRIDEATTRIBUTE_T485819796_H
#define DRAWOVERRIDEATTRIBUTE_T485819796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DrawOverrideAttribute
struct  DrawOverrideAttribute_t485819796  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWOVERRIDEATTRIBUTE_T485819796_H
#ifndef DOCLINKATTRIBUTE_T684588366_H
#define DOCLINKATTRIBUTE_T684588366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DocLinkAttribute
struct  DocLinkAttribute_t684588366  : public Attribute_t861562559
{
public:
	// System.String HoloToolkit.Unity.DocLinkAttribute::<DocURL>k__BackingField
	String_t* ___U3CDocURLU3Ek__BackingField_0;
	// System.String HoloToolkit.Unity.DocLinkAttribute::<Description>k__BackingField
	String_t* ___U3CDescriptionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CDocURLU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DocLinkAttribute_t684588366, ___U3CDocURLU3Ek__BackingField_0)); }
	inline String_t* get_U3CDocURLU3Ek__BackingField_0() const { return ___U3CDocURLU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CDocURLU3Ek__BackingField_0() { return &___U3CDocURLU3Ek__BackingField_0; }
	inline void set_U3CDocURLU3Ek__BackingField_0(String_t* value)
	{
		___U3CDocURLU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDocURLU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CDescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DocLinkAttribute_t684588366, ___U3CDescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CDescriptionU3Ek__BackingField_1() const { return ___U3CDescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CDescriptionU3Ek__BackingField_1() { return &___U3CDescriptionU3Ek__BackingField_1; }
	inline void set_U3CDescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CDescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDescriptionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCLINKATTRIBUTE_T684588366_H
#ifndef DRAWLASTATTRIBUTE_T330304297_H
#define DRAWLASTATTRIBUTE_T330304297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DrawLastAttribute
struct  DrawLastAttribute_t330304297  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWLASTATTRIBUTE_T330304297_H
#ifndef VECTOR3SMOOTHED_T107253641_H
#define VECTOR3SMOOTHED_T107253641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverHandler/Vector3Smoothed
struct  Vector3Smoothed_t107253641 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverHandler/Vector3Smoothed::<Current>k__BackingField
	Vector3_t3722313464  ___U3CCurrentU3Ek__BackingField_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverHandler/Vector3Smoothed::<Goal>k__BackingField
	Vector3_t3722313464  ___U3CGoalU3Ek__BackingField_1;
	// System.Single HoloToolkit.Unity.SolverHandler/Vector3Smoothed::<SmoothTime>k__BackingField
	float ___U3CSmoothTimeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCurrentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Vector3Smoothed_t107253641, ___U3CCurrentU3Ek__BackingField_0)); }
	inline Vector3_t3722313464  get_U3CCurrentU3Ek__BackingField_0() const { return ___U3CCurrentU3Ek__BackingField_0; }
	inline Vector3_t3722313464 * get_address_of_U3CCurrentU3Ek__BackingField_0() { return &___U3CCurrentU3Ek__BackingField_0; }
	inline void set_U3CCurrentU3Ek__BackingField_0(Vector3_t3722313464  value)
	{
		___U3CCurrentU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CGoalU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Vector3Smoothed_t107253641, ___U3CGoalU3Ek__BackingField_1)); }
	inline Vector3_t3722313464  get_U3CGoalU3Ek__BackingField_1() const { return ___U3CGoalU3Ek__BackingField_1; }
	inline Vector3_t3722313464 * get_address_of_U3CGoalU3Ek__BackingField_1() { return &___U3CGoalU3Ek__BackingField_1; }
	inline void set_U3CGoalU3Ek__BackingField_1(Vector3_t3722313464  value)
	{
		___U3CGoalU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CSmoothTimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vector3Smoothed_t107253641, ___U3CSmoothTimeU3Ek__BackingField_2)); }
	inline float get_U3CSmoothTimeU3Ek__BackingField_2() const { return ___U3CSmoothTimeU3Ek__BackingField_2; }
	inline float* get_address_of_U3CSmoothTimeU3Ek__BackingField_2() { return &___U3CSmoothTimeU3Ek__BackingField_2; }
	inline void set_U3CSmoothTimeU3Ek__BackingField_2(float value)
	{
		___U3CSmoothTimeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3SMOOTHED_T107253641_H
#ifndef ASYNCVOIDMETHODBUILDER_T3819840891_H
#define ASYNCVOIDMETHODBUILDER_T3819840891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct  AsyncVoidMethodBuilder_t3819840891 
{
public:
	// System.Threading.SynchronizationContext System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_synchronizationContext
	SynchronizationContext_t2326897723 * ___m_synchronizationContext_0;
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_coreState
	AsyncMethodBuilderCore_t2955600131  ___m_coreState_1;
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_task
	Task_t3187275312 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_synchronizationContext_0() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_t3819840891, ___m_synchronizationContext_0)); }
	inline SynchronizationContext_t2326897723 * get_m_synchronizationContext_0() const { return ___m_synchronizationContext_0; }
	inline SynchronizationContext_t2326897723 ** get_address_of_m_synchronizationContext_0() { return &___m_synchronizationContext_0; }
	inline void set_m_synchronizationContext_0(SynchronizationContext_t2326897723 * value)
	{
		___m_synchronizationContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_synchronizationContext_0), value);
	}

	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_t3819840891, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2955600131  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2955600131 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2955600131  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_t3819840891, ___m_task_2)); }
	inline Task_t3187275312 * get_m_task_2() const { return ___m_task_2; }
	inline Task_t3187275312 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_t3187275312 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_t3819840891_marshaled_pinvoke
{
	SynchronizationContext_t2326897723 * ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_t2955600131_marshaled_pinvoke ___m_coreState_1;
	Task_t3187275312 * ___m_task_2;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_t3819840891_marshaled_com
{
	SynchronizationContext_t2326897723 * ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_t2955600131_marshaled_com ___m_coreState_1;
	Task_t3187275312 * ___m_task_2;
};
#endif // ASYNCVOIDMETHODBUILDER_T3819840891_H
#ifndef OBJECTPLACEMENTRESULT_T4094328837_H
#define OBJECTPLACEMENTRESULT_T4094328837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementResult
#pragma pack(push, tp, 1)
struct  ObjectPlacementResult_t4094328837  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementResult::Position
	Vector3_t3722313464  ___Position_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementResult::HalfDims
	Vector3_t3722313464  ___HalfDims_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementResult::Forward
	Vector3_t3722313464  ___Forward_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementResult::Right
	Vector3_t3722313464  ___Right_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementResult::Up
	Vector3_t3722313464  ___Up_4;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(ObjectPlacementResult_t4094328837, ___Position_0)); }
	inline Vector3_t3722313464  get_Position_0() const { return ___Position_0; }
	inline Vector3_t3722313464 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector3_t3722313464  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_HalfDims_1() { return static_cast<int32_t>(offsetof(ObjectPlacementResult_t4094328837, ___HalfDims_1)); }
	inline Vector3_t3722313464  get_HalfDims_1() const { return ___HalfDims_1; }
	inline Vector3_t3722313464 * get_address_of_HalfDims_1() { return &___HalfDims_1; }
	inline void set_HalfDims_1(Vector3_t3722313464  value)
	{
		___HalfDims_1 = value;
	}

	inline static int32_t get_offset_of_Forward_2() { return static_cast<int32_t>(offsetof(ObjectPlacementResult_t4094328837, ___Forward_2)); }
	inline Vector3_t3722313464  get_Forward_2() const { return ___Forward_2; }
	inline Vector3_t3722313464 * get_address_of_Forward_2() { return &___Forward_2; }
	inline void set_Forward_2(Vector3_t3722313464  value)
	{
		___Forward_2 = value;
	}

	inline static int32_t get_offset_of_Right_3() { return static_cast<int32_t>(offsetof(ObjectPlacementResult_t4094328837, ___Right_3)); }
	inline Vector3_t3722313464  get_Right_3() const { return ___Right_3; }
	inline Vector3_t3722313464 * get_address_of_Right_3() { return &___Right_3; }
	inline void set_Right_3(Vector3_t3722313464  value)
	{
		___Right_3 = value;
	}

	inline static int32_t get_offset_of_Up_4() { return static_cast<int32_t>(offsetof(ObjectPlacementResult_t4094328837, ___Up_4)); }
	inline Vector3_t3722313464  get_Up_4() const { return ___Up_4; }
	inline Vector3_t3722313464 * get_address_of_Up_4() { return &___Up_4; }
	inline void set_Up_4(Vector3_t3722313464  value)
	{
		___Up_4 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementResult
#pragma pack(push, tp, 1)
struct ObjectPlacementResult_t4094328837_marshaled_pinvoke
{
	Vector3_t3722313464  ___Position_0;
	Vector3_t3722313464  ___HalfDims_1;
	Vector3_t3722313464  ___Forward_2;
	Vector3_t3722313464  ___Right_3;
	Vector3_t3722313464  ___Up_4;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementResult
#pragma pack(push, tp, 1)
struct ObjectPlacementResult_t4094328837_marshaled_com
{
	Vector3_t3722313464  ___Position_0;
	Vector3_t3722313464  ___HalfDims_1;
	Vector3_t3722313464  ___Forward_2;
	Vector3_t3722313464  ___Right_3;
	Vector3_t3722313464  ___Up_4;
};
#pragma pack(pop, tp)
#endif // OBJECTPLACEMENTRESULT_T4094328837_H
#ifndef FADESTATE_T4119448579_H
#define FADESTATE_T4119448579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.FadeManager/FadeState
struct  FadeState_t4119448579 
{
public:
	// System.Int32 HoloToolkit.Unity.FadeManager/FadeState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FadeState_t4119448579, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADESTATE_T4119448579_H
#ifndef TRACKEDOBJECTTOREFERENCEENUM_T2659894601_H
#define TRACKEDOBJECTTOREFERENCEENUM_T2659894601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverHandler/TrackedObjectToReferenceEnum
struct  TrackedObjectToReferenceEnum_t2659894601 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverHandler/TrackedObjectToReferenceEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackedObjectToReferenceEnum_t2659894601, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKEDOBJECTTOREFERENCEENUM_T2659894601_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef CONTROLLERELEMENTENUM_T4108764020_H
#define CONTROLLERELEMENTENUM_T4108764020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.MotionControllerInfo/ControllerElementEnum
struct  ControllerElementEnum_t4108764020 
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.MotionControllerInfo/ControllerElementEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerElementEnum_t4108764020, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERELEMENTENUM_T4108764020_H
#ifndef SHAPECOMPONENT_T3281486496_H
#define SHAPECOMPONENT_T3281486496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponent
#pragma pack(push, tp, 1)
struct  ShapeComponent_t3281486496 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponent::ConstraintCount
	int32_t ___ConstraintCount_0;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponent::Constraints
	intptr_t ___Constraints_1;

public:
	inline static int32_t get_offset_of_ConstraintCount_0() { return static_cast<int32_t>(offsetof(ShapeComponent_t3281486496, ___ConstraintCount_0)); }
	inline int32_t get_ConstraintCount_0() const { return ___ConstraintCount_0; }
	inline int32_t* get_address_of_ConstraintCount_0() { return &___ConstraintCount_0; }
	inline void set_ConstraintCount_0(int32_t value)
	{
		___ConstraintCount_0 = value;
	}

	inline static int32_t get_offset_of_Constraints_1() { return static_cast<int32_t>(offsetof(ShapeComponent_t3281486496, ___Constraints_1)); }
	inline intptr_t get_Constraints_1() const { return ___Constraints_1; }
	inline intptr_t* get_address_of_Constraints_1() { return &___Constraints_1; }
	inline void set_Constraints_1(intptr_t value)
	{
		___Constraints_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPECOMPONENT_T3281486496_H
#ifndef RAYCASTRESULTHELPER_T2007985016_H
#define RAYCASTRESULTHELPER_T2007985016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RaycastResultHelper
struct  RaycastResultHelper_t2007985016 
{
public:
	// UnityEngine.Collider HoloToolkit.Unity.RaycastResultHelper::collider
	Collider_t1773347010 * ___collider_0;
	// System.Int32 HoloToolkit.Unity.RaycastResultHelper::layer
	int32_t ___layer_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.RaycastResultHelper::normal
	Vector3_t3722313464  ___normal_2;
	// System.Single HoloToolkit.Unity.RaycastResultHelper::distance
	float ___distance_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.RaycastResultHelper::point
	Vector3_t3722313464  ___point_4;
	// UnityEngine.Transform HoloToolkit.Unity.RaycastResultHelper::transform
	Transform_t3600365921 * ___transform_5;
	// UnityEngine.Vector2 HoloToolkit.Unity.RaycastResultHelper::textureCoord
	Vector2_t2156229523  ___textureCoord_6;
	// UnityEngine.Vector2 HoloToolkit.Unity.RaycastResultHelper::textureCoord2
	Vector2_t2156229523  ___textureCoord2_7;

public:
	inline static int32_t get_offset_of_collider_0() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___collider_0)); }
	inline Collider_t1773347010 * get_collider_0() const { return ___collider_0; }
	inline Collider_t1773347010 ** get_address_of_collider_0() { return &___collider_0; }
	inline void set_collider_0(Collider_t1773347010 * value)
	{
		___collider_0 = value;
		Il2CppCodeGenWriteBarrier((&___collider_0), value);
	}

	inline static int32_t get_offset_of_layer_1() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___layer_1)); }
	inline int32_t get_layer_1() const { return ___layer_1; }
	inline int32_t* get_address_of_layer_1() { return &___layer_1; }
	inline void set_layer_1(int32_t value)
	{
		___layer_1 = value;
	}

	inline static int32_t get_offset_of_normal_2() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___normal_2)); }
	inline Vector3_t3722313464  get_normal_2() const { return ___normal_2; }
	inline Vector3_t3722313464 * get_address_of_normal_2() { return &___normal_2; }
	inline void set_normal_2(Vector3_t3722313464  value)
	{
		___normal_2 = value;
	}

	inline static int32_t get_offset_of_distance_3() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___distance_3)); }
	inline float get_distance_3() const { return ___distance_3; }
	inline float* get_address_of_distance_3() { return &___distance_3; }
	inline void set_distance_3(float value)
	{
		___distance_3 = value;
	}

	inline static int32_t get_offset_of_point_4() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___point_4)); }
	inline Vector3_t3722313464  get_point_4() const { return ___point_4; }
	inline Vector3_t3722313464 * get_address_of_point_4() { return &___point_4; }
	inline void set_point_4(Vector3_t3722313464  value)
	{
		___point_4 = value;
	}

	inline static int32_t get_offset_of_transform_5() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___transform_5)); }
	inline Transform_t3600365921 * get_transform_5() const { return ___transform_5; }
	inline Transform_t3600365921 ** get_address_of_transform_5() { return &___transform_5; }
	inline void set_transform_5(Transform_t3600365921 * value)
	{
		___transform_5 = value;
		Il2CppCodeGenWriteBarrier((&___transform_5), value);
	}

	inline static int32_t get_offset_of_textureCoord_6() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___textureCoord_6)); }
	inline Vector2_t2156229523  get_textureCoord_6() const { return ___textureCoord_6; }
	inline Vector2_t2156229523 * get_address_of_textureCoord_6() { return &___textureCoord_6; }
	inline void set_textureCoord_6(Vector2_t2156229523  value)
	{
		___textureCoord_6 = value;
	}

	inline static int32_t get_offset_of_textureCoord2_7() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___textureCoord2_7)); }
	inline Vector2_t2156229523  get_textureCoord2_7() const { return ___textureCoord2_7; }
	inline Vector2_t2156229523 * get_address_of_textureCoord2_7() { return &___textureCoord2_7; }
	inline void set_textureCoord2_7(Vector2_t2156229523  value)
	{
		___textureCoord2_7 = value;
	}
};

struct RaycastResultHelper_t2007985016_StaticFields
{
public:
	// HoloToolkit.Unity.RaycastResultHelper HoloToolkit.Unity.RaycastResultHelper::None
	RaycastResultHelper_t2007985016  ___None_8;

public:
	inline static int32_t get_offset_of_None_8() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016_StaticFields, ___None_8)); }
	inline RaycastResultHelper_t2007985016  get_None_8() const { return ___None_8; }
	inline RaycastResultHelper_t2007985016 * get_address_of_None_8() { return &___None_8; }
	inline void set_None_8(RaycastResultHelper_t2007985016  value)
	{
		___None_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.RaycastResultHelper
struct RaycastResultHelper_t2007985016_marshaled_pinvoke
{
	Collider_t1773347010 * ___collider_0;
	int32_t ___layer_1;
	Vector3_t3722313464  ___normal_2;
	float ___distance_3;
	Vector3_t3722313464  ___point_4;
	Transform_t3600365921 * ___transform_5;
	Vector2_t2156229523  ___textureCoord_6;
	Vector2_t2156229523  ___textureCoord2_7;
};
// Native definition for COM marshalling of HoloToolkit.Unity.RaycastResultHelper
struct RaycastResultHelper_t2007985016_marshaled_com
{
	Collider_t1773347010 * ___collider_0;
	int32_t ___layer_1;
	Vector3_t3722313464  ___normal_2;
	float ___distance_3;
	Vector3_t3722313464  ___point_4;
	Transform_t3600365921 * ___transform_5;
	Vector2_t2156229523  ___textureCoord_6;
	Vector2_t2156229523  ___textureCoord2_7;
};
#endif // RAYCASTRESULTHELPER_T2007985016_H
#ifndef SHAPERESULT_T4069931198_H
#define SHAPERESULT_T4069931198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeResult
#pragma pack(push, tp, 1)
struct  ShapeResult_t4069931198 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeResult::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeResult::halfDims
	Vector3_t3722313464  ___halfDims_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(ShapeResult_t4069931198, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_halfDims_1() { return static_cast<int32_t>(offsetof(ShapeResult_t4069931198, ___halfDims_1)); }
	inline Vector3_t3722313464  get_halfDims_1() const { return ___halfDims_1; }
	inline Vector3_t3722313464 * get_address_of_halfDims_1() { return &___halfDims_1; }
	inline void set_halfDims_1(Vector3_t3722313464  value)
	{
		___halfDims_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPERESULT_T4069931198_H
#ifndef SHAPECOMPONENTCONSTRAINTTYPE_T3710497406_H
#define SHAPECOMPONENTCONSTRAINTTYPE_T3710497406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraintType
struct  ShapeComponentConstraintType_t3710497406 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraintType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShapeComponentConstraintType_t3710497406, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPECOMPONENTCONSTRAINTTYPE_T3710497406_H
#ifndef ORIENTATIONREFERENCE_T825611799_H
#define ORIENTATIONREFERENCE_T825611799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverBodyLock/OrientationReference
struct  OrientationReference_t825611799 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverBodyLock/OrientationReference::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OrientationReference_t825611799, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTATIONREFERENCE_T825611799_H
#ifndef FRUSTUMPLANES_T4124430689_H
#define FRUSTUMPLANES_T4124430689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.HeadsUpDirectionIndicator/FrustumPlanes
struct  FrustumPlanes_t4124430689 
{
public:
	// System.Int32 HoloToolkit.Unity.HeadsUpDirectionIndicator/FrustumPlanes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FrustumPlanes_t4124430689, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRUSTUMPLANES_T4124430689_H
#ifndef TOPOLOGYRESULT_T1593710726_H
#define TOPOLOGYRESULT_T1593710726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllTopology/TopologyResult
#pragma pack(push, tp, 1)
struct  TopologyResult_t1593710726 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllTopology/TopologyResult::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllTopology/TopologyResult::normal
	Vector3_t3722313464  ___normal_1;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllTopology/TopologyResult::width
	float ___width_2;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllTopology/TopologyResult::length
	float ___length_3;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TopologyResult_t1593710726, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_normal_1() { return static_cast<int32_t>(offsetof(TopologyResult_t1593710726, ___normal_1)); }
	inline Vector3_t3722313464  get_normal_1() const { return ___normal_1; }
	inline Vector3_t3722313464 * get_address_of_normal_1() { return &___normal_1; }
	inline void set_normal_1(Vector3_t3722313464  value)
	{
		___normal_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(TopologyResult_t1593710726, ___width_2)); }
	inline float get_width_2() const { return ___width_2; }
	inline float* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(float value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_length_3() { return static_cast<int32_t>(offsetof(TopologyResult_t1593710726, ___length_3)); }
	inline float get_length_3() const { return ___length_3; }
	inline float* get_address_of_length_3() { return &___length_3; }
	inline void set_length_3(float value)
	{
		___length_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOPOLOGYRESULT_T1593710726_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SHAPECONSTRAINTTYPE_T1645122175_H
#define SHAPECONSTRAINTTYPE_T1645122175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeConstraintType
struct  ShapeConstraintType_t1645122175 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeConstraintType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShapeConstraintType_t1645122175, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPECONSTRAINTTYPE_T1645122175_H
#ifndef SCALESTATEENUM_T571318243_H
#define SCALESTATEENUM_T571318243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverConstantViewSize/ScaleStateEnum
struct  ScaleStateEnum_t571318243 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverConstantViewSize/ScaleStateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScaleStateEnum_t571318243, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALESTATEENUM_T571318243_H
#ifndef SCENEGAMEOBJECTATTRIBUTE_T4082581691_H
#define SCENEGAMEOBJECTATTRIBUTE_T4082581691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SceneGameObjectAttribute
struct  SceneGameObjectAttribute_t4082581691  : public DrawOverrideAttribute_t485819796
{
public:
	// System.String HoloToolkit.Unity.SceneGameObjectAttribute::<CustomLabel>k__BackingField
	String_t* ___U3CCustomLabelU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCustomLabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SceneGameObjectAttribute_t4082581691, ___U3CCustomLabelU3Ek__BackingField_0)); }
	inline String_t* get_U3CCustomLabelU3Ek__BackingField_0() const { return ___U3CCustomLabelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCustomLabelU3Ek__BackingField_0() { return &___U3CCustomLabelU3Ek__BackingField_0; }
	inline void set_U3CCustomLabelU3Ek__BackingField_0(String_t* value)
	{
		___U3CCustomLabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomLabelU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEGAMEOBJECTATTRIBUTE_T4082581691_H
#ifndef SCENECOMPONENTATTRIBUTE_T771947740_H
#define SCENECOMPONENTATTRIBUTE_T771947740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SceneComponentAttribute
struct  SceneComponentAttribute_t771947740  : public DrawOverrideAttribute_t485819796
{
public:
	// System.String HoloToolkit.Unity.SceneComponentAttribute::<CustomLabel>k__BackingField
	String_t* ___U3CCustomLabelU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCustomLabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SceneComponentAttribute_t771947740, ___U3CCustomLabelU3Ek__BackingField_0)); }
	inline String_t* get_U3CCustomLabelU3Ek__BackingField_0() const { return ___U3CCustomLabelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCustomLabelU3Ek__BackingField_0() { return &___U3CCustomLabelU3Ek__BackingField_0; }
	inline void set_U3CCustomLabelU3Ek__BackingField_0(String_t* value)
	{
		___U3CCustomLabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomLabelU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENECOMPONENTATTRIBUTE_T771947740_H
#ifndef SAVELOCALFILEATTRIBUTE_T3766135238_H
#define SAVELOCALFILEATTRIBUTE_T3766135238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SaveLocalFileAttribute
struct  SaveLocalFileAttribute_t3766135238  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVELOCALFILEATTRIBUTE_T3766135238_H
#ifndef HIDEINMRTKINSPECTOR_T2011525198_H
#define HIDEINMRTKINSPECTOR_T2011525198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.HideInMRTKInspector
struct  HideInMRTKInspector_t2011525198  : public ShowIfAttribute_t4062566328
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINMRTKINSPECTOR_T2011525198_H
#ifndef SHOWIFBOOLVALUEATTRIBUTE_T2057683491_H
#define SHOWIFBOOLVALUEATTRIBUTE_T2057683491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ShowIfBoolValueAttribute
struct  ShowIfBoolValueAttribute_t2057683491  : public ShowIfAttribute_t4062566328
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWIFBOOLVALUEATTRIBUTE_T2057683491_H
#ifndef GRADIENTDEFAULTATTRIBUTE_T165107163_H
#define GRADIENTDEFAULTATTRIBUTE_T165107163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.GradientDefaultAttribute
struct  GradientDefaultAttribute_t165107163  : public DrawOverrideAttribute_t485819796
{
public:
	// UnityEngine.Color HoloToolkit.Unity.GradientDefaultAttribute::startColor
	Color_t2555686324  ___startColor_0;
	// UnityEngine.Color HoloToolkit.Unity.GradientDefaultAttribute::endColor
	Color_t2555686324  ___endColor_1;

public:
	inline static int32_t get_offset_of_startColor_0() { return static_cast<int32_t>(offsetof(GradientDefaultAttribute_t165107163, ___startColor_0)); }
	inline Color_t2555686324  get_startColor_0() const { return ___startColor_0; }
	inline Color_t2555686324 * get_address_of_startColor_0() { return &___startColor_0; }
	inline void set_startColor_0(Color_t2555686324  value)
	{
		___startColor_0 = value;
	}

	inline static int32_t get_offset_of_endColor_1() { return static_cast<int32_t>(offsetof(GradientDefaultAttribute_t165107163, ___endColor_1)); }
	inline Color_t2555686324  get_endColor_1() const { return ___endColor_1; }
	inline Color_t2555686324 * get_address_of_endColor_1() { return &___endColor_1; }
	inline void set_endColor_1(Color_t2555686324  value)
	{
		___endColor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTDEFAULTATTRIBUTE_T165107163_H
#ifndef OPENLOCALFILEATTRIBUTE_T4101133295_H
#define OPENLOCALFILEATTRIBUTE_T4101133295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.OpenLocalFileAttribute
struct  OpenLocalFileAttribute_t4101133295  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENLOCALFILEATTRIBUTE_T4101133295_H
#ifndef PROPERTYTYPEENUM_T4136261769_H
#define PROPERTYTYPEENUM_T4136261769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MaterialPropertyAttribute/PropertyTypeEnum
struct  PropertyTypeEnum_t4136261769 
{
public:
	// System.Int32 HoloToolkit.Unity.MaterialPropertyAttribute/PropertyTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PropertyTypeEnum_t4136261769, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPEENUM_T4136261769_H
#ifndef INTERACTIONSOURCEHANDEDNESS_T3096408347_H
#define INTERACTIONSOURCEHANDEDNESS_T3096408347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.WSA.Input.InteractionSourceHandedness
struct  InteractionSourceHandedness_t3096408347 
{
public:
	// System.Int32 UnityEngine.XR.WSA.Input.InteractionSourceHandedness::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InteractionSourceHandedness_t3096408347, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSOURCEHANDEDNESS_T3096408347_H
#ifndef TYPEENUM_T3953134253_H
#define TYPEENUM_T3953134253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RangePropAttribute/TypeEnum
struct  TypeEnum_t3953134253 
{
public:
	// System.Int32 HoloToolkit.Unity.RangePropAttribute/TypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeEnum_t3953134253, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEENUM_T3953134253_H
#ifndef COLORENUM_T1662983804_H
#define COLORENUM_T1662983804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.GradientDefaultAttribute/ColorEnum
struct  ColorEnum_t1662983804 
{
public:
	// System.Int32 HoloToolkit.Unity.GradientDefaultAttribute/ColorEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorEnum_t1662983804, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORENUM_T1662983804_H
#ifndef OPENLOCALFOLDERATTRIBUTE_T1301565505_H
#define OPENLOCALFOLDERATTRIBUTE_T1301565505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.OpenLocalFolderAttribute
struct  OpenLocalFolderAttribute_t1301565505  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENLOCALFOLDERATTRIBUTE_T1301565505_H
#ifndef DROPDOWNGAMEOBJECTATTRIBUTE_T2587315465_H
#define DROPDOWNGAMEOBJECTATTRIBUTE_T2587315465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DropDownGameObjectAttribute
struct  DropDownGameObjectAttribute_t2587315465  : public DrawOverrideAttribute_t485819796
{
public:
	// System.String HoloToolkit.Unity.DropDownGameObjectAttribute::<CustomLabel>k__BackingField
	String_t* ___U3CCustomLabelU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCustomLabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DropDownGameObjectAttribute_t2587315465, ___U3CCustomLabelU3Ek__BackingField_0)); }
	inline String_t* get_U3CCustomLabelU3Ek__BackingField_0() const { return ___U3CCustomLabelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCustomLabelU3Ek__BackingField_0() { return &___U3CCustomLabelU3Ek__BackingField_0; }
	inline void set_U3CCustomLabelU3Ek__BackingField_0(String_t* value)
	{
		___U3CCustomLabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomLabelU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWNGAMEOBJECTATTRIBUTE_T2587315465_H
#ifndef PIVOTAXIS_T1004910028_H
#define PIVOTAXIS_T1004910028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.PivotAxis
struct  PivotAxis_t1004910028 
{
public:
	// System.Int32 HoloToolkit.Unity.PivotAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PivotAxis_t1004910028, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIVOTAXIS_T1004910028_H
#ifndef DROPDOWNCOMPONENTATTRIBUTE_T410457799_H
#define DROPDOWNCOMPONENTATTRIBUTE_T410457799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DropDownComponentAttribute
struct  DropDownComponentAttribute_t410457799  : public DrawOverrideAttribute_t485819796
{
public:
	// System.Boolean HoloToolkit.Unity.DropDownComponentAttribute::<AutoFill>k__BackingField
	bool ___U3CAutoFillU3Ek__BackingField_0;
	// System.Boolean HoloToolkit.Unity.DropDownComponentAttribute::<ShowComponentNames>k__BackingField
	bool ___U3CShowComponentNamesU3Ek__BackingField_1;
	// System.String HoloToolkit.Unity.DropDownComponentAttribute::<CustomLabel>k__BackingField
	String_t* ___U3CCustomLabelU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CAutoFillU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DropDownComponentAttribute_t410457799, ___U3CAutoFillU3Ek__BackingField_0)); }
	inline bool get_U3CAutoFillU3Ek__BackingField_0() const { return ___U3CAutoFillU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CAutoFillU3Ek__BackingField_0() { return &___U3CAutoFillU3Ek__BackingField_0; }
	inline void set_U3CAutoFillU3Ek__BackingField_0(bool value)
	{
		___U3CAutoFillU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CShowComponentNamesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DropDownComponentAttribute_t410457799, ___U3CShowComponentNamesU3Ek__BackingField_1)); }
	inline bool get_U3CShowComponentNamesU3Ek__BackingField_1() const { return ___U3CShowComponentNamesU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CShowComponentNamesU3Ek__BackingField_1() { return &___U3CShowComponentNamesU3Ek__BackingField_1; }
	inline void set_U3CShowComponentNamesU3Ek__BackingField_1(bool value)
	{
		___U3CShowComponentNamesU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCustomLabelU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DropDownComponentAttribute_t410457799, ___U3CCustomLabelU3Ek__BackingField_2)); }
	inline String_t* get_U3CCustomLabelU3Ek__BackingField_2() const { return ___U3CCustomLabelU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CCustomLabelU3Ek__BackingField_2() { return &___U3CCustomLabelU3Ek__BackingField_2; }
	inline void set_U3CCustomLabelU3Ek__BackingField_2(String_t* value)
	{
		___U3CCustomLabelU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomLabelU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWNCOMPONENTATTRIBUTE_T410457799_H
#ifndef SHOWIFENUMVALUEATTRIBUTE_T2926192619_H
#define SHOWIFENUMVALUEATTRIBUTE_T2926192619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ShowIfEnumValueAttribute
struct  ShowIfEnumValueAttribute_t2926192619  : public ShowIfAttribute_t4062566328
{
public:
	// System.Int32[] HoloToolkit.Unity.ShowIfEnumValueAttribute::<ShowValues>k__BackingField
	Int32U5BU5D_t385246372* ___U3CShowValuesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CShowValuesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ShowIfEnumValueAttribute_t2926192619, ___U3CShowValuesU3Ek__BackingField_2)); }
	inline Int32U5BU5D_t385246372* get_U3CShowValuesU3Ek__BackingField_2() const { return ___U3CShowValuesU3Ek__BackingField_2; }
	inline Int32U5BU5D_t385246372** get_address_of_U3CShowValuesU3Ek__BackingField_2() { return &___U3CShowValuesU3Ek__BackingField_2; }
	inline void set_U3CShowValuesU3Ek__BackingField_2(Int32U5BU5D_t385246372* value)
	{
		___U3CShowValuesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CShowValuesU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWIFENUMVALUEATTRIBUTE_T2926192619_H
#ifndef WRAPMODE_T730450702_H
#define WRAPMODE_T730450702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WrapMode
struct  WrapMode_t730450702 
{
public:
	// System.Int32 UnityEngine.WrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WrapMode_t730450702, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPMODE_T730450702_H
#ifndef ACTIONENUM_T3058967560_H
#define ACTIONENUM_T3058967560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ValidateUnityObjectAttribute/ActionEnum
struct  ActionEnum_t3058967560 
{
public:
	// System.Int32 HoloToolkit.Unity.ValidateUnityObjectAttribute/ActionEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActionEnum_t3058967560, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONENUM_T3058967560_H
#ifndef ENUMFLAGSATTRIBUTE_T1651967965_H
#define ENUMFLAGSATTRIBUTE_T1651967965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.EnumFlagsAttribute
struct  EnumFlagsAttribute_t1651967965  : public DrawOverrideAttribute_t485819796
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMFLAGSATTRIBUTE_T1651967965_H
#ifndef TEXTAREAPROP_T3268894636_H
#define TEXTAREAPROP_T3268894636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TextAreaProp
struct  TextAreaProp_t3268894636  : public DrawOverrideAttribute_t485819796
{
public:
	// System.Int32 HoloToolkit.Unity.TextAreaProp::<FontSize>k__BackingField
	int32_t ___U3CFontSizeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CFontSizeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TextAreaProp_t3268894636, ___U3CFontSizeU3Ek__BackingField_0)); }
	inline int32_t get_U3CFontSizeU3Ek__BackingField_0() const { return ___U3CFontSizeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CFontSizeU3Ek__BackingField_0() { return &___U3CFontSizeU3Ek__BackingField_0; }
	inline void set_U3CFontSizeU3Ek__BackingField_0(int32_t value)
	{
		___U3CFontSizeU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTAREAPROP_T3268894636_H
#ifndef SHOWIFNULLATTRIBUTE_T3061964185_H
#define SHOWIFNULLATTRIBUTE_T3061964185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ShowIfNullAttribute
struct  ShowIfNullAttribute_t3061964185  : public ShowIfAttribute_t4062566328
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWIFNULLATTRIBUTE_T3061964185_H
#ifndef ENUMCHECKBOXATTRIBUTE_T50826499_H
#define ENUMCHECKBOXATTRIBUTE_T50826499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.EnumCheckboxAttribute
struct  EnumCheckboxAttribute_t50826499  : public DrawOverrideAttribute_t485819796
{
public:
	// System.String HoloToolkit.Unity.EnumCheckboxAttribute::<DefaultName>k__BackingField
	String_t* ___U3CDefaultNameU3Ek__BackingField_0;
	// System.Int32 HoloToolkit.Unity.EnumCheckboxAttribute::<DefaultValue>k__BackingField
	int32_t ___U3CDefaultValueU3Ek__BackingField_1;
	// System.Int32 HoloToolkit.Unity.EnumCheckboxAttribute::<ValueOnZero>k__BackingField
	int32_t ___U3CValueOnZeroU3Ek__BackingField_2;
	// System.Boolean HoloToolkit.Unity.EnumCheckboxAttribute::<IgnoreNone>k__BackingField
	bool ___U3CIgnoreNoneU3Ek__BackingField_3;
	// System.Boolean HoloToolkit.Unity.EnumCheckboxAttribute::<IgnoreAll>k__BackingField
	bool ___U3CIgnoreAllU3Ek__BackingField_4;
	// System.String HoloToolkit.Unity.EnumCheckboxAttribute::<CustomLabel>k__BackingField
	String_t* ___U3CCustomLabelU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CDefaultNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EnumCheckboxAttribute_t50826499, ___U3CDefaultNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CDefaultNameU3Ek__BackingField_0() const { return ___U3CDefaultNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CDefaultNameU3Ek__BackingField_0() { return &___U3CDefaultNameU3Ek__BackingField_0; }
	inline void set_U3CDefaultNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CDefaultNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CDefaultValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EnumCheckboxAttribute_t50826499, ___U3CDefaultValueU3Ek__BackingField_1)); }
	inline int32_t get_U3CDefaultValueU3Ek__BackingField_1() const { return ___U3CDefaultValueU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CDefaultValueU3Ek__BackingField_1() { return &___U3CDefaultValueU3Ek__BackingField_1; }
	inline void set_U3CDefaultValueU3Ek__BackingField_1(int32_t value)
	{
		___U3CDefaultValueU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CValueOnZeroU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EnumCheckboxAttribute_t50826499, ___U3CValueOnZeroU3Ek__BackingField_2)); }
	inline int32_t get_U3CValueOnZeroU3Ek__BackingField_2() const { return ___U3CValueOnZeroU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CValueOnZeroU3Ek__BackingField_2() { return &___U3CValueOnZeroU3Ek__BackingField_2; }
	inline void set_U3CValueOnZeroU3Ek__BackingField_2(int32_t value)
	{
		___U3CValueOnZeroU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreNoneU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(EnumCheckboxAttribute_t50826499, ___U3CIgnoreNoneU3Ek__BackingField_3)); }
	inline bool get_U3CIgnoreNoneU3Ek__BackingField_3() const { return ___U3CIgnoreNoneU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIgnoreNoneU3Ek__BackingField_3() { return &___U3CIgnoreNoneU3Ek__BackingField_3; }
	inline void set_U3CIgnoreNoneU3Ek__BackingField_3(bool value)
	{
		___U3CIgnoreNoneU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreAllU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(EnumCheckboxAttribute_t50826499, ___U3CIgnoreAllU3Ek__BackingField_4)); }
	inline bool get_U3CIgnoreAllU3Ek__BackingField_4() const { return ___U3CIgnoreAllU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIgnoreAllU3Ek__BackingField_4() { return &___U3CIgnoreAllU3Ek__BackingField_4; }
	inline void set_U3CIgnoreAllU3Ek__BackingField_4(bool value)
	{
		___U3CIgnoreAllU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CCustomLabelU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(EnumCheckboxAttribute_t50826499, ___U3CCustomLabelU3Ek__BackingField_5)); }
	inline String_t* get_U3CCustomLabelU3Ek__BackingField_5() const { return ___U3CCustomLabelU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CCustomLabelU3Ek__BackingField_5() { return &___U3CCustomLabelU3Ek__BackingField_5; }
	inline void set_U3CCustomLabelU3Ek__BackingField_5(String_t* value)
	{
		___U3CCustomLabelU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomLabelU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMCHECKBOXATTRIBUTE_T50826499_H
#ifndef EDITABLEPROPATTRIBUTE_T2302503201_H
#define EDITABLEPROPATTRIBUTE_T2302503201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.EditablePropAttribute
struct  EditablePropAttribute_t2302503201  : public DrawOverrideAttribute_t485819796
{
public:
	// System.String HoloToolkit.Unity.EditablePropAttribute::<CustomLabel>k__BackingField
	String_t* ___U3CCustomLabelU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCustomLabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EditablePropAttribute_t2302503201, ___U3CCustomLabelU3Ek__BackingField_0)); }
	inline String_t* get_U3CCustomLabelU3Ek__BackingField_0() const { return ___U3CCustomLabelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCustomLabelU3Ek__BackingField_0() { return &___U3CCustomLabelU3Ek__BackingField_0; }
	inline void set_U3CCustomLabelU3Ek__BackingField_0(String_t* value)
	{
		___U3CCustomLabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomLabelU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITABLEPROPATTRIBUTE_T2302503201_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef MULTICASTDELEGATE_T157516450_H
#define MULTICASTDELEGATE_T157516450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t157516450  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t157516450, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T157516450_H
#ifndef MATERIALPROPERTYATTRIBUTE_T2178865252_H
#define MATERIALPROPERTYATTRIBUTE_T2178865252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MaterialPropertyAttribute
struct  MaterialPropertyAttribute_t2178865252  : public DrawOverrideAttribute_t485819796
{
public:
	// System.String HoloToolkit.Unity.MaterialPropertyAttribute::<Property>k__BackingField
	String_t* ___U3CPropertyU3Ek__BackingField_0;
	// HoloToolkit.Unity.MaterialPropertyAttribute/PropertyTypeEnum HoloToolkit.Unity.MaterialPropertyAttribute::<PropertyType>k__BackingField
	int32_t ___U3CPropertyTypeU3Ek__BackingField_1;
	// System.String HoloToolkit.Unity.MaterialPropertyAttribute::<MaterialMemberName>k__BackingField
	String_t* ___U3CMaterialMemberNameU3Ek__BackingField_2;
	// System.Boolean HoloToolkit.Unity.MaterialPropertyAttribute::<AllowNone>k__BackingField
	bool ___U3CAllowNoneU3Ek__BackingField_3;
	// System.String HoloToolkit.Unity.MaterialPropertyAttribute::<DefaultProperty>k__BackingField
	String_t* ___U3CDefaultPropertyU3Ek__BackingField_4;
	// System.String HoloToolkit.Unity.MaterialPropertyAttribute::<CustomLabel>k__BackingField
	String_t* ___U3CCustomLabelU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CPropertyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MaterialPropertyAttribute_t2178865252, ___U3CPropertyU3Ek__BackingField_0)); }
	inline String_t* get_U3CPropertyU3Ek__BackingField_0() const { return ___U3CPropertyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CPropertyU3Ek__BackingField_0() { return &___U3CPropertyU3Ek__BackingField_0; }
	inline void set_U3CPropertyU3Ek__BackingField_0(String_t* value)
	{
		___U3CPropertyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertyU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPropertyTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MaterialPropertyAttribute_t2178865252, ___U3CPropertyTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CPropertyTypeU3Ek__BackingField_1() const { return ___U3CPropertyTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPropertyTypeU3Ek__BackingField_1() { return &___U3CPropertyTypeU3Ek__BackingField_1; }
	inline void set_U3CPropertyTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CPropertyTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CMaterialMemberNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MaterialPropertyAttribute_t2178865252, ___U3CMaterialMemberNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CMaterialMemberNameU3Ek__BackingField_2() const { return ___U3CMaterialMemberNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CMaterialMemberNameU3Ek__BackingField_2() { return &___U3CMaterialMemberNameU3Ek__BackingField_2; }
	inline void set_U3CMaterialMemberNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CMaterialMemberNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaterialMemberNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CAllowNoneU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MaterialPropertyAttribute_t2178865252, ___U3CAllowNoneU3Ek__BackingField_3)); }
	inline bool get_U3CAllowNoneU3Ek__BackingField_3() const { return ___U3CAllowNoneU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CAllowNoneU3Ek__BackingField_3() { return &___U3CAllowNoneU3Ek__BackingField_3; }
	inline void set_U3CAllowNoneU3Ek__BackingField_3(bool value)
	{
		___U3CAllowNoneU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultPropertyU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MaterialPropertyAttribute_t2178865252, ___U3CDefaultPropertyU3Ek__BackingField_4)); }
	inline String_t* get_U3CDefaultPropertyU3Ek__BackingField_4() const { return ___U3CDefaultPropertyU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CDefaultPropertyU3Ek__BackingField_4() { return &___U3CDefaultPropertyU3Ek__BackingField_4; }
	inline void set_U3CDefaultPropertyU3Ek__BackingField_4(String_t* value)
	{
		___U3CDefaultPropertyU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultPropertyU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCustomLabelU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MaterialPropertyAttribute_t2178865252, ___U3CCustomLabelU3Ek__BackingField_5)); }
	inline String_t* get_U3CCustomLabelU3Ek__BackingField_5() const { return ___U3CCustomLabelU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CCustomLabelU3Ek__BackingField_5() { return &___U3CCustomLabelU3Ek__BackingField_5; }
	inline void set_U3CCustomLabelU3Ek__BackingField_5(String_t* value)
	{
		___U3CCustomLabelU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomLabelU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALPROPERTYATTRIBUTE_T2178865252_H
#ifndef SHAPECONSTRAINT_T2982058205_H
#define SHAPECONSTRAINT_T2982058205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeConstraint
#pragma pack(push, tp, 1)
struct  ShapeConstraint_t2982058205 
{
public:
	// HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeConstraintType HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeConstraint::Type
	int32_t ___Type_0;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeConstraint::Param_Float_0
	float ___Param_Float_0_1;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeConstraint::Param_Int_0
	int32_t ___Param_Int_0_2;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeConstraint::Param_Int_1
	int32_t ___Param_Int_1_3;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(ShapeConstraint_t2982058205, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Param_Float_0_1() { return static_cast<int32_t>(offsetof(ShapeConstraint_t2982058205, ___Param_Float_0_1)); }
	inline float get_Param_Float_0_1() const { return ___Param_Float_0_1; }
	inline float* get_address_of_Param_Float_0_1() { return &___Param_Float_0_1; }
	inline void set_Param_Float_0_1(float value)
	{
		___Param_Float_0_1 = value;
	}

	inline static int32_t get_offset_of_Param_Int_0_2() { return static_cast<int32_t>(offsetof(ShapeConstraint_t2982058205, ___Param_Int_0_2)); }
	inline int32_t get_Param_Int_0_2() const { return ___Param_Int_0_2; }
	inline int32_t* get_address_of_Param_Int_0_2() { return &___Param_Int_0_2; }
	inline void set_Param_Int_0_2(int32_t value)
	{
		___Param_Int_0_2 = value;
	}

	inline static int32_t get_offset_of_Param_Int_1_3() { return static_cast<int32_t>(offsetof(ShapeConstraint_t2982058205, ___Param_Int_1_3)); }
	inline int32_t get_Param_Int_1_3() const { return ___Param_Int_1_3; }
	inline int32_t* get_address_of_Param_Int_1_3() { return &___Param_Int_1_3; }
	inline void set_Param_Int_1_3(int32_t value)
	{
		___Param_Int_1_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPECONSTRAINT_T2982058205_H
#ifndef RANGEPROPATTRIBUTE_T1881743734_H
#define RANGEPROPATTRIBUTE_T1881743734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RangePropAttribute
struct  RangePropAttribute_t1881743734  : public DrawOverrideAttribute_t485819796
{
public:
	// System.Single HoloToolkit.Unity.RangePropAttribute::<MinFloat>k__BackingField
	float ___U3CMinFloatU3Ek__BackingField_0;
	// System.Single HoloToolkit.Unity.RangePropAttribute::<MaxFloat>k__BackingField
	float ___U3CMaxFloatU3Ek__BackingField_1;
	// System.Int32 HoloToolkit.Unity.RangePropAttribute::<MinInt>k__BackingField
	int32_t ___U3CMinIntU3Ek__BackingField_2;
	// System.Int32 HoloToolkit.Unity.RangePropAttribute::<MaxInt>k__BackingField
	int32_t ___U3CMaxIntU3Ek__BackingField_3;
	// HoloToolkit.Unity.RangePropAttribute/TypeEnum HoloToolkit.Unity.RangePropAttribute::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CMinFloatU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RangePropAttribute_t1881743734, ___U3CMinFloatU3Ek__BackingField_0)); }
	inline float get_U3CMinFloatU3Ek__BackingField_0() const { return ___U3CMinFloatU3Ek__BackingField_0; }
	inline float* get_address_of_U3CMinFloatU3Ek__BackingField_0() { return &___U3CMinFloatU3Ek__BackingField_0; }
	inline void set_U3CMinFloatU3Ek__BackingField_0(float value)
	{
		___U3CMinFloatU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMaxFloatU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RangePropAttribute_t1881743734, ___U3CMaxFloatU3Ek__BackingField_1)); }
	inline float get_U3CMaxFloatU3Ek__BackingField_1() const { return ___U3CMaxFloatU3Ek__BackingField_1; }
	inline float* get_address_of_U3CMaxFloatU3Ek__BackingField_1() { return &___U3CMaxFloatU3Ek__BackingField_1; }
	inline void set_U3CMaxFloatU3Ek__BackingField_1(float value)
	{
		___U3CMaxFloatU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CMinIntU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RangePropAttribute_t1881743734, ___U3CMinIntU3Ek__BackingField_2)); }
	inline int32_t get_U3CMinIntU3Ek__BackingField_2() const { return ___U3CMinIntU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CMinIntU3Ek__BackingField_2() { return &___U3CMinIntU3Ek__BackingField_2; }
	inline void set_U3CMinIntU3Ek__BackingField_2(int32_t value)
	{
		___U3CMinIntU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CMaxIntU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RangePropAttribute_t1881743734, ___U3CMaxIntU3Ek__BackingField_3)); }
	inline int32_t get_U3CMaxIntU3Ek__BackingField_3() const { return ___U3CMaxIntU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CMaxIntU3Ek__BackingField_3() { return &___U3CMaxIntU3Ek__BackingField_3; }
	inline void set_U3CMaxIntU3Ek__BackingField_3(int32_t value)
	{
		___U3CMaxIntU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RangePropAttribute_t1881743734, ___U3CTypeU3Ek__BackingField_4)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_4() const { return ___U3CTypeU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_4() { return &___U3CTypeU3Ek__BackingField_4; }
	inline void set_U3CTypeU3Ek__BackingField_4(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEPROPATTRIBUTE_T1881743734_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef VALIDATEUNITYOBJECTATTRIBUTE_T1018196378_H
#define VALIDATEUNITYOBJECTATTRIBUTE_T1018196378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ValidateUnityObjectAttribute
struct  ValidateUnityObjectAttribute_t1018196378  : public Attribute_t861562559
{
public:
	// HoloToolkit.Unity.ValidateUnityObjectAttribute/ActionEnum HoloToolkit.Unity.ValidateUnityObjectAttribute::<FailAction>k__BackingField
	int32_t ___U3CFailActionU3Ek__BackingField_0;
	// System.String HoloToolkit.Unity.ValidateUnityObjectAttribute::<MethodName>k__BackingField
	String_t* ___U3CMethodNameU3Ek__BackingField_1;
	// System.String HoloToolkit.Unity.ValidateUnityObjectAttribute::<MessageOnFail>k__BackingField
	String_t* ___U3CMessageOnFailU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CFailActionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ValidateUnityObjectAttribute_t1018196378, ___U3CFailActionU3Ek__BackingField_0)); }
	inline int32_t get_U3CFailActionU3Ek__BackingField_0() const { return ___U3CFailActionU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CFailActionU3Ek__BackingField_0() { return &___U3CFailActionU3Ek__BackingField_0; }
	inline void set_U3CFailActionU3Ek__BackingField_0(int32_t value)
	{
		___U3CFailActionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMethodNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ValidateUnityObjectAttribute_t1018196378, ___U3CMethodNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CMethodNameU3Ek__BackingField_1() const { return ___U3CMethodNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMethodNameU3Ek__BackingField_1() { return &___U3CMethodNameU3Ek__BackingField_1; }
	inline void set_U3CMethodNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CMethodNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMethodNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CMessageOnFailU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ValidateUnityObjectAttribute_t1018196378, ___U3CMessageOnFailU3Ek__BackingField_2)); }
	inline String_t* get_U3CMessageOnFailU3Ek__BackingField_2() const { return ___U3CMessageOnFailU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CMessageOnFailU3Ek__BackingField_2() { return &___U3CMessageOnFailU3Ek__BackingField_2; }
	inline void set_U3CMessageOnFailU3Ek__BackingField_2(String_t* value)
	{
		___U3CMessageOnFailU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageOnFailU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATEUNITYOBJECTATTRIBUTE_T1018196378_H
#ifndef ANIMATIONCURVEDEFAULTATTRIBUTE_T1761746171_H
#define ANIMATIONCURVEDEFAULTATTRIBUTE_T1761746171_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AnimationCurveDefaultAttribute
struct  AnimationCurveDefaultAttribute_t1761746171  : public DrawOverrideAttribute_t485819796
{
public:
	// UnityEngine.WrapMode HoloToolkit.Unity.AnimationCurveDefaultAttribute::<PostWrap>k__BackingField
	int32_t ___U3CPostWrapU3Ek__BackingField_0;
	// UnityEngine.Keyframe HoloToolkit.Unity.AnimationCurveDefaultAttribute::<StartVal>k__BackingField
	Keyframe_t4206410242  ___U3CStartValU3Ek__BackingField_1;
	// UnityEngine.Keyframe HoloToolkit.Unity.AnimationCurveDefaultAttribute::<EndVal>k__BackingField
	Keyframe_t4206410242  ___U3CEndValU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CPostWrapU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AnimationCurveDefaultAttribute_t1761746171, ___U3CPostWrapU3Ek__BackingField_0)); }
	inline int32_t get_U3CPostWrapU3Ek__BackingField_0() const { return ___U3CPostWrapU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CPostWrapU3Ek__BackingField_0() { return &___U3CPostWrapU3Ek__BackingField_0; }
	inline void set_U3CPostWrapU3Ek__BackingField_0(int32_t value)
	{
		___U3CPostWrapU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStartValU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AnimationCurveDefaultAttribute_t1761746171, ___U3CStartValU3Ek__BackingField_1)); }
	inline Keyframe_t4206410242  get_U3CStartValU3Ek__BackingField_1() const { return ___U3CStartValU3Ek__BackingField_1; }
	inline Keyframe_t4206410242 * get_address_of_U3CStartValU3Ek__BackingField_1() { return &___U3CStartValU3Ek__BackingField_1; }
	inline void set_U3CStartValU3Ek__BackingField_1(Keyframe_t4206410242  value)
	{
		___U3CStartValU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CEndValU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AnimationCurveDefaultAttribute_t1761746171, ___U3CEndValU3Ek__BackingField_2)); }
	inline Keyframe_t4206410242  get_U3CEndValU3Ek__BackingField_2() const { return ___U3CEndValU3Ek__BackingField_2; }
	inline Keyframe_t4206410242 * get_address_of_U3CEndValU3Ek__BackingField_2() { return &___U3CEndValU3Ek__BackingField_2; }
	inline void set_U3CEndValU3Ek__BackingField_2(Keyframe_t4206410242  value)
	{
		___U3CEndValU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCURVEDEFAULTATTRIBUTE_T1761746171_H
#ifndef U3CCALLBACKRETURNVALUEU3ED__6_T3485498644_H
#define U3CCALLBACKRETURNVALUEU3ED__6_T3485498644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ApplicationViewManager/<CallbackReturnValue>d__6
struct  U3CCallbackReturnValueU3Ed__6_t3485498644  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.ApplicationViewManager/<CallbackReturnValue>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncVoidMethodBuilder HoloToolkit.Unity.ApplicationViewManager/<CallbackReturnValue>d__6::<>t__builder
	AsyncVoidMethodBuilder_t3819840891  ___U3CU3Et__builder_1;
	// System.Object HoloToolkit.Unity.ApplicationViewManager/<CallbackReturnValue>d__6::returnValue
	RuntimeObject * ___returnValue_2;
	// System.Int32 HoloToolkit.Unity.ApplicationViewManager/<CallbackReturnValue>d__6::<viewId>5__1
	int32_t ___U3CviewIdU3E5__1_3;
	// Windows.ApplicationModel.Core.CoreApplicationView HoloToolkit.Unity.ApplicationViewManager/<CallbackReturnValue>d__6::<view>5__2
	CoreApplicationView_t1335682569 * ___U3CviewU3E5__2_4;
	// System.Action`1<System.Object> HoloToolkit.Unity.ApplicationViewManager/<CallbackReturnValue>d__6::<cb>5__3
	Action_1_t3252573759 * ___U3CcbU3E5__3_5;
	// System.Runtime.CompilerServices.TaskAwaiter`1<System.Boolean> HoloToolkit.Unity.ApplicationViewManager/<CallbackReturnValue>d__6::<>u__1
	TaskAwaiter_1_t2891770396  ___U3CU3Eu__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCallbackReturnValueU3Ed__6_t3485498644, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CCallbackReturnValueU3Ed__6_t3485498644, ___U3CU3Et__builder_1)); }
	inline AsyncVoidMethodBuilder_t3819840891  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncVoidMethodBuilder_t3819840891 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncVoidMethodBuilder_t3819840891  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_returnValue_2() { return static_cast<int32_t>(offsetof(U3CCallbackReturnValueU3Ed__6_t3485498644, ___returnValue_2)); }
	inline RuntimeObject * get_returnValue_2() const { return ___returnValue_2; }
	inline RuntimeObject ** get_address_of_returnValue_2() { return &___returnValue_2; }
	inline void set_returnValue_2(RuntimeObject * value)
	{
		___returnValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___returnValue_2), value);
	}

	inline static int32_t get_offset_of_U3CviewIdU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CCallbackReturnValueU3Ed__6_t3485498644, ___U3CviewIdU3E5__1_3)); }
	inline int32_t get_U3CviewIdU3E5__1_3() const { return ___U3CviewIdU3E5__1_3; }
	inline int32_t* get_address_of_U3CviewIdU3E5__1_3() { return &___U3CviewIdU3E5__1_3; }
	inline void set_U3CviewIdU3E5__1_3(int32_t value)
	{
		___U3CviewIdU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CviewU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CCallbackReturnValueU3Ed__6_t3485498644, ___U3CviewU3E5__2_4)); }
	inline CoreApplicationView_t1335682569 * get_U3CviewU3E5__2_4() const { return ___U3CviewU3E5__2_4; }
	inline CoreApplicationView_t1335682569 ** get_address_of_U3CviewU3E5__2_4() { return &___U3CviewU3E5__2_4; }
	inline void set_U3CviewU3E5__2_4(CoreApplicationView_t1335682569 * value)
	{
		___U3CviewU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CviewU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CcbU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CCallbackReturnValueU3Ed__6_t3485498644, ___U3CcbU3E5__3_5)); }
	inline Action_1_t3252573759 * get_U3CcbU3E5__3_5() const { return ___U3CcbU3E5__3_5; }
	inline Action_1_t3252573759 ** get_address_of_U3CcbU3E5__3_5() { return &___U3CcbU3E5__3_5; }
	inline void set_U3CcbU3E5__3_5(Action_1_t3252573759 * value)
	{
		___U3CcbU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcbU3E5__3_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_6() { return static_cast<int32_t>(offsetof(U3CCallbackReturnValueU3Ed__6_t3485498644, ___U3CU3Eu__1_6)); }
	inline TaskAwaiter_1_t2891770396  get_U3CU3Eu__1_6() const { return ___U3CU3Eu__1_6; }
	inline TaskAwaiter_1_t2891770396 * get_address_of_U3CU3Eu__1_6() { return &___U3CU3Eu__1_6; }
	inline void set_U3CU3Eu__1_6(TaskAwaiter_1_t2891770396  value)
	{
		___U3CU3Eu__1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCALLBACKRETURNVALUEU3ED__6_T3485498644_H
#ifndef CONTROLLERSTATE_T440280647_H
#define CONTROLLERSTATE_T440280647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState
struct  ControllerState_t440280647  : public RuntimeObject
{
public:
	// UnityEngine.XR.WSA.Input.InteractionSourceHandedness HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState::Handedness
	int32_t ___Handedness_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState::PointerPosition
	Vector3_t3722313464  ___PointerPosition_1;
	// UnityEngine.Quaternion HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState::PointerRotation
	Quaternion_t2301928331  ___PointerRotation_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState::GripPosition
	Vector3_t3722313464  ___GripPosition_3;
	// UnityEngine.Quaternion HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState::GripRotation
	Quaternion_t2301928331  ___GripRotation_4;
	// System.Boolean HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState::Grasped
	bool ___Grasped_5;
	// System.Boolean HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState::MenuPressed
	bool ___MenuPressed_6;
	// System.Boolean HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState::SelectPressed
	bool ___SelectPressed_7;
	// System.Single HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState::SelectPressedAmount
	float ___SelectPressedAmount_8;
	// System.Boolean HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState::ThumbstickPressed
	bool ___ThumbstickPressed_9;
	// UnityEngine.Vector2 HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState::ThumbstickPosition
	Vector2_t2156229523  ___ThumbstickPosition_10;
	// System.Boolean HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState::TouchpadPressed
	bool ___TouchpadPressed_11;
	// System.Boolean HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState::TouchpadTouched
	bool ___TouchpadTouched_12;
	// UnityEngine.Vector2 HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState::TouchpadPosition
	Vector2_t2156229523  ___TouchpadPosition_13;

public:
	inline static int32_t get_offset_of_Handedness_0() { return static_cast<int32_t>(offsetof(ControllerState_t440280647, ___Handedness_0)); }
	inline int32_t get_Handedness_0() const { return ___Handedness_0; }
	inline int32_t* get_address_of_Handedness_0() { return &___Handedness_0; }
	inline void set_Handedness_0(int32_t value)
	{
		___Handedness_0 = value;
	}

	inline static int32_t get_offset_of_PointerPosition_1() { return static_cast<int32_t>(offsetof(ControllerState_t440280647, ___PointerPosition_1)); }
	inline Vector3_t3722313464  get_PointerPosition_1() const { return ___PointerPosition_1; }
	inline Vector3_t3722313464 * get_address_of_PointerPosition_1() { return &___PointerPosition_1; }
	inline void set_PointerPosition_1(Vector3_t3722313464  value)
	{
		___PointerPosition_1 = value;
	}

	inline static int32_t get_offset_of_PointerRotation_2() { return static_cast<int32_t>(offsetof(ControllerState_t440280647, ___PointerRotation_2)); }
	inline Quaternion_t2301928331  get_PointerRotation_2() const { return ___PointerRotation_2; }
	inline Quaternion_t2301928331 * get_address_of_PointerRotation_2() { return &___PointerRotation_2; }
	inline void set_PointerRotation_2(Quaternion_t2301928331  value)
	{
		___PointerRotation_2 = value;
	}

	inline static int32_t get_offset_of_GripPosition_3() { return static_cast<int32_t>(offsetof(ControllerState_t440280647, ___GripPosition_3)); }
	inline Vector3_t3722313464  get_GripPosition_3() const { return ___GripPosition_3; }
	inline Vector3_t3722313464 * get_address_of_GripPosition_3() { return &___GripPosition_3; }
	inline void set_GripPosition_3(Vector3_t3722313464  value)
	{
		___GripPosition_3 = value;
	}

	inline static int32_t get_offset_of_GripRotation_4() { return static_cast<int32_t>(offsetof(ControllerState_t440280647, ___GripRotation_4)); }
	inline Quaternion_t2301928331  get_GripRotation_4() const { return ___GripRotation_4; }
	inline Quaternion_t2301928331 * get_address_of_GripRotation_4() { return &___GripRotation_4; }
	inline void set_GripRotation_4(Quaternion_t2301928331  value)
	{
		___GripRotation_4 = value;
	}

	inline static int32_t get_offset_of_Grasped_5() { return static_cast<int32_t>(offsetof(ControllerState_t440280647, ___Grasped_5)); }
	inline bool get_Grasped_5() const { return ___Grasped_5; }
	inline bool* get_address_of_Grasped_5() { return &___Grasped_5; }
	inline void set_Grasped_5(bool value)
	{
		___Grasped_5 = value;
	}

	inline static int32_t get_offset_of_MenuPressed_6() { return static_cast<int32_t>(offsetof(ControllerState_t440280647, ___MenuPressed_6)); }
	inline bool get_MenuPressed_6() const { return ___MenuPressed_6; }
	inline bool* get_address_of_MenuPressed_6() { return &___MenuPressed_6; }
	inline void set_MenuPressed_6(bool value)
	{
		___MenuPressed_6 = value;
	}

	inline static int32_t get_offset_of_SelectPressed_7() { return static_cast<int32_t>(offsetof(ControllerState_t440280647, ___SelectPressed_7)); }
	inline bool get_SelectPressed_7() const { return ___SelectPressed_7; }
	inline bool* get_address_of_SelectPressed_7() { return &___SelectPressed_7; }
	inline void set_SelectPressed_7(bool value)
	{
		___SelectPressed_7 = value;
	}

	inline static int32_t get_offset_of_SelectPressedAmount_8() { return static_cast<int32_t>(offsetof(ControllerState_t440280647, ___SelectPressedAmount_8)); }
	inline float get_SelectPressedAmount_8() const { return ___SelectPressedAmount_8; }
	inline float* get_address_of_SelectPressedAmount_8() { return &___SelectPressedAmount_8; }
	inline void set_SelectPressedAmount_8(float value)
	{
		___SelectPressedAmount_8 = value;
	}

	inline static int32_t get_offset_of_ThumbstickPressed_9() { return static_cast<int32_t>(offsetof(ControllerState_t440280647, ___ThumbstickPressed_9)); }
	inline bool get_ThumbstickPressed_9() const { return ___ThumbstickPressed_9; }
	inline bool* get_address_of_ThumbstickPressed_9() { return &___ThumbstickPressed_9; }
	inline void set_ThumbstickPressed_9(bool value)
	{
		___ThumbstickPressed_9 = value;
	}

	inline static int32_t get_offset_of_ThumbstickPosition_10() { return static_cast<int32_t>(offsetof(ControllerState_t440280647, ___ThumbstickPosition_10)); }
	inline Vector2_t2156229523  get_ThumbstickPosition_10() const { return ___ThumbstickPosition_10; }
	inline Vector2_t2156229523 * get_address_of_ThumbstickPosition_10() { return &___ThumbstickPosition_10; }
	inline void set_ThumbstickPosition_10(Vector2_t2156229523  value)
	{
		___ThumbstickPosition_10 = value;
	}

	inline static int32_t get_offset_of_TouchpadPressed_11() { return static_cast<int32_t>(offsetof(ControllerState_t440280647, ___TouchpadPressed_11)); }
	inline bool get_TouchpadPressed_11() const { return ___TouchpadPressed_11; }
	inline bool* get_address_of_TouchpadPressed_11() { return &___TouchpadPressed_11; }
	inline void set_TouchpadPressed_11(bool value)
	{
		___TouchpadPressed_11 = value;
	}

	inline static int32_t get_offset_of_TouchpadTouched_12() { return static_cast<int32_t>(offsetof(ControllerState_t440280647, ___TouchpadTouched_12)); }
	inline bool get_TouchpadTouched_12() const { return ___TouchpadTouched_12; }
	inline bool* get_address_of_TouchpadTouched_12() { return &___TouchpadTouched_12; }
	inline void set_TouchpadTouched_12(bool value)
	{
		___TouchpadTouched_12 = value;
	}

	inline static int32_t get_offset_of_TouchpadPosition_13() { return static_cast<int32_t>(offsetof(ControllerState_t440280647, ___TouchpadPosition_13)); }
	inline Vector2_t2156229523  get_TouchpadPosition_13() const { return ___TouchpadPosition_13; }
	inline Vector2_t2156229523 * get_address_of_TouchpadPosition_13() { return &___TouchpadPosition_13; }
	inline void set_TouchpadPosition_13(Vector2_t2156229523  value)
	{
		___TouchpadPosition_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERSTATE_T440280647_H
#ifndef SHAPECOMPONENTCONSTRAINT_T4241953735_H
#define SHAPECOMPONENTCONSTRAINT_T4241953735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint
#pragma pack(push, tp, 1)
struct  ShapeComponentConstraint_t4241953735 
{
public:
	// HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraintType HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Type
	int32_t ___Type_0;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Param_Float_0
	float ___Param_Float_0_1;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Param_Float_1
	float ___Param_Float_1_2;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Param_Float_2
	float ___Param_Float_2_3;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Param_Float_3
	float ___Param_Float_3_4;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Param_Int_0
	int32_t ___Param_Int_0_5;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Param_Int_1
	int32_t ___Param_Int_1_6;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Param_Str_0
	intptr_t ___Param_Str_0_7;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Param_Float_0_1() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Param_Float_0_1)); }
	inline float get_Param_Float_0_1() const { return ___Param_Float_0_1; }
	inline float* get_address_of_Param_Float_0_1() { return &___Param_Float_0_1; }
	inline void set_Param_Float_0_1(float value)
	{
		___Param_Float_0_1 = value;
	}

	inline static int32_t get_offset_of_Param_Float_1_2() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Param_Float_1_2)); }
	inline float get_Param_Float_1_2() const { return ___Param_Float_1_2; }
	inline float* get_address_of_Param_Float_1_2() { return &___Param_Float_1_2; }
	inline void set_Param_Float_1_2(float value)
	{
		___Param_Float_1_2 = value;
	}

	inline static int32_t get_offset_of_Param_Float_2_3() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Param_Float_2_3)); }
	inline float get_Param_Float_2_3() const { return ___Param_Float_2_3; }
	inline float* get_address_of_Param_Float_2_3() { return &___Param_Float_2_3; }
	inline void set_Param_Float_2_3(float value)
	{
		___Param_Float_2_3 = value;
	}

	inline static int32_t get_offset_of_Param_Float_3_4() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Param_Float_3_4)); }
	inline float get_Param_Float_3_4() const { return ___Param_Float_3_4; }
	inline float* get_address_of_Param_Float_3_4() { return &___Param_Float_3_4; }
	inline void set_Param_Float_3_4(float value)
	{
		___Param_Float_3_4 = value;
	}

	inline static int32_t get_offset_of_Param_Int_0_5() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Param_Int_0_5)); }
	inline int32_t get_Param_Int_0_5() const { return ___Param_Int_0_5; }
	inline int32_t* get_address_of_Param_Int_0_5() { return &___Param_Int_0_5; }
	inline void set_Param_Int_0_5(int32_t value)
	{
		___Param_Int_0_5 = value;
	}

	inline static int32_t get_offset_of_Param_Int_1_6() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Param_Int_1_6)); }
	inline int32_t get_Param_Int_1_6() const { return ___Param_Int_1_6; }
	inline int32_t* get_address_of_Param_Int_1_6() { return &___Param_Int_1_6; }
	inline void set_Param_Int_1_6(int32_t value)
	{
		___Param_Int_1_6 = value;
	}

	inline static int32_t get_offset_of_Param_Str_0_7() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Param_Str_0_7)); }
	inline intptr_t get_Param_Str_0_7() const { return ___Param_Str_0_7; }
	inline intptr_t* get_address_of_Param_Str_0_7() { return &___Param_Str_0_7; }
	inline void set_Param_Str_0_7(intptr_t value)
	{
		___Param_Str_0_7 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPECOMPONENTCONSTRAINT_T4241953735_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef QUALITYCHANGEDEVENT_T3536656938_H
#define QUALITYCHANGEDEVENT_T3536656938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AdaptiveQuality/QualityChangedEvent
struct  QualityChangedEvent_t3536656938  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITYCHANGEDEVENT_T3536656938_H
#ifndef ATLASPREFABREFERENCE_T289331666_H
#define ATLASPREFABREFERENCE_T289331666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AtlasPrefabReference
struct  AtlasPrefabReference_t289331666  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HoloToolkit.Unity.AtlasPrefabReference::Prefabs
	List_1_t2585711361 * ___Prefabs_2;
	// UnityEngine.U2D.SpriteAtlas HoloToolkit.Unity.AtlasPrefabReference::Atlas
	SpriteAtlas_t646931412 * ___Atlas_3;

public:
	inline static int32_t get_offset_of_Prefabs_2() { return static_cast<int32_t>(offsetof(AtlasPrefabReference_t289331666, ___Prefabs_2)); }
	inline List_1_t2585711361 * get_Prefabs_2() const { return ___Prefabs_2; }
	inline List_1_t2585711361 ** get_address_of_Prefabs_2() { return &___Prefabs_2; }
	inline void set_Prefabs_2(List_1_t2585711361 * value)
	{
		___Prefabs_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefabs_2), value);
	}

	inline static int32_t get_offset_of_Atlas_3() { return static_cast<int32_t>(offsetof(AtlasPrefabReference_t289331666, ___Atlas_3)); }
	inline SpriteAtlas_t646931412 * get_Atlas_3() const { return ___Atlas_3; }
	inline SpriteAtlas_t646931412 ** get_address_of_Atlas_3() { return &___Atlas_3; }
	inline void set_Atlas_3(SpriteAtlas_t646931412 * value)
	{
		___Atlas_3 = value;
		Il2CppCodeGenWriteBarrier((&___Atlas_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASPREFABREFERENCE_T289331666_H
#ifndef GETLOGLINE_T996454327_H
#define GETLOGLINE_T996454327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DebugPanel/GetLogLine
struct  GetLogLine_t996454327  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETLOGLINE_T996454327_H
#ifndef RAYCASTFUNC_T2509020078_H
#define RAYCASTFUNC_T2509020078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RaycastHelper/RaycastFunc
struct  RaycastFunc_t2509020078  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTFUNC_T2509020078_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SOLVER_T4167981057_H
#define SOLVER_T4167981057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Solver
struct  Solver_t4167981057  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean HoloToolkit.Unity.Solver::UpdateLinkedTransform
	bool ___UpdateLinkedTransform_2;
	// System.Single HoloToolkit.Unity.Solver::MoveLerpTime
	float ___MoveLerpTime_3;
	// System.Single HoloToolkit.Unity.Solver::RotateLerpTime
	float ___RotateLerpTime_4;
	// System.Single HoloToolkit.Unity.Solver::ScaleLerpTime
	float ___ScaleLerpTime_5;
	// System.Boolean HoloToolkit.Unity.Solver::MaintainScale
	bool ___MaintainScale_6;
	// UnityEngine.Vector3 HoloToolkit.Unity.Solver::GoalPosition
	Vector3_t3722313464  ___GoalPosition_7;
	// UnityEngine.Quaternion HoloToolkit.Unity.Solver::GoalRotation
	Quaternion_t2301928331  ___GoalRotation_8;
	// UnityEngine.Vector3 HoloToolkit.Unity.Solver::GoalScale
	Vector3_t3722313464  ___GoalScale_9;
	// System.Boolean HoloToolkit.Unity.Solver::Smoothing
	bool ___Smoothing_10;
	// System.Single HoloToolkit.Unity.Solver::Lifetime
	float ___Lifetime_11;
	// HoloToolkit.Unity.SolverHandler HoloToolkit.Unity.Solver::solverHandler
	SolverHandler_t1963548039 * ___solverHandler_12;
	// System.Single HoloToolkit.Unity.Solver::lifetime
	float ___lifetime_13;

public:
	inline static int32_t get_offset_of_UpdateLinkedTransform_2() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___UpdateLinkedTransform_2)); }
	inline bool get_UpdateLinkedTransform_2() const { return ___UpdateLinkedTransform_2; }
	inline bool* get_address_of_UpdateLinkedTransform_2() { return &___UpdateLinkedTransform_2; }
	inline void set_UpdateLinkedTransform_2(bool value)
	{
		___UpdateLinkedTransform_2 = value;
	}

	inline static int32_t get_offset_of_MoveLerpTime_3() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___MoveLerpTime_3)); }
	inline float get_MoveLerpTime_3() const { return ___MoveLerpTime_3; }
	inline float* get_address_of_MoveLerpTime_3() { return &___MoveLerpTime_3; }
	inline void set_MoveLerpTime_3(float value)
	{
		___MoveLerpTime_3 = value;
	}

	inline static int32_t get_offset_of_RotateLerpTime_4() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___RotateLerpTime_4)); }
	inline float get_RotateLerpTime_4() const { return ___RotateLerpTime_4; }
	inline float* get_address_of_RotateLerpTime_4() { return &___RotateLerpTime_4; }
	inline void set_RotateLerpTime_4(float value)
	{
		___RotateLerpTime_4 = value;
	}

	inline static int32_t get_offset_of_ScaleLerpTime_5() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___ScaleLerpTime_5)); }
	inline float get_ScaleLerpTime_5() const { return ___ScaleLerpTime_5; }
	inline float* get_address_of_ScaleLerpTime_5() { return &___ScaleLerpTime_5; }
	inline void set_ScaleLerpTime_5(float value)
	{
		___ScaleLerpTime_5 = value;
	}

	inline static int32_t get_offset_of_MaintainScale_6() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___MaintainScale_6)); }
	inline bool get_MaintainScale_6() const { return ___MaintainScale_6; }
	inline bool* get_address_of_MaintainScale_6() { return &___MaintainScale_6; }
	inline void set_MaintainScale_6(bool value)
	{
		___MaintainScale_6 = value;
	}

	inline static int32_t get_offset_of_GoalPosition_7() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___GoalPosition_7)); }
	inline Vector3_t3722313464  get_GoalPosition_7() const { return ___GoalPosition_7; }
	inline Vector3_t3722313464 * get_address_of_GoalPosition_7() { return &___GoalPosition_7; }
	inline void set_GoalPosition_7(Vector3_t3722313464  value)
	{
		___GoalPosition_7 = value;
	}

	inline static int32_t get_offset_of_GoalRotation_8() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___GoalRotation_8)); }
	inline Quaternion_t2301928331  get_GoalRotation_8() const { return ___GoalRotation_8; }
	inline Quaternion_t2301928331 * get_address_of_GoalRotation_8() { return &___GoalRotation_8; }
	inline void set_GoalRotation_8(Quaternion_t2301928331  value)
	{
		___GoalRotation_8 = value;
	}

	inline static int32_t get_offset_of_GoalScale_9() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___GoalScale_9)); }
	inline Vector3_t3722313464  get_GoalScale_9() const { return ___GoalScale_9; }
	inline Vector3_t3722313464 * get_address_of_GoalScale_9() { return &___GoalScale_9; }
	inline void set_GoalScale_9(Vector3_t3722313464  value)
	{
		___GoalScale_9 = value;
	}

	inline static int32_t get_offset_of_Smoothing_10() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___Smoothing_10)); }
	inline bool get_Smoothing_10() const { return ___Smoothing_10; }
	inline bool* get_address_of_Smoothing_10() { return &___Smoothing_10; }
	inline void set_Smoothing_10(bool value)
	{
		___Smoothing_10 = value;
	}

	inline static int32_t get_offset_of_Lifetime_11() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___Lifetime_11)); }
	inline float get_Lifetime_11() const { return ___Lifetime_11; }
	inline float* get_address_of_Lifetime_11() { return &___Lifetime_11; }
	inline void set_Lifetime_11(float value)
	{
		___Lifetime_11 = value;
	}

	inline static int32_t get_offset_of_solverHandler_12() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___solverHandler_12)); }
	inline SolverHandler_t1963548039 * get_solverHandler_12() const { return ___solverHandler_12; }
	inline SolverHandler_t1963548039 ** get_address_of_solverHandler_12() { return &___solverHandler_12; }
	inline void set_solverHandler_12(SolverHandler_t1963548039 * value)
	{
		___solverHandler_12 = value;
		Il2CppCodeGenWriteBarrier((&___solverHandler_12), value);
	}

	inline static int32_t get_offset_of_lifetime_13() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___lifetime_13)); }
	inline float get_lifetime_13() const { return ___lifetime_13; }
	inline float* get_address_of_lifetime_13() { return &___lifetime_13; }
	inline void set_lifetime_13(float value)
	{
		___lifetime_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVER_T4167981057_H
#ifndef SINGLETON_1_T2089007498_H
#define SINGLETON_1_T2089007498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.FadeManager>
struct  Singleton_1_t2089007498  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t2089007498_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	FadeManager_t1688783044 * ___instance_2;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2089007498_StaticFields, ___instance_2)); }
	inline FadeManager_t1688783044 * get_instance_2() const { return ___instance_2; }
	inline FadeManager_t1688783044 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(FadeManager_t1688783044 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_searchForInstance_3() { return static_cast<int32_t>(offsetof(Singleton_1_t2089007498_StaticFields, ___searchForInstance_3)); }
	inline bool get_searchForInstance_3() const { return ___searchForInstance_3; }
	inline bool* get_address_of_searchForInstance_3() { return &___searchForInstance_3; }
	inline void set_searchForInstance_3(bool value)
	{
		___searchForInstance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2089007498_H
#ifndef SINGLEINSTANCE_1_T3322408396_H
#define SINGLEINSTANCE_1_T3322408396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SingleInstance`1<HoloToolkit.Unity.DebugPanel>
struct  SingleInstance_1_t3322408396  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct SingleInstance_1_t3322408396_StaticFields
{
public:
	// T HoloToolkit.Unity.SingleInstance`1::_Instance
	DebugPanel_t2851753240 * ____Instance_2;

public:
	inline static int32_t get_offset_of__Instance_2() { return static_cast<int32_t>(offsetof(SingleInstance_1_t3322408396_StaticFields, ____Instance_2)); }
	inline DebugPanel_t2851753240 * get__Instance_2() const { return ____Instance_2; }
	inline DebugPanel_t2851753240 ** get_address_of__Instance_2() { return &____Instance_2; }
	inline void set__Instance_2(DebugPanel_t2851753240 * value)
	{
		____Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLEINSTANCE_1_T3322408396_H
#ifndef NEARPLANEFADE_T1087371927_H
#define NEARPLANEFADE_T1087371927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.NearPlaneFade
struct  NearPlaneFade_t1087371927  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HoloToolkit.Unity.NearPlaneFade::FadeDistanceStart
	float ___FadeDistanceStart_2;
	// System.Single HoloToolkit.Unity.NearPlaneFade::FadeDistanceEnd
	float ___FadeDistanceEnd_3;
	// System.Boolean HoloToolkit.Unity.NearPlaneFade::NearPlaneFadeOn
	bool ___NearPlaneFadeOn_4;
	// System.Int32 HoloToolkit.Unity.NearPlaneFade::fadeDistancePropertyID
	int32_t ___fadeDistancePropertyID_6;

public:
	inline static int32_t get_offset_of_FadeDistanceStart_2() { return static_cast<int32_t>(offsetof(NearPlaneFade_t1087371927, ___FadeDistanceStart_2)); }
	inline float get_FadeDistanceStart_2() const { return ___FadeDistanceStart_2; }
	inline float* get_address_of_FadeDistanceStart_2() { return &___FadeDistanceStart_2; }
	inline void set_FadeDistanceStart_2(float value)
	{
		___FadeDistanceStart_2 = value;
	}

	inline static int32_t get_offset_of_FadeDistanceEnd_3() { return static_cast<int32_t>(offsetof(NearPlaneFade_t1087371927, ___FadeDistanceEnd_3)); }
	inline float get_FadeDistanceEnd_3() const { return ___FadeDistanceEnd_3; }
	inline float* get_address_of_FadeDistanceEnd_3() { return &___FadeDistanceEnd_3; }
	inline void set_FadeDistanceEnd_3(float value)
	{
		___FadeDistanceEnd_3 = value;
	}

	inline static int32_t get_offset_of_NearPlaneFadeOn_4() { return static_cast<int32_t>(offsetof(NearPlaneFade_t1087371927, ___NearPlaneFadeOn_4)); }
	inline bool get_NearPlaneFadeOn_4() const { return ___NearPlaneFadeOn_4; }
	inline bool* get_address_of_NearPlaneFadeOn_4() { return &___NearPlaneFadeOn_4; }
	inline void set_NearPlaneFadeOn_4(bool value)
	{
		___NearPlaneFadeOn_4 = value;
	}

	inline static int32_t get_offset_of_fadeDistancePropertyID_6() { return static_cast<int32_t>(offsetof(NearPlaneFade_t1087371927, ___fadeDistancePropertyID_6)); }
	inline int32_t get_fadeDistancePropertyID_6() const { return ___fadeDistancePropertyID_6; }
	inline int32_t* get_address_of_fadeDistancePropertyID_6() { return &___fadeDistancePropertyID_6; }
	inline void set_fadeDistancePropertyID_6(int32_t value)
	{
		___fadeDistancePropertyID_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEARPLANEFADE_T1087371927_H
#ifndef APPLICATIONVIEWMANAGER_T1982588047_H
#define APPLICATIONVIEWMANAGER_T1982588047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ApplicationViewManager
struct  ApplicationViewManager_t1982588047  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct ApplicationViewManager_t1982588047_StaticFields
{
public:
	// System.Int32 HoloToolkit.Unity.ApplicationViewManager::<Full3DViewId>k__BackingField
	int32_t ___U3CFull3DViewIdU3Ek__BackingField_2;
	// System.Collections.Concurrent.ConcurrentDictionary`2<System.Int32,System.Action`1<System.Object>> HoloToolkit.Unity.ApplicationViewManager::CallbackDictionary
	ConcurrentDictionary_2_t1542073159 * ___CallbackDictionary_3;

public:
	inline static int32_t get_offset_of_U3CFull3DViewIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ApplicationViewManager_t1982588047_StaticFields, ___U3CFull3DViewIdU3Ek__BackingField_2)); }
	inline int32_t get_U3CFull3DViewIdU3Ek__BackingField_2() const { return ___U3CFull3DViewIdU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CFull3DViewIdU3Ek__BackingField_2() { return &___U3CFull3DViewIdU3Ek__BackingField_2; }
	inline void set_U3CFull3DViewIdU3Ek__BackingField_2(int32_t value)
	{
		___U3CFull3DViewIdU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_CallbackDictionary_3() { return static_cast<int32_t>(offsetof(ApplicationViewManager_t1982588047_StaticFields, ___CallbackDictionary_3)); }
	inline ConcurrentDictionary_2_t1542073159 * get_CallbackDictionary_3() const { return ___CallbackDictionary_3; }
	inline ConcurrentDictionary_2_t1542073159 ** get_address_of_CallbackDictionary_3() { return &___CallbackDictionary_3; }
	inline void set_CallbackDictionary_3(ConcurrentDictionary_2_t1542073159 * value)
	{
		___CallbackDictionary_3 = value;
		Il2CppCodeGenWriteBarrier((&___CallbackDictionary_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONVIEWMANAGER_T1982588047_H
#ifndef SIMPLETAGALONG_T3886944103_H
#define SIMPLETAGALONG_T3886944103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SimpleTagalong
struct  SimpleTagalong_t3886944103  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HoloToolkit.Unity.SimpleTagalong::TagalongDistance
	float ___TagalongDistance_2;
	// System.Boolean HoloToolkit.Unity.SimpleTagalong::EnforceDistance
	bool ___EnforceDistance_3;
	// System.Single HoloToolkit.Unity.SimpleTagalong::PositionUpdateSpeed
	float ___PositionUpdateSpeed_4;
	// System.Boolean HoloToolkit.Unity.SimpleTagalong::SmoothMotion
	bool ___SmoothMotion_5;
	// System.Single HoloToolkit.Unity.SimpleTagalong::SmoothingFactor
	float ___SmoothingFactor_6;
	// UnityEngine.BoxCollider HoloToolkit.Unity.SimpleTagalong::tagalongCollider
	BoxCollider_t1640800422 * ___tagalongCollider_7;
	// HoloToolkit.Unity.Interpolator HoloToolkit.Unity.SimpleTagalong::interpolator
	Interpolator_t3604653897 * ___interpolator_8;
	// UnityEngine.Plane[] HoloToolkit.Unity.SimpleTagalong::frustumPlanes
	PlaneU5BU5D_t3656189108* ___frustumPlanes_9;

public:
	inline static int32_t get_offset_of_TagalongDistance_2() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___TagalongDistance_2)); }
	inline float get_TagalongDistance_2() const { return ___TagalongDistance_2; }
	inline float* get_address_of_TagalongDistance_2() { return &___TagalongDistance_2; }
	inline void set_TagalongDistance_2(float value)
	{
		___TagalongDistance_2 = value;
	}

	inline static int32_t get_offset_of_EnforceDistance_3() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___EnforceDistance_3)); }
	inline bool get_EnforceDistance_3() const { return ___EnforceDistance_3; }
	inline bool* get_address_of_EnforceDistance_3() { return &___EnforceDistance_3; }
	inline void set_EnforceDistance_3(bool value)
	{
		___EnforceDistance_3 = value;
	}

	inline static int32_t get_offset_of_PositionUpdateSpeed_4() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___PositionUpdateSpeed_4)); }
	inline float get_PositionUpdateSpeed_4() const { return ___PositionUpdateSpeed_4; }
	inline float* get_address_of_PositionUpdateSpeed_4() { return &___PositionUpdateSpeed_4; }
	inline void set_PositionUpdateSpeed_4(float value)
	{
		___PositionUpdateSpeed_4 = value;
	}

	inline static int32_t get_offset_of_SmoothMotion_5() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___SmoothMotion_5)); }
	inline bool get_SmoothMotion_5() const { return ___SmoothMotion_5; }
	inline bool* get_address_of_SmoothMotion_5() { return &___SmoothMotion_5; }
	inline void set_SmoothMotion_5(bool value)
	{
		___SmoothMotion_5 = value;
	}

	inline static int32_t get_offset_of_SmoothingFactor_6() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___SmoothingFactor_6)); }
	inline float get_SmoothingFactor_6() const { return ___SmoothingFactor_6; }
	inline float* get_address_of_SmoothingFactor_6() { return &___SmoothingFactor_6; }
	inline void set_SmoothingFactor_6(float value)
	{
		___SmoothingFactor_6 = value;
	}

	inline static int32_t get_offset_of_tagalongCollider_7() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___tagalongCollider_7)); }
	inline BoxCollider_t1640800422 * get_tagalongCollider_7() const { return ___tagalongCollider_7; }
	inline BoxCollider_t1640800422 ** get_address_of_tagalongCollider_7() { return &___tagalongCollider_7; }
	inline void set_tagalongCollider_7(BoxCollider_t1640800422 * value)
	{
		___tagalongCollider_7 = value;
		Il2CppCodeGenWriteBarrier((&___tagalongCollider_7), value);
	}

	inline static int32_t get_offset_of_interpolator_8() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___interpolator_8)); }
	inline Interpolator_t3604653897 * get_interpolator_8() const { return ___interpolator_8; }
	inline Interpolator_t3604653897 ** get_address_of_interpolator_8() { return &___interpolator_8; }
	inline void set_interpolator_8(Interpolator_t3604653897 * value)
	{
		___interpolator_8 = value;
		Il2CppCodeGenWriteBarrier((&___interpolator_8), value);
	}

	inline static int32_t get_offset_of_frustumPlanes_9() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___frustumPlanes_9)); }
	inline PlaneU5BU5D_t3656189108* get_frustumPlanes_9() const { return ___frustumPlanes_9; }
	inline PlaneU5BU5D_t3656189108** get_address_of_frustumPlanes_9() { return &___frustumPlanes_9; }
	inline void set_frustumPlanes_9(PlaneU5BU5D_t3656189108* value)
	{
		___frustumPlanes_9 = value;
		Il2CppCodeGenWriteBarrier((&___frustumPlanes_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLETAGALONG_T3886944103_H
#ifndef SCENELAUNCHERBUTTON_T1133275438_H
#define SCENELAUNCHERBUTTON_T1133275438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SceneLauncherButton
struct  SceneLauncherButton_t1133275438  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 HoloToolkit.Unity.SceneLauncherButton::<SceneIndex>k__BackingField
	int32_t ___U3CSceneIndexU3Ek__BackingField_2;
	// UnityEngine.Color HoloToolkit.Unity.SceneLauncherButton::HighlightedTextColor
	Color_t2555686324  ___HighlightedTextColor_3;
	// UnityEngine.GameObject HoloToolkit.Unity.SceneLauncherButton::MenuReference
	GameObject_t1113636619 * ___MenuReference_4;
	// System.Boolean HoloToolkit.Unity.SceneLauncherButton::EnableDebug
	bool ___EnableDebug_5;
	// UnityEngine.TextMesh HoloToolkit.Unity.SceneLauncherButton::textMesh
	TextMesh_t1536577757 * ___textMesh_6;
	// UnityEngine.Color HoloToolkit.Unity.SceneLauncherButton::originalTextColor
	Color_t2555686324  ___originalTextColor_7;

public:
	inline static int32_t get_offset_of_U3CSceneIndexU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SceneLauncherButton_t1133275438, ___U3CSceneIndexU3Ek__BackingField_2)); }
	inline int32_t get_U3CSceneIndexU3Ek__BackingField_2() const { return ___U3CSceneIndexU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CSceneIndexU3Ek__BackingField_2() { return &___U3CSceneIndexU3Ek__BackingField_2; }
	inline void set_U3CSceneIndexU3Ek__BackingField_2(int32_t value)
	{
		___U3CSceneIndexU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_HighlightedTextColor_3() { return static_cast<int32_t>(offsetof(SceneLauncherButton_t1133275438, ___HighlightedTextColor_3)); }
	inline Color_t2555686324  get_HighlightedTextColor_3() const { return ___HighlightedTextColor_3; }
	inline Color_t2555686324 * get_address_of_HighlightedTextColor_3() { return &___HighlightedTextColor_3; }
	inline void set_HighlightedTextColor_3(Color_t2555686324  value)
	{
		___HighlightedTextColor_3 = value;
	}

	inline static int32_t get_offset_of_MenuReference_4() { return static_cast<int32_t>(offsetof(SceneLauncherButton_t1133275438, ___MenuReference_4)); }
	inline GameObject_t1113636619 * get_MenuReference_4() const { return ___MenuReference_4; }
	inline GameObject_t1113636619 ** get_address_of_MenuReference_4() { return &___MenuReference_4; }
	inline void set_MenuReference_4(GameObject_t1113636619 * value)
	{
		___MenuReference_4 = value;
		Il2CppCodeGenWriteBarrier((&___MenuReference_4), value);
	}

	inline static int32_t get_offset_of_EnableDebug_5() { return static_cast<int32_t>(offsetof(SceneLauncherButton_t1133275438, ___EnableDebug_5)); }
	inline bool get_EnableDebug_5() const { return ___EnableDebug_5; }
	inline bool* get_address_of_EnableDebug_5() { return &___EnableDebug_5; }
	inline void set_EnableDebug_5(bool value)
	{
		___EnableDebug_5 = value;
	}

	inline static int32_t get_offset_of_textMesh_6() { return static_cast<int32_t>(offsetof(SceneLauncherButton_t1133275438, ___textMesh_6)); }
	inline TextMesh_t1536577757 * get_textMesh_6() const { return ___textMesh_6; }
	inline TextMesh_t1536577757 ** get_address_of_textMesh_6() { return &___textMesh_6; }
	inline void set_textMesh_6(TextMesh_t1536577757 * value)
	{
		___textMesh_6 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_6), value);
	}

	inline static int32_t get_offset_of_originalTextColor_7() { return static_cast<int32_t>(offsetof(SceneLauncherButton_t1133275438, ___originalTextColor_7)); }
	inline Color_t2555686324  get_originalTextColor_7() const { return ___originalTextColor_7; }
	inline Color_t2555686324 * get_address_of_originalTextColor_7() { return &___originalTextColor_7; }
	inline void set_originalTextColor_7(Color_t2555686324  value)
	{
		___originalTextColor_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENELAUNCHERBUTTON_T1133275438_H
#ifndef ADAPTIVEVIEWPORT_T3912946756_H
#define ADAPTIVEVIEWPORT_T3912946756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AdaptiveViewport
struct  AdaptiveViewport_t3912946756  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 HoloToolkit.Unity.AdaptiveViewport::FullSizeQualityLevel
	int32_t ___FullSizeQualityLevel_2;
	// System.Int32 HoloToolkit.Unity.AdaptiveViewport::MinSizeQualityLevel
	int32_t ___MinSizeQualityLevel_3;
	// System.Single HoloToolkit.Unity.AdaptiveViewport::MinViewportSize
	float ___MinViewportSize_4;
	// HoloToolkit.Unity.AdaptiveQuality HoloToolkit.Unity.AdaptiveViewport::qualityController
	AdaptiveQuality_t1304437679 * ___qualityController_5;
	// System.Single HoloToolkit.Unity.AdaptiveViewport::<CurrentScale>k__BackingField
	float ___U3CCurrentScaleU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_FullSizeQualityLevel_2() { return static_cast<int32_t>(offsetof(AdaptiveViewport_t3912946756, ___FullSizeQualityLevel_2)); }
	inline int32_t get_FullSizeQualityLevel_2() const { return ___FullSizeQualityLevel_2; }
	inline int32_t* get_address_of_FullSizeQualityLevel_2() { return &___FullSizeQualityLevel_2; }
	inline void set_FullSizeQualityLevel_2(int32_t value)
	{
		___FullSizeQualityLevel_2 = value;
	}

	inline static int32_t get_offset_of_MinSizeQualityLevel_3() { return static_cast<int32_t>(offsetof(AdaptiveViewport_t3912946756, ___MinSizeQualityLevel_3)); }
	inline int32_t get_MinSizeQualityLevel_3() const { return ___MinSizeQualityLevel_3; }
	inline int32_t* get_address_of_MinSizeQualityLevel_3() { return &___MinSizeQualityLevel_3; }
	inline void set_MinSizeQualityLevel_3(int32_t value)
	{
		___MinSizeQualityLevel_3 = value;
	}

	inline static int32_t get_offset_of_MinViewportSize_4() { return static_cast<int32_t>(offsetof(AdaptiveViewport_t3912946756, ___MinViewportSize_4)); }
	inline float get_MinViewportSize_4() const { return ___MinViewportSize_4; }
	inline float* get_address_of_MinViewportSize_4() { return &___MinViewportSize_4; }
	inline void set_MinViewportSize_4(float value)
	{
		___MinViewportSize_4 = value;
	}

	inline static int32_t get_offset_of_qualityController_5() { return static_cast<int32_t>(offsetof(AdaptiveViewport_t3912946756, ___qualityController_5)); }
	inline AdaptiveQuality_t1304437679 * get_qualityController_5() const { return ___qualityController_5; }
	inline AdaptiveQuality_t1304437679 ** get_address_of_qualityController_5() { return &___qualityController_5; }
	inline void set_qualityController_5(AdaptiveQuality_t1304437679 * value)
	{
		___qualityController_5 = value;
		Il2CppCodeGenWriteBarrier((&___qualityController_5), value);
	}

	inline static int32_t get_offset_of_U3CCurrentScaleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdaptiveViewport_t3912946756, ___U3CCurrentScaleU3Ek__BackingField_6)); }
	inline float get_U3CCurrentScaleU3Ek__BackingField_6() const { return ___U3CCurrentScaleU3Ek__BackingField_6; }
	inline float* get_address_of_U3CCurrentScaleU3Ek__BackingField_6() { return &___U3CCurrentScaleU3Ek__BackingField_6; }
	inline void set_U3CCurrentScaleU3Ek__BackingField_6(float value)
	{
		___U3CCurrentScaleU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADAPTIVEVIEWPORT_T3912946756_H
#ifndef SINGLETON_1_T1952888893_H
#define SINGLETON_1_T1952888893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.SceneLauncher>
struct  Singleton_1_t1952888893  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t1952888893_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	SceneLauncher_t1552664439 * ___instance_2;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1952888893_StaticFields, ___instance_2)); }
	inline SceneLauncher_t1552664439 * get_instance_2() const { return ___instance_2; }
	inline SceneLauncher_t1552664439 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(SceneLauncher_t1552664439 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_searchForInstance_3() { return static_cast<int32_t>(offsetof(Singleton_1_t1952888893_StaticFields, ___searchForInstance_3)); }
	inline bool get_searchForInstance_3() const { return ___searchForInstance_3; }
	inline bool* get_address_of_searchForInstance_3() { return &___searchForInstance_3; }
	inline void set_searchForInstance_3(bool value)
	{
		___searchForInstance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1952888893_H
#ifndef CONTROLLERFINDER_T3151550888_H
#define CONTROLLERFINDER_T3151550888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.ControllerFinder
struct  ControllerFinder_t3151550888  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Unity.InputModule.MotionControllerInfo/ControllerElementEnum HoloToolkit.Unity.InputModule.ControllerFinder::element
	int32_t ___element_2;
	// UnityEngine.XR.WSA.Input.InteractionSourceHandedness HoloToolkit.Unity.InputModule.ControllerFinder::handedness
	int32_t ___handedness_3;
	// UnityEngine.Transform HoloToolkit.Unity.InputModule.ControllerFinder::elementTransform
	Transform_t3600365921 * ___elementTransform_4;
	// HoloToolkit.Unity.InputModule.MotionControllerInfo HoloToolkit.Unity.InputModule.ControllerFinder::ControllerInfo
	MotionControllerInfo_t938152223 * ___ControllerInfo_5;

public:
	inline static int32_t get_offset_of_element_2() { return static_cast<int32_t>(offsetof(ControllerFinder_t3151550888, ___element_2)); }
	inline int32_t get_element_2() const { return ___element_2; }
	inline int32_t* get_address_of_element_2() { return &___element_2; }
	inline void set_element_2(int32_t value)
	{
		___element_2 = value;
	}

	inline static int32_t get_offset_of_handedness_3() { return static_cast<int32_t>(offsetof(ControllerFinder_t3151550888, ___handedness_3)); }
	inline int32_t get_handedness_3() const { return ___handedness_3; }
	inline int32_t* get_address_of_handedness_3() { return &___handedness_3; }
	inline void set_handedness_3(int32_t value)
	{
		___handedness_3 = value;
	}

	inline static int32_t get_offset_of_elementTransform_4() { return static_cast<int32_t>(offsetof(ControllerFinder_t3151550888, ___elementTransform_4)); }
	inline Transform_t3600365921 * get_elementTransform_4() const { return ___elementTransform_4; }
	inline Transform_t3600365921 ** get_address_of_elementTransform_4() { return &___elementTransform_4; }
	inline void set_elementTransform_4(Transform_t3600365921 * value)
	{
		___elementTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___elementTransform_4), value);
	}

	inline static int32_t get_offset_of_ControllerInfo_5() { return static_cast<int32_t>(offsetof(ControllerFinder_t3151550888, ___ControllerInfo_5)); }
	inline MotionControllerInfo_t938152223 * get_ControllerInfo_5() const { return ___ControllerInfo_5; }
	inline MotionControllerInfo_t938152223 ** get_address_of_ControllerInfo_5() { return &___ControllerInfo_5; }
	inline void set_ControllerInfo_5(MotionControllerInfo_t938152223 * value)
	{
		___ControllerInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___ControllerInfo_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERFINDER_T3151550888_H
#ifndef SPATIALUNDERSTANDINGSOURCEMESH_T2539908667_H
#define SPATIALUNDERSTANDINGSOURCEMESH_T2539908667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingSourceMesh
struct  SpatialUnderstandingSourceMesh_t2539908667  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<HoloToolkit.Unity.SpatialUnderstandingDll/MeshData> HoloToolkit.Unity.SpatialUnderstandingSourceMesh::inputMeshList
	List_1_t538943048 * ___inputMeshList_2;

public:
	inline static int32_t get_offset_of_inputMeshList_2() { return static_cast<int32_t>(offsetof(SpatialUnderstandingSourceMesh_t2539908667, ___inputMeshList_2)); }
	inline List_1_t538943048 * get_inputMeshList_2() const { return ___inputMeshList_2; }
	inline List_1_t538943048 ** get_address_of_inputMeshList_2() { return &___inputMeshList_2; }
	inline void set_inputMeshList_2(List_1_t538943048 * value)
	{
		___inputMeshList_2 = value;
		Il2CppCodeGenWriteBarrier((&___inputMeshList_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALUNDERSTANDINGSOURCEMESH_T2539908667_H
#ifndef DEBUGPANELFPSCOUNTER_T4280904583_H
#define DEBUGPANELFPSCOUNTER_T4280904583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DebugPanelFPSCounter
struct  DebugPanelFPSCounter_t4280904583  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 HoloToolkit.Unity.DebugPanelFPSCounter::frameCount
	int32_t ___frameCount_2;
	// System.Int32 HoloToolkit.Unity.DebugPanelFPSCounter::framesPerSecond
	int32_t ___framesPerSecond_3;
	// System.Int32 HoloToolkit.Unity.DebugPanelFPSCounter::lastWholeTime
	int32_t ___lastWholeTime_4;

public:
	inline static int32_t get_offset_of_frameCount_2() { return static_cast<int32_t>(offsetof(DebugPanelFPSCounter_t4280904583, ___frameCount_2)); }
	inline int32_t get_frameCount_2() const { return ___frameCount_2; }
	inline int32_t* get_address_of_frameCount_2() { return &___frameCount_2; }
	inline void set_frameCount_2(int32_t value)
	{
		___frameCount_2 = value;
	}

	inline static int32_t get_offset_of_framesPerSecond_3() { return static_cast<int32_t>(offsetof(DebugPanelFPSCounter_t4280904583, ___framesPerSecond_3)); }
	inline int32_t get_framesPerSecond_3() const { return ___framesPerSecond_3; }
	inline int32_t* get_address_of_framesPerSecond_3() { return &___framesPerSecond_3; }
	inline void set_framesPerSecond_3(int32_t value)
	{
		___framesPerSecond_3 = value;
	}

	inline static int32_t get_offset_of_lastWholeTime_4() { return static_cast<int32_t>(offsetof(DebugPanelFPSCounter_t4280904583, ___lastWholeTime_4)); }
	inline int32_t get_lastWholeTime_4() const { return ___lastWholeTime_4; }
	inline int32_t* get_address_of_lastWholeTime_4() { return &___lastWholeTime_4; }
	inline void set_lastWholeTime_4(int32_t value)
	{
		___lastWholeTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGPANELFPSCOUNTER_T4280904583_H
#ifndef GPUTIMINGCAMERA_T3799301742_H
#define GPUTIMINGCAMERA_T3799301742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.GpuTimingCamera
struct  GpuTimingCamera_t3799301742  : public MonoBehaviour_t3962482529
{
public:
	// System.String HoloToolkit.Unity.GpuTimingCamera::TimingTag
	String_t* ___TimingTag_2;
	// UnityEngine.Camera HoloToolkit.Unity.GpuTimingCamera::timingCamera
	Camera_t4157153871 * ___timingCamera_3;

public:
	inline static int32_t get_offset_of_TimingTag_2() { return static_cast<int32_t>(offsetof(GpuTimingCamera_t3799301742, ___TimingTag_2)); }
	inline String_t* get_TimingTag_2() const { return ___TimingTag_2; }
	inline String_t** get_address_of_TimingTag_2() { return &___TimingTag_2; }
	inline void set_TimingTag_2(String_t* value)
	{
		___TimingTag_2 = value;
		Il2CppCodeGenWriteBarrier((&___TimingTag_2), value);
	}

	inline static int32_t get_offset_of_timingCamera_3() { return static_cast<int32_t>(offsetof(GpuTimingCamera_t3799301742, ___timingCamera_3)); }
	inline Camera_t4157153871 * get_timingCamera_3() const { return ___timingCamera_3; }
	inline Camera_t4157153871 ** get_address_of_timingCamera_3() { return &___timingCamera_3; }
	inline void set_timingCamera_3(Camera_t4157153871 * value)
	{
		___timingCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___timingCamera_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GPUTIMINGCAMERA_T3799301742_H
#ifndef DEBUGPANELCONTROLLERINFO_T974145763_H
#define DEBUGPANELCONTROLLERINFO_T974145763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DebugPanelControllerInfo
struct  DebugPanelControllerInfo_t974145763  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Dictionary`2<System.UInt32,HoloToolkit.Unity.DebugPanelControllerInfo/ControllerState> HoloToolkit.Unity.DebugPanelControllerInfo::controllers
	Dictionary_2_t1714548309 * ___controllers_2;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextPointerPosition
	TextMesh_t1536577757 * ___LeftInfoTextPointerPosition_3;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextPointerRotation
	TextMesh_t1536577757 * ___LeftInfoTextPointerRotation_4;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextGripPosition
	TextMesh_t1536577757 * ___LeftInfoTextGripPosition_5;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextGripRotation
	TextMesh_t1536577757 * ___LeftInfoTextGripRotation_6;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextGripGrasped
	TextMesh_t1536577757 * ___LeftInfoTextGripGrasped_7;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextMenuPressed
	TextMesh_t1536577757 * ___LeftInfoTextMenuPressed_8;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextTriggerPressed
	TextMesh_t1536577757 * ___LeftInfoTextTriggerPressed_9;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextTriggerPressedAmount
	TextMesh_t1536577757 * ___LeftInfoTextTriggerPressedAmount_10;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextThumbstickPressed
	TextMesh_t1536577757 * ___LeftInfoTextThumbstickPressed_11;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextThumbstickPosition
	TextMesh_t1536577757 * ___LeftInfoTextThumbstickPosition_12;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextTouchpadPressed
	TextMesh_t1536577757 * ___LeftInfoTextTouchpadPressed_13;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextTouchpadTouched
	TextMesh_t1536577757 * ___LeftInfoTextTouchpadTouched_14;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextTouchpadPosition
	TextMesh_t1536577757 * ___LeftInfoTextTouchpadPosition_15;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextPointerPosition
	TextMesh_t1536577757 * ___RightInfoTextPointerPosition_16;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextPointerRotation
	TextMesh_t1536577757 * ___RightInfoTextPointerRotation_17;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextGripPosition
	TextMesh_t1536577757 * ___RightInfoTextGripPosition_18;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextGripRotation
	TextMesh_t1536577757 * ___RightInfoTextGripRotation_19;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextGripGrasped
	TextMesh_t1536577757 * ___RightInfoTextGripGrasped_20;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextMenuPressed
	TextMesh_t1536577757 * ___RightInfoTextMenuPressed_21;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextTriggerPressed
	TextMesh_t1536577757 * ___RightInfoTextTriggerPressed_22;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextTriggerPressedAmount
	TextMesh_t1536577757 * ___RightInfoTextTriggerPressedAmount_23;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextThumbstickPressed
	TextMesh_t1536577757 * ___RightInfoTextThumbstickPressed_24;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextThumbstickPosition
	TextMesh_t1536577757 * ___RightInfoTextThumbstickPosition_25;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextTouchpadPressed
	TextMesh_t1536577757 * ___RightInfoTextTouchpadPressed_26;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextTouchpadTouched
	TextMesh_t1536577757 * ___RightInfoTextTouchpadTouched_27;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextTouchpadPosition
	TextMesh_t1536577757 * ___RightInfoTextTouchpadPosition_28;

public:
	inline static int32_t get_offset_of_controllers_2() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___controllers_2)); }
	inline Dictionary_2_t1714548309 * get_controllers_2() const { return ___controllers_2; }
	inline Dictionary_2_t1714548309 ** get_address_of_controllers_2() { return &___controllers_2; }
	inline void set_controllers_2(Dictionary_2_t1714548309 * value)
	{
		___controllers_2 = value;
		Il2CppCodeGenWriteBarrier((&___controllers_2), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextPointerPosition_3() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___LeftInfoTextPointerPosition_3)); }
	inline TextMesh_t1536577757 * get_LeftInfoTextPointerPosition_3() const { return ___LeftInfoTextPointerPosition_3; }
	inline TextMesh_t1536577757 ** get_address_of_LeftInfoTextPointerPosition_3() { return &___LeftInfoTextPointerPosition_3; }
	inline void set_LeftInfoTextPointerPosition_3(TextMesh_t1536577757 * value)
	{
		___LeftInfoTextPointerPosition_3 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextPointerPosition_3), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextPointerRotation_4() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___LeftInfoTextPointerRotation_4)); }
	inline TextMesh_t1536577757 * get_LeftInfoTextPointerRotation_4() const { return ___LeftInfoTextPointerRotation_4; }
	inline TextMesh_t1536577757 ** get_address_of_LeftInfoTextPointerRotation_4() { return &___LeftInfoTextPointerRotation_4; }
	inline void set_LeftInfoTextPointerRotation_4(TextMesh_t1536577757 * value)
	{
		___LeftInfoTextPointerRotation_4 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextPointerRotation_4), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextGripPosition_5() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___LeftInfoTextGripPosition_5)); }
	inline TextMesh_t1536577757 * get_LeftInfoTextGripPosition_5() const { return ___LeftInfoTextGripPosition_5; }
	inline TextMesh_t1536577757 ** get_address_of_LeftInfoTextGripPosition_5() { return &___LeftInfoTextGripPosition_5; }
	inline void set_LeftInfoTextGripPosition_5(TextMesh_t1536577757 * value)
	{
		___LeftInfoTextGripPosition_5 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextGripPosition_5), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextGripRotation_6() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___LeftInfoTextGripRotation_6)); }
	inline TextMesh_t1536577757 * get_LeftInfoTextGripRotation_6() const { return ___LeftInfoTextGripRotation_6; }
	inline TextMesh_t1536577757 ** get_address_of_LeftInfoTextGripRotation_6() { return &___LeftInfoTextGripRotation_6; }
	inline void set_LeftInfoTextGripRotation_6(TextMesh_t1536577757 * value)
	{
		___LeftInfoTextGripRotation_6 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextGripRotation_6), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextGripGrasped_7() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___LeftInfoTextGripGrasped_7)); }
	inline TextMesh_t1536577757 * get_LeftInfoTextGripGrasped_7() const { return ___LeftInfoTextGripGrasped_7; }
	inline TextMesh_t1536577757 ** get_address_of_LeftInfoTextGripGrasped_7() { return &___LeftInfoTextGripGrasped_7; }
	inline void set_LeftInfoTextGripGrasped_7(TextMesh_t1536577757 * value)
	{
		___LeftInfoTextGripGrasped_7 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextGripGrasped_7), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextMenuPressed_8() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___LeftInfoTextMenuPressed_8)); }
	inline TextMesh_t1536577757 * get_LeftInfoTextMenuPressed_8() const { return ___LeftInfoTextMenuPressed_8; }
	inline TextMesh_t1536577757 ** get_address_of_LeftInfoTextMenuPressed_8() { return &___LeftInfoTextMenuPressed_8; }
	inline void set_LeftInfoTextMenuPressed_8(TextMesh_t1536577757 * value)
	{
		___LeftInfoTextMenuPressed_8 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextMenuPressed_8), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextTriggerPressed_9() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___LeftInfoTextTriggerPressed_9)); }
	inline TextMesh_t1536577757 * get_LeftInfoTextTriggerPressed_9() const { return ___LeftInfoTextTriggerPressed_9; }
	inline TextMesh_t1536577757 ** get_address_of_LeftInfoTextTriggerPressed_9() { return &___LeftInfoTextTriggerPressed_9; }
	inline void set_LeftInfoTextTriggerPressed_9(TextMesh_t1536577757 * value)
	{
		___LeftInfoTextTriggerPressed_9 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextTriggerPressed_9), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextTriggerPressedAmount_10() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___LeftInfoTextTriggerPressedAmount_10)); }
	inline TextMesh_t1536577757 * get_LeftInfoTextTriggerPressedAmount_10() const { return ___LeftInfoTextTriggerPressedAmount_10; }
	inline TextMesh_t1536577757 ** get_address_of_LeftInfoTextTriggerPressedAmount_10() { return &___LeftInfoTextTriggerPressedAmount_10; }
	inline void set_LeftInfoTextTriggerPressedAmount_10(TextMesh_t1536577757 * value)
	{
		___LeftInfoTextTriggerPressedAmount_10 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextTriggerPressedAmount_10), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextThumbstickPressed_11() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___LeftInfoTextThumbstickPressed_11)); }
	inline TextMesh_t1536577757 * get_LeftInfoTextThumbstickPressed_11() const { return ___LeftInfoTextThumbstickPressed_11; }
	inline TextMesh_t1536577757 ** get_address_of_LeftInfoTextThumbstickPressed_11() { return &___LeftInfoTextThumbstickPressed_11; }
	inline void set_LeftInfoTextThumbstickPressed_11(TextMesh_t1536577757 * value)
	{
		___LeftInfoTextThumbstickPressed_11 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextThumbstickPressed_11), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextThumbstickPosition_12() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___LeftInfoTextThumbstickPosition_12)); }
	inline TextMesh_t1536577757 * get_LeftInfoTextThumbstickPosition_12() const { return ___LeftInfoTextThumbstickPosition_12; }
	inline TextMesh_t1536577757 ** get_address_of_LeftInfoTextThumbstickPosition_12() { return &___LeftInfoTextThumbstickPosition_12; }
	inline void set_LeftInfoTextThumbstickPosition_12(TextMesh_t1536577757 * value)
	{
		___LeftInfoTextThumbstickPosition_12 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextThumbstickPosition_12), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextTouchpadPressed_13() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___LeftInfoTextTouchpadPressed_13)); }
	inline TextMesh_t1536577757 * get_LeftInfoTextTouchpadPressed_13() const { return ___LeftInfoTextTouchpadPressed_13; }
	inline TextMesh_t1536577757 ** get_address_of_LeftInfoTextTouchpadPressed_13() { return &___LeftInfoTextTouchpadPressed_13; }
	inline void set_LeftInfoTextTouchpadPressed_13(TextMesh_t1536577757 * value)
	{
		___LeftInfoTextTouchpadPressed_13 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextTouchpadPressed_13), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextTouchpadTouched_14() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___LeftInfoTextTouchpadTouched_14)); }
	inline TextMesh_t1536577757 * get_LeftInfoTextTouchpadTouched_14() const { return ___LeftInfoTextTouchpadTouched_14; }
	inline TextMesh_t1536577757 ** get_address_of_LeftInfoTextTouchpadTouched_14() { return &___LeftInfoTextTouchpadTouched_14; }
	inline void set_LeftInfoTextTouchpadTouched_14(TextMesh_t1536577757 * value)
	{
		___LeftInfoTextTouchpadTouched_14 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextTouchpadTouched_14), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextTouchpadPosition_15() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___LeftInfoTextTouchpadPosition_15)); }
	inline TextMesh_t1536577757 * get_LeftInfoTextTouchpadPosition_15() const { return ___LeftInfoTextTouchpadPosition_15; }
	inline TextMesh_t1536577757 ** get_address_of_LeftInfoTextTouchpadPosition_15() { return &___LeftInfoTextTouchpadPosition_15; }
	inline void set_LeftInfoTextTouchpadPosition_15(TextMesh_t1536577757 * value)
	{
		___LeftInfoTextTouchpadPosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextTouchpadPosition_15), value);
	}

	inline static int32_t get_offset_of_RightInfoTextPointerPosition_16() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___RightInfoTextPointerPosition_16)); }
	inline TextMesh_t1536577757 * get_RightInfoTextPointerPosition_16() const { return ___RightInfoTextPointerPosition_16; }
	inline TextMesh_t1536577757 ** get_address_of_RightInfoTextPointerPosition_16() { return &___RightInfoTextPointerPosition_16; }
	inline void set_RightInfoTextPointerPosition_16(TextMesh_t1536577757 * value)
	{
		___RightInfoTextPointerPosition_16 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextPointerPosition_16), value);
	}

	inline static int32_t get_offset_of_RightInfoTextPointerRotation_17() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___RightInfoTextPointerRotation_17)); }
	inline TextMesh_t1536577757 * get_RightInfoTextPointerRotation_17() const { return ___RightInfoTextPointerRotation_17; }
	inline TextMesh_t1536577757 ** get_address_of_RightInfoTextPointerRotation_17() { return &___RightInfoTextPointerRotation_17; }
	inline void set_RightInfoTextPointerRotation_17(TextMesh_t1536577757 * value)
	{
		___RightInfoTextPointerRotation_17 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextPointerRotation_17), value);
	}

	inline static int32_t get_offset_of_RightInfoTextGripPosition_18() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___RightInfoTextGripPosition_18)); }
	inline TextMesh_t1536577757 * get_RightInfoTextGripPosition_18() const { return ___RightInfoTextGripPosition_18; }
	inline TextMesh_t1536577757 ** get_address_of_RightInfoTextGripPosition_18() { return &___RightInfoTextGripPosition_18; }
	inline void set_RightInfoTextGripPosition_18(TextMesh_t1536577757 * value)
	{
		___RightInfoTextGripPosition_18 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextGripPosition_18), value);
	}

	inline static int32_t get_offset_of_RightInfoTextGripRotation_19() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___RightInfoTextGripRotation_19)); }
	inline TextMesh_t1536577757 * get_RightInfoTextGripRotation_19() const { return ___RightInfoTextGripRotation_19; }
	inline TextMesh_t1536577757 ** get_address_of_RightInfoTextGripRotation_19() { return &___RightInfoTextGripRotation_19; }
	inline void set_RightInfoTextGripRotation_19(TextMesh_t1536577757 * value)
	{
		___RightInfoTextGripRotation_19 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextGripRotation_19), value);
	}

	inline static int32_t get_offset_of_RightInfoTextGripGrasped_20() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___RightInfoTextGripGrasped_20)); }
	inline TextMesh_t1536577757 * get_RightInfoTextGripGrasped_20() const { return ___RightInfoTextGripGrasped_20; }
	inline TextMesh_t1536577757 ** get_address_of_RightInfoTextGripGrasped_20() { return &___RightInfoTextGripGrasped_20; }
	inline void set_RightInfoTextGripGrasped_20(TextMesh_t1536577757 * value)
	{
		___RightInfoTextGripGrasped_20 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextGripGrasped_20), value);
	}

	inline static int32_t get_offset_of_RightInfoTextMenuPressed_21() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___RightInfoTextMenuPressed_21)); }
	inline TextMesh_t1536577757 * get_RightInfoTextMenuPressed_21() const { return ___RightInfoTextMenuPressed_21; }
	inline TextMesh_t1536577757 ** get_address_of_RightInfoTextMenuPressed_21() { return &___RightInfoTextMenuPressed_21; }
	inline void set_RightInfoTextMenuPressed_21(TextMesh_t1536577757 * value)
	{
		___RightInfoTextMenuPressed_21 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextMenuPressed_21), value);
	}

	inline static int32_t get_offset_of_RightInfoTextTriggerPressed_22() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___RightInfoTextTriggerPressed_22)); }
	inline TextMesh_t1536577757 * get_RightInfoTextTriggerPressed_22() const { return ___RightInfoTextTriggerPressed_22; }
	inline TextMesh_t1536577757 ** get_address_of_RightInfoTextTriggerPressed_22() { return &___RightInfoTextTriggerPressed_22; }
	inline void set_RightInfoTextTriggerPressed_22(TextMesh_t1536577757 * value)
	{
		___RightInfoTextTriggerPressed_22 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextTriggerPressed_22), value);
	}

	inline static int32_t get_offset_of_RightInfoTextTriggerPressedAmount_23() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___RightInfoTextTriggerPressedAmount_23)); }
	inline TextMesh_t1536577757 * get_RightInfoTextTriggerPressedAmount_23() const { return ___RightInfoTextTriggerPressedAmount_23; }
	inline TextMesh_t1536577757 ** get_address_of_RightInfoTextTriggerPressedAmount_23() { return &___RightInfoTextTriggerPressedAmount_23; }
	inline void set_RightInfoTextTriggerPressedAmount_23(TextMesh_t1536577757 * value)
	{
		___RightInfoTextTriggerPressedAmount_23 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextTriggerPressedAmount_23), value);
	}

	inline static int32_t get_offset_of_RightInfoTextThumbstickPressed_24() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___RightInfoTextThumbstickPressed_24)); }
	inline TextMesh_t1536577757 * get_RightInfoTextThumbstickPressed_24() const { return ___RightInfoTextThumbstickPressed_24; }
	inline TextMesh_t1536577757 ** get_address_of_RightInfoTextThumbstickPressed_24() { return &___RightInfoTextThumbstickPressed_24; }
	inline void set_RightInfoTextThumbstickPressed_24(TextMesh_t1536577757 * value)
	{
		___RightInfoTextThumbstickPressed_24 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextThumbstickPressed_24), value);
	}

	inline static int32_t get_offset_of_RightInfoTextThumbstickPosition_25() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___RightInfoTextThumbstickPosition_25)); }
	inline TextMesh_t1536577757 * get_RightInfoTextThumbstickPosition_25() const { return ___RightInfoTextThumbstickPosition_25; }
	inline TextMesh_t1536577757 ** get_address_of_RightInfoTextThumbstickPosition_25() { return &___RightInfoTextThumbstickPosition_25; }
	inline void set_RightInfoTextThumbstickPosition_25(TextMesh_t1536577757 * value)
	{
		___RightInfoTextThumbstickPosition_25 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextThumbstickPosition_25), value);
	}

	inline static int32_t get_offset_of_RightInfoTextTouchpadPressed_26() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___RightInfoTextTouchpadPressed_26)); }
	inline TextMesh_t1536577757 * get_RightInfoTextTouchpadPressed_26() const { return ___RightInfoTextTouchpadPressed_26; }
	inline TextMesh_t1536577757 ** get_address_of_RightInfoTextTouchpadPressed_26() { return &___RightInfoTextTouchpadPressed_26; }
	inline void set_RightInfoTextTouchpadPressed_26(TextMesh_t1536577757 * value)
	{
		___RightInfoTextTouchpadPressed_26 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextTouchpadPressed_26), value);
	}

	inline static int32_t get_offset_of_RightInfoTextTouchpadTouched_27() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___RightInfoTextTouchpadTouched_27)); }
	inline TextMesh_t1536577757 * get_RightInfoTextTouchpadTouched_27() const { return ___RightInfoTextTouchpadTouched_27; }
	inline TextMesh_t1536577757 ** get_address_of_RightInfoTextTouchpadTouched_27() { return &___RightInfoTextTouchpadTouched_27; }
	inline void set_RightInfoTextTouchpadTouched_27(TextMesh_t1536577757 * value)
	{
		___RightInfoTextTouchpadTouched_27 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextTouchpadTouched_27), value);
	}

	inline static int32_t get_offset_of_RightInfoTextTouchpadPosition_28() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t974145763, ___RightInfoTextTouchpadPosition_28)); }
	inline TextMesh_t1536577757 * get_RightInfoTextTouchpadPosition_28() const { return ___RightInfoTextTouchpadPosition_28; }
	inline TextMesh_t1536577757 ** get_address_of_RightInfoTextTouchpadPosition_28() { return &___RightInfoTextTouchpadPosition_28; }
	inline void set_RightInfoTextTouchpadPosition_28(TextMesh_t1536577757 * value)
	{
		___RightInfoTextTouchpadPosition_28 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextTouchpadPosition_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGPANELCONTROLLERINFO_T974145763_H
#ifndef FIXEDANGULARSIZE_T1485950223_H
#define FIXEDANGULARSIZE_T1485950223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.FixedAngularSize
struct  FixedAngularSize_t1485950223  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HoloToolkit.Unity.FixedAngularSize::SizeRatio
	float ___SizeRatio_2;
	// System.Single HoloToolkit.Unity.FixedAngularSize::startingDistance
	float ___startingDistance_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.FixedAngularSize::startingScale
	Vector3_t3722313464  ___startingScale_4;

public:
	inline static int32_t get_offset_of_SizeRatio_2() { return static_cast<int32_t>(offsetof(FixedAngularSize_t1485950223, ___SizeRatio_2)); }
	inline float get_SizeRatio_2() const { return ___SizeRatio_2; }
	inline float* get_address_of_SizeRatio_2() { return &___SizeRatio_2; }
	inline void set_SizeRatio_2(float value)
	{
		___SizeRatio_2 = value;
	}

	inline static int32_t get_offset_of_startingDistance_3() { return static_cast<int32_t>(offsetof(FixedAngularSize_t1485950223, ___startingDistance_3)); }
	inline float get_startingDistance_3() const { return ___startingDistance_3; }
	inline float* get_address_of_startingDistance_3() { return &___startingDistance_3; }
	inline void set_startingDistance_3(float value)
	{
		___startingDistance_3 = value;
	}

	inline static int32_t get_offset_of_startingScale_4() { return static_cast<int32_t>(offsetof(FixedAngularSize_t1485950223, ___startingScale_4)); }
	inline Vector3_t3722313464  get_startingScale_4() const { return ___startingScale_4; }
	inline Vector3_t3722313464 * get_address_of_startingScale_4() { return &___startingScale_4; }
	inline void set_startingScale_4(Vector3_t3722313464  value)
	{
		___startingScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDANGULARSIZE_T1485950223_H
#ifndef FPSDISPLAY_T3517157742_H
#define FPSDISPLAY_T3517157742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.FpsDisplay
struct  FpsDisplay_t3517157742  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.TextMesh HoloToolkit.Unity.FpsDisplay::textMesh
	TextMesh_t1536577757 * ___textMesh_2;
	// UnityEngine.UI.Text HoloToolkit.Unity.FpsDisplay::uGUIText
	Text_t1901882714 * ___uGUIText_3;
	// System.Int32 HoloToolkit.Unity.FpsDisplay::frameRange
	int32_t ___frameRange_4;
	// System.Int32 HoloToolkit.Unity.FpsDisplay::averageFps
	int32_t ___averageFps_5;
	// System.Int32[] HoloToolkit.Unity.FpsDisplay::fpsBuffer
	Int32U5BU5D_t385246372* ___fpsBuffer_6;
	// System.Int32 HoloToolkit.Unity.FpsDisplay::fpsBufferIndex
	int32_t ___fpsBufferIndex_7;

public:
	inline static int32_t get_offset_of_textMesh_2() { return static_cast<int32_t>(offsetof(FpsDisplay_t3517157742, ___textMesh_2)); }
	inline TextMesh_t1536577757 * get_textMesh_2() const { return ___textMesh_2; }
	inline TextMesh_t1536577757 ** get_address_of_textMesh_2() { return &___textMesh_2; }
	inline void set_textMesh_2(TextMesh_t1536577757 * value)
	{
		___textMesh_2 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_2), value);
	}

	inline static int32_t get_offset_of_uGUIText_3() { return static_cast<int32_t>(offsetof(FpsDisplay_t3517157742, ___uGUIText_3)); }
	inline Text_t1901882714 * get_uGUIText_3() const { return ___uGUIText_3; }
	inline Text_t1901882714 ** get_address_of_uGUIText_3() { return &___uGUIText_3; }
	inline void set_uGUIText_3(Text_t1901882714 * value)
	{
		___uGUIText_3 = value;
		Il2CppCodeGenWriteBarrier((&___uGUIText_3), value);
	}

	inline static int32_t get_offset_of_frameRange_4() { return static_cast<int32_t>(offsetof(FpsDisplay_t3517157742, ___frameRange_4)); }
	inline int32_t get_frameRange_4() const { return ___frameRange_4; }
	inline int32_t* get_address_of_frameRange_4() { return &___frameRange_4; }
	inline void set_frameRange_4(int32_t value)
	{
		___frameRange_4 = value;
	}

	inline static int32_t get_offset_of_averageFps_5() { return static_cast<int32_t>(offsetof(FpsDisplay_t3517157742, ___averageFps_5)); }
	inline int32_t get_averageFps_5() const { return ___averageFps_5; }
	inline int32_t* get_address_of_averageFps_5() { return &___averageFps_5; }
	inline void set_averageFps_5(int32_t value)
	{
		___averageFps_5 = value;
	}

	inline static int32_t get_offset_of_fpsBuffer_6() { return static_cast<int32_t>(offsetof(FpsDisplay_t3517157742, ___fpsBuffer_6)); }
	inline Int32U5BU5D_t385246372* get_fpsBuffer_6() const { return ___fpsBuffer_6; }
	inline Int32U5BU5D_t385246372** get_address_of_fpsBuffer_6() { return &___fpsBuffer_6; }
	inline void set_fpsBuffer_6(Int32U5BU5D_t385246372* value)
	{
		___fpsBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((&___fpsBuffer_6), value);
	}

	inline static int32_t get_offset_of_fpsBufferIndex_7() { return static_cast<int32_t>(offsetof(FpsDisplay_t3517157742, ___fpsBufferIndex_7)); }
	inline int32_t get_fpsBufferIndex_7() const { return ___fpsBufferIndex_7; }
	inline int32_t* get_address_of_fpsBufferIndex_7() { return &___fpsBufferIndex_7; }
	inline void set_fpsBufferIndex_7(int32_t value)
	{
		___fpsBufferIndex_7 = value;
	}
};

struct FpsDisplay_t3517157742_StaticFields
{
public:
	// System.String[] HoloToolkit.Unity.FpsDisplay::StringsFrom00To99
	StringU5BU5D_t1281789340* ___StringsFrom00To99_8;

public:
	inline static int32_t get_offset_of_StringsFrom00To99_8() { return static_cast<int32_t>(offsetof(FpsDisplay_t3517157742_StaticFields, ___StringsFrom00To99_8)); }
	inline StringU5BU5D_t1281789340* get_StringsFrom00To99_8() const { return ___StringsFrom00To99_8; }
	inline StringU5BU5D_t1281789340** get_address_of_StringsFrom00To99_8() { return &___StringsFrom00To99_8; }
	inline void set_StringsFrom00To99_8(StringU5BU5D_t1281789340* value)
	{
		___StringsFrom00To99_8 = value;
		Il2CppCodeGenWriteBarrier((&___StringsFrom00To99_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSDISPLAY_T3517157742_H
#ifndef DIRECTIONINDICATOR_T2683274306_H
#define DIRECTIONINDICATOR_T2683274306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DirectionIndicator
struct  DirectionIndicator_t2683274306  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject HoloToolkit.Unity.DirectionIndicator::Cursor
	GameObject_t1113636619 * ___Cursor_2;
	// UnityEngine.GameObject HoloToolkit.Unity.DirectionIndicator::DirectionIndicatorObject
	GameObject_t1113636619 * ___DirectionIndicatorObject_3;
	// UnityEngine.Color HoloToolkit.Unity.DirectionIndicator::DirectionIndicatorColor
	Color_t2555686324  ___DirectionIndicatorColor_4;
	// System.Single HoloToolkit.Unity.DirectionIndicator::VisibilitySafeFactor
	float ___VisibilitySafeFactor_5;
	// System.Single HoloToolkit.Unity.DirectionIndicator::MetersFromCursor
	float ___MetersFromCursor_6;
	// UnityEngine.Quaternion HoloToolkit.Unity.DirectionIndicator::directionIndicatorDefaultRotation
	Quaternion_t2301928331  ___directionIndicatorDefaultRotation_7;
	// UnityEngine.Renderer HoloToolkit.Unity.DirectionIndicator::directionIndicatorRenderer
	Renderer_t2627027031 * ___directionIndicatorRenderer_8;
	// UnityEngine.Material HoloToolkit.Unity.DirectionIndicator::indicatorMaterial
	Material_t340375123 * ___indicatorMaterial_9;
	// System.Boolean HoloToolkit.Unity.DirectionIndicator::isDirectionIndicatorVisible
	bool ___isDirectionIndicatorVisible_10;

public:
	inline static int32_t get_offset_of_Cursor_2() { return static_cast<int32_t>(offsetof(DirectionIndicator_t2683274306, ___Cursor_2)); }
	inline GameObject_t1113636619 * get_Cursor_2() const { return ___Cursor_2; }
	inline GameObject_t1113636619 ** get_address_of_Cursor_2() { return &___Cursor_2; }
	inline void set_Cursor_2(GameObject_t1113636619 * value)
	{
		___Cursor_2 = value;
		Il2CppCodeGenWriteBarrier((&___Cursor_2), value);
	}

	inline static int32_t get_offset_of_DirectionIndicatorObject_3() { return static_cast<int32_t>(offsetof(DirectionIndicator_t2683274306, ___DirectionIndicatorObject_3)); }
	inline GameObject_t1113636619 * get_DirectionIndicatorObject_3() const { return ___DirectionIndicatorObject_3; }
	inline GameObject_t1113636619 ** get_address_of_DirectionIndicatorObject_3() { return &___DirectionIndicatorObject_3; }
	inline void set_DirectionIndicatorObject_3(GameObject_t1113636619 * value)
	{
		___DirectionIndicatorObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___DirectionIndicatorObject_3), value);
	}

	inline static int32_t get_offset_of_DirectionIndicatorColor_4() { return static_cast<int32_t>(offsetof(DirectionIndicator_t2683274306, ___DirectionIndicatorColor_4)); }
	inline Color_t2555686324  get_DirectionIndicatorColor_4() const { return ___DirectionIndicatorColor_4; }
	inline Color_t2555686324 * get_address_of_DirectionIndicatorColor_4() { return &___DirectionIndicatorColor_4; }
	inline void set_DirectionIndicatorColor_4(Color_t2555686324  value)
	{
		___DirectionIndicatorColor_4 = value;
	}

	inline static int32_t get_offset_of_VisibilitySafeFactor_5() { return static_cast<int32_t>(offsetof(DirectionIndicator_t2683274306, ___VisibilitySafeFactor_5)); }
	inline float get_VisibilitySafeFactor_5() const { return ___VisibilitySafeFactor_5; }
	inline float* get_address_of_VisibilitySafeFactor_5() { return &___VisibilitySafeFactor_5; }
	inline void set_VisibilitySafeFactor_5(float value)
	{
		___VisibilitySafeFactor_5 = value;
	}

	inline static int32_t get_offset_of_MetersFromCursor_6() { return static_cast<int32_t>(offsetof(DirectionIndicator_t2683274306, ___MetersFromCursor_6)); }
	inline float get_MetersFromCursor_6() const { return ___MetersFromCursor_6; }
	inline float* get_address_of_MetersFromCursor_6() { return &___MetersFromCursor_6; }
	inline void set_MetersFromCursor_6(float value)
	{
		___MetersFromCursor_6 = value;
	}

	inline static int32_t get_offset_of_directionIndicatorDefaultRotation_7() { return static_cast<int32_t>(offsetof(DirectionIndicator_t2683274306, ___directionIndicatorDefaultRotation_7)); }
	inline Quaternion_t2301928331  get_directionIndicatorDefaultRotation_7() const { return ___directionIndicatorDefaultRotation_7; }
	inline Quaternion_t2301928331 * get_address_of_directionIndicatorDefaultRotation_7() { return &___directionIndicatorDefaultRotation_7; }
	inline void set_directionIndicatorDefaultRotation_7(Quaternion_t2301928331  value)
	{
		___directionIndicatorDefaultRotation_7 = value;
	}

	inline static int32_t get_offset_of_directionIndicatorRenderer_8() { return static_cast<int32_t>(offsetof(DirectionIndicator_t2683274306, ___directionIndicatorRenderer_8)); }
	inline Renderer_t2627027031 * get_directionIndicatorRenderer_8() const { return ___directionIndicatorRenderer_8; }
	inline Renderer_t2627027031 ** get_address_of_directionIndicatorRenderer_8() { return &___directionIndicatorRenderer_8; }
	inline void set_directionIndicatorRenderer_8(Renderer_t2627027031 * value)
	{
		___directionIndicatorRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___directionIndicatorRenderer_8), value);
	}

	inline static int32_t get_offset_of_indicatorMaterial_9() { return static_cast<int32_t>(offsetof(DirectionIndicator_t2683274306, ___indicatorMaterial_9)); }
	inline Material_t340375123 * get_indicatorMaterial_9() const { return ___indicatorMaterial_9; }
	inline Material_t340375123 ** get_address_of_indicatorMaterial_9() { return &___indicatorMaterial_9; }
	inline void set_indicatorMaterial_9(Material_t340375123 * value)
	{
		___indicatorMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___indicatorMaterial_9), value);
	}

	inline static int32_t get_offset_of_isDirectionIndicatorVisible_10() { return static_cast<int32_t>(offsetof(DirectionIndicator_t2683274306, ___isDirectionIndicatorVisible_10)); }
	inline bool get_isDirectionIndicatorVisible_10() const { return ___isDirectionIndicatorVisible_10; }
	inline bool* get_address_of_isDirectionIndicatorVisible_10() { return &___isDirectionIndicatorVisible_10; }
	inline void set_isDirectionIndicatorVisible_10(bool value)
	{
		___isDirectionIndicatorVisible_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONINDICATOR_T2683274306_H
#ifndef CANVASHELPER_T3155631873_H
#define CANVASHELPER_T3155631873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.CanvasHelper
struct  CanvasHelper_t3155631873  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Canvas HoloToolkit.Unity.CanvasHelper::Canvas
	Canvas_t3310196443 * ___Canvas_2;

public:
	inline static int32_t get_offset_of_Canvas_2() { return static_cast<int32_t>(offsetof(CanvasHelper_t3155631873, ___Canvas_2)); }
	inline Canvas_t3310196443 * get_Canvas_2() const { return ___Canvas_2; }
	inline Canvas_t3310196443 ** get_address_of_Canvas_2() { return &___Canvas_2; }
	inline void set_Canvas_2(Canvas_t3310196443 * value)
	{
		___Canvas_2 = value;
		Il2CppCodeGenWriteBarrier((&___Canvas_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASHELPER_T3155631873_H
#ifndef BILLBOARD_T2829830782_H
#define BILLBOARD_T2829830782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Billboard
struct  Billboard_t2829830782  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Unity.PivotAxis HoloToolkit.Unity.Billboard::PivotAxis
	int32_t ___PivotAxis_2;
	// UnityEngine.Transform HoloToolkit.Unity.Billboard::TargetTransform
	Transform_t3600365921 * ___TargetTransform_3;

public:
	inline static int32_t get_offset_of_PivotAxis_2() { return static_cast<int32_t>(offsetof(Billboard_t2829830782, ___PivotAxis_2)); }
	inline int32_t get_PivotAxis_2() const { return ___PivotAxis_2; }
	inline int32_t* get_address_of_PivotAxis_2() { return &___PivotAxis_2; }
	inline void set_PivotAxis_2(int32_t value)
	{
		___PivotAxis_2 = value;
	}

	inline static int32_t get_offset_of_TargetTransform_3() { return static_cast<int32_t>(offsetof(Billboard_t2829830782, ___TargetTransform_3)); }
	inline Transform_t3600365921 * get_TargetTransform_3() const { return ___TargetTransform_3; }
	inline Transform_t3600365921 ** get_address_of_TargetTransform_3() { return &___TargetTransform_3; }
	inline void set_TargetTransform_3(Transform_t3600365921 * value)
	{
		___TargetTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___TargetTransform_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BILLBOARD_T2829830782_H
#ifndef HEADSUPDIRECTIONINDICATOR_T483009371_H
#define HEADSUPDIRECTIONINDICATOR_T483009371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.HeadsUpDirectionIndicator
struct  HeadsUpDirectionIndicator_t483009371  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject HoloToolkit.Unity.HeadsUpDirectionIndicator::TargetObject
	GameObject_t1113636619 * ___TargetObject_2;
	// System.Single HoloToolkit.Unity.HeadsUpDirectionIndicator::Depth
	float ___Depth_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.HeadsUpDirectionIndicator::Pivot
	Vector3_t3722313464  ___Pivot_4;
	// UnityEngine.GameObject HoloToolkit.Unity.HeadsUpDirectionIndicator::PointerPrefab
	GameObject_t1113636619 * ___PointerPrefab_5;
	// System.Single HoloToolkit.Unity.HeadsUpDirectionIndicator::IndicatorMarginPercent
	float ___IndicatorMarginPercent_6;
	// System.Boolean HoloToolkit.Unity.HeadsUpDirectionIndicator::DebugDrawPointerOrientationPlanes
	bool ___DebugDrawPointerOrientationPlanes_7;
	// UnityEngine.GameObject HoloToolkit.Unity.HeadsUpDirectionIndicator::pointer
	GameObject_t1113636619 * ___pointer_8;
	// UnityEngine.Plane[] HoloToolkit.Unity.HeadsUpDirectionIndicator::indicatorVolume
	PlaneU5BU5D_t3656189108* ___indicatorVolume_15;

public:
	inline static int32_t get_offset_of_TargetObject_2() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_t483009371, ___TargetObject_2)); }
	inline GameObject_t1113636619 * get_TargetObject_2() const { return ___TargetObject_2; }
	inline GameObject_t1113636619 ** get_address_of_TargetObject_2() { return &___TargetObject_2; }
	inline void set_TargetObject_2(GameObject_t1113636619 * value)
	{
		___TargetObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetObject_2), value);
	}

	inline static int32_t get_offset_of_Depth_3() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_t483009371, ___Depth_3)); }
	inline float get_Depth_3() const { return ___Depth_3; }
	inline float* get_address_of_Depth_3() { return &___Depth_3; }
	inline void set_Depth_3(float value)
	{
		___Depth_3 = value;
	}

	inline static int32_t get_offset_of_Pivot_4() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_t483009371, ___Pivot_4)); }
	inline Vector3_t3722313464  get_Pivot_4() const { return ___Pivot_4; }
	inline Vector3_t3722313464 * get_address_of_Pivot_4() { return &___Pivot_4; }
	inline void set_Pivot_4(Vector3_t3722313464  value)
	{
		___Pivot_4 = value;
	}

	inline static int32_t get_offset_of_PointerPrefab_5() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_t483009371, ___PointerPrefab_5)); }
	inline GameObject_t1113636619 * get_PointerPrefab_5() const { return ___PointerPrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_PointerPrefab_5() { return &___PointerPrefab_5; }
	inline void set_PointerPrefab_5(GameObject_t1113636619 * value)
	{
		___PointerPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___PointerPrefab_5), value);
	}

	inline static int32_t get_offset_of_IndicatorMarginPercent_6() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_t483009371, ___IndicatorMarginPercent_6)); }
	inline float get_IndicatorMarginPercent_6() const { return ___IndicatorMarginPercent_6; }
	inline float* get_address_of_IndicatorMarginPercent_6() { return &___IndicatorMarginPercent_6; }
	inline void set_IndicatorMarginPercent_6(float value)
	{
		___IndicatorMarginPercent_6 = value;
	}

	inline static int32_t get_offset_of_DebugDrawPointerOrientationPlanes_7() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_t483009371, ___DebugDrawPointerOrientationPlanes_7)); }
	inline bool get_DebugDrawPointerOrientationPlanes_7() const { return ___DebugDrawPointerOrientationPlanes_7; }
	inline bool* get_address_of_DebugDrawPointerOrientationPlanes_7() { return &___DebugDrawPointerOrientationPlanes_7; }
	inline void set_DebugDrawPointerOrientationPlanes_7(bool value)
	{
		___DebugDrawPointerOrientationPlanes_7 = value;
	}

	inline static int32_t get_offset_of_pointer_8() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_t483009371, ___pointer_8)); }
	inline GameObject_t1113636619 * get_pointer_8() const { return ___pointer_8; }
	inline GameObject_t1113636619 ** get_address_of_pointer_8() { return &___pointer_8; }
	inline void set_pointer_8(GameObject_t1113636619 * value)
	{
		___pointer_8 = value;
		Il2CppCodeGenWriteBarrier((&___pointer_8), value);
	}

	inline static int32_t get_offset_of_indicatorVolume_15() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_t483009371, ___indicatorVolume_15)); }
	inline PlaneU5BU5D_t3656189108* get_indicatorVolume_15() const { return ___indicatorVolume_15; }
	inline PlaneU5BU5D_t3656189108** get_address_of_indicatorVolume_15() { return &___indicatorVolume_15; }
	inline void set_indicatorVolume_15(PlaneU5BU5D_t3656189108* value)
	{
		___indicatorVolume_15 = value;
		Il2CppCodeGenWriteBarrier((&___indicatorVolume_15), value);
	}
};

struct HeadsUpDirectionIndicator_t483009371_StaticFields
{
public:
	// System.Int32 HoloToolkit.Unity.HeadsUpDirectionIndicator::frustumLastUpdated
	int32_t ___frustumLastUpdated_9;
	// UnityEngine.Plane[] HoloToolkit.Unity.HeadsUpDirectionIndicator::frustumPlanes
	PlaneU5BU5D_t3656189108* ___frustumPlanes_10;
	// UnityEngine.Vector3 HoloToolkit.Unity.HeadsUpDirectionIndicator::cameraForward
	Vector3_t3722313464  ___cameraForward_11;
	// UnityEngine.Vector3 HoloToolkit.Unity.HeadsUpDirectionIndicator::cameraPosition
	Vector3_t3722313464  ___cameraPosition_12;
	// UnityEngine.Vector3 HoloToolkit.Unity.HeadsUpDirectionIndicator::cameraRight
	Vector3_t3722313464  ___cameraRight_13;
	// UnityEngine.Vector3 HoloToolkit.Unity.HeadsUpDirectionIndicator::cameraUp
	Vector3_t3722313464  ___cameraUp_14;

public:
	inline static int32_t get_offset_of_frustumLastUpdated_9() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_t483009371_StaticFields, ___frustumLastUpdated_9)); }
	inline int32_t get_frustumLastUpdated_9() const { return ___frustumLastUpdated_9; }
	inline int32_t* get_address_of_frustumLastUpdated_9() { return &___frustumLastUpdated_9; }
	inline void set_frustumLastUpdated_9(int32_t value)
	{
		___frustumLastUpdated_9 = value;
	}

	inline static int32_t get_offset_of_frustumPlanes_10() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_t483009371_StaticFields, ___frustumPlanes_10)); }
	inline PlaneU5BU5D_t3656189108* get_frustumPlanes_10() const { return ___frustumPlanes_10; }
	inline PlaneU5BU5D_t3656189108** get_address_of_frustumPlanes_10() { return &___frustumPlanes_10; }
	inline void set_frustumPlanes_10(PlaneU5BU5D_t3656189108* value)
	{
		___frustumPlanes_10 = value;
		Il2CppCodeGenWriteBarrier((&___frustumPlanes_10), value);
	}

	inline static int32_t get_offset_of_cameraForward_11() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_t483009371_StaticFields, ___cameraForward_11)); }
	inline Vector3_t3722313464  get_cameraForward_11() const { return ___cameraForward_11; }
	inline Vector3_t3722313464 * get_address_of_cameraForward_11() { return &___cameraForward_11; }
	inline void set_cameraForward_11(Vector3_t3722313464  value)
	{
		___cameraForward_11 = value;
	}

	inline static int32_t get_offset_of_cameraPosition_12() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_t483009371_StaticFields, ___cameraPosition_12)); }
	inline Vector3_t3722313464  get_cameraPosition_12() const { return ___cameraPosition_12; }
	inline Vector3_t3722313464 * get_address_of_cameraPosition_12() { return &___cameraPosition_12; }
	inline void set_cameraPosition_12(Vector3_t3722313464  value)
	{
		___cameraPosition_12 = value;
	}

	inline static int32_t get_offset_of_cameraRight_13() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_t483009371_StaticFields, ___cameraRight_13)); }
	inline Vector3_t3722313464  get_cameraRight_13() const { return ___cameraRight_13; }
	inline Vector3_t3722313464 * get_address_of_cameraRight_13() { return &___cameraRight_13; }
	inline void set_cameraRight_13(Vector3_t3722313464  value)
	{
		___cameraRight_13 = value;
	}

	inline static int32_t get_offset_of_cameraUp_14() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_t483009371_StaticFields, ___cameraUp_14)); }
	inline Vector3_t3722313464  get_cameraUp_14() const { return ___cameraUp_14; }
	inline Vector3_t3722313464 * get_address_of_cameraUp_14() { return &___cameraUp_14; }
	inline void set_cameraUp_14(Vector3_t3722313464  value)
	{
		___cameraUp_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADSUPDIRECTIONINDICATOR_T483009371_H
#ifndef HEADSETADJUSTMENT_T10718678_H
#define HEADSETADJUSTMENT_T10718678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.HeadsetAdjustment
struct  HeadsetAdjustment_t10718678  : public MonoBehaviour_t3962482529
{
public:
	// System.String HoloToolkit.Unity.HeadsetAdjustment::NextSceneName
	String_t* ___NextSceneName_2;

public:
	inline static int32_t get_offset_of_NextSceneName_2() { return static_cast<int32_t>(offsetof(HeadsetAdjustment_t10718678, ___NextSceneName_2)); }
	inline String_t* get_NextSceneName_2() const { return ___NextSceneName_2; }
	inline String_t** get_address_of_NextSceneName_2() { return &___NextSceneName_2; }
	inline void set_NextSceneName_2(String_t* value)
	{
		___NextSceneName_2 = value;
		Il2CppCodeGenWriteBarrier((&___NextSceneName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADSETADJUSTMENT_T10718678_H
#ifndef ADAPTIVEQUALITY_T1304437679_H
#define ADAPTIVEQUALITY_T1304437679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AdaptiveQuality
struct  AdaptiveQuality_t1304437679  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HoloToolkit.Unity.AdaptiveQuality::MinFrameTimeThreshold
	float ___MinFrameTimeThreshold_2;
	// System.Single HoloToolkit.Unity.AdaptiveQuality::MaxFrameTimeThreshold
	float ___MaxFrameTimeThreshold_3;
	// System.Int32 HoloToolkit.Unity.AdaptiveQuality::MinQualityLevel
	int32_t ___MinQualityLevel_4;
	// System.Int32 HoloToolkit.Unity.AdaptiveQuality::MaxQualityLevel
	int32_t ___MaxQualityLevel_5;
	// System.Int32 HoloToolkit.Unity.AdaptiveQuality::StartQualityLevel
	int32_t ___StartQualityLevel_6;
	// HoloToolkit.Unity.AdaptiveQuality/QualityChangedEvent HoloToolkit.Unity.AdaptiveQuality::QualityChanged
	QualityChangedEvent_t3536656938 * ___QualityChanged_7;
	// System.Int32 HoloToolkit.Unity.AdaptiveQuality::<QualityLevel>k__BackingField
	int32_t ___U3CQualityLevelU3Ek__BackingField_8;
	// System.Int32 HoloToolkit.Unity.AdaptiveQuality::<RefreshRate>k__BackingField
	int32_t ___U3CRefreshRateU3Ek__BackingField_9;
	// System.Single HoloToolkit.Unity.AdaptiveQuality::frameTimeQuota
	float ___frameTimeQuota_10;
	// System.Collections.Generic.Queue`1<System.Single> HoloToolkit.Unity.AdaptiveQuality::lastFrames
	Queue_1_t1243526268 * ___lastFrames_12;
	// System.Int32 HoloToolkit.Unity.AdaptiveQuality::frameCountSinceLastLevelUpdate
	int32_t ___frameCountSinceLastLevelUpdate_14;
	// UnityEngine.Camera HoloToolkit.Unity.AdaptiveQuality::adaptiveCamera
	Camera_t4157153871 * ___adaptiveCamera_15;

public:
	inline static int32_t get_offset_of_MinFrameTimeThreshold_2() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t1304437679, ___MinFrameTimeThreshold_2)); }
	inline float get_MinFrameTimeThreshold_2() const { return ___MinFrameTimeThreshold_2; }
	inline float* get_address_of_MinFrameTimeThreshold_2() { return &___MinFrameTimeThreshold_2; }
	inline void set_MinFrameTimeThreshold_2(float value)
	{
		___MinFrameTimeThreshold_2 = value;
	}

	inline static int32_t get_offset_of_MaxFrameTimeThreshold_3() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t1304437679, ___MaxFrameTimeThreshold_3)); }
	inline float get_MaxFrameTimeThreshold_3() const { return ___MaxFrameTimeThreshold_3; }
	inline float* get_address_of_MaxFrameTimeThreshold_3() { return &___MaxFrameTimeThreshold_3; }
	inline void set_MaxFrameTimeThreshold_3(float value)
	{
		___MaxFrameTimeThreshold_3 = value;
	}

	inline static int32_t get_offset_of_MinQualityLevel_4() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t1304437679, ___MinQualityLevel_4)); }
	inline int32_t get_MinQualityLevel_4() const { return ___MinQualityLevel_4; }
	inline int32_t* get_address_of_MinQualityLevel_4() { return &___MinQualityLevel_4; }
	inline void set_MinQualityLevel_4(int32_t value)
	{
		___MinQualityLevel_4 = value;
	}

	inline static int32_t get_offset_of_MaxQualityLevel_5() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t1304437679, ___MaxQualityLevel_5)); }
	inline int32_t get_MaxQualityLevel_5() const { return ___MaxQualityLevel_5; }
	inline int32_t* get_address_of_MaxQualityLevel_5() { return &___MaxQualityLevel_5; }
	inline void set_MaxQualityLevel_5(int32_t value)
	{
		___MaxQualityLevel_5 = value;
	}

	inline static int32_t get_offset_of_StartQualityLevel_6() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t1304437679, ___StartQualityLevel_6)); }
	inline int32_t get_StartQualityLevel_6() const { return ___StartQualityLevel_6; }
	inline int32_t* get_address_of_StartQualityLevel_6() { return &___StartQualityLevel_6; }
	inline void set_StartQualityLevel_6(int32_t value)
	{
		___StartQualityLevel_6 = value;
	}

	inline static int32_t get_offset_of_QualityChanged_7() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t1304437679, ___QualityChanged_7)); }
	inline QualityChangedEvent_t3536656938 * get_QualityChanged_7() const { return ___QualityChanged_7; }
	inline QualityChangedEvent_t3536656938 ** get_address_of_QualityChanged_7() { return &___QualityChanged_7; }
	inline void set_QualityChanged_7(QualityChangedEvent_t3536656938 * value)
	{
		___QualityChanged_7 = value;
		Il2CppCodeGenWriteBarrier((&___QualityChanged_7), value);
	}

	inline static int32_t get_offset_of_U3CQualityLevelU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t1304437679, ___U3CQualityLevelU3Ek__BackingField_8)); }
	inline int32_t get_U3CQualityLevelU3Ek__BackingField_8() const { return ___U3CQualityLevelU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CQualityLevelU3Ek__BackingField_8() { return &___U3CQualityLevelU3Ek__BackingField_8; }
	inline void set_U3CQualityLevelU3Ek__BackingField_8(int32_t value)
	{
		___U3CQualityLevelU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CRefreshRateU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t1304437679, ___U3CRefreshRateU3Ek__BackingField_9)); }
	inline int32_t get_U3CRefreshRateU3Ek__BackingField_9() const { return ___U3CRefreshRateU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CRefreshRateU3Ek__BackingField_9() { return &___U3CRefreshRateU3Ek__BackingField_9; }
	inline void set_U3CRefreshRateU3Ek__BackingField_9(int32_t value)
	{
		___U3CRefreshRateU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_frameTimeQuota_10() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t1304437679, ___frameTimeQuota_10)); }
	inline float get_frameTimeQuota_10() const { return ___frameTimeQuota_10; }
	inline float* get_address_of_frameTimeQuota_10() { return &___frameTimeQuota_10; }
	inline void set_frameTimeQuota_10(float value)
	{
		___frameTimeQuota_10 = value;
	}

	inline static int32_t get_offset_of_lastFrames_12() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t1304437679, ___lastFrames_12)); }
	inline Queue_1_t1243526268 * get_lastFrames_12() const { return ___lastFrames_12; }
	inline Queue_1_t1243526268 ** get_address_of_lastFrames_12() { return &___lastFrames_12; }
	inline void set_lastFrames_12(Queue_1_t1243526268 * value)
	{
		___lastFrames_12 = value;
		Il2CppCodeGenWriteBarrier((&___lastFrames_12), value);
	}

	inline static int32_t get_offset_of_frameCountSinceLastLevelUpdate_14() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t1304437679, ___frameCountSinceLastLevelUpdate_14)); }
	inline int32_t get_frameCountSinceLastLevelUpdate_14() const { return ___frameCountSinceLastLevelUpdate_14; }
	inline int32_t* get_address_of_frameCountSinceLastLevelUpdate_14() { return &___frameCountSinceLastLevelUpdate_14; }
	inline void set_frameCountSinceLastLevelUpdate_14(int32_t value)
	{
		___frameCountSinceLastLevelUpdate_14 = value;
	}

	inline static int32_t get_offset_of_adaptiveCamera_15() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t1304437679, ___adaptiveCamera_15)); }
	inline Camera_t4157153871 * get_adaptiveCamera_15() const { return ___adaptiveCamera_15; }
	inline Camera_t4157153871 ** get_address_of_adaptiveCamera_15() { return &___adaptiveCamera_15; }
	inline void set_adaptiveCamera_15(Camera_t4157153871 * value)
	{
		___adaptiveCamera_15 = value;
		Il2CppCodeGenWriteBarrier((&___adaptiveCamera_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADAPTIVEQUALITY_T1304437679_H
#ifndef FADEMANAGER_T1688783044_H
#define FADEMANAGER_T1688783044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.FadeManager
struct  FadeManager_t1688783044  : public Singleton_1_t2089007498
{
public:
	// System.Boolean HoloToolkit.Unity.FadeManager::FadeSharedMaterial
	bool ___FadeSharedMaterial_4;
	// UnityEngine.Material HoloToolkit.Unity.FadeManager::fadeMaterial
	Material_t340375123 * ___fadeMaterial_5;
	// UnityEngine.Color HoloToolkit.Unity.FadeManager::fadeColor
	Color_t2555686324  ___fadeColor_6;
	// HoloToolkit.Unity.FadeManager/FadeState HoloToolkit.Unity.FadeManager::currentState
	int32_t ___currentState_7;
	// System.Single HoloToolkit.Unity.FadeManager::startTime
	float ___startTime_8;
	// System.Single HoloToolkit.Unity.FadeManager::fadeOutTime
	float ___fadeOutTime_9;
	// System.Action HoloToolkit.Unity.FadeManager::fadeOutAction
	Action_t1264377477 * ___fadeOutAction_10;
	// System.Single HoloToolkit.Unity.FadeManager::fadeInTime
	float ___fadeInTime_11;
	// System.Action HoloToolkit.Unity.FadeManager::fadeInAction
	Action_t1264377477 * ___fadeInAction_12;

public:
	inline static int32_t get_offset_of_FadeSharedMaterial_4() { return static_cast<int32_t>(offsetof(FadeManager_t1688783044, ___FadeSharedMaterial_4)); }
	inline bool get_FadeSharedMaterial_4() const { return ___FadeSharedMaterial_4; }
	inline bool* get_address_of_FadeSharedMaterial_4() { return &___FadeSharedMaterial_4; }
	inline void set_FadeSharedMaterial_4(bool value)
	{
		___FadeSharedMaterial_4 = value;
	}

	inline static int32_t get_offset_of_fadeMaterial_5() { return static_cast<int32_t>(offsetof(FadeManager_t1688783044, ___fadeMaterial_5)); }
	inline Material_t340375123 * get_fadeMaterial_5() const { return ___fadeMaterial_5; }
	inline Material_t340375123 ** get_address_of_fadeMaterial_5() { return &___fadeMaterial_5; }
	inline void set_fadeMaterial_5(Material_t340375123 * value)
	{
		___fadeMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___fadeMaterial_5), value);
	}

	inline static int32_t get_offset_of_fadeColor_6() { return static_cast<int32_t>(offsetof(FadeManager_t1688783044, ___fadeColor_6)); }
	inline Color_t2555686324  get_fadeColor_6() const { return ___fadeColor_6; }
	inline Color_t2555686324 * get_address_of_fadeColor_6() { return &___fadeColor_6; }
	inline void set_fadeColor_6(Color_t2555686324  value)
	{
		___fadeColor_6 = value;
	}

	inline static int32_t get_offset_of_currentState_7() { return static_cast<int32_t>(offsetof(FadeManager_t1688783044, ___currentState_7)); }
	inline int32_t get_currentState_7() const { return ___currentState_7; }
	inline int32_t* get_address_of_currentState_7() { return &___currentState_7; }
	inline void set_currentState_7(int32_t value)
	{
		___currentState_7 = value;
	}

	inline static int32_t get_offset_of_startTime_8() { return static_cast<int32_t>(offsetof(FadeManager_t1688783044, ___startTime_8)); }
	inline float get_startTime_8() const { return ___startTime_8; }
	inline float* get_address_of_startTime_8() { return &___startTime_8; }
	inline void set_startTime_8(float value)
	{
		___startTime_8 = value;
	}

	inline static int32_t get_offset_of_fadeOutTime_9() { return static_cast<int32_t>(offsetof(FadeManager_t1688783044, ___fadeOutTime_9)); }
	inline float get_fadeOutTime_9() const { return ___fadeOutTime_9; }
	inline float* get_address_of_fadeOutTime_9() { return &___fadeOutTime_9; }
	inline void set_fadeOutTime_9(float value)
	{
		___fadeOutTime_9 = value;
	}

	inline static int32_t get_offset_of_fadeOutAction_10() { return static_cast<int32_t>(offsetof(FadeManager_t1688783044, ___fadeOutAction_10)); }
	inline Action_t1264377477 * get_fadeOutAction_10() const { return ___fadeOutAction_10; }
	inline Action_t1264377477 ** get_address_of_fadeOutAction_10() { return &___fadeOutAction_10; }
	inline void set_fadeOutAction_10(Action_t1264377477 * value)
	{
		___fadeOutAction_10 = value;
		Il2CppCodeGenWriteBarrier((&___fadeOutAction_10), value);
	}

	inline static int32_t get_offset_of_fadeInTime_11() { return static_cast<int32_t>(offsetof(FadeManager_t1688783044, ___fadeInTime_11)); }
	inline float get_fadeInTime_11() const { return ___fadeInTime_11; }
	inline float* get_address_of_fadeInTime_11() { return &___fadeInTime_11; }
	inline void set_fadeInTime_11(float value)
	{
		___fadeInTime_11 = value;
	}

	inline static int32_t get_offset_of_fadeInAction_12() { return static_cast<int32_t>(offsetof(FadeManager_t1688783044, ___fadeInAction_12)); }
	inline Action_t1264377477 * get_fadeInAction_12() const { return ___fadeInAction_12; }
	inline Action_t1264377477 ** get_address_of_fadeInAction_12() { return &___fadeInAction_12; }
	inline void set_fadeInAction_12(Action_t1264377477 * value)
	{
		___fadeInAction_12 = value;
		Il2CppCodeGenWriteBarrier((&___fadeInAction_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEMANAGER_T1688783044_H
#ifndef SOLVERBODYLOCK_T3340336644_H
#define SOLVERBODYLOCK_T3340336644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverBodyLock
struct  SolverBodyLock_t3340336644  : public Solver_t4167981057
{
public:
	// HoloToolkit.Unity.SolverBodyLock/OrientationReference HoloToolkit.Unity.SolverBodyLock::Orientation
	int32_t ___Orientation_14;
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverBodyLock::offset
	Vector3_t3722313464  ___offset_15;
	// System.Boolean HoloToolkit.Unity.SolverBodyLock::RotationTether
	bool ___RotationTether_16;
	// System.Int32 HoloToolkit.Unity.SolverBodyLock::TetherAngleSteps
	int32_t ___TetherAngleSteps_17;

public:
	inline static int32_t get_offset_of_Orientation_14() { return static_cast<int32_t>(offsetof(SolverBodyLock_t3340336644, ___Orientation_14)); }
	inline int32_t get_Orientation_14() const { return ___Orientation_14; }
	inline int32_t* get_address_of_Orientation_14() { return &___Orientation_14; }
	inline void set_Orientation_14(int32_t value)
	{
		___Orientation_14 = value;
	}

	inline static int32_t get_offset_of_offset_15() { return static_cast<int32_t>(offsetof(SolverBodyLock_t3340336644, ___offset_15)); }
	inline Vector3_t3722313464  get_offset_15() const { return ___offset_15; }
	inline Vector3_t3722313464 * get_address_of_offset_15() { return &___offset_15; }
	inline void set_offset_15(Vector3_t3722313464  value)
	{
		___offset_15 = value;
	}

	inline static int32_t get_offset_of_RotationTether_16() { return static_cast<int32_t>(offsetof(SolverBodyLock_t3340336644, ___RotationTether_16)); }
	inline bool get_RotationTether_16() const { return ___RotationTether_16; }
	inline bool* get_address_of_RotationTether_16() { return &___RotationTether_16; }
	inline void set_RotationTether_16(bool value)
	{
		___RotationTether_16 = value;
	}

	inline static int32_t get_offset_of_TetherAngleSteps_17() { return static_cast<int32_t>(offsetof(SolverBodyLock_t3340336644, ___TetherAngleSteps_17)); }
	inline int32_t get_TetherAngleSteps_17() const { return ___TetherAngleSteps_17; }
	inline int32_t* get_address_of_TetherAngleSteps_17() { return &___TetherAngleSteps_17; }
	inline void set_TetherAngleSteps_17(int32_t value)
	{
		___TetherAngleSteps_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVERBODYLOCK_T3340336644_H
#ifndef SCENELAUNCHER_T1552664439_H
#define SCENELAUNCHER_T1552664439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SceneLauncher
struct  SceneLauncher_t1552664439  : public Singleton_1_t1952888893
{
public:
	// HoloToolkit.Unity.SceneLauncher/SceneMapping[] HoloToolkit.Unity.SceneLauncher::sceneMapping
	SceneMappingU5BU5D_t1612672394* ___sceneMapping_4;
	// UnityEngine.GameObject HoloToolkit.Unity.SceneLauncher::ButtonSpawnLocation
	GameObject_t1113636619 * ___ButtonSpawnLocation_5;
	// HoloToolkit.Unity.SceneLauncherButton HoloToolkit.Unity.SceneLauncher::SceneButtonPrefab
	SceneLauncherButton_t1133275438 * ___SceneButtonPrefab_6;
	// System.Int32 HoloToolkit.Unity.SceneLauncher::MaxRows
	int32_t ___MaxRows_7;
	// System.Int32 HoloToolkit.Unity.SceneLauncher::<SceneLauncherBuildIndex>k__BackingField
	int32_t ___U3CSceneLauncherBuildIndexU3Ek__BackingField_8;
	// UnityEngine.Vector3 HoloToolkit.Unity.SceneLauncher::sceneButtonSize
	Vector3_t3722313464  ___sceneButtonSize_9;

public:
	inline static int32_t get_offset_of_sceneMapping_4() { return static_cast<int32_t>(offsetof(SceneLauncher_t1552664439, ___sceneMapping_4)); }
	inline SceneMappingU5BU5D_t1612672394* get_sceneMapping_4() const { return ___sceneMapping_4; }
	inline SceneMappingU5BU5D_t1612672394** get_address_of_sceneMapping_4() { return &___sceneMapping_4; }
	inline void set_sceneMapping_4(SceneMappingU5BU5D_t1612672394* value)
	{
		___sceneMapping_4 = value;
		Il2CppCodeGenWriteBarrier((&___sceneMapping_4), value);
	}

	inline static int32_t get_offset_of_ButtonSpawnLocation_5() { return static_cast<int32_t>(offsetof(SceneLauncher_t1552664439, ___ButtonSpawnLocation_5)); }
	inline GameObject_t1113636619 * get_ButtonSpawnLocation_5() const { return ___ButtonSpawnLocation_5; }
	inline GameObject_t1113636619 ** get_address_of_ButtonSpawnLocation_5() { return &___ButtonSpawnLocation_5; }
	inline void set_ButtonSpawnLocation_5(GameObject_t1113636619 * value)
	{
		___ButtonSpawnLocation_5 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonSpawnLocation_5), value);
	}

	inline static int32_t get_offset_of_SceneButtonPrefab_6() { return static_cast<int32_t>(offsetof(SceneLauncher_t1552664439, ___SceneButtonPrefab_6)); }
	inline SceneLauncherButton_t1133275438 * get_SceneButtonPrefab_6() const { return ___SceneButtonPrefab_6; }
	inline SceneLauncherButton_t1133275438 ** get_address_of_SceneButtonPrefab_6() { return &___SceneButtonPrefab_6; }
	inline void set_SceneButtonPrefab_6(SceneLauncherButton_t1133275438 * value)
	{
		___SceneButtonPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___SceneButtonPrefab_6), value);
	}

	inline static int32_t get_offset_of_MaxRows_7() { return static_cast<int32_t>(offsetof(SceneLauncher_t1552664439, ___MaxRows_7)); }
	inline int32_t get_MaxRows_7() const { return ___MaxRows_7; }
	inline int32_t* get_address_of_MaxRows_7() { return &___MaxRows_7; }
	inline void set_MaxRows_7(int32_t value)
	{
		___MaxRows_7 = value;
	}

	inline static int32_t get_offset_of_U3CSceneLauncherBuildIndexU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(SceneLauncher_t1552664439, ___U3CSceneLauncherBuildIndexU3Ek__BackingField_8)); }
	inline int32_t get_U3CSceneLauncherBuildIndexU3Ek__BackingField_8() const { return ___U3CSceneLauncherBuildIndexU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CSceneLauncherBuildIndexU3Ek__BackingField_8() { return &___U3CSceneLauncherBuildIndexU3Ek__BackingField_8; }
	inline void set_U3CSceneLauncherBuildIndexU3Ek__BackingField_8(int32_t value)
	{
		___U3CSceneLauncherBuildIndexU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_sceneButtonSize_9() { return static_cast<int32_t>(offsetof(SceneLauncher_t1552664439, ___sceneButtonSize_9)); }
	inline Vector3_t3722313464  get_sceneButtonSize_9() const { return ___sceneButtonSize_9; }
	inline Vector3_t3722313464 * get_address_of_sceneButtonSize_9() { return &___sceneButtonSize_9; }
	inline void set_sceneButtonSize_9(Vector3_t3722313464  value)
	{
		___sceneButtonSize_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENELAUNCHER_T1552664439_H
#ifndef SOLVERCONSTANTVIEWSIZE_T3996106604_H
#define SOLVERCONSTANTVIEWSIZE_T3996106604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverConstantViewSize
struct  SolverConstantViewSize_t3996106604  : public Solver_t4167981057
{
public:
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::TargetViewPercentV
	float ___TargetViewPercentV_14;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::MinDistance
	float ___MinDistance_15;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::MaxDistance
	float ___MaxDistance_16;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::MinScale
	float ___MinScale_17;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::MaxScale
	float ___MaxScale_18;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::ScaleBuffer
	float ___ScaleBuffer_19;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::ManualObjectSize
	float ___ManualObjectSize_20;
	// HoloToolkit.Unity.SolverConstantViewSize/ScaleStateEnum HoloToolkit.Unity.SolverConstantViewSize::ScaleState
	int32_t ___ScaleState_21;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::fovScalar
	float ___fovScalar_22;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::objectSize
	float ___objectSize_23;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::objectScalePercent
	float ___objectScalePercent_24;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::objectDistancePercent
	float ___objectDistancePercent_25;

public:
	inline static int32_t get_offset_of_TargetViewPercentV_14() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t3996106604, ___TargetViewPercentV_14)); }
	inline float get_TargetViewPercentV_14() const { return ___TargetViewPercentV_14; }
	inline float* get_address_of_TargetViewPercentV_14() { return &___TargetViewPercentV_14; }
	inline void set_TargetViewPercentV_14(float value)
	{
		___TargetViewPercentV_14 = value;
	}

	inline static int32_t get_offset_of_MinDistance_15() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t3996106604, ___MinDistance_15)); }
	inline float get_MinDistance_15() const { return ___MinDistance_15; }
	inline float* get_address_of_MinDistance_15() { return &___MinDistance_15; }
	inline void set_MinDistance_15(float value)
	{
		___MinDistance_15 = value;
	}

	inline static int32_t get_offset_of_MaxDistance_16() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t3996106604, ___MaxDistance_16)); }
	inline float get_MaxDistance_16() const { return ___MaxDistance_16; }
	inline float* get_address_of_MaxDistance_16() { return &___MaxDistance_16; }
	inline void set_MaxDistance_16(float value)
	{
		___MaxDistance_16 = value;
	}

	inline static int32_t get_offset_of_MinScale_17() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t3996106604, ___MinScale_17)); }
	inline float get_MinScale_17() const { return ___MinScale_17; }
	inline float* get_address_of_MinScale_17() { return &___MinScale_17; }
	inline void set_MinScale_17(float value)
	{
		___MinScale_17 = value;
	}

	inline static int32_t get_offset_of_MaxScale_18() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t3996106604, ___MaxScale_18)); }
	inline float get_MaxScale_18() const { return ___MaxScale_18; }
	inline float* get_address_of_MaxScale_18() { return &___MaxScale_18; }
	inline void set_MaxScale_18(float value)
	{
		___MaxScale_18 = value;
	}

	inline static int32_t get_offset_of_ScaleBuffer_19() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t3996106604, ___ScaleBuffer_19)); }
	inline float get_ScaleBuffer_19() const { return ___ScaleBuffer_19; }
	inline float* get_address_of_ScaleBuffer_19() { return &___ScaleBuffer_19; }
	inline void set_ScaleBuffer_19(float value)
	{
		___ScaleBuffer_19 = value;
	}

	inline static int32_t get_offset_of_ManualObjectSize_20() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t3996106604, ___ManualObjectSize_20)); }
	inline float get_ManualObjectSize_20() const { return ___ManualObjectSize_20; }
	inline float* get_address_of_ManualObjectSize_20() { return &___ManualObjectSize_20; }
	inline void set_ManualObjectSize_20(float value)
	{
		___ManualObjectSize_20 = value;
	}

	inline static int32_t get_offset_of_ScaleState_21() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t3996106604, ___ScaleState_21)); }
	inline int32_t get_ScaleState_21() const { return ___ScaleState_21; }
	inline int32_t* get_address_of_ScaleState_21() { return &___ScaleState_21; }
	inline void set_ScaleState_21(int32_t value)
	{
		___ScaleState_21 = value;
	}

	inline static int32_t get_offset_of_fovScalar_22() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t3996106604, ___fovScalar_22)); }
	inline float get_fovScalar_22() const { return ___fovScalar_22; }
	inline float* get_address_of_fovScalar_22() { return &___fovScalar_22; }
	inline void set_fovScalar_22(float value)
	{
		___fovScalar_22 = value;
	}

	inline static int32_t get_offset_of_objectSize_23() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t3996106604, ___objectSize_23)); }
	inline float get_objectSize_23() const { return ___objectSize_23; }
	inline float* get_address_of_objectSize_23() { return &___objectSize_23; }
	inline void set_objectSize_23(float value)
	{
		___objectSize_23 = value;
	}

	inline static int32_t get_offset_of_objectScalePercent_24() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t3996106604, ___objectScalePercent_24)); }
	inline float get_objectScalePercent_24() const { return ___objectScalePercent_24; }
	inline float* get_address_of_objectScalePercent_24() { return &___objectScalePercent_24; }
	inline void set_objectScalePercent_24(float value)
	{
		___objectScalePercent_24 = value;
	}

	inline static int32_t get_offset_of_objectDistancePercent_25() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t3996106604, ___objectDistancePercent_25)); }
	inline float get_objectDistancePercent_25() const { return ___objectDistancePercent_25; }
	inline float* get_address_of_objectDistancePercent_25() { return &___objectDistancePercent_25; }
	inline void set_objectDistancePercent_25(float value)
	{
		___objectDistancePercent_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVERCONSTANTVIEWSIZE_T3996106604_H
#ifndef DEBUGPANEL_T2851753240_H
#define DEBUGPANEL_T2851753240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DebugPanel
struct  DebugPanel_t2851753240  : public SingleInstance_1_t3322408396
{
public:
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanel::textMesh
	TextMesh_t1536577757 * ___textMesh_3;
	// System.Collections.Generic.Queue`1<System.String> HoloToolkit.Unity.DebugPanel::logMessages
	Queue_1_t1693710183 * ___logMessages_4;
	// System.Collections.Generic.Queue`1<System.String> HoloToolkit.Unity.DebugPanel::nextLogMessages
	Queue_1_t1693710183 * ___nextLogMessages_5;
	// System.Int32 HoloToolkit.Unity.DebugPanel::maxLogMessages
	int32_t ___maxLogMessages_6;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.DebugPanel/GetLogLine> HoloToolkit.Unity.DebugPanel::externalLogs
	List_1_t2468529069 * ___externalLogs_7;

public:
	inline static int32_t get_offset_of_textMesh_3() { return static_cast<int32_t>(offsetof(DebugPanel_t2851753240, ___textMesh_3)); }
	inline TextMesh_t1536577757 * get_textMesh_3() const { return ___textMesh_3; }
	inline TextMesh_t1536577757 ** get_address_of_textMesh_3() { return &___textMesh_3; }
	inline void set_textMesh_3(TextMesh_t1536577757 * value)
	{
		___textMesh_3 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_3), value);
	}

	inline static int32_t get_offset_of_logMessages_4() { return static_cast<int32_t>(offsetof(DebugPanel_t2851753240, ___logMessages_4)); }
	inline Queue_1_t1693710183 * get_logMessages_4() const { return ___logMessages_4; }
	inline Queue_1_t1693710183 ** get_address_of_logMessages_4() { return &___logMessages_4; }
	inline void set_logMessages_4(Queue_1_t1693710183 * value)
	{
		___logMessages_4 = value;
		Il2CppCodeGenWriteBarrier((&___logMessages_4), value);
	}

	inline static int32_t get_offset_of_nextLogMessages_5() { return static_cast<int32_t>(offsetof(DebugPanel_t2851753240, ___nextLogMessages_5)); }
	inline Queue_1_t1693710183 * get_nextLogMessages_5() const { return ___nextLogMessages_5; }
	inline Queue_1_t1693710183 ** get_address_of_nextLogMessages_5() { return &___nextLogMessages_5; }
	inline void set_nextLogMessages_5(Queue_1_t1693710183 * value)
	{
		___nextLogMessages_5 = value;
		Il2CppCodeGenWriteBarrier((&___nextLogMessages_5), value);
	}

	inline static int32_t get_offset_of_maxLogMessages_6() { return static_cast<int32_t>(offsetof(DebugPanel_t2851753240, ___maxLogMessages_6)); }
	inline int32_t get_maxLogMessages_6() const { return ___maxLogMessages_6; }
	inline int32_t* get_address_of_maxLogMessages_6() { return &___maxLogMessages_6; }
	inline void set_maxLogMessages_6(int32_t value)
	{
		___maxLogMessages_6 = value;
	}

	inline static int32_t get_offset_of_externalLogs_7() { return static_cast<int32_t>(offsetof(DebugPanel_t2851753240, ___externalLogs_7)); }
	inline List_1_t2468529069 * get_externalLogs_7() const { return ___externalLogs_7; }
	inline List_1_t2468529069 ** get_address_of_externalLogs_7() { return &___externalLogs_7; }
	inline void set_externalLogs_7(List_1_t2468529069 * value)
	{
		___externalLogs_7 = value;
		Il2CppCodeGenWriteBarrier((&___externalLogs_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGPANEL_T2851753240_H
#ifndef SOLVERHANDLER_T1963548039_H
#define SOLVERHANDLER_T1963548039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverHandler
struct  SolverHandler_t1963548039  : public ControllerFinder_t3151550888
{
public:
	// HoloToolkit.Unity.SolverHandler/TrackedObjectToReferenceEnum HoloToolkit.Unity.SolverHandler::trackedObjectToReference
	int32_t ___trackedObjectToReference_6;
	// UnityEngine.Transform HoloToolkit.Unity.SolverHandler::transformTarget
	Transform_t3600365921 * ___transformTarget_7;
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverHandler::<GoalPosition>k__BackingField
	Vector3_t3722313464  ___U3CGoalPositionU3Ek__BackingField_8;
	// UnityEngine.Quaternion HoloToolkit.Unity.SolverHandler::<GoalRotation>k__BackingField
	Quaternion_t2301928331  ___U3CGoalRotationU3Ek__BackingField_9;
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverHandler::<GoalScale>k__BackingField
	Vector3_t3722313464  ___U3CGoalScaleU3Ek__BackingField_10;
	// HoloToolkit.Unity.SolverHandler/Vector3Smoothed HoloToolkit.Unity.SolverHandler::<AltScale>k__BackingField
	Vector3Smoothed_t107253641  ___U3CAltScaleU3Ek__BackingField_11;
	// System.Single HoloToolkit.Unity.SolverHandler::<DeltaTime>k__BackingField
	float ___U3CDeltaTimeU3Ek__BackingField_12;
	// System.Single HoloToolkit.Unity.SolverHandler::<LastUpdateTime>k__BackingField
	float ___U3CLastUpdateTimeU3Ek__BackingField_13;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.Solver> HoloToolkit.Unity.SolverHandler::m_Solvers
	List_1_t1345088503 * ___m_Solvers_14;

public:
	inline static int32_t get_offset_of_trackedObjectToReference_6() { return static_cast<int32_t>(offsetof(SolverHandler_t1963548039, ___trackedObjectToReference_6)); }
	inline int32_t get_trackedObjectToReference_6() const { return ___trackedObjectToReference_6; }
	inline int32_t* get_address_of_trackedObjectToReference_6() { return &___trackedObjectToReference_6; }
	inline void set_trackedObjectToReference_6(int32_t value)
	{
		___trackedObjectToReference_6 = value;
	}

	inline static int32_t get_offset_of_transformTarget_7() { return static_cast<int32_t>(offsetof(SolverHandler_t1963548039, ___transformTarget_7)); }
	inline Transform_t3600365921 * get_transformTarget_7() const { return ___transformTarget_7; }
	inline Transform_t3600365921 ** get_address_of_transformTarget_7() { return &___transformTarget_7; }
	inline void set_transformTarget_7(Transform_t3600365921 * value)
	{
		___transformTarget_7 = value;
		Il2CppCodeGenWriteBarrier((&___transformTarget_7), value);
	}

	inline static int32_t get_offset_of_U3CGoalPositionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(SolverHandler_t1963548039, ___U3CGoalPositionU3Ek__BackingField_8)); }
	inline Vector3_t3722313464  get_U3CGoalPositionU3Ek__BackingField_8() const { return ___U3CGoalPositionU3Ek__BackingField_8; }
	inline Vector3_t3722313464 * get_address_of_U3CGoalPositionU3Ek__BackingField_8() { return &___U3CGoalPositionU3Ek__BackingField_8; }
	inline void set_U3CGoalPositionU3Ek__BackingField_8(Vector3_t3722313464  value)
	{
		___U3CGoalPositionU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CGoalRotationU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(SolverHandler_t1963548039, ___U3CGoalRotationU3Ek__BackingField_9)); }
	inline Quaternion_t2301928331  get_U3CGoalRotationU3Ek__BackingField_9() const { return ___U3CGoalRotationU3Ek__BackingField_9; }
	inline Quaternion_t2301928331 * get_address_of_U3CGoalRotationU3Ek__BackingField_9() { return &___U3CGoalRotationU3Ek__BackingField_9; }
	inline void set_U3CGoalRotationU3Ek__BackingField_9(Quaternion_t2301928331  value)
	{
		___U3CGoalRotationU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CGoalScaleU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(SolverHandler_t1963548039, ___U3CGoalScaleU3Ek__BackingField_10)); }
	inline Vector3_t3722313464  get_U3CGoalScaleU3Ek__BackingField_10() const { return ___U3CGoalScaleU3Ek__BackingField_10; }
	inline Vector3_t3722313464 * get_address_of_U3CGoalScaleU3Ek__BackingField_10() { return &___U3CGoalScaleU3Ek__BackingField_10; }
	inline void set_U3CGoalScaleU3Ek__BackingField_10(Vector3_t3722313464  value)
	{
		___U3CGoalScaleU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CAltScaleU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(SolverHandler_t1963548039, ___U3CAltScaleU3Ek__BackingField_11)); }
	inline Vector3Smoothed_t107253641  get_U3CAltScaleU3Ek__BackingField_11() const { return ___U3CAltScaleU3Ek__BackingField_11; }
	inline Vector3Smoothed_t107253641 * get_address_of_U3CAltScaleU3Ek__BackingField_11() { return &___U3CAltScaleU3Ek__BackingField_11; }
	inline void set_U3CAltScaleU3Ek__BackingField_11(Vector3Smoothed_t107253641  value)
	{
		___U3CAltScaleU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CDeltaTimeU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(SolverHandler_t1963548039, ___U3CDeltaTimeU3Ek__BackingField_12)); }
	inline float get_U3CDeltaTimeU3Ek__BackingField_12() const { return ___U3CDeltaTimeU3Ek__BackingField_12; }
	inline float* get_address_of_U3CDeltaTimeU3Ek__BackingField_12() { return &___U3CDeltaTimeU3Ek__BackingField_12; }
	inline void set_U3CDeltaTimeU3Ek__BackingField_12(float value)
	{
		___U3CDeltaTimeU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CLastUpdateTimeU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(SolverHandler_t1963548039, ___U3CLastUpdateTimeU3Ek__BackingField_13)); }
	inline float get_U3CLastUpdateTimeU3Ek__BackingField_13() const { return ___U3CLastUpdateTimeU3Ek__BackingField_13; }
	inline float* get_address_of_U3CLastUpdateTimeU3Ek__BackingField_13() { return &___U3CLastUpdateTimeU3Ek__BackingField_13; }
	inline void set_U3CLastUpdateTimeU3Ek__BackingField_13(float value)
	{
		___U3CLastUpdateTimeU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_m_Solvers_14() { return static_cast<int32_t>(offsetof(SolverHandler_t1963548039, ___m_Solvers_14)); }
	inline List_1_t1345088503 * get_m_Solvers_14() const { return ___m_Solvers_14; }
	inline List_1_t1345088503 ** get_address_of_m_Solvers_14() { return &___m_Solvers_14; }
	inline void set_m_Solvers_14(List_1_t1345088503 * value)
	{
		___m_Solvers_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Solvers_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVERHANDLER_T1963548039_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5600 = { sizeof (ObjectPlacementResult_t4094328837), sizeof(ObjectPlacementResult_t4094328837_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5600[5] = 
{
	ObjectPlacementResult_t4094328837::get_offset_of_Position_0(),
	ObjectPlacementResult_t4094328837::get_offset_of_HalfDims_1(),
	ObjectPlacementResult_t4094328837::get_offset_of_Forward_2(),
	ObjectPlacementResult_t4094328837::get_offset_of_Right_3(),
	ObjectPlacementResult_t4094328837::get_offset_of_Up_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5601 = { sizeof (SpatialUnderstandingDllShapes_t2489144933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5602 = { sizeof (ShapeResult_t4069931198)+ sizeof (RuntimeObject), sizeof(ShapeResult_t4069931198 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5602[2] = 
{
	ShapeResult_t4069931198::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeResult_t4069931198::get_offset_of_halfDims_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5603 = { sizeof (ShapeComponentConstraintType_t3710497406)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5603[37] = 
{
	ShapeComponentConstraintType_t3710497406::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5604 = { sizeof (ShapeComponentConstraint_t4241953735)+ sizeof (RuntimeObject), sizeof(ShapeComponentConstraint_t4241953735 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5604[8] = 
{
	ShapeComponentConstraint_t4241953735::get_offset_of_Type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponentConstraint_t4241953735::get_offset_of_Param_Float_0_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponentConstraint_t4241953735::get_offset_of_Param_Float_1_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponentConstraint_t4241953735::get_offset_of_Param_Float_2_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponentConstraint_t4241953735::get_offset_of_Param_Float_3_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponentConstraint_t4241953735::get_offset_of_Param_Int_0_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponentConstraint_t4241953735::get_offset_of_Param_Int_1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponentConstraint_t4241953735::get_offset_of_Param_Str_0_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5605 = { sizeof (ShapeComponent_t3281486496)+ sizeof (RuntimeObject), sizeof(ShapeComponent_t3281486496 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5605[2] = 
{
	ShapeComponent_t3281486496::get_offset_of_ConstraintCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponent_t3281486496::get_offset_of_Constraints_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5606 = { sizeof (ShapeConstraintType_t1645122175)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5606[11] = 
{
	ShapeConstraintType_t1645122175::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5607 = { sizeof (ShapeConstraint_t2982058205)+ sizeof (RuntimeObject), sizeof(ShapeConstraint_t2982058205 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5607[4] = 
{
	ShapeConstraint_t2982058205::get_offset_of_Type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeConstraint_t2982058205::get_offset_of_Param_Float_0_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeConstraint_t2982058205::get_offset_of_Param_Int_0_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeConstraint_t2982058205::get_offset_of_Param_Int_1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5608 = { sizeof (SpatialUnderstandingDllTopology_t221813560), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5609 = { sizeof (TopologyResult_t1593710726)+ sizeof (RuntimeObject), sizeof(TopologyResult_t1593710726 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5609[4] = 
{
	TopologyResult_t1593710726::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TopologyResult_t1593710726::get_offset_of_normal_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TopologyResult_t1593710726::get_offset_of_width_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TopologyResult_t1593710726::get_offset_of_length_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5610 = { sizeof (SpatialUnderstandingSourceMesh_t2539908667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5610[1] = 
{
	SpatialUnderstandingSourceMesh_t2539908667::get_offset_of_inputMeshList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5611 = { sizeof (AdaptiveQuality_t1304437679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5611[15] = 
{
	AdaptiveQuality_t1304437679::get_offset_of_MinFrameTimeThreshold_2(),
	AdaptiveQuality_t1304437679::get_offset_of_MaxFrameTimeThreshold_3(),
	AdaptiveQuality_t1304437679::get_offset_of_MinQualityLevel_4(),
	AdaptiveQuality_t1304437679::get_offset_of_MaxQualityLevel_5(),
	AdaptiveQuality_t1304437679::get_offset_of_StartQualityLevel_6(),
	AdaptiveQuality_t1304437679::get_offset_of_QualityChanged_7(),
	AdaptiveQuality_t1304437679::get_offset_of_U3CQualityLevelU3Ek__BackingField_8(),
	AdaptiveQuality_t1304437679::get_offset_of_U3CRefreshRateU3Ek__BackingField_9(),
	AdaptiveQuality_t1304437679::get_offset_of_frameTimeQuota_10(),
	0,
	AdaptiveQuality_t1304437679::get_offset_of_lastFrames_12(),
	0,
	AdaptiveQuality_t1304437679::get_offset_of_frameCountSinceLastLevelUpdate_14(),
	AdaptiveQuality_t1304437679::get_offset_of_adaptiveCamera_15(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5612 = { sizeof (QualityChangedEvent_t3536656938), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5613 = { sizeof (AdaptiveViewport_t3912946756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5613[5] = 
{
	AdaptiveViewport_t3912946756::get_offset_of_FullSizeQualityLevel_2(),
	AdaptiveViewport_t3912946756::get_offset_of_MinSizeQualityLevel_3(),
	AdaptiveViewport_t3912946756::get_offset_of_MinViewportSize_4(),
	AdaptiveViewport_t3912946756::get_offset_of_qualityController_5(),
	AdaptiveViewport_t3912946756::get_offset_of_U3CCurrentScaleU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5614 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5615 = { sizeof (ApplicationViewManager_t1982588047), -1, sizeof(ApplicationViewManager_t1982588047_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5615[2] = 
{
	ApplicationViewManager_t1982588047_StaticFields::get_offset_of_U3CFull3DViewIdU3Ek__BackingField_2(),
	ApplicationViewManager_t1982588047_StaticFields::get_offset_of_CallbackDictionary_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5616 = { sizeof (U3CU3Ec_t2509199788), -1, sizeof(U3CU3Ec_t2509199788_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5616[2] = 
{
	U3CU3Ec_t2509199788_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2509199788_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5617 = { sizeof (U3CCallbackReturnValueU3Ed__6_t3485498644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5617[7] = 
{
	U3CCallbackReturnValueU3Ed__6_t3485498644::get_offset_of_U3CU3E1__state_0(),
	U3CCallbackReturnValueU3Ed__6_t3485498644::get_offset_of_U3CU3Et__builder_1(),
	U3CCallbackReturnValueU3Ed__6_t3485498644::get_offset_of_returnValue_2(),
	U3CCallbackReturnValueU3Ed__6_t3485498644::get_offset_of_U3CviewIdU3E5__1_3(),
	U3CCallbackReturnValueU3Ed__6_t3485498644::get_offset_of_U3CviewU3E5__2_4(),
	U3CCallbackReturnValueU3Ed__6_t3485498644::get_offset_of_U3CcbU3E5__3_5(),
	U3CCallbackReturnValueU3Ed__6_t3485498644::get_offset_of_U3CU3Eu__1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5618 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5618[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5619 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5619[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5620 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5620[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5621 = { sizeof (AtlasPrefabReference_t289331666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5621[2] = 
{
	AtlasPrefabReference_t289331666::get_offset_of_Prefabs_2(),
	AtlasPrefabReference_t289331666::get_offset_of_Atlas_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5622 = { sizeof (AnimationCurveDefaultAttribute_t1761746171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5622[3] = 
{
	AnimationCurveDefaultAttribute_t1761746171::get_offset_of_U3CPostWrapU3Ek__BackingField_0(),
	AnimationCurveDefaultAttribute_t1761746171::get_offset_of_U3CStartValU3Ek__BackingField_1(),
	AnimationCurveDefaultAttribute_t1761746171::get_offset_of_U3CEndValU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5623 = { sizeof (DocLinkAttribute_t684588366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5623[2] = 
{
	DocLinkAttribute_t684588366::get_offset_of_U3CDocURLU3Ek__BackingField_0(),
	DocLinkAttribute_t684588366::get_offset_of_U3CDescriptionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5624 = { sizeof (DrawLastAttribute_t330304297), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5625 = { sizeof (DrawOverrideAttribute_t485819796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5626 = { sizeof (DropDownComponentAttribute_t410457799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5626[3] = 
{
	DropDownComponentAttribute_t410457799::get_offset_of_U3CAutoFillU3Ek__BackingField_0(),
	DropDownComponentAttribute_t410457799::get_offset_of_U3CShowComponentNamesU3Ek__BackingField_1(),
	DropDownComponentAttribute_t410457799::get_offset_of_U3CCustomLabelU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5627 = { sizeof (DropDownGameObjectAttribute_t2587315465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5627[1] = 
{
	DropDownGameObjectAttribute_t2587315465::get_offset_of_U3CCustomLabelU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5628 = { sizeof (EditablePropAttribute_t2302503201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5628[1] = 
{
	EditablePropAttribute_t2302503201::get_offset_of_U3CCustomLabelU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5629 = { sizeof (EnumCheckboxAttribute_t50826499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5629[6] = 
{
	EnumCheckboxAttribute_t50826499::get_offset_of_U3CDefaultNameU3Ek__BackingField_0(),
	EnumCheckboxAttribute_t50826499::get_offset_of_U3CDefaultValueU3Ek__BackingField_1(),
	EnumCheckboxAttribute_t50826499::get_offset_of_U3CValueOnZeroU3Ek__BackingField_2(),
	EnumCheckboxAttribute_t50826499::get_offset_of_U3CIgnoreNoneU3Ek__BackingField_3(),
	EnumCheckboxAttribute_t50826499::get_offset_of_U3CIgnoreAllU3Ek__BackingField_4(),
	EnumCheckboxAttribute_t50826499::get_offset_of_U3CCustomLabelU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5630 = { sizeof (EnumFlagsAttribute_t1651967965), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5631 = { sizeof (FeatureInProgressAttribute_t1665528438), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5632 = { sizeof (GradientDefaultAttribute_t165107163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5632[2] = 
{
	GradientDefaultAttribute_t165107163::get_offset_of_startColor_0(),
	GradientDefaultAttribute_t165107163::get_offset_of_endColor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5633 = { sizeof (ColorEnum_t1662983804)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5633[11] = 
{
	ColorEnum_t1662983804::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5634 = { sizeof (HideInMRTKInspector_t2011525198), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5635 = { sizeof (MaterialPropertyAttribute_t2178865252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5635[6] = 
{
	MaterialPropertyAttribute_t2178865252::get_offset_of_U3CPropertyU3Ek__BackingField_0(),
	MaterialPropertyAttribute_t2178865252::get_offset_of_U3CPropertyTypeU3Ek__BackingField_1(),
	MaterialPropertyAttribute_t2178865252::get_offset_of_U3CMaterialMemberNameU3Ek__BackingField_2(),
	MaterialPropertyAttribute_t2178865252::get_offset_of_U3CAllowNoneU3Ek__BackingField_3(),
	MaterialPropertyAttribute_t2178865252::get_offset_of_U3CDefaultPropertyU3Ek__BackingField_4(),
	MaterialPropertyAttribute_t2178865252::get_offset_of_U3CCustomLabelU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5636 = { sizeof (PropertyTypeEnum_t4136261769)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5636[5] = 
{
	PropertyTypeEnum_t4136261769::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5637 = { sizeof (OpenLocalFileAttribute_t4101133295), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5638 = { sizeof (OpenLocalFolderAttribute_t1301565505), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5639 = { sizeof (RangePropAttribute_t1881743734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5639[5] = 
{
	RangePropAttribute_t1881743734::get_offset_of_U3CMinFloatU3Ek__BackingField_0(),
	RangePropAttribute_t1881743734::get_offset_of_U3CMaxFloatU3Ek__BackingField_1(),
	RangePropAttribute_t1881743734::get_offset_of_U3CMinIntU3Ek__BackingField_2(),
	RangePropAttribute_t1881743734::get_offset_of_U3CMaxIntU3Ek__BackingField_3(),
	RangePropAttribute_t1881743734::get_offset_of_U3CTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5640 = { sizeof (TypeEnum_t3953134253)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5640[3] = 
{
	TypeEnum_t3953134253::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5641 = { sizeof (SaveLocalFileAttribute_t3766135238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5642 = { sizeof (SceneComponentAttribute_t771947740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5642[1] = 
{
	SceneComponentAttribute_t771947740::get_offset_of_U3CCustomLabelU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5643 = { sizeof (SceneGameObjectAttribute_t4082581691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5643[1] = 
{
	SceneGameObjectAttribute_t4082581691::get_offset_of_U3CCustomLabelU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5644 = { sizeof (SetIndentAttribute_t768412030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5644[1] = 
{
	SetIndentAttribute_t768412030::get_offset_of_U3CIndentU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5645 = { sizeof (ShowIfAttribute_t4062566328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5645[2] = 
{
	ShowIfAttribute_t4062566328::get_offset_of_U3CMemberNameU3Ek__BackingField_0(),
	ShowIfAttribute_t4062566328::get_offset_of_U3CShowIfConditionMetU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5646 = { sizeof (ShowIfBoolValueAttribute_t2057683491), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5647 = { sizeof (ShowIfEnumValueAttribute_t2926192619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5647[1] = 
{
	ShowIfEnumValueAttribute_t2926192619::get_offset_of_U3CShowValuesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5648 = { sizeof (ShowIfNullAttribute_t3061964185), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5649 = { sizeof (TextAreaProp_t3268894636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5649[1] = 
{
	TextAreaProp_t3268894636::get_offset_of_U3CFontSizeU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5650 = { sizeof (TutorialAttribute_t3869340965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5650[2] = 
{
	TutorialAttribute_t3869340965::get_offset_of_U3CTutorialURLU3Ek__BackingField_0(),
	TutorialAttribute_t3869340965::get_offset_of_U3CDescriptionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5651 = { sizeof (UseWithAttribute_t4112209736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5651[1] = 
{
	UseWithAttribute_t4112209736::get_offset_of_U3CUseWithTypesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5652 = { sizeof (ValidateUnityObjectAttribute_t1018196378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5652[3] = 
{
	ValidateUnityObjectAttribute_t1018196378::get_offset_of_U3CFailActionU3Ek__BackingField_0(),
	ValidateUnityObjectAttribute_t1018196378::get_offset_of_U3CMethodNameU3Ek__BackingField_1(),
	ValidateUnityObjectAttribute_t1018196378::get_offset_of_U3CMessageOnFailU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5653 = { sizeof (ActionEnum_t3058967560)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5653[5] = 
{
	ActionEnum_t3058967560::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5654 = { sizeof (PivotAxis_t1004910028)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5654[3] = 
{
	PivotAxis_t1004910028::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5655 = { sizeof (Billboard_t2829830782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5655[2] = 
{
	Billboard_t2829830782::get_offset_of_PivotAxis_2(),
	Billboard_t2829830782::get_offset_of_TargetTransform_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5656 = { sizeof (BitManipulator_t1741666884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5656[2] = 
{
	BitManipulator_t1741666884::get_offset_of_mask_0(),
	BitManipulator_t1741666884::get_offset_of_shift_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5657 = { sizeof (CanvasHelper_t3155631873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5657[1] = 
{
	CanvasHelper_t3155631873::get_offset_of_Canvas_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5658 = { sizeof (CircularBuffer_t1956967364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5658[5] = 
{
	CircularBuffer_t1956967364::get_offset_of_data_0(),
	CircularBuffer_t1956967364::get_offset_of_writeOffset_1(),
	CircularBuffer_t1956967364::get_offset_of_readOffset_2(),
	CircularBuffer_t1956967364::get_offset_of_readWritePadding_3(),
	CircularBuffer_t1956967364::get_offset_of_allowOverwrite_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5659 = { sizeof (DebugPanel_t2851753240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5659[5] = 
{
	DebugPanel_t2851753240::get_offset_of_textMesh_3(),
	DebugPanel_t2851753240::get_offset_of_logMessages_4(),
	DebugPanel_t2851753240::get_offset_of_nextLogMessages_5(),
	DebugPanel_t2851753240::get_offset_of_maxLogMessages_6(),
	DebugPanel_t2851753240::get_offset_of_externalLogs_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5660 = { sizeof (GetLogLine_t996454327), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5661 = { sizeof (DebugPanelControllerInfo_t974145763), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5661[27] = 
{
	DebugPanelControllerInfo_t974145763::get_offset_of_controllers_2(),
	DebugPanelControllerInfo_t974145763::get_offset_of_LeftInfoTextPointerPosition_3(),
	DebugPanelControllerInfo_t974145763::get_offset_of_LeftInfoTextPointerRotation_4(),
	DebugPanelControllerInfo_t974145763::get_offset_of_LeftInfoTextGripPosition_5(),
	DebugPanelControllerInfo_t974145763::get_offset_of_LeftInfoTextGripRotation_6(),
	DebugPanelControllerInfo_t974145763::get_offset_of_LeftInfoTextGripGrasped_7(),
	DebugPanelControllerInfo_t974145763::get_offset_of_LeftInfoTextMenuPressed_8(),
	DebugPanelControllerInfo_t974145763::get_offset_of_LeftInfoTextTriggerPressed_9(),
	DebugPanelControllerInfo_t974145763::get_offset_of_LeftInfoTextTriggerPressedAmount_10(),
	DebugPanelControllerInfo_t974145763::get_offset_of_LeftInfoTextThumbstickPressed_11(),
	DebugPanelControllerInfo_t974145763::get_offset_of_LeftInfoTextThumbstickPosition_12(),
	DebugPanelControllerInfo_t974145763::get_offset_of_LeftInfoTextTouchpadPressed_13(),
	DebugPanelControllerInfo_t974145763::get_offset_of_LeftInfoTextTouchpadTouched_14(),
	DebugPanelControllerInfo_t974145763::get_offset_of_LeftInfoTextTouchpadPosition_15(),
	DebugPanelControllerInfo_t974145763::get_offset_of_RightInfoTextPointerPosition_16(),
	DebugPanelControllerInfo_t974145763::get_offset_of_RightInfoTextPointerRotation_17(),
	DebugPanelControllerInfo_t974145763::get_offset_of_RightInfoTextGripPosition_18(),
	DebugPanelControllerInfo_t974145763::get_offset_of_RightInfoTextGripRotation_19(),
	DebugPanelControllerInfo_t974145763::get_offset_of_RightInfoTextGripGrasped_20(),
	DebugPanelControllerInfo_t974145763::get_offset_of_RightInfoTextMenuPressed_21(),
	DebugPanelControllerInfo_t974145763::get_offset_of_RightInfoTextTriggerPressed_22(),
	DebugPanelControllerInfo_t974145763::get_offset_of_RightInfoTextTriggerPressedAmount_23(),
	DebugPanelControllerInfo_t974145763::get_offset_of_RightInfoTextThumbstickPressed_24(),
	DebugPanelControllerInfo_t974145763::get_offset_of_RightInfoTextThumbstickPosition_25(),
	DebugPanelControllerInfo_t974145763::get_offset_of_RightInfoTextTouchpadPressed_26(),
	DebugPanelControllerInfo_t974145763::get_offset_of_RightInfoTextTouchpadTouched_27(),
	DebugPanelControllerInfo_t974145763::get_offset_of_RightInfoTextTouchpadPosition_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5662 = { sizeof (ControllerState_t440280647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5662[14] = 
{
	ControllerState_t440280647::get_offset_of_Handedness_0(),
	ControllerState_t440280647::get_offset_of_PointerPosition_1(),
	ControllerState_t440280647::get_offset_of_PointerRotation_2(),
	ControllerState_t440280647::get_offset_of_GripPosition_3(),
	ControllerState_t440280647::get_offset_of_GripRotation_4(),
	ControllerState_t440280647::get_offset_of_Grasped_5(),
	ControllerState_t440280647::get_offset_of_MenuPressed_6(),
	ControllerState_t440280647::get_offset_of_SelectPressed_7(),
	ControllerState_t440280647::get_offset_of_SelectPressedAmount_8(),
	ControllerState_t440280647::get_offset_of_ThumbstickPressed_9(),
	ControllerState_t440280647::get_offset_of_ThumbstickPosition_10(),
	ControllerState_t440280647::get_offset_of_TouchpadPressed_11(),
	ControllerState_t440280647::get_offset_of_TouchpadTouched_12(),
	ControllerState_t440280647::get_offset_of_TouchpadPosition_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5663 = { sizeof (DebugPanelFPSCounter_t4280904583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5663[3] = 
{
	DebugPanelFPSCounter_t4280904583::get_offset_of_frameCount_2(),
	DebugPanelFPSCounter_t4280904583::get_offset_of_framesPerSecond_3(),
	DebugPanelFPSCounter_t4280904583::get_offset_of_lastWholeTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5664 = { sizeof (DirectionIndicator_t2683274306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5664[9] = 
{
	DirectionIndicator_t2683274306::get_offset_of_Cursor_2(),
	DirectionIndicator_t2683274306::get_offset_of_DirectionIndicatorObject_3(),
	DirectionIndicator_t2683274306::get_offset_of_DirectionIndicatorColor_4(),
	DirectionIndicator_t2683274306::get_offset_of_VisibilitySafeFactor_5(),
	DirectionIndicator_t2683274306::get_offset_of_MetersFromCursor_6(),
	DirectionIndicator_t2683274306::get_offset_of_directionIndicatorDefaultRotation_7(),
	DirectionIndicator_t2683274306::get_offset_of_directionIndicatorRenderer_8(),
	DirectionIndicator_t2683274306::get_offset_of_indicatorMaterial_9(),
	DirectionIndicator_t2683274306::get_offset_of_isDirectionIndicatorVisible_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5665 = { sizeof (FadeManager_t1688783044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5665[9] = 
{
	FadeManager_t1688783044::get_offset_of_FadeSharedMaterial_4(),
	FadeManager_t1688783044::get_offset_of_fadeMaterial_5(),
	FadeManager_t1688783044::get_offset_of_fadeColor_6(),
	FadeManager_t1688783044::get_offset_of_currentState_7(),
	FadeManager_t1688783044::get_offset_of_startTime_8(),
	FadeManager_t1688783044::get_offset_of_fadeOutTime_9(),
	FadeManager_t1688783044::get_offset_of_fadeOutAction_10(),
	FadeManager_t1688783044::get_offset_of_fadeInTime_11(),
	FadeManager_t1688783044::get_offset_of_fadeInAction_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5666 = { sizeof (FadeState_t4119448579)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5666[4] = 
{
	FadeState_t4119448579::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5667 = { sizeof (FixedAngularSize_t1485950223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5667[3] = 
{
	FixedAngularSize_t1485950223::get_offset_of_SizeRatio_2(),
	FixedAngularSize_t1485950223::get_offset_of_startingDistance_3(),
	FixedAngularSize_t1485950223::get_offset_of_startingScale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5668 = { sizeof (FpsDisplay_t3517157742), -1, sizeof(FpsDisplay_t3517157742_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5668[7] = 
{
	FpsDisplay_t3517157742::get_offset_of_textMesh_2(),
	FpsDisplay_t3517157742::get_offset_of_uGUIText_3(),
	FpsDisplay_t3517157742::get_offset_of_frameRange_4(),
	FpsDisplay_t3517157742::get_offset_of_averageFps_5(),
	FpsDisplay_t3517157742::get_offset_of_fpsBuffer_6(),
	FpsDisplay_t3517157742::get_offset_of_fpsBufferIndex_7(),
	FpsDisplay_t3517157742_StaticFields::get_offset_of_StringsFrom00To99_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5669 = { sizeof (GpuTiming_t2571224352), -1, sizeof(GpuTiming_t2571224352_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5669[5] = 
{
	0,
	0,
	GpuTiming_t2571224352_StaticFields::get_offset_of_nextAvailableEventId_2(),
	GpuTiming_t2571224352_StaticFields::get_offset_of_currentEventId_3(),
	GpuTiming_t2571224352_StaticFields::get_offset_of_eventIds_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5670 = { sizeof (GpuTimingCamera_t3799301742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5670[2] = 
{
	GpuTimingCamera_t3799301742::get_offset_of_TimingTag_2(),
	GpuTimingCamera_t3799301742::get_offset_of_timingCamera_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5671 = { sizeof (HashCodes_t2825567999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5672 = { sizeof (HeadsetAdjustment_t10718678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5672[1] = 
{
	HeadsetAdjustment_t10718678::get_offset_of_NextSceneName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5673 = { sizeof (HeadsUpDirectionIndicator_t483009371), -1, sizeof(HeadsUpDirectionIndicator_t483009371_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5673[14] = 
{
	HeadsUpDirectionIndicator_t483009371::get_offset_of_TargetObject_2(),
	HeadsUpDirectionIndicator_t483009371::get_offset_of_Depth_3(),
	HeadsUpDirectionIndicator_t483009371::get_offset_of_Pivot_4(),
	HeadsUpDirectionIndicator_t483009371::get_offset_of_PointerPrefab_5(),
	HeadsUpDirectionIndicator_t483009371::get_offset_of_IndicatorMarginPercent_6(),
	HeadsUpDirectionIndicator_t483009371::get_offset_of_DebugDrawPointerOrientationPlanes_7(),
	HeadsUpDirectionIndicator_t483009371::get_offset_of_pointer_8(),
	HeadsUpDirectionIndicator_t483009371_StaticFields::get_offset_of_frustumLastUpdated_9(),
	HeadsUpDirectionIndicator_t483009371_StaticFields::get_offset_of_frustumPlanes_10(),
	HeadsUpDirectionIndicator_t483009371_StaticFields::get_offset_of_cameraForward_11(),
	HeadsUpDirectionIndicator_t483009371_StaticFields::get_offset_of_cameraPosition_12(),
	HeadsUpDirectionIndicator_t483009371_StaticFields::get_offset_of_cameraRight_13(),
	HeadsUpDirectionIndicator_t483009371_StaticFields::get_offset_of_cameraUp_14(),
	HeadsUpDirectionIndicator_t483009371::get_offset_of_indicatorVolume_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5674 = { sizeof (FrustumPlanes_t4124430689)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5674[7] = 
{
	FrustumPlanes_t4124430689::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5675 = { sizeof (InputMappingAxisUtility_t387181254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5675[19] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5676 = { sizeof (NearPlaneFade_t1087371927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5676[5] = 
{
	NearPlaneFade_t1087371927::get_offset_of_FadeDistanceStart_2(),
	NearPlaneFade_t1087371927::get_offset_of_FadeDistanceEnd_3(),
	NearPlaneFade_t1087371927::get_offset_of_NearPlaneFadeOn_4(),
	0,
	NearPlaneFade_t1087371927::get_offset_of_fadeDistancePropertyID_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5677 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5677[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5678 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5678[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5679 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5679[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5680 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5680[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5681 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5681[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5682 = { sizeof (RaycastHelper_t3966188783), -1, sizeof(RaycastHelper_t3966188783_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5682[1] = 
{
	RaycastHelper_t3966188783_StaticFields::get_offset_of_DebugEnabled_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5683 = { sizeof (RaycastFunc_t2509020078), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5684 = { sizeof (U3CU3Ec__DisplayClass5_0_t860465322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5684[1] = 
{
	U3CU3Ec__DisplayClass5_0_t860465322::get_offset_of_collider_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5685 = { sizeof (U3CU3Ec_t4080613386), -1, sizeof(U3CU3Ec_t4080613386_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5685[2] = 
{
	U3CU3Ec_t4080613386_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4080613386_StaticFields::get_offset_of_U3CU3E9__5_1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5686 = { sizeof (RaycastResultHelper_t2007985016)+ sizeof (RuntimeObject), -1, sizeof(RaycastResultHelper_t2007985016_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5686[9] = 
{
	RaycastResultHelper_t2007985016::get_offset_of_collider_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t2007985016::get_offset_of_layer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t2007985016::get_offset_of_normal_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t2007985016::get_offset_of_distance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t2007985016::get_offset_of_point_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t2007985016::get_offset_of_transform_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t2007985016::get_offset_of_textureCoord_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t2007985016::get_offset_of_textureCoord2_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t2007985016_StaticFields::get_offset_of_None_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5687 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5687[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5688 = { sizeof (ReadOnlyHashSetRelatedExtensions_t2711452126), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5689 = { sizeof (SceneLauncher_t1552664439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5689[6] = 
{
	SceneLauncher_t1552664439::get_offset_of_sceneMapping_4(),
	SceneLauncher_t1552664439::get_offset_of_ButtonSpawnLocation_5(),
	SceneLauncher_t1552664439::get_offset_of_SceneButtonPrefab_6(),
	SceneLauncher_t1552664439::get_offset_of_MaxRows_7(),
	SceneLauncher_t1552664439::get_offset_of_U3CSceneLauncherBuildIndexU3Ek__BackingField_8(),
	SceneLauncher_t1552664439::get_offset_of_sceneButtonSize_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5690 = { sizeof (SceneMapping_t92329067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5690[2] = 
{
	SceneMapping_t92329067::get_offset_of_ScenePath_0(),
	SceneMapping_t92329067::get_offset_of_IsButtonEnabled_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5691 = { sizeof (SceneLauncherButton_t1133275438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5691[6] = 
{
	SceneLauncherButton_t1133275438::get_offset_of_U3CSceneIndexU3Ek__BackingField_2(),
	SceneLauncherButton_t1133275438::get_offset_of_HighlightedTextColor_3(),
	SceneLauncherButton_t1133275438::get_offset_of_MenuReference_4(),
	SceneLauncherButton_t1133275438::get_offset_of_EnableDebug_5(),
	SceneLauncherButton_t1133275438::get_offset_of_textMesh_6(),
	SceneLauncherButton_t1133275438::get_offset_of_originalTextColor_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5692 = { sizeof (SimpleTagalong_t3886944103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5692[12] = 
{
	SimpleTagalong_t3886944103::get_offset_of_TagalongDistance_2(),
	SimpleTagalong_t3886944103::get_offset_of_EnforceDistance_3(),
	SimpleTagalong_t3886944103::get_offset_of_PositionUpdateSpeed_4(),
	SimpleTagalong_t3886944103::get_offset_of_SmoothMotion_5(),
	SimpleTagalong_t3886944103::get_offset_of_SmoothingFactor_6(),
	SimpleTagalong_t3886944103::get_offset_of_tagalongCollider_7(),
	SimpleTagalong_t3886944103::get_offset_of_interpolator_8(),
	SimpleTagalong_t3886944103::get_offset_of_frustumPlanes_9(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5693 = { sizeof (Solver_t4167981057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5693[12] = 
{
	Solver_t4167981057::get_offset_of_UpdateLinkedTransform_2(),
	Solver_t4167981057::get_offset_of_MoveLerpTime_3(),
	Solver_t4167981057::get_offset_of_RotateLerpTime_4(),
	Solver_t4167981057::get_offset_of_ScaleLerpTime_5(),
	Solver_t4167981057::get_offset_of_MaintainScale_6(),
	Solver_t4167981057::get_offset_of_GoalPosition_7(),
	Solver_t4167981057::get_offset_of_GoalRotation_8(),
	Solver_t4167981057::get_offset_of_GoalScale_9(),
	Solver_t4167981057::get_offset_of_Smoothing_10(),
	Solver_t4167981057::get_offset_of_Lifetime_11(),
	Solver_t4167981057::get_offset_of_solverHandler_12(),
	Solver_t4167981057::get_offset_of_lifetime_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5694 = { sizeof (U3CCoStartU3Ed__15_t802829203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5694[3] = 
{
	U3CCoStartU3Ed__15_t802829203::get_offset_of_U3CU3E1__state_0(),
	U3CCoStartU3Ed__15_t802829203::get_offset_of_U3CU3E2__current_1(),
	U3CCoStartU3Ed__15_t802829203::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5695 = { sizeof (SolverBodyLock_t3340336644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5695[4] = 
{
	SolverBodyLock_t3340336644::get_offset_of_Orientation_14(),
	SolverBodyLock_t3340336644::get_offset_of_offset_15(),
	SolverBodyLock_t3340336644::get_offset_of_RotationTether_16(),
	SolverBodyLock_t3340336644::get_offset_of_TetherAngleSteps_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5696 = { sizeof (OrientationReference_t825611799)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5696[3] = 
{
	OrientationReference_t825611799::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5697 = { sizeof (SolverConstantViewSize_t3996106604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5697[12] = 
{
	SolverConstantViewSize_t3996106604::get_offset_of_TargetViewPercentV_14(),
	SolverConstantViewSize_t3996106604::get_offset_of_MinDistance_15(),
	SolverConstantViewSize_t3996106604::get_offset_of_MaxDistance_16(),
	SolverConstantViewSize_t3996106604::get_offset_of_MinScale_17(),
	SolverConstantViewSize_t3996106604::get_offset_of_MaxScale_18(),
	SolverConstantViewSize_t3996106604::get_offset_of_ScaleBuffer_19(),
	SolverConstantViewSize_t3996106604::get_offset_of_ManualObjectSize_20(),
	SolverConstantViewSize_t3996106604::get_offset_of_ScaleState_21(),
	SolverConstantViewSize_t3996106604::get_offset_of_fovScalar_22(),
	SolverConstantViewSize_t3996106604::get_offset_of_objectSize_23(),
	SolverConstantViewSize_t3996106604::get_offset_of_objectScalePercent_24(),
	SolverConstantViewSize_t3996106604::get_offset_of_objectDistancePercent_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5698 = { sizeof (ScaleStateEnum_t571318243)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5698[4] = 
{
	ScaleStateEnum_t571318243::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5699 = { sizeof (SolverHandler_t1963548039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5699[9] = 
{
	SolverHandler_t1963548039::get_offset_of_trackedObjectToReference_6(),
	SolverHandler_t1963548039::get_offset_of_transformTarget_7(),
	SolverHandler_t1963548039::get_offset_of_U3CGoalPositionU3Ek__BackingField_8(),
	SolverHandler_t1963548039::get_offset_of_U3CGoalRotationU3Ek__BackingField_9(),
	SolverHandler_t1963548039::get_offset_of_U3CGoalScaleU3Ek__BackingField_10(),
	SolverHandler_t1963548039::get_offset_of_U3CAltScaleU3Ek__BackingField_11(),
	SolverHandler_t1963548039::get_offset_of_U3CDeltaTimeU3Ek__BackingField_12(),
	SolverHandler_t1963548039::get_offset_of_U3CLastUpdateTimeU3Ek__BackingField_13(),
	SolverHandler_t1963548039::get_offset_of_m_Solvers_14(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
