﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.AspectRatioFitter/AspectMode>
struct IInspectableToIReadOnlyCollectionAdapter_1_t1934872071;
// System.InvalidOperationException
struct InvalidOperationException_t56020091;
// System.String
struct String_t;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.ColorBlock>
struct IInspectableToIReadOnlyCollectionAdapter_1_t656710646;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.ContentSizeFitter/FitMode>
struct IInspectableToIReadOnlyCollectionAdapter_1_t1785560286;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Image/FillMethod>
struct IInspectableToIReadOnlyCollectionAdapter_1_t3980103938;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Image/Type>
struct IInspectableToIReadOnlyCollectionAdapter_1_t3965527896;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/CharacterValidation>
struct IInspectableToIReadOnlyCollectionAdapter_1_t2569593509;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/ContentType>
struct IInspectableToIReadOnlyCollectionAdapter_1_t304982468;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/InputType>
struct IInspectableToIReadOnlyCollectionAdapter_1_t288079751;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/LineType>
struct IInspectableToIReadOnlyCollectionAdapter_1_t2732327541;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Navigation>
struct IInspectableToIReadOnlyCollectionAdapter_1_t1566995651;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Scrollbar/Direction>
struct IInspectableToIReadOnlyCollectionAdapter_1_t1988393425;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Selectable/Transition>
struct IInspectableToIReadOnlyCollectionAdapter_1_t287587703;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Slider/Direction>
struct IInspectableToIReadOnlyCollectionAdapter_1_t3150555603;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.SpriteState>
struct IInspectableToIReadOnlyCollectionAdapter_1_t4175632847;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UICharInfo>
struct IInspectableToIReadOnlyCollectionAdapter_1_t2888147474;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UILineInfo>
struct IInspectableToIReadOnlyCollectionAdapter_1_t2712945882;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UIVertex>
struct IInspectableToIReadOnlyCollectionAdapter_1_t2575176677;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UnitySynchronizationContext/WorkRequest>
struct IInspectableToIReadOnlyCollectionAdapter_1_t4167164980;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Vector2>
struct IInspectableToIReadOnlyCollectionAdapter_1_t673908595;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Vector3>
struct IInspectableToIReadOnlyCollectionAdapter_1_t2239992536;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Vector4>
struct IInspectableToIReadOnlyCollectionAdapter_1_t1836708009;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Windows.Speech.SemanticMeaning>
struct IInspectableToIReadOnlyCollectionAdapter_1_t2051177558;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.XR.WSA.Input.InteractionSourceState>
struct IInspectableToIReadOnlyCollectionAdapter_1_t1943492846;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.XR.WSA.SurfaceId>
struct IInspectableToIReadOnlyCollectionAdapter_1_t1516773818;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityGLTF.GLTFSceneImporter/MaterialType>
struct IInspectableToIReadOnlyCollectionAdapter_1_t3410419400;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<Windows.Foundation.Point>
struct IInspectableToIReadOnlyCollectionAdapter_1_t2682632611;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<Windows.Foundation.Rect>
struct IInspectableToIReadOnlyCollectionAdapter_1_t1212792559;
// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<Windows.Foundation.Size>
struct IInspectableToIReadOnlyCollectionAdapter_1_t3363564006;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<GLTF.Math.Color>
struct IIterableToIEnumerableAdapter_1_t3935527553;
// System.Collections.Generic.IEnumerator`1<GLTF.Math.Color>
struct IEnumerator_1_t2070170769;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<GLTF.Math.Vector2>
struct IIterableToIEnumerableAdapter_1_t2139488089;
// System.Collections.Generic.IEnumerator`1<GLTF.Math.Vector2>
struct IEnumerator_1_t274131305;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<GLTF.Math.Vector3>
struct IIterableToIEnumerableAdapter_1_t2139488088;
// System.Collections.Generic.IEnumerator`1<GLTF.Math.Vector3>
struct IEnumerator_1_t274131304;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<GLTF.Math.Vector4>
struct IIterableToIEnumerableAdapter_1_t2139488091;
// System.Collections.Generic.IEnumerator`1<GLTF.Math.Vector4>
struct IEnumerator_1_t274131307;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Sharing.Spawning.PrefabToDataModel>
struct IIterableToIEnumerableAdapter_1_t4060230472;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Sharing.Spawning.PrefabToDataModel>
struct IEnumerator_1_t2194873688;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.Buttons.AnimatorControllerAction>
struct IIterableToIEnumerableAdapter_1_t2448138060;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.Buttons.AnimatorControllerAction>
struct IEnumerator_1_t582781276;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.Design.SplinePoint>
struct IIterableToIEnumerableAdapter_1_t1318270711;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.Design.SplinePoint>
struct IEnumerator_1_t3747881223;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.AnimatorParameter>
struct IIterableToIEnumerableAdapter_1_t2152661038;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.InputModule.AnimatorParameter>
struct IEnumerator_1_t287304254;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.AnimCursorDatum>
struct IIterableToIEnumerableAdapter_1_t2220864576;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.InputModule.AnimCursorDatum>
struct IEnumerator_1_t355507792;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.InputSourceInfo>
struct IIterableToIEnumerableAdapter_1_t2397954994;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.InputModule.InputSourceInfo>
struct IEnumerator_1_t532598210;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair>
struct IIterableToIEnumerableAdapter_1_t3808032750;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair>
struct IEnumerator_1_t1942675966;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.KeywordAndKeyCode>
struct IIterableToIEnumerableAdapter_1_t717916647;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.InputModule.KeywordAndKeyCode>
struct IEnumerator_1_t3147527159;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.MeshCursor/MeshCursorDatum>
struct IIterableToIEnumerableAdapter_1_t149313005;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.InputModule.MeshCursor/MeshCursorDatum>
struct IEnumerator_1_t2578923517;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.ObjectCursor/ObjectCursorDatum>
struct IIterableToIEnumerableAdapter_1_t2086744183;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.InputModule.ObjectCursor/ObjectCursorDatum>
struct IEnumerator_1_t221387399;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.SpeechInputHandler/KeywordAndResponse>
struct IIterableToIEnumerableAdapter_1_t1414184956;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.InputModule.SpeechInputHandler/KeywordAndResponse>
struct IEnumerator_1_t3843795468;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.SpriteCursor/SpriteCursorDatum>
struct IIterableToIEnumerableAdapter_1_t2367326288;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.InputModule.SpriteCursor/SpriteCursorDatum>
struct IEnumerator_1_t501969504;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.XboxControllerData>
struct IIterableToIEnumerableAdapter_1_t4218148398;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.InputModule.XboxControllerData>
struct IEnumerator_1_t2352791614;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.RaycastResultHelper>
struct IIterableToIEnumerableAdapter_1_t10944972;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.RaycastResultHelper>
struct IEnumerator_1_t2440555484;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.RayStep>
struct IIterableToIEnumerableAdapter_1_t150612463;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.RayStep>
struct IEnumerator_1_t2580222975;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialMapping.BoundedPlane>
struct IIterableToIEnumerableAdapter_1_t1409134785;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.SpatialMapping.BoundedPlane>
struct IEnumerator_1_t3838745297;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialMapping.PlaneFinding/DLLImports/ImportedMeshData>
struct IIterableToIEnumerableAdapter_1_t2856865207;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.SpatialMapping.PlaneFinding/DLLImports/ImportedMeshData>
struct IEnumerator_1_t991508423;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialMapping.PlaneFinding/MeshData>
struct IIterableToIEnumerableAdapter_1_t2117375307;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.SpatialMapping.PlaneFinding/MeshData>
struct IEnumerator_1_t252018523;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject>
struct IIterableToIEnumerableAdapter_1_t616200274;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject>
struct IEnumerator_1_t3045810786;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData>
struct IIterableToIEnumerableAdapter_1_t1860586188;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData>
struct IEnumerator_1_t4290196700;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialUnderstandingDll/MeshData>
struct IIterableToIEnumerableAdapter_1_t1364795558;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.SpatialUnderstandingDll/MeshData>
struct IEnumerator_1_t3794406070;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint>
struct IIterableToIEnumerableAdapter_1_t2244913691;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint>
struct IEnumerator_1_t379556907;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.TimerScheduler/TimerData>
struct IIterableToIEnumerableAdapter_1_t2059675446;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.TimerScheduler/TimerData>
struct IEnumerator_1_t194318662;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.TimerScheduler/TimerIdPair>
struct IIterableToIEnumerableAdapter_1_t3219251157;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.TimerScheduler/TimerIdPair>
struct IEnumerator_1_t1353894373;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.UX.SplinePoint>
struct IIterableToIEnumerableAdapter_1_t2158001848;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.UX.SplinePoint>
struct IEnumerator_1_t292645064;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo>
struct IIterableToIEnumerableAdapter_1_t391484503;
// System.Collections.Generic.IEnumerator`1<HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo>
struct IEnumerator_1_t2821095015;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
struct IIterableToIEnumerableAdapter_1_t1335827848;
// System.Collections.Generic.IEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
struct IEnumerator_1_t3765438360;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Mono.Security.Interface.CipherSuiteCode>
struct IIterableToIEnumerableAdapter_1_t3030489463;
// System.Collections.Generic.IEnumerator`1<Mono.Security.Interface.CipherSuiteCode>
struct IEnumerator_1_t1165132679;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
struct IIterableToIEnumerableAdapter_1_t3302632160;
// System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
struct IEnumerator_1_t1437275376;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Mono.Security.Uri/UriScheme>
struct IIterableToIEnumerableAdapter_1_t870766298;
// System.Collections.Generic.IEnumerator`1<Mono.Security.Uri/UriScheme>
struct IEnumerator_1_t3300376810;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<MS.Internal.Xml.Cache.XPathNode>
struct IIterableToIEnumerableAdapter_1_t211032832;
// System.Collections.Generic.IEnumerator`1<MS.Internal.Xml.Cache.XPathNode>
struct IEnumerator_1_t2640643344;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<MS.Internal.Xml.Cache.XPathNodeRef>
struct IIterableToIEnumerableAdapter_1_t1501148974;
// System.Collections.Generic.IEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>
struct IEnumerator_1_t3930759486;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<MS.Internal.Xml.XPath.Operator/Op>
struct IIterableToIEnumerableAdapter_1_t49765125;
// System.Collections.Generic.IEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>
struct IEnumerator_1_t2479375637;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.JsonPosition>
struct IIterableToIEnumerableAdapter_1_t530987670;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.JsonPosition>
struct IEnumerator_1_t2960598182;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.JsonWriter/State>
struct IIterableToIEnumerableAdapter_1_t598626605;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.JsonWriter/State>
struct IEnumerator_1_t3028237117;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.Linq.JTokenType>
struct IIterableToIEnumerableAdapter_1_t1025321618;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JTokenType>
struct IEnumerator_1_t3454932130;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.ReadType>
struct IIterableToIEnumerableAdapter_1_t2638713832;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.ReadType>
struct IEnumerator_1_t773357048;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>
struct IIterableToIEnumerableAdapter_1_t988501917;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>
struct IEnumerator_1_t3418112429;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.Serialization.ResolverContractKey>
struct IIterableToIEnumerableAdapter_1_t1295811243;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Serialization.ResolverContractKey>
struct IEnumerator_1_t3725421755;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>
struct IIterableToIEnumerableAdapter_1_t2583234012;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>
struct IEnumerator_1_t717877228;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct IIterableToIEnumerableAdapter_1_t3096877156;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct IEnumerator_1_t1231520372;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.AppContext/SwitchValueState>
struct IIterableToIEnumerableAdapter_1_t808211423;
// System.Collections.Generic.IEnumerator`1<System.AppContext/SwitchValueState>
struct IEnumerator_1_t3237821935;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.ArraySegment`1<System.Byte>>
struct IIterableToIEnumerableAdapter_1_t2581488239;
// System.Collections.Generic.IEnumerator`1<System.ArraySegment`1<System.Byte>>
struct IEnumerator_1_t716131455;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Boolean>
struct IIterableToIEnumerableAdapter_1_t2395215217;
// System.Collections.Generic.IEnumerator`1<System.Boolean>
struct IEnumerator_1_t529858433;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Byte>
struct IIterableToIEnumerableAdapter_1_t3432223628;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t1566866844;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Char>
struct IIterableToIEnumerableAdapter_1_t1637420426;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t4067030938;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.DictionaryEntry>
struct IIterableToIEnumerableAdapter_1_t1126935594;
// System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>
struct IEnumerator_1_t3556546106;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>>
struct IIterableToIEnumerableAdapter_1_t4056732158;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>>
struct IEnumerator_1_t2191375374;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>
struct IIterableToIEnumerableAdapter_1_t92757476;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>
struct IEnumerator_1_t2522367988;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>
struct IIterableToIEnumerableAdapter_1_t2159831731;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>
struct IEnumerator_1_t294474947;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>
struct IIterableToIEnumerableAdapter_1_t488053677;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>
struct IEnumerator_1_t2917664189;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>
struct IIterableToIEnumerableAdapter_1_t2268747928;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>
struct IEnumerator_1_t403391144;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>
struct IIterableToIEnumerableAdapter_1_t1746948141;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>
struct IEnumerator_1_t4176558653;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>
struct IIterableToIEnumerableAdapter_1_t1586643939;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>
struct IEnumerator_1_t4016254451;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Int64,System.Object>>
struct IIterableToIEnumerableAdapter_1_t3760570392;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int64,System.Object>>
struct IEnumerator_1_t1895213608;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.IntPtr,System.Object>>
struct IIterableToIEnumerableAdapter_1_t919796327;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.IntPtr,System.Object>>
struct IEnumerator_1_t3349406839;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>>
struct IIterableToIEnumerableAdapter_1_t1641124000;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>>
struct IEnumerator_1_t4070734512;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>>
struct IIterableToIEnumerableAdapter_1_t1770284411;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>>
struct IEnumerator_1_t4199894923;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.ReadType>>
struct IIterableToIEnumerableAdapter_1_t1306017308;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.ReadType>>
struct IEnumerator_1_t3735627820;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>
struct IIterableToIEnumerableAdapter_1_t1764180632;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>
struct IEnumerator_1_t4193791144;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.AppContext/SwitchValueState>>
struct IIterableToIEnumerableAdapter_1_t3770482195;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.AppContext/SwitchValueState>>
struct IEnumerator_1_t1905125411;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>
struct IIterableToIEnumerableAdapter_1_t1062518693;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>
struct IEnumerator_1_t3492129205;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>
struct IIterableToIEnumerableAdapter_1_t3916176481;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>
struct IEnumerator_1_t2050819697;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>>
struct IIterableToIEnumerableAdapter_1_t4245520317;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>>
struct IEnumerator_1_t2380163533;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>
struct IIterableToIEnumerableAdapter_1_t4045336892;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>
struct IEnumerator_1_t2179980108;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>
struct IIterableToIEnumerableAdapter_1_t394234239;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>
struct IEnumerator_1_t2823844751;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>>
struct IIterableToIEnumerableAdapter_1_t4253737224;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>>
struct IEnumerator_1_t2388380440;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>>
struct IIterableToIEnumerableAdapter_1_t1474094753;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>>
struct IEnumerator_1_t3903705265;
// Windows.Foundation.Collections.IIterator`1<System.Char>
struct IIterator_1_t1508603943;
// Windows.Foundation.Collections.IIterator`1<System.Boolean>
struct IIterator_1_t2266398734;
// Windows.Foundation.Collections.IIterator`1<System.Byte>
struct IIterator_1_t3303407145;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Type
struct Type_t;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventRegistrationTokenListWithCount>
struct ConditionalWeakTable_2_t3044373657;
// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/TokenListCount
struct TokenListCount_t1606756367;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.MeshCollider
struct MeshCollider_t903564387;
// HoloToolkit.Unity.TimerScheduler/Callback
struct Callback_t2663646540;
// HoloToolkit.Unity.InputModule.IInputSource
struct IInputSource_t3332243029;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// MS.Internal.Xml.Cache.XPathNode[]
struct XPathNodeU5BU5D_t47339301;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// MS.Internal.Xml.Cache.XPathNodeInfoAtom
struct XPathNodeInfoAtom_t1760358141;
// System.Void
struct Void_t1185182177;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Transform
struct Transform_t3600365921;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.TimerScheduler/TimerIdPair>
struct IIterator_1_t3090434674;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.InputModule.InputSourceInfo>
struct IIterator_1_t2269138511;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.TimerScheduler/TimerData>
struct IIterator_1_t1930858963;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.InputModule.SpeechInputHandler/KeywordAndResponse>
struct IIterator_1_t1285368473;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.InputModule.XboxControllerData>
struct IIterator_1_t4089331915;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject>
struct IIterator_1_t487383791;
// Windows.Foundation.Collections.IIterator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
struct IIterator_1_t1207011365;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>
struct IIterator_1_t933702210;
// Windows.Foundation.Collections.IIterator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>
struct IIterator_1_t2454417529;
// Windows.Foundation.Collections.IIterator`1<GLTF.Math.Color>
struct IIterator_1_t3806711070;
// Windows.Foundation.Collections.IIterator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>
struct IIterator_1_t859685434;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>
struct IIterator_1_t3787359998;
// Windows.Foundation.Collections.IIterator`1<Newtonsoft.Json.Serialization.ResolverContractKey>
struct IIterator_1_t1166994760;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int64,System.Object>>
struct IIterator_1_t3631753909;
// System.Collections.Generic.List`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct List_1_t1790965530;
// Windows.Foundation.Collections.IIterator`1<System.Collections.DictionaryEntry>
struct IIterator_1_t998119111;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>
struct IIterator_1_t1457827456;
// Windows.Foundation.Collections.IIterator`1<System.ArraySegment`1<System.Byte>>
struct IIterator_1_t2452671756;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>
struct IIterator_1_t3916520409;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Sharing.Spawning.PrefabToDataModel>
struct IIterator_1_t3931413989;
// Windows.Foundation.Collections.IIterator`1<Mono.Security.Uri/UriScheme>
struct IIterator_1_t741949815;
// Windows.Foundation.Collections.IIterator`1<GLTF.Math.Vector4>
struct IIterator_1_t2010671608;
// Windows.Foundation.Collections.IIterator`1<MS.Internal.Xml.Cache.XPathNodeRef>
struct IIterator_1_t1372332491;
// Windows.Foundation.Collections.IIterator`1<MS.Internal.Xml.Cache.XPathNode>
struct IIterator_1_t82216349;
// Windows.Foundation.Collections.IIterator`1<GLTF.Math.Vector2>
struct IIterator_1_t2010671606;
// Windows.Foundation.Collections.IIterator`1<GLTF.Math.Vector3>
struct IIterator_1_t2010671605;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>
struct IIterator_1_t1618131658;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.Design.SplinePoint>
struct IIterator_1_t1189454228;
// Windows.Foundation.Collections.IIterator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
struct IIterator_1_t3173815677;
// Windows.Foundation.Collections.IIterator`1<Mono.Security.Interface.CipherSuiteCode>
struct IIterator_1_t2901672980;
// Windows.Foundation.Collections.IIterator`1<MS.Internal.Xml.XPath.Operator/Op>
struct IIterator_1_t4215915938;
// Windows.Foundation.Collections.IIterator`1<Newtonsoft.Json.JsonWriter/State>
struct IIterator_1_t469810122;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>>
struct IIterator_1_t1345278270;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.UX.SplinePoint>
struct IIterator_1_t2029185365;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>
struct IIterator_1_t2139931445;
// Windows.Foundation.Collections.IIterator`1<Newtonsoft.Json.Linq.JTokenType>
struct IIterator_1_t896505135;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>
struct IIterator_1_t4258908289;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>>
struct IIterator_1_t1512307517;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.IntPtr,System.Object>>
struct IIterator_1_t790979844;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>
struct IIterator_1_t359237194;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>
struct IIterator_1_t2031015248;
// Windows.Foundation.Collections.IIterator`1<Newtonsoft.Json.ReadType>
struct IIterator_1_t2509897349;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>
struct IIterator_1_t265417756;
// Windows.Foundation.Collections.IIterator`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct IIterator_1_t2968060673;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>>
struct IIterator_1_t1641467928;
// Windows.Foundation.Collections.IIterator`1<System.AppContext/SwitchValueState>
struct IIterator_1_t679394940;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData>
struct IIterator_1_t1731769705;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.SpatialUnderstandingDll/MeshData>
struct IIterator_1_t1235979075;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.SpatialMapping.PlaneFinding/DLLImports/ImportedMeshData>
struct IIterator_1_t2728048724;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.SpatialMapping.PlaneFinding/MeshData>
struct IIterator_1_t1988558824;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.RaycastResultHelper>
struct IIterator_1_t4177095785;
// UnityEngine.Sprite
struct Sprite_t280657092;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.RayStep>
struct IIterator_1_t21795980;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.SpatialMapping.BoundedPlane>
struct IIterator_1_t1280318302;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.AppContext/SwitchValueState>>
struct IIterator_1_t3641665712;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.InputModule.MeshCursor/MeshCursorDatum>
struct IIterator_1_t20496522;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.InputModule.ObjectCursor/ObjectCursorDatum>
struct IIterator_1_t1957927700;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.ReadType>>
struct IIterator_1_t1177200825;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>
struct IIterator_1_t1635364149;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint>
struct IIterator_1_t2116097208;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo>
struct IIterator_1_t262668020;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>>
struct IIterator_1_t4124920741;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.InputModule.AnimatorParameter>
struct IIterator_1_t2023844555;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.Buttons.AnimatorControllerAction>
struct IIterator_1_t2319321577;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.InputModule.KeywordAndKeyCode>
struct IIterator_1_t589100164;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.InputModule.SpriteCursor/SpriteCursorDatum>
struct IIterator_1_t2238509805;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>>
struct IIterator_1_t4116703834;
// Windows.Foundation.Collections.IIterator`1<Newtonsoft.Json.JsonPosition>
struct IIterator_1_t402171187;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair>
struct IIterator_1_t3679216267;
// Windows.Foundation.Collections.IIterator`1<HoloToolkit.Unity.InputModule.AnimCursorDatum>
struct IIterator_1_t2092048093;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>>
struct IIterator_1_t3927915675;

extern RuntimeClass* InvalidOperationException_t56020091_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3700751949;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m2371684166_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m185714638_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3410248800_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4090770565_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4008501801_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m473212014_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4274431090_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m586500471_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1289551344_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3322370442_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m2257275539_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3296463687_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3254002506_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m478565721_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1037077666_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3347196995_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1447146653_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4139970572_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m2298537932_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3672359736_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m2771082488_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1480329006_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m2885142129_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4044255659_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1384635258_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1925995359_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4228673602_MetadataUsageId;
extern const uint32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1701789447_MetadataUsageId;
struct IIterator_1_t1508603943;
struct IIterator_1_t2266398734;
struct IIterator_1_t3303407145;
struct Exception_t1436737249_marshaled_pinvoke;
struct Exception_t1436737249_marshaled_com;
struct XPathNode_t2208072876_marshaled_pinvoke;
struct XPathNode_t2208072876_marshaled_com;
struct Vector3_t3722313464 ;
struct Size_t550917638 ;
struct Rect_t2695113487 ;
struct Point_t4164953539 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
// Windows.Foundation.Collections.IIterator`1<System.Char>
struct NOVTABLE IIterator_1_t1508603943 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m917445785(Il2CppChar* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2716317294(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3026282028(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2511776893(uint32_t ___items0ArraySize, Il2CppChar* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Char>
struct NOVTABLE IIterable_1_t1255857930 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m2678403700(IIterator_1_t1508603943** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Boolean>
struct NOVTABLE IIterator_1_t2266398734 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2444129316(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2357690268(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1331932701(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m280069361(uint32_t ___items0ArraySize, bool* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Boolean>
struct NOVTABLE IIterable_1_t2013652721 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m91359581(IIterator_1_t2266398734** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Byte>
struct NOVTABLE IIterator_1_t3303407145 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2682899182(uint8_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2127558108(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3980616567(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3559710954(uint32_t ___items0ArraySize, uint8_t* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Byte>
struct NOVTABLE IIterable_1_t3050661132 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m770671629(IIterator_1_t3303407145** comReturnValue) = 0;
};
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T211032832_H
#define IITERABLETOIENUMERABLEADAPTER_1_T211032832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<MS.Internal.Xml.Cache.XPathNode>
struct  IIterableToIEnumerableAdapter_1_t211032832  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T211032832_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T394234239_H
#define IITERABLETOIENUMERABLEADAPTER_1_T394234239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>
struct  IIterableToIEnumerableAdapter_1_t394234239  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T394234239_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3302632160_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3302632160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
struct  IIterableToIEnumerableAdapter_1_t3302632160  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3302632160_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T870766298_H
#define IITERABLETOIENUMERABLEADAPTER_1_T870766298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Mono.Security.Uri/UriScheme>
struct  IIterableToIEnumerableAdapter_1_t870766298  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T870766298_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1025321618_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1025321618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.Linq.JTokenType>
struct  IIterableToIEnumerableAdapter_1_t1025321618  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1025321618_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1474094753_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1474094753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>>
struct  IIterableToIEnumerableAdapter_1_t1474094753  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1474094753_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2638713832_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2638713832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.ReadType>
struct  IIterableToIEnumerableAdapter_1_t2638713832  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2638713832_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T4253737224_H
#define IITERABLETOIENUMERABLEADAPTER_1_T4253737224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>>
struct  IIterableToIEnumerableAdapter_1_t4253737224  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T4253737224_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T598626605_H
#define IITERABLETOIENUMERABLEADAPTER_1_T598626605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.JsonWriter/State>
struct  IIterableToIEnumerableAdapter_1_t598626605  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T598626605_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T49765125_H
#define IITERABLETOIENUMERABLEADAPTER_1_T49765125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<MS.Internal.Xml.XPath.Operator/Op>
struct  IIterableToIEnumerableAdapter_1_t49765125  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T49765125_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1501148974_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1501148974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<MS.Internal.Xml.Cache.XPathNodeRef>
struct  IIterableToIEnumerableAdapter_1_t1501148974  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1501148974_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T530987670_H
#define IITERABLETOIENUMERABLEADAPTER_1_T530987670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.JsonPosition>
struct  IIterableToIEnumerableAdapter_1_t530987670  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T530987670_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2117375307_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2117375307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialMapping.PlaneFinding/MeshData>
struct  IIterableToIEnumerableAdapter_1_t2117375307  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2117375307_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2856865207_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2856865207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialMapping.PlaneFinding/DLLImports/ImportedMeshData>
struct  IIterableToIEnumerableAdapter_1_t2856865207  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2856865207_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1860586188_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1860586188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData>
struct  IIterableToIEnumerableAdapter_1_t1860586188  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1860586188_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T616200274_H
#define IITERABLETOIENUMERABLEADAPTER_1_T616200274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject>
struct  IIterableToIEnumerableAdapter_1_t616200274  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T616200274_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T10944972_H
#define IITERABLETOIENUMERABLEADAPTER_1_T10944972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.RaycastResultHelper>
struct  IIterableToIEnumerableAdapter_1_t10944972  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T10944972_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T4218148398_H
#define IITERABLETOIENUMERABLEADAPTER_1_T4218148398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.XboxControllerData>
struct  IIterableToIEnumerableAdapter_1_t4218148398  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T4218148398_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1409134785_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1409134785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialMapping.BoundedPlane>
struct  IIterableToIEnumerableAdapter_1_t1409134785  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1409134785_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T150612463_H
#define IITERABLETOIENUMERABLEADAPTER_1_T150612463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.RayStep>
struct  IIterableToIEnumerableAdapter_1_t150612463  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T150612463_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T391484503_H
#define IITERABLETOIENUMERABLEADAPTER_1_T391484503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo>
struct  IIterableToIEnumerableAdapter_1_t391484503  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T391484503_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2158001848_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2158001848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.UX.SplinePoint>
struct  IIterableToIEnumerableAdapter_1_t2158001848  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2158001848_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3030489463_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3030489463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Mono.Security.Interface.CipherSuiteCode>
struct  IIterableToIEnumerableAdapter_1_t3030489463  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3030489463_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1335827848_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1335827848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
struct  IIterableToIEnumerableAdapter_1_t1335827848  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1335827848_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2244913691_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2244913691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint>
struct  IIterableToIEnumerableAdapter_1_t2244913691  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2244913691_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1364795558_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1364795558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialUnderstandingDll/MeshData>
struct  IIterableToIEnumerableAdapter_1_t1364795558  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1364795558_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3219251157_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3219251157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.TimerScheduler/TimerIdPair>
struct  IIterableToIEnumerableAdapter_1_t3219251157  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3219251157_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2059675446_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2059675446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.TimerScheduler/TimerData>
struct  IIterableToIEnumerableAdapter_1_t2059675446  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2059675446_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T988501917_H
#define IITERABLETOIENUMERABLEADAPTER_1_T988501917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>
struct  IIterableToIEnumerableAdapter_1_t988501917  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T988501917_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T4056732158_H
#define IITERABLETOIENUMERABLEADAPTER_1_T4056732158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t4056732158  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T4056732158_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1770284411_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1770284411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t1770284411  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1770284411_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T92757476_H
#define IITERABLETOIENUMERABLEADAPTER_1_T92757476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>
struct  IIterableToIEnumerableAdapter_1_t92757476  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T92757476_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1306017308_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1306017308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.ReadType>>
struct  IIterableToIEnumerableAdapter_1_t1306017308  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1306017308_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1637420426_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1637420426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Char>
struct  IIterableToIEnumerableAdapter_1_t1637420426  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1637420426_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1543523563_H
#define IITERATORTOIENUMERATORADAPTER_1_T1543523563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Char>
struct  IIteratorToIEnumeratorAdapter_1_t1543523563  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Il2CppChar ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1543523563, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1543523563, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1543523563, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1543523563, ___current_3)); }
	inline Il2CppChar get_current_3() const { return ___current_3; }
	inline Il2CppChar* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Il2CppChar value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1543523563_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1126935594_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1126935594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.DictionaryEntry>
struct  IIterableToIEnumerableAdapter_1_t1126935594  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1126935594_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1641124000_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1641124000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>>
struct  IIterableToIEnumerableAdapter_1_t1641124000  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1641124000_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2268747928_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2268747928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t2268747928  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2268747928_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1586643939_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1586643939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t1586643939  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1586643939_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1746948141_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1746948141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t1746948141  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1746948141_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3760570392_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3760570392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Int64,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t3760570392  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3760570392_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2159831731_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2159831731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t2159831731  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2159831731_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T919796327_H
#define IITERABLETOIENUMERABLEADAPTER_1_T919796327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.IntPtr,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t919796327  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T919796327_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T488053677_H
#define IITERABLETOIENUMERABLEADAPTER_1_T488053677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t488053677  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T488053677_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3916176481_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3916176481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>
struct  IIterableToIEnumerableAdapter_1_t3916176481  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3916176481_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3096877156_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3096877156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct  IIterableToIEnumerableAdapter_1_t3096877156  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3096877156_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1062518693_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1062518693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>
struct  IIterableToIEnumerableAdapter_1_t1062518693  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1062518693_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2583234012_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2583234012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>
struct  IIterableToIEnumerableAdapter_1_t2583234012  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2583234012_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T4045336892_H
#define IITERABLETOIENUMERABLEADAPTER_1_T4045336892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t4045336892  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T4045336892_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1295811243_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1295811243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.Serialization.ResolverContractKey>
struct  IIterableToIEnumerableAdapter_1_t1295811243  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1295811243_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T4245520317_H
#define IITERABLETOIENUMERABLEADAPTER_1_T4245520317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>>
struct  IIterableToIEnumerableAdapter_1_t4245520317  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T4245520317_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T808211423_H
#define IITERABLETOIENUMERABLEADAPTER_1_T808211423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.AppContext/SwitchValueState>
struct  IIterableToIEnumerableAdapter_1_t808211423  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T808211423_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2301318354_H
#define IITERATORTOIENUMERATORADAPTER_1_T2301318354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Boolean>
struct  IIteratorToIEnumeratorAdapter_1_t2301318354  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	bool ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2301318354, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2301318354, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2301318354, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2301318354, ___current_3)); }
	inline bool get_current_3() const { return ___current_3; }
	inline bool* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(bool value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2301318354_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3432223628_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3432223628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Byte>
struct  IIterableToIEnumerableAdapter_1_t3432223628  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3432223628_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3338326765_H
#define IITERATORTOIENUMERATORADAPTER_1_T3338326765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Byte>
struct  IIteratorToIEnumeratorAdapter_1_t3338326765  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	uint8_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3338326765, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3338326765, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3338326765, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3338326765, ___current_3)); }
	inline uint8_t get_current_3() const { return ___current_3; }
	inline uint8_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(uint8_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3338326765_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2395215217_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2395215217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Boolean>
struct  IIterableToIEnumerableAdapter_1_t2395215217  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2395215217_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3770482195_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3770482195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.AppContext/SwitchValueState>>
struct  IIterableToIEnumerableAdapter_1_t3770482195  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3770482195_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2581488239_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2581488239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.ArraySegment`1<System.Byte>>
struct  IIterableToIEnumerableAdapter_1_t2581488239  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2581488239_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1764180632_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1764180632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>
struct  IIterableToIEnumerableAdapter_1_t1764180632  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1764180632_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2367326288_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2367326288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.SpriteCursor/SpriteCursorDatum>
struct  IIterableToIEnumerableAdapter_1_t2367326288  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2367326288_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T673908595_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T673908595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Vector2>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t673908595  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T673908595_H
#ifndef EXCEPTION_T1436737249_H
#define EXCEPTION_T1436737249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1436737249  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t1436737249 * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____innerException_4)); }
	inline Exception_t1436737249 * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t1436737249 ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t1436737249 * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t1436737249_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t1436737249_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t1436737249_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1436737249_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t1436737249_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1436737249_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T1436737249_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T4167164980_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T4167164980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UnitySynchronizationContext/WorkRequest>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t4167164980  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T4167164980_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1836708009_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1836708009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Vector4>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t1836708009  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1836708009_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2239992536_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2239992536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Vector3>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t2239992536  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2239992536_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1318270711_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1318270711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.Design.SplinePoint>
struct  IIterableToIEnumerableAdapter_1_t1318270711  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1318270711_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2888147474_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2888147474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UICharInfo>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t2888147474  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2888147474_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2220864576_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2220864576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.AnimCursorDatum>
struct  IIterableToIEnumerableAdapter_1_t2220864576  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2220864576_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T4175632847_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T4175632847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.SpriteState>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t4175632847  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T4175632847_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2152661038_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2152661038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.AnimatorParameter>
struct  IIterableToIEnumerableAdapter_1_t2152661038  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2152661038_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2575176677_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2575176677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UIVertex>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t2575176677  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2575176677_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2712945882_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2712945882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UILineInfo>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t2712945882  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2712945882_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2051177558_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2051177558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Windows.Speech.SemanticMeaning>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t2051177558  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2051177558_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2139488089_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2139488089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<GLTF.Math.Vector2>
struct  IIterableToIEnumerableAdapter_1_t2139488089  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2139488089_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2139488088_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2139488088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<GLTF.Math.Vector3>
struct  IIterableToIEnumerableAdapter_1_t2139488088  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2139488088_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2682632611_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2682632611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<Windows.Foundation.Point>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t2682632611  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2682632611_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T3363564006_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T3363564006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<Windows.Foundation.Size>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t3363564006  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T3363564006_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3935527553_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3935527553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<GLTF.Math.Color>
struct  IIterableToIEnumerableAdapter_1_t3935527553  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3935527553_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1212792559_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1212792559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<Windows.Foundation.Rect>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t1212792559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1212792559_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1516773818_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1516773818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.XR.WSA.SurfaceId>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t1516773818  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1516773818_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1943492846_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1943492846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.XR.WSA.Input.InteractionSourceState>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t1943492846  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1943492846_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2448138060_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2448138060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.Buttons.AnimatorControllerAction>
struct  IIterableToIEnumerableAdapter_1_t2448138060  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2448138060_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2139488091_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2139488091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<GLTF.Math.Vector4>
struct  IIterableToIEnumerableAdapter_1_t2139488091  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2139488091_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T3410419400_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T3410419400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityGLTF.GLTFSceneImporter/MaterialType>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t3410419400  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T3410419400_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T4060230472_H
#define IITERABLETOIENUMERABLEADAPTER_1_T4060230472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Sharing.Spawning.PrefabToDataModel>
struct  IIterableToIEnumerableAdapter_1_t4060230472  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T4060230472_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T3150555603_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T3150555603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Slider/Direction>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t3150555603  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T3150555603_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T149313005_H
#define IITERABLETOIENUMERABLEADAPTER_1_T149313005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.MeshCursor/MeshCursorDatum>
struct  IIterableToIEnumerableAdapter_1_t149313005  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T149313005_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T656710646_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T656710646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.ColorBlock>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t656710646  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T656710646_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1785560286_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1785560286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.ContentSizeFitter/FitMode>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t1785560286  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1785560286_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T3965527896_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T3965527896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Image/Type>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t3965527896  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T3965527896_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T3980103938_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T3980103938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Image/FillMethod>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t3980103938  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T3980103938_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1934872071_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1934872071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.AspectRatioFitter/AspectMode>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t1934872071  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1934872071_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1414184956_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1414184956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.SpeechInputHandler/KeywordAndResponse>
struct  IIterableToIEnumerableAdapter_1_t1414184956  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1414184956_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2086744183_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2086744183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.ObjectCursor/ObjectCursorDatum>
struct  IIterableToIEnumerableAdapter_1_t2086744183  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2086744183_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T287587703_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T287587703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Selectable/Transition>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t287587703  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T287587703_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2732327541_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2732327541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/LineType>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t2732327541  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2732327541_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1566995651_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1566995651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Navigation>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t1566995651  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1566995651_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2397954994_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2397954994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.InputSourceInfo>
struct  IIterableToIEnumerableAdapter_1_t2397954994  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2397954994_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1988393425_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1988393425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Scrollbar/Direction>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t1988393425  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T1988393425_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2569593509_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2569593509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/CharacterValidation>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t2569593509  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T2569593509_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T304982468_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T304982468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/ContentType>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t304982468  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T304982468_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T717916647_H
#define IITERABLETOIENUMERABLEADAPTER_1_T717916647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.KeywordAndKeyCode>
struct  IIterableToIEnumerableAdapter_1_t717916647  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T717916647_H
#ifndef IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T288079751_H
#define IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T288079751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/InputType>
struct  IInspectableToIReadOnlyCollectionAdapter_1_t288079751  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IINSPECTABLETOIREADONLYCOLLECTIONADAPTER_1_T288079751_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3808032750_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3808032750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair>
struct  IIterableToIEnumerableAdapter_1_t3808032750  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3808032750_H
#ifndef EVENTCACHEKEY_T3133620722_H
#define EVENTCACHEKEY_T3133620722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey
struct  EventCacheKey_t3133620722 
{
public:
	// System.Object System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey::target
	RuntimeObject * ___target_0;
	// System.Reflection.MethodInfo System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey::method
	MethodInfo_t * ___method_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(EventCacheKey_t3133620722, ___target_0)); }
	inline RuntimeObject * get_target_0() const { return ___target_0; }
	inline RuntimeObject ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RuntimeObject * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(EventCacheKey_t3133620722, ___method_1)); }
	inline MethodInfo_t * get_method_1() const { return ___method_1; }
	inline MethodInfo_t ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(MethodInfo_t * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier((&___method_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey
struct EventCacheKey_t3133620722_marshaled_pinvoke
{
	Il2CppIUnknown* ___target_0;
	MethodInfo_t * ___method_1;
};
// Native definition for COM marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey
struct EventCacheKey_t3133620722_marshaled_com
{
	Il2CppIUnknown* ___target_0;
	MethodInfo_t * ___method_1;
};
#endif // EVENTCACHEKEY_T3133620722_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef RESOLVERCONTRACTKEY_T3292851287_H
#define RESOLVERCONTRACTKEY_T3292851287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ResolverContractKey
struct  ResolverContractKey_t3292851287 
{
public:
	// System.Type Newtonsoft.Json.Serialization.ResolverContractKey::_resolverType
	Type_t * ____resolverType_0;
	// System.Type Newtonsoft.Json.Serialization.ResolverContractKey::_contractType
	Type_t * ____contractType_1;

public:
	inline static int32_t get_offset_of__resolverType_0() { return static_cast<int32_t>(offsetof(ResolverContractKey_t3292851287, ____resolverType_0)); }
	inline Type_t * get__resolverType_0() const { return ____resolverType_0; }
	inline Type_t ** get_address_of__resolverType_0() { return &____resolverType_0; }
	inline void set__resolverType_0(Type_t * value)
	{
		____resolverType_0 = value;
		Il2CppCodeGenWriteBarrier((&____resolverType_0), value);
	}

	inline static int32_t get_offset_of__contractType_1() { return static_cast<int32_t>(offsetof(ResolverContractKey_t3292851287, ____contractType_1)); }
	inline Type_t * get__contractType_1() const { return ____contractType_1; }
	inline Type_t ** get_address_of__contractType_1() { return &____contractType_1; }
	inline void set__contractType_1(Type_t * value)
	{
		____contractType_1 = value;
		Il2CppCodeGenWriteBarrier((&____contractType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Serialization.ResolverContractKey
struct ResolverContractKey_t3292851287_marshaled_pinvoke
{
	Type_t * ____resolverType_0;
	Type_t * ____contractType_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Serialization.ResolverContractKey
struct ResolverContractKey_t3292851287_marshaled_com
{
	Type_t * ____resolverType_0;
	Type_t * ____contractType_1;
};
#endif // RESOLVERCONTRACTKEY_T3292851287_H
#ifndef RESOURCELOCATOR_T3723970807_H
#define RESOURCELOCATOR_T3723970807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResourceLocator
struct  ResourceLocator_t3723970807 
{
public:
	// System.Object System.Resources.ResourceLocator::_value
	RuntimeObject * ____value_0;
	// System.Int32 System.Resources.ResourceLocator::_dataPos
	int32_t ____dataPos_1;

public:
	inline static int32_t get_offset_of__value_0() { return static_cast<int32_t>(offsetof(ResourceLocator_t3723970807, ____value_0)); }
	inline RuntimeObject * get__value_0() const { return ____value_0; }
	inline RuntimeObject ** get_address_of__value_0() { return &____value_0; }
	inline void set__value_0(RuntimeObject * value)
	{
		____value_0 = value;
		Il2CppCodeGenWriteBarrier((&____value_0), value);
	}

	inline static int32_t get_offset_of__dataPos_1() { return static_cast<int32_t>(offsetof(ResourceLocator_t3723970807, ____dataPos_1)); }
	inline int32_t get__dataPos_1() const { return ____dataPos_1; }
	inline int32_t* get_address_of__dataPos_1() { return &____dataPos_1; }
	inline void set__dataPos_1(int32_t value)
	{
		____dataPos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Resources.ResourceLocator
struct ResourceLocator_t3723970807_marshaled_pinvoke
{
	Il2CppIUnknown* ____value_0;
	int32_t ____dataPos_1;
};
// Native definition for COM marshalling of System.Resources.ResourceLocator
struct ResourceLocator_t3723970807_marshaled_com
{
	Il2CppIUnknown* ____value_0;
	int32_t ____dataPos_1;
};
#endif // RESOURCELOCATOR_T3723970807_H
#ifndef TYPEDCONSTANT_T714020897_H
#define TYPEDCONSTANT_T714020897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Compiler.BoundConstants/TypedConstant
struct  TypedConstant_t714020897 
{
public:
	// System.Object System.Linq.Expressions.Compiler.BoundConstants/TypedConstant::Value
	RuntimeObject * ___Value_0;
	// System.Type System.Linq.Expressions.Compiler.BoundConstants/TypedConstant::Type
	Type_t * ___Type_1;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(TypedConstant_t714020897, ___Value_0)); }
	inline RuntimeObject * get_Value_0() const { return ___Value_0; }
	inline RuntimeObject ** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(RuntimeObject * value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((&___Value_0), value);
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(TypedConstant_t714020897, ___Type_1)); }
	inline Type_t * get_Type_1() const { return ___Type_1; }
	inline Type_t ** get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(Type_t * value)
	{
		___Type_1 = value;
		Il2CppCodeGenWriteBarrier((&___Type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Linq.Expressions.Compiler.BoundConstants/TypedConstant
struct TypedConstant_t714020897_marshaled_pinvoke
{
	Il2CppIUnknown* ___Value_0;
	Type_t * ___Type_1;
};
// Native definition for COM marshalling of System.Linq.Expressions.Compiler.BoundConstants/TypedConstant
struct TypedConstant_t714020897_marshaled_com
{
	Il2CppIUnknown* ___Value_0;
	Type_t * ___Type_1;
};
#endif // TYPEDCONSTANT_T714020897_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_4;
	// System.Int16 System.Guid::_b
	int16_t ____b_5;
	// System.Int16 System.Guid::_c
	int16_t ____c_6;
	// System.Byte System.Guid::_d
	uint8_t ____d_7;
	// System.Byte System.Guid::_e
	uint8_t ____e_8;
	// System.Byte System.Guid::_f
	uint8_t ____f_9;
	// System.Byte System.Guid::_g
	uint8_t ____g_10;
	// System.Byte System.Guid::_h
	uint8_t ____h_11;
	// System.Byte System.Guid::_i
	uint8_t ____i_12;
	// System.Byte System.Guid::_j
	uint8_t ____j_13;
	// System.Byte System.Guid::_k
	uint8_t ____k_14;

public:
	inline static int32_t get_offset_of__a_4() { return static_cast<int32_t>(offsetof(Guid_t, ____a_4)); }
	inline int32_t get__a_4() const { return ____a_4; }
	inline int32_t* get_address_of__a_4() { return &____a_4; }
	inline void set__a_4(int32_t value)
	{
		____a_4 = value;
	}

	inline static int32_t get_offset_of__b_5() { return static_cast<int32_t>(offsetof(Guid_t, ____b_5)); }
	inline int16_t get__b_5() const { return ____b_5; }
	inline int16_t* get_address_of__b_5() { return &____b_5; }
	inline void set__b_5(int16_t value)
	{
		____b_5 = value;
	}

	inline static int32_t get_offset_of__c_6() { return static_cast<int32_t>(offsetof(Guid_t, ____c_6)); }
	inline int16_t get__c_6() const { return ____c_6; }
	inline int16_t* get_address_of__c_6() { return &____c_6; }
	inline void set__c_6(int16_t value)
	{
		____c_6 = value;
	}

	inline static int32_t get_offset_of__d_7() { return static_cast<int32_t>(offsetof(Guid_t, ____d_7)); }
	inline uint8_t get__d_7() const { return ____d_7; }
	inline uint8_t* get_address_of__d_7() { return &____d_7; }
	inline void set__d_7(uint8_t value)
	{
		____d_7 = value;
	}

	inline static int32_t get_offset_of__e_8() { return static_cast<int32_t>(offsetof(Guid_t, ____e_8)); }
	inline uint8_t get__e_8() const { return ____e_8; }
	inline uint8_t* get_address_of__e_8() { return &____e_8; }
	inline void set__e_8(uint8_t value)
	{
		____e_8 = value;
	}

	inline static int32_t get_offset_of__f_9() { return static_cast<int32_t>(offsetof(Guid_t, ____f_9)); }
	inline uint8_t get__f_9() const { return ____f_9; }
	inline uint8_t* get_address_of__f_9() { return &____f_9; }
	inline void set__f_9(uint8_t value)
	{
		____f_9 = value;
	}

	inline static int32_t get_offset_of__g_10() { return static_cast<int32_t>(offsetof(Guid_t, ____g_10)); }
	inline uint8_t get__g_10() const { return ____g_10; }
	inline uint8_t* get_address_of__g_10() { return &____g_10; }
	inline void set__g_10(uint8_t value)
	{
		____g_10 = value;
	}

	inline static int32_t get_offset_of__h_11() { return static_cast<int32_t>(offsetof(Guid_t, ____h_11)); }
	inline uint8_t get__h_11() const { return ____h_11; }
	inline uint8_t* get_address_of__h_11() { return &____h_11; }
	inline void set__h_11(uint8_t value)
	{
		____h_11 = value;
	}

	inline static int32_t get_offset_of__i_12() { return static_cast<int32_t>(offsetof(Guid_t, ____i_12)); }
	inline uint8_t get__i_12() const { return ____i_12; }
	inline uint8_t* get_address_of__i_12() { return &____i_12; }
	inline void set__i_12(uint8_t value)
	{
		____i_12 = value;
	}

	inline static int32_t get_offset_of__j_13() { return static_cast<int32_t>(offsetof(Guid_t, ____j_13)); }
	inline uint8_t get__j_13() const { return ____j_13; }
	inline uint8_t* get_address_of__j_13() { return &____j_13; }
	inline void set__j_13(uint8_t value)
	{
		____j_13 = value;
	}

	inline static int32_t get_offset_of__k_14() { return static_cast<int32_t>(offsetof(Guid_t, ____k_14)); }
	inline uint8_t get__k_14() const { return ____k_14; }
	inline uint8_t* get_address_of__k_14() { return &____k_14; }
	inline void set__k_14(uint8_t value)
	{
		____k_14 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_0;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_1;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_2;
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_3;

public:
	inline static int32_t get_offset_of__rngAccess_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_0)); }
	inline RuntimeObject * get__rngAccess_0() const { return ____rngAccess_0; }
	inline RuntimeObject ** get_address_of__rngAccess_0() { return &____rngAccess_0; }
	inline void set__rngAccess_0(RuntimeObject * value)
	{
		____rngAccess_0 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_0), value);
	}

	inline static int32_t get_offset_of__rng_1() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_1)); }
	inline RandomNumberGenerator_t386037858 * get__rng_1() const { return ____rng_1; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_1() { return &____rng_1; }
	inline void set__rng_1(RandomNumberGenerator_t386037858 * value)
	{
		____rng_1 = value;
		Il2CppCodeGenWriteBarrier((&____rng_1), value);
	}

	inline static int32_t get_offset_of__fastRng_2() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_2)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_2() const { return ____fastRng_2; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_2() { return &____fastRng_2; }
	inline void set__fastRng_2(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_2 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_2), value);
	}

	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_3)); }
	inline Guid_t  get_Empty_3() const { return ___Empty_3; }
	inline Guid_t * get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(Guid_t  value)
	{
		___Empty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef EVENTCACHEENTRY_T156445199_H
#define EVENTCACHEENTRY_T156445199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry
struct  EventCacheEntry_t156445199 
{
public:
	// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventRegistrationTokenListWithCount> System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry::registrationTable
	ConditionalWeakTable_2_t3044373657 * ___registrationTable_0;
	// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/TokenListCount System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry::tokenListCount
	TokenListCount_t1606756367 * ___tokenListCount_1;

public:
	inline static int32_t get_offset_of_registrationTable_0() { return static_cast<int32_t>(offsetof(EventCacheEntry_t156445199, ___registrationTable_0)); }
	inline ConditionalWeakTable_2_t3044373657 * get_registrationTable_0() const { return ___registrationTable_0; }
	inline ConditionalWeakTable_2_t3044373657 ** get_address_of_registrationTable_0() { return &___registrationTable_0; }
	inline void set_registrationTable_0(ConditionalWeakTable_2_t3044373657 * value)
	{
		___registrationTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___registrationTable_0), value);
	}

	inline static int32_t get_offset_of_tokenListCount_1() { return static_cast<int32_t>(offsetof(EventCacheEntry_t156445199, ___tokenListCount_1)); }
	inline TokenListCount_t1606756367 * get_tokenListCount_1() const { return ___tokenListCount_1; }
	inline TokenListCount_t1606756367 ** get_address_of_tokenListCount_1() { return &___tokenListCount_1; }
	inline void set_tokenListCount_1(TokenListCount_t1606756367 * value)
	{
		___tokenListCount_1 = value;
		Il2CppCodeGenWriteBarrier((&___tokenListCount_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry
struct EventCacheEntry_t156445199_marshaled_pinvoke
{
	ConditionalWeakTable_2_t3044373657 * ___registrationTable_0;
	TokenListCount_t1606756367 * ___tokenListCount_1;
};
// Native definition for COM marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry
struct EventCacheEntry_t156445199_marshaled_com
{
	ConditionalWeakTable_2_t3044373657 * ___registrationTable_0;
	TokenListCount_t1606756367 * ___tokenListCount_1;
};
#endif // EVENTCACHEENTRY_T156445199_H
#ifndef SURFACEOBJECT_T2613240318_H
#define SURFACEOBJECT_T2613240318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject
struct  SurfaceObject_t2613240318 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject::ID
	int32_t ___ID_0;
	// UnityEngine.GameObject HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject::Object
	GameObject_t1113636619 * ___Object_1;
	// UnityEngine.MeshRenderer HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject::Renderer
	MeshRenderer_t587009260 * ___Renderer_2;
	// UnityEngine.MeshFilter HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject::Filter
	MeshFilter_t3523625662 * ___Filter_3;
	// UnityEngine.MeshCollider HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject::Collider
	MeshCollider_t903564387 * ___Collider_4;

public:
	inline static int32_t get_offset_of_ID_0() { return static_cast<int32_t>(offsetof(SurfaceObject_t2613240318, ___ID_0)); }
	inline int32_t get_ID_0() const { return ___ID_0; }
	inline int32_t* get_address_of_ID_0() { return &___ID_0; }
	inline void set_ID_0(int32_t value)
	{
		___ID_0 = value;
	}

	inline static int32_t get_offset_of_Object_1() { return static_cast<int32_t>(offsetof(SurfaceObject_t2613240318, ___Object_1)); }
	inline GameObject_t1113636619 * get_Object_1() const { return ___Object_1; }
	inline GameObject_t1113636619 ** get_address_of_Object_1() { return &___Object_1; }
	inline void set_Object_1(GameObject_t1113636619 * value)
	{
		___Object_1 = value;
		Il2CppCodeGenWriteBarrier((&___Object_1), value);
	}

	inline static int32_t get_offset_of_Renderer_2() { return static_cast<int32_t>(offsetof(SurfaceObject_t2613240318, ___Renderer_2)); }
	inline MeshRenderer_t587009260 * get_Renderer_2() const { return ___Renderer_2; }
	inline MeshRenderer_t587009260 ** get_address_of_Renderer_2() { return &___Renderer_2; }
	inline void set_Renderer_2(MeshRenderer_t587009260 * value)
	{
		___Renderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Renderer_2), value);
	}

	inline static int32_t get_offset_of_Filter_3() { return static_cast<int32_t>(offsetof(SurfaceObject_t2613240318, ___Filter_3)); }
	inline MeshFilter_t3523625662 * get_Filter_3() const { return ___Filter_3; }
	inline MeshFilter_t3523625662 ** get_address_of_Filter_3() { return &___Filter_3; }
	inline void set_Filter_3(MeshFilter_t3523625662 * value)
	{
		___Filter_3 = value;
		Il2CppCodeGenWriteBarrier((&___Filter_3), value);
	}

	inline static int32_t get_offset_of_Collider_4() { return static_cast<int32_t>(offsetof(SurfaceObject_t2613240318, ___Collider_4)); }
	inline MeshCollider_t903564387 * get_Collider_4() const { return ___Collider_4; }
	inline MeshCollider_t903564387 ** get_address_of_Collider_4() { return &___Collider_4; }
	inline void set_Collider_4(MeshCollider_t903564387 * value)
	{
		___Collider_4 = value;
		Il2CppCodeGenWriteBarrier((&___Collider_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject
struct SurfaceObject_t2613240318_marshaled_pinvoke
{
	int32_t ___ID_0;
	GameObject_t1113636619 * ___Object_1;
	MeshRenderer_t587009260 * ___Renderer_2;
	MeshFilter_t3523625662 * ___Filter_3;
	MeshCollider_t903564387 * ___Collider_4;
};
// Native definition for COM marshalling of HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject
struct SurfaceObject_t2613240318_marshaled_com
{
	int32_t ___ID_0;
	GameObject_t1113636619 * ___Object_1;
	MeshRenderer_t587009260 * ___Renderer_2;
	MeshFilter_t3523625662 * ___Filter_3;
	MeshCollider_t903564387 * ___Collider_4;
};
#endif // SURFACEOBJECT_T2613240318_H
#ifndef ENTRY_T1462643140_H
#define ENTRY_T1462643140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Int64,System.Object>
struct  Entry_t1462643140 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	int64_t ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	RuntimeObject * ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t1462643140, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t1462643140, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t1462643140, ___key_2)); }
	inline int64_t get_key_2() const { return ___key_2; }
	inline int64_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int64_t value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t1462643140, ___value_3)); }
	inline RuntimeObject * get_value_3() const { return ___value_3; }
	inline RuntimeObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(RuntimeObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T1462643140_H
#ifndef ENTRY_T3583683983_H
#define ENTRY_T3583683983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>
struct  Entry_t3583683983 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	int32_t ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	RuntimeObject * ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t3583683983, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t3583683983, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t3583683983, ___key_2)); }
	inline int32_t get_key_2() const { return ___key_2; }
	inline int32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int32_t value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t3583683983, ___value_3)); }
	inline RuntimeObject * get_value_3() const { return ___value_3; }
	inline RuntimeObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(RuntimeObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3583683983_H
#ifndef TIMERDATA_T4056715490_H
#define TIMERDATA_T4056715490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TimerScheduler/TimerData
struct  TimerData_t4056715490 
{
public:
	// HoloToolkit.Unity.TimerScheduler/Callback HoloToolkit.Unity.TimerScheduler/TimerData::Callback
	Callback_t2663646540 * ___Callback_0;
	// System.Single HoloToolkit.Unity.TimerScheduler/TimerData::Duration
	float ___Duration_1;
	// System.Boolean HoloToolkit.Unity.TimerScheduler/TimerData::Loop
	bool ___Loop_2;
	// System.Int32 HoloToolkit.Unity.TimerScheduler/TimerData::Id
	int32_t ___Id_3;

public:
	inline static int32_t get_offset_of_Callback_0() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Callback_0)); }
	inline Callback_t2663646540 * get_Callback_0() const { return ___Callback_0; }
	inline Callback_t2663646540 ** get_address_of_Callback_0() { return &___Callback_0; }
	inline void set_Callback_0(Callback_t2663646540 * value)
	{
		___Callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___Callback_0), value);
	}

	inline static int32_t get_offset_of_Duration_1() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Duration_1)); }
	inline float get_Duration_1() const { return ___Duration_1; }
	inline float* get_address_of_Duration_1() { return &___Duration_1; }
	inline void set_Duration_1(float value)
	{
		___Duration_1 = value;
	}

	inline static int32_t get_offset_of_Loop_2() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Loop_2)); }
	inline bool get_Loop_2() const { return ___Loop_2; }
	inline bool* get_address_of_Loop_2() { return &___Loop_2; }
	inline void set_Loop_2(bool value)
	{
		___Loop_2 = value;
	}

	inline static int32_t get_offset_of_Id_3() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Id_3)); }
	inline int32_t get_Id_3() const { return ___Id_3; }
	inline int32_t* get_address_of_Id_3() { return &___Id_3; }
	inline void set_Id_3(int32_t value)
	{
		___Id_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.TimerScheduler/TimerData
struct TimerData_t4056715490_marshaled_pinvoke
{
	Il2CppMethodPointer ___Callback_0;
	float ___Duration_1;
	int32_t ___Loop_2;
	int32_t ___Id_3;
};
// Native definition for COM marshalling of HoloToolkit.Unity.TimerScheduler/TimerData
struct TimerData_t4056715490_marshaled_com
{
	Il2CppMethodPointer ___Callback_0;
	float ___Duration_1;
	int32_t ___Loop_2;
	int32_t ___Id_3;
};
#endif // TIMERDATA_T4056715490_H
#ifndef ENTRY_T3059558737_H
#define ENTRY_T3059558737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>
struct  Entry_t3059558737 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	RuntimeObject * ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	bool ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t3059558737, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t3059558737, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t3059558737, ___key_2)); }
	inline RuntimeObject * get_key_2() const { return ___key_2; }
	inline RuntimeObject ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(RuntimeObject * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t3059558737, ___value_3)); }
	inline bool get_value_3() const { return ___value_3; }
	inline bool* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(bool value)
	{
		___value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3059558737_H
#ifndef INPUTSOURCEINFO_T100027742_H
#define INPUTSOURCEINFO_T100027742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.InputSourceInfo
struct  InputSourceInfo_t100027742 
{
public:
	// HoloToolkit.Unity.InputModule.IInputSource HoloToolkit.Unity.InputModule.InputSourceInfo::InputSource
	RuntimeObject* ___InputSource_0;
	// System.UInt32 HoloToolkit.Unity.InputModule.InputSourceInfo::SourceId
	uint32_t ___SourceId_1;

public:
	inline static int32_t get_offset_of_InputSource_0() { return static_cast<int32_t>(offsetof(InputSourceInfo_t100027742, ___InputSource_0)); }
	inline RuntimeObject* get_InputSource_0() const { return ___InputSource_0; }
	inline RuntimeObject** get_address_of_InputSource_0() { return &___InputSource_0; }
	inline void set_InputSource_0(RuntimeObject* value)
	{
		___InputSource_0 = value;
		Il2CppCodeGenWriteBarrier((&___InputSource_0), value);
	}

	inline static int32_t get_offset_of_SourceId_1() { return static_cast<int32_t>(offsetof(InputSourceInfo_t100027742, ___SourceId_1)); }
	inline uint32_t get_SourceId_1() const { return ___SourceId_1; }
	inline uint32_t* get_address_of_SourceId_1() { return &___SourceId_1; }
	inline void set_SourceId_1(uint32_t value)
	{
		___SourceId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.InputModule.InputSourceInfo
struct InputSourceInfo_t100027742_marshaled_pinvoke
{
	RuntimeObject* ___InputSource_0;
	uint32_t ___SourceId_1;
};
// Native definition for COM marshalling of HoloToolkit.Unity.InputModule.InputSourceInfo
struct InputSourceInfo_t100027742_marshaled_com
{
	RuntimeObject* ___InputSource_0;
	uint32_t ___SourceId_1;
};
#endif // INPUTSOURCEINFO_T100027742_H
#ifndef XBOXCONTROLLERDATA_T1920221146_H
#define XBOXCONTROLLERDATA_T1920221146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.XboxControllerData
struct  XboxControllerData_t1920221146 
{
public:
	// System.String HoloToolkit.Unity.InputModule.XboxControllerData::<GamePadName>k__BackingField
	String_t* ___U3CGamePadNameU3Ek__BackingField_0;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftStickHorizontalAxis>k__BackingField
	float ___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftStickVerticalAxis>k__BackingField
	float ___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightStickHorizontalAxis>k__BackingField
	float ___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightStickVerticalAxis>k__BackingField
	float ___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxDpadHorizontalAxis>k__BackingField
	float ___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxDpadVerticalAxis>k__BackingField
	float ___U3CXboxDpadVerticalAxisU3Ek__BackingField_6;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftTriggerAxis>k__BackingField
	float ___U3CXboxLeftTriggerAxisU3Ek__BackingField_7;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightTriggerAxis>k__BackingField
	float ___U3CXboxRightTriggerAxisU3Ek__BackingField_8;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxSharedTriggerAxis>k__BackingField
	float ___U3CXboxSharedTriggerAxisU3Ek__BackingField_9;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxA_Pressed>k__BackingField
	bool ___U3CXboxA_PressedU3Ek__BackingField_10;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxB_Pressed>k__BackingField
	bool ___U3CXboxB_PressedU3Ek__BackingField_11;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxX_Pressed>k__BackingField
	bool ___U3CXboxX_PressedU3Ek__BackingField_12;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxY_Pressed>k__BackingField
	bool ___U3CXboxY_PressedU3Ek__BackingField_13;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftBumper_Pressed>k__BackingField
	bool ___U3CXboxLeftBumper_PressedU3Ek__BackingField_14;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightBumper_Pressed>k__BackingField
	bool ___U3CXboxRightBumper_PressedU3Ek__BackingField_15;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftStick_Pressed>k__BackingField
	bool ___U3CXboxLeftStick_PressedU3Ek__BackingField_16;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightStick_Pressed>k__BackingField
	bool ___U3CXboxRightStick_PressedU3Ek__BackingField_17;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxView_Pressed>k__BackingField
	bool ___U3CXboxView_PressedU3Ek__BackingField_18;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxMenu_Pressed>k__BackingField
	bool ___U3CXboxMenu_PressedU3Ek__BackingField_19;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxA_Up>k__BackingField
	bool ___U3CXboxA_UpU3Ek__BackingField_20;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxB_Up>k__BackingField
	bool ___U3CXboxB_UpU3Ek__BackingField_21;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxX_Up>k__BackingField
	bool ___U3CXboxX_UpU3Ek__BackingField_22;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxY_Up>k__BackingField
	bool ___U3CXboxY_UpU3Ek__BackingField_23;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftBumper_Up>k__BackingField
	bool ___U3CXboxLeftBumper_UpU3Ek__BackingField_24;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightBumper_Up>k__BackingField
	bool ___U3CXboxRightBumper_UpU3Ek__BackingField_25;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftStick_Up>k__BackingField
	bool ___U3CXboxLeftStick_UpU3Ek__BackingField_26;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightStick_Up>k__BackingField
	bool ___U3CXboxRightStick_UpU3Ek__BackingField_27;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxView_Up>k__BackingField
	bool ___U3CXboxView_UpU3Ek__BackingField_28;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxMenu_Up>k__BackingField
	bool ___U3CXboxMenu_UpU3Ek__BackingField_29;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxA_Down>k__BackingField
	bool ___U3CXboxA_DownU3Ek__BackingField_30;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxB_Down>k__BackingField
	bool ___U3CXboxB_DownU3Ek__BackingField_31;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxX_Down>k__BackingField
	bool ___U3CXboxX_DownU3Ek__BackingField_32;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxY_Down>k__BackingField
	bool ___U3CXboxY_DownU3Ek__BackingField_33;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftBumper_Down>k__BackingField
	bool ___U3CXboxLeftBumper_DownU3Ek__BackingField_34;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightBumper_Down>k__BackingField
	bool ___U3CXboxRightBumper_DownU3Ek__BackingField_35;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftStick_Down>k__BackingField
	bool ___U3CXboxLeftStick_DownU3Ek__BackingField_36;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightStick_Down>k__BackingField
	bool ___U3CXboxRightStick_DownU3Ek__BackingField_37;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxView_Down>k__BackingField
	bool ___U3CXboxView_DownU3Ek__BackingField_38;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxMenu_Down>k__BackingField
	bool ___U3CXboxMenu_DownU3Ek__BackingField_39;

public:
	inline static int32_t get_offset_of_U3CGamePadNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CGamePadNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CGamePadNameU3Ek__BackingField_0() const { return ___U3CGamePadNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CGamePadNameU3Ek__BackingField_0() { return &___U3CGamePadNameU3Ek__BackingField_0; }
	inline void set_U3CGamePadNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CGamePadNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGamePadNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1)); }
	inline float get_U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1() const { return ___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1; }
	inline float* get_address_of_U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1() { return &___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1; }
	inline void set_U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1(float value)
	{
		___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2)); }
	inline float get_U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2() const { return ___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2; }
	inline float* get_address_of_U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2() { return &___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2; }
	inline void set_U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2(float value)
	{
		___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3)); }
	inline float get_U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3() const { return ___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3; }
	inline float* get_address_of_U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3() { return &___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3; }
	inline void set_U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3(float value)
	{
		___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightStickVerticalAxisU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4)); }
	inline float get_U3CXboxRightStickVerticalAxisU3Ek__BackingField_4() const { return ___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4; }
	inline float* get_address_of_U3CXboxRightStickVerticalAxisU3Ek__BackingField_4() { return &___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4; }
	inline void set_U3CXboxRightStickVerticalAxisU3Ek__BackingField_4(float value)
	{
		___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CXboxDpadHorizontalAxisU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5)); }
	inline float get_U3CXboxDpadHorizontalAxisU3Ek__BackingField_5() const { return ___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5; }
	inline float* get_address_of_U3CXboxDpadHorizontalAxisU3Ek__BackingField_5() { return &___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5; }
	inline void set_U3CXboxDpadHorizontalAxisU3Ek__BackingField_5(float value)
	{
		___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CXboxDpadVerticalAxisU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxDpadVerticalAxisU3Ek__BackingField_6)); }
	inline float get_U3CXboxDpadVerticalAxisU3Ek__BackingField_6() const { return ___U3CXboxDpadVerticalAxisU3Ek__BackingField_6; }
	inline float* get_address_of_U3CXboxDpadVerticalAxisU3Ek__BackingField_6() { return &___U3CXboxDpadVerticalAxisU3Ek__BackingField_6; }
	inline void set_U3CXboxDpadVerticalAxisU3Ek__BackingField_6(float value)
	{
		___U3CXboxDpadVerticalAxisU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftTriggerAxisU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftTriggerAxisU3Ek__BackingField_7)); }
	inline float get_U3CXboxLeftTriggerAxisU3Ek__BackingField_7() const { return ___U3CXboxLeftTriggerAxisU3Ek__BackingField_7; }
	inline float* get_address_of_U3CXboxLeftTriggerAxisU3Ek__BackingField_7() { return &___U3CXboxLeftTriggerAxisU3Ek__BackingField_7; }
	inline void set_U3CXboxLeftTriggerAxisU3Ek__BackingField_7(float value)
	{
		___U3CXboxLeftTriggerAxisU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightTriggerAxisU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightTriggerAxisU3Ek__BackingField_8)); }
	inline float get_U3CXboxRightTriggerAxisU3Ek__BackingField_8() const { return ___U3CXboxRightTriggerAxisU3Ek__BackingField_8; }
	inline float* get_address_of_U3CXboxRightTriggerAxisU3Ek__BackingField_8() { return &___U3CXboxRightTriggerAxisU3Ek__BackingField_8; }
	inline void set_U3CXboxRightTriggerAxisU3Ek__BackingField_8(float value)
	{
		___U3CXboxRightTriggerAxisU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CXboxSharedTriggerAxisU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxSharedTriggerAxisU3Ek__BackingField_9)); }
	inline float get_U3CXboxSharedTriggerAxisU3Ek__BackingField_9() const { return ___U3CXboxSharedTriggerAxisU3Ek__BackingField_9; }
	inline float* get_address_of_U3CXboxSharedTriggerAxisU3Ek__BackingField_9() { return &___U3CXboxSharedTriggerAxisU3Ek__BackingField_9; }
	inline void set_U3CXboxSharedTriggerAxisU3Ek__BackingField_9(float value)
	{
		___U3CXboxSharedTriggerAxisU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CXboxA_PressedU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxA_PressedU3Ek__BackingField_10)); }
	inline bool get_U3CXboxA_PressedU3Ek__BackingField_10() const { return ___U3CXboxA_PressedU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CXboxA_PressedU3Ek__BackingField_10() { return &___U3CXboxA_PressedU3Ek__BackingField_10; }
	inline void set_U3CXboxA_PressedU3Ek__BackingField_10(bool value)
	{
		___U3CXboxA_PressedU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CXboxB_PressedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxB_PressedU3Ek__BackingField_11)); }
	inline bool get_U3CXboxB_PressedU3Ek__BackingField_11() const { return ___U3CXboxB_PressedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CXboxB_PressedU3Ek__BackingField_11() { return &___U3CXboxB_PressedU3Ek__BackingField_11; }
	inline void set_U3CXboxB_PressedU3Ek__BackingField_11(bool value)
	{
		___U3CXboxB_PressedU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CXboxX_PressedU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxX_PressedU3Ek__BackingField_12)); }
	inline bool get_U3CXboxX_PressedU3Ek__BackingField_12() const { return ___U3CXboxX_PressedU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CXboxX_PressedU3Ek__BackingField_12() { return &___U3CXboxX_PressedU3Ek__BackingField_12; }
	inline void set_U3CXboxX_PressedU3Ek__BackingField_12(bool value)
	{
		___U3CXboxX_PressedU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CXboxY_PressedU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxY_PressedU3Ek__BackingField_13)); }
	inline bool get_U3CXboxY_PressedU3Ek__BackingField_13() const { return ___U3CXboxY_PressedU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CXboxY_PressedU3Ek__BackingField_13() { return &___U3CXboxY_PressedU3Ek__BackingField_13; }
	inline void set_U3CXboxY_PressedU3Ek__BackingField_13(bool value)
	{
		___U3CXboxY_PressedU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftBumper_PressedU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftBumper_PressedU3Ek__BackingField_14)); }
	inline bool get_U3CXboxLeftBumper_PressedU3Ek__BackingField_14() const { return ___U3CXboxLeftBumper_PressedU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CXboxLeftBumper_PressedU3Ek__BackingField_14() { return &___U3CXboxLeftBumper_PressedU3Ek__BackingField_14; }
	inline void set_U3CXboxLeftBumper_PressedU3Ek__BackingField_14(bool value)
	{
		___U3CXboxLeftBumper_PressedU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightBumper_PressedU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightBumper_PressedU3Ek__BackingField_15)); }
	inline bool get_U3CXboxRightBumper_PressedU3Ek__BackingField_15() const { return ___U3CXboxRightBumper_PressedU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CXboxRightBumper_PressedU3Ek__BackingField_15() { return &___U3CXboxRightBumper_PressedU3Ek__BackingField_15; }
	inline void set_U3CXboxRightBumper_PressedU3Ek__BackingField_15(bool value)
	{
		___U3CXboxRightBumper_PressedU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftStick_PressedU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftStick_PressedU3Ek__BackingField_16)); }
	inline bool get_U3CXboxLeftStick_PressedU3Ek__BackingField_16() const { return ___U3CXboxLeftStick_PressedU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CXboxLeftStick_PressedU3Ek__BackingField_16() { return &___U3CXboxLeftStick_PressedU3Ek__BackingField_16; }
	inline void set_U3CXboxLeftStick_PressedU3Ek__BackingField_16(bool value)
	{
		___U3CXboxLeftStick_PressedU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightStick_PressedU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightStick_PressedU3Ek__BackingField_17)); }
	inline bool get_U3CXboxRightStick_PressedU3Ek__BackingField_17() const { return ___U3CXboxRightStick_PressedU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CXboxRightStick_PressedU3Ek__BackingField_17() { return &___U3CXboxRightStick_PressedU3Ek__BackingField_17; }
	inline void set_U3CXboxRightStick_PressedU3Ek__BackingField_17(bool value)
	{
		___U3CXboxRightStick_PressedU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CXboxView_PressedU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxView_PressedU3Ek__BackingField_18)); }
	inline bool get_U3CXboxView_PressedU3Ek__BackingField_18() const { return ___U3CXboxView_PressedU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CXboxView_PressedU3Ek__BackingField_18() { return &___U3CXboxView_PressedU3Ek__BackingField_18; }
	inline void set_U3CXboxView_PressedU3Ek__BackingField_18(bool value)
	{
		___U3CXboxView_PressedU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CXboxMenu_PressedU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxMenu_PressedU3Ek__BackingField_19)); }
	inline bool get_U3CXboxMenu_PressedU3Ek__BackingField_19() const { return ___U3CXboxMenu_PressedU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CXboxMenu_PressedU3Ek__BackingField_19() { return &___U3CXboxMenu_PressedU3Ek__BackingField_19; }
	inline void set_U3CXboxMenu_PressedU3Ek__BackingField_19(bool value)
	{
		___U3CXboxMenu_PressedU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CXboxA_UpU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxA_UpU3Ek__BackingField_20)); }
	inline bool get_U3CXboxA_UpU3Ek__BackingField_20() const { return ___U3CXboxA_UpU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CXboxA_UpU3Ek__BackingField_20() { return &___U3CXboxA_UpU3Ek__BackingField_20; }
	inline void set_U3CXboxA_UpU3Ek__BackingField_20(bool value)
	{
		___U3CXboxA_UpU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CXboxB_UpU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxB_UpU3Ek__BackingField_21)); }
	inline bool get_U3CXboxB_UpU3Ek__BackingField_21() const { return ___U3CXboxB_UpU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CXboxB_UpU3Ek__BackingField_21() { return &___U3CXboxB_UpU3Ek__BackingField_21; }
	inline void set_U3CXboxB_UpU3Ek__BackingField_21(bool value)
	{
		___U3CXboxB_UpU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CXboxX_UpU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxX_UpU3Ek__BackingField_22)); }
	inline bool get_U3CXboxX_UpU3Ek__BackingField_22() const { return ___U3CXboxX_UpU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CXboxX_UpU3Ek__BackingField_22() { return &___U3CXboxX_UpU3Ek__BackingField_22; }
	inline void set_U3CXboxX_UpU3Ek__BackingField_22(bool value)
	{
		___U3CXboxX_UpU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CXboxY_UpU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxY_UpU3Ek__BackingField_23)); }
	inline bool get_U3CXboxY_UpU3Ek__BackingField_23() const { return ___U3CXboxY_UpU3Ek__BackingField_23; }
	inline bool* get_address_of_U3CXboxY_UpU3Ek__BackingField_23() { return &___U3CXboxY_UpU3Ek__BackingField_23; }
	inline void set_U3CXboxY_UpU3Ek__BackingField_23(bool value)
	{
		___U3CXboxY_UpU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftBumper_UpU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftBumper_UpU3Ek__BackingField_24)); }
	inline bool get_U3CXboxLeftBumper_UpU3Ek__BackingField_24() const { return ___U3CXboxLeftBumper_UpU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CXboxLeftBumper_UpU3Ek__BackingField_24() { return &___U3CXboxLeftBumper_UpU3Ek__BackingField_24; }
	inline void set_U3CXboxLeftBumper_UpU3Ek__BackingField_24(bool value)
	{
		___U3CXboxLeftBumper_UpU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightBumper_UpU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightBumper_UpU3Ek__BackingField_25)); }
	inline bool get_U3CXboxRightBumper_UpU3Ek__BackingField_25() const { return ___U3CXboxRightBumper_UpU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CXboxRightBumper_UpU3Ek__BackingField_25() { return &___U3CXboxRightBumper_UpU3Ek__BackingField_25; }
	inline void set_U3CXboxRightBumper_UpU3Ek__BackingField_25(bool value)
	{
		___U3CXboxRightBumper_UpU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftStick_UpU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftStick_UpU3Ek__BackingField_26)); }
	inline bool get_U3CXboxLeftStick_UpU3Ek__BackingField_26() const { return ___U3CXboxLeftStick_UpU3Ek__BackingField_26; }
	inline bool* get_address_of_U3CXboxLeftStick_UpU3Ek__BackingField_26() { return &___U3CXboxLeftStick_UpU3Ek__BackingField_26; }
	inline void set_U3CXboxLeftStick_UpU3Ek__BackingField_26(bool value)
	{
		___U3CXboxLeftStick_UpU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightStick_UpU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightStick_UpU3Ek__BackingField_27)); }
	inline bool get_U3CXboxRightStick_UpU3Ek__BackingField_27() const { return ___U3CXboxRightStick_UpU3Ek__BackingField_27; }
	inline bool* get_address_of_U3CXboxRightStick_UpU3Ek__BackingField_27() { return &___U3CXboxRightStick_UpU3Ek__BackingField_27; }
	inline void set_U3CXboxRightStick_UpU3Ek__BackingField_27(bool value)
	{
		___U3CXboxRightStick_UpU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CXboxView_UpU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxView_UpU3Ek__BackingField_28)); }
	inline bool get_U3CXboxView_UpU3Ek__BackingField_28() const { return ___U3CXboxView_UpU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CXboxView_UpU3Ek__BackingField_28() { return &___U3CXboxView_UpU3Ek__BackingField_28; }
	inline void set_U3CXboxView_UpU3Ek__BackingField_28(bool value)
	{
		___U3CXboxView_UpU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CXboxMenu_UpU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxMenu_UpU3Ek__BackingField_29)); }
	inline bool get_U3CXboxMenu_UpU3Ek__BackingField_29() const { return ___U3CXboxMenu_UpU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CXboxMenu_UpU3Ek__BackingField_29() { return &___U3CXboxMenu_UpU3Ek__BackingField_29; }
	inline void set_U3CXboxMenu_UpU3Ek__BackingField_29(bool value)
	{
		___U3CXboxMenu_UpU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CXboxA_DownU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxA_DownU3Ek__BackingField_30)); }
	inline bool get_U3CXboxA_DownU3Ek__BackingField_30() const { return ___U3CXboxA_DownU3Ek__BackingField_30; }
	inline bool* get_address_of_U3CXboxA_DownU3Ek__BackingField_30() { return &___U3CXboxA_DownU3Ek__BackingField_30; }
	inline void set_U3CXboxA_DownU3Ek__BackingField_30(bool value)
	{
		___U3CXboxA_DownU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CXboxB_DownU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxB_DownU3Ek__BackingField_31)); }
	inline bool get_U3CXboxB_DownU3Ek__BackingField_31() const { return ___U3CXboxB_DownU3Ek__BackingField_31; }
	inline bool* get_address_of_U3CXboxB_DownU3Ek__BackingField_31() { return &___U3CXboxB_DownU3Ek__BackingField_31; }
	inline void set_U3CXboxB_DownU3Ek__BackingField_31(bool value)
	{
		___U3CXboxB_DownU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CXboxX_DownU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxX_DownU3Ek__BackingField_32)); }
	inline bool get_U3CXboxX_DownU3Ek__BackingField_32() const { return ___U3CXboxX_DownU3Ek__BackingField_32; }
	inline bool* get_address_of_U3CXboxX_DownU3Ek__BackingField_32() { return &___U3CXboxX_DownU3Ek__BackingField_32; }
	inline void set_U3CXboxX_DownU3Ek__BackingField_32(bool value)
	{
		___U3CXboxX_DownU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CXboxY_DownU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxY_DownU3Ek__BackingField_33)); }
	inline bool get_U3CXboxY_DownU3Ek__BackingField_33() const { return ___U3CXboxY_DownU3Ek__BackingField_33; }
	inline bool* get_address_of_U3CXboxY_DownU3Ek__BackingField_33() { return &___U3CXboxY_DownU3Ek__BackingField_33; }
	inline void set_U3CXboxY_DownU3Ek__BackingField_33(bool value)
	{
		___U3CXboxY_DownU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftBumper_DownU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftBumper_DownU3Ek__BackingField_34)); }
	inline bool get_U3CXboxLeftBumper_DownU3Ek__BackingField_34() const { return ___U3CXboxLeftBumper_DownU3Ek__BackingField_34; }
	inline bool* get_address_of_U3CXboxLeftBumper_DownU3Ek__BackingField_34() { return &___U3CXboxLeftBumper_DownU3Ek__BackingField_34; }
	inline void set_U3CXboxLeftBumper_DownU3Ek__BackingField_34(bool value)
	{
		___U3CXboxLeftBumper_DownU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightBumper_DownU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightBumper_DownU3Ek__BackingField_35)); }
	inline bool get_U3CXboxRightBumper_DownU3Ek__BackingField_35() const { return ___U3CXboxRightBumper_DownU3Ek__BackingField_35; }
	inline bool* get_address_of_U3CXboxRightBumper_DownU3Ek__BackingField_35() { return &___U3CXboxRightBumper_DownU3Ek__BackingField_35; }
	inline void set_U3CXboxRightBumper_DownU3Ek__BackingField_35(bool value)
	{
		___U3CXboxRightBumper_DownU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftStick_DownU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftStick_DownU3Ek__BackingField_36)); }
	inline bool get_U3CXboxLeftStick_DownU3Ek__BackingField_36() const { return ___U3CXboxLeftStick_DownU3Ek__BackingField_36; }
	inline bool* get_address_of_U3CXboxLeftStick_DownU3Ek__BackingField_36() { return &___U3CXboxLeftStick_DownU3Ek__BackingField_36; }
	inline void set_U3CXboxLeftStick_DownU3Ek__BackingField_36(bool value)
	{
		___U3CXboxLeftStick_DownU3Ek__BackingField_36 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightStick_DownU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightStick_DownU3Ek__BackingField_37)); }
	inline bool get_U3CXboxRightStick_DownU3Ek__BackingField_37() const { return ___U3CXboxRightStick_DownU3Ek__BackingField_37; }
	inline bool* get_address_of_U3CXboxRightStick_DownU3Ek__BackingField_37() { return &___U3CXboxRightStick_DownU3Ek__BackingField_37; }
	inline void set_U3CXboxRightStick_DownU3Ek__BackingField_37(bool value)
	{
		___U3CXboxRightStick_DownU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_U3CXboxView_DownU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxView_DownU3Ek__BackingField_38)); }
	inline bool get_U3CXboxView_DownU3Ek__BackingField_38() const { return ___U3CXboxView_DownU3Ek__BackingField_38; }
	inline bool* get_address_of_U3CXboxView_DownU3Ek__BackingField_38() { return &___U3CXboxView_DownU3Ek__BackingField_38; }
	inline void set_U3CXboxView_DownU3Ek__BackingField_38(bool value)
	{
		___U3CXboxView_DownU3Ek__BackingField_38 = value;
	}

	inline static int32_t get_offset_of_U3CXboxMenu_DownU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxMenu_DownU3Ek__BackingField_39)); }
	inline bool get_U3CXboxMenu_DownU3Ek__BackingField_39() const { return ___U3CXboxMenu_DownU3Ek__BackingField_39; }
	inline bool* get_address_of_U3CXboxMenu_DownU3Ek__BackingField_39() { return &___U3CXboxMenu_DownU3Ek__BackingField_39; }
	inline void set_U3CXboxMenu_DownU3Ek__BackingField_39(bool value)
	{
		___U3CXboxMenu_DownU3Ek__BackingField_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.InputModule.XboxControllerData
struct XboxControllerData_t1920221146_marshaled_pinvoke
{
	char* ___U3CGamePadNameU3Ek__BackingField_0;
	float ___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1;
	float ___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2;
	float ___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3;
	float ___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4;
	float ___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5;
	float ___U3CXboxDpadVerticalAxisU3Ek__BackingField_6;
	float ___U3CXboxLeftTriggerAxisU3Ek__BackingField_7;
	float ___U3CXboxRightTriggerAxisU3Ek__BackingField_8;
	float ___U3CXboxSharedTriggerAxisU3Ek__BackingField_9;
	int32_t ___U3CXboxA_PressedU3Ek__BackingField_10;
	int32_t ___U3CXboxB_PressedU3Ek__BackingField_11;
	int32_t ___U3CXboxX_PressedU3Ek__BackingField_12;
	int32_t ___U3CXboxY_PressedU3Ek__BackingField_13;
	int32_t ___U3CXboxLeftBumper_PressedU3Ek__BackingField_14;
	int32_t ___U3CXboxRightBumper_PressedU3Ek__BackingField_15;
	int32_t ___U3CXboxLeftStick_PressedU3Ek__BackingField_16;
	int32_t ___U3CXboxRightStick_PressedU3Ek__BackingField_17;
	int32_t ___U3CXboxView_PressedU3Ek__BackingField_18;
	int32_t ___U3CXboxMenu_PressedU3Ek__BackingField_19;
	int32_t ___U3CXboxA_UpU3Ek__BackingField_20;
	int32_t ___U3CXboxB_UpU3Ek__BackingField_21;
	int32_t ___U3CXboxX_UpU3Ek__BackingField_22;
	int32_t ___U3CXboxY_UpU3Ek__BackingField_23;
	int32_t ___U3CXboxLeftBumper_UpU3Ek__BackingField_24;
	int32_t ___U3CXboxRightBumper_UpU3Ek__BackingField_25;
	int32_t ___U3CXboxLeftStick_UpU3Ek__BackingField_26;
	int32_t ___U3CXboxRightStick_UpU3Ek__BackingField_27;
	int32_t ___U3CXboxView_UpU3Ek__BackingField_28;
	int32_t ___U3CXboxMenu_UpU3Ek__BackingField_29;
	int32_t ___U3CXboxA_DownU3Ek__BackingField_30;
	int32_t ___U3CXboxB_DownU3Ek__BackingField_31;
	int32_t ___U3CXboxX_DownU3Ek__BackingField_32;
	int32_t ___U3CXboxY_DownU3Ek__BackingField_33;
	int32_t ___U3CXboxLeftBumper_DownU3Ek__BackingField_34;
	int32_t ___U3CXboxRightBumper_DownU3Ek__BackingField_35;
	int32_t ___U3CXboxLeftStick_DownU3Ek__BackingField_36;
	int32_t ___U3CXboxRightStick_DownU3Ek__BackingField_37;
	int32_t ___U3CXboxView_DownU3Ek__BackingField_38;
	int32_t ___U3CXboxMenu_DownU3Ek__BackingField_39;
};
// Native definition for COM marshalling of HoloToolkit.Unity.InputModule.XboxControllerData
struct XboxControllerData_t1920221146_marshaled_com
{
	Il2CppChar* ___U3CGamePadNameU3Ek__BackingField_0;
	float ___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1;
	float ___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2;
	float ___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3;
	float ___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4;
	float ___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5;
	float ___U3CXboxDpadVerticalAxisU3Ek__BackingField_6;
	float ___U3CXboxLeftTriggerAxisU3Ek__BackingField_7;
	float ___U3CXboxRightTriggerAxisU3Ek__BackingField_8;
	float ___U3CXboxSharedTriggerAxisU3Ek__BackingField_9;
	int32_t ___U3CXboxA_PressedU3Ek__BackingField_10;
	int32_t ___U3CXboxB_PressedU3Ek__BackingField_11;
	int32_t ___U3CXboxX_PressedU3Ek__BackingField_12;
	int32_t ___U3CXboxY_PressedU3Ek__BackingField_13;
	int32_t ___U3CXboxLeftBumper_PressedU3Ek__BackingField_14;
	int32_t ___U3CXboxRightBumper_PressedU3Ek__BackingField_15;
	int32_t ___U3CXboxLeftStick_PressedU3Ek__BackingField_16;
	int32_t ___U3CXboxRightStick_PressedU3Ek__BackingField_17;
	int32_t ___U3CXboxView_PressedU3Ek__BackingField_18;
	int32_t ___U3CXboxMenu_PressedU3Ek__BackingField_19;
	int32_t ___U3CXboxA_UpU3Ek__BackingField_20;
	int32_t ___U3CXboxB_UpU3Ek__BackingField_21;
	int32_t ___U3CXboxX_UpU3Ek__BackingField_22;
	int32_t ___U3CXboxY_UpU3Ek__BackingField_23;
	int32_t ___U3CXboxLeftBumper_UpU3Ek__BackingField_24;
	int32_t ___U3CXboxRightBumper_UpU3Ek__BackingField_25;
	int32_t ___U3CXboxLeftStick_UpU3Ek__BackingField_26;
	int32_t ___U3CXboxRightStick_UpU3Ek__BackingField_27;
	int32_t ___U3CXboxView_UpU3Ek__BackingField_28;
	int32_t ___U3CXboxMenu_UpU3Ek__BackingField_29;
	int32_t ___U3CXboxA_DownU3Ek__BackingField_30;
	int32_t ___U3CXboxB_DownU3Ek__BackingField_31;
	int32_t ___U3CXboxX_DownU3Ek__BackingField_32;
	int32_t ___U3CXboxY_DownU3Ek__BackingField_33;
	int32_t ___U3CXboxLeftBumper_DownU3Ek__BackingField_34;
	int32_t ___U3CXboxRightBumper_DownU3Ek__BackingField_35;
	int32_t ___U3CXboxLeftStick_DownU3Ek__BackingField_36;
	int32_t ___U3CXboxRightStick_DownU3Ek__BackingField_37;
	int32_t ___U3CXboxView_DownU3Ek__BackingField_38;
	int32_t ___U3CXboxMenu_DownU3Ek__BackingField_39;
};
#endif // XBOXCONTROLLERDATA_T1920221146_H
#ifndef KEYWORDANDRESPONSE_T3411225000_H
#define KEYWORDANDRESPONSE_T3411225000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.SpeechInputHandler/KeywordAndResponse
struct  KeywordAndResponse_t3411225000 
{
public:
	// System.String HoloToolkit.Unity.InputModule.SpeechInputHandler/KeywordAndResponse::Keyword
	String_t* ___Keyword_0;
	// UnityEngine.Events.UnityEvent HoloToolkit.Unity.InputModule.SpeechInputHandler/KeywordAndResponse::Response
	UnityEvent_t2581268647 * ___Response_1;

public:
	inline static int32_t get_offset_of_Keyword_0() { return static_cast<int32_t>(offsetof(KeywordAndResponse_t3411225000, ___Keyword_0)); }
	inline String_t* get_Keyword_0() const { return ___Keyword_0; }
	inline String_t** get_address_of_Keyword_0() { return &___Keyword_0; }
	inline void set_Keyword_0(String_t* value)
	{
		___Keyword_0 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_0), value);
	}

	inline static int32_t get_offset_of_Response_1() { return static_cast<int32_t>(offsetof(KeywordAndResponse_t3411225000, ___Response_1)); }
	inline UnityEvent_t2581268647 * get_Response_1() const { return ___Response_1; }
	inline UnityEvent_t2581268647 ** get_address_of_Response_1() { return &___Response_1; }
	inline void set_Response_1(UnityEvent_t2581268647 * value)
	{
		___Response_1 = value;
		Il2CppCodeGenWriteBarrier((&___Response_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.InputModule.SpeechInputHandler/KeywordAndResponse
struct KeywordAndResponse_t3411225000_marshaled_pinvoke
{
	char* ___Keyword_0;
	UnityEvent_t2581268647 * ___Response_1;
};
// Native definition for COM marshalling of HoloToolkit.Unity.InputModule.SpeechInputHandler/KeywordAndResponse
struct KeywordAndResponse_t3411225000_marshaled_com
{
	Il2CppChar* ___Keyword_0;
	UnityEvent_t2581268647 * ___Response_1;
};
#endif // KEYWORDANDRESPONSE_T3411225000_H
#ifndef TIMERIDPAIR_T921323905_H
#define TIMERIDPAIR_T921323905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TimerScheduler/TimerIdPair
struct  TimerIdPair_t921323905 
{
public:
	// System.Int32 HoloToolkit.Unity.TimerScheduler/TimerIdPair::Id
	int32_t ___Id_0;
	// System.Int32 HoloToolkit.Unity.TimerScheduler/TimerIdPair::KeyTime
	int32_t ___KeyTime_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(TimerIdPair_t921323905, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_KeyTime_1() { return static_cast<int32_t>(offsetof(TimerIdPair_t921323905, ___KeyTime_1)); }
	inline int32_t get_KeyTime_1() const { return ___KeyTime_1; }
	inline int32_t* get_address_of_KeyTime_1() { return &___KeyTime_1; }
	inline void set_KeyTime_1(int32_t value)
	{
		___KeyTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERIDPAIR_T921323905_H
#ifndef XPATHNODEREF_T3498189018_H
#define XPATHNODEREF_T3498189018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodeRef
struct  XPathNodeRef_t3498189018 
{
public:
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeRef::page
	XPathNodeU5BU5D_t47339301* ___page_0;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeRef::idx
	int32_t ___idx_1;

public:
	inline static int32_t get_offset_of_page_0() { return static_cast<int32_t>(offsetof(XPathNodeRef_t3498189018, ___page_0)); }
	inline XPathNodeU5BU5D_t47339301* get_page_0() const { return ___page_0; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_page_0() { return &___page_0; }
	inline void set_page_0(XPathNodeU5BU5D_t47339301* value)
	{
		___page_0 = value;
		Il2CppCodeGenWriteBarrier((&___page_0), value);
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(XPathNodeRef_t3498189018, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MS.Internal.Xml.Cache.XPathNodeRef
struct XPathNodeRef_t3498189018_marshaled_pinvoke
{
	XPathNode_t2208072876_marshaled_pinvoke* ___page_0;
	int32_t ___idx_1;
};
// Native definition for COM marshalling of MS.Internal.Xml.Cache.XPathNodeRef
struct XPathNodeRef_t3498189018_marshaled_com
{
	XPathNode_t2208072876_marshaled_com* ___page_0;
	int32_t ___idx_1;
};
#endif // XPATHNODEREF_T3498189018_H
#ifndef ARRAYSEGMENT_1_T283560987_H
#define ARRAYSEGMENT_1_T283560987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArraySegment`1<System.Byte>
struct  ArraySegment_1_t283560987 
{
public:
	// T[] System.ArraySegment`1::_array
	ByteU5BU5D_t4116647657* ____array_0;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_1;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegment_1_t283560987, ____array_0)); }
	inline ByteU5BU5D_t4116647657* get__array_0() const { return ____array_0; }
	inline ByteU5BU5D_t4116647657** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ByteU5BU5D_t4116647657* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(ArraySegment_1_t283560987, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ArraySegment_1_t283560987, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYSEGMENT_1_T283560987_H
#ifndef TYPECONVERTKEY_T285306760_H
#define TYPECONVERTKEY_T285306760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct  TypeConvertKey_t285306760 
{
public:
	// System.Type Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey::_initialType
	Type_t * ____initialType_0;
	// System.Type Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey::_targetType
	Type_t * ____targetType_1;

public:
	inline static int32_t get_offset_of__initialType_0() { return static_cast<int32_t>(offsetof(TypeConvertKey_t285306760, ____initialType_0)); }
	inline Type_t * get__initialType_0() const { return ____initialType_0; }
	inline Type_t ** get_address_of__initialType_0() { return &____initialType_0; }
	inline void set__initialType_0(Type_t * value)
	{
		____initialType_0 = value;
		Il2CppCodeGenWriteBarrier((&____initialType_0), value);
	}

	inline static int32_t get_offset_of__targetType_1() { return static_cast<int32_t>(offsetof(TypeConvertKey_t285306760, ____targetType_1)); }
	inline Type_t * get__targetType_1() const { return ____targetType_1; }
	inline Type_t ** get_address_of__targetType_1() { return &____targetType_1; }
	inline void set__targetType_1(Type_t * value)
	{
		____targetType_1 = value;
		Il2CppCodeGenWriteBarrier((&____targetType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t285306760_marshaled_pinvoke
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t285306760_marshaled_com
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
#endif // TYPECONVERTKEY_T285306760_H
#ifndef TYPENAMEKEY_T2985541961_H
#define TYPENAMEKEY_T2985541961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey
struct  TypeNameKey_t2985541961 
{
public:
	// System.String Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::AssemblyName
	String_t* ___AssemblyName_0;
	// System.String Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::TypeName
	String_t* ___TypeName_1;

public:
	inline static int32_t get_offset_of_AssemblyName_0() { return static_cast<int32_t>(offsetof(TypeNameKey_t2985541961, ___AssemblyName_0)); }
	inline String_t* get_AssemblyName_0() const { return ___AssemblyName_0; }
	inline String_t** get_address_of_AssemblyName_0() { return &___AssemblyName_0; }
	inline void set_AssemblyName_0(String_t* value)
	{
		___AssemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___AssemblyName_0), value);
	}

	inline static int32_t get_offset_of_TypeName_1() { return static_cast<int32_t>(offsetof(TypeNameKey_t2985541961, ___TypeName_1)); }
	inline String_t* get_TypeName_1() const { return ___TypeName_1; }
	inline String_t** get_address_of_TypeName_1() { return &___TypeName_1; }
	inline void set_TypeName_1(String_t* value)
	{
		___TypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey
struct TypeNameKey_t2985541961_marshaled_pinvoke
{
	char* ___AssemblyName_0;
	char* ___TypeName_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey
struct TypeNameKey_t2985541961_marshaled_com
{
	Il2CppChar* ___AssemblyName_0;
	Il2CppChar* ___TypeName_1;
};
#endif // TYPENAMEKEY_T2985541961_H
#ifndef DICTIONARYENTRY_T3123975638_H
#define DICTIONARYENTRY_T3123975638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.DictionaryEntry
struct  DictionaryEntry_t3123975638 
{
public:
	// System.Object System.Collections.DictionaryEntry::_key
	RuntimeObject * ____key_0;
	// System.Object System.Collections.DictionaryEntry::_value
	RuntimeObject * ____value_1;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(DictionaryEntry_t3123975638, ____key_0)); }
	inline RuntimeObject * get__key_0() const { return ____key_0; }
	inline RuntimeObject ** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(RuntimeObject * value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier((&____key_0), value);
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(DictionaryEntry_t3123975638, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Collections.DictionaryEntry
struct DictionaryEntry_t3123975638_marshaled_pinvoke
{
	Il2CppIUnknown* ____key_0;
	Il2CppIUnknown* ____value_1;
};
// Native definition for COM marshalling of System.Collections.DictionaryEntry
struct DictionaryEntry_t3123975638_marshaled_com
{
	Il2CppIUnknown* ____key_0;
	Il2CppIUnknown* ____value_1;
};
#endif // DICTIONARYENTRY_T3123975638_H
#ifndef TABLERANGE_T3332867892_H
#define TABLERANGE_T3332867892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Globalization.Unicode.CodePointIndexer/TableRange
struct  TableRange_t3332867892 
{
public:
	// System.Int32 Mono.Globalization.Unicode.CodePointIndexer/TableRange::Start
	int32_t ___Start_0;
	// System.Int32 Mono.Globalization.Unicode.CodePointIndexer/TableRange::End
	int32_t ___End_1;
	// System.Int32 Mono.Globalization.Unicode.CodePointIndexer/TableRange::Count
	int32_t ___Count_2;
	// System.Int32 Mono.Globalization.Unicode.CodePointIndexer/TableRange::IndexStart
	int32_t ___IndexStart_3;
	// System.Int32 Mono.Globalization.Unicode.CodePointIndexer/TableRange::IndexEnd
	int32_t ___IndexEnd_4;

public:
	inline static int32_t get_offset_of_Start_0() { return static_cast<int32_t>(offsetof(TableRange_t3332867892, ___Start_0)); }
	inline int32_t get_Start_0() const { return ___Start_0; }
	inline int32_t* get_address_of_Start_0() { return &___Start_0; }
	inline void set_Start_0(int32_t value)
	{
		___Start_0 = value;
	}

	inline static int32_t get_offset_of_End_1() { return static_cast<int32_t>(offsetof(TableRange_t3332867892, ___End_1)); }
	inline int32_t get_End_1() const { return ___End_1; }
	inline int32_t* get_address_of_End_1() { return &___End_1; }
	inline void set_End_1(int32_t value)
	{
		___End_1 = value;
	}

	inline static int32_t get_offset_of_Count_2() { return static_cast<int32_t>(offsetof(TableRange_t3332867892, ___Count_2)); }
	inline int32_t get_Count_2() const { return ___Count_2; }
	inline int32_t* get_address_of_Count_2() { return &___Count_2; }
	inline void set_Count_2(int32_t value)
	{
		___Count_2 = value;
	}

	inline static int32_t get_offset_of_IndexStart_3() { return static_cast<int32_t>(offsetof(TableRange_t3332867892, ___IndexStart_3)); }
	inline int32_t get_IndexStart_3() const { return ___IndexStart_3; }
	inline int32_t* get_address_of_IndexStart_3() { return &___IndexStart_3; }
	inline void set_IndexStart_3(int32_t value)
	{
		___IndexStart_3 = value;
	}

	inline static int32_t get_offset_of_IndexEnd_4() { return static_cast<int32_t>(offsetof(TableRange_t3332867892, ___IndexEnd_4)); }
	inline int32_t get_IndexEnd_4() const { return ___IndexEnd_4; }
	inline int32_t* get_address_of_IndexEnd_4() { return &___IndexEnd_4; }
	inline void set_IndexEnd_4(int32_t value)
	{
		___IndexEnd_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLERANGE_T3332867892_H
#ifndef XPATHNODE_T2208072876_H
#define XPATHNODE_T2208072876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNode
struct  XPathNode_t2208072876 
{
public:
	// MS.Internal.Xml.Cache.XPathNodeInfoAtom MS.Internal.Xml.Cache.XPathNode::info
	XPathNodeInfoAtom_t1760358141 * ___info_0;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::idxSibling
	uint16_t ___idxSibling_1;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::idxParent
	uint16_t ___idxParent_2;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::idxSimilar
	uint16_t ___idxSimilar_3;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::posOffset
	uint16_t ___posOffset_4;
	// System.UInt32 MS.Internal.Xml.Cache.XPathNode::props
	uint32_t ___props_5;
	// System.String MS.Internal.Xml.Cache.XPathNode::value
	String_t* ___value_6;

public:
	inline static int32_t get_offset_of_info_0() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___info_0)); }
	inline XPathNodeInfoAtom_t1760358141 * get_info_0() const { return ___info_0; }
	inline XPathNodeInfoAtom_t1760358141 ** get_address_of_info_0() { return &___info_0; }
	inline void set_info_0(XPathNodeInfoAtom_t1760358141 * value)
	{
		___info_0 = value;
		Il2CppCodeGenWriteBarrier((&___info_0), value);
	}

	inline static int32_t get_offset_of_idxSibling_1() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___idxSibling_1)); }
	inline uint16_t get_idxSibling_1() const { return ___idxSibling_1; }
	inline uint16_t* get_address_of_idxSibling_1() { return &___idxSibling_1; }
	inline void set_idxSibling_1(uint16_t value)
	{
		___idxSibling_1 = value;
	}

	inline static int32_t get_offset_of_idxParent_2() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___idxParent_2)); }
	inline uint16_t get_idxParent_2() const { return ___idxParent_2; }
	inline uint16_t* get_address_of_idxParent_2() { return &___idxParent_2; }
	inline void set_idxParent_2(uint16_t value)
	{
		___idxParent_2 = value;
	}

	inline static int32_t get_offset_of_idxSimilar_3() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___idxSimilar_3)); }
	inline uint16_t get_idxSimilar_3() const { return ___idxSimilar_3; }
	inline uint16_t* get_address_of_idxSimilar_3() { return &___idxSimilar_3; }
	inline void set_idxSimilar_3(uint16_t value)
	{
		___idxSimilar_3 = value;
	}

	inline static int32_t get_offset_of_posOffset_4() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___posOffset_4)); }
	inline uint16_t get_posOffset_4() const { return ___posOffset_4; }
	inline uint16_t* get_address_of_posOffset_4() { return &___posOffset_4; }
	inline void set_posOffset_4(uint16_t value)
	{
		___posOffset_4 = value;
	}

	inline static int32_t get_offset_of_props_5() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___props_5)); }
	inline uint32_t get_props_5() const { return ___props_5; }
	inline uint32_t* get_address_of_props_5() { return &___props_5; }
	inline void set_props_5(uint32_t value)
	{
		___props_5 = value;
	}

	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___value_6)); }
	inline String_t* get_value_6() const { return ___value_6; }
	inline String_t** get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(String_t* value)
	{
		___value_6 = value;
		Il2CppCodeGenWriteBarrier((&___value_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MS.Internal.Xml.Cache.XPathNode
struct XPathNode_t2208072876_marshaled_pinvoke
{
	XPathNodeInfoAtom_t1760358141 * ___info_0;
	uint16_t ___idxSibling_1;
	uint16_t ___idxParent_2;
	uint16_t ___idxSimilar_3;
	uint16_t ___posOffset_4;
	uint32_t ___props_5;
	char* ___value_6;
};
// Native definition for COM marshalling of MS.Internal.Xml.Cache.XPathNode
struct XPathNode_t2208072876_marshaled_com
{
	XPathNodeInfoAtom_t1760358141 * ___info_0;
	uint16_t ___idxSibling_1;
	uint16_t ___idxParent_2;
	uint16_t ___idxSimilar_3;
	uint16_t ___posOffset_4;
	uint32_t ___props_5;
	Il2CppChar* ___value_6;
};
#endif // XPATHNODE_T2208072876_H
#ifndef URISCHEME_T2867806342_H
#define URISCHEME_T2867806342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Uri/UriScheme
struct  UriScheme_t2867806342 
{
public:
	// System.String Mono.Security.Uri/UriScheme::scheme
	String_t* ___scheme_0;
	// System.String Mono.Security.Uri/UriScheme::delimiter
	String_t* ___delimiter_1;
	// System.Int32 Mono.Security.Uri/UriScheme::defaultPort
	int32_t ___defaultPort_2;

public:
	inline static int32_t get_offset_of_scheme_0() { return static_cast<int32_t>(offsetof(UriScheme_t2867806342, ___scheme_0)); }
	inline String_t* get_scheme_0() const { return ___scheme_0; }
	inline String_t** get_address_of_scheme_0() { return &___scheme_0; }
	inline void set_scheme_0(String_t* value)
	{
		___scheme_0 = value;
		Il2CppCodeGenWriteBarrier((&___scheme_0), value);
	}

	inline static int32_t get_offset_of_delimiter_1() { return static_cast<int32_t>(offsetof(UriScheme_t2867806342, ___delimiter_1)); }
	inline String_t* get_delimiter_1() const { return ___delimiter_1; }
	inline String_t** get_address_of_delimiter_1() { return &___delimiter_1; }
	inline void set_delimiter_1(String_t* value)
	{
		___delimiter_1 = value;
		Il2CppCodeGenWriteBarrier((&___delimiter_1), value);
	}

	inline static int32_t get_offset_of_defaultPort_2() { return static_cast<int32_t>(offsetof(UriScheme_t2867806342, ___defaultPort_2)); }
	inline int32_t get_defaultPort_2() const { return ___defaultPort_2; }
	inline int32_t* get_address_of_defaultPort_2() { return &___defaultPort_2; }
	inline void set_defaultPort_2(int32_t value)
	{
		___defaultPort_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.Security.Uri/UriScheme
struct UriScheme_t2867806342_marshaled_pinvoke
{
	char* ___scheme_0;
	char* ___delimiter_1;
	int32_t ___defaultPort_2;
};
// Native definition for COM marshalling of Mono.Security.Uri/UriScheme
struct UriScheme_t2867806342_marshaled_com
{
	Il2CppChar* ___scheme_0;
	Il2CppChar* ___delimiter_1;
	int32_t ___defaultPort_2;
};
#endif // URISCHEME_T2867806342_H
#ifndef PREFABTODATAMODEL_T1762303220_H
#define PREFABTODATAMODEL_T1762303220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Spawning.PrefabToDataModel
struct  PrefabToDataModel_t1762303220 
{
public:
	// System.String HoloToolkit.Sharing.Spawning.PrefabToDataModel::DataModelClassName
	String_t* ___DataModelClassName_0;
	// UnityEngine.GameObject HoloToolkit.Sharing.Spawning.PrefabToDataModel::Prefab
	GameObject_t1113636619 * ___Prefab_1;

public:
	inline static int32_t get_offset_of_DataModelClassName_0() { return static_cast<int32_t>(offsetof(PrefabToDataModel_t1762303220, ___DataModelClassName_0)); }
	inline String_t* get_DataModelClassName_0() const { return ___DataModelClassName_0; }
	inline String_t** get_address_of_DataModelClassName_0() { return &___DataModelClassName_0; }
	inline void set_DataModelClassName_0(String_t* value)
	{
		___DataModelClassName_0 = value;
		Il2CppCodeGenWriteBarrier((&___DataModelClassName_0), value);
	}

	inline static int32_t get_offset_of_Prefab_1() { return static_cast<int32_t>(offsetof(PrefabToDataModel_t1762303220, ___Prefab_1)); }
	inline GameObject_t1113636619 * get_Prefab_1() const { return ___Prefab_1; }
	inline GameObject_t1113636619 ** get_address_of_Prefab_1() { return &___Prefab_1; }
	inline void set_Prefab_1(GameObject_t1113636619 * value)
	{
		___Prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Sharing.Spawning.PrefabToDataModel
struct PrefabToDataModel_t1762303220_marshaled_pinvoke
{
	char* ___DataModelClassName_0;
	GameObject_t1113636619 * ___Prefab_1;
};
// Native definition for COM marshalling of HoloToolkit.Sharing.Spawning.PrefabToDataModel
struct PrefabToDataModel_t1762303220_marshaled_com
{
	Il2CppChar* ___DataModelClassName_0;
	GameObject_t1113636619 * ___Prefab_1;
};
#endif // PREFABTODATAMODEL_T1762303220_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef NULLABLE_1_T378540539_H
#define NULLABLE_1_T378540539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t378540539 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T378540539_H
#ifndef POINT_T4164953539_H
#define POINT_T4164953539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Point
struct  Point_t4164953539 
{
public:
	// System.Single Windows.Foundation.Point::_x
	float ____x_0;
	// System.Single Windows.Foundation.Point::_y
	float ____y_1;

public:
	inline static int32_t get_offset_of__x_0() { return static_cast<int32_t>(offsetof(Point_t4164953539, ____x_0)); }
	inline float get__x_0() const { return ____x_0; }
	inline float* get_address_of__x_0() { return &____x_0; }
	inline void set__x_0(float value)
	{
		____x_0 = value;
	}

	inline static int32_t get_offset_of__y_1() { return static_cast<int32_t>(offsetof(Point_t4164953539, ____y_1)); }
	inline float get__y_1() const { return ____y_1; }
	inline float* get_address_of__y_1() { return &____y_1; }
	inline void set__y_1(float value)
	{
		____y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT_T4164953539_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t1436737249
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR3_T4136528132_H
#define VECTOR3_T4136528132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Vector3
struct  Vector3_t4136528132 
{
public:
	// System.Single GLTF.Math.Vector3::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_2;
	// System.Single GLTF.Math.Vector3::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_3;
	// System.Single GLTF.Math.Vector3::<Z>k__BackingField
	float ___U3CZU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vector3_t4136528132, ___U3CXU3Ek__BackingField_2)); }
	inline float get_U3CXU3Ek__BackingField_2() const { return ___U3CXU3Ek__BackingField_2; }
	inline float* get_address_of_U3CXU3Ek__BackingField_2() { return &___U3CXU3Ek__BackingField_2; }
	inline void set_U3CXU3Ek__BackingField_2(float value)
	{
		___U3CXU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Vector3_t4136528132, ___U3CYU3Ek__BackingField_3)); }
	inline float get_U3CYU3Ek__BackingField_3() const { return ___U3CYU3Ek__BackingField_3; }
	inline float* get_address_of_U3CYU3Ek__BackingField_3() { return &___U3CYU3Ek__BackingField_3; }
	inline void set_U3CYU3Ek__BackingField_3(float value)
	{
		___U3CYU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CZU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Vector3_t4136528132, ___U3CZU3Ek__BackingField_4)); }
	inline float get_U3CZU3Ek__BackingField_4() const { return ___U3CZU3Ek__BackingField_4; }
	inline float* get_address_of_U3CZU3Ek__BackingField_4() { return &___U3CZU3Ek__BackingField_4; }
	inline void set_U3CZU3Ek__BackingField_4(float value)
	{
		___U3CZU3Ek__BackingField_4 = value;
	}
};

struct Vector3_t4136528132_StaticFields
{
public:
	// GLTF.Math.Vector3 GLTF.Math.Vector3::Zero
	Vector3_t4136528132  ___Zero_0;
	// GLTF.Math.Vector3 GLTF.Math.Vector3::One
	Vector3_t4136528132  ___One_1;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(Vector3_t4136528132_StaticFields, ___Zero_0)); }
	inline Vector3_t4136528132  get_Zero_0() const { return ___Zero_0; }
	inline Vector3_t4136528132 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Vector3_t4136528132  value)
	{
		___Zero_0 = value;
	}

	inline static int32_t get_offset_of_One_1() { return static_cast<int32_t>(offsetof(Vector3_t4136528132_StaticFields, ___One_1)); }
	inline Vector3_t4136528132  get_One_1() const { return ___One_1; }
	inline Vector3_t4136528132 * get_address_of_One_1() { return &___One_1; }
	inline void set_One_1(Vector3_t4136528132  value)
	{
		___One_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T4136528132_H
#ifndef ENTRY_T1747409640_H
#define ENTRY_T1747409640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>
struct  Entry_t1747409640 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	RuntimeObject * ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	RuntimeObject * ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t1747409640, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t1747409640, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t1747409640, ___key_2)); }
	inline RuntimeObject * get_key_2() const { return ___key_2; }
	inline RuntimeObject ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(RuntimeObject * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t1747409640, ___value_3)); }
	inline RuntimeObject * get_value_3() const { return ___value_3; }
	inline RuntimeObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(RuntimeObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T1747409640_H
#ifndef ENTRY_T1618249229_H
#define ENTRY_T1618249229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>
struct  Entry_t1618249229 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	RuntimeObject * ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	int32_t ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t1618249229, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t1618249229, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t1618249229, ___key_2)); }
	inline RuntimeObject * get_key_2() const { return ___key_2; }
	inline RuntimeObject ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(RuntimeObject * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t1618249229, ___value_3)); }
	inline int32_t get_value_3() const { return ___value_3; }
	inline int32_t* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(int32_t value)
	{
		___value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T1618249229_H
#ifndef VECTOR4_T4136528135_H
#define VECTOR4_T4136528135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Vector4
struct  Vector4_t4136528135 
{
public:
	// System.Single GLTF.Math.Vector4::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_0;
	// System.Single GLTF.Math.Vector4::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_1;
	// System.Single GLTF.Math.Vector4::<Z>k__BackingField
	float ___U3CZU3Ek__BackingField_2;
	// System.Single GLTF.Math.Vector4::<W>k__BackingField
	float ___U3CWU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Vector4_t4136528135, ___U3CXU3Ek__BackingField_0)); }
	inline float get_U3CXU3Ek__BackingField_0() const { return ___U3CXU3Ek__BackingField_0; }
	inline float* get_address_of_U3CXU3Ek__BackingField_0() { return &___U3CXU3Ek__BackingField_0; }
	inline void set_U3CXU3Ek__BackingField_0(float value)
	{
		___U3CXU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Vector4_t4136528135, ___U3CYU3Ek__BackingField_1)); }
	inline float get_U3CYU3Ek__BackingField_1() const { return ___U3CYU3Ek__BackingField_1; }
	inline float* get_address_of_U3CYU3Ek__BackingField_1() { return &___U3CYU3Ek__BackingField_1; }
	inline void set_U3CYU3Ek__BackingField_1(float value)
	{
		___U3CYU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CZU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vector4_t4136528135, ___U3CZU3Ek__BackingField_2)); }
	inline float get_U3CZU3Ek__BackingField_2() const { return ___U3CZU3Ek__BackingField_2; }
	inline float* get_address_of_U3CZU3Ek__BackingField_2() { return &___U3CZU3Ek__BackingField_2; }
	inline void set_U3CZU3Ek__BackingField_2(float value)
	{
		___U3CZU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CWU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Vector4_t4136528135, ___U3CWU3Ek__BackingField_3)); }
	inline float get_U3CWU3Ek__BackingField_3() const { return ___U3CWU3Ek__BackingField_3; }
	inline float* get_address_of_U3CWU3Ek__BackingField_3() { return &___U3CWU3Ek__BackingField_3; }
	inline void set_U3CWU3Ek__BackingField_3(float value)
	{
		___U3CWU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T4136528135_H
#ifndef SIZE_T550917638_H
#define SIZE_T550917638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Size
struct  Size_t550917638 
{
public:
	// System.Single Windows.Foundation.Size::_width
	float ____width_0;
	// System.Single Windows.Foundation.Size::_height
	float ____height_1;

public:
	inline static int32_t get_offset_of__width_0() { return static_cast<int32_t>(offsetof(Size_t550917638, ____width_0)); }
	inline float get__width_0() const { return ____width_0; }
	inline float* get_address_of__width_0() { return &____width_0; }
	inline void set__width_0(float value)
	{
		____width_0 = value;
	}

	inline static int32_t get_offset_of__height_1() { return static_cast<int32_t>(offsetof(Size_t550917638, ____height_1)); }
	inline float get__height_1() const { return ____height_1; }
	inline float* get_address_of__height_1() { return &____height_1; }
	inline void set__height_1(float value)
	{
		____height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIZE_T550917638_H
#ifndef RECT_T2695113487_H
#define RECT_T2695113487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Rect
struct  Rect_t2695113487 
{
public:
	// System.Single Windows.Foundation.Rect::_x
	float ____x_0;
	// System.Single Windows.Foundation.Rect::_y
	float ____y_1;
	// System.Single Windows.Foundation.Rect::_width
	float ____width_2;
	// System.Single Windows.Foundation.Rect::_height
	float ____height_3;

public:
	inline static int32_t get_offset_of__x_0() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____x_0)); }
	inline float get__x_0() const { return ____x_0; }
	inline float* get_address_of__x_0() { return &____x_0; }
	inline void set__x_0(float value)
	{
		____x_0 = value;
	}

	inline static int32_t get_offset_of__y_1() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____y_1)); }
	inline float get__y_1() const { return ____y_1; }
	inline float* get_address_of__y_1() { return &____y_1; }
	inline void set__y_1(float value)
	{
		____y_1 = value;
	}

	inline static int32_t get_offset_of__width_2() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____width_2)); }
	inline float get__width_2() const { return ____width_2; }
	inline float* get_address_of__width_2() { return &____width_2; }
	inline void set__width_2(float value)
	{
		____width_2 = value;
	}

	inline static int32_t get_offset_of__height_3() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____height_3)); }
	inline float get__height_3() const { return ____height_3; }
	inline float* get_address_of__height_3() { return &____height_3; }
	inline void set__height_3(float value)
	{
		____height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2695113487_H
#ifndef VECTOR2_T4136528133_H
#define VECTOR2_T4136528133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Vector2
struct  Vector2_t4136528133 
{
public:
	// System.Single GLTF.Math.Vector2::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_0;
	// System.Single GLTF.Math.Vector2::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Vector2_t4136528133, ___U3CXU3Ek__BackingField_0)); }
	inline float get_U3CXU3Ek__BackingField_0() const { return ___U3CXU3Ek__BackingField_0; }
	inline float* get_address_of_U3CXU3Ek__BackingField_0() { return &___U3CXU3Ek__BackingField_0; }
	inline void set_U3CXU3Ek__BackingField_0(float value)
	{
		___U3CXU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Vector2_t4136528133, ___U3CYU3Ek__BackingField_1)); }
	inline float get_U3CYU3Ek__BackingField_1() const { return ___U3CYU3Ek__BackingField_1; }
	inline float* get_address_of_U3CYU3Ek__BackingField_1() { return &___U3CYU3Ek__BackingField_1; }
	inline void set_U3CYU3Ek__BackingField_1(float value)
	{
		___U3CYU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T4136528133_H
#ifndef COLOR_T1637600301_H
#define COLOR_T1637600301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Color
struct  Color_t1637600301 
{
public:
	// System.Single GLTF.Math.Color::<R>k__BackingField
	float ___U3CRU3Ek__BackingField_0;
	// System.Single GLTF.Math.Color::<G>k__BackingField
	float ___U3CGU3Ek__BackingField_1;
	// System.Single GLTF.Math.Color::<B>k__BackingField
	float ___U3CBU3Ek__BackingField_2;
	// System.Single GLTF.Math.Color::<A>k__BackingField
	float ___U3CAU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CRU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Color_t1637600301, ___U3CRU3Ek__BackingField_0)); }
	inline float get_U3CRU3Ek__BackingField_0() const { return ___U3CRU3Ek__BackingField_0; }
	inline float* get_address_of_U3CRU3Ek__BackingField_0() { return &___U3CRU3Ek__BackingField_0; }
	inline void set_U3CRU3Ek__BackingField_0(float value)
	{
		___U3CRU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CGU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Color_t1637600301, ___U3CGU3Ek__BackingField_1)); }
	inline float get_U3CGU3Ek__BackingField_1() const { return ___U3CGU3Ek__BackingField_1; }
	inline float* get_address_of_U3CGU3Ek__BackingField_1() { return &___U3CGU3Ek__BackingField_1; }
	inline void set_U3CGU3Ek__BackingField_1(float value)
	{
		___U3CGU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CBU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Color_t1637600301, ___U3CBU3Ek__BackingField_2)); }
	inline float get_U3CBU3Ek__BackingField_2() const { return ___U3CBU3Ek__BackingField_2; }
	inline float* get_address_of_U3CBU3Ek__BackingField_2() { return &___U3CBU3Ek__BackingField_2; }
	inline void set_U3CBU3Ek__BackingField_2(float value)
	{
		___U3CBU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CAU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Color_t1637600301, ___U3CAU3Ek__BackingField_3)); }
	inline float get_U3CAU3Ek__BackingField_3() const { return ___U3CAU3Ek__BackingField_3; }
	inline float* get_address_of_U3CAU3Ek__BackingField_3() { return &___U3CAU3Ek__BackingField_3; }
	inline void set_U3CAU3Ek__BackingField_3(float value)
	{
		___U3CAU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T1637600301_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef EVENTREGISTRATIONTOKEN_T318890788_H
#define EVENTREGISTRATIONTOKEN_T318890788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken
struct  EventRegistrationToken_t318890788 
{
public:
	// System.UInt64 System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(EventRegistrationToken_t318890788, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTREGISTRATIONTOKEN_T318890788_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef RAYSTEP_T2147652507_H
#define RAYSTEP_T2147652507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RayStep
struct  RayStep_t2147652507 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.RayStep::<Origin>k__BackingField
	Vector3_t3722313464  ___U3COriginU3Ek__BackingField_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.RayStep::<Terminus>k__BackingField
	Vector3_t3722313464  ___U3CTerminusU3Ek__BackingField_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.RayStep::<Direction>k__BackingField
	Vector3_t3722313464  ___U3CDirectionU3Ek__BackingField_2;
	// System.Single HoloToolkit.Unity.RayStep::<Length>k__BackingField
	float ___U3CLengthU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3COriginU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RayStep_t2147652507, ___U3COriginU3Ek__BackingField_0)); }
	inline Vector3_t3722313464  get_U3COriginU3Ek__BackingField_0() const { return ___U3COriginU3Ek__BackingField_0; }
	inline Vector3_t3722313464 * get_address_of_U3COriginU3Ek__BackingField_0() { return &___U3COriginU3Ek__BackingField_0; }
	inline void set_U3COriginU3Ek__BackingField_0(Vector3_t3722313464  value)
	{
		___U3COriginU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTerminusU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RayStep_t2147652507, ___U3CTerminusU3Ek__BackingField_1)); }
	inline Vector3_t3722313464  get_U3CTerminusU3Ek__BackingField_1() const { return ___U3CTerminusU3Ek__BackingField_1; }
	inline Vector3_t3722313464 * get_address_of_U3CTerminusU3Ek__BackingField_1() { return &___U3CTerminusU3Ek__BackingField_1; }
	inline void set_U3CTerminusU3Ek__BackingField_1(Vector3_t3722313464  value)
	{
		___U3CTerminusU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDirectionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RayStep_t2147652507, ___U3CDirectionU3Ek__BackingField_2)); }
	inline Vector3_t3722313464  get_U3CDirectionU3Ek__BackingField_2() const { return ___U3CDirectionU3Ek__BackingField_2; }
	inline Vector3_t3722313464 * get_address_of_U3CDirectionU3Ek__BackingField_2() { return &___U3CDirectionU3Ek__BackingField_2; }
	inline void set_U3CDirectionU3Ek__BackingField_2(Vector3_t3722313464  value)
	{
		___U3CDirectionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RayStep_t2147652507, ___U3CLengthU3Ek__BackingField_3)); }
	inline float get_U3CLengthU3Ek__BackingField_3() const { return ___U3CLengthU3Ek__BackingField_3; }
	inline float* get_address_of_U3CLengthU3Ek__BackingField_3() { return &___U3CLengthU3Ek__BackingField_3; }
	inline void set_U3CLengthU3Ek__BackingField_3(float value)
	{
		___U3CLengthU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYSTEP_T2147652507_H
#ifndef MESHDATA_T3361835602_H
#define MESHDATA_T3361835602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll/MeshData
struct  MeshData_t3361835602 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/MeshData::MeshID
	int32_t ___MeshID_0;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/MeshData::LastUpdateID
	int32_t ___LastUpdateID_1;
	// UnityEngine.Matrix4x4 HoloToolkit.Unity.SpatialUnderstandingDll/MeshData::Transform
	Matrix4x4_t1817901843  ___Transform_2;
	// UnityEngine.Vector3[] HoloToolkit.Unity.SpatialUnderstandingDll/MeshData::Verts
	Vector3U5BU5D_t1718750761* ___Verts_3;
	// UnityEngine.Vector3[] HoloToolkit.Unity.SpatialUnderstandingDll/MeshData::Normals
	Vector3U5BU5D_t1718750761* ___Normals_4;
	// System.Int32[] HoloToolkit.Unity.SpatialUnderstandingDll/MeshData::Indices
	Int32U5BU5D_t385246372* ___Indices_5;

public:
	inline static int32_t get_offset_of_MeshID_0() { return static_cast<int32_t>(offsetof(MeshData_t3361835602, ___MeshID_0)); }
	inline int32_t get_MeshID_0() const { return ___MeshID_0; }
	inline int32_t* get_address_of_MeshID_0() { return &___MeshID_0; }
	inline void set_MeshID_0(int32_t value)
	{
		___MeshID_0 = value;
	}

	inline static int32_t get_offset_of_LastUpdateID_1() { return static_cast<int32_t>(offsetof(MeshData_t3361835602, ___LastUpdateID_1)); }
	inline int32_t get_LastUpdateID_1() const { return ___LastUpdateID_1; }
	inline int32_t* get_address_of_LastUpdateID_1() { return &___LastUpdateID_1; }
	inline void set_LastUpdateID_1(int32_t value)
	{
		___LastUpdateID_1 = value;
	}

	inline static int32_t get_offset_of_Transform_2() { return static_cast<int32_t>(offsetof(MeshData_t3361835602, ___Transform_2)); }
	inline Matrix4x4_t1817901843  get_Transform_2() const { return ___Transform_2; }
	inline Matrix4x4_t1817901843 * get_address_of_Transform_2() { return &___Transform_2; }
	inline void set_Transform_2(Matrix4x4_t1817901843  value)
	{
		___Transform_2 = value;
	}

	inline static int32_t get_offset_of_Verts_3() { return static_cast<int32_t>(offsetof(MeshData_t3361835602, ___Verts_3)); }
	inline Vector3U5BU5D_t1718750761* get_Verts_3() const { return ___Verts_3; }
	inline Vector3U5BU5D_t1718750761** get_address_of_Verts_3() { return &___Verts_3; }
	inline void set_Verts_3(Vector3U5BU5D_t1718750761* value)
	{
		___Verts_3 = value;
		Il2CppCodeGenWriteBarrier((&___Verts_3), value);
	}

	inline static int32_t get_offset_of_Normals_4() { return static_cast<int32_t>(offsetof(MeshData_t3361835602, ___Normals_4)); }
	inline Vector3U5BU5D_t1718750761* get_Normals_4() const { return ___Normals_4; }
	inline Vector3U5BU5D_t1718750761** get_address_of_Normals_4() { return &___Normals_4; }
	inline void set_Normals_4(Vector3U5BU5D_t1718750761* value)
	{
		___Normals_4 = value;
		Il2CppCodeGenWriteBarrier((&___Normals_4), value);
	}

	inline static int32_t get_offset_of_Indices_5() { return static_cast<int32_t>(offsetof(MeshData_t3361835602, ___Indices_5)); }
	inline Int32U5BU5D_t385246372* get_Indices_5() const { return ___Indices_5; }
	inline Int32U5BU5D_t385246372** get_address_of_Indices_5() { return &___Indices_5; }
	inline void set_Indices_5(Int32U5BU5D_t385246372* value)
	{
		___Indices_5 = value;
		Il2CppCodeGenWriteBarrier((&___Indices_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/MeshData
struct MeshData_t3361835602_marshaled_pinvoke
{
	int32_t ___MeshID_0;
	int32_t ___LastUpdateID_1;
	Matrix4x4_t1817901843  ___Transform_2;
	Vector3_t3722313464 * ___Verts_3;
	Vector3_t3722313464 * ___Normals_4;
	int32_t* ___Indices_5;
};
// Native definition for COM marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/MeshData
struct MeshData_t3361835602_marshaled_com
{
	int32_t ___MeshID_0;
	int32_t ___LastUpdateID_1;
	Matrix4x4_t1817901843  ___Transform_2;
	Vector3_t3722313464 * ___Verts_3;
	Vector3_t3722313464 * ___Normals_4;
	int32_t* ___Indices_5;
};
#endif // MESHDATA_T3361835602_H
#ifndef RAYCASTRESULTHELPER_T2007985016_H
#define RAYCASTRESULTHELPER_T2007985016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RaycastResultHelper
struct  RaycastResultHelper_t2007985016 
{
public:
	// UnityEngine.Collider HoloToolkit.Unity.RaycastResultHelper::collider
	Collider_t1773347010 * ___collider_0;
	// System.Int32 HoloToolkit.Unity.RaycastResultHelper::layer
	int32_t ___layer_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.RaycastResultHelper::normal
	Vector3_t3722313464  ___normal_2;
	// System.Single HoloToolkit.Unity.RaycastResultHelper::distance
	float ___distance_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.RaycastResultHelper::point
	Vector3_t3722313464  ___point_4;
	// UnityEngine.Transform HoloToolkit.Unity.RaycastResultHelper::transform
	Transform_t3600365921 * ___transform_5;
	// UnityEngine.Vector2 HoloToolkit.Unity.RaycastResultHelper::textureCoord
	Vector2_t2156229523  ___textureCoord_6;
	// UnityEngine.Vector2 HoloToolkit.Unity.RaycastResultHelper::textureCoord2
	Vector2_t2156229523  ___textureCoord2_7;

public:
	inline static int32_t get_offset_of_collider_0() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___collider_0)); }
	inline Collider_t1773347010 * get_collider_0() const { return ___collider_0; }
	inline Collider_t1773347010 ** get_address_of_collider_0() { return &___collider_0; }
	inline void set_collider_0(Collider_t1773347010 * value)
	{
		___collider_0 = value;
		Il2CppCodeGenWriteBarrier((&___collider_0), value);
	}

	inline static int32_t get_offset_of_layer_1() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___layer_1)); }
	inline int32_t get_layer_1() const { return ___layer_1; }
	inline int32_t* get_address_of_layer_1() { return &___layer_1; }
	inline void set_layer_1(int32_t value)
	{
		___layer_1 = value;
	}

	inline static int32_t get_offset_of_normal_2() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___normal_2)); }
	inline Vector3_t3722313464  get_normal_2() const { return ___normal_2; }
	inline Vector3_t3722313464 * get_address_of_normal_2() { return &___normal_2; }
	inline void set_normal_2(Vector3_t3722313464  value)
	{
		___normal_2 = value;
	}

	inline static int32_t get_offset_of_distance_3() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___distance_3)); }
	inline float get_distance_3() const { return ___distance_3; }
	inline float* get_address_of_distance_3() { return &___distance_3; }
	inline void set_distance_3(float value)
	{
		___distance_3 = value;
	}

	inline static int32_t get_offset_of_point_4() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___point_4)); }
	inline Vector3_t3722313464  get_point_4() const { return ___point_4; }
	inline Vector3_t3722313464 * get_address_of_point_4() { return &___point_4; }
	inline void set_point_4(Vector3_t3722313464  value)
	{
		___point_4 = value;
	}

	inline static int32_t get_offset_of_transform_5() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___transform_5)); }
	inline Transform_t3600365921 * get_transform_5() const { return ___transform_5; }
	inline Transform_t3600365921 ** get_address_of_transform_5() { return &___transform_5; }
	inline void set_transform_5(Transform_t3600365921 * value)
	{
		___transform_5 = value;
		Il2CppCodeGenWriteBarrier((&___transform_5), value);
	}

	inline static int32_t get_offset_of_textureCoord_6() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___textureCoord_6)); }
	inline Vector2_t2156229523  get_textureCoord_6() const { return ___textureCoord_6; }
	inline Vector2_t2156229523 * get_address_of_textureCoord_6() { return &___textureCoord_6; }
	inline void set_textureCoord_6(Vector2_t2156229523  value)
	{
		___textureCoord_6 = value;
	}

	inline static int32_t get_offset_of_textureCoord2_7() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016, ___textureCoord2_7)); }
	inline Vector2_t2156229523  get_textureCoord2_7() const { return ___textureCoord2_7; }
	inline Vector2_t2156229523 * get_address_of_textureCoord2_7() { return &___textureCoord2_7; }
	inline void set_textureCoord2_7(Vector2_t2156229523  value)
	{
		___textureCoord2_7 = value;
	}
};

struct RaycastResultHelper_t2007985016_StaticFields
{
public:
	// HoloToolkit.Unity.RaycastResultHelper HoloToolkit.Unity.RaycastResultHelper::None
	RaycastResultHelper_t2007985016  ___None_8;

public:
	inline static int32_t get_offset_of_None_8() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t2007985016_StaticFields, ___None_8)); }
	inline RaycastResultHelper_t2007985016  get_None_8() const { return ___None_8; }
	inline RaycastResultHelper_t2007985016 * get_address_of_None_8() { return &___None_8; }
	inline void set_None_8(RaycastResultHelper_t2007985016  value)
	{
		___None_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.RaycastResultHelper
struct RaycastResultHelper_t2007985016_marshaled_pinvoke
{
	Collider_t1773347010 * ___collider_0;
	int32_t ___layer_1;
	Vector3_t3722313464  ___normal_2;
	float ___distance_3;
	Vector3_t3722313464  ___point_4;
	Transform_t3600365921 * ___transform_5;
	Vector2_t2156229523  ___textureCoord_6;
	Vector2_t2156229523  ___textureCoord2_7;
};
// Native definition for COM marshalling of HoloToolkit.Unity.RaycastResultHelper
struct RaycastResultHelper_t2007985016_marshaled_com
{
	Collider_t1773347010 * ___collider_0;
	int32_t ___layer_1;
	Vector3_t3722313464  ___normal_2;
	float ___distance_3;
	Vector3_t3722313464  ___point_4;
	Transform_t3600365921 * ___transform_5;
	Vector2_t2156229523  ___textureCoord_6;
	Vector2_t2156229523  ___textureCoord2_7;
};
#endif // RAYCASTRESULTHELPER_T2007985016_H
#ifndef ENTRY_T3638164044_H
#define ENTRY_T3638164044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>
struct  Entry_t3638164044 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	TypedConstant_t714020897  ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	int32_t ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t3638164044, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t3638164044, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t3638164044, ___key_2)); }
	inline TypedConstant_t714020897  get_key_2() const { return ___key_2; }
	inline TypedConstant_t714020897 * get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(TypedConstant_t714020897  value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t3638164044, ___value_3)); }
	inline int32_t get_value_3() const { return ___value_3; }
	inline int32_t* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(int32_t value)
	{
		___value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3638164044_H
#ifndef MESHDATA_T4114415351_H
#define MESHDATA_T4114415351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialMapping.PlaneFinding/MeshData
struct  MeshData_t4114415351 
{
public:
	// UnityEngine.Matrix4x4 HoloToolkit.Unity.SpatialMapping.PlaneFinding/MeshData::Transform
	Matrix4x4_t1817901843  ___Transform_0;
	// UnityEngine.Vector3[] HoloToolkit.Unity.SpatialMapping.PlaneFinding/MeshData::Verts
	Vector3U5BU5D_t1718750761* ___Verts_1;
	// UnityEngine.Vector3[] HoloToolkit.Unity.SpatialMapping.PlaneFinding/MeshData::Normals
	Vector3U5BU5D_t1718750761* ___Normals_2;
	// System.Int32[] HoloToolkit.Unity.SpatialMapping.PlaneFinding/MeshData::Indices
	Int32U5BU5D_t385246372* ___Indices_3;

public:
	inline static int32_t get_offset_of_Transform_0() { return static_cast<int32_t>(offsetof(MeshData_t4114415351, ___Transform_0)); }
	inline Matrix4x4_t1817901843  get_Transform_0() const { return ___Transform_0; }
	inline Matrix4x4_t1817901843 * get_address_of_Transform_0() { return &___Transform_0; }
	inline void set_Transform_0(Matrix4x4_t1817901843  value)
	{
		___Transform_0 = value;
	}

	inline static int32_t get_offset_of_Verts_1() { return static_cast<int32_t>(offsetof(MeshData_t4114415351, ___Verts_1)); }
	inline Vector3U5BU5D_t1718750761* get_Verts_1() const { return ___Verts_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of_Verts_1() { return &___Verts_1; }
	inline void set_Verts_1(Vector3U5BU5D_t1718750761* value)
	{
		___Verts_1 = value;
		Il2CppCodeGenWriteBarrier((&___Verts_1), value);
	}

	inline static int32_t get_offset_of_Normals_2() { return static_cast<int32_t>(offsetof(MeshData_t4114415351, ___Normals_2)); }
	inline Vector3U5BU5D_t1718750761* get_Normals_2() const { return ___Normals_2; }
	inline Vector3U5BU5D_t1718750761** get_address_of_Normals_2() { return &___Normals_2; }
	inline void set_Normals_2(Vector3U5BU5D_t1718750761* value)
	{
		___Normals_2 = value;
		Il2CppCodeGenWriteBarrier((&___Normals_2), value);
	}

	inline static int32_t get_offset_of_Indices_3() { return static_cast<int32_t>(offsetof(MeshData_t4114415351, ___Indices_3)); }
	inline Int32U5BU5D_t385246372* get_Indices_3() const { return ___Indices_3; }
	inline Int32U5BU5D_t385246372** get_address_of_Indices_3() { return &___Indices_3; }
	inline void set_Indices_3(Int32U5BU5D_t385246372* value)
	{
		___Indices_3 = value;
		Il2CppCodeGenWriteBarrier((&___Indices_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.SpatialMapping.PlaneFinding/MeshData
struct MeshData_t4114415351_marshaled_pinvoke
{
	Matrix4x4_t1817901843  ___Transform_0;
	Vector3_t3722313464 * ___Verts_1;
	Vector3_t3722313464 * ___Normals_2;
	int32_t* ___Indices_3;
};
// Native definition for COM marshalling of HoloToolkit.Unity.SpatialMapping.PlaneFinding/MeshData
struct MeshData_t4114415351_marshaled_com
{
	Matrix4x4_t1817901843  ___Transform_0;
	Vector3_t3722313464 * ___Verts_1;
	Vector3_t3722313464 * ___Normals_2;
	int32_t* ___Indices_3;
};
#endif // MESHDATA_T4114415351_H
#ifndef ENTRY_T2916836371_H
#define ENTRY_T2916836371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.IntPtr,System.Object>
struct  Entry_t2916836371 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	intptr_t ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	RuntimeObject * ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t2916836371, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t2916836371, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t2916836371, ___key_2)); }
	inline intptr_t get_key_2() const { return ___key_2; }
	inline intptr_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(intptr_t value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t2916836371, ___value_3)); }
	inline RuntimeObject * get_value_3() const { return ___value_3; }
	inline RuntimeObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(RuntimeObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T2916836371_H
#ifndef IMPORTEDMESHDATA_T558937955_H
#define IMPORTEDMESHDATA_T558937955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialMapping.PlaneFinding/DLLImports/ImportedMeshData
struct  ImportedMeshData_t558937955 
{
public:
	// UnityEngine.Matrix4x4 HoloToolkit.Unity.SpatialMapping.PlaneFinding/DLLImports/ImportedMeshData::transform
	Matrix4x4_t1817901843  ___transform_0;
	// System.Int32 HoloToolkit.Unity.SpatialMapping.PlaneFinding/DLLImports/ImportedMeshData::vertCount
	int32_t ___vertCount_1;
	// System.Int32 HoloToolkit.Unity.SpatialMapping.PlaneFinding/DLLImports/ImportedMeshData::indexCount
	int32_t ___indexCount_2;
	// System.IntPtr HoloToolkit.Unity.SpatialMapping.PlaneFinding/DLLImports/ImportedMeshData::verts
	intptr_t ___verts_3;
	// System.IntPtr HoloToolkit.Unity.SpatialMapping.PlaneFinding/DLLImports/ImportedMeshData::normals
	intptr_t ___normals_4;
	// System.IntPtr HoloToolkit.Unity.SpatialMapping.PlaneFinding/DLLImports/ImportedMeshData::indices
	intptr_t ___indices_5;

public:
	inline static int32_t get_offset_of_transform_0() { return static_cast<int32_t>(offsetof(ImportedMeshData_t558937955, ___transform_0)); }
	inline Matrix4x4_t1817901843  get_transform_0() const { return ___transform_0; }
	inline Matrix4x4_t1817901843 * get_address_of_transform_0() { return &___transform_0; }
	inline void set_transform_0(Matrix4x4_t1817901843  value)
	{
		___transform_0 = value;
	}

	inline static int32_t get_offset_of_vertCount_1() { return static_cast<int32_t>(offsetof(ImportedMeshData_t558937955, ___vertCount_1)); }
	inline int32_t get_vertCount_1() const { return ___vertCount_1; }
	inline int32_t* get_address_of_vertCount_1() { return &___vertCount_1; }
	inline void set_vertCount_1(int32_t value)
	{
		___vertCount_1 = value;
	}

	inline static int32_t get_offset_of_indexCount_2() { return static_cast<int32_t>(offsetof(ImportedMeshData_t558937955, ___indexCount_2)); }
	inline int32_t get_indexCount_2() const { return ___indexCount_2; }
	inline int32_t* get_address_of_indexCount_2() { return &___indexCount_2; }
	inline void set_indexCount_2(int32_t value)
	{
		___indexCount_2 = value;
	}

	inline static int32_t get_offset_of_verts_3() { return static_cast<int32_t>(offsetof(ImportedMeshData_t558937955, ___verts_3)); }
	inline intptr_t get_verts_3() const { return ___verts_3; }
	inline intptr_t* get_address_of_verts_3() { return &___verts_3; }
	inline void set_verts_3(intptr_t value)
	{
		___verts_3 = value;
	}

	inline static int32_t get_offset_of_normals_4() { return static_cast<int32_t>(offsetof(ImportedMeshData_t558937955, ___normals_4)); }
	inline intptr_t get_normals_4() const { return ___normals_4; }
	inline intptr_t* get_address_of_normals_4() { return &___normals_4; }
	inline void set_normals_4(intptr_t value)
	{
		___normals_4 = value;
	}

	inline static int32_t get_offset_of_indices_5() { return static_cast<int32_t>(offsetof(ImportedMeshData_t558937955, ___indices_5)); }
	inline intptr_t get_indices_5() const { return ___indices_5; }
	inline intptr_t* get_address_of_indices_5() { return &___indices_5; }
	inline void set_indices_5(intptr_t value)
	{
		___indices_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPORTEDMESHDATA_T558937955_H
#ifndef MESHDATA_T3857626232_H
#define MESHDATA_T3857626232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData
#pragma pack(push, tp, 1)
struct  MeshData_t3857626232 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::meshID
	int32_t ___meshID_0;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::lastUpdateID
	int32_t ___lastUpdateID_1;
	// UnityEngine.Matrix4x4 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::transform
	Matrix4x4_t1817901843  ___transform_2;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::vertCount
	int32_t ___vertCount_3;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::indexCount
	int32_t ___indexCount_4;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::verts
	intptr_t ___verts_5;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::normals
	intptr_t ___normals_6;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::indices
	intptr_t ___indices_7;

public:
	inline static int32_t get_offset_of_meshID_0() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___meshID_0)); }
	inline int32_t get_meshID_0() const { return ___meshID_0; }
	inline int32_t* get_address_of_meshID_0() { return &___meshID_0; }
	inline void set_meshID_0(int32_t value)
	{
		___meshID_0 = value;
	}

	inline static int32_t get_offset_of_lastUpdateID_1() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___lastUpdateID_1)); }
	inline int32_t get_lastUpdateID_1() const { return ___lastUpdateID_1; }
	inline int32_t* get_address_of_lastUpdateID_1() { return &___lastUpdateID_1; }
	inline void set_lastUpdateID_1(int32_t value)
	{
		___lastUpdateID_1 = value;
	}

	inline static int32_t get_offset_of_transform_2() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___transform_2)); }
	inline Matrix4x4_t1817901843  get_transform_2() const { return ___transform_2; }
	inline Matrix4x4_t1817901843 * get_address_of_transform_2() { return &___transform_2; }
	inline void set_transform_2(Matrix4x4_t1817901843  value)
	{
		___transform_2 = value;
	}

	inline static int32_t get_offset_of_vertCount_3() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___vertCount_3)); }
	inline int32_t get_vertCount_3() const { return ___vertCount_3; }
	inline int32_t* get_address_of_vertCount_3() { return &___vertCount_3; }
	inline void set_vertCount_3(int32_t value)
	{
		___vertCount_3 = value;
	}

	inline static int32_t get_offset_of_indexCount_4() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___indexCount_4)); }
	inline int32_t get_indexCount_4() const { return ___indexCount_4; }
	inline int32_t* get_address_of_indexCount_4() { return &___indexCount_4; }
	inline void set_indexCount_4(int32_t value)
	{
		___indexCount_4 = value;
	}

	inline static int32_t get_offset_of_verts_5() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___verts_5)); }
	inline intptr_t get_verts_5() const { return ___verts_5; }
	inline intptr_t* get_address_of_verts_5() { return &___verts_5; }
	inline void set_verts_5(intptr_t value)
	{
		___verts_5 = value;
	}

	inline static int32_t get_offset_of_normals_6() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___normals_6)); }
	inline intptr_t get_normals_6() const { return ___normals_6; }
	inline intptr_t* get_address_of_normals_6() { return &___normals_6; }
	inline void set_normals_6(intptr_t value)
	{
		___normals_6 = value;
	}

	inline static int32_t get_offset_of_indices_7() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___indices_7)); }
	inline intptr_t get_indices_7() const { return ___indices_7; }
	inline intptr_t* get_address_of_indices_7() { return &___indices_7; }
	inline void set_indices_7(intptr_t value)
	{
		___indices_7 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHDATA_T3857626232_H
#ifndef ENTRY_T3767324455_H
#define ENTRY_T3767324455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>
struct  Entry_t3767324455 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	TypedConstant_t714020897  ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	RuntimeObject * ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t3767324455, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t3767324455, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t3767324455, ___key_2)); }
	inline TypedConstant_t714020897  get_key_2() const { return ___key_2; }
	inline TypedConstant_t714020897 * get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(TypedConstant_t714020897  value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t3767324455, ___value_3)); }
	inline RuntimeObject * get_value_3() const { return ___value_3; }
	inline RuntimeObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(RuntimeObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3767324455_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3125354294_H
#define IITERATORTOIENUMERATORADAPTER_1_T3125354294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.TimerScheduler/TimerIdPair>
struct  IIteratorToIEnumeratorAdapter_1_t3125354294  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	TimerIdPair_t921323905  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3125354294, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3125354294, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3125354294, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3125354294, ___current_3)); }
	inline TimerIdPair_t921323905  get_current_3() const { return ___current_3; }
	inline TimerIdPair_t921323905 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(TimerIdPair_t921323905  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3125354294_H
#ifndef SPLINEPOINT_T3315310755_H
#define SPLINEPOINT_T3315310755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.SplinePoint
struct  SplinePoint_t3315310755 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.Design.SplinePoint::Point
	Vector3_t3722313464  ___Point_0;
	// UnityEngine.Quaternion HoloToolkit.Unity.Design.SplinePoint::Rotation
	Quaternion_t2301928331  ___Rotation_1;

public:
	inline static int32_t get_offset_of_Point_0() { return static_cast<int32_t>(offsetof(SplinePoint_t3315310755, ___Point_0)); }
	inline Vector3_t3722313464  get_Point_0() const { return ___Point_0; }
	inline Vector3_t3722313464 * get_address_of_Point_0() { return &___Point_0; }
	inline void set_Point_0(Vector3_t3722313464  value)
	{
		___Point_0 = value;
	}

	inline static int32_t get_offset_of_Rotation_1() { return static_cast<int32_t>(offsetof(SplinePoint_t3315310755, ___Rotation_1)); }
	inline Quaternion_t2301928331  get_Rotation_1() const { return ___Rotation_1; }
	inline Quaternion_t2301928331 * get_address_of_Rotation_1() { return &___Rotation_1; }
	inline void set_Rotation_1(Quaternion_t2301928331  value)
	{
		___Rotation_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEPOINT_T3315310755_H
#ifndef ANIMATORCONTROLLERPARAMETERTYPE_T3317225440_H
#define ANIMATORCONTROLLERPARAMETERTYPE_T3317225440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorControllerParameterType
struct  AnimatorControllerParameterType_t3317225440 
{
public:
	// System.Int32 UnityEngine.AnimatorControllerParameterType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnimatorControllerParameterType_t3317225440, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORCONTROLLERPARAMETERTYPE_T3317225440_H
#ifndef BUTTONSTATEENUM_T3287638195_H
#define BUTTONSTATEENUM_T3287638195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonStateEnum
struct  ButtonStateEnum_t3287638195 
{
public:
	// System.Int32 HoloToolkit.Unity.Buttons.ButtonStateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonStateEnum_t3287638195, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATEENUM_T3287638195_H
#ifndef ENTRY_T2391274283_H
#define ENTRY_T2391274283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>
struct  Entry_t2391274283 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	RuntimeObject * ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	ResourceLocator_t3723970807  ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t2391274283, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t2391274283, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t2391274283, ___key_2)); }
	inline RuntimeObject * get_key_2() const { return ___key_2; }
	inline RuntimeObject ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(RuntimeObject * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t2391274283, ___value_3)); }
	inline ResourceLocator_t3723970807  get_value_3() const { return ___value_3; }
	inline ResourceLocator_t3723970807 * get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(ResourceLocator_t3723970807  value)
	{
		___value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T2391274283_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2304058131_H
#define IITERATORTOIENUMERATORADAPTER_1_T2304058131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.InputModule.InputSourceInfo>
struct  IIteratorToIEnumeratorAdapter_1_t2304058131  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	InputSourceInfo_t100027742  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2304058131, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2304058131, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2304058131, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2304058131, ___current_3)); }
	inline InputSourceInfo_t100027742  get_current_3() const { return ___current_3; }
	inline InputSourceInfo_t100027742 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(InputSourceInfo_t100027742  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2304058131_H
#ifndef ENTRY_T3471134797_H
#define ENTRY_T3471134797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>
struct  Entry_t3471134797 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	EventCacheKey_t3133620722  ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	EventCacheEntry_t156445199  ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t3471134797, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t3471134797, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t3471134797, ___key_2)); }
	inline EventCacheKey_t3133620722  get_key_2() const { return ___key_2; }
	inline EventCacheKey_t3133620722 * get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(EventCacheKey_t3133620722  value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t3471134797, ___value_3)); }
	inline EventCacheEntry_t156445199  get_value_3() const { return ___value_3; }
	inline EventCacheEntry_t156445199 * get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(EventCacheEntry_t156445199  value)
	{
		___value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3471134797_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1965778583_H
#define IITERATORTOIENUMERATORADAPTER_1_T1965778583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.TimerScheduler/TimerData>
struct  IIteratorToIEnumeratorAdapter_1_t1965778583  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	TimerData_t4056715490  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1965778583, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1965778583, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1965778583, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1965778583, ___current_3)); }
	inline TimerData_t4056715490  get_current_3() const { return ___current_3; }
	inline TimerData_t4056715490 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(TimerData_t4056715490  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1965778583_H
#ifndef OP_T2046805169_H
#define OP_T2046805169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Operator/Op
struct  Op_t2046805169 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.Operator/Op::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Op_t2046805169, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OP_T2046805169_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1320288093_H
#define IITERATORTOIENUMERATORADAPTER_1_T1320288093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.InputModule.SpeechInputHandler/KeywordAndResponse>
struct  IIteratorToIEnumeratorAdapter_1_t1320288093  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeywordAndResponse_t3411225000  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1320288093, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1320288093, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1320288093, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1320288093, ___current_3)); }
	inline KeywordAndResponse_t3411225000  get_current_3() const { return ___current_3; }
	inline KeywordAndResponse_t3411225000 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeywordAndResponse_t3411225000  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1320288093_H
#ifndef SWITCHVALUESTATE_T2805251467_H
#define SWITCHVALUESTATE_T2805251467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AppContext/SwitchValueState
struct  SwitchValueState_t2805251467 
{
public:
	// System.Int32 System.AppContext/SwitchValueState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SwitchValueState_t2805251467, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCHVALUESTATE_T2805251467_H
#ifndef PRIMITIVETYPECODE_T798949904_H
#define PRIMITIVETYPECODE_T798949904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PrimitiveTypeCode
struct  PrimitiveTypeCode_t798949904 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PrimitiveTypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PrimitiveTypeCode_t798949904, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPECODE_T798949904_H
#ifndef READTYPE_T340786580_H
#define READTYPE_T340786580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReadType
struct  ReadType_t340786580 
{
public:
	// System.Int32 Newtonsoft.Json.ReadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReadType_t340786580, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READTYPE_T340786580_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T4124251535_H
#define IITERATORTOIENUMERATORADAPTER_1_T4124251535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.InputModule.XboxControllerData>
struct  IIteratorToIEnumeratorAdapter_1_t4124251535  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	XboxControllerData_t1920221146  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4124251535, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4124251535, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4124251535, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4124251535, ___current_3)); }
	inline XboxControllerData_t1920221146  get_current_3() const { return ___current_3; }
	inline XboxControllerData_t1920221146 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(XboxControllerData_t1920221146  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T4124251535_H
#ifndef STATE_T2595666649_H
#define STATE_T2595666649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter/State
struct  State_t2595666649 
{
public:
	// System.Int32 Newtonsoft.Json.JsonWriter/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t2595666649, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T2595666649_H
#ifndef JTOKENTYPE_T3022361662_H
#define JTOKENTYPE_T3022361662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenType
struct  JTokenType_t3022361662 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JTokenType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JTokenType_t3022361662, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENTYPE_T3022361662_H
#ifndef ENTRY_T2089797520_H
#define ENTRY_T2089797520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>
struct  Entry_t2089797520 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	XPathNodeRef_t3498189018  ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	XPathNodeRef_t3498189018  ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t2089797520, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t2089797520, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t2089797520, ___key_2)); }
	inline XPathNodeRef_t3498189018  get_key_2() const { return ___key_2; }
	inline XPathNodeRef_t3498189018 * get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(XPathNodeRef_t3498189018  value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t2089797520, ___value_3)); }
	inline XPathNodeRef_t3498189018  get_value_3() const { return ___value_3; }
	inline XPathNodeRef_t3498189018 * get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(XPathNodeRef_t3498189018  value)
	{
		___value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T2089797520_H
#ifndef ENTRY_T4265787972_H
#define ENTRY_T4265787972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct  Entry_t4265787972 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	TypeConvertKey_t285306760  ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	RuntimeObject * ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t4265787972, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t4265787972, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t4265787972, ___key_2)); }
	inline TypeConvertKey_t285306760  get_key_2() const { return ___key_2; }
	inline TypeConvertKey_t285306760 * get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(TypeConvertKey_t285306760  value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t4265787972, ___value_3)); }
	inline RuntimeObject * get_value_3() const { return ___value_3; }
	inline RuntimeObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(RuntimeObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T4265787972_H
#ifndef SPLINEPOINT_T4155041892_H
#define SPLINEPOINT_T4155041892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.SplinePoint
struct  SplinePoint_t4155041892 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.SplinePoint::Point
	Vector3_t3722313464  ___Point_0;
	// UnityEngine.Quaternion HoloToolkit.Unity.UX.SplinePoint::Rotation
	Quaternion_t2301928331  ___Rotation_1;

public:
	inline static int32_t get_offset_of_Point_0() { return static_cast<int32_t>(offsetof(SplinePoint_t4155041892, ___Point_0)); }
	inline Vector3_t3722313464  get_Point_0() const { return ___Point_0; }
	inline Vector3_t3722313464 * get_address_of_Point_0() { return &___Point_0; }
	inline void set_Point_0(Vector3_t3722313464  value)
	{
		___Point_0 = value;
	}

	inline static int32_t get_offset_of_Rotation_1() { return static_cast<int32_t>(offsetof(SplinePoint_t4155041892, ___Rotation_1)); }
	inline Quaternion_t2301928331  get_Rotation_1() const { return ___Rotation_1; }
	inline Quaternion_t2301928331 * get_address_of_Rotation_1() { return &___Rotation_1; }
	inline void set_Rotation_1(Quaternion_t2301928331  value)
	{
		___Rotation_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEPOINT_T4155041892_H
#ifndef ENTRY_T3743988185_H
#define ENTRY_T3743988185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>
struct  Entry_t3743988185 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	Guid_t  ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	RuntimeObject * ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t3743988185, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t3743988185, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t3743988185, ___key_2)); }
	inline Guid_t  get_key_2() const { return ___key_2; }
	inline Guid_t * get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(Guid_t  value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t3743988185, ___value_3)); }
	inline RuntimeObject * get_value_3() const { return ___value_3; }
	inline RuntimeObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(RuntimeObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3743988185_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T522303411_H
#define IITERATORTOIENUMERATORADAPTER_1_T522303411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject>
struct  IIteratorToIEnumeratorAdapter_1_t522303411  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	SurfaceObject_t2613240318  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t522303411, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t522303411, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t522303411, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t522303411, ___current_3)); }
	inline SurfaceObject_t2613240318  get_current_3() const { return ___current_3; }
	inline SurfaceObject_t2613240318 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(SurfaceObject_t2613240318  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T522303411_H
#ifndef CIPHERSUITECODE_T732562211_H
#define CIPHERSUITECODE_T732562211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.CipherSuiteCode
struct  CipherSuiteCode_t732562211 
{
public:
	// System.UInt16 Mono.Security.Interface.CipherSuiteCode::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherSuiteCode_t732562211, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERSUITECODE_T732562211_H
#ifndef CLIENTCERTIFICATETYPE_T1004704908_H
#define CLIENTCERTIFICATETYPE_T1004704908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType
struct  ClientCertificateType_t1004704908 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.Handshake.ClientCertificateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClientCertificateType_t1004704908, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTCERTIFICATETYPE_T1004704908_H
#ifndef ENTRY_T2485093721_H
#define ENTRY_T2485093721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>
struct  Entry_t2485093721 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	ResolverContractKey_t3292851287  ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	RuntimeObject * ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t2485093721, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t2485093721, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t2485093721, ___key_2)); }
	inline ResolverContractKey_t3292851287  get_key_2() const { return ___key_2; }
	inline ResolverContractKey_t3292851287 * get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(ResolverContractKey_t3292851287  value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t2485093721, ___value_3)); }
	inline RuntimeObject * get_value_3() const { return ___value_3; }
	inline RuntimeObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(RuntimeObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T2485093721_H
#ifndef ENTRY_T4156871775_H
#define ENTRY_T4156871775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct  Entry_t4156871775 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	TypeNameKey_t2985541961  ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	RuntimeObject * ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t4156871775, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t4156871775, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t4156871775, ___key_2)); }
	inline TypeNameKey_t2985541961  get_key_2() const { return ___key_2; }
	inline TypeNameKey_t2985541961 * get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(TypeNameKey_t2985541961  value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t4156871775, ___value_3)); }
	inline RuntimeObject * get_value_3() const { return ___value_3; }
	inline RuntimeObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(RuntimeObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T4156871775_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1241930985_H
#define IITERATORTOIENUMERATORADAPTER_1_T1241930985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
struct  IIteratorToIEnumeratorAdapter_1_t1241930985  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	TableRange_t3332867892  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1241930985, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1241930985, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1241930985, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1241930985, ___current_3)); }
	inline TableRange_t3332867892  get_current_3() const { return ___current_3; }
	inline TableRange_t3332867892 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(TableRange_t3332867892  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1241930985_H
// Windows.Foundation.Collections.IVectorView`1<Windows.Foundation.Size>
struct NOVTABLE IVectorView_1_t4184750196 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m2196457089(uint32_t ___index0, Size_t550917638 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3307328074(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1205944866(Size_t550917638  ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2898007465(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Size_t550917638 * ___items1, uint32_t* comReturnValue) = 0;
};
#ifndef IITERATORTOIENUMERATORADAPTER_1_T968621830_H
#define IITERATORTOIENUMERATORADAPTER_1_T968621830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>
struct  IIteratorToIEnumeratorAdapter_1_t968621830  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t3059558737  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t968621830, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t968621830, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t968621830, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t968621830, ___current_3)); }
	inline Entry_t3059558737  get_current_3() const { return ___current_3; }
	inline Entry_t3059558737 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t3059558737  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T968621830_H
#ifndef ANCHOROPERATION_T2663234089_H
#define ANCHOROPERATION_T2663234089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.WorldAnchorManager/AnchorOperation
struct  AnchorOperation_t2663234089 
{
public:
	// System.Int32 HoloToolkit.Unity.WorldAnchorManager/AnchorOperation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnchorOperation_t2663234089, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHOROPERATION_T2663234089_H
#ifndef SHAPECOMPONENTCONSTRAINTTYPE_T3710497406_H
#define SHAPECOMPONENTCONSTRAINTTYPE_T3710497406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraintType
struct  ShapeComponentConstraintType_t3710497406 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraintType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShapeComponentConstraintType_t3710497406, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPECOMPONENTCONSTRAINTTYPE_T3710497406_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2489337149_H
#define IITERATORTOIENUMERATORADAPTER_1_T2489337149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>
struct  IIteratorToIEnumeratorAdapter_1_t2489337149  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	TypeConvertKey_t285306760  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2489337149, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2489337149, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2489337149, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2489337149, ___current_3)); }
	inline TypeConvertKey_t285306760  get_current_3() const { return ___current_3; }
	inline TypeConvertKey_t285306760 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(TypeConvertKey_t285306760  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2489337149_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3841630690_H
#define IITERATORTOIENUMERATORADAPTER_1_T3841630690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<GLTF.Math.Color>
struct  IIteratorToIEnumeratorAdapter_1_t3841630690  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Color_t1637600301  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3841630690, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3841630690, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3841630690, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3841630690, ___current_3)); }
	inline Color_t1637600301  get_current_3() const { return ___current_3; }
	inline Color_t1637600301 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Color_t1637600301  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3841630690_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T894605054_H
#define IITERATORTOIENUMERATORADAPTER_1_T894605054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>
struct  IIteratorToIEnumeratorAdapter_1_t894605054  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	TypeNameKey_t2985541961  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t894605054, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t894605054, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t894605054, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t894605054, ___current_3)); }
	inline TypeNameKey_t2985541961  get_current_3() const { return ___current_3; }
	inline TypeNameKey_t2985541961 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(TypeNameKey_t2985541961  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T894605054_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3822279618_H
#define IITERATORTOIENUMERATORADAPTER_1_T3822279618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>
struct  IIteratorToIEnumeratorAdapter_1_t3822279618  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t1618249229  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3822279618, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3822279618, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3822279618, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3822279618, ___current_3)); }
	inline Entry_t1618249229  get_current_3() const { return ___current_3; }
	inline Entry_t1618249229 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t1618249229  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3822279618_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1201914380_H
#define IITERATORTOIENUMERATORADAPTER_1_T1201914380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<Newtonsoft.Json.Serialization.ResolverContractKey>
struct  IIteratorToIEnumeratorAdapter_1_t1201914380  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	ResolverContractKey_t3292851287  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1201914380, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1201914380, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1201914380, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1201914380, ___current_3)); }
	inline ResolverContractKey_t3292851287  get_current_3() const { return ___current_3; }
	inline ResolverContractKey_t3292851287 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ResolverContractKey_t3292851287  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1201914380_H
// Windows.Foundation.Collections.IVectorView`1<Windows.Foundation.Rect>
struct NOVTABLE IVectorView_1_t2033978749 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m2736327627(uint32_t ___index0, Rect_t2695113487 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m2984082999(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m3219133186(Rect_t2695113487  ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m4144836697(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Rect_t2695113487 * ___items1, uint32_t* comReturnValue) = 0;
};
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3666673529_H
#define IITERATORTOIENUMERATORADAPTER_1_T3666673529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Int64,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t3666673529  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t1462643140  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3666673529, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3666673529, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3666673529, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3666673529, ___current_3)); }
	inline Entry_t1462643140  get_current_3() const { return ___current_3; }
	inline Entry_t1462643140 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t1462643140  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3666673529_H
#ifndef EVENTREGISTRATIONTOKENLIST_T3288506496_H
#define EVENTREGISTRATIONTOKENLIST_T3288506496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList
struct  EventRegistrationTokenList_t3288506496 
{
public:
	// System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList::firstToken
	EventRegistrationToken_t318890788  ___firstToken_0;
	// System.Collections.Generic.List`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken> System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList::restTokens
	List_1_t1790965530 * ___restTokens_1;

public:
	inline static int32_t get_offset_of_firstToken_0() { return static_cast<int32_t>(offsetof(EventRegistrationTokenList_t3288506496, ___firstToken_0)); }
	inline EventRegistrationToken_t318890788  get_firstToken_0() const { return ___firstToken_0; }
	inline EventRegistrationToken_t318890788 * get_address_of_firstToken_0() { return &___firstToken_0; }
	inline void set_firstToken_0(EventRegistrationToken_t318890788  value)
	{
		___firstToken_0 = value;
	}

	inline static int32_t get_offset_of_restTokens_1() { return static_cast<int32_t>(offsetof(EventRegistrationTokenList_t3288506496, ___restTokens_1)); }
	inline List_1_t1790965530 * get_restTokens_1() const { return ___restTokens_1; }
	inline List_1_t1790965530 ** get_address_of_restTokens_1() { return &___restTokens_1; }
	inline void set_restTokens_1(List_1_t1790965530 * value)
	{
		___restTokens_1 = value;
		Il2CppCodeGenWriteBarrier((&___restTokens_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList
struct EventRegistrationTokenList_t3288506496_marshaled_pinvoke
{
	EventRegistrationToken_t318890788  ___firstToken_0;
	List_1_t1790965530 * ___restTokens_1;
};
// Native definition for COM marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList
struct EventRegistrationTokenList_t3288506496_marshaled_com
{
	EventRegistrationToken_t318890788  ___firstToken_0;
	List_1_t1790965530 * ___restTokens_1;
};
#endif // EVENTREGISTRATIONTOKENLIST_T3288506496_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1033038731_H
#define IITERATORTOIENUMERATORADAPTER_1_T1033038731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.DictionaryEntry>
struct  IIteratorToIEnumeratorAdapter_1_t1033038731  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	DictionaryEntry_t3123975638  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1033038731, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1033038731, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1033038731, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1033038731, ___current_3)); }
	inline DictionaryEntry_t3123975638  get_current_3() const { return ___current_3; }
	inline DictionaryEntry_t3123975638 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(DictionaryEntry_t3123975638  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1033038731_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1492747076_H
#define IITERATORTOIENUMERATORADAPTER_1_T1492747076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t1492747076  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t3583683983  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1492747076, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1492747076, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1492747076, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1492747076, ___current_3)); }
	inline Entry_t3583683983  get_current_3() const { return ___current_3; }
	inline Entry_t3583683983 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t3583683983  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1492747076_H
#ifndef INVALIDOPERATIONEXCEPTION_T56020091_H
#define INVALIDOPERATIONEXCEPTION_T56020091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t56020091  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T56020091_H
#ifndef JSONCONTAINERTYPE_T3191599701_H
#define JSONCONTAINERTYPE_T3191599701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerType
struct  JsonContainerType_t3191599701 
{
public:
	// System.Int32 Newtonsoft.Json.JsonContainerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonContainerType_t3191599701, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERTYPE_T3191599701_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2487591376_H
#define IITERATORTOIENUMERATORADAPTER_1_T2487591376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.ArraySegment`1<System.Byte>>
struct  IIteratorToIEnumeratorAdapter_1_t2487591376  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	ArraySegment_1_t283560987  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2487591376, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2487591376, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2487591376, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2487591376, ___current_3)); }
	inline ArraySegment_1_t283560987  get_current_3() const { return ___current_3; }
	inline ArraySegment_1_t283560987 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ArraySegment_1_t283560987  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2487591376_H
#ifndef VARIABLESTORAGEKIND_T3280289589_H
#define VARIABLESTORAGEKIND_T3280289589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Compiler.VariableStorageKind
struct  VariableStorageKind_t3280289589 
{
public:
	// System.Int32 System.Linq.Expressions.Compiler.VariableStorageKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VariableStorageKind_t3280289589, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLESTORAGEKIND_T3280289589_H
// Windows.Foundation.Collections.IVectorView`1<Windows.Foundation.Point>
struct NOVTABLE IVectorView_1_t3503818801 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m828460566(uint32_t ___index0, Point_t4164953539 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3766324835(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1240834200(Point_t4164953539  ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m320624579(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Point_t4164953539 * ___items1, uint32_t* comReturnValue) = 0;
};
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3951440029_H
#define IITERATORTOIENUMERATORADAPTER_1_T3951440029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t3951440029  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t1747409640  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3951440029, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3951440029, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3951440029, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3951440029, ___current_3)); }
	inline Entry_t1747409640  get_current_3() const { return ___current_3; }
	inline Entry_t1747409640 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t1747409640  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3951440029_H
#ifndef ANIMINPUTTYPEENUM_T1172345381_H
#define ANIMINPUTTYPEENUM_T1172345381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.AnimCursorDatum/AnimInputTypeEnum
struct  AnimInputTypeEnum_t1172345381 
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.AnimCursorDatum/AnimInputTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnimInputTypeEnum_t1172345381, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMINPUTTYPEENUM_T1172345381_H
#ifndef CURSORSTATEENUM_T193155941_H
#define CURSORSTATEENUM_T193155941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.CursorStateEnum
struct  CursorStateEnum_t193155941 
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.CursorStateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CursorStateEnum_t193155941, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSORSTATEENUM_T193155941_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3966333609_H
#define IITERATORTOIENUMERATORADAPTER_1_T3966333609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Sharing.Spawning.PrefabToDataModel>
struct  IIteratorToIEnumeratorAdapter_1_t3966333609  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	PrefabToDataModel_t1762303220  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3966333609, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3966333609, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3966333609, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3966333609, ___current_3)); }
	inline PrefabToDataModel_t1762303220  get_current_3() const { return ___current_3; }
	inline PrefabToDataModel_t1762303220 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(PrefabToDataModel_t1762303220  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3966333609_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T776869435_H
#define IITERATORTOIENUMERATORADAPTER_1_T776869435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<Mono.Security.Uri/UriScheme>
struct  IIteratorToIEnumeratorAdapter_1_t776869435  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	UriScheme_t2867806342  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t776869435, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t776869435, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t776869435, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t776869435, ___current_3)); }
	inline UriScheme_t2867806342  get_current_3() const { return ___current_3; }
	inline UriScheme_t2867806342 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(UriScheme_t2867806342  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T776869435_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2045591228_H
#define IITERATORTOIENUMERATORADAPTER_1_T2045591228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<GLTF.Math.Vector4>
struct  IIteratorToIEnumeratorAdapter_1_t2045591228  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Vector4_t4136528135  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2045591228, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2045591228, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2045591228, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2045591228, ___current_3)); }
	inline Vector4_t4136528135  get_current_3() const { return ___current_3; }
	inline Vector4_t4136528135 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Vector4_t4136528135  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2045591228_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1407252111_H
#define IITERATORTOIENUMERATORADAPTER_1_T1407252111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<MS.Internal.Xml.Cache.XPathNodeRef>
struct  IIteratorToIEnumeratorAdapter_1_t1407252111  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	XPathNodeRef_t3498189018  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1407252111, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1407252111, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1407252111, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1407252111, ___current_3)); }
	inline XPathNodeRef_t3498189018  get_current_3() const { return ___current_3; }
	inline XPathNodeRef_t3498189018 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(XPathNodeRef_t3498189018  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1407252111_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T117135969_H
#define IITERATORTOIENUMERATORADAPTER_1_T117135969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<MS.Internal.Xml.Cache.XPathNode>
struct  IIteratorToIEnumeratorAdapter_1_t117135969  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	XPathNode_t2208072876  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t117135969, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t117135969, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t117135969, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t117135969, ___current_3)); }
	inline XPathNode_t2208072876  get_current_3() const { return ___current_3; }
	inline XPathNode_t2208072876 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(XPathNode_t2208072876  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T117135969_H
#ifndef ORIENTEDBOUNDINGBOX_T3623798211_H
#define ORIENTEDBOUNDINGBOX_T3623798211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialMapping.OrientedBoundingBox
struct  OrientedBoundingBox_t3623798211 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialMapping.OrientedBoundingBox::Center
	Vector3_t3722313464  ___Center_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialMapping.OrientedBoundingBox::Extents
	Vector3_t3722313464  ___Extents_1;
	// UnityEngine.Quaternion HoloToolkit.Unity.SpatialMapping.OrientedBoundingBox::Rotation
	Quaternion_t2301928331  ___Rotation_2;

public:
	inline static int32_t get_offset_of_Center_0() { return static_cast<int32_t>(offsetof(OrientedBoundingBox_t3623798211, ___Center_0)); }
	inline Vector3_t3722313464  get_Center_0() const { return ___Center_0; }
	inline Vector3_t3722313464 * get_address_of_Center_0() { return &___Center_0; }
	inline void set_Center_0(Vector3_t3722313464  value)
	{
		___Center_0 = value;
	}

	inline static int32_t get_offset_of_Extents_1() { return static_cast<int32_t>(offsetof(OrientedBoundingBox_t3623798211, ___Extents_1)); }
	inline Vector3_t3722313464  get_Extents_1() const { return ___Extents_1; }
	inline Vector3_t3722313464 * get_address_of_Extents_1() { return &___Extents_1; }
	inline void set_Extents_1(Vector3_t3722313464  value)
	{
		___Extents_1 = value;
	}

	inline static int32_t get_offset_of_Rotation_2() { return static_cast<int32_t>(offsetof(OrientedBoundingBox_t3623798211, ___Rotation_2)); }
	inline Quaternion_t2301928331  get_Rotation_2() const { return ___Rotation_2; }
	inline Quaternion_t2301928331 * get_address_of_Rotation_2() { return &___Rotation_2; }
	inline void set_Rotation_2(Quaternion_t2301928331  value)
	{
		___Rotation_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTEDBOUNDINGBOX_T3623798211_H
#ifndef PLANE_T1000493321_H
#define PLANE_T1000493321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t1000493321 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t3722313464  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t1000493321, ___m_Normal_0)); }
	inline Vector3_t3722313464  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_t3722313464  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t1000493321, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T1000493321_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2045591226_H
#define IITERATORTOIENUMERATORADAPTER_1_T2045591226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<GLTF.Math.Vector2>
struct  IIteratorToIEnumeratorAdapter_1_t2045591226  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Vector2_t4136528133  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2045591226, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2045591226, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2045591226, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2045591226, ___current_3)); }
	inline Vector2_t4136528133  get_current_3() const { return ___current_3; }
	inline Vector2_t4136528133 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Vector2_t4136528133  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2045591226_H
#ifndef KEYEVENT_T245959883_H
#define KEYEVENT_T245959883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.KeyboardManager/KeyEvent
struct  KeyEvent_t245959883 
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.KeyboardManager/KeyEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyEvent_t245959883, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEVENT_T245959883_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2045591225_H
#define IITERATORTOIENUMERATORADAPTER_1_T2045591225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<GLTF.Math.Vector3>
struct  IIteratorToIEnumeratorAdapter_1_t2045591225  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Vector3_t4136528132  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2045591225, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2045591225, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2045591225, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2045591225, ___current_3)); }
	inline Vector3_t4136528132  get_current_3() const { return ___current_3; }
	inline Vector3_t4136528132 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Vector3_t4136528132  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2045591225_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1653051278_H
#define IITERATORTOIENUMERATORADAPTER_1_T1653051278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t1653051278  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t3743988185  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1653051278, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1653051278, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1653051278, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1653051278, ___current_3)); }
	inline Entry_t3743988185  get_current_3() const { return ___current_3; }
	inline Entry_t3743988185 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t3743988185  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1653051278_H
#ifndef ENTRY_T1947593065_H
#define ENTRY_T1947593065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>
struct  Entry_t1947593065 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	RuntimeObject * ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	int32_t ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t1947593065, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t1947593065, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t1947593065, ___key_2)); }
	inline RuntimeObject * get_key_2() const { return ___key_2; }
	inline RuntimeObject ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(RuntimeObject * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t1947593065, ___value_3)); }
	inline int32_t get_value_3() const { return ___value_3; }
	inline int32_t* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(int32_t value)
	{
		___value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T1947593065_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1224373848_H
#define IITERATORTOIENUMERATORADAPTER_1_T1224373848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.Design.SplinePoint>
struct  IIteratorToIEnumeratorAdapter_1_t1224373848  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	SplinePoint_t3315310755  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1224373848, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1224373848, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1224373848, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1224373848, ___current_3)); }
	inline SplinePoint_t3315310755  get_current_3() const { return ___current_3; }
	inline SplinePoint_t3315310755 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(SplinePoint_t3315310755  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1224373848_H
#ifndef ENTRY_T3303057352_H
#define ENTRY_T3303057352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.ReadType>
struct  Entry_t3303057352 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	RuntimeObject * ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	int32_t ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t3303057352, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t3303057352, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t3303057352, ___key_2)); }
	inline RuntimeObject * get_key_2() const { return ___key_2; }
	inline RuntimeObject ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(RuntimeObject * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t3303057352, ___value_3)); }
	inline int32_t get_value_3() const { return ___value_3; }
	inline int32_t* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(int32_t value)
	{
		___value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3303057352_H
#ifndef ENTRY_T3761220676_H
#define ENTRY_T3761220676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct  Entry_t3761220676 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	RuntimeObject * ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	int32_t ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t3761220676, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t3761220676, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t3761220676, ___key_2)); }
	inline RuntimeObject * get_key_2() const { return ___key_2; }
	inline RuntimeObject ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(RuntimeObject * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t3761220676, ___value_3)); }
	inline int32_t get_value_3() const { return ___value_3; }
	inline int32_t* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(int32_t value)
	{
		___value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3761220676_H
#ifndef ENTRY_T1472554943_H
#define ENTRY_T1472554943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Object,System.AppContext/SwitchValueState>
struct  Entry_t1472554943 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	RuntimeObject * ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	int32_t ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t1472554943, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t1472554943, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t1472554943, ___key_2)); }
	inline RuntimeObject * get_key_2() const { return ___key_2; }
	inline RuntimeObject ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(RuntimeObject * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t1472554943, ___value_3)); }
	inline int32_t get_value_3() const { return ___value_3; }
	inline int32_t* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(int32_t value)
	{
		___value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T1472554943_H
#ifndef ENTRY_T1955809972_H
#define ENTRY_T1955809972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>
struct  Entry_t1955809972 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	RuntimeObject * ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	EventRegistrationTokenList_t3288506496  ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t1955809972, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t1955809972, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t1955809972, ___key_2)); }
	inline RuntimeObject * get_key_2() const { return ___key_2; }
	inline RuntimeObject ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(RuntimeObject * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t1955809972, ___value_3)); }
	inline EventRegistrationTokenList_t3288506496  get_value_3() const { return ___value_3; }
	inline EventRegistrationTokenList_t3288506496 * get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(EventRegistrationTokenList_t3288506496  value)
	{
		___value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T1955809972_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3208735297_H
#define IITERATORTOIENUMERATORADAPTER_1_T3208735297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
struct  IIteratorToIEnumeratorAdapter_1_t3208735297  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3208735297, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3208735297, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3208735297, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3208735297, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3208735297_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2936592600_H
#define IITERATORTOIENUMERATORADAPTER_1_T2936592600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<Mono.Security.Interface.CipherSuiteCode>
struct  IIteratorToIEnumeratorAdapter_1_t2936592600  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	uint16_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2936592600, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2936592600, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2936592600, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2936592600, ___current_3)); }
	inline uint16_t get_current_3() const { return ___current_3; }
	inline uint16_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(uint16_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2936592600_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T4250835558_H
#define IITERATORTOIENUMERATORADAPTER_1_T4250835558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<MS.Internal.Xml.XPath.Operator/Op>
struct  IIteratorToIEnumeratorAdapter_1_t4250835558  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4250835558, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4250835558, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4250835558, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4250835558, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T4250835558_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T504729742_H
#define IITERATORTOIENUMERATORADAPTER_1_T504729742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<Newtonsoft.Json.JsonWriter/State>
struct  IIteratorToIEnumeratorAdapter_1_t504729742  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t504729742, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t504729742, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t504729742, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t504729742, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T504729742_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1380197890_H
#define IITERATORTOIENUMERATORADAPTER_1_T1380197890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>>
struct  IIteratorToIEnumeratorAdapter_1_t1380197890  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t3471134797  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1380197890, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1380197890, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1380197890, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1380197890, ___current_3)); }
	inline Entry_t3471134797  get_current_3() const { return ___current_3; }
	inline Entry_t3471134797 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t3471134797  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1380197890_H
#ifndef KEYCODEEVENTPAIR_T1510105498_H
#define KEYCODEEVENTPAIR_T1510105498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair
struct  KeyCodeEventPair_t1510105498 
{
public:
	// UnityEngine.KeyCode HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair::KeyCode
	int32_t ___KeyCode_0;
	// HoloToolkit.Unity.InputModule.KeyboardManager/KeyEvent HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair::KeyEvent
	int32_t ___KeyEvent_1;

public:
	inline static int32_t get_offset_of_KeyCode_0() { return static_cast<int32_t>(offsetof(KeyCodeEventPair_t1510105498, ___KeyCode_0)); }
	inline int32_t get_KeyCode_0() const { return ___KeyCode_0; }
	inline int32_t* get_address_of_KeyCode_0() { return &___KeyCode_0; }
	inline void set_KeyCode_0(int32_t value)
	{
		___KeyCode_0 = value;
	}

	inline static int32_t get_offset_of_KeyEvent_1() { return static_cast<int32_t>(offsetof(KeyCodeEventPair_t1510105498, ___KeyEvent_1)); }
	inline int32_t get_KeyEvent_1() const { return ___KeyEvent_1; }
	inline int32_t* get_address_of_KeyEvent_1() { return &___KeyEvent_1; }
	inline void set_KeyEvent_1(int32_t value)
	{
		___KeyEvent_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODEEVENTPAIR_T1510105498_H
#ifndef KEYWORDANDKEYCODE_T2714956691_H
#define KEYWORDANDKEYCODE_T2714956691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.KeywordAndKeyCode
struct  KeywordAndKeyCode_t2714956691 
{
public:
	// System.String HoloToolkit.Unity.InputModule.KeywordAndKeyCode::Keyword
	String_t* ___Keyword_0;
	// UnityEngine.KeyCode HoloToolkit.Unity.InputModule.KeywordAndKeyCode::KeyCode
	int32_t ___KeyCode_1;

public:
	inline static int32_t get_offset_of_Keyword_0() { return static_cast<int32_t>(offsetof(KeywordAndKeyCode_t2714956691, ___Keyword_0)); }
	inline String_t* get_Keyword_0() const { return ___Keyword_0; }
	inline String_t** get_address_of_Keyword_0() { return &___Keyword_0; }
	inline void set_Keyword_0(String_t* value)
	{
		___Keyword_0 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_0), value);
	}

	inline static int32_t get_offset_of_KeyCode_1() { return static_cast<int32_t>(offsetof(KeywordAndKeyCode_t2714956691, ___KeyCode_1)); }
	inline int32_t get_KeyCode_1() const { return ___KeyCode_1; }
	inline int32_t* get_address_of_KeyCode_1() { return &___KeyCode_1; }
	inline void set_KeyCode_1(int32_t value)
	{
		___KeyCode_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.InputModule.KeywordAndKeyCode
struct KeywordAndKeyCode_t2714956691_marshaled_pinvoke
{
	char* ___Keyword_0;
	int32_t ___KeyCode_1;
};
// Native definition for COM marshalling of HoloToolkit.Unity.InputModule.KeywordAndKeyCode
struct KeywordAndKeyCode_t2714956691_marshaled_com
{
	Il2CppChar* ___Keyword_0;
	int32_t ___KeyCode_1;
};
#endif // KEYWORDANDKEYCODE_T2714956691_H
#ifndef ANIMATORPARAMETER_T4149701082_H
#define ANIMATORPARAMETER_T4149701082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.AnimatorParameter
struct  AnimatorParameter_t4149701082 
{
public:
	// UnityEngine.AnimatorControllerParameterType HoloToolkit.Unity.InputModule.AnimatorParameter::Type
	int32_t ___Type_0;
	// System.Int32 HoloToolkit.Unity.InputModule.AnimatorParameter::DefaultInt
	int32_t ___DefaultInt_1;
	// System.Single HoloToolkit.Unity.InputModule.AnimatorParameter::DefaultFloat
	float ___DefaultFloat_2;
	// System.Boolean HoloToolkit.Unity.InputModule.AnimatorParameter::DefaultBool
	bool ___DefaultBool_3;
	// System.String HoloToolkit.Unity.InputModule.AnimatorParameter::Name
	String_t* ___Name_4;
	// System.Nullable`1<System.Int32> HoloToolkit.Unity.InputModule.AnimatorParameter::nameStringHash
	Nullable_1_t378540539  ___nameStringHash_5;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(AnimatorParameter_t4149701082, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_DefaultInt_1() { return static_cast<int32_t>(offsetof(AnimatorParameter_t4149701082, ___DefaultInt_1)); }
	inline int32_t get_DefaultInt_1() const { return ___DefaultInt_1; }
	inline int32_t* get_address_of_DefaultInt_1() { return &___DefaultInt_1; }
	inline void set_DefaultInt_1(int32_t value)
	{
		___DefaultInt_1 = value;
	}

	inline static int32_t get_offset_of_DefaultFloat_2() { return static_cast<int32_t>(offsetof(AnimatorParameter_t4149701082, ___DefaultFloat_2)); }
	inline float get_DefaultFloat_2() const { return ___DefaultFloat_2; }
	inline float* get_address_of_DefaultFloat_2() { return &___DefaultFloat_2; }
	inline void set_DefaultFloat_2(float value)
	{
		___DefaultFloat_2 = value;
	}

	inline static int32_t get_offset_of_DefaultBool_3() { return static_cast<int32_t>(offsetof(AnimatorParameter_t4149701082, ___DefaultBool_3)); }
	inline bool get_DefaultBool_3() const { return ___DefaultBool_3; }
	inline bool* get_address_of_DefaultBool_3() { return &___DefaultBool_3; }
	inline void set_DefaultBool_3(bool value)
	{
		___DefaultBool_3 = value;
	}

	inline static int32_t get_offset_of_Name_4() { return static_cast<int32_t>(offsetof(AnimatorParameter_t4149701082, ___Name_4)); }
	inline String_t* get_Name_4() const { return ___Name_4; }
	inline String_t** get_address_of_Name_4() { return &___Name_4; }
	inline void set_Name_4(String_t* value)
	{
		___Name_4 = value;
		Il2CppCodeGenWriteBarrier((&___Name_4), value);
	}

	inline static int32_t get_offset_of_nameStringHash_5() { return static_cast<int32_t>(offsetof(AnimatorParameter_t4149701082, ___nameStringHash_5)); }
	inline Nullable_1_t378540539  get_nameStringHash_5() const { return ___nameStringHash_5; }
	inline Nullable_1_t378540539 * get_address_of_nameStringHash_5() { return &___nameStringHash_5; }
	inline void set_nameStringHash_5(Nullable_1_t378540539  value)
	{
		___nameStringHash_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.InputModule.AnimatorParameter
struct AnimatorParameter_t4149701082_marshaled_pinvoke
{
	int32_t ___Type_0;
	int32_t ___DefaultInt_1;
	float ___DefaultFloat_2;
	int32_t ___DefaultBool_3;
	char* ___Name_4;
	Nullable_1_t378540539  ___nameStringHash_5;
};
// Native definition for COM marshalling of HoloToolkit.Unity.InputModule.AnimatorParameter
struct AnimatorParameter_t4149701082_marshaled_com
{
	int32_t ___Type_0;
	int32_t ___DefaultInt_1;
	float ___DefaultFloat_2;
	int32_t ___DefaultBool_3;
	Il2CppChar* ___Name_4;
	Nullable_1_t378540539  ___nameStringHash_5;
};
#endif // ANIMATORPARAMETER_T4149701082_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2064104985_H
#define IITERATORTOIENUMERATORADAPTER_1_T2064104985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.UX.SplinePoint>
struct  IIteratorToIEnumeratorAdapter_1_t2064104985  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	SplinePoint_t4155041892  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2064104985, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2064104985, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2064104985, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2064104985, ___current_3)); }
	inline SplinePoint_t4155041892  get_current_3() const { return ___current_3; }
	inline SplinePoint_t4155041892 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(SplinePoint_t4155041892  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2064104985_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2174851065_H
#define IITERATORTOIENUMERATORADAPTER_1_T2174851065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t2174851065  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t4265787972  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2174851065, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2174851065, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2174851065, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2174851065, ___current_3)); }
	inline Entry_t4265787972  get_current_3() const { return ___current_3; }
	inline Entry_t4265787972 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t4265787972  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2174851065_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T931424755_H
#define IITERATORTOIENUMERATORADAPTER_1_T931424755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<Newtonsoft.Json.Linq.JTokenType>
struct  IIteratorToIEnumeratorAdapter_1_t931424755  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t931424755, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t931424755, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t931424755, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t931424755, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T931424755_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T4293827909_H
#define IITERATORTOIENUMERATORADAPTER_1_T4293827909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>
struct  IIteratorToIEnumeratorAdapter_1_t4293827909  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t2089797520  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4293827909, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4293827909, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4293827909, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4293827909, ___current_3)); }
	inline Entry_t2089797520  get_current_3() const { return ___current_3; }
	inline Entry_t2089797520 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t2089797520  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T4293827909_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1547227137_H
#define IITERATORTOIENUMERATORADAPTER_1_T1547227137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>>
struct  IIteratorToIEnumeratorAdapter_1_t1547227137  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t3638164044  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1547227137, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1547227137, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1547227137, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1547227137, ___current_3)); }
	inline Entry_t3638164044  get_current_3() const { return ___current_3; }
	inline Entry_t3638164044 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t3638164044  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1547227137_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T825899464_H
#define IITERATORTOIENUMERATORADAPTER_1_T825899464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.IntPtr,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t825899464  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t2916836371  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t825899464, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t825899464, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t825899464, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t825899464, ___current_3)); }
	inline Entry_t2916836371  get_current_3() const { return ___current_3; }
	inline Entry_t2916836371 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t2916836371  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T825899464_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T394156814_H
#define IITERATORTOIENUMERATORADAPTER_1_T394156814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t394156814  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t2485093721  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t394156814, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t394156814, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t394156814, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t394156814, ___current_3)); }
	inline Entry_t2485093721  get_current_3() const { return ___current_3; }
	inline Entry_t2485093721 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t2485093721  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T394156814_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2065934868_H
#define IITERATORTOIENUMERATORADAPTER_1_T2065934868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t2065934868  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t4156871775  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2065934868, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2065934868, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2065934868, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2065934868, ___current_3)); }
	inline Entry_t4156871775  get_current_3() const { return ___current_3; }
	inline Entry_t4156871775 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t4156871775  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2065934868_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2544816969_H
#define IITERATORTOIENUMERATORADAPTER_1_T2544816969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<Newtonsoft.Json.ReadType>
struct  IIteratorToIEnumeratorAdapter_1_t2544816969  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2544816969, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2544816969, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2544816969, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2544816969, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2544816969_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T300337376_H
#define IITERATORTOIENUMERATORADAPTER_1_T300337376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>
struct  IIteratorToIEnumeratorAdapter_1_t300337376  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t2391274283  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t300337376, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t300337376, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t300337376, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t300337376, ___current_3)); }
	inline Entry_t2391274283  get_current_3() const { return ___current_3; }
	inline Entry_t2391274283 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t2391274283  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T300337376_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3002980293_H
#define IITERATORTOIENUMERATORADAPTER_1_T3002980293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct  IIteratorToIEnumeratorAdapter_1_t3002980293  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3002980293, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3002980293, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3002980293, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3002980293, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3002980293_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1676387548_H
#define IITERATORTOIENUMERATORADAPTER_1_T1676387548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t1676387548  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t3767324455  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1676387548, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1676387548, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1676387548, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1676387548, ___current_3)); }
	inline Entry_t3767324455  get_current_3() const { return ___current_3; }
	inline Entry_t3767324455 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t3767324455  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1676387548_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T714314560_H
#define IITERATORTOIENUMERATORADAPTER_1_T714314560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.AppContext/SwitchValueState>
struct  IIteratorToIEnumeratorAdapter_1_t714314560  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t714314560, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t714314560, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t714314560, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t714314560, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T714314560_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1766689325_H
#define IITERATORTOIENUMERATORADAPTER_1_T1766689325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData>
struct  IIteratorToIEnumeratorAdapter_1_t1766689325  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	MeshData_t3857626232  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1766689325, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1766689325, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1766689325, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1766689325, ___current_3)); }
	inline MeshData_t3857626232  get_current_3() const { return ___current_3; }
	inline MeshData_t3857626232 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(MeshData_t3857626232  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1766689325_H
#ifndef BOUNDEDPLANE_T3406174829_H
#define BOUNDEDPLANE_T3406174829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialMapping.BoundedPlane
struct  BoundedPlane_t3406174829 
{
public:
	// UnityEngine.Plane HoloToolkit.Unity.SpatialMapping.BoundedPlane::Plane
	Plane_t1000493321  ___Plane_0;
	// HoloToolkit.Unity.SpatialMapping.OrientedBoundingBox HoloToolkit.Unity.SpatialMapping.BoundedPlane::Bounds
	OrientedBoundingBox_t3623798211  ___Bounds_1;
	// System.Single HoloToolkit.Unity.SpatialMapping.BoundedPlane::Area
	float ___Area_2;

public:
	inline static int32_t get_offset_of_Plane_0() { return static_cast<int32_t>(offsetof(BoundedPlane_t3406174829, ___Plane_0)); }
	inline Plane_t1000493321  get_Plane_0() const { return ___Plane_0; }
	inline Plane_t1000493321 * get_address_of_Plane_0() { return &___Plane_0; }
	inline void set_Plane_0(Plane_t1000493321  value)
	{
		___Plane_0 = value;
	}

	inline static int32_t get_offset_of_Bounds_1() { return static_cast<int32_t>(offsetof(BoundedPlane_t3406174829, ___Bounds_1)); }
	inline OrientedBoundingBox_t3623798211  get_Bounds_1() const { return ___Bounds_1; }
	inline OrientedBoundingBox_t3623798211 * get_address_of_Bounds_1() { return &___Bounds_1; }
	inline void set_Bounds_1(OrientedBoundingBox_t3623798211  value)
	{
		___Bounds_1 = value;
	}

	inline static int32_t get_offset_of_Area_2() { return static_cast<int32_t>(offsetof(BoundedPlane_t3406174829, ___Area_2)); }
	inline float get_Area_2() const { return ___Area_2; }
	inline float* get_address_of_Area_2() { return &___Area_2; }
	inline void set_Area_2(float value)
	{
		___Area_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDEDPLANE_T3406174829_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1270898695_H
#define IITERATORTOIENUMERATORADAPTER_1_T1270898695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.SpatialUnderstandingDll/MeshData>
struct  IIteratorToIEnumeratorAdapter_1_t1270898695  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	MeshData_t3361835602  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1270898695, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1270898695, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1270898695, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1270898695, ___current_3)); }
	inline MeshData_t3361835602  get_current_3() const { return ___current_3; }
	inline MeshData_t3361835602 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(MeshData_t3361835602  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1270898695_H
#ifndef SHAPECOMPONENTCONSTRAINT_T4241953735_H
#define SHAPECOMPONENTCONSTRAINT_T4241953735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint
#pragma pack(push, tp, 1)
struct  ShapeComponentConstraint_t4241953735 
{
public:
	// HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraintType HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Type
	int32_t ___Type_0;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Param_Float_0
	float ___Param_Float_0_1;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Param_Float_1
	float ___Param_Float_1_2;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Param_Float_2
	float ___Param_Float_2_3;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Param_Float_3
	float ___Param_Float_3_4;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Param_Int_0
	int32_t ___Param_Int_0_5;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Param_Int_1
	int32_t ___Param_Int_1_6;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint::Param_Str_0
	intptr_t ___Param_Str_0_7;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Param_Float_0_1() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Param_Float_0_1)); }
	inline float get_Param_Float_0_1() const { return ___Param_Float_0_1; }
	inline float* get_address_of_Param_Float_0_1() { return &___Param_Float_0_1; }
	inline void set_Param_Float_0_1(float value)
	{
		___Param_Float_0_1 = value;
	}

	inline static int32_t get_offset_of_Param_Float_1_2() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Param_Float_1_2)); }
	inline float get_Param_Float_1_2() const { return ___Param_Float_1_2; }
	inline float* get_address_of_Param_Float_1_2() { return &___Param_Float_1_2; }
	inline void set_Param_Float_1_2(float value)
	{
		___Param_Float_1_2 = value;
	}

	inline static int32_t get_offset_of_Param_Float_2_3() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Param_Float_2_3)); }
	inline float get_Param_Float_2_3() const { return ___Param_Float_2_3; }
	inline float* get_address_of_Param_Float_2_3() { return &___Param_Float_2_3; }
	inline void set_Param_Float_2_3(float value)
	{
		___Param_Float_2_3 = value;
	}

	inline static int32_t get_offset_of_Param_Float_3_4() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Param_Float_3_4)); }
	inline float get_Param_Float_3_4() const { return ___Param_Float_3_4; }
	inline float* get_address_of_Param_Float_3_4() { return &___Param_Float_3_4; }
	inline void set_Param_Float_3_4(float value)
	{
		___Param_Float_3_4 = value;
	}

	inline static int32_t get_offset_of_Param_Int_0_5() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Param_Int_0_5)); }
	inline int32_t get_Param_Int_0_5() const { return ___Param_Int_0_5; }
	inline int32_t* get_address_of_Param_Int_0_5() { return &___Param_Int_0_5; }
	inline void set_Param_Int_0_5(int32_t value)
	{
		___Param_Int_0_5 = value;
	}

	inline static int32_t get_offset_of_Param_Int_1_6() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Param_Int_1_6)); }
	inline int32_t get_Param_Int_1_6() const { return ___Param_Int_1_6; }
	inline int32_t* get_address_of_Param_Int_1_6() { return &___Param_Int_1_6; }
	inline void set_Param_Int_1_6(int32_t value)
	{
		___Param_Int_1_6 = value;
	}

	inline static int32_t get_offset_of_Param_Str_0_7() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_t4241953735, ___Param_Str_0_7)); }
	inline intptr_t get_Param_Str_0_7() const { return ___Param_Str_0_7; }
	inline intptr_t* get_address_of_Param_Str_0_7() { return &___Param_Str_0_7; }
	inline void set_Param_Str_0_7(intptr_t value)
	{
		___Param_Str_0_7 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPECOMPONENTCONSTRAINT_T4241953735_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2762968344_H
#define IITERATORTOIENUMERATORADAPTER_1_T2762968344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.SpatialMapping.PlaneFinding/DLLImports/ImportedMeshData>
struct  IIteratorToIEnumeratorAdapter_1_t2762968344  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	ImportedMeshData_t558937955  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2762968344, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2762968344, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2762968344, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2762968344, ___current_3)); }
	inline ImportedMeshData_t558937955  get_current_3() const { return ___current_3; }
	inline ImportedMeshData_t558937955 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ImportedMeshData_t558937955  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2762968344_H
#ifndef ANCHORATTACHMENTINFO_T2388524547_H
#define ANCHORATTACHMENTINFO_T2388524547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo
struct  AnchorAttachmentInfo_t2388524547 
{
public:
	// UnityEngine.GameObject HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo::<AnchoredGameObject>k__BackingField
	GameObject_t1113636619 * ___U3CAnchoredGameObjectU3Ek__BackingField_0;
	// System.String HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo::<AnchorName>k__BackingField
	String_t* ___U3CAnchorNameU3Ek__BackingField_1;
	// HoloToolkit.Unity.WorldAnchorManager/AnchorOperation HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo::<Operation>k__BackingField
	int32_t ___U3COperationU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CAnchoredGameObjectU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AnchorAttachmentInfo_t2388524547, ___U3CAnchoredGameObjectU3Ek__BackingField_0)); }
	inline GameObject_t1113636619 * get_U3CAnchoredGameObjectU3Ek__BackingField_0() const { return ___U3CAnchoredGameObjectU3Ek__BackingField_0; }
	inline GameObject_t1113636619 ** get_address_of_U3CAnchoredGameObjectU3Ek__BackingField_0() { return &___U3CAnchoredGameObjectU3Ek__BackingField_0; }
	inline void set_U3CAnchoredGameObjectU3Ek__BackingField_0(GameObject_t1113636619 * value)
	{
		___U3CAnchoredGameObjectU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnchoredGameObjectU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAnchorNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AnchorAttachmentInfo_t2388524547, ___U3CAnchorNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CAnchorNameU3Ek__BackingField_1() const { return ___U3CAnchorNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CAnchorNameU3Ek__BackingField_1() { return &___U3CAnchorNameU3Ek__BackingField_1; }
	inline void set_U3CAnchorNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CAnchorNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnchorNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3COperationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AnchorAttachmentInfo_t2388524547, ___U3COperationU3Ek__BackingField_2)); }
	inline int32_t get_U3COperationU3Ek__BackingField_2() const { return ___U3COperationU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3COperationU3Ek__BackingField_2() { return &___U3COperationU3Ek__BackingField_2; }
	inline void set_U3COperationU3Ek__BackingField_2(int32_t value)
	{
		___U3COperationU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo
struct AnchorAttachmentInfo_t2388524547_marshaled_pinvoke
{
	GameObject_t1113636619 * ___U3CAnchoredGameObjectU3Ek__BackingField_0;
	char* ___U3CAnchorNameU3Ek__BackingField_1;
	int32_t ___U3COperationU3Ek__BackingField_2;
};
// Native definition for COM marshalling of HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo
struct AnchorAttachmentInfo_t2388524547_marshaled_com
{
	GameObject_t1113636619 * ___U3CAnchoredGameObjectU3Ek__BackingField_0;
	Il2CppChar* ___U3CAnchorNameU3Ek__BackingField_1;
	int32_t ___U3COperationU3Ek__BackingField_2;
};
#endif // ANCHORATTACHMENTINFO_T2388524547_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2023478444_H
#define IITERATORTOIENUMERATORADAPTER_1_T2023478444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.SpatialMapping.PlaneFinding/MeshData>
struct  IIteratorToIEnumeratorAdapter_1_t2023478444  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	MeshData_t4114415351  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2023478444, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2023478444, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2023478444, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2023478444, ___current_3)); }
	inline MeshData_t4114415351  get_current_3() const { return ___current_3; }
	inline MeshData_t4114415351 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(MeshData_t4114415351  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2023478444_H
#ifndef MESHCURSORDATUM_T2146353049_H
#define MESHCURSORDATUM_T2146353049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.MeshCursor/MeshCursorDatum
struct  MeshCursorDatum_t2146353049 
{
public:
	// System.String HoloToolkit.Unity.InputModule.MeshCursor/MeshCursorDatum::Name
	String_t* ___Name_0;
	// HoloToolkit.Unity.InputModule.CursorStateEnum HoloToolkit.Unity.InputModule.MeshCursor/MeshCursorDatum::CursorState
	int32_t ___CursorState_1;
	// UnityEngine.Mesh HoloToolkit.Unity.InputModule.MeshCursor/MeshCursorDatum::CursorMesh
	Mesh_t3648964284 * ___CursorMesh_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.InputModule.MeshCursor/MeshCursorDatum::LocalScale
	Vector3_t3722313464  ___LocalScale_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.InputModule.MeshCursor/MeshCursorDatum::LocalOffset
	Vector3_t3722313464  ___LocalOffset_4;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MeshCursorDatum_t2146353049, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_CursorState_1() { return static_cast<int32_t>(offsetof(MeshCursorDatum_t2146353049, ___CursorState_1)); }
	inline int32_t get_CursorState_1() const { return ___CursorState_1; }
	inline int32_t* get_address_of_CursorState_1() { return &___CursorState_1; }
	inline void set_CursorState_1(int32_t value)
	{
		___CursorState_1 = value;
	}

	inline static int32_t get_offset_of_CursorMesh_2() { return static_cast<int32_t>(offsetof(MeshCursorDatum_t2146353049, ___CursorMesh_2)); }
	inline Mesh_t3648964284 * get_CursorMesh_2() const { return ___CursorMesh_2; }
	inline Mesh_t3648964284 ** get_address_of_CursorMesh_2() { return &___CursorMesh_2; }
	inline void set_CursorMesh_2(Mesh_t3648964284 * value)
	{
		___CursorMesh_2 = value;
		Il2CppCodeGenWriteBarrier((&___CursorMesh_2), value);
	}

	inline static int32_t get_offset_of_LocalScale_3() { return static_cast<int32_t>(offsetof(MeshCursorDatum_t2146353049, ___LocalScale_3)); }
	inline Vector3_t3722313464  get_LocalScale_3() const { return ___LocalScale_3; }
	inline Vector3_t3722313464 * get_address_of_LocalScale_3() { return &___LocalScale_3; }
	inline void set_LocalScale_3(Vector3_t3722313464  value)
	{
		___LocalScale_3 = value;
	}

	inline static int32_t get_offset_of_LocalOffset_4() { return static_cast<int32_t>(offsetof(MeshCursorDatum_t2146353049, ___LocalOffset_4)); }
	inline Vector3_t3722313464  get_LocalOffset_4() const { return ___LocalOffset_4; }
	inline Vector3_t3722313464 * get_address_of_LocalOffset_4() { return &___LocalOffset_4; }
	inline void set_LocalOffset_4(Vector3_t3722313464  value)
	{
		___LocalOffset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.InputModule.MeshCursor/MeshCursorDatum
struct MeshCursorDatum_t2146353049_marshaled_pinvoke
{
	char* ___Name_0;
	int32_t ___CursorState_1;
	Mesh_t3648964284 * ___CursorMesh_2;
	Vector3_t3722313464  ___LocalScale_3;
	Vector3_t3722313464  ___LocalOffset_4;
};
// Native definition for COM marshalling of HoloToolkit.Unity.InputModule.MeshCursor/MeshCursorDatum
struct MeshCursorDatum_t2146353049_marshaled_com
{
	Il2CppChar* ___Name_0;
	int32_t ___CursorState_1;
	Mesh_t3648964284 * ___CursorMesh_2;
	Vector3_t3722313464  ___LocalScale_3;
	Vector3_t3722313464  ___LocalOffset_4;
};
#endif // MESHCURSORDATUM_T2146353049_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T4212015405_H
#define IITERATORTOIENUMERATORADAPTER_1_T4212015405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.RaycastResultHelper>
struct  IIteratorToIEnumeratorAdapter_1_t4212015405  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	RaycastResultHelper_t2007985016  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4212015405, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4212015405, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4212015405, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4212015405, ___current_3)); }
	inline RaycastResultHelper_t2007985016  get_current_3() const { return ___current_3; }
	inline RaycastResultHelper_t2007985016 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RaycastResultHelper_t2007985016  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T4212015405_H
#ifndef OBJECTCURSORDATUM_T4083784227_H
#define OBJECTCURSORDATUM_T4083784227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.ObjectCursor/ObjectCursorDatum
struct  ObjectCursorDatum_t4083784227 
{
public:
	// System.String HoloToolkit.Unity.InputModule.ObjectCursor/ObjectCursorDatum::Name
	String_t* ___Name_0;
	// HoloToolkit.Unity.InputModule.CursorStateEnum HoloToolkit.Unity.InputModule.ObjectCursor/ObjectCursorDatum::CursorState
	int32_t ___CursorState_1;
	// UnityEngine.GameObject HoloToolkit.Unity.InputModule.ObjectCursor/ObjectCursorDatum::CursorObject
	GameObject_t1113636619 * ___CursorObject_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ObjectCursorDatum_t4083784227, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_CursorState_1() { return static_cast<int32_t>(offsetof(ObjectCursorDatum_t4083784227, ___CursorState_1)); }
	inline int32_t get_CursorState_1() const { return ___CursorState_1; }
	inline int32_t* get_address_of_CursorState_1() { return &___CursorState_1; }
	inline void set_CursorState_1(int32_t value)
	{
		___CursorState_1 = value;
	}

	inline static int32_t get_offset_of_CursorObject_2() { return static_cast<int32_t>(offsetof(ObjectCursorDatum_t4083784227, ___CursorObject_2)); }
	inline GameObject_t1113636619 * get_CursorObject_2() const { return ___CursorObject_2; }
	inline GameObject_t1113636619 ** get_address_of_CursorObject_2() { return &___CursorObject_2; }
	inline void set_CursorObject_2(GameObject_t1113636619 * value)
	{
		___CursorObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___CursorObject_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.InputModule.ObjectCursor/ObjectCursorDatum
struct ObjectCursorDatum_t4083784227_marshaled_pinvoke
{
	char* ___Name_0;
	int32_t ___CursorState_1;
	GameObject_t1113636619 * ___CursorObject_2;
};
// Native definition for COM marshalling of HoloToolkit.Unity.InputModule.ObjectCursor/ObjectCursorDatum
struct ObjectCursorDatum_t4083784227_marshaled_com
{
	Il2CppChar* ___Name_0;
	int32_t ___CursorState_1;
	GameObject_t1113636619 * ___CursorObject_2;
};
#endif // OBJECTCURSORDATUM_T4083784227_H
#ifndef JSONPOSITION_T2528027714_H
#define JSONPOSITION_T2528027714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPosition
struct  JsonPosition_t2528027714 
{
public:
	// Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonPosition::Type
	int32_t ___Type_1;
	// System.Int32 Newtonsoft.Json.JsonPosition::Position
	int32_t ___Position_2;
	// System.String Newtonsoft.Json.JsonPosition::PropertyName
	String_t* ___PropertyName_3;
	// System.Boolean Newtonsoft.Json.JsonPosition::HasIndex
	bool ___HasIndex_4;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_HasIndex_4() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___HasIndex_4)); }
	inline bool get_HasIndex_4() const { return ___HasIndex_4; }
	inline bool* get_address_of_HasIndex_4() { return &___HasIndex_4; }
	inline void set_HasIndex_4(bool value)
	{
		___HasIndex_4 = value;
	}
};

struct JsonPosition_t2528027714_StaticFields
{
public:
	// System.Char[] Newtonsoft.Json.JsonPosition::SpecialCharacters
	CharU5BU5D_t3528271667* ___SpecialCharacters_0;

public:
	inline static int32_t get_offset_of_SpecialCharacters_0() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714_StaticFields, ___SpecialCharacters_0)); }
	inline CharU5BU5D_t3528271667* get_SpecialCharacters_0() const { return ___SpecialCharacters_0; }
	inline CharU5BU5D_t3528271667** get_address_of_SpecialCharacters_0() { return &___SpecialCharacters_0; }
	inline void set_SpecialCharacters_0(CharU5BU5D_t3528271667* value)
	{
		___SpecialCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialCharacters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t2528027714_marshaled_pinvoke
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	char* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
// Native definition for COM marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t2528027714_marshaled_com
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	Il2CppChar* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
#endif // JSONPOSITION_T2528027714_H
#ifndef SPRITECURSORDATUM_T69399036_H
#define SPRITECURSORDATUM_T69399036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.SpriteCursor/SpriteCursorDatum
struct  SpriteCursorDatum_t69399036 
{
public:
	// System.String HoloToolkit.Unity.InputModule.SpriteCursor/SpriteCursorDatum::Name
	String_t* ___Name_0;
	// HoloToolkit.Unity.InputModule.CursorStateEnum HoloToolkit.Unity.InputModule.SpriteCursor/SpriteCursorDatum::CursorState
	int32_t ___CursorState_1;
	// UnityEngine.Sprite HoloToolkit.Unity.InputModule.SpriteCursor/SpriteCursorDatum::CursorSprite
	Sprite_t280657092 * ___CursorSprite_2;
	// UnityEngine.Color HoloToolkit.Unity.InputModule.SpriteCursor/SpriteCursorDatum::CursorColor
	Color_t2555686324  ___CursorColor_3;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(SpriteCursorDatum_t69399036, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_CursorState_1() { return static_cast<int32_t>(offsetof(SpriteCursorDatum_t69399036, ___CursorState_1)); }
	inline int32_t get_CursorState_1() const { return ___CursorState_1; }
	inline int32_t* get_address_of_CursorState_1() { return &___CursorState_1; }
	inline void set_CursorState_1(int32_t value)
	{
		___CursorState_1 = value;
	}

	inline static int32_t get_offset_of_CursorSprite_2() { return static_cast<int32_t>(offsetof(SpriteCursorDatum_t69399036, ___CursorSprite_2)); }
	inline Sprite_t280657092 * get_CursorSprite_2() const { return ___CursorSprite_2; }
	inline Sprite_t280657092 ** get_address_of_CursorSprite_2() { return &___CursorSprite_2; }
	inline void set_CursorSprite_2(Sprite_t280657092 * value)
	{
		___CursorSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___CursorSprite_2), value);
	}

	inline static int32_t get_offset_of_CursorColor_3() { return static_cast<int32_t>(offsetof(SpriteCursorDatum_t69399036, ___CursorColor_3)); }
	inline Color_t2555686324  get_CursorColor_3() const { return ___CursorColor_3; }
	inline Color_t2555686324 * get_address_of_CursorColor_3() { return &___CursorColor_3; }
	inline void set_CursorColor_3(Color_t2555686324  value)
	{
		___CursorColor_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.InputModule.SpriteCursor/SpriteCursorDatum
struct SpriteCursorDatum_t69399036_marshaled_pinvoke
{
	char* ___Name_0;
	int32_t ___CursorState_1;
	Sprite_t280657092 * ___CursorSprite_2;
	Color_t2555686324  ___CursorColor_3;
};
// Native definition for COM marshalling of HoloToolkit.Unity.InputModule.SpriteCursor/SpriteCursorDatum
struct SpriteCursorDatum_t69399036_marshaled_com
{
	Il2CppChar* ___Name_0;
	int32_t ___CursorState_1;
	Sprite_t280657092 * ___CursorSprite_2;
	Color_t2555686324  ___CursorColor_3;
};
#endif // SPRITECURSORDATUM_T69399036_H
#ifndef ANIMATORCONTROLLERACTION_T150210808_H
#define ANIMATORCONTROLLERACTION_T150210808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.AnimatorControllerAction
struct  AnimatorControllerAction_t150210808 
{
public:
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.AnimatorControllerAction::ButtonState
	int32_t ___ButtonState_0;
	// System.String HoloToolkit.Unity.Buttons.AnimatorControllerAction::ParamName
	String_t* ___ParamName_1;
	// UnityEngine.AnimatorControllerParameterType HoloToolkit.Unity.Buttons.AnimatorControllerAction::ParamType
	int32_t ___ParamType_2;
	// System.Boolean HoloToolkit.Unity.Buttons.AnimatorControllerAction::BoolValue
	bool ___BoolValue_3;
	// System.Int32 HoloToolkit.Unity.Buttons.AnimatorControllerAction::IntValue
	int32_t ___IntValue_4;
	// System.Single HoloToolkit.Unity.Buttons.AnimatorControllerAction::FloatValue
	float ___FloatValue_5;
	// System.Boolean HoloToolkit.Unity.Buttons.AnimatorControllerAction::InvalidParam
	bool ___InvalidParam_6;

public:
	inline static int32_t get_offset_of_ButtonState_0() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t150210808, ___ButtonState_0)); }
	inline int32_t get_ButtonState_0() const { return ___ButtonState_0; }
	inline int32_t* get_address_of_ButtonState_0() { return &___ButtonState_0; }
	inline void set_ButtonState_0(int32_t value)
	{
		___ButtonState_0 = value;
	}

	inline static int32_t get_offset_of_ParamName_1() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t150210808, ___ParamName_1)); }
	inline String_t* get_ParamName_1() const { return ___ParamName_1; }
	inline String_t** get_address_of_ParamName_1() { return &___ParamName_1; }
	inline void set_ParamName_1(String_t* value)
	{
		___ParamName_1 = value;
		Il2CppCodeGenWriteBarrier((&___ParamName_1), value);
	}

	inline static int32_t get_offset_of_ParamType_2() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t150210808, ___ParamType_2)); }
	inline int32_t get_ParamType_2() const { return ___ParamType_2; }
	inline int32_t* get_address_of_ParamType_2() { return &___ParamType_2; }
	inline void set_ParamType_2(int32_t value)
	{
		___ParamType_2 = value;
	}

	inline static int32_t get_offset_of_BoolValue_3() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t150210808, ___BoolValue_3)); }
	inline bool get_BoolValue_3() const { return ___BoolValue_3; }
	inline bool* get_address_of_BoolValue_3() { return &___BoolValue_3; }
	inline void set_BoolValue_3(bool value)
	{
		___BoolValue_3 = value;
	}

	inline static int32_t get_offset_of_IntValue_4() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t150210808, ___IntValue_4)); }
	inline int32_t get_IntValue_4() const { return ___IntValue_4; }
	inline int32_t* get_address_of_IntValue_4() { return &___IntValue_4; }
	inline void set_IntValue_4(int32_t value)
	{
		___IntValue_4 = value;
	}

	inline static int32_t get_offset_of_FloatValue_5() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t150210808, ___FloatValue_5)); }
	inline float get_FloatValue_5() const { return ___FloatValue_5; }
	inline float* get_address_of_FloatValue_5() { return &___FloatValue_5; }
	inline void set_FloatValue_5(float value)
	{
		___FloatValue_5 = value;
	}

	inline static int32_t get_offset_of_InvalidParam_6() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t150210808, ___InvalidParam_6)); }
	inline bool get_InvalidParam_6() const { return ___InvalidParam_6; }
	inline bool* get_address_of_InvalidParam_6() { return &___InvalidParam_6; }
	inline void set_InvalidParam_6(bool value)
	{
		___InvalidParam_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.Buttons.AnimatorControllerAction
struct AnimatorControllerAction_t150210808_marshaled_pinvoke
{
	int32_t ___ButtonState_0;
	char* ___ParamName_1;
	int32_t ___ParamType_2;
	int32_t ___BoolValue_3;
	int32_t ___IntValue_4;
	float ___FloatValue_5;
	int32_t ___InvalidParam_6;
};
// Native definition for COM marshalling of HoloToolkit.Unity.Buttons.AnimatorControllerAction
struct AnimatorControllerAction_t150210808_marshaled_com
{
	int32_t ___ButtonState_0;
	Il2CppChar* ___ParamName_1;
	int32_t ___ParamType_2;
	int32_t ___BoolValue_3;
	int32_t ___IntValue_4;
	float ___FloatValue_5;
	int32_t ___InvalidParam_6;
};
#endif // ANIMATORCONTROLLERACTION_T150210808_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T56715600_H
#define IITERATORTOIENUMERATORADAPTER_1_T56715600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.RayStep>
struct  IIteratorToIEnumeratorAdapter_1_t56715600  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	RayStep_t2147652507  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t56715600, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t56715600, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t56715600, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t56715600, ___current_3)); }
	inline RayStep_t2147652507  get_current_3() const { return ___current_3; }
	inline RayStep_t2147652507 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RayStep_t2147652507  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T56715600_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1315237922_H
#define IITERATORTOIENUMERATORADAPTER_1_T1315237922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.SpatialMapping.BoundedPlane>
struct  IIteratorToIEnumeratorAdapter_1_t1315237922  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	BoundedPlane_t3406174829  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1315237922, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1315237922, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1315237922, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1315237922, ___current_3)); }
	inline BoundedPlane_t3406174829  get_current_3() const { return ___current_3; }
	inline BoundedPlane_t3406174829 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(BoundedPlane_t3406174829  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1315237922_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3676585332_H
#define IITERATORTOIENUMERATORADAPTER_1_T3676585332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.AppContext/SwitchValueState>>
struct  IIteratorToIEnumeratorAdapter_1_t3676585332  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t1472554943  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3676585332, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3676585332, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3676585332, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3676585332, ___current_3)); }
	inline Entry_t1472554943  get_current_3() const { return ___current_3; }
	inline Entry_t1472554943 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t1472554943  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3676585332_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T55416142_H
#define IITERATORTOIENUMERATORADAPTER_1_T55416142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.InputModule.MeshCursor/MeshCursorDatum>
struct  IIteratorToIEnumeratorAdapter_1_t55416142  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	MeshCursorDatum_t2146353049  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t55416142, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t55416142, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t55416142, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t55416142, ___current_3)); }
	inline MeshCursorDatum_t2146353049  get_current_3() const { return ___current_3; }
	inline MeshCursorDatum_t2146353049 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(MeshCursorDatum_t2146353049  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T55416142_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1992847320_H
#define IITERATORTOIENUMERATORADAPTER_1_T1992847320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.InputModule.ObjectCursor/ObjectCursorDatum>
struct  IIteratorToIEnumeratorAdapter_1_t1992847320  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	ObjectCursorDatum_t4083784227  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1992847320, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1992847320, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1992847320, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1992847320, ___current_3)); }
	inline ObjectCursorDatum_t4083784227  get_current_3() const { return ___current_3; }
	inline ObjectCursorDatum_t4083784227 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ObjectCursorDatum_t4083784227  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1992847320_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1212120445_H
#define IITERATORTOIENUMERATORADAPTER_1_T1212120445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.ReadType>>
struct  IIteratorToIEnumeratorAdapter_1_t1212120445  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t3303057352  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1212120445, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1212120445, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1212120445, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1212120445, ___current_3)); }
	inline Entry_t3303057352  get_current_3() const { return ___current_3; }
	inline Entry_t3303057352 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t3303057352  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1212120445_H
#ifndef ENTRY_T1758804906_H
#define ENTRY_T1758804906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>
struct  Entry_t1758804906 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	KeyCodeEventPair_t1510105498  ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	RuntimeObject * ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t1758804906, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t1758804906, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t1758804906, ___key_2)); }
	inline KeyCodeEventPair_t1510105498  get_key_2() const { return ___key_2; }
	inline KeyCodeEventPair_t1510105498 * get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(KeyCodeEventPair_t1510105498  value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t1758804906, ___value_3)); }
	inline RuntimeObject * get_value_3() const { return ___value_3; }
	inline RuntimeObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(RuntimeObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T1758804906_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1670283769_H
#define IITERATORTOIENUMERATORADAPTER_1_T1670283769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>
struct  IIteratorToIEnumeratorAdapter_1_t1670283769  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t3761220676  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1670283769, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1670283769, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1670283769, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1670283769, ___current_3)); }
	inline Entry_t3761220676  get_current_3() const { return ___current_3; }
	inline Entry_t3761220676 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t3761220676  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1670283769_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2151016828_H
#define IITERATORTOIENUMERATORADAPTER_1_T2151016828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint>
struct  IIteratorToIEnumeratorAdapter_1_t2151016828  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	ShapeComponentConstraint_t4241953735  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2151016828, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2151016828, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2151016828, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2151016828, ___current_3)); }
	inline ShapeComponentConstraint_t4241953735  get_current_3() const { return ___current_3; }
	inline ShapeComponentConstraint_t4241953735 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ShapeComponentConstraint_t4241953735  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2151016828_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T297587640_H
#define IITERATORTOIENUMERATORADAPTER_1_T297587640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo>
struct  IIteratorToIEnumeratorAdapter_1_t297587640  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	AnchorAttachmentInfo_t2388524547  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t297587640, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t297587640, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t297587640, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t297587640, ___current_3)); }
	inline AnchorAttachmentInfo_t2388524547  get_current_3() const { return ___current_3; }
	inline AnchorAttachmentInfo_t2388524547 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(AnchorAttachmentInfo_t2388524547  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T297587640_H
#ifndef ANIMCURSORDATUM_T4217904620_H
#define ANIMCURSORDATUM_T4217904620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.AnimCursorDatum
struct  AnimCursorDatum_t4217904620 
{
public:
	// System.String HoloToolkit.Unity.InputModule.AnimCursorDatum::Name
	String_t* ___Name_0;
	// HoloToolkit.Unity.InputModule.CursorStateEnum HoloToolkit.Unity.InputModule.AnimCursorDatum::CursorState
	int32_t ___CursorState_1;
	// HoloToolkit.Unity.InputModule.AnimatorParameter HoloToolkit.Unity.InputModule.AnimCursorDatum::Parameter
	AnimatorParameter_t4149701082  ___Parameter_2;
	// HoloToolkit.Unity.InputModule.AnimCursorDatum/AnimInputTypeEnum HoloToolkit.Unity.InputModule.AnimCursorDatum::AnimInputType
	int32_t ___AnimInputType_3;
	// System.String HoloToolkit.Unity.InputModule.AnimCursorDatum::AnimParameterName
	String_t* ___AnimParameterName_4;
	// System.Boolean HoloToolkit.Unity.InputModule.AnimCursorDatum::AnimBoolValue
	bool ___AnimBoolValue_5;
	// System.Int32 HoloToolkit.Unity.InputModule.AnimCursorDatum::AnimIntValue
	int32_t ___AnimIntValue_6;
	// System.Single HoloToolkit.Unity.InputModule.AnimCursorDatum::AnimFloatValue
	float ___AnimFloatValue_7;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(AnimCursorDatum_t4217904620, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_CursorState_1() { return static_cast<int32_t>(offsetof(AnimCursorDatum_t4217904620, ___CursorState_1)); }
	inline int32_t get_CursorState_1() const { return ___CursorState_1; }
	inline int32_t* get_address_of_CursorState_1() { return &___CursorState_1; }
	inline void set_CursorState_1(int32_t value)
	{
		___CursorState_1 = value;
	}

	inline static int32_t get_offset_of_Parameter_2() { return static_cast<int32_t>(offsetof(AnimCursorDatum_t4217904620, ___Parameter_2)); }
	inline AnimatorParameter_t4149701082  get_Parameter_2() const { return ___Parameter_2; }
	inline AnimatorParameter_t4149701082 * get_address_of_Parameter_2() { return &___Parameter_2; }
	inline void set_Parameter_2(AnimatorParameter_t4149701082  value)
	{
		___Parameter_2 = value;
	}

	inline static int32_t get_offset_of_AnimInputType_3() { return static_cast<int32_t>(offsetof(AnimCursorDatum_t4217904620, ___AnimInputType_3)); }
	inline int32_t get_AnimInputType_3() const { return ___AnimInputType_3; }
	inline int32_t* get_address_of_AnimInputType_3() { return &___AnimInputType_3; }
	inline void set_AnimInputType_3(int32_t value)
	{
		___AnimInputType_3 = value;
	}

	inline static int32_t get_offset_of_AnimParameterName_4() { return static_cast<int32_t>(offsetof(AnimCursorDatum_t4217904620, ___AnimParameterName_4)); }
	inline String_t* get_AnimParameterName_4() const { return ___AnimParameterName_4; }
	inline String_t** get_address_of_AnimParameterName_4() { return &___AnimParameterName_4; }
	inline void set_AnimParameterName_4(String_t* value)
	{
		___AnimParameterName_4 = value;
		Il2CppCodeGenWriteBarrier((&___AnimParameterName_4), value);
	}

	inline static int32_t get_offset_of_AnimBoolValue_5() { return static_cast<int32_t>(offsetof(AnimCursorDatum_t4217904620, ___AnimBoolValue_5)); }
	inline bool get_AnimBoolValue_5() const { return ___AnimBoolValue_5; }
	inline bool* get_address_of_AnimBoolValue_5() { return &___AnimBoolValue_5; }
	inline void set_AnimBoolValue_5(bool value)
	{
		___AnimBoolValue_5 = value;
	}

	inline static int32_t get_offset_of_AnimIntValue_6() { return static_cast<int32_t>(offsetof(AnimCursorDatum_t4217904620, ___AnimIntValue_6)); }
	inline int32_t get_AnimIntValue_6() const { return ___AnimIntValue_6; }
	inline int32_t* get_address_of_AnimIntValue_6() { return &___AnimIntValue_6; }
	inline void set_AnimIntValue_6(int32_t value)
	{
		___AnimIntValue_6 = value;
	}

	inline static int32_t get_offset_of_AnimFloatValue_7() { return static_cast<int32_t>(offsetof(AnimCursorDatum_t4217904620, ___AnimFloatValue_7)); }
	inline float get_AnimFloatValue_7() const { return ___AnimFloatValue_7; }
	inline float* get_address_of_AnimFloatValue_7() { return &___AnimFloatValue_7; }
	inline void set_AnimFloatValue_7(float value)
	{
		___AnimFloatValue_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.InputModule.AnimCursorDatum
struct AnimCursorDatum_t4217904620_marshaled_pinvoke
{
	char* ___Name_0;
	int32_t ___CursorState_1;
	AnimatorParameter_t4149701082_marshaled_pinvoke ___Parameter_2;
	int32_t ___AnimInputType_3;
	char* ___AnimParameterName_4;
	int32_t ___AnimBoolValue_5;
	int32_t ___AnimIntValue_6;
	float ___AnimFloatValue_7;
};
// Native definition for COM marshalling of HoloToolkit.Unity.InputModule.AnimCursorDatum
struct AnimCursorDatum_t4217904620_marshaled_com
{
	Il2CppChar* ___Name_0;
	int32_t ___CursorState_1;
	AnimatorParameter_t4149701082_marshaled_com ___Parameter_2;
	int32_t ___AnimInputType_3;
	Il2CppChar* ___AnimParameterName_4;
	int32_t ___AnimBoolValue_5;
	int32_t ___AnimIntValue_6;
	float ___AnimFloatValue_7;
};
#endif // ANIMCURSORDATUM_T4217904620_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T4159840361_H
#define IITERATORTOIENUMERATORADAPTER_1_T4159840361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>>
struct  IIteratorToIEnumeratorAdapter_1_t4159840361  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t1955809972  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4159840361, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4159840361, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4159840361, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4159840361, ___current_3)); }
	inline Entry_t1955809972  get_current_3() const { return ___current_3; }
	inline Entry_t1955809972 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t1955809972  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T4159840361_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2058764175_H
#define IITERATORTOIENUMERATORADAPTER_1_T2058764175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.InputModule.AnimatorParameter>
struct  IIteratorToIEnumeratorAdapter_1_t2058764175  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	AnimatorParameter_t4149701082  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2058764175, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2058764175, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2058764175, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2058764175, ___current_3)); }
	inline AnimatorParameter_t4149701082  get_current_3() const { return ___current_3; }
	inline AnimatorParameter_t4149701082 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(AnimatorParameter_t4149701082  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2058764175_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2354241197_H
#define IITERATORTOIENUMERATORADAPTER_1_T2354241197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.Buttons.AnimatorControllerAction>
struct  IIteratorToIEnumeratorAdapter_1_t2354241197  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	AnimatorControllerAction_t150210808  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2354241197, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2354241197, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2354241197, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2354241197, ___current_3)); }
	inline AnimatorControllerAction_t150210808  get_current_3() const { return ___current_3; }
	inline AnimatorControllerAction_t150210808 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(AnimatorControllerAction_t150210808  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2354241197_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T624019784_H
#define IITERATORTOIENUMERATORADAPTER_1_T624019784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.InputModule.KeywordAndKeyCode>
struct  IIteratorToIEnumeratorAdapter_1_t624019784  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeywordAndKeyCode_t2714956691  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t624019784, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t624019784, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t624019784, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t624019784, ___current_3)); }
	inline KeywordAndKeyCode_t2714956691  get_current_3() const { return ___current_3; }
	inline KeywordAndKeyCode_t2714956691 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeywordAndKeyCode_t2714956691  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T624019784_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2273429425_H
#define IITERATORTOIENUMERATORADAPTER_1_T2273429425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.InputModule.SpriteCursor/SpriteCursorDatum>
struct  IIteratorToIEnumeratorAdapter_1_t2273429425  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	SpriteCursorDatum_t69399036  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2273429425, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2273429425, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2273429425, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2273429425, ___current_3)); }
	inline SpriteCursorDatum_t69399036  get_current_3() const { return ___current_3; }
	inline SpriteCursorDatum_t69399036 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(SpriteCursorDatum_t69399036  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2273429425_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T4151623454_H
#define IITERATORTOIENUMERATORADAPTER_1_T4151623454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>>
struct  IIteratorToIEnumeratorAdapter_1_t4151623454  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t1947593065  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4151623454, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4151623454, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4151623454, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4151623454, ___current_3)); }
	inline Entry_t1947593065  get_current_3() const { return ___current_3; }
	inline Entry_t1947593065 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t1947593065  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T4151623454_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T437090807_H
#define IITERATORTOIENUMERATORADAPTER_1_T437090807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<Newtonsoft.Json.JsonPosition>
struct  IIteratorToIEnumeratorAdapter_1_t437090807  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	JsonPosition_t2528027714  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t437090807, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t437090807, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t437090807, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t437090807, ___current_3)); }
	inline JsonPosition_t2528027714  get_current_3() const { return ___current_3; }
	inline JsonPosition_t2528027714 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(JsonPosition_t2528027714  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T437090807_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3714135887_H
#define IITERATORTOIENUMERATORADAPTER_1_T3714135887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair>
struct  IIteratorToIEnumeratorAdapter_1_t3714135887  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyCodeEventPair_t1510105498  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3714135887, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3714135887, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3714135887, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3714135887, ___current_3)); }
	inline KeyCodeEventPair_t1510105498  get_current_3() const { return ___current_3; }
	inline KeyCodeEventPair_t1510105498 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyCodeEventPair_t1510105498  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3714135887_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2126967713_H
#define IITERATORTOIENUMERATORADAPTER_1_T2126967713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<HoloToolkit.Unity.InputModule.AnimCursorDatum>
struct  IIteratorToIEnumeratorAdapter_1_t2126967713  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	AnimCursorDatum_t4217904620  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2126967713, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2126967713, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2126967713, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2126967713, ___current_3)); }
	inline AnimCursorDatum_t4217904620  get_current_3() const { return ___current_3; }
	inline AnimCursorDatum_t4217904620 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(AnimCursorDatum_t4217904620  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2126967713_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3962835295_H
#define IITERATORTOIENUMERATORADAPTER_1_T3962835295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t3962835295  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t1758804906  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3962835295, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3962835295, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3962835295, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3962835295, ___current_3)); }
	inline Entry_t1758804906  get_current_3() const { return ___current_3; }
	inline Entry_t1758804906 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t1758804906  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3962835295_H



// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m237278729 (InvalidOperationException_t56020091 * __this, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m2371684166_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1934872071 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m2371684166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t1934872071 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t1934872071 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t1934872071 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m3764016578_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1934872071 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.ColorBlock>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m185714638_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t656710646 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m185714638_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UI.ColorBlock>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t656710646 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t656710646 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t656710646 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.ColorBlock>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m2934911245_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t656710646 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.ContentSizeFitter/FitMode>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3410248800_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1785560286 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3410248800_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UI.ContentSizeFitter/FitMode>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t1785560286 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t1785560286 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t1785560286 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.ContentSizeFitter/FitMode>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m3603226591_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1785560286 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Image/FillMethod>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4090770565_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t3980103938 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4090770565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UI.Image/FillMethod>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t3980103938 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t3980103938 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t3980103938 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Image/FillMethod>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m4081128358_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t3980103938 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Image/Type>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4008501801_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t3965527896 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4008501801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UI.Image/Type>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t3965527896 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t3965527896 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t3965527896 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Image/Type>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m1829372168_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t3965527896 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/CharacterValidation>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m473212014_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2569593509 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m473212014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UI.InputField/CharacterValidation>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t2569593509 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t2569593509 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t2569593509 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/CharacterValidation>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m405887344_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2569593509 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/ContentType>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4274431090_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t304982468 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4274431090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UI.InputField/ContentType>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t304982468 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t304982468 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t304982468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/ContentType>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m3330119302_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t304982468 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/InputType>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m586500471_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t288079751 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m586500471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UI.InputField/InputType>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t288079751 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t288079751 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t288079751 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/InputType>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m1795723193_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t288079751 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/LineType>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1289551344_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2732327541 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1289551344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UI.InputField/LineType>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t2732327541 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t2732327541 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t2732327541 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.InputField/LineType>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m1714625647_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2732327541 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Navigation>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3322370442_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1566995651 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3322370442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UI.Navigation>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t1566995651 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t1566995651 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t1566995651 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Navigation>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m1379838715_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1566995651 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Scrollbar/Direction>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m2257275539_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1988393425 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m2257275539_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UI.Scrollbar/Direction>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t1988393425 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t1988393425 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t1988393425 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Scrollbar/Direction>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m2155258994_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1988393425 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Selectable/Transition>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3296463687_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t287587703 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3296463687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UI.Selectable/Transition>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t287587703 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t287587703 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t287587703 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Selectable/Transition>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m3575551944_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t287587703 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Slider/Direction>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3254002506_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t3150555603 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3254002506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UI.Slider/Direction>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t3150555603 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t3150555603 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t3150555603 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.Slider/Direction>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m1172466480_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t3150555603 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.SpriteState>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m478565721_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t4175632847 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m478565721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UI.SpriteState>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t4175632847 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t4175632847 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t4175632847 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UI.SpriteState>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m2308539122_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t4175632847 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UICharInfo>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1037077666_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2888147474 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1037077666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UICharInfo>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t2888147474 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t2888147474 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t2888147474 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UICharInfo>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m1024342338_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2888147474 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UILineInfo>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3347196995_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2712945882 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3347196995_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UILineInfo>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t2712945882 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t2712945882 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t2712945882 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UILineInfo>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m837900153_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2712945882 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UIVertex>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1447146653_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2575176677 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1447146653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UIVertex>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t2575176677 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t2575176677 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t2575176677 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UIVertex>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m1231446473_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2575176677 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4139970572_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t4167164980 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4139970572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t4167164980 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t4167164980 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t4167164980 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m3521633119_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t4167164980 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Vector2>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m2298537932_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t673908595 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m2298537932_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.Vector2>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t673908595 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t673908595 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t673908595 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Vector2>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m2981728608_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t673908595 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Vector3>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3672359736_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2239992536 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m3672359736_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.Vector3>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t2239992536 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t2239992536 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t2239992536 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Vector3>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m3791032658_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2239992536 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Vector4>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m2771082488_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1836708009 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m2771082488_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.Vector4>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t1836708009 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t1836708009 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t1836708009 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Vector4>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m165384489_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1836708009 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Windows.Speech.SemanticMeaning>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1480329006_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2051177558 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1480329006_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.Windows.Speech.SemanticMeaning>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t2051177558 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t2051177558 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t2051177558 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.Windows.Speech.SemanticMeaning>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m1614089172_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2051177558 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.XR.WSA.Input.InteractionSourceState>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m2885142129_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1943492846 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m2885142129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.XR.WSA.Input.InteractionSourceState>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t1943492846 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t1943492846 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t1943492846 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.XR.WSA.Input.InteractionSourceState>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m2480849942_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1943492846 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.XR.WSA.SurfaceId>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4044255659_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1516773818 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4044255659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityEngine.XR.WSA.SurfaceId>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t1516773818 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t1516773818 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t1516773818 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityEngine.XR.WSA.SurfaceId>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m2442346932_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1516773818 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityGLTF.GLTFSceneImporter/MaterialType>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1384635258_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t3410419400 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1384635258_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<UnityGLTF.GLTFSceneImporter/MaterialType>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t3410419400 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t3410419400 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t3410419400 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<UnityGLTF.GLTFSceneImporter/MaterialType>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m3518665790_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t3410419400 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<Windows.Foundation.Point>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1925995359_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2682632611 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1925995359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<Windows.Foundation.Point>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t2682632611 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t2682632611 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t2682632611 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<Windows.Foundation.Point>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m1746583644_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t2682632611 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<Windows.Foundation.Rect>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4228673602_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1212792559 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m4228673602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<Windows.Foundation.Rect>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t1212792559 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t1212792559 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t1212792559 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<Windows.Foundation.Rect>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m3067549924_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t1212792559 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<Windows.Foundation.Size>::System.Collections.Generic.IReadOnlyCollection`1.get_Count()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1701789447_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t3363564006 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IInspectableToIReadOnlyCollectionAdapter_1_System_Collections_Generic_IReadOnlyCollection_1_get_Count_m1701789447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		if (!((RuntimeObject*)IsInst((RuntimeObject*)__this, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((RuntimeObject*)__this);
		uint32_t L_0 = InterfaceFuncInvoker0< uint32_t >::Invoke(1 /* System.UInt32 Windows.Foundation.Collections.IVectorView`1<Windows.Foundation.Size>::get_Size() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		V_0 = (int32_t)L_0;
		goto IL_0019;
	}

IL_0011:
	{
		NullCheck((IInspectableToIReadOnlyCollectionAdapter_1_t3363564006 *)__this);
		int32_t L_1 = ((  int32_t (*) (IInspectableToIReadOnlyCollectionAdapter_1_t3363564006 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((IInspectableToIReadOnlyCollectionAdapter_1_t3363564006 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (int32_t)L_1;
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) < ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_4, (String_t*)_stringLiteral3700751949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Int32 System.Runtime.InteropServices.WindowsRuntime.IInspectableToIReadOnlyCollectionAdapter`1<Windows.Foundation.Size>::GetIMapSize()
extern "C"  int32_t IInspectableToIReadOnlyCollectionAdapter_1_GetIMapSize_m3957491875_gshared (IInspectableToIReadOnlyCollectionAdapter_1_t3363564006 * __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_invalid_cast_exception(""));
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<GLTF.Math.Color>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2839321159_gshared (IIterableToIEnumerableAdapter_1_t3935527553 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<GLTF.Math.Color>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3841630690 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3841630690 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3841630690 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<GLTF.Math.Vector2>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m981101248_gshared (IIterableToIEnumerableAdapter_1_t2139488089 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<GLTF.Math.Vector2>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2045591226 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2045591226 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2045591226 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<GLTF.Math.Vector3>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3296441228_gshared (IIterableToIEnumerableAdapter_1_t2139488088 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<GLTF.Math.Vector3>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2045591225 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2045591225 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2045591225 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<GLTF.Math.Vector4>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m630843703_gshared (IIterableToIEnumerableAdapter_1_t2139488091 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<GLTF.Math.Vector4>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2045591228 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2045591228 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2045591228 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Sharing.Spawning.PrefabToDataModel>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m788240945_gshared (IIterableToIEnumerableAdapter_1_t4060230472 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Sharing.Spawning.PrefabToDataModel>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3966333609 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3966333609 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3966333609 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.Buttons.AnimatorControllerAction>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3192595404_gshared (IIterableToIEnumerableAdapter_1_t2448138060 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.Buttons.AnimatorControllerAction>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2354241197 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2354241197 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2354241197 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.Design.SplinePoint>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m4070237061_gshared (IIterableToIEnumerableAdapter_1_t1318270711 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.Design.SplinePoint>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1224373848 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1224373848 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1224373848 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.AnimatorParameter>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1194919610_gshared (IIterableToIEnumerableAdapter_1_t2152661038 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.InputModule.AnimatorParameter>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2058764175 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2058764175 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2058764175 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.AnimCursorDatum>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m278400757_gshared (IIterableToIEnumerableAdapter_1_t2220864576 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.InputModule.AnimCursorDatum>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2126967713 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2126967713 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2126967713 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.InputSourceInfo>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3523220599_gshared (IIterableToIEnumerableAdapter_1_t2397954994 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.InputModule.InputSourceInfo>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2304058131 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2304058131 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2304058131 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2127536990_gshared (IIterableToIEnumerableAdapter_1_t3808032750 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3714135887 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3714135887 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3714135887 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.KeywordAndKeyCode>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1332649543_gshared (IIterableToIEnumerableAdapter_1_t717916647 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.InputModule.KeywordAndKeyCode>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t624019784 * L_3 = (IIteratorToIEnumeratorAdapter_1_t624019784 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t624019784 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.MeshCursor/MeshCursorDatum>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1139766903_gshared (IIterableToIEnumerableAdapter_1_t149313005 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.InputModule.MeshCursor/MeshCursorDatum>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t55416142 * L_3 = (IIteratorToIEnumeratorAdapter_1_t55416142 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t55416142 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.ObjectCursor/ObjectCursorDatum>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m4080583070_gshared (IIterableToIEnumerableAdapter_1_t2086744183 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.InputModule.ObjectCursor/ObjectCursorDatum>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1992847320 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1992847320 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1992847320 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.SpeechInputHandler/KeywordAndResponse>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m4154003652_gshared (IIterableToIEnumerableAdapter_1_t1414184956 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.InputModule.SpeechInputHandler/KeywordAndResponse>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1320288093 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1320288093 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1320288093 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.SpriteCursor/SpriteCursorDatum>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1158811585_gshared (IIterableToIEnumerableAdapter_1_t2367326288 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.InputModule.SpriteCursor/SpriteCursorDatum>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2273429425 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2273429425 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2273429425 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.InputModule.XboxControllerData>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m4077114858_gshared (IIterableToIEnumerableAdapter_1_t4218148398 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.InputModule.XboxControllerData>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t4124251535 * L_3 = (IIteratorToIEnumeratorAdapter_1_t4124251535 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t4124251535 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.RaycastResultHelper>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1529863492_gshared (IIterableToIEnumerableAdapter_1_t10944972 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.RaycastResultHelper>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t4212015405 * L_3 = (IIteratorToIEnumeratorAdapter_1_t4212015405 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t4212015405 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.RayStep>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m600733831_gshared (IIterableToIEnumerableAdapter_1_t150612463 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.RayStep>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t56715600 * L_3 = (IIteratorToIEnumeratorAdapter_1_t56715600 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t56715600 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialMapping.BoundedPlane>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1528376168_gshared (IIterableToIEnumerableAdapter_1_t1409134785 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.SpatialMapping.BoundedPlane>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1315237922 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1315237922 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1315237922 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialMapping.PlaneFinding/DLLImports/ImportedMeshData>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1523134314_gshared (IIterableToIEnumerableAdapter_1_t2856865207 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.SpatialMapping.PlaneFinding/DLLImports/ImportedMeshData>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2762968344 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2762968344 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2762968344 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialMapping.PlaneFinding/MeshData>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m12558835_gshared (IIterableToIEnumerableAdapter_1_t2117375307 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.SpatialMapping.PlaneFinding/MeshData>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2023478444 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2023478444 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2023478444 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m734062386_gshared (IIterableToIEnumerableAdapter_1_t616200274 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t522303411 * L_3 = (IIteratorToIEnumeratorAdapter_1_t522303411 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t522303411 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3531870119_gshared (IIterableToIEnumerableAdapter_1_t1860586188 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1766689325 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1766689325 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1766689325 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialUnderstandingDll/MeshData>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1871217186_gshared (IIterableToIEnumerableAdapter_1_t1364795558 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.SpatialUnderstandingDll/MeshData>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1270898695 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1270898695 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1270898695 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2047738000_gshared (IIterableToIEnumerableAdapter_1_t2244913691 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.SpatialUnderstandingDllShapes/ShapeComponentConstraint>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2151016828 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2151016828 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2151016828 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.TimerScheduler/TimerData>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m143240901_gshared (IIterableToIEnumerableAdapter_1_t2059675446 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.TimerScheduler/TimerData>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1965778583 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1965778583 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1965778583 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.TimerScheduler/TimerIdPair>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3880924113_gshared (IIterableToIEnumerableAdapter_1_t3219251157 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.TimerScheduler/TimerIdPair>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3125354294 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3125354294 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3125354294 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.UX.SplinePoint>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m312399871_gshared (IIterableToIEnumerableAdapter_1_t2158001848 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.UX.SplinePoint>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2064104985 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2064104985 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2064104985 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2946806855_gshared (IIterableToIEnumerableAdapter_1_t391484503 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t297587640 * L_3 = (IIteratorToIEnumeratorAdapter_1_t297587640 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t297587640 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m728793190_gshared (IIterableToIEnumerableAdapter_1_t1335827848 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1241930985 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1241930985 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1241930985 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Mono.Security.Interface.CipherSuiteCode>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3816991457_gshared (IIterableToIEnumerableAdapter_1_t3030489463 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Mono.Security.Interface.CipherSuiteCode>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2936592600 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2936592600 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2936592600 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3783345380_gshared (IIterableToIEnumerableAdapter_1_t3302632160 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3208735297 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3208735297 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3208735297 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Mono.Security.Uri/UriScheme>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1243054257_gshared (IIterableToIEnumerableAdapter_1_t870766298 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Mono.Security.Uri/UriScheme>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t776869435 * L_3 = (IIteratorToIEnumeratorAdapter_1_t776869435 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t776869435 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<MS.Internal.Xml.Cache.XPathNode>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m875494172_gshared (IIterableToIEnumerableAdapter_1_t211032832 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<MS.Internal.Xml.Cache.XPathNode>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t117135969 * L_3 = (IIteratorToIEnumeratorAdapter_1_t117135969 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t117135969 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<MS.Internal.Xml.Cache.XPathNodeRef>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3145222548_gshared (IIterableToIEnumerableAdapter_1_t1501148974 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<MS.Internal.Xml.Cache.XPathNodeRef>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1407252111 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1407252111 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1407252111 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<MS.Internal.Xml.XPath.Operator/Op>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m4233445051_gshared (IIterableToIEnumerableAdapter_1_t49765125 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<MS.Internal.Xml.XPath.Operator/Op>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t4250835558 * L_3 = (IIteratorToIEnumeratorAdapter_1_t4250835558 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t4250835558 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.JsonPosition>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m784846363_gshared (IIterableToIEnumerableAdapter_1_t530987670 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Newtonsoft.Json.JsonPosition>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t437090807 * L_3 = (IIteratorToIEnumeratorAdapter_1_t437090807 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t437090807 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.JsonWriter/State>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m4029613773_gshared (IIterableToIEnumerableAdapter_1_t598626605 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Newtonsoft.Json.JsonWriter/State>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t504729742 * L_3 = (IIteratorToIEnumeratorAdapter_1_t504729742 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t504729742 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1363406182_gshared (IIterableToIEnumerableAdapter_1_t1025321618 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Newtonsoft.Json.Linq.JTokenType>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t931424755 * L_3 = (IIteratorToIEnumeratorAdapter_1_t931424755 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t931424755 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.ReadType>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m4013226657_gshared (IIterableToIEnumerableAdapter_1_t2638713832 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Newtonsoft.Json.ReadType>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2544816969 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2544816969 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2544816969 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3416980396_gshared (IIterableToIEnumerableAdapter_1_t988501917 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t894605054 * L_3 = (IIteratorToIEnumeratorAdapter_1_t894605054 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t894605054 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.Serialization.ResolverContractKey>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2616339538_gshared (IIterableToIEnumerableAdapter_1_t1295811243 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Newtonsoft.Json.Serialization.ResolverContractKey>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1201914380 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1201914380 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1201914380 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m804853807_gshared (IIterableToIEnumerableAdapter_1_t2583234012 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2489337149 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2489337149 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2489337149 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1012979729_gshared (IIterableToIEnumerableAdapter_1_t3096877156 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3002980293 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3002980293 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3002980293 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.AppContext/SwitchValueState>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1588164342_gshared (IIterableToIEnumerableAdapter_1_t808211423 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.AppContext/SwitchValueState>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t714314560 * L_3 = (IIteratorToIEnumeratorAdapter_1_t714314560 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t714314560 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.ArraySegment`1<System.Byte>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3009188430_gshared (IIterableToIEnumerableAdapter_1_t2581488239 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.ArraySegment`1<System.Byte>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2487591376 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2487591376 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2487591376 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Boolean>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1406459206_gshared (IIterableToIEnumerableAdapter_1_t2395215217 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Boolean>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2301318354 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2301318354 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2301318354 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Byte>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m596549953_gshared (IIterableToIEnumerableAdapter_1_t3432223628 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Byte>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3338326765 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3338326765 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3338326765 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Char>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1268911933_gshared (IIterableToIEnumerableAdapter_1_t1637420426 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Char>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1543523563 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1543523563 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1543523563 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.DictionaryEntry>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m4127391819_gshared (IIterableToIEnumerableAdapter_1_t1126935594 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.DictionaryEntry>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1033038731 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1033038731 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1033038731 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1437943689_gshared (IIterableToIEnumerableAdapter_1_t4056732158 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3962835295 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3962835295 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3962835295 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m914075430_gshared (IIterableToIEnumerableAdapter_1_t92757476 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t4293827909 * L_3 = (IIteratorToIEnumeratorAdapter_1_t4293827909 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t4293827909 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2560958827_gshared (IIterableToIEnumerableAdapter_1_t2159831731 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2065934868 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2065934868 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2065934868 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2170141322_gshared (IIterableToIEnumerableAdapter_1_t488053677 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t394156814 * L_3 = (IIteratorToIEnumeratorAdapter_1_t394156814 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t394156814 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m445402314_gshared (IIterableToIEnumerableAdapter_1_t2268747928 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2174851065 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2174851065 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2174851065 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2870997221_gshared (IIterableToIEnumerableAdapter_1_t1746948141 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1653051278 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1653051278 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1653051278 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2769531368_gshared (IIterableToIEnumerableAdapter_1_t1586643939 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1492747076 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1492747076 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1492747076 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Int64,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1070719587_gshared (IIterableToIEnumerableAdapter_1_t3760570392 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.Int64,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3666673529 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3666673529 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3666673529 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.IntPtr,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2119157602_gshared (IIterableToIEnumerableAdapter_1_t919796327 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.IntPtr,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t825899464 * L_3 = (IIteratorToIEnumeratorAdapter_1_t825899464 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t825899464 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1520015451_gshared (IIterableToIEnumerableAdapter_1_t1641124000 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1547227137 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1547227137 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1547227137 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3034594939_gshared (IIterableToIEnumerableAdapter_1_t1770284411 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1676387548 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1676387548 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1676387548 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.ReadType>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1015236369_gshared (IIterableToIEnumerableAdapter_1_t1306017308 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.ReadType>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1212120445 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1212120445 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1212120445 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2703196173_gshared (IIterableToIEnumerableAdapter_1_t1764180632 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1670283769 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1670283769 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1670283769 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.AppContext/SwitchValueState>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m4224403912_gshared (IIterableToIEnumerableAdapter_1_t3770482195 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.AppContext/SwitchValueState>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3676585332 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3676585332 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3676585332 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2462093087_gshared (IIterableToIEnumerableAdapter_1_t1062518693 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t968621830 * L_3 = (IIteratorToIEnumeratorAdapter_1_t968621830 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t968621830 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2453202825_gshared (IIterableToIEnumerableAdapter_1_t3916176481 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3822279618 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3822279618 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3822279618 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1823781731_gshared (IIterableToIEnumerableAdapter_1_t4245520317 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t4151623454 * L_3 = (IIteratorToIEnumeratorAdapter_1_t4151623454 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t4151623454 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m165555727_gshared (IIterableToIEnumerableAdapter_1_t4045336892 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3951440029 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3951440029 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3951440029 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m801717795_gshared (IIterableToIEnumerableAdapter_1_t394234239 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t300337376 * L_3 = (IIteratorToIEnumeratorAdapter_1_t300337376 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t300337376 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1390392155_gshared (IIterableToIEnumerableAdapter_1_t4253737224 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t4159840361 * L_3 = (IIteratorToIEnumeratorAdapter_1_t4159840361 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t4159840361 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1160798534_gshared (IIterableToIEnumerableAdapter_1_t1474094753 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1380197890 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1380197890 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1380197890 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
