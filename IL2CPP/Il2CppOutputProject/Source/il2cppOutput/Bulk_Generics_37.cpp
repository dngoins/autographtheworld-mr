﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.IO.FileSystemEnumerableIterator`1<System.Object>
struct FileSystemEnumerableIterator_1_t25181536;
// System.String
struct String_t;
// System.IO.SearchResultHandler`1<System.Object>
struct SearchResultHandler_1_t2377980137;
// System.Collections.Generic.List`1<System.IO.Directory/SearchData>
struct List_1_t4120301035;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.IO.Directory/SearchData
struct SearchData_t2648226293;
// Microsoft.Win32.Win32Native/WIN32_FIND_DATA
struct WIN32_FIND_DATA_t4232796738;
// Microsoft.Win32.SafeHandles.SafeFindHandle
struct SafeFindHandle_t2068834300;
// System.Runtime.InteropServices.SafeHandle
struct SafeHandle_t3273388951;
// System.IO.Iterator`1<System.Object>
struct Iterator_1_t3764629478;
// System.IO.SearchResult
struct SearchResult_t2600365382;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Threading.Thread
struct Thread_t2300836069;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t3512676632;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
struct IEnumerable_1_t1136082412;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t2702166353;
// System.Linq.CachingComparer`1<System.Object>
struct CachingComparer_1_t1378817919;
// System.Linq.CachingComparer`1<UnityEngine.Vector2>
struct CachingComparer_1_t454941278;
// System.Linq.CachingComparer`1<UnityEngine.Vector3>
struct CachingComparer_1_t2021025219;
// System.Linq.CachingComparer`2<System.Object,System.Int32>
struct CachingComparer_2_t2712248853;
// System.Func`2<System.Object,System.Int32>
struct Func_2_t2317969963;
// System.Collections.Generic.IComparer`1<System.Int32>
struct IComparer_1_t4205211232;
// System.Linq.CachingComparer`2<System.Object,System.Object>
struct CachingComparer_2_t2841409264;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2447130374;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t39404347;
// System.Linq.CachingComparer`2<UnityEngine.Vector2,System.Single>
struct CachingComparer_2_t4066783831;
// System.Func`2<UnityEngine.Vector2,System.Single>
struct Func_2_t3672504941;
// System.Collections.Generic.IComparer`1<System.Single>
struct IComparer_1_t2651532253;
// System.Linq.CachingComparer`2<UnityEngine.Vector3,System.Single>
struct CachingComparer_2_t33381310;
// System.Func`2<UnityEngine.Vector3,System.Single>
struct Func_2_t3934069716;
// System.Linq.CachingComparerWithChild`2<System.Object,System.Int32>
struct CachingComparerWithChild_2_t2010013078;
// System.Linq.CachingComparerWithChild`2<System.Object,System.Object>
struct CachingComparerWithChild_2_t2139173489;
// System.Linq.CachingComparerWithChild`2<UnityEngine.Vector2,System.Single>
struct CachingComparerWithChild_2_t3364548056;
// System.Linq.CachingComparerWithChild`2<UnityEngine.Vector3,System.Single>
struct CachingComparerWithChild_2_t3626112831;
// System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct EmptyPartition_1_t2163426353;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t2962787787;
// System.Exception
struct Exception_t1436737249;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t118269214;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct List_1_t4002292061;
// System.Linq.EmptyPartition`1<System.Object>
struct EmptyPartition_1_t2713315198;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Linq.EmptyPartition`1<UnityEngine.Ray>
struct EmptyPartition_1_t3419060527;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Ray>
struct IEnumerator_1_t4218421961;
// UnityEngine.Ray[]
struct RayU5BU5D_t1836217960;
// System.Collections.Generic.List`1<UnityEngine.Ray>
struct List_1_t962958939;
// System.Linq.Enumerable/<CastIterator>d__35`1<System.Collections.DictionaryEntry>
struct U3CCastIteratorU3Ed__35_1_t2794392488;
// System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>
struct IEnumerator_1_t3556546106;
// System.Linq.Enumerable/<CastIterator>d__35`1<System.Object>
struct U3CCastIteratorU3Ed__35_1_t2750523014;
// System.Linq.Enumerable/Iterator`1<System.Char>
struct Iterator_1_t2588820807;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t4067030938;
// System.Collections.Generic.IEnumerable`1<System.Char>
struct IEnumerable_1_t2614313359;
// System.Func`2<System.Char,System.Boolean>
struct Func_2_t148644517;
// System.Linq.Enumerable/Iterator`1<System.Collections.DictionaryEntry>
struct Iterator_1_t2078335975;
// System.Collections.Generic.IEnumerable`1<System.Collections.DictionaryEntry>
struct IEnumerable_1_t2103828527;
// System.Func`2<System.Collections.DictionaryEntry,System.Boolean>
struct Func_2_t837490053;
// System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Iterator_1_t1484577656;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t1510070208;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>
struct Func_2_t1033609360;
// System.Linq.Enumerable/Iterator`1<System.Object>
struct Iterator_1_t2034466501;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3759279471;
// System.Linq.Enumerable/Iterator`1<UnityEngine.Ray>
struct Iterator_1_t2740211830;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Ray>
struct IEnumerable_1_t2765704382;
// System.Func`2<UnityEngine.Ray,System.Boolean>
struct Func_2_t2751558106;
// System.Linq.Enumerable/SelectArrayIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct SelectArrayIterator_2_t1643067185;
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t4217117203;
// System.Func`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Func_2_t3270419407;
// System.Linq.Enumerable/SelectArrayIterator`2<System.Object,System.Object>
struct SelectArrayIterator_2_t819778152;
// System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct SelectEnumerableIterator_2_t760503204;
// System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct SelectEnumerableIterator_2_t956622511;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Func_2_t3466538714;
// System.Linq.Enumerable/SelectEnumerableIterator`2<System.Object,System.Object>
struct SelectEnumerableIterator_2_t4232181467;
// System.Linq.Enumerable/SelectIListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct SelectIListIterator_2_t130090036;
// System.Collections.Generic.IList`1<System.Collections.DictionaryEntry>
struct IList_1_t644328125;
// System.Linq.Enumerable/SelectIListIterator`2<System.Object,System.Object>
struct SelectIListIterator_2_t3601768299;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t600458651;
// System.Linq.Enumerable/SelectIPartitionIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct SelectIPartitionIterator_2_t2954477804;
// System.Linq.IPartition`1<System.Collections.DictionaryEntry>
struct IPartition_1_t1949326113;
// System.Linq.Enumerable/SelectIPartitionIterator`2<System.Object,System.Object>
struct SelectIPartitionIterator_2_t2131188771;
// System.Linq.IPartition`1<System.Object>
struct IPartition_1_t1905456639;
// System.Linq.Enumerable/SelectListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct SelectListIterator_2_t2565991656;
// System.Collections.Generic.List`1<System.Collections.DictionaryEntry>
struct List_1_t301083084;
// System.Linq.Enumerable/SelectListIterator`2<System.Object,System.Object>
struct SelectListIterator_2_t1742702623;
// System.Linq.Enumerable/SelectManySingleSelectorIterator`2<System.Object,System.Object>
struct SelectManySingleSelectorIterator_2_t963716220;
// System.Func`2<System.Object,System.Collections.Generic.IEnumerable`1<System.Object>>
struct Func_2_t1426983263;
// System.Linq.Enumerable/UnionIterator`1<System.Char>
struct UnionIterator_1_t3402001247;
// System.Collections.Generic.IEqualityComparer`1<System.Char>
struct IEqualityComparer_1_t1446825192;
// System.Linq.Set`1<System.Char>
struct Set_1_t329142533;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t811567916;
// System.Linq.Enumerable/UnionIterator`1<System.Object>
struct UnionIterator_1_t2847646941;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t892470886;
// System.Linq.Set`1<System.Object>
struct Set_1_t4069755523;
// System.Linq.Enumerable/UnionIterator2`1<System.Char>
struct UnionIterator2_1_t1321734732;
// System.Linq.Enumerable/UnionIterator2`1<System.Object>
struct UnionIterator2_1_t767380426;
// System.Linq.Enumerable/UnionIteratorN`1<System.Char>
struct UnionIteratorN_1_t1272659448;
// System.Linq.Enumerable/UnionIteratorN`1<System.Object>
struct UnionIteratorN_1_t718305142;
// System.Linq.IPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IPartition_1_t1355567794;
// System.Collections.IEnumerable
struct IEnumerable_t1941168011;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Linq.Set`1/Slot<System.Object>[]
struct SlotU5BU5D_t973212065;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Linq.Set`1/Slot<System.Char>[]
struct SlotU5BU5D_t1657544407;
// System.Linq.IPartition`1<UnityEngine.Ray>
struct IPartition_1_t2611201968;
// System.IO.Directory/SearchData[]
struct SearchDataU5BU5D_t2471894680;
// System.Collections.Generic.IEnumerable`1<System.Object>[]
struct IEnumerable_1U5BU5D_t2115075616;
// System.Collections.Generic.Marker[]
struct MarkerU5BU5D_t2881615980;
// System.Threading.InternalThread
struct InternalThread_t95296544;
// System.Security.Principal.IPrincipal
struct IPrincipal_t2343618843;
// System.MulticastDelegate
struct MulticastDelegate_t157516450;
// System.Threading.ExecutionContext
struct ExecutionContext_t1748372627;
// System.LocalDataStoreMgr
struct LocalDataStoreMgr_t1707895399;
// System.LocalDataStoreHolder
struct LocalDataStoreHolder_t2567786569;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo>
struct AsyncLocal_1_t2427220165;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Void
struct Void_t1185182177;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[][]
struct KeyValuePair_2U5BU5DU5BU5D_t2409249963;
// System.Object[][]
struct ObjectU5BU5DU5BU5D_t831815024;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;

extern RuntimeClass* List_1_t4120301035_il2cpp_TypeInfo_var;
extern RuntimeClass* Path_t1605229823_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern RuntimeClass* SearchData_t2648226293_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m3635573260_RuntimeMethod_var;
extern const uint32_t FileSystemEnumerableIterator_1__ctor_m3232840268_MetadataUsageId;
extern RuntimeClass* WIN32_FIND_DATA_t4232796738_il2cpp_TypeInfo_var;
extern RuntimeClass* MonoIO_t2601436415_il2cpp_TypeInfo_var;
extern RuntimeClass* SafeFindHandle_t2068834300_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Add_m1559120923_RuntimeMethod_var;
extern const uint32_t FileSystemEnumerableIterator_1_CommonInit_m3137623192_MetadataUsageId;
extern const uint32_t FileSystemEnumerableIterator_1__ctor_m2628860621_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m3638502404_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_m762780374_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m958122929_RuntimeMethod_var;
extern const uint32_t FileSystemEnumerableIterator_1_MoveNext_m808852106_MetadataUsageId;
extern RuntimeClass* SearchResult_t2600365382_il2cpp_TypeInfo_var;
extern const uint32_t FileSystemEnumerableIterator_1_CreateSearchResult_m1408355418_MetadataUsageId;
extern const RuntimeMethod* List_1_Insert_m2523363920_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3452614534;
extern const uint32_t FileSystemEnumerableIterator_1_AddSearchableDirsToStack_m2987866814_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3452614530;
extern const uint32_t FileSystemEnumerableIterator_1_NormalizeSearchPattern_m1627217068_MetadataUsageId;
extern const uint32_t FileSystemEnumerableIterator_1_GetNormalizedSearchCriteria_m63937011_MetadataUsageId;
extern const uint32_t FileSystemEnumerableIterator_1_GetFullSearchString_m3869162752_MetadataUsageId;
extern RuntimeClass* GC_t959872083_il2cpp_TypeInfo_var;
extern const uint32_t Iterator_1_Dispose_m1804293407_MetadataUsageId;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern const uint32_t Iterator_1_Dispose_m768625415_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const uint32_t Iterator_1_System_Collections_IEnumerator_Reset_m2231441267_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var;
extern const uint32_t EmptyPartition_1_get_Current_m456712_MetadataUsageId;
extern const uint32_t EmptyPartition_1_System_Collections_IEnumerator_get_Current_m150476704_MetadataUsageId;
extern const uint32_t EmptyPartition_1_TryGetElementAt_m2497710296_MetadataUsageId;
extern const uint32_t EmptyPartition_1_TryGetFirst_m559482675_MetadataUsageId;
extern const uint32_t EmptyPartition_1_TryGetLast_m1441299256_MetadataUsageId;
extern const uint32_t EmptyPartition_1_get_Current_m4265207326_MetadataUsageId;
extern const uint32_t EmptyPartition_1_System_Collections_IEnumerator_get_Current_m1601347863_MetadataUsageId;
extern const uint32_t EmptyPartition_1_TryGetElementAt_m3940163582_MetadataUsageId;
extern const uint32_t EmptyPartition_1_TryGetFirst_m593080765_MetadataUsageId;
extern const uint32_t EmptyPartition_1_TryGetLast_m2150537268_MetadataUsageId;
extern RuntimeClass* Ray_t3785851493_il2cpp_TypeInfo_var;
extern const uint32_t EmptyPartition_1_get_Current_m559141384_MetadataUsageId;
extern const uint32_t EmptyPartition_1_System_Collections_IEnumerator_get_Current_m2051492582_MetadataUsageId;
extern const uint32_t EmptyPartition_1_TryGetElementAt_m3815154181_MetadataUsageId;
extern const uint32_t EmptyPartition_1_TryGetFirst_m2649955484_MetadataUsageId;
extern const uint32_t EmptyPartition_1_TryGetLast_m3105323548_MetadataUsageId;
extern RuntimeClass* IEnumerable_t1941168011_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern const uint32_t U3CCastIteratorU3Ed__35_1_MoveNext_m936381739_MetadataUsageId;
extern RuntimeClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern const uint32_t U3CCastIteratorU3Ed__35_1_U3CU3Em__Finally1_m3047170860_MetadataUsageId;
extern const uint32_t U3CCastIteratorU3Ed__35_1_System_Collections_IEnumerator_Reset_m1143028353_MetadataUsageId;
extern const uint32_t U3CCastIteratorU3Ed__35_1_MoveNext_m2750562796_MetadataUsageId;
extern const uint32_t U3CCastIteratorU3Ed__35_1_U3CU3Em__Finally1_m855207833_MetadataUsageId;
extern const uint32_t U3CCastIteratorU3Ed__35_1_System_Collections_IEnumerator_Reset_m880153489_MetadataUsageId;
extern RuntimeClass* Char_t3634460470_il2cpp_TypeInfo_var;
extern const uint32_t Iterator_1_Dispose_m172456093_MetadataUsageId;
extern RuntimeClass* DictionaryEntry_t3123975638_il2cpp_TypeInfo_var;
extern const uint32_t Iterator_1_Dispose_m101278873_MetadataUsageId;
extern const uint32_t Iterator_1_Dispose_m3566902763_MetadataUsageId;
extern const uint32_t Iterator_1_Dispose_m274730813_MetadataUsageId;
extern const uint32_t Iterator_1_Dispose_m705651954_MetadataUsageId;
extern const uint32_t SelectArrayIterator_2_TryGetElementAt_m4000501117_MetadataUsageId;
extern const uint32_t SelectArrayIterator_2_TryGetElementAt_m3795460227_MetadataUsageId;
extern const uint32_t SelectEnumerableIterator_2_Dispose_m3862804282_MetadataUsageId;
extern const uint32_t SelectEnumerableIterator_2_MoveNext_m1500635119_MetadataUsageId;
extern const uint32_t SelectEnumerableIterator_2_ToArray_m1883646757_MetadataUsageId;
extern const uint32_t SelectEnumerableIterator_2_ToList_m1958580226_MetadataUsageId;
extern const uint32_t SelectEnumerableIterator_2_GetCount_m357576939_MetadataUsageId;
extern const uint32_t SelectEnumerableIterator_2_Dispose_m3571586847_MetadataUsageId;
extern const uint32_t SelectEnumerableIterator_2_MoveNext_m2562910218_MetadataUsageId;
extern const uint32_t SelectEnumerableIterator_2_ToArray_m675881335_MetadataUsageId;
extern const uint32_t SelectEnumerableIterator_2_ToList_m2709422520_MetadataUsageId;
extern const uint32_t SelectEnumerableIterator_2_GetCount_m2168038858_MetadataUsageId;
extern const uint32_t SelectEnumerableIterator_2_Dispose_m2790816898_MetadataUsageId;
extern const uint32_t SelectEnumerableIterator_2_MoveNext_m1563322355_MetadataUsageId;
extern const uint32_t SelectEnumerableIterator_2_ToArray_m3280562849_MetadataUsageId;
extern const uint32_t SelectEnumerableIterator_2_ToList_m1184928937_MetadataUsageId;
extern const uint32_t SelectEnumerableIterator_2_GetCount_m149151510_MetadataUsageId;
extern const uint32_t SelectIListIterator_2_MoveNext_m4196171901_MetadataUsageId;
extern const uint32_t SelectIListIterator_2_Dispose_m3100268919_MetadataUsageId;
extern const uint32_t SelectIListIterator_2_TryGetElementAt_m3366412195_MetadataUsageId;
extern const uint32_t SelectIListIterator_2_TryGetFirst_m4235623572_MetadataUsageId;
extern const uint32_t SelectIListIterator_2_TryGetLast_m3204209618_MetadataUsageId;
extern const uint32_t SelectIListIterator_2_MoveNext_m4209761780_MetadataUsageId;
extern const uint32_t SelectIListIterator_2_Dispose_m1388706584_MetadataUsageId;
extern const uint32_t SelectIListIterator_2_TryGetElementAt_m2987796583_MetadataUsageId;
extern const uint32_t SelectIListIterator_2_TryGetFirst_m3430082063_MetadataUsageId;
extern const uint32_t SelectIListIterator_2_TryGetLast_m2318008031_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_MoveNext_m2485470954_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_Dispose_m405696305_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_TryGetElementAt_m2269338986_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_TryGetFirst_m308715622_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_TryGetLast_m3505464966_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_LazyToArray_m2882688373_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_PreallocatingToArray_m4096222762_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_ToList_m531909262_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_GetCount_m3814325761_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_MoveNext_m3403270584_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_Dispose_m1751187625_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_TryGetElementAt_m1620325504_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_TryGetFirst_m2295832775_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_TryGetLast_m3162752631_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_LazyToArray_m24814827_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_PreallocatingToArray_m2279286047_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_ToList_m1963142377_MetadataUsageId;
extern const uint32_t SelectIPartitionIterator_2_GetCount_m842092247_MetadataUsageId;
extern const uint32_t SelectListIterator_2_TryGetElementAt_m876464443_MetadataUsageId;
extern const uint32_t SelectListIterator_2_TryGetFirst_m1293288055_MetadataUsageId;
extern const uint32_t SelectListIterator_2_TryGetLast_m3800486358_MetadataUsageId;
extern const uint32_t SelectListIterator_2_TryGetElementAt_m3949705530_MetadataUsageId;
extern const uint32_t SelectListIterator_2_TryGetFirst_m2392574845_MetadataUsageId;
extern const uint32_t SelectListIterator_2_TryGetLast_m491821547_MetadataUsageId;
extern const uint32_t SelectManySingleSelectorIterator_2_Dispose_m3642031398_MetadataUsageId;
extern const uint32_t SelectManySingleSelectorIterator_2_GetCount_m3920743042_MetadataUsageId;
extern const uint32_t SelectManySingleSelectorIterator_2_MoveNext_m1777559799_MetadataUsageId;
extern RuntimeClass* ArrayBuilder_1_t3343055733_il2cpp_TypeInfo_var;
extern const RuntimeMethod* ArrayBuilder_1_get_Item_m3563499930_RuntimeMethod_var;
extern const RuntimeMethod* ArrayBuilder_1_get_Count_m2587795986_RuntimeMethod_var;
extern const uint32_t SelectManySingleSelectorIterator_2_ToArray_m2521061925_MetadataUsageId;
extern const uint32_t SelectManySingleSelectorIterator_2_ToList_m2597685119_MetadataUsageId;
extern const uint32_t UnionIterator_1_Dispose_m3312131536_MetadataUsageId;
extern const uint32_t UnionIterator_1_SetEnumerator_m1125297949_MetadataUsageId;
extern const uint32_t UnionIterator_1_GetNext_m1279570535_MetadataUsageId;
extern const uint32_t UnionIterator_1_MoveNext_m375289487_MetadataUsageId;
extern const uint32_t UnionIterator_1_FillSet_m2793201565_MetadataUsageId;
extern const uint32_t UnionIterator_1_Dispose_m745608159_MetadataUsageId;
extern const uint32_t UnionIterator_1_SetEnumerator_m3710368852_MetadataUsageId;
extern const uint32_t UnionIterator_1_GetNext_m235872769_MetadataUsageId;
extern const uint32_t UnionIterator_1_MoveNext_m4242229629_MetadataUsageId;
extern const uint32_t UnionIterator_1_FillSet_m552649502_MetadataUsageId;
struct Exception_t1436737249_marshaled_pinvoke;
struct Exception_t1436737249_marshaled_com;

struct StringU5BU5D_t1281789340;
struct CharU5BU5D_t3528271667;
struct ObjectU5BU5D_t2843939325;
struct Vector2U5BU5D_t1457185986;
struct Vector3U5BU5D_t1718750761;
struct KeyValuePair_2U5BU5D_t118269214;
struct RayU5BU5D_t1836217960;
struct DictionaryEntryU5BU5D_t4217117203;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CACHINGCOMPARER_1_T2021025219_H
#define CACHINGCOMPARER_1_T2021025219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.CachingComparer`1<UnityEngine.Vector3>
struct  CachingComparer_1_t2021025219  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHINGCOMPARER_1_T2021025219_H
#ifndef CACHINGCOMPARER_1_T1378817919_H
#define CACHINGCOMPARER_1_T1378817919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.CachingComparer`1<System.Object>
struct  CachingComparer_1_t1378817919  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHINGCOMPARER_1_T1378817919_H
#ifndef CACHINGCOMPARER_1_T454941278_H
#define CACHINGCOMPARER_1_T454941278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.CachingComparer`1<UnityEngine.Vector2>
struct  CachingComparer_1_t454941278  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHINGCOMPARER_1_T454941278_H
#ifndef EMPTYPARTITION_1_T2163426353_H
#define EMPTYPARTITION_1_T2163426353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  EmptyPartition_1_t2163426353  : public RuntimeObject
{
public:

public:
};

struct EmptyPartition_1_t2163426353_StaticFields
{
public:
	// System.Linq.IPartition`1<TElement> System.Linq.EmptyPartition`1::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(EmptyPartition_1_t2163426353_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYPARTITION_1_T2163426353_H
#ifndef PATH_T1605229823_H
#define PATH_T1605229823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Path
struct  Path_t1605229823  : public RuntimeObject
{
public:

public:
};

struct Path_t1605229823_StaticFields
{
public:
	// System.Char[] System.IO.Path::InvalidPathChars
	CharU5BU5D_t3528271667* ___InvalidPathChars_0;
	// System.Char System.IO.Path::AltDirectorySeparatorChar
	Il2CppChar ___AltDirectorySeparatorChar_1;
	// System.Char System.IO.Path::DirectorySeparatorChar
	Il2CppChar ___DirectorySeparatorChar_2;
	// System.Char System.IO.Path::PathSeparator
	Il2CppChar ___PathSeparator_3;
	// System.String System.IO.Path::DirectorySeparatorStr
	String_t* ___DirectorySeparatorStr_4;
	// System.Char System.IO.Path::VolumeSeparatorChar
	Il2CppChar ___VolumeSeparatorChar_5;
	// System.Char[] System.IO.Path::PathSeparatorChars
	CharU5BU5D_t3528271667* ___PathSeparatorChars_6;
	// System.Boolean System.IO.Path::dirEqualsVolume
	bool ___dirEqualsVolume_7;
	// System.Char[] System.IO.Path::trimEndCharsWindows
	CharU5BU5D_t3528271667* ___trimEndCharsWindows_8;
	// System.Char[] System.IO.Path::trimEndCharsUnix
	CharU5BU5D_t3528271667* ___trimEndCharsUnix_9;

public:
	inline static int32_t get_offset_of_InvalidPathChars_0() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___InvalidPathChars_0)); }
	inline CharU5BU5D_t3528271667* get_InvalidPathChars_0() const { return ___InvalidPathChars_0; }
	inline CharU5BU5D_t3528271667** get_address_of_InvalidPathChars_0() { return &___InvalidPathChars_0; }
	inline void set_InvalidPathChars_0(CharU5BU5D_t3528271667* value)
	{
		___InvalidPathChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidPathChars_0), value);
	}

	inline static int32_t get_offset_of_AltDirectorySeparatorChar_1() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___AltDirectorySeparatorChar_1)); }
	inline Il2CppChar get_AltDirectorySeparatorChar_1() const { return ___AltDirectorySeparatorChar_1; }
	inline Il2CppChar* get_address_of_AltDirectorySeparatorChar_1() { return &___AltDirectorySeparatorChar_1; }
	inline void set_AltDirectorySeparatorChar_1(Il2CppChar value)
	{
		___AltDirectorySeparatorChar_1 = value;
	}

	inline static int32_t get_offset_of_DirectorySeparatorChar_2() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___DirectorySeparatorChar_2)); }
	inline Il2CppChar get_DirectorySeparatorChar_2() const { return ___DirectorySeparatorChar_2; }
	inline Il2CppChar* get_address_of_DirectorySeparatorChar_2() { return &___DirectorySeparatorChar_2; }
	inline void set_DirectorySeparatorChar_2(Il2CppChar value)
	{
		___DirectorySeparatorChar_2 = value;
	}

	inline static int32_t get_offset_of_PathSeparator_3() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___PathSeparator_3)); }
	inline Il2CppChar get_PathSeparator_3() const { return ___PathSeparator_3; }
	inline Il2CppChar* get_address_of_PathSeparator_3() { return &___PathSeparator_3; }
	inline void set_PathSeparator_3(Il2CppChar value)
	{
		___PathSeparator_3 = value;
	}

	inline static int32_t get_offset_of_DirectorySeparatorStr_4() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___DirectorySeparatorStr_4)); }
	inline String_t* get_DirectorySeparatorStr_4() const { return ___DirectorySeparatorStr_4; }
	inline String_t** get_address_of_DirectorySeparatorStr_4() { return &___DirectorySeparatorStr_4; }
	inline void set_DirectorySeparatorStr_4(String_t* value)
	{
		___DirectorySeparatorStr_4 = value;
		Il2CppCodeGenWriteBarrier((&___DirectorySeparatorStr_4), value);
	}

	inline static int32_t get_offset_of_VolumeSeparatorChar_5() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___VolumeSeparatorChar_5)); }
	inline Il2CppChar get_VolumeSeparatorChar_5() const { return ___VolumeSeparatorChar_5; }
	inline Il2CppChar* get_address_of_VolumeSeparatorChar_5() { return &___VolumeSeparatorChar_5; }
	inline void set_VolumeSeparatorChar_5(Il2CppChar value)
	{
		___VolumeSeparatorChar_5 = value;
	}

	inline static int32_t get_offset_of_PathSeparatorChars_6() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___PathSeparatorChars_6)); }
	inline CharU5BU5D_t3528271667* get_PathSeparatorChars_6() const { return ___PathSeparatorChars_6; }
	inline CharU5BU5D_t3528271667** get_address_of_PathSeparatorChars_6() { return &___PathSeparatorChars_6; }
	inline void set_PathSeparatorChars_6(CharU5BU5D_t3528271667* value)
	{
		___PathSeparatorChars_6 = value;
		Il2CppCodeGenWriteBarrier((&___PathSeparatorChars_6), value);
	}

	inline static int32_t get_offset_of_dirEqualsVolume_7() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___dirEqualsVolume_7)); }
	inline bool get_dirEqualsVolume_7() const { return ___dirEqualsVolume_7; }
	inline bool* get_address_of_dirEqualsVolume_7() { return &___dirEqualsVolume_7; }
	inline void set_dirEqualsVolume_7(bool value)
	{
		___dirEqualsVolume_7 = value;
	}

	inline static int32_t get_offset_of_trimEndCharsWindows_8() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___trimEndCharsWindows_8)); }
	inline CharU5BU5D_t3528271667* get_trimEndCharsWindows_8() const { return ___trimEndCharsWindows_8; }
	inline CharU5BU5D_t3528271667** get_address_of_trimEndCharsWindows_8() { return &___trimEndCharsWindows_8; }
	inline void set_trimEndCharsWindows_8(CharU5BU5D_t3528271667* value)
	{
		___trimEndCharsWindows_8 = value;
		Il2CppCodeGenWriteBarrier((&___trimEndCharsWindows_8), value);
	}

	inline static int32_t get_offset_of_trimEndCharsUnix_9() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___trimEndCharsUnix_9)); }
	inline CharU5BU5D_t3528271667* get_trimEndCharsUnix_9() const { return ___trimEndCharsUnix_9; }
	inline CharU5BU5D_t3528271667** get_address_of_trimEndCharsUnix_9() { return &___trimEndCharsUnix_9; }
	inline void set_trimEndCharsUnix_9(CharU5BU5D_t3528271667* value)
	{
		___trimEndCharsUnix_9 = value;
		Il2CppCodeGenWriteBarrier((&___trimEndCharsUnix_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATH_T1605229823_H
#ifndef U3CCASTITERATORU3ED__35_1_T2750523014_H
#define U3CCASTITERATORU3ED__35_1_T2750523014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/<CastIterator>d__35`1<System.Object>
struct  U3CCastIteratorU3Ed__35_1_t2750523014  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/<CastIterator>d__35`1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// TResult System.Linq.Enumerable/<CastIterator>d__35`1::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 System.Linq.Enumerable/<CastIterator>d__35`1::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.IEnumerable System.Linq.Enumerable/<CastIterator>d__35`1::source
	RuntimeObject* ___source_3;
	// System.Collections.IEnumerable System.Linq.Enumerable/<CastIterator>d__35`1::<>3__source
	RuntimeObject* ___U3CU3E3__source_4;
	// System.Collections.IEnumerator System.Linq.Enumerable/<CastIterator>d__35`1::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCastIteratorU3Ed__35_1_t2750523014, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCastIteratorU3Ed__35_1_t2750523014, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CCastIteratorU3Ed__35_1_t2750523014, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(U3CCastIteratorU3Ed__35_1_t2750523014, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((&___source_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__source_4() { return static_cast<int32_t>(offsetof(U3CCastIteratorU3Ed__35_1_t2750523014, ___U3CU3E3__source_4)); }
	inline RuntimeObject* get_U3CU3E3__source_4() const { return ___U3CU3E3__source_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__source_4() { return &___U3CU3E3__source_4; }
	inline void set_U3CU3E3__source_4(RuntimeObject* value)
	{
		___U3CU3E3__source_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__source_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CCastIteratorU3Ed__35_1_t2750523014, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCASTITERATORU3ED__35_1_T2750523014_H
#ifndef ITERATOR_1_T2588820807_H
#define ITERATOR_1_T2588820807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/Iterator`1<System.Char>
struct  Iterator_1_t2588820807  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::_threadId
	int32_t ____threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::_state
	int32_t ____state_1;
	// TSource System.Linq.Enumerable/Iterator`1::_current
	Il2CppChar ____current_2;

public:
	inline static int32_t get_offset_of__threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t2588820807, ____threadId_0)); }
	inline int32_t get__threadId_0() const { return ____threadId_0; }
	inline int32_t* get_address_of__threadId_0() { return &____threadId_0; }
	inline void set__threadId_0(int32_t value)
	{
		____threadId_0 = value;
	}

	inline static int32_t get_offset_of__state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t2588820807, ____state_1)); }
	inline int32_t get__state_1() const { return ____state_1; }
	inline int32_t* get_address_of__state_1() { return &____state_1; }
	inline void set__state_1(int32_t value)
	{
		____state_1 = value;
	}

	inline static int32_t get_offset_of__current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t2588820807, ____current_2)); }
	inline Il2CppChar get__current_2() const { return ____current_2; }
	inline Il2CppChar* get_address_of__current_2() { return &____current_2; }
	inline void set__current_2(Il2CppChar value)
	{
		____current_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITERATOR_1_T2588820807_H
#ifndef LIST_1_T962958939_H
#define LIST_1_T962958939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Ray>
struct  List_1_t962958939  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	RayU5BU5D_t1836217960* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t962958939, ____items_1)); }
	inline RayU5BU5D_t1836217960* get__items_1() const { return ____items_1; }
	inline RayU5BU5D_t1836217960** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(RayU5BU5D_t1836217960* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t962958939, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t962958939, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t962958939, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t962958939_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	RayU5BU5D_t1836217960* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t962958939_StaticFields, ____emptyArray_5)); }
	inline RayU5BU5D_t1836217960* get__emptyArray_5() const { return ____emptyArray_5; }
	inline RayU5BU5D_t1836217960** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(RayU5BU5D_t1836217960* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T962958939_H
#ifndef SET_1_T4069755523_H
#define SET_1_T4069755523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Set`1<System.Object>
struct  Set_1_t4069755523  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEqualityComparer`1<TElement> System.Linq.Set`1::_comparer
	RuntimeObject* ____comparer_0;
	// System.Int32[] System.Linq.Set`1::_buckets
	Int32U5BU5D_t385246372* ____buckets_1;
	// System.Linq.Set`1/Slot<TElement>[] System.Linq.Set`1::_slots
	SlotU5BU5D_t973212065* ____slots_2;
	// System.Int32 System.Linq.Set`1::_count
	int32_t ____count_3;

public:
	inline static int32_t get_offset_of__comparer_0() { return static_cast<int32_t>(offsetof(Set_1_t4069755523, ____comparer_0)); }
	inline RuntimeObject* get__comparer_0() const { return ____comparer_0; }
	inline RuntimeObject** get_address_of__comparer_0() { return &____comparer_0; }
	inline void set__comparer_0(RuntimeObject* value)
	{
		____comparer_0 = value;
		Il2CppCodeGenWriteBarrier((&____comparer_0), value);
	}

	inline static int32_t get_offset_of__buckets_1() { return static_cast<int32_t>(offsetof(Set_1_t4069755523, ____buckets_1)); }
	inline Int32U5BU5D_t385246372* get__buckets_1() const { return ____buckets_1; }
	inline Int32U5BU5D_t385246372** get_address_of__buckets_1() { return &____buckets_1; }
	inline void set__buckets_1(Int32U5BU5D_t385246372* value)
	{
		____buckets_1 = value;
		Il2CppCodeGenWriteBarrier((&____buckets_1), value);
	}

	inline static int32_t get_offset_of__slots_2() { return static_cast<int32_t>(offsetof(Set_1_t4069755523, ____slots_2)); }
	inline SlotU5BU5D_t973212065* get__slots_2() const { return ____slots_2; }
	inline SlotU5BU5D_t973212065** get_address_of__slots_2() { return &____slots_2; }
	inline void set__slots_2(SlotU5BU5D_t973212065* value)
	{
		____slots_2 = value;
		Il2CppCodeGenWriteBarrier((&____slots_2), value);
	}

	inline static int32_t get_offset_of__count_3() { return static_cast<int32_t>(offsetof(Set_1_t4069755523, ____count_3)); }
	inline int32_t get__count_3() const { return ____count_3; }
	inline int32_t* get_address_of__count_3() { return &____count_3; }
	inline void set__count_3(int32_t value)
	{
		____count_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SET_1_T4069755523_H
#ifndef EXCEPTION_T1436737249_H
#define EXCEPTION_T1436737249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1436737249  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t1436737249 * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____innerException_4)); }
	inline Exception_t1436737249 * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t1436737249 ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t1436737249 * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t1436737249_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t1436737249_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t1436737249_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1436737249_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t1436737249_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1436737249_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T1436737249_H
#ifndef SET_1_T329142533_H
#define SET_1_T329142533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Set`1<System.Char>
struct  Set_1_t329142533  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEqualityComparer`1<TElement> System.Linq.Set`1::_comparer
	RuntimeObject* ____comparer_0;
	// System.Int32[] System.Linq.Set`1::_buckets
	Int32U5BU5D_t385246372* ____buckets_1;
	// System.Linq.Set`1/Slot<TElement>[] System.Linq.Set`1::_slots
	SlotU5BU5D_t1657544407* ____slots_2;
	// System.Int32 System.Linq.Set`1::_count
	int32_t ____count_3;

public:
	inline static int32_t get_offset_of__comparer_0() { return static_cast<int32_t>(offsetof(Set_1_t329142533, ____comparer_0)); }
	inline RuntimeObject* get__comparer_0() const { return ____comparer_0; }
	inline RuntimeObject** get_address_of__comparer_0() { return &____comparer_0; }
	inline void set__comparer_0(RuntimeObject* value)
	{
		____comparer_0 = value;
		Il2CppCodeGenWriteBarrier((&____comparer_0), value);
	}

	inline static int32_t get_offset_of__buckets_1() { return static_cast<int32_t>(offsetof(Set_1_t329142533, ____buckets_1)); }
	inline Int32U5BU5D_t385246372* get__buckets_1() const { return ____buckets_1; }
	inline Int32U5BU5D_t385246372** get_address_of__buckets_1() { return &____buckets_1; }
	inline void set__buckets_1(Int32U5BU5D_t385246372* value)
	{
		____buckets_1 = value;
		Il2CppCodeGenWriteBarrier((&____buckets_1), value);
	}

	inline static int32_t get_offset_of__slots_2() { return static_cast<int32_t>(offsetof(Set_1_t329142533, ____slots_2)); }
	inline SlotU5BU5D_t1657544407* get__slots_2() const { return ____slots_2; }
	inline SlotU5BU5D_t1657544407** get_address_of__slots_2() { return &____slots_2; }
	inline void set__slots_2(SlotU5BU5D_t1657544407* value)
	{
		____slots_2 = value;
		Il2CppCodeGenWriteBarrier((&____slots_2), value);
	}

	inline static int32_t get_offset_of__count_3() { return static_cast<int32_t>(offsetof(Set_1_t329142533, ____count_3)); }
	inline int32_t get__count_3() const { return ____count_3; }
	inline int32_t* get_address_of__count_3() { return &____count_3; }
	inline void set__count_3(int32_t value)
	{
		____count_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SET_1_T329142533_H
#ifndef LIST_1_T811567916_H
#define LIST_1_T811567916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Char>
struct  List_1_t811567916  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	CharU5BU5D_t3528271667* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t811567916, ____items_1)); }
	inline CharU5BU5D_t3528271667* get__items_1() const { return ____items_1; }
	inline CharU5BU5D_t3528271667** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(CharU5BU5D_t3528271667* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t811567916, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t811567916, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t811567916, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t811567916_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	CharU5BU5D_t3528271667* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t811567916_StaticFields, ____emptyArray_5)); }
	inline CharU5BU5D_t3528271667* get__emptyArray_5() const { return ____emptyArray_5; }
	inline CharU5BU5D_t3528271667** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(CharU5BU5D_t3528271667* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T811567916_H
#ifndef LIST_1_T257213610_H
#define LIST_1_T257213610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Object>
struct  List_1_t257213610  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t2843939325* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____items_1)); }
	inline ObjectU5BU5D_t2843939325* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t2843939325** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t2843939325* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t257213610_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t2843939325* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t257213610_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t2843939325* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t2843939325** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t2843939325* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T257213610_H
#ifndef EMPTYPARTITION_1_T3419060527_H
#define EMPTYPARTITION_1_T3419060527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.EmptyPartition`1<UnityEngine.Ray>
struct  EmptyPartition_1_t3419060527  : public RuntimeObject
{
public:

public:
};

struct EmptyPartition_1_t3419060527_StaticFields
{
public:
	// System.Linq.IPartition`1<TElement> System.Linq.EmptyPartition`1::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(EmptyPartition_1_t3419060527_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYPARTITION_1_T3419060527_H
#ifndef LIST_1_T4002292061_H
#define LIST_1_T4002292061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  List_1_t4002292061  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_t118269214* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4002292061, ____items_1)); }
	inline KeyValuePair_2U5BU5D_t118269214* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_t118269214** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_t118269214* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4002292061, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4002292061, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4002292061, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t4002292061_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	KeyValuePair_2U5BU5D_t118269214* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4002292061_StaticFields, ____emptyArray_5)); }
	inline KeyValuePair_2U5BU5D_t118269214* get__emptyArray_5() const { return ____emptyArray_5; }
	inline KeyValuePair_2U5BU5D_t118269214** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(KeyValuePair_2U5BU5D_t118269214* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4002292061_H
#ifndef EMPTYPARTITION_1_T2713315198_H
#define EMPTYPARTITION_1_T2713315198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.EmptyPartition`1<System.Object>
struct  EmptyPartition_1_t2713315198  : public RuntimeObject
{
public:

public:
};

struct EmptyPartition_1_t2713315198_StaticFields
{
public:
	// System.Linq.IPartition`1<TElement> System.Linq.EmptyPartition`1::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(EmptyPartition_1_t2713315198_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYPARTITION_1_T2713315198_H
#ifndef ITERATOR_1_T2034466501_H
#define ITERATOR_1_T2034466501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/Iterator`1<System.Object>
struct  Iterator_1_t2034466501  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::_threadId
	int32_t ____threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::_state
	int32_t ____state_1;
	// TSource System.Linq.Enumerable/Iterator`1::_current
	RuntimeObject * ____current_2;

public:
	inline static int32_t get_offset_of__threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t2034466501, ____threadId_0)); }
	inline int32_t get__threadId_0() const { return ____threadId_0; }
	inline int32_t* get_address_of__threadId_0() { return &____threadId_0; }
	inline void set__threadId_0(int32_t value)
	{
		____threadId_0 = value;
	}

	inline static int32_t get_offset_of__state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t2034466501, ____state_1)); }
	inline int32_t get__state_1() const { return ____state_1; }
	inline int32_t* get_address_of__state_1() { return &____state_1; }
	inline void set__state_1(int32_t value)
	{
		____state_1 = value;
	}

	inline static int32_t get_offset_of__current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t2034466501, ____current_2)); }
	inline RuntimeObject * get__current_2() const { return ____current_2; }
	inline RuntimeObject ** get_address_of__current_2() { return &____current_2; }
	inline void set__current_2(RuntimeObject * value)
	{
		____current_2 = value;
		Il2CppCodeGenWriteBarrier((&____current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITERATOR_1_T2034466501_H
#ifndef ITERATOR_1_T3764629478_H
#define ITERATOR_1_T3764629478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Iterator`1<System.Object>
struct  Iterator_1_t3764629478  : public RuntimeObject
{
public:
	// System.Int32 System.IO.Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.IO.Iterator`1::state
	int32_t ___state_1;
	// TSource System.IO.Iterator`1::current
	RuntimeObject * ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t3764629478, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t3764629478, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t3764629478, ___current_2)); }
	inline RuntimeObject * get_current_2() const { return ___current_2; }
	inline RuntimeObject ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(RuntimeObject * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITERATOR_1_T3764629478_H
#ifndef SEARCHRESULT_T2600365382_H
#define SEARCHRESULT_T2600365382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.SearchResult
struct  SearchResult_t2600365382  : public RuntimeObject
{
public:
	// System.String System.IO.SearchResult::fullPath
	String_t* ___fullPath_0;
	// System.String System.IO.SearchResult::userPath
	String_t* ___userPath_1;
	// Microsoft.Win32.Win32Native/WIN32_FIND_DATA System.IO.SearchResult::findData
	WIN32_FIND_DATA_t4232796738 * ___findData_2;

public:
	inline static int32_t get_offset_of_fullPath_0() { return static_cast<int32_t>(offsetof(SearchResult_t2600365382, ___fullPath_0)); }
	inline String_t* get_fullPath_0() const { return ___fullPath_0; }
	inline String_t** get_address_of_fullPath_0() { return &___fullPath_0; }
	inline void set_fullPath_0(String_t* value)
	{
		___fullPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___fullPath_0), value);
	}

	inline static int32_t get_offset_of_userPath_1() { return static_cast<int32_t>(offsetof(SearchResult_t2600365382, ___userPath_1)); }
	inline String_t* get_userPath_1() const { return ___userPath_1; }
	inline String_t** get_address_of_userPath_1() { return &___userPath_1; }
	inline void set_userPath_1(String_t* value)
	{
		___userPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___userPath_1), value);
	}

	inline static int32_t get_offset_of_findData_2() { return static_cast<int32_t>(offsetof(SearchResult_t2600365382, ___findData_2)); }
	inline WIN32_FIND_DATA_t4232796738 * get_findData_2() const { return ___findData_2; }
	inline WIN32_FIND_DATA_t4232796738 ** get_address_of_findData_2() { return &___findData_2; }
	inline void set_findData_2(WIN32_FIND_DATA_t4232796738 * value)
	{
		___findData_2 = value;
		Il2CppCodeGenWriteBarrier((&___findData_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHRESULT_T2600365382_H
#ifndef LIST_1_T4120301035_H
#define LIST_1_T4120301035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.IO.Directory/SearchData>
struct  List_1_t4120301035  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SearchDataU5BU5D_t2471894680* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4120301035, ____items_1)); }
	inline SearchDataU5BU5D_t2471894680* get__items_1() const { return ____items_1; }
	inline SearchDataU5BU5D_t2471894680** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SearchDataU5BU5D_t2471894680* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4120301035, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4120301035, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4120301035, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t4120301035_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SearchDataU5BU5D_t2471894680* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4120301035_StaticFields, ____emptyArray_5)); }
	inline SearchDataU5BU5D_t2471894680* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SearchDataU5BU5D_t2471894680** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SearchDataU5BU5D_t2471894680* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4120301035_H
#ifndef SEARCHRESULTHANDLER_1_T2377980137_H
#define SEARCHRESULTHANDLER_1_T2377980137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.SearchResultHandler`1<System.Object>
struct  SearchResultHandler_1_t2377980137  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHRESULTHANDLER_1_T2377980137_H
#ifndef LIST_1_T301083084_H
#define LIST_1_T301083084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Collections.DictionaryEntry>
struct  List_1_t301083084  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	DictionaryEntryU5BU5D_t4217117203* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t301083084, ____items_1)); }
	inline DictionaryEntryU5BU5D_t4217117203* get__items_1() const { return ____items_1; }
	inline DictionaryEntryU5BU5D_t4217117203** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(DictionaryEntryU5BU5D_t4217117203* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t301083084, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t301083084, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t301083084, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t301083084_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	DictionaryEntryU5BU5D_t4217117203* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t301083084_StaticFields, ____emptyArray_5)); }
	inline DictionaryEntryU5BU5D_t4217117203* get__emptyArray_5() const { return ____emptyArray_5; }
	inline DictionaryEntryU5BU5D_t4217117203** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(DictionaryEntryU5BU5D_t4217117203* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T301083084_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef WIN32_FIND_DATA_T4232796738_H
#define WIN32_FIND_DATA_T4232796738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.Win32.Win32Native/WIN32_FIND_DATA
struct  WIN32_FIND_DATA_t4232796738  : public RuntimeObject
{
public:
	// System.Int32 Microsoft.Win32.Win32Native/WIN32_FIND_DATA::dwFileAttributes
	int32_t ___dwFileAttributes_0;
	// System.UInt32 Microsoft.Win32.Win32Native/WIN32_FIND_DATA::ftCreationTime_dwLowDateTime
	uint32_t ___ftCreationTime_dwLowDateTime_1;
	// System.UInt32 Microsoft.Win32.Win32Native/WIN32_FIND_DATA::ftCreationTime_dwHighDateTime
	uint32_t ___ftCreationTime_dwHighDateTime_2;
	// System.UInt32 Microsoft.Win32.Win32Native/WIN32_FIND_DATA::ftLastAccessTime_dwLowDateTime
	uint32_t ___ftLastAccessTime_dwLowDateTime_3;
	// System.UInt32 Microsoft.Win32.Win32Native/WIN32_FIND_DATA::ftLastAccessTime_dwHighDateTime
	uint32_t ___ftLastAccessTime_dwHighDateTime_4;
	// System.UInt32 Microsoft.Win32.Win32Native/WIN32_FIND_DATA::ftLastWriteTime_dwLowDateTime
	uint32_t ___ftLastWriteTime_dwLowDateTime_5;
	// System.UInt32 Microsoft.Win32.Win32Native/WIN32_FIND_DATA::ftLastWriteTime_dwHighDateTime
	uint32_t ___ftLastWriteTime_dwHighDateTime_6;
	// System.Int32 Microsoft.Win32.Win32Native/WIN32_FIND_DATA::nFileSizeHigh
	int32_t ___nFileSizeHigh_7;
	// System.Int32 Microsoft.Win32.Win32Native/WIN32_FIND_DATA::nFileSizeLow
	int32_t ___nFileSizeLow_8;
	// System.Int32 Microsoft.Win32.Win32Native/WIN32_FIND_DATA::dwReserved0
	int32_t ___dwReserved0_9;
	// System.Int32 Microsoft.Win32.Win32Native/WIN32_FIND_DATA::dwReserved1
	int32_t ___dwReserved1_10;
	// System.String Microsoft.Win32.Win32Native/WIN32_FIND_DATA::cFileName
	String_t* ___cFileName_11;
	// System.String Microsoft.Win32.Win32Native/WIN32_FIND_DATA::cAlternateFileName
	String_t* ___cAlternateFileName_12;

public:
	inline static int32_t get_offset_of_dwFileAttributes_0() { return static_cast<int32_t>(offsetof(WIN32_FIND_DATA_t4232796738, ___dwFileAttributes_0)); }
	inline int32_t get_dwFileAttributes_0() const { return ___dwFileAttributes_0; }
	inline int32_t* get_address_of_dwFileAttributes_0() { return &___dwFileAttributes_0; }
	inline void set_dwFileAttributes_0(int32_t value)
	{
		___dwFileAttributes_0 = value;
	}

	inline static int32_t get_offset_of_ftCreationTime_dwLowDateTime_1() { return static_cast<int32_t>(offsetof(WIN32_FIND_DATA_t4232796738, ___ftCreationTime_dwLowDateTime_1)); }
	inline uint32_t get_ftCreationTime_dwLowDateTime_1() const { return ___ftCreationTime_dwLowDateTime_1; }
	inline uint32_t* get_address_of_ftCreationTime_dwLowDateTime_1() { return &___ftCreationTime_dwLowDateTime_1; }
	inline void set_ftCreationTime_dwLowDateTime_1(uint32_t value)
	{
		___ftCreationTime_dwLowDateTime_1 = value;
	}

	inline static int32_t get_offset_of_ftCreationTime_dwHighDateTime_2() { return static_cast<int32_t>(offsetof(WIN32_FIND_DATA_t4232796738, ___ftCreationTime_dwHighDateTime_2)); }
	inline uint32_t get_ftCreationTime_dwHighDateTime_2() const { return ___ftCreationTime_dwHighDateTime_2; }
	inline uint32_t* get_address_of_ftCreationTime_dwHighDateTime_2() { return &___ftCreationTime_dwHighDateTime_2; }
	inline void set_ftCreationTime_dwHighDateTime_2(uint32_t value)
	{
		___ftCreationTime_dwHighDateTime_2 = value;
	}

	inline static int32_t get_offset_of_ftLastAccessTime_dwLowDateTime_3() { return static_cast<int32_t>(offsetof(WIN32_FIND_DATA_t4232796738, ___ftLastAccessTime_dwLowDateTime_3)); }
	inline uint32_t get_ftLastAccessTime_dwLowDateTime_3() const { return ___ftLastAccessTime_dwLowDateTime_3; }
	inline uint32_t* get_address_of_ftLastAccessTime_dwLowDateTime_3() { return &___ftLastAccessTime_dwLowDateTime_3; }
	inline void set_ftLastAccessTime_dwLowDateTime_3(uint32_t value)
	{
		___ftLastAccessTime_dwLowDateTime_3 = value;
	}

	inline static int32_t get_offset_of_ftLastAccessTime_dwHighDateTime_4() { return static_cast<int32_t>(offsetof(WIN32_FIND_DATA_t4232796738, ___ftLastAccessTime_dwHighDateTime_4)); }
	inline uint32_t get_ftLastAccessTime_dwHighDateTime_4() const { return ___ftLastAccessTime_dwHighDateTime_4; }
	inline uint32_t* get_address_of_ftLastAccessTime_dwHighDateTime_4() { return &___ftLastAccessTime_dwHighDateTime_4; }
	inline void set_ftLastAccessTime_dwHighDateTime_4(uint32_t value)
	{
		___ftLastAccessTime_dwHighDateTime_4 = value;
	}

	inline static int32_t get_offset_of_ftLastWriteTime_dwLowDateTime_5() { return static_cast<int32_t>(offsetof(WIN32_FIND_DATA_t4232796738, ___ftLastWriteTime_dwLowDateTime_5)); }
	inline uint32_t get_ftLastWriteTime_dwLowDateTime_5() const { return ___ftLastWriteTime_dwLowDateTime_5; }
	inline uint32_t* get_address_of_ftLastWriteTime_dwLowDateTime_5() { return &___ftLastWriteTime_dwLowDateTime_5; }
	inline void set_ftLastWriteTime_dwLowDateTime_5(uint32_t value)
	{
		___ftLastWriteTime_dwLowDateTime_5 = value;
	}

	inline static int32_t get_offset_of_ftLastWriteTime_dwHighDateTime_6() { return static_cast<int32_t>(offsetof(WIN32_FIND_DATA_t4232796738, ___ftLastWriteTime_dwHighDateTime_6)); }
	inline uint32_t get_ftLastWriteTime_dwHighDateTime_6() const { return ___ftLastWriteTime_dwHighDateTime_6; }
	inline uint32_t* get_address_of_ftLastWriteTime_dwHighDateTime_6() { return &___ftLastWriteTime_dwHighDateTime_6; }
	inline void set_ftLastWriteTime_dwHighDateTime_6(uint32_t value)
	{
		___ftLastWriteTime_dwHighDateTime_6 = value;
	}

	inline static int32_t get_offset_of_nFileSizeHigh_7() { return static_cast<int32_t>(offsetof(WIN32_FIND_DATA_t4232796738, ___nFileSizeHigh_7)); }
	inline int32_t get_nFileSizeHigh_7() const { return ___nFileSizeHigh_7; }
	inline int32_t* get_address_of_nFileSizeHigh_7() { return &___nFileSizeHigh_7; }
	inline void set_nFileSizeHigh_7(int32_t value)
	{
		___nFileSizeHigh_7 = value;
	}

	inline static int32_t get_offset_of_nFileSizeLow_8() { return static_cast<int32_t>(offsetof(WIN32_FIND_DATA_t4232796738, ___nFileSizeLow_8)); }
	inline int32_t get_nFileSizeLow_8() const { return ___nFileSizeLow_8; }
	inline int32_t* get_address_of_nFileSizeLow_8() { return &___nFileSizeLow_8; }
	inline void set_nFileSizeLow_8(int32_t value)
	{
		___nFileSizeLow_8 = value;
	}

	inline static int32_t get_offset_of_dwReserved0_9() { return static_cast<int32_t>(offsetof(WIN32_FIND_DATA_t4232796738, ___dwReserved0_9)); }
	inline int32_t get_dwReserved0_9() const { return ___dwReserved0_9; }
	inline int32_t* get_address_of_dwReserved0_9() { return &___dwReserved0_9; }
	inline void set_dwReserved0_9(int32_t value)
	{
		___dwReserved0_9 = value;
	}

	inline static int32_t get_offset_of_dwReserved1_10() { return static_cast<int32_t>(offsetof(WIN32_FIND_DATA_t4232796738, ___dwReserved1_10)); }
	inline int32_t get_dwReserved1_10() const { return ___dwReserved1_10; }
	inline int32_t* get_address_of_dwReserved1_10() { return &___dwReserved1_10; }
	inline void set_dwReserved1_10(int32_t value)
	{
		___dwReserved1_10 = value;
	}

	inline static int32_t get_offset_of_cFileName_11() { return static_cast<int32_t>(offsetof(WIN32_FIND_DATA_t4232796738, ___cFileName_11)); }
	inline String_t* get_cFileName_11() const { return ___cFileName_11; }
	inline String_t** get_address_of_cFileName_11() { return &___cFileName_11; }
	inline void set_cFileName_11(String_t* value)
	{
		___cFileName_11 = value;
		Il2CppCodeGenWriteBarrier((&___cFileName_11), value);
	}

	inline static int32_t get_offset_of_cAlternateFileName_12() { return static_cast<int32_t>(offsetof(WIN32_FIND_DATA_t4232796738, ___cAlternateFileName_12)); }
	inline String_t* get_cAlternateFileName_12() const { return ___cAlternateFileName_12; }
	inline String_t** get_address_of_cAlternateFileName_12() { return &___cAlternateFileName_12; }
	inline void set_cAlternateFileName_12(String_t* value)
	{
		___cAlternateFileName_12 = value;
		Il2CppCodeGenWriteBarrier((&___cAlternateFileName_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Microsoft.Win32.Win32Native/WIN32_FIND_DATA
struct WIN32_FIND_DATA_t4232796738_marshaled_pinvoke
{
	int32_t ___dwFileAttributes_0;
	uint32_t ___ftCreationTime_dwLowDateTime_1;
	uint32_t ___ftCreationTime_dwHighDateTime_2;
	uint32_t ___ftLastAccessTime_dwLowDateTime_3;
	uint32_t ___ftLastAccessTime_dwHighDateTime_4;
	uint32_t ___ftLastWriteTime_dwLowDateTime_5;
	uint32_t ___ftLastWriteTime_dwHighDateTime_6;
	int32_t ___nFileSizeHigh_7;
	int32_t ___nFileSizeLow_8;
	int32_t ___dwReserved0_9;
	int32_t ___dwReserved1_10;
	char ___cFileName_11[260];
	char ___cAlternateFileName_12[14];
};
// Native definition for COM marshalling of Microsoft.Win32.Win32Native/WIN32_FIND_DATA
struct WIN32_FIND_DATA_t4232796738_marshaled_com
{
	int32_t ___dwFileAttributes_0;
	uint32_t ___ftCreationTime_dwLowDateTime_1;
	uint32_t ___ftCreationTime_dwHighDateTime_2;
	uint32_t ___ftLastAccessTime_dwLowDateTime_3;
	uint32_t ___ftLastAccessTime_dwHighDateTime_4;
	uint32_t ___ftLastWriteTime_dwLowDateTime_5;
	uint32_t ___ftLastWriteTime_dwHighDateTime_6;
	int32_t ___nFileSizeHigh_7;
	int32_t ___nFileSizeLow_8;
	int32_t ___dwReserved0_9;
	int32_t ___dwReserved1_10;
	char ___cFileName_11[260];
	char ___cAlternateFileName_12[14];
};
#endif // WIN32_FIND_DATA_T4232796738_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef CRITICALFINALIZEROBJECT_T701527852_H
#define CRITICALFINALIZEROBJECT_T701527852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct  CriticalFinalizerObject_t701527852  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRITICALFINALIZEROBJECT_T701527852_H
#ifndef ARRAYBUILDER_1_T68235548_H
#define ARRAYBUILDER_1_T68235548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.ArrayBuilder`1<System.Object>
struct  ArrayBuilder_1_t68235548 
{
public:
	// T[] System.Collections.Generic.ArrayBuilder`1::_array
	ObjectU5BU5D_t2843939325* ____array_0;
	// System.Int32 System.Collections.Generic.ArrayBuilder`1::_count
	int32_t ____count_1;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArrayBuilder_1_t68235548, ____array_0)); }
	inline ObjectU5BU5D_t2843939325* get__array_0() const { return ____array_0; }
	inline ObjectU5BU5D_t2843939325** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ObjectU5BU5D_t2843939325* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(ArrayBuilder_1_t68235548, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYBUILDER_1_T68235548_H
#ifndef SELECTIPARTITIONITERATOR_2_T2131188771_H
#define SELECTIPARTITIONITERATOR_2_T2131188771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/SelectIPartitionIterator`2<System.Object,System.Object>
struct  SelectIPartitionIterator_2_t2131188771  : public Iterator_1_t2034466501
{
public:
	// System.Linq.IPartition`1<TSource> System.Linq.Enumerable/SelectIPartitionIterator`2::_source
	RuntimeObject* ____source_3;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/SelectIPartitionIterator`2::_selector
	Func_2_t2447130374 * ____selector_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/SelectIPartitionIterator`2::_enumerator
	RuntimeObject* ____enumerator_5;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(SelectIPartitionIterator_2_t2131188771, ____source_3)); }
	inline RuntimeObject* get__source_3() const { return ____source_3; }
	inline RuntimeObject** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(RuntimeObject* value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__selector_4() { return static_cast<int32_t>(offsetof(SelectIPartitionIterator_2_t2131188771, ____selector_4)); }
	inline Func_2_t2447130374 * get__selector_4() const { return ____selector_4; }
	inline Func_2_t2447130374 ** get_address_of__selector_4() { return &____selector_4; }
	inline void set__selector_4(Func_2_t2447130374 * value)
	{
		____selector_4 = value;
		Il2CppCodeGenWriteBarrier((&____selector_4), value);
	}

	inline static int32_t get_offset_of__enumerator_5() { return static_cast<int32_t>(offsetof(SelectIPartitionIterator_2_t2131188771, ____enumerator_5)); }
	inline RuntimeObject* get__enumerator_5() const { return ____enumerator_5; }
	inline RuntimeObject** get_address_of__enumerator_5() { return &____enumerator_5; }
	inline void set__enumerator_5(RuntimeObject* value)
	{
		____enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((&____enumerator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIPARTITIONITERATOR_2_T2131188771_H
#ifndef SELECTARRAYITERATOR_2_T819778152_H
#define SELECTARRAYITERATOR_2_T819778152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/SelectArrayIterator`2<System.Object,System.Object>
struct  SelectArrayIterator_2_t819778152  : public Iterator_1_t2034466501
{
public:
	// TSource[] System.Linq.Enumerable/SelectArrayIterator`2::_source
	ObjectU5BU5D_t2843939325* ____source_3;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/SelectArrayIterator`2::_selector
	Func_2_t2447130374 * ____selector_4;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(SelectArrayIterator_2_t819778152, ____source_3)); }
	inline ObjectU5BU5D_t2843939325* get__source_3() const { return ____source_3; }
	inline ObjectU5BU5D_t2843939325** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(ObjectU5BU5D_t2843939325* value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__selector_4() { return static_cast<int32_t>(offsetof(SelectArrayIterator_2_t819778152, ____selector_4)); }
	inline Func_2_t2447130374 * get__selector_4() const { return ____selector_4; }
	inline Func_2_t2447130374 ** get_address_of__selector_4() { return &____selector_4; }
	inline void set__selector_4(Func_2_t2447130374 * value)
	{
		____selector_4 = value;
		Il2CppCodeGenWriteBarrier((&____selector_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTARRAYITERATOR_2_T819778152_H
#ifndef KEYVALUEPAIR_2_T2530217319_H
#define KEYVALUEPAIR_2_T2530217319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t2530217319 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2530217319_H
#ifndef SELECTILISTITERATOR_2_T3601768299_H
#define SELECTILISTITERATOR_2_T3601768299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/SelectIListIterator`2<System.Object,System.Object>
struct  SelectIListIterator_2_t3601768299  : public Iterator_1_t2034466501
{
public:
	// System.Collections.Generic.IList`1<TSource> System.Linq.Enumerable/SelectIListIterator`2::_source
	RuntimeObject* ____source_3;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/SelectIListIterator`2::_selector
	Func_2_t2447130374 * ____selector_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/SelectIListIterator`2::_enumerator
	RuntimeObject* ____enumerator_5;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(SelectIListIterator_2_t3601768299, ____source_3)); }
	inline RuntimeObject* get__source_3() const { return ____source_3; }
	inline RuntimeObject** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(RuntimeObject* value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__selector_4() { return static_cast<int32_t>(offsetof(SelectIListIterator_2_t3601768299, ____selector_4)); }
	inline Func_2_t2447130374 * get__selector_4() const { return ____selector_4; }
	inline Func_2_t2447130374 ** get_address_of__selector_4() { return &____selector_4; }
	inline void set__selector_4(Func_2_t2447130374 * value)
	{
		____selector_4 = value;
		Il2CppCodeGenWriteBarrier((&____selector_4), value);
	}

	inline static int32_t get_offset_of__enumerator_5() { return static_cast<int32_t>(offsetof(SelectIListIterator_2_t3601768299, ____enumerator_5)); }
	inline RuntimeObject* get__enumerator_5() const { return ____enumerator_5; }
	inline RuntimeObject** get_address_of__enumerator_5() { return &____enumerator_5; }
	inline void set__enumerator_5(RuntimeObject* value)
	{
		____enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((&____enumerator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTILISTITERATOR_2_T3601768299_H
#ifndef SELECTENUMERABLEITERATOR_2_T4232181467_H
#define SELECTENUMERABLEITERATOR_2_T4232181467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/SelectEnumerableIterator`2<System.Object,System.Object>
struct  SelectEnumerableIterator_2_t4232181467  : public Iterator_1_t2034466501
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/SelectEnumerableIterator`2::_source
	RuntimeObject* ____source_3;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/SelectEnumerableIterator`2::_selector
	Func_2_t2447130374 * ____selector_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/SelectEnumerableIterator`2::_enumerator
	RuntimeObject* ____enumerator_5;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(SelectEnumerableIterator_2_t4232181467, ____source_3)); }
	inline RuntimeObject* get__source_3() const { return ____source_3; }
	inline RuntimeObject** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(RuntimeObject* value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__selector_4() { return static_cast<int32_t>(offsetof(SelectEnumerableIterator_2_t4232181467, ____selector_4)); }
	inline Func_2_t2447130374 * get__selector_4() const { return ____selector_4; }
	inline Func_2_t2447130374 ** get_address_of__selector_4() { return &____selector_4; }
	inline void set__selector_4(Func_2_t2447130374 * value)
	{
		____selector_4 = value;
		Il2CppCodeGenWriteBarrier((&____selector_4), value);
	}

	inline static int32_t get_offset_of__enumerator_5() { return static_cast<int32_t>(offsetof(SelectEnumerableIterator_2_t4232181467, ____enumerator_5)); }
	inline RuntimeObject* get__enumerator_5() const { return ____enumerator_5; }
	inline RuntimeObject** get_address_of__enumerator_5() { return &____enumerator_5; }
	inline void set__enumerator_5(RuntimeObject* value)
	{
		____enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((&____enumerator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTENUMERABLEITERATOR_2_T4232181467_H
#ifndef ARRAYBUILDER_1_T3343055733_H
#define ARRAYBUILDER_1_T3343055733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.ArrayBuilder`1<System.Collections.Generic.IEnumerable`1<System.Object>>
struct  ArrayBuilder_1_t3343055733 
{
public:
	// T[] System.Collections.Generic.ArrayBuilder`1::_array
	IEnumerable_1U5BU5D_t2115075616* ____array_0;
	// System.Int32 System.Collections.Generic.ArrayBuilder`1::_count
	int32_t ____count_1;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArrayBuilder_1_t3343055733, ____array_0)); }
	inline IEnumerable_1U5BU5D_t2115075616* get__array_0() const { return ____array_0; }
	inline IEnumerable_1U5BU5D_t2115075616** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(IEnumerable_1U5BU5D_t2115075616* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(ArrayBuilder_1_t3343055733, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYBUILDER_1_T3343055733_H
#ifndef WHEREENUMERABLEITERATOR_1_T2739994797_H
#define WHEREENUMERABLEITERATOR_1_T2739994797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/WhereEnumerableIterator`1<System.Char>
struct  WhereEnumerableIterator_1_t2739994797  : public Iterator_1_t2588820807
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::_source
	RuntimeObject* ____source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::_predicate
	Func_2_t148644517 * ____predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::_enumerator
	RuntimeObject* ____enumerator_5;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t2739994797, ____source_3)); }
	inline RuntimeObject* get__source_3() const { return ____source_3; }
	inline RuntimeObject** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(RuntimeObject* value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t2739994797, ____predicate_4)); }
	inline Func_2_t148644517 * get__predicate_4() const { return ____predicate_4; }
	inline Func_2_t148644517 ** get_address_of__predicate_4() { return &____predicate_4; }
	inline void set__predicate_4(Func_2_t148644517 * value)
	{
		____predicate_4 = value;
		Il2CppCodeGenWriteBarrier((&____predicate_4), value);
	}

	inline static int32_t get_offset_of__enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t2739994797, ____enumerator_5)); }
	inline RuntimeObject* get__enumerator_5() const { return ____enumerator_5; }
	inline RuntimeObject** get_address_of__enumerator_5() { return &____enumerator_5; }
	inline void set__enumerator_5(RuntimeObject* value)
	{
		____enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((&____enumerator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHEREENUMERABLEITERATOR_1_T2739994797_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t257213610 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___list_0)); }
	inline List_1_t257213610 * get_list_0() const { return ___list_0; }
	inline List_1_t257213610 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t257213610 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef SELECTMANYSINGLESELECTORITERATOR_2_T963716220_H
#define SELECTMANYSINGLESELECTORITERATOR_2_T963716220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/SelectManySingleSelectorIterator`2<System.Object,System.Object>
struct  SelectManySingleSelectorIterator_2_t963716220  : public Iterator_1_t2034466501
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/SelectManySingleSelectorIterator`2::_source
	RuntimeObject* ____source_3;
	// System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>> System.Linq.Enumerable/SelectManySingleSelectorIterator`2::_selector
	Func_2_t1426983263 * ____selector_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/SelectManySingleSelectorIterator`2::_sourceEnumerator
	RuntimeObject* ____sourceEnumerator_5;
	// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/SelectManySingleSelectorIterator`2::_subEnumerator
	RuntimeObject* ____subEnumerator_6;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(SelectManySingleSelectorIterator_2_t963716220, ____source_3)); }
	inline RuntimeObject* get__source_3() const { return ____source_3; }
	inline RuntimeObject** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(RuntimeObject* value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__selector_4() { return static_cast<int32_t>(offsetof(SelectManySingleSelectorIterator_2_t963716220, ____selector_4)); }
	inline Func_2_t1426983263 * get__selector_4() const { return ____selector_4; }
	inline Func_2_t1426983263 ** get_address_of__selector_4() { return &____selector_4; }
	inline void set__selector_4(Func_2_t1426983263 * value)
	{
		____selector_4 = value;
		Il2CppCodeGenWriteBarrier((&____selector_4), value);
	}

	inline static int32_t get_offset_of__sourceEnumerator_5() { return static_cast<int32_t>(offsetof(SelectManySingleSelectorIterator_2_t963716220, ____sourceEnumerator_5)); }
	inline RuntimeObject* get__sourceEnumerator_5() const { return ____sourceEnumerator_5; }
	inline RuntimeObject** get_address_of__sourceEnumerator_5() { return &____sourceEnumerator_5; }
	inline void set__sourceEnumerator_5(RuntimeObject* value)
	{
		____sourceEnumerator_5 = value;
		Il2CppCodeGenWriteBarrier((&____sourceEnumerator_5), value);
	}

	inline static int32_t get_offset_of__subEnumerator_6() { return static_cast<int32_t>(offsetof(SelectManySingleSelectorIterator_2_t963716220, ____subEnumerator_6)); }
	inline RuntimeObject* get__subEnumerator_6() const { return ____subEnumerator_6; }
	inline RuntimeObject** get_address_of__subEnumerator_6() { return &____subEnumerator_6; }
	inline void set__subEnumerator_6(RuntimeObject* value)
	{
		____subEnumerator_6 = value;
		Il2CppCodeGenWriteBarrier((&____subEnumerator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTMANYSINGLESELECTORITERATOR_2_T963716220_H
#ifndef ARRAYBUILDER_1_T4177874457_H
#define ARRAYBUILDER_1_T4177874457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.ArrayBuilder`1<System.Collections.Generic.Marker>
struct  ArrayBuilder_1_t4177874457 
{
public:
	// T[] System.Collections.Generic.ArrayBuilder`1::_array
	MarkerU5BU5D_t2881615980* ____array_0;
	// System.Int32 System.Collections.Generic.ArrayBuilder`1::_count
	int32_t ____count_1;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArrayBuilder_1_t4177874457, ____array_0)); }
	inline MarkerU5BU5D_t2881615980* get__array_0() const { return ____array_0; }
	inline MarkerU5BU5D_t2881615980** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(MarkerU5BU5D_t2881615980* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(ArrayBuilder_1_t4177874457, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYBUILDER_1_T4177874457_H
#ifndef MARKER_T2894777777_H
#define MARKER_T2894777777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Marker
struct  Marker_t2894777777 
{
public:
	// System.Int32 System.Collections.Generic.Marker::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_0;
	// System.Int32 System.Collections.Generic.Marker::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Marker_t2894777777, ___U3CCountU3Ek__BackingField_0)); }
	inline int32_t get_U3CCountU3Ek__BackingField_0() const { return ___U3CCountU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_0() { return &___U3CCountU3Ek__BackingField_0; }
	inline void set_U3CCountU3Ek__BackingField_0(int32_t value)
	{
		___U3CCountU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIndexU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Marker_t2894777777, ___U3CIndexU3Ek__BackingField_1)); }
	inline int32_t get_U3CIndexU3Ek__BackingField_1() const { return ___U3CIndexU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CIndexU3Ek__BackingField_1() { return &___U3CIndexU3Ek__BackingField_1; }
	inline void set_U3CIndexU3Ek__BackingField_1(int32_t value)
	{
		___U3CIndexU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKER_T2894777777_H
#ifndef UNIONITERATOR_1_T3402001247_H
#define UNIONITERATOR_1_T3402001247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/UnionIterator`1<System.Char>
struct  UnionIterator_1_t3402001247  : public Iterator_1_t2588820807
{
public:
	// System.Collections.Generic.IEqualityComparer`1<TSource> System.Linq.Enumerable/UnionIterator`1::_comparer
	RuntimeObject* ____comparer_3;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/UnionIterator`1::_enumerator
	RuntimeObject* ____enumerator_4;
	// System.Linq.Set`1<TSource> System.Linq.Enumerable/UnionIterator`1::_set
	Set_1_t329142533 * ____set_5;

public:
	inline static int32_t get_offset_of__comparer_3() { return static_cast<int32_t>(offsetof(UnionIterator_1_t3402001247, ____comparer_3)); }
	inline RuntimeObject* get__comparer_3() const { return ____comparer_3; }
	inline RuntimeObject** get_address_of__comparer_3() { return &____comparer_3; }
	inline void set__comparer_3(RuntimeObject* value)
	{
		____comparer_3 = value;
		Il2CppCodeGenWriteBarrier((&____comparer_3), value);
	}

	inline static int32_t get_offset_of__enumerator_4() { return static_cast<int32_t>(offsetof(UnionIterator_1_t3402001247, ____enumerator_4)); }
	inline RuntimeObject* get__enumerator_4() const { return ____enumerator_4; }
	inline RuntimeObject** get_address_of__enumerator_4() { return &____enumerator_4; }
	inline void set__enumerator_4(RuntimeObject* value)
	{
		____enumerator_4 = value;
		Il2CppCodeGenWriteBarrier((&____enumerator_4), value);
	}

	inline static int32_t get_offset_of__set_5() { return static_cast<int32_t>(offsetof(UnionIterator_1_t3402001247, ____set_5)); }
	inline Set_1_t329142533 * get__set_5() const { return ____set_5; }
	inline Set_1_t329142533 ** get_address_of__set_5() { return &____set_5; }
	inline void set__set_5(Set_1_t329142533 * value)
	{
		____set_5 = value;
		Il2CppCodeGenWriteBarrier((&____set_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIONITERATOR_1_T3402001247_H
#ifndef WHEREENUMERABLEITERATOR_1_T2185640491_H
#define WHEREENUMERABLEITERATOR_1_T2185640491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>
struct  WhereEnumerableIterator_1_t2185640491  : public Iterator_1_t2034466501
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::_source
	RuntimeObject* ____source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::_predicate
	Func_2_t3759279471 * ____predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::_enumerator
	RuntimeObject* ____enumerator_5;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t2185640491, ____source_3)); }
	inline RuntimeObject* get__source_3() const { return ____source_3; }
	inline RuntimeObject** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(RuntimeObject* value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t2185640491, ____predicate_4)); }
	inline Func_2_t3759279471 * get__predicate_4() const { return ____predicate_4; }
	inline Func_2_t3759279471 ** get_address_of__predicate_4() { return &____predicate_4; }
	inline void set__predicate_4(Func_2_t3759279471 * value)
	{
		____predicate_4 = value;
		Il2CppCodeGenWriteBarrier((&____predicate_4), value);
	}

	inline static int32_t get_offset_of__enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t2185640491, ____enumerator_5)); }
	inline RuntimeObject* get__enumerator_5() const { return ____enumerator_5; }
	inline RuntimeObject** get_address_of__enumerator_5() { return &____enumerator_5; }
	inline void set__enumerator_5(RuntimeObject* value)
	{
		____enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((&____enumerator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHEREENUMERABLEITERATOR_1_T2185640491_H
#ifndef DICTIONARYENTRY_T3123975638_H
#define DICTIONARYENTRY_T3123975638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.DictionaryEntry
struct  DictionaryEntry_t3123975638 
{
public:
	// System.Object System.Collections.DictionaryEntry::_key
	RuntimeObject * ____key_0;
	// System.Object System.Collections.DictionaryEntry::_value
	RuntimeObject * ____value_1;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(DictionaryEntry_t3123975638, ____key_0)); }
	inline RuntimeObject * get__key_0() const { return ____key_0; }
	inline RuntimeObject ** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(RuntimeObject * value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier((&____key_0), value);
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(DictionaryEntry_t3123975638, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Collections.DictionaryEntry
struct DictionaryEntry_t3123975638_marshaled_pinvoke
{
	Il2CppIUnknown* ____key_0;
	Il2CppIUnknown* ____value_1;
};
// Native definition for COM marshalling of System.Collections.DictionaryEntry
struct DictionaryEntry_t3123975638_marshaled_com
{
	Il2CppIUnknown* ____key_0;
	Il2CppIUnknown* ____value_1;
};
#endif // DICTIONARYENTRY_T3123975638_H
#ifndef THREAD_T2300836069_H
#define THREAD_T2300836069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Thread
struct  Thread_t2300836069  : public CriticalFinalizerObject_t701527852
{
public:
	// System.Threading.InternalThread System.Threading.Thread::internal_thread
	InternalThread_t95296544 * ___internal_thread_0;
	// System.Object System.Threading.Thread::m_ThreadStartArg
	RuntimeObject * ___m_ThreadStartArg_1;
	// System.Object System.Threading.Thread::pending_exception
	RuntimeObject * ___pending_exception_2;
	// System.Security.Principal.IPrincipal System.Threading.Thread::principal
	RuntimeObject* ___principal_3;
	// System.Int32 System.Threading.Thread::principal_version
	int32_t ___principal_version_4;
	// System.MulticastDelegate System.Threading.Thread::m_Delegate
	MulticastDelegate_t157516450 * ___m_Delegate_6;
	// System.Threading.ExecutionContext System.Threading.Thread::m_ExecutionContext
	ExecutionContext_t1748372627 * ___m_ExecutionContext_7;
	// System.Boolean System.Threading.Thread::m_ExecutionContextBelongsToOuterScope
	bool ___m_ExecutionContextBelongsToOuterScope_8;

public:
	inline static int32_t get_offset_of_internal_thread_0() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___internal_thread_0)); }
	inline InternalThread_t95296544 * get_internal_thread_0() const { return ___internal_thread_0; }
	inline InternalThread_t95296544 ** get_address_of_internal_thread_0() { return &___internal_thread_0; }
	inline void set_internal_thread_0(InternalThread_t95296544 * value)
	{
		___internal_thread_0 = value;
		Il2CppCodeGenWriteBarrier((&___internal_thread_0), value);
	}

	inline static int32_t get_offset_of_m_ThreadStartArg_1() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___m_ThreadStartArg_1)); }
	inline RuntimeObject * get_m_ThreadStartArg_1() const { return ___m_ThreadStartArg_1; }
	inline RuntimeObject ** get_address_of_m_ThreadStartArg_1() { return &___m_ThreadStartArg_1; }
	inline void set_m_ThreadStartArg_1(RuntimeObject * value)
	{
		___m_ThreadStartArg_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ThreadStartArg_1), value);
	}

	inline static int32_t get_offset_of_pending_exception_2() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___pending_exception_2)); }
	inline RuntimeObject * get_pending_exception_2() const { return ___pending_exception_2; }
	inline RuntimeObject ** get_address_of_pending_exception_2() { return &___pending_exception_2; }
	inline void set_pending_exception_2(RuntimeObject * value)
	{
		___pending_exception_2 = value;
		Il2CppCodeGenWriteBarrier((&___pending_exception_2), value);
	}

	inline static int32_t get_offset_of_principal_3() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___principal_3)); }
	inline RuntimeObject* get_principal_3() const { return ___principal_3; }
	inline RuntimeObject** get_address_of_principal_3() { return &___principal_3; }
	inline void set_principal_3(RuntimeObject* value)
	{
		___principal_3 = value;
		Il2CppCodeGenWriteBarrier((&___principal_3), value);
	}

	inline static int32_t get_offset_of_principal_version_4() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___principal_version_4)); }
	inline int32_t get_principal_version_4() const { return ___principal_version_4; }
	inline int32_t* get_address_of_principal_version_4() { return &___principal_version_4; }
	inline void set_principal_version_4(int32_t value)
	{
		___principal_version_4 = value;
	}

	inline static int32_t get_offset_of_m_Delegate_6() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___m_Delegate_6)); }
	inline MulticastDelegate_t157516450 * get_m_Delegate_6() const { return ___m_Delegate_6; }
	inline MulticastDelegate_t157516450 ** get_address_of_m_Delegate_6() { return &___m_Delegate_6; }
	inline void set_m_Delegate_6(MulticastDelegate_t157516450 * value)
	{
		___m_Delegate_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Delegate_6), value);
	}

	inline static int32_t get_offset_of_m_ExecutionContext_7() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___m_ExecutionContext_7)); }
	inline ExecutionContext_t1748372627 * get_m_ExecutionContext_7() const { return ___m_ExecutionContext_7; }
	inline ExecutionContext_t1748372627 ** get_address_of_m_ExecutionContext_7() { return &___m_ExecutionContext_7; }
	inline void set_m_ExecutionContext_7(ExecutionContext_t1748372627 * value)
	{
		___m_ExecutionContext_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExecutionContext_7), value);
	}

	inline static int32_t get_offset_of_m_ExecutionContextBelongsToOuterScope_8() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___m_ExecutionContextBelongsToOuterScope_8)); }
	inline bool get_m_ExecutionContextBelongsToOuterScope_8() const { return ___m_ExecutionContextBelongsToOuterScope_8; }
	inline bool* get_address_of_m_ExecutionContextBelongsToOuterScope_8() { return &___m_ExecutionContextBelongsToOuterScope_8; }
	inline void set_m_ExecutionContextBelongsToOuterScope_8(bool value)
	{
		___m_ExecutionContextBelongsToOuterScope_8 = value;
	}
};

struct Thread_t2300836069_StaticFields
{
public:
	// System.LocalDataStoreMgr System.Threading.Thread::s_LocalDataStoreMgr
	LocalDataStoreMgr_t1707895399 * ___s_LocalDataStoreMgr_9;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentCulture
	AsyncLocal_1_t2427220165 * ___s_asyncLocalCurrentCulture_13;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentUICulture
	AsyncLocal_1_t2427220165 * ___s_asyncLocalCurrentUICulture_14;

public:
	inline static int32_t get_offset_of_s_LocalDataStoreMgr_9() { return static_cast<int32_t>(offsetof(Thread_t2300836069_StaticFields, ___s_LocalDataStoreMgr_9)); }
	inline LocalDataStoreMgr_t1707895399 * get_s_LocalDataStoreMgr_9() const { return ___s_LocalDataStoreMgr_9; }
	inline LocalDataStoreMgr_t1707895399 ** get_address_of_s_LocalDataStoreMgr_9() { return &___s_LocalDataStoreMgr_9; }
	inline void set_s_LocalDataStoreMgr_9(LocalDataStoreMgr_t1707895399 * value)
	{
		___s_LocalDataStoreMgr_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_LocalDataStoreMgr_9), value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentCulture_13() { return static_cast<int32_t>(offsetof(Thread_t2300836069_StaticFields, ___s_asyncLocalCurrentCulture_13)); }
	inline AsyncLocal_1_t2427220165 * get_s_asyncLocalCurrentCulture_13() const { return ___s_asyncLocalCurrentCulture_13; }
	inline AsyncLocal_1_t2427220165 ** get_address_of_s_asyncLocalCurrentCulture_13() { return &___s_asyncLocalCurrentCulture_13; }
	inline void set_s_asyncLocalCurrentCulture_13(AsyncLocal_1_t2427220165 * value)
	{
		___s_asyncLocalCurrentCulture_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_asyncLocalCurrentCulture_13), value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentUICulture_14() { return static_cast<int32_t>(offsetof(Thread_t2300836069_StaticFields, ___s_asyncLocalCurrentUICulture_14)); }
	inline AsyncLocal_1_t2427220165 * get_s_asyncLocalCurrentUICulture_14() const { return ___s_asyncLocalCurrentUICulture_14; }
	inline AsyncLocal_1_t2427220165 ** get_address_of_s_asyncLocalCurrentUICulture_14() { return &___s_asyncLocalCurrentUICulture_14; }
	inline void set_s_asyncLocalCurrentUICulture_14(AsyncLocal_1_t2427220165 * value)
	{
		___s_asyncLocalCurrentUICulture_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_asyncLocalCurrentUICulture_14), value);
	}
};

struct Thread_t2300836069_ThreadStaticFields
{
public:
	// System.Threading.Thread System.Threading.Thread::current_thread
	Thread_t2300836069 * ___current_thread_5;
	// System.LocalDataStoreHolder System.Threading.Thread::s_LocalDataStore
	LocalDataStoreHolder_t2567786569 * ___s_LocalDataStore_10;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentCulture
	CultureInfo_t4157843068 * ___m_CurrentCulture_11;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentUICulture
	CultureInfo_t4157843068 * ___m_CurrentUICulture_12;

public:
	inline static int32_t get_offset_of_current_thread_5() { return static_cast<int32_t>(offsetof(Thread_t2300836069_ThreadStaticFields, ___current_thread_5)); }
	inline Thread_t2300836069 * get_current_thread_5() const { return ___current_thread_5; }
	inline Thread_t2300836069 ** get_address_of_current_thread_5() { return &___current_thread_5; }
	inline void set_current_thread_5(Thread_t2300836069 * value)
	{
		___current_thread_5 = value;
		Il2CppCodeGenWriteBarrier((&___current_thread_5), value);
	}

	inline static int32_t get_offset_of_s_LocalDataStore_10() { return static_cast<int32_t>(offsetof(Thread_t2300836069_ThreadStaticFields, ___s_LocalDataStore_10)); }
	inline LocalDataStoreHolder_t2567786569 * get_s_LocalDataStore_10() const { return ___s_LocalDataStore_10; }
	inline LocalDataStoreHolder_t2567786569 ** get_address_of_s_LocalDataStore_10() { return &___s_LocalDataStore_10; }
	inline void set_s_LocalDataStore_10(LocalDataStoreHolder_t2567786569 * value)
	{
		___s_LocalDataStore_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_LocalDataStore_10), value);
	}

	inline static int32_t get_offset_of_m_CurrentCulture_11() { return static_cast<int32_t>(offsetof(Thread_t2300836069_ThreadStaticFields, ___m_CurrentCulture_11)); }
	inline CultureInfo_t4157843068 * get_m_CurrentCulture_11() const { return ___m_CurrentCulture_11; }
	inline CultureInfo_t4157843068 ** get_address_of_m_CurrentCulture_11() { return &___m_CurrentCulture_11; }
	inline void set_m_CurrentCulture_11(CultureInfo_t4157843068 * value)
	{
		___m_CurrentCulture_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentCulture_11), value);
	}

	inline static int32_t get_offset_of_m_CurrentUICulture_12() { return static_cast<int32_t>(offsetof(Thread_t2300836069_ThreadStaticFields, ___m_CurrentUICulture_12)); }
	inline CultureInfo_t4157843068 * get_m_CurrentUICulture_12() const { return ___m_CurrentUICulture_12; }
	inline CultureInfo_t4157843068 ** get_address_of_m_CurrentUICulture_12() { return &___m_CurrentUICulture_12; }
	inline void set_m_CurrentUICulture_12(CultureInfo_t4157843068 * value)
	{
		___m_CurrentUICulture_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentUICulture_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREAD_T2300836069_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_t4116647657* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_t4116647657* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_t4116647657* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((&___categoryForLatin1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BUFFER_1_T8667653_H
#define BUFFER_1_T8667653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Buffer`1<UnityEngine.Vector2>
struct  Buffer_1_t8667653 
{
public:
	// TElement[] System.Linq.Buffer`1::_items
	Vector2U5BU5D_t1457185986* ____items_0;
	// System.Int32 System.Linq.Buffer`1::_count
	int32_t ____count_1;

public:
	inline static int32_t get_offset_of__items_0() { return static_cast<int32_t>(offsetof(Buffer_1_t8667653, ____items_0)); }
	inline Vector2U5BU5D_t1457185986* get__items_0() const { return ____items_0; }
	inline Vector2U5BU5D_t1457185986** get_address_of__items_0() { return &____items_0; }
	inline void set__items_0(Vector2U5BU5D_t1457185986* value)
	{
		____items_0 = value;
		Il2CppCodeGenWriteBarrier((&____items_0), value);
	}

	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(Buffer_1_t8667653, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFER_1_T8667653_H
#ifndef BUFFER_1_T932544294_H
#define BUFFER_1_T932544294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Buffer`1<System.Object>
struct  Buffer_1_t932544294 
{
public:
	// TElement[] System.Linq.Buffer`1::_items
	ObjectU5BU5D_t2843939325* ____items_0;
	// System.Int32 System.Linq.Buffer`1::_count
	int32_t ____count_1;

public:
	inline static int32_t get_offset_of__items_0() { return static_cast<int32_t>(offsetof(Buffer_1_t932544294, ____items_0)); }
	inline ObjectU5BU5D_t2843939325* get__items_0() const { return ____items_0; }
	inline ObjectU5BU5D_t2843939325** get_address_of__items_0() { return &____items_0; }
	inline void set__items_0(ObjectU5BU5D_t2843939325* value)
	{
		____items_0 = value;
		Il2CppCodeGenWriteBarrier((&____items_0), value);
	}

	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(Buffer_1_t932544294, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFER_1_T932544294_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ARRAYBUILDER_1_T1401365894_H
#define ARRAYBUILDER_1_T1401365894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.ArrayBuilder`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]>
struct  ArrayBuilder_1_t1401365894 
{
public:
	// T[] System.Collections.Generic.ArrayBuilder`1::_array
	KeyValuePair_2U5BU5DU5BU5D_t2409249963* ____array_0;
	// System.Int32 System.Collections.Generic.ArrayBuilder`1::_count
	int32_t ____count_1;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArrayBuilder_1_t1401365894, ____array_0)); }
	inline KeyValuePair_2U5BU5DU5BU5D_t2409249963* get__array_0() const { return ____array_0; }
	inline KeyValuePair_2U5BU5DU5BU5D_t2409249963** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(KeyValuePair_2U5BU5DU5BU5D_t2409249963* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(ArrayBuilder_1_t1401365894, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYBUILDER_1_T1401365894_H
#ifndef ARRAYBUILDER_1_T4127036005_H
#define ARRAYBUILDER_1_T4127036005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.ArrayBuilder`1<System.Object[]>
struct  ArrayBuilder_1_t4127036005 
{
public:
	// T[] System.Collections.Generic.ArrayBuilder`1::_array
	ObjectU5BU5DU5BU5D_t831815024* ____array_0;
	// System.Int32 System.Collections.Generic.ArrayBuilder`1::_count
	int32_t ____count_1;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArrayBuilder_1_t4127036005, ____array_0)); }
	inline ObjectU5BU5DU5BU5D_t831815024* get__array_0() const { return ____array_0; }
	inline ObjectU5BU5DU5BU5D_t831815024** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ObjectU5BU5DU5BU5D_t831815024* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(ArrayBuilder_1_t4127036005, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYBUILDER_1_T4127036005_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t1436737249
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef CACHINGCOMPARER_2_T4066783831_H
#define CACHINGCOMPARER_2_T4066783831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.CachingComparer`2<UnityEngine.Vector2,System.Single>
struct  CachingComparer_2_t4066783831  : public CachingComparer_1_t454941278
{
public:
	// System.Func`2<TElement,TKey> System.Linq.CachingComparer`2::_keySelector
	Func_2_t3672504941 * ____keySelector_0;
	// System.Collections.Generic.IComparer`1<TKey> System.Linq.CachingComparer`2::_comparer
	RuntimeObject* ____comparer_1;
	// System.Boolean System.Linq.CachingComparer`2::_descending
	bool ____descending_2;
	// TKey System.Linq.CachingComparer`2::_lastKey
	float ____lastKey_3;

public:
	inline static int32_t get_offset_of__keySelector_0() { return static_cast<int32_t>(offsetof(CachingComparer_2_t4066783831, ____keySelector_0)); }
	inline Func_2_t3672504941 * get__keySelector_0() const { return ____keySelector_0; }
	inline Func_2_t3672504941 ** get_address_of__keySelector_0() { return &____keySelector_0; }
	inline void set__keySelector_0(Func_2_t3672504941 * value)
	{
		____keySelector_0 = value;
		Il2CppCodeGenWriteBarrier((&____keySelector_0), value);
	}

	inline static int32_t get_offset_of__comparer_1() { return static_cast<int32_t>(offsetof(CachingComparer_2_t4066783831, ____comparer_1)); }
	inline RuntimeObject* get__comparer_1() const { return ____comparer_1; }
	inline RuntimeObject** get_address_of__comparer_1() { return &____comparer_1; }
	inline void set__comparer_1(RuntimeObject* value)
	{
		____comparer_1 = value;
		Il2CppCodeGenWriteBarrier((&____comparer_1), value);
	}

	inline static int32_t get_offset_of__descending_2() { return static_cast<int32_t>(offsetof(CachingComparer_2_t4066783831, ____descending_2)); }
	inline bool get__descending_2() const { return ____descending_2; }
	inline bool* get_address_of__descending_2() { return &____descending_2; }
	inline void set__descending_2(bool value)
	{
		____descending_2 = value;
	}

	inline static int32_t get_offset_of__lastKey_3() { return static_cast<int32_t>(offsetof(CachingComparer_2_t4066783831, ____lastKey_3)); }
	inline float get__lastKey_3() const { return ____lastKey_3; }
	inline float* get_address_of__lastKey_3() { return &____lastKey_3; }
	inline void set__lastKey_3(float value)
	{
		____lastKey_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHINGCOMPARER_2_T4066783831_H
#ifndef UNIONITERATOR_1_T2847646941_H
#define UNIONITERATOR_1_T2847646941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/UnionIterator`1<System.Object>
struct  UnionIterator_1_t2847646941  : public Iterator_1_t2034466501
{
public:
	// System.Collections.Generic.IEqualityComparer`1<TSource> System.Linq.Enumerable/UnionIterator`1::_comparer
	RuntimeObject* ____comparer_3;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/UnionIterator`1::_enumerator
	RuntimeObject* ____enumerator_4;
	// System.Linq.Set`1<TSource> System.Linq.Enumerable/UnionIterator`1::_set
	Set_1_t4069755523 * ____set_5;

public:
	inline static int32_t get_offset_of__comparer_3() { return static_cast<int32_t>(offsetof(UnionIterator_1_t2847646941, ____comparer_3)); }
	inline RuntimeObject* get__comparer_3() const { return ____comparer_3; }
	inline RuntimeObject** get_address_of__comparer_3() { return &____comparer_3; }
	inline void set__comparer_3(RuntimeObject* value)
	{
		____comparer_3 = value;
		Il2CppCodeGenWriteBarrier((&____comparer_3), value);
	}

	inline static int32_t get_offset_of__enumerator_4() { return static_cast<int32_t>(offsetof(UnionIterator_1_t2847646941, ____enumerator_4)); }
	inline RuntimeObject* get__enumerator_4() const { return ____enumerator_4; }
	inline RuntimeObject** get_address_of__enumerator_4() { return &____enumerator_4; }
	inline void set__enumerator_4(RuntimeObject* value)
	{
		____enumerator_4 = value;
		Il2CppCodeGenWriteBarrier((&____enumerator_4), value);
	}

	inline static int32_t get_offset_of__set_5() { return static_cast<int32_t>(offsetof(UnionIterator_1_t2847646941, ____set_5)); }
	inline Set_1_t4069755523 * get__set_5() const { return ____set_5; }
	inline Set_1_t4069755523 ** get_address_of__set_5() { return &____set_5; }
	inline void set__set_5(Set_1_t4069755523 * value)
	{
		____set_5 = value;
		Il2CppCodeGenWriteBarrier((&____set_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIONITERATOR_1_T2847646941_H
#ifndef CACHINGCOMPARER_2_T33381310_H
#define CACHINGCOMPARER_2_T33381310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.CachingComparer`2<UnityEngine.Vector3,System.Single>
struct  CachingComparer_2_t33381310  : public CachingComparer_1_t2021025219
{
public:
	// System.Func`2<TElement,TKey> System.Linq.CachingComparer`2::_keySelector
	Func_2_t3934069716 * ____keySelector_0;
	// System.Collections.Generic.IComparer`1<TKey> System.Linq.CachingComparer`2::_comparer
	RuntimeObject* ____comparer_1;
	// System.Boolean System.Linq.CachingComparer`2::_descending
	bool ____descending_2;
	// TKey System.Linq.CachingComparer`2::_lastKey
	float ____lastKey_3;

public:
	inline static int32_t get_offset_of__keySelector_0() { return static_cast<int32_t>(offsetof(CachingComparer_2_t33381310, ____keySelector_0)); }
	inline Func_2_t3934069716 * get__keySelector_0() const { return ____keySelector_0; }
	inline Func_2_t3934069716 ** get_address_of__keySelector_0() { return &____keySelector_0; }
	inline void set__keySelector_0(Func_2_t3934069716 * value)
	{
		____keySelector_0 = value;
		Il2CppCodeGenWriteBarrier((&____keySelector_0), value);
	}

	inline static int32_t get_offset_of__comparer_1() { return static_cast<int32_t>(offsetof(CachingComparer_2_t33381310, ____comparer_1)); }
	inline RuntimeObject* get__comparer_1() const { return ____comparer_1; }
	inline RuntimeObject** get_address_of__comparer_1() { return &____comparer_1; }
	inline void set__comparer_1(RuntimeObject* value)
	{
		____comparer_1 = value;
		Il2CppCodeGenWriteBarrier((&____comparer_1), value);
	}

	inline static int32_t get_offset_of__descending_2() { return static_cast<int32_t>(offsetof(CachingComparer_2_t33381310, ____descending_2)); }
	inline bool get__descending_2() const { return ____descending_2; }
	inline bool* get_address_of__descending_2() { return &____descending_2; }
	inline void set__descending_2(bool value)
	{
		____descending_2 = value;
	}

	inline static int32_t get_offset_of__lastKey_3() { return static_cast<int32_t>(offsetof(CachingComparer_2_t33381310, ____lastKey_3)); }
	inline float get__lastKey_3() const { return ____lastKey_3; }
	inline float* get_address_of__lastKey_3() { return &____lastKey_3; }
	inline void set__lastKey_3(float value)
	{
		____lastKey_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHINGCOMPARER_2_T33381310_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef CACHINGCOMPARER_2_T2841409264_H
#define CACHINGCOMPARER_2_T2841409264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.CachingComparer`2<System.Object,System.Object>
struct  CachingComparer_2_t2841409264  : public CachingComparer_1_t1378817919
{
public:
	// System.Func`2<TElement,TKey> System.Linq.CachingComparer`2::_keySelector
	Func_2_t2447130374 * ____keySelector_0;
	// System.Collections.Generic.IComparer`1<TKey> System.Linq.CachingComparer`2::_comparer
	RuntimeObject* ____comparer_1;
	// System.Boolean System.Linq.CachingComparer`2::_descending
	bool ____descending_2;
	// TKey System.Linq.CachingComparer`2::_lastKey
	RuntimeObject * ____lastKey_3;

public:
	inline static int32_t get_offset_of__keySelector_0() { return static_cast<int32_t>(offsetof(CachingComparer_2_t2841409264, ____keySelector_0)); }
	inline Func_2_t2447130374 * get__keySelector_0() const { return ____keySelector_0; }
	inline Func_2_t2447130374 ** get_address_of__keySelector_0() { return &____keySelector_0; }
	inline void set__keySelector_0(Func_2_t2447130374 * value)
	{
		____keySelector_0 = value;
		Il2CppCodeGenWriteBarrier((&____keySelector_0), value);
	}

	inline static int32_t get_offset_of__comparer_1() { return static_cast<int32_t>(offsetof(CachingComparer_2_t2841409264, ____comparer_1)); }
	inline RuntimeObject* get__comparer_1() const { return ____comparer_1; }
	inline RuntimeObject** get_address_of__comparer_1() { return &____comparer_1; }
	inline void set__comparer_1(RuntimeObject* value)
	{
		____comparer_1 = value;
		Il2CppCodeGenWriteBarrier((&____comparer_1), value);
	}

	inline static int32_t get_offset_of__descending_2() { return static_cast<int32_t>(offsetof(CachingComparer_2_t2841409264, ____descending_2)); }
	inline bool get__descending_2() const { return ____descending_2; }
	inline bool* get_address_of__descending_2() { return &____descending_2; }
	inline void set__descending_2(bool value)
	{
		____descending_2 = value;
	}

	inline static int32_t get_offset_of__lastKey_3() { return static_cast<int32_t>(offsetof(CachingComparer_2_t2841409264, ____lastKey_3)); }
	inline RuntimeObject * get__lastKey_3() const { return ____lastKey_3; }
	inline RuntimeObject ** get_address_of__lastKey_3() { return &____lastKey_3; }
	inline void set__lastKey_3(RuntimeObject * value)
	{
		____lastKey_3 = value;
		Il2CppCodeGenWriteBarrier((&____lastKey_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHINGCOMPARER_2_T2841409264_H
#ifndef CACHINGCOMPARER_2_T2712248853_H
#define CACHINGCOMPARER_2_T2712248853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.CachingComparer`2<System.Object,System.Int32>
struct  CachingComparer_2_t2712248853  : public CachingComparer_1_t1378817919
{
public:
	// System.Func`2<TElement,TKey> System.Linq.CachingComparer`2::_keySelector
	Func_2_t2317969963 * ____keySelector_0;
	// System.Collections.Generic.IComparer`1<TKey> System.Linq.CachingComparer`2::_comparer
	RuntimeObject* ____comparer_1;
	// System.Boolean System.Linq.CachingComparer`2::_descending
	bool ____descending_2;
	// TKey System.Linq.CachingComparer`2::_lastKey
	int32_t ____lastKey_3;

public:
	inline static int32_t get_offset_of__keySelector_0() { return static_cast<int32_t>(offsetof(CachingComparer_2_t2712248853, ____keySelector_0)); }
	inline Func_2_t2317969963 * get__keySelector_0() const { return ____keySelector_0; }
	inline Func_2_t2317969963 ** get_address_of__keySelector_0() { return &____keySelector_0; }
	inline void set__keySelector_0(Func_2_t2317969963 * value)
	{
		____keySelector_0 = value;
		Il2CppCodeGenWriteBarrier((&____keySelector_0), value);
	}

	inline static int32_t get_offset_of__comparer_1() { return static_cast<int32_t>(offsetof(CachingComparer_2_t2712248853, ____comparer_1)); }
	inline RuntimeObject* get__comparer_1() const { return ____comparer_1; }
	inline RuntimeObject** get_address_of__comparer_1() { return &____comparer_1; }
	inline void set__comparer_1(RuntimeObject* value)
	{
		____comparer_1 = value;
		Il2CppCodeGenWriteBarrier((&____comparer_1), value);
	}

	inline static int32_t get_offset_of__descending_2() { return static_cast<int32_t>(offsetof(CachingComparer_2_t2712248853, ____descending_2)); }
	inline bool get__descending_2() const { return ____descending_2; }
	inline bool* get_address_of__descending_2() { return &____descending_2; }
	inline void set__descending_2(bool value)
	{
		____descending_2 = value;
	}

	inline static int32_t get_offset_of__lastKey_3() { return static_cast<int32_t>(offsetof(CachingComparer_2_t2712248853, ____lastKey_3)); }
	inline int32_t get__lastKey_3() const { return ____lastKey_3; }
	inline int32_t* get_address_of__lastKey_3() { return &____lastKey_3; }
	inline void set__lastKey_3(int32_t value)
	{
		____lastKey_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHINGCOMPARER_2_T2712248853_H
#ifndef BUFFER_1_T1574751594_H
#define BUFFER_1_T1574751594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Buffer`1<UnityEngine.Vector3>
struct  Buffer_1_t1574751594 
{
public:
	// TElement[] System.Linq.Buffer`1::_items
	Vector3U5BU5D_t1718750761* ____items_0;
	// System.Int32 System.Linq.Buffer`1::_count
	int32_t ____count_1;

public:
	inline static int32_t get_offset_of__items_0() { return static_cast<int32_t>(offsetof(Buffer_1_t1574751594, ____items_0)); }
	inline Vector3U5BU5D_t1718750761* get__items_0() const { return ____items_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of__items_0() { return &____items_0; }
	inline void set__items_0(Vector3U5BU5D_t1718750761* value)
	{
		____items_0 = value;
		Il2CppCodeGenWriteBarrier((&____items_0), value);
	}

	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(Buffer_1_t1574751594, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFER_1_T1574751594_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef LARGEARRAYBUILDER_1_T2990459185_H
#define LARGEARRAYBUILDER_1_T2990459185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.LargeArrayBuilder`1<System.Object>
struct  LargeArrayBuilder_1_t2990459185 
{
public:
	// System.Int32 System.Collections.Generic.LargeArrayBuilder`1::_maxCapacity
	int32_t ____maxCapacity_0;
	// T[] System.Collections.Generic.LargeArrayBuilder`1::_first
	ObjectU5BU5D_t2843939325* ____first_1;
	// System.Collections.Generic.ArrayBuilder`1<T[]> System.Collections.Generic.LargeArrayBuilder`1::_buffers
	ArrayBuilder_1_t4127036005  ____buffers_2;
	// T[] System.Collections.Generic.LargeArrayBuilder`1::_current
	ObjectU5BU5D_t2843939325* ____current_3;
	// System.Int32 System.Collections.Generic.LargeArrayBuilder`1::_index
	int32_t ____index_4;
	// System.Int32 System.Collections.Generic.LargeArrayBuilder`1::_count
	int32_t ____count_5;

public:
	inline static int32_t get_offset_of__maxCapacity_0() { return static_cast<int32_t>(offsetof(LargeArrayBuilder_1_t2990459185, ____maxCapacity_0)); }
	inline int32_t get__maxCapacity_0() const { return ____maxCapacity_0; }
	inline int32_t* get_address_of__maxCapacity_0() { return &____maxCapacity_0; }
	inline void set__maxCapacity_0(int32_t value)
	{
		____maxCapacity_0 = value;
	}

	inline static int32_t get_offset_of__first_1() { return static_cast<int32_t>(offsetof(LargeArrayBuilder_1_t2990459185, ____first_1)); }
	inline ObjectU5BU5D_t2843939325* get__first_1() const { return ____first_1; }
	inline ObjectU5BU5D_t2843939325** get_address_of__first_1() { return &____first_1; }
	inline void set__first_1(ObjectU5BU5D_t2843939325* value)
	{
		____first_1 = value;
		Il2CppCodeGenWriteBarrier((&____first_1), value);
	}

	inline static int32_t get_offset_of__buffers_2() { return static_cast<int32_t>(offsetof(LargeArrayBuilder_1_t2990459185, ____buffers_2)); }
	inline ArrayBuilder_1_t4127036005  get__buffers_2() const { return ____buffers_2; }
	inline ArrayBuilder_1_t4127036005 * get_address_of__buffers_2() { return &____buffers_2; }
	inline void set__buffers_2(ArrayBuilder_1_t4127036005  value)
	{
		____buffers_2 = value;
	}

	inline static int32_t get_offset_of__current_3() { return static_cast<int32_t>(offsetof(LargeArrayBuilder_1_t2990459185, ____current_3)); }
	inline ObjectU5BU5D_t2843939325* get__current_3() const { return ____current_3; }
	inline ObjectU5BU5D_t2843939325** get_address_of__current_3() { return &____current_3; }
	inline void set__current_3(ObjectU5BU5D_t2843939325* value)
	{
		____current_3 = value;
		Il2CppCodeGenWriteBarrier((&____current_3), value);
	}

	inline static int32_t get_offset_of__index_4() { return static_cast<int32_t>(offsetof(LargeArrayBuilder_1_t2990459185, ____index_4)); }
	inline int32_t get__index_4() const { return ____index_4; }
	inline int32_t* get_address_of__index_4() { return &____index_4; }
	inline void set__index_4(int32_t value)
	{
		____index_4 = value;
	}

	inline static int32_t get_offset_of__count_5() { return static_cast<int32_t>(offsetof(LargeArrayBuilder_1_t2990459185, ____count_5)); }
	inline int32_t get__count_5() const { return ____count_5; }
	inline int32_t* get_address_of__count_5() { return &____count_5; }
	inline void set__count_5(int32_t value)
	{
		____count_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LARGEARRAYBUILDER_1_T2990459185_H
#ifndef LARGEARRAYBUILDER_1_T2440570340_H
#define LARGEARRAYBUILDER_1_T2440570340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.LargeArrayBuilder`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  LargeArrayBuilder_1_t2440570340 
{
public:
	// System.Int32 System.Collections.Generic.LargeArrayBuilder`1::_maxCapacity
	int32_t ____maxCapacity_0;
	// T[] System.Collections.Generic.LargeArrayBuilder`1::_first
	KeyValuePair_2U5BU5D_t118269214* ____first_1;
	// System.Collections.Generic.ArrayBuilder`1<T[]> System.Collections.Generic.LargeArrayBuilder`1::_buffers
	ArrayBuilder_1_t1401365894  ____buffers_2;
	// T[] System.Collections.Generic.LargeArrayBuilder`1::_current
	KeyValuePair_2U5BU5D_t118269214* ____current_3;
	// System.Int32 System.Collections.Generic.LargeArrayBuilder`1::_index
	int32_t ____index_4;
	// System.Int32 System.Collections.Generic.LargeArrayBuilder`1::_count
	int32_t ____count_5;

public:
	inline static int32_t get_offset_of__maxCapacity_0() { return static_cast<int32_t>(offsetof(LargeArrayBuilder_1_t2440570340, ____maxCapacity_0)); }
	inline int32_t get__maxCapacity_0() const { return ____maxCapacity_0; }
	inline int32_t* get_address_of__maxCapacity_0() { return &____maxCapacity_0; }
	inline void set__maxCapacity_0(int32_t value)
	{
		____maxCapacity_0 = value;
	}

	inline static int32_t get_offset_of__first_1() { return static_cast<int32_t>(offsetof(LargeArrayBuilder_1_t2440570340, ____first_1)); }
	inline KeyValuePair_2U5BU5D_t118269214* get__first_1() const { return ____first_1; }
	inline KeyValuePair_2U5BU5D_t118269214** get_address_of__first_1() { return &____first_1; }
	inline void set__first_1(KeyValuePair_2U5BU5D_t118269214* value)
	{
		____first_1 = value;
		Il2CppCodeGenWriteBarrier((&____first_1), value);
	}

	inline static int32_t get_offset_of__buffers_2() { return static_cast<int32_t>(offsetof(LargeArrayBuilder_1_t2440570340, ____buffers_2)); }
	inline ArrayBuilder_1_t1401365894  get__buffers_2() const { return ____buffers_2; }
	inline ArrayBuilder_1_t1401365894 * get_address_of__buffers_2() { return &____buffers_2; }
	inline void set__buffers_2(ArrayBuilder_1_t1401365894  value)
	{
		____buffers_2 = value;
	}

	inline static int32_t get_offset_of__current_3() { return static_cast<int32_t>(offsetof(LargeArrayBuilder_1_t2440570340, ____current_3)); }
	inline KeyValuePair_2U5BU5D_t118269214* get__current_3() const { return ____current_3; }
	inline KeyValuePair_2U5BU5D_t118269214** get_address_of__current_3() { return &____current_3; }
	inline void set__current_3(KeyValuePair_2U5BU5D_t118269214* value)
	{
		____current_3 = value;
		Il2CppCodeGenWriteBarrier((&____current_3), value);
	}

	inline static int32_t get_offset_of__index_4() { return static_cast<int32_t>(offsetof(LargeArrayBuilder_1_t2440570340, ____index_4)); }
	inline int32_t get__index_4() const { return ____index_4; }
	inline int32_t* get_address_of__index_4() { return &____index_4; }
	inline void set__index_4(int32_t value)
	{
		____index_4 = value;
	}

	inline static int32_t get_offset_of__count_5() { return static_cast<int32_t>(offsetof(LargeArrayBuilder_1_t2440570340, ____count_5)); }
	inline int32_t get__count_5() const { return ____count_5; }
	inline int32_t* get_address_of__count_5() { return &____count_5; }
	inline void set__count_5(int32_t value)
	{
		____count_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LARGEARRAYBUILDER_1_T2440570340_H
#ifndef UNIONITERATOR2_1_T767380426_H
#define UNIONITERATOR2_1_T767380426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/UnionIterator2`1<System.Object>
struct  UnionIterator2_1_t767380426  : public UnionIterator_1_t2847646941
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator2`1::_first
	RuntimeObject* ____first_6;
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator2`1::_second
	RuntimeObject* ____second_7;

public:
	inline static int32_t get_offset_of__first_6() { return static_cast<int32_t>(offsetof(UnionIterator2_1_t767380426, ____first_6)); }
	inline RuntimeObject* get__first_6() const { return ____first_6; }
	inline RuntimeObject** get_address_of__first_6() { return &____first_6; }
	inline void set__first_6(RuntimeObject* value)
	{
		____first_6 = value;
		Il2CppCodeGenWriteBarrier((&____first_6), value);
	}

	inline static int32_t get_offset_of__second_7() { return static_cast<int32_t>(offsetof(UnionIterator2_1_t767380426, ____second_7)); }
	inline RuntimeObject* get__second_7() const { return ____second_7; }
	inline RuntimeObject** get_address_of__second_7() { return &____second_7; }
	inline void set__second_7(RuntimeObject* value)
	{
		____second_7 = value;
		Il2CppCodeGenWriteBarrier((&____second_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIONITERATOR2_1_T767380426_H
#ifndef UNIONITERATOR2_1_T1321734732_H
#define UNIONITERATOR2_1_T1321734732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/UnionIterator2`1<System.Char>
struct  UnionIterator2_1_t1321734732  : public UnionIterator_1_t3402001247
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator2`1::_first
	RuntimeObject* ____first_6;
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator2`1::_second
	RuntimeObject* ____second_7;

public:
	inline static int32_t get_offset_of__first_6() { return static_cast<int32_t>(offsetof(UnionIterator2_1_t1321734732, ____first_6)); }
	inline RuntimeObject* get__first_6() const { return ____first_6; }
	inline RuntimeObject** get_address_of__first_6() { return &____first_6; }
	inline void set__first_6(RuntimeObject* value)
	{
		____first_6 = value;
		Il2CppCodeGenWriteBarrier((&____first_6), value);
	}

	inline static int32_t get_offset_of__second_7() { return static_cast<int32_t>(offsetof(UnionIterator2_1_t1321734732, ____second_7)); }
	inline RuntimeObject* get__second_7() const { return ____second_7; }
	inline RuntimeObject** get_address_of__second_7() { return &____second_7; }
	inline void set__second_7(RuntimeObject* value)
	{
		____second_7 = value;
		Il2CppCodeGenWriteBarrier((&____second_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIONITERATOR2_1_T1321734732_H
#ifndef UNIONITERATORN_1_T718305142_H
#define UNIONITERATORN_1_T718305142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/UnionIteratorN`1<System.Object>
struct  UnionIteratorN_1_t718305142  : public UnionIterator_1_t2847646941
{
public:
	// System.Linq.Enumerable/UnionIterator`1<TSource> System.Linq.Enumerable/UnionIteratorN`1::_previous
	UnionIterator_1_t2847646941 * ____previous_6;
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIteratorN`1::_next
	RuntimeObject* ____next_7;
	// System.Int32 System.Linq.Enumerable/UnionIteratorN`1::_nextIndex
	int32_t ____nextIndex_8;

public:
	inline static int32_t get_offset_of__previous_6() { return static_cast<int32_t>(offsetof(UnionIteratorN_1_t718305142, ____previous_6)); }
	inline UnionIterator_1_t2847646941 * get__previous_6() const { return ____previous_6; }
	inline UnionIterator_1_t2847646941 ** get_address_of__previous_6() { return &____previous_6; }
	inline void set__previous_6(UnionIterator_1_t2847646941 * value)
	{
		____previous_6 = value;
		Il2CppCodeGenWriteBarrier((&____previous_6), value);
	}

	inline static int32_t get_offset_of__next_7() { return static_cast<int32_t>(offsetof(UnionIteratorN_1_t718305142, ____next_7)); }
	inline RuntimeObject* get__next_7() const { return ____next_7; }
	inline RuntimeObject** get_address_of__next_7() { return &____next_7; }
	inline void set__next_7(RuntimeObject* value)
	{
		____next_7 = value;
		Il2CppCodeGenWriteBarrier((&____next_7), value);
	}

	inline static int32_t get_offset_of__nextIndex_8() { return static_cast<int32_t>(offsetof(UnionIteratorN_1_t718305142, ____nextIndex_8)); }
	inline int32_t get__nextIndex_8() const { return ____nextIndex_8; }
	inline int32_t* get_address_of__nextIndex_8() { return &____nextIndex_8; }
	inline void set__nextIndex_8(int32_t value)
	{
		____nextIndex_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIONITERATORN_1_T718305142_H
#ifndef ENUMERATOR_T2190326961_H
#define ENUMERATOR_T2190326961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Collections.DictionaryEntry>
struct  Enumerator_t2190326961 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t301083084 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	DictionaryEntry_t3123975638  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t2190326961, ___list_0)); }
	inline List_1_t301083084 * get_list_0() const { return ___list_0; }
	inline List_1_t301083084 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t301083084 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t2190326961, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t2190326961, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2190326961, ___current_3)); }
	inline DictionaryEntry_t3123975638  get_current_3() const { return ___current_3; }
	inline DictionaryEntry_t3123975638 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(DictionaryEntry_t3123975638  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2190326961_H
#ifndef SELECTLISTITERATOR_2_T1742702623_H
#define SELECTLISTITERATOR_2_T1742702623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/SelectListIterator`2<System.Object,System.Object>
struct  SelectListIterator_2_t1742702623  : public Iterator_1_t2034466501
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/SelectListIterator`2::_source
	List_1_t257213610 * ____source_3;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/SelectListIterator`2::_selector
	Func_2_t2447130374 * ____selector_4;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/SelectListIterator`2::_enumerator
	Enumerator_t2146457487  ____enumerator_5;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(SelectListIterator_2_t1742702623, ____source_3)); }
	inline List_1_t257213610 * get__source_3() const { return ____source_3; }
	inline List_1_t257213610 ** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(List_1_t257213610 * value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__selector_4() { return static_cast<int32_t>(offsetof(SelectListIterator_2_t1742702623, ____selector_4)); }
	inline Func_2_t2447130374 * get__selector_4() const { return ____selector_4; }
	inline Func_2_t2447130374 ** get_address_of__selector_4() { return &____selector_4; }
	inline void set__selector_4(Func_2_t2447130374 * value)
	{
		____selector_4 = value;
		Il2CppCodeGenWriteBarrier((&____selector_4), value);
	}

	inline static int32_t get_offset_of__enumerator_5() { return static_cast<int32_t>(offsetof(SelectListIterator_2_t1742702623, ____enumerator_5)); }
	inline Enumerator_t2146457487  get__enumerator_5() const { return ____enumerator_5; }
	inline Enumerator_t2146457487 * get_address_of__enumerator_5() { return &____enumerator_5; }
	inline void set__enumerator_5(Enumerator_t2146457487  value)
	{
		____enumerator_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTLISTITERATOR_2_T1742702623_H
#ifndef UNIONITERATORN_1_T1272659448_H
#define UNIONITERATORN_1_T1272659448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/UnionIteratorN`1<System.Char>
struct  UnionIteratorN_1_t1272659448  : public UnionIterator_1_t3402001247
{
public:
	// System.Linq.Enumerable/UnionIterator`1<TSource> System.Linq.Enumerable/UnionIteratorN`1::_previous
	UnionIterator_1_t3402001247 * ____previous_6;
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIteratorN`1::_next
	RuntimeObject* ____next_7;
	// System.Int32 System.Linq.Enumerable/UnionIteratorN`1::_nextIndex
	int32_t ____nextIndex_8;

public:
	inline static int32_t get_offset_of__previous_6() { return static_cast<int32_t>(offsetof(UnionIteratorN_1_t1272659448, ____previous_6)); }
	inline UnionIterator_1_t3402001247 * get__previous_6() const { return ____previous_6; }
	inline UnionIterator_1_t3402001247 ** get_address_of__previous_6() { return &____previous_6; }
	inline void set__previous_6(UnionIterator_1_t3402001247 * value)
	{
		____previous_6 = value;
		Il2CppCodeGenWriteBarrier((&____previous_6), value);
	}

	inline static int32_t get_offset_of__next_7() { return static_cast<int32_t>(offsetof(UnionIteratorN_1_t1272659448, ____next_7)); }
	inline RuntimeObject* get__next_7() const { return ____next_7; }
	inline RuntimeObject** get_address_of__next_7() { return &____next_7; }
	inline void set__next_7(RuntimeObject* value)
	{
		____next_7 = value;
		Il2CppCodeGenWriteBarrier((&____next_7), value);
	}

	inline static int32_t get_offset_of__nextIndex_8() { return static_cast<int32_t>(offsetof(UnionIteratorN_1_t1272659448, ____nextIndex_8)); }
	inline int32_t get__nextIndex_8() const { return ____nextIndex_8; }
	inline int32_t* get_address_of__nextIndex_8() { return &____nextIndex_8; }
	inline void set__nextIndex_8(int32_t value)
	{
		____nextIndex_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIONITERATORN_1_T1272659448_H
#ifndef CACHINGCOMPARERWITHCHILD_2_T3364548056_H
#define CACHINGCOMPARERWITHCHILD_2_T3364548056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.CachingComparerWithChild`2<UnityEngine.Vector2,System.Single>
struct  CachingComparerWithChild_2_t3364548056  : public CachingComparer_2_t4066783831
{
public:
	// System.Linq.CachingComparer`1<TElement> System.Linq.CachingComparerWithChild`2::_child
	CachingComparer_1_t454941278 * ____child_4;

public:
	inline static int32_t get_offset_of__child_4() { return static_cast<int32_t>(offsetof(CachingComparerWithChild_2_t3364548056, ____child_4)); }
	inline CachingComparer_1_t454941278 * get__child_4() const { return ____child_4; }
	inline CachingComparer_1_t454941278 ** get_address_of__child_4() { return &____child_4; }
	inline void set__child_4(CachingComparer_1_t454941278 * value)
	{
		____child_4 = value;
		Il2CppCodeGenWriteBarrier((&____child_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHINGCOMPARERWITHCHILD_2_T3364548056_H
#ifndef CACHINGCOMPARERWITHCHILD_2_T3626112831_H
#define CACHINGCOMPARERWITHCHILD_2_T3626112831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.CachingComparerWithChild`2<UnityEngine.Vector3,System.Single>
struct  CachingComparerWithChild_2_t3626112831  : public CachingComparer_2_t33381310
{
public:
	// System.Linq.CachingComparer`1<TElement> System.Linq.CachingComparerWithChild`2::_child
	CachingComparer_1_t2021025219 * ____child_4;

public:
	inline static int32_t get_offset_of__child_4() { return static_cast<int32_t>(offsetof(CachingComparerWithChild_2_t3626112831, ____child_4)); }
	inline CachingComparer_1_t2021025219 * get__child_4() const { return ____child_4; }
	inline CachingComparer_1_t2021025219 ** get_address_of__child_4() { return &____child_4; }
	inline void set__child_4(CachingComparer_1_t2021025219 * value)
	{
		____child_4 = value;
		Il2CppCodeGenWriteBarrier((&____child_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHINGCOMPARERWITHCHILD_2_T3626112831_H
#ifndef CACHINGCOMPARERWITHCHILD_2_T2010013078_H
#define CACHINGCOMPARERWITHCHILD_2_T2010013078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.CachingComparerWithChild`2<System.Object,System.Int32>
struct  CachingComparerWithChild_2_t2010013078  : public CachingComparer_2_t2712248853
{
public:
	// System.Linq.CachingComparer`1<TElement> System.Linq.CachingComparerWithChild`2::_child
	CachingComparer_1_t1378817919 * ____child_4;

public:
	inline static int32_t get_offset_of__child_4() { return static_cast<int32_t>(offsetof(CachingComparerWithChild_2_t2010013078, ____child_4)); }
	inline CachingComparer_1_t1378817919 * get__child_4() const { return ____child_4; }
	inline CachingComparer_1_t1378817919 ** get_address_of__child_4() { return &____child_4; }
	inline void set__child_4(CachingComparer_1_t1378817919 * value)
	{
		____child_4 = value;
		Il2CppCodeGenWriteBarrier((&____child_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHINGCOMPARERWITHCHILD_2_T2010013078_H
#ifndef CACHINGCOMPARERWITHCHILD_2_T2139173489_H
#define CACHINGCOMPARERWITHCHILD_2_T2139173489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.CachingComparerWithChild`2<System.Object,System.Object>
struct  CachingComparerWithChild_2_t2139173489  : public CachingComparer_2_t2841409264
{
public:
	// System.Linq.CachingComparer`1<TElement> System.Linq.CachingComparerWithChild`2::_child
	CachingComparer_1_t1378817919 * ____child_4;

public:
	inline static int32_t get_offset_of__child_4() { return static_cast<int32_t>(offsetof(CachingComparerWithChild_2_t2139173489, ____child_4)); }
	inline CachingComparer_1_t1378817919 * get__child_4() const { return ____child_4; }
	inline CachingComparer_1_t1378817919 ** get_address_of__child_4() { return &____child_4; }
	inline void set__child_4(CachingComparer_1_t1378817919 * value)
	{
		____child_4 = value;
		Il2CppCodeGenWriteBarrier((&____child_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHINGCOMPARERWITHCHILD_2_T2139173489_H
#ifndef ITERATOR_1_T1484577656_H
#define ITERATOR_1_T1484577656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  Iterator_1_t1484577656  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::_threadId
	int32_t ____threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::_state
	int32_t ____state_1;
	// TSource System.Linq.Enumerable/Iterator`1::_current
	KeyValuePair_2_t2530217319  ____current_2;

public:
	inline static int32_t get_offset_of__threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t1484577656, ____threadId_0)); }
	inline int32_t get__threadId_0() const { return ____threadId_0; }
	inline int32_t* get_address_of__threadId_0() { return &____threadId_0; }
	inline void set__threadId_0(int32_t value)
	{
		____threadId_0 = value;
	}

	inline static int32_t get_offset_of__state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t1484577656, ____state_1)); }
	inline int32_t get__state_1() const { return ____state_1; }
	inline int32_t* get_address_of__state_1() { return &____state_1; }
	inline void set__state_1(int32_t value)
	{
		____state_1 = value;
	}

	inline static int32_t get_offset_of__current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t1484577656, ____current_2)); }
	inline KeyValuePair_2_t2530217319  get__current_2() const { return ____current_2; }
	inline KeyValuePair_2_t2530217319 * get_address_of__current_2() { return &____current_2; }
	inline void set__current_2(KeyValuePair_2_t2530217319  value)
	{
		____current_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITERATOR_1_T1484577656_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef U3CCASTITERATORU3ED__35_1_T2794392488_H
#define U3CCASTITERATORU3ED__35_1_T2794392488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/<CastIterator>d__35`1<System.Collections.DictionaryEntry>
struct  U3CCastIteratorU3Ed__35_1_t2794392488  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/<CastIterator>d__35`1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// TResult System.Linq.Enumerable/<CastIterator>d__35`1::<>2__current
	DictionaryEntry_t3123975638  ___U3CU3E2__current_1;
	// System.Int32 System.Linq.Enumerable/<CastIterator>d__35`1::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.IEnumerable System.Linq.Enumerable/<CastIterator>d__35`1::source
	RuntimeObject* ___source_3;
	// System.Collections.IEnumerable System.Linq.Enumerable/<CastIterator>d__35`1::<>3__source
	RuntimeObject* ___U3CU3E3__source_4;
	// System.Collections.IEnumerator System.Linq.Enumerable/<CastIterator>d__35`1::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCastIteratorU3Ed__35_1_t2794392488, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCastIteratorU3Ed__35_1_t2794392488, ___U3CU3E2__current_1)); }
	inline DictionaryEntry_t3123975638  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline DictionaryEntry_t3123975638 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(DictionaryEntry_t3123975638  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CCastIteratorU3Ed__35_1_t2794392488, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(U3CCastIteratorU3Ed__35_1_t2794392488, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((&___source_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__source_4() { return static_cast<int32_t>(offsetof(U3CCastIteratorU3Ed__35_1_t2794392488, ___U3CU3E3__source_4)); }
	inline RuntimeObject* get_U3CU3E3__source_4() const { return ___U3CU3E3__source_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__source_4() { return &___U3CU3E3__source_4; }
	inline void set_U3CU3E3__source_4(RuntimeObject* value)
	{
		___U3CU3E3__source_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__source_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CCastIteratorU3Ed__35_1_t2794392488, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCASTITERATORU3ED__35_1_T2794392488_H
#ifndef SEARCHOPTION_T2353013399_H
#define SEARCHOPTION_T2353013399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.SearchOption
struct  SearchOption_t2353013399 
{
public:
	// System.Int32 System.IO.SearchOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SearchOption_t2353013399, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHOPTION_T2353013399_H
#ifndef ITERATOR_1_T2078335975_H
#define ITERATOR_1_T2078335975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/Iterator`1<System.Collections.DictionaryEntry>
struct  Iterator_1_t2078335975  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::_threadId
	int32_t ____threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::_state
	int32_t ____state_1;
	// TSource System.Linq.Enumerable/Iterator`1::_current
	DictionaryEntry_t3123975638  ____current_2;

public:
	inline static int32_t get_offset_of__threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t2078335975, ____threadId_0)); }
	inline int32_t get__threadId_0() const { return ____threadId_0; }
	inline int32_t* get_address_of__threadId_0() { return &____threadId_0; }
	inline void set__threadId_0(int32_t value)
	{
		____threadId_0 = value;
	}

	inline static int32_t get_offset_of__state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t2078335975, ____state_1)); }
	inline int32_t get__state_1() const { return ____state_1; }
	inline int32_t* get_address_of__state_1() { return &____state_1; }
	inline void set__state_1(int32_t value)
	{
		____state_1 = value;
	}

	inline static int32_t get_offset_of__current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t2078335975, ____current_2)); }
	inline DictionaryEntry_t3123975638  get__current_2() const { return ____current_2; }
	inline DictionaryEntry_t3123975638 * get_address_of__current_2() { return &____current_2; }
	inline void set__current_2(DictionaryEntry_t3123975638  value)
	{
		____current_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITERATOR_1_T2078335975_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef SAFEHANDLE_T3273388951_H
#define SAFEHANDLE_T3273388951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.SafeHandle
struct  SafeHandle_t3273388951  : public CriticalFinalizerObject_t701527852
{
public:
	// System.IntPtr System.Runtime.InteropServices.SafeHandle::handle
	intptr_t ___handle_2;
	// System.Int32 System.Runtime.InteropServices.SafeHandle::_state
	int32_t ____state_3;
	// System.Boolean System.Runtime.InteropServices.SafeHandle::_ownsHandle
	bool ____ownsHandle_4;
	// System.Boolean System.Runtime.InteropServices.SafeHandle::_fullyInitialized
	bool ____fullyInitialized_5;

public:
	inline static int32_t get_offset_of_handle_2() { return static_cast<int32_t>(offsetof(SafeHandle_t3273388951, ___handle_2)); }
	inline intptr_t get_handle_2() const { return ___handle_2; }
	inline intptr_t* get_address_of_handle_2() { return &___handle_2; }
	inline void set_handle_2(intptr_t value)
	{
		___handle_2 = value;
	}

	inline static int32_t get_offset_of__state_3() { return static_cast<int32_t>(offsetof(SafeHandle_t3273388951, ____state_3)); }
	inline int32_t get__state_3() const { return ____state_3; }
	inline int32_t* get_address_of__state_3() { return &____state_3; }
	inline void set__state_3(int32_t value)
	{
		____state_3 = value;
	}

	inline static int32_t get_offset_of__ownsHandle_4() { return static_cast<int32_t>(offsetof(SafeHandle_t3273388951, ____ownsHandle_4)); }
	inline bool get__ownsHandle_4() const { return ____ownsHandle_4; }
	inline bool* get_address_of__ownsHandle_4() { return &____ownsHandle_4; }
	inline void set__ownsHandle_4(bool value)
	{
		____ownsHandle_4 = value;
	}

	inline static int32_t get_offset_of__fullyInitialized_5() { return static_cast<int32_t>(offsetof(SafeHandle_t3273388951, ____fullyInitialized_5)); }
	inline bool get__fullyInitialized_5() const { return ____fullyInitialized_5; }
	inline bool* get_address_of__fullyInitialized_5() { return &____fullyInitialized_5; }
	inline void set__fullyInitialized_5(bool value)
	{
		____fullyInitialized_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEHANDLE_T3273388951_H
#ifndef SELECTENUMERABLEITERATOR_2_T760503204_H
#define SELECTENUMERABLEITERATOR_2_T760503204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  SelectEnumerableIterator_2_t760503204  : public Iterator_1_t1484577656
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/SelectEnumerableIterator`2::_source
	RuntimeObject* ____source_3;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/SelectEnumerableIterator`2::_selector
	Func_2_t3270419407 * ____selector_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/SelectEnumerableIterator`2::_enumerator
	RuntimeObject* ____enumerator_5;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(SelectEnumerableIterator_2_t760503204, ____source_3)); }
	inline RuntimeObject* get__source_3() const { return ____source_3; }
	inline RuntimeObject** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(RuntimeObject* value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__selector_4() { return static_cast<int32_t>(offsetof(SelectEnumerableIterator_2_t760503204, ____selector_4)); }
	inline Func_2_t3270419407 * get__selector_4() const { return ____selector_4; }
	inline Func_2_t3270419407 ** get_address_of__selector_4() { return &____selector_4; }
	inline void set__selector_4(Func_2_t3270419407 * value)
	{
		____selector_4 = value;
		Il2CppCodeGenWriteBarrier((&____selector_4), value);
	}

	inline static int32_t get_offset_of__enumerator_5() { return static_cast<int32_t>(offsetof(SelectEnumerableIterator_2_t760503204, ____enumerator_5)); }
	inline RuntimeObject* get__enumerator_5() const { return ____enumerator_5; }
	inline RuntimeObject** get_address_of__enumerator_5() { return &____enumerator_5; }
	inline void set__enumerator_5(RuntimeObject* value)
	{
		____enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((&____enumerator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTENUMERABLEITERATOR_2_T760503204_H
#ifndef FILESYSTEMENUMERABLEITERATOR_1_T25181536_H
#define FILESYSTEMENUMERABLEITERATOR_1_T25181536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemEnumerableIterator`1<System.Object>
struct  FileSystemEnumerableIterator_1_t25181536  : public Iterator_1_t3764629478
{
public:
	// System.IO.SearchResultHandler`1<TSource> System.IO.FileSystemEnumerableIterator`1::_resultHandler
	SearchResultHandler_1_t2377980137 * ____resultHandler_3;
	// System.Collections.Generic.List`1<System.IO.Directory/SearchData> System.IO.FileSystemEnumerableIterator`1::searchStack
	List_1_t4120301035 * ___searchStack_4;
	// System.IO.Directory/SearchData System.IO.FileSystemEnumerableIterator`1::searchData
	SearchData_t2648226293 * ___searchData_5;
	// System.String System.IO.FileSystemEnumerableIterator`1::searchCriteria
	String_t* ___searchCriteria_6;
	// Microsoft.Win32.SafeHandles.SafeFindHandle System.IO.FileSystemEnumerableIterator`1::_hnd
	SafeFindHandle_t2068834300 * ____hnd_7;
	// System.Boolean System.IO.FileSystemEnumerableIterator`1::needsParentPathDiscoveryDemand
	bool ___needsParentPathDiscoveryDemand_8;
	// System.Boolean System.IO.FileSystemEnumerableIterator`1::empty
	bool ___empty_9;
	// System.String System.IO.FileSystemEnumerableIterator`1::userPath
	String_t* ___userPath_10;
	// System.IO.SearchOption System.IO.FileSystemEnumerableIterator`1::searchOption
	int32_t ___searchOption_11;
	// System.String System.IO.FileSystemEnumerableIterator`1::fullPath
	String_t* ___fullPath_12;
	// System.String System.IO.FileSystemEnumerableIterator`1::normalizedSearchPath
	String_t* ___normalizedSearchPath_13;
	// System.Boolean System.IO.FileSystemEnumerableIterator`1::_checkHost
	bool ____checkHost_14;

public:
	inline static int32_t get_offset_of__resultHandler_3() { return static_cast<int32_t>(offsetof(FileSystemEnumerableIterator_1_t25181536, ____resultHandler_3)); }
	inline SearchResultHandler_1_t2377980137 * get__resultHandler_3() const { return ____resultHandler_3; }
	inline SearchResultHandler_1_t2377980137 ** get_address_of__resultHandler_3() { return &____resultHandler_3; }
	inline void set__resultHandler_3(SearchResultHandler_1_t2377980137 * value)
	{
		____resultHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&____resultHandler_3), value);
	}

	inline static int32_t get_offset_of_searchStack_4() { return static_cast<int32_t>(offsetof(FileSystemEnumerableIterator_1_t25181536, ___searchStack_4)); }
	inline List_1_t4120301035 * get_searchStack_4() const { return ___searchStack_4; }
	inline List_1_t4120301035 ** get_address_of_searchStack_4() { return &___searchStack_4; }
	inline void set_searchStack_4(List_1_t4120301035 * value)
	{
		___searchStack_4 = value;
		Il2CppCodeGenWriteBarrier((&___searchStack_4), value);
	}

	inline static int32_t get_offset_of_searchData_5() { return static_cast<int32_t>(offsetof(FileSystemEnumerableIterator_1_t25181536, ___searchData_5)); }
	inline SearchData_t2648226293 * get_searchData_5() const { return ___searchData_5; }
	inline SearchData_t2648226293 ** get_address_of_searchData_5() { return &___searchData_5; }
	inline void set_searchData_5(SearchData_t2648226293 * value)
	{
		___searchData_5 = value;
		Il2CppCodeGenWriteBarrier((&___searchData_5), value);
	}

	inline static int32_t get_offset_of_searchCriteria_6() { return static_cast<int32_t>(offsetof(FileSystemEnumerableIterator_1_t25181536, ___searchCriteria_6)); }
	inline String_t* get_searchCriteria_6() const { return ___searchCriteria_6; }
	inline String_t** get_address_of_searchCriteria_6() { return &___searchCriteria_6; }
	inline void set_searchCriteria_6(String_t* value)
	{
		___searchCriteria_6 = value;
		Il2CppCodeGenWriteBarrier((&___searchCriteria_6), value);
	}

	inline static int32_t get_offset_of__hnd_7() { return static_cast<int32_t>(offsetof(FileSystemEnumerableIterator_1_t25181536, ____hnd_7)); }
	inline SafeFindHandle_t2068834300 * get__hnd_7() const { return ____hnd_7; }
	inline SafeFindHandle_t2068834300 ** get_address_of__hnd_7() { return &____hnd_7; }
	inline void set__hnd_7(SafeFindHandle_t2068834300 * value)
	{
		____hnd_7 = value;
		Il2CppCodeGenWriteBarrier((&____hnd_7), value);
	}

	inline static int32_t get_offset_of_needsParentPathDiscoveryDemand_8() { return static_cast<int32_t>(offsetof(FileSystemEnumerableIterator_1_t25181536, ___needsParentPathDiscoveryDemand_8)); }
	inline bool get_needsParentPathDiscoveryDemand_8() const { return ___needsParentPathDiscoveryDemand_8; }
	inline bool* get_address_of_needsParentPathDiscoveryDemand_8() { return &___needsParentPathDiscoveryDemand_8; }
	inline void set_needsParentPathDiscoveryDemand_8(bool value)
	{
		___needsParentPathDiscoveryDemand_8 = value;
	}

	inline static int32_t get_offset_of_empty_9() { return static_cast<int32_t>(offsetof(FileSystemEnumerableIterator_1_t25181536, ___empty_9)); }
	inline bool get_empty_9() const { return ___empty_9; }
	inline bool* get_address_of_empty_9() { return &___empty_9; }
	inline void set_empty_9(bool value)
	{
		___empty_9 = value;
	}

	inline static int32_t get_offset_of_userPath_10() { return static_cast<int32_t>(offsetof(FileSystemEnumerableIterator_1_t25181536, ___userPath_10)); }
	inline String_t* get_userPath_10() const { return ___userPath_10; }
	inline String_t** get_address_of_userPath_10() { return &___userPath_10; }
	inline void set_userPath_10(String_t* value)
	{
		___userPath_10 = value;
		Il2CppCodeGenWriteBarrier((&___userPath_10), value);
	}

	inline static int32_t get_offset_of_searchOption_11() { return static_cast<int32_t>(offsetof(FileSystemEnumerableIterator_1_t25181536, ___searchOption_11)); }
	inline int32_t get_searchOption_11() const { return ___searchOption_11; }
	inline int32_t* get_address_of_searchOption_11() { return &___searchOption_11; }
	inline void set_searchOption_11(int32_t value)
	{
		___searchOption_11 = value;
	}

	inline static int32_t get_offset_of_fullPath_12() { return static_cast<int32_t>(offsetof(FileSystemEnumerableIterator_1_t25181536, ___fullPath_12)); }
	inline String_t* get_fullPath_12() const { return ___fullPath_12; }
	inline String_t** get_address_of_fullPath_12() { return &___fullPath_12; }
	inline void set_fullPath_12(String_t* value)
	{
		___fullPath_12 = value;
		Il2CppCodeGenWriteBarrier((&___fullPath_12), value);
	}

	inline static int32_t get_offset_of_normalizedSearchPath_13() { return static_cast<int32_t>(offsetof(FileSystemEnumerableIterator_1_t25181536, ___normalizedSearchPath_13)); }
	inline String_t* get_normalizedSearchPath_13() const { return ___normalizedSearchPath_13; }
	inline String_t** get_address_of_normalizedSearchPath_13() { return &___normalizedSearchPath_13; }
	inline void set_normalizedSearchPath_13(String_t* value)
	{
		___normalizedSearchPath_13 = value;
		Il2CppCodeGenWriteBarrier((&___normalizedSearchPath_13), value);
	}

	inline static int32_t get_offset_of__checkHost_14() { return static_cast<int32_t>(offsetof(FileSystemEnumerableIterator_1_t25181536, ____checkHost_14)); }
	inline bool get__checkHost_14() const { return ____checkHost_14; }
	inline bool* get_address_of__checkHost_14() { return &____checkHost_14; }
	inline void set__checkHost_14(bool value)
	{
		____checkHost_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESYSTEMENUMERABLEITERATOR_1_T25181536_H
#ifndef SAFEHANDLEZEROORMINUSONEISINVALID_T1182193648_H
#define SAFEHANDLEZEROORMINUSONEISINVALID_T1182193648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
struct  SafeHandleZeroOrMinusOneIsInvalid_t1182193648  : public SafeHandle_t3273388951
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEHANDLEZEROORMINUSONEISINVALID_T1182193648_H
#ifndef SEARCHDATA_T2648226293_H
#define SEARCHDATA_T2648226293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Directory/SearchData
struct  SearchData_t2648226293  : public RuntimeObject
{
public:
	// System.String System.IO.Directory/SearchData::fullPath
	String_t* ___fullPath_0;
	// System.String System.IO.Directory/SearchData::userPath
	String_t* ___userPath_1;
	// System.IO.SearchOption System.IO.Directory/SearchData::searchOption
	int32_t ___searchOption_2;

public:
	inline static int32_t get_offset_of_fullPath_0() { return static_cast<int32_t>(offsetof(SearchData_t2648226293, ___fullPath_0)); }
	inline String_t* get_fullPath_0() const { return ___fullPath_0; }
	inline String_t** get_address_of_fullPath_0() { return &___fullPath_0; }
	inline void set_fullPath_0(String_t* value)
	{
		___fullPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___fullPath_0), value);
	}

	inline static int32_t get_offset_of_userPath_1() { return static_cast<int32_t>(offsetof(SearchData_t2648226293, ___userPath_1)); }
	inline String_t* get_userPath_1() const { return ___userPath_1; }
	inline String_t** get_address_of_userPath_1() { return &___userPath_1; }
	inline void set_userPath_1(String_t* value)
	{
		___userPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___userPath_1), value);
	}

	inline static int32_t get_offset_of_searchOption_2() { return static_cast<int32_t>(offsetof(SearchData_t2648226293, ___searchOption_2)); }
	inline int32_t get_searchOption_2() const { return ___searchOption_2; }
	inline int32_t* get_address_of_searchOption_2() { return &___searchOption_2; }
	inline void set_searchOption_2(int32_t value)
	{
		___searchOption_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHDATA_T2648226293_H
#ifndef MULTICASTDELEGATE_T157516450_H
#define MULTICASTDELEGATE_T157516450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t157516450  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t157516450, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T157516450_H
#ifndef SPARSEARRAYBUILDER_1_T3713953326_H
#define SPARSEARRAYBUILDER_1_T3713953326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SparseArrayBuilder`1<System.Object>
struct  SparseArrayBuilder_1_t3713953326 
{
public:
	// System.Collections.Generic.LargeArrayBuilder`1<T> System.Collections.Generic.SparseArrayBuilder`1::_builder
	LargeArrayBuilder_1_t2990459185  ____builder_0;
	// System.Collections.Generic.ArrayBuilder`1<System.Collections.Generic.Marker> System.Collections.Generic.SparseArrayBuilder`1::_markers
	ArrayBuilder_1_t4177874457  ____markers_1;
	// System.Int32 System.Collections.Generic.SparseArrayBuilder`1::_reservedCount
	int32_t ____reservedCount_2;

public:
	inline static int32_t get_offset_of__builder_0() { return static_cast<int32_t>(offsetof(SparseArrayBuilder_1_t3713953326, ____builder_0)); }
	inline LargeArrayBuilder_1_t2990459185  get__builder_0() const { return ____builder_0; }
	inline LargeArrayBuilder_1_t2990459185 * get_address_of__builder_0() { return &____builder_0; }
	inline void set__builder_0(LargeArrayBuilder_1_t2990459185  value)
	{
		____builder_0 = value;
	}

	inline static int32_t get_offset_of__markers_1() { return static_cast<int32_t>(offsetof(SparseArrayBuilder_1_t3713953326, ____markers_1)); }
	inline ArrayBuilder_1_t4177874457  get__markers_1() const { return ____markers_1; }
	inline ArrayBuilder_1_t4177874457 * get_address_of__markers_1() { return &____markers_1; }
	inline void set__markers_1(ArrayBuilder_1_t4177874457  value)
	{
		____markers_1 = value;
	}

	inline static int32_t get_offset_of__reservedCount_2() { return static_cast<int32_t>(offsetof(SparseArrayBuilder_1_t3713953326, ____reservedCount_2)); }
	inline int32_t get__reservedCount_2() const { return ____reservedCount_2; }
	inline int32_t* get_address_of__reservedCount_2() { return &____reservedCount_2; }
	inline void set__reservedCount_2(int32_t value)
	{
		____reservedCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPARSEARRAYBUILDER_1_T3713953326_H
#ifndef ITERATOR_1_T2740211830_H
#define ITERATOR_1_T2740211830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/Iterator`1<UnityEngine.Ray>
struct  Iterator_1_t2740211830  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::_threadId
	int32_t ____threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::_state
	int32_t ____state_1;
	// TSource System.Linq.Enumerable/Iterator`1::_current
	Ray_t3785851493  ____current_2;

public:
	inline static int32_t get_offset_of__threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t2740211830, ____threadId_0)); }
	inline int32_t get__threadId_0() const { return ____threadId_0; }
	inline int32_t* get_address_of__threadId_0() { return &____threadId_0; }
	inline void set__threadId_0(int32_t value)
	{
		____threadId_0 = value;
	}

	inline static int32_t get_offset_of__state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t2740211830, ____state_1)); }
	inline int32_t get__state_1() const { return ____state_1; }
	inline int32_t* get_address_of__state_1() { return &____state_1; }
	inline void set__state_1(int32_t value)
	{
		____state_1 = value;
	}

	inline static int32_t get_offset_of__current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t2740211830, ____current_2)); }
	inline Ray_t3785851493  get__current_2() const { return ____current_2; }
	inline Ray_t3785851493 * get_address_of__current_2() { return &____current_2; }
	inline void set__current_2(Ray_t3785851493  value)
	{
		____current_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITERATOR_1_T2740211830_H
#ifndef SELECTILISTITERATOR_2_T130090036_H
#define SELECTILISTITERATOR_2_T130090036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/SelectIListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  SelectIListIterator_2_t130090036  : public Iterator_1_t1484577656
{
public:
	// System.Collections.Generic.IList`1<TSource> System.Linq.Enumerable/SelectIListIterator`2::_source
	RuntimeObject* ____source_3;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/SelectIListIterator`2::_selector
	Func_2_t3270419407 * ____selector_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/SelectIListIterator`2::_enumerator
	RuntimeObject* ____enumerator_5;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(SelectIListIterator_2_t130090036, ____source_3)); }
	inline RuntimeObject* get__source_3() const { return ____source_3; }
	inline RuntimeObject** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(RuntimeObject* value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__selector_4() { return static_cast<int32_t>(offsetof(SelectIListIterator_2_t130090036, ____selector_4)); }
	inline Func_2_t3270419407 * get__selector_4() const { return ____selector_4; }
	inline Func_2_t3270419407 ** get_address_of__selector_4() { return &____selector_4; }
	inline void set__selector_4(Func_2_t3270419407 * value)
	{
		____selector_4 = value;
		Il2CppCodeGenWriteBarrier((&____selector_4), value);
	}

	inline static int32_t get_offset_of__enumerator_5() { return static_cast<int32_t>(offsetof(SelectIListIterator_2_t130090036, ____enumerator_5)); }
	inline RuntimeObject* get__enumerator_5() const { return ____enumerator_5; }
	inline RuntimeObject** get_address_of__enumerator_5() { return &____enumerator_5; }
	inline void set__enumerator_5(RuntimeObject* value)
	{
		____enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((&____enumerator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTILISTITERATOR_2_T130090036_H
#ifndef SELECTARRAYITERATOR_2_T1643067185_H
#define SELECTARRAYITERATOR_2_T1643067185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/SelectArrayIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  SelectArrayIterator_2_t1643067185  : public Iterator_1_t1484577656
{
public:
	// TSource[] System.Linq.Enumerable/SelectArrayIterator`2::_source
	DictionaryEntryU5BU5D_t4217117203* ____source_3;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/SelectArrayIterator`2::_selector
	Func_2_t3270419407 * ____selector_4;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(SelectArrayIterator_2_t1643067185, ____source_3)); }
	inline DictionaryEntryU5BU5D_t4217117203* get__source_3() const { return ____source_3; }
	inline DictionaryEntryU5BU5D_t4217117203** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(DictionaryEntryU5BU5D_t4217117203* value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__selector_4() { return static_cast<int32_t>(offsetof(SelectArrayIterator_2_t1643067185, ____selector_4)); }
	inline Func_2_t3270419407 * get__selector_4() const { return ____selector_4; }
	inline Func_2_t3270419407 ** get_address_of__selector_4() { return &____selector_4; }
	inline void set__selector_4(Func_2_t3270419407 * value)
	{
		____selector_4 = value;
		Il2CppCodeGenWriteBarrier((&____selector_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTARRAYITERATOR_2_T1643067185_H
#ifndef SELECTIPARTITIONITERATOR_2_T2954477804_H
#define SELECTIPARTITIONITERATOR_2_T2954477804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/SelectIPartitionIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  SelectIPartitionIterator_2_t2954477804  : public Iterator_1_t1484577656
{
public:
	// System.Linq.IPartition`1<TSource> System.Linq.Enumerable/SelectIPartitionIterator`2::_source
	RuntimeObject* ____source_3;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/SelectIPartitionIterator`2::_selector
	Func_2_t3270419407 * ____selector_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/SelectIPartitionIterator`2::_enumerator
	RuntimeObject* ____enumerator_5;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(SelectIPartitionIterator_2_t2954477804, ____source_3)); }
	inline RuntimeObject* get__source_3() const { return ____source_3; }
	inline RuntimeObject** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(RuntimeObject* value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__selector_4() { return static_cast<int32_t>(offsetof(SelectIPartitionIterator_2_t2954477804, ____selector_4)); }
	inline Func_2_t3270419407 * get__selector_4() const { return ____selector_4; }
	inline Func_2_t3270419407 ** get_address_of__selector_4() { return &____selector_4; }
	inline void set__selector_4(Func_2_t3270419407 * value)
	{
		____selector_4 = value;
		Il2CppCodeGenWriteBarrier((&____selector_4), value);
	}

	inline static int32_t get_offset_of__enumerator_5() { return static_cast<int32_t>(offsetof(SelectIPartitionIterator_2_t2954477804, ____enumerator_5)); }
	inline RuntimeObject* get__enumerator_5() const { return ____enumerator_5; }
	inline RuntimeObject** get_address_of__enumerator_5() { return &____enumerator_5; }
	inline void set__enumerator_5(RuntimeObject* value)
	{
		____enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((&____enumerator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIPARTITIONITERATOR_2_T2954477804_H
#ifndef WHEREENUMERABLEITERATOR_1_T1635751646_H
#define WHEREENUMERABLEITERATOR_1_T1635751646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/WhereEnumerableIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  WhereEnumerableIterator_1_t1635751646  : public Iterator_1_t1484577656
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::_source
	RuntimeObject* ____source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::_predicate
	Func_2_t1033609360 * ____predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::_enumerator
	RuntimeObject* ____enumerator_5;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1635751646, ____source_3)); }
	inline RuntimeObject* get__source_3() const { return ____source_3; }
	inline RuntimeObject** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(RuntimeObject* value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1635751646, ____predicate_4)); }
	inline Func_2_t1033609360 * get__predicate_4() const { return ____predicate_4; }
	inline Func_2_t1033609360 ** get_address_of__predicate_4() { return &____predicate_4; }
	inline void set__predicate_4(Func_2_t1033609360 * value)
	{
		____predicate_4 = value;
		Il2CppCodeGenWriteBarrier((&____predicate_4), value);
	}

	inline static int32_t get_offset_of__enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1635751646, ____enumerator_5)); }
	inline RuntimeObject* get__enumerator_5() const { return ____enumerator_5; }
	inline RuntimeObject** get_address_of__enumerator_5() { return &____enumerator_5; }
	inline void set__enumerator_5(RuntimeObject* value)
	{
		____enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((&____enumerator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHEREENUMERABLEITERATOR_1_T1635751646_H
#ifndef SELECTLISTITERATOR_2_T2565991656_H
#define SELECTLISTITERATOR_2_T2565991656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/SelectListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  SelectListIterator_2_t2565991656  : public Iterator_1_t1484577656
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/SelectListIterator`2::_source
	List_1_t301083084 * ____source_3;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/SelectListIterator`2::_selector
	Func_2_t3270419407 * ____selector_4;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/SelectListIterator`2::_enumerator
	Enumerator_t2190326961  ____enumerator_5;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(SelectListIterator_2_t2565991656, ____source_3)); }
	inline List_1_t301083084 * get__source_3() const { return ____source_3; }
	inline List_1_t301083084 ** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(List_1_t301083084 * value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__selector_4() { return static_cast<int32_t>(offsetof(SelectListIterator_2_t2565991656, ____selector_4)); }
	inline Func_2_t3270419407 * get__selector_4() const { return ____selector_4; }
	inline Func_2_t3270419407 ** get_address_of__selector_4() { return &____selector_4; }
	inline void set__selector_4(Func_2_t3270419407 * value)
	{
		____selector_4 = value;
		Il2CppCodeGenWriteBarrier((&____selector_4), value);
	}

	inline static int32_t get_offset_of__enumerator_5() { return static_cast<int32_t>(offsetof(SelectListIterator_2_t2565991656, ____enumerator_5)); }
	inline Enumerator_t2190326961  get__enumerator_5() const { return ____enumerator_5; }
	inline Enumerator_t2190326961 * get_address_of__enumerator_5() { return &____enumerator_5; }
	inline void set__enumerator_5(Enumerator_t2190326961  value)
	{
		____enumerator_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTLISTITERATOR_2_T2565991656_H
#ifndef SELECTENUMERABLEITERATOR_2_T956622511_H
#define SELECTENUMERABLEITERATOR_2_T956622511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  SelectEnumerableIterator_2_t956622511  : public Iterator_1_t1484577656
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/SelectEnumerableIterator`2::_source
	RuntimeObject* ____source_3;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/SelectEnumerableIterator`2::_selector
	Func_2_t3466538714 * ____selector_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/SelectEnumerableIterator`2::_enumerator
	RuntimeObject* ____enumerator_5;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(SelectEnumerableIterator_2_t956622511, ____source_3)); }
	inline RuntimeObject* get__source_3() const { return ____source_3; }
	inline RuntimeObject** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(RuntimeObject* value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__selector_4() { return static_cast<int32_t>(offsetof(SelectEnumerableIterator_2_t956622511, ____selector_4)); }
	inline Func_2_t3466538714 * get__selector_4() const { return ____selector_4; }
	inline Func_2_t3466538714 ** get_address_of__selector_4() { return &____selector_4; }
	inline void set__selector_4(Func_2_t3466538714 * value)
	{
		____selector_4 = value;
		Il2CppCodeGenWriteBarrier((&____selector_4), value);
	}

	inline static int32_t get_offset_of__enumerator_5() { return static_cast<int32_t>(offsetof(SelectEnumerableIterator_2_t956622511, ____enumerator_5)); }
	inline RuntimeObject* get__enumerator_5() const { return ____enumerator_5; }
	inline RuntimeObject** get_address_of__enumerator_5() { return &____enumerator_5; }
	inline void set__enumerator_5(RuntimeObject* value)
	{
		____enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((&____enumerator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTENUMERABLEITERATOR_2_T956622511_H
#ifndef WHEREENUMERABLEITERATOR_1_T2229509965_H
#define WHEREENUMERABLEITERATOR_1_T2229509965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/WhereEnumerableIterator`1<System.Collections.DictionaryEntry>
struct  WhereEnumerableIterator_1_t2229509965  : public Iterator_1_t2078335975
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::_source
	RuntimeObject* ____source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::_predicate
	Func_2_t837490053 * ____predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::_enumerator
	RuntimeObject* ____enumerator_5;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t2229509965, ____source_3)); }
	inline RuntimeObject* get__source_3() const { return ____source_3; }
	inline RuntimeObject** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(RuntimeObject* value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t2229509965, ____predicate_4)); }
	inline Func_2_t837490053 * get__predicate_4() const { return ____predicate_4; }
	inline Func_2_t837490053 ** get_address_of__predicate_4() { return &____predicate_4; }
	inline void set__predicate_4(Func_2_t837490053 * value)
	{
		____predicate_4 = value;
		Il2CppCodeGenWriteBarrier((&____predicate_4), value);
	}

	inline static int32_t get_offset_of__enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t2229509965, ____enumerator_5)); }
	inline RuntimeObject* get__enumerator_5() const { return ____enumerator_5; }
	inline RuntimeObject** get_address_of__enumerator_5() { return &____enumerator_5; }
	inline void set__enumerator_5(RuntimeObject* value)
	{
		____enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((&____enumerator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHEREENUMERABLEITERATOR_1_T2229509965_H
#ifndef FUNC_2_T148644517_H
#define FUNC_2_T148644517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Char,System.Boolean>
struct  Func_2_t148644517  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T148644517_H
#ifndef WHEREENUMERABLEITERATOR_1_T2891385820_H
#define WHEREENUMERABLEITERATOR_1_T2891385820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.Ray>
struct  WhereEnumerableIterator_1_t2891385820  : public Iterator_1_t2740211830
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::_source
	RuntimeObject* ____source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::_predicate
	Func_2_t2751558106 * ____predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::_enumerator
	RuntimeObject* ____enumerator_5;

public:
	inline static int32_t get_offset_of__source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t2891385820, ____source_3)); }
	inline RuntimeObject* get__source_3() const { return ____source_3; }
	inline RuntimeObject** get_address_of__source_3() { return &____source_3; }
	inline void set__source_3(RuntimeObject* value)
	{
		____source_3 = value;
		Il2CppCodeGenWriteBarrier((&____source_3), value);
	}

	inline static int32_t get_offset_of__predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t2891385820, ____predicate_4)); }
	inline Func_2_t2751558106 * get__predicate_4() const { return ____predicate_4; }
	inline Func_2_t2751558106 ** get_address_of__predicate_4() { return &____predicate_4; }
	inline void set__predicate_4(Func_2_t2751558106 * value)
	{
		____predicate_4 = value;
		Il2CppCodeGenWriteBarrier((&____predicate_4), value);
	}

	inline static int32_t get_offset_of__enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t2891385820, ____enumerator_5)); }
	inline RuntimeObject* get__enumerator_5() const { return ____enumerator_5; }
	inline RuntimeObject** get_address_of__enumerator_5() { return &____enumerator_5; }
	inline void set__enumerator_5(RuntimeObject* value)
	{
		____enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((&____enumerator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHEREENUMERABLEITERATOR_1_T2891385820_H
#ifndef SAFEFINDHANDLE_T2068834300_H
#define SAFEFINDHANDLE_T2068834300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.Win32.SafeHandles.SafeFindHandle
struct  SafeFindHandle_t2068834300  : public SafeHandleZeroOrMinusOneIsInvalid_t1182193648
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEFINDHANDLE_T2068834300_H
#ifndef FUNC_2_T3466538714_H
#define FUNC_2_T3466538714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  Func_2_t3466538714  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T3466538714_H
#ifndef FUNC_2_T3270419407_H
#define FUNC_2_T3270419407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  Func_2_t3270419407  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T3270419407_H
#ifndef FUNC_2_T2751558106_H
#define FUNC_2_T2751558106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<UnityEngine.Ray,System.Boolean>
struct  Func_2_t2751558106  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T2751558106_H
#ifndef FUNC_2_T1033609360_H
#define FUNC_2_T1033609360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>
struct  Func_2_t1033609360  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T1033609360_H
#ifndef FUNC_2_T3759279471_H
#define FUNC_2_T3759279471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.Boolean>
struct  Func_2_t3759279471  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T3759279471_H
#ifndef FUNC_2_T837490053_H
#define FUNC_2_T837490053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Collections.DictionaryEntry,System.Boolean>
struct  Func_2_t837490053  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T837490053_H
#ifndef FUNC_2_T1426983263_H
#define FUNC_2_T1426983263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.Collections.Generic.IEnumerable`1<System.Object>>
struct  Func_2_t1426983263  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T1426983263_H
#ifndef FUNC_2_T2447130374_H
#define FUNC_2_T2447130374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.Object>
struct  Func_2_t2447130374  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T2447130374_H
#ifndef FUNC_2_T2317969963_H
#define FUNC_2_T2317969963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.Int32>
struct  Func_2_t2317969963  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T2317969963_H
#ifndef FUNC_2_T3934069716_H
#define FUNC_2_T3934069716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<UnityEngine.Vector3,System.Single>
struct  Func_2_t3934069716  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T3934069716_H
#ifndef FUNC_2_T3672504941_H
#define FUNC_2_T3672504941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<UnityEngine.Vector2,System.Single>
struct  Func_2_t3672504941  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T3672504941_H
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Char[]
struct CharU5BU5D_t3528271667  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_t2156229523  m_Items[1];

public:
	inline Vector2_t2156229523  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_t2156229523 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_t2156229523  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_t2156229523  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_t2156229523 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_t2156229523  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t3722313464  m_Items[1];

public:
	inline Vector3_t3722313464  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t3722313464  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t118269214  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2530217319  m_Items[1];

public:
	inline KeyValuePair_2_t2530217319  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t2530217319 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2530217319  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t2530217319  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t2530217319 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t2530217319  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Ray[]
struct RayU5BU5D_t1836217960  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Ray_t3785851493  m_Items[1];

public:
	inline Ray_t3785851493  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Ray_t3785851493 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Ray_t3785851493  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Ray_t3785851493  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Ray_t3785851493 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Ray_t3785851493  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t4217117203  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) DictionaryEntry_t3123975638  m_Items[1];

public:
	inline DictionaryEntry_t3123975638  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DictionaryEntry_t3123975638 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DictionaryEntry_t3123975638  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline DictionaryEntry_t3123975638  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DictionaryEntry_t3123975638 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DictionaryEntry_t3123975638  value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m1328026504_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2730968292_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3705215113_gshared (List_1_t257213610 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Linq.Buffer`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  void Buffer_1__ctor_m3439248099_gshared (Buffer_1_t932544294 * __this, RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Void System.Linq.Buffer`1<UnityEngine.Vector2>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  void Buffer_1__ctor_m1836940346_gshared (Buffer_1_t8667653 * __this, RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Void System.Linq.Buffer`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  void Buffer_1__ctor_m2075330287_gshared (Buffer_1_t1574751594 * __this, RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LargeArrayBuilder`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Boolean)
extern "C"  void LargeArrayBuilder_1__ctor_m2226121364_gshared (LargeArrayBuilder_1_t2440570340 * __this, bool p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LargeArrayBuilder`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(T)
extern "C"  void LargeArrayBuilder_1_Add_m982335036_gshared (LargeArrayBuilder_1_t2440570340 * __this, KeyValuePair_2_t2530217319  p0, const RuntimeMethod* method);
// T[] System.Collections.Generic.LargeArrayBuilder`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t118269214* LargeArrayBuilder_1_ToArray_m3030376891_gshared (LargeArrayBuilder_1_t2440570340 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LargeArrayBuilder`1<System.Object>::.ctor(System.Boolean)
extern "C"  void LargeArrayBuilder_1__ctor_m104969882_gshared (LargeArrayBuilder_1_t2990459185 * __this, bool p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LargeArrayBuilder`1<System.Object>::Add(T)
extern "C"  void LargeArrayBuilder_1_Add_m3802412589_gshared (LargeArrayBuilder_1_t2990459185 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// T[] System.Collections.Generic.LargeArrayBuilder`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t2843939325* LargeArrayBuilder_1_ToArray_m390648332_gshared (LargeArrayBuilder_1_t2990459185 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.DictionaryEntry>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2875527037_gshared (Enumerator_t2190326961 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Collections.DictionaryEntry>::get_Current()
extern "C"  DictionaryEntry_t3123975638  Enumerator_get_Current_m3966199253_gshared (Enumerator_t2190326961 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m337713592_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.SparseArrayBuilder`1<System.Object>::.ctor(System.Boolean)
extern "C"  void SparseArrayBuilder_1__ctor_m3599075535_gshared (SparseArrayBuilder_1_t3713953326 * __this, bool p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.SparseArrayBuilder`1<System.Object>::Reserve(System.Int32)
extern "C"  void SparseArrayBuilder_1_Reserve_m3186431978_gshared (SparseArrayBuilder_1_t3713953326 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.ArrayBuilder`1<System.Object>::Add(T)
extern "C"  void ArrayBuilder_1_Add_m1469477416_gshared (ArrayBuilder_1_t68235548 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.SparseArrayBuilder`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void SparseArrayBuilder_1_AddRange_m3173835490_gshared (SparseArrayBuilder_1_t3713953326 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// T[] System.Collections.Generic.SparseArrayBuilder`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t2843939325* SparseArrayBuilder_1_ToArray_m1576911221_gshared (SparseArrayBuilder_1_t3713953326 * __this, const RuntimeMethod* method);
// System.Collections.Generic.ArrayBuilder`1<System.Collections.Generic.Marker> System.Collections.Generic.SparseArrayBuilder`1<System.Object>::get_Markers()
extern "C"  ArrayBuilder_1_t4177874457  SparseArrayBuilder_1_get_Markers_m716823650_gshared (SparseArrayBuilder_1_t3713953326 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.ArrayBuilder`1<System.Collections.Generic.Marker>::get_Item(System.Int32)
extern "C"  Marker_t2894777777  ArrayBuilder_1_get_Item_m3563499930_gshared (ArrayBuilder_1_t4177874457 * __this, int32_t p0, const RuntimeMethod* method);
// T System.Collections.Generic.ArrayBuilder`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * ArrayBuilder_1_get_Item_m2217381838_gshared (ArrayBuilder_1_t68235548 * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.ArrayBuilder`1<System.Collections.Generic.Marker>::get_Count()
extern "C"  int32_t ArrayBuilder_1_get_Count_m2587795986_gshared (ArrayBuilder_1_t4177874457 * __this, const RuntimeMethod* method);

// System.Void System.Collections.Generic.List`1<System.IO.Directory/SearchData>::.ctor()
#define List_1__ctor_m3635573260(__this, method) ((  void (*) (List_1_t4120301035 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m3847582255 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Path::GetFullPathInternal(System.String)
extern "C"  String_t* Path_GetFullPathInternal_m1405359470 (RuntimeObject * __this /* static, unused */, String_t* ___path0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Path::GetDirectoryName(System.String)
extern "C"  String_t* Path_GetDirectoryName_m3496866581 (RuntimeObject * __this /* static, unused */, String_t* ___path0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Directory::GetDemandDir(System.String,System.Boolean)
extern "C"  String_t* Directory_GetDemandDir_m3334461828 (RuntimeObject * __this /* static, unused */, String_t* ___fullPath0, bool ___thisDirOnly1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Path::Combine(System.String,System.String)
extern "C"  String_t* Path_Combine_m3389272516 (RuntimeObject * __this /* static, unused */, String_t* ___path10, String_t* ___path21, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory/SearchData::.ctor(System.String,System.String,System.IO.SearchOption)
extern "C"  void SearchData__ctor_m4162952925 (SearchData_t2648226293 * __this, String_t* ___fullPath0, String_t* ___userPath1, int32_t ___searchOption2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Path::InternalCombine(System.String,System.String)
extern "C"  String_t* Path_InternalCombine_m3863307630 (RuntimeObject * __this /* static, unused */, String_t* ___path10, String_t* ___path21, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.Win32Native/WIN32_FIND_DATA::.ctor()
extern "C"  void WIN32_FIND_DATA__ctor_m1509629805 (WIN32_FIND_DATA_t4232796738 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.IO.MonoIO::FindFirstFile(System.String,System.String&,System.Int32&,System.Int32&)
extern "C"  intptr_t MonoIO_FindFirstFile_m1358799729 (RuntimeObject * __this /* static, unused */, String_t* ___path_with_pattern0, String_t** ___fileName1, int32_t* ___fileAttr2, int32_t* ___error3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.SafeHandles.SafeFindHandle::.ctor(System.IntPtr)
extern "C"  void SafeFindHandle__ctor_m3833665175 (SafeFindHandle_t2068834300 * __this, intptr_t ___preexistingHandle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.SafeHandle::Dispose()
extern "C"  void SafeHandle_Dispose_m817995135 (SafeHandle_t3273388951 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.IO.Directory/SearchData>::Add(T)
#define List_1_Add_m1559120923(__this, p0, method) ((  void (*) (List_1_t4120301035 *, SearchData_t2648226293 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// T System.Collections.Generic.List`1<System.IO.Directory/SearchData>::get_Item(System.Int32)
#define List_1_get_Item_m3638502404(__this, p0, method) ((  SearchData_t2648226293 * (*) (List_1_t4120301035 *, int32_t, const RuntimeMethod*))List_1_get_Item_m1328026504_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.IO.Directory/SearchData>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m762780374(__this, p0, method) ((  void (*) (List_1_t4120301035 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m2730968292_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<System.IO.Directory/SearchData>::get_Count()
#define List_1_get_Count_m958122929(__this, method) ((  int32_t (*) (List_1_t4120301035 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.IntPtr System.Runtime.InteropServices.SafeHandle::DangerousGetHandle()
extern "C"  intptr_t SafeHandle_DangerousGetHandle_m3697436134 (SafeHandle_t3273388951 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.MonoIO::FindNextFile(System.IntPtr,System.String&,System.Int32&,System.Int32&)
extern "C"  bool MonoIO_FindNextFile_m3163114833 (RuntimeObject * __this /* static, unused */, intptr_t ___hnd0, String_t** ___fileName1, int32_t* ___fileAttr2, int32_t* ___error3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SearchResult::.ctor(System.String,System.String,Microsoft.Win32.Win32Native/WIN32_FIND_DATA)
extern "C"  void SearchResult__ctor_m203165125 (SearchResult_t2600365382 * __this, String_t* ___fullPath0, String_t* ___userPath1, WIN32_FIND_DATA_t4232796738 * ___findData2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.__Error::WinIOError(System.Int32,System.String)
extern "C"  void __Error_WinIOError_m2555862149 (RuntimeObject * __this /* static, unused */, int32_t ___errorCode0, String_t* ___maybeFullPath1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.FileSystemEnumerableHelpers::IsDir(Microsoft.Win32.Win32Native/WIN32_FIND_DATA)
extern "C"  bool FileSystemEnumerableHelpers_IsDir_m2524844520 (RuntimeObject * __this /* static, unused */, WIN32_FIND_DATA_t4232796738 * ___data0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.IO.Directory/SearchData>::Insert(System.Int32,T)
#define List_1_Insert_m2523363920(__this, p0, p1, method) ((  void (*) (List_1_t4120301035 *, int32_t, SearchData_t2648226293 *, const RuntimeMethod*))List_1_Insert_m3705215113_gshared)(__this, p0, p1, method)
// System.Char[] System.IO.Path::get_TrimEndChars()
extern "C"  CharU5BU5D_t3528271667* Path_get_TrimEndChars_m4264594297 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::TrimEnd(System.Char[])
extern "C"  String_t* String_TrimEnd_m3824727301 (String_t* __this, CharU5BU5D_t3528271667* ___trimChars0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::Equals(System.String)
extern "C"  bool String_Equals_m2270643605 (String_t* __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Path::CheckSearchPattern(System.String)
extern "C"  void Path_CheckSearchPattern_m974385397 (RuntimeObject * __this /* static, unused */, String_t* ___searchPattern0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char System.String::get_Chars(System.Int32)
extern "C"  Il2CppChar String_get_Chars_m2986988803 (String_t* __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.Path::IsDirectorySeparator(System.Char)
extern "C"  bool Path_IsDirectorySeparator_m1029452715 (RuntimeObject * __this /* static, unused */, Il2CppChar ___c0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32)
extern "C"  String_t* String_Substring_m2848979100 (String_t* __this, int32_t ___startIndex0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* ___str00, String_t* ___str11, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Threading.Thread System.Threading.Thread::get_CurrentThread()
extern "C"  Thread_t2300836069 * Thread_get_CurrentThread_m4142136012 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Thread::get_ManagedThreadId()
extern "C"  int32_t Thread_get_ManagedThreadId_m1068113671 (Thread_t2300836069 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::SuppressFinalize(System.Object)
extern "C"  void GC_SuppressFinalize_m1177400158 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Buffer`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
#define Buffer_1__ctor_m3439248099(__this, ___source0, method) ((  void (*) (Buffer_1_t932544294 *, RuntimeObject*, const RuntimeMethod*))Buffer_1__ctor_m3439248099_gshared)(__this, ___source0, method)
// System.Void System.Linq.Buffer`1<UnityEngine.Vector2>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
#define Buffer_1__ctor_m1836940346(__this, ___source0, method) ((  void (*) (Buffer_1_t8667653 *, RuntimeObject*, const RuntimeMethod*))Buffer_1__ctor_m1836940346_gshared)(__this, ___source0, method)
// System.Void System.Linq.Buffer`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
#define Buffer_1__ctor_m2075330287(__this, ___source0, method) ((  void (*) (Buffer_1_t1574751594 *, RuntimeObject*, const RuntimeMethod*))Buffer_1__ctor_m2075330287_gshared)(__this, ___source0, method)
// System.Exception System.Linq.Error::NotSupported()
extern "C"  Exception_t1436737249 * Error_NotSupported_m1072967690 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Environment::get_CurrentManagedThreadId()
extern "C"  int32_t Environment_get_CurrentManagedThreadId_m3454612449 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LargeArrayBuilder`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Boolean)
#define LargeArrayBuilder_1__ctor_m2226121364(__this, p0, method) ((  void (*) (LargeArrayBuilder_1_t2440570340 *, bool, const RuntimeMethod*))LargeArrayBuilder_1__ctor_m2226121364_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.LargeArrayBuilder`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(T)
#define LargeArrayBuilder_1_Add_m982335036(__this, p0, method) ((  void (*) (LargeArrayBuilder_1_t2440570340 *, KeyValuePair_2_t2530217319 , const RuntimeMethod*))LargeArrayBuilder_1_Add_m982335036_gshared)(__this, p0, method)
// T[] System.Collections.Generic.LargeArrayBuilder`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToArray()
#define LargeArrayBuilder_1_ToArray_m3030376891(__this, method) ((  KeyValuePair_2U5BU5D_t118269214* (*) (LargeArrayBuilder_1_t2440570340 *, const RuntimeMethod*))LargeArrayBuilder_1_ToArray_m3030376891_gshared)(__this, method)
// System.Void System.Collections.Generic.LargeArrayBuilder`1<System.Object>::.ctor(System.Boolean)
#define LargeArrayBuilder_1__ctor_m104969882(__this, p0, method) ((  void (*) (LargeArrayBuilder_1_t2990459185 *, bool, const RuntimeMethod*))LargeArrayBuilder_1__ctor_m104969882_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.LargeArrayBuilder`1<System.Object>::Add(T)
#define LargeArrayBuilder_1_Add_m3802412589(__this, p0, method) ((  void (*) (LargeArrayBuilder_1_t2990459185 *, RuntimeObject *, const RuntimeMethod*))LargeArrayBuilder_1_Add_m3802412589_gshared)(__this, p0, method)
// T[] System.Collections.Generic.LargeArrayBuilder`1<System.Object>::ToArray()
#define LargeArrayBuilder_1_ToArray_m390648332(__this, method) ((  ObjectU5BU5D_t2843939325* (*) (LargeArrayBuilder_1_t2990459185 *, const RuntimeMethod*))LargeArrayBuilder_1_ToArray_m390648332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.DictionaryEntry>::MoveNext()
#define Enumerator_MoveNext_m2875527037(__this, method) ((  bool (*) (Enumerator_t2190326961 *, const RuntimeMethod*))Enumerator_MoveNext_m2875527037_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<System.Collections.DictionaryEntry>::get_Current()
#define Enumerator_get_Current_m3966199253(__this, method) ((  DictionaryEntry_t3123975638  (*) (Enumerator_t2190326961 *, const RuntimeMethod*))Enumerator_get_Current_m3966199253_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
#define Enumerator_MoveNext_m2142368520(__this, method) ((  bool (*) (Enumerator_t2146457487 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
#define Enumerator_get_Current_m337713592(__this, method) ((  RuntimeObject * (*) (Enumerator_t2146457487 *, const RuntimeMethod*))Enumerator_get_Current_m337713592_gshared)(__this, method)
// System.Void System.Collections.Generic.SparseArrayBuilder`1<System.Object>::.ctor(System.Boolean)
#define SparseArrayBuilder_1__ctor_m3599075535(__this, p0, method) ((  void (*) (SparseArrayBuilder_1_t3713953326 *, bool, const RuntimeMethod*))SparseArrayBuilder_1__ctor_m3599075535_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.SparseArrayBuilder`1<System.Object>::Reserve(System.Int32)
#define SparseArrayBuilder_1_Reserve_m3186431978(__this, p0, method) ((  void (*) (SparseArrayBuilder_1_t3713953326 *, int32_t, const RuntimeMethod*))SparseArrayBuilder_1_Reserve_m3186431978_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.ArrayBuilder`1<System.Collections.Generic.IEnumerable`1<System.Object>>::Add(T)
#define ArrayBuilder_1_Add_m3352372406(__this, p0, method) ((  void (*) (ArrayBuilder_1_t3343055733 *, RuntimeObject*, const RuntimeMethod*))ArrayBuilder_1_Add_m1469477416_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.SparseArrayBuilder`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define SparseArrayBuilder_1_AddRange_m3173835490(__this, p0, method) ((  void (*) (SparseArrayBuilder_1_t3713953326 *, RuntimeObject*, const RuntimeMethod*))SparseArrayBuilder_1_AddRange_m3173835490_gshared)(__this, p0, method)
// T[] System.Collections.Generic.SparseArrayBuilder`1<System.Object>::ToArray()
#define SparseArrayBuilder_1_ToArray_m1576911221(__this, method) ((  ObjectU5BU5D_t2843939325* (*) (SparseArrayBuilder_1_t3713953326 *, const RuntimeMethod*))SparseArrayBuilder_1_ToArray_m1576911221_gshared)(__this, method)
// System.Collections.Generic.ArrayBuilder`1<System.Collections.Generic.Marker> System.Collections.Generic.SparseArrayBuilder`1<System.Object>::get_Markers()
#define SparseArrayBuilder_1_get_Markers_m716823650(__this, method) ((  ArrayBuilder_1_t4177874457  (*) (SparseArrayBuilder_1_t3713953326 *, const RuntimeMethod*))SparseArrayBuilder_1_get_Markers_m716823650_gshared)(__this, method)
// T System.Collections.Generic.ArrayBuilder`1<System.Collections.Generic.Marker>::get_Item(System.Int32)
#define ArrayBuilder_1_get_Item_m3563499930(__this, p0, method) ((  Marker_t2894777777  (*) (ArrayBuilder_1_t4177874457 *, int32_t, const RuntimeMethod*))ArrayBuilder_1_get_Item_m3563499930_gshared)(__this, p0, method)
// T System.Collections.Generic.ArrayBuilder`1<System.Collections.Generic.IEnumerable`1<System.Object>>::get_Item(System.Int32)
#define ArrayBuilder_1_get_Item_m3142556344(__this, p0, method) ((  RuntimeObject* (*) (ArrayBuilder_1_t3343055733 *, int32_t, const RuntimeMethod*))ArrayBuilder_1_get_Item_m2217381838_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.Marker::get_Index()
extern "C"  int32_t Marker_get_Index_m3525311458 (Marker_t2894777777 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Marker::get_Count()
extern "C"  int32_t Marker_get_Count_m126194417 (Marker_t2894777777 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.ArrayBuilder`1<System.Collections.Generic.Marker>::get_Count()
#define ArrayBuilder_1_get_Count_m2587795986(__this, method) ((  int32_t (*) (ArrayBuilder_1_t4177874457 *, const RuntimeMethod*))ArrayBuilder_1_get_Count_m2587795986_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.IO.FileSystemEnumerableIterator`1<System.Object>::.ctor(System.String,System.String,System.String,System.IO.SearchOption,System.IO.SearchResultHandler`1<TSource>,System.Boolean)
extern "C"  void FileSystemEnumerableIterator_1__ctor_m3232840268_gshared (FileSystemEnumerableIterator_1_t25181536 * __this, String_t* ___path0, String_t* ___originalUserPath1, String_t* ___searchPattern2, int32_t ___searchOption3, SearchResultHandler_1_t2377980137 * ___resultHandler4, bool ___checkHost5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileSystemEnumerableIterator_1__ctor_m3232840268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	{
		NullCheck((Iterator_1_t3764629478 *)__this);
		((  void (*) (Iterator_1_t3764629478 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t3764629478 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		List_1_t4120301035 * L_0 = (List_1_t4120301035 *)il2cpp_codegen_object_new(List_1_t4120301035_il2cpp_TypeInfo_var);
		List_1__ctor_m3635573260(L_0, /*hidden argument*/List_1__ctor_m3635573260_RuntimeMethod_var);
		__this->set_searchStack_4(L_0);
		String_t* L_1 = ___searchPattern2;
		String_t* L_2 = ((  String_t* (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (String_t*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (String_t*)L_2;
		String_t* L_3 = V_0;
		NullCheck((String_t*)L_3);
		int32_t L_4 = String_get_Length_m3847582255((String_t*)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0028;
		}
	}
	{
		__this->set_empty_9((bool)1);
		return;
	}

IL_0028:
	{
		SearchResultHandler_1_t2377980137 * L_5 = ___resultHandler4;
		__this->set__resultHandler_3(L_5);
		int32_t L_6 = ___searchOption3;
		__this->set_searchOption_11(L_6);
		String_t* L_7 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_8 = Path_GetFullPathInternal_m1405359470(NULL /*static, unused*/, (String_t*)L_7, /*hidden argument*/NULL);
		__this->set_fullPath_12(L_8);
		String_t* L_9 = (String_t*)__this->get_fullPath_12();
		String_t* L_10 = V_0;
		String_t* L_11 = ((  String_t* (*) (RuntimeObject * /* static, unused */, String_t*, String_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (String_t*)L_9, (String_t*)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_1 = (String_t*)L_11;
		String_t* L_12 = V_1;
		String_t* L_13 = Path_GetDirectoryName_m3496866581(NULL /*static, unused*/, (String_t*)L_12, /*hidden argument*/NULL);
		__this->set_normalizedSearchPath_13(L_13);
		StringU5BU5D_t1281789340* L_14 = (StringU5BU5D_t1281789340*)((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)2));
		String_t* L_15 = (String_t*)__this->get_fullPath_12();
		String_t* L_16 = Directory_GetDemandDir_m3334461828(NULL /*static, unused*/, (String_t*)L_15, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_16);
		String_t* L_17 = (String_t*)__this->get_normalizedSearchPath_13();
		String_t* L_18 = Directory_GetDemandDir_m3334461828(NULL /*static, unused*/, (String_t*)L_17, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_18);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_18);
		bool L_19 = ___checkHost5;
		__this->set__checkHost_14(L_19);
		String_t* L_20 = V_1;
		String_t* L_21 = (String_t*)__this->get_normalizedSearchPath_13();
		String_t* L_22 = ((  String_t* (*) (RuntimeObject * /* static, unused */, String_t*, String_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (String_t*)L_20, (String_t*)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		__this->set_searchCriteria_6(L_22);
		String_t* L_23 = V_0;
		String_t* L_24 = Path_GetDirectoryName_m3496866581(NULL /*static, unused*/, (String_t*)L_23, /*hidden argument*/NULL);
		V_2 = (String_t*)L_24;
		String_t* L_25 = ___originalUserPath1;
		V_3 = (String_t*)L_25;
		String_t* L_26 = V_2;
		if (!L_26)
		{
			goto IL_00b6;
		}
	}
	{
		String_t* L_27 = V_2;
		NullCheck((String_t*)L_27);
		int32_t L_28 = String_get_Length_m3847582255((String_t*)L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00b6;
		}
	}
	{
		String_t* L_29 = V_3;
		String_t* L_30 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_31 = Path_Combine_m3389272516(NULL /*static, unused*/, (String_t*)L_29, (String_t*)L_30, /*hidden argument*/NULL);
		V_3 = (String_t*)L_31;
	}

IL_00b6:
	{
		String_t* L_32 = V_3;
		__this->set_userPath_10(L_32);
		String_t* L_33 = (String_t*)__this->get_normalizedSearchPath_13();
		String_t* L_34 = (String_t*)__this->get_userPath_10();
		int32_t L_35 = ___searchOption3;
		SearchData_t2648226293 * L_36 = (SearchData_t2648226293 *)il2cpp_codegen_object_new(SearchData_t2648226293_il2cpp_TypeInfo_var);
		SearchData__ctor_m4162952925(L_36, (String_t*)L_33, (String_t*)L_34, (int32_t)L_35, /*hidden argument*/NULL);
		__this->set_searchData_5(L_36);
		NullCheck((FileSystemEnumerableIterator_1_t25181536 *)__this);
		((  void (*) (FileSystemEnumerableIterator_1_t25181536 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((FileSystemEnumerableIterator_1_t25181536 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return;
	}
}
// System.Void System.IO.FileSystemEnumerableIterator`1<System.Object>::CommonInit()
extern "C"  void FileSystemEnumerableIterator_1_CommonInit_m3137623192_gshared (FileSystemEnumerableIterator_1_t25181536 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileSystemEnumerableIterator_1_CommonInit_m3137623192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	WIN32_FIND_DATA_t4232796738 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	SearchResult_t2600365382 * V_4 = NULL;
	{
		SearchData_t2648226293 * L_0 = (SearchData_t2648226293 *)__this->get_searchData_5();
		NullCheck(L_0);
		String_t* L_1 = (String_t*)L_0->get_fullPath_0();
		String_t* L_2 = (String_t*)__this->get_searchCriteria_6();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_3 = Path_InternalCombine_m3863307630(NULL /*static, unused*/, (String_t*)L_1, (String_t*)L_2, /*hidden argument*/NULL);
		V_0 = (String_t*)L_3;
		WIN32_FIND_DATA_t4232796738 * L_4 = (WIN32_FIND_DATA_t4232796738 *)il2cpp_codegen_object_new(WIN32_FIND_DATA_t4232796738_il2cpp_TypeInfo_var);
		WIN32_FIND_DATA__ctor_m1509629805(L_4, /*hidden argument*/NULL);
		V_1 = (WIN32_FIND_DATA_t4232796738 *)L_4;
		String_t* L_5 = V_0;
		WIN32_FIND_DATA_t4232796738 * L_6 = V_1;
		NullCheck(L_6);
		String_t** L_7 = (String_t**)L_6->get_address_of_cFileName_11();
		WIN32_FIND_DATA_t4232796738 * L_8 = V_1;
		NullCheck(L_8);
		int32_t* L_9 = (int32_t*)L_8->get_address_of_dwFileAttributes_0();
		IL2CPP_RUNTIME_CLASS_INIT(MonoIO_t2601436415_il2cpp_TypeInfo_var);
		intptr_t L_10 = MonoIO_FindFirstFile_m1358799729(NULL /*static, unused*/, (String_t*)L_5, (String_t**)L_7, (int32_t*)L_9, (int32_t*)(&V_2), /*hidden argument*/NULL);
		SafeFindHandle_t2068834300 * L_11 = (SafeFindHandle_t2068834300 *)il2cpp_codegen_object_new(SafeFindHandle_t2068834300_il2cpp_TypeInfo_var);
		SafeFindHandle__ctor_m3833665175(L_11, (intptr_t)L_10, /*hidden argument*/NULL);
		__this->set__hnd_7(L_11);
		SafeFindHandle_t2068834300 * L_12 = (SafeFindHandle_t2068834300 *)__this->get__hnd_7();
		NullCheck((SafeHandle_t3273388951 *)L_12);
		bool L_13 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid() */, (SafeHandle_t3273388951 *)L_12);
		if (!L_13)
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_14 = V_2;
		V_3 = (int32_t)L_14;
		int32_t L_15 = V_3;
		if ((((int32_t)L_15) == ((int32_t)2)))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_16 = V_3;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)18))))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_17 = V_3;
		SearchData_t2648226293 * L_18 = (SearchData_t2648226293 *)__this->get_searchData_5();
		NullCheck(L_18);
		String_t* L_19 = (String_t*)L_18->get_fullPath_0();
		NullCheck((FileSystemEnumerableIterator_1_t25181536 *)__this);
		((  void (*) (FileSystemEnumerableIterator_1_t25181536 *, int32_t, String_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((FileSystemEnumerableIterator_1_t25181536 *)__this, (int32_t)L_17, (String_t*)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		goto IL_007c;
	}

IL_0068:
	{
		SearchData_t2648226293 * L_20 = (SearchData_t2648226293 *)__this->get_searchData_5();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_searchOption_2();
		__this->set_empty_9((bool)((((int32_t)L_21) == ((int32_t)0))? 1 : 0));
	}

IL_007c:
	{
		SearchData_t2648226293 * L_22 = (SearchData_t2648226293 *)__this->get_searchData_5();
		NullCheck(L_22);
		int32_t L_23 = (int32_t)L_22->get_searchOption_2();
		if (L_23)
		{
			goto IL_00cf;
		}
	}
	{
		bool L_24 = (bool)__this->get_empty_9();
		if (!L_24)
		{
			goto IL_009d;
		}
	}
	{
		SafeFindHandle_t2068834300 * L_25 = (SafeFindHandle_t2068834300 *)__this->get__hnd_7();
		NullCheck((SafeHandle_t3273388951 *)L_25);
		SafeHandle_Dispose_m817995135((SafeHandle_t3273388951 *)L_25, /*hidden argument*/NULL);
		return;
	}

IL_009d:
	{
		SearchData_t2648226293 * L_26 = (SearchData_t2648226293 *)__this->get_searchData_5();
		WIN32_FIND_DATA_t4232796738 * L_27 = V_1;
		NullCheck((FileSystemEnumerableIterator_1_t25181536 *)__this);
		SearchResult_t2600365382 * L_28 = ((  SearchResult_t2600365382 * (*) (FileSystemEnumerableIterator_1_t25181536 *, SearchData_t2648226293 *, WIN32_FIND_DATA_t4232796738 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((FileSystemEnumerableIterator_1_t25181536 *)__this, (SearchData_t2648226293 *)L_26, (WIN32_FIND_DATA_t4232796738 *)L_27, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_4 = (SearchResult_t2600365382 *)L_28;
		SearchResultHandler_1_t2377980137 * L_29 = (SearchResultHandler_1_t2377980137 *)__this->get__resultHandler_3();
		SearchResult_t2600365382 * L_30 = V_4;
		NullCheck((SearchResultHandler_1_t2377980137 *)L_29);
		bool L_31 = VirtFuncInvoker1< bool, SearchResult_t2600365382 * >::Invoke(4 /* System.Boolean System.IO.SearchResultHandler`1<System.Object>::IsResultIncluded(System.IO.SearchResult) */, (SearchResultHandler_1_t2377980137 *)L_29, (SearchResult_t2600365382 *)L_30);
		if (!L_31)
		{
			goto IL_00eb;
		}
	}
	{
		SearchResultHandler_1_t2377980137 * L_32 = (SearchResultHandler_1_t2377980137 *)__this->get__resultHandler_3();
		SearchResult_t2600365382 * L_33 = V_4;
		NullCheck((SearchResultHandler_1_t2377980137 *)L_32);
		RuntimeObject * L_34 = VirtFuncInvoker1< RuntimeObject *, SearchResult_t2600365382 * >::Invoke(5 /* TSource System.IO.SearchResultHandler`1<System.Object>::CreateObject(System.IO.SearchResult) */, (SearchResultHandler_1_t2377980137 *)L_32, (SearchResult_t2600365382 *)L_33);
		((Iterator_1_t3764629478 *)__this)->set_current_2(L_34);
		return;
	}

IL_00cf:
	{
		SafeFindHandle_t2068834300 * L_35 = (SafeFindHandle_t2068834300 *)__this->get__hnd_7();
		NullCheck((SafeHandle_t3273388951 *)L_35);
		SafeHandle_Dispose_m817995135((SafeHandle_t3273388951 *)L_35, /*hidden argument*/NULL);
		List_1_t4120301035 * L_36 = (List_1_t4120301035 *)__this->get_searchStack_4();
		SearchData_t2648226293 * L_37 = (SearchData_t2648226293 *)__this->get_searchData_5();
		NullCheck((List_1_t4120301035 *)L_36);
		List_1_Add_m1559120923((List_1_t4120301035 *)L_36, (SearchData_t2648226293 *)L_37, /*hidden argument*/List_1_Add_m1559120923_RuntimeMethod_var);
	}

IL_00eb:
	{
		return;
	}
}
// System.Void System.IO.FileSystemEnumerableIterator`1<System.Object>::.ctor(System.String,System.String,System.String,System.String,System.IO.SearchOption,System.IO.SearchResultHandler`1<TSource>,System.Boolean)
extern "C"  void FileSystemEnumerableIterator_1__ctor_m2628860621_gshared (FileSystemEnumerableIterator_1_t25181536 * __this, String_t* ___fullPath0, String_t* ___normalizedSearchPath1, String_t* ___searchCriteria2, String_t* ___userPath3, int32_t ___searchOption4, SearchResultHandler_1_t2377980137 * ___resultHandler5, bool ___checkHost6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileSystemEnumerableIterator_1__ctor_m2628860621_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Iterator_1_t3764629478 *)__this);
		((  void (*) (Iterator_1_t3764629478 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t3764629478 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		String_t* L_0 = ___fullPath0;
		__this->set_fullPath_12(L_0);
		String_t* L_1 = ___normalizedSearchPath1;
		__this->set_normalizedSearchPath_13(L_1);
		String_t* L_2 = ___searchCriteria2;
		__this->set_searchCriteria_6(L_2);
		SearchResultHandler_1_t2377980137 * L_3 = ___resultHandler5;
		__this->set__resultHandler_3(L_3);
		String_t* L_4 = ___userPath3;
		__this->set_userPath_10(L_4);
		int32_t L_5 = ___searchOption4;
		__this->set_searchOption_11(L_5);
		bool L_6 = ___checkHost6;
		__this->set__checkHost_14(L_6);
		List_1_t4120301035 * L_7 = (List_1_t4120301035 *)il2cpp_codegen_object_new(List_1_t4120301035_il2cpp_TypeInfo_var);
		List_1__ctor_m3635573260(L_7, /*hidden argument*/List_1__ctor_m3635573260_RuntimeMethod_var);
		__this->set_searchStack_4(L_7);
		String_t* L_8 = ___searchCriteria2;
		if (!L_8)
		{
			goto IL_0079;
		}
	}
	{
		StringU5BU5D_t1281789340* L_9 = (StringU5BU5D_t1281789340*)((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)2));
		String_t* L_10 = ___fullPath0;
		String_t* L_11 = Directory_GetDemandDir_m3334461828(NULL /*static, unused*/, (String_t*)L_10, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_11);
		String_t* L_12 = ___normalizedSearchPath1;
		String_t* L_13 = Directory_GetDemandDir_m3334461828(NULL /*static, unused*/, (String_t*)L_12, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_13);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_13);
		String_t* L_14 = ___normalizedSearchPath1;
		String_t* L_15 = ___userPath3;
		int32_t L_16 = ___searchOption4;
		SearchData_t2648226293 * L_17 = (SearchData_t2648226293 *)il2cpp_codegen_object_new(SearchData_t2648226293_il2cpp_TypeInfo_var);
		SearchData__ctor_m4162952925(L_17, (String_t*)L_14, (String_t*)L_15, (int32_t)L_16, /*hidden argument*/NULL);
		__this->set_searchData_5(L_17);
		NullCheck((FileSystemEnumerableIterator_1_t25181536 *)__this);
		((  void (*) (FileSystemEnumerableIterator_1_t25181536 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((FileSystemEnumerableIterator_1_t25181536 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return;
	}

IL_0079:
	{
		__this->set_empty_9((bool)1);
		return;
	}
}
// System.IO.Iterator`1<TSource> System.IO.FileSystemEnumerableIterator`1<System.Object>::Clone()
extern "C"  Iterator_1_t3764629478 * FileSystemEnumerableIterator_1_Clone_m2698474989_gshared (FileSystemEnumerableIterator_1_t25181536 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_fullPath_12();
		String_t* L_1 = (String_t*)__this->get_normalizedSearchPath_13();
		String_t* L_2 = (String_t*)__this->get_searchCriteria_6();
		String_t* L_3 = (String_t*)__this->get_userPath_10();
		int32_t L_4 = (int32_t)__this->get_searchOption_11();
		SearchResultHandler_1_t2377980137 * L_5 = (SearchResultHandler_1_t2377980137 *)__this->get__resultHandler_3();
		bool L_6 = (bool)__this->get__checkHost_14();
		FileSystemEnumerableIterator_1_t25181536 * L_7 = (FileSystemEnumerableIterator_1_t25181536 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((  void (*) (FileSystemEnumerableIterator_1_t25181536 *, String_t*, String_t*, String_t*, String_t*, int32_t, SearchResultHandler_1_t2377980137 *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(L_7, (String_t*)L_0, (String_t*)L_1, (String_t*)L_2, (String_t*)L_3, (int32_t)L_4, (SearchResultHandler_1_t2377980137 *)L_5, (bool)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_7;
	}
}
// System.Void System.IO.FileSystemEnumerableIterator`1<System.Object>::Dispose(System.Boolean)
extern "C"  void FileSystemEnumerableIterator_1_Dispose_m1208222860_gshared (FileSystemEnumerableIterator_1_t25181536 * __this, bool ___disposing0, const RuntimeMethod* method)
{
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			SafeFindHandle_t2068834300 * L_0 = (SafeFindHandle_t2068834300 *)__this->get__hnd_7();
			if (!L_0)
			{
				goto IL_0013;
			}
		}

IL_0008:
		{
			SafeFindHandle_t2068834300 * L_1 = (SafeFindHandle_t2068834300 *)__this->get__hnd_7();
			NullCheck((SafeHandle_t3273388951 *)L_1);
			SafeHandle_Dispose_m817995135((SafeHandle_t3273388951 *)L_1, /*hidden argument*/NULL);
		}

IL_0013:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0015);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0015;
	}

FINALLY_0015:
	{ // begin finally (depth: 1)
		bool L_2 = ___disposing0;
		NullCheck((Iterator_1_t3764629478 *)__this);
		((  void (*) (Iterator_1_t3764629478 *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Iterator_1_t3764629478 *)__this, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		IL2CPP_END_FINALLY(21)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(21)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean System.IO.FileSystemEnumerableIterator`1<System.Object>::MoveNext()
extern "C"  bool FileSystemEnumerableIterator_1_MoveNext_m808852106_gshared (FileSystemEnumerableIterator_1_t25181536 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileSystemEnumerableIterator_1_MoveNext_m808852106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WIN32_FIND_DATA_t4232796738 * V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	SearchResult_t2600365382 * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	SearchResult_t2600365382 * V_8 = NULL;
	{
		WIN32_FIND_DATA_t4232796738 * L_0 = (WIN32_FIND_DATA_t4232796738 *)il2cpp_codegen_object_new(WIN32_FIND_DATA_t4232796738_il2cpp_TypeInfo_var);
		WIN32_FIND_DATA__ctor_m1509629805(L_0, /*hidden argument*/NULL);
		V_0 = (WIN32_FIND_DATA_t4232796738 *)L_0;
		int32_t L_1 = (int32_t)((Iterator_1_t3764629478 *)__this)->get_state_1();
		V_1 = (int32_t)L_1;
		int32_t L_2 = V_1;
		switch (((int32_t)((int32_t)L_2-(int32_t)1)))
		{
			case 0:
			{
				goto IL_002a;
			}
			case 1:
			{
				goto IL_0175;
			}
			case 2:
			{
				goto IL_0192;
			}
			case 3:
			{
				goto IL_0278;
			}
		}
	}
	{
		goto IL_027e;
	}

IL_002a:
	{
		bool L_3 = (bool)__this->get_empty_9();
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		((Iterator_1_t3764629478 *)__this)->set_state_1(4);
		goto IL_0278;
	}

IL_003e:
	{
		SearchData_t2648226293 * L_4 = (SearchData_t2648226293 *)__this->get_searchData_5();
		NullCheck(L_4);
		int32_t L_5 = (int32_t)L_4->get_searchOption_2();
		if (L_5)
		{
			goto IL_0064;
		}
	}
	{
		((Iterator_1_t3764629478 *)__this)->set_state_1(3);
		RuntimeObject * L_6 = (RuntimeObject *)((Iterator_1_t3764629478 *)__this)->get_current_2();
		if (!L_6)
		{
			goto IL_0192;
		}
	}
	{
		return (bool)1;
	}

IL_0064:
	{
		((Iterator_1_t3764629478 *)__this)->set_state_1(2);
		goto IL_0175;
	}

IL_0070:
	{
		List_1_t4120301035 * L_7 = (List_1_t4120301035 *)__this->get_searchStack_4();
		NullCheck((List_1_t4120301035 *)L_7);
		SearchData_t2648226293 * L_8 = List_1_get_Item_m3638502404((List_1_t4120301035 *)L_7, (int32_t)0, /*hidden argument*/List_1_get_Item_m3638502404_RuntimeMethod_var);
		__this->set_searchData_5(L_8);
		List_1_t4120301035 * L_9 = (List_1_t4120301035 *)__this->get_searchStack_4();
		NullCheck((List_1_t4120301035 *)L_9);
		List_1_RemoveAt_m762780374((List_1_t4120301035 *)L_9, (int32_t)0, /*hidden argument*/List_1_RemoveAt_m762780374_RuntimeMethod_var);
		SearchData_t2648226293 * L_10 = (SearchData_t2648226293 *)__this->get_searchData_5();
		NullCheck((FileSystemEnumerableIterator_1_t25181536 *)__this);
		((  void (*) (FileSystemEnumerableIterator_1_t25181536 *, SearchData_t2648226293 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((FileSystemEnumerableIterator_1_t25181536 *)__this, (SearchData_t2648226293 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		SearchData_t2648226293 * L_11 = (SearchData_t2648226293 *)__this->get_searchData_5();
		NullCheck(L_11);
		String_t* L_12 = (String_t*)L_11->get_fullPath_0();
		String_t* L_13 = (String_t*)__this->get_searchCriteria_6();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_14 = Path_InternalCombine_m3863307630(NULL /*static, unused*/, (String_t*)L_12, (String_t*)L_13, /*hidden argument*/NULL);
		V_2 = (String_t*)L_14;
		String_t* L_15 = V_2;
		WIN32_FIND_DATA_t4232796738 * L_16 = V_0;
		NullCheck(L_16);
		String_t** L_17 = (String_t**)L_16->get_address_of_cFileName_11();
		WIN32_FIND_DATA_t4232796738 * L_18 = V_0;
		NullCheck(L_18);
		int32_t* L_19 = (int32_t*)L_18->get_address_of_dwFileAttributes_0();
		IL2CPP_RUNTIME_CLASS_INIT(MonoIO_t2601436415_il2cpp_TypeInfo_var);
		intptr_t L_20 = MonoIO_FindFirstFile_m1358799729(NULL /*static, unused*/, (String_t*)L_15, (String_t**)L_17, (int32_t*)L_19, (int32_t*)(&V_3), /*hidden argument*/NULL);
		SafeFindHandle_t2068834300 * L_21 = (SafeFindHandle_t2068834300 *)il2cpp_codegen_object_new(SafeFindHandle_t2068834300_il2cpp_TypeInfo_var);
		SafeFindHandle__ctor_m3833665175(L_21, (intptr_t)L_20, /*hidden argument*/NULL);
		__this->set__hnd_7(L_21);
		SafeFindHandle_t2068834300 * L_22 = (SafeFindHandle_t2068834300 *)__this->get__hnd_7();
		NullCheck((SafeHandle_t3273388951 *)L_22);
		bool L_23 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid() */, (SafeHandle_t3273388951 *)L_22);
		if (!L_23)
		{
			goto IL_0114;
		}
	}
	{
		int32_t L_24 = V_3;
		V_5 = (int32_t)L_24;
		int32_t L_25 = V_5;
		if ((((int32_t)L_25) == ((int32_t)2)))
		{
			goto IL_0175;
		}
	}
	{
		int32_t L_26 = V_5;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)18))))
		{
			goto IL_0175;
		}
	}
	{
		int32_t L_27 = V_5;
		if ((((int32_t)L_27) == ((int32_t)3)))
		{
			goto IL_0175;
		}
	}
	{
		SafeFindHandle_t2068834300 * L_28 = (SafeFindHandle_t2068834300 *)__this->get__hnd_7();
		NullCheck((SafeHandle_t3273388951 *)L_28);
		SafeHandle_Dispose_m817995135((SafeHandle_t3273388951 *)L_28, /*hidden argument*/NULL);
		int32_t L_29 = V_5;
		SearchData_t2648226293 * L_30 = (SearchData_t2648226293 *)__this->get_searchData_5();
		NullCheck(L_30);
		String_t* L_31 = (String_t*)L_30->get_fullPath_0();
		NullCheck((FileSystemEnumerableIterator_1_t25181536 *)__this);
		((  void (*) (FileSystemEnumerableIterator_1_t25181536 *, int32_t, String_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((FileSystemEnumerableIterator_1_t25181536 *)__this, (int32_t)L_29, (String_t*)L_31, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
	}

IL_0114:
	{
		((Iterator_1_t3764629478 *)__this)->set_state_1(3);
		__this->set_needsParentPathDiscoveryDemand_8((bool)1);
		SearchData_t2648226293 * L_32 = (SearchData_t2648226293 *)__this->get_searchData_5();
		WIN32_FIND_DATA_t4232796738 * L_33 = V_0;
		NullCheck((FileSystemEnumerableIterator_1_t25181536 *)__this);
		SearchResult_t2600365382 * L_34 = ((  SearchResult_t2600365382 * (*) (FileSystemEnumerableIterator_1_t25181536 *, SearchData_t2648226293 *, WIN32_FIND_DATA_t4232796738 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((FileSystemEnumerableIterator_1_t25181536 *)__this, (SearchData_t2648226293 *)L_32, (WIN32_FIND_DATA_t4232796738 *)L_33, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_4 = (SearchResult_t2600365382 *)L_34;
		SearchResultHandler_1_t2377980137 * L_35 = (SearchResultHandler_1_t2377980137 *)__this->get__resultHandler_3();
		SearchResult_t2600365382 * L_36 = V_4;
		NullCheck((SearchResultHandler_1_t2377980137 *)L_35);
		bool L_37 = VirtFuncInvoker1< bool, SearchResult_t2600365382 * >::Invoke(4 /* System.Boolean System.IO.SearchResultHandler`1<System.Object>::IsResultIncluded(System.IO.SearchResult) */, (SearchResultHandler_1_t2377980137 *)L_35, (SearchResult_t2600365382 *)L_36);
		if (!L_37)
		{
			goto IL_0192;
		}
	}
	{
		bool L_38 = (bool)__this->get_needsParentPathDiscoveryDemand_8();
		if (!L_38)
		{
			goto IL_0160;
		}
	}
	{
		SearchData_t2648226293 * L_39 = (SearchData_t2648226293 *)__this->get_searchData_5();
		NullCheck(L_39);
		String_t* L_40 = (String_t*)L_39->get_fullPath_0();
		NullCheck((FileSystemEnumerableIterator_1_t25181536 *)__this);
		((  void (*) (FileSystemEnumerableIterator_1_t25181536 *, String_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((FileSystemEnumerableIterator_1_t25181536 *)__this, (String_t*)L_40, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		__this->set_needsParentPathDiscoveryDemand_8((bool)0);
	}

IL_0160:
	{
		SearchResultHandler_1_t2377980137 * L_41 = (SearchResultHandler_1_t2377980137 *)__this->get__resultHandler_3();
		SearchResult_t2600365382 * L_42 = V_4;
		NullCheck((SearchResultHandler_1_t2377980137 *)L_41);
		RuntimeObject * L_43 = VirtFuncInvoker1< RuntimeObject *, SearchResult_t2600365382 * >::Invoke(5 /* TSource System.IO.SearchResultHandler`1<System.Object>::CreateObject(System.IO.SearchResult) */, (SearchResultHandler_1_t2377980137 *)L_41, (SearchResult_t2600365382 *)L_42);
		((Iterator_1_t3764629478 *)__this)->set_current_2(L_43);
		return (bool)1;
	}

IL_0175:
	{
		List_1_t4120301035 * L_44 = (List_1_t4120301035 *)__this->get_searchStack_4();
		NullCheck((List_1_t4120301035 *)L_44);
		int32_t L_45 = List_1_get_Count_m958122929((List_1_t4120301035 *)L_44, /*hidden argument*/List_1_get_Count_m958122929_RuntimeMethod_var);
		if ((((int32_t)L_45) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		((Iterator_1_t3764629478 *)__this)->set_state_1(4);
		goto IL_0278;
	}

IL_0192:
	{
		SearchData_t2648226293 * L_46 = (SearchData_t2648226293 *)__this->get_searchData_5();
		if (!L_46)
		{
			goto IL_0256;
		}
	}
	{
		SafeFindHandle_t2068834300 * L_47 = (SafeFindHandle_t2068834300 *)__this->get__hnd_7();
		if (!L_47)
		{
			goto IL_0256;
		}
	}
	{
		goto IL_01fd;
	}

IL_01aa:
	{
		SearchData_t2648226293 * L_48 = (SearchData_t2648226293 *)__this->get_searchData_5();
		WIN32_FIND_DATA_t4232796738 * L_49 = V_0;
		NullCheck((FileSystemEnumerableIterator_1_t25181536 *)__this);
		SearchResult_t2600365382 * L_50 = ((  SearchResult_t2600365382 * (*) (FileSystemEnumerableIterator_1_t25181536 *, SearchData_t2648226293 *, WIN32_FIND_DATA_t4232796738 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((FileSystemEnumerableIterator_1_t25181536 *)__this, (SearchData_t2648226293 *)L_48, (WIN32_FIND_DATA_t4232796738 *)L_49, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_8 = (SearchResult_t2600365382 *)L_50;
		SearchResultHandler_1_t2377980137 * L_51 = (SearchResultHandler_1_t2377980137 *)__this->get__resultHandler_3();
		SearchResult_t2600365382 * L_52 = V_8;
		NullCheck((SearchResultHandler_1_t2377980137 *)L_51);
		bool L_53 = VirtFuncInvoker1< bool, SearchResult_t2600365382 * >::Invoke(4 /* System.Boolean System.IO.SearchResultHandler`1<System.Object>::IsResultIncluded(System.IO.SearchResult) */, (SearchResultHandler_1_t2377980137 *)L_51, (SearchResult_t2600365382 *)L_52);
		if (!L_53)
		{
			goto IL_01fd;
		}
	}
	{
		bool L_54 = (bool)__this->get_needsParentPathDiscoveryDemand_8();
		if (!L_54)
		{
			goto IL_01e8;
		}
	}
	{
		SearchData_t2648226293 * L_55 = (SearchData_t2648226293 *)__this->get_searchData_5();
		NullCheck(L_55);
		String_t* L_56 = (String_t*)L_55->get_fullPath_0();
		NullCheck((FileSystemEnumerableIterator_1_t25181536 *)__this);
		((  void (*) (FileSystemEnumerableIterator_1_t25181536 *, String_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((FileSystemEnumerableIterator_1_t25181536 *)__this, (String_t*)L_56, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		__this->set_needsParentPathDiscoveryDemand_8((bool)0);
	}

IL_01e8:
	{
		SearchResultHandler_1_t2377980137 * L_57 = (SearchResultHandler_1_t2377980137 *)__this->get__resultHandler_3();
		SearchResult_t2600365382 * L_58 = V_8;
		NullCheck((SearchResultHandler_1_t2377980137 *)L_57);
		RuntimeObject * L_59 = VirtFuncInvoker1< RuntimeObject *, SearchResult_t2600365382 * >::Invoke(5 /* TSource System.IO.SearchResultHandler`1<System.Object>::CreateObject(System.IO.SearchResult) */, (SearchResultHandler_1_t2377980137 *)L_57, (SearchResult_t2600365382 *)L_58);
		((Iterator_1_t3764629478 *)__this)->set_current_2(L_59);
		return (bool)1;
	}

IL_01fd:
	{
		SafeFindHandle_t2068834300 * L_60 = (SafeFindHandle_t2068834300 *)__this->get__hnd_7();
		NullCheck((SafeHandle_t3273388951 *)L_60);
		intptr_t L_61 = SafeHandle_DangerousGetHandle_m3697436134((SafeHandle_t3273388951 *)L_60, /*hidden argument*/NULL);
		WIN32_FIND_DATA_t4232796738 * L_62 = V_0;
		NullCheck(L_62);
		String_t** L_63 = (String_t**)L_62->get_address_of_cFileName_11();
		WIN32_FIND_DATA_t4232796738 * L_64 = V_0;
		NullCheck(L_64);
		int32_t* L_65 = (int32_t*)L_64->get_address_of_dwFileAttributes_0();
		IL2CPP_RUNTIME_CLASS_INIT(MonoIO_t2601436415_il2cpp_TypeInfo_var);
		bool L_66 = MonoIO_FindNextFile_m3163114833(NULL /*static, unused*/, (intptr_t)L_61, (String_t**)L_63, (int32_t*)L_65, (int32_t*)(&V_6), /*hidden argument*/NULL);
		if (L_66)
		{
			goto IL_01aa;
		}
	}
	{
		int32_t L_67 = V_6;
		V_7 = (int32_t)L_67;
		SafeFindHandle_t2068834300 * L_68 = (SafeFindHandle_t2068834300 *)__this->get__hnd_7();
		if (!L_68)
		{
			goto IL_0234;
		}
	}
	{
		SafeFindHandle_t2068834300 * L_69 = (SafeFindHandle_t2068834300 *)__this->get__hnd_7();
		NullCheck((SafeHandle_t3273388951 *)L_69);
		SafeHandle_Dispose_m817995135((SafeHandle_t3273388951 *)L_69, /*hidden argument*/NULL);
	}

IL_0234:
	{
		int32_t L_70 = V_7;
		if (!L_70)
		{
			goto IL_0256;
		}
	}
	{
		int32_t L_71 = V_7;
		if ((((int32_t)L_71) == ((int32_t)((int32_t)18))))
		{
			goto IL_0256;
		}
	}
	{
		int32_t L_72 = V_7;
		if ((((int32_t)L_72) == ((int32_t)2)))
		{
			goto IL_0256;
		}
	}
	{
		int32_t L_73 = V_7;
		SearchData_t2648226293 * L_74 = (SearchData_t2648226293 *)__this->get_searchData_5();
		NullCheck(L_74);
		String_t* L_75 = (String_t*)L_74->get_fullPath_0();
		NullCheck((FileSystemEnumerableIterator_1_t25181536 *)__this);
		((  void (*) (FileSystemEnumerableIterator_1_t25181536 *, int32_t, String_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((FileSystemEnumerableIterator_1_t25181536 *)__this, (int32_t)L_73, (String_t*)L_75, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
	}

IL_0256:
	{
		SearchData_t2648226293 * L_76 = (SearchData_t2648226293 *)__this->get_searchData_5();
		NullCheck(L_76);
		int32_t L_77 = (int32_t)L_76->get_searchOption_2();
		if (L_77)
		{
			goto IL_026c;
		}
	}
	{
		((Iterator_1_t3764629478 *)__this)->set_state_1(4);
		goto IL_0278;
	}

IL_026c:
	{
		((Iterator_1_t3764629478 *)__this)->set_state_1(2);
		goto IL_0175;
	}

IL_0278:
	{
		NullCheck((Iterator_1_t3764629478 *)__this);
		((  void (*) (Iterator_1_t3764629478 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((Iterator_1_t3764629478 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_027e:
	{
		return (bool)0;
	}
}
// System.IO.SearchResult System.IO.FileSystemEnumerableIterator`1<System.Object>::CreateSearchResult(System.IO.Directory/SearchData,Microsoft.Win32.Win32Native/WIN32_FIND_DATA)
extern "C"  SearchResult_t2600365382 * FileSystemEnumerableIterator_1_CreateSearchResult_m1408355418_gshared (FileSystemEnumerableIterator_1_t25181536 * __this, SearchData_t2648226293 * ___localSearchData0, WIN32_FIND_DATA_t4232796738 * ___findData1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileSystemEnumerableIterator_1_CreateSearchResult_m1408355418_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		SearchData_t2648226293 * L_0 = ___localSearchData0;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)L_0->get_userPath_1();
		WIN32_FIND_DATA_t4232796738 * L_2 = ___findData1;
		NullCheck(L_2);
		String_t* L_3 = (String_t*)L_2->get_cFileName_11();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_4 = Path_InternalCombine_m3863307630(NULL /*static, unused*/, (String_t*)L_1, (String_t*)L_3, /*hidden argument*/NULL);
		V_0 = (String_t*)L_4;
		SearchData_t2648226293 * L_5 = ___localSearchData0;
		NullCheck(L_5);
		String_t* L_6 = (String_t*)L_5->get_fullPath_0();
		WIN32_FIND_DATA_t4232796738 * L_7 = ___findData1;
		NullCheck(L_7);
		String_t* L_8 = (String_t*)L_7->get_cFileName_11();
		String_t* L_9 = Path_InternalCombine_m3863307630(NULL /*static, unused*/, (String_t*)L_6, (String_t*)L_8, /*hidden argument*/NULL);
		String_t* L_10 = V_0;
		WIN32_FIND_DATA_t4232796738 * L_11 = ___findData1;
		SearchResult_t2600365382 * L_12 = (SearchResult_t2600365382 *)il2cpp_codegen_object_new(SearchResult_t2600365382_il2cpp_TypeInfo_var);
		SearchResult__ctor_m203165125(L_12, (String_t*)L_9, (String_t*)L_10, (WIN32_FIND_DATA_t4232796738 *)L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void System.IO.FileSystemEnumerableIterator`1<System.Object>::HandleError(System.Int32,System.String)
extern "C"  void FileSystemEnumerableIterator_1_HandleError_m3891558950_gshared (FileSystemEnumerableIterator_1_t25181536 * __this, int32_t ___hr0, String_t* ___path1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t3764629478 *)__this);
		((  void (*) (Iterator_1_t3764629478 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((Iterator_1_t3764629478 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		int32_t L_0 = ___hr0;
		String_t* L_1 = ___path1;
		__Error_WinIOError_m2555862149(NULL /*static, unused*/, (int32_t)L_0, (String_t*)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.FileSystemEnumerableIterator`1<System.Object>::AddSearchableDirsToStack(System.IO.Directory/SearchData)
extern "C"  void FileSystemEnumerableIterator_1_AddSearchableDirsToStack_m2987866814_gshared (FileSystemEnumerableIterator_1_t25181536 * __this, SearchData_t2648226293 * ___localSearchData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileSystemEnumerableIterator_1_AddSearchableDirsToStack_m2987866814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	SafeFindHandle_t2068834300 * V_1 = NULL;
	WIN32_FIND_DATA_t4232796738 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	String_t* V_6 = NULL;
	int32_t V_7 = 0;
	SearchData_t2648226293 * V_8 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SearchData_t2648226293 * L_0 = ___localSearchData0;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)L_0->get_fullPath_0();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_2 = Path_InternalCombine_m3863307630(NULL /*static, unused*/, (String_t*)L_1, (String_t*)_stringLiteral3452614534, /*hidden argument*/NULL);
		V_0 = (String_t*)L_2;
		V_1 = (SafeFindHandle_t2068834300 *)NULL;
		WIN32_FIND_DATA_t4232796738 * L_3 = (WIN32_FIND_DATA_t4232796738 *)il2cpp_codegen_object_new(WIN32_FIND_DATA_t4232796738_il2cpp_TypeInfo_var);
		WIN32_FIND_DATA__ctor_m1509629805(L_3, /*hidden argument*/NULL);
		V_2 = (WIN32_FIND_DATA_t4232796738 *)L_3;
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_4 = V_0;
			WIN32_FIND_DATA_t4232796738 * L_5 = V_2;
			NullCheck(L_5);
			String_t** L_6 = (String_t**)L_5->get_address_of_cFileName_11();
			WIN32_FIND_DATA_t4232796738 * L_7 = V_2;
			NullCheck(L_7);
			int32_t* L_8 = (int32_t*)L_7->get_address_of_dwFileAttributes_0();
			IL2CPP_RUNTIME_CLASS_INIT(MonoIO_t2601436415_il2cpp_TypeInfo_var);
			intptr_t L_9 = MonoIO_FindFirstFile_m1358799729(NULL /*static, unused*/, (String_t*)L_4, (String_t**)L_6, (int32_t*)L_8, (int32_t*)(&V_3), /*hidden argument*/NULL);
			SafeFindHandle_t2068834300 * L_10 = (SafeFindHandle_t2068834300 *)il2cpp_codegen_object_new(SafeFindHandle_t2068834300_il2cpp_TypeInfo_var);
			SafeFindHandle__ctor_m3833665175(L_10, (intptr_t)L_9, /*hidden argument*/NULL);
			V_1 = (SafeFindHandle_t2068834300 *)L_10;
			SafeFindHandle_t2068834300 * L_11 = V_1;
			NullCheck((SafeHandle_t3273388951 *)L_11);
			bool L_12 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid() */, (SafeHandle_t3273388951 *)L_11);
			if (!L_12)
			{
				goto IL_0061;
			}
		}

IL_003b:
		{
			int32_t L_13 = V_3;
			V_5 = (int32_t)L_13;
			int32_t L_14 = V_5;
			if ((((int32_t)L_14) == ((int32_t)2)))
			{
				goto IL_004e;
			}
		}

IL_0043:
		{
			int32_t L_15 = V_5;
			if ((((int32_t)L_15) == ((int32_t)((int32_t)18))))
			{
				goto IL_004e;
			}
		}

IL_0049:
		{
			int32_t L_16 = V_5;
			if ((!(((uint32_t)L_16) == ((uint32_t)3))))
			{
				goto IL_0053;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0xDE, FINALLY_00d4);
		}

IL_0053:
		{
			int32_t L_17 = V_5;
			SearchData_t2648226293 * L_18 = ___localSearchData0;
			NullCheck(L_18);
			String_t* L_19 = (String_t*)L_18->get_fullPath_0();
			NullCheck((FileSystemEnumerableIterator_1_t25181536 *)__this);
			((  void (*) (FileSystemEnumerableIterator_1_t25181536 *, int32_t, String_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((FileSystemEnumerableIterator_1_t25181536 *)__this, (int32_t)L_17, (String_t*)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		}

IL_0061:
		{
			V_4 = (int32_t)0;
		}

IL_0064:
		{
			WIN32_FIND_DATA_t4232796738 * L_20 = V_2;
			bool L_21 = FileSystemEnumerableHelpers_IsDir_m2524844520(NULL /*static, unused*/, (WIN32_FIND_DATA_t4232796738 *)L_20, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_00b7;
			}
		}

IL_006c:
		{
			SearchData_t2648226293 * L_22 = ___localSearchData0;
			NullCheck(L_22);
			String_t* L_23 = (String_t*)L_22->get_fullPath_0();
			WIN32_FIND_DATA_t4232796738 * L_24 = V_2;
			NullCheck(L_24);
			String_t* L_25 = (String_t*)L_24->get_cFileName_11();
			IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
			String_t* L_26 = Path_InternalCombine_m3863307630(NULL /*static, unused*/, (String_t*)L_23, (String_t*)L_25, /*hidden argument*/NULL);
			SearchData_t2648226293 * L_27 = ___localSearchData0;
			NullCheck(L_27);
			String_t* L_28 = (String_t*)L_27->get_userPath_1();
			WIN32_FIND_DATA_t4232796738 * L_29 = V_2;
			NullCheck(L_29);
			String_t* L_30 = (String_t*)L_29->get_cFileName_11();
			String_t* L_31 = Path_InternalCombine_m3863307630(NULL /*static, unused*/, (String_t*)L_28, (String_t*)L_30, /*hidden argument*/NULL);
			V_6 = (String_t*)L_31;
			SearchData_t2648226293 * L_32 = ___localSearchData0;
			NullCheck(L_32);
			int32_t L_33 = (int32_t)L_32->get_searchOption_2();
			V_7 = (int32_t)L_33;
			String_t* L_34 = V_6;
			int32_t L_35 = V_7;
			SearchData_t2648226293 * L_36 = (SearchData_t2648226293 *)il2cpp_codegen_object_new(SearchData_t2648226293_il2cpp_TypeInfo_var);
			SearchData__ctor_m4162952925(L_36, (String_t*)L_26, (String_t*)L_34, (int32_t)L_35, /*hidden argument*/NULL);
			V_8 = (SearchData_t2648226293 *)L_36;
			List_1_t4120301035 * L_37 = (List_1_t4120301035 *)__this->get_searchStack_4();
			int32_t L_38 = V_4;
			int32_t L_39 = (int32_t)L_38;
			V_4 = (int32_t)((int32_t)((int32_t)L_39+(int32_t)1));
			SearchData_t2648226293 * L_40 = V_8;
			NullCheck((List_1_t4120301035 *)L_37);
			List_1_Insert_m2523363920((List_1_t4120301035 *)L_37, (int32_t)L_39, (SearchData_t2648226293 *)L_40, /*hidden argument*/List_1_Insert_m2523363920_RuntimeMethod_var);
		}

IL_00b7:
		{
			SafeFindHandle_t2068834300 * L_41 = V_1;
			NullCheck((SafeHandle_t3273388951 *)L_41);
			intptr_t L_42 = SafeHandle_DangerousGetHandle_m3697436134((SafeHandle_t3273388951 *)L_41, /*hidden argument*/NULL);
			WIN32_FIND_DATA_t4232796738 * L_43 = V_2;
			NullCheck(L_43);
			String_t** L_44 = (String_t**)L_43->get_address_of_cFileName_11();
			WIN32_FIND_DATA_t4232796738 * L_45 = V_2;
			NullCheck(L_45);
			int32_t* L_46 = (int32_t*)L_45->get_address_of_dwFileAttributes_0();
			IL2CPP_RUNTIME_CLASS_INIT(MonoIO_t2601436415_il2cpp_TypeInfo_var);
			bool L_47 = MonoIO_FindNextFile_m3163114833(NULL /*static, unused*/, (intptr_t)L_42, (String_t**)L_44, (int32_t*)L_46, (int32_t*)(&V_3), /*hidden argument*/NULL);
			if (L_47)
			{
				goto IL_0064;
			}
		}

IL_00d2:
		{
			IL2CPP_LEAVE(0xDE, FINALLY_00d4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_00d4;
	}

FINALLY_00d4:
	{ // begin finally (depth: 1)
		{
			SafeFindHandle_t2068834300 * L_48 = V_1;
			if (!L_48)
			{
				goto IL_00dd;
			}
		}

IL_00d7:
		{
			SafeFindHandle_t2068834300 * L_49 = V_1;
			NullCheck((SafeHandle_t3273388951 *)L_49);
			SafeHandle_Dispose_m817995135((SafeHandle_t3273388951 *)L_49, /*hidden argument*/NULL);
		}

IL_00dd:
		{
			IL2CPP_END_FINALLY(212)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(212)
	{
		IL2CPP_JUMP_TBL(0xDE, IL_00de)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_00de:
	{
		return;
	}
}
// System.Void System.IO.FileSystemEnumerableIterator`1<System.Object>::DoDemand(System.String)
extern "C"  void FileSystemEnumerableIterator_1_DoDemand_m3309936201_gshared (FileSystemEnumerableIterator_1_t25181536 * __this, String_t* ___fullPathToDemand0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.String System.IO.FileSystemEnumerableIterator`1<System.Object>::NormalizeSearchPattern(System.String)
extern "C"  String_t* FileSystemEnumerableIterator_1_NormalizeSearchPattern_m1627217068_gshared (RuntimeObject * __this /* static, unused */, String_t* ___searchPattern0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileSystemEnumerableIterator_1_NormalizeSearchPattern_m1627217068_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___searchPattern0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		CharU5BU5D_t3528271667* L_1 = Path_get_TrimEndChars_m4264594297(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((String_t*)L_0);
		String_t* L_2 = String_TrimEnd_m3824727301((String_t*)L_0, (CharU5BU5D_t3528271667*)L_1, /*hidden argument*/NULL);
		V_0 = (String_t*)L_2;
		String_t* L_3 = V_0;
		NullCheck((String_t*)L_3);
		bool L_4 = String_Equals_m2270643605((String_t*)L_3, (String_t*)_stringLiteral3452614530, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		V_0 = (String_t*)_stringLiteral3452614534;
	}

IL_001f:
	{
		String_t* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		Path_CheckSearchPattern_m974385397(NULL /*static, unused*/, (String_t*)L_5, /*hidden argument*/NULL);
		String_t* L_6 = V_0;
		return L_6;
	}
}
// System.String System.IO.FileSystemEnumerableIterator`1<System.Object>::GetNormalizedSearchCriteria(System.String,System.String)
extern "C"  String_t* FileSystemEnumerableIterator_1_GetNormalizedSearchCriteria_m63937011_gshared (RuntimeObject * __this /* static, unused */, String_t* ___fullSearchString0, String_t* ___fullPathMod1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileSystemEnumerableIterator_1_GetNormalizedSearchCriteria_m63937011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = (String_t*)NULL;
		String_t* L_0 = ___fullPathMod1;
		String_t* L_1 = ___fullPathMod1;
		NullCheck((String_t*)L_1);
		int32_t L_2 = String_get_Length_m3847582255((String_t*)L_1, /*hidden argument*/NULL);
		NullCheck((String_t*)L_0);
		Il2CppChar L_3 = String_get_Chars_m2986988803((String_t*)L_0, (int32_t)((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		bool L_4 = Path_IsDirectorySeparator_m1029452715(NULL /*static, unused*/, (Il2CppChar)L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		String_t* L_5 = ___fullSearchString0;
		String_t* L_6 = ___fullPathMod1;
		NullCheck((String_t*)L_6);
		int32_t L_7 = String_get_Length_m3847582255((String_t*)L_6, /*hidden argument*/NULL);
		NullCheck((String_t*)L_5);
		String_t* L_8 = String_Substring_m2848979100((String_t*)L_5, (int32_t)L_7, /*hidden argument*/NULL);
		V_0 = (String_t*)L_8;
		goto IL_0035;
	}

IL_0026:
	{
		String_t* L_9 = ___fullSearchString0;
		String_t* L_10 = ___fullPathMod1;
		NullCheck((String_t*)L_10);
		int32_t L_11 = String_get_Length_m3847582255((String_t*)L_10, /*hidden argument*/NULL);
		NullCheck((String_t*)L_9);
		String_t* L_12 = String_Substring_m2848979100((String_t*)L_9, (int32_t)((int32_t)((int32_t)L_11+(int32_t)1)), /*hidden argument*/NULL);
		V_0 = (String_t*)L_12;
	}

IL_0035:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
// System.String System.IO.FileSystemEnumerableIterator`1<System.Object>::GetFullSearchString(System.String,System.String)
extern "C"  String_t* FileSystemEnumerableIterator_1_GetFullSearchString_m3869162752_gshared (RuntimeObject * __this /* static, unused */, String_t* ___fullPath0, String_t* ___searchPattern1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileSystemEnumerableIterator_1_GetFullSearchString_m3869162752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Il2CppChar V_1 = 0x0;
	{
		String_t* L_0 = ___fullPath0;
		String_t* L_1 = ___searchPattern1;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_2 = Path_InternalCombine_m3863307630(NULL /*static, unused*/, (String_t*)L_0, (String_t*)L_1, /*hidden argument*/NULL);
		V_0 = (String_t*)L_2;
		String_t* L_3 = V_0;
		String_t* L_4 = V_0;
		NullCheck((String_t*)L_4);
		int32_t L_5 = String_get_Length_m3847582255((String_t*)L_4, /*hidden argument*/NULL);
		NullCheck((String_t*)L_3);
		Il2CppChar L_6 = String_get_Chars_m2986988803((String_t*)L_3, (int32_t)((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/NULL);
		V_1 = (Il2CppChar)L_6;
		Il2CppChar L_7 = V_1;
		bool L_8 = Path_IsDirectorySeparator_m1029452715(NULL /*static, unused*/, (Il2CppChar)L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0027;
		}
	}
	{
		Il2CppChar L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		Il2CppChar L_10 = ((Path_t1605229823_StaticFields*)il2cpp_codegen_static_fields_for(Path_t1605229823_il2cpp_TypeInfo_var))->get_VolumeSeparatorChar_5();
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_0033;
		}
	}

IL_0027:
	{
		String_t* L_11 = V_0;
		String_t* L_12 = String_Concat_m3937257545(NULL /*static, unused*/, (String_t*)L_11, (String_t*)_stringLiteral3452614534, /*hidden argument*/NULL);
		V_0 = (String_t*)L_12;
	}

IL_0033:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
// System.Void System.IO.Iterator`1<System.Object>::.ctor()
extern "C"  void Iterator_1__ctor_m3107024526_gshared (Iterator_1_t3764629478 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m297566312((RuntimeObject *)__this, /*hidden argument*/NULL);
		Thread_t2300836069 * L_0 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Thread_t2300836069 *)L_0);
		int32_t L_1 = Thread_get_ManagedThreadId_m1068113671((Thread_t2300836069 *)L_0, /*hidden argument*/NULL);
		__this->set_threadId_0(L_1);
		return;
	}
}
// TSource System.IO.Iterator`1<System.Object>::get_Current()
extern "C"  RuntimeObject * Iterator_1_get_Current_m3226020659_gshared (Iterator_1_t3764629478 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_2();
		return L_0;
	}
}
// System.Void System.IO.Iterator`1<System.Object>::Dispose()
extern "C"  void Iterator_1_Dispose_m1804293407_gshared (Iterator_1_t3764629478 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Iterator_1_Dispose_m1804293407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Iterator_1_t3764629478 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void System.IO.Iterator`1<System.Object>::Dispose(System.Boolean) */, (Iterator_1_t3764629478 *)__this, (bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(GC_t959872083_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m1177400158(NULL /*static, unused*/, (RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Iterator`1<System.Object>::Dispose(System.Boolean)
extern "C"  void Iterator_1_Dispose_m768625415_gshared (Iterator_1_t3764629478 * __this, bool ___disposing0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Iterator_1_Dispose_m768625415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject ** L_0 = (RuntimeObject **)__this->get_address_of_current_2();
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, L_0);
		__this->set_state_1((-1));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.IO.Iterator`1<System.Object>::GetEnumerator()
extern "C"  RuntimeObject* Iterator_1_GetEnumerator_m396814019_gshared (Iterator_1_t3764629478 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_threadId_0();
		Thread_t2300836069 * L_1 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Thread_t2300836069 *)L_1);
		int32_t L_2 = Thread_get_ManagedThreadId_m1068113671((Thread_t2300836069 *)L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_state_1();
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		__this->set_state_1(1);
		return __this;
	}

IL_0023:
	{
		NullCheck((Iterator_1_t3764629478 *)__this);
		Iterator_1_t3764629478 * L_4 = VirtFuncInvoker0< Iterator_1_t3764629478 * >::Invoke(11 /* System.IO.Iterator`1<TSource> System.IO.Iterator`1<System.Object>::Clone() */, (Iterator_1_t3764629478 *)__this);
		Iterator_1_t3764629478 * L_5 = (Iterator_1_t3764629478 *)L_4;
		NullCheck(L_5);
		L_5->set_state_1(1);
		return L_5;
	}
}
// System.Object System.IO.Iterator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Iterator_1_System_Collections_IEnumerator_get_Current_m716568830_gshared (Iterator_1_t3764629478 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t3764629478 *)__this);
		RuntimeObject * L_0 = ((  RuntimeObject * (*) (Iterator_1_t3764629478 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Iterator_1_t3764629478 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_0;
	}
}
// System.Collections.IEnumerator System.IO.Iterator`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* Iterator_1_System_Collections_IEnumerable_GetEnumerator_m4153551952_gshared (Iterator_1_t3764629478 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t3764629478 *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (Iterator_1_t3764629478 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Iterator_1_t3764629478 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_0;
	}
}
// System.Void System.IO.Iterator`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Iterator_1_System_Collections_IEnumerator_Reset_m2231441267_gshared (Iterator_1_t3764629478 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Iterator_1_System_Collections_IEnumerator_Reset_m2231441267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.IO.SearchResultHandler`1<System.Object>::.ctor()
extern "C"  void SearchResultHandler_1__ctor_m4152223460_gshared (SearchResultHandler_1_t2377980137 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m297566312((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Linq.Buffer`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  void Buffer_1__ctor_m3439248099_gshared (Buffer_1_t932544294 * __this, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	ObjectU5BU5D_t2843939325* V_1 = NULL;
	{
		RuntimeObject* L_0 = ___source0;
		V_0 = (RuntimeObject*)((RuntimeObject*)IsInst((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		RuntimeObject* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		RuntimeObject* L_2 = V_0;
		NullCheck((RuntimeObject*)L_2);
		ObjectU5BU5D_t2843939325* L_3 = InterfaceFuncInvoker0< ObjectU5BU5D_t2843939325* >::Invoke(0 /* TElement[] System.Linq.IIListProvider`1<System.Object>::ToArray() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (RuntimeObject*)L_2);
		V_1 = (ObjectU5BU5D_t2843939325*)L_3;
		ObjectU5BU5D_t2843939325* L_4 = V_1;
		__this->set__items_0(L_4);
		ObjectU5BU5D_t2843939325* L_5 = V_1;
		NullCheck(L_5);
		__this->set__count_1((((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length)))));
		return;
	}

IL_0022:
	{
		RuntimeObject* L_6 = ___source0;
		int32_t* L_7 = (int32_t*)__this->get_address_of__count_1();
		ObjectU5BU5D_t2843939325* L_8 = ((  ObjectU5BU5D_t2843939325* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, int32_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_6, (int32_t*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set__items_0(L_8);
		return;
	}
}
extern "C"  void Buffer_1__ctor_m3439248099_AdjustorThunk (RuntimeObject * __this, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	Buffer_1_t932544294 * _thisAdjusted = reinterpret_cast<Buffer_1_t932544294 *>(__this + 1);
	Buffer_1__ctor_m3439248099(_thisAdjusted, ___source0, method);
}
// System.Void System.Linq.Buffer`1<UnityEngine.Vector2>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  void Buffer_1__ctor_m1836940346_gshared (Buffer_1_t8667653 * __this, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	Vector2U5BU5D_t1457185986* V_1 = NULL;
	{
		RuntimeObject* L_0 = ___source0;
		V_0 = (RuntimeObject*)((RuntimeObject*)IsInst((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		RuntimeObject* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		RuntimeObject* L_2 = V_0;
		NullCheck((RuntimeObject*)L_2);
		Vector2U5BU5D_t1457185986* L_3 = InterfaceFuncInvoker0< Vector2U5BU5D_t1457185986* >::Invoke(0 /* TElement[] System.Linq.IIListProvider`1<UnityEngine.Vector2>::ToArray() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (RuntimeObject*)L_2);
		V_1 = (Vector2U5BU5D_t1457185986*)L_3;
		Vector2U5BU5D_t1457185986* L_4 = V_1;
		__this->set__items_0(L_4);
		Vector2U5BU5D_t1457185986* L_5 = V_1;
		NullCheck(L_5);
		__this->set__count_1((((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length)))));
		return;
	}

IL_0022:
	{
		RuntimeObject* L_6 = ___source0;
		int32_t* L_7 = (int32_t*)__this->get_address_of__count_1();
		Vector2U5BU5D_t1457185986* L_8 = ((  Vector2U5BU5D_t1457185986* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, int32_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_6, (int32_t*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set__items_0(L_8);
		return;
	}
}
extern "C"  void Buffer_1__ctor_m1836940346_AdjustorThunk (RuntimeObject * __this, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	Buffer_1_t8667653 * _thisAdjusted = reinterpret_cast<Buffer_1_t8667653 *>(__this + 1);
	Buffer_1__ctor_m1836940346(_thisAdjusted, ___source0, method);
}
// System.Void System.Linq.Buffer`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  void Buffer_1__ctor_m2075330287_gshared (Buffer_1_t1574751594 * __this, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	Vector3U5BU5D_t1718750761* V_1 = NULL;
	{
		RuntimeObject* L_0 = ___source0;
		V_0 = (RuntimeObject*)((RuntimeObject*)IsInst((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		RuntimeObject* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		RuntimeObject* L_2 = V_0;
		NullCheck((RuntimeObject*)L_2);
		Vector3U5BU5D_t1718750761* L_3 = InterfaceFuncInvoker0< Vector3U5BU5D_t1718750761* >::Invoke(0 /* TElement[] System.Linq.IIListProvider`1<UnityEngine.Vector3>::ToArray() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (RuntimeObject*)L_2);
		V_1 = (Vector3U5BU5D_t1718750761*)L_3;
		Vector3U5BU5D_t1718750761* L_4 = V_1;
		__this->set__items_0(L_4);
		Vector3U5BU5D_t1718750761* L_5 = V_1;
		NullCheck(L_5);
		__this->set__count_1((((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length)))));
		return;
	}

IL_0022:
	{
		RuntimeObject* L_6 = ___source0;
		int32_t* L_7 = (int32_t*)__this->get_address_of__count_1();
		Vector3U5BU5D_t1718750761* L_8 = ((  Vector3U5BU5D_t1718750761* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, int32_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_6, (int32_t*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set__items_0(L_8);
		return;
	}
}
extern "C"  void Buffer_1__ctor_m2075330287_AdjustorThunk (RuntimeObject * __this, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	Buffer_1_t1574751594 * _thisAdjusted = reinterpret_cast<Buffer_1_t1574751594 *>(__this + 1);
	Buffer_1__ctor_m2075330287(_thisAdjusted, ___source0, method);
}
// System.Void System.Linq.CachingComparer`1<System.Object>::.ctor()
extern "C"  void CachingComparer_1__ctor_m368776016_gshared (CachingComparer_1_t1378817919 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m297566312((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Linq.CachingComparer`1<UnityEngine.Vector2>::.ctor()
extern "C"  void CachingComparer_1__ctor_m3132819659_gshared (CachingComparer_1_t454941278 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m297566312((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Linq.CachingComparer`1<UnityEngine.Vector3>::.ctor()
extern "C"  void CachingComparer_1__ctor_m3132816462_gshared (CachingComparer_1_t2021025219 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m297566312((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Linq.CachingComparer`2<System.Object,System.Int32>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
extern "C"  void CachingComparer_2__ctor_m3341089146_gshared (CachingComparer_2_t2712248853 * __this, Func_2_t2317969963 * ___keySelector0, RuntimeObject* ___comparer1, bool ___descending2, const RuntimeMethod* method)
{
	{
		NullCheck((CachingComparer_1_t1378817919 *)__this);
		((  void (*) (CachingComparer_1_t1378817919 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CachingComparer_1_t1378817919 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t2317969963 * L_0 = ___keySelector0;
		__this->set__keySelector_0(L_0);
		RuntimeObject* L_1 = ___comparer1;
		__this->set__comparer_1(L_1);
		bool L_2 = ___descending2;
		__this->set__descending_2(L_2);
		return;
	}
}
// System.Int32 System.Linq.CachingComparer`2<System.Object,System.Int32>::Compare(TElement,System.Boolean)
extern "C"  int32_t CachingComparer_2_Compare_m3427904112_gshared (CachingComparer_2_t2712248853 * __this, RuntimeObject * ___element0, bool ___cacheLower1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		Func_2_t2317969963 * L_0 = (Func_2_t2317969963 *)__this->get__keySelector_0();
		RuntimeObject * L_1 = ___element0;
		NullCheck((Func_2_t2317969963 *)L_0);
		int32_t L_2 = ((  int32_t (*) (Func_2_t2317969963 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Func_2_t2317969963 *)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (int32_t)L_2;
		bool L_3 = (bool)__this->get__descending_2();
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		RuntimeObject* L_4 = (RuntimeObject*)__this->get__comparer_1();
		int32_t L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get__lastKey_3();
		NullCheck((RuntimeObject*)L_4);
		int32_t L_7 = InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Int32>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)L_4, (int32_t)L_5, (int32_t)L_6);
		G_B3_0 = L_7;
		goto IL_003b;
	}

IL_0029:
	{
		RuntimeObject* L_8 = (RuntimeObject*)__this->get__comparer_1();
		int32_t L_9 = (int32_t)__this->get__lastKey_3();
		int32_t L_10 = V_0;
		NullCheck((RuntimeObject*)L_8);
		int32_t L_11 = InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Int32>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)L_8, (int32_t)L_9, (int32_t)L_10);
		G_B3_0 = L_11;
	}

IL_003b:
	{
		V_1 = (int32_t)G_B3_0;
		bool L_12 = ___cacheLower1;
		int32_t L_13 = V_1;
		if ((!(((uint32_t)L_12) == ((uint32_t)((((int32_t)L_13) < ((int32_t)0))? 1 : 0)))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_14 = V_0;
		__this->set__lastKey_3(L_14);
	}

IL_004a:
	{
		int32_t L_15 = V_1;
		return L_15;
	}
}
// System.Void System.Linq.CachingComparer`2<System.Object,System.Int32>::SetElement(TElement)
extern "C"  void CachingComparer_2_SetElement_m1265669563_gshared (CachingComparer_2_t2712248853 * __this, RuntimeObject * ___element0, const RuntimeMethod* method)
{
	{
		Func_2_t2317969963 * L_0 = (Func_2_t2317969963 *)__this->get__keySelector_0();
		RuntimeObject * L_1 = ___element0;
		NullCheck((Func_2_t2317969963 *)L_0);
		int32_t L_2 = ((  int32_t (*) (Func_2_t2317969963 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Func_2_t2317969963 *)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		__this->set__lastKey_3(L_2);
		return;
	}
}
// System.Void System.Linq.CachingComparer`2<System.Object,System.Object>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
extern "C"  void CachingComparer_2__ctor_m35813242_gshared (CachingComparer_2_t2841409264 * __this, Func_2_t2447130374 * ___keySelector0, RuntimeObject* ___comparer1, bool ___descending2, const RuntimeMethod* method)
{
	{
		NullCheck((CachingComparer_1_t1378817919 *)__this);
		((  void (*) (CachingComparer_1_t1378817919 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CachingComparer_1_t1378817919 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t2447130374 * L_0 = ___keySelector0;
		__this->set__keySelector_0(L_0);
		RuntimeObject* L_1 = ___comparer1;
		__this->set__comparer_1(L_1);
		bool L_2 = ___descending2;
		__this->set__descending_2(L_2);
		return;
	}
}
// System.Int32 System.Linq.CachingComparer`2<System.Object,System.Object>::Compare(TElement,System.Boolean)
extern "C"  int32_t CachingComparer_2_Compare_m464691470_gshared (CachingComparer_2_t2841409264 * __this, RuntimeObject * ___element0, bool ___cacheLower1, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		Func_2_t2447130374 * L_0 = (Func_2_t2447130374 *)__this->get__keySelector_0();
		RuntimeObject * L_1 = ___element0;
		NullCheck((Func_2_t2447130374 *)L_0);
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Func_2_t2447130374 *)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (RuntimeObject *)L_2;
		bool L_3 = (bool)__this->get__descending_2();
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		RuntimeObject* L_4 = (RuntimeObject*)__this->get__comparer_1();
		RuntimeObject * L_5 = V_0;
		RuntimeObject * L_6 = (RuntimeObject *)__this->get__lastKey_3();
		NullCheck((RuntimeObject*)L_4);
		int32_t L_7 = InterfaceFuncInvoker2< int32_t, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)L_4, (RuntimeObject *)L_5, (RuntimeObject *)L_6);
		G_B3_0 = L_7;
		goto IL_003b;
	}

IL_0029:
	{
		RuntimeObject* L_8 = (RuntimeObject*)__this->get__comparer_1();
		RuntimeObject * L_9 = (RuntimeObject *)__this->get__lastKey_3();
		RuntimeObject * L_10 = V_0;
		NullCheck((RuntimeObject*)L_8);
		int32_t L_11 = InterfaceFuncInvoker2< int32_t, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)L_8, (RuntimeObject *)L_9, (RuntimeObject *)L_10);
		G_B3_0 = L_11;
	}

IL_003b:
	{
		V_1 = (int32_t)G_B3_0;
		bool L_12 = ___cacheLower1;
		int32_t L_13 = V_1;
		if ((!(((uint32_t)L_12) == ((uint32_t)((((int32_t)L_13) < ((int32_t)0))? 1 : 0)))))
		{
			goto IL_004a;
		}
	}
	{
		RuntimeObject * L_14 = V_0;
		__this->set__lastKey_3(L_14);
	}

IL_004a:
	{
		int32_t L_15 = V_1;
		return L_15;
	}
}
// System.Void System.Linq.CachingComparer`2<System.Object,System.Object>::SetElement(TElement)
extern "C"  void CachingComparer_2_SetElement_m4108298066_gshared (CachingComparer_2_t2841409264 * __this, RuntimeObject * ___element0, const RuntimeMethod* method)
{
	{
		Func_2_t2447130374 * L_0 = (Func_2_t2447130374 *)__this->get__keySelector_0();
		RuntimeObject * L_1 = ___element0;
		NullCheck((Func_2_t2447130374 *)L_0);
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Func_2_t2447130374 *)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		__this->set__lastKey_3(L_2);
		return;
	}
}
// System.Void System.Linq.CachingComparer`2<UnityEngine.Vector2,System.Single>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
extern "C"  void CachingComparer_2__ctor_m1285312215_gshared (CachingComparer_2_t4066783831 * __this, Func_2_t3672504941 * ___keySelector0, RuntimeObject* ___comparer1, bool ___descending2, const RuntimeMethod* method)
{
	{
		NullCheck((CachingComparer_1_t454941278 *)__this);
		((  void (*) (CachingComparer_1_t454941278 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CachingComparer_1_t454941278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t3672504941 * L_0 = ___keySelector0;
		__this->set__keySelector_0(L_0);
		RuntimeObject* L_1 = ___comparer1;
		__this->set__comparer_1(L_1);
		bool L_2 = ___descending2;
		__this->set__descending_2(L_2);
		return;
	}
}
// System.Int32 System.Linq.CachingComparer`2<UnityEngine.Vector2,System.Single>::Compare(TElement,System.Boolean)
extern "C"  int32_t CachingComparer_2_Compare_m4038830749_gshared (CachingComparer_2_t4066783831 * __this, Vector2_t2156229523  ___element0, bool ___cacheLower1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		Func_2_t3672504941 * L_0 = (Func_2_t3672504941 *)__this->get__keySelector_0();
		Vector2_t2156229523  L_1 = ___element0;
		NullCheck((Func_2_t3672504941 *)L_0);
		float L_2 = ((  float (*) (Func_2_t3672504941 *, Vector2_t2156229523 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Func_2_t3672504941 *)L_0, (Vector2_t2156229523 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (float)L_2;
		bool L_3 = (bool)__this->get__descending_2();
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		RuntimeObject* L_4 = (RuntimeObject*)__this->get__comparer_1();
		float L_5 = V_0;
		float L_6 = (float)__this->get__lastKey_3();
		NullCheck((RuntimeObject*)L_4);
		int32_t L_7 = InterfaceFuncInvoker2< int32_t, float, float >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Single>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)L_4, (float)L_5, (float)L_6);
		G_B3_0 = L_7;
		goto IL_003b;
	}

IL_0029:
	{
		RuntimeObject* L_8 = (RuntimeObject*)__this->get__comparer_1();
		float L_9 = (float)__this->get__lastKey_3();
		float L_10 = V_0;
		NullCheck((RuntimeObject*)L_8);
		int32_t L_11 = InterfaceFuncInvoker2< int32_t, float, float >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Single>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)L_8, (float)L_9, (float)L_10);
		G_B3_0 = L_11;
	}

IL_003b:
	{
		V_1 = (int32_t)G_B3_0;
		bool L_12 = ___cacheLower1;
		int32_t L_13 = V_1;
		if ((!(((uint32_t)L_12) == ((uint32_t)((((int32_t)L_13) < ((int32_t)0))? 1 : 0)))))
		{
			goto IL_004a;
		}
	}
	{
		float L_14 = V_0;
		__this->set__lastKey_3(L_14);
	}

IL_004a:
	{
		int32_t L_15 = V_1;
		return L_15;
	}
}
// System.Void System.Linq.CachingComparer`2<UnityEngine.Vector2,System.Single>::SetElement(TElement)
extern "C"  void CachingComparer_2_SetElement_m323424526_gshared (CachingComparer_2_t4066783831 * __this, Vector2_t2156229523  ___element0, const RuntimeMethod* method)
{
	{
		Func_2_t3672504941 * L_0 = (Func_2_t3672504941 *)__this->get__keySelector_0();
		Vector2_t2156229523  L_1 = ___element0;
		NullCheck((Func_2_t3672504941 *)L_0);
		float L_2 = ((  float (*) (Func_2_t3672504941 *, Vector2_t2156229523 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Func_2_t3672504941 *)L_0, (Vector2_t2156229523 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		__this->set__lastKey_3(L_2);
		return;
	}
}
// System.Void System.Linq.CachingComparer`2<UnityEngine.Vector3,System.Single>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
extern "C"  void CachingComparer_2__ctor_m2485067193_gshared (CachingComparer_2_t33381310 * __this, Func_2_t3934069716 * ___keySelector0, RuntimeObject* ___comparer1, bool ___descending2, const RuntimeMethod* method)
{
	{
		NullCheck((CachingComparer_1_t2021025219 *)__this);
		((  void (*) (CachingComparer_1_t2021025219 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CachingComparer_1_t2021025219 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t3934069716 * L_0 = ___keySelector0;
		__this->set__keySelector_0(L_0);
		RuntimeObject* L_1 = ___comparer1;
		__this->set__comparer_1(L_1);
		bool L_2 = ___descending2;
		__this->set__descending_2(L_2);
		return;
	}
}
// System.Int32 System.Linq.CachingComparer`2<UnityEngine.Vector3,System.Single>::Compare(TElement,System.Boolean)
extern "C"  int32_t CachingComparer_2_Compare_m2373354029_gshared (CachingComparer_2_t33381310 * __this, Vector3_t3722313464  ___element0, bool ___cacheLower1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		Func_2_t3934069716 * L_0 = (Func_2_t3934069716 *)__this->get__keySelector_0();
		Vector3_t3722313464  L_1 = ___element0;
		NullCheck((Func_2_t3934069716 *)L_0);
		float L_2 = ((  float (*) (Func_2_t3934069716 *, Vector3_t3722313464 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Func_2_t3934069716 *)L_0, (Vector3_t3722313464 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (float)L_2;
		bool L_3 = (bool)__this->get__descending_2();
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		RuntimeObject* L_4 = (RuntimeObject*)__this->get__comparer_1();
		float L_5 = V_0;
		float L_6 = (float)__this->get__lastKey_3();
		NullCheck((RuntimeObject*)L_4);
		int32_t L_7 = InterfaceFuncInvoker2< int32_t, float, float >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Single>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)L_4, (float)L_5, (float)L_6);
		G_B3_0 = L_7;
		goto IL_003b;
	}

IL_0029:
	{
		RuntimeObject* L_8 = (RuntimeObject*)__this->get__comparer_1();
		float L_9 = (float)__this->get__lastKey_3();
		float L_10 = V_0;
		NullCheck((RuntimeObject*)L_8);
		int32_t L_11 = InterfaceFuncInvoker2< int32_t, float, float >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Single>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)L_8, (float)L_9, (float)L_10);
		G_B3_0 = L_11;
	}

IL_003b:
	{
		V_1 = (int32_t)G_B3_0;
		bool L_12 = ___cacheLower1;
		int32_t L_13 = V_1;
		if ((!(((uint32_t)L_12) == ((uint32_t)((((int32_t)L_13) < ((int32_t)0))? 1 : 0)))))
		{
			goto IL_004a;
		}
	}
	{
		float L_14 = V_0;
		__this->set__lastKey_3(L_14);
	}

IL_004a:
	{
		int32_t L_15 = V_1;
		return L_15;
	}
}
// System.Void System.Linq.CachingComparer`2<UnityEngine.Vector3,System.Single>::SetElement(TElement)
extern "C"  void CachingComparer_2_SetElement_m3282216209_gshared (CachingComparer_2_t33381310 * __this, Vector3_t3722313464  ___element0, const RuntimeMethod* method)
{
	{
		Func_2_t3934069716 * L_0 = (Func_2_t3934069716 *)__this->get__keySelector_0();
		Vector3_t3722313464  L_1 = ___element0;
		NullCheck((Func_2_t3934069716 *)L_0);
		float L_2 = ((  float (*) (Func_2_t3934069716 *, Vector3_t3722313464 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Func_2_t3934069716 *)L_0, (Vector3_t3722313464 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		__this->set__lastKey_3(L_2);
		return;
	}
}
// System.Void System.Linq.CachingComparerWithChild`2<System.Object,System.Int32>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.CachingComparer`1<TElement>)
extern "C"  void CachingComparerWithChild_2__ctor_m2362989616_gshared (CachingComparerWithChild_2_t2010013078 * __this, Func_2_t2317969963 * ___keySelector0, RuntimeObject* ___comparer1, bool ___descending2, CachingComparer_1_t1378817919 * ___child3, const RuntimeMethod* method)
{
	{
		Func_2_t2317969963 * L_0 = ___keySelector0;
		RuntimeObject* L_1 = ___comparer1;
		bool L_2 = ___descending2;
		NullCheck((CachingComparer_2_t2712248853 *)__this);
		((  void (*) (CachingComparer_2_t2712248853 *, Func_2_t2317969963 *, RuntimeObject*, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CachingComparer_2_t2712248853 *)__this, (Func_2_t2317969963 *)L_0, (RuntimeObject*)L_1, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		CachingComparer_1_t1378817919 * L_3 = ___child3;
		__this->set__child_4(L_3);
		return;
	}
}
// System.Int32 System.Linq.CachingComparerWithChild`2<System.Object,System.Int32>::Compare(TElement,System.Boolean)
extern "C"  int32_t CachingComparerWithChild_2_Compare_m3819745285_gshared (CachingComparerWithChild_2_t2010013078 * __this, RuntimeObject * ___element0, bool ___cacheLower1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		Func_2_t2317969963 * L_0 = (Func_2_t2317969963 *)((CachingComparer_2_t2712248853 *)__this)->get__keySelector_0();
		RuntimeObject * L_1 = ___element0;
		NullCheck((Func_2_t2317969963 *)L_0);
		int32_t L_2 = ((  int32_t (*) (Func_2_t2317969963 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Func_2_t2317969963 *)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (int32_t)L_2;
		bool L_3 = (bool)((CachingComparer_2_t2712248853 *)__this)->get__descending_2();
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		RuntimeObject* L_4 = (RuntimeObject*)((CachingComparer_2_t2712248853 *)__this)->get__comparer_1();
		int32_t L_5 = V_0;
		int32_t L_6 = (int32_t)((CachingComparer_2_t2712248853 *)__this)->get__lastKey_3();
		NullCheck((RuntimeObject*)L_4);
		int32_t L_7 = InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Int32>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_4, (int32_t)L_5, (int32_t)L_6);
		G_B3_0 = L_7;
		goto IL_003b;
	}

IL_0029:
	{
		RuntimeObject* L_8 = (RuntimeObject*)((CachingComparer_2_t2712248853 *)__this)->get__comparer_1();
		int32_t L_9 = (int32_t)((CachingComparer_2_t2712248853 *)__this)->get__lastKey_3();
		int32_t L_10 = V_0;
		NullCheck((RuntimeObject*)L_8);
		int32_t L_11 = InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Int32>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_8, (int32_t)L_9, (int32_t)L_10);
		G_B3_0 = L_11;
	}

IL_003b:
	{
		V_1 = (int32_t)G_B3_0;
		int32_t L_12 = V_1;
		if (L_12)
		{
			goto IL_004d;
		}
	}
	{
		CachingComparer_1_t1378817919 * L_13 = (CachingComparer_1_t1378817919 *)__this->get__child_4();
		RuntimeObject * L_14 = ___element0;
		bool L_15 = ___cacheLower1;
		NullCheck((CachingComparer_1_t1378817919 *)L_13);
		int32_t L_16 = VirtFuncInvoker2< int32_t, RuntimeObject *, bool >::Invoke(4 /* System.Int32 System.Linq.CachingComparer`1<System.Object>::Compare(TElement,System.Boolean) */, (CachingComparer_1_t1378817919 *)L_13, (RuntimeObject *)L_14, (bool)L_15);
		return L_16;
	}

IL_004d:
	{
		bool L_17 = ___cacheLower1;
		int32_t L_18 = V_1;
		if ((!(((uint32_t)L_17) == ((uint32_t)((((int32_t)L_18) < ((int32_t)0))? 1 : 0)))))
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_19 = V_0;
		((CachingComparer_2_t2712248853 *)__this)->set__lastKey_3(L_19);
		CachingComparer_1_t1378817919 * L_20 = (CachingComparer_1_t1378817919 *)__this->get__child_4();
		RuntimeObject * L_21 = ___element0;
		NullCheck((CachingComparer_1_t1378817919 *)L_20);
		VirtActionInvoker1< RuntimeObject * >::Invoke(5 /* System.Void System.Linq.CachingComparer`1<System.Object>::SetElement(TElement) */, (CachingComparer_1_t1378817919 *)L_20, (RuntimeObject *)L_21);
	}

IL_0067:
	{
		int32_t L_22 = V_1;
		return L_22;
	}
}
// System.Void System.Linq.CachingComparerWithChild`2<System.Object,System.Int32>::SetElement(TElement)
extern "C"  void CachingComparerWithChild_2_SetElement_m3123677967_gshared (CachingComparerWithChild_2_t2010013078 * __this, RuntimeObject * ___element0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___element0;
		NullCheck((CachingComparer_2_t2712248853 *)__this);
		((  void (*) (CachingComparer_2_t2712248853 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((CachingComparer_2_t2712248853 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		CachingComparer_1_t1378817919 * L_1 = (CachingComparer_1_t1378817919 *)__this->get__child_4();
		RuntimeObject * L_2 = ___element0;
		NullCheck((CachingComparer_1_t1378817919 *)L_1);
		VirtActionInvoker1< RuntimeObject * >::Invoke(5 /* System.Void System.Linq.CachingComparer`1<System.Object>::SetElement(TElement) */, (CachingComparer_1_t1378817919 *)L_1, (RuntimeObject *)L_2);
		return;
	}
}
// System.Void System.Linq.CachingComparerWithChild`2<System.Object,System.Object>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.CachingComparer`1<TElement>)
extern "C"  void CachingComparerWithChild_2__ctor_m2479719590_gshared (CachingComparerWithChild_2_t2139173489 * __this, Func_2_t2447130374 * ___keySelector0, RuntimeObject* ___comparer1, bool ___descending2, CachingComparer_1_t1378817919 * ___child3, const RuntimeMethod* method)
{
	{
		Func_2_t2447130374 * L_0 = ___keySelector0;
		RuntimeObject* L_1 = ___comparer1;
		bool L_2 = ___descending2;
		NullCheck((CachingComparer_2_t2841409264 *)__this);
		((  void (*) (CachingComparer_2_t2841409264 *, Func_2_t2447130374 *, RuntimeObject*, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CachingComparer_2_t2841409264 *)__this, (Func_2_t2447130374 *)L_0, (RuntimeObject*)L_1, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		CachingComparer_1_t1378817919 * L_3 = ___child3;
		__this->set__child_4(L_3);
		return;
	}
}
// System.Int32 System.Linq.CachingComparerWithChild`2<System.Object,System.Object>::Compare(TElement,System.Boolean)
extern "C"  int32_t CachingComparerWithChild_2_Compare_m741207413_gshared (CachingComparerWithChild_2_t2139173489 * __this, RuntimeObject * ___element0, bool ___cacheLower1, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		Func_2_t2447130374 * L_0 = (Func_2_t2447130374 *)((CachingComparer_2_t2841409264 *)__this)->get__keySelector_0();
		RuntimeObject * L_1 = ___element0;
		NullCheck((Func_2_t2447130374 *)L_0);
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Func_2_t2447130374 *)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (RuntimeObject *)L_2;
		bool L_3 = (bool)((CachingComparer_2_t2841409264 *)__this)->get__descending_2();
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		RuntimeObject* L_4 = (RuntimeObject*)((CachingComparer_2_t2841409264 *)__this)->get__comparer_1();
		RuntimeObject * L_5 = V_0;
		RuntimeObject * L_6 = (RuntimeObject *)((CachingComparer_2_t2841409264 *)__this)->get__lastKey_3();
		NullCheck((RuntimeObject*)L_4);
		int32_t L_7 = InterfaceFuncInvoker2< int32_t, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_4, (RuntimeObject *)L_5, (RuntimeObject *)L_6);
		G_B3_0 = L_7;
		goto IL_003b;
	}

IL_0029:
	{
		RuntimeObject* L_8 = (RuntimeObject*)((CachingComparer_2_t2841409264 *)__this)->get__comparer_1();
		RuntimeObject * L_9 = (RuntimeObject *)((CachingComparer_2_t2841409264 *)__this)->get__lastKey_3();
		RuntimeObject * L_10 = V_0;
		NullCheck((RuntimeObject*)L_8);
		int32_t L_11 = InterfaceFuncInvoker2< int32_t, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_8, (RuntimeObject *)L_9, (RuntimeObject *)L_10);
		G_B3_0 = L_11;
	}

IL_003b:
	{
		V_1 = (int32_t)G_B3_0;
		int32_t L_12 = V_1;
		if (L_12)
		{
			goto IL_004d;
		}
	}
	{
		CachingComparer_1_t1378817919 * L_13 = (CachingComparer_1_t1378817919 *)__this->get__child_4();
		RuntimeObject * L_14 = ___element0;
		bool L_15 = ___cacheLower1;
		NullCheck((CachingComparer_1_t1378817919 *)L_13);
		int32_t L_16 = VirtFuncInvoker2< int32_t, RuntimeObject *, bool >::Invoke(4 /* System.Int32 System.Linq.CachingComparer`1<System.Object>::Compare(TElement,System.Boolean) */, (CachingComparer_1_t1378817919 *)L_13, (RuntimeObject *)L_14, (bool)L_15);
		return L_16;
	}

IL_004d:
	{
		bool L_17 = ___cacheLower1;
		int32_t L_18 = V_1;
		if ((!(((uint32_t)L_17) == ((uint32_t)((((int32_t)L_18) < ((int32_t)0))? 1 : 0)))))
		{
			goto IL_0067;
		}
	}
	{
		RuntimeObject * L_19 = V_0;
		((CachingComparer_2_t2841409264 *)__this)->set__lastKey_3(L_19);
		CachingComparer_1_t1378817919 * L_20 = (CachingComparer_1_t1378817919 *)__this->get__child_4();
		RuntimeObject * L_21 = ___element0;
		NullCheck((CachingComparer_1_t1378817919 *)L_20);
		VirtActionInvoker1< RuntimeObject * >::Invoke(5 /* System.Void System.Linq.CachingComparer`1<System.Object>::SetElement(TElement) */, (CachingComparer_1_t1378817919 *)L_20, (RuntimeObject *)L_21);
	}

IL_0067:
	{
		int32_t L_22 = V_1;
		return L_22;
	}
}
// System.Void System.Linq.CachingComparerWithChild`2<System.Object,System.Object>::SetElement(TElement)
extern "C"  void CachingComparerWithChild_2_SetElement_m3298530176_gshared (CachingComparerWithChild_2_t2139173489 * __this, RuntimeObject * ___element0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___element0;
		NullCheck((CachingComparer_2_t2841409264 *)__this);
		((  void (*) (CachingComparer_2_t2841409264 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((CachingComparer_2_t2841409264 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		CachingComparer_1_t1378817919 * L_1 = (CachingComparer_1_t1378817919 *)__this->get__child_4();
		RuntimeObject * L_2 = ___element0;
		NullCheck((CachingComparer_1_t1378817919 *)L_1);
		VirtActionInvoker1< RuntimeObject * >::Invoke(5 /* System.Void System.Linq.CachingComparer`1<System.Object>::SetElement(TElement) */, (CachingComparer_1_t1378817919 *)L_1, (RuntimeObject *)L_2);
		return;
	}
}
// System.Void System.Linq.CachingComparerWithChild`2<UnityEngine.Vector2,System.Single>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.CachingComparer`1<TElement>)
extern "C"  void CachingComparerWithChild_2__ctor_m714499546_gshared (CachingComparerWithChild_2_t3364548056 * __this, Func_2_t3672504941 * ___keySelector0, RuntimeObject* ___comparer1, bool ___descending2, CachingComparer_1_t454941278 * ___child3, const RuntimeMethod* method)
{
	{
		Func_2_t3672504941 * L_0 = ___keySelector0;
		RuntimeObject* L_1 = ___comparer1;
		bool L_2 = ___descending2;
		NullCheck((CachingComparer_2_t4066783831 *)__this);
		((  void (*) (CachingComparer_2_t4066783831 *, Func_2_t3672504941 *, RuntimeObject*, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CachingComparer_2_t4066783831 *)__this, (Func_2_t3672504941 *)L_0, (RuntimeObject*)L_1, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		CachingComparer_1_t454941278 * L_3 = ___child3;
		__this->set__child_4(L_3);
		return;
	}
}
// System.Int32 System.Linq.CachingComparerWithChild`2<UnityEngine.Vector2,System.Single>::Compare(TElement,System.Boolean)
extern "C"  int32_t CachingComparerWithChild_2_Compare_m1616620100_gshared (CachingComparerWithChild_2_t3364548056 * __this, Vector2_t2156229523  ___element0, bool ___cacheLower1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		Func_2_t3672504941 * L_0 = (Func_2_t3672504941 *)((CachingComparer_2_t4066783831 *)__this)->get__keySelector_0();
		Vector2_t2156229523  L_1 = ___element0;
		NullCheck((Func_2_t3672504941 *)L_0);
		float L_2 = ((  float (*) (Func_2_t3672504941 *, Vector2_t2156229523 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Func_2_t3672504941 *)L_0, (Vector2_t2156229523 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (float)L_2;
		bool L_3 = (bool)((CachingComparer_2_t4066783831 *)__this)->get__descending_2();
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		RuntimeObject* L_4 = (RuntimeObject*)((CachingComparer_2_t4066783831 *)__this)->get__comparer_1();
		float L_5 = V_0;
		float L_6 = (float)((CachingComparer_2_t4066783831 *)__this)->get__lastKey_3();
		NullCheck((RuntimeObject*)L_4);
		int32_t L_7 = InterfaceFuncInvoker2< int32_t, float, float >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Single>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_4, (float)L_5, (float)L_6);
		G_B3_0 = L_7;
		goto IL_003b;
	}

IL_0029:
	{
		RuntimeObject* L_8 = (RuntimeObject*)((CachingComparer_2_t4066783831 *)__this)->get__comparer_1();
		float L_9 = (float)((CachingComparer_2_t4066783831 *)__this)->get__lastKey_3();
		float L_10 = V_0;
		NullCheck((RuntimeObject*)L_8);
		int32_t L_11 = InterfaceFuncInvoker2< int32_t, float, float >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Single>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_8, (float)L_9, (float)L_10);
		G_B3_0 = L_11;
	}

IL_003b:
	{
		V_1 = (int32_t)G_B3_0;
		int32_t L_12 = V_1;
		if (L_12)
		{
			goto IL_004d;
		}
	}
	{
		CachingComparer_1_t454941278 * L_13 = (CachingComparer_1_t454941278 *)__this->get__child_4();
		Vector2_t2156229523  L_14 = ___element0;
		bool L_15 = ___cacheLower1;
		NullCheck((CachingComparer_1_t454941278 *)L_13);
		int32_t L_16 = VirtFuncInvoker2< int32_t, Vector2_t2156229523 , bool >::Invoke(4 /* System.Int32 System.Linq.CachingComparer`1<UnityEngine.Vector2>::Compare(TElement,System.Boolean) */, (CachingComparer_1_t454941278 *)L_13, (Vector2_t2156229523 )L_14, (bool)L_15);
		return L_16;
	}

IL_004d:
	{
		bool L_17 = ___cacheLower1;
		int32_t L_18 = V_1;
		if ((!(((uint32_t)L_17) == ((uint32_t)((((int32_t)L_18) < ((int32_t)0))? 1 : 0)))))
		{
			goto IL_0067;
		}
	}
	{
		float L_19 = V_0;
		((CachingComparer_2_t4066783831 *)__this)->set__lastKey_3(L_19);
		CachingComparer_1_t454941278 * L_20 = (CachingComparer_1_t454941278 *)__this->get__child_4();
		Vector2_t2156229523  L_21 = ___element0;
		NullCheck((CachingComparer_1_t454941278 *)L_20);
		VirtActionInvoker1< Vector2_t2156229523  >::Invoke(5 /* System.Void System.Linq.CachingComparer`1<UnityEngine.Vector2>::SetElement(TElement) */, (CachingComparer_1_t454941278 *)L_20, (Vector2_t2156229523 )L_21);
	}

IL_0067:
	{
		int32_t L_22 = V_1;
		return L_22;
	}
}
// System.Void System.Linq.CachingComparerWithChild`2<UnityEngine.Vector2,System.Single>::SetElement(TElement)
extern "C"  void CachingComparerWithChild_2_SetElement_m3512286775_gshared (CachingComparerWithChild_2_t3364548056 * __this, Vector2_t2156229523  ___element0, const RuntimeMethod* method)
{
	{
		Vector2_t2156229523  L_0 = ___element0;
		NullCheck((CachingComparer_2_t4066783831 *)__this);
		((  void (*) (CachingComparer_2_t4066783831 *, Vector2_t2156229523 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((CachingComparer_2_t4066783831 *)__this, (Vector2_t2156229523 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		CachingComparer_1_t454941278 * L_1 = (CachingComparer_1_t454941278 *)__this->get__child_4();
		Vector2_t2156229523  L_2 = ___element0;
		NullCheck((CachingComparer_1_t454941278 *)L_1);
		VirtActionInvoker1< Vector2_t2156229523  >::Invoke(5 /* System.Void System.Linq.CachingComparer`1<UnityEngine.Vector2>::SetElement(TElement) */, (CachingComparer_1_t454941278 *)L_1, (Vector2_t2156229523 )L_2);
		return;
	}
}
// System.Void System.Linq.CachingComparerWithChild`2<UnityEngine.Vector3,System.Single>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.CachingComparer`1<TElement>)
extern "C"  void CachingComparerWithChild_2__ctor_m2854784206_gshared (CachingComparerWithChild_2_t3626112831 * __this, Func_2_t3934069716 * ___keySelector0, RuntimeObject* ___comparer1, bool ___descending2, CachingComparer_1_t2021025219 * ___child3, const RuntimeMethod* method)
{
	{
		Func_2_t3934069716 * L_0 = ___keySelector0;
		RuntimeObject* L_1 = ___comparer1;
		bool L_2 = ___descending2;
		NullCheck((CachingComparer_2_t33381310 *)__this);
		((  void (*) (CachingComparer_2_t33381310 *, Func_2_t3934069716 *, RuntimeObject*, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CachingComparer_2_t33381310 *)__this, (Func_2_t3934069716 *)L_0, (RuntimeObject*)L_1, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		CachingComparer_1_t2021025219 * L_3 = ___child3;
		__this->set__child_4(L_3);
		return;
	}
}
// System.Int32 System.Linq.CachingComparerWithChild`2<UnityEngine.Vector3,System.Single>::Compare(TElement,System.Boolean)
extern "C"  int32_t CachingComparerWithChild_2_Compare_m719019543_gshared (CachingComparerWithChild_2_t3626112831 * __this, Vector3_t3722313464  ___element0, bool ___cacheLower1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		Func_2_t3934069716 * L_0 = (Func_2_t3934069716 *)((CachingComparer_2_t33381310 *)__this)->get__keySelector_0();
		Vector3_t3722313464  L_1 = ___element0;
		NullCheck((Func_2_t3934069716 *)L_0);
		float L_2 = ((  float (*) (Func_2_t3934069716 *, Vector3_t3722313464 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Func_2_t3934069716 *)L_0, (Vector3_t3722313464 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (float)L_2;
		bool L_3 = (bool)((CachingComparer_2_t33381310 *)__this)->get__descending_2();
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		RuntimeObject* L_4 = (RuntimeObject*)((CachingComparer_2_t33381310 *)__this)->get__comparer_1();
		float L_5 = V_0;
		float L_6 = (float)((CachingComparer_2_t33381310 *)__this)->get__lastKey_3();
		NullCheck((RuntimeObject*)L_4);
		int32_t L_7 = InterfaceFuncInvoker2< int32_t, float, float >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Single>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_4, (float)L_5, (float)L_6);
		G_B3_0 = L_7;
		goto IL_003b;
	}

IL_0029:
	{
		RuntimeObject* L_8 = (RuntimeObject*)((CachingComparer_2_t33381310 *)__this)->get__comparer_1();
		float L_9 = (float)((CachingComparer_2_t33381310 *)__this)->get__lastKey_3();
		float L_10 = V_0;
		NullCheck((RuntimeObject*)L_8);
		int32_t L_11 = InterfaceFuncInvoker2< int32_t, float, float >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Single>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_8, (float)L_9, (float)L_10);
		G_B3_0 = L_11;
	}

IL_003b:
	{
		V_1 = (int32_t)G_B3_0;
		int32_t L_12 = V_1;
		if (L_12)
		{
			goto IL_004d;
		}
	}
	{
		CachingComparer_1_t2021025219 * L_13 = (CachingComparer_1_t2021025219 *)__this->get__child_4();
		Vector3_t3722313464  L_14 = ___element0;
		bool L_15 = ___cacheLower1;
		NullCheck((CachingComparer_1_t2021025219 *)L_13);
		int32_t L_16 = VirtFuncInvoker2< int32_t, Vector3_t3722313464 , bool >::Invoke(4 /* System.Int32 System.Linq.CachingComparer`1<UnityEngine.Vector3>::Compare(TElement,System.Boolean) */, (CachingComparer_1_t2021025219 *)L_13, (Vector3_t3722313464 )L_14, (bool)L_15);
		return L_16;
	}

IL_004d:
	{
		bool L_17 = ___cacheLower1;
		int32_t L_18 = V_1;
		if ((!(((uint32_t)L_17) == ((uint32_t)((((int32_t)L_18) < ((int32_t)0))? 1 : 0)))))
		{
			goto IL_0067;
		}
	}
	{
		float L_19 = V_0;
		((CachingComparer_2_t33381310 *)__this)->set__lastKey_3(L_19);
		CachingComparer_1_t2021025219 * L_20 = (CachingComparer_1_t2021025219 *)__this->get__child_4();
		Vector3_t3722313464  L_21 = ___element0;
		NullCheck((CachingComparer_1_t2021025219 *)L_20);
		VirtActionInvoker1< Vector3_t3722313464  >::Invoke(5 /* System.Void System.Linq.CachingComparer`1<UnityEngine.Vector3>::SetElement(TElement) */, (CachingComparer_1_t2021025219 *)L_20, (Vector3_t3722313464 )L_21);
	}

IL_0067:
	{
		int32_t L_22 = V_1;
		return L_22;
	}
}
// System.Void System.Linq.CachingComparerWithChild`2<UnityEngine.Vector3,System.Single>::SetElement(TElement)
extern "C"  void CachingComparerWithChild_2_SetElement_m3180861878_gshared (CachingComparerWithChild_2_t3626112831 * __this, Vector3_t3722313464  ___element0, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0 = ___element0;
		NullCheck((CachingComparer_2_t33381310 *)__this);
		((  void (*) (CachingComparer_2_t33381310 *, Vector3_t3722313464 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((CachingComparer_2_t33381310 *)__this, (Vector3_t3722313464 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		CachingComparer_1_t2021025219 * L_1 = (CachingComparer_1_t2021025219 *)__this->get__child_4();
		Vector3_t3722313464  L_2 = ___element0;
		NullCheck((CachingComparer_1_t2021025219 *)L_1);
		VirtActionInvoker1< Vector3_t3722313464  >::Invoke(5 /* System.Void System.Linq.CachingComparer`1<UnityEngine.Vector3>::SetElement(TElement) */, (CachingComparer_1_t2021025219 *)L_1, (Vector3_t3722313464 )L_2);
		return;
	}
}
// System.Void System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void EmptyPartition_1__ctor_m275519022_gshared (EmptyPartition_1_t2163426353 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m297566312((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C"  RuntimeObject* EmptyPartition_1_GetEnumerator_m2365040334_gshared (EmptyPartition_1_t2163426353 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Collections.IEnumerator System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* EmptyPartition_1_System_Collections_IEnumerable_GetEnumerator_m3248602084_gshared (EmptyPartition_1_t2163426353 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool EmptyPartition_1_MoveNext_m2365371069_gshared (EmptyPartition_1_t2163426353 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// TElement System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t2530217319  EmptyPartition_1_get_Current_m456712_gshared (EmptyPartition_1_t2163426353 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmptyPartition_1_get_Current_m456712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2530217319  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_0));
		KeyValuePair_2_t2530217319  L_0 = V_0;
		return L_0;
	}
}
// System.Object System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * EmptyPartition_1_System_Collections_IEnumerator_get_Current_m150476704_gshared (EmptyPartition_1_t2163426353 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmptyPartition_1_System_Collections_IEnumerator_get_Current_m150476704_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2530217319  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_0));
		KeyValuePair_2_t2530217319  L_0 = V_0;
		KeyValuePair_2_t2530217319  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Void System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void EmptyPartition_1_System_Collections_IEnumerator_Reset_m944359954_gshared (EmptyPartition_1_t2163426353 * __this, const RuntimeMethod* method)
{
	{
		Exception_t1436737249 * L_0 = Error_NotSupported_m1072967690(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.IDisposable.Dispose()
extern "C"  void EmptyPartition_1_System_IDisposable_Dispose_m3492674962_gshared (EmptyPartition_1_t2163426353 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// TElement System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetElementAt(System.Int32,System.Boolean&)
extern "C"  KeyValuePair_2_t2530217319  EmptyPartition_1_TryGetElementAt_m2497710296_gshared (EmptyPartition_1_t2163426353 * __this, int32_t ___index0, bool* ___found1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmptyPartition_1_TryGetElementAt_m2497710296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2530217319  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool* L_0 = ___found1;
		*((int8_t*)(L_0)) = (int8_t)0;
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_0));
		KeyValuePair_2_t2530217319  L_1 = V_0;
		return L_1;
	}
}
// TElement System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetFirst(System.Boolean&)
extern "C"  KeyValuePair_2_t2530217319  EmptyPartition_1_TryGetFirst_m559482675_gshared (EmptyPartition_1_t2163426353 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmptyPartition_1_TryGetFirst_m559482675_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2530217319  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool* L_0 = ___found0;
		*((int8_t*)(L_0)) = (int8_t)0;
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_0));
		KeyValuePair_2_t2530217319  L_1 = V_0;
		return L_1;
	}
}
// TElement System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetLast(System.Boolean&)
extern "C"  KeyValuePair_2_t2530217319  EmptyPartition_1_TryGetLast_m1441299256_gshared (EmptyPartition_1_t2163426353 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmptyPartition_1_TryGetLast_m1441299256_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2530217319  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool* L_0 = ___found0;
		*((int8_t*)(L_0)) = (int8_t)0;
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_0));
		KeyValuePair_2_t2530217319  L_1 = V_0;
		return L_1;
	}
}
// TElement[] System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t118269214* EmptyPartition_1_ToArray_m595655386_gshared (EmptyPartition_1_t2163426353 * __this, const RuntimeMethod* method)
{
	{
		KeyValuePair_2U5BU5D_t118269214* L_0 = ((  KeyValuePair_2U5BU5D_t118269214* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.List`1<TElement> System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToList()
extern "C"  List_1_t4002292061 * EmptyPartition_1_ToList_m2227413023_gshared (EmptyPartition_1_t2163426353 * __this, const RuntimeMethod* method)
{
	{
		List_1_t4002292061 * L_0 = (List_1_t4002292061 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (List_1_t4002292061 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_0;
	}
}
// System.Int32 System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetCount(System.Boolean)
extern "C"  int32_t EmptyPartition_1_GetCount_m2087578991_gshared (EmptyPartition_1_t2163426353 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	{
		return 0;
	}
}
// System.Void System.Linq.EmptyPartition`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.cctor()
extern "C"  void EmptyPartition_1__cctor_m3326397427_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		EmptyPartition_1_t2163426353 * L_0 = (EmptyPartition_1_t2163426353 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (EmptyPartition_1_t2163426353 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EmptyPartition_1_t2163426353_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)))->set_Instance_0(L_0);
		return;
	}
}
// System.Void System.Linq.EmptyPartition`1<System.Object>::.ctor()
extern "C"  void EmptyPartition_1__ctor_m1324556942_gshared (EmptyPartition_1_t2713315198 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m297566312((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.EmptyPartition`1<System.Object>::GetEnumerator()
extern "C"  RuntimeObject* EmptyPartition_1_GetEnumerator_m4034385110_gshared (EmptyPartition_1_t2713315198 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Collections.IEnumerator System.Linq.EmptyPartition`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* EmptyPartition_1_System_Collections_IEnumerable_GetEnumerator_m1943512692_gshared (EmptyPartition_1_t2713315198 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Linq.EmptyPartition`1<System.Object>::MoveNext()
extern "C"  bool EmptyPartition_1_MoveNext_m3027126934_gshared (EmptyPartition_1_t2713315198 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// TElement System.Linq.EmptyPartition`1<System.Object>::get_Current()
extern "C"  RuntimeObject * EmptyPartition_1_get_Current_m4265207326_gshared (EmptyPartition_1_t2713315198 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmptyPartition_1_get_Current_m4265207326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_0));
		RuntimeObject * L_0 = V_0;
		return L_0;
	}
}
// System.Object System.Linq.EmptyPartition`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * EmptyPartition_1_System_Collections_IEnumerator_get_Current_m1601347863_gshared (EmptyPartition_1_t2713315198 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmptyPartition_1_System_Collections_IEnumerator_get_Current_m1601347863_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_0));
		RuntimeObject * L_0 = V_0;
		return L_0;
	}
}
// System.Void System.Linq.EmptyPartition`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void EmptyPartition_1_System_Collections_IEnumerator_Reset_m1113105900_gshared (EmptyPartition_1_t2713315198 * __this, const RuntimeMethod* method)
{
	{
		Exception_t1436737249 * L_0 = Error_NotSupported_m1072967690(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.EmptyPartition`1<System.Object>::System.IDisposable.Dispose()
extern "C"  void EmptyPartition_1_System_IDisposable_Dispose_m3117725665_gshared (EmptyPartition_1_t2713315198 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// TElement System.Linq.EmptyPartition`1<System.Object>::TryGetElementAt(System.Int32,System.Boolean&)
extern "C"  RuntimeObject * EmptyPartition_1_TryGetElementAt_m3940163582_gshared (EmptyPartition_1_t2713315198 * __this, int32_t ___index0, bool* ___found1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmptyPartition_1_TryGetElementAt_m3940163582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		bool* L_0 = ___found1;
		*((int8_t*)(L_0)) = (int8_t)0;
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_0));
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// TElement System.Linq.EmptyPartition`1<System.Object>::TryGetFirst(System.Boolean&)
extern "C"  RuntimeObject * EmptyPartition_1_TryGetFirst_m593080765_gshared (EmptyPartition_1_t2713315198 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmptyPartition_1_TryGetFirst_m593080765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		bool* L_0 = ___found0;
		*((int8_t*)(L_0)) = (int8_t)0;
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_0));
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// TElement System.Linq.EmptyPartition`1<System.Object>::TryGetLast(System.Boolean&)
extern "C"  RuntimeObject * EmptyPartition_1_TryGetLast_m2150537268_gshared (EmptyPartition_1_t2713315198 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmptyPartition_1_TryGetLast_m2150537268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		bool* L_0 = ___found0;
		*((int8_t*)(L_0)) = (int8_t)0;
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_0));
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// TElement[] System.Linq.EmptyPartition`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t2843939325* EmptyPartition_1_ToArray_m1773565891_gshared (EmptyPartition_1_t2713315198 * __this, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = ((  ObjectU5BU5D_t2843939325* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.List`1<TElement> System.Linq.EmptyPartition`1<System.Object>::ToList()
extern "C"  List_1_t257213610 * EmptyPartition_1_ToList_m2944878630_gshared (EmptyPartition_1_t2713315198 * __this, const RuntimeMethod* method)
{
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (List_1_t257213610 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_0;
	}
}
// System.Int32 System.Linq.EmptyPartition`1<System.Object>::GetCount(System.Boolean)
extern "C"  int32_t EmptyPartition_1_GetCount_m3325448135_gshared (EmptyPartition_1_t2713315198 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	{
		return 0;
	}
}
// System.Void System.Linq.EmptyPartition`1<System.Object>::.cctor()
extern "C"  void EmptyPartition_1__cctor_m1677634983_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		EmptyPartition_1_t2713315198 * L_0 = (EmptyPartition_1_t2713315198 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (EmptyPartition_1_t2713315198 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EmptyPartition_1_t2713315198_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)))->set_Instance_0(L_0);
		return;
	}
}
// System.Void System.Linq.EmptyPartition`1<UnityEngine.Ray>::.ctor()
extern "C"  void EmptyPartition_1__ctor_m3454558495_gshared (EmptyPartition_1_t3419060527 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m297566312((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.EmptyPartition`1<UnityEngine.Ray>::GetEnumerator()
extern "C"  RuntimeObject* EmptyPartition_1_GetEnumerator_m2841442048_gshared (EmptyPartition_1_t3419060527 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Collections.IEnumerator System.Linq.EmptyPartition`1<UnityEngine.Ray>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* EmptyPartition_1_System_Collections_IEnumerable_GetEnumerator_m3017943730_gshared (EmptyPartition_1_t3419060527 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Linq.EmptyPartition`1<UnityEngine.Ray>::MoveNext()
extern "C"  bool EmptyPartition_1_MoveNext_m3527948209_gshared (EmptyPartition_1_t3419060527 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// TElement System.Linq.EmptyPartition`1<UnityEngine.Ray>::get_Current()
extern "C"  Ray_t3785851493  EmptyPartition_1_get_Current_m559141384_gshared (EmptyPartition_1_t3419060527 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmptyPartition_1_get_Current_m559141384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t3785851493  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Ray_t3785851493_il2cpp_TypeInfo_var, (&V_0));
		Ray_t3785851493  L_0 = V_0;
		return L_0;
	}
}
// System.Object System.Linq.EmptyPartition`1<UnityEngine.Ray>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * EmptyPartition_1_System_Collections_IEnumerator_get_Current_m2051492582_gshared (EmptyPartition_1_t3419060527 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmptyPartition_1_System_Collections_IEnumerator_get_Current_m2051492582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t3785851493  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Ray_t3785851493_il2cpp_TypeInfo_var, (&V_0));
		Ray_t3785851493  L_0 = V_0;
		Ray_t3785851493  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Void System.Linq.EmptyPartition`1<UnityEngine.Ray>::System.Collections.IEnumerator.Reset()
extern "C"  void EmptyPartition_1_System_Collections_IEnumerator_Reset_m400612218_gshared (EmptyPartition_1_t3419060527 * __this, const RuntimeMethod* method)
{
	{
		Exception_t1436737249 * L_0 = Error_NotSupported_m1072967690(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.EmptyPartition`1<UnityEngine.Ray>::System.IDisposable.Dispose()
extern "C"  void EmptyPartition_1_System_IDisposable_Dispose_m3639569699_gshared (EmptyPartition_1_t3419060527 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// TElement System.Linq.EmptyPartition`1<UnityEngine.Ray>::TryGetElementAt(System.Int32,System.Boolean&)
extern "C"  Ray_t3785851493  EmptyPartition_1_TryGetElementAt_m3815154181_gshared (EmptyPartition_1_t3419060527 * __this, int32_t ___index0, bool* ___found1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmptyPartition_1_TryGetElementAt_m3815154181_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t3785851493  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool* L_0 = ___found1;
		*((int8_t*)(L_0)) = (int8_t)0;
		Initobj (Ray_t3785851493_il2cpp_TypeInfo_var, (&V_0));
		Ray_t3785851493  L_1 = V_0;
		return L_1;
	}
}
// TElement System.Linq.EmptyPartition`1<UnityEngine.Ray>::TryGetFirst(System.Boolean&)
extern "C"  Ray_t3785851493  EmptyPartition_1_TryGetFirst_m2649955484_gshared (EmptyPartition_1_t3419060527 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmptyPartition_1_TryGetFirst_m2649955484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t3785851493  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool* L_0 = ___found0;
		*((int8_t*)(L_0)) = (int8_t)0;
		Initobj (Ray_t3785851493_il2cpp_TypeInfo_var, (&V_0));
		Ray_t3785851493  L_1 = V_0;
		return L_1;
	}
}
// TElement System.Linq.EmptyPartition`1<UnityEngine.Ray>::TryGetLast(System.Boolean&)
extern "C"  Ray_t3785851493  EmptyPartition_1_TryGetLast_m3105323548_gshared (EmptyPartition_1_t3419060527 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmptyPartition_1_TryGetLast_m3105323548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t3785851493  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool* L_0 = ___found0;
		*((int8_t*)(L_0)) = (int8_t)0;
		Initobj (Ray_t3785851493_il2cpp_TypeInfo_var, (&V_0));
		Ray_t3785851493  L_1 = V_0;
		return L_1;
	}
}
// TElement[] System.Linq.EmptyPartition`1<UnityEngine.Ray>::ToArray()
extern "C"  RayU5BU5D_t1836217960* EmptyPartition_1_ToArray_m3357972257_gshared (EmptyPartition_1_t3419060527 * __this, const RuntimeMethod* method)
{
	{
		RayU5BU5D_t1836217960* L_0 = ((  RayU5BU5D_t1836217960* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.List`1<TElement> System.Linq.EmptyPartition`1<UnityEngine.Ray>::ToList()
extern "C"  List_1_t962958939 * EmptyPartition_1_ToList_m2432462464_gshared (EmptyPartition_1_t3419060527 * __this, const RuntimeMethod* method)
{
	{
		List_1_t962958939 * L_0 = (List_1_t962958939 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (List_1_t962958939 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_0;
	}
}
// System.Int32 System.Linq.EmptyPartition`1<UnityEngine.Ray>::GetCount(System.Boolean)
extern "C"  int32_t EmptyPartition_1_GetCount_m718360808_gshared (EmptyPartition_1_t3419060527 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	{
		return 0;
	}
}
// System.Void System.Linq.EmptyPartition`1<UnityEngine.Ray>::.cctor()
extern "C"  void EmptyPartition_1__cctor_m3362940958_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		EmptyPartition_1_t3419060527 * L_0 = (EmptyPartition_1_t3419060527 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (EmptyPartition_1_t3419060527 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EmptyPartition_1_t3419060527_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)))->set_Instance_0(L_0);
		return;
	}
}
// System.Void System.Linq.Enumerable/<CastIterator>d__35`1<System.Collections.DictionaryEntry>::.ctor(System.Int32)
extern "C"  void U3CCastIteratorU3Ed__35_1__ctor_m1478184395_gshared (U3CCastIteratorU3Ed__35_1_t2794392488 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m297566312((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1 = Environment_get_CurrentManagedThreadId_m3454612449(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void System.Linq.Enumerable/<CastIterator>d__35`1<System.Collections.DictionaryEntry>::System.IDisposable.Dispose()
extern "C"  void U3CCastIteratorU3Ed__35_1_System_IDisposable_Dispose_m2799862707_gshared (U3CCastIteratorU3Ed__35_1_t2794392488 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U3CU3E1__state_0();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_001a;
		}
	}

IL_0010:
	{
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((U3CCastIteratorU3Ed__35_1_t2794392488 *)__this);
		((  void (*) (U3CCastIteratorU3Ed__35_1_t2794392488 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((U3CCastIteratorU3Ed__35_1_t2794392488 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean System.Linq.Enumerable/<CastIterator>d__35`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C"  bool U3CCastIteratorU3Ed__35_1_MoveNext_m936381739_gshared (U3CCastIteratorU3Ed__35_1_t2794392488 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCastIteratorU3Ed__35_1_MoveNext_m936381739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = (int32_t)__this->get_U3CU3E1__state_0();
			V_1 = (int32_t)L_0;
			int32_t L_1 = V_1;
			if (!L_1)
			{
				goto IL_0012;
			}
		}

IL_000a:
		{
			int32_t L_2 = V_1;
			if ((((int32_t)L_2) == ((int32_t)1)))
			{
				goto IL_0057;
			}
		}

IL_000e:
		{
			V_0 = (bool)0;
			goto IL_0084;
		}

IL_0012:
		{
			__this->set_U3CU3E1__state_0((-1));
			RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
			NullCheck((RuntimeObject*)L_3);
			RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1941168011_il2cpp_TypeInfo_var, (RuntimeObject*)L_3);
			__this->set_U3CU3E7__wrap1_5(L_4);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			goto IL_005f;
		}

IL_0034:
		{
			RuntimeObject* L_5 = (RuntimeObject*)__this->get_U3CU3E7__wrap1_5();
			NullCheck((RuntimeObject*)L_5);
			RuntimeObject * L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_5);
			V_2 = (RuntimeObject *)L_6;
			RuntimeObject * L_7 = V_2;
			__this->set_U3CU3E2__current_1(((*(DictionaryEntry_t3123975638 *)((DictionaryEntry_t3123975638 *)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))))));
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_0084;
		}

IL_0057:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_005f:
		{
			RuntimeObject* L_8 = (RuntimeObject*)__this->get_U3CU3E7__wrap1_5();
			NullCheck((RuntimeObject*)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_8);
			if (L_9)
			{
				goto IL_0034;
			}
		}

IL_006c:
		{
			NullCheck((U3CCastIteratorU3Ed__35_1_t2794392488 *)__this);
			((  void (*) (U3CCastIteratorU3Ed__35_1_t2794392488 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((U3CCastIteratorU3Ed__35_1_t2794392488 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
			__this->set_U3CU3E7__wrap1_5((RuntimeObject*)NULL);
			V_0 = (bool)0;
			goto IL_0084;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FAULT_007d;
	}

FAULT_007d:
	{ // begin fault (depth: 1)
		NullCheck((U3CCastIteratorU3Ed__35_1_t2794392488 *)__this);
		((  void (*) (U3CCastIteratorU3Ed__35_1_t2794392488 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((U3CCastIteratorU3Ed__35_1_t2794392488 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		IL2CPP_END_FINALLY(125)
	} // end fault
	IL2CPP_CLEANUP(125)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0084:
	{
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Void System.Linq.Enumerable/<CastIterator>d__35`1<System.Collections.DictionaryEntry>::<>m__Finally1()
extern "C"  void U3CCastIteratorU3Ed__35_1_U3CU3Em__Finally1_m3047170860_gshared (U3CCastIteratorU3Ed__35_1_t2794392488 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCastIteratorU3Ed__35_1_U3CU3Em__Finally1_m3047170860_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		__this->set_U3CU3E1__state_0((-1));
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_U3CU3E7__wrap1_5();
		V_0 = (RuntimeObject*)((RuntimeObject*)IsInst((RuntimeObject*)L_0, IDisposable_t3640265483_il2cpp_TypeInfo_var));
		RuntimeObject* L_1 = V_0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		RuntimeObject* L_2 = V_0;
		NullCheck((RuntimeObject*)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_2);
	}

IL_001c:
	{
		return;
	}
}
// TResult System.Linq.Enumerable/<CastIterator>d__35`1<System.Collections.DictionaryEntry>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  DictionaryEntry_t3123975638  U3CCastIteratorU3Ed__35_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2584166013_gshared (U3CCastIteratorU3Ed__35_1_t2794392488 * __this, const RuntimeMethod* method)
{
	{
		DictionaryEntry_t3123975638  L_0 = (DictionaryEntry_t3123975638 )__this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/<CastIterator>d__35`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void U3CCastIteratorU3Ed__35_1_System_Collections_IEnumerator_Reset_m1143028353_gshared (U3CCastIteratorU3Ed__35_1_t2794392488 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCastIteratorU3Ed__35_1_System_Collections_IEnumerator_Reset_m1143028353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Object System.Linq.Enumerable/<CastIterator>d__35`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CCastIteratorU3Ed__35_1_System_Collections_IEnumerator_get_Current_m1103380503_gshared (U3CCastIteratorU3Ed__35_1_t2794392488 * __this, const RuntimeMethod* method)
{
	{
		DictionaryEntry_t3123975638  L_0 = (DictionaryEntry_t3123975638 )__this->get_U3CU3E2__current_1();
		DictionaryEntry_t3123975638  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CastIterator>d__35`1<System.Collections.DictionaryEntry>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  RuntimeObject* U3CCastIteratorU3Ed__35_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m2269350479_gshared (U3CCastIteratorU3Ed__35_1_t2794392488 * __this, const RuntimeMethod* method)
{
	U3CCastIteratorU3Ed__35_1_t2794392488 * V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_U3CU3El__initialThreadId_2();
		int32_t L_2 = Environment_get_CurrentManagedThreadId_m3454612449(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = (U3CCastIteratorU3Ed__35_1_t2794392488 *)__this;
		goto IL_0029;
	}

IL_0022:
	{
		U3CCastIteratorU3Ed__35_1_t2794392488 * L_3 = (U3CCastIteratorU3Ed__35_1_t2794392488 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (U3CCastIteratorU3Ed__35_1_t2794392488 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_3, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (U3CCastIteratorU3Ed__35_1_t2794392488 *)L_3;
	}

IL_0029:
	{
		U3CCastIteratorU3Ed__35_1_t2794392488 * L_4 = V_0;
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_U3CU3E3__source_4();
		NullCheck(L_4);
		L_4->set_source_3(L_5);
		U3CCastIteratorU3Ed__35_1_t2794392488 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CastIterator>d__35`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* U3CCastIteratorU3Ed__35_1_System_Collections_IEnumerable_GetEnumerator_m419143193_gshared (U3CCastIteratorU3Ed__35_1_t2794392488 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((U3CCastIteratorU3Ed__35_1_t2794392488 *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (U3CCastIteratorU3Ed__35_1_t2794392488 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((U3CCastIteratorU3Ed__35_1_t2794392488 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/<CastIterator>d__35`1<System.Object>::.ctor(System.Int32)
extern "C"  void U3CCastIteratorU3Ed__35_1__ctor_m3487785149_gshared (U3CCastIteratorU3Ed__35_1_t2750523014 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m297566312((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1 = Environment_get_CurrentManagedThreadId_m3454612449(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void System.Linq.Enumerable/<CastIterator>d__35`1<System.Object>::System.IDisposable.Dispose()
extern "C"  void U3CCastIteratorU3Ed__35_1_System_IDisposable_Dispose_m2378773332_gshared (U3CCastIteratorU3Ed__35_1_t2750523014 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U3CU3E1__state_0();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_001a;
		}
	}

IL_0010:
	{
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((U3CCastIteratorU3Ed__35_1_t2750523014 *)__this);
		((  void (*) (U3CCastIteratorU3Ed__35_1_t2750523014 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((U3CCastIteratorU3Ed__35_1_t2750523014 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean System.Linq.Enumerable/<CastIterator>d__35`1<System.Object>::MoveNext()
extern "C"  bool U3CCastIteratorU3Ed__35_1_MoveNext_m2750562796_gshared (U3CCastIteratorU3Ed__35_1_t2750523014 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCastIteratorU3Ed__35_1_MoveNext_m2750562796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = (int32_t)__this->get_U3CU3E1__state_0();
			V_1 = (int32_t)L_0;
			int32_t L_1 = V_1;
			if (!L_1)
			{
				goto IL_0012;
			}
		}

IL_000a:
		{
			int32_t L_2 = V_1;
			if ((((int32_t)L_2) == ((int32_t)1)))
			{
				goto IL_0057;
			}
		}

IL_000e:
		{
			V_0 = (bool)0;
			goto IL_0084;
		}

IL_0012:
		{
			__this->set_U3CU3E1__state_0((-1));
			RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
			NullCheck((RuntimeObject*)L_3);
			RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1941168011_il2cpp_TypeInfo_var, (RuntimeObject*)L_3);
			__this->set_U3CU3E7__wrap1_5(L_4);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			goto IL_005f;
		}

IL_0034:
		{
			RuntimeObject* L_5 = (RuntimeObject*)__this->get_U3CU3E7__wrap1_5();
			NullCheck((RuntimeObject*)L_5);
			RuntimeObject * L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_5);
			V_2 = (RuntimeObject *)L_6;
			RuntimeObject * L_7 = V_2;
			__this->set_U3CU3E2__current_1(((RuntimeObject *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_0084;
		}

IL_0057:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_005f:
		{
			RuntimeObject* L_8 = (RuntimeObject*)__this->get_U3CU3E7__wrap1_5();
			NullCheck((RuntimeObject*)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_8);
			if (L_9)
			{
				goto IL_0034;
			}
		}

IL_006c:
		{
			NullCheck((U3CCastIteratorU3Ed__35_1_t2750523014 *)__this);
			((  void (*) (U3CCastIteratorU3Ed__35_1_t2750523014 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((U3CCastIteratorU3Ed__35_1_t2750523014 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
			__this->set_U3CU3E7__wrap1_5((RuntimeObject*)NULL);
			V_0 = (bool)0;
			goto IL_0084;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FAULT_007d;
	}

FAULT_007d:
	{ // begin fault (depth: 1)
		NullCheck((U3CCastIteratorU3Ed__35_1_t2750523014 *)__this);
		((  void (*) (U3CCastIteratorU3Ed__35_1_t2750523014 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((U3CCastIteratorU3Ed__35_1_t2750523014 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		IL2CPP_END_FINALLY(125)
	} // end fault
	IL2CPP_CLEANUP(125)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0084:
	{
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Void System.Linq.Enumerable/<CastIterator>d__35`1<System.Object>::<>m__Finally1()
extern "C"  void U3CCastIteratorU3Ed__35_1_U3CU3Em__Finally1_m855207833_gshared (U3CCastIteratorU3Ed__35_1_t2750523014 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCastIteratorU3Ed__35_1_U3CU3Em__Finally1_m855207833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		__this->set_U3CU3E1__state_0((-1));
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_U3CU3E7__wrap1_5();
		V_0 = (RuntimeObject*)((RuntimeObject*)IsInst((RuntimeObject*)L_0, IDisposable_t3640265483_il2cpp_TypeInfo_var));
		RuntimeObject* L_1 = V_0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		RuntimeObject* L_2 = V_0;
		NullCheck((RuntimeObject*)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_2);
	}

IL_001c:
	{
		return;
	}
}
// TResult System.Linq.Enumerable/<CastIterator>d__35`1<System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  RuntimeObject * U3CCastIteratorU3Ed__35_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2935601638_gshared (U3CCastIteratorU3Ed__35_1_t2750523014 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/<CastIterator>d__35`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void U3CCastIteratorU3Ed__35_1_System_Collections_IEnumerator_Reset_m880153489_gshared (U3CCastIteratorU3Ed__35_1_t2750523014 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCastIteratorU3Ed__35_1_System_Collections_IEnumerator_Reset_m880153489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Object System.Linq.Enumerable/<CastIterator>d__35`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CCastIteratorU3Ed__35_1_System_Collections_IEnumerator_get_Current_m392871466_gshared (U3CCastIteratorU3Ed__35_1_t2750523014 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CastIterator>d__35`1<System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  RuntimeObject* U3CCastIteratorU3Ed__35_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m2774360475_gshared (U3CCastIteratorU3Ed__35_1_t2750523014 * __this, const RuntimeMethod* method)
{
	U3CCastIteratorU3Ed__35_1_t2750523014 * V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_U3CU3El__initialThreadId_2();
		int32_t L_2 = Environment_get_CurrentManagedThreadId_m3454612449(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = (U3CCastIteratorU3Ed__35_1_t2750523014 *)__this;
		goto IL_0029;
	}

IL_0022:
	{
		U3CCastIteratorU3Ed__35_1_t2750523014 * L_3 = (U3CCastIteratorU3Ed__35_1_t2750523014 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (U3CCastIteratorU3Ed__35_1_t2750523014 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_3, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (U3CCastIteratorU3Ed__35_1_t2750523014 *)L_3;
	}

IL_0029:
	{
		U3CCastIteratorU3Ed__35_1_t2750523014 * L_4 = V_0;
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_U3CU3E3__source_4();
		NullCheck(L_4);
		L_4->set_source_3(L_5);
		U3CCastIteratorU3Ed__35_1_t2750523014 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CastIterator>d__35`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* U3CCastIteratorU3Ed__35_1_System_Collections_IEnumerable_GetEnumerator_m1598616306_gshared (U3CCastIteratorU3Ed__35_1_t2750523014 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((U3CCastIteratorU3Ed__35_1_t2750523014 *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (U3CCastIteratorU3Ed__35_1_t2750523014 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((U3CCastIteratorU3Ed__35_1_t2750523014 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<System.Char>::.ctor()
extern "C"  void Iterator_1__ctor_m1480409028_gshared (Iterator_1_t2588820807 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m297566312((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = Environment_get_CurrentManagedThreadId_m3454612449(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__threadId_0(L_0);
		return;
	}
}
// TSource System.Linq.Enumerable/Iterator`1<System.Char>::get_Current()
extern "C"  Il2CppChar Iterator_1_get_Current_m965409121_gshared (Iterator_1_t2588820807 * __this, const RuntimeMethod* method)
{
	{
		Il2CppChar L_0 = (Il2CppChar)__this->get__current_2();
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<System.Char>::Dispose()
extern "C"  void Iterator_1_Dispose_m172456093_gshared (Iterator_1_t2588820807 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Iterator_1_Dispose_m172456093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar* L_0 = (Il2CppChar*)__this->get_address_of__current_2();
		Initobj (Char_t3634460470_il2cpp_TypeInfo_var, L_0);
		__this->set__state_1((-1));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1<System.Char>::GetEnumerator()
extern "C"  RuntimeObject* Iterator_1_GetEnumerator_m381843590_gshared (Iterator_1_t2588820807 * __this, const RuntimeMethod* method)
{
	Iterator_1_t2588820807 * G_B4_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__state_1();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get__threadId_0();
		int32_t L_2 = Environment_get_CurrentManagedThreadId_m3454612449(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}

IL_0015:
	{
		NullCheck((Iterator_1_t2588820807 *)__this);
		Iterator_1_t2588820807 * L_3 = VirtFuncInvoker0< Iterator_1_t2588820807 * >::Invoke(11 /* System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1<System.Char>::Clone() */, (Iterator_1_t2588820807 *)__this);
		G_B4_0 = L_3;
		goto IL_001e;
	}

IL_001d:
	{
		G_B4_0 = __this;
	}

IL_001e:
	{
		Iterator_1_t2588820807 * L_4 = (Iterator_1_t2588820807 *)G_B4_0;
		NullCheck(L_4);
		L_4->set__state_1(1);
		return L_4;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1<System.Char>::Where(System.Func`2<TSource,System.Boolean>)
extern "C"  RuntimeObject* Iterator_1_Where_m3593116308_gshared (Iterator_1_t2588820807 * __this, Func_2_t148644517 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t148644517 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t2739994797 * L_1 = (WhereEnumerableIterator_1_t2739994797 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (WhereEnumerableIterator_1_t2739994797 *, RuntimeObject*, Func_2_t148644517 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t148644517 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_1;
	}
}
// System.Object System.Linq.Enumerable/Iterator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Iterator_1_System_Collections_IEnumerator_get_Current_m3299259701_gshared (Iterator_1_t2588820807 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2588820807 *)__this);
		Il2CppChar L_0 = ((  Il2CppChar (*) (Iterator_1_t2588820807 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Iterator_1_t2588820807 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		Il2CppChar L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1<System.Char>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* Iterator_1_System_Collections_IEnumerable_GetEnumerator_m2610373035_gshared (Iterator_1_t2588820807 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2588820807 *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (Iterator_1_t2588820807 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Iterator_1_t2588820807 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void Iterator_1_System_Collections_IEnumerator_Reset_m3089157613_gshared (Iterator_1_t2588820807 * __this, const RuntimeMethod* method)
{
	{
		Exception_t1436737249 * L_0 = Error_NotSupported_m1072967690(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<System.Collections.DictionaryEntry>::.ctor()
extern "C"  void Iterator_1__ctor_m2419643334_gshared (Iterator_1_t2078335975 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m297566312((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = Environment_get_CurrentManagedThreadId_m3454612449(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__threadId_0(L_0);
		return;
	}
}
// TSource System.Linq.Enumerable/Iterator`1<System.Collections.DictionaryEntry>::get_Current()
extern "C"  DictionaryEntry_t3123975638  Iterator_1_get_Current_m1841868796_gshared (Iterator_1_t2078335975 * __this, const RuntimeMethod* method)
{
	{
		DictionaryEntry_t3123975638  L_0 = (DictionaryEntry_t3123975638 )__this->get__current_2();
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C"  void Iterator_1_Dispose_m101278873_gshared (Iterator_1_t2078335975 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Iterator_1_Dispose_m101278873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DictionaryEntry_t3123975638 * L_0 = (DictionaryEntry_t3123975638 *)__this->get_address_of__current_2();
		Initobj (DictionaryEntry_t3123975638_il2cpp_TypeInfo_var, L_0);
		__this->set__state_1((-1));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1<System.Collections.DictionaryEntry>::GetEnumerator()
extern "C"  RuntimeObject* Iterator_1_GetEnumerator_m4127302831_gshared (Iterator_1_t2078335975 * __this, const RuntimeMethod* method)
{
	Iterator_1_t2078335975 * G_B4_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__state_1();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get__threadId_0();
		int32_t L_2 = Environment_get_CurrentManagedThreadId_m3454612449(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}

IL_0015:
	{
		NullCheck((Iterator_1_t2078335975 *)__this);
		Iterator_1_t2078335975 * L_3 = VirtFuncInvoker0< Iterator_1_t2078335975 * >::Invoke(11 /* System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1<System.Collections.DictionaryEntry>::Clone() */, (Iterator_1_t2078335975 *)__this);
		G_B4_0 = L_3;
		goto IL_001e;
	}

IL_001d:
	{
		G_B4_0 = __this;
	}

IL_001e:
	{
		Iterator_1_t2078335975 * L_4 = (Iterator_1_t2078335975 *)G_B4_0;
		NullCheck(L_4);
		L_4->set__state_1(1);
		return L_4;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1<System.Collections.DictionaryEntry>::Where(System.Func`2<TSource,System.Boolean>)
extern "C"  RuntimeObject* Iterator_1_Where_m2092277878_gshared (Iterator_1_t2078335975 * __this, Func_2_t837490053 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t837490053 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t2229509965 * L_1 = (WhereEnumerableIterator_1_t2229509965 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (WhereEnumerableIterator_1_t2229509965 *, RuntimeObject*, Func_2_t837490053 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t837490053 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_1;
	}
}
// System.Object System.Linq.Enumerable/Iterator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Iterator_1_System_Collections_IEnumerator_get_Current_m2321364664_gshared (Iterator_1_t2078335975 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2078335975 *)__this);
		DictionaryEntry_t3123975638  L_0 = ((  DictionaryEntry_t3123975638  (*) (Iterator_1_t2078335975 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Iterator_1_t2078335975 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		DictionaryEntry_t3123975638  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* Iterator_1_System_Collections_IEnumerable_GetEnumerator_m773252142_gshared (Iterator_1_t2078335975 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2078335975 *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (Iterator_1_t2078335975 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Iterator_1_t2078335975 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void Iterator_1_System_Collections_IEnumerator_Reset_m6790728_gshared (Iterator_1_t2078335975 * __this, const RuntimeMethod* method)
{
	{
		Exception_t1436737249 * L_0 = Error_NotSupported_m1072967690(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void Iterator_1__ctor_m693774339_gshared (Iterator_1_t1484577656 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m297566312((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = Environment_get_CurrentManagedThreadId_m3454612449(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__threadId_0(L_0);
		return;
	}
}
// TSource System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t2530217319  Iterator_1_get_Current_m2095114607_gshared (Iterator_1_t1484577656 * __this, const RuntimeMethod* method)
{
	{
		KeyValuePair_2_t2530217319  L_0 = (KeyValuePair_2_t2530217319 )__this->get__current_2();
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void Iterator_1_Dispose_m3566902763_gshared (Iterator_1_t1484577656 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Iterator_1_Dispose_m3566902763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		KeyValuePair_2_t2530217319 * L_0 = (KeyValuePair_2_t2530217319 *)__this->get_address_of__current_2();
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, L_0);
		__this->set__state_1((-1));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C"  RuntimeObject* Iterator_1_GetEnumerator_m1838651557_gshared (Iterator_1_t1484577656 * __this, const RuntimeMethod* method)
{
	Iterator_1_t1484577656 * G_B4_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__state_1();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get__threadId_0();
		int32_t L_2 = Environment_get_CurrentManagedThreadId_m3454612449(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}

IL_0015:
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		Iterator_1_t1484577656 * L_3 = VirtFuncInvoker0< Iterator_1_t1484577656 * >::Invoke(11 /* System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clone() */, (Iterator_1_t1484577656 *)__this);
		G_B4_0 = L_3;
		goto IL_001e;
	}

IL_001d:
	{
		G_B4_0 = __this;
	}

IL_001e:
	{
		Iterator_1_t1484577656 * L_4 = (Iterator_1_t1484577656 *)G_B4_0;
		NullCheck(L_4);
		L_4->set__state_1(1);
		return L_4;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Where(System.Func`2<TSource,System.Boolean>)
extern "C"  RuntimeObject* Iterator_1_Where_m2679016702_gshared (Iterator_1_t1484577656 * __this, Func_2_t1033609360 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t1033609360 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1635751646 * L_1 = (WhereEnumerableIterator_1_t1635751646 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (WhereEnumerableIterator_1_t1635751646 *, RuntimeObject*, Func_2_t1033609360 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t1033609360 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_1;
	}
}
// System.Object System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Iterator_1_System_Collections_IEnumerator_get_Current_m4020877125_gshared (Iterator_1_t1484577656 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		KeyValuePair_2_t2530217319  L_0 = ((  KeyValuePair_2_t2530217319  (*) (Iterator_1_t1484577656 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Iterator_1_t1484577656 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		KeyValuePair_2_t2530217319  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* Iterator_1_System_Collections_IEnumerable_GetEnumerator_m1563948712_gshared (Iterator_1_t1484577656 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (Iterator_1_t1484577656 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Iterator_1_t1484577656 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Iterator_1_System_Collections_IEnumerator_Reset_m4174779311_gshared (Iterator_1_t1484577656 * __this, const RuntimeMethod* method)
{
	{
		Exception_t1436737249 * L_0 = Error_NotSupported_m1072967690(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<System.Object>::.ctor()
extern "C"  void Iterator_1__ctor_m374079053_gshared (Iterator_1_t2034466501 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m297566312((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = Environment_get_CurrentManagedThreadId_m3454612449(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__threadId_0(L_0);
		return;
	}
}
// TSource System.Linq.Enumerable/Iterator`1<System.Object>::get_Current()
extern "C"  RuntimeObject * Iterator_1_get_Current_m208940386_gshared (Iterator_1_t2034466501 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get__current_2();
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose()
extern "C"  void Iterator_1_Dispose_m274730813_gshared (Iterator_1_t2034466501 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Iterator_1_Dispose_m274730813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject ** L_0 = (RuntimeObject **)__this->get_address_of__current_2();
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, L_0);
		__this->set__state_1((-1));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1<System.Object>::GetEnumerator()
extern "C"  RuntimeObject* Iterator_1_GetEnumerator_m4184209818_gshared (Iterator_1_t2034466501 * __this, const RuntimeMethod* method)
{
	Iterator_1_t2034466501 * G_B4_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__state_1();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get__threadId_0();
		int32_t L_2 = Environment_get_CurrentManagedThreadId_m3454612449(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}

IL_0015:
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		Iterator_1_t2034466501 * L_3 = VirtFuncInvoker0< Iterator_1_t2034466501 * >::Invoke(11 /* System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1<System.Object>::Clone() */, (Iterator_1_t2034466501 *)__this);
		G_B4_0 = L_3;
		goto IL_001e;
	}

IL_001d:
	{
		G_B4_0 = __this;
	}

IL_001e:
	{
		Iterator_1_t2034466501 * L_4 = (Iterator_1_t2034466501 *)G_B4_0;
		NullCheck(L_4);
		L_4->set__state_1(1);
		return L_4;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1<System.Object>::Where(System.Func`2<TSource,System.Boolean>)
extern "C"  RuntimeObject* Iterator_1_Where_m3853384958_gshared (Iterator_1_t2034466501 * __this, Func_2_t3759279471 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t3759279471 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t2185640491 * L_1 = (WhereEnumerableIterator_1_t2185640491 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (WhereEnumerableIterator_1_t2185640491 *, RuntimeObject*, Func_2_t3759279471 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t3759279471 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_1;
	}
}
// System.Object System.Linq.Enumerable/Iterator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Iterator_1_System_Collections_IEnumerator_get_Current_m1841106287_gshared (Iterator_1_t2034466501 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		RuntimeObject * L_0 = ((  RuntimeObject * (*) (Iterator_1_t2034466501 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Iterator_1_t2034466501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* Iterator_1_System_Collections_IEnumerable_GetEnumerator_m1813022145_gshared (Iterator_1_t2034466501 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (Iterator_1_t2034466501 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Iterator_1_t2034466501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Iterator_1_System_Collections_IEnumerator_Reset_m2523254832_gshared (Iterator_1_t2034466501 * __this, const RuntimeMethod* method)
{
	{
		Exception_t1436737249 * L_0 = Error_NotSupported_m1072967690(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.Ray>::.ctor()
extern "C"  void Iterator_1__ctor_m2958259103_gshared (Iterator_1_t2740211830 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m297566312((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = Environment_get_CurrentManagedThreadId_m3454612449(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__threadId_0(L_0);
		return;
	}
}
// TSource System.Linq.Enumerable/Iterator`1<UnityEngine.Ray>::get_Current()
extern "C"  Ray_t3785851493  Iterator_1_get_Current_m4237267243_gshared (Iterator_1_t2740211830 * __this, const RuntimeMethod* method)
{
	{
		Ray_t3785851493  L_0 = (Ray_t3785851493 )__this->get__current_2();
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.Ray>::Dispose()
extern "C"  void Iterator_1_Dispose_m705651954_gshared (Iterator_1_t2740211830 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Iterator_1_Dispose_m705651954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Ray_t3785851493 * L_0 = (Ray_t3785851493 *)__this->get_address_of__current_2();
		Initobj (Ray_t3785851493_il2cpp_TypeInfo_var, L_0);
		__this->set__state_1((-1));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1<UnityEngine.Ray>::GetEnumerator()
extern "C"  RuntimeObject* Iterator_1_GetEnumerator_m3408152577_gshared (Iterator_1_t2740211830 * __this, const RuntimeMethod* method)
{
	Iterator_1_t2740211830 * G_B4_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__state_1();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get__threadId_0();
		int32_t L_2 = Environment_get_CurrentManagedThreadId_m3454612449(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}

IL_0015:
	{
		NullCheck((Iterator_1_t2740211830 *)__this);
		Iterator_1_t2740211830 * L_3 = VirtFuncInvoker0< Iterator_1_t2740211830 * >::Invoke(11 /* System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1<UnityEngine.Ray>::Clone() */, (Iterator_1_t2740211830 *)__this);
		G_B4_0 = L_3;
		goto IL_001e;
	}

IL_001d:
	{
		G_B4_0 = __this;
	}

IL_001e:
	{
		Iterator_1_t2740211830 * L_4 = (Iterator_1_t2740211830 *)G_B4_0;
		NullCheck(L_4);
		L_4->set__state_1(1);
		return L_4;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1<UnityEngine.Ray>::Where(System.Func`2<TSource,System.Boolean>)
extern "C"  RuntimeObject* Iterator_1_Where_m2001696234_gshared (Iterator_1_t2740211830 * __this, Func_2_t2751558106 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2751558106 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t2891385820 * L_1 = (WhereEnumerableIterator_1_t2891385820 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (WhereEnumerableIterator_1_t2891385820 *, RuntimeObject*, Func_2_t2751558106 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2751558106 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_1;
	}
}
// System.Object System.Linq.Enumerable/Iterator`1<UnityEngine.Ray>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Iterator_1_System_Collections_IEnumerator_get_Current_m2446919313_gshared (Iterator_1_t2740211830 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2740211830 *)__this);
		Ray_t3785851493  L_0 = ((  Ray_t3785851493  (*) (Iterator_1_t2740211830 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Iterator_1_t2740211830 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		Ray_t3785851493  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1<UnityEngine.Ray>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* Iterator_1_System_Collections_IEnumerable_GetEnumerator_m1928497675_gshared (Iterator_1_t2740211830 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2740211830 *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (Iterator_1_t2740211830 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Iterator_1_t2740211830 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.Ray>::System.Collections.IEnumerator.Reset()
extern "C"  void Iterator_1_System_Collections_IEnumerator_Reset_m2805409862_gshared (Iterator_1_t2740211830 * __this, const RuntimeMethod* method)
{
	{
		Exception_t1436737249 * L_0 = Error_NotSupported_m1072967690(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/SelectArrayIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(TSource[],System.Func`2<TSource,TResult>)
extern "C"  void SelectArrayIterator_2__ctor_m3415729862_gshared (SelectArrayIterator_2_t1643067185 * __this, DictionaryEntryU5BU5D_t4217117203* ___source0, Func_2_t3270419407 * ___selector1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		((  void (*) (Iterator_1_t1484577656 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t1484577656 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		DictionaryEntryU5BU5D_t4217117203* L_0 = ___source0;
		__this->set__source_3(L_0);
		Func_2_t3270419407 * L_1 = ___selector1;
		__this->set__selector_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/SelectArrayIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clone()
extern "C"  Iterator_1_t1484577656 * SelectArrayIterator_2_Clone_m1968224958_gshared (SelectArrayIterator_2_t1643067185 * __this, const RuntimeMethod* method)
{
	{
		DictionaryEntryU5BU5D_t4217117203* L_0 = (DictionaryEntryU5BU5D_t4217117203*)__this->get__source_3();
		Func_2_t3270419407 * L_1 = (Func_2_t3270419407 *)__this->get__selector_4();
		SelectArrayIterator_2_t1643067185 * L_2 = (SelectArrayIterator_2_t1643067185 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (SelectArrayIterator_2_t1643067185 *, DictionaryEntryU5BU5D_t4217117203*, Func_2_t3270419407 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (DictionaryEntryU5BU5D_t4217117203*)L_0, (Func_2_t3270419407 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Boolean System.Linq.Enumerable/SelectArrayIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool SelectArrayIterator_2_MoveNext_m3207850643_gshared (SelectArrayIterator_2_t1643067185 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t1484577656 *)__this)->get__state_1();
		int32_t L_1 = (int32_t)((Iterator_1_t1484577656 *)__this)->get__state_1();
		DictionaryEntryU5BU5D_t4217117203* L_2 = (DictionaryEntryU5BU5D_t4217117203*)__this->get__source_3();
		NullCheck(L_2);
		if (!((int32_t)((int32_t)((((int32_t)L_0) < ((int32_t)1))? 1 : 0)|(int32_t)((((int32_t)L_1) == ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))))+(int32_t)1))))? 1 : 0))))
		{
			goto IL_0026;
		}
	}
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose() */, (Iterator_1_t1484577656 *)__this);
		return (bool)0;
	}

IL_0026:
	{
		int32_t L_3 = (int32_t)((Iterator_1_t1484577656 *)__this)->get__state_1();
		V_1 = (int32_t)L_3;
		int32_t L_4 = V_1;
		((Iterator_1_t1484577656 *)__this)->set__state_1(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_1;
		V_0 = (int32_t)((int32_t)((int32_t)L_5-(int32_t)1));
		Func_2_t3270419407 * L_6 = (Func_2_t3270419407 *)__this->get__selector_4();
		DictionaryEntryU5BU5D_t4217117203* L_7 = (DictionaryEntryU5BU5D_t4217117203*)__this->get__source_3();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		DictionaryEntry_t3123975638  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((Func_2_t3270419407 *)L_6);
		KeyValuePair_2_t2530217319  L_11 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Func_2_t3270419407 *)L_6, (DictionaryEntry_t3123975638 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		((Iterator_1_t1484577656 *)__this)->set__current_2(L_11);
		return (bool)1;
	}
}
// TResult[] System.Linq.Enumerable/SelectArrayIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t118269214* SelectArrayIterator_2_ToArray_m2917801920_gshared (SelectArrayIterator_2_t1643067185 * __this, const RuntimeMethod* method)
{
	KeyValuePair_2U5BU5D_t118269214* V_0 = NULL;
	int32_t V_1 = 0;
	{
		DictionaryEntryU5BU5D_t4217117203* L_0 = (DictionaryEntryU5BU5D_t4217117203*)__this->get__source_3();
		NullCheck(L_0);
		V_0 = (KeyValuePair_2U5BU5D_t118269214*)((KeyValuePair_2U5BU5D_t118269214*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0034;
	}

IL_0012:
	{
		KeyValuePair_2U5BU5D_t118269214* L_1 = V_0;
		int32_t L_2 = V_1;
		Func_2_t3270419407 * L_3 = (Func_2_t3270419407 *)__this->get__selector_4();
		DictionaryEntryU5BU5D_t4217117203* L_4 = (DictionaryEntryU5BU5D_t4217117203*)__this->get__source_3();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		DictionaryEntry_t3123975638  L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Func_2_t3270419407 *)L_3);
		KeyValuePair_2_t2530217319  L_8 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Func_2_t3270419407 *)L_3, (DictionaryEntry_t3123975638 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (KeyValuePair_2_t2530217319 )L_8);
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_10 = V_1;
		KeyValuePair_2U5BU5D_t118269214* L_11 = V_0;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length)))))))
		{
			goto IL_0012;
		}
	}
	{
		KeyValuePair_2U5BU5D_t118269214* L_12 = V_0;
		return L_12;
	}
}
// System.Collections.Generic.List`1<TResult> System.Linq.Enumerable/SelectArrayIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToList()
extern "C"  List_1_t4002292061 * SelectArrayIterator_2_ToList_m664023846_gshared (SelectArrayIterator_2_t1643067185 * __this, const RuntimeMethod* method)
{
	DictionaryEntryU5BU5D_t4217117203* V_0 = NULL;
	List_1_t4002292061 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		DictionaryEntryU5BU5D_t4217117203* L_0 = (DictionaryEntryU5BU5D_t4217117203*)__this->get__source_3();
		V_0 = (DictionaryEntryU5BU5D_t4217117203*)L_0;
		DictionaryEntryU5BU5D_t4217117203* L_1 = V_0;
		NullCheck(L_1);
		List_1_t4002292061 * L_2 = (List_1_t4002292061 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (List_1_t4002292061 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_2, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_1 = (List_1_t4002292061 *)L_2;
		V_2 = (int32_t)0;
		goto IL_0030;
	}

IL_0014:
	{
		List_1_t4002292061 * L_3 = V_1;
		Func_2_t3270419407 * L_4 = (Func_2_t3270419407 *)__this->get__selector_4();
		DictionaryEntryU5BU5D_t4217117203* L_5 = V_0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		DictionaryEntry_t3123975638  L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck((Func_2_t3270419407 *)L_4);
		KeyValuePair_2_t2530217319  L_9 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Func_2_t3270419407 *)L_4, (DictionaryEntry_t3123975638 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((List_1_t4002292061 *)L_3);
		((  void (*) (List_1_t4002292061 *, KeyValuePair_2_t2530217319 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t4002292061 *)L_3, (KeyValuePair_2_t2530217319 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		int32_t L_10 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_11 = V_2;
		DictionaryEntryU5BU5D_t4217117203* L_12 = V_0;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		List_1_t4002292061 * L_13 = V_1;
		return L_13;
	}
}
// System.Int32 System.Linq.Enumerable/SelectArrayIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetCount(System.Boolean)
extern "C"  int32_t SelectArrayIterator_2_GetCount_m312621225_gshared (SelectArrayIterator_2_t1643067185 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	DictionaryEntryU5BU5D_t4217117203* V_0 = NULL;
	int32_t V_1 = 0;
	DictionaryEntry_t3123975638  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		bool L_0 = ___onlyIfCheap0;
		if (L_0)
		{
			goto IL_002d;
		}
	}
	{
		DictionaryEntryU5BU5D_t4217117203* L_1 = (DictionaryEntryU5BU5D_t4217117203*)__this->get__source_3();
		V_0 = (DictionaryEntryU5BU5D_t4217117203*)L_1;
		V_1 = (int32_t)0;
		goto IL_0027;
	}

IL_000e:
	{
		DictionaryEntryU5BU5D_t4217117203* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		DictionaryEntry_t3123975638  L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_2 = (DictionaryEntry_t3123975638 )L_5;
		Func_2_t3270419407 * L_6 = (Func_2_t3270419407 *)__this->get__selector_4();
		DictionaryEntry_t3123975638  L_7 = V_2;
		NullCheck((Func_2_t3270419407 *)L_6);
		((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Func_2_t3270419407 *)L_6, (DictionaryEntry_t3123975638 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		int32_t L_8 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_9 = V_1;
		DictionaryEntryU5BU5D_t4217117203* L_10 = V_0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))))))
		{
			goto IL_000e;
		}
	}

IL_002d:
	{
		DictionaryEntryU5BU5D_t4217117203* L_11 = (DictionaryEntryU5BU5D_t4217117203*)__this->get__source_3();
		NullCheck(L_11);
		return (((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length))));
	}
}
// TResult System.Linq.Enumerable/SelectArrayIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetElementAt(System.Int32,System.Boolean&)
extern "C"  KeyValuePair_2_t2530217319  SelectArrayIterator_2_TryGetElementAt_m4000501117_gshared (SelectArrayIterator_2_t1643067185 * __this, int32_t ___index0, bool* ___found1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectArrayIterator_2_TryGetElementAt_m4000501117_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2530217319  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		DictionaryEntryU5BU5D_t4217117203* L_1 = (DictionaryEntryU5BU5D_t4217117203*)__this->get__source_3();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) < ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))))))
		{
			goto IL_0026;
		}
	}
	{
		bool* L_2 = ___found1;
		*((int8_t*)(L_2)) = (int8_t)1;
		Func_2_t3270419407 * L_3 = (Func_2_t3270419407 *)__this->get__selector_4();
		DictionaryEntryU5BU5D_t4217117203* L_4 = (DictionaryEntryU5BU5D_t4217117203*)__this->get__source_3();
		int32_t L_5 = ___index0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		DictionaryEntry_t3123975638  L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Func_2_t3270419407 *)L_3);
		KeyValuePair_2_t2530217319  L_8 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Func_2_t3270419407 *)L_3, (DictionaryEntry_t3123975638 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_8;
	}

IL_0026:
	{
		bool* L_9 = ___found1;
		*((int8_t*)(L_9)) = (int8_t)0;
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_0));
		KeyValuePair_2_t2530217319  L_10 = V_0;
		return L_10;
	}
}
// TResult System.Linq.Enumerable/SelectArrayIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetFirst(System.Boolean&)
extern "C"  KeyValuePair_2_t2530217319  SelectArrayIterator_2_TryGetFirst_m3977257204_gshared (SelectArrayIterator_2_t1643067185 * __this, bool* ___found0, const RuntimeMethod* method)
{
	{
		bool* L_0 = ___found0;
		*((int8_t*)(L_0)) = (int8_t)1;
		Func_2_t3270419407 * L_1 = (Func_2_t3270419407 *)__this->get__selector_4();
		DictionaryEntryU5BU5D_t4217117203* L_2 = (DictionaryEntryU5BU5D_t4217117203*)__this->get__source_3();
		NullCheck(L_2);
		int32_t L_3 = 0;
		DictionaryEntry_t3123975638  L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck((Func_2_t3270419407 *)L_1);
		KeyValuePair_2_t2530217319  L_5 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Func_2_t3270419407 *)L_1, (DictionaryEntry_t3123975638 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_5;
	}
}
// TResult System.Linq.Enumerable/SelectArrayIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetLast(System.Boolean&)
extern "C"  KeyValuePair_2_t2530217319  SelectArrayIterator_2_TryGetLast_m2976240509_gshared (SelectArrayIterator_2_t1643067185 * __this, bool* ___found0, const RuntimeMethod* method)
{
	{
		bool* L_0 = ___found0;
		*((int8_t*)(L_0)) = (int8_t)1;
		Func_2_t3270419407 * L_1 = (Func_2_t3270419407 *)__this->get__selector_4();
		DictionaryEntryU5BU5D_t4217117203* L_2 = (DictionaryEntryU5BU5D_t4217117203*)__this->get__source_3();
		DictionaryEntryU5BU5D_t4217117203* L_3 = (DictionaryEntryU5BU5D_t4217117203*)__this->get__source_3();
		NullCheck(L_3);
		NullCheck(L_2);
		int32_t L_4 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))-(int32_t)1));
		DictionaryEntry_t3123975638  L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck((Func_2_t3270419407 *)L_1);
		KeyValuePair_2_t2530217319  L_6 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Func_2_t3270419407 *)L_1, (DictionaryEntry_t3123975638 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_6;
	}
}
// System.Void System.Linq.Enumerable/SelectArrayIterator`2<System.Object,System.Object>::.ctor(TSource[],System.Func`2<TSource,TResult>)
extern "C"  void SelectArrayIterator_2__ctor_m623539725_gshared (SelectArrayIterator_2_t819778152 * __this, ObjectU5BU5D_t2843939325* ___source0, Func_2_t2447130374 * ___selector1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		((  void (*) (Iterator_1_t2034466501 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t2034466501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t2843939325* L_0 = ___source0;
		__this->set__source_3(L_0);
		Func_2_t2447130374 * L_1 = ___selector1;
		__this->set__selector_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/SelectArrayIterator`2<System.Object,System.Object>::Clone()
extern "C"  Iterator_1_t2034466501 * SelectArrayIterator_2_Clone_m1894810258_gshared (SelectArrayIterator_2_t819778152 * __this, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get__source_3();
		Func_2_t2447130374 * L_1 = (Func_2_t2447130374 *)__this->get__selector_4();
		SelectArrayIterator_2_t819778152 * L_2 = (SelectArrayIterator_2_t819778152 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (SelectArrayIterator_2_t819778152 *, ObjectU5BU5D_t2843939325*, Func_2_t2447130374 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (ObjectU5BU5D_t2843939325*)L_0, (Func_2_t2447130374 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Boolean System.Linq.Enumerable/SelectArrayIterator`2<System.Object,System.Object>::MoveNext()
extern "C"  bool SelectArrayIterator_2_MoveNext_m1546943899_gshared (SelectArrayIterator_2_t819778152 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t2034466501 *)__this)->get__state_1();
		int32_t L_1 = (int32_t)((Iterator_1_t2034466501 *)__this)->get__state_1();
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)__this->get__source_3();
		NullCheck(L_2);
		if (!((int32_t)((int32_t)((((int32_t)L_0) < ((int32_t)1))? 1 : 0)|(int32_t)((((int32_t)L_1) == ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))))+(int32_t)1))))? 1 : 0))))
		{
			goto IL_0026;
		}
	}
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t2034466501 *)__this);
		return (bool)0;
	}

IL_0026:
	{
		int32_t L_3 = (int32_t)((Iterator_1_t2034466501 *)__this)->get__state_1();
		V_1 = (int32_t)L_3;
		int32_t L_4 = V_1;
		((Iterator_1_t2034466501 *)__this)->set__state_1(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_1;
		V_0 = (int32_t)((int32_t)((int32_t)L_5-(int32_t)1));
		Func_2_t2447130374 * L_6 = (Func_2_t2447130374 *)__this->get__selector_4();
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get__source_3();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		RuntimeObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((Func_2_t2447130374 *)L_6);
		RuntimeObject * L_11 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Func_2_t2447130374 *)L_6, (RuntimeObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		((Iterator_1_t2034466501 *)__this)->set__current_2(L_11);
		return (bool)1;
	}
}
// TResult[] System.Linq.Enumerable/SelectArrayIterator`2<System.Object,System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t2843939325* SelectArrayIterator_2_ToArray_m2773337652_gshared (SelectArrayIterator_2_t819778152 * __this, const RuntimeMethod* method)
{
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get__source_3();
		NullCheck(L_0);
		V_0 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0034;
	}

IL_0012:
	{
		ObjectU5BU5D_t2843939325* L_1 = V_0;
		int32_t L_2 = V_1;
		Func_2_t2447130374 * L_3 = (Func_2_t2447130374 *)__this->get__selector_4();
		ObjectU5BU5D_t2843939325* L_4 = (ObjectU5BU5D_t2843939325*)__this->get__source_3();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Func_2_t2447130374 *)L_3);
		RuntimeObject * L_8 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Func_2_t2447130374 *)L_3, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (RuntimeObject *)L_8);
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_10 = V_1;
		ObjectU5BU5D_t2843939325* L_11 = V_0;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length)))))))
		{
			goto IL_0012;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_12 = V_0;
		return L_12;
	}
}
// System.Collections.Generic.List`1<TResult> System.Linq.Enumerable/SelectArrayIterator`2<System.Object,System.Object>::ToList()
extern "C"  List_1_t257213610 * SelectArrayIterator_2_ToList_m3952854846_gshared (SelectArrayIterator_2_t819778152 * __this, const RuntimeMethod* method)
{
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	List_1_t257213610 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get__source_3();
		V_0 = (ObjectU5BU5D_t2843939325*)L_0;
		ObjectU5BU5D_t2843939325* L_1 = V_0;
		NullCheck(L_1);
		List_1_t257213610 * L_2 = (List_1_t257213610 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (List_1_t257213610 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_2, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_1 = (List_1_t257213610 *)L_2;
		V_2 = (int32_t)0;
		goto IL_0030;
	}

IL_0014:
	{
		List_1_t257213610 * L_3 = V_1;
		Func_2_t2447130374 * L_4 = (Func_2_t2447130374 *)__this->get__selector_4();
		ObjectU5BU5D_t2843939325* L_5 = V_0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck((Func_2_t2447130374 *)L_4);
		RuntimeObject * L_9 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Func_2_t2447130374 *)L_4, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((List_1_t257213610 *)L_3);
		((  void (*) (List_1_t257213610 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t257213610 *)L_3, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		int32_t L_10 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_11 = V_2;
		ObjectU5BU5D_t2843939325* L_12 = V_0;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		List_1_t257213610 * L_13 = V_1;
		return L_13;
	}
}
// System.Int32 System.Linq.Enumerable/SelectArrayIterator`2<System.Object,System.Object>::GetCount(System.Boolean)
extern "C"  int32_t SelectArrayIterator_2_GetCount_m2180995122_gshared (SelectArrayIterator_2_t819778152 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	int32_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	{
		bool L_0 = ___onlyIfCheap0;
		if (L_0)
		{
			goto IL_002d;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_1 = (ObjectU5BU5D_t2843939325*)__this->get__source_3();
		V_0 = (ObjectU5BU5D_t2843939325*)L_1;
		V_1 = (int32_t)0;
		goto IL_0027;
	}

IL_000e:
	{
		ObjectU5BU5D_t2843939325* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_2 = (RuntimeObject *)L_5;
		Func_2_t2447130374 * L_6 = (Func_2_t2447130374 *)__this->get__selector_4();
		RuntimeObject * L_7 = V_2;
		NullCheck((Func_2_t2447130374 *)L_6);
		((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Func_2_t2447130374 *)L_6, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		int32_t L_8 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_9 = V_1;
		ObjectU5BU5D_t2843939325* L_10 = V_0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))))))
		{
			goto IL_000e;
		}
	}

IL_002d:
	{
		ObjectU5BU5D_t2843939325* L_11 = (ObjectU5BU5D_t2843939325*)__this->get__source_3();
		NullCheck(L_11);
		return (((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length))));
	}
}
// TResult System.Linq.Enumerable/SelectArrayIterator`2<System.Object,System.Object>::TryGetElementAt(System.Int32,System.Boolean&)
extern "C"  RuntimeObject * SelectArrayIterator_2_TryGetElementAt_m3795460227_gshared (SelectArrayIterator_2_t819778152 * __this, int32_t ___index0, bool* ___found1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectArrayIterator_2_TryGetElementAt_m3795460227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		int32_t L_0 = ___index0;
		ObjectU5BU5D_t2843939325* L_1 = (ObjectU5BU5D_t2843939325*)__this->get__source_3();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) < ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))))))
		{
			goto IL_0026;
		}
	}
	{
		bool* L_2 = ___found1;
		*((int8_t*)(L_2)) = (int8_t)1;
		Func_2_t2447130374 * L_3 = (Func_2_t2447130374 *)__this->get__selector_4();
		ObjectU5BU5D_t2843939325* L_4 = (ObjectU5BU5D_t2843939325*)__this->get__source_3();
		int32_t L_5 = ___index0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Func_2_t2447130374 *)L_3);
		RuntimeObject * L_8 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Func_2_t2447130374 *)L_3, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_8;
	}

IL_0026:
	{
		bool* L_9 = ___found1;
		*((int8_t*)(L_9)) = (int8_t)0;
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_0));
		RuntimeObject * L_10 = V_0;
		return L_10;
	}
}
// TResult System.Linq.Enumerable/SelectArrayIterator`2<System.Object,System.Object>::TryGetFirst(System.Boolean&)
extern "C"  RuntimeObject * SelectArrayIterator_2_TryGetFirst_m2215273228_gshared (SelectArrayIterator_2_t819778152 * __this, bool* ___found0, const RuntimeMethod* method)
{
	{
		bool* L_0 = ___found0;
		*((int8_t*)(L_0)) = (int8_t)1;
		Func_2_t2447130374 * L_1 = (Func_2_t2447130374 *)__this->get__selector_4();
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)__this->get__source_3();
		NullCheck(L_2);
		int32_t L_3 = 0;
		RuntimeObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck((Func_2_t2447130374 *)L_1);
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Func_2_t2447130374 *)L_1, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_5;
	}
}
// TResult System.Linq.Enumerable/SelectArrayIterator`2<System.Object,System.Object>::TryGetLast(System.Boolean&)
extern "C"  RuntimeObject * SelectArrayIterator_2_TryGetLast_m605526328_gshared (SelectArrayIterator_2_t819778152 * __this, bool* ___found0, const RuntimeMethod* method)
{
	{
		bool* L_0 = ___found0;
		*((int8_t*)(L_0)) = (int8_t)1;
		Func_2_t2447130374 * L_1 = (Func_2_t2447130374 *)__this->get__selector_4();
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)__this->get__source_3();
		ObjectU5BU5D_t2843939325* L_3 = (ObjectU5BU5D_t2843939325*)__this->get__source_3();
		NullCheck(L_3);
		NullCheck(L_2);
		int32_t L_4 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))-(int32_t)1));
		RuntimeObject * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck((Func_2_t2447130374 *)L_1);
		RuntimeObject * L_6 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Func_2_t2447130374 *)L_1, (RuntimeObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_6;
	}
}
// System.Void System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
extern "C"  void SelectEnumerableIterator_2__ctor_m3076153172_gshared (SelectEnumerableIterator_2_t760503204 * __this, RuntimeObject* ___source0, Func_2_t3270419407 * ___selector1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		((  void (*) (Iterator_1_t1484577656 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t1484577656 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set__source_3(L_0);
		Func_2_t3270419407 * L_1 = ___selector1;
		__this->set__selector_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clone()
extern "C"  Iterator_1_t1484577656 * SelectEnumerableIterator_2_Clone_m1781680563_gshared (SelectEnumerableIterator_2_t760503204 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		Func_2_t3270419407 * L_1 = (Func_2_t3270419407 *)__this->get__selector_4();
		SelectEnumerableIterator_2_t760503204 * L_2 = (SelectEnumerableIterator_2_t760503204 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (SelectEnumerableIterator_2_t760503204 *, RuntimeObject*, Func_2_t3270419407 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (RuntimeObject*)L_0, (Func_2_t3270419407 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void SelectEnumerableIterator_2_Dispose_m3862804282_gshared (SelectEnumerableIterator_2_t760503204 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectEnumerableIterator_2_Dispose_m3862804282_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__enumerator_5();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
		__this->set__enumerator_5((RuntimeObject*)NULL);
	}

IL_001a:
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		((  void (*) (Iterator_1_t1484577656 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Iterator_1_t1484577656 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool SelectEnumerableIterator_2_MoveNext_m1500635119_gshared (SelectEnumerableIterator_2_t760503204 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectEnumerableIterator_2_MoveNext_m1500635119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t1484577656 *)__this)->get__state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0029;
		}
	}
	{
		goto IL_005a;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.DictionaryEntry>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set__enumerator_5(L_4);
		((Iterator_1_t1484577656 *)__this)->set__state_1(2);
	}

IL_0029:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_5);
		if (!L_6)
		{
			goto IL_0054;
		}
	}
	{
		Func_2_t3270419407 * L_7 = (Func_2_t3270419407 *)__this->get__selector_4();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_8);
		DictionaryEntry_t3123975638  L_9 = InterfaceFuncInvoker0< DictionaryEntry_t3123975638  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_8);
		NullCheck((Func_2_t3270419407 *)L_7);
		KeyValuePair_2_t2530217319  L_10 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3270419407 *)L_7, (DictionaryEntry_t3123975638 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		((Iterator_1_t1484577656 *)__this)->set__current_2(L_10);
		return (bool)1;
	}

IL_0054:
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose() */, (Iterator_1_t1484577656 *)__this);
	}

IL_005a:
	{
		return (bool)0;
	}
}
// TResult[] System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t118269214* SelectEnumerableIterator_2_ToArray_m1883646757_gshared (SelectEnumerableIterator_2_t760503204 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectEnumerableIterator_2_ToArray_m1883646757_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LargeArrayBuilder_1_t2440570340  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RuntimeObject* V_1 = NULL;
	DictionaryEntry_t3123975638  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		LargeArrayBuilder_1__ctor_m2226121364((LargeArrayBuilder_1_t2440570340 *)(&V_0), (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.DictionaryEntry>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_0);
		V_1 = (RuntimeObject*)L_1;
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0030;
		}

IL_0016:
		{
			RuntimeObject* L_2 = V_1;
			NullCheck((RuntimeObject*)L_2);
			DictionaryEntry_t3123975638  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3123975638  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_2);
			V_2 = (DictionaryEntry_t3123975638 )L_3;
			Func_2_t3270419407 * L_4 = (Func_2_t3270419407 *)__this->get__selector_4();
			DictionaryEntry_t3123975638  L_5 = V_2;
			NullCheck((Func_2_t3270419407 *)L_4);
			KeyValuePair_2_t2530217319  L_6 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3270419407 *)L_4, (DictionaryEntry_t3123975638 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			LargeArrayBuilder_1_Add_m982335036((LargeArrayBuilder_1_t2440570340 *)(&V_0), (KeyValuePair_2_t2530217319 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_0030:
		{
			RuntimeObject* L_7 = V_1;
			NullCheck((RuntimeObject*)L_7);
			bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_7);
			if (L_8)
			{
				goto IL_0016;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x44, FINALLY_003a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_9 = V_1;
			if (!L_9)
			{
				goto IL_0043;
			}
		}

IL_003d:
		{
			RuntimeObject* L_10 = V_1;
			NullCheck((RuntimeObject*)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_10);
		}

IL_0043:
		{
			IL2CPP_END_FINALLY(58)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0044:
	{
		KeyValuePair_2U5BU5D_t118269214* L_11 = LargeArrayBuilder_1_ToArray_m3030376891((LargeArrayBuilder_1_t2440570340 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_11;
	}
}
// System.Collections.Generic.List`1<TResult> System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToList()
extern "C"  List_1_t4002292061 * SelectEnumerableIterator_2_ToList_m1958580226_gshared (SelectEnumerableIterator_2_t760503204 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectEnumerableIterator_2_ToList_m1958580226_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t4002292061 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	DictionaryEntry_t3123975638  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t4002292061 * L_0 = (List_1_t4002292061 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((  void (*) (List_1_t4002292061 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		V_0 = (List_1_t4002292061 *)L_0;
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.DictionaryEntry>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_1);
		V_1 = (RuntimeObject*)L_2;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002d;
		}

IL_0014:
		{
			RuntimeObject* L_3 = V_1;
			NullCheck((RuntimeObject*)L_3);
			DictionaryEntry_t3123975638  L_4 = InterfaceFuncInvoker0< DictionaryEntry_t3123975638  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_3);
			V_2 = (DictionaryEntry_t3123975638 )L_4;
			List_1_t4002292061 * L_5 = V_0;
			Func_2_t3270419407 * L_6 = (Func_2_t3270419407 *)__this->get__selector_4();
			DictionaryEntry_t3123975638  L_7 = V_2;
			NullCheck((Func_2_t3270419407 *)L_6);
			KeyValuePair_2_t2530217319  L_8 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3270419407 *)L_6, (DictionaryEntry_t3123975638 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			NullCheck((List_1_t4002292061 *)L_5);
			((  void (*) (List_1_t4002292061 *, KeyValuePair_2_t2530217319 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t4002292061 *)L_5, (KeyValuePair_2_t2530217319 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		}

IL_002d:
		{
			RuntimeObject* L_9 = V_1;
			NullCheck((RuntimeObject*)L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_9);
			if (L_10)
			{
				goto IL_0014;
			}
		}

IL_0035:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_11 = V_1;
			if (!L_11)
			{
				goto IL_0040;
			}
		}

IL_003a:
		{
			RuntimeObject* L_12 = V_1;
			NullCheck((RuntimeObject*)L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_12);
		}

IL_0040:
		{
			IL2CPP_END_FINALLY(55)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0041:
	{
		List_1_t4002292061 * L_13 = V_0;
		return L_13;
	}
}
// System.Int32 System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetCount(System.Boolean)
extern "C"  int32_t SelectEnumerableIterator_2_GetCount_m357576939_gshared (SelectEnumerableIterator_2_t760503204 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectEnumerableIterator_2_GetCount_m357576939_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject* V_1 = NULL;
	DictionaryEntry_t3123975638  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ___onlyIfCheap0;
		if (!L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (-1);
	}

IL_0005:
	{
		V_0 = (int32_t)0;
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.DictionaryEntry>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_1);
		V_1 = (RuntimeObject*)L_2;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002d;
		}

IL_0015:
		{
			RuntimeObject* L_3 = V_1;
			NullCheck((RuntimeObject*)L_3);
			DictionaryEntry_t3123975638  L_4 = InterfaceFuncInvoker0< DictionaryEntry_t3123975638  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_3);
			V_2 = (DictionaryEntry_t3123975638 )L_4;
			Func_2_t3270419407 * L_5 = (Func_2_t3270419407 *)__this->get__selector_4();
			DictionaryEntry_t3123975638  L_6 = V_2;
			NullCheck((Func_2_t3270419407 *)L_5);
			((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3270419407 *)L_5, (DictionaryEntry_t3123975638 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			int32_t L_7 = V_0;
			if (((int64_t)L_7 + (int64_t)1 < (int64_t)kIl2CppInt32Min) || ((int64_t)L_7 + (int64_t)1 > (int64_t)kIl2CppInt32Max))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
			V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
		}

IL_002d:
		{
			RuntimeObject* L_8 = V_1;
			NullCheck((RuntimeObject*)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_8);
			if (L_9)
			{
				goto IL_0015;
			}
		}

IL_0035:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_10 = V_1;
			if (!L_10)
			{
				goto IL_0040;
			}
		}

IL_003a:
		{
			RuntimeObject* L_11 = V_1;
			NullCheck((RuntimeObject*)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_11);
		}

IL_0040:
		{
			IL2CPP_END_FINALLY(55)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0041:
	{
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.Void System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
extern "C"  void SelectEnumerableIterator_2__ctor_m1300772613_gshared (SelectEnumerableIterator_2_t956622511 * __this, RuntimeObject* ___source0, Func_2_t3466538714 * ___selector1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		((  void (*) (Iterator_1_t1484577656 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t1484577656 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set__source_3(L_0);
		Func_2_t3466538714 * L_1 = ___selector1;
		__this->set__selector_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clone()
extern "C"  Iterator_1_t1484577656 * SelectEnumerableIterator_2_Clone_m646357888_gshared (SelectEnumerableIterator_2_t956622511 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		Func_2_t3466538714 * L_1 = (Func_2_t3466538714 *)__this->get__selector_4();
		SelectEnumerableIterator_2_t956622511 * L_2 = (SelectEnumerableIterator_2_t956622511 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (SelectEnumerableIterator_2_t956622511 *, RuntimeObject*, Func_2_t3466538714 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (RuntimeObject*)L_0, (Func_2_t3466538714 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void SelectEnumerableIterator_2_Dispose_m3571586847_gshared (SelectEnumerableIterator_2_t956622511 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectEnumerableIterator_2_Dispose_m3571586847_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__enumerator_5();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
		__this->set__enumerator_5((RuntimeObject*)NULL);
	}

IL_001a:
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		((  void (*) (Iterator_1_t1484577656 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Iterator_1_t1484577656 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool SelectEnumerableIterator_2_MoveNext_m2562910218_gshared (SelectEnumerableIterator_2_t956622511 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectEnumerableIterator_2_MoveNext_m2562910218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t1484577656 *)__this)->get__state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0029;
		}
	}
	{
		goto IL_005a;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set__enumerator_5(L_4);
		((Iterator_1_t1484577656 *)__this)->set__state_1(2);
	}

IL_0029:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_5);
		if (!L_6)
		{
			goto IL_0054;
		}
	}
	{
		Func_2_t3466538714 * L_7 = (Func_2_t3466538714 *)__this->get__selector_4();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_8);
		KeyValuePair_2_t2530217319  L_9 = InterfaceFuncInvoker0< KeyValuePair_2_t2530217319  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_8);
		NullCheck((Func_2_t3466538714 *)L_7);
		KeyValuePair_2_t2530217319  L_10 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3466538714 *, KeyValuePair_2_t2530217319 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3466538714 *)L_7, (KeyValuePair_2_t2530217319 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		((Iterator_1_t1484577656 *)__this)->set__current_2(L_10);
		return (bool)1;
	}

IL_0054:
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose() */, (Iterator_1_t1484577656 *)__this);
	}

IL_005a:
	{
		return (bool)0;
	}
}
// TResult[] System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t118269214* SelectEnumerableIterator_2_ToArray_m675881335_gshared (SelectEnumerableIterator_2_t956622511 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectEnumerableIterator_2_ToArray_m675881335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LargeArrayBuilder_1_t2440570340  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RuntimeObject* V_1 = NULL;
	KeyValuePair_2_t2530217319  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		LargeArrayBuilder_1__ctor_m2226121364((LargeArrayBuilder_1_t2440570340 *)(&V_0), (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_0);
		V_1 = (RuntimeObject*)L_1;
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0030;
		}

IL_0016:
		{
			RuntimeObject* L_2 = V_1;
			NullCheck((RuntimeObject*)L_2);
			KeyValuePair_2_t2530217319  L_3 = InterfaceFuncInvoker0< KeyValuePair_2_t2530217319  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_2);
			V_2 = (KeyValuePair_2_t2530217319 )L_3;
			Func_2_t3466538714 * L_4 = (Func_2_t3466538714 *)__this->get__selector_4();
			KeyValuePair_2_t2530217319  L_5 = V_2;
			NullCheck((Func_2_t3466538714 *)L_4);
			KeyValuePair_2_t2530217319  L_6 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3466538714 *, KeyValuePair_2_t2530217319 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3466538714 *)L_4, (KeyValuePair_2_t2530217319 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			LargeArrayBuilder_1_Add_m982335036((LargeArrayBuilder_1_t2440570340 *)(&V_0), (KeyValuePair_2_t2530217319 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_0030:
		{
			RuntimeObject* L_7 = V_1;
			NullCheck((RuntimeObject*)L_7);
			bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_7);
			if (L_8)
			{
				goto IL_0016;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x44, FINALLY_003a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_9 = V_1;
			if (!L_9)
			{
				goto IL_0043;
			}
		}

IL_003d:
		{
			RuntimeObject* L_10 = V_1;
			NullCheck((RuntimeObject*)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_10);
		}

IL_0043:
		{
			IL2CPP_END_FINALLY(58)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0044:
	{
		KeyValuePair_2U5BU5D_t118269214* L_11 = LargeArrayBuilder_1_ToArray_m3030376891((LargeArrayBuilder_1_t2440570340 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_11;
	}
}
// System.Collections.Generic.List`1<TResult> System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToList()
extern "C"  List_1_t4002292061 * SelectEnumerableIterator_2_ToList_m2709422520_gshared (SelectEnumerableIterator_2_t956622511 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectEnumerableIterator_2_ToList_m2709422520_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t4002292061 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	KeyValuePair_2_t2530217319  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t4002292061 * L_0 = (List_1_t4002292061 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((  void (*) (List_1_t4002292061 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		V_0 = (List_1_t4002292061 *)L_0;
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_1);
		V_1 = (RuntimeObject*)L_2;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002d;
		}

IL_0014:
		{
			RuntimeObject* L_3 = V_1;
			NullCheck((RuntimeObject*)L_3);
			KeyValuePair_2_t2530217319  L_4 = InterfaceFuncInvoker0< KeyValuePair_2_t2530217319  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_3);
			V_2 = (KeyValuePair_2_t2530217319 )L_4;
			List_1_t4002292061 * L_5 = V_0;
			Func_2_t3466538714 * L_6 = (Func_2_t3466538714 *)__this->get__selector_4();
			KeyValuePair_2_t2530217319  L_7 = V_2;
			NullCheck((Func_2_t3466538714 *)L_6);
			KeyValuePair_2_t2530217319  L_8 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3466538714 *, KeyValuePair_2_t2530217319 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3466538714 *)L_6, (KeyValuePair_2_t2530217319 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			NullCheck((List_1_t4002292061 *)L_5);
			((  void (*) (List_1_t4002292061 *, KeyValuePair_2_t2530217319 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t4002292061 *)L_5, (KeyValuePair_2_t2530217319 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		}

IL_002d:
		{
			RuntimeObject* L_9 = V_1;
			NullCheck((RuntimeObject*)L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_9);
			if (L_10)
			{
				goto IL_0014;
			}
		}

IL_0035:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_11 = V_1;
			if (!L_11)
			{
				goto IL_0040;
			}
		}

IL_003a:
		{
			RuntimeObject* L_12 = V_1;
			NullCheck((RuntimeObject*)L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_12);
		}

IL_0040:
		{
			IL2CPP_END_FINALLY(55)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0041:
	{
		List_1_t4002292061 * L_13 = V_0;
		return L_13;
	}
}
// System.Int32 System.Linq.Enumerable/SelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetCount(System.Boolean)
extern "C"  int32_t SelectEnumerableIterator_2_GetCount_m2168038858_gshared (SelectEnumerableIterator_2_t956622511 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectEnumerableIterator_2_GetCount_m2168038858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject* V_1 = NULL;
	KeyValuePair_2_t2530217319  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ___onlyIfCheap0;
		if (!L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (-1);
	}

IL_0005:
	{
		V_0 = (int32_t)0;
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_1);
		V_1 = (RuntimeObject*)L_2;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002d;
		}

IL_0015:
		{
			RuntimeObject* L_3 = V_1;
			NullCheck((RuntimeObject*)L_3);
			KeyValuePair_2_t2530217319  L_4 = InterfaceFuncInvoker0< KeyValuePair_2_t2530217319  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_3);
			V_2 = (KeyValuePair_2_t2530217319 )L_4;
			Func_2_t3466538714 * L_5 = (Func_2_t3466538714 *)__this->get__selector_4();
			KeyValuePair_2_t2530217319  L_6 = V_2;
			NullCheck((Func_2_t3466538714 *)L_5);
			((  KeyValuePair_2_t2530217319  (*) (Func_2_t3466538714 *, KeyValuePair_2_t2530217319 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3466538714 *)L_5, (KeyValuePair_2_t2530217319 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			int32_t L_7 = V_0;
			if (((int64_t)L_7 + (int64_t)1 < (int64_t)kIl2CppInt32Min) || ((int64_t)L_7 + (int64_t)1 > (int64_t)kIl2CppInt32Max))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
			V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
		}

IL_002d:
		{
			RuntimeObject* L_8 = V_1;
			NullCheck((RuntimeObject*)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_8);
			if (L_9)
			{
				goto IL_0015;
			}
		}

IL_0035:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_10 = V_1;
			if (!L_10)
			{
				goto IL_0040;
			}
		}

IL_003a:
		{
			RuntimeObject* L_11 = V_1;
			NullCheck((RuntimeObject*)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_11);
		}

IL_0040:
		{
			IL2CPP_END_FINALLY(55)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0041:
	{
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.Void System.Linq.Enumerable/SelectEnumerableIterator`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
extern "C"  void SelectEnumerableIterator_2__ctor_m1944375350_gshared (SelectEnumerableIterator_2_t4232181467 * __this, RuntimeObject* ___source0, Func_2_t2447130374 * ___selector1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		((  void (*) (Iterator_1_t2034466501 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t2034466501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set__source_3(L_0);
		Func_2_t2447130374 * L_1 = ___selector1;
		__this->set__selector_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/SelectEnumerableIterator`2<System.Object,System.Object>::Clone()
extern "C"  Iterator_1_t2034466501 * SelectEnumerableIterator_2_Clone_m1317343610_gshared (SelectEnumerableIterator_2_t4232181467 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		Func_2_t2447130374 * L_1 = (Func_2_t2447130374 *)__this->get__selector_4();
		SelectEnumerableIterator_2_t4232181467 * L_2 = (SelectEnumerableIterator_2_t4232181467 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (SelectEnumerableIterator_2_t4232181467 *, RuntimeObject*, Func_2_t2447130374 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (RuntimeObject*)L_0, (Func_2_t2447130374 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Linq.Enumerable/SelectEnumerableIterator`2<System.Object,System.Object>::Dispose()
extern "C"  void SelectEnumerableIterator_2_Dispose_m2790816898_gshared (SelectEnumerableIterator_2_t4232181467 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectEnumerableIterator_2_Dispose_m2790816898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__enumerator_5();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
		__this->set__enumerator_5((RuntimeObject*)NULL);
	}

IL_001a:
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		((  void (*) (Iterator_1_t2034466501 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Iterator_1_t2034466501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/SelectEnumerableIterator`2<System.Object,System.Object>::MoveNext()
extern "C"  bool SelectEnumerableIterator_2_MoveNext_m1563322355_gshared (SelectEnumerableIterator_2_t4232181467 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectEnumerableIterator_2_MoveNext_m1563322355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t2034466501 *)__this)->get__state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0029;
		}
	}
	{
		goto IL_005a;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set__enumerator_5(L_4);
		((Iterator_1_t2034466501 *)__this)->set__state_1(2);
	}

IL_0029:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_5);
		if (!L_6)
		{
			goto IL_0054;
		}
	}
	{
		Func_2_t2447130374 * L_7 = (Func_2_t2447130374 *)__this->get__selector_4();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_8);
		RuntimeObject * L_9 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_8);
		NullCheck((Func_2_t2447130374 *)L_7);
		RuntimeObject * L_10 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t2447130374 *)L_7, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		((Iterator_1_t2034466501 *)__this)->set__current_2(L_10);
		return (bool)1;
	}

IL_0054:
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t2034466501 *)__this);
	}

IL_005a:
	{
		return (bool)0;
	}
}
// TResult[] System.Linq.Enumerable/SelectEnumerableIterator`2<System.Object,System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t2843939325* SelectEnumerableIterator_2_ToArray_m3280562849_gshared (SelectEnumerableIterator_2_t4232181467 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectEnumerableIterator_2_ToArray_m3280562849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LargeArrayBuilder_1_t2990459185  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RuntimeObject* V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		LargeArrayBuilder_1__ctor_m104969882((LargeArrayBuilder_1_t2990459185 *)(&V_0), (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_0);
		V_1 = (RuntimeObject*)L_1;
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0030;
		}

IL_0016:
		{
			RuntimeObject* L_2 = V_1;
			NullCheck((RuntimeObject*)L_2);
			RuntimeObject * L_3 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_2);
			V_2 = (RuntimeObject *)L_3;
			Func_2_t2447130374 * L_4 = (Func_2_t2447130374 *)__this->get__selector_4();
			RuntimeObject * L_5 = V_2;
			NullCheck((Func_2_t2447130374 *)L_4);
			RuntimeObject * L_6 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t2447130374 *)L_4, (RuntimeObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			LargeArrayBuilder_1_Add_m3802412589((LargeArrayBuilder_1_t2990459185 *)(&V_0), (RuntimeObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_0030:
		{
			RuntimeObject* L_7 = V_1;
			NullCheck((RuntimeObject*)L_7);
			bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_7);
			if (L_8)
			{
				goto IL_0016;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x44, FINALLY_003a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_9 = V_1;
			if (!L_9)
			{
				goto IL_0043;
			}
		}

IL_003d:
		{
			RuntimeObject* L_10 = V_1;
			NullCheck((RuntimeObject*)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_10);
		}

IL_0043:
		{
			IL2CPP_END_FINALLY(58)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0044:
	{
		ObjectU5BU5D_t2843939325* L_11 = LargeArrayBuilder_1_ToArray_m390648332((LargeArrayBuilder_1_t2990459185 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_11;
	}
}
// System.Collections.Generic.List`1<TResult> System.Linq.Enumerable/SelectEnumerableIterator`2<System.Object,System.Object>::ToList()
extern "C"  List_1_t257213610 * SelectEnumerableIterator_2_ToList_m1184928937_gshared (SelectEnumerableIterator_2_t4232181467 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectEnumerableIterator_2_ToList_m1184928937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t257213610 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((  void (*) (List_1_t257213610 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		V_0 = (List_1_t257213610 *)L_0;
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_1);
		V_1 = (RuntimeObject*)L_2;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002d;
		}

IL_0014:
		{
			RuntimeObject* L_3 = V_1;
			NullCheck((RuntimeObject*)L_3);
			RuntimeObject * L_4 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_3);
			V_2 = (RuntimeObject *)L_4;
			List_1_t257213610 * L_5 = V_0;
			Func_2_t2447130374 * L_6 = (Func_2_t2447130374 *)__this->get__selector_4();
			RuntimeObject * L_7 = V_2;
			NullCheck((Func_2_t2447130374 *)L_6);
			RuntimeObject * L_8 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t2447130374 *)L_6, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			NullCheck((List_1_t257213610 *)L_5);
			((  void (*) (List_1_t257213610 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t257213610 *)L_5, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		}

IL_002d:
		{
			RuntimeObject* L_9 = V_1;
			NullCheck((RuntimeObject*)L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_9);
			if (L_10)
			{
				goto IL_0014;
			}
		}

IL_0035:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_11 = V_1;
			if (!L_11)
			{
				goto IL_0040;
			}
		}

IL_003a:
		{
			RuntimeObject* L_12 = V_1;
			NullCheck((RuntimeObject*)L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_12);
		}

IL_0040:
		{
			IL2CPP_END_FINALLY(55)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0041:
	{
		List_1_t257213610 * L_13 = V_0;
		return L_13;
	}
}
// System.Int32 System.Linq.Enumerable/SelectEnumerableIterator`2<System.Object,System.Object>::GetCount(System.Boolean)
extern "C"  int32_t SelectEnumerableIterator_2_GetCount_m149151510_gshared (SelectEnumerableIterator_2_t4232181467 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectEnumerableIterator_2_GetCount_m149151510_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject* V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ___onlyIfCheap0;
		if (!L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (-1);
	}

IL_0005:
	{
		V_0 = (int32_t)0;
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_1);
		V_1 = (RuntimeObject*)L_2;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002d;
		}

IL_0015:
		{
			RuntimeObject* L_3 = V_1;
			NullCheck((RuntimeObject*)L_3);
			RuntimeObject * L_4 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_3);
			V_2 = (RuntimeObject *)L_4;
			Func_2_t2447130374 * L_5 = (Func_2_t2447130374 *)__this->get__selector_4();
			RuntimeObject * L_6 = V_2;
			NullCheck((Func_2_t2447130374 *)L_5);
			((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t2447130374 *)L_5, (RuntimeObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			int32_t L_7 = V_0;
			if (((int64_t)L_7 + (int64_t)1 < (int64_t)kIl2CppInt32Min) || ((int64_t)L_7 + (int64_t)1 > (int64_t)kIl2CppInt32Max))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
			V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
		}

IL_002d:
		{
			RuntimeObject* L_8 = V_1;
			NullCheck((RuntimeObject*)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_8);
			if (L_9)
			{
				goto IL_0015;
			}
		}

IL_0035:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_10 = V_1;
			if (!L_10)
			{
				goto IL_0040;
			}
		}

IL_003a:
		{
			RuntimeObject* L_11 = V_1;
			NullCheck((RuntimeObject*)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_11);
		}

IL_0040:
		{
			IL2CPP_END_FINALLY(55)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0041:
	{
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.Void System.Linq.Enumerable/SelectIListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IList`1<TSource>,System.Func`2<TSource,TResult>)
extern "C"  void SelectIListIterator_2__ctor_m3687113827_gshared (SelectIListIterator_2_t130090036 * __this, RuntimeObject* ___source0, Func_2_t3270419407 * ___selector1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		((  void (*) (Iterator_1_t1484577656 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t1484577656 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set__source_3(L_0);
		Func_2_t3270419407 * L_1 = ___selector1;
		__this->set__selector_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/SelectIListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clone()
extern "C"  Iterator_1_t1484577656 * SelectIListIterator_2_Clone_m2557005930_gshared (SelectIListIterator_2_t130090036 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		Func_2_t3270419407 * L_1 = (Func_2_t3270419407 *)__this->get__selector_4();
		SelectIListIterator_2_t130090036 * L_2 = (SelectIListIterator_2_t130090036 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (SelectIListIterator_2_t130090036 *, RuntimeObject*, Func_2_t3270419407 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (RuntimeObject*)L_0, (Func_2_t3270419407 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Boolean System.Linq.Enumerable/SelectIListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool SelectIListIterator_2_MoveNext_m4196171901_gshared (SelectIListIterator_2_t130090036 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIListIterator_2_MoveNext_m4196171901_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t1484577656 *)__this)->get__state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0029;
		}
	}
	{
		goto IL_005a;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.DictionaryEntry>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_3);
		__this->set__enumerator_5(L_4);
		((Iterator_1_t1484577656 *)__this)->set__state_1(2);
	}

IL_0029:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_5);
		if (!L_6)
		{
			goto IL_0054;
		}
	}
	{
		Func_2_t3270419407 * L_7 = (Func_2_t3270419407 *)__this->get__selector_4();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_8);
		DictionaryEntry_t3123975638  L_9 = InterfaceFuncInvoker0< DictionaryEntry_t3123975638  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_8);
		NullCheck((Func_2_t3270419407 *)L_7);
		KeyValuePair_2_t2530217319  L_10 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3270419407 *)L_7, (DictionaryEntry_t3123975638 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		((Iterator_1_t1484577656 *)__this)->set__current_2(L_10);
		return (bool)1;
	}

IL_0054:
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose() */, (Iterator_1_t1484577656 *)__this);
	}

IL_005a:
	{
		return (bool)0;
	}
}
// System.Void System.Linq.Enumerable/SelectIListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void SelectIListIterator_2_Dispose_m3100268919_gshared (SelectIListIterator_2_t130090036 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIListIterator_2_Dispose_m3100268919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__enumerator_5();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
		__this->set__enumerator_5((RuntimeObject*)NULL);
	}

IL_001a:
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		((  void (*) (Iterator_1_t1484577656 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Iterator_1_t1484577656 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// TResult[] System.Linq.Enumerable/SelectIListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t118269214* SelectIListIterator_2_ToArray_m3830241284_gshared (SelectIListIterator_2_t130090036 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2U5BU5D_t118269214* V_1 = NULL;
	int32_t V_2 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.DictionaryEntry>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		KeyValuePair_2U5BU5D_t118269214* L_3 = ((  KeyValuePair_2U5BU5D_t118269214* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_3;
	}

IL_0015:
	{
		int32_t L_4 = V_0;
		V_1 = (KeyValuePair_2U5BU5D_t118269214*)((KeyValuePair_2U5BU5D_t118269214*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10), (uint32_t)L_4));
		V_2 = (int32_t)0;
		goto IL_0042;
	}

IL_0020:
	{
		KeyValuePair_2U5BU5D_t118269214* L_5 = V_1;
		int32_t L_6 = V_2;
		Func_2_t3270419407 * L_7 = (Func_2_t3270419407 *)__this->get__selector_4();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get__source_3();
		int32_t L_9 = V_2;
		NullCheck((RuntimeObject*)L_8);
		DictionaryEntry_t3123975638  L_10 = InterfaceFuncInvoker1< DictionaryEntry_t3123975638 , int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Collections.DictionaryEntry>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (RuntimeObject*)L_8, (int32_t)L_9);
		NullCheck((Func_2_t3270419407 *)L_7);
		KeyValuePair_2_t2530217319  L_11 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3270419407 *)L_7, (DictionaryEntry_t3123975638 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (KeyValuePair_2_t2530217319 )L_11);
		int32_t L_12 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_13 = V_2;
		KeyValuePair_2U5BU5D_t118269214* L_14 = V_1;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_14)->max_length)))))))
		{
			goto IL_0020;
		}
	}
	{
		KeyValuePair_2U5BU5D_t118269214* L_15 = V_1;
		return L_15;
	}
}
// System.Collections.Generic.List`1<TResult> System.Linq.Enumerable/SelectIListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToList()
extern "C"  List_1_t4002292061 * SelectIListIterator_2_ToList_m3933437014_gshared (SelectIListIterator_2_t130090036 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	List_1_t4002292061 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.DictionaryEntry>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		List_1_t4002292061 * L_3 = (List_1_t4002292061 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12));
		((  void (*) (List_1_t4002292061 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)(L_3, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		V_1 = (List_1_t4002292061 *)L_3;
		V_2 = (int32_t)0;
		goto IL_0038;
	}

IL_0017:
	{
		List_1_t4002292061 * L_4 = V_1;
		Func_2_t3270419407 * L_5 = (Func_2_t3270419407 *)__this->get__selector_4();
		RuntimeObject* L_6 = (RuntimeObject*)__this->get__source_3();
		int32_t L_7 = V_2;
		NullCheck((RuntimeObject*)L_6);
		DictionaryEntry_t3123975638  L_8 = InterfaceFuncInvoker1< DictionaryEntry_t3123975638 , int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Collections.DictionaryEntry>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (RuntimeObject*)L_6, (int32_t)L_7);
		NullCheck((Func_2_t3270419407 *)L_5);
		KeyValuePair_2_t2530217319  L_9 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3270419407 *)L_5, (DictionaryEntry_t3123975638 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t4002292061 *)L_4);
		((  void (*) (List_1_t4002292061 *, KeyValuePair_2_t2530217319 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t4002292061 *)L_4, (KeyValuePair_2_t2530217319 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		int32_t L_10 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0038:
	{
		int32_t L_11 = V_2;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0017;
		}
	}
	{
		List_1_t4002292061 * L_13 = V_1;
		return L_13;
	}
}
// System.Int32 System.Linq.Enumerable/SelectIListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetCount(System.Boolean)
extern "C"  int32_t SelectIListIterator_2_GetCount_m885313721_gshared (SelectIListIterator_2_t130090036 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.DictionaryEntry>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		bool L_2 = ___onlyIfCheap0;
		if (L_2)
		{
			goto IL_0033;
		}
	}
	{
		V_1 = (int32_t)0;
		goto IL_002f;
	}

IL_0013:
	{
		Func_2_t3270419407 * L_3 = (Func_2_t3270419407 *)__this->get__selector_4();
		RuntimeObject* L_4 = (RuntimeObject*)__this->get__source_3();
		int32_t L_5 = V_1;
		NullCheck((RuntimeObject*)L_4);
		DictionaryEntry_t3123975638  L_6 = InterfaceFuncInvoker1< DictionaryEntry_t3123975638 , int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Collections.DictionaryEntry>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (RuntimeObject*)L_4, (int32_t)L_5);
		NullCheck((Func_2_t3270419407 *)L_3);
		((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3270419407 *)L_3, (DictionaryEntry_t3123975638 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_8 = V_1;
		int32_t L_9 = V_0;
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0013;
		}
	}

IL_0033:
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// TResult System.Linq.Enumerable/SelectIListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetElementAt(System.Int32,System.Boolean&)
extern "C"  KeyValuePair_2_t2530217319  SelectIListIterator_2_TryGetElementAt_m3366412195_gshared (SelectIListIterator_2_t130090036 * __this, int32_t ___index0, bool* ___found1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIListIterator_2_TryGetElementAt_m3366412195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2530217319  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.DictionaryEntry>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_1);
		if ((!(((uint32_t)L_0) < ((uint32_t)L_2))))
		{
			goto IL_0029;
		}
	}
	{
		bool* L_3 = ___found1;
		*((int8_t*)(L_3)) = (int8_t)1;
		Func_2_t3270419407 * L_4 = (Func_2_t3270419407 *)__this->get__selector_4();
		RuntimeObject* L_5 = (RuntimeObject*)__this->get__source_3();
		int32_t L_6 = ___index0;
		NullCheck((RuntimeObject*)L_5);
		DictionaryEntry_t3123975638  L_7 = InterfaceFuncInvoker1< DictionaryEntry_t3123975638 , int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Collections.DictionaryEntry>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (RuntimeObject*)L_5, (int32_t)L_6);
		NullCheck((Func_2_t3270419407 *)L_4);
		KeyValuePair_2_t2530217319  L_8 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3270419407 *)L_4, (DictionaryEntry_t3123975638 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_8;
	}

IL_0029:
	{
		bool* L_9 = ___found1;
		*((int8_t*)(L_9)) = (int8_t)0;
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_0));
		KeyValuePair_2_t2530217319  L_10 = V_0;
		return L_10;
	}
}
// TResult System.Linq.Enumerable/SelectIListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetFirst(System.Boolean&)
extern "C"  KeyValuePair_2_t2530217319  SelectIListIterator_2_TryGetFirst_m4235623572_gshared (SelectIListIterator_2_t130090036 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIListIterator_2_TryGetFirst_m4235623572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2530217319  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.DictionaryEntry>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		bool* L_2 = ___found0;
		*((int8_t*)(L_2)) = (int8_t)1;
		Func_2_t3270419407 * L_3 = (Func_2_t3270419407 *)__this->get__selector_4();
		RuntimeObject* L_4 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_4);
		DictionaryEntry_t3123975638  L_5 = InterfaceFuncInvoker1< DictionaryEntry_t3123975638 , int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Collections.DictionaryEntry>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (RuntimeObject*)L_4, (int32_t)0);
		NullCheck((Func_2_t3270419407 *)L_3);
		KeyValuePair_2_t2530217319  L_6 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3270419407 *)L_3, (DictionaryEntry_t3123975638 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_6;
	}

IL_0028:
	{
		bool* L_7 = ___found0;
		*((int8_t*)(L_7)) = (int8_t)0;
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_0));
		KeyValuePair_2_t2530217319  L_8 = V_0;
		return L_8;
	}
}
// TResult System.Linq.Enumerable/SelectIListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetLast(System.Boolean&)
extern "C"  KeyValuePair_2_t2530217319  SelectIListIterator_2_TryGetLast_m3204209618_gshared (SelectIListIterator_2_t130090036 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIListIterator_2_TryGetLast_m3204209618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t2530217319  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.DictionaryEntry>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		bool* L_3 = ___found0;
		*((int8_t*)(L_3)) = (int8_t)1;
		Func_2_t3270419407 * L_4 = (Func_2_t3270419407 *)__this->get__selector_4();
		RuntimeObject* L_5 = (RuntimeObject*)__this->get__source_3();
		int32_t L_6 = V_0;
		NullCheck((RuntimeObject*)L_5);
		DictionaryEntry_t3123975638  L_7 = InterfaceFuncInvoker1< DictionaryEntry_t3123975638 , int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Collections.DictionaryEntry>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (RuntimeObject*)L_5, (int32_t)((int32_t)((int32_t)L_6-(int32_t)1)));
		NullCheck((Func_2_t3270419407 *)L_4);
		KeyValuePair_2_t2530217319  L_8 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3270419407 *)L_4, (DictionaryEntry_t3123975638 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_8;
	}

IL_002c:
	{
		bool* L_9 = ___found0;
		*((int8_t*)(L_9)) = (int8_t)0;
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_1));
		KeyValuePair_2_t2530217319  L_10 = V_1;
		return L_10;
	}
}
// System.Void System.Linq.Enumerable/SelectIListIterator`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IList`1<TSource>,System.Func`2<TSource,TResult>)
extern "C"  void SelectIListIterator_2__ctor_m187568293_gshared (SelectIListIterator_2_t3601768299 * __this, RuntimeObject* ___source0, Func_2_t2447130374 * ___selector1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		((  void (*) (Iterator_1_t2034466501 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t2034466501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set__source_3(L_0);
		Func_2_t2447130374 * L_1 = ___selector1;
		__this->set__selector_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/SelectIListIterator`2<System.Object,System.Object>::Clone()
extern "C"  Iterator_1_t2034466501 * SelectIListIterator_2_Clone_m614505362_gshared (SelectIListIterator_2_t3601768299 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		Func_2_t2447130374 * L_1 = (Func_2_t2447130374 *)__this->get__selector_4();
		SelectIListIterator_2_t3601768299 * L_2 = (SelectIListIterator_2_t3601768299 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (SelectIListIterator_2_t3601768299 *, RuntimeObject*, Func_2_t2447130374 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (RuntimeObject*)L_0, (Func_2_t2447130374 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Boolean System.Linq.Enumerable/SelectIListIterator`2<System.Object,System.Object>::MoveNext()
extern "C"  bool SelectIListIterator_2_MoveNext_m4209761780_gshared (SelectIListIterator_2_t3601768299 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIListIterator_2_MoveNext_m4209761780_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t2034466501 *)__this)->get__state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0029;
		}
	}
	{
		goto IL_005a;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_3);
		__this->set__enumerator_5(L_4);
		((Iterator_1_t2034466501 *)__this)->set__state_1(2);
	}

IL_0029:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_5);
		if (!L_6)
		{
			goto IL_0054;
		}
	}
	{
		Func_2_t2447130374 * L_7 = (Func_2_t2447130374 *)__this->get__selector_4();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_8);
		RuntimeObject * L_9 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_8);
		NullCheck((Func_2_t2447130374 *)L_7);
		RuntimeObject * L_10 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2447130374 *)L_7, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		((Iterator_1_t2034466501 *)__this)->set__current_2(L_10);
		return (bool)1;
	}

IL_0054:
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t2034466501 *)__this);
	}

IL_005a:
	{
		return (bool)0;
	}
}
// System.Void System.Linq.Enumerable/SelectIListIterator`2<System.Object,System.Object>::Dispose()
extern "C"  void SelectIListIterator_2_Dispose_m1388706584_gshared (SelectIListIterator_2_t3601768299 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIListIterator_2_Dispose_m1388706584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__enumerator_5();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
		__this->set__enumerator_5((RuntimeObject*)NULL);
	}

IL_001a:
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		((  void (*) (Iterator_1_t2034466501 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Iterator_1_t2034466501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// TResult[] System.Linq.Enumerable/SelectIListIterator`2<System.Object,System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t2843939325* SelectIListIterator_2_ToArray_m2011540596_gshared (SelectIListIterator_2_t3601768299 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	ObjectU5BU5D_t2843939325* V_1 = NULL;
	int32_t V_2 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ((  ObjectU5BU5D_t2843939325* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_3;
	}

IL_0015:
	{
		int32_t L_4 = V_0;
		V_1 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10), (uint32_t)L_4));
		V_2 = (int32_t)0;
		goto IL_0042;
	}

IL_0020:
	{
		ObjectU5BU5D_t2843939325* L_5 = V_1;
		int32_t L_6 = V_2;
		Func_2_t2447130374 * L_7 = (Func_2_t2447130374 *)__this->get__selector_4();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get__source_3();
		int32_t L_9 = V_2;
		NullCheck((RuntimeObject*)L_8);
		RuntimeObject * L_10 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (RuntimeObject*)L_8, (int32_t)L_9);
		NullCheck((Func_2_t2447130374 *)L_7);
		RuntimeObject * L_11 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2447130374 *)L_7, (RuntimeObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (RuntimeObject *)L_11);
		int32_t L_12 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_13 = V_2;
		ObjectU5BU5D_t2843939325* L_14 = V_1;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_14)->max_length)))))))
		{
			goto IL_0020;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_15 = V_1;
		return L_15;
	}
}
// System.Collections.Generic.List`1<TResult> System.Linq.Enumerable/SelectIListIterator`2<System.Object,System.Object>::ToList()
extern "C"  List_1_t257213610 * SelectIListIterator_2_ToList_m907852024_gshared (SelectIListIterator_2_t3601768299 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	List_1_t257213610 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		List_1_t257213610 * L_3 = (List_1_t257213610 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12));
		((  void (*) (List_1_t257213610 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)(L_3, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		V_1 = (List_1_t257213610 *)L_3;
		V_2 = (int32_t)0;
		goto IL_0038;
	}

IL_0017:
	{
		List_1_t257213610 * L_4 = V_1;
		Func_2_t2447130374 * L_5 = (Func_2_t2447130374 *)__this->get__selector_4();
		RuntimeObject* L_6 = (RuntimeObject*)__this->get__source_3();
		int32_t L_7 = V_2;
		NullCheck((RuntimeObject*)L_6);
		RuntimeObject * L_8 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (RuntimeObject*)L_6, (int32_t)L_7);
		NullCheck((Func_2_t2447130374 *)L_5);
		RuntimeObject * L_9 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2447130374 *)L_5, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t257213610 *)L_4);
		((  void (*) (List_1_t257213610 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t257213610 *)L_4, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		int32_t L_10 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0038:
	{
		int32_t L_11 = V_2;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0017;
		}
	}
	{
		List_1_t257213610 * L_13 = V_1;
		return L_13;
	}
}
// System.Int32 System.Linq.Enumerable/SelectIListIterator`2<System.Object,System.Object>::GetCount(System.Boolean)
extern "C"  int32_t SelectIListIterator_2_GetCount_m2783411396_gshared (SelectIListIterator_2_t3601768299 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		bool L_2 = ___onlyIfCheap0;
		if (L_2)
		{
			goto IL_0033;
		}
	}
	{
		V_1 = (int32_t)0;
		goto IL_002f;
	}

IL_0013:
	{
		Func_2_t2447130374 * L_3 = (Func_2_t2447130374 *)__this->get__selector_4();
		RuntimeObject* L_4 = (RuntimeObject*)__this->get__source_3();
		int32_t L_5 = V_1;
		NullCheck((RuntimeObject*)L_4);
		RuntimeObject * L_6 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (RuntimeObject*)L_4, (int32_t)L_5);
		NullCheck((Func_2_t2447130374 *)L_3);
		((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2447130374 *)L_3, (RuntimeObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_8 = V_1;
		int32_t L_9 = V_0;
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0013;
		}
	}

IL_0033:
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// TResult System.Linq.Enumerable/SelectIListIterator`2<System.Object,System.Object>::TryGetElementAt(System.Int32,System.Boolean&)
extern "C"  RuntimeObject * SelectIListIterator_2_TryGetElementAt_m2987796583_gshared (SelectIListIterator_2_t3601768299 * __this, int32_t ___index0, bool* ___found1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIListIterator_2_TryGetElementAt_m2987796583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		int32_t L_0 = ___index0;
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_1);
		if ((!(((uint32_t)L_0) < ((uint32_t)L_2))))
		{
			goto IL_0029;
		}
	}
	{
		bool* L_3 = ___found1;
		*((int8_t*)(L_3)) = (int8_t)1;
		Func_2_t2447130374 * L_4 = (Func_2_t2447130374 *)__this->get__selector_4();
		RuntimeObject* L_5 = (RuntimeObject*)__this->get__source_3();
		int32_t L_6 = ___index0;
		NullCheck((RuntimeObject*)L_5);
		RuntimeObject * L_7 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (RuntimeObject*)L_5, (int32_t)L_6);
		NullCheck((Func_2_t2447130374 *)L_4);
		RuntimeObject * L_8 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2447130374 *)L_4, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_8;
	}

IL_0029:
	{
		bool* L_9 = ___found1;
		*((int8_t*)(L_9)) = (int8_t)0;
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_0));
		RuntimeObject * L_10 = V_0;
		return L_10;
	}
}
// TResult System.Linq.Enumerable/SelectIListIterator`2<System.Object,System.Object>::TryGetFirst(System.Boolean&)
extern "C"  RuntimeObject * SelectIListIterator_2_TryGetFirst_m3430082063_gshared (SelectIListIterator_2_t3601768299 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIListIterator_2_TryGetFirst_m3430082063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		bool* L_2 = ___found0;
		*((int8_t*)(L_2)) = (int8_t)1;
		Func_2_t2447130374 * L_3 = (Func_2_t2447130374 *)__this->get__selector_4();
		RuntimeObject* L_4 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_4);
		RuntimeObject * L_5 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (RuntimeObject*)L_4, (int32_t)0);
		NullCheck((Func_2_t2447130374 *)L_3);
		RuntimeObject * L_6 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2447130374 *)L_3, (RuntimeObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_6;
	}

IL_0028:
	{
		bool* L_7 = ___found0;
		*((int8_t*)(L_7)) = (int8_t)0;
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_0));
		RuntimeObject * L_8 = V_0;
		return L_8;
	}
}
// TResult System.Linq.Enumerable/SelectIListIterator`2<System.Object,System.Object>::TryGetLast(System.Boolean&)
extern "C"  RuntimeObject * SelectIListIterator_2_TryGetLast_m2318008031_gshared (SelectIListIterator_2_t3601768299 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIListIterator_2_TryGetLast_m2318008031_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		bool* L_3 = ___found0;
		*((int8_t*)(L_3)) = (int8_t)1;
		Func_2_t2447130374 * L_4 = (Func_2_t2447130374 *)__this->get__selector_4();
		RuntimeObject* L_5 = (RuntimeObject*)__this->get__source_3();
		int32_t L_6 = V_0;
		NullCheck((RuntimeObject*)L_5);
		RuntimeObject * L_7 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (RuntimeObject*)L_5, (int32_t)((int32_t)((int32_t)L_6-(int32_t)1)));
		NullCheck((Func_2_t2447130374 *)L_4);
		RuntimeObject * L_8 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2447130374 *)L_4, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_8;
	}

IL_002c:
	{
		bool* L_9 = ___found0;
		*((int8_t*)(L_9)) = (int8_t)0;
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_1));
		RuntimeObject * L_10 = V_1;
		return L_10;
	}
}
// System.Void System.Linq.Enumerable/SelectIPartitionIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Linq.IPartition`1<TSource>,System.Func`2<TSource,TResult>)
extern "C"  void SelectIPartitionIterator_2__ctor_m1294395231_gshared (SelectIPartitionIterator_2_t2954477804 * __this, RuntimeObject* ___source0, Func_2_t3270419407 * ___selector1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		((  void (*) (Iterator_1_t1484577656 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t1484577656 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set__source_3(L_0);
		Func_2_t3270419407 * L_1 = ___selector1;
		__this->set__selector_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/SelectIPartitionIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clone()
extern "C"  Iterator_1_t1484577656 * SelectIPartitionIterator_2_Clone_m2144683163_gshared (SelectIPartitionIterator_2_t2954477804 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		Func_2_t3270419407 * L_1 = (Func_2_t3270419407 *)__this->get__selector_4();
		SelectIPartitionIterator_2_t2954477804 * L_2 = (SelectIPartitionIterator_2_t2954477804 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (SelectIPartitionIterator_2_t2954477804 *, RuntimeObject*, Func_2_t3270419407 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (RuntimeObject*)L_0, (Func_2_t3270419407 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Boolean System.Linq.Enumerable/SelectIPartitionIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool SelectIPartitionIterator_2_MoveNext_m2485470954_gshared (SelectIPartitionIterator_2_t2954477804 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_MoveNext_m2485470954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t1484577656 *)__this)->get__state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0029;
		}
	}
	{
		goto IL_005a;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.DictionaryEntry>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_3);
		__this->set__enumerator_5(L_4);
		((Iterator_1_t1484577656 *)__this)->set__state_1(2);
	}

IL_0029:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_5);
		if (!L_6)
		{
			goto IL_0054;
		}
	}
	{
		Func_2_t3270419407 * L_7 = (Func_2_t3270419407 *)__this->get__selector_4();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_8);
		DictionaryEntry_t3123975638  L_9 = InterfaceFuncInvoker0< DictionaryEntry_t3123975638  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_8);
		NullCheck((Func_2_t3270419407 *)L_7);
		KeyValuePair_2_t2530217319  L_10 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3270419407 *)L_7, (DictionaryEntry_t3123975638 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		((Iterator_1_t1484577656 *)__this)->set__current_2(L_10);
		return (bool)1;
	}

IL_0054:
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose() */, (Iterator_1_t1484577656 *)__this);
	}

IL_005a:
	{
		return (bool)0;
	}
}
// System.Void System.Linq.Enumerable/SelectIPartitionIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void SelectIPartitionIterator_2_Dispose_m405696305_gshared (SelectIPartitionIterator_2_t2954477804 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_Dispose_m405696305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__enumerator_5();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
		__this->set__enumerator_5((RuntimeObject*)NULL);
	}

IL_001a:
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		((  void (*) (Iterator_1_t1484577656 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Iterator_1_t1484577656 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// TResult System.Linq.Enumerable/SelectIPartitionIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetElementAt(System.Int32,System.Boolean&)
extern "C"  KeyValuePair_2_t2530217319  SelectIPartitionIterator_2_TryGetElementAt_m2269338986_gshared (SelectIPartitionIterator_2_t2954477804 * __this, int32_t ___index0, bool* ___found1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_TryGetElementAt_m2269338986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	DictionaryEntry_t3123975638  V_1;
	memset(&V_1, 0, sizeof(V_1));
	KeyValuePair_2_t2530217319  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		DictionaryEntry_t3123975638  L_2 = InterfaceFuncInvoker2< DictionaryEntry_t3123975638 , int32_t, bool* >::Invoke(0 /* TElement System.Linq.IPartition`1<System.Collections.DictionaryEntry>::TryGetElementAt(System.Int32,System.Boolean&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0, (int32_t)L_1, (bool*)(&V_0));
		V_1 = (DictionaryEntry_t3123975638 )L_2;
		bool* L_3 = ___found1;
		bool L_4 = V_0;
		*((int8_t*)(L_3)) = (int8_t)L_4;
		bool L_5 = V_0;
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_2));
		KeyValuePair_2_t2530217319  L_6 = V_2;
		return L_6;
	}

IL_001f:
	{
		Func_2_t3270419407 * L_7 = (Func_2_t3270419407 *)__this->get__selector_4();
		DictionaryEntry_t3123975638  L_8 = V_1;
		NullCheck((Func_2_t3270419407 *)L_7);
		KeyValuePair_2_t2530217319  L_9 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3270419407 *)L_7, (DictionaryEntry_t3123975638 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_9;
	}
}
// TResult System.Linq.Enumerable/SelectIPartitionIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetFirst(System.Boolean&)
extern "C"  KeyValuePair_2_t2530217319  SelectIPartitionIterator_2_TryGetFirst_m308715622_gshared (SelectIPartitionIterator_2_t2954477804 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_TryGetFirst_m308715622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	DictionaryEntry_t3123975638  V_1;
	memset(&V_1, 0, sizeof(V_1));
	KeyValuePair_2_t2530217319  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		DictionaryEntry_t3123975638  L_1 = InterfaceFuncInvoker1< DictionaryEntry_t3123975638 , bool* >::Invoke(1 /* TElement System.Linq.IPartition`1<System.Collections.DictionaryEntry>::TryGetFirst(System.Boolean&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0, (bool*)(&V_0));
		V_1 = (DictionaryEntry_t3123975638 )L_1;
		bool* L_2 = ___found0;
		bool L_3 = V_0;
		*((int8_t*)(L_2)) = (int8_t)L_3;
		bool L_4 = V_0;
		if (L_4)
		{
			goto IL_001e;
		}
	}
	{
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_2));
		KeyValuePair_2_t2530217319  L_5 = V_2;
		return L_5;
	}

IL_001e:
	{
		Func_2_t3270419407 * L_6 = (Func_2_t3270419407 *)__this->get__selector_4();
		DictionaryEntry_t3123975638  L_7 = V_1;
		NullCheck((Func_2_t3270419407 *)L_6);
		KeyValuePair_2_t2530217319  L_8 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3270419407 *)L_6, (DictionaryEntry_t3123975638 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_8;
	}
}
// TResult System.Linq.Enumerable/SelectIPartitionIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetLast(System.Boolean&)
extern "C"  KeyValuePair_2_t2530217319  SelectIPartitionIterator_2_TryGetLast_m3505464966_gshared (SelectIPartitionIterator_2_t2954477804 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_TryGetLast_m3505464966_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	DictionaryEntry_t3123975638  V_1;
	memset(&V_1, 0, sizeof(V_1));
	KeyValuePair_2_t2530217319  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		DictionaryEntry_t3123975638  L_1 = InterfaceFuncInvoker1< DictionaryEntry_t3123975638 , bool* >::Invoke(2 /* TElement System.Linq.IPartition`1<System.Collections.DictionaryEntry>::TryGetLast(System.Boolean&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0, (bool*)(&V_0));
		V_1 = (DictionaryEntry_t3123975638 )L_1;
		bool* L_2 = ___found0;
		bool L_3 = V_0;
		*((int8_t*)(L_2)) = (int8_t)L_3;
		bool L_4 = V_0;
		if (L_4)
		{
			goto IL_001e;
		}
	}
	{
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_2));
		KeyValuePair_2_t2530217319  L_5 = V_2;
		return L_5;
	}

IL_001e:
	{
		Func_2_t3270419407 * L_6 = (Func_2_t3270419407 *)__this->get__selector_4();
		DictionaryEntry_t3123975638  L_7 = V_1;
		NullCheck((Func_2_t3270419407 *)L_6);
		KeyValuePair_2_t2530217319  L_8 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3270419407 *)L_6, (DictionaryEntry_t3123975638 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_8;
	}
}
// TResult[] System.Linq.Enumerable/SelectIPartitionIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::LazyToArray()
extern "C"  KeyValuePair_2U5BU5D_t118269214* SelectIPartitionIterator_2_LazyToArray_m2882688373_gshared (SelectIPartitionIterator_2_t2954477804 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_LazyToArray_m2882688373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LargeArrayBuilder_1_t2440570340  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RuntimeObject* V_1 = NULL;
	DictionaryEntry_t3123975638  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		LargeArrayBuilder_1__ctor_m2226121364((LargeArrayBuilder_1_t2440570340 *)(&V_0), (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.DictionaryEntry>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_0);
		V_1 = (RuntimeObject*)L_1;
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0030;
		}

IL_0016:
		{
			RuntimeObject* L_2 = V_1;
			NullCheck((RuntimeObject*)L_2);
			DictionaryEntry_t3123975638  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3123975638  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_2);
			V_2 = (DictionaryEntry_t3123975638 )L_3;
			Func_2_t3270419407 * L_4 = (Func_2_t3270419407 *)__this->get__selector_4();
			DictionaryEntry_t3123975638  L_5 = V_2;
			NullCheck((Func_2_t3270419407 *)L_4);
			KeyValuePair_2_t2530217319  L_6 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3270419407 *)L_4, (DictionaryEntry_t3123975638 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			LargeArrayBuilder_1_Add_m982335036((LargeArrayBuilder_1_t2440570340 *)(&V_0), (KeyValuePair_2_t2530217319 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		}

IL_0030:
		{
			RuntimeObject* L_7 = V_1;
			NullCheck((RuntimeObject*)L_7);
			bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_7);
			if (L_8)
			{
				goto IL_0016;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x44, FINALLY_003a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_9 = V_1;
			if (!L_9)
			{
				goto IL_0043;
			}
		}

IL_003d:
		{
			RuntimeObject* L_10 = V_1;
			NullCheck((RuntimeObject*)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_10);
		}

IL_0043:
		{
			IL2CPP_END_FINALLY(58)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0044:
	{
		KeyValuePair_2U5BU5D_t118269214* L_11 = LargeArrayBuilder_1_ToArray_m3030376891((LargeArrayBuilder_1_t2440570340 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_11;
	}
}
// TResult[] System.Linq.Enumerable/SelectIPartitionIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::PreallocatingToArray(System.Int32)
extern "C"  KeyValuePair_2U5BU5D_t118269214* SelectIPartitionIterator_2_PreallocatingToArray_m4096222762_gshared (SelectIPartitionIterator_2_t2954477804 * __this, int32_t ___count0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_PreallocatingToArray_m4096222762_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2U5BU5D_t118269214* V_0 = NULL;
	int32_t V_1 = 0;
	RuntimeObject* V_2 = NULL;
	DictionaryEntry_t3123975638  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___count0;
		V_0 = (KeyValuePair_2U5BU5D_t118269214*)((KeyValuePair_2U5BU5D_t118269214*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12), (uint32_t)L_0));
		V_1 = (int32_t)0;
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.DictionaryEntry>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_1);
		V_2 = (RuntimeObject*)L_2;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0035;
		}

IL_0017:
		{
			RuntimeObject* L_3 = V_2;
			NullCheck((RuntimeObject*)L_3);
			DictionaryEntry_t3123975638  L_4 = InterfaceFuncInvoker0< DictionaryEntry_t3123975638  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_3);
			V_3 = (DictionaryEntry_t3123975638 )L_4;
			KeyValuePair_2U5BU5D_t118269214* L_5 = V_0;
			int32_t L_6 = V_1;
			Func_2_t3270419407 * L_7 = (Func_2_t3270419407 *)__this->get__selector_4();
			DictionaryEntry_t3123975638  L_8 = V_3;
			NullCheck((Func_2_t3270419407 *)L_7);
			KeyValuePair_2_t2530217319  L_9 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3270419407 *)L_7, (DictionaryEntry_t3123975638 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			NullCheck(L_5);
			(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (KeyValuePair_2_t2530217319 )L_9);
			int32_t L_10 = V_1;
			V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
		}

IL_0035:
		{
			RuntimeObject* L_11 = V_2;
			NullCheck((RuntimeObject*)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_11);
			if (L_12)
			{
				goto IL_0017;
			}
		}

IL_003d:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_13 = V_2;
			if (!L_13)
			{
				goto IL_0048;
			}
		}

IL_0042:
		{
			RuntimeObject* L_14 = V_2;
			NullCheck((RuntimeObject*)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		}

IL_0048:
		{
			IL2CPP_END_FINALLY(63)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0049:
	{
		KeyValuePair_2U5BU5D_t118269214* L_15 = V_0;
		return L_15;
	}
}
// TResult[] System.Linq.Enumerable/SelectIPartitionIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t118269214* SelectIPartitionIterator_2_ToArray_m442913566_gshared (SelectIPartitionIterator_2_t2954477804 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker1< int32_t, bool >::Invoke(2 /* System.Int32 System.Linq.IIListProvider`1<System.Collections.DictionaryEntry>::GetCount(System.Boolean) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13), (RuntimeObject*)L_0, (bool)1);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		if (!L_3)
		{
			goto IL_001d;
		}
	}
	{
		goto IL_0023;
	}

IL_0016:
	{
		NullCheck((SelectIPartitionIterator_2_t2954477804 *)__this);
		KeyValuePair_2U5BU5D_t118269214* L_4 = ((  KeyValuePair_2U5BU5D_t118269214* (*) (SelectIPartitionIterator_2_t2954477804 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((SelectIPartitionIterator_2_t2954477804 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_4;
	}

IL_001d:
	{
		KeyValuePair_2U5BU5D_t118269214* L_5 = ((  KeyValuePair_2U5BU5D_t118269214* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_5;
	}

IL_0023:
	{
		int32_t L_6 = V_0;
		NullCheck((SelectIPartitionIterator_2_t2954477804 *)__this);
		KeyValuePair_2U5BU5D_t118269214* L_7 = ((  KeyValuePair_2U5BU5D_t118269214* (*) (SelectIPartitionIterator_2_t2954477804 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((SelectIPartitionIterator_2_t2954477804 *)__this, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		return L_7;
	}
}
// System.Collections.Generic.List`1<TResult> System.Linq.Enumerable/SelectIPartitionIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToList()
extern "C"  List_1_t4002292061 * SelectIPartitionIterator_2_ToList_m531909262_gshared (SelectIPartitionIterator_2_t2954477804 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_ToList_m531909262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	List_1_t4002292061 * V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	DictionaryEntry_t3123975638  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker1< int32_t, bool >::Invoke(2 /* System.Int32 System.Linq.IIListProvider`1<System.Collections.DictionaryEntry>::GetCount(System.Boolean) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13), (RuntimeObject*)L_0, (bool)1);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		goto IL_0024;
	}

IL_0016:
	{
		List_1_t4002292061 * L_4 = (List_1_t4002292061 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 17));
		((  void (*) (List_1_t4002292061 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		V_1 = (List_1_t4002292061 *)L_4;
		goto IL_002b;
	}

IL_001e:
	{
		List_1_t4002292061 * L_5 = (List_1_t4002292061 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 17));
		((  void (*) (List_1_t4002292061 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)(L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		return L_5;
	}

IL_0024:
	{
		int32_t L_6 = V_0;
		List_1_t4002292061 * L_7 = (List_1_t4002292061 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 17));
		((  void (*) (List_1_t4002292061 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(L_7, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		V_1 = (List_1_t4002292061 *)L_7;
	}

IL_002b:
	{
		RuntimeObject* L_8 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_8);
		RuntimeObject* L_9 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.DictionaryEntry>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_8);
		V_2 = (RuntimeObject*)L_9;
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0052;
		}

IL_0039:
		{
			RuntimeObject* L_10 = V_2;
			NullCheck((RuntimeObject*)L_10);
			DictionaryEntry_t3123975638  L_11 = InterfaceFuncInvoker0< DictionaryEntry_t3123975638  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_10);
			V_3 = (DictionaryEntry_t3123975638 )L_11;
			List_1_t4002292061 * L_12 = V_1;
			Func_2_t3270419407 * L_13 = (Func_2_t3270419407 *)__this->get__selector_4();
			DictionaryEntry_t3123975638  L_14 = V_3;
			NullCheck((Func_2_t3270419407 *)L_13);
			KeyValuePair_2_t2530217319  L_15 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3270419407 *)L_13, (DictionaryEntry_t3123975638 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			NullCheck((List_1_t4002292061 *)L_12);
			((  void (*) (List_1_t4002292061 *, KeyValuePair_2_t2530217319 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((List_1_t4002292061 *)L_12, (KeyValuePair_2_t2530217319 )L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		}

IL_0052:
		{
			RuntimeObject* L_16 = V_2;
			NullCheck((RuntimeObject*)L_16);
			bool L_17 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_16);
			if (L_17)
			{
				goto IL_0039;
			}
		}

IL_005a:
		{
			IL2CPP_LEAVE(0x66, FINALLY_005c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_005c;
	}

FINALLY_005c:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_18 = V_2;
			if (!L_18)
			{
				goto IL_0065;
			}
		}

IL_005f:
		{
			RuntimeObject* L_19 = V_2;
			NullCheck((RuntimeObject*)L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_19);
		}

IL_0065:
		{
			IL2CPP_END_FINALLY(92)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(92)
	{
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0066:
	{
		List_1_t4002292061 * L_20 = V_1;
		return L_20;
	}
}
// System.Int32 System.Linq.Enumerable/SelectIPartitionIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetCount(System.Boolean)
extern "C"  int32_t SelectIPartitionIterator_2_GetCount_m3814325761_gshared (SelectIPartitionIterator_2_t2954477804 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_GetCount_m3814325761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	DictionaryEntry_t3123975638  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ___onlyIfCheap0;
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.DictionaryEntry>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_1);
		V_0 = (RuntimeObject*)L_2;
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0025;
		}

IL_0011:
		{
			RuntimeObject* L_3 = V_0;
			NullCheck((RuntimeObject*)L_3);
			DictionaryEntry_t3123975638  L_4 = InterfaceFuncInvoker0< DictionaryEntry_t3123975638  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_3);
			V_1 = (DictionaryEntry_t3123975638 )L_4;
			Func_2_t3270419407 * L_5 = (Func_2_t3270419407 *)__this->get__selector_4();
			DictionaryEntry_t3123975638  L_6 = V_1;
			NullCheck((Func_2_t3270419407 *)L_5);
			((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3270419407 *)L_5, (DictionaryEntry_t3123975638 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		}

IL_0025:
		{
			RuntimeObject* L_7 = V_0;
			NullCheck((RuntimeObject*)L_7);
			bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_7);
			if (L_8)
			{
				goto IL_0011;
			}
		}

IL_002d:
		{
			IL2CPP_LEAVE(0x39, FINALLY_002f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_9 = V_0;
			if (!L_9)
			{
				goto IL_0038;
			}
		}

IL_0032:
		{
			RuntimeObject* L_10 = V_0;
			NullCheck((RuntimeObject*)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_10);
		}

IL_0038:
		{
			IL2CPP_END_FINALLY(47)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_JUMP_TBL(0x39, IL_0039)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0039:
	{
		RuntimeObject* L_11 = (RuntimeObject*)__this->get__source_3();
		bool L_12 = ___onlyIfCheap0;
		NullCheck((RuntimeObject*)L_11);
		int32_t L_13 = InterfaceFuncInvoker1< int32_t, bool >::Invoke(2 /* System.Int32 System.Linq.IIListProvider`1<System.Collections.DictionaryEntry>::GetCount(System.Boolean) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13), (RuntimeObject*)L_11, (bool)L_12);
		return L_13;
	}
}
// System.Void System.Linq.Enumerable/SelectIPartitionIterator`2<System.Object,System.Object>::.ctor(System.Linq.IPartition`1<TSource>,System.Func`2<TSource,TResult>)
extern "C"  void SelectIPartitionIterator_2__ctor_m1653123252_gshared (SelectIPartitionIterator_2_t2131188771 * __this, RuntimeObject* ___source0, Func_2_t2447130374 * ___selector1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		((  void (*) (Iterator_1_t2034466501 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t2034466501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set__source_3(L_0);
		Func_2_t2447130374 * L_1 = ___selector1;
		__this->set__selector_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/SelectIPartitionIterator`2<System.Object,System.Object>::Clone()
extern "C"  Iterator_1_t2034466501 * SelectIPartitionIterator_2_Clone_m2925309097_gshared (SelectIPartitionIterator_2_t2131188771 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		Func_2_t2447130374 * L_1 = (Func_2_t2447130374 *)__this->get__selector_4();
		SelectIPartitionIterator_2_t2131188771 * L_2 = (SelectIPartitionIterator_2_t2131188771 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (SelectIPartitionIterator_2_t2131188771 *, RuntimeObject*, Func_2_t2447130374 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (RuntimeObject*)L_0, (Func_2_t2447130374 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Boolean System.Linq.Enumerable/SelectIPartitionIterator`2<System.Object,System.Object>::MoveNext()
extern "C"  bool SelectIPartitionIterator_2_MoveNext_m3403270584_gshared (SelectIPartitionIterator_2_t2131188771 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_MoveNext_m3403270584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t2034466501 *)__this)->get__state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0029;
		}
	}
	{
		goto IL_005a;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_3);
		__this->set__enumerator_5(L_4);
		((Iterator_1_t2034466501 *)__this)->set__state_1(2);
	}

IL_0029:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_5);
		if (!L_6)
		{
			goto IL_0054;
		}
	}
	{
		Func_2_t2447130374 * L_7 = (Func_2_t2447130374 *)__this->get__selector_4();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_8);
		RuntimeObject * L_9 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_8);
		NullCheck((Func_2_t2447130374 *)L_7);
		RuntimeObject * L_10 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2447130374 *)L_7, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		((Iterator_1_t2034466501 *)__this)->set__current_2(L_10);
		return (bool)1;
	}

IL_0054:
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t2034466501 *)__this);
	}

IL_005a:
	{
		return (bool)0;
	}
}
// System.Void System.Linq.Enumerable/SelectIPartitionIterator`2<System.Object,System.Object>::Dispose()
extern "C"  void SelectIPartitionIterator_2_Dispose_m1751187625_gshared (SelectIPartitionIterator_2_t2131188771 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_Dispose_m1751187625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__enumerator_5();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__enumerator_5();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
		__this->set__enumerator_5((RuntimeObject*)NULL);
	}

IL_001a:
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		((  void (*) (Iterator_1_t2034466501 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Iterator_1_t2034466501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// TResult System.Linq.Enumerable/SelectIPartitionIterator`2<System.Object,System.Object>::TryGetElementAt(System.Int32,System.Boolean&)
extern "C"  RuntimeObject * SelectIPartitionIterator_2_TryGetElementAt_m1620325504_gshared (SelectIPartitionIterator_2_t2131188771 * __this, int32_t ___index0, bool* ___found1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_TryGetElementAt_m1620325504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	RuntimeObject * V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		int32_t L_1 = ___index0;
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject * L_2 = InterfaceFuncInvoker2< RuntimeObject *, int32_t, bool* >::Invoke(0 /* TElement System.Linq.IPartition`1<System.Object>::TryGetElementAt(System.Int32,System.Boolean&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0, (int32_t)L_1, (bool*)(&V_0));
		V_1 = (RuntimeObject *)L_2;
		bool* L_3 = ___found1;
		bool L_4 = V_0;
		*((int8_t*)(L_3)) = (int8_t)L_4;
		bool L_5 = V_0;
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_2));
		RuntimeObject * L_6 = V_2;
		return L_6;
	}

IL_001f:
	{
		Func_2_t2447130374 * L_7 = (Func_2_t2447130374 *)__this->get__selector_4();
		RuntimeObject * L_8 = V_1;
		NullCheck((Func_2_t2447130374 *)L_7);
		RuntimeObject * L_9 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2447130374 *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_9;
	}
}
// TResult System.Linq.Enumerable/SelectIPartitionIterator`2<System.Object,System.Object>::TryGetFirst(System.Boolean&)
extern "C"  RuntimeObject * SelectIPartitionIterator_2_TryGetFirst_m2295832775_gshared (SelectIPartitionIterator_2_t2131188771 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_TryGetFirst_m2295832775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	RuntimeObject * V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject * L_1 = InterfaceFuncInvoker1< RuntimeObject *, bool* >::Invoke(1 /* TElement System.Linq.IPartition`1<System.Object>::TryGetFirst(System.Boolean&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0, (bool*)(&V_0));
		V_1 = (RuntimeObject *)L_1;
		bool* L_2 = ___found0;
		bool L_3 = V_0;
		*((int8_t*)(L_2)) = (int8_t)L_3;
		bool L_4 = V_0;
		if (L_4)
		{
			goto IL_001e;
		}
	}
	{
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_2));
		RuntimeObject * L_5 = V_2;
		return L_5;
	}

IL_001e:
	{
		Func_2_t2447130374 * L_6 = (Func_2_t2447130374 *)__this->get__selector_4();
		RuntimeObject * L_7 = V_1;
		NullCheck((Func_2_t2447130374 *)L_6);
		RuntimeObject * L_8 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2447130374 *)L_6, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_8;
	}
}
// TResult System.Linq.Enumerable/SelectIPartitionIterator`2<System.Object,System.Object>::TryGetLast(System.Boolean&)
extern "C"  RuntimeObject * SelectIPartitionIterator_2_TryGetLast_m3162752631_gshared (SelectIPartitionIterator_2_t2131188771 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_TryGetLast_m3162752631_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	RuntimeObject * V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject * L_1 = InterfaceFuncInvoker1< RuntimeObject *, bool* >::Invoke(2 /* TElement System.Linq.IPartition`1<System.Object>::TryGetLast(System.Boolean&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_0, (bool*)(&V_0));
		V_1 = (RuntimeObject *)L_1;
		bool* L_2 = ___found0;
		bool L_3 = V_0;
		*((int8_t*)(L_2)) = (int8_t)L_3;
		bool L_4 = V_0;
		if (L_4)
		{
			goto IL_001e;
		}
	}
	{
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_2));
		RuntimeObject * L_5 = V_2;
		return L_5;
	}

IL_001e:
	{
		Func_2_t2447130374 * L_6 = (Func_2_t2447130374 *)__this->get__selector_4();
		RuntimeObject * L_7 = V_1;
		NullCheck((Func_2_t2447130374 *)L_6);
		RuntimeObject * L_8 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2447130374 *)L_6, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_8;
	}
}
// TResult[] System.Linq.Enumerable/SelectIPartitionIterator`2<System.Object,System.Object>::LazyToArray()
extern "C"  ObjectU5BU5D_t2843939325* SelectIPartitionIterator_2_LazyToArray_m24814827_gshared (SelectIPartitionIterator_2_t2131188771 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_LazyToArray_m24814827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LargeArrayBuilder_1_t2990459185  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RuntimeObject* V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		LargeArrayBuilder_1__ctor_m104969882((LargeArrayBuilder_1_t2990459185 *)(&V_0), (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_0);
		V_1 = (RuntimeObject*)L_1;
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0030;
		}

IL_0016:
		{
			RuntimeObject* L_2 = V_1;
			NullCheck((RuntimeObject*)L_2);
			RuntimeObject * L_3 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_2);
			V_2 = (RuntimeObject *)L_3;
			Func_2_t2447130374 * L_4 = (Func_2_t2447130374 *)__this->get__selector_4();
			RuntimeObject * L_5 = V_2;
			NullCheck((Func_2_t2447130374 *)L_4);
			RuntimeObject * L_6 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2447130374 *)L_4, (RuntimeObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			LargeArrayBuilder_1_Add_m3802412589((LargeArrayBuilder_1_t2990459185 *)(&V_0), (RuntimeObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		}

IL_0030:
		{
			RuntimeObject* L_7 = V_1;
			NullCheck((RuntimeObject*)L_7);
			bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_7);
			if (L_8)
			{
				goto IL_0016;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x44, FINALLY_003a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_9 = V_1;
			if (!L_9)
			{
				goto IL_0043;
			}
		}

IL_003d:
		{
			RuntimeObject* L_10 = V_1;
			NullCheck((RuntimeObject*)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_10);
		}

IL_0043:
		{
			IL2CPP_END_FINALLY(58)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0044:
	{
		ObjectU5BU5D_t2843939325* L_11 = LargeArrayBuilder_1_ToArray_m390648332((LargeArrayBuilder_1_t2990459185 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_11;
	}
}
// TResult[] System.Linq.Enumerable/SelectIPartitionIterator`2<System.Object,System.Object>::PreallocatingToArray(System.Int32)
extern "C"  ObjectU5BU5D_t2843939325* SelectIPartitionIterator_2_PreallocatingToArray_m2279286047_gshared (SelectIPartitionIterator_2_t2131188771 * __this, int32_t ___count0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_PreallocatingToArray_m2279286047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	int32_t V_1 = 0;
	RuntimeObject* V_2 = NULL;
	RuntimeObject * V_3 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___count0;
		V_0 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12), (uint32_t)L_0));
		V_1 = (int32_t)0;
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_1);
		V_2 = (RuntimeObject*)L_2;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0035;
		}

IL_0017:
		{
			RuntimeObject* L_3 = V_2;
			NullCheck((RuntimeObject*)L_3);
			RuntimeObject * L_4 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_3);
			V_3 = (RuntimeObject *)L_4;
			ObjectU5BU5D_t2843939325* L_5 = V_0;
			int32_t L_6 = V_1;
			Func_2_t2447130374 * L_7 = (Func_2_t2447130374 *)__this->get__selector_4();
			RuntimeObject * L_8 = V_3;
			NullCheck((Func_2_t2447130374 *)L_7);
			RuntimeObject * L_9 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2447130374 *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			NullCheck(L_5);
			(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (RuntimeObject *)L_9);
			int32_t L_10 = V_1;
			V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
		}

IL_0035:
		{
			RuntimeObject* L_11 = V_2;
			NullCheck((RuntimeObject*)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_11);
			if (L_12)
			{
				goto IL_0017;
			}
		}

IL_003d:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_13 = V_2;
			if (!L_13)
			{
				goto IL_0048;
			}
		}

IL_0042:
		{
			RuntimeObject* L_14 = V_2;
			NullCheck((RuntimeObject*)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		}

IL_0048:
		{
			IL2CPP_END_FINALLY(63)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0049:
	{
		ObjectU5BU5D_t2843939325* L_15 = V_0;
		return L_15;
	}
}
// TResult[] System.Linq.Enumerable/SelectIPartitionIterator`2<System.Object,System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t2843939325* SelectIPartitionIterator_2_ToArray_m1485375029_gshared (SelectIPartitionIterator_2_t2131188771 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker1< int32_t, bool >::Invoke(2 /* System.Int32 System.Linq.IIListProvider`1<System.Object>::GetCount(System.Boolean) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13), (RuntimeObject*)L_0, (bool)1);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		if (!L_3)
		{
			goto IL_001d;
		}
	}
	{
		goto IL_0023;
	}

IL_0016:
	{
		NullCheck((SelectIPartitionIterator_2_t2131188771 *)__this);
		ObjectU5BU5D_t2843939325* L_4 = ((  ObjectU5BU5D_t2843939325* (*) (SelectIPartitionIterator_2_t2131188771 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((SelectIPartitionIterator_2_t2131188771 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_4;
	}

IL_001d:
	{
		ObjectU5BU5D_t2843939325* L_5 = ((  ObjectU5BU5D_t2843939325* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_5;
	}

IL_0023:
	{
		int32_t L_6 = V_0;
		NullCheck((SelectIPartitionIterator_2_t2131188771 *)__this);
		ObjectU5BU5D_t2843939325* L_7 = ((  ObjectU5BU5D_t2843939325* (*) (SelectIPartitionIterator_2_t2131188771 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((SelectIPartitionIterator_2_t2131188771 *)__this, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		return L_7;
	}
}
// System.Collections.Generic.List`1<TResult> System.Linq.Enumerable/SelectIPartitionIterator`2<System.Object,System.Object>::ToList()
extern "C"  List_1_t257213610 * SelectIPartitionIterator_2_ToList_m1963142377_gshared (SelectIPartitionIterator_2_t2131188771 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_ToList_m1963142377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	List_1_t257213610 * V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	RuntimeObject * V_3 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker1< int32_t, bool >::Invoke(2 /* System.Int32 System.Linq.IIListProvider`1<System.Object>::GetCount(System.Boolean) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13), (RuntimeObject*)L_0, (bool)1);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		goto IL_0024;
	}

IL_0016:
	{
		List_1_t257213610 * L_4 = (List_1_t257213610 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 17));
		((  void (*) (List_1_t257213610 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		V_1 = (List_1_t257213610 *)L_4;
		goto IL_002b;
	}

IL_001e:
	{
		List_1_t257213610 * L_5 = (List_1_t257213610 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 17));
		((  void (*) (List_1_t257213610 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)(L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		return L_5;
	}

IL_0024:
	{
		int32_t L_6 = V_0;
		List_1_t257213610 * L_7 = (List_1_t257213610 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 17));
		((  void (*) (List_1_t257213610 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(L_7, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		V_1 = (List_1_t257213610 *)L_7;
	}

IL_002b:
	{
		RuntimeObject* L_8 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_8);
		RuntimeObject* L_9 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_8);
		V_2 = (RuntimeObject*)L_9;
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0052;
		}

IL_0039:
		{
			RuntimeObject* L_10 = V_2;
			NullCheck((RuntimeObject*)L_10);
			RuntimeObject * L_11 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_10);
			V_3 = (RuntimeObject *)L_11;
			List_1_t257213610 * L_12 = V_1;
			Func_2_t2447130374 * L_13 = (Func_2_t2447130374 *)__this->get__selector_4();
			RuntimeObject * L_14 = V_3;
			NullCheck((Func_2_t2447130374 *)L_13);
			RuntimeObject * L_15 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2447130374 *)L_13, (RuntimeObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			NullCheck((List_1_t257213610 *)L_12);
			((  void (*) (List_1_t257213610 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((List_1_t257213610 *)L_12, (RuntimeObject *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		}

IL_0052:
		{
			RuntimeObject* L_16 = V_2;
			NullCheck((RuntimeObject*)L_16);
			bool L_17 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_16);
			if (L_17)
			{
				goto IL_0039;
			}
		}

IL_005a:
		{
			IL2CPP_LEAVE(0x66, FINALLY_005c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_005c;
	}

FINALLY_005c:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_18 = V_2;
			if (!L_18)
			{
				goto IL_0065;
			}
		}

IL_005f:
		{
			RuntimeObject* L_19 = V_2;
			NullCheck((RuntimeObject*)L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_19);
		}

IL_0065:
		{
			IL2CPP_END_FINALLY(92)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(92)
	{
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0066:
	{
		List_1_t257213610 * L_20 = V_1;
		return L_20;
	}
}
// System.Int32 System.Linq.Enumerable/SelectIPartitionIterator`2<System.Object,System.Object>::GetCount(System.Boolean)
extern "C"  int32_t SelectIPartitionIterator_2_GetCount_m842092247_gshared (SelectIPartitionIterator_2_t2131188771 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectIPartitionIterator_2_GetCount_m842092247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ___onlyIfCheap0;
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (RuntimeObject*)L_1);
		V_0 = (RuntimeObject*)L_2;
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0025;
		}

IL_0011:
		{
			RuntimeObject* L_3 = V_0;
			NullCheck((RuntimeObject*)L_3);
			RuntimeObject * L_4 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_3);
			V_1 = (RuntimeObject *)L_4;
			Func_2_t2447130374 * L_5 = (Func_2_t2447130374 *)__this->get__selector_4();
			RuntimeObject * L_6 = V_1;
			NullCheck((Func_2_t2447130374 *)L_5);
			((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2447130374 *)L_5, (RuntimeObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		}

IL_0025:
		{
			RuntimeObject* L_7 = V_0;
			NullCheck((RuntimeObject*)L_7);
			bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_7);
			if (L_8)
			{
				goto IL_0011;
			}
		}

IL_002d:
		{
			IL2CPP_LEAVE(0x39, FINALLY_002f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_9 = V_0;
			if (!L_9)
			{
				goto IL_0038;
			}
		}

IL_0032:
		{
			RuntimeObject* L_10 = V_0;
			NullCheck((RuntimeObject*)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_10);
		}

IL_0038:
		{
			IL2CPP_END_FINALLY(47)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_JUMP_TBL(0x39, IL_0039)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0039:
	{
		RuntimeObject* L_11 = (RuntimeObject*)__this->get__source_3();
		bool L_12 = ___onlyIfCheap0;
		NullCheck((RuntimeObject*)L_11);
		int32_t L_13 = InterfaceFuncInvoker1< int32_t, bool >::Invoke(2 /* System.Int32 System.Linq.IIListProvider`1<System.Object>::GetCount(System.Boolean) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13), (RuntimeObject*)L_11, (bool)L_12);
		return L_13;
	}
}
// System.Void System.Linq.Enumerable/SelectListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,TResult>)
extern "C"  void SelectListIterator_2__ctor_m2541052279_gshared (SelectListIterator_2_t2565991656 * __this, List_1_t301083084 * ___source0, Func_2_t3270419407 * ___selector1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		((  void (*) (Iterator_1_t1484577656 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t1484577656 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		List_1_t301083084 * L_0 = ___source0;
		__this->set__source_3(L_0);
		Func_2_t3270419407 * L_1 = ___selector1;
		__this->set__selector_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/SelectListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clone()
extern "C"  Iterator_1_t1484577656 * SelectListIterator_2_Clone_m963722903_gshared (SelectListIterator_2_t2565991656 * __this, const RuntimeMethod* method)
{
	{
		List_1_t301083084 * L_0 = (List_1_t301083084 *)__this->get__source_3();
		Func_2_t3270419407 * L_1 = (Func_2_t3270419407 *)__this->get__selector_4();
		SelectListIterator_2_t2565991656 * L_2 = (SelectListIterator_2_t2565991656 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (SelectListIterator_2_t2565991656 *, List_1_t301083084 *, Func_2_t3270419407 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (List_1_t301083084 *)L_0, (Func_2_t3270419407 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Boolean System.Linq.Enumerable/SelectListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool SelectListIterator_2_MoveNext_m4104038772_gshared (SelectListIterator_2_t2565991656 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t1484577656 *)__this)->get__state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0029;
		}
	}
	{
		goto IL_005a;
	}

IL_0011:
	{
		List_1_t301083084 * L_3 = (List_1_t301083084 *)__this->get__source_3();
		NullCheck((List_1_t301083084 *)L_3);
		Enumerator_t2190326961  L_4 = ((  Enumerator_t2190326961  (*) (List_1_t301083084 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((List_1_t301083084 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set__enumerator_5(L_4);
		((Iterator_1_t1484577656 *)__this)->set__state_1(2);
	}

IL_0029:
	{
		Enumerator_t2190326961 * L_5 = (Enumerator_t2190326961 *)__this->get_address_of__enumerator_5();
		bool L_6 = Enumerator_MoveNext_m2875527037((Enumerator_t2190326961 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		if (!L_6)
		{
			goto IL_0054;
		}
	}
	{
		Func_2_t3270419407 * L_7 = (Func_2_t3270419407 *)__this->get__selector_4();
		Enumerator_t2190326961 * L_8 = (Enumerator_t2190326961 *)__this->get_address_of__enumerator_5();
		DictionaryEntry_t3123975638  L_9 = Enumerator_get_Current_m3966199253((Enumerator_t2190326961 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Func_2_t3270419407 *)L_7);
		KeyValuePair_2_t2530217319  L_10 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3270419407 *)L_7, (DictionaryEntry_t3123975638 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		((Iterator_1_t1484577656 *)__this)->set__current_2(L_10);
		return (bool)1;
	}

IL_0054:
	{
		NullCheck((Iterator_1_t1484577656 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose() */, (Iterator_1_t1484577656 *)__this);
	}

IL_005a:
	{
		return (bool)0;
	}
}
// TResult[] System.Linq.Enumerable/SelectListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t118269214* SelectListIterator_2_ToArray_m1733111365_gshared (SelectListIterator_2_t2565991656 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2U5BU5D_t118269214* V_1 = NULL;
	int32_t V_2 = 0;
	{
		List_1_t301083084 * L_0 = (List_1_t301083084 *)__this->get__source_3();
		NullCheck((List_1_t301083084 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t301083084 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t301083084 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		KeyValuePair_2U5BU5D_t118269214* L_3 = ((  KeyValuePair_2U5BU5D_t118269214* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_3;
	}

IL_0015:
	{
		int32_t L_4 = V_0;
		V_1 = (KeyValuePair_2U5BU5D_t118269214*)((KeyValuePair_2U5BU5D_t118269214*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (uint32_t)L_4));
		V_2 = (int32_t)0;
		goto IL_0042;
	}

IL_0020:
	{
		KeyValuePair_2U5BU5D_t118269214* L_5 = V_1;
		int32_t L_6 = V_2;
		Func_2_t3270419407 * L_7 = (Func_2_t3270419407 *)__this->get__selector_4();
		List_1_t301083084 * L_8 = (List_1_t301083084 *)__this->get__source_3();
		int32_t L_9 = V_2;
		NullCheck((List_1_t301083084 *)L_8);
		DictionaryEntry_t3123975638  L_10 = ((  DictionaryEntry_t3123975638  (*) (List_1_t301083084 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t301083084 *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Func_2_t3270419407 *)L_7);
		KeyValuePair_2_t2530217319  L_11 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3270419407 *)L_7, (DictionaryEntry_t3123975638 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (KeyValuePair_2_t2530217319 )L_11);
		int32_t L_12 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_13 = V_2;
		KeyValuePair_2U5BU5D_t118269214* L_14 = V_1;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_14)->max_length)))))))
		{
			goto IL_0020;
		}
	}
	{
		KeyValuePair_2U5BU5D_t118269214* L_15 = V_1;
		return L_15;
	}
}
// System.Collections.Generic.List`1<TResult> System.Linq.Enumerable/SelectListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToList()
extern "C"  List_1_t4002292061 * SelectListIterator_2_ToList_m3155326254_gshared (SelectListIterator_2_t2565991656 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	List_1_t4002292061 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		List_1_t301083084 * L_0 = (List_1_t301083084 *)__this->get__source_3();
		NullCheck((List_1_t301083084 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t301083084 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t301083084 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		List_1_t4002292061 * L_3 = (List_1_t4002292061 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13));
		((  void (*) (List_1_t4002292061 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(L_3, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_1 = (List_1_t4002292061 *)L_3;
		V_2 = (int32_t)0;
		goto IL_0038;
	}

IL_0017:
	{
		List_1_t4002292061 * L_4 = V_1;
		Func_2_t3270419407 * L_5 = (Func_2_t3270419407 *)__this->get__selector_4();
		List_1_t301083084 * L_6 = (List_1_t301083084 *)__this->get__source_3();
		int32_t L_7 = V_2;
		NullCheck((List_1_t301083084 *)L_6);
		DictionaryEntry_t3123975638  L_8 = ((  DictionaryEntry_t3123975638  (*) (List_1_t301083084 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t301083084 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Func_2_t3270419407 *)L_5);
		KeyValuePair_2_t2530217319  L_9 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3270419407 *)L_5, (DictionaryEntry_t3123975638 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((List_1_t4002292061 *)L_4);
		((  void (*) (List_1_t4002292061 *, KeyValuePair_2_t2530217319 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t4002292061 *)L_4, (KeyValuePair_2_t2530217319 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		int32_t L_10 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0038:
	{
		int32_t L_11 = V_2;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0017;
		}
	}
	{
		List_1_t4002292061 * L_13 = V_1;
		return L_13;
	}
}
// System.Int32 System.Linq.Enumerable/SelectListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetCount(System.Boolean)
extern "C"  int32_t SelectListIterator_2_GetCount_m1483956162_gshared (SelectListIterator_2_t2565991656 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		List_1_t301083084 * L_0 = (List_1_t301083084 *)__this->get__source_3();
		NullCheck((List_1_t301083084 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t301083084 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t301083084 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (int32_t)L_1;
		bool L_2 = ___onlyIfCheap0;
		if (L_2)
		{
			goto IL_0033;
		}
	}
	{
		V_1 = (int32_t)0;
		goto IL_002f;
	}

IL_0013:
	{
		Func_2_t3270419407 * L_3 = (Func_2_t3270419407 *)__this->get__selector_4();
		List_1_t301083084 * L_4 = (List_1_t301083084 *)__this->get__source_3();
		int32_t L_5 = V_1;
		NullCheck((List_1_t301083084 *)L_4);
		DictionaryEntry_t3123975638  L_6 = ((  DictionaryEntry_t3123975638  (*) (List_1_t301083084 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t301083084 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Func_2_t3270419407 *)L_3);
		((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3270419407 *)L_3, (DictionaryEntry_t3123975638 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_8 = V_1;
		int32_t L_9 = V_0;
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0013;
		}
	}

IL_0033:
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// TResult System.Linq.Enumerable/SelectListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetElementAt(System.Int32,System.Boolean&)
extern "C"  KeyValuePair_2_t2530217319  SelectListIterator_2_TryGetElementAt_m876464443_gshared (SelectListIterator_2_t2565991656 * __this, int32_t ___index0, bool* ___found1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectListIterator_2_TryGetElementAt_m876464443_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2530217319  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		List_1_t301083084 * L_1 = (List_1_t301083084 *)__this->get__source_3();
		NullCheck((List_1_t301083084 *)L_1);
		int32_t L_2 = ((  int32_t (*) (List_1_t301083084 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t301083084 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		if ((!(((uint32_t)L_0) < ((uint32_t)L_2))))
		{
			goto IL_0029;
		}
	}
	{
		bool* L_3 = ___found1;
		*((int8_t*)(L_3)) = (int8_t)1;
		Func_2_t3270419407 * L_4 = (Func_2_t3270419407 *)__this->get__selector_4();
		List_1_t301083084 * L_5 = (List_1_t301083084 *)__this->get__source_3();
		int32_t L_6 = ___index0;
		NullCheck((List_1_t301083084 *)L_5);
		DictionaryEntry_t3123975638  L_7 = ((  DictionaryEntry_t3123975638  (*) (List_1_t301083084 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t301083084 *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Func_2_t3270419407 *)L_4);
		KeyValuePair_2_t2530217319  L_8 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3270419407 *)L_4, (DictionaryEntry_t3123975638 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return L_8;
	}

IL_0029:
	{
		bool* L_9 = ___found1;
		*((int8_t*)(L_9)) = (int8_t)0;
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_0));
		KeyValuePair_2_t2530217319  L_10 = V_0;
		return L_10;
	}
}
// TResult System.Linq.Enumerable/SelectListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetFirst(System.Boolean&)
extern "C"  KeyValuePair_2_t2530217319  SelectListIterator_2_TryGetFirst_m1293288055_gshared (SelectListIterator_2_t2565991656 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectListIterator_2_TryGetFirst_m1293288055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2530217319  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		List_1_t301083084 * L_0 = (List_1_t301083084 *)__this->get__source_3();
		NullCheck((List_1_t301083084 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t301083084 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t301083084 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		bool* L_2 = ___found0;
		*((int8_t*)(L_2)) = (int8_t)1;
		Func_2_t3270419407 * L_3 = (Func_2_t3270419407 *)__this->get__selector_4();
		List_1_t301083084 * L_4 = (List_1_t301083084 *)__this->get__source_3();
		NullCheck((List_1_t301083084 *)L_4);
		DictionaryEntry_t3123975638  L_5 = ((  DictionaryEntry_t3123975638  (*) (List_1_t301083084 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t301083084 *)L_4, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Func_2_t3270419407 *)L_3);
		KeyValuePair_2_t2530217319  L_6 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3270419407 *)L_3, (DictionaryEntry_t3123975638 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return L_6;
	}

IL_0028:
	{
		bool* L_7 = ___found0;
		*((int8_t*)(L_7)) = (int8_t)0;
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_0));
		KeyValuePair_2_t2530217319  L_8 = V_0;
		return L_8;
	}
}
// TResult System.Linq.Enumerable/SelectListIterator`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetLast(System.Boolean&)
extern "C"  KeyValuePair_2_t2530217319  SelectListIterator_2_TryGetLast_m3800486358_gshared (SelectListIterator_2_t2565991656 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectListIterator_2_TryGetLast_m3800486358_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t2530217319  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		List_1_t301083084 * L_0 = (List_1_t301083084 *)__this->get__source_3();
		NullCheck((List_1_t301083084 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t301083084 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t301083084 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		bool* L_3 = ___found0;
		*((int8_t*)(L_3)) = (int8_t)1;
		Func_2_t3270419407 * L_4 = (Func_2_t3270419407 *)__this->get__selector_4();
		List_1_t301083084 * L_5 = (List_1_t301083084 *)__this->get__source_3();
		int32_t L_6 = V_0;
		NullCheck((List_1_t301083084 *)L_5);
		DictionaryEntry_t3123975638  L_7 = ((  DictionaryEntry_t3123975638  (*) (List_1_t301083084 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t301083084 *)L_5, (int32_t)((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Func_2_t3270419407 *)L_4);
		KeyValuePair_2_t2530217319  L_8 = ((  KeyValuePair_2_t2530217319  (*) (Func_2_t3270419407 *, DictionaryEntry_t3123975638 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3270419407 *)L_4, (DictionaryEntry_t3123975638 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return L_8;
	}

IL_002c:
	{
		bool* L_9 = ___found0;
		*((int8_t*)(L_9)) = (int8_t)0;
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_1));
		KeyValuePair_2_t2530217319  L_10 = V_1;
		return L_10;
	}
}
// System.Void System.Linq.Enumerable/SelectListIterator`2<System.Object,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,TResult>)
extern "C"  void SelectListIterator_2__ctor_m1396350295_gshared (SelectListIterator_2_t1742702623 * __this, List_1_t257213610 * ___source0, Func_2_t2447130374 * ___selector1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		((  void (*) (Iterator_1_t2034466501 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t2034466501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		List_1_t257213610 * L_0 = ___source0;
		__this->set__source_3(L_0);
		Func_2_t2447130374 * L_1 = ___selector1;
		__this->set__selector_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/SelectListIterator`2<System.Object,System.Object>::Clone()
extern "C"  Iterator_1_t2034466501 * SelectListIterator_2_Clone_m3141305114_gshared (SelectListIterator_2_t1742702623 * __this, const RuntimeMethod* method)
{
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get__source_3();
		Func_2_t2447130374 * L_1 = (Func_2_t2447130374 *)__this->get__selector_4();
		SelectListIterator_2_t1742702623 * L_2 = (SelectListIterator_2_t1742702623 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (SelectListIterator_2_t1742702623 *, List_1_t257213610 *, Func_2_t2447130374 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (List_1_t257213610 *)L_0, (Func_2_t2447130374 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Boolean System.Linq.Enumerable/SelectListIterator`2<System.Object,System.Object>::MoveNext()
extern "C"  bool SelectListIterator_2_MoveNext_m3296767953_gshared (SelectListIterator_2_t1742702623 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t2034466501 *)__this)->get__state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0029;
		}
	}
	{
		goto IL_005a;
	}

IL_0011:
	{
		List_1_t257213610 * L_3 = (List_1_t257213610 *)__this->get__source_3();
		NullCheck((List_1_t257213610 *)L_3);
		Enumerator_t2146457487  L_4 = ((  Enumerator_t2146457487  (*) (List_1_t257213610 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((List_1_t257213610 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set__enumerator_5(L_4);
		((Iterator_1_t2034466501 *)__this)->set__state_1(2);
	}

IL_0029:
	{
		Enumerator_t2146457487 * L_5 = (Enumerator_t2146457487 *)__this->get_address_of__enumerator_5();
		bool L_6 = Enumerator_MoveNext_m2142368520((Enumerator_t2146457487 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		if (!L_6)
		{
			goto IL_0054;
		}
	}
	{
		Func_2_t2447130374 * L_7 = (Func_2_t2447130374 *)__this->get__selector_4();
		Enumerator_t2146457487 * L_8 = (Enumerator_t2146457487 *)__this->get_address_of__enumerator_5();
		RuntimeObject * L_9 = Enumerator_get_Current_m337713592((Enumerator_t2146457487 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Func_2_t2447130374 *)L_7);
		RuntimeObject * L_10 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t2447130374 *)L_7, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		((Iterator_1_t2034466501 *)__this)->set__current_2(L_10);
		return (bool)1;
	}

IL_0054:
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t2034466501 *)__this);
	}

IL_005a:
	{
		return (bool)0;
	}
}
// TResult[] System.Linq.Enumerable/SelectListIterator`2<System.Object,System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t2843939325* SelectListIterator_2_ToArray_m2579775491_gshared (SelectListIterator_2_t1742702623 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	ObjectU5BU5D_t2843939325* V_1 = NULL;
	int32_t V_2 = 0;
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get__source_3();
		NullCheck((List_1_t257213610 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t257213610 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t257213610 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ((  ObjectU5BU5D_t2843939325* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_3;
	}

IL_0015:
	{
		int32_t L_4 = V_0;
		V_1 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (uint32_t)L_4));
		V_2 = (int32_t)0;
		goto IL_0042;
	}

IL_0020:
	{
		ObjectU5BU5D_t2843939325* L_5 = V_1;
		int32_t L_6 = V_2;
		Func_2_t2447130374 * L_7 = (Func_2_t2447130374 *)__this->get__selector_4();
		List_1_t257213610 * L_8 = (List_1_t257213610 *)__this->get__source_3();
		int32_t L_9 = V_2;
		NullCheck((List_1_t257213610 *)L_8);
		RuntimeObject * L_10 = ((  RuntimeObject * (*) (List_1_t257213610 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t257213610 *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Func_2_t2447130374 *)L_7);
		RuntimeObject * L_11 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t2447130374 *)L_7, (RuntimeObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (RuntimeObject *)L_11);
		int32_t L_12 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_13 = V_2;
		ObjectU5BU5D_t2843939325* L_14 = V_1;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_14)->max_length)))))))
		{
			goto IL_0020;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_15 = V_1;
		return L_15;
	}
}
// System.Collections.Generic.List`1<TResult> System.Linq.Enumerable/SelectListIterator`2<System.Object,System.Object>::ToList()
extern "C"  List_1_t257213610 * SelectListIterator_2_ToList_m2016497570_gshared (SelectListIterator_2_t1742702623 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	List_1_t257213610 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get__source_3();
		NullCheck((List_1_t257213610 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t257213610 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t257213610 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		List_1_t257213610 * L_3 = (List_1_t257213610 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13));
		((  void (*) (List_1_t257213610 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(L_3, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_1 = (List_1_t257213610 *)L_3;
		V_2 = (int32_t)0;
		goto IL_0038;
	}

IL_0017:
	{
		List_1_t257213610 * L_4 = V_1;
		Func_2_t2447130374 * L_5 = (Func_2_t2447130374 *)__this->get__selector_4();
		List_1_t257213610 * L_6 = (List_1_t257213610 *)__this->get__source_3();
		int32_t L_7 = V_2;
		NullCheck((List_1_t257213610 *)L_6);
		RuntimeObject * L_8 = ((  RuntimeObject * (*) (List_1_t257213610 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t257213610 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Func_2_t2447130374 *)L_5);
		RuntimeObject * L_9 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t2447130374 *)L_5, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((List_1_t257213610 *)L_4);
		((  void (*) (List_1_t257213610 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t257213610 *)L_4, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		int32_t L_10 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0038:
	{
		int32_t L_11 = V_2;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0017;
		}
	}
	{
		List_1_t257213610 * L_13 = V_1;
		return L_13;
	}
}
// System.Int32 System.Linq.Enumerable/SelectListIterator`2<System.Object,System.Object>::GetCount(System.Boolean)
extern "C"  int32_t SelectListIterator_2_GetCount_m2827964842_gshared (SelectListIterator_2_t1742702623 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get__source_3();
		NullCheck((List_1_t257213610 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t257213610 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t257213610 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (int32_t)L_1;
		bool L_2 = ___onlyIfCheap0;
		if (L_2)
		{
			goto IL_0033;
		}
	}
	{
		V_1 = (int32_t)0;
		goto IL_002f;
	}

IL_0013:
	{
		Func_2_t2447130374 * L_3 = (Func_2_t2447130374 *)__this->get__selector_4();
		List_1_t257213610 * L_4 = (List_1_t257213610 *)__this->get__source_3();
		int32_t L_5 = V_1;
		NullCheck((List_1_t257213610 *)L_4);
		RuntimeObject * L_6 = ((  RuntimeObject * (*) (List_1_t257213610 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t257213610 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Func_2_t2447130374 *)L_3);
		((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t2447130374 *)L_3, (RuntimeObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_8 = V_1;
		int32_t L_9 = V_0;
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0013;
		}
	}

IL_0033:
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// TResult System.Linq.Enumerable/SelectListIterator`2<System.Object,System.Object>::TryGetElementAt(System.Int32,System.Boolean&)
extern "C"  RuntimeObject * SelectListIterator_2_TryGetElementAt_m3949705530_gshared (SelectListIterator_2_t1742702623 * __this, int32_t ___index0, bool* ___found1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectListIterator_2_TryGetElementAt_m3949705530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		int32_t L_0 = ___index0;
		List_1_t257213610 * L_1 = (List_1_t257213610 *)__this->get__source_3();
		NullCheck((List_1_t257213610 *)L_1);
		int32_t L_2 = ((  int32_t (*) (List_1_t257213610 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t257213610 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		if ((!(((uint32_t)L_0) < ((uint32_t)L_2))))
		{
			goto IL_0029;
		}
	}
	{
		bool* L_3 = ___found1;
		*((int8_t*)(L_3)) = (int8_t)1;
		Func_2_t2447130374 * L_4 = (Func_2_t2447130374 *)__this->get__selector_4();
		List_1_t257213610 * L_5 = (List_1_t257213610 *)__this->get__source_3();
		int32_t L_6 = ___index0;
		NullCheck((List_1_t257213610 *)L_5);
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (List_1_t257213610 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t257213610 *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Func_2_t2447130374 *)L_4);
		RuntimeObject * L_8 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t2447130374 *)L_4, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return L_8;
	}

IL_0029:
	{
		bool* L_9 = ___found1;
		*((int8_t*)(L_9)) = (int8_t)0;
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_0));
		RuntimeObject * L_10 = V_0;
		return L_10;
	}
}
// TResult System.Linq.Enumerable/SelectListIterator`2<System.Object,System.Object>::TryGetFirst(System.Boolean&)
extern "C"  RuntimeObject * SelectListIterator_2_TryGetFirst_m2392574845_gshared (SelectListIterator_2_t1742702623 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectListIterator_2_TryGetFirst_m2392574845_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get__source_3();
		NullCheck((List_1_t257213610 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t257213610 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t257213610 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		bool* L_2 = ___found0;
		*((int8_t*)(L_2)) = (int8_t)1;
		Func_2_t2447130374 * L_3 = (Func_2_t2447130374 *)__this->get__selector_4();
		List_1_t257213610 * L_4 = (List_1_t257213610 *)__this->get__source_3();
		NullCheck((List_1_t257213610 *)L_4);
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (List_1_t257213610 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t257213610 *)L_4, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Func_2_t2447130374 *)L_3);
		RuntimeObject * L_6 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t2447130374 *)L_3, (RuntimeObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return L_6;
	}

IL_0028:
	{
		bool* L_7 = ___found0;
		*((int8_t*)(L_7)) = (int8_t)0;
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_0));
		RuntimeObject * L_8 = V_0;
		return L_8;
	}
}
// TResult System.Linq.Enumerable/SelectListIterator`2<System.Object,System.Object>::TryGetLast(System.Boolean&)
extern "C"  RuntimeObject * SelectListIterator_2_TryGetLast_m491821547_gshared (SelectListIterator_2_t1742702623 * __this, bool* ___found0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectListIterator_2_TryGetLast_m491821547_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get__source_3();
		NullCheck((List_1_t257213610 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t257213610 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t257213610 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		bool* L_3 = ___found0;
		*((int8_t*)(L_3)) = (int8_t)1;
		Func_2_t2447130374 * L_4 = (Func_2_t2447130374 *)__this->get__selector_4();
		List_1_t257213610 * L_5 = (List_1_t257213610 *)__this->get__source_3();
		int32_t L_6 = V_0;
		NullCheck((List_1_t257213610 *)L_5);
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (List_1_t257213610 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t257213610 *)L_5, (int32_t)((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Func_2_t2447130374 *)L_4);
		RuntimeObject * L_8 = ((  RuntimeObject * (*) (Func_2_t2447130374 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t2447130374 *)L_4, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return L_8;
	}

IL_002c:
	{
		bool* L_9 = ___found0;
		*((int8_t*)(L_9)) = (int8_t)0;
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_1));
		RuntimeObject * L_10 = V_1;
		return L_10;
	}
}
// System.Void System.Linq.Enumerable/SelectManySingleSelectorIterator`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
extern "C"  void SelectManySingleSelectorIterator_2__ctor_m564398177_gshared (SelectManySingleSelectorIterator_2_t963716220 * __this, RuntimeObject* ___source0, Func_2_t1426983263 * ___selector1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		((  void (*) (Iterator_1_t2034466501 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t2034466501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set__source_3(L_0);
		Func_2_t1426983263 * L_1 = ___selector1;
		__this->set__selector_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/SelectManySingleSelectorIterator`2<System.Object,System.Object>::Clone()
extern "C"  Iterator_1_t2034466501 * SelectManySingleSelectorIterator_2_Clone_m1574800785_gshared (SelectManySingleSelectorIterator_2_t963716220 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		Func_2_t1426983263 * L_1 = (Func_2_t1426983263 *)__this->get__selector_4();
		SelectManySingleSelectorIterator_2_t963716220 * L_2 = (SelectManySingleSelectorIterator_2_t963716220 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (SelectManySingleSelectorIterator_2_t963716220 *, RuntimeObject*, Func_2_t1426983263 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (RuntimeObject*)L_0, (Func_2_t1426983263 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Linq.Enumerable/SelectManySingleSelectorIterator`2<System.Object,System.Object>::Dispose()
extern "C"  void SelectManySingleSelectorIterator_2_Dispose_m3642031398_gshared (SelectManySingleSelectorIterator_2_t963716220 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectManySingleSelectorIterator_2_Dispose_m3642031398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__subEnumerator_6();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__subEnumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
		__this->set__subEnumerator_6((RuntimeObject*)NULL);
	}

IL_001a:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get__sourceEnumerator_5();
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get__sourceEnumerator_5();
		NullCheck((RuntimeObject*)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_3);
		__this->set__sourceEnumerator_5((RuntimeObject*)NULL);
	}

IL_0034:
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		((  void (*) (Iterator_1_t2034466501 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Iterator_1_t2034466501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Int32 System.Linq.Enumerable/SelectManySingleSelectorIterator`2<System.Object,System.Object>::GetCount(System.Boolean)
extern "C"  int32_t SelectManySingleSelectorIterator_2_GetCount_m3920743042_gshared (SelectManySingleSelectorIterator_2_t963716220 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectManySingleSelectorIterator_2_GetCount_m3920743042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject* V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ___onlyIfCheap0;
		if (!L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (-1);
	}

IL_0005:
	{
		V_0 = (int32_t)0;
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_1);
		V_1 = (RuntimeObject*)L_2;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0030;
		}

IL_0015:
		{
			RuntimeObject* L_3 = V_1;
			NullCheck((RuntimeObject*)L_3);
			RuntimeObject * L_4 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_3);
			V_2 = (RuntimeObject *)L_4;
			int32_t L_5 = V_0;
			Func_2_t1426983263 * L_6 = (Func_2_t1426983263 *)__this->get__selector_4();
			RuntimeObject * L_7 = V_2;
			NullCheck((Func_2_t1426983263 *)L_6);
			RuntimeObject* L_8 = ((  RuntimeObject* (*) (Func_2_t1426983263 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t1426983263 *)L_6, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			int32_t L_9 = ((  int32_t (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
			if (((int64_t)L_5 + (int64_t)L_9 < (int64_t)kIl2CppInt32Min) || ((int64_t)L_5 + (int64_t)L_9 > (int64_t)kIl2CppInt32Max))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
			V_0 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)L_9));
		}

IL_0030:
		{
			RuntimeObject* L_10 = V_1;
			NullCheck((RuntimeObject*)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_10);
			if (L_11)
			{
				goto IL_0015;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x44, FINALLY_003a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_12 = V_1;
			if (!L_12)
			{
				goto IL_0043;
			}
		}

IL_003d:
		{
			RuntimeObject* L_13 = V_1;
			NullCheck((RuntimeObject*)L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_13);
		}

IL_0043:
		{
			IL2CPP_END_FINALLY(58)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0044:
	{
		int32_t L_14 = V_0;
		return L_14;
	}
}
// System.Boolean System.Linq.Enumerable/SelectManySingleSelectorIterator`2<System.Object,System.Object>::MoveNext()
extern "C"  bool SelectManySingleSelectorIterator_2_MoveNext_m1777559799_gshared (SelectManySingleSelectorIterator_2_t963716220 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectManySingleSelectorIterator_2_MoveNext_m1777559799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t2034466501 *)__this)->get__state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)1)))
		{
			case 0:
			{
				goto IL_0020;
			}
			case 1:
			{
				goto IL_0038;
			}
			case 2:
			{
				goto IL_006f;
			}
		}
	}
	{
		goto IL_00aa;
	}

IL_0020:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_2);
		RuntimeObject* L_3 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_2);
		__this->set__sourceEnumerator_5(L_3);
		((Iterator_1_t2034466501 *)__this)->set__state_1(2);
	}

IL_0038:
	{
		RuntimeObject* L_4 = (RuntimeObject*)__this->get__sourceEnumerator_5();
		NullCheck((RuntimeObject*)L_4);
		bool L_5 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_4);
		if (!L_5)
		{
			goto IL_00aa;
		}
	}
	{
		RuntimeObject* L_6 = (RuntimeObject*)__this->get__sourceEnumerator_5();
		NullCheck((RuntimeObject*)L_6);
		RuntimeObject * L_7 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_6);
		V_1 = (RuntimeObject *)L_7;
		Func_2_t1426983263 * L_8 = (Func_2_t1426983263 *)__this->get__selector_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t1426983263 *)L_8);
		RuntimeObject* L_10 = ((  RuntimeObject* (*) (Func_2_t1426983263 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t1426983263 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		NullCheck((RuntimeObject*)L_10);
		RuntimeObject* L_11 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (RuntimeObject*)L_10);
		__this->set__subEnumerator_6(L_11);
		((Iterator_1_t2034466501 *)__this)->set__state_1(3);
	}

IL_006f:
	{
		RuntimeObject* L_12 = (RuntimeObject*)__this->get__subEnumerator_6();
		NullCheck((RuntimeObject*)L_12);
		bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_12);
		if (L_13)
		{
			goto IL_0097;
		}
	}
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get__subEnumerator_6();
		NullCheck((RuntimeObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		__this->set__subEnumerator_6((RuntimeObject*)NULL);
		((Iterator_1_t2034466501 *)__this)->set__state_1(2);
		goto IL_0038;
	}

IL_0097:
	{
		RuntimeObject* L_15 = (RuntimeObject*)__this->get__subEnumerator_6();
		NullCheck((RuntimeObject*)L_15);
		RuntimeObject * L_16 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10), (RuntimeObject*)L_15);
		((Iterator_1_t2034466501 *)__this)->set__current_2(L_16);
		return (bool)1;
	}

IL_00aa:
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t2034466501 *)__this);
		return (bool)0;
	}
}
// TResult[] System.Linq.Enumerable/SelectManySingleSelectorIterator`2<System.Object,System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t2843939325* SelectManySingleSelectorIterator_2_ToArray_m2521061925_gshared (SelectManySingleSelectorIterator_2_t963716220 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectManySingleSelectorIterator_2_ToArray_m2521061925_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SparseArrayBuilder_1_t3713953326  V_0;
	memset(&V_0, 0, sizeof(V_0));
	ArrayBuilder_1_t3343055733  V_1;
	memset(&V_1, 0, sizeof(V_1));
	ObjectU5BU5D_t2843939325* V_2 = NULL;
	ArrayBuilder_1_t4177874457  V_3;
	memset(&V_3, 0, sizeof(V_3));
	RuntimeObject* V_4 = NULL;
	RuntimeObject * V_5 = NULL;
	RuntimeObject* V_6 = NULL;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	Marker_t2894777777  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SparseArrayBuilder_1__ctor_m3599075535((SparseArrayBuilder_1_t3713953326 *)(&V_0), (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		Initobj (ArrayBuilder_1_t3343055733_il2cpp_TypeInfo_var, (&V_1));
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_0);
		V_4 = (RuntimeObject*)L_1;
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006f;
		}

IL_001f:
		{
			RuntimeObject* L_2 = V_4;
			NullCheck((RuntimeObject*)L_2);
			RuntimeObject * L_3 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_2);
			V_5 = (RuntimeObject *)L_3;
			Func_2_t1426983263 * L_4 = (Func_2_t1426983263 *)__this->get__selector_4();
			RuntimeObject * L_5 = V_5;
			NullCheck((Func_2_t1426983263 *)L_4);
			RuntimeObject* L_6 = ((  RuntimeObject* (*) (Func_2_t1426983263 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t1426983263 *)L_4, (RuntimeObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			V_6 = (RuntimeObject*)L_6;
			RuntimeObject* L_7 = V_6;
			bool L_8 = ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject*, int32_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_7, (int32_t*)(&V_7), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
			if (!L_8)
			{
				goto IL_005b;
			}
		}

IL_0042:
		{
			int32_t L_9 = V_7;
			if ((((int32_t)L_9) <= ((int32_t)0)))
			{
				goto IL_006f;
			}
		}

IL_0047:
		{
			int32_t L_10 = V_7;
			SparseArrayBuilder_1_Reserve_m3186431978((SparseArrayBuilder_1_t3713953326 *)(&V_0), (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			RuntimeObject* L_11 = V_6;
			ArrayBuilder_1_Add_m3352372406((ArrayBuilder_1_t3343055733 *)(&V_1), (RuntimeObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_006f;
		}

IL_005b:
		{
			Func_2_t1426983263 * L_12 = (Func_2_t1426983263 *)__this->get__selector_4();
			RuntimeObject * L_13 = V_5;
			NullCheck((Func_2_t1426983263 *)L_12);
			RuntimeObject* L_14 = ((  RuntimeObject* (*) (Func_2_t1426983263 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t1426983263 *)L_12, (RuntimeObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			SparseArrayBuilder_1_AddRange_m3173835490((SparseArrayBuilder_1_t3713953326 *)(&V_0), (RuntimeObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		}

IL_006f:
		{
			RuntimeObject* L_15 = V_4;
			NullCheck((RuntimeObject*)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_15);
			if (L_16)
			{
				goto IL_001f;
			}
		}

IL_0078:
		{
			IL2CPP_LEAVE(0x86, FINALLY_007a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_007a;
	}

FINALLY_007a:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_17 = V_4;
			if (!L_17)
			{
				goto IL_0085;
			}
		}

IL_007e:
		{
			RuntimeObject* L_18 = V_4;
			NullCheck((RuntimeObject*)L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_18);
		}

IL_0085:
		{
			IL2CPP_END_FINALLY(122)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(122)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0086:
	{
		ObjectU5BU5D_t2843939325* L_19 = SparseArrayBuilder_1_ToArray_m1576911221((SparseArrayBuilder_1_t3713953326 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_2 = (ObjectU5BU5D_t2843939325*)L_19;
		ArrayBuilder_1_t4177874457  L_20 = SparseArrayBuilder_1_get_Markers_m716823650((SparseArrayBuilder_1_t3713953326 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_3 = (ArrayBuilder_1_t4177874457 )L_20;
		V_8 = (int32_t)0;
		goto IL_00c9;
	}

IL_009b:
	{
		int32_t L_21 = V_8;
		Marker_t2894777777  L_22 = ArrayBuilder_1_get_Item_m3563499930((ArrayBuilder_1_t4177874457 *)(&V_3), (int32_t)L_21, /*hidden argument*/ArrayBuilder_1_get_Item_m3563499930_RuntimeMethod_var);
		V_9 = (Marker_t2894777777 )L_22;
		int32_t L_23 = V_8;
		RuntimeObject* L_24 = ArrayBuilder_1_get_Item_m3142556344((ArrayBuilder_1_t3343055733 *)(&V_1), (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		ObjectU5BU5D_t2843939325* L_25 = V_2;
		int32_t L_26 = Marker_get_Index_m3525311458((Marker_t2894777777 *)(&V_9), /*hidden argument*/NULL);
		int32_t L_27 = Marker_get_Count_m126194417((Marker_t2894777777 *)(&V_9), /*hidden argument*/NULL);
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject*, ObjectU5BU5D_t2843939325*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_24, (ObjectU5BU5D_t2843939325*)L_25, (int32_t)L_26, (int32_t)L_27, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		int32_t L_28 = V_8;
		V_8 = (int32_t)((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00c9:
	{
		int32_t L_29 = V_8;
		int32_t L_30 = ArrayBuilder_1_get_Count_m2587795986((ArrayBuilder_1_t4177874457 *)(&V_3), /*hidden argument*/ArrayBuilder_1_get_Count_m2587795986_RuntimeMethod_var);
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_009b;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_31 = V_2;
		return L_31;
	}
}
// System.Collections.Generic.List`1<TResult> System.Linq.Enumerable/SelectManySingleSelectorIterator`2<System.Object,System.Object>::ToList()
extern "C"  List_1_t257213610 * SelectManySingleSelectorIterator_2_ToList_m2597685119_gshared (SelectManySingleSelectorIterator_2_t963716220 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectManySingleSelectorIterator_2_ToList_m2597685119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t257213610 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20));
		((  void (*) (List_1_t257213610 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		V_0 = (List_1_t257213610 *)L_0;
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__source_3();
		NullCheck((RuntimeObject*)L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_1);
		V_1 = (RuntimeObject*)L_2;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002d;
		}

IL_0014:
		{
			RuntimeObject* L_3 = V_1;
			NullCheck((RuntimeObject*)L_3);
			RuntimeObject * L_4 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (RuntimeObject*)L_3);
			V_2 = (RuntimeObject *)L_4;
			List_1_t257213610 * L_5 = V_0;
			Func_2_t1426983263 * L_6 = (Func_2_t1426983263 *)__this->get__selector_4();
			RuntimeObject * L_7 = V_2;
			NullCheck((Func_2_t1426983263 *)L_6);
			RuntimeObject* L_8 = ((  RuntimeObject* (*) (Func_2_t1426983263 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t1426983263 *)L_6, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			NullCheck((List_1_t257213610 *)L_5);
			((  void (*) (List_1_t257213610 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)((List_1_t257213610 *)L_5, (RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		}

IL_002d:
		{
			RuntimeObject* L_9 = V_1;
			NullCheck((RuntimeObject*)L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_9);
			if (L_10)
			{
				goto IL_0014;
			}
		}

IL_0035:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_11 = V_1;
			if (!L_11)
			{
				goto IL_0040;
			}
		}

IL_003a:
		{
			RuntimeObject* L_12 = V_1;
			NullCheck((RuntimeObject*)L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_12);
		}

IL_0040:
		{
			IL2CPP_END_FINALLY(55)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0041:
	{
		List_1_t257213610 * L_13 = V_0;
		return L_13;
	}
}
// System.Void System.Linq.Enumerable/UnionIterator`1<System.Char>::.ctor(System.Collections.Generic.IEqualityComparer`1<TSource>)
extern "C"  void UnionIterator_1__ctor_m1456386432_gshared (UnionIterator_1_t3402001247 * __this, RuntimeObject* ___comparer0, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2588820807 *)__this);
		((  void (*) (Iterator_1_t2588820807 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t2588820807 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		RuntimeObject* L_0 = ___comparer0;
		__this->set__comparer_3(L_0);
		return;
	}
}
// System.Void System.Linq.Enumerable/UnionIterator`1<System.Char>::Dispose()
extern "C"  void UnionIterator_1_Dispose_m3312131536_gshared (UnionIterator_1_t3402001247 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator_1_Dispose_m3312131536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__enumerator_4();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__enumerator_4();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
		__this->set__enumerator_4((RuntimeObject*)NULL);
		__this->set__set_5((Set_1_t329142533 *)NULL);
	}

IL_0021:
	{
		NullCheck((Iterator_1_t2588820807 *)__this);
		((  void (*) (Iterator_1_t2588820807 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Iterator_1_t2588820807 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void System.Linq.Enumerable/UnionIterator`1<System.Char>::SetEnumerator(System.Collections.Generic.IEnumerator`1<TSource>)
extern "C"  void UnionIterator_1_SetEnumerator_m1125297949_gshared (UnionIterator_1_t3402001247 * __this, RuntimeObject* ___enumerator0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator_1_SetEnumerator_m1125297949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__enumerator_4();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__enumerator_4();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		RuntimeObject* L_2 = ___enumerator0;
		__this->set__enumerator_4(L_2);
		return;
	}
}
// System.Void System.Linq.Enumerable/UnionIterator`1<System.Char>::StoreFirst()
extern "C"  void UnionIterator_1_StoreFirst_m3241527884_gshared (UnionIterator_1_t3402001247 * __this, const RuntimeMethod* method)
{
	Set_1_t329142533 * V_0 = NULL;
	Il2CppChar V_1 = 0x0;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__comparer_3();
		Set_1_t329142533 * L_1 = (Set_1_t329142533 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (Set_1_t329142533 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_1, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (Set_1_t329142533 *)L_1;
		RuntimeObject* L_2 = (RuntimeObject*)__this->get__enumerator_4();
		NullCheck((RuntimeObject*)L_2);
		Il2CppChar L_3 = InterfaceFuncInvoker0< Il2CppChar >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Char>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_2);
		V_1 = (Il2CppChar)L_3;
		Set_1_t329142533 * L_4 = V_0;
		Il2CppChar L_5 = V_1;
		NullCheck((Set_1_t329142533 *)L_4);
		((  bool (*) (Set_1_t329142533 *, Il2CppChar, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Set_1_t329142533 *)L_4, (Il2CppChar)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Il2CppChar L_6 = V_1;
		((Iterator_1_t2588820807 *)__this)->set__current_2(L_6);
		Set_1_t329142533 * L_7 = V_0;
		__this->set__set_5(L_7);
		return;
	}
}
// System.Boolean System.Linq.Enumerable/UnionIterator`1<System.Char>::GetNext()
extern "C"  bool UnionIterator_1_GetNext_m1279570535_gshared (UnionIterator_1_t3402001247 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator_1_GetNext_m1279570535_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Set_1_t329142533 * V_0 = NULL;
	Il2CppChar V_1 = 0x0;
	{
		Set_1_t329142533 * L_0 = (Set_1_t329142533 *)__this->get__set_5();
		V_0 = (Set_1_t329142533 *)L_0;
		goto IL_0027;
	}

IL_0009:
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__enumerator_4();
		NullCheck((RuntimeObject*)L_1);
		Il2CppChar L_2 = InterfaceFuncInvoker0< Il2CppChar >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Char>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_1);
		V_1 = (Il2CppChar)L_2;
		Set_1_t329142533 * L_3 = V_0;
		Il2CppChar L_4 = V_1;
		NullCheck((Set_1_t329142533 *)L_3);
		bool L_5 = ((  bool (*) (Set_1_t329142533 *, Il2CppChar, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Set_1_t329142533 *)L_3, (Il2CppChar)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if (!L_5)
		{
			goto IL_0027;
		}
	}
	{
		Il2CppChar L_6 = V_1;
		((Iterator_1_t2588820807 *)__this)->set__current_2(L_6);
		return (bool)1;
	}

IL_0027:
	{
		RuntimeObject* L_7 = (RuntimeObject*)__this->get__enumerator_4();
		NullCheck((RuntimeObject*)L_7);
		bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_7);
		if (L_8)
		{
			goto IL_0009;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean System.Linq.Enumerable/UnionIterator`1<System.Char>::MoveNext()
extern "C"  bool UnionIterator_1_MoveNext_m375289487_gshared (UnionIterator_1_t3402001247 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator_1_MoveNext_m375289487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t2588820807 *)__this)->get__state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0053;
		}
	}
	{
		NullCheck((UnionIterator_1_t3402001247 *)__this);
		RuntimeObject* L_1 = VirtFuncInvoker1< RuntimeObject*, int32_t >::Invoke(19 /* System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator`1<System.Char>::GetEnumerable(System.Int32) */, (UnionIterator_1_t3402001247 *)__this, (int32_t)0);
		V_0 = (RuntimeObject*)L_1;
		goto IL_004e;
	}

IL_0013:
	{
		RuntimeObject* L_2 = V_0;
		NullCheck((RuntimeObject*)L_2);
		RuntimeObject* L_3 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Char>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_2);
		V_1 = (RuntimeObject*)L_3;
		int32_t L_4 = (int32_t)((Iterator_1_t2588820807 *)__this)->get__state_1();
		((Iterator_1_t2588820807 *)__this)->set__state_1(((int32_t)((int32_t)L_4+(int32_t)1)));
		RuntimeObject* L_5 = V_1;
		NullCheck((RuntimeObject*)L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_5);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		RuntimeObject* L_7 = V_1;
		NullCheck((UnionIterator_1_t3402001247 *)__this);
		((  void (*) (UnionIterator_1_t3402001247 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((UnionIterator_1_t3402001247 *)__this, (RuntimeObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		NullCheck((UnionIterator_1_t3402001247 *)__this);
		((  void (*) (UnionIterator_1_t3402001247 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((UnionIterator_1_t3402001247 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return (bool)1;
	}

IL_003f:
	{
		int32_t L_8 = (int32_t)((Iterator_1_t2588820807 *)__this)->get__state_1();
		NullCheck((UnionIterator_1_t3402001247 *)__this);
		RuntimeObject* L_9 = VirtFuncInvoker1< RuntimeObject*, int32_t >::Invoke(19 /* System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator`1<System.Char>::GetEnumerable(System.Int32) */, (UnionIterator_1_t3402001247 *)__this, (int32_t)((int32_t)((int32_t)L_8-(int32_t)1)));
		V_0 = (RuntimeObject*)L_9;
	}

IL_004e:
	{
		RuntimeObject* L_10 = V_0;
		if (L_10)
		{
			goto IL_0013;
		}
	}
	{
		goto IL_0094;
	}

IL_0053:
	{
		int32_t L_11 = (int32_t)((Iterator_1_t2588820807 *)__this)->get__state_1();
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0094;
		}
	}

IL_005c:
	{
		NullCheck((UnionIterator_1_t3402001247 *)__this);
		bool L_12 = ((  bool (*) (UnionIterator_1_t3402001247 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((UnionIterator_1_t3402001247 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_12)
		{
			goto IL_0066;
		}
	}
	{
		return (bool)1;
	}

IL_0066:
	{
		int32_t L_13 = (int32_t)((Iterator_1_t2588820807 *)__this)->get__state_1();
		NullCheck((UnionIterator_1_t3402001247 *)__this);
		RuntimeObject* L_14 = VirtFuncInvoker1< RuntimeObject*, int32_t >::Invoke(19 /* System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator`1<System.Char>::GetEnumerable(System.Int32) */, (UnionIterator_1_t3402001247 *)__this, (int32_t)((int32_t)((int32_t)L_13-(int32_t)1)));
		V_2 = (RuntimeObject*)L_14;
		RuntimeObject* L_15 = V_2;
		if (!L_15)
		{
			goto IL_0094;
		}
	}
	{
		RuntimeObject* L_16 = V_2;
		NullCheck((RuntimeObject*)L_16);
		RuntimeObject* L_17 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Char>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_16);
		NullCheck((UnionIterator_1_t3402001247 *)__this);
		((  void (*) (UnionIterator_1_t3402001247 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((UnionIterator_1_t3402001247 *)__this, (RuntimeObject*)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		int32_t L_18 = (int32_t)((Iterator_1_t2588820807 *)__this)->get__state_1();
		((Iterator_1_t2588820807 *)__this)->set__state_1(((int32_t)((int32_t)L_18+(int32_t)1)));
		goto IL_005c;
	}

IL_0094:
	{
		NullCheck((Iterator_1_t2588820807 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Char>::Dispose() */, (Iterator_1_t2588820807 *)__this);
		return (bool)0;
	}
}
// System.Linq.Set`1<TSource> System.Linq.Enumerable/UnionIterator`1<System.Char>::FillSet()
extern "C"  Set_1_t329142533 * UnionIterator_1_FillSet_m2793201565_gshared (UnionIterator_1_t3402001247 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator_1_FillSet_m2793201565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Set_1_t329142533 * V_0 = NULL;
	int32_t V_1 = 0;
	RuntimeObject* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	Il2CppChar V_4 = 0x0;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__comparer_3();
		Set_1_t329142533 * L_1 = (Set_1_t329142533 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (Set_1_t329142533 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_1, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (Set_1_t329142533 *)L_1;
		V_1 = (int32_t)0;
	}

IL_000e:
	{
		int32_t L_2 = V_1;
		NullCheck((UnionIterator_1_t3402001247 *)__this);
		RuntimeObject* L_3 = VirtFuncInvoker1< RuntimeObject*, int32_t >::Invoke(19 /* System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator`1<System.Char>::GetEnumerable(System.Int32) */, (UnionIterator_1_t3402001247 *)__this, (int32_t)L_2);
		V_2 = (RuntimeObject*)L_3;
		RuntimeObject* L_4 = V_2;
		if (L_4)
		{
			goto IL_001b;
		}
	}
	{
		Set_1_t329142533 * L_5 = V_0;
		return L_5;
	}

IL_001b:
	{
		RuntimeObject* L_6 = V_2;
		NullCheck((RuntimeObject*)L_6);
		RuntimeObject* L_7 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Char>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_6);
		V_3 = (RuntimeObject*)L_7;
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0035;
		}

IL_0024:
		{
			RuntimeObject* L_8 = V_3;
			NullCheck((RuntimeObject*)L_8);
			Il2CppChar L_9 = InterfaceFuncInvoker0< Il2CppChar >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Char>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_8);
			V_4 = (Il2CppChar)L_9;
			Set_1_t329142533 * L_10 = V_0;
			Il2CppChar L_11 = V_4;
			NullCheck((Set_1_t329142533 *)L_10);
			((  bool (*) (Set_1_t329142533 *, Il2CppChar, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Set_1_t329142533 *)L_10, (Il2CppChar)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		}

IL_0035:
		{
			RuntimeObject* L_12 = V_3;
			NullCheck((RuntimeObject*)L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_12);
			if (L_13)
			{
				goto IL_0024;
			}
		}

IL_003d:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_14 = V_3;
			if (!L_14)
			{
				goto IL_0048;
			}
		}

IL_0042:
		{
			RuntimeObject* L_15 = V_3;
			NullCheck((RuntimeObject*)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_15);
		}

IL_0048:
		{
			IL2CPP_END_FINALLY(63)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0049:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
		goto IL_000e;
	}
}
// TSource[] System.Linq.Enumerable/UnionIterator`1<System.Char>::ToArray()
extern "C"  CharU5BU5D_t3528271667* UnionIterator_1_ToArray_m1391168839_gshared (UnionIterator_1_t3402001247 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((UnionIterator_1_t3402001247 *)__this);
		Set_1_t329142533 * L_0 = ((  Set_1_t329142533 * (*) (UnionIterator_1_t3402001247 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((UnionIterator_1_t3402001247 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Set_1_t329142533 *)L_0);
		CharU5BU5D_t3528271667* L_1 = ((  CharU5BU5D_t3528271667* (*) (Set_1_t329142533 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Set_1_t329142533 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_1;
	}
}
// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/UnionIterator`1<System.Char>::ToList()
extern "C"  List_1_t811567916 * UnionIterator_1_ToList_m2593088018_gshared (UnionIterator_1_t3402001247 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((UnionIterator_1_t3402001247 *)__this);
		Set_1_t329142533 * L_0 = ((  Set_1_t329142533 * (*) (UnionIterator_1_t3402001247 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((UnionIterator_1_t3402001247 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Set_1_t329142533 *)L_0);
		List_1_t811567916 * L_1 = ((  List_1_t811567916 * (*) (Set_1_t329142533 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((Set_1_t329142533 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_1;
	}
}
// System.Int32 System.Linq.Enumerable/UnionIterator`1<System.Char>::GetCount(System.Boolean)
extern "C"  int32_t UnionIterator_1_GetCount_m83266941_gshared (UnionIterator_1_t3402001247 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___onlyIfCheap0;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		NullCheck((UnionIterator_1_t3402001247 *)__this);
		Set_1_t329142533 * L_1 = ((  Set_1_t329142533 * (*) (UnionIterator_1_t3402001247 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((UnionIterator_1_t3402001247 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Set_1_t329142533 *)L_1);
		int32_t L_2 = ((  int32_t (*) (Set_1_t329142533 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Set_1_t329142533 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_2;
	}

IL_000f:
	{
		return (-1);
	}
}
// System.Void System.Linq.Enumerable/UnionIterator`1<System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TSource>)
extern "C"  void UnionIterator_1__ctor_m2074701123_gshared (UnionIterator_1_t2847646941 * __this, RuntimeObject* ___comparer0, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		((  void (*) (Iterator_1_t2034466501 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t2034466501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		RuntimeObject* L_0 = ___comparer0;
		__this->set__comparer_3(L_0);
		return;
	}
}
// System.Void System.Linq.Enumerable/UnionIterator`1<System.Object>::Dispose()
extern "C"  void UnionIterator_1_Dispose_m745608159_gshared (UnionIterator_1_t2847646941 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator_1_Dispose_m745608159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__enumerator_4();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__enumerator_4();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
		__this->set__enumerator_4((RuntimeObject*)NULL);
		__this->set__set_5((Set_1_t4069755523 *)NULL);
	}

IL_0021:
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		((  void (*) (Iterator_1_t2034466501 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Iterator_1_t2034466501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void System.Linq.Enumerable/UnionIterator`1<System.Object>::SetEnumerator(System.Collections.Generic.IEnumerator`1<TSource>)
extern "C"  void UnionIterator_1_SetEnumerator_m3710368852_gshared (UnionIterator_1_t2847646941 * __this, RuntimeObject* ___enumerator0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator_1_SetEnumerator_m3710368852_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__enumerator_4();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__enumerator_4();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		RuntimeObject* L_2 = ___enumerator0;
		__this->set__enumerator_4(L_2);
		return;
	}
}
// System.Void System.Linq.Enumerable/UnionIterator`1<System.Object>::StoreFirst()
extern "C"  void UnionIterator_1_StoreFirst_m329119166_gshared (UnionIterator_1_t2847646941 * __this, const RuntimeMethod* method)
{
	Set_1_t4069755523 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__comparer_3();
		Set_1_t4069755523 * L_1 = (Set_1_t4069755523 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (Set_1_t4069755523 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_1, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (Set_1_t4069755523 *)L_1;
		RuntimeObject* L_2 = (RuntimeObject*)__this->get__enumerator_4();
		NullCheck((RuntimeObject*)L_2);
		RuntimeObject * L_3 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_2);
		V_1 = (RuntimeObject *)L_3;
		Set_1_t4069755523 * L_4 = V_0;
		RuntimeObject * L_5 = V_1;
		NullCheck((Set_1_t4069755523 *)L_4);
		((  bool (*) (Set_1_t4069755523 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Set_1_t4069755523 *)L_4, (RuntimeObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		RuntimeObject * L_6 = V_1;
		((Iterator_1_t2034466501 *)__this)->set__current_2(L_6);
		Set_1_t4069755523 * L_7 = V_0;
		__this->set__set_5(L_7);
		return;
	}
}
// System.Boolean System.Linq.Enumerable/UnionIterator`1<System.Object>::GetNext()
extern "C"  bool UnionIterator_1_GetNext_m235872769_gshared (UnionIterator_1_t2847646941 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator_1_GetNext_m235872769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Set_1_t4069755523 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	{
		Set_1_t4069755523 * L_0 = (Set_1_t4069755523 *)__this->get__set_5();
		V_0 = (Set_1_t4069755523 *)L_0;
		goto IL_0027;
	}

IL_0009:
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__enumerator_4();
		NullCheck((RuntimeObject*)L_1);
		RuntimeObject * L_2 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_1);
		V_1 = (RuntimeObject *)L_2;
		Set_1_t4069755523 * L_3 = V_0;
		RuntimeObject * L_4 = V_1;
		NullCheck((Set_1_t4069755523 *)L_3);
		bool L_5 = ((  bool (*) (Set_1_t4069755523 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Set_1_t4069755523 *)L_3, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if (!L_5)
		{
			goto IL_0027;
		}
	}
	{
		RuntimeObject * L_6 = V_1;
		((Iterator_1_t2034466501 *)__this)->set__current_2(L_6);
		return (bool)1;
	}

IL_0027:
	{
		RuntimeObject* L_7 = (RuntimeObject*)__this->get__enumerator_4();
		NullCheck((RuntimeObject*)L_7);
		bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_7);
		if (L_8)
		{
			goto IL_0009;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean System.Linq.Enumerable/UnionIterator`1<System.Object>::MoveNext()
extern "C"  bool UnionIterator_1_MoveNext_m4242229629_gshared (UnionIterator_1_t2847646941 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator_1_MoveNext_m4242229629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t2034466501 *)__this)->get__state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0053;
		}
	}
	{
		NullCheck((UnionIterator_1_t2847646941 *)__this);
		RuntimeObject* L_1 = VirtFuncInvoker1< RuntimeObject*, int32_t >::Invoke(19 /* System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator`1<System.Object>::GetEnumerable(System.Int32) */, (UnionIterator_1_t2847646941 *)__this, (int32_t)0);
		V_0 = (RuntimeObject*)L_1;
		goto IL_004e;
	}

IL_0013:
	{
		RuntimeObject* L_2 = V_0;
		NullCheck((RuntimeObject*)L_2);
		RuntimeObject* L_3 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_2);
		V_1 = (RuntimeObject*)L_3;
		int32_t L_4 = (int32_t)((Iterator_1_t2034466501 *)__this)->get__state_1();
		((Iterator_1_t2034466501 *)__this)->set__state_1(((int32_t)((int32_t)L_4+(int32_t)1)));
		RuntimeObject* L_5 = V_1;
		NullCheck((RuntimeObject*)L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_5);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		RuntimeObject* L_7 = V_1;
		NullCheck((UnionIterator_1_t2847646941 *)__this);
		((  void (*) (UnionIterator_1_t2847646941 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((UnionIterator_1_t2847646941 *)__this, (RuntimeObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		NullCheck((UnionIterator_1_t2847646941 *)__this);
		((  void (*) (UnionIterator_1_t2847646941 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((UnionIterator_1_t2847646941 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return (bool)1;
	}

IL_003f:
	{
		int32_t L_8 = (int32_t)((Iterator_1_t2034466501 *)__this)->get__state_1();
		NullCheck((UnionIterator_1_t2847646941 *)__this);
		RuntimeObject* L_9 = VirtFuncInvoker1< RuntimeObject*, int32_t >::Invoke(19 /* System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator`1<System.Object>::GetEnumerable(System.Int32) */, (UnionIterator_1_t2847646941 *)__this, (int32_t)((int32_t)((int32_t)L_8-(int32_t)1)));
		V_0 = (RuntimeObject*)L_9;
	}

IL_004e:
	{
		RuntimeObject* L_10 = V_0;
		if (L_10)
		{
			goto IL_0013;
		}
	}
	{
		goto IL_0094;
	}

IL_0053:
	{
		int32_t L_11 = (int32_t)((Iterator_1_t2034466501 *)__this)->get__state_1();
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0094;
		}
	}

IL_005c:
	{
		NullCheck((UnionIterator_1_t2847646941 *)__this);
		bool L_12 = ((  bool (*) (UnionIterator_1_t2847646941 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((UnionIterator_1_t2847646941 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_12)
		{
			goto IL_0066;
		}
	}
	{
		return (bool)1;
	}

IL_0066:
	{
		int32_t L_13 = (int32_t)((Iterator_1_t2034466501 *)__this)->get__state_1();
		NullCheck((UnionIterator_1_t2847646941 *)__this);
		RuntimeObject* L_14 = VirtFuncInvoker1< RuntimeObject*, int32_t >::Invoke(19 /* System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator`1<System.Object>::GetEnumerable(System.Int32) */, (UnionIterator_1_t2847646941 *)__this, (int32_t)((int32_t)((int32_t)L_13-(int32_t)1)));
		V_2 = (RuntimeObject*)L_14;
		RuntimeObject* L_15 = V_2;
		if (!L_15)
		{
			goto IL_0094;
		}
	}
	{
		RuntimeObject* L_16 = V_2;
		NullCheck((RuntimeObject*)L_16);
		RuntimeObject* L_17 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_16);
		NullCheck((UnionIterator_1_t2847646941 *)__this);
		((  void (*) (UnionIterator_1_t2847646941 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((UnionIterator_1_t2847646941 *)__this, (RuntimeObject*)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		int32_t L_18 = (int32_t)((Iterator_1_t2034466501 *)__this)->get__state_1();
		((Iterator_1_t2034466501 *)__this)->set__state_1(((int32_t)((int32_t)L_18+(int32_t)1)));
		goto IL_005c;
	}

IL_0094:
	{
		NullCheck((Iterator_1_t2034466501 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t2034466501 *)__this);
		return (bool)0;
	}
}
// System.Linq.Set`1<TSource> System.Linq.Enumerable/UnionIterator`1<System.Object>::FillSet()
extern "C"  Set_1_t4069755523 * UnionIterator_1_FillSet_m552649502_gshared (UnionIterator_1_t2847646941 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator_1_FillSet_m552649502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Set_1_t4069755523 * V_0 = NULL;
	int32_t V_1 = 0;
	RuntimeObject* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	RuntimeObject * V_4 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__comparer_3();
		Set_1_t4069755523 * L_1 = (Set_1_t4069755523 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (Set_1_t4069755523 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_1, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (Set_1_t4069755523 *)L_1;
		V_1 = (int32_t)0;
	}

IL_000e:
	{
		int32_t L_2 = V_1;
		NullCheck((UnionIterator_1_t2847646941 *)__this);
		RuntimeObject* L_3 = VirtFuncInvoker1< RuntimeObject*, int32_t >::Invoke(19 /* System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator`1<System.Object>::GetEnumerable(System.Int32) */, (UnionIterator_1_t2847646941 *)__this, (int32_t)L_2);
		V_2 = (RuntimeObject*)L_3;
		RuntimeObject* L_4 = V_2;
		if (L_4)
		{
			goto IL_001b;
		}
	}
	{
		Set_1_t4069755523 * L_5 = V_0;
		return L_5;
	}

IL_001b:
	{
		RuntimeObject* L_6 = V_2;
		NullCheck((RuntimeObject*)L_6);
		RuntimeObject* L_7 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (RuntimeObject*)L_6);
		V_3 = (RuntimeObject*)L_7;
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0035;
		}

IL_0024:
		{
			RuntimeObject* L_8 = V_3;
			NullCheck((RuntimeObject*)L_8);
			RuntimeObject * L_9 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (RuntimeObject*)L_8);
			V_4 = (RuntimeObject *)L_9;
			Set_1_t4069755523 * L_10 = V_0;
			RuntimeObject * L_11 = V_4;
			NullCheck((Set_1_t4069755523 *)L_10);
			((  bool (*) (Set_1_t4069755523 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Set_1_t4069755523 *)L_10, (RuntimeObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		}

IL_0035:
		{
			RuntimeObject* L_12 = V_3;
			NullCheck((RuntimeObject*)L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (RuntimeObject*)L_12);
			if (L_13)
			{
				goto IL_0024;
			}
		}

IL_003d:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_14 = V_3;
			if (!L_14)
			{
				goto IL_0048;
			}
		}

IL_0042:
		{
			RuntimeObject* L_15 = V_3;
			NullCheck((RuntimeObject*)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (RuntimeObject*)L_15);
		}

IL_0048:
		{
			IL2CPP_END_FINALLY(63)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0049:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
		goto IL_000e;
	}
}
// TSource[] System.Linq.Enumerable/UnionIterator`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t2843939325* UnionIterator_1_ToArray_m1560549058_gshared (UnionIterator_1_t2847646941 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((UnionIterator_1_t2847646941 *)__this);
		Set_1_t4069755523 * L_0 = ((  Set_1_t4069755523 * (*) (UnionIterator_1_t2847646941 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((UnionIterator_1_t2847646941 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Set_1_t4069755523 *)L_0);
		ObjectU5BU5D_t2843939325* L_1 = ((  ObjectU5BU5D_t2843939325* (*) (Set_1_t4069755523 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Set_1_t4069755523 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_1;
	}
}
// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/UnionIterator`1<System.Object>::ToList()
extern "C"  List_1_t257213610 * UnionIterator_1_ToList_m2876085498_gshared (UnionIterator_1_t2847646941 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((UnionIterator_1_t2847646941 *)__this);
		Set_1_t4069755523 * L_0 = ((  Set_1_t4069755523 * (*) (UnionIterator_1_t2847646941 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((UnionIterator_1_t2847646941 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Set_1_t4069755523 *)L_0);
		List_1_t257213610 * L_1 = ((  List_1_t257213610 * (*) (Set_1_t4069755523 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((Set_1_t4069755523 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_1;
	}
}
// System.Int32 System.Linq.Enumerable/UnionIterator`1<System.Object>::GetCount(System.Boolean)
extern "C"  int32_t UnionIterator_1_GetCount_m3748804620_gshared (UnionIterator_1_t2847646941 * __this, bool ___onlyIfCheap0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___onlyIfCheap0;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		NullCheck((UnionIterator_1_t2847646941 *)__this);
		Set_1_t4069755523 * L_1 = ((  Set_1_t4069755523 * (*) (UnionIterator_1_t2847646941 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((UnionIterator_1_t2847646941 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Set_1_t4069755523 *)L_1);
		int32_t L_2 = ((  int32_t (*) (Set_1_t4069755523 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Set_1_t4069755523 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_2;
	}

IL_000f:
	{
		return (-1);
	}
}
// System.Void System.Linq.Enumerable/UnionIterator2`1<System.Char>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
extern "C"  void UnionIterator2_1__ctor_m832565037_gshared (UnionIterator2_1_t1321734732 * __this, RuntimeObject* ___first0, RuntimeObject* ___second1, RuntimeObject* ___comparer2, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___comparer2;
		NullCheck((UnionIterator_1_t3402001247 *)__this);
		((  void (*) (UnionIterator_1_t3402001247 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((UnionIterator_1_t3402001247 *)__this, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		RuntimeObject* L_1 = ___first0;
		__this->set__first_6(L_1);
		RuntimeObject* L_2 = ___second1;
		__this->set__second_7(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/UnionIterator2`1<System.Char>::Clone()
extern "C"  Iterator_1_t2588820807 * UnionIterator2_1_Clone_m1092059002_gshared (UnionIterator2_1_t1321734732 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__first_6();
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__second_7();
		RuntimeObject* L_2 = (RuntimeObject*)((UnionIterator_1_t3402001247 *)__this)->get__comparer_3();
		UnionIterator2_1_t1321734732 * L_3 = (UnionIterator2_1_t1321734732 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (UnionIterator2_1_t1321734732 *, RuntimeObject*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_3, (RuntimeObject*)L_0, (RuntimeObject*)L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator2`1<System.Char>::GetEnumerable(System.Int32)
extern "C"  RuntimeObject* UnionIterator2_1_GetEnumerable_m3932698187_gshared (UnionIterator2_1_t1321734732 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0010;
		}
	}
	{
		goto IL_0017;
	}

IL_0009:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get__first_6();
		return L_2;
	}

IL_0010:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get__second_7();
		return L_3;
	}

IL_0017:
	{
		return (RuntimeObject*)NULL;
	}
}
// System.Linq.Enumerable/UnionIterator`1<TSource> System.Linq.Enumerable/UnionIterator2`1<System.Char>::Union(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  UnionIterator_1_t3402001247 * UnionIterator2_1_Union_m4094657135_gshared (UnionIterator2_1_t1321734732 * __this, RuntimeObject* ___next0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___next0;
		UnionIteratorN_1_t1272659448 * L_1 = (UnionIteratorN_1_t1272659448 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		((  void (*) (UnionIteratorN_1_t1272659448 *, UnionIterator_1_t3402001247 *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_1, (UnionIterator_1_t3402001247 *)__this, (RuntimeObject*)L_0, (int32_t)2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_1;
	}
}
// System.Void System.Linq.Enumerable/UnionIterator2`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
extern "C"  void UnionIterator2_1__ctor_m4294640512_gshared (UnionIterator2_1_t767380426 * __this, RuntimeObject* ___first0, RuntimeObject* ___second1, RuntimeObject* ___comparer2, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___comparer2;
		NullCheck((UnionIterator_1_t2847646941 *)__this);
		((  void (*) (UnionIterator_1_t2847646941 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((UnionIterator_1_t2847646941 *)__this, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		RuntimeObject* L_1 = ___first0;
		__this->set__first_6(L_1);
		RuntimeObject* L_2 = ___second1;
		__this->set__second_7(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/UnionIterator2`1<System.Object>::Clone()
extern "C"  Iterator_1_t2034466501 * UnionIterator2_1_Clone_m3694072997_gshared (UnionIterator2_1_t767380426 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get__first_6();
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__second_7();
		RuntimeObject* L_2 = (RuntimeObject*)((UnionIterator_1_t2847646941 *)__this)->get__comparer_3();
		UnionIterator2_1_t767380426 * L_3 = (UnionIterator2_1_t767380426 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (UnionIterator2_1_t767380426 *, RuntimeObject*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_3, (RuntimeObject*)L_0, (RuntimeObject*)L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator2`1<System.Object>::GetEnumerable(System.Int32)
extern "C"  RuntimeObject* UnionIterator2_1_GetEnumerable_m3722855478_gshared (UnionIterator2_1_t767380426 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0010;
		}
	}
	{
		goto IL_0017;
	}

IL_0009:
	{
		RuntimeObject* L_2 = (RuntimeObject*)__this->get__first_6();
		return L_2;
	}

IL_0010:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get__second_7();
		return L_3;
	}

IL_0017:
	{
		return (RuntimeObject*)NULL;
	}
}
// System.Linq.Enumerable/UnionIterator`1<TSource> System.Linq.Enumerable/UnionIterator2`1<System.Object>::Union(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  UnionIterator_1_t2847646941 * UnionIterator2_1_Union_m3761667175_gshared (UnionIterator2_1_t767380426 * __this, RuntimeObject* ___next0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___next0;
		UnionIteratorN_1_t718305142 * L_1 = (UnionIteratorN_1_t718305142 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		((  void (*) (UnionIteratorN_1_t718305142 *, UnionIterator_1_t2847646941 *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_1, (UnionIterator_1_t2847646941 *)__this, (RuntimeObject*)L_0, (int32_t)2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_1;
	}
}
// System.Void System.Linq.Enumerable/UnionIteratorN`1<System.Char>::.ctor(System.Linq.Enumerable/UnionIterator`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
extern "C"  void UnionIteratorN_1__ctor_m204670800_gshared (UnionIteratorN_1_t1272659448 * __this, UnionIterator_1_t3402001247 * ___previous0, RuntimeObject* ___next1, int32_t ___nextIndex2, const RuntimeMethod* method)
{
	{
		UnionIterator_1_t3402001247 * L_0 = ___previous0;
		NullCheck(L_0);
		RuntimeObject* L_1 = (RuntimeObject*)L_0->get__comparer_3();
		NullCheck((UnionIterator_1_t3402001247 *)__this);
		((  void (*) (UnionIterator_1_t3402001247 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((UnionIterator_1_t3402001247 *)__this, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		UnionIterator_1_t3402001247 * L_2 = ___previous0;
		__this->set__previous_6(L_2);
		RuntimeObject* L_3 = ___next1;
		__this->set__next_7(L_3);
		int32_t L_4 = ___nextIndex2;
		__this->set__nextIndex_8(L_4);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/UnionIteratorN`1<System.Char>::Clone()
extern "C"  Iterator_1_t2588820807 * UnionIteratorN_1_Clone_m2271401880_gshared (UnionIteratorN_1_t1272659448 * __this, const RuntimeMethod* method)
{
	{
		UnionIterator_1_t3402001247 * L_0 = (UnionIterator_1_t3402001247 *)__this->get__previous_6();
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__next_7();
		int32_t L_2 = (int32_t)__this->get__nextIndex_8();
		UnionIteratorN_1_t1272659448 * L_3 = (UnionIteratorN_1_t1272659448 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (UnionIteratorN_1_t1272659448 *, UnionIterator_1_t3402001247 *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_3, (UnionIterator_1_t3402001247 *)L_0, (RuntimeObject*)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIteratorN`1<System.Char>::GetEnumerable(System.Int32)
extern "C"  RuntimeObject* UnionIteratorN_1_GetEnumerable_m3036892948_gshared (UnionIteratorN_1_t1272659448 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	UnionIteratorN_1_t1272659448 * V_0 = NULL;
	UnionIterator_1_t3402001247 * V_1 = NULL;
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__nextIndex_8();
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_000b;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000b:
	{
		V_0 = (UnionIteratorN_1_t1272659448 *)__this;
		goto IL_0028;
	}

IL_000f:
	{
		UnionIteratorN_1_t1272659448 * L_2 = V_0;
		NullCheck(L_2);
		UnionIterator_1_t3402001247 * L_3 = (UnionIterator_1_t3402001247 *)L_2->get__previous_6();
		V_1 = (UnionIterator_1_t3402001247 *)L_3;
		UnionIterator_1_t3402001247 * L_4 = V_1;
		V_0 = (UnionIteratorN_1_t1272659448 *)((UnionIteratorN_1_t1272659448 *)IsInst((RuntimeObject*)L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		UnionIteratorN_1_t1272659448 * L_5 = V_0;
		if (L_5)
		{
			goto IL_0028;
		}
	}
	{
		UnionIterator_1_t3402001247 * L_6 = V_1;
		int32_t L_7 = ___index0;
		NullCheck((UnionIterator_1_t3402001247 *)L_6);
		RuntimeObject* L_8 = VirtFuncInvoker1< RuntimeObject*, int32_t >::Invoke(19 /* System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator`1<System.Char>::GetEnumerable(System.Int32) */, (UnionIterator_1_t3402001247 *)L_6, (int32_t)L_7);
		return L_8;
	}

IL_0028:
	{
		int32_t L_9 = ___index0;
		UnionIteratorN_1_t1272659448 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = (int32_t)L_10->get__nextIndex_8();
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_000f;
		}
	}
	{
		UnionIteratorN_1_t1272659448 * L_12 = V_0;
		NullCheck(L_12);
		RuntimeObject* L_13 = (RuntimeObject*)L_12->get__next_7();
		return L_13;
	}
}
// System.Linq.Enumerable/UnionIterator`1<TSource> System.Linq.Enumerable/UnionIteratorN`1<System.Char>::Union(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  UnionIterator_1_t3402001247 * UnionIteratorN_1_Union_m2780482306_gshared (UnionIteratorN_1_t1272659448 * __this, RuntimeObject* ___next0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__nextIndex_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)2147483645)))))
		{
			goto IL_001b;
		}
	}
	{
		RuntimeObject* L_1 = ___next0;
		RuntimeObject* L_2 = (RuntimeObject*)((UnionIterator_1_t3402001247 *)__this)->get__comparer_3();
		UnionIterator2_1_t1321734732 * L_3 = (UnionIterator2_1_t1321734732 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6));
		((  void (*) (UnionIterator2_1_t1321734732 *, RuntimeObject*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(L_3, (RuntimeObject*)__this, (RuntimeObject*)L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return L_3;
	}

IL_001b:
	{
		RuntimeObject* L_4 = ___next0;
		int32_t L_5 = (int32_t)__this->get__nextIndex_8();
		UnionIteratorN_1_t1272659448 * L_6 = (UnionIteratorN_1_t1272659448 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (UnionIteratorN_1_t1272659448 *, UnionIterator_1_t3402001247 *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_6, (UnionIterator_1_t3402001247 *)__this, (RuntimeObject*)L_4, (int32_t)((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_6;
	}
}
// System.Void System.Linq.Enumerable/UnionIteratorN`1<System.Object>::.ctor(System.Linq.Enumerable/UnionIterator`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
extern "C"  void UnionIteratorN_1__ctor_m710504400_gshared (UnionIteratorN_1_t718305142 * __this, UnionIterator_1_t2847646941 * ___previous0, RuntimeObject* ___next1, int32_t ___nextIndex2, const RuntimeMethod* method)
{
	{
		UnionIterator_1_t2847646941 * L_0 = ___previous0;
		NullCheck(L_0);
		RuntimeObject* L_1 = (RuntimeObject*)L_0->get__comparer_3();
		NullCheck((UnionIterator_1_t2847646941 *)__this);
		((  void (*) (UnionIterator_1_t2847646941 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((UnionIterator_1_t2847646941 *)__this, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		UnionIterator_1_t2847646941 * L_2 = ___previous0;
		__this->set__previous_6(L_2);
		RuntimeObject* L_3 = ___next1;
		__this->set__next_7(L_3);
		int32_t L_4 = ___nextIndex2;
		__this->set__nextIndex_8(L_4);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/UnionIteratorN`1<System.Object>::Clone()
extern "C"  Iterator_1_t2034466501 * UnionIteratorN_1_Clone_m1648276427_gshared (UnionIteratorN_1_t718305142 * __this, const RuntimeMethod* method)
{
	{
		UnionIterator_1_t2847646941 * L_0 = (UnionIterator_1_t2847646941 *)__this->get__previous_6();
		RuntimeObject* L_1 = (RuntimeObject*)__this->get__next_7();
		int32_t L_2 = (int32_t)__this->get__nextIndex_8();
		UnionIteratorN_1_t718305142 * L_3 = (UnionIteratorN_1_t718305142 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (UnionIteratorN_1_t718305142 *, UnionIterator_1_t2847646941 *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_3, (UnionIterator_1_t2847646941 *)L_0, (RuntimeObject*)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIteratorN`1<System.Object>::GetEnumerable(System.Int32)
extern "C"  RuntimeObject* UnionIteratorN_1_GetEnumerable_m3891809794_gshared (UnionIteratorN_1_t718305142 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	UnionIteratorN_1_t718305142 * V_0 = NULL;
	UnionIterator_1_t2847646941 * V_1 = NULL;
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__nextIndex_8();
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_000b;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000b:
	{
		V_0 = (UnionIteratorN_1_t718305142 *)__this;
		goto IL_0028;
	}

IL_000f:
	{
		UnionIteratorN_1_t718305142 * L_2 = V_0;
		NullCheck(L_2);
		UnionIterator_1_t2847646941 * L_3 = (UnionIterator_1_t2847646941 *)L_2->get__previous_6();
		V_1 = (UnionIterator_1_t2847646941 *)L_3;
		UnionIterator_1_t2847646941 * L_4 = V_1;
		V_0 = (UnionIteratorN_1_t718305142 *)((UnionIteratorN_1_t718305142 *)IsInst((RuntimeObject*)L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		UnionIteratorN_1_t718305142 * L_5 = V_0;
		if (L_5)
		{
			goto IL_0028;
		}
	}
	{
		UnionIterator_1_t2847646941 * L_6 = V_1;
		int32_t L_7 = ___index0;
		NullCheck((UnionIterator_1_t2847646941 *)L_6);
		RuntimeObject* L_8 = VirtFuncInvoker1< RuntimeObject*, int32_t >::Invoke(19 /* System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/UnionIterator`1<System.Object>::GetEnumerable(System.Int32) */, (UnionIterator_1_t2847646941 *)L_6, (int32_t)L_7);
		return L_8;
	}

IL_0028:
	{
		int32_t L_9 = ___index0;
		UnionIteratorN_1_t718305142 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = (int32_t)L_10->get__nextIndex_8();
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_000f;
		}
	}
	{
		UnionIteratorN_1_t718305142 * L_12 = V_0;
		NullCheck(L_12);
		RuntimeObject* L_13 = (RuntimeObject*)L_12->get__next_7();
		return L_13;
	}
}
// System.Linq.Enumerable/UnionIterator`1<TSource> System.Linq.Enumerable/UnionIteratorN`1<System.Object>::Union(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  UnionIterator_1_t2847646941 * UnionIteratorN_1_Union_m4070762018_gshared (UnionIteratorN_1_t718305142 * __this, RuntimeObject* ___next0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__nextIndex_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)2147483645)))))
		{
			goto IL_001b;
		}
	}
	{
		RuntimeObject* L_1 = ___next0;
		RuntimeObject* L_2 = (RuntimeObject*)((UnionIterator_1_t2847646941 *)__this)->get__comparer_3();
		UnionIterator2_1_t767380426 * L_3 = (UnionIterator2_1_t767380426 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6));
		((  void (*) (UnionIterator2_1_t767380426 *, RuntimeObject*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(L_3, (RuntimeObject*)__this, (RuntimeObject*)L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return L_3;
	}

IL_001b:
	{
		RuntimeObject* L_4 = ___next0;
		int32_t L_5 = (int32_t)__this->get__nextIndex_8();
		UnionIteratorN_1_t718305142 * L_6 = (UnionIteratorN_1_t718305142 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (UnionIteratorN_1_t718305142 *, UnionIterator_1_t2847646941 *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_6, (UnionIterator_1_t2847646941 *)__this, (RuntimeObject*)L_4, (int32_t)((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
