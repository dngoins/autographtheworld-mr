﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Dynamic.Utils.CacheDict`2<System.Type,System.Reflection.MethodInfo>
struct CacheDict_2_t213319420;
// System.Dynamic.Utils.CacheDict`2<System.Type,System.Func`5<System.Linq.Expressions.Expression,System.String,System.Boolean,System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ParameterExpression>,System.Linq.Expressions.LambdaExpression>>
struct CacheDict_2_t430650805;
// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Linq.Expressions.Expression,System.Linq.Expressions.Expression/ExtensionInfo>
struct ConditionalWeakTable_2_t2404503487;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t5769829;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Linq.Expressions.LabelTarget
struct LabelTarget_t3951553093;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Runtime.CompilerServices.CallSiteBinder
struct CallSiteBinder_t1870160633;
// System.Linq.Expressions.Expression
struct Expression_t1588164026;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;
// System.Linq.Expressions.LambdaExpression
struct LambdaExpression_t3131094331;
// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t4254223087;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t722666473;
// System.Type
struct Type_t;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Security.Cryptography.RijndaelManaged
struct RijndaelManaged_t3586970409;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745530_H
#define U3CMODULEU3E_T692745530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745530 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745530_H
#ifndef BLOCKEXPRESSIONPROXY_T135452077_H
#define BLOCKEXPRESSIONPROXY_T135452077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/BlockExpressionProxy
struct  BlockExpressionProxy_t135452077  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKEXPRESSIONPROXY_T135452077_H
#ifndef CATCHBLOCKPROXY_T2867642995_H
#define CATCHBLOCKPROXY_T2867642995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/CatchBlockProxy
struct  CatchBlockProxy_t2867642995  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATCHBLOCKPROXY_T2867642995_H
#ifndef CONDITIONALEXPRESSIONPROXY_T3440623873_H
#define CONDITIONALEXPRESSIONPROXY_T3440623873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/ConditionalExpressionProxy
struct  ConditionalExpressionProxy_t3440623873  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONALEXPRESSIONPROXY_T3440623873_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef EXPRESSION_T1588164026_H
#define EXPRESSION_T1588164026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression
struct  Expression_t1588164026  : public RuntimeObject
{
public:

public:
};

struct Expression_t1588164026_StaticFields
{
public:
	// System.Dynamic.Utils.CacheDict`2<System.Type,System.Reflection.MethodInfo> System.Linq.Expressions.Expression::s_lambdaDelegateCache
	CacheDict_2_t213319420 * ___s_lambdaDelegateCache_0;
	// System.Dynamic.Utils.CacheDict`2<System.Type,System.Func`5<System.Linq.Expressions.Expression,System.String,System.Boolean,System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ParameterExpression>,System.Linq.Expressions.LambdaExpression>> modreq(System.Runtime.CompilerServices.IsVolatile) System.Linq.Expressions.Expression::s_lambdaFactories
	CacheDict_2_t430650805 * ___s_lambdaFactories_1;
	// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Linq.Expressions.Expression,System.Linq.Expressions.Expression/ExtensionInfo> System.Linq.Expressions.Expression::s_legacyCtorSupportTable
	ConditionalWeakTable_2_t2404503487 * ___s_legacyCtorSupportTable_2;

public:
	inline static int32_t get_offset_of_s_lambdaDelegateCache_0() { return static_cast<int32_t>(offsetof(Expression_t1588164026_StaticFields, ___s_lambdaDelegateCache_0)); }
	inline CacheDict_2_t213319420 * get_s_lambdaDelegateCache_0() const { return ___s_lambdaDelegateCache_0; }
	inline CacheDict_2_t213319420 ** get_address_of_s_lambdaDelegateCache_0() { return &___s_lambdaDelegateCache_0; }
	inline void set_s_lambdaDelegateCache_0(CacheDict_2_t213319420 * value)
	{
		___s_lambdaDelegateCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_lambdaDelegateCache_0), value);
	}

	inline static int32_t get_offset_of_s_lambdaFactories_1() { return static_cast<int32_t>(offsetof(Expression_t1588164026_StaticFields, ___s_lambdaFactories_1)); }
	inline CacheDict_2_t430650805 * get_s_lambdaFactories_1() const { return ___s_lambdaFactories_1; }
	inline CacheDict_2_t430650805 ** get_address_of_s_lambdaFactories_1() { return &___s_lambdaFactories_1; }
	inline void set_s_lambdaFactories_1(CacheDict_2_t430650805 * value)
	{
		___s_lambdaFactories_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_lambdaFactories_1), value);
	}

	inline static int32_t get_offset_of_s_legacyCtorSupportTable_2() { return static_cast<int32_t>(offsetof(Expression_t1588164026_StaticFields, ___s_legacyCtorSupportTable_2)); }
	inline ConditionalWeakTable_2_t2404503487 * get_s_legacyCtorSupportTable_2() const { return ___s_legacyCtorSupportTable_2; }
	inline ConditionalWeakTable_2_t2404503487 ** get_address_of_s_legacyCtorSupportTable_2() { return &___s_legacyCtorSupportTable_2; }
	inline void set_s_legacyCtorSupportTable_2(ConditionalWeakTable_2_t2404503487 * value)
	{
		___s_legacyCtorSupportTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_legacyCtorSupportTable_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSION_T1588164026_H
#ifndef BINARYEXPRESSIONPROXY_T2974803306_H
#define BINARYEXPRESSIONPROXY_T2974803306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/BinaryExpressionProxy
struct  BinaryExpressionProxy_t2974803306  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYEXPRESSIONPROXY_T2974803306_H
#ifndef GOTOEXPRESSIONPROXY_T2147349472_H
#define GOTOEXPRESSIONPROXY_T2147349472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/GotoExpressionProxy
struct  GotoExpressionProxy_t2147349472  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOTOEXPRESSIONPROXY_T2147349472_H
#ifndef INDEXEXPRESSIONPROXY_T3043937733_H
#define INDEXEXPRESSIONPROXY_T3043937733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/IndexExpressionProxy
struct  IndexExpressionProxy_t3043937733  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXEXPRESSIONPROXY_T3043937733_H
#ifndef INVOCATIONEXPRESSIONPROXY_T2926000647_H
#define INVOCATIONEXPRESSIONPROXY_T2926000647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/InvocationExpressionProxy
struct  InvocationExpressionProxy_t2926000647  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOCATIONEXPRESSIONPROXY_T2926000647_H
#ifndef CONSTANTEXPRESSIONPROXY_T2781329678_H
#define CONSTANTEXPRESSIONPROXY_T2781329678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/ConstantExpressionProxy
struct  ConstantExpressionProxy_t2781329678  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTEXPRESSIONPROXY_T2781329678_H
#ifndef DEBUGINFOEXPRESSIONPROXY_T3441722008_H
#define DEBUGINFOEXPRESSIONPROXY_T3441722008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/DebugInfoExpressionProxy
struct  DebugInfoExpressionProxy_t3441722008  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGINFOEXPRESSIONPROXY_T3441722008_H
#ifndef DEFAULTEXPRESSIONPROXY_T3404437725_H
#define DEFAULTEXPRESSIONPROXY_T3404437725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Expression/DefaultExpressionProxy
struct  DefaultExpressionProxy_t3404437725  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTEXPRESSIONPROXY_T3404437725_H
#ifndef CACHEDREFLECTIONINFO_T3891313302_H
#define CACHEDREFLECTIONINFO_T3891313302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.CachedReflectionInfo
struct  CachedReflectionInfo_t3891313302  : public RuntimeObject
{
public:

public:
};

struct CachedReflectionInfo_t3891313302_StaticFields
{
public:
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_CreateMatchmaker
	MethodInfo_t * ___s_CallSiteOps_CreateMatchmaker_0;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_GetMatch
	MethodInfo_t * ___s_CallSiteOps_GetMatch_1;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_ClearMatch
	MethodInfo_t * ___s_CallSiteOps_ClearMatch_2;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_UpdateRules
	MethodInfo_t * ___s_CallSiteOps_UpdateRules_3;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_GetRules
	MethodInfo_t * ___s_CallSiteOps_GetRules_4;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_GetRuleCache
	MethodInfo_t * ___s_CallSiteOps_GetRuleCache_5;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_GetCachedRules
	MethodInfo_t * ___s_CallSiteOps_GetCachedRules_6;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_AddRule
	MethodInfo_t * ___s_CallSiteOps_AddRule_7;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_MoveRule
	MethodInfo_t * ___s_CallSiteOps_MoveRule_8;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_CallSiteOps_Bind
	MethodInfo_t * ___s_CallSiteOps_Bind_9;
	// System.Type[] System.Linq.Expressions.CachedReflectionInfo::s_ArrayOfType_Bool
	TypeU5BU5D_t3940880105* ___s_ArrayOfType_Bool_10;
	// System.Reflection.ConstructorInfo System.Linq.Expressions.CachedReflectionInfo::s_Decimal_Ctor_Int32
	ConstructorInfo_t5769829 * ___s_Decimal_Ctor_Int32_11;
	// System.Reflection.ConstructorInfo System.Linq.Expressions.CachedReflectionInfo::s_Decimal_Ctor_Int64
	ConstructorInfo_t5769829 * ___s_Decimal_Ctor_Int64_12;
	// System.Reflection.ConstructorInfo System.Linq.Expressions.CachedReflectionInfo::s_Decimal_Ctor_Int32_Int32_Int32_Bool_Byte
	ConstructorInfo_t5769829 * ___s_Decimal_Ctor_Int32_Int32_Int32_Bool_Byte_13;
	// System.Reflection.ConstructorInfo System.Linq.Expressions.CachedReflectionInfo::s_Closure_ObjectArray_ObjectArray
	ConstructorInfo_t5769829 * ___s_Closure_ObjectArray_ObjectArray_14;
	// System.Reflection.FieldInfo System.Linq.Expressions.CachedReflectionInfo::s_Closure_Constants
	FieldInfo_t * ___s_Closure_Constants_15;
	// System.Reflection.FieldInfo System.Linq.Expressions.CachedReflectionInfo::s_Closure_Locals
	FieldInfo_t * ___s_Closure_Locals_16;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle
	MethodInfo_t * ___s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_17;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle
	MethodInfo_t * ___s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle_18;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_MethodInfo_CreateDelegate_Type_Object
	MethodInfo_t * ___s_MethodInfo_CreateDelegate_Type_Object_19;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_String_op_Equality_String_String
	MethodInfo_t * ___s_String_op_Equality_String_String_20;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_RuntimeOps_CreateRuntimeVariables_ObjectArray_Int64Array
	MethodInfo_t * ___s_RuntimeOps_CreateRuntimeVariables_ObjectArray_Int64Array_21;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_RuntimeOps_CreateRuntimeVariables
	MethodInfo_t * ___s_RuntimeOps_CreateRuntimeVariables_22;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_RuntimeOps_Quote
	MethodInfo_t * ___s_RuntimeOps_Quote_23;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_DictionaryOfStringInt32_Add_String_Int32
	MethodInfo_t * ___s_DictionaryOfStringInt32_Add_String_Int32_24;
	// System.Reflection.ConstructorInfo System.Linq.Expressions.CachedReflectionInfo::s_DictionaryOfStringInt32_Ctor_Int32
	ConstructorInfo_t5769829 * ___s_DictionaryOfStringInt32_Ctor_Int32_25;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_Type_GetTypeFromHandle
	MethodInfo_t * ___s_Type_GetTypeFromHandle_26;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_Object_GetType
	MethodInfo_t * ___s_Object_GetType_27;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_Decimal_op_Implicit_Byte
	MethodInfo_t * ___s_Decimal_op_Implicit_Byte_28;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_Decimal_op_Implicit_SByte
	MethodInfo_t * ___s_Decimal_op_Implicit_SByte_29;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_Decimal_op_Implicit_Int16
	MethodInfo_t * ___s_Decimal_op_Implicit_Int16_30;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_Decimal_op_Implicit_UInt16
	MethodInfo_t * ___s_Decimal_op_Implicit_UInt16_31;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_Decimal_op_Implicit_Int32
	MethodInfo_t * ___s_Decimal_op_Implicit_Int32_32;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_Decimal_op_Implicit_UInt32
	MethodInfo_t * ___s_Decimal_op_Implicit_UInt32_33;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_Decimal_op_Implicit_Int64
	MethodInfo_t * ___s_Decimal_op_Implicit_Int64_34;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_Decimal_op_Implicit_UInt64
	MethodInfo_t * ___s_Decimal_op_Implicit_UInt64_35;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_Decimal_op_Implicit_Char
	MethodInfo_t * ___s_Decimal_op_Implicit_Char_36;
	// System.Reflection.MethodInfo System.Linq.Expressions.CachedReflectionInfo::s_Math_Pow_Double_Double
	MethodInfo_t * ___s_Math_Pow_Double_Double_37;

public:
	inline static int32_t get_offset_of_s_CallSiteOps_CreateMatchmaker_0() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_CallSiteOps_CreateMatchmaker_0)); }
	inline MethodInfo_t * get_s_CallSiteOps_CreateMatchmaker_0() const { return ___s_CallSiteOps_CreateMatchmaker_0; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_CreateMatchmaker_0() { return &___s_CallSiteOps_CreateMatchmaker_0; }
	inline void set_s_CallSiteOps_CreateMatchmaker_0(MethodInfo_t * value)
	{
		___s_CallSiteOps_CreateMatchmaker_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_CreateMatchmaker_0), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_GetMatch_1() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_CallSiteOps_GetMatch_1)); }
	inline MethodInfo_t * get_s_CallSiteOps_GetMatch_1() const { return ___s_CallSiteOps_GetMatch_1; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_GetMatch_1() { return &___s_CallSiteOps_GetMatch_1; }
	inline void set_s_CallSiteOps_GetMatch_1(MethodInfo_t * value)
	{
		___s_CallSiteOps_GetMatch_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_GetMatch_1), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_ClearMatch_2() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_CallSiteOps_ClearMatch_2)); }
	inline MethodInfo_t * get_s_CallSiteOps_ClearMatch_2() const { return ___s_CallSiteOps_ClearMatch_2; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_ClearMatch_2() { return &___s_CallSiteOps_ClearMatch_2; }
	inline void set_s_CallSiteOps_ClearMatch_2(MethodInfo_t * value)
	{
		___s_CallSiteOps_ClearMatch_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_ClearMatch_2), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_UpdateRules_3() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_CallSiteOps_UpdateRules_3)); }
	inline MethodInfo_t * get_s_CallSiteOps_UpdateRules_3() const { return ___s_CallSiteOps_UpdateRules_3; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_UpdateRules_3() { return &___s_CallSiteOps_UpdateRules_3; }
	inline void set_s_CallSiteOps_UpdateRules_3(MethodInfo_t * value)
	{
		___s_CallSiteOps_UpdateRules_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_UpdateRules_3), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_GetRules_4() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_CallSiteOps_GetRules_4)); }
	inline MethodInfo_t * get_s_CallSiteOps_GetRules_4() const { return ___s_CallSiteOps_GetRules_4; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_GetRules_4() { return &___s_CallSiteOps_GetRules_4; }
	inline void set_s_CallSiteOps_GetRules_4(MethodInfo_t * value)
	{
		___s_CallSiteOps_GetRules_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_GetRules_4), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_GetRuleCache_5() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_CallSiteOps_GetRuleCache_5)); }
	inline MethodInfo_t * get_s_CallSiteOps_GetRuleCache_5() const { return ___s_CallSiteOps_GetRuleCache_5; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_GetRuleCache_5() { return &___s_CallSiteOps_GetRuleCache_5; }
	inline void set_s_CallSiteOps_GetRuleCache_5(MethodInfo_t * value)
	{
		___s_CallSiteOps_GetRuleCache_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_GetRuleCache_5), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_GetCachedRules_6() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_CallSiteOps_GetCachedRules_6)); }
	inline MethodInfo_t * get_s_CallSiteOps_GetCachedRules_6() const { return ___s_CallSiteOps_GetCachedRules_6; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_GetCachedRules_6() { return &___s_CallSiteOps_GetCachedRules_6; }
	inline void set_s_CallSiteOps_GetCachedRules_6(MethodInfo_t * value)
	{
		___s_CallSiteOps_GetCachedRules_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_GetCachedRules_6), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_AddRule_7() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_CallSiteOps_AddRule_7)); }
	inline MethodInfo_t * get_s_CallSiteOps_AddRule_7() const { return ___s_CallSiteOps_AddRule_7; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_AddRule_7() { return &___s_CallSiteOps_AddRule_7; }
	inline void set_s_CallSiteOps_AddRule_7(MethodInfo_t * value)
	{
		___s_CallSiteOps_AddRule_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_AddRule_7), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_MoveRule_8() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_CallSiteOps_MoveRule_8)); }
	inline MethodInfo_t * get_s_CallSiteOps_MoveRule_8() const { return ___s_CallSiteOps_MoveRule_8; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_MoveRule_8() { return &___s_CallSiteOps_MoveRule_8; }
	inline void set_s_CallSiteOps_MoveRule_8(MethodInfo_t * value)
	{
		___s_CallSiteOps_MoveRule_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_MoveRule_8), value);
	}

	inline static int32_t get_offset_of_s_CallSiteOps_Bind_9() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_CallSiteOps_Bind_9)); }
	inline MethodInfo_t * get_s_CallSiteOps_Bind_9() const { return ___s_CallSiteOps_Bind_9; }
	inline MethodInfo_t ** get_address_of_s_CallSiteOps_Bind_9() { return &___s_CallSiteOps_Bind_9; }
	inline void set_s_CallSiteOps_Bind_9(MethodInfo_t * value)
	{
		___s_CallSiteOps_Bind_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallSiteOps_Bind_9), value);
	}

	inline static int32_t get_offset_of_s_ArrayOfType_Bool_10() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_ArrayOfType_Bool_10)); }
	inline TypeU5BU5D_t3940880105* get_s_ArrayOfType_Bool_10() const { return ___s_ArrayOfType_Bool_10; }
	inline TypeU5BU5D_t3940880105** get_address_of_s_ArrayOfType_Bool_10() { return &___s_ArrayOfType_Bool_10; }
	inline void set_s_ArrayOfType_Bool_10(TypeU5BU5D_t3940880105* value)
	{
		___s_ArrayOfType_Bool_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_ArrayOfType_Bool_10), value);
	}

	inline static int32_t get_offset_of_s_Decimal_Ctor_Int32_11() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Decimal_Ctor_Int32_11)); }
	inline ConstructorInfo_t5769829 * get_s_Decimal_Ctor_Int32_11() const { return ___s_Decimal_Ctor_Int32_11; }
	inline ConstructorInfo_t5769829 ** get_address_of_s_Decimal_Ctor_Int32_11() { return &___s_Decimal_Ctor_Int32_11; }
	inline void set_s_Decimal_Ctor_Int32_11(ConstructorInfo_t5769829 * value)
	{
		___s_Decimal_Ctor_Int32_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_Decimal_Ctor_Int32_11), value);
	}

	inline static int32_t get_offset_of_s_Decimal_Ctor_Int64_12() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Decimal_Ctor_Int64_12)); }
	inline ConstructorInfo_t5769829 * get_s_Decimal_Ctor_Int64_12() const { return ___s_Decimal_Ctor_Int64_12; }
	inline ConstructorInfo_t5769829 ** get_address_of_s_Decimal_Ctor_Int64_12() { return &___s_Decimal_Ctor_Int64_12; }
	inline void set_s_Decimal_Ctor_Int64_12(ConstructorInfo_t5769829 * value)
	{
		___s_Decimal_Ctor_Int64_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_Decimal_Ctor_Int64_12), value);
	}

	inline static int32_t get_offset_of_s_Decimal_Ctor_Int32_Int32_Int32_Bool_Byte_13() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Decimal_Ctor_Int32_Int32_Int32_Bool_Byte_13)); }
	inline ConstructorInfo_t5769829 * get_s_Decimal_Ctor_Int32_Int32_Int32_Bool_Byte_13() const { return ___s_Decimal_Ctor_Int32_Int32_Int32_Bool_Byte_13; }
	inline ConstructorInfo_t5769829 ** get_address_of_s_Decimal_Ctor_Int32_Int32_Int32_Bool_Byte_13() { return &___s_Decimal_Ctor_Int32_Int32_Int32_Bool_Byte_13; }
	inline void set_s_Decimal_Ctor_Int32_Int32_Int32_Bool_Byte_13(ConstructorInfo_t5769829 * value)
	{
		___s_Decimal_Ctor_Int32_Int32_Int32_Bool_Byte_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_Decimal_Ctor_Int32_Int32_Int32_Bool_Byte_13), value);
	}

	inline static int32_t get_offset_of_s_Closure_ObjectArray_ObjectArray_14() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Closure_ObjectArray_ObjectArray_14)); }
	inline ConstructorInfo_t5769829 * get_s_Closure_ObjectArray_ObjectArray_14() const { return ___s_Closure_ObjectArray_ObjectArray_14; }
	inline ConstructorInfo_t5769829 ** get_address_of_s_Closure_ObjectArray_ObjectArray_14() { return &___s_Closure_ObjectArray_ObjectArray_14; }
	inline void set_s_Closure_ObjectArray_ObjectArray_14(ConstructorInfo_t5769829 * value)
	{
		___s_Closure_ObjectArray_ObjectArray_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_Closure_ObjectArray_ObjectArray_14), value);
	}

	inline static int32_t get_offset_of_s_Closure_Constants_15() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Closure_Constants_15)); }
	inline FieldInfo_t * get_s_Closure_Constants_15() const { return ___s_Closure_Constants_15; }
	inline FieldInfo_t ** get_address_of_s_Closure_Constants_15() { return &___s_Closure_Constants_15; }
	inline void set_s_Closure_Constants_15(FieldInfo_t * value)
	{
		___s_Closure_Constants_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Closure_Constants_15), value);
	}

	inline static int32_t get_offset_of_s_Closure_Locals_16() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Closure_Locals_16)); }
	inline FieldInfo_t * get_s_Closure_Locals_16() const { return ___s_Closure_Locals_16; }
	inline FieldInfo_t ** get_address_of_s_Closure_Locals_16() { return &___s_Closure_Locals_16; }
	inline void set_s_Closure_Locals_16(FieldInfo_t * value)
	{
		___s_Closure_Locals_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_Closure_Locals_16), value);
	}

	inline static int32_t get_offset_of_s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_17() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_17)); }
	inline MethodInfo_t * get_s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_17() const { return ___s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_17; }
	inline MethodInfo_t ** get_address_of_s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_17() { return &___s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_17; }
	inline void set_s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_17(MethodInfo_t * value)
	{
		___s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_17), value);
	}

	inline static int32_t get_offset_of_s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle_18() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle_18)); }
	inline MethodInfo_t * get_s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle_18() const { return ___s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle_18; }
	inline MethodInfo_t ** get_address_of_s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle_18() { return &___s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle_18; }
	inline void set_s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle_18(MethodInfo_t * value)
	{
		___s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle_18), value);
	}

	inline static int32_t get_offset_of_s_MethodInfo_CreateDelegate_Type_Object_19() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_MethodInfo_CreateDelegate_Type_Object_19)); }
	inline MethodInfo_t * get_s_MethodInfo_CreateDelegate_Type_Object_19() const { return ___s_MethodInfo_CreateDelegate_Type_Object_19; }
	inline MethodInfo_t ** get_address_of_s_MethodInfo_CreateDelegate_Type_Object_19() { return &___s_MethodInfo_CreateDelegate_Type_Object_19; }
	inline void set_s_MethodInfo_CreateDelegate_Type_Object_19(MethodInfo_t * value)
	{
		___s_MethodInfo_CreateDelegate_Type_Object_19 = value;
		Il2CppCodeGenWriteBarrier((&___s_MethodInfo_CreateDelegate_Type_Object_19), value);
	}

	inline static int32_t get_offset_of_s_String_op_Equality_String_String_20() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_String_op_Equality_String_String_20)); }
	inline MethodInfo_t * get_s_String_op_Equality_String_String_20() const { return ___s_String_op_Equality_String_String_20; }
	inline MethodInfo_t ** get_address_of_s_String_op_Equality_String_String_20() { return &___s_String_op_Equality_String_String_20; }
	inline void set_s_String_op_Equality_String_String_20(MethodInfo_t * value)
	{
		___s_String_op_Equality_String_String_20 = value;
		Il2CppCodeGenWriteBarrier((&___s_String_op_Equality_String_String_20), value);
	}

	inline static int32_t get_offset_of_s_RuntimeOps_CreateRuntimeVariables_ObjectArray_Int64Array_21() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_RuntimeOps_CreateRuntimeVariables_ObjectArray_Int64Array_21)); }
	inline MethodInfo_t * get_s_RuntimeOps_CreateRuntimeVariables_ObjectArray_Int64Array_21() const { return ___s_RuntimeOps_CreateRuntimeVariables_ObjectArray_Int64Array_21; }
	inline MethodInfo_t ** get_address_of_s_RuntimeOps_CreateRuntimeVariables_ObjectArray_Int64Array_21() { return &___s_RuntimeOps_CreateRuntimeVariables_ObjectArray_Int64Array_21; }
	inline void set_s_RuntimeOps_CreateRuntimeVariables_ObjectArray_Int64Array_21(MethodInfo_t * value)
	{
		___s_RuntimeOps_CreateRuntimeVariables_ObjectArray_Int64Array_21 = value;
		Il2CppCodeGenWriteBarrier((&___s_RuntimeOps_CreateRuntimeVariables_ObjectArray_Int64Array_21), value);
	}

	inline static int32_t get_offset_of_s_RuntimeOps_CreateRuntimeVariables_22() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_RuntimeOps_CreateRuntimeVariables_22)); }
	inline MethodInfo_t * get_s_RuntimeOps_CreateRuntimeVariables_22() const { return ___s_RuntimeOps_CreateRuntimeVariables_22; }
	inline MethodInfo_t ** get_address_of_s_RuntimeOps_CreateRuntimeVariables_22() { return &___s_RuntimeOps_CreateRuntimeVariables_22; }
	inline void set_s_RuntimeOps_CreateRuntimeVariables_22(MethodInfo_t * value)
	{
		___s_RuntimeOps_CreateRuntimeVariables_22 = value;
		Il2CppCodeGenWriteBarrier((&___s_RuntimeOps_CreateRuntimeVariables_22), value);
	}

	inline static int32_t get_offset_of_s_RuntimeOps_Quote_23() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_RuntimeOps_Quote_23)); }
	inline MethodInfo_t * get_s_RuntimeOps_Quote_23() const { return ___s_RuntimeOps_Quote_23; }
	inline MethodInfo_t ** get_address_of_s_RuntimeOps_Quote_23() { return &___s_RuntimeOps_Quote_23; }
	inline void set_s_RuntimeOps_Quote_23(MethodInfo_t * value)
	{
		___s_RuntimeOps_Quote_23 = value;
		Il2CppCodeGenWriteBarrier((&___s_RuntimeOps_Quote_23), value);
	}

	inline static int32_t get_offset_of_s_DictionaryOfStringInt32_Add_String_Int32_24() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_DictionaryOfStringInt32_Add_String_Int32_24)); }
	inline MethodInfo_t * get_s_DictionaryOfStringInt32_Add_String_Int32_24() const { return ___s_DictionaryOfStringInt32_Add_String_Int32_24; }
	inline MethodInfo_t ** get_address_of_s_DictionaryOfStringInt32_Add_String_Int32_24() { return &___s_DictionaryOfStringInt32_Add_String_Int32_24; }
	inline void set_s_DictionaryOfStringInt32_Add_String_Int32_24(MethodInfo_t * value)
	{
		___s_DictionaryOfStringInt32_Add_String_Int32_24 = value;
		Il2CppCodeGenWriteBarrier((&___s_DictionaryOfStringInt32_Add_String_Int32_24), value);
	}

	inline static int32_t get_offset_of_s_DictionaryOfStringInt32_Ctor_Int32_25() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_DictionaryOfStringInt32_Ctor_Int32_25)); }
	inline ConstructorInfo_t5769829 * get_s_DictionaryOfStringInt32_Ctor_Int32_25() const { return ___s_DictionaryOfStringInt32_Ctor_Int32_25; }
	inline ConstructorInfo_t5769829 ** get_address_of_s_DictionaryOfStringInt32_Ctor_Int32_25() { return &___s_DictionaryOfStringInt32_Ctor_Int32_25; }
	inline void set_s_DictionaryOfStringInt32_Ctor_Int32_25(ConstructorInfo_t5769829 * value)
	{
		___s_DictionaryOfStringInt32_Ctor_Int32_25 = value;
		Il2CppCodeGenWriteBarrier((&___s_DictionaryOfStringInt32_Ctor_Int32_25), value);
	}

	inline static int32_t get_offset_of_s_Type_GetTypeFromHandle_26() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Type_GetTypeFromHandle_26)); }
	inline MethodInfo_t * get_s_Type_GetTypeFromHandle_26() const { return ___s_Type_GetTypeFromHandle_26; }
	inline MethodInfo_t ** get_address_of_s_Type_GetTypeFromHandle_26() { return &___s_Type_GetTypeFromHandle_26; }
	inline void set_s_Type_GetTypeFromHandle_26(MethodInfo_t * value)
	{
		___s_Type_GetTypeFromHandle_26 = value;
		Il2CppCodeGenWriteBarrier((&___s_Type_GetTypeFromHandle_26), value);
	}

	inline static int32_t get_offset_of_s_Object_GetType_27() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Object_GetType_27)); }
	inline MethodInfo_t * get_s_Object_GetType_27() const { return ___s_Object_GetType_27; }
	inline MethodInfo_t ** get_address_of_s_Object_GetType_27() { return &___s_Object_GetType_27; }
	inline void set_s_Object_GetType_27(MethodInfo_t * value)
	{
		___s_Object_GetType_27 = value;
		Il2CppCodeGenWriteBarrier((&___s_Object_GetType_27), value);
	}

	inline static int32_t get_offset_of_s_Decimal_op_Implicit_Byte_28() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Decimal_op_Implicit_Byte_28)); }
	inline MethodInfo_t * get_s_Decimal_op_Implicit_Byte_28() const { return ___s_Decimal_op_Implicit_Byte_28; }
	inline MethodInfo_t ** get_address_of_s_Decimal_op_Implicit_Byte_28() { return &___s_Decimal_op_Implicit_Byte_28; }
	inline void set_s_Decimal_op_Implicit_Byte_28(MethodInfo_t * value)
	{
		___s_Decimal_op_Implicit_Byte_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_Decimal_op_Implicit_Byte_28), value);
	}

	inline static int32_t get_offset_of_s_Decimal_op_Implicit_SByte_29() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Decimal_op_Implicit_SByte_29)); }
	inline MethodInfo_t * get_s_Decimal_op_Implicit_SByte_29() const { return ___s_Decimal_op_Implicit_SByte_29; }
	inline MethodInfo_t ** get_address_of_s_Decimal_op_Implicit_SByte_29() { return &___s_Decimal_op_Implicit_SByte_29; }
	inline void set_s_Decimal_op_Implicit_SByte_29(MethodInfo_t * value)
	{
		___s_Decimal_op_Implicit_SByte_29 = value;
		Il2CppCodeGenWriteBarrier((&___s_Decimal_op_Implicit_SByte_29), value);
	}

	inline static int32_t get_offset_of_s_Decimal_op_Implicit_Int16_30() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Decimal_op_Implicit_Int16_30)); }
	inline MethodInfo_t * get_s_Decimal_op_Implicit_Int16_30() const { return ___s_Decimal_op_Implicit_Int16_30; }
	inline MethodInfo_t ** get_address_of_s_Decimal_op_Implicit_Int16_30() { return &___s_Decimal_op_Implicit_Int16_30; }
	inline void set_s_Decimal_op_Implicit_Int16_30(MethodInfo_t * value)
	{
		___s_Decimal_op_Implicit_Int16_30 = value;
		Il2CppCodeGenWriteBarrier((&___s_Decimal_op_Implicit_Int16_30), value);
	}

	inline static int32_t get_offset_of_s_Decimal_op_Implicit_UInt16_31() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Decimal_op_Implicit_UInt16_31)); }
	inline MethodInfo_t * get_s_Decimal_op_Implicit_UInt16_31() const { return ___s_Decimal_op_Implicit_UInt16_31; }
	inline MethodInfo_t ** get_address_of_s_Decimal_op_Implicit_UInt16_31() { return &___s_Decimal_op_Implicit_UInt16_31; }
	inline void set_s_Decimal_op_Implicit_UInt16_31(MethodInfo_t * value)
	{
		___s_Decimal_op_Implicit_UInt16_31 = value;
		Il2CppCodeGenWriteBarrier((&___s_Decimal_op_Implicit_UInt16_31), value);
	}

	inline static int32_t get_offset_of_s_Decimal_op_Implicit_Int32_32() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Decimal_op_Implicit_Int32_32)); }
	inline MethodInfo_t * get_s_Decimal_op_Implicit_Int32_32() const { return ___s_Decimal_op_Implicit_Int32_32; }
	inline MethodInfo_t ** get_address_of_s_Decimal_op_Implicit_Int32_32() { return &___s_Decimal_op_Implicit_Int32_32; }
	inline void set_s_Decimal_op_Implicit_Int32_32(MethodInfo_t * value)
	{
		___s_Decimal_op_Implicit_Int32_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_Decimal_op_Implicit_Int32_32), value);
	}

	inline static int32_t get_offset_of_s_Decimal_op_Implicit_UInt32_33() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Decimal_op_Implicit_UInt32_33)); }
	inline MethodInfo_t * get_s_Decimal_op_Implicit_UInt32_33() const { return ___s_Decimal_op_Implicit_UInt32_33; }
	inline MethodInfo_t ** get_address_of_s_Decimal_op_Implicit_UInt32_33() { return &___s_Decimal_op_Implicit_UInt32_33; }
	inline void set_s_Decimal_op_Implicit_UInt32_33(MethodInfo_t * value)
	{
		___s_Decimal_op_Implicit_UInt32_33 = value;
		Il2CppCodeGenWriteBarrier((&___s_Decimal_op_Implicit_UInt32_33), value);
	}

	inline static int32_t get_offset_of_s_Decimal_op_Implicit_Int64_34() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Decimal_op_Implicit_Int64_34)); }
	inline MethodInfo_t * get_s_Decimal_op_Implicit_Int64_34() const { return ___s_Decimal_op_Implicit_Int64_34; }
	inline MethodInfo_t ** get_address_of_s_Decimal_op_Implicit_Int64_34() { return &___s_Decimal_op_Implicit_Int64_34; }
	inline void set_s_Decimal_op_Implicit_Int64_34(MethodInfo_t * value)
	{
		___s_Decimal_op_Implicit_Int64_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_Decimal_op_Implicit_Int64_34), value);
	}

	inline static int32_t get_offset_of_s_Decimal_op_Implicit_UInt64_35() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Decimal_op_Implicit_UInt64_35)); }
	inline MethodInfo_t * get_s_Decimal_op_Implicit_UInt64_35() const { return ___s_Decimal_op_Implicit_UInt64_35; }
	inline MethodInfo_t ** get_address_of_s_Decimal_op_Implicit_UInt64_35() { return &___s_Decimal_op_Implicit_UInt64_35; }
	inline void set_s_Decimal_op_Implicit_UInt64_35(MethodInfo_t * value)
	{
		___s_Decimal_op_Implicit_UInt64_35 = value;
		Il2CppCodeGenWriteBarrier((&___s_Decimal_op_Implicit_UInt64_35), value);
	}

	inline static int32_t get_offset_of_s_Decimal_op_Implicit_Char_36() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Decimal_op_Implicit_Char_36)); }
	inline MethodInfo_t * get_s_Decimal_op_Implicit_Char_36() const { return ___s_Decimal_op_Implicit_Char_36; }
	inline MethodInfo_t ** get_address_of_s_Decimal_op_Implicit_Char_36() { return &___s_Decimal_op_Implicit_Char_36; }
	inline void set_s_Decimal_op_Implicit_Char_36(MethodInfo_t * value)
	{
		___s_Decimal_op_Implicit_Char_36 = value;
		Il2CppCodeGenWriteBarrier((&___s_Decimal_op_Implicit_Char_36), value);
	}

	inline static int32_t get_offset_of_s_Math_Pow_Double_Double_37() { return static_cast<int32_t>(offsetof(CachedReflectionInfo_t3891313302_StaticFields, ___s_Math_Pow_Double_Double_37)); }
	inline MethodInfo_t * get_s_Math_Pow_Double_Double_37() const { return ___s_Math_Pow_Double_Double_37; }
	inline MethodInfo_t ** get_address_of_s_Math_Pow_Double_Double_37() { return &___s_Math_Pow_Double_Double_37; }
	inline void set_s_Math_Pow_Double_Double_37(MethodInfo_t * value)
	{
		___s_Math_Pow_Double_Double_37 = value;
		Il2CppCodeGenWriteBarrier((&___s_Math_Pow_Double_Double_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDREFLECTIONINFO_T3891313302_H
#ifndef CALLSITEBINDER_T1870160633_H
#define CALLSITEBINDER_T1870160633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.CallSiteBinder
struct  CallSiteBinder_t1870160633  : public RuntimeObject
{
public:

public:
};

struct CallSiteBinder_t1870160633_StaticFields
{
public:
	// System.Linq.Expressions.LabelTarget System.Runtime.CompilerServices.CallSiteBinder::<UpdateLabel>k__BackingField
	LabelTarget_t3951553093 * ___U3CUpdateLabelU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CUpdateLabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CallSiteBinder_t1870160633_StaticFields, ___U3CUpdateLabelU3Ek__BackingField_0)); }
	inline LabelTarget_t3951553093 * get_U3CUpdateLabelU3Ek__BackingField_0() const { return ___U3CUpdateLabelU3Ek__BackingField_0; }
	inline LabelTarget_t3951553093 ** get_address_of_U3CUpdateLabelU3Ek__BackingField_0() { return &___U3CUpdateLabelU3Ek__BackingField_0; }
	inline void set_U3CUpdateLabelU3Ek__BackingField_0(LabelTarget_t3951553093 * value)
	{
		___U3CUpdateLabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUpdateLabelU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLSITEBINDER_T1870160633_H
#ifndef UTILITIES_T3288484762_H
#define UTILITIES_T3288484762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Utilities
struct  Utilities_t3288484762  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITIES_T3288484762_H
#ifndef ENUMERABLE_T538148348_H
#define ENUMERABLE_T538148348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable
struct  Enumerable_t538148348  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERABLE_T538148348_H
#ifndef CLOSURE_T348671509_H
#define CLOSURE_T348671509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.Closure
struct  Closure_t348671509  : public RuntimeObject
{
public:
	// System.Object[] System.Runtime.CompilerServices.Closure::Constants
	ObjectU5BU5D_t2843939325* ___Constants_0;
	// System.Object[] System.Runtime.CompilerServices.Closure::Locals
	ObjectU5BU5D_t2843939325* ___Locals_1;

public:
	inline static int32_t get_offset_of_Constants_0() { return static_cast<int32_t>(offsetof(Closure_t348671509, ___Constants_0)); }
	inline ObjectU5BU5D_t2843939325* get_Constants_0() const { return ___Constants_0; }
	inline ObjectU5BU5D_t2843939325** get_address_of_Constants_0() { return &___Constants_0; }
	inline void set_Constants_0(ObjectU5BU5D_t2843939325* value)
	{
		___Constants_0 = value;
		Il2CppCodeGenWriteBarrier((&___Constants_0), value);
	}

	inline static int32_t get_offset_of_Locals_1() { return static_cast<int32_t>(offsetof(Closure_t348671509, ___Locals_1)); }
	inline ObjectU5BU5D_t2843939325* get_Locals_1() const { return ___Locals_1; }
	inline ObjectU5BU5D_t2843939325** get_address_of_Locals_1() { return &___Locals_1; }
	inline void set_Locals_1(ObjectU5BU5D_t2843939325* value)
	{
		___Locals_1 = value;
		Il2CppCodeGenWriteBarrier((&___Locals_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSURE_T348671509_H
#ifndef ERROR_T2882114465_H
#define ERROR_T2882114465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Error
struct  Error_t2882114465  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROR_T2882114465_H
#ifndef CALLSITEOPS_T258988244_H
#define CALLSITEOPS_T258988244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.CallSiteOps
struct  CallSiteOps_t258988244  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLSITEOPS_T258988244_H
#ifndef SR_T167583546_H
#define SR_T167583546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SR
struct  SR_t167583546  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SR_T167583546_H
#ifndef CALLSITE_T1653101453_H
#define CALLSITE_T1653101453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.CallSite
struct  CallSite_t1653101453  : public RuntimeObject
{
public:
	// System.Runtime.CompilerServices.CallSiteBinder System.Runtime.CompilerServices.CallSite::_binder
	CallSiteBinder_t1870160633 * ____binder_0;

public:
	inline static int32_t get_offset_of__binder_0() { return static_cast<int32_t>(offsetof(CallSite_t1653101453, ____binder_0)); }
	inline CallSiteBinder_t1870160633 * get__binder_0() const { return ____binder_0; }
	inline CallSiteBinder_t1870160633 ** get_address_of__binder_0() { return &____binder_0; }
	inline void set__binder_0(CallSiteBinder_t1870160633 * value)
	{
		____binder_0 = value;
		Il2CppCodeGenWriteBarrier((&____binder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLSITE_T1653101453_H
#ifndef RUNTIMEOPS_T4267392184_H
#define RUNTIMEOPS_T4267392184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.RuntimeOps
struct  RuntimeOps_t4267392184  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOPS_T4267392184_H
#ifndef NOTIMPLEMENTED_T776753164_H
#define NOTIMPLEMENTED_T776753164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotImplemented
struct  NotImplemented_t776753164  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIMPLEMENTED_T776753164_H
#ifndef BINARYEXPRESSION_T77573129_H
#define BINARYEXPRESSION_T77573129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.BinaryExpression
struct  BinaryExpression_t77573129  : public Expression_t1588164026
{
public:
	// System.Linq.Expressions.Expression System.Linq.Expressions.BinaryExpression::<Right>k__BackingField
	Expression_t1588164026 * ___U3CRightU3Ek__BackingField_3;
	// System.Linq.Expressions.Expression System.Linq.Expressions.BinaryExpression::<Left>k__BackingField
	Expression_t1588164026 * ___U3CLeftU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CRightU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BinaryExpression_t77573129, ___U3CRightU3Ek__BackingField_3)); }
	inline Expression_t1588164026 * get_U3CRightU3Ek__BackingField_3() const { return ___U3CRightU3Ek__BackingField_3; }
	inline Expression_t1588164026 ** get_address_of_U3CRightU3Ek__BackingField_3() { return &___U3CRightU3Ek__BackingField_3; }
	inline void set_U3CRightU3Ek__BackingField_3(Expression_t1588164026 * value)
	{
		___U3CRightU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRightU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CLeftU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BinaryExpression_t77573129, ___U3CLeftU3Ek__BackingField_4)); }
	inline Expression_t1588164026 * get_U3CLeftU3Ek__BackingField_4() const { return ___U3CLeftU3Ek__BackingField_4; }
	inline Expression_t1588164026 ** get_address_of_U3CLeftU3Ek__BackingField_4() { return &___U3CLeftU3Ek__BackingField_4; }
	inline void set_U3CLeftU3Ek__BackingField_4(Expression_t1588164026 * value)
	{
		___U3CLeftU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLeftU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYEXPRESSION_T77573129_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef PADDINGMODE_T2546806710_H
#define PADDINGMODE_T2546806710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.PaddingMode
struct  PaddingMode_t2546806710 
{
public:
	// System.Int32 System.Security.Cryptography.PaddingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PaddingMode_t2546806710, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADDINGMODE_T2546806710_H
#ifndef EXPRESSIONTYPE_T2886294549_H
#define EXPRESSIONTYPE_T2886294549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ExpressionType
struct  ExpressionType_t2886294549 
{
public:
	// System.Int32 System.Linq.Expressions.ExpressionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExpressionType_t2886294549, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONTYPE_T2886294549_H
#ifndef COALESCECONVERSIONBINARYEXPRESSION_T1217329768_H
#define COALESCECONVERSIONBINARYEXPRESSION_T1217329768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.CoalesceConversionBinaryExpression
struct  CoalesceConversionBinaryExpression_t1217329768  : public BinaryExpression_t77573129
{
public:
	// System.Linq.Expressions.LambdaExpression System.Linq.Expressions.CoalesceConversionBinaryExpression::_conversion
	LambdaExpression_t3131094331 * ____conversion_5;

public:
	inline static int32_t get_offset_of__conversion_5() { return static_cast<int32_t>(offsetof(CoalesceConversionBinaryExpression_t1217329768, ____conversion_5)); }
	inline LambdaExpression_t3131094331 * get__conversion_5() const { return ____conversion_5; }
	inline LambdaExpression_t3131094331 ** get_address_of__conversion_5() { return &____conversion_5; }
	inline void set__conversion_5(LambdaExpression_t3131094331 * value)
	{
		____conversion_5 = value;
		Il2CppCodeGenWriteBarrier((&____conversion_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COALESCECONVERSIONBINARYEXPRESSION_T1217329768_H
#ifndef ASSIGNBINARYEXPRESSION_T2591659520_H
#define ASSIGNBINARYEXPRESSION_T2591659520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.AssignBinaryExpression
struct  AssignBinaryExpression_t2591659520  : public BinaryExpression_t77573129
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIGNBINARYEXPRESSION_T2591659520_H
#ifndef CIPHERMODE_T84635067_H
#define CIPHERMODE_T84635067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CipherMode
struct  CipherMode_t84635067 
{
public:
	// System.Int32 System.Security.Cryptography.CipherMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherMode_t84635067, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERMODE_T84635067_H
#ifndef SYMMETRICTRANSFORM_T3802591842_H
#define SYMMETRICTRANSFORM_T3802591842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.SymmetricTransform
struct  SymmetricTransform_t3802591842  : public RuntimeObject
{
public:
	// System.Security.Cryptography.SymmetricAlgorithm Mono.Security.Cryptography.SymmetricTransform::algo
	SymmetricAlgorithm_t4254223087 * ___algo_0;
	// System.Boolean Mono.Security.Cryptography.SymmetricTransform::encrypt
	bool ___encrypt_1;
	// System.Int32 Mono.Security.Cryptography.SymmetricTransform::BlockSizeByte
	int32_t ___BlockSizeByte_2;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::temp
	ByteU5BU5D_t4116647657* ___temp_3;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::temp2
	ByteU5BU5D_t4116647657* ___temp2_4;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::workBuff
	ByteU5BU5D_t4116647657* ___workBuff_5;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::workout
	ByteU5BU5D_t4116647657* ___workout_6;
	// System.Security.Cryptography.PaddingMode Mono.Security.Cryptography.SymmetricTransform::padmode
	int32_t ___padmode_7;
	// System.Int32 Mono.Security.Cryptography.SymmetricTransform::FeedBackByte
	int32_t ___FeedBackByte_8;
	// System.Boolean Mono.Security.Cryptography.SymmetricTransform::m_disposed
	bool ___m_disposed_9;
	// System.Boolean Mono.Security.Cryptography.SymmetricTransform::lastBlock
	bool ___lastBlock_10;
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.SymmetricTransform::_rng
	RandomNumberGenerator_t386037858 * ____rng_11;

public:
	inline static int32_t get_offset_of_algo_0() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___algo_0)); }
	inline SymmetricAlgorithm_t4254223087 * get_algo_0() const { return ___algo_0; }
	inline SymmetricAlgorithm_t4254223087 ** get_address_of_algo_0() { return &___algo_0; }
	inline void set_algo_0(SymmetricAlgorithm_t4254223087 * value)
	{
		___algo_0 = value;
		Il2CppCodeGenWriteBarrier((&___algo_0), value);
	}

	inline static int32_t get_offset_of_encrypt_1() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___encrypt_1)); }
	inline bool get_encrypt_1() const { return ___encrypt_1; }
	inline bool* get_address_of_encrypt_1() { return &___encrypt_1; }
	inline void set_encrypt_1(bool value)
	{
		___encrypt_1 = value;
	}

	inline static int32_t get_offset_of_BlockSizeByte_2() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___BlockSizeByte_2)); }
	inline int32_t get_BlockSizeByte_2() const { return ___BlockSizeByte_2; }
	inline int32_t* get_address_of_BlockSizeByte_2() { return &___BlockSizeByte_2; }
	inline void set_BlockSizeByte_2(int32_t value)
	{
		___BlockSizeByte_2 = value;
	}

	inline static int32_t get_offset_of_temp_3() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___temp_3)); }
	inline ByteU5BU5D_t4116647657* get_temp_3() const { return ___temp_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_temp_3() { return &___temp_3; }
	inline void set_temp_3(ByteU5BU5D_t4116647657* value)
	{
		___temp_3 = value;
		Il2CppCodeGenWriteBarrier((&___temp_3), value);
	}

	inline static int32_t get_offset_of_temp2_4() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___temp2_4)); }
	inline ByteU5BU5D_t4116647657* get_temp2_4() const { return ___temp2_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_temp2_4() { return &___temp2_4; }
	inline void set_temp2_4(ByteU5BU5D_t4116647657* value)
	{
		___temp2_4 = value;
		Il2CppCodeGenWriteBarrier((&___temp2_4), value);
	}

	inline static int32_t get_offset_of_workBuff_5() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___workBuff_5)); }
	inline ByteU5BU5D_t4116647657* get_workBuff_5() const { return ___workBuff_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_workBuff_5() { return &___workBuff_5; }
	inline void set_workBuff_5(ByteU5BU5D_t4116647657* value)
	{
		___workBuff_5 = value;
		Il2CppCodeGenWriteBarrier((&___workBuff_5), value);
	}

	inline static int32_t get_offset_of_workout_6() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___workout_6)); }
	inline ByteU5BU5D_t4116647657* get_workout_6() const { return ___workout_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_workout_6() { return &___workout_6; }
	inline void set_workout_6(ByteU5BU5D_t4116647657* value)
	{
		___workout_6 = value;
		Il2CppCodeGenWriteBarrier((&___workout_6), value);
	}

	inline static int32_t get_offset_of_padmode_7() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___padmode_7)); }
	inline int32_t get_padmode_7() const { return ___padmode_7; }
	inline int32_t* get_address_of_padmode_7() { return &___padmode_7; }
	inline void set_padmode_7(int32_t value)
	{
		___padmode_7 = value;
	}

	inline static int32_t get_offset_of_FeedBackByte_8() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___FeedBackByte_8)); }
	inline int32_t get_FeedBackByte_8() const { return ___FeedBackByte_8; }
	inline int32_t* get_address_of_FeedBackByte_8() { return &___FeedBackByte_8; }
	inline void set_FeedBackByte_8(int32_t value)
	{
		___FeedBackByte_8 = value;
	}

	inline static int32_t get_offset_of_m_disposed_9() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___m_disposed_9)); }
	inline bool get_m_disposed_9() const { return ___m_disposed_9; }
	inline bool* get_address_of_m_disposed_9() { return &___m_disposed_9; }
	inline void set_m_disposed_9(bool value)
	{
		___m_disposed_9 = value;
	}

	inline static int32_t get_offset_of_lastBlock_10() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___lastBlock_10)); }
	inline bool get_lastBlock_10() const { return ___lastBlock_10; }
	inline bool* get_address_of_lastBlock_10() { return &___lastBlock_10; }
	inline void set_lastBlock_10(bool value)
	{
		___lastBlock_10 = value;
	}

	inline static int32_t get_offset_of__rng_11() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ____rng_11)); }
	inline RandomNumberGenerator_t386037858 * get__rng_11() const { return ____rng_11; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_11() { return &____rng_11; }
	inline void set__rng_11(RandomNumberGenerator_t386037858 * value)
	{
		____rng_11 = value;
		Il2CppCodeGenWriteBarrier((&____rng_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMMETRICTRANSFORM_T3802591842_H
#ifndef LOGICALBINARYEXPRESSION_T1440714930_H
#define LOGICALBINARYEXPRESSION_T1440714930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.LogicalBinaryExpression
struct  LogicalBinaryExpression_t1440714930  : public BinaryExpression_t77573129
{
public:
	// System.Linq.Expressions.ExpressionType System.Linq.Expressions.LogicalBinaryExpression::<NodeType>k__BackingField
	int32_t ___U3CNodeTypeU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CNodeTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LogicalBinaryExpression_t1440714930, ___U3CNodeTypeU3Ek__BackingField_5)); }
	inline int32_t get_U3CNodeTypeU3Ek__BackingField_5() const { return ___U3CNodeTypeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CNodeTypeU3Ek__BackingField_5() { return &___U3CNodeTypeU3Ek__BackingField_5; }
	inline void set_U3CNodeTypeU3Ek__BackingField_5(int32_t value)
	{
		___U3CNodeTypeU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGICALBINARYEXPRESSION_T1440714930_H
#ifndef SYMMETRICALGORITHM_T4254223087_H
#define SYMMETRICALGORITHM_T4254223087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SymmetricAlgorithm
struct  SymmetricAlgorithm_t4254223087  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::BlockSizeValue
	int32_t ___BlockSizeValue_0;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::FeedbackSizeValue
	int32_t ___FeedbackSizeValue_1;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::IVValue
	ByteU5BU5D_t4116647657* ___IVValue_2;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::KeyValue
	ByteU5BU5D_t4116647657* ___KeyValue_3;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalBlockSizesValue
	KeySizesU5BU5D_t722666473* ___LegalBlockSizesValue_4;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t722666473* ___LegalKeySizesValue_5;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_6;
	// System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::ModeValue
	int32_t ___ModeValue_7;
	// System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::PaddingValue
	int32_t ___PaddingValue_8;

public:
	inline static int32_t get_offset_of_BlockSizeValue_0() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___BlockSizeValue_0)); }
	inline int32_t get_BlockSizeValue_0() const { return ___BlockSizeValue_0; }
	inline int32_t* get_address_of_BlockSizeValue_0() { return &___BlockSizeValue_0; }
	inline void set_BlockSizeValue_0(int32_t value)
	{
		___BlockSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_FeedbackSizeValue_1() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___FeedbackSizeValue_1)); }
	inline int32_t get_FeedbackSizeValue_1() const { return ___FeedbackSizeValue_1; }
	inline int32_t* get_address_of_FeedbackSizeValue_1() { return &___FeedbackSizeValue_1; }
	inline void set_FeedbackSizeValue_1(int32_t value)
	{
		___FeedbackSizeValue_1 = value;
	}

	inline static int32_t get_offset_of_IVValue_2() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___IVValue_2)); }
	inline ByteU5BU5D_t4116647657* get_IVValue_2() const { return ___IVValue_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_IVValue_2() { return &___IVValue_2; }
	inline void set_IVValue_2(ByteU5BU5D_t4116647657* value)
	{
		___IVValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___IVValue_2), value);
	}

	inline static int32_t get_offset_of_KeyValue_3() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___KeyValue_3)); }
	inline ByteU5BU5D_t4116647657* get_KeyValue_3() const { return ___KeyValue_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_KeyValue_3() { return &___KeyValue_3; }
	inline void set_KeyValue_3(ByteU5BU5D_t4116647657* value)
	{
		___KeyValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_3), value);
	}

	inline static int32_t get_offset_of_LegalBlockSizesValue_4() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___LegalBlockSizesValue_4)); }
	inline KeySizesU5BU5D_t722666473* get_LegalBlockSizesValue_4() const { return ___LegalBlockSizesValue_4; }
	inline KeySizesU5BU5D_t722666473** get_address_of_LegalBlockSizesValue_4() { return &___LegalBlockSizesValue_4; }
	inline void set_LegalBlockSizesValue_4(KeySizesU5BU5D_t722666473* value)
	{
		___LegalBlockSizesValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___LegalBlockSizesValue_4), value);
	}

	inline static int32_t get_offset_of_LegalKeySizesValue_5() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___LegalKeySizesValue_5)); }
	inline KeySizesU5BU5D_t722666473* get_LegalKeySizesValue_5() const { return ___LegalKeySizesValue_5; }
	inline KeySizesU5BU5D_t722666473** get_address_of_LegalKeySizesValue_5() { return &___LegalKeySizesValue_5; }
	inline void set_LegalKeySizesValue_5(KeySizesU5BU5D_t722666473* value)
	{
		___LegalKeySizesValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___LegalKeySizesValue_5), value);
	}

	inline static int32_t get_offset_of_KeySizeValue_6() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___KeySizeValue_6)); }
	inline int32_t get_KeySizeValue_6() const { return ___KeySizeValue_6; }
	inline int32_t* get_address_of_KeySizeValue_6() { return &___KeySizeValue_6; }
	inline void set_KeySizeValue_6(int32_t value)
	{
		___KeySizeValue_6 = value;
	}

	inline static int32_t get_offset_of_ModeValue_7() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___ModeValue_7)); }
	inline int32_t get_ModeValue_7() const { return ___ModeValue_7; }
	inline int32_t* get_address_of_ModeValue_7() { return &___ModeValue_7; }
	inline void set_ModeValue_7(int32_t value)
	{
		___ModeValue_7 = value;
	}

	inline static int32_t get_offset_of_PaddingValue_8() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___PaddingValue_8)); }
	inline int32_t get_PaddingValue_8() const { return ___PaddingValue_8; }
	inline int32_t* get_address_of_PaddingValue_8() { return &___PaddingValue_8; }
	inline void set_PaddingValue_8(int32_t value)
	{
		___PaddingValue_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMMETRICALGORITHM_T4254223087_H
#ifndef BYREFASSIGNBINARYEXPRESSION_T2460840393_H
#define BYREFASSIGNBINARYEXPRESSION_T2460840393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ByRefAssignBinaryExpression
struct  ByRefAssignBinaryExpression_t2460840393  : public AssignBinaryExpression_t2591659520
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYREFASSIGNBINARYEXPRESSION_T2460840393_H
#ifndef SIMPLEBINARYEXPRESSION_T1873369197_H
#define SIMPLEBINARYEXPRESSION_T1873369197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.SimpleBinaryExpression
struct  SimpleBinaryExpression_t1873369197  : public BinaryExpression_t77573129
{
public:
	// System.Linq.Expressions.ExpressionType System.Linq.Expressions.SimpleBinaryExpression::<NodeType>k__BackingField
	int32_t ___U3CNodeTypeU3Ek__BackingField_5;
	// System.Type System.Linq.Expressions.SimpleBinaryExpression::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CNodeTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SimpleBinaryExpression_t1873369197, ___U3CNodeTypeU3Ek__BackingField_5)); }
	inline int32_t get_U3CNodeTypeU3Ek__BackingField_5() const { return ___U3CNodeTypeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CNodeTypeU3Ek__BackingField_5() { return &___U3CNodeTypeU3Ek__BackingField_5; }
	inline void set_U3CNodeTypeU3Ek__BackingField_5(int32_t value)
	{
		___U3CNodeTypeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SimpleBinaryExpression_t1873369197, ___U3CTypeU3Ek__BackingField_6)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_6() const { return ___U3CTypeU3Ek__BackingField_6; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_6() { return &___U3CTypeU3Ek__BackingField_6; }
	inline void set_U3CTypeU3Ek__BackingField_6(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEBINARYEXPRESSION_T1873369197_H
#ifndef AESTRANSFORM_T2957123611_H
#define AESTRANSFORM_T2957123611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AesTransform
struct  AesTransform_t2957123611  : public SymmetricTransform_t3802591842
{
public:
	// System.UInt32[] System.Security.Cryptography.AesTransform::expandedKey
	UInt32U5BU5D_t2770800703* ___expandedKey_12;
	// System.Int32 System.Security.Cryptography.AesTransform::Nk
	int32_t ___Nk_13;
	// System.Int32 System.Security.Cryptography.AesTransform::Nr
	int32_t ___Nr_14;

public:
	inline static int32_t get_offset_of_expandedKey_12() { return static_cast<int32_t>(offsetof(AesTransform_t2957123611, ___expandedKey_12)); }
	inline UInt32U5BU5D_t2770800703* get_expandedKey_12() const { return ___expandedKey_12; }
	inline UInt32U5BU5D_t2770800703** get_address_of_expandedKey_12() { return &___expandedKey_12; }
	inline void set_expandedKey_12(UInt32U5BU5D_t2770800703* value)
	{
		___expandedKey_12 = value;
		Il2CppCodeGenWriteBarrier((&___expandedKey_12), value);
	}

	inline static int32_t get_offset_of_Nk_13() { return static_cast<int32_t>(offsetof(AesTransform_t2957123611, ___Nk_13)); }
	inline int32_t get_Nk_13() const { return ___Nk_13; }
	inline int32_t* get_address_of_Nk_13() { return &___Nk_13; }
	inline void set_Nk_13(int32_t value)
	{
		___Nk_13 = value;
	}

	inline static int32_t get_offset_of_Nr_14() { return static_cast<int32_t>(offsetof(AesTransform_t2957123611, ___Nr_14)); }
	inline int32_t get_Nr_14() const { return ___Nr_14; }
	inline int32_t* get_address_of_Nr_14() { return &___Nr_14; }
	inline void set_Nr_14(int32_t value)
	{
		___Nr_14 = value;
	}
};

struct AesTransform_t2957123611_StaticFields
{
public:
	// System.UInt32[] System.Security.Cryptography.AesTransform::Rcon
	UInt32U5BU5D_t2770800703* ___Rcon_15;
	// System.Byte[] System.Security.Cryptography.AesTransform::SBox
	ByteU5BU5D_t4116647657* ___SBox_16;
	// System.Byte[] System.Security.Cryptography.AesTransform::iSBox
	ByteU5BU5D_t4116647657* ___iSBox_17;
	// System.UInt32[] System.Security.Cryptography.AesTransform::T0
	UInt32U5BU5D_t2770800703* ___T0_18;
	// System.UInt32[] System.Security.Cryptography.AesTransform::T1
	UInt32U5BU5D_t2770800703* ___T1_19;
	// System.UInt32[] System.Security.Cryptography.AesTransform::T2
	UInt32U5BU5D_t2770800703* ___T2_20;
	// System.UInt32[] System.Security.Cryptography.AesTransform::T3
	UInt32U5BU5D_t2770800703* ___T3_21;
	// System.UInt32[] System.Security.Cryptography.AesTransform::iT0
	UInt32U5BU5D_t2770800703* ___iT0_22;
	// System.UInt32[] System.Security.Cryptography.AesTransform::iT1
	UInt32U5BU5D_t2770800703* ___iT1_23;
	// System.UInt32[] System.Security.Cryptography.AesTransform::iT2
	UInt32U5BU5D_t2770800703* ___iT2_24;
	// System.UInt32[] System.Security.Cryptography.AesTransform::iT3
	UInt32U5BU5D_t2770800703* ___iT3_25;

public:
	inline static int32_t get_offset_of_Rcon_15() { return static_cast<int32_t>(offsetof(AesTransform_t2957123611_StaticFields, ___Rcon_15)); }
	inline UInt32U5BU5D_t2770800703* get_Rcon_15() const { return ___Rcon_15; }
	inline UInt32U5BU5D_t2770800703** get_address_of_Rcon_15() { return &___Rcon_15; }
	inline void set_Rcon_15(UInt32U5BU5D_t2770800703* value)
	{
		___Rcon_15 = value;
		Il2CppCodeGenWriteBarrier((&___Rcon_15), value);
	}

	inline static int32_t get_offset_of_SBox_16() { return static_cast<int32_t>(offsetof(AesTransform_t2957123611_StaticFields, ___SBox_16)); }
	inline ByteU5BU5D_t4116647657* get_SBox_16() const { return ___SBox_16; }
	inline ByteU5BU5D_t4116647657** get_address_of_SBox_16() { return &___SBox_16; }
	inline void set_SBox_16(ByteU5BU5D_t4116647657* value)
	{
		___SBox_16 = value;
		Il2CppCodeGenWriteBarrier((&___SBox_16), value);
	}

	inline static int32_t get_offset_of_iSBox_17() { return static_cast<int32_t>(offsetof(AesTransform_t2957123611_StaticFields, ___iSBox_17)); }
	inline ByteU5BU5D_t4116647657* get_iSBox_17() const { return ___iSBox_17; }
	inline ByteU5BU5D_t4116647657** get_address_of_iSBox_17() { return &___iSBox_17; }
	inline void set_iSBox_17(ByteU5BU5D_t4116647657* value)
	{
		___iSBox_17 = value;
		Il2CppCodeGenWriteBarrier((&___iSBox_17), value);
	}

	inline static int32_t get_offset_of_T0_18() { return static_cast<int32_t>(offsetof(AesTransform_t2957123611_StaticFields, ___T0_18)); }
	inline UInt32U5BU5D_t2770800703* get_T0_18() const { return ___T0_18; }
	inline UInt32U5BU5D_t2770800703** get_address_of_T0_18() { return &___T0_18; }
	inline void set_T0_18(UInt32U5BU5D_t2770800703* value)
	{
		___T0_18 = value;
		Il2CppCodeGenWriteBarrier((&___T0_18), value);
	}

	inline static int32_t get_offset_of_T1_19() { return static_cast<int32_t>(offsetof(AesTransform_t2957123611_StaticFields, ___T1_19)); }
	inline UInt32U5BU5D_t2770800703* get_T1_19() const { return ___T1_19; }
	inline UInt32U5BU5D_t2770800703** get_address_of_T1_19() { return &___T1_19; }
	inline void set_T1_19(UInt32U5BU5D_t2770800703* value)
	{
		___T1_19 = value;
		Il2CppCodeGenWriteBarrier((&___T1_19), value);
	}

	inline static int32_t get_offset_of_T2_20() { return static_cast<int32_t>(offsetof(AesTransform_t2957123611_StaticFields, ___T2_20)); }
	inline UInt32U5BU5D_t2770800703* get_T2_20() const { return ___T2_20; }
	inline UInt32U5BU5D_t2770800703** get_address_of_T2_20() { return &___T2_20; }
	inline void set_T2_20(UInt32U5BU5D_t2770800703* value)
	{
		___T2_20 = value;
		Il2CppCodeGenWriteBarrier((&___T2_20), value);
	}

	inline static int32_t get_offset_of_T3_21() { return static_cast<int32_t>(offsetof(AesTransform_t2957123611_StaticFields, ___T3_21)); }
	inline UInt32U5BU5D_t2770800703* get_T3_21() const { return ___T3_21; }
	inline UInt32U5BU5D_t2770800703** get_address_of_T3_21() { return &___T3_21; }
	inline void set_T3_21(UInt32U5BU5D_t2770800703* value)
	{
		___T3_21 = value;
		Il2CppCodeGenWriteBarrier((&___T3_21), value);
	}

	inline static int32_t get_offset_of_iT0_22() { return static_cast<int32_t>(offsetof(AesTransform_t2957123611_StaticFields, ___iT0_22)); }
	inline UInt32U5BU5D_t2770800703* get_iT0_22() const { return ___iT0_22; }
	inline UInt32U5BU5D_t2770800703** get_address_of_iT0_22() { return &___iT0_22; }
	inline void set_iT0_22(UInt32U5BU5D_t2770800703* value)
	{
		___iT0_22 = value;
		Il2CppCodeGenWriteBarrier((&___iT0_22), value);
	}

	inline static int32_t get_offset_of_iT1_23() { return static_cast<int32_t>(offsetof(AesTransform_t2957123611_StaticFields, ___iT1_23)); }
	inline UInt32U5BU5D_t2770800703* get_iT1_23() const { return ___iT1_23; }
	inline UInt32U5BU5D_t2770800703** get_address_of_iT1_23() { return &___iT1_23; }
	inline void set_iT1_23(UInt32U5BU5D_t2770800703* value)
	{
		___iT1_23 = value;
		Il2CppCodeGenWriteBarrier((&___iT1_23), value);
	}

	inline static int32_t get_offset_of_iT2_24() { return static_cast<int32_t>(offsetof(AesTransform_t2957123611_StaticFields, ___iT2_24)); }
	inline UInt32U5BU5D_t2770800703* get_iT2_24() const { return ___iT2_24; }
	inline UInt32U5BU5D_t2770800703** get_address_of_iT2_24() { return &___iT2_24; }
	inline void set_iT2_24(UInt32U5BU5D_t2770800703* value)
	{
		___iT2_24 = value;
		Il2CppCodeGenWriteBarrier((&___iT2_24), value);
	}

	inline static int32_t get_offset_of_iT3_25() { return static_cast<int32_t>(offsetof(AesTransform_t2957123611_StaticFields, ___iT3_25)); }
	inline UInt32U5BU5D_t2770800703* get_iT3_25() const { return ___iT3_25; }
	inline UInt32U5BU5D_t2770800703** get_address_of_iT3_25() { return &___iT3_25; }
	inline void set_iT3_25(UInt32U5BU5D_t2770800703* value)
	{
		___iT3_25 = value;
		Il2CppCodeGenWriteBarrier((&___iT3_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AESTRANSFORM_T2957123611_H
#ifndef METHODBINARYEXPRESSION_T4129838888_H
#define METHODBINARYEXPRESSION_T4129838888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.MethodBinaryExpression
struct  MethodBinaryExpression_t4129838888  : public SimpleBinaryExpression_t1873369197
{
public:
	// System.Reflection.MethodInfo System.Linq.Expressions.MethodBinaryExpression::_method
	MethodInfo_t * ____method_7;

public:
	inline static int32_t get_offset_of__method_7() { return static_cast<int32_t>(offsetof(MethodBinaryExpression_t4129838888, ____method_7)); }
	inline MethodInfo_t * get__method_7() const { return ____method_7; }
	inline MethodInfo_t ** get_address_of__method_7() { return &____method_7; }
	inline void set__method_7(MethodInfo_t * value)
	{
		____method_7 = value;
		Il2CppCodeGenWriteBarrier((&____method_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBINARYEXPRESSION_T4129838888_H
#ifndef AES_T1218282760_H
#define AES_T1218282760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Aes
struct  Aes_t1218282760  : public SymmetricAlgorithm_t4254223087
{
public:

public:
};

struct Aes_t1218282760_StaticFields
{
public:
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.Aes::s_legalBlockSizes
	KeySizesU5BU5D_t722666473* ___s_legalBlockSizes_9;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.Aes::s_legalKeySizes
	KeySizesU5BU5D_t722666473* ___s_legalKeySizes_10;

public:
	inline static int32_t get_offset_of_s_legalBlockSizes_9() { return static_cast<int32_t>(offsetof(Aes_t1218282760_StaticFields, ___s_legalBlockSizes_9)); }
	inline KeySizesU5BU5D_t722666473* get_s_legalBlockSizes_9() const { return ___s_legalBlockSizes_9; }
	inline KeySizesU5BU5D_t722666473** get_address_of_s_legalBlockSizes_9() { return &___s_legalBlockSizes_9; }
	inline void set_s_legalBlockSizes_9(KeySizesU5BU5D_t722666473* value)
	{
		___s_legalBlockSizes_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_legalBlockSizes_9), value);
	}

	inline static int32_t get_offset_of_s_legalKeySizes_10() { return static_cast<int32_t>(offsetof(Aes_t1218282760_StaticFields, ___s_legalKeySizes_10)); }
	inline KeySizesU5BU5D_t722666473* get_s_legalKeySizes_10() const { return ___s_legalKeySizes_10; }
	inline KeySizesU5BU5D_t722666473** get_address_of_s_legalKeySizes_10() { return &___s_legalKeySizes_10; }
	inline void set_s_legalKeySizes_10(KeySizesU5BU5D_t722666473* value)
	{
		___s_legalKeySizes_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_legalKeySizes_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AES_T1218282760_H
#ifndef AESMANAGED_T1129950597_H
#define AESMANAGED_T1129950597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AesManaged
struct  AesManaged_t1129950597  : public Aes_t1218282760
{
public:
	// System.Security.Cryptography.RijndaelManaged System.Security.Cryptography.AesManaged::m_rijndael
	RijndaelManaged_t3586970409 * ___m_rijndael_11;

public:
	inline static int32_t get_offset_of_m_rijndael_11() { return static_cast<int32_t>(offsetof(AesManaged_t1129950597, ___m_rijndael_11)); }
	inline RijndaelManaged_t3586970409 * get_m_rijndael_11() const { return ___m_rijndael_11; }
	inline RijndaelManaged_t3586970409 ** get_address_of_m_rijndael_11() { return &___m_rijndael_11; }
	inline void set_m_rijndael_11(RijndaelManaged_t3586970409 * value)
	{
		___m_rijndael_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_rijndael_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AESMANAGED_T1129950597_H
#ifndef OPASSIGNMETHODCONVERSIONBINARYEXPRESSION_T393512413_H
#define OPASSIGNMETHODCONVERSIONBINARYEXPRESSION_T393512413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.OpAssignMethodConversionBinaryExpression
struct  OpAssignMethodConversionBinaryExpression_t393512413  : public MethodBinaryExpression_t4129838888
{
public:
	// System.Linq.Expressions.LambdaExpression System.Linq.Expressions.OpAssignMethodConversionBinaryExpression::_conversion
	LambdaExpression_t3131094331 * ____conversion_8;

public:
	inline static int32_t get_offset_of__conversion_8() { return static_cast<int32_t>(offsetof(OpAssignMethodConversionBinaryExpression_t393512413, ____conversion_8)); }
	inline LambdaExpression_t3131094331 * get__conversion_8() const { return ____conversion_8; }
	inline LambdaExpression_t3131094331 ** get_address_of__conversion_8() { return &____conversion_8; }
	inline void set__conversion_8(LambdaExpression_t3131094331 * value)
	{
		____conversion_8 = value;
		Il2CppCodeGenWriteBarrier((&____conversion_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPASSIGNMETHODCONVERSIONBINARYEXPRESSION_T393512413_H
#ifndef AESCRYPTOSERVICEPROVIDER_T345478893_H
#define AESCRYPTOSERVICEPROVIDER_T345478893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AesCryptoServiceProvider
struct  AesCryptoServiceProvider_t345478893  : public Aes_t1218282760
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AESCRYPTOSERVICEPROVIDER_T345478893_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3200 = { sizeof (U3CModuleU3E_t692745530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3201 = { sizeof (SR_t167583546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3202 = { sizeof (NotImplemented_t776753164), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3203 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3204 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3205 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3206 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3207 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3208 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3209 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3210 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3211 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3212 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3213 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3214 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3215 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3216 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3217 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3218 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3219 = { sizeof (AesManaged_t1129950597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3219[1] = 
{
	AesManaged_t1129950597::get_offset_of_m_rijndael_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3220 = { sizeof (AesCryptoServiceProvider_t345478893), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3221 = { sizeof (AesTransform_t2957123611), -1, sizeof(AesTransform_t2957123611_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3221[14] = 
{
	AesTransform_t2957123611::get_offset_of_expandedKey_12(),
	AesTransform_t2957123611::get_offset_of_Nk_13(),
	AesTransform_t2957123611::get_offset_of_Nr_14(),
	AesTransform_t2957123611_StaticFields::get_offset_of_Rcon_15(),
	AesTransform_t2957123611_StaticFields::get_offset_of_SBox_16(),
	AesTransform_t2957123611_StaticFields::get_offset_of_iSBox_17(),
	AesTransform_t2957123611_StaticFields::get_offset_of_T0_18(),
	AesTransform_t2957123611_StaticFields::get_offset_of_T1_19(),
	AesTransform_t2957123611_StaticFields::get_offset_of_T2_20(),
	AesTransform_t2957123611_StaticFields::get_offset_of_T3_21(),
	AesTransform_t2957123611_StaticFields::get_offset_of_iT0_22(),
	AesTransform_t2957123611_StaticFields::get_offset_of_iT1_23(),
	AesTransform_t2957123611_StaticFields::get_offset_of_iT2_24(),
	AesTransform_t2957123611_StaticFields::get_offset_of_iT3_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3222 = { sizeof (RuntimeOps_t4267392184), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3223 = { sizeof (CallSite_t1653101453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3223[1] = 
{
	CallSite_t1653101453::get_offset_of__binder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3224 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3224[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3225 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3225[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3226 = { sizeof (CallSiteBinder_t1870160633), -1, sizeof(CallSiteBinder_t1870160633_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3226[1] = 
{
	CallSiteBinder_t1870160633_StaticFields::get_offset_of_U3CUpdateLabelU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3227 = { sizeof (CallSiteOps_t258988244), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3228 = { sizeof (Closure_t348671509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3228[2] = 
{
	Closure_t348671509::get_offset_of_Constants_0(),
	Closure_t348671509::get_offset_of_Locals_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3229 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3230 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3230[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3231 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3231[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3232 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3233 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3234 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3234[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3235 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3236 = { sizeof (Error_t2882114465), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3237 = { sizeof (Enumerable_t538148348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3238 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3238[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3239 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3239[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3240 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3240[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3241 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3241[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3242 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3242[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3243 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3243[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3244 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3244[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3245 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3245[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3246 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3246[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3247 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3247[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3248 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3248[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3249 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3249[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3250 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3250[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3251 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3251[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3252 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3252[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3253 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3253[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3254 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3254[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3255 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3255[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3256 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3257 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3257[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3258 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3258[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3259 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3259[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3260 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3261 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3261[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3262 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3262[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3263 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3264 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3264[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3265 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3265[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3266 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3266[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3267 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3268 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3268[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3269 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3269[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3270 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3271 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3271[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3272 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3273 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3274 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3274[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3275 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3275[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3276 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3276[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3277 = { sizeof (Utilities_t3288484762), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3278 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3278[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3279 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3279[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3280 = { sizeof (CachedReflectionInfo_t3891313302), -1, sizeof(CachedReflectionInfo_t3891313302_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3280[38] = 
{
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_CallSiteOps_CreateMatchmaker_0(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_CallSiteOps_GetMatch_1(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_CallSiteOps_ClearMatch_2(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_CallSiteOps_UpdateRules_3(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_CallSiteOps_GetRules_4(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_CallSiteOps_GetRuleCache_5(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_CallSiteOps_GetCachedRules_6(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_CallSiteOps_AddRule_7(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_CallSiteOps_MoveRule_8(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_CallSiteOps_Bind_9(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_ArrayOfType_Bool_10(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Decimal_Ctor_Int32_11(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Decimal_Ctor_Int64_12(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Decimal_Ctor_Int32_Int32_Int32_Bool_Byte_13(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Closure_ObjectArray_ObjectArray_14(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Closure_Constants_15(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Closure_Locals_16(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_17(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle_18(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_MethodInfo_CreateDelegate_Type_Object_19(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_String_op_Equality_String_String_20(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_RuntimeOps_CreateRuntimeVariables_ObjectArray_Int64Array_21(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_RuntimeOps_CreateRuntimeVariables_22(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_RuntimeOps_Quote_23(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_DictionaryOfStringInt32_Add_String_Int32_24(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_DictionaryOfStringInt32_Ctor_Int32_25(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Type_GetTypeFromHandle_26(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Object_GetType_27(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Decimal_op_Implicit_Byte_28(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Decimal_op_Implicit_SByte_29(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Decimal_op_Implicit_Int16_30(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Decimal_op_Implicit_UInt16_31(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Decimal_op_Implicit_Int32_32(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Decimal_op_Implicit_UInt32_33(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Decimal_op_Implicit_Int64_34(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Decimal_op_Implicit_UInt64_35(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Decimal_op_Implicit_Char_36(),
	CachedReflectionInfo_t3891313302_StaticFields::get_offset_of_s_Math_Pow_Double_Double_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3281 = { sizeof (BinaryExpression_t77573129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3281[2] = 
{
	BinaryExpression_t77573129::get_offset_of_U3CRightU3Ek__BackingField_3(),
	BinaryExpression_t77573129::get_offset_of_U3CLeftU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3282 = { sizeof (LogicalBinaryExpression_t1440714930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3282[1] = 
{
	LogicalBinaryExpression_t1440714930::get_offset_of_U3CNodeTypeU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3283 = { sizeof (AssignBinaryExpression_t2591659520), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3284 = { sizeof (ByRefAssignBinaryExpression_t2460840393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3285 = { sizeof (CoalesceConversionBinaryExpression_t1217329768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3285[1] = 
{
	CoalesceConversionBinaryExpression_t1217329768::get_offset_of__conversion_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3286 = { sizeof (OpAssignMethodConversionBinaryExpression_t393512413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3286[1] = 
{
	OpAssignMethodConversionBinaryExpression_t393512413::get_offset_of__conversion_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3287 = { sizeof (SimpleBinaryExpression_t1873369197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3287[2] = 
{
	SimpleBinaryExpression_t1873369197::get_offset_of_U3CNodeTypeU3Ek__BackingField_5(),
	SimpleBinaryExpression_t1873369197::get_offset_of_U3CTypeU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3288 = { sizeof (MethodBinaryExpression_t4129838888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3288[1] = 
{
	MethodBinaryExpression_t4129838888::get_offset_of__method_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3289 = { sizeof (Expression_t1588164026), -1, sizeof(Expression_t1588164026_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3289[3] = 
{
	Expression_t1588164026_StaticFields::get_offset_of_s_lambdaDelegateCache_0(),
	Expression_t1588164026_StaticFields::get_offset_of_s_lambdaFactories_1(),
	Expression_t1588164026_StaticFields::get_offset_of_s_legacyCtorSupportTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3290 = { sizeof (BinaryExpressionProxy_t2974803306), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3291 = { sizeof (BlockExpressionProxy_t135452077), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3292 = { sizeof (CatchBlockProxy_t2867642995), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3293 = { sizeof (ConditionalExpressionProxy_t3440623873), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3294 = { sizeof (ConstantExpressionProxy_t2781329678), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3295 = { sizeof (DebugInfoExpressionProxy_t3441722008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3296 = { sizeof (DefaultExpressionProxy_t3404437725), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3297 = { sizeof (GotoExpressionProxy_t2147349472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3298 = { sizeof (IndexExpressionProxy_t3043937733), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3299 = { sizeof (InvocationExpressionProxy_t2926000647), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
