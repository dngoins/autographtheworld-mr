﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Generic.Dictionary`2<System.Reflection.TypeInfo,System.String>
struct Dictionary_2_t1031253066;
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.TypeInfo>
struct Dictionary_2_t1476042982;
// HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom
struct AutoJoinSessionAndRoom_t479681466;
// HoloToolkit.Sharing.Utilities.ManualIpConfiguration
struct ManualIpConfiguration_t1673208871;
// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct ExceptionDelegate_t827543389;
// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct ExceptionArgumentDelegate_t3478888600;
// UnityEngine.Camera
struct Camera_t4157153871;
// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t3643858324;
// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t801960530;
// System.String
struct String_t;
// HoloToolkit.Sharing.XString
struct XString_t940838754;
// HoloToolkit.Sharing.Element
struct Element_t654894041;
// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_t4212175550;
// System.Exception
struct Exception_t1436737249;
// HoloToolkit.Sharing.SessionManager
struct SessionManager_t3810561508;
// HoloToolkit.Sharing.SessionManagerAdapter
struct SessionManagerAdapter_t2913645974;
// System.Action`2<System.Boolean,System.String>
struct Action_2_t1290832230;
// System.Action`1<HoloToolkit.Sharing.Session>
struct Action_1_t2017209846;
// System.Action`2<HoloToolkit.Sharing.Session,HoloToolkit.Sharing.User>
struct Action_2_t473560391;
// System.Action
struct Action_t1264377477;
// System.Collections.Generic.List`1<HoloToolkit.Sharing.Session>
struct List_1_t3316816993;
// System.Action`1<HoloToolkit.Sharing.User>
struct Action_1_t962567155;
// HoloToolkit.Sharing.ServerSessionsTracker
struct ServerSessionsTracker_t3663004919;
// System.Collections.Generic.List`1<HoloToolkit.Sharing.User>
struct List_1_t2262174302;
// HoloToolkit.Sharing.FloatElement
struct FloatElement_t4176382651;
// HoloToolkit.Sharing.IntElement
struct IntElement_t687264645;
// HoloToolkit.Sharing.ObjectElementAdapter
struct ObjectElementAdapter_t1935870410;
// System.Collections.Generic.Dictionary`2<System.Int64,HoloToolkit.Sharing.SyncModel.SyncPrimitive>
struct Dictionary_2_t432307203;
// System.Collections.Generic.List`1<HoloToolkit.Sharing.SyncModel.SyncPrimitive>
struct List_1_t841742161;
// System.Action`1<HoloToolkit.Sharing.SyncModel.SyncObject>
struct Action_1_t3038819986;
// HoloToolkit.Sharing.ObjectElement
struct ObjectElement_t3710848331;
// HoloToolkit.Sharing.User
struct User_t790099560;
// HoloToolkit.Sharing.LongElement
struct LongElement_t2161894770;
// HoloToolkit.Sharing.BoolElement
struct BoolElement_t553673861;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// HoloToolkit.Sharing.DoubleElement
struct DoubleElement_t3393924332;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// HoloToolkit.Sharing.StringElement
struct StringElement_t2038048136;
// HoloToolkit.Sharing.SyncModel.SyncArray`1<HoloToolkit.Sharing.Spawning.SyncSpawnedObject>
struct SyncArray_1_t1616759892;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// HoloToolkit.Sharing.SyncModel.SyncVector3
struct SyncVector3_t1671304744;
// HoloToolkit.Sharing.SyncModel.SyncQuaternion
struct SyncQuaternion_t228159934;
// HoloToolkit.Sharing.SyncModel.SyncFloat
struct SyncFloat_t2189089750;
// HoloToolkit.Sharing.SyncModel.SyncTransform
struct SyncTransform_t4091961955;
// HoloToolkit.Sharing.SyncModel.SyncString
struct SyncString_t2813399380;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_0
struct SwigDelegatePairMaker_0_t3088859200;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_1
struct SwigDelegatePairMaker_1_t3088859199;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_2
struct SwigDelegatePairMaker_2_t3088859202;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_3
struct SwigDelegatePairMaker_3_t3088859201;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_4
struct SwigDelegatePairMaker_4_t3088859196;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_5
struct SwigDelegatePairMaker_5_t3088859195;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_6
struct SwigDelegatePairMaker_6_t3088859198;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_7
struct SwigDelegatePairMaker_7_t3088859197;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// HoloToolkit.Sharing.LogWriter/SwigDelegateLogWriter_0
struct SwigDelegateLogWriter_0_t50445701;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_0
struct SwigDelegateSessionManagerListener_0_t176756126;
// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_1
struct SwigDelegateSessionManagerListener_1_t1742840067;
// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_2
struct SwigDelegateSessionManagerListener_2_t3308924008;
// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_3
struct SwigDelegateSessionManagerListener_3_t580040653;
// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_4
struct SwigDelegateSessionManagerListener_4_t2502354954;
// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_5
struct SwigDelegateSessionManagerListener_5_t4068438895;
// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_6
struct SwigDelegateSessionManagerListener_6_t1339555540;
// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_7
struct SwigDelegateSessionManagerListener_7_t2905639481;
// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_8
struct SwigDelegateSessionManagerListener_8_t532986486;
// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_0
struct SwigDelegateRoomManagerListener_0_t3232391369;
// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_1
struct SwigDelegateRoomManagerListener_1_t3232391370;
// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_2
struct SwigDelegateRoomManagerListener_2_t3232391367;
// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_3
struct SwigDelegateRoomManagerListener_3_t3232391368;
// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_4
struct SwigDelegateRoomManagerListener_4_t3232391365;
// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_5
struct SwigDelegateRoomManagerListener_5_t3232391366;
// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_6
struct SwigDelegateRoomManagerListener_6_t3232391363;
// HoloToolkit.Sharing.SessionListener/SwigDelegateSessionListener_0
struct SwigDelegateSessionListener_0_t458903625;
// HoloToolkit.Sharing.SessionListener/SwigDelegateSessionListener_1
struct SwigDelegateSessionListener_1_t458903626;
// HoloToolkit.Sharing.SessionListener/SwigDelegateSessionListener_2
struct SwigDelegateSessionListener_2_t458903623;
// HoloToolkit.Sharing.SessionListener/SwigDelegateSessionListener_3
struct SwigDelegateSessionListener_3_t458903624;
// HoloToolkit.Sharing.StringArrayListener/SwigDelegateStringArrayListener_0
struct SwigDelegateStringArrayListener_0_t1418370588;
// HoloToolkit.Sharing.StringArrayListener/SwigDelegateStringArrayListener_1
struct SwigDelegateStringArrayListener_1_t1418370589;
// HoloToolkit.Sharing.StringArrayListener/SwigDelegateStringArrayListener_2
struct SwigDelegateStringArrayListener_2_t1418370590;
// HoloToolkit.Sharing.SyncListener/SwigDelegateSyncListener_0
struct SwigDelegateSyncListener_0_t1307184870;
// HoloToolkit.Sharing.SyncListener/SwigDelegateSyncListener_1
struct SwigDelegateSyncListener_1_t4036068225;
// HoloToolkit.Sharing.UserPresenceManagerListener/SwigDelegateUserPresenceManagerListener_0
struct SwigDelegateUserPresenceManagerListener_0_t4115417027;
// System.Action`1<HoloToolkit.Sharing.Room>
struct Action_1_t1763387898;
// System.Action`2<HoloToolkit.Sharing.Room,System.Int32>
struct Action_2_t1395259940;
// System.Action`3<System.Boolean,HoloToolkit.Sharing.AnchorDownloadRequest,HoloToolkit.Sharing.XString>
struct Action_3_t1312624934;
// System.Action`2<System.Boolean,HoloToolkit.Sharing.XString>
struct Action_2_t384220295;
// System.Action`1<HoloToolkit.Sharing.XString>
struct Action_1_t1113306349;
// HoloToolkit.Sharing.SharingStage
struct SharingStage_t1191029027;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// HoloToolkit.Unity.Vector3Interpolated
struct Vector3Interpolated_t2867391164;
// HoloToolkit.Unity.QuaternionInterpolated
struct QuaternionInterpolated_t376031970;
// HoloToolkit.Sharing.SyncModel.SyncObject
struct SyncObject_t2866352391;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// HoloToolkit.Unity.CircularBuffer
struct CircularBuffer_t1956967364;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// HoloToolkit.Unity.BitManipulator
struct BitManipulator_t1741666884;
// System.Threading.Mutex
struct Mutex_t3066672582;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// HoloToolkit.Unity.WorldAnchorManager
struct WorldAnchorManager_t2731889775;
// HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver/ProminentSpeakerInfo[]
struct ProminentSpeakerInfoU5BU5D_t3587787525;
// HoloToolkit.Sharing.NetworkConnectionAdapter
struct NetworkConnectionAdapter_t239632668;
// HoloToolkit.Sharing.SharingManager
struct SharingManager_t2871218373;
// HoloToolkit.Sharing.PairMaker
struct PairMaker_t2569523026;
// HoloToolkit.Sharing.PairingAdapter
struct PairingAdapter_t2261856171;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// System.Collections.Generic.Queue`1<HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo>
struct Queue_1_t2234784041;
// UnityEngine.XR.WSA.Persistence.WorldAnchorStore
struct WorldAnchorStore_t633400888;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t898892918;
// System.Collections.Generic.List`1<HoloToolkit.Sharing.Spawning.PrefabToDataModel>
struct List_1_t3234377962;
// System.EventHandler
struct EventHandler_t1348719766;
// HoloToolkit.Sharing.Utilities.ConsoleLogWriter
struct ConsoleLogWriter_t525723634;
// HoloToolkit.Sharing.SyncRoot
struct SyncRoot_t1223809262;
// HoloToolkit.Sharing.SessionUsersTracker
struct SessionUsersTracker_t2471166300;
// HoloToolkit.Sharing.SyncStateListener
struct SyncStateListener_t778580365;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// HoloToolkit.Sharing.DiscoveryClient
struct DiscoveryClient_t1530907974;
// HoloToolkit.Sharing.DiscoveryClientAdapter
struct DiscoveryClientAdapter_t99005982;
// HoloToolkit.Sharing.RoomManagerAdapter
struct RoomManagerAdapter_t3977794230;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Action`2<System.Boolean,UnityEngine.GameObject>
struct Action_2_t557018160;
// UnityEngine.XR.WSA.Sharing.WorldAnchorTransferBatch
struct WorldAnchorTransferBatch_t2392365061;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t2606371118;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SYNCSETTINGS_T2375960512_H
#define SYNCSETTINGS_T2375960512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncSettings
struct  SyncSettings_t2375960512  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Reflection.TypeInfo,System.String> HoloToolkit.Sharing.SyncModel.SyncSettings::dataModelTypeToName
	Dictionary_2_t1031253066 * ___dataModelTypeToName_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.TypeInfo> HoloToolkit.Sharing.SyncModel.SyncSettings::dataModelNameToType
	Dictionary_2_t1476042982 * ___dataModelNameToType_1;

public:
	inline static int32_t get_offset_of_dataModelTypeToName_0() { return static_cast<int32_t>(offsetof(SyncSettings_t2375960512, ___dataModelTypeToName_0)); }
	inline Dictionary_2_t1031253066 * get_dataModelTypeToName_0() const { return ___dataModelTypeToName_0; }
	inline Dictionary_2_t1031253066 ** get_address_of_dataModelTypeToName_0() { return &___dataModelTypeToName_0; }
	inline void set_dataModelTypeToName_0(Dictionary_2_t1031253066 * value)
	{
		___dataModelTypeToName_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataModelTypeToName_0), value);
	}

	inline static int32_t get_offset_of_dataModelNameToType_1() { return static_cast<int32_t>(offsetof(SyncSettings_t2375960512, ___dataModelNameToType_1)); }
	inline Dictionary_2_t1476042982 * get_dataModelNameToType_1() const { return ___dataModelNameToType_1; }
	inline Dictionary_2_t1476042982 ** get_address_of_dataModelNameToType_1() { return &___dataModelNameToType_1; }
	inline void set_dataModelNameToType_1(Dictionary_2_t1476042982 * value)
	{
		___dataModelNameToType_1 = value;
		Il2CppCodeGenWriteBarrier((&___dataModelNameToType_1), value);
	}
};

struct SyncSettings_t2375960512_StaticFields
{
public:
	// HoloToolkit.Sharing.SyncModel.SyncSettings HoloToolkit.Sharing.SyncModel.SyncSettings::instance
	SyncSettings_t2375960512 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SyncSettings_t2375960512_StaticFields, ___instance_2)); }
	inline SyncSettings_t2375960512 * get_instance_2() const { return ___instance_2; }
	inline SyncSettings_t2375960512 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(SyncSettings_t2375960512 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCSETTINGS_T2375960512_H
#ifndef U3CAUTOCONNECTU3ED__8_T3408352236_H
#define U3CAUTOCONNECTU3ED__8_T3408352236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom/<AutoConnect>d__8
struct  U3CAutoConnectU3Ed__8_t3408352236  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom/<AutoConnect>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom/<AutoConnect>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom/<AutoConnect>d__8::<>4__this
	AutoJoinSessionAndRoom_t479681466 * ___U3CU3E4__this_2;
	// System.Boolean HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom/<AutoConnect>d__8::<sessionExists>5__1
	bool ___U3CsessionExistsU3E5__1_3;
	// System.Int32 HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom/<AutoConnect>d__8::<i>5__2
	int32_t ___U3CiU3E5__2_4;
	// System.Int32 HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom/<AutoConnect>d__8::<i>5__3
	int32_t ___U3CiU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAutoConnectU3Ed__8_t3408352236, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAutoConnectU3Ed__8_t3408352236, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAutoConnectU3Ed__8_t3408352236, ___U3CU3E4__this_2)); }
	inline AutoJoinSessionAndRoom_t479681466 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AutoJoinSessionAndRoom_t479681466 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AutoJoinSessionAndRoom_t479681466 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CsessionExistsU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CAutoConnectU3Ed__8_t3408352236, ___U3CsessionExistsU3E5__1_3)); }
	inline bool get_U3CsessionExistsU3E5__1_3() const { return ___U3CsessionExistsU3E5__1_3; }
	inline bool* get_address_of_U3CsessionExistsU3E5__1_3() { return &___U3CsessionExistsU3E5__1_3; }
	inline void set_U3CsessionExistsU3E5__1_3(bool value)
	{
		___U3CsessionExistsU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CAutoConnectU3Ed__8_t3408352236, ___U3CiU3E5__2_4)); }
	inline int32_t get_U3CiU3E5__2_4() const { return ___U3CiU3E5__2_4; }
	inline int32_t* get_address_of_U3CiU3E5__2_4() { return &___U3CiU3E5__2_4; }
	inline void set_U3CiU3E5__2_4(int32_t value)
	{
		___U3CiU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CAutoConnectU3Ed__8_t3408352236, ___U3CiU3E5__3_5)); }
	inline int32_t get_U3CiU3E5__3_5() const { return ___U3CiU3E5__3_5; }
	inline int32_t* get_address_of_U3CiU3E5__3_5() { return &___U3CiU3E5__3_5; }
	inline void set_U3CiU3E5__3_5(int32_t value)
	{
		___U3CiU3E5__3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTOCONNECTU3ED__8_T3408352236_H
#ifndef U3CHIDEU3ED__24_T2549062377_H
#define U3CHIDEU3ED__24_T2549062377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Utilities.ManualIpConfiguration/<Hide>d__24
struct  U3CHideU3Ed__24_t2549062377  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Sharing.Utilities.ManualIpConfiguration/<Hide>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Sharing.Utilities.ManualIpConfiguration/<Hide>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Sharing.Utilities.ManualIpConfiguration HoloToolkit.Sharing.Utilities.ManualIpConfiguration/<Hide>d__24::<>4__this
	ManualIpConfiguration_t1673208871 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CHideU3Ed__24_t2549062377, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CHideU3Ed__24_t2549062377, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CHideU3Ed__24_t2549062377, ___U3CU3E4__this_2)); }
	inline ManualIpConfiguration_t1673208871 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ManualIpConfiguration_t1673208871 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ManualIpConfiguration_t1673208871 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIDEU3ED__24_T2549062377_H
#ifndef SWIGEXCEPTIONHELPER_T3643858324_H
#define SWIGEXCEPTIONHELPER_T3643858324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper
struct  SWIGExceptionHelper_t3643858324  : public RuntimeObject
{
public:

public:
};

struct SWIGExceptionHelper_t3643858324_StaticFields
{
public:
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionDelegate HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper::applicationDelegate
	ExceptionDelegate_t827543389 * ___applicationDelegate_0;
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionDelegate HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper::arithmeticDelegate
	ExceptionDelegate_t827543389 * ___arithmeticDelegate_1;
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionDelegate HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper::divideByZeroDelegate
	ExceptionDelegate_t827543389 * ___divideByZeroDelegate_2;
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionDelegate HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper::indexOutOfRangeDelegate
	ExceptionDelegate_t827543389 * ___indexOutOfRangeDelegate_3;
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionDelegate HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper::invalidCastDelegate
	ExceptionDelegate_t827543389 * ___invalidCastDelegate_4;
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionDelegate HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper::invalidOperationDelegate
	ExceptionDelegate_t827543389 * ___invalidOperationDelegate_5;
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionDelegate HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper::ioDelegate
	ExceptionDelegate_t827543389 * ___ioDelegate_6;
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionDelegate HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper::nullReferenceDelegate
	ExceptionDelegate_t827543389 * ___nullReferenceDelegate_7;
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionDelegate HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper::outOfMemoryDelegate
	ExceptionDelegate_t827543389 * ___outOfMemoryDelegate_8;
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionDelegate HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper::overflowDelegate
	ExceptionDelegate_t827543389 * ___overflowDelegate_9;
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionDelegate HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper::systemDelegate
	ExceptionDelegate_t827543389 * ___systemDelegate_10;
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper::argumentDelegate
	ExceptionArgumentDelegate_t3478888600 * ___argumentDelegate_11;
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper::argumentNullDelegate
	ExceptionArgumentDelegate_t3478888600 * ___argumentNullDelegate_12;
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper::argumentOutOfRangeDelegate
	ExceptionArgumentDelegate_t3478888600 * ___argumentOutOfRangeDelegate_13;

public:
	inline static int32_t get_offset_of_applicationDelegate_0() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t3643858324_StaticFields, ___applicationDelegate_0)); }
	inline ExceptionDelegate_t827543389 * get_applicationDelegate_0() const { return ___applicationDelegate_0; }
	inline ExceptionDelegate_t827543389 ** get_address_of_applicationDelegate_0() { return &___applicationDelegate_0; }
	inline void set_applicationDelegate_0(ExceptionDelegate_t827543389 * value)
	{
		___applicationDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___applicationDelegate_0), value);
	}

	inline static int32_t get_offset_of_arithmeticDelegate_1() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t3643858324_StaticFields, ___arithmeticDelegate_1)); }
	inline ExceptionDelegate_t827543389 * get_arithmeticDelegate_1() const { return ___arithmeticDelegate_1; }
	inline ExceptionDelegate_t827543389 ** get_address_of_arithmeticDelegate_1() { return &___arithmeticDelegate_1; }
	inline void set_arithmeticDelegate_1(ExceptionDelegate_t827543389 * value)
	{
		___arithmeticDelegate_1 = value;
		Il2CppCodeGenWriteBarrier((&___arithmeticDelegate_1), value);
	}

	inline static int32_t get_offset_of_divideByZeroDelegate_2() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t3643858324_StaticFields, ___divideByZeroDelegate_2)); }
	inline ExceptionDelegate_t827543389 * get_divideByZeroDelegate_2() const { return ___divideByZeroDelegate_2; }
	inline ExceptionDelegate_t827543389 ** get_address_of_divideByZeroDelegate_2() { return &___divideByZeroDelegate_2; }
	inline void set_divideByZeroDelegate_2(ExceptionDelegate_t827543389 * value)
	{
		___divideByZeroDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((&___divideByZeroDelegate_2), value);
	}

	inline static int32_t get_offset_of_indexOutOfRangeDelegate_3() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t3643858324_StaticFields, ___indexOutOfRangeDelegate_3)); }
	inline ExceptionDelegate_t827543389 * get_indexOutOfRangeDelegate_3() const { return ___indexOutOfRangeDelegate_3; }
	inline ExceptionDelegate_t827543389 ** get_address_of_indexOutOfRangeDelegate_3() { return &___indexOutOfRangeDelegate_3; }
	inline void set_indexOutOfRangeDelegate_3(ExceptionDelegate_t827543389 * value)
	{
		___indexOutOfRangeDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((&___indexOutOfRangeDelegate_3), value);
	}

	inline static int32_t get_offset_of_invalidCastDelegate_4() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t3643858324_StaticFields, ___invalidCastDelegate_4)); }
	inline ExceptionDelegate_t827543389 * get_invalidCastDelegate_4() const { return ___invalidCastDelegate_4; }
	inline ExceptionDelegate_t827543389 ** get_address_of_invalidCastDelegate_4() { return &___invalidCastDelegate_4; }
	inline void set_invalidCastDelegate_4(ExceptionDelegate_t827543389 * value)
	{
		___invalidCastDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((&___invalidCastDelegate_4), value);
	}

	inline static int32_t get_offset_of_invalidOperationDelegate_5() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t3643858324_StaticFields, ___invalidOperationDelegate_5)); }
	inline ExceptionDelegate_t827543389 * get_invalidOperationDelegate_5() const { return ___invalidOperationDelegate_5; }
	inline ExceptionDelegate_t827543389 ** get_address_of_invalidOperationDelegate_5() { return &___invalidOperationDelegate_5; }
	inline void set_invalidOperationDelegate_5(ExceptionDelegate_t827543389 * value)
	{
		___invalidOperationDelegate_5 = value;
		Il2CppCodeGenWriteBarrier((&___invalidOperationDelegate_5), value);
	}

	inline static int32_t get_offset_of_ioDelegate_6() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t3643858324_StaticFields, ___ioDelegate_6)); }
	inline ExceptionDelegate_t827543389 * get_ioDelegate_6() const { return ___ioDelegate_6; }
	inline ExceptionDelegate_t827543389 ** get_address_of_ioDelegate_6() { return &___ioDelegate_6; }
	inline void set_ioDelegate_6(ExceptionDelegate_t827543389 * value)
	{
		___ioDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((&___ioDelegate_6), value);
	}

	inline static int32_t get_offset_of_nullReferenceDelegate_7() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t3643858324_StaticFields, ___nullReferenceDelegate_7)); }
	inline ExceptionDelegate_t827543389 * get_nullReferenceDelegate_7() const { return ___nullReferenceDelegate_7; }
	inline ExceptionDelegate_t827543389 ** get_address_of_nullReferenceDelegate_7() { return &___nullReferenceDelegate_7; }
	inline void set_nullReferenceDelegate_7(ExceptionDelegate_t827543389 * value)
	{
		___nullReferenceDelegate_7 = value;
		Il2CppCodeGenWriteBarrier((&___nullReferenceDelegate_7), value);
	}

	inline static int32_t get_offset_of_outOfMemoryDelegate_8() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t3643858324_StaticFields, ___outOfMemoryDelegate_8)); }
	inline ExceptionDelegate_t827543389 * get_outOfMemoryDelegate_8() const { return ___outOfMemoryDelegate_8; }
	inline ExceptionDelegate_t827543389 ** get_address_of_outOfMemoryDelegate_8() { return &___outOfMemoryDelegate_8; }
	inline void set_outOfMemoryDelegate_8(ExceptionDelegate_t827543389 * value)
	{
		___outOfMemoryDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((&___outOfMemoryDelegate_8), value);
	}

	inline static int32_t get_offset_of_overflowDelegate_9() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t3643858324_StaticFields, ___overflowDelegate_9)); }
	inline ExceptionDelegate_t827543389 * get_overflowDelegate_9() const { return ___overflowDelegate_9; }
	inline ExceptionDelegate_t827543389 ** get_address_of_overflowDelegate_9() { return &___overflowDelegate_9; }
	inline void set_overflowDelegate_9(ExceptionDelegate_t827543389 * value)
	{
		___overflowDelegate_9 = value;
		Il2CppCodeGenWriteBarrier((&___overflowDelegate_9), value);
	}

	inline static int32_t get_offset_of_systemDelegate_10() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t3643858324_StaticFields, ___systemDelegate_10)); }
	inline ExceptionDelegate_t827543389 * get_systemDelegate_10() const { return ___systemDelegate_10; }
	inline ExceptionDelegate_t827543389 ** get_address_of_systemDelegate_10() { return &___systemDelegate_10; }
	inline void set_systemDelegate_10(ExceptionDelegate_t827543389 * value)
	{
		___systemDelegate_10 = value;
		Il2CppCodeGenWriteBarrier((&___systemDelegate_10), value);
	}

	inline static int32_t get_offset_of_argumentDelegate_11() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t3643858324_StaticFields, ___argumentDelegate_11)); }
	inline ExceptionArgumentDelegate_t3478888600 * get_argumentDelegate_11() const { return ___argumentDelegate_11; }
	inline ExceptionArgumentDelegate_t3478888600 ** get_address_of_argumentDelegate_11() { return &___argumentDelegate_11; }
	inline void set_argumentDelegate_11(ExceptionArgumentDelegate_t3478888600 * value)
	{
		___argumentDelegate_11 = value;
		Il2CppCodeGenWriteBarrier((&___argumentDelegate_11), value);
	}

	inline static int32_t get_offset_of_argumentNullDelegate_12() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t3643858324_StaticFields, ___argumentNullDelegate_12)); }
	inline ExceptionArgumentDelegate_t3478888600 * get_argumentNullDelegate_12() const { return ___argumentNullDelegate_12; }
	inline ExceptionArgumentDelegate_t3478888600 ** get_address_of_argumentNullDelegate_12() { return &___argumentNullDelegate_12; }
	inline void set_argumentNullDelegate_12(ExceptionArgumentDelegate_t3478888600 * value)
	{
		___argumentNullDelegate_12 = value;
		Il2CppCodeGenWriteBarrier((&___argumentNullDelegate_12), value);
	}

	inline static int32_t get_offset_of_argumentOutOfRangeDelegate_13() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t3643858324_StaticFields, ___argumentOutOfRangeDelegate_13)); }
	inline ExceptionArgumentDelegate_t3478888600 * get_argumentOutOfRangeDelegate_13() const { return ___argumentOutOfRangeDelegate_13; }
	inline ExceptionArgumentDelegate_t3478888600 ** get_address_of_argumentOutOfRangeDelegate_13() { return &___argumentOutOfRangeDelegate_13; }
	inline void set_argumentOutOfRangeDelegate_13(ExceptionArgumentDelegate_t3478888600 * value)
	{
		___argumentOutOfRangeDelegate_13 = value;
		Il2CppCodeGenWriteBarrier((&___argumentOutOfRangeDelegate_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGEXCEPTIONHELPER_T3643858324_H
#ifndef CAMERACACHE_T1177433023_H
#define CAMERACACHE_T1177433023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.CameraCache
struct  CameraCache_t1177433023  : public RuntimeObject
{
public:

public:
};

struct CameraCache_t1177433023_StaticFields
{
public:
	// UnityEngine.Camera HoloToolkit.Unity.CameraCache::cachedCamera
	Camera_t4157153871 * ___cachedCamera_0;

public:
	inline static int32_t get_offset_of_cachedCamera_0() { return static_cast<int32_t>(offsetof(CameraCache_t1177433023_StaticFields, ___cachedCamera_0)); }
	inline Camera_t4157153871 * get_cachedCamera_0() const { return ___cachedCamera_0; }
	inline Camera_t4157153871 ** get_address_of_cachedCamera_0() { return &___cachedCamera_0; }
	inline void set_cachedCamera_0(Camera_t4157153871 * value)
	{
		___cachedCamera_0 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCamera_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACACHE_T1177433023_H
#ifndef SHARINGCLIENT_T1608744197_H
#define SHARINGCLIENT_T1608744197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SharingClient
struct  SharingClient_t1608744197  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARINGCLIENT_T1608744197_H
#ifndef SHARINGCLIENTPINVOKE_T520792832_H
#define SHARINGCLIENTPINVOKE_T520792832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SharingClientPINVOKE
struct  SharingClientPINVOKE_t520792832  : public RuntimeObject
{
public:

public:
};

struct SharingClientPINVOKE_t520792832_StaticFields
{
public:
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper HoloToolkit.Sharing.SharingClientPINVOKE::swigExceptionHelper
	SWIGExceptionHelper_t3643858324 * ___swigExceptionHelper_0;
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGStringHelper HoloToolkit.Sharing.SharingClientPINVOKE::swigStringHelper
	SWIGStringHelper_t801960530 * ___swigStringHelper_1;

public:
	inline static int32_t get_offset_of_swigExceptionHelper_0() { return static_cast<int32_t>(offsetof(SharingClientPINVOKE_t520792832_StaticFields, ___swigExceptionHelper_0)); }
	inline SWIGExceptionHelper_t3643858324 * get_swigExceptionHelper_0() const { return ___swigExceptionHelper_0; }
	inline SWIGExceptionHelper_t3643858324 ** get_address_of_swigExceptionHelper_0() { return &___swigExceptionHelper_0; }
	inline void set_swigExceptionHelper_0(SWIGExceptionHelper_t3643858324 * value)
	{
		___swigExceptionHelper_0 = value;
		Il2CppCodeGenWriteBarrier((&___swigExceptionHelper_0), value);
	}

	inline static int32_t get_offset_of_swigStringHelper_1() { return static_cast<int32_t>(offsetof(SharingClientPINVOKE_t520792832_StaticFields, ___swigStringHelper_1)); }
	inline SWIGStringHelper_t801960530 * get_swigStringHelper_1() const { return ___swigStringHelper_1; }
	inline SWIGStringHelper_t801960530 ** get_address_of_swigStringHelper_1() { return &___swigStringHelper_1; }
	inline void set_swigStringHelper_1(SWIGStringHelper_t801960530 * value)
	{
		___swigStringHelper_1 = value;
		Il2CppCodeGenWriteBarrier((&___swigStringHelper_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARINGCLIENTPINVOKE_T520792832_H
#ifndef SYNCPRIMITIVE_T3664634715_H
#define SYNCPRIMITIVE_T3664634715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncPrimitive
struct  SyncPrimitive_t3664634715  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Sharing.SyncModel.SyncPrimitive::fieldName
	String_t* ___fieldName_0;
	// HoloToolkit.Sharing.XString HoloToolkit.Sharing.SyncModel.SyncPrimitive::xStringFieldName
	XString_t940838754 * ___xStringFieldName_1;
	// HoloToolkit.Sharing.Element HoloToolkit.Sharing.SyncModel.SyncPrimitive::internalElement
	Element_t654894041 * ___internalElement_2;

public:
	inline static int32_t get_offset_of_fieldName_0() { return static_cast<int32_t>(offsetof(SyncPrimitive_t3664634715, ___fieldName_0)); }
	inline String_t* get_fieldName_0() const { return ___fieldName_0; }
	inline String_t** get_address_of_fieldName_0() { return &___fieldName_0; }
	inline void set_fieldName_0(String_t* value)
	{
		___fieldName_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldName_0), value);
	}

	inline static int32_t get_offset_of_xStringFieldName_1() { return static_cast<int32_t>(offsetof(SyncPrimitive_t3664634715, ___xStringFieldName_1)); }
	inline XString_t940838754 * get_xStringFieldName_1() const { return ___xStringFieldName_1; }
	inline XString_t940838754 ** get_address_of_xStringFieldName_1() { return &___xStringFieldName_1; }
	inline void set_xStringFieldName_1(XString_t940838754 * value)
	{
		___xStringFieldName_1 = value;
		Il2CppCodeGenWriteBarrier((&___xStringFieldName_1), value);
	}

	inline static int32_t get_offset_of_internalElement_2() { return static_cast<int32_t>(offsetof(SyncPrimitive_t3664634715, ___internalElement_2)); }
	inline Element_t654894041 * get_internalElement_2() const { return ___internalElement_2; }
	inline Element_t654894041 ** get_address_of_internalElement_2() { return &___internalElement_2; }
	inline void set_internalElement_2(Element_t654894041 * value)
	{
		___internalElement_2 = value;
		Il2CppCodeGenWriteBarrier((&___internalElement_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCPRIMITIVE_T3664634715_H
#ifndef SWIGSTRINGHELPER_T801960530_H
#define SWIGSTRINGHELPER_T801960530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGStringHelper
struct  SWIGStringHelper_t801960530  : public RuntimeObject
{
public:

public:
};

struct SWIGStringHelper_t801960530_StaticFields
{
public:
	// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGStringHelper/SWIGStringDelegate HoloToolkit.Sharing.SharingClientPINVOKE/SWIGStringHelper::stringDelegate
	SWIGStringDelegate_t4212175550 * ___stringDelegate_0;

public:
	inline static int32_t get_offset_of_stringDelegate_0() { return static_cast<int32_t>(offsetof(SWIGStringHelper_t801960530_StaticFields, ___stringDelegate_0)); }
	inline SWIGStringDelegate_t4212175550 * get_stringDelegate_0() const { return ___stringDelegate_0; }
	inline SWIGStringDelegate_t4212175550 ** get_address_of_stringDelegate_0() { return &___stringDelegate_0; }
	inline void set_stringDelegate_0(SWIGStringDelegate_t4212175550 * value)
	{
		___stringDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___stringDelegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGSTRINGHELPER_T801960530_H
#ifndef SWIGPENDINGEXCEPTION_T3488673755_H
#define SWIGPENDINGEXCEPTION_T3488673755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGPendingException
struct  SWIGPendingException_t3488673755  : public RuntimeObject
{
public:

public:
};

struct SWIGPendingException_t3488673755_StaticFields
{
public:
	// System.Int32 HoloToolkit.Sharing.SharingClientPINVOKE/SWIGPendingException::numExceptionsPending
	int32_t ___numExceptionsPending_1;

public:
	inline static int32_t get_offset_of_numExceptionsPending_1() { return static_cast<int32_t>(offsetof(SWIGPendingException_t3488673755_StaticFields, ___numExceptionsPending_1)); }
	inline int32_t get_numExceptionsPending_1() const { return ___numExceptionsPending_1; }
	inline int32_t* get_address_of_numExceptionsPending_1() { return &___numExceptionsPending_1; }
	inline void set_numExceptionsPending_1(int32_t value)
	{
		___numExceptionsPending_1 = value;
	}
};

struct SWIGPendingException_t3488673755_ThreadStaticFields
{
public:
	// System.Exception HoloToolkit.Sharing.SharingClientPINVOKE/SWIGPendingException::pendingException
	Exception_t1436737249 * ___pendingException_0;

public:
	inline static int32_t get_offset_of_pendingException_0() { return static_cast<int32_t>(offsetof(SWIGPendingException_t3488673755_ThreadStaticFields, ___pendingException_0)); }
	inline Exception_t1436737249 * get_pendingException_0() const { return ___pendingException_0; }
	inline Exception_t1436737249 ** get_address_of_pendingException_0() { return &___pendingException_0; }
	inline void set_pendingException_0(Exception_t1436737249 * value)
	{
		___pendingException_0 = value;
		Il2CppCodeGenWriteBarrier((&___pendingException_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGPENDINGEXCEPTION_T3488673755_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef SERVERSESSIONSTRACKER_T3663004919_H
#define SERVERSESSIONSTRACKER_T3663004919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ServerSessionsTracker
struct  ServerSessionsTracker_t3663004919  : public RuntimeObject
{
public:
	// System.Boolean HoloToolkit.Sharing.ServerSessionsTracker::disposed
	bool ___disposed_0;
	// HoloToolkit.Sharing.SessionManager HoloToolkit.Sharing.ServerSessionsTracker::sessionManager
	SessionManager_t3810561508 * ___sessionManager_1;
	// HoloToolkit.Sharing.SessionManagerAdapter HoloToolkit.Sharing.ServerSessionsTracker::sessionManagerAdapter
	SessionManagerAdapter_t2913645974 * ___sessionManagerAdapter_2;
	// System.Action`2<System.Boolean,System.String> HoloToolkit.Sharing.ServerSessionsTracker::SessionCreated
	Action_2_t1290832230 * ___SessionCreated_3;
	// System.Action`1<HoloToolkit.Sharing.Session> HoloToolkit.Sharing.ServerSessionsTracker::SessionAdded
	Action_1_t2017209846 * ___SessionAdded_4;
	// System.Action`1<HoloToolkit.Sharing.Session> HoloToolkit.Sharing.ServerSessionsTracker::SessionClosed
	Action_1_t2017209846 * ___SessionClosed_5;
	// System.Action`2<HoloToolkit.Sharing.Session,HoloToolkit.Sharing.User> HoloToolkit.Sharing.ServerSessionsTracker::UserChanged
	Action_2_t473560391 * ___UserChanged_6;
	// System.Action`2<HoloToolkit.Sharing.Session,HoloToolkit.Sharing.User> HoloToolkit.Sharing.ServerSessionsTracker::UserJoined
	Action_2_t473560391 * ___UserJoined_7;
	// System.Action`2<HoloToolkit.Sharing.Session,HoloToolkit.Sharing.User> HoloToolkit.Sharing.ServerSessionsTracker::UserLeft
	Action_2_t473560391 * ___UserLeft_8;
	// System.Action`1<HoloToolkit.Sharing.Session> HoloToolkit.Sharing.ServerSessionsTracker::CurrentUserLeft
	Action_1_t2017209846 * ___CurrentUserLeft_9;
	// System.Action`1<HoloToolkit.Sharing.Session> HoloToolkit.Sharing.ServerSessionsTracker::CurrentUserJoined
	Action_1_t2017209846 * ___CurrentUserJoined_10;
	// System.Action HoloToolkit.Sharing.ServerSessionsTracker::ServerConnected
	Action_t1264377477 * ___ServerConnected_11;
	// System.Action HoloToolkit.Sharing.ServerSessionsTracker::ServerDisconnected
	Action_t1264377477 * ___ServerDisconnected_12;
	// System.Collections.Generic.List`1<HoloToolkit.Sharing.Session> HoloToolkit.Sharing.ServerSessionsTracker::<Sessions>k__BackingField
	List_1_t3316816993 * ___U3CSessionsU3Ek__BackingField_13;
	// System.Boolean HoloToolkit.Sharing.ServerSessionsTracker::<IsServerConnected>k__BackingField
	bool ___U3CIsServerConnectedU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_disposed_0() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t3663004919, ___disposed_0)); }
	inline bool get_disposed_0() const { return ___disposed_0; }
	inline bool* get_address_of_disposed_0() { return &___disposed_0; }
	inline void set_disposed_0(bool value)
	{
		___disposed_0 = value;
	}

	inline static int32_t get_offset_of_sessionManager_1() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t3663004919, ___sessionManager_1)); }
	inline SessionManager_t3810561508 * get_sessionManager_1() const { return ___sessionManager_1; }
	inline SessionManager_t3810561508 ** get_address_of_sessionManager_1() { return &___sessionManager_1; }
	inline void set_sessionManager_1(SessionManager_t3810561508 * value)
	{
		___sessionManager_1 = value;
		Il2CppCodeGenWriteBarrier((&___sessionManager_1), value);
	}

	inline static int32_t get_offset_of_sessionManagerAdapter_2() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t3663004919, ___sessionManagerAdapter_2)); }
	inline SessionManagerAdapter_t2913645974 * get_sessionManagerAdapter_2() const { return ___sessionManagerAdapter_2; }
	inline SessionManagerAdapter_t2913645974 ** get_address_of_sessionManagerAdapter_2() { return &___sessionManagerAdapter_2; }
	inline void set_sessionManagerAdapter_2(SessionManagerAdapter_t2913645974 * value)
	{
		___sessionManagerAdapter_2 = value;
		Il2CppCodeGenWriteBarrier((&___sessionManagerAdapter_2), value);
	}

	inline static int32_t get_offset_of_SessionCreated_3() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t3663004919, ___SessionCreated_3)); }
	inline Action_2_t1290832230 * get_SessionCreated_3() const { return ___SessionCreated_3; }
	inline Action_2_t1290832230 ** get_address_of_SessionCreated_3() { return &___SessionCreated_3; }
	inline void set_SessionCreated_3(Action_2_t1290832230 * value)
	{
		___SessionCreated_3 = value;
		Il2CppCodeGenWriteBarrier((&___SessionCreated_3), value);
	}

	inline static int32_t get_offset_of_SessionAdded_4() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t3663004919, ___SessionAdded_4)); }
	inline Action_1_t2017209846 * get_SessionAdded_4() const { return ___SessionAdded_4; }
	inline Action_1_t2017209846 ** get_address_of_SessionAdded_4() { return &___SessionAdded_4; }
	inline void set_SessionAdded_4(Action_1_t2017209846 * value)
	{
		___SessionAdded_4 = value;
		Il2CppCodeGenWriteBarrier((&___SessionAdded_4), value);
	}

	inline static int32_t get_offset_of_SessionClosed_5() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t3663004919, ___SessionClosed_5)); }
	inline Action_1_t2017209846 * get_SessionClosed_5() const { return ___SessionClosed_5; }
	inline Action_1_t2017209846 ** get_address_of_SessionClosed_5() { return &___SessionClosed_5; }
	inline void set_SessionClosed_5(Action_1_t2017209846 * value)
	{
		___SessionClosed_5 = value;
		Il2CppCodeGenWriteBarrier((&___SessionClosed_5), value);
	}

	inline static int32_t get_offset_of_UserChanged_6() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t3663004919, ___UserChanged_6)); }
	inline Action_2_t473560391 * get_UserChanged_6() const { return ___UserChanged_6; }
	inline Action_2_t473560391 ** get_address_of_UserChanged_6() { return &___UserChanged_6; }
	inline void set_UserChanged_6(Action_2_t473560391 * value)
	{
		___UserChanged_6 = value;
		Il2CppCodeGenWriteBarrier((&___UserChanged_6), value);
	}

	inline static int32_t get_offset_of_UserJoined_7() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t3663004919, ___UserJoined_7)); }
	inline Action_2_t473560391 * get_UserJoined_7() const { return ___UserJoined_7; }
	inline Action_2_t473560391 ** get_address_of_UserJoined_7() { return &___UserJoined_7; }
	inline void set_UserJoined_7(Action_2_t473560391 * value)
	{
		___UserJoined_7 = value;
		Il2CppCodeGenWriteBarrier((&___UserJoined_7), value);
	}

	inline static int32_t get_offset_of_UserLeft_8() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t3663004919, ___UserLeft_8)); }
	inline Action_2_t473560391 * get_UserLeft_8() const { return ___UserLeft_8; }
	inline Action_2_t473560391 ** get_address_of_UserLeft_8() { return &___UserLeft_8; }
	inline void set_UserLeft_8(Action_2_t473560391 * value)
	{
		___UserLeft_8 = value;
		Il2CppCodeGenWriteBarrier((&___UserLeft_8), value);
	}

	inline static int32_t get_offset_of_CurrentUserLeft_9() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t3663004919, ___CurrentUserLeft_9)); }
	inline Action_1_t2017209846 * get_CurrentUserLeft_9() const { return ___CurrentUserLeft_9; }
	inline Action_1_t2017209846 ** get_address_of_CurrentUserLeft_9() { return &___CurrentUserLeft_9; }
	inline void set_CurrentUserLeft_9(Action_1_t2017209846 * value)
	{
		___CurrentUserLeft_9 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentUserLeft_9), value);
	}

	inline static int32_t get_offset_of_CurrentUserJoined_10() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t3663004919, ___CurrentUserJoined_10)); }
	inline Action_1_t2017209846 * get_CurrentUserJoined_10() const { return ___CurrentUserJoined_10; }
	inline Action_1_t2017209846 ** get_address_of_CurrentUserJoined_10() { return &___CurrentUserJoined_10; }
	inline void set_CurrentUserJoined_10(Action_1_t2017209846 * value)
	{
		___CurrentUserJoined_10 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentUserJoined_10), value);
	}

	inline static int32_t get_offset_of_ServerConnected_11() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t3663004919, ___ServerConnected_11)); }
	inline Action_t1264377477 * get_ServerConnected_11() const { return ___ServerConnected_11; }
	inline Action_t1264377477 ** get_address_of_ServerConnected_11() { return &___ServerConnected_11; }
	inline void set_ServerConnected_11(Action_t1264377477 * value)
	{
		___ServerConnected_11 = value;
		Il2CppCodeGenWriteBarrier((&___ServerConnected_11), value);
	}

	inline static int32_t get_offset_of_ServerDisconnected_12() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t3663004919, ___ServerDisconnected_12)); }
	inline Action_t1264377477 * get_ServerDisconnected_12() const { return ___ServerDisconnected_12; }
	inline Action_t1264377477 ** get_address_of_ServerDisconnected_12() { return &___ServerDisconnected_12; }
	inline void set_ServerDisconnected_12(Action_t1264377477 * value)
	{
		___ServerDisconnected_12 = value;
		Il2CppCodeGenWriteBarrier((&___ServerDisconnected_12), value);
	}

	inline static int32_t get_offset_of_U3CSessionsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t3663004919, ___U3CSessionsU3Ek__BackingField_13)); }
	inline List_1_t3316816993 * get_U3CSessionsU3Ek__BackingField_13() const { return ___U3CSessionsU3Ek__BackingField_13; }
	inline List_1_t3316816993 ** get_address_of_U3CSessionsU3Ek__BackingField_13() { return &___U3CSessionsU3Ek__BackingField_13; }
	inline void set_U3CSessionsU3Ek__BackingField_13(List_1_t3316816993 * value)
	{
		___U3CSessionsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSessionsU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CIsServerConnectedU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t3663004919, ___U3CIsServerConnectedU3Ek__BackingField_14)); }
	inline bool get_U3CIsServerConnectedU3Ek__BackingField_14() const { return ___U3CIsServerConnectedU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CIsServerConnectedU3Ek__BackingField_14() { return &___U3CIsServerConnectedU3Ek__BackingField_14; }
	inline void set_U3CIsServerConnectedU3Ek__BackingField_14(bool value)
	{
		___U3CIsServerConnectedU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERSESSIONSTRACKER_T3663004919_H
#ifndef SESSIONUSERSTRACKER_T2471166300_H
#define SESSIONUSERSTRACKER_T2471166300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionUsersTracker
struct  SessionUsersTracker_t2471166300  : public RuntimeObject
{
public:
	// System.Action`1<HoloToolkit.Sharing.User> HoloToolkit.Sharing.SessionUsersTracker::UserJoined
	Action_1_t962567155 * ___UserJoined_0;
	// System.Action`1<HoloToolkit.Sharing.User> HoloToolkit.Sharing.SessionUsersTracker::UserLeft
	Action_1_t962567155 * ___UserLeft_1;
	// HoloToolkit.Sharing.ServerSessionsTracker HoloToolkit.Sharing.SessionUsersTracker::serverSessionsTracker
	ServerSessionsTracker_t3663004919 * ___serverSessionsTracker_2;
	// System.Collections.Generic.List`1<HoloToolkit.Sharing.User> HoloToolkit.Sharing.SessionUsersTracker::<CurrentUsers>k__BackingField
	List_1_t2262174302 * ___U3CCurrentUsersU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_UserJoined_0() { return static_cast<int32_t>(offsetof(SessionUsersTracker_t2471166300, ___UserJoined_0)); }
	inline Action_1_t962567155 * get_UserJoined_0() const { return ___UserJoined_0; }
	inline Action_1_t962567155 ** get_address_of_UserJoined_0() { return &___UserJoined_0; }
	inline void set_UserJoined_0(Action_1_t962567155 * value)
	{
		___UserJoined_0 = value;
		Il2CppCodeGenWriteBarrier((&___UserJoined_0), value);
	}

	inline static int32_t get_offset_of_UserLeft_1() { return static_cast<int32_t>(offsetof(SessionUsersTracker_t2471166300, ___UserLeft_1)); }
	inline Action_1_t962567155 * get_UserLeft_1() const { return ___UserLeft_1; }
	inline Action_1_t962567155 ** get_address_of_UserLeft_1() { return &___UserLeft_1; }
	inline void set_UserLeft_1(Action_1_t962567155 * value)
	{
		___UserLeft_1 = value;
		Il2CppCodeGenWriteBarrier((&___UserLeft_1), value);
	}

	inline static int32_t get_offset_of_serverSessionsTracker_2() { return static_cast<int32_t>(offsetof(SessionUsersTracker_t2471166300, ___serverSessionsTracker_2)); }
	inline ServerSessionsTracker_t3663004919 * get_serverSessionsTracker_2() const { return ___serverSessionsTracker_2; }
	inline ServerSessionsTracker_t3663004919 ** get_address_of_serverSessionsTracker_2() { return &___serverSessionsTracker_2; }
	inline void set_serverSessionsTracker_2(ServerSessionsTracker_t3663004919 * value)
	{
		___serverSessionsTracker_2 = value;
		Il2CppCodeGenWriteBarrier((&___serverSessionsTracker_2), value);
	}

	inline static int32_t get_offset_of_U3CCurrentUsersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SessionUsersTracker_t2471166300, ___U3CCurrentUsersU3Ek__BackingField_3)); }
	inline List_1_t2262174302 * get_U3CCurrentUsersU3Ek__BackingField_3() const { return ___U3CCurrentUsersU3Ek__BackingField_3; }
	inline List_1_t2262174302 ** get_address_of_U3CCurrentUsersU3Ek__BackingField_3() { return &___U3CCurrentUsersU3Ek__BackingField_3; }
	inline void set_U3CCurrentUsersU3Ek__BackingField_3(List_1_t2262174302 * value)
	{
		___U3CCurrentUsersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentUsersU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONUSERSTRACKER_T2471166300_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef SYNCFLOAT_T2189089750_H
#define SYNCFLOAT_T2189089750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncFloat
struct  SyncFloat_t2189089750  : public SyncPrimitive_t3664634715
{
public:
	// HoloToolkit.Sharing.FloatElement HoloToolkit.Sharing.SyncModel.SyncFloat::element
	FloatElement_t4176382651 * ___element_3;
	// System.Single HoloToolkit.Sharing.SyncModel.SyncFloat::value
	float ___value_4;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(SyncFloat_t2189089750, ___element_3)); }
	inline FloatElement_t4176382651 * get_element_3() const { return ___element_3; }
	inline FloatElement_t4176382651 ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(FloatElement_t4176382651 * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(SyncFloat_t2189089750, ___value_4)); }
	inline float get_value_4() const { return ___value_4; }
	inline float* get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(float value)
	{
		___value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCFLOAT_T2189089750_H
#ifndef SYNCINTEGER_T2757910165_H
#define SYNCINTEGER_T2757910165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncInteger
struct  SyncInteger_t2757910165  : public SyncPrimitive_t3664634715
{
public:
	// HoloToolkit.Sharing.IntElement HoloToolkit.Sharing.SyncModel.SyncInteger::element
	IntElement_t687264645 * ___element_3;
	// System.Int32 HoloToolkit.Sharing.SyncModel.SyncInteger::value
	int32_t ___value_4;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(SyncInteger_t2757910165, ___element_3)); }
	inline IntElement_t687264645 * get_element_3() const { return ___element_3; }
	inline IntElement_t687264645 ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(IntElement_t687264645 * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(SyncInteger_t2757910165, ___value_4)); }
	inline int32_t get_value_4() const { return ___value_4; }
	inline int32_t* get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(int32_t value)
	{
		___value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCINTEGER_T2757910165_H
#ifndef SYNCOBJECT_T2866352391_H
#define SYNCOBJECT_T2866352391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncObject
struct  SyncObject_t2866352391  : public SyncPrimitive_t3664634715
{
public:
	// HoloToolkit.Sharing.ObjectElementAdapter HoloToolkit.Sharing.SyncModel.SyncObject::syncListener
	ObjectElementAdapter_t1935870410 * ___syncListener_3;
	// System.Collections.Generic.Dictionary`2<System.Int64,HoloToolkit.Sharing.SyncModel.SyncPrimitive> HoloToolkit.Sharing.SyncModel.SyncObject::primitiveMap
	Dictionary_2_t432307203 * ___primitiveMap_4;
	// System.Collections.Generic.List`1<HoloToolkit.Sharing.SyncModel.SyncPrimitive> HoloToolkit.Sharing.SyncModel.SyncObject::primitives
	List_1_t841742161 * ___primitives_5;
	// System.Action`1<HoloToolkit.Sharing.SyncModel.SyncObject> HoloToolkit.Sharing.SyncModel.SyncObject::ObjectChanged
	Action_1_t3038819986 * ___ObjectChanged_6;
	// System.Action`1<HoloToolkit.Sharing.SyncModel.SyncObject> HoloToolkit.Sharing.SyncModel.SyncObject::InitializationComplete
	Action_1_t3038819986 * ___InitializationComplete_7;
	// HoloToolkit.Sharing.ObjectElement HoloToolkit.Sharing.SyncModel.SyncObject::internalObjectElement
	ObjectElement_t3710848331 * ___internalObjectElement_8;
	// HoloToolkit.Sharing.User HoloToolkit.Sharing.SyncModel.SyncObject::owner
	User_t790099560 * ___owner_9;

public:
	inline static int32_t get_offset_of_syncListener_3() { return static_cast<int32_t>(offsetof(SyncObject_t2866352391, ___syncListener_3)); }
	inline ObjectElementAdapter_t1935870410 * get_syncListener_3() const { return ___syncListener_3; }
	inline ObjectElementAdapter_t1935870410 ** get_address_of_syncListener_3() { return &___syncListener_3; }
	inline void set_syncListener_3(ObjectElementAdapter_t1935870410 * value)
	{
		___syncListener_3 = value;
		Il2CppCodeGenWriteBarrier((&___syncListener_3), value);
	}

	inline static int32_t get_offset_of_primitiveMap_4() { return static_cast<int32_t>(offsetof(SyncObject_t2866352391, ___primitiveMap_4)); }
	inline Dictionary_2_t432307203 * get_primitiveMap_4() const { return ___primitiveMap_4; }
	inline Dictionary_2_t432307203 ** get_address_of_primitiveMap_4() { return &___primitiveMap_4; }
	inline void set_primitiveMap_4(Dictionary_2_t432307203 * value)
	{
		___primitiveMap_4 = value;
		Il2CppCodeGenWriteBarrier((&___primitiveMap_4), value);
	}

	inline static int32_t get_offset_of_primitives_5() { return static_cast<int32_t>(offsetof(SyncObject_t2866352391, ___primitives_5)); }
	inline List_1_t841742161 * get_primitives_5() const { return ___primitives_5; }
	inline List_1_t841742161 ** get_address_of_primitives_5() { return &___primitives_5; }
	inline void set_primitives_5(List_1_t841742161 * value)
	{
		___primitives_5 = value;
		Il2CppCodeGenWriteBarrier((&___primitives_5), value);
	}

	inline static int32_t get_offset_of_ObjectChanged_6() { return static_cast<int32_t>(offsetof(SyncObject_t2866352391, ___ObjectChanged_6)); }
	inline Action_1_t3038819986 * get_ObjectChanged_6() const { return ___ObjectChanged_6; }
	inline Action_1_t3038819986 ** get_address_of_ObjectChanged_6() { return &___ObjectChanged_6; }
	inline void set_ObjectChanged_6(Action_1_t3038819986 * value)
	{
		___ObjectChanged_6 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectChanged_6), value);
	}

	inline static int32_t get_offset_of_InitializationComplete_7() { return static_cast<int32_t>(offsetof(SyncObject_t2866352391, ___InitializationComplete_7)); }
	inline Action_1_t3038819986 * get_InitializationComplete_7() const { return ___InitializationComplete_7; }
	inline Action_1_t3038819986 ** get_address_of_InitializationComplete_7() { return &___InitializationComplete_7; }
	inline void set_InitializationComplete_7(Action_1_t3038819986 * value)
	{
		___InitializationComplete_7 = value;
		Il2CppCodeGenWriteBarrier((&___InitializationComplete_7), value);
	}

	inline static int32_t get_offset_of_internalObjectElement_8() { return static_cast<int32_t>(offsetof(SyncObject_t2866352391, ___internalObjectElement_8)); }
	inline ObjectElement_t3710848331 * get_internalObjectElement_8() const { return ___internalObjectElement_8; }
	inline ObjectElement_t3710848331 ** get_address_of_internalObjectElement_8() { return &___internalObjectElement_8; }
	inline void set_internalObjectElement_8(ObjectElement_t3710848331 * value)
	{
		___internalObjectElement_8 = value;
		Il2CppCodeGenWriteBarrier((&___internalObjectElement_8), value);
	}

	inline static int32_t get_offset_of_owner_9() { return static_cast<int32_t>(offsetof(SyncObject_t2866352391, ___owner_9)); }
	inline User_t790099560 * get_owner_9() const { return ___owner_9; }
	inline User_t790099560 ** get_address_of_owner_9() { return &___owner_9; }
	inline void set_owner_9(User_t790099560 * value)
	{
		___owner_9 = value;
		Il2CppCodeGenWriteBarrier((&___owner_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCOBJECT_T2866352391_H
#ifndef SYNCLONG_T1964838366_H
#define SYNCLONG_T1964838366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncLong
struct  SyncLong_t1964838366  : public SyncPrimitive_t3664634715
{
public:
	// HoloToolkit.Sharing.LongElement HoloToolkit.Sharing.SyncModel.SyncLong::element
	LongElement_t2161894770 * ___element_3;
	// System.Int64 HoloToolkit.Sharing.SyncModel.SyncLong::value
	int64_t ___value_4;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(SyncLong_t1964838366, ___element_3)); }
	inline LongElement_t2161894770 * get_element_3() const { return ___element_3; }
	inline LongElement_t2161894770 ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(LongElement_t2161894770 * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(SyncLong_t1964838366, ___value_4)); }
	inline int64_t get_value_4() const { return ___value_4; }
	inline int64_t* get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(int64_t value)
	{
		___value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCLONG_T1964838366_H
#ifndef SYNCBOOL_T2415766883_H
#define SYNCBOOL_T2415766883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncBool
struct  SyncBool_t2415766883  : public SyncPrimitive_t3664634715
{
public:
	// HoloToolkit.Sharing.BoolElement HoloToolkit.Sharing.SyncModel.SyncBool::element
	BoolElement_t553673861 * ___element_3;
	// System.Boolean HoloToolkit.Sharing.SyncModel.SyncBool::value
	bool ___value_4;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(SyncBool_t2415766883, ___element_3)); }
	inline BoolElement_t553673861 * get_element_3() const { return ___element_3; }
	inline BoolElement_t553673861 ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(BoolElement_t553673861 * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(SyncBool_t2415766883, ___value_4)); }
	inline bool get_value_4() const { return ___value_4; }
	inline bool* get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(bool value)
	{
		___value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCBOOL_T2415766883_H
#ifndef PREFABTODATAMODEL_T1762303220_H
#define PREFABTODATAMODEL_T1762303220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Spawning.PrefabToDataModel
struct  PrefabToDataModel_t1762303220 
{
public:
	// System.String HoloToolkit.Sharing.Spawning.PrefabToDataModel::DataModelClassName
	String_t* ___DataModelClassName_0;
	// UnityEngine.GameObject HoloToolkit.Sharing.Spawning.PrefabToDataModel::Prefab
	GameObject_t1113636619 * ___Prefab_1;

public:
	inline static int32_t get_offset_of_DataModelClassName_0() { return static_cast<int32_t>(offsetof(PrefabToDataModel_t1762303220, ___DataModelClassName_0)); }
	inline String_t* get_DataModelClassName_0() const { return ___DataModelClassName_0; }
	inline String_t** get_address_of_DataModelClassName_0() { return &___DataModelClassName_0; }
	inline void set_DataModelClassName_0(String_t* value)
	{
		___DataModelClassName_0 = value;
		Il2CppCodeGenWriteBarrier((&___DataModelClassName_0), value);
	}

	inline static int32_t get_offset_of_Prefab_1() { return static_cast<int32_t>(offsetof(PrefabToDataModel_t1762303220, ___Prefab_1)); }
	inline GameObject_t1113636619 * get_Prefab_1() const { return ___Prefab_1; }
	inline GameObject_t1113636619 ** get_address_of_Prefab_1() { return &___Prefab_1; }
	inline void set_Prefab_1(GameObject_t1113636619 * value)
	{
		___Prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Sharing.Spawning.PrefabToDataModel
struct PrefabToDataModel_t1762303220_marshaled_pinvoke
{
	char* ___DataModelClassName_0;
	GameObject_t1113636619 * ___Prefab_1;
};
// Native definition for COM marshalling of HoloToolkit.Sharing.Spawning.PrefabToDataModel
struct PrefabToDataModel_t1762303220_marshaled_com
{
	Il2CppChar* ___DataModelClassName_0;
	GameObject_t1113636619 * ___Prefab_1;
};
#endif // PREFABTODATAMODEL_T1762303220_H
#ifndef SYNCDATAATTRIBUTE_T3499957947_H
#define SYNCDATAATTRIBUTE_T3499957947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncDataAttribute
struct  SyncDataAttribute_t3499957947  : public Attribute_t861562559
{
public:
	// System.String HoloToolkit.Sharing.SyncModel.SyncDataAttribute::CustomFieldName
	String_t* ___CustomFieldName_0;

public:
	inline static int32_t get_offset_of_CustomFieldName_0() { return static_cast<int32_t>(offsetof(SyncDataAttribute_t3499957947, ___CustomFieldName_0)); }
	inline String_t* get_CustomFieldName_0() const { return ___CustomFieldName_0; }
	inline String_t** get_address_of_CustomFieldName_0() { return &___CustomFieldName_0; }
	inline void set_CustomFieldName_0(String_t* value)
	{
		___CustomFieldName_0 = value;
		Il2CppCodeGenWriteBarrier((&___CustomFieldName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCDATAATTRIBUTE_T3499957947_H
#ifndef SYNCDOUBLE_T3192565710_H
#define SYNCDOUBLE_T3192565710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncDouble
struct  SyncDouble_t3192565710  : public SyncPrimitive_t3664634715
{
public:
	// HoloToolkit.Sharing.DoubleElement HoloToolkit.Sharing.SyncModel.SyncDouble::element
	DoubleElement_t3393924332 * ___element_3;
	// System.Double HoloToolkit.Sharing.SyncModel.SyncDouble::value
	double ___value_4;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(SyncDouble_t3192565710, ___element_3)); }
	inline DoubleElement_t3393924332 * get_element_3() const { return ___element_3; }
	inline DoubleElement_t3393924332 ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(DoubleElement_t3393924332 * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(SyncDouble_t3192565710, ___value_4)); }
	inline double get_value_4() const { return ___value_4; }
	inline double* get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(double value)
	{
		___value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCDOUBLE_T3192565710_H
#ifndef SYNCDATACLASSATTRIBUTE_T3940588719_H
#define SYNCDATACLASSATTRIBUTE_T3940588719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncDataClassAttribute
struct  SyncDataClassAttribute_t3940588719  : public Attribute_t861562559
{
public:
	// System.String HoloToolkit.Sharing.SyncModel.SyncDataClassAttribute::CustomClassName
	String_t* ___CustomClassName_0;

public:
	inline static int32_t get_offset_of_CustomClassName_0() { return static_cast<int32_t>(offsetof(SyncDataClassAttribute_t3940588719, ___CustomClassName_0)); }
	inline String_t* get_CustomClassName_0() const { return ___CustomClassName_0; }
	inline String_t** get_address_of_CustomClassName_0() { return &___CustomClassName_0; }
	inline void set_CustomClassName_0(String_t* value)
	{
		___CustomClassName_0 = value;
		Il2CppCodeGenWriteBarrier((&___CustomClassName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCDATACLASSATTRIBUTE_T3940588719_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t385246372* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t385246372* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_31)); }
	inline DateTime_t3738529785  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t3738529785 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t3738529785  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_32)); }
	inline DateTime_t3738529785  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t3738529785  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef SYNCSTRING_T2813399380_H
#define SYNCSTRING_T2813399380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncString
struct  SyncString_t2813399380  : public SyncPrimitive_t3664634715
{
public:
	// HoloToolkit.Sharing.StringElement HoloToolkit.Sharing.SyncModel.SyncString::element
	StringElement_t2038048136 * ___element_3;
	// System.String HoloToolkit.Sharing.SyncModel.SyncString::value
	String_t* ___value_4;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(SyncString_t2813399380, ___element_3)); }
	inline StringElement_t2038048136 * get_element_3() const { return ___element_3; }
	inline StringElement_t2038048136 ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(StringElement_t2038048136 * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(SyncString_t2813399380, ___value_4)); }
	inline String_t* get_value_4() const { return ___value_4; }
	inline String_t** get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(String_t* value)
	{
		___value_4 = value;
		Il2CppCodeGenWriteBarrier((&___value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCSTRING_T2813399380_H
#ifndef SYNCROOT_T1223809262_H
#define SYNCROOT_T1223809262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncRoot
struct  SyncRoot_t1223809262  : public SyncObject_t2866352391
{
public:
	// HoloToolkit.Sharing.SyncModel.SyncArray`1<HoloToolkit.Sharing.Spawning.SyncSpawnedObject> HoloToolkit.Sharing.SyncRoot::InstantiatedPrefabs
	SyncArray_1_t1616759892 * ___InstantiatedPrefabs_10;

public:
	inline static int32_t get_offset_of_InstantiatedPrefabs_10() { return static_cast<int32_t>(offsetof(SyncRoot_t1223809262, ___InstantiatedPrefabs_10)); }
	inline SyncArray_1_t1616759892 * get_InstantiatedPrefabs_10() const { return ___InstantiatedPrefabs_10; }
	inline SyncArray_1_t1616759892 ** get_address_of_InstantiatedPrefabs_10() { return &___InstantiatedPrefabs_10; }
	inline void set_InstantiatedPrefabs_10(SyncArray_1_t1616759892 * value)
	{
		___InstantiatedPrefabs_10 = value;
		Il2CppCodeGenWriteBarrier((&___InstantiatedPrefabs_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCROOT_T1223809262_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef PROMINENTSPEAKERINFO_T3676337740_H
#define PROMINENTSPEAKERINFO_T3676337740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver/ProminentSpeakerInfo
struct  ProminentSpeakerInfo_t3676337740  : public RuntimeObject
{
public:
	// System.UInt32 HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver/ProminentSpeakerInfo::SourceId
	uint32_t ___SourceId_0;
	// System.Single HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver/ProminentSpeakerInfo::AverageAmplitude
	float ___AverageAmplitude_1;
	// UnityEngine.Vector3 HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver/ProminentSpeakerInfo::HrtfPosition
	Vector3_t3722313464  ___HrtfPosition_2;

public:
	inline static int32_t get_offset_of_SourceId_0() { return static_cast<int32_t>(offsetof(ProminentSpeakerInfo_t3676337740, ___SourceId_0)); }
	inline uint32_t get_SourceId_0() const { return ___SourceId_0; }
	inline uint32_t* get_address_of_SourceId_0() { return &___SourceId_0; }
	inline void set_SourceId_0(uint32_t value)
	{
		___SourceId_0 = value;
	}

	inline static int32_t get_offset_of_AverageAmplitude_1() { return static_cast<int32_t>(offsetof(ProminentSpeakerInfo_t3676337740, ___AverageAmplitude_1)); }
	inline float get_AverageAmplitude_1() const { return ___AverageAmplitude_1; }
	inline float* get_address_of_AverageAmplitude_1() { return &___AverageAmplitude_1; }
	inline void set_AverageAmplitude_1(float value)
	{
		___AverageAmplitude_1 = value;
	}

	inline static int32_t get_offset_of_HrtfPosition_2() { return static_cast<int32_t>(offsetof(ProminentSpeakerInfo_t3676337740, ___HrtfPosition_2)); }
	inline Vector3_t3722313464  get_HrtfPosition_2() const { return ___HrtfPosition_2; }
	inline Vector3_t3722313464 * get_address_of_HrtfPosition_2() { return &___HrtfPosition_2; }
	inline void set_HrtfPosition_2(Vector3_t3722313464  value)
	{
		___HrtfPosition_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROMINENTSPEAKERINFO_T3676337740_H
#ifndef SYSTEMROLE_T769564548_H
#define SYSTEMROLE_T769564548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SystemRole
struct  SystemRole_t769564548 
{
public:
	// System.Int32 HoloToolkit.Sharing.SystemRole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SystemRole_t769564548, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMROLE_T769564548_H
#ifndef ROLE_T2521298479_H
#define ROLE_T2521298479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Utilities.DirectPairing/Role
struct  Role_t2521298479 
{
public:
	// System.Int32 HoloToolkit.Sharing.Utilities.DirectPairing/Role::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Role_t2521298479, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROLE_T2521298479_H
#ifndef CLIENTROLE_T373356971_H
#define CLIENTROLE_T373356971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ClientRole
struct  ClientRole_t373356971 
{
public:
	// System.Int32 HoloToolkit.Sharing.ClientRole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClientRole_t373356971, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTROLE_T373356971_H
#ifndef HANDLEREF_T3745784363_H
#define HANDLEREF_T3745784363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.HandleRef
struct  HandleRef_t3745784363 
{
public:
	// System.Object System.Runtime.InteropServices.HandleRef::wrapper
	RuntimeObject * ___wrapper_0;
	// System.IntPtr System.Runtime.InteropServices.HandleRef::handle
	intptr_t ___handle_1;

public:
	inline static int32_t get_offset_of_wrapper_0() { return static_cast<int32_t>(offsetof(HandleRef_t3745784363, ___wrapper_0)); }
	inline RuntimeObject * get_wrapper_0() const { return ___wrapper_0; }
	inline RuntimeObject ** get_address_of_wrapper_0() { return &___wrapper_0; }
	inline void set_wrapper_0(RuntimeObject * value)
	{
		___wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___wrapper_0), value);
	}

	inline static int32_t get_offset_of_handle_1() { return static_cast<int32_t>(offsetof(HandleRef_t3745784363, ___handle_1)); }
	inline intptr_t get_handle_1() const { return ___handle_1; }
	inline intptr_t* get_address_of_handle_1() { return &___handle_1; }
	inline void set_handle_1(intptr_t value)
	{
		___handle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.InteropServices.HandleRef
struct HandleRef_t3745784363_marshaled_pinvoke
{
	Il2CppIUnknown* ___wrapper_0;
	intptr_t ___handle_1;
};
// Native definition for COM marshalling of System.Runtime.InteropServices.HandleRef
struct HandleRef_t3745784363_marshaled_com
{
	Il2CppIUnknown* ___wrapper_0;
	intptr_t ___handle_1;
};
#endif // HANDLEREF_T3745784363_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef STREAMCATEGORY_T2445411138_H
#define STREAMCATEGORY_T2445411138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.MicStream/StreamCategory
struct  StreamCategory_t2445411138 
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.MicStream/StreamCategory::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamCategory_t2445411138, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMCATEGORY_T2445411138_H
#ifndef SYNCTRANSFORM_T4091961955_H
#define SYNCTRANSFORM_T4091961955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncTransform
struct  SyncTransform_t4091961955  : public SyncObject_t2866352391
{
public:
	// HoloToolkit.Sharing.SyncModel.SyncVector3 HoloToolkit.Sharing.SyncModel.SyncTransform::Position
	SyncVector3_t1671304744 * ___Position_10;
	// HoloToolkit.Sharing.SyncModel.SyncQuaternion HoloToolkit.Sharing.SyncModel.SyncTransform::Rotation
	SyncQuaternion_t228159934 * ___Rotation_11;
	// HoloToolkit.Sharing.SyncModel.SyncVector3 HoloToolkit.Sharing.SyncModel.SyncTransform::Scale
	SyncVector3_t1671304744 * ___Scale_12;
	// System.Action HoloToolkit.Sharing.SyncModel.SyncTransform::PositionChanged
	Action_t1264377477 * ___PositionChanged_13;
	// System.Action HoloToolkit.Sharing.SyncModel.SyncTransform::RotationChanged
	Action_t1264377477 * ___RotationChanged_14;
	// System.Action HoloToolkit.Sharing.SyncModel.SyncTransform::ScaleChanged
	Action_t1264377477 * ___ScaleChanged_15;

public:
	inline static int32_t get_offset_of_Position_10() { return static_cast<int32_t>(offsetof(SyncTransform_t4091961955, ___Position_10)); }
	inline SyncVector3_t1671304744 * get_Position_10() const { return ___Position_10; }
	inline SyncVector3_t1671304744 ** get_address_of_Position_10() { return &___Position_10; }
	inline void set_Position_10(SyncVector3_t1671304744 * value)
	{
		___Position_10 = value;
		Il2CppCodeGenWriteBarrier((&___Position_10), value);
	}

	inline static int32_t get_offset_of_Rotation_11() { return static_cast<int32_t>(offsetof(SyncTransform_t4091961955, ___Rotation_11)); }
	inline SyncQuaternion_t228159934 * get_Rotation_11() const { return ___Rotation_11; }
	inline SyncQuaternion_t228159934 ** get_address_of_Rotation_11() { return &___Rotation_11; }
	inline void set_Rotation_11(SyncQuaternion_t228159934 * value)
	{
		___Rotation_11 = value;
		Il2CppCodeGenWriteBarrier((&___Rotation_11), value);
	}

	inline static int32_t get_offset_of_Scale_12() { return static_cast<int32_t>(offsetof(SyncTransform_t4091961955, ___Scale_12)); }
	inline SyncVector3_t1671304744 * get_Scale_12() const { return ___Scale_12; }
	inline SyncVector3_t1671304744 ** get_address_of_Scale_12() { return &___Scale_12; }
	inline void set_Scale_12(SyncVector3_t1671304744 * value)
	{
		___Scale_12 = value;
		Il2CppCodeGenWriteBarrier((&___Scale_12), value);
	}

	inline static int32_t get_offset_of_PositionChanged_13() { return static_cast<int32_t>(offsetof(SyncTransform_t4091961955, ___PositionChanged_13)); }
	inline Action_t1264377477 * get_PositionChanged_13() const { return ___PositionChanged_13; }
	inline Action_t1264377477 ** get_address_of_PositionChanged_13() { return &___PositionChanged_13; }
	inline void set_PositionChanged_13(Action_t1264377477 * value)
	{
		___PositionChanged_13 = value;
		Il2CppCodeGenWriteBarrier((&___PositionChanged_13), value);
	}

	inline static int32_t get_offset_of_RotationChanged_14() { return static_cast<int32_t>(offsetof(SyncTransform_t4091961955, ___RotationChanged_14)); }
	inline Action_t1264377477 * get_RotationChanged_14() const { return ___RotationChanged_14; }
	inline Action_t1264377477 ** get_address_of_RotationChanged_14() { return &___RotationChanged_14; }
	inline void set_RotationChanged_14(Action_t1264377477 * value)
	{
		___RotationChanged_14 = value;
		Il2CppCodeGenWriteBarrier((&___RotationChanged_14), value);
	}

	inline static int32_t get_offset_of_ScaleChanged_15() { return static_cast<int32_t>(offsetof(SyncTransform_t4091961955, ___ScaleChanged_15)); }
	inline Action_t1264377477 * get_ScaleChanged_15() const { return ___ScaleChanged_15; }
	inline Action_t1264377477 ** get_address_of_ScaleChanged_15() { return &___ScaleChanged_15; }
	inline void set_ScaleChanged_15(Action_t1264377477 * value)
	{
		___ScaleChanged_15 = value;
		Il2CppCodeGenWriteBarrier((&___ScaleChanged_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCTRANSFORM_T4091961955_H
#ifndef SYNCQUATERNION_T228159934_H
#define SYNCQUATERNION_T228159934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncQuaternion
struct  SyncQuaternion_t228159934  : public SyncObject_t2866352391
{
public:
	// HoloToolkit.Sharing.SyncModel.SyncFloat HoloToolkit.Sharing.SyncModel.SyncQuaternion::x
	SyncFloat_t2189089750 * ___x_10;
	// HoloToolkit.Sharing.SyncModel.SyncFloat HoloToolkit.Sharing.SyncModel.SyncQuaternion::y
	SyncFloat_t2189089750 * ___y_11;
	// HoloToolkit.Sharing.SyncModel.SyncFloat HoloToolkit.Sharing.SyncModel.SyncQuaternion::z
	SyncFloat_t2189089750 * ___z_12;
	// HoloToolkit.Sharing.SyncModel.SyncFloat HoloToolkit.Sharing.SyncModel.SyncQuaternion::w
	SyncFloat_t2189089750 * ___w_13;

public:
	inline static int32_t get_offset_of_x_10() { return static_cast<int32_t>(offsetof(SyncQuaternion_t228159934, ___x_10)); }
	inline SyncFloat_t2189089750 * get_x_10() const { return ___x_10; }
	inline SyncFloat_t2189089750 ** get_address_of_x_10() { return &___x_10; }
	inline void set_x_10(SyncFloat_t2189089750 * value)
	{
		___x_10 = value;
		Il2CppCodeGenWriteBarrier((&___x_10), value);
	}

	inline static int32_t get_offset_of_y_11() { return static_cast<int32_t>(offsetof(SyncQuaternion_t228159934, ___y_11)); }
	inline SyncFloat_t2189089750 * get_y_11() const { return ___y_11; }
	inline SyncFloat_t2189089750 ** get_address_of_y_11() { return &___y_11; }
	inline void set_y_11(SyncFloat_t2189089750 * value)
	{
		___y_11 = value;
		Il2CppCodeGenWriteBarrier((&___y_11), value);
	}

	inline static int32_t get_offset_of_z_12() { return static_cast<int32_t>(offsetof(SyncQuaternion_t228159934, ___z_12)); }
	inline SyncFloat_t2189089750 * get_z_12() const { return ___z_12; }
	inline SyncFloat_t2189089750 ** get_address_of_z_12() { return &___z_12; }
	inline void set_z_12(SyncFloat_t2189089750 * value)
	{
		___z_12 = value;
		Il2CppCodeGenWriteBarrier((&___z_12), value);
	}

	inline static int32_t get_offset_of_w_13() { return static_cast<int32_t>(offsetof(SyncQuaternion_t228159934, ___w_13)); }
	inline SyncFloat_t2189089750 * get_w_13() const { return ___w_13; }
	inline SyncFloat_t2189089750 ** get_address_of_w_13() { return &___w_13; }
	inline void set_w_13(SyncFloat_t2189089750 * value)
	{
		___w_13 = value;
		Il2CppCodeGenWriteBarrier((&___w_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCQUATERNION_T228159934_H
#ifndef SYNCSPAWNEDOBJECT_T3749163513_H
#define SYNCSPAWNEDOBJECT_T3749163513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Spawning.SyncSpawnedObject
struct  SyncSpawnedObject_t3749163513  : public SyncObject_t2866352391
{
public:
	// HoloToolkit.Sharing.SyncModel.SyncTransform HoloToolkit.Sharing.Spawning.SyncSpawnedObject::Transform
	SyncTransform_t4091961955 * ___Transform_10;
	// HoloToolkit.Sharing.SyncModel.SyncString HoloToolkit.Sharing.Spawning.SyncSpawnedObject::Name
	SyncString_t2813399380 * ___Name_11;
	// HoloToolkit.Sharing.SyncModel.SyncString HoloToolkit.Sharing.Spawning.SyncSpawnedObject::ParentPath
	SyncString_t2813399380 * ___ParentPath_12;
	// HoloToolkit.Sharing.SyncModel.SyncString HoloToolkit.Sharing.Spawning.SyncSpawnedObject::ObjectPath
	SyncString_t2813399380 * ___ObjectPath_13;
	// UnityEngine.GameObject HoloToolkit.Sharing.Spawning.SyncSpawnedObject::<GameObject>k__BackingField
	GameObject_t1113636619 * ___U3CGameObjectU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_Transform_10() { return static_cast<int32_t>(offsetof(SyncSpawnedObject_t3749163513, ___Transform_10)); }
	inline SyncTransform_t4091961955 * get_Transform_10() const { return ___Transform_10; }
	inline SyncTransform_t4091961955 ** get_address_of_Transform_10() { return &___Transform_10; }
	inline void set_Transform_10(SyncTransform_t4091961955 * value)
	{
		___Transform_10 = value;
		Il2CppCodeGenWriteBarrier((&___Transform_10), value);
	}

	inline static int32_t get_offset_of_Name_11() { return static_cast<int32_t>(offsetof(SyncSpawnedObject_t3749163513, ___Name_11)); }
	inline SyncString_t2813399380 * get_Name_11() const { return ___Name_11; }
	inline SyncString_t2813399380 ** get_address_of_Name_11() { return &___Name_11; }
	inline void set_Name_11(SyncString_t2813399380 * value)
	{
		___Name_11 = value;
		Il2CppCodeGenWriteBarrier((&___Name_11), value);
	}

	inline static int32_t get_offset_of_ParentPath_12() { return static_cast<int32_t>(offsetof(SyncSpawnedObject_t3749163513, ___ParentPath_12)); }
	inline SyncString_t2813399380 * get_ParentPath_12() const { return ___ParentPath_12; }
	inline SyncString_t2813399380 ** get_address_of_ParentPath_12() { return &___ParentPath_12; }
	inline void set_ParentPath_12(SyncString_t2813399380 * value)
	{
		___ParentPath_12 = value;
		Il2CppCodeGenWriteBarrier((&___ParentPath_12), value);
	}

	inline static int32_t get_offset_of_ObjectPath_13() { return static_cast<int32_t>(offsetof(SyncSpawnedObject_t3749163513, ___ObjectPath_13)); }
	inline SyncString_t2813399380 * get_ObjectPath_13() const { return ___ObjectPath_13; }
	inline SyncString_t2813399380 ** get_address_of_ObjectPath_13() { return &___ObjectPath_13; }
	inline void set_ObjectPath_13(SyncString_t2813399380 * value)
	{
		___ObjectPath_13 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectPath_13), value);
	}

	inline static int32_t get_offset_of_U3CGameObjectU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(SyncSpawnedObject_t3749163513, ___U3CGameObjectU3Ek__BackingField_14)); }
	inline GameObject_t1113636619 * get_U3CGameObjectU3Ek__BackingField_14() const { return ___U3CGameObjectU3Ek__BackingField_14; }
	inline GameObject_t1113636619 ** get_address_of_U3CGameObjectU3Ek__BackingField_14() { return &___U3CGameObjectU3Ek__BackingField_14; }
	inline void set_U3CGameObjectU3Ek__BackingField_14(GameObject_t1113636619 * value)
	{
		___U3CGameObjectU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCSPAWNEDOBJECT_T3749163513_H
#ifndef SYNCVECTOR3_T1671304744_H
#define SYNCVECTOR3_T1671304744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncVector3
struct  SyncVector3_t1671304744  : public SyncObject_t2866352391
{
public:
	// HoloToolkit.Sharing.SyncModel.SyncFloat HoloToolkit.Sharing.SyncModel.SyncVector3::x
	SyncFloat_t2189089750 * ___x_10;
	// HoloToolkit.Sharing.SyncModel.SyncFloat HoloToolkit.Sharing.SyncModel.SyncVector3::y
	SyncFloat_t2189089750 * ___y_11;
	// HoloToolkit.Sharing.SyncModel.SyncFloat HoloToolkit.Sharing.SyncModel.SyncVector3::z
	SyncFloat_t2189089750 * ___z_12;

public:
	inline static int32_t get_offset_of_x_10() { return static_cast<int32_t>(offsetof(SyncVector3_t1671304744, ___x_10)); }
	inline SyncFloat_t2189089750 * get_x_10() const { return ___x_10; }
	inline SyncFloat_t2189089750 ** get_address_of_x_10() { return &___x_10; }
	inline void set_x_10(SyncFloat_t2189089750 * value)
	{
		___x_10 = value;
		Il2CppCodeGenWriteBarrier((&___x_10), value);
	}

	inline static int32_t get_offset_of_y_11() { return static_cast<int32_t>(offsetof(SyncVector3_t1671304744, ___y_11)); }
	inline SyncFloat_t2189089750 * get_y_11() const { return ___y_11; }
	inline SyncFloat_t2189089750 ** get_address_of_y_11() { return &___y_11; }
	inline void set_y_11(SyncFloat_t2189089750 * value)
	{
		___y_11 = value;
		Il2CppCodeGenWriteBarrier((&___y_11), value);
	}

	inline static int32_t get_offset_of_z_12() { return static_cast<int32_t>(offsetof(SyncVector3_t1671304744, ___z_12)); }
	inline SyncFloat_t2189089750 * get_z_12() const { return ___z_12; }
	inline SyncFloat_t2189089750 ** get_address_of_z_12() { return &___z_12; }
	inline void set_z_12(SyncFloat_t2189089750 * value)
	{
		___z_12 = value;
		Il2CppCodeGenWriteBarrier((&___z_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCVECTOR3_T1671304744_H
#ifndef SESSIONTYPE_T1285190767_H
#define SESSIONTYPE_T1285190767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionType
struct  SessionType_t1285190767 
{
public:
	// System.Int32 HoloToolkit.Sharing.SessionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SessionType_t1285190767, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONTYPE_T1285190767_H
#ifndef XSTRING_T940838754_H
#define XSTRING_T940838754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.XString
struct  XString_t940838754  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.XString::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.XString::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(XString_t940838754, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(XString_t940838754, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSTRING_T940838754_H
#ifndef PAIRMAKER_T2569523026_H
#define PAIRMAKER_T2569523026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairMaker
struct  PairMaker_t2569523026  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.PairMaker::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.PairMaker::swigCMemOwn
	bool ___swigCMemOwn_1;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_0 HoloToolkit.Sharing.PairMaker::swigDelegate0
	SwigDelegatePairMaker_0_t3088859200 * ___swigDelegate0_2;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_1 HoloToolkit.Sharing.PairMaker::swigDelegate1
	SwigDelegatePairMaker_1_t3088859199 * ___swigDelegate1_3;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_2 HoloToolkit.Sharing.PairMaker::swigDelegate2
	SwigDelegatePairMaker_2_t3088859202 * ___swigDelegate2_4;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_3 HoloToolkit.Sharing.PairMaker::swigDelegate3
	SwigDelegatePairMaker_3_t3088859201 * ___swigDelegate3_5;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_4 HoloToolkit.Sharing.PairMaker::swigDelegate4
	SwigDelegatePairMaker_4_t3088859196 * ___swigDelegate4_6;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_5 HoloToolkit.Sharing.PairMaker::swigDelegate5
	SwigDelegatePairMaker_5_t3088859195 * ___swigDelegate5_7;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_6 HoloToolkit.Sharing.PairMaker::swigDelegate6
	SwigDelegatePairMaker_6_t3088859198 * ___swigDelegate6_8;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_7 HoloToolkit.Sharing.PairMaker::swigDelegate7
	SwigDelegatePairMaker_7_t3088859197 * ___swigDelegate7_9;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_2() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate0_2)); }
	inline SwigDelegatePairMaker_0_t3088859200 * get_swigDelegate0_2() const { return ___swigDelegate0_2; }
	inline SwigDelegatePairMaker_0_t3088859200 ** get_address_of_swigDelegate0_2() { return &___swigDelegate0_2; }
	inline void set_swigDelegate0_2(SwigDelegatePairMaker_0_t3088859200 * value)
	{
		___swigDelegate0_2 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_2), value);
	}

	inline static int32_t get_offset_of_swigDelegate1_3() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate1_3)); }
	inline SwigDelegatePairMaker_1_t3088859199 * get_swigDelegate1_3() const { return ___swigDelegate1_3; }
	inline SwigDelegatePairMaker_1_t3088859199 ** get_address_of_swigDelegate1_3() { return &___swigDelegate1_3; }
	inline void set_swigDelegate1_3(SwigDelegatePairMaker_1_t3088859199 * value)
	{
		___swigDelegate1_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate1_3), value);
	}

	inline static int32_t get_offset_of_swigDelegate2_4() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate2_4)); }
	inline SwigDelegatePairMaker_2_t3088859202 * get_swigDelegate2_4() const { return ___swigDelegate2_4; }
	inline SwigDelegatePairMaker_2_t3088859202 ** get_address_of_swigDelegate2_4() { return &___swigDelegate2_4; }
	inline void set_swigDelegate2_4(SwigDelegatePairMaker_2_t3088859202 * value)
	{
		___swigDelegate2_4 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate2_4), value);
	}

	inline static int32_t get_offset_of_swigDelegate3_5() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate3_5)); }
	inline SwigDelegatePairMaker_3_t3088859201 * get_swigDelegate3_5() const { return ___swigDelegate3_5; }
	inline SwigDelegatePairMaker_3_t3088859201 ** get_address_of_swigDelegate3_5() { return &___swigDelegate3_5; }
	inline void set_swigDelegate3_5(SwigDelegatePairMaker_3_t3088859201 * value)
	{
		___swigDelegate3_5 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate3_5), value);
	}

	inline static int32_t get_offset_of_swigDelegate4_6() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate4_6)); }
	inline SwigDelegatePairMaker_4_t3088859196 * get_swigDelegate4_6() const { return ___swigDelegate4_6; }
	inline SwigDelegatePairMaker_4_t3088859196 ** get_address_of_swigDelegate4_6() { return &___swigDelegate4_6; }
	inline void set_swigDelegate4_6(SwigDelegatePairMaker_4_t3088859196 * value)
	{
		___swigDelegate4_6 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate4_6), value);
	}

	inline static int32_t get_offset_of_swigDelegate5_7() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate5_7)); }
	inline SwigDelegatePairMaker_5_t3088859195 * get_swigDelegate5_7() const { return ___swigDelegate5_7; }
	inline SwigDelegatePairMaker_5_t3088859195 ** get_address_of_swigDelegate5_7() { return &___swigDelegate5_7; }
	inline void set_swigDelegate5_7(SwigDelegatePairMaker_5_t3088859195 * value)
	{
		___swigDelegate5_7 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate5_7), value);
	}

	inline static int32_t get_offset_of_swigDelegate6_8() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate6_8)); }
	inline SwigDelegatePairMaker_6_t3088859198 * get_swigDelegate6_8() const { return ___swigDelegate6_8; }
	inline SwigDelegatePairMaker_6_t3088859198 ** get_address_of_swigDelegate6_8() { return &___swigDelegate6_8; }
	inline void set_swigDelegate6_8(SwigDelegatePairMaker_6_t3088859198 * value)
	{
		___swigDelegate6_8 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate6_8), value);
	}

	inline static int32_t get_offset_of_swigDelegate7_9() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate7_9)); }
	inline SwigDelegatePairMaker_7_t3088859197 * get_swigDelegate7_9() const { return ___swigDelegate7_9; }
	inline SwigDelegatePairMaker_7_t3088859197 ** get_address_of_swigDelegate7_9() { return &___swigDelegate7_9; }
	inline void set_swigDelegate7_9(SwigDelegatePairMaker_7_t3088859197 * value)
	{
		___swigDelegate7_9 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate7_9), value);
	}
};

struct PairMaker_t2569523026_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_10;
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes1
	TypeU5BU5D_t3940880105* ___swigMethodTypes1_11;
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes2
	TypeU5BU5D_t3940880105* ___swigMethodTypes2_12;
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes3
	TypeU5BU5D_t3940880105* ___swigMethodTypes3_13;
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes4
	TypeU5BU5D_t3940880105* ___swigMethodTypes4_14;
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes5
	TypeU5BU5D_t3940880105* ___swigMethodTypes5_15;
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes6
	TypeU5BU5D_t3940880105* ___swigMethodTypes6_16;
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes7
	TypeU5BU5D_t3940880105* ___swigMethodTypes7_17;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_10() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes0_10)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_10() const { return ___swigMethodTypes0_10; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_10() { return &___swigMethodTypes0_10; }
	inline void set_swigMethodTypes0_10(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_10 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_10), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes1_11() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes1_11)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes1_11() const { return ___swigMethodTypes1_11; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes1_11() { return &___swigMethodTypes1_11; }
	inline void set_swigMethodTypes1_11(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes1_11 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes1_11), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes2_12() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes2_12)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes2_12() const { return ___swigMethodTypes2_12; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes2_12() { return &___swigMethodTypes2_12; }
	inline void set_swigMethodTypes2_12(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes2_12 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes2_12), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes3_13() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes3_13)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes3_13() const { return ___swigMethodTypes3_13; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes3_13() { return &___swigMethodTypes3_13; }
	inline void set_swigMethodTypes3_13(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes3_13 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes3_13), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes4_14() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes4_14)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes4_14() const { return ___swigMethodTypes4_14; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes4_14() { return &___swigMethodTypes4_14; }
	inline void set_swigMethodTypes4_14(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes4_14 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes4_14), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes5_15() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes5_15)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes5_15() const { return ___swigMethodTypes5_15; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes5_15() { return &___swigMethodTypes5_15; }
	inline void set_swigMethodTypes5_15(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes5_15 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes5_15), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes6_16() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes6_16)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes6_16() const { return ___swigMethodTypes6_16; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes6_16() { return &___swigMethodTypes6_16; }
	inline void set_swigMethodTypes6_16(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes6_16 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes6_16), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes7_17() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes7_17)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes7_17() const { return ___swigMethodTypes7_17; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes7_17() { return &___swigMethodTypes7_17; }
	inline void set_swigMethodTypes7_17(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes7_17 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes7_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIRMAKER_T2569523026_H
#ifndef SHARINGMANAGER_T2871218373_H
#define SHARINGMANAGER_T2871218373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SharingManager
struct  SharingManager_t2871218373  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.SharingManager::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.SharingManager::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(SharingManager_t2871218373, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(SharingManager_t2871218373, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARINGMANAGER_T2871218373_H
#ifndef SETTINGS_T4264890459_H
#define SETTINGS_T4264890459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Settings
struct  Settings_t4264890459  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.Settings::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.Settings::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(Settings_t4264890459, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(Settings_t4264890459, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T4264890459_H
#ifndef ELEMENT_T654894041_H
#define ELEMENT_T654894041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Element
struct  Element_t654894041  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.Element::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.Element::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(Element_t654894041, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(Element_t654894041, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_T654894041_H
#ifndef MULTICASTDELEGATE_T157516450_H
#define MULTICASTDELEGATE_T157516450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t157516450  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t157516450, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T157516450_H
#ifndef LISTENER_T2209039043_H
#define LISTENER_T2209039043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Listener
struct  Listener_t2209039043  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.Listener::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.Listener::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(Listener_t2209039043, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(Listener_t2209039043, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTENER_T2209039043_H
#ifndef SESSIONMANAGER_T3810561508_H
#define SESSIONMANAGER_T3810561508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionManager
struct  SessionManager_t3810561508  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.SessionManager::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.SessionManager::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(SessionManager_t3810561508, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(SessionManager_t3810561508, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONMANAGER_T3810561508_H
#ifndef SESSION_T1844742251_H
#define SESSION_T1844742251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Session
struct  Session_t1844742251  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.Session::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.Session::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(Session_t1844742251, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(Session_t1844742251, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSION_T1844742251_H
#ifndef ROOMMANAGER_T2684434268_H
#define ROOMMANAGER_T2684434268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.RoomManager
struct  RoomManager_t2684434268  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.RoomManager::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.RoomManager::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(RoomManager_t2684434268, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(RoomManager_t2684434268, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMMANAGER_T2684434268_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ROOM_T1590920303_H
#define ROOM_T1590920303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Room
struct  Room_t1590920303  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.Room::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.Room::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(Room_t1590920303, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(Room_t1590920303, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOM_T1590920303_H
#ifndef RECEIPT_T1200975798_H
#define RECEIPT_T1200975798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Receipt
struct  Receipt_t1200975798  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.Receipt::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.Receipt::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(Receipt_t1200975798, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(Receipt_t1200975798, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECEIPT_T1200975798_H
#ifndef USER_T790099560_H
#define USER_T790099560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.User
struct  User_t790099560  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.User::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.User::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(User_t790099560, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(User_t790099560, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USER_T790099560_H
#ifndef TAGIMAGE_T1147278201_H
#define TAGIMAGE_T1147278201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.TagImage
struct  TagImage_t1147278201  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.TagImage::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.TagImage::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(TagImage_t1147278201, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(TagImage_t1147278201, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGIMAGE_T1147278201_H
#ifndef LOGWRITER_T3550065378_H
#define LOGWRITER_T3550065378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.LogWriter
struct  LogWriter_t3550065378  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.LogWriter::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.LogWriter::swigCMemOwn
	bool ___swigCMemOwn_1;
	// HoloToolkit.Sharing.LogWriter/SwigDelegateLogWriter_0 HoloToolkit.Sharing.LogWriter::swigDelegate0
	SwigDelegateLogWriter_0_t50445701 * ___swigDelegate0_2;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(LogWriter_t3550065378, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(LogWriter_t3550065378, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_2() { return static_cast<int32_t>(offsetof(LogWriter_t3550065378, ___swigDelegate0_2)); }
	inline SwigDelegateLogWriter_0_t50445701 * get_swigDelegate0_2() const { return ___swigDelegate0_2; }
	inline SwigDelegateLogWriter_0_t50445701 ** get_address_of_swigDelegate0_2() { return &___swigDelegate0_2; }
	inline void set_swigDelegate0_2(SwigDelegateLogWriter_0_t50445701 * value)
	{
		___swigDelegate0_2 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_2), value);
	}
};

struct LogWriter_t3550065378_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.LogWriter::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_3;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_3() { return static_cast<int32_t>(offsetof(LogWriter_t3550065378_StaticFields, ___swigMethodTypes0_3)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_3() const { return ___swigMethodTypes0_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_3() { return &___swigMethodTypes0_3; }
	inline void set_swigMethodTypes0_3(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGWRITER_T3550065378_H
#ifndef USERPRESENCEMANAGER_T3005000763_H
#define USERPRESENCEMANAGER_T3005000763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.UserPresenceManager
struct  UserPresenceManager_t3005000763  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.UserPresenceManager::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.UserPresenceManager::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(UserPresenceManager_t3005000763, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(UserPresenceManager_t3005000763, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERPRESENCEMANAGER_T3005000763_H
#ifndef SWIGDELEGATESESSIONLISTENER_1_T458903626_H
#define SWIGDELEGATESESSIONLISTENER_1_T458903626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionListener/SwigDelegateSessionListener_1
struct  SwigDelegateSessionListener_1_t458903626  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESESSIONLISTENER_1_T458903626_H
#ifndef SWIGDELEGATESESSIONLISTENER_2_T458903623_H
#define SWIGDELEGATESESSIONLISTENER_2_T458903623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionListener/SwigDelegateSessionListener_2
struct  SwigDelegateSessionListener_2_t458903623  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESESSIONLISTENER_2_T458903623_H
#ifndef SWIGDELEGATESESSIONMANAGERLISTENER_4_T2502354954_H
#define SWIGDELEGATESESSIONMANAGERLISTENER_4_T2502354954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_4
struct  SwigDelegateSessionManagerListener_4_t2502354954  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESESSIONMANAGERLISTENER_4_T2502354954_H
#ifndef SWIGDELEGATESESSIONMANAGERLISTENER_3_T580040653_H
#define SWIGDELEGATESESSIONMANAGERLISTENER_3_T580040653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_3
struct  SwigDelegateSessionManagerListener_3_t580040653  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESESSIONMANAGERLISTENER_3_T580040653_H
#ifndef SWIGDELEGATESESSIONMANAGERLISTENER_1_T1742840067_H
#define SWIGDELEGATESESSIONMANAGERLISTENER_1_T1742840067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_1
struct  SwigDelegateSessionManagerListener_1_t1742840067  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESESSIONMANAGERLISTENER_1_T1742840067_H
#ifndef SWIGDELEGATESESSIONMANAGERLISTENER_2_T3308924008_H
#define SWIGDELEGATESESSIONMANAGERLISTENER_2_T3308924008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_2
struct  SwigDelegateSessionManagerListener_2_t3308924008  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESESSIONMANAGERLISTENER_2_T3308924008_H
#ifndef SWIGDELEGATESESSIONMANAGERLISTENER_0_T176756126_H
#define SWIGDELEGATESESSIONMANAGERLISTENER_0_T176756126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_0
struct  SwigDelegateSessionManagerListener_0_t176756126  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESESSIONMANAGERLISTENER_0_T176756126_H
#ifndef SWIGDELEGATESESSIONLISTENER_3_T458903624_H
#define SWIGDELEGATESESSIONLISTENER_3_T458903624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionListener/SwigDelegateSessionListener_3
struct  SwigDelegateSessionListener_3_t458903624  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESESSIONLISTENER_3_T458903624_H
#ifndef SESSIONMANAGERLISTENER_T234187265_H
#define SESSIONMANAGERLISTENER_T234187265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionManagerListener
struct  SessionManagerListener_t234187265  : public Listener_t2209039043
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.SessionManagerListener::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;
	// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_0 HoloToolkit.Sharing.SessionManagerListener::swigDelegate0
	SwigDelegateSessionManagerListener_0_t176756126 * ___swigDelegate0_3;
	// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_1 HoloToolkit.Sharing.SessionManagerListener::swigDelegate1
	SwigDelegateSessionManagerListener_1_t1742840067 * ___swigDelegate1_4;
	// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_2 HoloToolkit.Sharing.SessionManagerListener::swigDelegate2
	SwigDelegateSessionManagerListener_2_t3308924008 * ___swigDelegate2_5;
	// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_3 HoloToolkit.Sharing.SessionManagerListener::swigDelegate3
	SwigDelegateSessionManagerListener_3_t580040653 * ___swigDelegate3_6;
	// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_4 HoloToolkit.Sharing.SessionManagerListener::swigDelegate4
	SwigDelegateSessionManagerListener_4_t2502354954 * ___swigDelegate4_7;
	// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_5 HoloToolkit.Sharing.SessionManagerListener::swigDelegate5
	SwigDelegateSessionManagerListener_5_t4068438895 * ___swigDelegate5_8;
	// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_6 HoloToolkit.Sharing.SessionManagerListener::swigDelegate6
	SwigDelegateSessionManagerListener_6_t1339555540 * ___swigDelegate6_9;
	// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_7 HoloToolkit.Sharing.SessionManagerListener::swigDelegate7
	SwigDelegateSessionManagerListener_7_t2905639481 * ___swigDelegate7_10;
	// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_8 HoloToolkit.Sharing.SessionManagerListener::swigDelegate8
	SwigDelegateSessionManagerListener_8_t532986486 * ___swigDelegate8_11;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_3() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265, ___swigDelegate0_3)); }
	inline SwigDelegateSessionManagerListener_0_t176756126 * get_swigDelegate0_3() const { return ___swigDelegate0_3; }
	inline SwigDelegateSessionManagerListener_0_t176756126 ** get_address_of_swigDelegate0_3() { return &___swigDelegate0_3; }
	inline void set_swigDelegate0_3(SwigDelegateSessionManagerListener_0_t176756126 * value)
	{
		___swigDelegate0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_3), value);
	}

	inline static int32_t get_offset_of_swigDelegate1_4() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265, ___swigDelegate1_4)); }
	inline SwigDelegateSessionManagerListener_1_t1742840067 * get_swigDelegate1_4() const { return ___swigDelegate1_4; }
	inline SwigDelegateSessionManagerListener_1_t1742840067 ** get_address_of_swigDelegate1_4() { return &___swigDelegate1_4; }
	inline void set_swigDelegate1_4(SwigDelegateSessionManagerListener_1_t1742840067 * value)
	{
		___swigDelegate1_4 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate1_4), value);
	}

	inline static int32_t get_offset_of_swigDelegate2_5() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265, ___swigDelegate2_5)); }
	inline SwigDelegateSessionManagerListener_2_t3308924008 * get_swigDelegate2_5() const { return ___swigDelegate2_5; }
	inline SwigDelegateSessionManagerListener_2_t3308924008 ** get_address_of_swigDelegate2_5() { return &___swigDelegate2_5; }
	inline void set_swigDelegate2_5(SwigDelegateSessionManagerListener_2_t3308924008 * value)
	{
		___swigDelegate2_5 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate2_5), value);
	}

	inline static int32_t get_offset_of_swigDelegate3_6() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265, ___swigDelegate3_6)); }
	inline SwigDelegateSessionManagerListener_3_t580040653 * get_swigDelegate3_6() const { return ___swigDelegate3_6; }
	inline SwigDelegateSessionManagerListener_3_t580040653 ** get_address_of_swigDelegate3_6() { return &___swigDelegate3_6; }
	inline void set_swigDelegate3_6(SwigDelegateSessionManagerListener_3_t580040653 * value)
	{
		___swigDelegate3_6 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate3_6), value);
	}

	inline static int32_t get_offset_of_swigDelegate4_7() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265, ___swigDelegate4_7)); }
	inline SwigDelegateSessionManagerListener_4_t2502354954 * get_swigDelegate4_7() const { return ___swigDelegate4_7; }
	inline SwigDelegateSessionManagerListener_4_t2502354954 ** get_address_of_swigDelegate4_7() { return &___swigDelegate4_7; }
	inline void set_swigDelegate4_7(SwigDelegateSessionManagerListener_4_t2502354954 * value)
	{
		___swigDelegate4_7 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate4_7), value);
	}

	inline static int32_t get_offset_of_swigDelegate5_8() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265, ___swigDelegate5_8)); }
	inline SwigDelegateSessionManagerListener_5_t4068438895 * get_swigDelegate5_8() const { return ___swigDelegate5_8; }
	inline SwigDelegateSessionManagerListener_5_t4068438895 ** get_address_of_swigDelegate5_8() { return &___swigDelegate5_8; }
	inline void set_swigDelegate5_8(SwigDelegateSessionManagerListener_5_t4068438895 * value)
	{
		___swigDelegate5_8 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate5_8), value);
	}

	inline static int32_t get_offset_of_swigDelegate6_9() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265, ___swigDelegate6_9)); }
	inline SwigDelegateSessionManagerListener_6_t1339555540 * get_swigDelegate6_9() const { return ___swigDelegate6_9; }
	inline SwigDelegateSessionManagerListener_6_t1339555540 ** get_address_of_swigDelegate6_9() { return &___swigDelegate6_9; }
	inline void set_swigDelegate6_9(SwigDelegateSessionManagerListener_6_t1339555540 * value)
	{
		___swigDelegate6_9 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate6_9), value);
	}

	inline static int32_t get_offset_of_swigDelegate7_10() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265, ___swigDelegate7_10)); }
	inline SwigDelegateSessionManagerListener_7_t2905639481 * get_swigDelegate7_10() const { return ___swigDelegate7_10; }
	inline SwigDelegateSessionManagerListener_7_t2905639481 ** get_address_of_swigDelegate7_10() { return &___swigDelegate7_10; }
	inline void set_swigDelegate7_10(SwigDelegateSessionManagerListener_7_t2905639481 * value)
	{
		___swigDelegate7_10 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate7_10), value);
	}

	inline static int32_t get_offset_of_swigDelegate8_11() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265, ___swigDelegate8_11)); }
	inline SwigDelegateSessionManagerListener_8_t532986486 * get_swigDelegate8_11() const { return ___swigDelegate8_11; }
	inline SwigDelegateSessionManagerListener_8_t532986486 ** get_address_of_swigDelegate8_11() { return &___swigDelegate8_11; }
	inline void set_swigDelegate8_11(SwigDelegateSessionManagerListener_8_t532986486 * value)
	{
		___swigDelegate8_11 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate8_11), value);
	}
};

struct SessionManagerListener_t234187265_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.SessionManagerListener::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_12;
	// System.Type[] HoloToolkit.Sharing.SessionManagerListener::swigMethodTypes1
	TypeU5BU5D_t3940880105* ___swigMethodTypes1_13;
	// System.Type[] HoloToolkit.Sharing.SessionManagerListener::swigMethodTypes2
	TypeU5BU5D_t3940880105* ___swigMethodTypes2_14;
	// System.Type[] HoloToolkit.Sharing.SessionManagerListener::swigMethodTypes3
	TypeU5BU5D_t3940880105* ___swigMethodTypes3_15;
	// System.Type[] HoloToolkit.Sharing.SessionManagerListener::swigMethodTypes4
	TypeU5BU5D_t3940880105* ___swigMethodTypes4_16;
	// System.Type[] HoloToolkit.Sharing.SessionManagerListener::swigMethodTypes5
	TypeU5BU5D_t3940880105* ___swigMethodTypes5_17;
	// System.Type[] HoloToolkit.Sharing.SessionManagerListener::swigMethodTypes6
	TypeU5BU5D_t3940880105* ___swigMethodTypes6_18;
	// System.Type[] HoloToolkit.Sharing.SessionManagerListener::swigMethodTypes7
	TypeU5BU5D_t3940880105* ___swigMethodTypes7_19;
	// System.Type[] HoloToolkit.Sharing.SessionManagerListener::swigMethodTypes8
	TypeU5BU5D_t3940880105* ___swigMethodTypes8_20;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_12() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265_StaticFields, ___swigMethodTypes0_12)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_12() const { return ___swigMethodTypes0_12; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_12() { return &___swigMethodTypes0_12; }
	inline void set_swigMethodTypes0_12(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_12 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_12), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes1_13() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265_StaticFields, ___swigMethodTypes1_13)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes1_13() const { return ___swigMethodTypes1_13; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes1_13() { return &___swigMethodTypes1_13; }
	inline void set_swigMethodTypes1_13(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes1_13 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes1_13), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes2_14() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265_StaticFields, ___swigMethodTypes2_14)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes2_14() const { return ___swigMethodTypes2_14; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes2_14() { return &___swigMethodTypes2_14; }
	inline void set_swigMethodTypes2_14(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes2_14 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes2_14), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes3_15() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265_StaticFields, ___swigMethodTypes3_15)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes3_15() const { return ___swigMethodTypes3_15; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes3_15() { return &___swigMethodTypes3_15; }
	inline void set_swigMethodTypes3_15(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes3_15 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes3_15), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes4_16() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265_StaticFields, ___swigMethodTypes4_16)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes4_16() const { return ___swigMethodTypes4_16; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes4_16() { return &___swigMethodTypes4_16; }
	inline void set_swigMethodTypes4_16(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes4_16 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes4_16), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes5_17() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265_StaticFields, ___swigMethodTypes5_17)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes5_17() const { return ___swigMethodTypes5_17; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes5_17() { return &___swigMethodTypes5_17; }
	inline void set_swigMethodTypes5_17(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes5_17 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes5_17), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes6_18() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265_StaticFields, ___swigMethodTypes6_18)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes6_18() const { return ___swigMethodTypes6_18; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes6_18() { return &___swigMethodTypes6_18; }
	inline void set_swigMethodTypes6_18(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes6_18 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes6_18), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes7_19() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265_StaticFields, ___swigMethodTypes7_19)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes7_19() const { return ___swigMethodTypes7_19; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes7_19() { return &___swigMethodTypes7_19; }
	inline void set_swigMethodTypes7_19(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes7_19 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes7_19), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes8_20() { return static_cast<int32_t>(offsetof(SessionManagerListener_t234187265_StaticFields, ___swigMethodTypes8_20)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes8_20() const { return ___swigMethodTypes8_20; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes8_20() { return &___swigMethodTypes8_20; }
	inline void set_swigMethodTypes8_20(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes8_20 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes8_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONMANAGERLISTENER_T234187265_H
#ifndef SWIGDELEGATEROOMMANAGERLISTENER_0_T3232391369_H
#define SWIGDELEGATEROOMMANAGERLISTENER_0_T3232391369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_0
struct  SwigDelegateRoomManagerListener_0_t3232391369  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEROOMMANAGERLISTENER_0_T3232391369_H
#ifndef SWIGDELEGATEROOMMANAGERLISTENER_1_T3232391370_H
#define SWIGDELEGATEROOMMANAGERLISTENER_1_T3232391370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_1
struct  SwigDelegateRoomManagerListener_1_t3232391370  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEROOMMANAGERLISTENER_1_T3232391370_H
#ifndef SWIGDELEGATEROOMMANAGERLISTENER_2_T3232391367_H
#define SWIGDELEGATEROOMMANAGERLISTENER_2_T3232391367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_2
struct  SwigDelegateRoomManagerListener_2_t3232391367  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEROOMMANAGERLISTENER_2_T3232391367_H
#ifndef SWIGDELEGATESTRINGARRAYLISTENER_2_T1418370590_H
#define SWIGDELEGATESTRINGARRAYLISTENER_2_T1418370590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.StringArrayListener/SwigDelegateStringArrayListener_2
struct  SwigDelegateStringArrayListener_2_t1418370590  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESTRINGARRAYLISTENER_2_T1418370590_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ROOMMANAGERLISTENER_T1172308708_H
#define ROOMMANAGERLISTENER_T1172308708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.RoomManagerListener
struct  RoomManagerListener_t1172308708  : public Listener_t2209039043
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.RoomManagerListener::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;
	// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_0 HoloToolkit.Sharing.RoomManagerListener::swigDelegate0
	SwigDelegateRoomManagerListener_0_t3232391369 * ___swigDelegate0_3;
	// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_1 HoloToolkit.Sharing.RoomManagerListener::swigDelegate1
	SwigDelegateRoomManagerListener_1_t3232391370 * ___swigDelegate1_4;
	// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_2 HoloToolkit.Sharing.RoomManagerListener::swigDelegate2
	SwigDelegateRoomManagerListener_2_t3232391367 * ___swigDelegate2_5;
	// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_3 HoloToolkit.Sharing.RoomManagerListener::swigDelegate3
	SwigDelegateRoomManagerListener_3_t3232391368 * ___swigDelegate3_6;
	// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_4 HoloToolkit.Sharing.RoomManagerListener::swigDelegate4
	SwigDelegateRoomManagerListener_4_t3232391365 * ___swigDelegate4_7;
	// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_5 HoloToolkit.Sharing.RoomManagerListener::swigDelegate5
	SwigDelegateRoomManagerListener_5_t3232391366 * ___swigDelegate5_8;
	// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_6 HoloToolkit.Sharing.RoomManagerListener::swigDelegate6
	SwigDelegateRoomManagerListener_6_t3232391363 * ___swigDelegate6_9;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(RoomManagerListener_t1172308708, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_3() { return static_cast<int32_t>(offsetof(RoomManagerListener_t1172308708, ___swigDelegate0_3)); }
	inline SwigDelegateRoomManagerListener_0_t3232391369 * get_swigDelegate0_3() const { return ___swigDelegate0_3; }
	inline SwigDelegateRoomManagerListener_0_t3232391369 ** get_address_of_swigDelegate0_3() { return &___swigDelegate0_3; }
	inline void set_swigDelegate0_3(SwigDelegateRoomManagerListener_0_t3232391369 * value)
	{
		___swigDelegate0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_3), value);
	}

	inline static int32_t get_offset_of_swigDelegate1_4() { return static_cast<int32_t>(offsetof(RoomManagerListener_t1172308708, ___swigDelegate1_4)); }
	inline SwigDelegateRoomManagerListener_1_t3232391370 * get_swigDelegate1_4() const { return ___swigDelegate1_4; }
	inline SwigDelegateRoomManagerListener_1_t3232391370 ** get_address_of_swigDelegate1_4() { return &___swigDelegate1_4; }
	inline void set_swigDelegate1_4(SwigDelegateRoomManagerListener_1_t3232391370 * value)
	{
		___swigDelegate1_4 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate1_4), value);
	}

	inline static int32_t get_offset_of_swigDelegate2_5() { return static_cast<int32_t>(offsetof(RoomManagerListener_t1172308708, ___swigDelegate2_5)); }
	inline SwigDelegateRoomManagerListener_2_t3232391367 * get_swigDelegate2_5() const { return ___swigDelegate2_5; }
	inline SwigDelegateRoomManagerListener_2_t3232391367 ** get_address_of_swigDelegate2_5() { return &___swigDelegate2_5; }
	inline void set_swigDelegate2_5(SwigDelegateRoomManagerListener_2_t3232391367 * value)
	{
		___swigDelegate2_5 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate2_5), value);
	}

	inline static int32_t get_offset_of_swigDelegate3_6() { return static_cast<int32_t>(offsetof(RoomManagerListener_t1172308708, ___swigDelegate3_6)); }
	inline SwigDelegateRoomManagerListener_3_t3232391368 * get_swigDelegate3_6() const { return ___swigDelegate3_6; }
	inline SwigDelegateRoomManagerListener_3_t3232391368 ** get_address_of_swigDelegate3_6() { return &___swigDelegate3_6; }
	inline void set_swigDelegate3_6(SwigDelegateRoomManagerListener_3_t3232391368 * value)
	{
		___swigDelegate3_6 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate3_6), value);
	}

	inline static int32_t get_offset_of_swigDelegate4_7() { return static_cast<int32_t>(offsetof(RoomManagerListener_t1172308708, ___swigDelegate4_7)); }
	inline SwigDelegateRoomManagerListener_4_t3232391365 * get_swigDelegate4_7() const { return ___swigDelegate4_7; }
	inline SwigDelegateRoomManagerListener_4_t3232391365 ** get_address_of_swigDelegate4_7() { return &___swigDelegate4_7; }
	inline void set_swigDelegate4_7(SwigDelegateRoomManagerListener_4_t3232391365 * value)
	{
		___swigDelegate4_7 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate4_7), value);
	}

	inline static int32_t get_offset_of_swigDelegate5_8() { return static_cast<int32_t>(offsetof(RoomManagerListener_t1172308708, ___swigDelegate5_8)); }
	inline SwigDelegateRoomManagerListener_5_t3232391366 * get_swigDelegate5_8() const { return ___swigDelegate5_8; }
	inline SwigDelegateRoomManagerListener_5_t3232391366 ** get_address_of_swigDelegate5_8() { return &___swigDelegate5_8; }
	inline void set_swigDelegate5_8(SwigDelegateRoomManagerListener_5_t3232391366 * value)
	{
		___swigDelegate5_8 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate5_8), value);
	}

	inline static int32_t get_offset_of_swigDelegate6_9() { return static_cast<int32_t>(offsetof(RoomManagerListener_t1172308708, ___swigDelegate6_9)); }
	inline SwigDelegateRoomManagerListener_6_t3232391363 * get_swigDelegate6_9() const { return ___swigDelegate6_9; }
	inline SwigDelegateRoomManagerListener_6_t3232391363 ** get_address_of_swigDelegate6_9() { return &___swigDelegate6_9; }
	inline void set_swigDelegate6_9(SwigDelegateRoomManagerListener_6_t3232391363 * value)
	{
		___swigDelegate6_9 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate6_9), value);
	}
};

struct RoomManagerListener_t1172308708_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.RoomManagerListener::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_10;
	// System.Type[] HoloToolkit.Sharing.RoomManagerListener::swigMethodTypes1
	TypeU5BU5D_t3940880105* ___swigMethodTypes1_11;
	// System.Type[] HoloToolkit.Sharing.RoomManagerListener::swigMethodTypes2
	TypeU5BU5D_t3940880105* ___swigMethodTypes2_12;
	// System.Type[] HoloToolkit.Sharing.RoomManagerListener::swigMethodTypes3
	TypeU5BU5D_t3940880105* ___swigMethodTypes3_13;
	// System.Type[] HoloToolkit.Sharing.RoomManagerListener::swigMethodTypes4
	TypeU5BU5D_t3940880105* ___swigMethodTypes4_14;
	// System.Type[] HoloToolkit.Sharing.RoomManagerListener::swigMethodTypes5
	TypeU5BU5D_t3940880105* ___swigMethodTypes5_15;
	// System.Type[] HoloToolkit.Sharing.RoomManagerListener::swigMethodTypes6
	TypeU5BU5D_t3940880105* ___swigMethodTypes6_16;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_10() { return static_cast<int32_t>(offsetof(RoomManagerListener_t1172308708_StaticFields, ___swigMethodTypes0_10)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_10() const { return ___swigMethodTypes0_10; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_10() { return &___swigMethodTypes0_10; }
	inline void set_swigMethodTypes0_10(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_10 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_10), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes1_11() { return static_cast<int32_t>(offsetof(RoomManagerListener_t1172308708_StaticFields, ___swigMethodTypes1_11)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes1_11() const { return ___swigMethodTypes1_11; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes1_11() { return &___swigMethodTypes1_11; }
	inline void set_swigMethodTypes1_11(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes1_11 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes1_11), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes2_12() { return static_cast<int32_t>(offsetof(RoomManagerListener_t1172308708_StaticFields, ___swigMethodTypes2_12)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes2_12() const { return ___swigMethodTypes2_12; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes2_12() { return &___swigMethodTypes2_12; }
	inline void set_swigMethodTypes2_12(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes2_12 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes2_12), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes3_13() { return static_cast<int32_t>(offsetof(RoomManagerListener_t1172308708_StaticFields, ___swigMethodTypes3_13)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes3_13() const { return ___swigMethodTypes3_13; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes3_13() { return &___swigMethodTypes3_13; }
	inline void set_swigMethodTypes3_13(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes3_13 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes3_13), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes4_14() { return static_cast<int32_t>(offsetof(RoomManagerListener_t1172308708_StaticFields, ___swigMethodTypes4_14)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes4_14() const { return ___swigMethodTypes4_14; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes4_14() { return &___swigMethodTypes4_14; }
	inline void set_swigMethodTypes4_14(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes4_14 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes4_14), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes5_15() { return static_cast<int32_t>(offsetof(RoomManagerListener_t1172308708_StaticFields, ___swigMethodTypes5_15)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes5_15() const { return ___swigMethodTypes5_15; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes5_15() { return &___swigMethodTypes5_15; }
	inline void set_swigMethodTypes5_15(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes5_15 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes5_15), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes6_16() { return static_cast<int32_t>(offsetof(RoomManagerListener_t1172308708_StaticFields, ___swigMethodTypes6_16)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes6_16() const { return ___swigMethodTypes6_16; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes6_16() { return &___swigMethodTypes6_16; }
	inline void set_swigMethodTypes6_16(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes6_16 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes6_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMMANAGERLISTENER_T1172308708_H
#ifndef SWIGDELEGATEROOMMANAGERLISTENER_6_T3232391363_H
#define SWIGDELEGATEROOMMANAGERLISTENER_6_T3232391363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_6
struct  SwigDelegateRoomManagerListener_6_t3232391363  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEROOMMANAGERLISTENER_6_T3232391363_H
#ifndef SESSIONLISTENER_T1219443984_H
#define SESSIONLISTENER_T1219443984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionListener
struct  SessionListener_t1219443984  : public Listener_t2209039043
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.SessionListener::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;
	// HoloToolkit.Sharing.SessionListener/SwigDelegateSessionListener_0 HoloToolkit.Sharing.SessionListener::swigDelegate0
	SwigDelegateSessionListener_0_t458903625 * ___swigDelegate0_3;
	// HoloToolkit.Sharing.SessionListener/SwigDelegateSessionListener_1 HoloToolkit.Sharing.SessionListener::swigDelegate1
	SwigDelegateSessionListener_1_t458903626 * ___swigDelegate1_4;
	// HoloToolkit.Sharing.SessionListener/SwigDelegateSessionListener_2 HoloToolkit.Sharing.SessionListener::swigDelegate2
	SwigDelegateSessionListener_2_t458903623 * ___swigDelegate2_5;
	// HoloToolkit.Sharing.SessionListener/SwigDelegateSessionListener_3 HoloToolkit.Sharing.SessionListener::swigDelegate3
	SwigDelegateSessionListener_3_t458903624 * ___swigDelegate3_6;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(SessionListener_t1219443984, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_3() { return static_cast<int32_t>(offsetof(SessionListener_t1219443984, ___swigDelegate0_3)); }
	inline SwigDelegateSessionListener_0_t458903625 * get_swigDelegate0_3() const { return ___swigDelegate0_3; }
	inline SwigDelegateSessionListener_0_t458903625 ** get_address_of_swigDelegate0_3() { return &___swigDelegate0_3; }
	inline void set_swigDelegate0_3(SwigDelegateSessionListener_0_t458903625 * value)
	{
		___swigDelegate0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_3), value);
	}

	inline static int32_t get_offset_of_swigDelegate1_4() { return static_cast<int32_t>(offsetof(SessionListener_t1219443984, ___swigDelegate1_4)); }
	inline SwigDelegateSessionListener_1_t458903626 * get_swigDelegate1_4() const { return ___swigDelegate1_4; }
	inline SwigDelegateSessionListener_1_t458903626 ** get_address_of_swigDelegate1_4() { return &___swigDelegate1_4; }
	inline void set_swigDelegate1_4(SwigDelegateSessionListener_1_t458903626 * value)
	{
		___swigDelegate1_4 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate1_4), value);
	}

	inline static int32_t get_offset_of_swigDelegate2_5() { return static_cast<int32_t>(offsetof(SessionListener_t1219443984, ___swigDelegate2_5)); }
	inline SwigDelegateSessionListener_2_t458903623 * get_swigDelegate2_5() const { return ___swigDelegate2_5; }
	inline SwigDelegateSessionListener_2_t458903623 ** get_address_of_swigDelegate2_5() { return &___swigDelegate2_5; }
	inline void set_swigDelegate2_5(SwigDelegateSessionListener_2_t458903623 * value)
	{
		___swigDelegate2_5 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate2_5), value);
	}

	inline static int32_t get_offset_of_swigDelegate3_6() { return static_cast<int32_t>(offsetof(SessionListener_t1219443984, ___swigDelegate3_6)); }
	inline SwigDelegateSessionListener_3_t458903624 * get_swigDelegate3_6() const { return ___swigDelegate3_6; }
	inline SwigDelegateSessionListener_3_t458903624 ** get_address_of_swigDelegate3_6() { return &___swigDelegate3_6; }
	inline void set_swigDelegate3_6(SwigDelegateSessionListener_3_t458903624 * value)
	{
		___swigDelegate3_6 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate3_6), value);
	}
};

struct SessionListener_t1219443984_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.SessionListener::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_7;
	// System.Type[] HoloToolkit.Sharing.SessionListener::swigMethodTypes1
	TypeU5BU5D_t3940880105* ___swigMethodTypes1_8;
	// System.Type[] HoloToolkit.Sharing.SessionListener::swigMethodTypes2
	TypeU5BU5D_t3940880105* ___swigMethodTypes2_9;
	// System.Type[] HoloToolkit.Sharing.SessionListener::swigMethodTypes3
	TypeU5BU5D_t3940880105* ___swigMethodTypes3_10;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_7() { return static_cast<int32_t>(offsetof(SessionListener_t1219443984_StaticFields, ___swigMethodTypes0_7)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_7() const { return ___swigMethodTypes0_7; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_7() { return &___swigMethodTypes0_7; }
	inline void set_swigMethodTypes0_7(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_7 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_7), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes1_8() { return static_cast<int32_t>(offsetof(SessionListener_t1219443984_StaticFields, ___swigMethodTypes1_8)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes1_8() const { return ___swigMethodTypes1_8; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes1_8() { return &___swigMethodTypes1_8; }
	inline void set_swigMethodTypes1_8(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes1_8 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes1_8), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes2_9() { return static_cast<int32_t>(offsetof(SessionListener_t1219443984_StaticFields, ___swigMethodTypes2_9)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes2_9() const { return ___swigMethodTypes2_9; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes2_9() { return &___swigMethodTypes2_9; }
	inline void set_swigMethodTypes2_9(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes2_9 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes2_9), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes3_10() { return static_cast<int32_t>(offsetof(SessionListener_t1219443984_StaticFields, ___swigMethodTypes3_10)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes3_10() const { return ___swigMethodTypes3_10; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes3_10() { return &___swigMethodTypes3_10; }
	inline void set_swigMethodTypes3_10(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes3_10 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes3_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONLISTENER_T1219443984_H
#ifndef SWIGDELEGATESESSIONLISTENER_0_T458903625_H
#define SWIGDELEGATESESSIONLISTENER_0_T458903625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionListener/SwigDelegateSessionListener_0
struct  SwigDelegateSessionListener_0_t458903625  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESESSIONLISTENER_0_T458903625_H
#ifndef SWIGDELEGATEROOMMANAGERLISTENER_3_T3232391368_H
#define SWIGDELEGATEROOMMANAGERLISTENER_3_T3232391368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_3
struct  SwigDelegateRoomManagerListener_3_t3232391368  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEROOMMANAGERLISTENER_3_T3232391368_H
#ifndef SWIGDELEGATEROOMMANAGERLISTENER_4_T3232391365_H
#define SWIGDELEGATEROOMMANAGERLISTENER_4_T3232391365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_4
struct  SwigDelegateRoomManagerListener_4_t3232391365  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEROOMMANAGERLISTENER_4_T3232391365_H
#ifndef SWIGDELEGATEROOMMANAGERLISTENER_5_T3232391366_H
#define SWIGDELEGATEROOMMANAGERLISTENER_5_T3232391366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.RoomManagerListener/SwigDelegateRoomManagerListener_5
struct  SwigDelegateRoomManagerListener_5_t3232391366  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEROOMMANAGERLISTENER_5_T3232391366_H
#ifndef VISUALPAIRRECEIVER_T3311246970_H
#define VISUALPAIRRECEIVER_T3311246970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.VisualPairReceiver
struct  VisualPairReceiver_t3311246970  : public PairMaker_t2569523026
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.VisualPairReceiver::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_18;

public:
	inline static int32_t get_offset_of_swigCPtr_18() { return static_cast<int32_t>(offsetof(VisualPairReceiver_t3311246970, ___swigCPtr_18)); }
	inline HandleRef_t3745784363  get_swigCPtr_18() const { return ___swigCPtr_18; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_18() { return &___swigCPtr_18; }
	inline void set_swigCPtr_18(HandleRef_t3745784363  value)
	{
		___swigCPtr_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISUALPAIRRECEIVER_T3311246970_H
#ifndef VISUALPAIRCONNECTOR_T1673154465_H
#define VISUALPAIRCONNECTOR_T1673154465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.VisualPairConnector
struct  VisualPairConnector_t1673154465  : public PairMaker_t2569523026
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.VisualPairConnector::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_18;

public:
	inline static int32_t get_offset_of_swigCPtr_18() { return static_cast<int32_t>(offsetof(VisualPairConnector_t1673154465, ___swigCPtr_18)); }
	inline HandleRef_t3745784363  get_swigCPtr_18() const { return ___swigCPtr_18; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_18() { return &___swigCPtr_18; }
	inline void set_swigCPtr_18(HandleRef_t3745784363  value)
	{
		___swigCPtr_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISUALPAIRCONNECTOR_T1673154465_H
#ifndef SWIGDELEGATEUSERPRESENCEMANAGERLISTENER_0_T4115417027_H
#define SWIGDELEGATEUSERPRESENCEMANAGERLISTENER_0_T4115417027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.UserPresenceManagerListener/SwigDelegateUserPresenceManagerListener_0
struct  SwigDelegateUserPresenceManagerListener_0_t4115417027  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEUSERPRESENCEMANAGERLISTENER_0_T4115417027_H
#ifndef STRINGARRAYELEMENT_T1419279831_H
#define STRINGARRAYELEMENT_T1419279831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.StringArrayElement
struct  StringArrayElement_t1419279831  : public Element_t654894041
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.StringArrayElement::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(StringArrayElement_t1419279831, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGARRAYELEMENT_T1419279831_H
#ifndef STRINGARRAYLISTENER_T707674793_H
#define STRINGARRAYLISTENER_T707674793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.StringArrayListener
struct  StringArrayListener_t707674793  : public Listener_t2209039043
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.StringArrayListener::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;
	// HoloToolkit.Sharing.StringArrayListener/SwigDelegateStringArrayListener_0 HoloToolkit.Sharing.StringArrayListener::swigDelegate0
	SwigDelegateStringArrayListener_0_t1418370588 * ___swigDelegate0_3;
	// HoloToolkit.Sharing.StringArrayListener/SwigDelegateStringArrayListener_1 HoloToolkit.Sharing.StringArrayListener::swigDelegate1
	SwigDelegateStringArrayListener_1_t1418370589 * ___swigDelegate1_4;
	// HoloToolkit.Sharing.StringArrayListener/SwigDelegateStringArrayListener_2 HoloToolkit.Sharing.StringArrayListener::swigDelegate2
	SwigDelegateStringArrayListener_2_t1418370590 * ___swigDelegate2_5;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(StringArrayListener_t707674793, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_3() { return static_cast<int32_t>(offsetof(StringArrayListener_t707674793, ___swigDelegate0_3)); }
	inline SwigDelegateStringArrayListener_0_t1418370588 * get_swigDelegate0_3() const { return ___swigDelegate0_3; }
	inline SwigDelegateStringArrayListener_0_t1418370588 ** get_address_of_swigDelegate0_3() { return &___swigDelegate0_3; }
	inline void set_swigDelegate0_3(SwigDelegateStringArrayListener_0_t1418370588 * value)
	{
		___swigDelegate0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_3), value);
	}

	inline static int32_t get_offset_of_swigDelegate1_4() { return static_cast<int32_t>(offsetof(StringArrayListener_t707674793, ___swigDelegate1_4)); }
	inline SwigDelegateStringArrayListener_1_t1418370589 * get_swigDelegate1_4() const { return ___swigDelegate1_4; }
	inline SwigDelegateStringArrayListener_1_t1418370589 ** get_address_of_swigDelegate1_4() { return &___swigDelegate1_4; }
	inline void set_swigDelegate1_4(SwigDelegateStringArrayListener_1_t1418370589 * value)
	{
		___swigDelegate1_4 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate1_4), value);
	}

	inline static int32_t get_offset_of_swigDelegate2_5() { return static_cast<int32_t>(offsetof(StringArrayListener_t707674793, ___swigDelegate2_5)); }
	inline SwigDelegateStringArrayListener_2_t1418370590 * get_swigDelegate2_5() const { return ___swigDelegate2_5; }
	inline SwigDelegateStringArrayListener_2_t1418370590 ** get_address_of_swigDelegate2_5() { return &___swigDelegate2_5; }
	inline void set_swigDelegate2_5(SwigDelegateStringArrayListener_2_t1418370590 * value)
	{
		___swigDelegate2_5 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate2_5), value);
	}
};

struct StringArrayListener_t707674793_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.StringArrayListener::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_6;
	// System.Type[] HoloToolkit.Sharing.StringArrayListener::swigMethodTypes1
	TypeU5BU5D_t3940880105* ___swigMethodTypes1_7;
	// System.Type[] HoloToolkit.Sharing.StringArrayListener::swigMethodTypes2
	TypeU5BU5D_t3940880105* ___swigMethodTypes2_8;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_6() { return static_cast<int32_t>(offsetof(StringArrayListener_t707674793_StaticFields, ___swigMethodTypes0_6)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_6() const { return ___swigMethodTypes0_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_6() { return &___swigMethodTypes0_6; }
	inline void set_swigMethodTypes0_6(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_6 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_6), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes1_7() { return static_cast<int32_t>(offsetof(StringArrayListener_t707674793_StaticFields, ___swigMethodTypes1_7)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes1_7() const { return ___swigMethodTypes1_7; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes1_7() { return &___swigMethodTypes1_7; }
	inline void set_swigMethodTypes1_7(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes1_7 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes1_7), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes2_8() { return static_cast<int32_t>(offsetof(StringArrayListener_t707674793_StaticFields, ___swigMethodTypes2_8)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes2_8() const { return ___swigMethodTypes2_8; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes2_8() { return &___swigMethodTypes2_8; }
	inline void set_swigMethodTypes2_8(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes2_8 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes2_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGARRAYLISTENER_T707674793_H
#ifndef SWIGDELEGATESTRINGARRAYLISTENER_0_T1418370588_H
#define SWIGDELEGATESTRINGARRAYLISTENER_0_T1418370588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.StringArrayListener/SwigDelegateStringArrayListener_0
struct  SwigDelegateStringArrayListener_0_t1418370588  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESTRINGARRAYLISTENER_0_T1418370588_H
#ifndef SWIGDELEGATESYNCLISTENER_0_T1307184870_H
#define SWIGDELEGATESYNCLISTENER_0_T1307184870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncListener/SwigDelegateSyncListener_0
struct  SwigDelegateSyncListener_0_t1307184870  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESYNCLISTENER_0_T1307184870_H
#ifndef SYNCLISTENER_T1939120163_H
#define SYNCLISTENER_T1939120163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncListener
struct  SyncListener_t1939120163  : public Listener_t2209039043
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.SyncListener::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;
	// HoloToolkit.Sharing.SyncListener/SwigDelegateSyncListener_0 HoloToolkit.Sharing.SyncListener::swigDelegate0
	SwigDelegateSyncListener_0_t1307184870 * ___swigDelegate0_3;
	// HoloToolkit.Sharing.SyncListener/SwigDelegateSyncListener_1 HoloToolkit.Sharing.SyncListener::swigDelegate1
	SwigDelegateSyncListener_1_t4036068225 * ___swigDelegate1_4;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(SyncListener_t1939120163, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_3() { return static_cast<int32_t>(offsetof(SyncListener_t1939120163, ___swigDelegate0_3)); }
	inline SwigDelegateSyncListener_0_t1307184870 * get_swigDelegate0_3() const { return ___swigDelegate0_3; }
	inline SwigDelegateSyncListener_0_t1307184870 ** get_address_of_swigDelegate0_3() { return &___swigDelegate0_3; }
	inline void set_swigDelegate0_3(SwigDelegateSyncListener_0_t1307184870 * value)
	{
		___swigDelegate0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_3), value);
	}

	inline static int32_t get_offset_of_swigDelegate1_4() { return static_cast<int32_t>(offsetof(SyncListener_t1939120163, ___swigDelegate1_4)); }
	inline SwigDelegateSyncListener_1_t4036068225 * get_swigDelegate1_4() const { return ___swigDelegate1_4; }
	inline SwigDelegateSyncListener_1_t4036068225 ** get_address_of_swigDelegate1_4() { return &___swigDelegate1_4; }
	inline void set_swigDelegate1_4(SwigDelegateSyncListener_1_t4036068225 * value)
	{
		___swigDelegate1_4 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate1_4), value);
	}
};

struct SyncListener_t1939120163_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.SyncListener::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_5;
	// System.Type[] HoloToolkit.Sharing.SyncListener::swigMethodTypes1
	TypeU5BU5D_t3940880105* ___swigMethodTypes1_6;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_5() { return static_cast<int32_t>(offsetof(SyncListener_t1939120163_StaticFields, ___swigMethodTypes0_5)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_5() const { return ___swigMethodTypes0_5; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_5() { return &___swigMethodTypes0_5; }
	inline void set_swigMethodTypes0_5(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_5 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_5), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes1_6() { return static_cast<int32_t>(offsetof(SyncListener_t1939120163_StaticFields, ___swigMethodTypes1_6)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes1_6() const { return ___swigMethodTypes1_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes1_6() { return &___swigMethodTypes1_6; }
	inline void set_swigMethodTypes1_6(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes1_6 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCLISTENER_T1939120163_H
#ifndef STRINGELEMENT_T2038048136_H
#define STRINGELEMENT_T2038048136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.StringElement
struct  StringElement_t2038048136  : public Element_t654894041
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.StringElement::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(StringElement_t2038048136, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGELEMENT_T2038048136_H
#ifndef USERPRESENCEMANAGERLISTENER_T1715939279_H
#define USERPRESENCEMANAGERLISTENER_T1715939279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.UserPresenceManagerListener
struct  UserPresenceManagerListener_t1715939279  : public Listener_t2209039043
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.UserPresenceManagerListener::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;
	// HoloToolkit.Sharing.UserPresenceManagerListener/SwigDelegateUserPresenceManagerListener_0 HoloToolkit.Sharing.UserPresenceManagerListener::swigDelegate0
	SwigDelegateUserPresenceManagerListener_0_t4115417027 * ___swigDelegate0_3;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(UserPresenceManagerListener_t1715939279, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_3() { return static_cast<int32_t>(offsetof(UserPresenceManagerListener_t1715939279, ___swigDelegate0_3)); }
	inline SwigDelegateUserPresenceManagerListener_0_t4115417027 * get_swigDelegate0_3() const { return ___swigDelegate0_3; }
	inline SwigDelegateUserPresenceManagerListener_0_t4115417027 ** get_address_of_swigDelegate0_3() { return &___swigDelegate0_3; }
	inline void set_swigDelegate0_3(SwigDelegateUserPresenceManagerListener_0_t4115417027 * value)
	{
		___swigDelegate0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_3), value);
	}
};

struct UserPresenceManagerListener_t1715939279_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.UserPresenceManagerListener::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_4;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_4() { return static_cast<int32_t>(offsetof(UserPresenceManagerListener_t1715939279_StaticFields, ___swigMethodTypes0_4)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_4() const { return ___swigMethodTypes0_4; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_4() { return &___swigMethodTypes0_4; }
	inline void set_swigMethodTypes0_4(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_4 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERPRESENCEMANAGERLISTENER_T1715939279_H
#ifndef SWIGDELEGATESTRINGARRAYLISTENER_1_T1418370589_H
#define SWIGDELEGATESTRINGARRAYLISTENER_1_T1418370589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.StringArrayListener/SwigDelegateStringArrayListener_1
struct  SwigDelegateStringArrayListener_1_t1418370589  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESTRINGARRAYLISTENER_1_T1418370589_H
#ifndef SWIGDELEGATESYNCLISTENER_1_T4036068225_H
#define SWIGDELEGATESYNCLISTENER_1_T4036068225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncListener/SwigDelegateSyncListener_1
struct  SwigDelegateSyncListener_1_t4036068225  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESYNCLISTENER_1_T4036068225_H
#ifndef EXCEPTIONARGUMENTDELEGATE_T3478888600_H
#define EXCEPTIONARGUMENTDELEGATE_T3478888600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct  ExceptionArgumentDelegate_t3478888600  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONARGUMENTDELEGATE_T3478888600_H
#ifndef EXCEPTIONDELEGATE_T827543389_H
#define EXCEPTIONDELEGATE_T827543389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct  ExceptionDelegate_t827543389  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONDELEGATE_T827543389_H
#ifndef CONSOLELOGWRITER_T525723634_H
#define CONSOLELOGWRITER_T525723634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Utilities.ConsoleLogWriter
struct  ConsoleLogWriter_t525723634  : public LogWriter_t3550065378
{
public:
	// System.Boolean HoloToolkit.Sharing.Utilities.ConsoleLogWriter::ShowDetailedLogs
	bool ___ShowDetailedLogs_4;

public:
	inline static int32_t get_offset_of_ShowDetailedLogs_4() { return static_cast<int32_t>(offsetof(ConsoleLogWriter_t525723634, ___ShowDetailedLogs_4)); }
	inline bool get_ShowDetailedLogs_4() const { return ___ShowDetailedLogs_4; }
	inline bool* get_address_of_ShowDetailedLogs_4() { return &___ShowDetailedLogs_4; }
	inline void set_ShowDetailedLogs_4(bool value)
	{
		___ShowDetailedLogs_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSOLELOGWRITER_T525723634_H
#ifndef SWIGSTRINGDELEGATE_T4212175550_H
#define SWIGSTRINGDELEGATE_T4212175550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SharingClientPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct  SWIGStringDelegate_t4212175550  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGSTRINGDELEGATE_T4212175550_H
#ifndef SWIGDELEGATESESSIONMANAGERLISTENER_6_T1339555540_H
#define SWIGDELEGATESESSIONMANAGERLISTENER_6_T1339555540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_6
struct  SwigDelegateSessionManagerListener_6_t1339555540  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESESSIONMANAGERLISTENER_6_T1339555540_H
#ifndef SWIGDELEGATESESSIONMANAGERLISTENER_5_T4068438895_H
#define SWIGDELEGATESESSIONMANAGERLISTENER_5_T4068438895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_5
struct  SwigDelegateSessionManagerListener_5_t4068438895  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESESSIONMANAGERLISTENER_5_T4068438895_H
#ifndef SWIGDELEGATESESSIONMANAGERLISTENER_7_T2905639481_H
#define SWIGDELEGATESESSIONMANAGERLISTENER_7_T2905639481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_7
struct  SwigDelegateSessionManagerListener_7_t2905639481  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESESSIONMANAGERLISTENER_7_T2905639481_H
#ifndef SWIGDELEGATESESSIONMANAGERLISTENER_8_T532986486_H
#define SWIGDELEGATESESSIONMANAGERLISTENER_8_T532986486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionManagerListener/SwigDelegateSessionManagerListener_8
struct  SwigDelegateSessionManagerListener_8_t532986486  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATESESSIONMANAGERLISTENER_8_T532986486_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ROOMMANAGERADAPTER_T3977794230_H
#define ROOMMANAGERADAPTER_T3977794230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.RoomManagerAdapter
struct  RoomManagerAdapter_t3977794230  : public RoomManagerListener_t1172308708
{
public:
	// System.Action`1<HoloToolkit.Sharing.Room> HoloToolkit.Sharing.RoomManagerAdapter::RoomAddedEvent
	Action_1_t1763387898 * ___RoomAddedEvent_17;
	// System.Action`1<HoloToolkit.Sharing.Room> HoloToolkit.Sharing.RoomManagerAdapter::RoomClosedEvent
	Action_1_t1763387898 * ___RoomClosedEvent_18;
	// System.Action`2<HoloToolkit.Sharing.Room,System.Int32> HoloToolkit.Sharing.RoomManagerAdapter::UserJoinedRoomEvent
	Action_2_t1395259940 * ___UserJoinedRoomEvent_19;
	// System.Action`2<HoloToolkit.Sharing.Room,System.Int32> HoloToolkit.Sharing.RoomManagerAdapter::UserLeftRoomEvent
	Action_2_t1395259940 * ___UserLeftRoomEvent_20;
	// System.Action`1<HoloToolkit.Sharing.Room> HoloToolkit.Sharing.RoomManagerAdapter::AnchorsChangedEvent
	Action_1_t1763387898 * ___AnchorsChangedEvent_21;
	// System.Action`3<System.Boolean,HoloToolkit.Sharing.AnchorDownloadRequest,HoloToolkit.Sharing.XString> HoloToolkit.Sharing.RoomManagerAdapter::AnchorsDownloadedEvent
	Action_3_t1312624934 * ___AnchorsDownloadedEvent_22;
	// System.Action`2<System.Boolean,HoloToolkit.Sharing.XString> HoloToolkit.Sharing.RoomManagerAdapter::AnchorUploadedEvent
	Action_2_t384220295 * ___AnchorUploadedEvent_23;

public:
	inline static int32_t get_offset_of_RoomAddedEvent_17() { return static_cast<int32_t>(offsetof(RoomManagerAdapter_t3977794230, ___RoomAddedEvent_17)); }
	inline Action_1_t1763387898 * get_RoomAddedEvent_17() const { return ___RoomAddedEvent_17; }
	inline Action_1_t1763387898 ** get_address_of_RoomAddedEvent_17() { return &___RoomAddedEvent_17; }
	inline void set_RoomAddedEvent_17(Action_1_t1763387898 * value)
	{
		___RoomAddedEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___RoomAddedEvent_17), value);
	}

	inline static int32_t get_offset_of_RoomClosedEvent_18() { return static_cast<int32_t>(offsetof(RoomManagerAdapter_t3977794230, ___RoomClosedEvent_18)); }
	inline Action_1_t1763387898 * get_RoomClosedEvent_18() const { return ___RoomClosedEvent_18; }
	inline Action_1_t1763387898 ** get_address_of_RoomClosedEvent_18() { return &___RoomClosedEvent_18; }
	inline void set_RoomClosedEvent_18(Action_1_t1763387898 * value)
	{
		___RoomClosedEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___RoomClosedEvent_18), value);
	}

	inline static int32_t get_offset_of_UserJoinedRoomEvent_19() { return static_cast<int32_t>(offsetof(RoomManagerAdapter_t3977794230, ___UserJoinedRoomEvent_19)); }
	inline Action_2_t1395259940 * get_UserJoinedRoomEvent_19() const { return ___UserJoinedRoomEvent_19; }
	inline Action_2_t1395259940 ** get_address_of_UserJoinedRoomEvent_19() { return &___UserJoinedRoomEvent_19; }
	inline void set_UserJoinedRoomEvent_19(Action_2_t1395259940 * value)
	{
		___UserJoinedRoomEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___UserJoinedRoomEvent_19), value);
	}

	inline static int32_t get_offset_of_UserLeftRoomEvent_20() { return static_cast<int32_t>(offsetof(RoomManagerAdapter_t3977794230, ___UserLeftRoomEvent_20)); }
	inline Action_2_t1395259940 * get_UserLeftRoomEvent_20() const { return ___UserLeftRoomEvent_20; }
	inline Action_2_t1395259940 ** get_address_of_UserLeftRoomEvent_20() { return &___UserLeftRoomEvent_20; }
	inline void set_UserLeftRoomEvent_20(Action_2_t1395259940 * value)
	{
		___UserLeftRoomEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___UserLeftRoomEvent_20), value);
	}

	inline static int32_t get_offset_of_AnchorsChangedEvent_21() { return static_cast<int32_t>(offsetof(RoomManagerAdapter_t3977794230, ___AnchorsChangedEvent_21)); }
	inline Action_1_t1763387898 * get_AnchorsChangedEvent_21() const { return ___AnchorsChangedEvent_21; }
	inline Action_1_t1763387898 ** get_address_of_AnchorsChangedEvent_21() { return &___AnchorsChangedEvent_21; }
	inline void set_AnchorsChangedEvent_21(Action_1_t1763387898 * value)
	{
		___AnchorsChangedEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___AnchorsChangedEvent_21), value);
	}

	inline static int32_t get_offset_of_AnchorsDownloadedEvent_22() { return static_cast<int32_t>(offsetof(RoomManagerAdapter_t3977794230, ___AnchorsDownloadedEvent_22)); }
	inline Action_3_t1312624934 * get_AnchorsDownloadedEvent_22() const { return ___AnchorsDownloadedEvent_22; }
	inline Action_3_t1312624934 ** get_address_of_AnchorsDownloadedEvent_22() { return &___AnchorsDownloadedEvent_22; }
	inline void set_AnchorsDownloadedEvent_22(Action_3_t1312624934 * value)
	{
		___AnchorsDownloadedEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___AnchorsDownloadedEvent_22), value);
	}

	inline static int32_t get_offset_of_AnchorUploadedEvent_23() { return static_cast<int32_t>(offsetof(RoomManagerAdapter_t3977794230, ___AnchorUploadedEvent_23)); }
	inline Action_2_t384220295 * get_AnchorUploadedEvent_23() const { return ___AnchorUploadedEvent_23; }
	inline Action_2_t384220295 ** get_address_of_AnchorUploadedEvent_23() { return &___AnchorUploadedEvent_23; }
	inline void set_AnchorUploadedEvent_23(Action_2_t384220295 * value)
	{
		___AnchorUploadedEvent_23 = value;
		Il2CppCodeGenWriteBarrier((&___AnchorUploadedEvent_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMMANAGERADAPTER_T3977794230_H
#ifndef USERPRESENCEMANAGERADAPTER_T1106856923_H
#define USERPRESENCEMANAGERADAPTER_T1106856923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.UserPresenceManagerAdapter
struct  UserPresenceManagerAdapter_t1106856923  : public UserPresenceManagerListener_t1715939279
{
public:
	// System.Action`1<HoloToolkit.Sharing.User> HoloToolkit.Sharing.UserPresenceManagerAdapter::UserPresenceChangedEvent
	Action_1_t962567155 * ___UserPresenceChangedEvent_5;

public:
	inline static int32_t get_offset_of_UserPresenceChangedEvent_5() { return static_cast<int32_t>(offsetof(UserPresenceManagerAdapter_t1106856923, ___UserPresenceChangedEvent_5)); }
	inline Action_1_t962567155 * get_UserPresenceChangedEvent_5() const { return ___UserPresenceChangedEvent_5; }
	inline Action_1_t962567155 ** get_address_of_UserPresenceChangedEvent_5() { return &___UserPresenceChangedEvent_5; }
	inline void set_UserPresenceChangedEvent_5(Action_1_t962567155 * value)
	{
		___UserPresenceChangedEvent_5 = value;
		Il2CppCodeGenWriteBarrier((&___UserPresenceChangedEvent_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERPRESENCEMANAGERADAPTER_T1106856923_H
#ifndef SESSIONMANAGERADAPTER_T2913645974_H
#define SESSIONMANAGERADAPTER_T2913645974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionManagerAdapter
struct  SessionManagerAdapter_t2913645974  : public SessionManagerListener_t234187265
{
public:
	// System.Action`1<HoloToolkit.Sharing.Session> HoloToolkit.Sharing.SessionManagerAdapter::CreateSucceededEvent
	Action_1_t2017209846 * ___CreateSucceededEvent_21;
	// System.Action`1<HoloToolkit.Sharing.XString> HoloToolkit.Sharing.SessionManagerAdapter::CreateFailedEvent
	Action_1_t1113306349 * ___CreateFailedEvent_22;
	// System.Action`1<HoloToolkit.Sharing.Session> HoloToolkit.Sharing.SessionManagerAdapter::SessionAddedEvent
	Action_1_t2017209846 * ___SessionAddedEvent_23;
	// System.Action`1<HoloToolkit.Sharing.Session> HoloToolkit.Sharing.SessionManagerAdapter::SessionClosedEvent
	Action_1_t2017209846 * ___SessionClosedEvent_24;
	// System.Action`2<HoloToolkit.Sharing.Session,HoloToolkit.Sharing.User> HoloToolkit.Sharing.SessionManagerAdapter::UserJoinedSessionEvent
	Action_2_t473560391 * ___UserJoinedSessionEvent_25;
	// System.Action`2<HoloToolkit.Sharing.Session,HoloToolkit.Sharing.User> HoloToolkit.Sharing.SessionManagerAdapter::UserLeftSessionEvent
	Action_2_t473560391 * ___UserLeftSessionEvent_26;
	// System.Action`2<HoloToolkit.Sharing.Session,HoloToolkit.Sharing.User> HoloToolkit.Sharing.SessionManagerAdapter::UserChangedEvent
	Action_2_t473560391 * ___UserChangedEvent_27;
	// System.Action HoloToolkit.Sharing.SessionManagerAdapter::ServerConnectedEvent
	Action_t1264377477 * ___ServerConnectedEvent_28;
	// System.Action HoloToolkit.Sharing.SessionManagerAdapter::ServerDisconnectedEvent
	Action_t1264377477 * ___ServerDisconnectedEvent_29;

public:
	inline static int32_t get_offset_of_CreateSucceededEvent_21() { return static_cast<int32_t>(offsetof(SessionManagerAdapter_t2913645974, ___CreateSucceededEvent_21)); }
	inline Action_1_t2017209846 * get_CreateSucceededEvent_21() const { return ___CreateSucceededEvent_21; }
	inline Action_1_t2017209846 ** get_address_of_CreateSucceededEvent_21() { return &___CreateSucceededEvent_21; }
	inline void set_CreateSucceededEvent_21(Action_1_t2017209846 * value)
	{
		___CreateSucceededEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___CreateSucceededEvent_21), value);
	}

	inline static int32_t get_offset_of_CreateFailedEvent_22() { return static_cast<int32_t>(offsetof(SessionManagerAdapter_t2913645974, ___CreateFailedEvent_22)); }
	inline Action_1_t1113306349 * get_CreateFailedEvent_22() const { return ___CreateFailedEvent_22; }
	inline Action_1_t1113306349 ** get_address_of_CreateFailedEvent_22() { return &___CreateFailedEvent_22; }
	inline void set_CreateFailedEvent_22(Action_1_t1113306349 * value)
	{
		___CreateFailedEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___CreateFailedEvent_22), value);
	}

	inline static int32_t get_offset_of_SessionAddedEvent_23() { return static_cast<int32_t>(offsetof(SessionManagerAdapter_t2913645974, ___SessionAddedEvent_23)); }
	inline Action_1_t2017209846 * get_SessionAddedEvent_23() const { return ___SessionAddedEvent_23; }
	inline Action_1_t2017209846 ** get_address_of_SessionAddedEvent_23() { return &___SessionAddedEvent_23; }
	inline void set_SessionAddedEvent_23(Action_1_t2017209846 * value)
	{
		___SessionAddedEvent_23 = value;
		Il2CppCodeGenWriteBarrier((&___SessionAddedEvent_23), value);
	}

	inline static int32_t get_offset_of_SessionClosedEvent_24() { return static_cast<int32_t>(offsetof(SessionManagerAdapter_t2913645974, ___SessionClosedEvent_24)); }
	inline Action_1_t2017209846 * get_SessionClosedEvent_24() const { return ___SessionClosedEvent_24; }
	inline Action_1_t2017209846 ** get_address_of_SessionClosedEvent_24() { return &___SessionClosedEvent_24; }
	inline void set_SessionClosedEvent_24(Action_1_t2017209846 * value)
	{
		___SessionClosedEvent_24 = value;
		Il2CppCodeGenWriteBarrier((&___SessionClosedEvent_24), value);
	}

	inline static int32_t get_offset_of_UserJoinedSessionEvent_25() { return static_cast<int32_t>(offsetof(SessionManagerAdapter_t2913645974, ___UserJoinedSessionEvent_25)); }
	inline Action_2_t473560391 * get_UserJoinedSessionEvent_25() const { return ___UserJoinedSessionEvent_25; }
	inline Action_2_t473560391 ** get_address_of_UserJoinedSessionEvent_25() { return &___UserJoinedSessionEvent_25; }
	inline void set_UserJoinedSessionEvent_25(Action_2_t473560391 * value)
	{
		___UserJoinedSessionEvent_25 = value;
		Il2CppCodeGenWriteBarrier((&___UserJoinedSessionEvent_25), value);
	}

	inline static int32_t get_offset_of_UserLeftSessionEvent_26() { return static_cast<int32_t>(offsetof(SessionManagerAdapter_t2913645974, ___UserLeftSessionEvent_26)); }
	inline Action_2_t473560391 * get_UserLeftSessionEvent_26() const { return ___UserLeftSessionEvent_26; }
	inline Action_2_t473560391 ** get_address_of_UserLeftSessionEvent_26() { return &___UserLeftSessionEvent_26; }
	inline void set_UserLeftSessionEvent_26(Action_2_t473560391 * value)
	{
		___UserLeftSessionEvent_26 = value;
		Il2CppCodeGenWriteBarrier((&___UserLeftSessionEvent_26), value);
	}

	inline static int32_t get_offset_of_UserChangedEvent_27() { return static_cast<int32_t>(offsetof(SessionManagerAdapter_t2913645974, ___UserChangedEvent_27)); }
	inline Action_2_t473560391 * get_UserChangedEvent_27() const { return ___UserChangedEvent_27; }
	inline Action_2_t473560391 ** get_address_of_UserChangedEvent_27() { return &___UserChangedEvent_27; }
	inline void set_UserChangedEvent_27(Action_2_t473560391 * value)
	{
		___UserChangedEvent_27 = value;
		Il2CppCodeGenWriteBarrier((&___UserChangedEvent_27), value);
	}

	inline static int32_t get_offset_of_ServerConnectedEvent_28() { return static_cast<int32_t>(offsetof(SessionManagerAdapter_t2913645974, ___ServerConnectedEvent_28)); }
	inline Action_t1264377477 * get_ServerConnectedEvent_28() const { return ___ServerConnectedEvent_28; }
	inline Action_t1264377477 ** get_address_of_ServerConnectedEvent_28() { return &___ServerConnectedEvent_28; }
	inline void set_ServerConnectedEvent_28(Action_t1264377477 * value)
	{
		___ServerConnectedEvent_28 = value;
		Il2CppCodeGenWriteBarrier((&___ServerConnectedEvent_28), value);
	}

	inline static int32_t get_offset_of_ServerDisconnectedEvent_29() { return static_cast<int32_t>(offsetof(SessionManagerAdapter_t2913645974, ___ServerDisconnectedEvent_29)); }
	inline Action_t1264377477 * get_ServerDisconnectedEvent_29() const { return ___ServerDisconnectedEvent_29; }
	inline Action_t1264377477 ** get_address_of_ServerDisconnectedEvent_29() { return &___ServerDisconnectedEvent_29; }
	inline void set_ServerDisconnectedEvent_29(Action_t1264377477 * value)
	{
		___ServerDisconnectedEvent_29 = value;
		Il2CppCodeGenWriteBarrier((&___ServerDisconnectedEvent_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONMANAGERADAPTER_T2913645974_H
#ifndef SYNCSTATELISTENER_T778580365_H
#define SYNCSTATELISTENER_T778580365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncStateListener
struct  SyncStateListener_t778580365  : public SyncListener_t1939120163
{
public:
	// System.Action HoloToolkit.Sharing.SyncStateListener::SyncChangesBeginEvent
	Action_t1264377477 * ___SyncChangesBeginEvent_7;
	// System.Action HoloToolkit.Sharing.SyncStateListener::SyncChangesEndEvent
	Action_t1264377477 * ___SyncChangesEndEvent_8;

public:
	inline static int32_t get_offset_of_SyncChangesBeginEvent_7() { return static_cast<int32_t>(offsetof(SyncStateListener_t778580365, ___SyncChangesBeginEvent_7)); }
	inline Action_t1264377477 * get_SyncChangesBeginEvent_7() const { return ___SyncChangesBeginEvent_7; }
	inline Action_t1264377477 ** get_address_of_SyncChangesBeginEvent_7() { return &___SyncChangesBeginEvent_7; }
	inline void set_SyncChangesBeginEvent_7(Action_t1264377477 * value)
	{
		___SyncChangesBeginEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___SyncChangesBeginEvent_7), value);
	}

	inline static int32_t get_offset_of_SyncChangesEndEvent_8() { return static_cast<int32_t>(offsetof(SyncStateListener_t778580365, ___SyncChangesEndEvent_8)); }
	inline Action_t1264377477 * get_SyncChangesEndEvent_8() const { return ___SyncChangesEndEvent_8; }
	inline Action_t1264377477 ** get_address_of_SyncChangesEndEvent_8() { return &___SyncChangesEndEvent_8; }
	inline void set_SyncChangesEndEvent_8(Action_t1264377477 * value)
	{
		___SyncChangesEndEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&___SyncChangesEndEvent_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCSTATELISTENER_T778580365_H
#ifndef SPAWNMANAGER_1_T2442342032_H
#define SPAWNMANAGER_1_T2442342032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Spawning.SpawnManager`1<HoloToolkit.Sharing.Spawning.SyncSpawnedObject>
struct  SpawnManager_1_t2442342032  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Sharing.SharingStage HoloToolkit.Sharing.Spawning.SpawnManager`1::<NetworkManager>k__BackingField
	SharingStage_t1191029027 * ___U3CNetworkManagerU3Ek__BackingField_2;
	// HoloToolkit.Sharing.SyncModel.SyncArray`1<T> HoloToolkit.Sharing.Spawning.SpawnManager`1::SyncSource
	SyncArray_1_t1616759892 * ___SyncSource_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HoloToolkit.Sharing.Spawning.SpawnManager`1::SyncSpawnObjectListInternal
	List_1_t2585711361 * ___SyncSpawnObjectListInternal_4;
	// System.Boolean HoloToolkit.Sharing.Spawning.SpawnManager`1::<IsSpawningObjects>k__BackingField
	bool ___U3CIsSpawningObjectsU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CNetworkManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SpawnManager_1_t2442342032, ___U3CNetworkManagerU3Ek__BackingField_2)); }
	inline SharingStage_t1191029027 * get_U3CNetworkManagerU3Ek__BackingField_2() const { return ___U3CNetworkManagerU3Ek__BackingField_2; }
	inline SharingStage_t1191029027 ** get_address_of_U3CNetworkManagerU3Ek__BackingField_2() { return &___U3CNetworkManagerU3Ek__BackingField_2; }
	inline void set_U3CNetworkManagerU3Ek__BackingField_2(SharingStage_t1191029027 * value)
	{
		___U3CNetworkManagerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNetworkManagerU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_SyncSource_3() { return static_cast<int32_t>(offsetof(SpawnManager_1_t2442342032, ___SyncSource_3)); }
	inline SyncArray_1_t1616759892 * get_SyncSource_3() const { return ___SyncSource_3; }
	inline SyncArray_1_t1616759892 ** get_address_of_SyncSource_3() { return &___SyncSource_3; }
	inline void set_SyncSource_3(SyncArray_1_t1616759892 * value)
	{
		___SyncSource_3 = value;
		Il2CppCodeGenWriteBarrier((&___SyncSource_3), value);
	}

	inline static int32_t get_offset_of_SyncSpawnObjectListInternal_4() { return static_cast<int32_t>(offsetof(SpawnManager_1_t2442342032, ___SyncSpawnObjectListInternal_4)); }
	inline List_1_t2585711361 * get_SyncSpawnObjectListInternal_4() const { return ___SyncSpawnObjectListInternal_4; }
	inline List_1_t2585711361 ** get_address_of_SyncSpawnObjectListInternal_4() { return &___SyncSpawnObjectListInternal_4; }
	inline void set_SyncSpawnObjectListInternal_4(List_1_t2585711361 * value)
	{
		___SyncSpawnObjectListInternal_4 = value;
		Il2CppCodeGenWriteBarrier((&___SyncSpawnObjectListInternal_4), value);
	}

	inline static int32_t get_offset_of_U3CIsSpawningObjectsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SpawnManager_1_t2442342032, ___U3CIsSpawningObjectsU3Ek__BackingField_5)); }
	inline bool get_U3CIsSpawningObjectsU3Ek__BackingField_5() const { return ___U3CIsSpawningObjectsU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsSpawningObjectsU3Ek__BackingField_5() { return &___U3CIsSpawningObjectsU3Ek__BackingField_5; }
	inline void set_U3CIsSpawningObjectsU3Ek__BackingField_5(bool value)
	{
		___U3CIsSpawningObjectsU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWNMANAGER_1_T2442342032_H
#ifndef TRANSFORMSYNCHRONIZER_T999777773_H
#define TRANSFORMSYNCHRONIZER_T999777773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.TransformSynchronizer
struct  TransformSynchronizer_t999777773  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Unity.Vector3Interpolated HoloToolkit.Sharing.TransformSynchronizer::Position
	Vector3Interpolated_t2867391164 * ___Position_2;
	// HoloToolkit.Unity.QuaternionInterpolated HoloToolkit.Sharing.TransformSynchronizer::Rotation
	QuaternionInterpolated_t376031970 * ___Rotation_3;
	// HoloToolkit.Unity.Vector3Interpolated HoloToolkit.Sharing.TransformSynchronizer::Scale
	Vector3Interpolated_t2867391164 * ___Scale_4;
	// HoloToolkit.Sharing.SyncModel.SyncTransform HoloToolkit.Sharing.TransformSynchronizer::transformDataModel
	SyncTransform_t4091961955 * ___transformDataModel_5;

public:
	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(TransformSynchronizer_t999777773, ___Position_2)); }
	inline Vector3Interpolated_t2867391164 * get_Position_2() const { return ___Position_2; }
	inline Vector3Interpolated_t2867391164 ** get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(Vector3Interpolated_t2867391164 * value)
	{
		___Position_2 = value;
		Il2CppCodeGenWriteBarrier((&___Position_2), value);
	}

	inline static int32_t get_offset_of_Rotation_3() { return static_cast<int32_t>(offsetof(TransformSynchronizer_t999777773, ___Rotation_3)); }
	inline QuaternionInterpolated_t376031970 * get_Rotation_3() const { return ___Rotation_3; }
	inline QuaternionInterpolated_t376031970 ** get_address_of_Rotation_3() { return &___Rotation_3; }
	inline void set_Rotation_3(QuaternionInterpolated_t376031970 * value)
	{
		___Rotation_3 = value;
		Il2CppCodeGenWriteBarrier((&___Rotation_3), value);
	}

	inline static int32_t get_offset_of_Scale_4() { return static_cast<int32_t>(offsetof(TransformSynchronizer_t999777773, ___Scale_4)); }
	inline Vector3Interpolated_t2867391164 * get_Scale_4() const { return ___Scale_4; }
	inline Vector3Interpolated_t2867391164 ** get_address_of_Scale_4() { return &___Scale_4; }
	inline void set_Scale_4(Vector3Interpolated_t2867391164 * value)
	{
		___Scale_4 = value;
		Il2CppCodeGenWriteBarrier((&___Scale_4), value);
	}

	inline static int32_t get_offset_of_transformDataModel_5() { return static_cast<int32_t>(offsetof(TransformSynchronizer_t999777773, ___transformDataModel_5)); }
	inline SyncTransform_t4091961955 * get_transformDataModel_5() const { return ___transformDataModel_5; }
	inline SyncTransform_t4091961955 ** get_address_of_transformDataModel_5() { return &___transformDataModel_5; }
	inline void set_transformDataModel_5(SyncTransform_t4091961955 * value)
	{
		___transformDataModel_5 = value;
		Il2CppCodeGenWriteBarrier((&___transformDataModel_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMSYNCHRONIZER_T999777773_H
#ifndef DEFAULTSYNCMODELACCESSOR_T624511954_H
#define DEFAULTSYNCMODELACCESSOR_T624511954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.DefaultSyncModelAccessor
struct  DefaultSyncModelAccessor_t624511954  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Sharing.SyncModel.SyncObject HoloToolkit.Sharing.DefaultSyncModelAccessor::<SyncModel>k__BackingField
	SyncObject_t2866352391 * ___U3CSyncModelU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CSyncModelU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DefaultSyncModelAccessor_t624511954, ___U3CSyncModelU3Ek__BackingField_2)); }
	inline SyncObject_t2866352391 * get_U3CSyncModelU3Ek__BackingField_2() const { return ___U3CSyncModelU3Ek__BackingField_2; }
	inline SyncObject_t2866352391 ** get_address_of_U3CSyncModelU3Ek__BackingField_2() { return &___U3CSyncModelU3Ek__BackingField_2; }
	inline void set_U3CSyncModelU3Ek__BackingField_2(SyncObject_t2866352391 * value)
	{
		___U3CSyncModelU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSyncModelU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSYNCMODELACCESSOR_T624511954_H
#ifndef MICROPHONETRANSMITTER_T285957085_H
#define MICROPHONETRANSMITTER_T285957085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter
struct  MicrophoneTransmitter_t285957085  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Unity.InputModule.MicStream/StreamCategory HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::Streamtype
	int32_t ___Streamtype_2;
	// System.Single HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::InputGain
	float ___InputGain_3;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::ShouldTransmitAudio
	bool ___ShouldTransmitAudio_4;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::Mute
	bool ___Mute_5;
	// UnityEngine.Transform HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::GlobalAnchorTransform
	Transform_t3600365921 * ___GlobalAnchorTransform_6;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::ShowInterPacketTime
	bool ___ShowInterPacketTime_7;
	// System.DateTime HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::timeOfLastPacketSend
	DateTime_t3738529785  ___timeOfLastPacketSend_8;
	// System.Single HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::worstTimeBetweenPackets
	float ___worstTimeBetweenPackets_9;
	// System.Int32 HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::sequenceNumber
	int32_t ___sequenceNumber_10;
	// System.Int32 HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::sampleRateType
	int32_t ___sampleRateType_11;
	// UnityEngine.AudioSource HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::audioSource
	AudioSource_t3935305588 * ___audioSource_12;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::hasServerConnection
	bool ___hasServerConnection_13;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::micStarted
	bool ___micStarted_14;
	// HoloToolkit.Unity.CircularBuffer HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::micBuffer
	CircularBuffer_t1956967364 * ___micBuffer_16;
	// System.Byte[] HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::packetSamples
	ByteU5BU5D_t4116647657* ___packetSamples_17;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::versionPacker
	BitManipulator_t1741666884 * ___versionPacker_18;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::audioStreamCountPacker
	BitManipulator_t1741666884 * ___audioStreamCountPacker_19;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::channelCountPacker
	BitManipulator_t1741666884 * ___channelCountPacker_20;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::sampleRatePacker
	BitManipulator_t1741666884 * ___sampleRatePacker_21;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::sampleTypePacker
	BitManipulator_t1741666884 * ___sampleTypePacker_22;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::sampleCountPacker
	BitManipulator_t1741666884 * ___sampleCountPacker_23;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::codecTypePacker
	BitManipulator_t1741666884 * ___codecTypePacker_24;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::mutePacker
	BitManipulator_t1741666884 * ___mutePacker_25;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::sequenceNumberPacker
	BitManipulator_t1741666884 * ___sequenceNumberPacker_26;
	// System.Threading.Mutex HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::audioDataMutex
	Mutex_t3066672582 * ___audioDataMutex_27;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::HearSelf
	bool ___HearSelf_28;
	// HoloToolkit.Unity.CircularBuffer HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::testCircularBuffer
	CircularBuffer_t1956967364 * ___testCircularBuffer_29;
	// UnityEngine.AudioSource HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::testSource
	AudioSource_t3935305588 * ___testSource_30;
	// UnityEngine.AudioClip HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::TestClip
	AudioClip_t3680889665 * ___TestClip_31;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::SaveTestClip
	bool ___SaveTestClip_32;

public:
	inline static int32_t get_offset_of_Streamtype_2() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___Streamtype_2)); }
	inline int32_t get_Streamtype_2() const { return ___Streamtype_2; }
	inline int32_t* get_address_of_Streamtype_2() { return &___Streamtype_2; }
	inline void set_Streamtype_2(int32_t value)
	{
		___Streamtype_2 = value;
	}

	inline static int32_t get_offset_of_InputGain_3() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___InputGain_3)); }
	inline float get_InputGain_3() const { return ___InputGain_3; }
	inline float* get_address_of_InputGain_3() { return &___InputGain_3; }
	inline void set_InputGain_3(float value)
	{
		___InputGain_3 = value;
	}

	inline static int32_t get_offset_of_ShouldTransmitAudio_4() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___ShouldTransmitAudio_4)); }
	inline bool get_ShouldTransmitAudio_4() const { return ___ShouldTransmitAudio_4; }
	inline bool* get_address_of_ShouldTransmitAudio_4() { return &___ShouldTransmitAudio_4; }
	inline void set_ShouldTransmitAudio_4(bool value)
	{
		___ShouldTransmitAudio_4 = value;
	}

	inline static int32_t get_offset_of_Mute_5() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___Mute_5)); }
	inline bool get_Mute_5() const { return ___Mute_5; }
	inline bool* get_address_of_Mute_5() { return &___Mute_5; }
	inline void set_Mute_5(bool value)
	{
		___Mute_5 = value;
	}

	inline static int32_t get_offset_of_GlobalAnchorTransform_6() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___GlobalAnchorTransform_6)); }
	inline Transform_t3600365921 * get_GlobalAnchorTransform_6() const { return ___GlobalAnchorTransform_6; }
	inline Transform_t3600365921 ** get_address_of_GlobalAnchorTransform_6() { return &___GlobalAnchorTransform_6; }
	inline void set_GlobalAnchorTransform_6(Transform_t3600365921 * value)
	{
		___GlobalAnchorTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___GlobalAnchorTransform_6), value);
	}

	inline static int32_t get_offset_of_ShowInterPacketTime_7() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___ShowInterPacketTime_7)); }
	inline bool get_ShowInterPacketTime_7() const { return ___ShowInterPacketTime_7; }
	inline bool* get_address_of_ShowInterPacketTime_7() { return &___ShowInterPacketTime_7; }
	inline void set_ShowInterPacketTime_7(bool value)
	{
		___ShowInterPacketTime_7 = value;
	}

	inline static int32_t get_offset_of_timeOfLastPacketSend_8() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___timeOfLastPacketSend_8)); }
	inline DateTime_t3738529785  get_timeOfLastPacketSend_8() const { return ___timeOfLastPacketSend_8; }
	inline DateTime_t3738529785 * get_address_of_timeOfLastPacketSend_8() { return &___timeOfLastPacketSend_8; }
	inline void set_timeOfLastPacketSend_8(DateTime_t3738529785  value)
	{
		___timeOfLastPacketSend_8 = value;
	}

	inline static int32_t get_offset_of_worstTimeBetweenPackets_9() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___worstTimeBetweenPackets_9)); }
	inline float get_worstTimeBetweenPackets_9() const { return ___worstTimeBetweenPackets_9; }
	inline float* get_address_of_worstTimeBetweenPackets_9() { return &___worstTimeBetweenPackets_9; }
	inline void set_worstTimeBetweenPackets_9(float value)
	{
		___worstTimeBetweenPackets_9 = value;
	}

	inline static int32_t get_offset_of_sequenceNumber_10() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___sequenceNumber_10)); }
	inline int32_t get_sequenceNumber_10() const { return ___sequenceNumber_10; }
	inline int32_t* get_address_of_sequenceNumber_10() { return &___sequenceNumber_10; }
	inline void set_sequenceNumber_10(int32_t value)
	{
		___sequenceNumber_10 = value;
	}

	inline static int32_t get_offset_of_sampleRateType_11() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___sampleRateType_11)); }
	inline int32_t get_sampleRateType_11() const { return ___sampleRateType_11; }
	inline int32_t* get_address_of_sampleRateType_11() { return &___sampleRateType_11; }
	inline void set_sampleRateType_11(int32_t value)
	{
		___sampleRateType_11 = value;
	}

	inline static int32_t get_offset_of_audioSource_12() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___audioSource_12)); }
	inline AudioSource_t3935305588 * get_audioSource_12() const { return ___audioSource_12; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_12() { return &___audioSource_12; }
	inline void set_audioSource_12(AudioSource_t3935305588 * value)
	{
		___audioSource_12 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_12), value);
	}

	inline static int32_t get_offset_of_hasServerConnection_13() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___hasServerConnection_13)); }
	inline bool get_hasServerConnection_13() const { return ___hasServerConnection_13; }
	inline bool* get_address_of_hasServerConnection_13() { return &___hasServerConnection_13; }
	inline void set_hasServerConnection_13(bool value)
	{
		___hasServerConnection_13 = value;
	}

	inline static int32_t get_offset_of_micStarted_14() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___micStarted_14)); }
	inline bool get_micStarted_14() const { return ___micStarted_14; }
	inline bool* get_address_of_micStarted_14() { return &___micStarted_14; }
	inline void set_micStarted_14(bool value)
	{
		___micStarted_14 = value;
	}

	inline static int32_t get_offset_of_micBuffer_16() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___micBuffer_16)); }
	inline CircularBuffer_t1956967364 * get_micBuffer_16() const { return ___micBuffer_16; }
	inline CircularBuffer_t1956967364 ** get_address_of_micBuffer_16() { return &___micBuffer_16; }
	inline void set_micBuffer_16(CircularBuffer_t1956967364 * value)
	{
		___micBuffer_16 = value;
		Il2CppCodeGenWriteBarrier((&___micBuffer_16), value);
	}

	inline static int32_t get_offset_of_packetSamples_17() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___packetSamples_17)); }
	inline ByteU5BU5D_t4116647657* get_packetSamples_17() const { return ___packetSamples_17; }
	inline ByteU5BU5D_t4116647657** get_address_of_packetSamples_17() { return &___packetSamples_17; }
	inline void set_packetSamples_17(ByteU5BU5D_t4116647657* value)
	{
		___packetSamples_17 = value;
		Il2CppCodeGenWriteBarrier((&___packetSamples_17), value);
	}

	inline static int32_t get_offset_of_versionPacker_18() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___versionPacker_18)); }
	inline BitManipulator_t1741666884 * get_versionPacker_18() const { return ___versionPacker_18; }
	inline BitManipulator_t1741666884 ** get_address_of_versionPacker_18() { return &___versionPacker_18; }
	inline void set_versionPacker_18(BitManipulator_t1741666884 * value)
	{
		___versionPacker_18 = value;
		Il2CppCodeGenWriteBarrier((&___versionPacker_18), value);
	}

	inline static int32_t get_offset_of_audioStreamCountPacker_19() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___audioStreamCountPacker_19)); }
	inline BitManipulator_t1741666884 * get_audioStreamCountPacker_19() const { return ___audioStreamCountPacker_19; }
	inline BitManipulator_t1741666884 ** get_address_of_audioStreamCountPacker_19() { return &___audioStreamCountPacker_19; }
	inline void set_audioStreamCountPacker_19(BitManipulator_t1741666884 * value)
	{
		___audioStreamCountPacker_19 = value;
		Il2CppCodeGenWriteBarrier((&___audioStreamCountPacker_19), value);
	}

	inline static int32_t get_offset_of_channelCountPacker_20() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___channelCountPacker_20)); }
	inline BitManipulator_t1741666884 * get_channelCountPacker_20() const { return ___channelCountPacker_20; }
	inline BitManipulator_t1741666884 ** get_address_of_channelCountPacker_20() { return &___channelCountPacker_20; }
	inline void set_channelCountPacker_20(BitManipulator_t1741666884 * value)
	{
		___channelCountPacker_20 = value;
		Il2CppCodeGenWriteBarrier((&___channelCountPacker_20), value);
	}

	inline static int32_t get_offset_of_sampleRatePacker_21() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___sampleRatePacker_21)); }
	inline BitManipulator_t1741666884 * get_sampleRatePacker_21() const { return ___sampleRatePacker_21; }
	inline BitManipulator_t1741666884 ** get_address_of_sampleRatePacker_21() { return &___sampleRatePacker_21; }
	inline void set_sampleRatePacker_21(BitManipulator_t1741666884 * value)
	{
		___sampleRatePacker_21 = value;
		Il2CppCodeGenWriteBarrier((&___sampleRatePacker_21), value);
	}

	inline static int32_t get_offset_of_sampleTypePacker_22() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___sampleTypePacker_22)); }
	inline BitManipulator_t1741666884 * get_sampleTypePacker_22() const { return ___sampleTypePacker_22; }
	inline BitManipulator_t1741666884 ** get_address_of_sampleTypePacker_22() { return &___sampleTypePacker_22; }
	inline void set_sampleTypePacker_22(BitManipulator_t1741666884 * value)
	{
		___sampleTypePacker_22 = value;
		Il2CppCodeGenWriteBarrier((&___sampleTypePacker_22), value);
	}

	inline static int32_t get_offset_of_sampleCountPacker_23() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___sampleCountPacker_23)); }
	inline BitManipulator_t1741666884 * get_sampleCountPacker_23() const { return ___sampleCountPacker_23; }
	inline BitManipulator_t1741666884 ** get_address_of_sampleCountPacker_23() { return &___sampleCountPacker_23; }
	inline void set_sampleCountPacker_23(BitManipulator_t1741666884 * value)
	{
		___sampleCountPacker_23 = value;
		Il2CppCodeGenWriteBarrier((&___sampleCountPacker_23), value);
	}

	inline static int32_t get_offset_of_codecTypePacker_24() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___codecTypePacker_24)); }
	inline BitManipulator_t1741666884 * get_codecTypePacker_24() const { return ___codecTypePacker_24; }
	inline BitManipulator_t1741666884 ** get_address_of_codecTypePacker_24() { return &___codecTypePacker_24; }
	inline void set_codecTypePacker_24(BitManipulator_t1741666884 * value)
	{
		___codecTypePacker_24 = value;
		Il2CppCodeGenWriteBarrier((&___codecTypePacker_24), value);
	}

	inline static int32_t get_offset_of_mutePacker_25() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___mutePacker_25)); }
	inline BitManipulator_t1741666884 * get_mutePacker_25() const { return ___mutePacker_25; }
	inline BitManipulator_t1741666884 ** get_address_of_mutePacker_25() { return &___mutePacker_25; }
	inline void set_mutePacker_25(BitManipulator_t1741666884 * value)
	{
		___mutePacker_25 = value;
		Il2CppCodeGenWriteBarrier((&___mutePacker_25), value);
	}

	inline static int32_t get_offset_of_sequenceNumberPacker_26() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___sequenceNumberPacker_26)); }
	inline BitManipulator_t1741666884 * get_sequenceNumberPacker_26() const { return ___sequenceNumberPacker_26; }
	inline BitManipulator_t1741666884 ** get_address_of_sequenceNumberPacker_26() { return &___sequenceNumberPacker_26; }
	inline void set_sequenceNumberPacker_26(BitManipulator_t1741666884 * value)
	{
		___sequenceNumberPacker_26 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceNumberPacker_26), value);
	}

	inline static int32_t get_offset_of_audioDataMutex_27() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___audioDataMutex_27)); }
	inline Mutex_t3066672582 * get_audioDataMutex_27() const { return ___audioDataMutex_27; }
	inline Mutex_t3066672582 ** get_address_of_audioDataMutex_27() { return &___audioDataMutex_27; }
	inline void set_audioDataMutex_27(Mutex_t3066672582 * value)
	{
		___audioDataMutex_27 = value;
		Il2CppCodeGenWriteBarrier((&___audioDataMutex_27), value);
	}

	inline static int32_t get_offset_of_HearSelf_28() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___HearSelf_28)); }
	inline bool get_HearSelf_28() const { return ___HearSelf_28; }
	inline bool* get_address_of_HearSelf_28() { return &___HearSelf_28; }
	inline void set_HearSelf_28(bool value)
	{
		___HearSelf_28 = value;
	}

	inline static int32_t get_offset_of_testCircularBuffer_29() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___testCircularBuffer_29)); }
	inline CircularBuffer_t1956967364 * get_testCircularBuffer_29() const { return ___testCircularBuffer_29; }
	inline CircularBuffer_t1956967364 ** get_address_of_testCircularBuffer_29() { return &___testCircularBuffer_29; }
	inline void set_testCircularBuffer_29(CircularBuffer_t1956967364 * value)
	{
		___testCircularBuffer_29 = value;
		Il2CppCodeGenWriteBarrier((&___testCircularBuffer_29), value);
	}

	inline static int32_t get_offset_of_testSource_30() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___testSource_30)); }
	inline AudioSource_t3935305588 * get_testSource_30() const { return ___testSource_30; }
	inline AudioSource_t3935305588 ** get_address_of_testSource_30() { return &___testSource_30; }
	inline void set_testSource_30(AudioSource_t3935305588 * value)
	{
		___testSource_30 = value;
		Il2CppCodeGenWriteBarrier((&___testSource_30), value);
	}

	inline static int32_t get_offset_of_TestClip_31() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___TestClip_31)); }
	inline AudioClip_t3680889665 * get_TestClip_31() const { return ___TestClip_31; }
	inline AudioClip_t3680889665 ** get_address_of_TestClip_31() { return &___TestClip_31; }
	inline void set_TestClip_31(AudioClip_t3680889665 * value)
	{
		___TestClip_31 = value;
		Il2CppCodeGenWriteBarrier((&___TestClip_31), value);
	}

	inline static int32_t get_offset_of_SaveTestClip_32() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t285957085, ___SaveTestClip_32)); }
	inline bool get_SaveTestClip_32() const { return ___SaveTestClip_32; }
	inline bool* get_address_of_SaveTestClip_32() { return &___SaveTestClip_32; }
	inline void set_SaveTestClip_32(bool value)
	{
		___SaveTestClip_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROPHONETRANSMITTER_T285957085_H
#ifndef SINGLETON_1_T3132114229_H
#define SINGLETON_1_T3132114229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.WorldAnchorManager>
struct  Singleton_1_t3132114229  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t3132114229_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	WorldAnchorManager_t2731889775 * ___instance_2;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t3132114229_StaticFields, ___instance_2)); }
	inline WorldAnchorManager_t2731889775 * get_instance_2() const { return ___instance_2; }
	inline WorldAnchorManager_t2731889775 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(WorldAnchorManager_t2731889775 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_searchForInstance_3() { return static_cast<int32_t>(offsetof(Singleton_1_t3132114229_StaticFields, ___searchForInstance_3)); }
	inline bool get_searchForInstance_3() const { return ___searchForInstance_3; }
	inline bool* get_address_of_searchForInstance_3() { return &___searchForInstance_3; }
	inline void set_searchForInstance_3(bool value)
	{
		___searchForInstance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T3132114229_H
#ifndef MICROPHONERECEIVER_T1621290537_H
#define MICROPHONERECEIVER_T1621290537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver
struct  MicrophoneReceiver_t1621290537  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::versionExtractor
	BitManipulator_t1741666884 * ___versionExtractor_2;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::audioStreamCountExtractor
	BitManipulator_t1741666884 * ___audioStreamCountExtractor_3;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::channelCountExtractor
	BitManipulator_t1741666884 * ___channelCountExtractor_4;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::sampleRateExtractor
	BitManipulator_t1741666884 * ___sampleRateExtractor_5;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::sampleTypeExtractor
	BitManipulator_t1741666884 * ___sampleTypeExtractor_6;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::sampleCountExtractor
	BitManipulator_t1741666884 * ___sampleCountExtractor_7;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::codecTypeExtractor
	BitManipulator_t1741666884 * ___codecTypeExtractor_8;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::sequenceNumberExtractor
	BitManipulator_t1741666884 * ___sequenceNumberExtractor_9;
	// UnityEngine.Transform HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::GlobalAnchorTransform
	Transform_t3600365921 * ___GlobalAnchorTransform_10;
	// System.Int32 HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::prominentSpeakerCount
	int32_t ___prominentSpeakerCount_12;
	// HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver/ProminentSpeakerInfo[] HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::prominentSpeakerList
	ProminentSpeakerInfoU5BU5D_t3587787525* ___prominentSpeakerList_13;
	// HoloToolkit.Sharing.NetworkConnectionAdapter HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::listener
	NetworkConnectionAdapter_t239632668 * ___listener_14;
	// System.Threading.Mutex HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::audioDataMutex
	Mutex_t3066672582 * ___audioDataMutex_15;
	// System.Single HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::DropOffMaximumMetres
	float ___DropOffMaximumMetres_18;
	// System.Single HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::PanMaximumMetres
	float ___PanMaximumMetres_19;
	// System.Single HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::MinimumDistance
	float ___MinimumDistance_20;
	// System.Byte[] HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::networkPacketBufferBytes
	ByteU5BU5D_t4116647657* ___networkPacketBufferBytes_21;
	// HoloToolkit.Unity.CircularBuffer HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::circularBuffer
	CircularBuffer_t1956967364 * ___circularBuffer_22;
	// HoloToolkit.Unity.CircularBuffer HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::testCircularBuffer
	CircularBuffer_t1956967364 * ___testCircularBuffer_23;
	// UnityEngine.AudioSource HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::testSource
	AudioSource_t3935305588 * ___testSource_24;
	// UnityEngine.AudioClip HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::TestClip
	AudioClip_t3680889665 * ___TestClip_25;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::SaveTestClip
	bool ___SaveTestClip_26;

public:
	inline static int32_t get_offset_of_versionExtractor_2() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___versionExtractor_2)); }
	inline BitManipulator_t1741666884 * get_versionExtractor_2() const { return ___versionExtractor_2; }
	inline BitManipulator_t1741666884 ** get_address_of_versionExtractor_2() { return &___versionExtractor_2; }
	inline void set_versionExtractor_2(BitManipulator_t1741666884 * value)
	{
		___versionExtractor_2 = value;
		Il2CppCodeGenWriteBarrier((&___versionExtractor_2), value);
	}

	inline static int32_t get_offset_of_audioStreamCountExtractor_3() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___audioStreamCountExtractor_3)); }
	inline BitManipulator_t1741666884 * get_audioStreamCountExtractor_3() const { return ___audioStreamCountExtractor_3; }
	inline BitManipulator_t1741666884 ** get_address_of_audioStreamCountExtractor_3() { return &___audioStreamCountExtractor_3; }
	inline void set_audioStreamCountExtractor_3(BitManipulator_t1741666884 * value)
	{
		___audioStreamCountExtractor_3 = value;
		Il2CppCodeGenWriteBarrier((&___audioStreamCountExtractor_3), value);
	}

	inline static int32_t get_offset_of_channelCountExtractor_4() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___channelCountExtractor_4)); }
	inline BitManipulator_t1741666884 * get_channelCountExtractor_4() const { return ___channelCountExtractor_4; }
	inline BitManipulator_t1741666884 ** get_address_of_channelCountExtractor_4() { return &___channelCountExtractor_4; }
	inline void set_channelCountExtractor_4(BitManipulator_t1741666884 * value)
	{
		___channelCountExtractor_4 = value;
		Il2CppCodeGenWriteBarrier((&___channelCountExtractor_4), value);
	}

	inline static int32_t get_offset_of_sampleRateExtractor_5() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___sampleRateExtractor_5)); }
	inline BitManipulator_t1741666884 * get_sampleRateExtractor_5() const { return ___sampleRateExtractor_5; }
	inline BitManipulator_t1741666884 ** get_address_of_sampleRateExtractor_5() { return &___sampleRateExtractor_5; }
	inline void set_sampleRateExtractor_5(BitManipulator_t1741666884 * value)
	{
		___sampleRateExtractor_5 = value;
		Il2CppCodeGenWriteBarrier((&___sampleRateExtractor_5), value);
	}

	inline static int32_t get_offset_of_sampleTypeExtractor_6() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___sampleTypeExtractor_6)); }
	inline BitManipulator_t1741666884 * get_sampleTypeExtractor_6() const { return ___sampleTypeExtractor_6; }
	inline BitManipulator_t1741666884 ** get_address_of_sampleTypeExtractor_6() { return &___sampleTypeExtractor_6; }
	inline void set_sampleTypeExtractor_6(BitManipulator_t1741666884 * value)
	{
		___sampleTypeExtractor_6 = value;
		Il2CppCodeGenWriteBarrier((&___sampleTypeExtractor_6), value);
	}

	inline static int32_t get_offset_of_sampleCountExtractor_7() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___sampleCountExtractor_7)); }
	inline BitManipulator_t1741666884 * get_sampleCountExtractor_7() const { return ___sampleCountExtractor_7; }
	inline BitManipulator_t1741666884 ** get_address_of_sampleCountExtractor_7() { return &___sampleCountExtractor_7; }
	inline void set_sampleCountExtractor_7(BitManipulator_t1741666884 * value)
	{
		___sampleCountExtractor_7 = value;
		Il2CppCodeGenWriteBarrier((&___sampleCountExtractor_7), value);
	}

	inline static int32_t get_offset_of_codecTypeExtractor_8() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___codecTypeExtractor_8)); }
	inline BitManipulator_t1741666884 * get_codecTypeExtractor_8() const { return ___codecTypeExtractor_8; }
	inline BitManipulator_t1741666884 ** get_address_of_codecTypeExtractor_8() { return &___codecTypeExtractor_8; }
	inline void set_codecTypeExtractor_8(BitManipulator_t1741666884 * value)
	{
		___codecTypeExtractor_8 = value;
		Il2CppCodeGenWriteBarrier((&___codecTypeExtractor_8), value);
	}

	inline static int32_t get_offset_of_sequenceNumberExtractor_9() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___sequenceNumberExtractor_9)); }
	inline BitManipulator_t1741666884 * get_sequenceNumberExtractor_9() const { return ___sequenceNumberExtractor_9; }
	inline BitManipulator_t1741666884 ** get_address_of_sequenceNumberExtractor_9() { return &___sequenceNumberExtractor_9; }
	inline void set_sequenceNumberExtractor_9(BitManipulator_t1741666884 * value)
	{
		___sequenceNumberExtractor_9 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceNumberExtractor_9), value);
	}

	inline static int32_t get_offset_of_GlobalAnchorTransform_10() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___GlobalAnchorTransform_10)); }
	inline Transform_t3600365921 * get_GlobalAnchorTransform_10() const { return ___GlobalAnchorTransform_10; }
	inline Transform_t3600365921 ** get_address_of_GlobalAnchorTransform_10() { return &___GlobalAnchorTransform_10; }
	inline void set_GlobalAnchorTransform_10(Transform_t3600365921 * value)
	{
		___GlobalAnchorTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___GlobalAnchorTransform_10), value);
	}

	inline static int32_t get_offset_of_prominentSpeakerCount_12() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___prominentSpeakerCount_12)); }
	inline int32_t get_prominentSpeakerCount_12() const { return ___prominentSpeakerCount_12; }
	inline int32_t* get_address_of_prominentSpeakerCount_12() { return &___prominentSpeakerCount_12; }
	inline void set_prominentSpeakerCount_12(int32_t value)
	{
		___prominentSpeakerCount_12 = value;
	}

	inline static int32_t get_offset_of_prominentSpeakerList_13() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___prominentSpeakerList_13)); }
	inline ProminentSpeakerInfoU5BU5D_t3587787525* get_prominentSpeakerList_13() const { return ___prominentSpeakerList_13; }
	inline ProminentSpeakerInfoU5BU5D_t3587787525** get_address_of_prominentSpeakerList_13() { return &___prominentSpeakerList_13; }
	inline void set_prominentSpeakerList_13(ProminentSpeakerInfoU5BU5D_t3587787525* value)
	{
		___prominentSpeakerList_13 = value;
		Il2CppCodeGenWriteBarrier((&___prominentSpeakerList_13), value);
	}

	inline static int32_t get_offset_of_listener_14() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___listener_14)); }
	inline NetworkConnectionAdapter_t239632668 * get_listener_14() const { return ___listener_14; }
	inline NetworkConnectionAdapter_t239632668 ** get_address_of_listener_14() { return &___listener_14; }
	inline void set_listener_14(NetworkConnectionAdapter_t239632668 * value)
	{
		___listener_14 = value;
		Il2CppCodeGenWriteBarrier((&___listener_14), value);
	}

	inline static int32_t get_offset_of_audioDataMutex_15() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___audioDataMutex_15)); }
	inline Mutex_t3066672582 * get_audioDataMutex_15() const { return ___audioDataMutex_15; }
	inline Mutex_t3066672582 ** get_address_of_audioDataMutex_15() { return &___audioDataMutex_15; }
	inline void set_audioDataMutex_15(Mutex_t3066672582 * value)
	{
		___audioDataMutex_15 = value;
		Il2CppCodeGenWriteBarrier((&___audioDataMutex_15), value);
	}

	inline static int32_t get_offset_of_DropOffMaximumMetres_18() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___DropOffMaximumMetres_18)); }
	inline float get_DropOffMaximumMetres_18() const { return ___DropOffMaximumMetres_18; }
	inline float* get_address_of_DropOffMaximumMetres_18() { return &___DropOffMaximumMetres_18; }
	inline void set_DropOffMaximumMetres_18(float value)
	{
		___DropOffMaximumMetres_18 = value;
	}

	inline static int32_t get_offset_of_PanMaximumMetres_19() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___PanMaximumMetres_19)); }
	inline float get_PanMaximumMetres_19() const { return ___PanMaximumMetres_19; }
	inline float* get_address_of_PanMaximumMetres_19() { return &___PanMaximumMetres_19; }
	inline void set_PanMaximumMetres_19(float value)
	{
		___PanMaximumMetres_19 = value;
	}

	inline static int32_t get_offset_of_MinimumDistance_20() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___MinimumDistance_20)); }
	inline float get_MinimumDistance_20() const { return ___MinimumDistance_20; }
	inline float* get_address_of_MinimumDistance_20() { return &___MinimumDistance_20; }
	inline void set_MinimumDistance_20(float value)
	{
		___MinimumDistance_20 = value;
	}

	inline static int32_t get_offset_of_networkPacketBufferBytes_21() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___networkPacketBufferBytes_21)); }
	inline ByteU5BU5D_t4116647657* get_networkPacketBufferBytes_21() const { return ___networkPacketBufferBytes_21; }
	inline ByteU5BU5D_t4116647657** get_address_of_networkPacketBufferBytes_21() { return &___networkPacketBufferBytes_21; }
	inline void set_networkPacketBufferBytes_21(ByteU5BU5D_t4116647657* value)
	{
		___networkPacketBufferBytes_21 = value;
		Il2CppCodeGenWriteBarrier((&___networkPacketBufferBytes_21), value);
	}

	inline static int32_t get_offset_of_circularBuffer_22() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___circularBuffer_22)); }
	inline CircularBuffer_t1956967364 * get_circularBuffer_22() const { return ___circularBuffer_22; }
	inline CircularBuffer_t1956967364 ** get_address_of_circularBuffer_22() { return &___circularBuffer_22; }
	inline void set_circularBuffer_22(CircularBuffer_t1956967364 * value)
	{
		___circularBuffer_22 = value;
		Il2CppCodeGenWriteBarrier((&___circularBuffer_22), value);
	}

	inline static int32_t get_offset_of_testCircularBuffer_23() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___testCircularBuffer_23)); }
	inline CircularBuffer_t1956967364 * get_testCircularBuffer_23() const { return ___testCircularBuffer_23; }
	inline CircularBuffer_t1956967364 ** get_address_of_testCircularBuffer_23() { return &___testCircularBuffer_23; }
	inline void set_testCircularBuffer_23(CircularBuffer_t1956967364 * value)
	{
		___testCircularBuffer_23 = value;
		Il2CppCodeGenWriteBarrier((&___testCircularBuffer_23), value);
	}

	inline static int32_t get_offset_of_testSource_24() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___testSource_24)); }
	inline AudioSource_t3935305588 * get_testSource_24() const { return ___testSource_24; }
	inline AudioSource_t3935305588 ** get_address_of_testSource_24() { return &___testSource_24; }
	inline void set_testSource_24(AudioSource_t3935305588 * value)
	{
		___testSource_24 = value;
		Il2CppCodeGenWriteBarrier((&___testSource_24), value);
	}

	inline static int32_t get_offset_of_TestClip_25() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___TestClip_25)); }
	inline AudioClip_t3680889665 * get_TestClip_25() const { return ___TestClip_25; }
	inline AudioClip_t3680889665 ** get_address_of_TestClip_25() { return &___TestClip_25; }
	inline void set_TestClip_25(AudioClip_t3680889665 * value)
	{
		___TestClip_25 = value;
		Il2CppCodeGenWriteBarrier((&___TestClip_25), value);
	}

	inline static int32_t get_offset_of_SaveTestClip_26() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_t1621290537, ___SaveTestClip_26)); }
	inline bool get_SaveTestClip_26() const { return ___SaveTestClip_26; }
	inline bool* get_address_of_SaveTestClip_26() { return &___SaveTestClip_26; }
	inline void set_SaveTestClip_26(bool value)
	{
		___SaveTestClip_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROPHONERECEIVER_T1621290537_H
#ifndef DIRECTPAIRING_T1147775817_H
#define DIRECTPAIRING_T1147775817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Utilities.DirectPairing
struct  DirectPairing_t1147775817  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Sharing.Utilities.DirectPairing/Role HoloToolkit.Sharing.Utilities.DirectPairing::PairingRole
	int32_t ___PairingRole_2;
	// System.String HoloToolkit.Sharing.Utilities.DirectPairing::RemoteAddress
	String_t* ___RemoteAddress_3;
	// System.UInt16 HoloToolkit.Sharing.Utilities.DirectPairing::RemotePort
	uint16_t ___RemotePort_4;
	// System.UInt16 HoloToolkit.Sharing.Utilities.DirectPairing::LocalPort
	uint16_t ___LocalPort_5;
	// System.Boolean HoloToolkit.Sharing.Utilities.DirectPairing::AutoReconnect
	bool ___AutoReconnect_6;
	// HoloToolkit.Sharing.SharingManager HoloToolkit.Sharing.Utilities.DirectPairing::sharingMgr
	SharingManager_t2871218373 * ___sharingMgr_7;
	// HoloToolkit.Sharing.PairMaker HoloToolkit.Sharing.Utilities.DirectPairing::pairMaker
	PairMaker_t2569523026 * ___pairMaker_8;
	// HoloToolkit.Sharing.PairingAdapter HoloToolkit.Sharing.Utilities.DirectPairing::pairingAdapter
	PairingAdapter_t2261856171 * ___pairingAdapter_9;
	// HoloToolkit.Sharing.NetworkConnectionAdapter HoloToolkit.Sharing.Utilities.DirectPairing::connectionAdapter
	NetworkConnectionAdapter_t239632668 * ___connectionAdapter_10;

public:
	inline static int32_t get_offset_of_PairingRole_2() { return static_cast<int32_t>(offsetof(DirectPairing_t1147775817, ___PairingRole_2)); }
	inline int32_t get_PairingRole_2() const { return ___PairingRole_2; }
	inline int32_t* get_address_of_PairingRole_2() { return &___PairingRole_2; }
	inline void set_PairingRole_2(int32_t value)
	{
		___PairingRole_2 = value;
	}

	inline static int32_t get_offset_of_RemoteAddress_3() { return static_cast<int32_t>(offsetof(DirectPairing_t1147775817, ___RemoteAddress_3)); }
	inline String_t* get_RemoteAddress_3() const { return ___RemoteAddress_3; }
	inline String_t** get_address_of_RemoteAddress_3() { return &___RemoteAddress_3; }
	inline void set_RemoteAddress_3(String_t* value)
	{
		___RemoteAddress_3 = value;
		Il2CppCodeGenWriteBarrier((&___RemoteAddress_3), value);
	}

	inline static int32_t get_offset_of_RemotePort_4() { return static_cast<int32_t>(offsetof(DirectPairing_t1147775817, ___RemotePort_4)); }
	inline uint16_t get_RemotePort_4() const { return ___RemotePort_4; }
	inline uint16_t* get_address_of_RemotePort_4() { return &___RemotePort_4; }
	inline void set_RemotePort_4(uint16_t value)
	{
		___RemotePort_4 = value;
	}

	inline static int32_t get_offset_of_LocalPort_5() { return static_cast<int32_t>(offsetof(DirectPairing_t1147775817, ___LocalPort_5)); }
	inline uint16_t get_LocalPort_5() const { return ___LocalPort_5; }
	inline uint16_t* get_address_of_LocalPort_5() { return &___LocalPort_5; }
	inline void set_LocalPort_5(uint16_t value)
	{
		___LocalPort_5 = value;
	}

	inline static int32_t get_offset_of_AutoReconnect_6() { return static_cast<int32_t>(offsetof(DirectPairing_t1147775817, ___AutoReconnect_6)); }
	inline bool get_AutoReconnect_6() const { return ___AutoReconnect_6; }
	inline bool* get_address_of_AutoReconnect_6() { return &___AutoReconnect_6; }
	inline void set_AutoReconnect_6(bool value)
	{
		___AutoReconnect_6 = value;
	}

	inline static int32_t get_offset_of_sharingMgr_7() { return static_cast<int32_t>(offsetof(DirectPairing_t1147775817, ___sharingMgr_7)); }
	inline SharingManager_t2871218373 * get_sharingMgr_7() const { return ___sharingMgr_7; }
	inline SharingManager_t2871218373 ** get_address_of_sharingMgr_7() { return &___sharingMgr_7; }
	inline void set_sharingMgr_7(SharingManager_t2871218373 * value)
	{
		___sharingMgr_7 = value;
		Il2CppCodeGenWriteBarrier((&___sharingMgr_7), value);
	}

	inline static int32_t get_offset_of_pairMaker_8() { return static_cast<int32_t>(offsetof(DirectPairing_t1147775817, ___pairMaker_8)); }
	inline PairMaker_t2569523026 * get_pairMaker_8() const { return ___pairMaker_8; }
	inline PairMaker_t2569523026 ** get_address_of_pairMaker_8() { return &___pairMaker_8; }
	inline void set_pairMaker_8(PairMaker_t2569523026 * value)
	{
		___pairMaker_8 = value;
		Il2CppCodeGenWriteBarrier((&___pairMaker_8), value);
	}

	inline static int32_t get_offset_of_pairingAdapter_9() { return static_cast<int32_t>(offsetof(DirectPairing_t1147775817, ___pairingAdapter_9)); }
	inline PairingAdapter_t2261856171 * get_pairingAdapter_9() const { return ___pairingAdapter_9; }
	inline PairingAdapter_t2261856171 ** get_address_of_pairingAdapter_9() { return &___pairingAdapter_9; }
	inline void set_pairingAdapter_9(PairingAdapter_t2261856171 * value)
	{
		___pairingAdapter_9 = value;
		Il2CppCodeGenWriteBarrier((&___pairingAdapter_9), value);
	}

	inline static int32_t get_offset_of_connectionAdapter_10() { return static_cast<int32_t>(offsetof(DirectPairing_t1147775817, ___connectionAdapter_10)); }
	inline NetworkConnectionAdapter_t239632668 * get_connectionAdapter_10() const { return ___connectionAdapter_10; }
	inline NetworkConnectionAdapter_t239632668 ** get_address_of_connectionAdapter_10() { return &___connectionAdapter_10; }
	inline void set_connectionAdapter_10(NetworkConnectionAdapter_t239632668 * value)
	{
		___connectionAdapter_10 = value;
		Il2CppCodeGenWriteBarrier((&___connectionAdapter_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTPAIRING_T1147775817_H
#ifndef SINGLETON_1_T1591253481_H
#define SINGLETON_1_T1591253481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Sharing.SharingStage>
struct  Singleton_1_t1591253481  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t1591253481_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	SharingStage_t1191029027 * ___instance_2;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1591253481_StaticFields, ___instance_2)); }
	inline SharingStage_t1191029027 * get_instance_2() const { return ___instance_2; }
	inline SharingStage_t1191029027 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(SharingStage_t1191029027 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_searchForInstance_3() { return static_cast<int32_t>(offsetof(Singleton_1_t1591253481_StaticFields, ___searchForInstance_3)); }
	inline bool get_searchForInstance_3() const { return ___searchForInstance_3; }
	inline bool* get_address_of_searchForInstance_3() { return &___searchForInstance_3; }
	inline void set_searchForInstance_3(bool value)
	{
		___searchForInstance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1591253481_H
#ifndef MANUALIPCONFIGURATION_T1673208871_H
#define MANUALIPCONFIGURATION_T1673208871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Utilities.ManualIpConfiguration
struct  ManualIpConfiguration_t1673208871  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean HoloToolkit.Sharing.Utilities.ManualIpConfiguration::HideWhenConnected
	bool ___HideWhenConnected_3;
	// System.Single HoloToolkit.Sharing.Utilities.ManualIpConfiguration::HideAfterSeconds
	float ___HideAfterSeconds_4;
	// System.Int32 HoloToolkit.Sharing.Utilities.ManualIpConfiguration::Timeout
	int32_t ___Timeout_5;
	// UnityEngine.UI.Text HoloToolkit.Sharing.Utilities.ManualIpConfiguration::ipAddress
	Text_t1901882714 * ___ipAddress_6;
	// UnityEngine.UI.Image HoloToolkit.Sharing.Utilities.ManualIpConfiguration::connectionIndicator
	Image_t2670269651 * ___connectionIndicator_7;
	// System.Boolean HoloToolkit.Sharing.Utilities.ManualIpConfiguration::timerRunning
	bool ___timerRunning_8;
	// System.Single HoloToolkit.Sharing.Utilities.ManualIpConfiguration::timer
	float ___timer_9;
	// System.Boolean HoloToolkit.Sharing.Utilities.ManualIpConfiguration::isTryingToConnect
	bool ___isTryingToConnect_10;
	// System.Boolean HoloToolkit.Sharing.Utilities.ManualIpConfiguration::firstRun
	bool ___firstRun_11;

public:
	inline static int32_t get_offset_of_HideWhenConnected_3() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_t1673208871, ___HideWhenConnected_3)); }
	inline bool get_HideWhenConnected_3() const { return ___HideWhenConnected_3; }
	inline bool* get_address_of_HideWhenConnected_3() { return &___HideWhenConnected_3; }
	inline void set_HideWhenConnected_3(bool value)
	{
		___HideWhenConnected_3 = value;
	}

	inline static int32_t get_offset_of_HideAfterSeconds_4() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_t1673208871, ___HideAfterSeconds_4)); }
	inline float get_HideAfterSeconds_4() const { return ___HideAfterSeconds_4; }
	inline float* get_address_of_HideAfterSeconds_4() { return &___HideAfterSeconds_4; }
	inline void set_HideAfterSeconds_4(float value)
	{
		___HideAfterSeconds_4 = value;
	}

	inline static int32_t get_offset_of_Timeout_5() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_t1673208871, ___Timeout_5)); }
	inline int32_t get_Timeout_5() const { return ___Timeout_5; }
	inline int32_t* get_address_of_Timeout_5() { return &___Timeout_5; }
	inline void set_Timeout_5(int32_t value)
	{
		___Timeout_5 = value;
	}

	inline static int32_t get_offset_of_ipAddress_6() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_t1673208871, ___ipAddress_6)); }
	inline Text_t1901882714 * get_ipAddress_6() const { return ___ipAddress_6; }
	inline Text_t1901882714 ** get_address_of_ipAddress_6() { return &___ipAddress_6; }
	inline void set_ipAddress_6(Text_t1901882714 * value)
	{
		___ipAddress_6 = value;
		Il2CppCodeGenWriteBarrier((&___ipAddress_6), value);
	}

	inline static int32_t get_offset_of_connectionIndicator_7() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_t1673208871, ___connectionIndicator_7)); }
	inline Image_t2670269651 * get_connectionIndicator_7() const { return ___connectionIndicator_7; }
	inline Image_t2670269651 ** get_address_of_connectionIndicator_7() { return &___connectionIndicator_7; }
	inline void set_connectionIndicator_7(Image_t2670269651 * value)
	{
		___connectionIndicator_7 = value;
		Il2CppCodeGenWriteBarrier((&___connectionIndicator_7), value);
	}

	inline static int32_t get_offset_of_timerRunning_8() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_t1673208871, ___timerRunning_8)); }
	inline bool get_timerRunning_8() const { return ___timerRunning_8; }
	inline bool* get_address_of_timerRunning_8() { return &___timerRunning_8; }
	inline void set_timerRunning_8(bool value)
	{
		___timerRunning_8 = value;
	}

	inline static int32_t get_offset_of_timer_9() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_t1673208871, ___timer_9)); }
	inline float get_timer_9() const { return ___timer_9; }
	inline float* get_address_of_timer_9() { return &___timer_9; }
	inline void set_timer_9(float value)
	{
		___timer_9 = value;
	}

	inline static int32_t get_offset_of_isTryingToConnect_10() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_t1673208871, ___isTryingToConnect_10)); }
	inline bool get_isTryingToConnect_10() const { return ___isTryingToConnect_10; }
	inline bool* get_address_of_isTryingToConnect_10() { return &___isTryingToConnect_10; }
	inline void set_isTryingToConnect_10(bool value)
	{
		___isTryingToConnect_10 = value;
	}

	inline static int32_t get_offset_of_firstRun_11() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_t1673208871, ___firstRun_11)); }
	inline bool get_firstRun_11() const { return ___firstRun_11; }
	inline bool* get_address_of_firstRun_11() { return &___firstRun_11; }
	inline void set_firstRun_11(bool value)
	{
		___firstRun_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANUALIPCONFIGURATION_T1673208871_H
#ifndef SINGLETON_1_T879905920_H
#define SINGLETON_1_T879905920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom>
struct  Singleton_1_t879905920  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t879905920_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	AutoJoinSessionAndRoom_t479681466 * ___instance_2;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t879905920_StaticFields, ___instance_2)); }
	inline AutoJoinSessionAndRoom_t479681466 * get_instance_2() const { return ___instance_2; }
	inline AutoJoinSessionAndRoom_t479681466 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(AutoJoinSessionAndRoom_t479681466 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_searchForInstance_3() { return static_cast<int32_t>(offsetof(Singleton_1_t879905920_StaticFields, ___searchForInstance_3)); }
	inline bool get_searchForInstance_3() const { return ___searchForInstance_3; }
	inline bool* get_address_of_searchForInstance_3() { return &___searchForInstance_3; }
	inline void set_searchForInstance_3(bool value)
	{
		___searchForInstance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T879905920_H
#ifndef WORLDANCHORMANAGER_T2731889775_H
#define WORLDANCHORMANAGER_T2731889775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.WorldAnchorManager
struct  WorldAnchorManager_t2731889775  : public Singleton_1_t3132114229
{
public:
	// UnityEngine.TextMesh HoloToolkit.Unity.WorldAnchorManager::AnchorDebugText
	TextMesh_t1536577757 * ___AnchorDebugText_4;
	// System.Boolean HoloToolkit.Unity.WorldAnchorManager::ShowDetailedLogs
	bool ___ShowDetailedLogs_5;
	// System.Boolean HoloToolkit.Unity.WorldAnchorManager::PersistentAnchors
	bool ___PersistentAnchors_6;
	// System.Collections.Generic.Queue`1<HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo> HoloToolkit.Unity.WorldAnchorManager::LocalAnchorOperations
	Queue_1_t2234784041 * ___LocalAnchorOperations_7;
	// UnityEngine.XR.WSA.Persistence.WorldAnchorStore HoloToolkit.Unity.WorldAnchorManager::<AnchorStore>k__BackingField
	WorldAnchorStore_t633400888 * ___U3CAnchorStoreU3Ek__BackingField_8;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> HoloToolkit.Unity.WorldAnchorManager::AnchorGameObjectReferenceList
	Dictionary_2_t898892918 * ___AnchorGameObjectReferenceList_9;

public:
	inline static int32_t get_offset_of_AnchorDebugText_4() { return static_cast<int32_t>(offsetof(WorldAnchorManager_t2731889775, ___AnchorDebugText_4)); }
	inline TextMesh_t1536577757 * get_AnchorDebugText_4() const { return ___AnchorDebugText_4; }
	inline TextMesh_t1536577757 ** get_address_of_AnchorDebugText_4() { return &___AnchorDebugText_4; }
	inline void set_AnchorDebugText_4(TextMesh_t1536577757 * value)
	{
		___AnchorDebugText_4 = value;
		Il2CppCodeGenWriteBarrier((&___AnchorDebugText_4), value);
	}

	inline static int32_t get_offset_of_ShowDetailedLogs_5() { return static_cast<int32_t>(offsetof(WorldAnchorManager_t2731889775, ___ShowDetailedLogs_5)); }
	inline bool get_ShowDetailedLogs_5() const { return ___ShowDetailedLogs_5; }
	inline bool* get_address_of_ShowDetailedLogs_5() { return &___ShowDetailedLogs_5; }
	inline void set_ShowDetailedLogs_5(bool value)
	{
		___ShowDetailedLogs_5 = value;
	}

	inline static int32_t get_offset_of_PersistentAnchors_6() { return static_cast<int32_t>(offsetof(WorldAnchorManager_t2731889775, ___PersistentAnchors_6)); }
	inline bool get_PersistentAnchors_6() const { return ___PersistentAnchors_6; }
	inline bool* get_address_of_PersistentAnchors_6() { return &___PersistentAnchors_6; }
	inline void set_PersistentAnchors_6(bool value)
	{
		___PersistentAnchors_6 = value;
	}

	inline static int32_t get_offset_of_LocalAnchorOperations_7() { return static_cast<int32_t>(offsetof(WorldAnchorManager_t2731889775, ___LocalAnchorOperations_7)); }
	inline Queue_1_t2234784041 * get_LocalAnchorOperations_7() const { return ___LocalAnchorOperations_7; }
	inline Queue_1_t2234784041 ** get_address_of_LocalAnchorOperations_7() { return &___LocalAnchorOperations_7; }
	inline void set_LocalAnchorOperations_7(Queue_1_t2234784041 * value)
	{
		___LocalAnchorOperations_7 = value;
		Il2CppCodeGenWriteBarrier((&___LocalAnchorOperations_7), value);
	}

	inline static int32_t get_offset_of_U3CAnchorStoreU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(WorldAnchorManager_t2731889775, ___U3CAnchorStoreU3Ek__BackingField_8)); }
	inline WorldAnchorStore_t633400888 * get_U3CAnchorStoreU3Ek__BackingField_8() const { return ___U3CAnchorStoreU3Ek__BackingField_8; }
	inline WorldAnchorStore_t633400888 ** get_address_of_U3CAnchorStoreU3Ek__BackingField_8() { return &___U3CAnchorStoreU3Ek__BackingField_8; }
	inline void set_U3CAnchorStoreU3Ek__BackingField_8(WorldAnchorStore_t633400888 * value)
	{
		___U3CAnchorStoreU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnchorStoreU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_AnchorGameObjectReferenceList_9() { return static_cast<int32_t>(offsetof(WorldAnchorManager_t2731889775, ___AnchorGameObjectReferenceList_9)); }
	inline Dictionary_2_t898892918 * get_AnchorGameObjectReferenceList_9() const { return ___AnchorGameObjectReferenceList_9; }
	inline Dictionary_2_t898892918 ** get_address_of_AnchorGameObjectReferenceList_9() { return &___AnchorGameObjectReferenceList_9; }
	inline void set_AnchorGameObjectReferenceList_9(Dictionary_2_t898892918 * value)
	{
		___AnchorGameObjectReferenceList_9 = value;
		Il2CppCodeGenWriteBarrier((&___AnchorGameObjectReferenceList_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDANCHORMANAGER_T2731889775_H
#ifndef PREFABSPAWNMANAGER_T3856760055_H
#define PREFABSPAWNMANAGER_T3856760055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Spawning.PrefabSpawnManager
struct  PrefabSpawnManager_t3856760055  : public SpawnManager_1_t2442342032
{
public:
	// System.Collections.Generic.List`1<HoloToolkit.Sharing.Spawning.PrefabToDataModel> HoloToolkit.Sharing.Spawning.PrefabSpawnManager::spawnablePrefabs
	List_1_t3234377962 * ___spawnablePrefabs_6;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> HoloToolkit.Sharing.Spawning.PrefabSpawnManager::typeToPrefab
	Dictionary_2_t898892918 * ___typeToPrefab_7;
	// System.Int32 HoloToolkit.Sharing.Spawning.PrefabSpawnManager::objectCreationCounter
	int32_t ___objectCreationCounter_8;

public:
	inline static int32_t get_offset_of_spawnablePrefabs_6() { return static_cast<int32_t>(offsetof(PrefabSpawnManager_t3856760055, ___spawnablePrefabs_6)); }
	inline List_1_t3234377962 * get_spawnablePrefabs_6() const { return ___spawnablePrefabs_6; }
	inline List_1_t3234377962 ** get_address_of_spawnablePrefabs_6() { return &___spawnablePrefabs_6; }
	inline void set_spawnablePrefabs_6(List_1_t3234377962 * value)
	{
		___spawnablePrefabs_6 = value;
		Il2CppCodeGenWriteBarrier((&___spawnablePrefabs_6), value);
	}

	inline static int32_t get_offset_of_typeToPrefab_7() { return static_cast<int32_t>(offsetof(PrefabSpawnManager_t3856760055, ___typeToPrefab_7)); }
	inline Dictionary_2_t898892918 * get_typeToPrefab_7() const { return ___typeToPrefab_7; }
	inline Dictionary_2_t898892918 ** get_address_of_typeToPrefab_7() { return &___typeToPrefab_7; }
	inline void set_typeToPrefab_7(Dictionary_2_t898892918 * value)
	{
		___typeToPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___typeToPrefab_7), value);
	}

	inline static int32_t get_offset_of_objectCreationCounter_8() { return static_cast<int32_t>(offsetof(PrefabSpawnManager_t3856760055, ___objectCreationCounter_8)); }
	inline int32_t get_objectCreationCounter_8() const { return ___objectCreationCounter_8; }
	inline int32_t* get_address_of_objectCreationCounter_8() { return &___objectCreationCounter_8; }
	inline void set_objectCreationCounter_8(int32_t value)
	{
		___objectCreationCounter_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABSPAWNMANAGER_T3856760055_H
#ifndef SHARINGSTAGE_T1191029027_H
#define SHARINGSTAGE_T1191029027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SharingStage
struct  SharingStage_t1191029027  : public Singleton_1_t1591253481
{
public:
	// System.EventHandler HoloToolkit.Sharing.SharingStage::SharingManagerConnected
	EventHandler_t1348719766 * ___SharingManagerConnected_4;
	// System.EventHandler HoloToolkit.Sharing.SharingStage::SharingManagerDisconnected
	EventHandler_t1348719766 * ___SharingManagerDisconnected_5;
	// HoloToolkit.Sharing.ClientRole HoloToolkit.Sharing.SharingStage::ClientRole
	int32_t ___ClientRole_7;
	// System.String HoloToolkit.Sharing.SharingStage::ServerAddress
	String_t* ___ServerAddress_8;
	// System.String HoloToolkit.Sharing.SharingStage::defaultSessionName
	String_t* ___defaultSessionName_9;
	// System.String HoloToolkit.Sharing.SharingStage::defaultRoomName
	String_t* ___defaultRoomName_10;
	// System.Boolean HoloToolkit.Sharing.SharingStage::KeepRoomAlive
	bool ___KeepRoomAlive_11;
	// System.Int32 HoloToolkit.Sharing.SharingStage::ServerPort
	int32_t ___ServerPort_12;
	// System.Boolean HoloToolkit.Sharing.SharingStage::connectOnAwake
	bool ___connectOnAwake_13;
	// HoloToolkit.Sharing.SharingManager HoloToolkit.Sharing.SharingStage::<Manager>k__BackingField
	SharingManager_t2871218373 * ___U3CManagerU3Ek__BackingField_14;
	// System.Boolean HoloToolkit.Sharing.SharingStage::AutoDiscoverServer
	bool ___AutoDiscoverServer_15;
	// System.Single HoloToolkit.Sharing.SharingStage::PingIntervalSec
	float ___PingIntervalSec_16;
	// System.Boolean HoloToolkit.Sharing.SharingStage::IsAudioEndpoint
	bool ___IsAudioEndpoint_17;
	// HoloToolkit.Sharing.Utilities.ConsoleLogWriter HoloToolkit.Sharing.SharingStage::logWriter
	ConsoleLogWriter_t525723634 * ___logWriter_18;
	// HoloToolkit.Sharing.SyncRoot HoloToolkit.Sharing.SharingStage::<Root>k__BackingField
	SyncRoot_t1223809262 * ___U3CRootU3Ek__BackingField_19;
	// HoloToolkit.Sharing.ServerSessionsTracker HoloToolkit.Sharing.SharingStage::<SessionsTracker>k__BackingField
	ServerSessionsTracker_t3663004919 * ___U3CSessionsTrackerU3Ek__BackingField_20;
	// HoloToolkit.Sharing.SessionUsersTracker HoloToolkit.Sharing.SharingStage::<SessionUsersTracker>k__BackingField
	SessionUsersTracker_t2471166300 * ___U3CSessionUsersTrackerU3Ek__BackingField_21;
	// HoloToolkit.Sharing.SyncStateListener HoloToolkit.Sharing.SharingStage::<SyncStateListener>k__BackingField
	SyncStateListener_t778580365 * ___U3CSyncStateListenerU3Ek__BackingField_22;
	// System.String HoloToolkit.Sharing.SharingStage::<AppInstanceUniqueId>k__BackingField
	String_t* ___U3CAppInstanceUniqueIdU3Ek__BackingField_23;
	// System.Action`1<System.String> HoloToolkit.Sharing.SharingStage::UserNameChanged
	Action_1_t2019918284 * ___UserNameChanged_24;
	// HoloToolkit.Sharing.DiscoveryClient HoloToolkit.Sharing.SharingStage::discoveryClient
	DiscoveryClient_t1530907974 * ___discoveryClient_25;
	// HoloToolkit.Sharing.DiscoveryClientAdapter HoloToolkit.Sharing.SharingStage::discoveryClientAdapter
	DiscoveryClientAdapter_t99005982 * ___discoveryClientAdapter_26;
	// System.Single HoloToolkit.Sharing.SharingStage::pingIntervalCurrent
	float ___pingIntervalCurrent_27;
	// System.Boolean HoloToolkit.Sharing.SharingStage::isTryingToFindServer
	bool ___isTryingToFindServer_28;
	// System.Boolean HoloToolkit.Sharing.SharingStage::ShowDetailedLogs
	bool ___ShowDetailedLogs_29;
	// HoloToolkit.Sharing.RoomManagerAdapter HoloToolkit.Sharing.SharingStage::RoomManagerAdapter
	RoomManagerAdapter_t3977794230 * ___RoomManagerAdapter_30;
	// HoloToolkit.Sharing.NetworkConnectionAdapter HoloToolkit.Sharing.SharingStage::networkConnectionAdapter
	NetworkConnectionAdapter_t239632668 * ___networkConnectionAdapter_31;

public:
	inline static int32_t get_offset_of_SharingManagerConnected_4() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___SharingManagerConnected_4)); }
	inline EventHandler_t1348719766 * get_SharingManagerConnected_4() const { return ___SharingManagerConnected_4; }
	inline EventHandler_t1348719766 ** get_address_of_SharingManagerConnected_4() { return &___SharingManagerConnected_4; }
	inline void set_SharingManagerConnected_4(EventHandler_t1348719766 * value)
	{
		___SharingManagerConnected_4 = value;
		Il2CppCodeGenWriteBarrier((&___SharingManagerConnected_4), value);
	}

	inline static int32_t get_offset_of_SharingManagerDisconnected_5() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___SharingManagerDisconnected_5)); }
	inline EventHandler_t1348719766 * get_SharingManagerDisconnected_5() const { return ___SharingManagerDisconnected_5; }
	inline EventHandler_t1348719766 ** get_address_of_SharingManagerDisconnected_5() { return &___SharingManagerDisconnected_5; }
	inline void set_SharingManagerDisconnected_5(EventHandler_t1348719766 * value)
	{
		___SharingManagerDisconnected_5 = value;
		Il2CppCodeGenWriteBarrier((&___SharingManagerDisconnected_5), value);
	}

	inline static int32_t get_offset_of_ClientRole_7() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___ClientRole_7)); }
	inline int32_t get_ClientRole_7() const { return ___ClientRole_7; }
	inline int32_t* get_address_of_ClientRole_7() { return &___ClientRole_7; }
	inline void set_ClientRole_7(int32_t value)
	{
		___ClientRole_7 = value;
	}

	inline static int32_t get_offset_of_ServerAddress_8() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___ServerAddress_8)); }
	inline String_t* get_ServerAddress_8() const { return ___ServerAddress_8; }
	inline String_t** get_address_of_ServerAddress_8() { return &___ServerAddress_8; }
	inline void set_ServerAddress_8(String_t* value)
	{
		___ServerAddress_8 = value;
		Il2CppCodeGenWriteBarrier((&___ServerAddress_8), value);
	}

	inline static int32_t get_offset_of_defaultSessionName_9() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___defaultSessionName_9)); }
	inline String_t* get_defaultSessionName_9() const { return ___defaultSessionName_9; }
	inline String_t** get_address_of_defaultSessionName_9() { return &___defaultSessionName_9; }
	inline void set_defaultSessionName_9(String_t* value)
	{
		___defaultSessionName_9 = value;
		Il2CppCodeGenWriteBarrier((&___defaultSessionName_9), value);
	}

	inline static int32_t get_offset_of_defaultRoomName_10() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___defaultRoomName_10)); }
	inline String_t* get_defaultRoomName_10() const { return ___defaultRoomName_10; }
	inline String_t** get_address_of_defaultRoomName_10() { return &___defaultRoomName_10; }
	inline void set_defaultRoomName_10(String_t* value)
	{
		___defaultRoomName_10 = value;
		Il2CppCodeGenWriteBarrier((&___defaultRoomName_10), value);
	}

	inline static int32_t get_offset_of_KeepRoomAlive_11() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___KeepRoomAlive_11)); }
	inline bool get_KeepRoomAlive_11() const { return ___KeepRoomAlive_11; }
	inline bool* get_address_of_KeepRoomAlive_11() { return &___KeepRoomAlive_11; }
	inline void set_KeepRoomAlive_11(bool value)
	{
		___KeepRoomAlive_11 = value;
	}

	inline static int32_t get_offset_of_ServerPort_12() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___ServerPort_12)); }
	inline int32_t get_ServerPort_12() const { return ___ServerPort_12; }
	inline int32_t* get_address_of_ServerPort_12() { return &___ServerPort_12; }
	inline void set_ServerPort_12(int32_t value)
	{
		___ServerPort_12 = value;
	}

	inline static int32_t get_offset_of_connectOnAwake_13() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___connectOnAwake_13)); }
	inline bool get_connectOnAwake_13() const { return ___connectOnAwake_13; }
	inline bool* get_address_of_connectOnAwake_13() { return &___connectOnAwake_13; }
	inline void set_connectOnAwake_13(bool value)
	{
		___connectOnAwake_13 = value;
	}

	inline static int32_t get_offset_of_U3CManagerU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___U3CManagerU3Ek__BackingField_14)); }
	inline SharingManager_t2871218373 * get_U3CManagerU3Ek__BackingField_14() const { return ___U3CManagerU3Ek__BackingField_14; }
	inline SharingManager_t2871218373 ** get_address_of_U3CManagerU3Ek__BackingField_14() { return &___U3CManagerU3Ek__BackingField_14; }
	inline void set_U3CManagerU3Ek__BackingField_14(SharingManager_t2871218373 * value)
	{
		___U3CManagerU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CManagerU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_AutoDiscoverServer_15() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___AutoDiscoverServer_15)); }
	inline bool get_AutoDiscoverServer_15() const { return ___AutoDiscoverServer_15; }
	inline bool* get_address_of_AutoDiscoverServer_15() { return &___AutoDiscoverServer_15; }
	inline void set_AutoDiscoverServer_15(bool value)
	{
		___AutoDiscoverServer_15 = value;
	}

	inline static int32_t get_offset_of_PingIntervalSec_16() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___PingIntervalSec_16)); }
	inline float get_PingIntervalSec_16() const { return ___PingIntervalSec_16; }
	inline float* get_address_of_PingIntervalSec_16() { return &___PingIntervalSec_16; }
	inline void set_PingIntervalSec_16(float value)
	{
		___PingIntervalSec_16 = value;
	}

	inline static int32_t get_offset_of_IsAudioEndpoint_17() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___IsAudioEndpoint_17)); }
	inline bool get_IsAudioEndpoint_17() const { return ___IsAudioEndpoint_17; }
	inline bool* get_address_of_IsAudioEndpoint_17() { return &___IsAudioEndpoint_17; }
	inline void set_IsAudioEndpoint_17(bool value)
	{
		___IsAudioEndpoint_17 = value;
	}

	inline static int32_t get_offset_of_logWriter_18() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___logWriter_18)); }
	inline ConsoleLogWriter_t525723634 * get_logWriter_18() const { return ___logWriter_18; }
	inline ConsoleLogWriter_t525723634 ** get_address_of_logWriter_18() { return &___logWriter_18; }
	inline void set_logWriter_18(ConsoleLogWriter_t525723634 * value)
	{
		___logWriter_18 = value;
		Il2CppCodeGenWriteBarrier((&___logWriter_18), value);
	}

	inline static int32_t get_offset_of_U3CRootU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___U3CRootU3Ek__BackingField_19)); }
	inline SyncRoot_t1223809262 * get_U3CRootU3Ek__BackingField_19() const { return ___U3CRootU3Ek__BackingField_19; }
	inline SyncRoot_t1223809262 ** get_address_of_U3CRootU3Ek__BackingField_19() { return &___U3CRootU3Ek__BackingField_19; }
	inline void set_U3CRootU3Ek__BackingField_19(SyncRoot_t1223809262 * value)
	{
		___U3CRootU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CSessionsTrackerU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___U3CSessionsTrackerU3Ek__BackingField_20)); }
	inline ServerSessionsTracker_t3663004919 * get_U3CSessionsTrackerU3Ek__BackingField_20() const { return ___U3CSessionsTrackerU3Ek__BackingField_20; }
	inline ServerSessionsTracker_t3663004919 ** get_address_of_U3CSessionsTrackerU3Ek__BackingField_20() { return &___U3CSessionsTrackerU3Ek__BackingField_20; }
	inline void set_U3CSessionsTrackerU3Ek__BackingField_20(ServerSessionsTracker_t3663004919 * value)
	{
		___U3CSessionsTrackerU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSessionsTrackerU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CSessionUsersTrackerU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___U3CSessionUsersTrackerU3Ek__BackingField_21)); }
	inline SessionUsersTracker_t2471166300 * get_U3CSessionUsersTrackerU3Ek__BackingField_21() const { return ___U3CSessionUsersTrackerU3Ek__BackingField_21; }
	inline SessionUsersTracker_t2471166300 ** get_address_of_U3CSessionUsersTrackerU3Ek__BackingField_21() { return &___U3CSessionUsersTrackerU3Ek__BackingField_21; }
	inline void set_U3CSessionUsersTrackerU3Ek__BackingField_21(SessionUsersTracker_t2471166300 * value)
	{
		___U3CSessionUsersTrackerU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSessionUsersTrackerU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3CSyncStateListenerU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___U3CSyncStateListenerU3Ek__BackingField_22)); }
	inline SyncStateListener_t778580365 * get_U3CSyncStateListenerU3Ek__BackingField_22() const { return ___U3CSyncStateListenerU3Ek__BackingField_22; }
	inline SyncStateListener_t778580365 ** get_address_of_U3CSyncStateListenerU3Ek__BackingField_22() { return &___U3CSyncStateListenerU3Ek__BackingField_22; }
	inline void set_U3CSyncStateListenerU3Ek__BackingField_22(SyncStateListener_t778580365 * value)
	{
		___U3CSyncStateListenerU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSyncStateListenerU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_U3CAppInstanceUniqueIdU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___U3CAppInstanceUniqueIdU3Ek__BackingField_23)); }
	inline String_t* get_U3CAppInstanceUniqueIdU3Ek__BackingField_23() const { return ___U3CAppInstanceUniqueIdU3Ek__BackingField_23; }
	inline String_t** get_address_of_U3CAppInstanceUniqueIdU3Ek__BackingField_23() { return &___U3CAppInstanceUniqueIdU3Ek__BackingField_23; }
	inline void set_U3CAppInstanceUniqueIdU3Ek__BackingField_23(String_t* value)
	{
		___U3CAppInstanceUniqueIdU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAppInstanceUniqueIdU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_UserNameChanged_24() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___UserNameChanged_24)); }
	inline Action_1_t2019918284 * get_UserNameChanged_24() const { return ___UserNameChanged_24; }
	inline Action_1_t2019918284 ** get_address_of_UserNameChanged_24() { return &___UserNameChanged_24; }
	inline void set_UserNameChanged_24(Action_1_t2019918284 * value)
	{
		___UserNameChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___UserNameChanged_24), value);
	}

	inline static int32_t get_offset_of_discoveryClient_25() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___discoveryClient_25)); }
	inline DiscoveryClient_t1530907974 * get_discoveryClient_25() const { return ___discoveryClient_25; }
	inline DiscoveryClient_t1530907974 ** get_address_of_discoveryClient_25() { return &___discoveryClient_25; }
	inline void set_discoveryClient_25(DiscoveryClient_t1530907974 * value)
	{
		___discoveryClient_25 = value;
		Il2CppCodeGenWriteBarrier((&___discoveryClient_25), value);
	}

	inline static int32_t get_offset_of_discoveryClientAdapter_26() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___discoveryClientAdapter_26)); }
	inline DiscoveryClientAdapter_t99005982 * get_discoveryClientAdapter_26() const { return ___discoveryClientAdapter_26; }
	inline DiscoveryClientAdapter_t99005982 ** get_address_of_discoveryClientAdapter_26() { return &___discoveryClientAdapter_26; }
	inline void set_discoveryClientAdapter_26(DiscoveryClientAdapter_t99005982 * value)
	{
		___discoveryClientAdapter_26 = value;
		Il2CppCodeGenWriteBarrier((&___discoveryClientAdapter_26), value);
	}

	inline static int32_t get_offset_of_pingIntervalCurrent_27() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___pingIntervalCurrent_27)); }
	inline float get_pingIntervalCurrent_27() const { return ___pingIntervalCurrent_27; }
	inline float* get_address_of_pingIntervalCurrent_27() { return &___pingIntervalCurrent_27; }
	inline void set_pingIntervalCurrent_27(float value)
	{
		___pingIntervalCurrent_27 = value;
	}

	inline static int32_t get_offset_of_isTryingToFindServer_28() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___isTryingToFindServer_28)); }
	inline bool get_isTryingToFindServer_28() const { return ___isTryingToFindServer_28; }
	inline bool* get_address_of_isTryingToFindServer_28() { return &___isTryingToFindServer_28; }
	inline void set_isTryingToFindServer_28(bool value)
	{
		___isTryingToFindServer_28 = value;
	}

	inline static int32_t get_offset_of_ShowDetailedLogs_29() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___ShowDetailedLogs_29)); }
	inline bool get_ShowDetailedLogs_29() const { return ___ShowDetailedLogs_29; }
	inline bool* get_address_of_ShowDetailedLogs_29() { return &___ShowDetailedLogs_29; }
	inline void set_ShowDetailedLogs_29(bool value)
	{
		___ShowDetailedLogs_29 = value;
	}

	inline static int32_t get_offset_of_RoomManagerAdapter_30() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___RoomManagerAdapter_30)); }
	inline RoomManagerAdapter_t3977794230 * get_RoomManagerAdapter_30() const { return ___RoomManagerAdapter_30; }
	inline RoomManagerAdapter_t3977794230 ** get_address_of_RoomManagerAdapter_30() { return &___RoomManagerAdapter_30; }
	inline void set_RoomManagerAdapter_30(RoomManagerAdapter_t3977794230 * value)
	{
		___RoomManagerAdapter_30 = value;
		Il2CppCodeGenWriteBarrier((&___RoomManagerAdapter_30), value);
	}

	inline static int32_t get_offset_of_networkConnectionAdapter_31() { return static_cast<int32_t>(offsetof(SharingStage_t1191029027, ___networkConnectionAdapter_31)); }
	inline NetworkConnectionAdapter_t239632668 * get_networkConnectionAdapter_31() const { return ___networkConnectionAdapter_31; }
	inline NetworkConnectionAdapter_t239632668 ** get_address_of_networkConnectionAdapter_31() { return &___networkConnectionAdapter_31; }
	inline void set_networkConnectionAdapter_31(NetworkConnectionAdapter_t239632668 * value)
	{
		___networkConnectionAdapter_31 = value;
		Il2CppCodeGenWriteBarrier((&___networkConnectionAdapter_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARINGSTAGE_T1191029027_H
#ifndef AUTOJOINSESSIONANDROOM_T479681466_H
#define AUTOJOINSESSIONANDROOM_T479681466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom
struct  AutoJoinSessionAndRoom_t479681466  : public Singleton_1_t879905920
{
public:
	// System.Int64 HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom::roomID
	int64_t ___roomID_4;
	// System.Single HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom::Timeout
	float ___Timeout_5;

public:
	inline static int32_t get_offset_of_roomID_4() { return static_cast<int32_t>(offsetof(AutoJoinSessionAndRoom_t479681466, ___roomID_4)); }
	inline int64_t get_roomID_4() const { return ___roomID_4; }
	inline int64_t* get_address_of_roomID_4() { return &___roomID_4; }
	inline void set_roomID_4(int64_t value)
	{
		___roomID_4 = value;
	}

	inline static int32_t get_offset_of_Timeout_5() { return static_cast<int32_t>(offsetof(AutoJoinSessionAndRoom_t479681466, ___Timeout_5)); }
	inline float get_Timeout_5() const { return ___Timeout_5; }
	inline float* get_address_of_Timeout_5() { return &___Timeout_5; }
	inline void set_Timeout_5(float value)
	{
		___Timeout_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOJOINSESSIONANDROOM_T479681466_H
#ifndef SHARINGWORLDANCHORMANAGER_T183689775_H
#define SHARINGWORLDANCHORMANAGER_T183689775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SharingWorldAnchorManager
struct  SharingWorldAnchorManager_t183689775  : public WorldAnchorManager_t2731889775
{
public:
	// System.Action`1<System.Boolean> HoloToolkit.Sharing.SharingWorldAnchorManager::AnchorUploaded
	Action_1_t269755560 * ___AnchorUploaded_10;
	// System.Action`2<System.Boolean,UnityEngine.GameObject> HoloToolkit.Sharing.SharingWorldAnchorManager::AnchorDownloaded
	Action_2_t557018160 * ___AnchorDownloaded_11;
	// UnityEngine.XR.WSA.Sharing.WorldAnchorTransferBatch HoloToolkit.Sharing.SharingWorldAnchorManager::currentAnchorTransferBatch
	WorldAnchorTransferBatch_t2392365061 * ___currentAnchorTransferBatch_13;
	// System.Boolean HoloToolkit.Sharing.SharingWorldAnchorManager::isExportingAnchors
	bool ___isExportingAnchors_14;
	// System.Boolean HoloToolkit.Sharing.SharingWorldAnchorManager::shouldExportAnchors
	bool ___shouldExportAnchors_15;
	// System.Collections.Generic.List`1<System.Byte> HoloToolkit.Sharing.SharingWorldAnchorManager::rawAnchorUploadData
	List_1_t2606371118 * ___rawAnchorUploadData_16;
	// System.Boolean HoloToolkit.Sharing.SharingWorldAnchorManager::canUpdate
	bool ___canUpdate_17;
	// System.Boolean HoloToolkit.Sharing.SharingWorldAnchorManager::isImportingAnchors
	bool ___isImportingAnchors_18;
	// System.Boolean HoloToolkit.Sharing.SharingWorldAnchorManager::shouldImportAnchors
	bool ___shouldImportAnchors_19;
	// System.Byte[] HoloToolkit.Sharing.SharingWorldAnchorManager::rawAnchorDownloadData
	ByteU5BU5D_t4116647657* ___rawAnchorDownloadData_20;

public:
	inline static int32_t get_offset_of_AnchorUploaded_10() { return static_cast<int32_t>(offsetof(SharingWorldAnchorManager_t183689775, ___AnchorUploaded_10)); }
	inline Action_1_t269755560 * get_AnchorUploaded_10() const { return ___AnchorUploaded_10; }
	inline Action_1_t269755560 ** get_address_of_AnchorUploaded_10() { return &___AnchorUploaded_10; }
	inline void set_AnchorUploaded_10(Action_1_t269755560 * value)
	{
		___AnchorUploaded_10 = value;
		Il2CppCodeGenWriteBarrier((&___AnchorUploaded_10), value);
	}

	inline static int32_t get_offset_of_AnchorDownloaded_11() { return static_cast<int32_t>(offsetof(SharingWorldAnchorManager_t183689775, ___AnchorDownloaded_11)); }
	inline Action_2_t557018160 * get_AnchorDownloaded_11() const { return ___AnchorDownloaded_11; }
	inline Action_2_t557018160 ** get_address_of_AnchorDownloaded_11() { return &___AnchorDownloaded_11; }
	inline void set_AnchorDownloaded_11(Action_2_t557018160 * value)
	{
		___AnchorDownloaded_11 = value;
		Il2CppCodeGenWriteBarrier((&___AnchorDownloaded_11), value);
	}

	inline static int32_t get_offset_of_currentAnchorTransferBatch_13() { return static_cast<int32_t>(offsetof(SharingWorldAnchorManager_t183689775, ___currentAnchorTransferBatch_13)); }
	inline WorldAnchorTransferBatch_t2392365061 * get_currentAnchorTransferBatch_13() const { return ___currentAnchorTransferBatch_13; }
	inline WorldAnchorTransferBatch_t2392365061 ** get_address_of_currentAnchorTransferBatch_13() { return &___currentAnchorTransferBatch_13; }
	inline void set_currentAnchorTransferBatch_13(WorldAnchorTransferBatch_t2392365061 * value)
	{
		___currentAnchorTransferBatch_13 = value;
		Il2CppCodeGenWriteBarrier((&___currentAnchorTransferBatch_13), value);
	}

	inline static int32_t get_offset_of_isExportingAnchors_14() { return static_cast<int32_t>(offsetof(SharingWorldAnchorManager_t183689775, ___isExportingAnchors_14)); }
	inline bool get_isExportingAnchors_14() const { return ___isExportingAnchors_14; }
	inline bool* get_address_of_isExportingAnchors_14() { return &___isExportingAnchors_14; }
	inline void set_isExportingAnchors_14(bool value)
	{
		___isExportingAnchors_14 = value;
	}

	inline static int32_t get_offset_of_shouldExportAnchors_15() { return static_cast<int32_t>(offsetof(SharingWorldAnchorManager_t183689775, ___shouldExportAnchors_15)); }
	inline bool get_shouldExportAnchors_15() const { return ___shouldExportAnchors_15; }
	inline bool* get_address_of_shouldExportAnchors_15() { return &___shouldExportAnchors_15; }
	inline void set_shouldExportAnchors_15(bool value)
	{
		___shouldExportAnchors_15 = value;
	}

	inline static int32_t get_offset_of_rawAnchorUploadData_16() { return static_cast<int32_t>(offsetof(SharingWorldAnchorManager_t183689775, ___rawAnchorUploadData_16)); }
	inline List_1_t2606371118 * get_rawAnchorUploadData_16() const { return ___rawAnchorUploadData_16; }
	inline List_1_t2606371118 ** get_address_of_rawAnchorUploadData_16() { return &___rawAnchorUploadData_16; }
	inline void set_rawAnchorUploadData_16(List_1_t2606371118 * value)
	{
		___rawAnchorUploadData_16 = value;
		Il2CppCodeGenWriteBarrier((&___rawAnchorUploadData_16), value);
	}

	inline static int32_t get_offset_of_canUpdate_17() { return static_cast<int32_t>(offsetof(SharingWorldAnchorManager_t183689775, ___canUpdate_17)); }
	inline bool get_canUpdate_17() const { return ___canUpdate_17; }
	inline bool* get_address_of_canUpdate_17() { return &___canUpdate_17; }
	inline void set_canUpdate_17(bool value)
	{
		___canUpdate_17 = value;
	}

	inline static int32_t get_offset_of_isImportingAnchors_18() { return static_cast<int32_t>(offsetof(SharingWorldAnchorManager_t183689775, ___isImportingAnchors_18)); }
	inline bool get_isImportingAnchors_18() const { return ___isImportingAnchors_18; }
	inline bool* get_address_of_isImportingAnchors_18() { return &___isImportingAnchors_18; }
	inline void set_isImportingAnchors_18(bool value)
	{
		___isImportingAnchors_18 = value;
	}

	inline static int32_t get_offset_of_shouldImportAnchors_19() { return static_cast<int32_t>(offsetof(SharingWorldAnchorManager_t183689775, ___shouldImportAnchors_19)); }
	inline bool get_shouldImportAnchors_19() const { return ___shouldImportAnchors_19; }
	inline bool* get_address_of_shouldImportAnchors_19() { return &___shouldImportAnchors_19; }
	inline void set_shouldImportAnchors_19(bool value)
	{
		___shouldImportAnchors_19 = value;
	}

	inline static int32_t get_offset_of_rawAnchorDownloadData_20() { return static_cast<int32_t>(offsetof(SharingWorldAnchorManager_t183689775, ___rawAnchorDownloadData_20)); }
	inline ByteU5BU5D_t4116647657* get_rawAnchorDownloadData_20() const { return ___rawAnchorDownloadData_20; }
	inline ByteU5BU5D_t4116647657** get_address_of_rawAnchorDownloadData_20() { return &___rawAnchorDownloadData_20; }
	inline void set_rawAnchorDownloadData_20(ByteU5BU5D_t4116647657* value)
	{
		___rawAnchorDownloadData_20 = value;
		Il2CppCodeGenWriteBarrier((&___rawAnchorDownloadData_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARINGWORLDANCHORMANAGER_T183689775_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5400 = { sizeof (Receipt_t1200975798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5400[2] = 
{
	Receipt_t1200975798::get_offset_of_swigCPtr_0(),
	Receipt_t1200975798::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5401 = { sizeof (Room_t1590920303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5401[3] = 
{
	Room_t1590920303::get_offset_of_swigCPtr_0(),
	Room_t1590920303::get_offset_of_swigCMemOwn_1(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5402 = { sizeof (RoomManager_t2684434268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5402[2] = 
{
	RoomManager_t2684434268::get_offset_of_swigCPtr_0(),
	RoomManager_t2684434268::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5403 = { sizeof (RoomManagerAdapter_t3977794230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5403[7] = 
{
	RoomManagerAdapter_t3977794230::get_offset_of_RoomAddedEvent_17(),
	RoomManagerAdapter_t3977794230::get_offset_of_RoomClosedEvent_18(),
	RoomManagerAdapter_t3977794230::get_offset_of_UserJoinedRoomEvent_19(),
	RoomManagerAdapter_t3977794230::get_offset_of_UserLeftRoomEvent_20(),
	RoomManagerAdapter_t3977794230::get_offset_of_AnchorsChangedEvent_21(),
	RoomManagerAdapter_t3977794230::get_offset_of_AnchorsDownloadedEvent_22(),
	RoomManagerAdapter_t3977794230::get_offset_of_AnchorUploadedEvent_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5404 = { sizeof (RoomManagerListener_t1172308708), -1, sizeof(RoomManagerListener_t1172308708_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5404[15] = 
{
	RoomManagerListener_t1172308708::get_offset_of_swigCPtr_2(),
	RoomManagerListener_t1172308708::get_offset_of_swigDelegate0_3(),
	RoomManagerListener_t1172308708::get_offset_of_swigDelegate1_4(),
	RoomManagerListener_t1172308708::get_offset_of_swigDelegate2_5(),
	RoomManagerListener_t1172308708::get_offset_of_swigDelegate3_6(),
	RoomManagerListener_t1172308708::get_offset_of_swigDelegate4_7(),
	RoomManagerListener_t1172308708::get_offset_of_swigDelegate5_8(),
	RoomManagerListener_t1172308708::get_offset_of_swigDelegate6_9(),
	RoomManagerListener_t1172308708_StaticFields::get_offset_of_swigMethodTypes0_10(),
	RoomManagerListener_t1172308708_StaticFields::get_offset_of_swigMethodTypes1_11(),
	RoomManagerListener_t1172308708_StaticFields::get_offset_of_swigMethodTypes2_12(),
	RoomManagerListener_t1172308708_StaticFields::get_offset_of_swigMethodTypes3_13(),
	RoomManagerListener_t1172308708_StaticFields::get_offset_of_swigMethodTypes4_14(),
	RoomManagerListener_t1172308708_StaticFields::get_offset_of_swigMethodTypes5_15(),
	RoomManagerListener_t1172308708_StaticFields::get_offset_of_swigMethodTypes6_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5405 = { sizeof (SwigDelegateRoomManagerListener_0_t3232391369), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5406 = { sizeof (SwigDelegateRoomManagerListener_1_t3232391370), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5407 = { sizeof (SwigDelegateRoomManagerListener_2_t3232391367), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5408 = { sizeof (SwigDelegateRoomManagerListener_3_t3232391368), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5409 = { sizeof (SwigDelegateRoomManagerListener_4_t3232391365), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5410 = { sizeof (SwigDelegateRoomManagerListener_5_t3232391366), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5411 = { sizeof (SwigDelegateRoomManagerListener_6_t3232391363), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5412 = { sizeof (Session_t1844742251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5412[2] = 
{
	Session_t1844742251::get_offset_of_swigCPtr_0(),
	Session_t1844742251::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5413 = { sizeof (SessionListener_t1219443984), -1, sizeof(SessionListener_t1219443984_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5413[9] = 
{
	SessionListener_t1219443984::get_offset_of_swigCPtr_2(),
	SessionListener_t1219443984::get_offset_of_swigDelegate0_3(),
	SessionListener_t1219443984::get_offset_of_swigDelegate1_4(),
	SessionListener_t1219443984::get_offset_of_swigDelegate2_5(),
	SessionListener_t1219443984::get_offset_of_swigDelegate3_6(),
	SessionListener_t1219443984_StaticFields::get_offset_of_swigMethodTypes0_7(),
	SessionListener_t1219443984_StaticFields::get_offset_of_swigMethodTypes1_8(),
	SessionListener_t1219443984_StaticFields::get_offset_of_swigMethodTypes2_9(),
	SessionListener_t1219443984_StaticFields::get_offset_of_swigMethodTypes3_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5414 = { sizeof (SwigDelegateSessionListener_0_t458903625), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5415 = { sizeof (SwigDelegateSessionListener_1_t458903626), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5416 = { sizeof (SwigDelegateSessionListener_2_t458903623), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5417 = { sizeof (SwigDelegateSessionListener_3_t458903624), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5418 = { sizeof (SessionManager_t3810561508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5418[2] = 
{
	SessionManager_t3810561508::get_offset_of_swigCPtr_0(),
	SessionManager_t3810561508::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5419 = { sizeof (SessionManagerAdapter_t2913645974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5419[9] = 
{
	SessionManagerAdapter_t2913645974::get_offset_of_CreateSucceededEvent_21(),
	SessionManagerAdapter_t2913645974::get_offset_of_CreateFailedEvent_22(),
	SessionManagerAdapter_t2913645974::get_offset_of_SessionAddedEvent_23(),
	SessionManagerAdapter_t2913645974::get_offset_of_SessionClosedEvent_24(),
	SessionManagerAdapter_t2913645974::get_offset_of_UserJoinedSessionEvent_25(),
	SessionManagerAdapter_t2913645974::get_offset_of_UserLeftSessionEvent_26(),
	SessionManagerAdapter_t2913645974::get_offset_of_UserChangedEvent_27(),
	SessionManagerAdapter_t2913645974::get_offset_of_ServerConnectedEvent_28(),
	SessionManagerAdapter_t2913645974::get_offset_of_ServerDisconnectedEvent_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5420 = { sizeof (SessionManagerListener_t234187265), -1, sizeof(SessionManagerListener_t234187265_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5420[19] = 
{
	SessionManagerListener_t234187265::get_offset_of_swigCPtr_2(),
	SessionManagerListener_t234187265::get_offset_of_swigDelegate0_3(),
	SessionManagerListener_t234187265::get_offset_of_swigDelegate1_4(),
	SessionManagerListener_t234187265::get_offset_of_swigDelegate2_5(),
	SessionManagerListener_t234187265::get_offset_of_swigDelegate3_6(),
	SessionManagerListener_t234187265::get_offset_of_swigDelegate4_7(),
	SessionManagerListener_t234187265::get_offset_of_swigDelegate5_8(),
	SessionManagerListener_t234187265::get_offset_of_swigDelegate6_9(),
	SessionManagerListener_t234187265::get_offset_of_swigDelegate7_10(),
	SessionManagerListener_t234187265::get_offset_of_swigDelegate8_11(),
	SessionManagerListener_t234187265_StaticFields::get_offset_of_swigMethodTypes0_12(),
	SessionManagerListener_t234187265_StaticFields::get_offset_of_swigMethodTypes1_13(),
	SessionManagerListener_t234187265_StaticFields::get_offset_of_swigMethodTypes2_14(),
	SessionManagerListener_t234187265_StaticFields::get_offset_of_swigMethodTypes3_15(),
	SessionManagerListener_t234187265_StaticFields::get_offset_of_swigMethodTypes4_16(),
	SessionManagerListener_t234187265_StaticFields::get_offset_of_swigMethodTypes5_17(),
	SessionManagerListener_t234187265_StaticFields::get_offset_of_swigMethodTypes6_18(),
	SessionManagerListener_t234187265_StaticFields::get_offset_of_swigMethodTypes7_19(),
	SessionManagerListener_t234187265_StaticFields::get_offset_of_swigMethodTypes8_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5421 = { sizeof (SwigDelegateSessionManagerListener_0_t176756126), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5422 = { sizeof (SwigDelegateSessionManagerListener_1_t1742840067), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5423 = { sizeof (SwigDelegateSessionManagerListener_2_t3308924008), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5424 = { sizeof (SwigDelegateSessionManagerListener_3_t580040653), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5425 = { sizeof (SwigDelegateSessionManagerListener_4_t2502354954), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5426 = { sizeof (SwigDelegateSessionManagerListener_5_t4068438895), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5427 = { sizeof (SwigDelegateSessionManagerListener_6_t1339555540), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5428 = { sizeof (SwigDelegateSessionManagerListener_7_t2905639481), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5429 = { sizeof (SwigDelegateSessionManagerListener_8_t532986486), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5430 = { sizeof (SessionType_t1285190767)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5430[3] = 
{
	SessionType_t1285190767::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5431 = { sizeof (Settings_t4264890459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5431[2] = 
{
	Settings_t4264890459::get_offset_of_swigCPtr_0(),
	Settings_t4264890459::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5432 = { sizeof (SharingClient_t1608744197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5433 = { sizeof (SharingClientPINVOKE_t520792832), -1, sizeof(SharingClientPINVOKE_t520792832_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5433[2] = 
{
	SharingClientPINVOKE_t520792832_StaticFields::get_offset_of_swigExceptionHelper_0(),
	SharingClientPINVOKE_t520792832_StaticFields::get_offset_of_swigStringHelper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5434 = { sizeof (SWIGExceptionHelper_t3643858324), -1, sizeof(SWIGExceptionHelper_t3643858324_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5434[14] = 
{
	SWIGExceptionHelper_t3643858324_StaticFields::get_offset_of_applicationDelegate_0(),
	SWIGExceptionHelper_t3643858324_StaticFields::get_offset_of_arithmeticDelegate_1(),
	SWIGExceptionHelper_t3643858324_StaticFields::get_offset_of_divideByZeroDelegate_2(),
	SWIGExceptionHelper_t3643858324_StaticFields::get_offset_of_indexOutOfRangeDelegate_3(),
	SWIGExceptionHelper_t3643858324_StaticFields::get_offset_of_invalidCastDelegate_4(),
	SWIGExceptionHelper_t3643858324_StaticFields::get_offset_of_invalidOperationDelegate_5(),
	SWIGExceptionHelper_t3643858324_StaticFields::get_offset_of_ioDelegate_6(),
	SWIGExceptionHelper_t3643858324_StaticFields::get_offset_of_nullReferenceDelegate_7(),
	SWIGExceptionHelper_t3643858324_StaticFields::get_offset_of_outOfMemoryDelegate_8(),
	SWIGExceptionHelper_t3643858324_StaticFields::get_offset_of_overflowDelegate_9(),
	SWIGExceptionHelper_t3643858324_StaticFields::get_offset_of_systemDelegate_10(),
	SWIGExceptionHelper_t3643858324_StaticFields::get_offset_of_argumentDelegate_11(),
	SWIGExceptionHelper_t3643858324_StaticFields::get_offset_of_argumentNullDelegate_12(),
	SWIGExceptionHelper_t3643858324_StaticFields::get_offset_of_argumentOutOfRangeDelegate_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5435 = { sizeof (ExceptionDelegate_t827543389), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5436 = { sizeof (ExceptionArgumentDelegate_t3478888600), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5437 = { sizeof (SWIGPendingException_t3488673755), -1, sizeof(SWIGPendingException_t3488673755_StaticFields), sizeof(SWIGPendingException_t3488673755_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable5437[2] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	SWIGPendingException_t3488673755_StaticFields::get_offset_of_numExceptionsPending_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5438 = { sizeof (SWIGStringHelper_t801960530), -1, sizeof(SWIGStringHelper_t801960530_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5438[1] = 
{
	SWIGStringHelper_t801960530_StaticFields::get_offset_of_stringDelegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5439 = { sizeof (SWIGStringDelegate_t4212175550), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5440 = { sizeof (SharingManager_t2871218373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5440[2] = 
{
	SharingManager_t2871218373::get_offset_of_swigCPtr_0(),
	SharingManager_t2871218373::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5441 = { sizeof (StringArrayElement_t1419279831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5441[1] = 
{
	StringArrayElement_t1419279831::get_offset_of_swigCPtr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5442 = { sizeof (StringArrayListener_t707674793), -1, sizeof(StringArrayListener_t707674793_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5442[7] = 
{
	StringArrayListener_t707674793::get_offset_of_swigCPtr_2(),
	StringArrayListener_t707674793::get_offset_of_swigDelegate0_3(),
	StringArrayListener_t707674793::get_offset_of_swigDelegate1_4(),
	StringArrayListener_t707674793::get_offset_of_swigDelegate2_5(),
	StringArrayListener_t707674793_StaticFields::get_offset_of_swigMethodTypes0_6(),
	StringArrayListener_t707674793_StaticFields::get_offset_of_swigMethodTypes1_7(),
	StringArrayListener_t707674793_StaticFields::get_offset_of_swigMethodTypes2_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5443 = { sizeof (SwigDelegateStringArrayListener_0_t1418370588), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5444 = { sizeof (SwigDelegateStringArrayListener_1_t1418370589), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5445 = { sizeof (SwigDelegateStringArrayListener_2_t1418370590), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5446 = { sizeof (StringElement_t2038048136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5446[1] = 
{
	StringElement_t2038048136::get_offset_of_swigCPtr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5447 = { sizeof (SyncListener_t1939120163), -1, sizeof(SyncListener_t1939120163_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5447[5] = 
{
	SyncListener_t1939120163::get_offset_of_swigCPtr_2(),
	SyncListener_t1939120163::get_offset_of_swigDelegate0_3(),
	SyncListener_t1939120163::get_offset_of_swigDelegate1_4(),
	SyncListener_t1939120163_StaticFields::get_offset_of_swigMethodTypes0_5(),
	SyncListener_t1939120163_StaticFields::get_offset_of_swigMethodTypes1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5448 = { sizeof (SwigDelegateSyncListener_0_t1307184870), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5449 = { sizeof (SwigDelegateSyncListener_1_t4036068225), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5450 = { sizeof (SystemRole_t769564548)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5450[5] = 
{
	SystemRole_t769564548::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5451 = { sizeof (TagImage_t1147278201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5451[2] = 
{
	TagImage_t1147278201::get_offset_of_swigCPtr_0(),
	TagImage_t1147278201::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5452 = { sizeof (User_t790099560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5452[3] = 
{
	User_t790099560::get_offset_of_swigCPtr_0(),
	User_t790099560::get_offset_of_swigCMemOwn_1(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5453 = { sizeof (UserPresenceManager_t3005000763), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5453[2] = 
{
	UserPresenceManager_t3005000763::get_offset_of_swigCPtr_0(),
	UserPresenceManager_t3005000763::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5454 = { sizeof (UserPresenceManagerAdapter_t1106856923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5454[1] = 
{
	UserPresenceManagerAdapter_t1106856923::get_offset_of_UserPresenceChangedEvent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5455 = { sizeof (UserPresenceManagerListener_t1715939279), -1, sizeof(UserPresenceManagerListener_t1715939279_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5455[3] = 
{
	UserPresenceManagerListener_t1715939279::get_offset_of_swigCPtr_2(),
	UserPresenceManagerListener_t1715939279::get_offset_of_swigDelegate0_3(),
	UserPresenceManagerListener_t1715939279_StaticFields::get_offset_of_swigMethodTypes0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5456 = { sizeof (SwigDelegateUserPresenceManagerListener_0_t4115417027), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5457 = { sizeof (VisualPairConnector_t1673154465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5457[1] = 
{
	VisualPairConnector_t1673154465::get_offset_of_swigCPtr_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5458 = { sizeof (VisualPairReceiver_t3311246970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5458[1] = 
{
	VisualPairReceiver_t3311246970::get_offset_of_swigCPtr_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5459 = { sizeof (XString_t940838754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5459[2] = 
{
	XString_t940838754::get_offset_of_swigCPtr_0(),
	XString_t940838754::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5460 = { sizeof (ServerSessionsTracker_t3663004919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5460[15] = 
{
	ServerSessionsTracker_t3663004919::get_offset_of_disposed_0(),
	ServerSessionsTracker_t3663004919::get_offset_of_sessionManager_1(),
	ServerSessionsTracker_t3663004919::get_offset_of_sessionManagerAdapter_2(),
	ServerSessionsTracker_t3663004919::get_offset_of_SessionCreated_3(),
	ServerSessionsTracker_t3663004919::get_offset_of_SessionAdded_4(),
	ServerSessionsTracker_t3663004919::get_offset_of_SessionClosed_5(),
	ServerSessionsTracker_t3663004919::get_offset_of_UserChanged_6(),
	ServerSessionsTracker_t3663004919::get_offset_of_UserJoined_7(),
	ServerSessionsTracker_t3663004919::get_offset_of_UserLeft_8(),
	ServerSessionsTracker_t3663004919::get_offset_of_CurrentUserLeft_9(),
	ServerSessionsTracker_t3663004919::get_offset_of_CurrentUserJoined_10(),
	ServerSessionsTracker_t3663004919::get_offset_of_ServerConnected_11(),
	ServerSessionsTracker_t3663004919::get_offset_of_ServerDisconnected_12(),
	ServerSessionsTracker_t3663004919::get_offset_of_U3CSessionsU3Ek__BackingField_13(),
	ServerSessionsTracker_t3663004919::get_offset_of_U3CIsServerConnectedU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5461 = { sizeof (SessionUsersTracker_t2471166300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5461[4] = 
{
	SessionUsersTracker_t2471166300::get_offset_of_UserJoined_0(),
	SessionUsersTracker_t2471166300::get_offset_of_UserLeft_1(),
	SessionUsersTracker_t2471166300::get_offset_of_serverSessionsTracker_2(),
	SessionUsersTracker_t2471166300::get_offset_of_U3CCurrentUsersU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5462 = { sizeof (SharingStage_t1191029027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5462[28] = 
{
	SharingStage_t1191029027::get_offset_of_SharingManagerConnected_4(),
	SharingStage_t1191029027::get_offset_of_SharingManagerDisconnected_5(),
	0,
	SharingStage_t1191029027::get_offset_of_ClientRole_7(),
	SharingStage_t1191029027::get_offset_of_ServerAddress_8(),
	SharingStage_t1191029027::get_offset_of_defaultSessionName_9(),
	SharingStage_t1191029027::get_offset_of_defaultRoomName_10(),
	SharingStage_t1191029027::get_offset_of_KeepRoomAlive_11(),
	SharingStage_t1191029027::get_offset_of_ServerPort_12(),
	SharingStage_t1191029027::get_offset_of_connectOnAwake_13(),
	SharingStage_t1191029027::get_offset_of_U3CManagerU3Ek__BackingField_14(),
	SharingStage_t1191029027::get_offset_of_AutoDiscoverServer_15(),
	SharingStage_t1191029027::get_offset_of_PingIntervalSec_16(),
	SharingStage_t1191029027::get_offset_of_IsAudioEndpoint_17(),
	SharingStage_t1191029027::get_offset_of_logWriter_18(),
	SharingStage_t1191029027::get_offset_of_U3CRootU3Ek__BackingField_19(),
	SharingStage_t1191029027::get_offset_of_U3CSessionsTrackerU3Ek__BackingField_20(),
	SharingStage_t1191029027::get_offset_of_U3CSessionUsersTrackerU3Ek__BackingField_21(),
	SharingStage_t1191029027::get_offset_of_U3CSyncStateListenerU3Ek__BackingField_22(),
	SharingStage_t1191029027::get_offset_of_U3CAppInstanceUniqueIdU3Ek__BackingField_23(),
	SharingStage_t1191029027::get_offset_of_UserNameChanged_24(),
	SharingStage_t1191029027::get_offset_of_discoveryClient_25(),
	SharingStage_t1191029027::get_offset_of_discoveryClientAdapter_26(),
	SharingStage_t1191029027::get_offset_of_pingIntervalCurrent_27(),
	SharingStage_t1191029027::get_offset_of_isTryingToFindServer_28(),
	SharingStage_t1191029027::get_offset_of_ShowDetailedLogs_29(),
	SharingStage_t1191029027::get_offset_of_RoomManagerAdapter_30(),
	SharingStage_t1191029027::get_offset_of_networkConnectionAdapter_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5463 = { sizeof (SyncRoot_t1223809262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5463[1] = 
{
	SyncRoot_t1223809262::get_offset_of_InstantiatedPrefabs_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5464 = { sizeof (SyncStateListener_t778580365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5464[2] = 
{
	SyncStateListener_t778580365::get_offset_of_SyncChangesBeginEvent_7(),
	SyncStateListener_t778580365::get_offset_of_SyncChangesEndEvent_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5465 = { sizeof (DefaultSyncModelAccessor_t624511954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5465[1] = 
{
	DefaultSyncModelAccessor_t624511954::get_offset_of_U3CSyncModelU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5466 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5467 = { sizeof (TransformSynchronizer_t999777773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5467[4] = 
{
	TransformSynchronizer_t999777773::get_offset_of_Position_2(),
	TransformSynchronizer_t999777773::get_offset_of_Rotation_3(),
	TransformSynchronizer_t999777773::get_offset_of_Scale_4(),
	TransformSynchronizer_t999777773::get_offset_of_transformDataModel_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5468 = { sizeof (SharingWorldAnchorManager_t183689775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5468[11] = 
{
	SharingWorldAnchorManager_t183689775::get_offset_of_AnchorUploaded_10(),
	SharingWorldAnchorManager_t183689775::get_offset_of_AnchorDownloaded_11(),
	0,
	SharingWorldAnchorManager_t183689775::get_offset_of_currentAnchorTransferBatch_13(),
	SharingWorldAnchorManager_t183689775::get_offset_of_isExportingAnchors_14(),
	SharingWorldAnchorManager_t183689775::get_offset_of_shouldExportAnchors_15(),
	SharingWorldAnchorManager_t183689775::get_offset_of_rawAnchorUploadData_16(),
	SharingWorldAnchorManager_t183689775::get_offset_of_canUpdate_17(),
	SharingWorldAnchorManager_t183689775::get_offset_of_isImportingAnchors_18(),
	SharingWorldAnchorManager_t183689775::get_offset_of_shouldImportAnchors_19(),
	SharingWorldAnchorManager_t183689775::get_offset_of_rawAnchorDownloadData_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5469 = { sizeof (MicrophoneReceiver_t1621290537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5469[25] = 
{
	MicrophoneReceiver_t1621290537::get_offset_of_versionExtractor_2(),
	MicrophoneReceiver_t1621290537::get_offset_of_audioStreamCountExtractor_3(),
	MicrophoneReceiver_t1621290537::get_offset_of_channelCountExtractor_4(),
	MicrophoneReceiver_t1621290537::get_offset_of_sampleRateExtractor_5(),
	MicrophoneReceiver_t1621290537::get_offset_of_sampleTypeExtractor_6(),
	MicrophoneReceiver_t1621290537::get_offset_of_sampleCountExtractor_7(),
	MicrophoneReceiver_t1621290537::get_offset_of_codecTypeExtractor_8(),
	MicrophoneReceiver_t1621290537::get_offset_of_sequenceNumberExtractor_9(),
	MicrophoneReceiver_t1621290537::get_offset_of_GlobalAnchorTransform_10(),
	0,
	MicrophoneReceiver_t1621290537::get_offset_of_prominentSpeakerCount_12(),
	MicrophoneReceiver_t1621290537::get_offset_of_prominentSpeakerList_13(),
	MicrophoneReceiver_t1621290537::get_offset_of_listener_14(),
	MicrophoneReceiver_t1621290537::get_offset_of_audioDataMutex_15(),
	0,
	0,
	MicrophoneReceiver_t1621290537::get_offset_of_DropOffMaximumMetres_18(),
	MicrophoneReceiver_t1621290537::get_offset_of_PanMaximumMetres_19(),
	MicrophoneReceiver_t1621290537::get_offset_of_MinimumDistance_20(),
	MicrophoneReceiver_t1621290537::get_offset_of_networkPacketBufferBytes_21(),
	MicrophoneReceiver_t1621290537::get_offset_of_circularBuffer_22(),
	MicrophoneReceiver_t1621290537::get_offset_of_testCircularBuffer_23(),
	MicrophoneReceiver_t1621290537::get_offset_of_testSource_24(),
	MicrophoneReceiver_t1621290537::get_offset_of_TestClip_25(),
	MicrophoneReceiver_t1621290537::get_offset_of_SaveTestClip_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5470 = { sizeof (ProminentSpeakerInfo_t3676337740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5470[3] = 
{
	ProminentSpeakerInfo_t3676337740::get_offset_of_SourceId_0(),
	ProminentSpeakerInfo_t3676337740::get_offset_of_AverageAmplitude_1(),
	ProminentSpeakerInfo_t3676337740::get_offset_of_HrtfPosition_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5471 = { sizeof (MicrophoneTransmitter_t285957085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5471[31] = 
{
	MicrophoneTransmitter_t285957085::get_offset_of_Streamtype_2(),
	MicrophoneTransmitter_t285957085::get_offset_of_InputGain_3(),
	MicrophoneTransmitter_t285957085::get_offset_of_ShouldTransmitAudio_4(),
	MicrophoneTransmitter_t285957085::get_offset_of_Mute_5(),
	MicrophoneTransmitter_t285957085::get_offset_of_GlobalAnchorTransform_6(),
	MicrophoneTransmitter_t285957085::get_offset_of_ShowInterPacketTime_7(),
	MicrophoneTransmitter_t285957085::get_offset_of_timeOfLastPacketSend_8(),
	MicrophoneTransmitter_t285957085::get_offset_of_worstTimeBetweenPackets_9(),
	MicrophoneTransmitter_t285957085::get_offset_of_sequenceNumber_10(),
	MicrophoneTransmitter_t285957085::get_offset_of_sampleRateType_11(),
	MicrophoneTransmitter_t285957085::get_offset_of_audioSource_12(),
	MicrophoneTransmitter_t285957085::get_offset_of_hasServerConnection_13(),
	MicrophoneTransmitter_t285957085::get_offset_of_micStarted_14(),
	0,
	MicrophoneTransmitter_t285957085::get_offset_of_micBuffer_16(),
	MicrophoneTransmitter_t285957085::get_offset_of_packetSamples_17(),
	MicrophoneTransmitter_t285957085::get_offset_of_versionPacker_18(),
	MicrophoneTransmitter_t285957085::get_offset_of_audioStreamCountPacker_19(),
	MicrophoneTransmitter_t285957085::get_offset_of_channelCountPacker_20(),
	MicrophoneTransmitter_t285957085::get_offset_of_sampleRatePacker_21(),
	MicrophoneTransmitter_t285957085::get_offset_of_sampleTypePacker_22(),
	MicrophoneTransmitter_t285957085::get_offset_of_sampleCountPacker_23(),
	MicrophoneTransmitter_t285957085::get_offset_of_codecTypePacker_24(),
	MicrophoneTransmitter_t285957085::get_offset_of_mutePacker_25(),
	MicrophoneTransmitter_t285957085::get_offset_of_sequenceNumberPacker_26(),
	MicrophoneTransmitter_t285957085::get_offset_of_audioDataMutex_27(),
	MicrophoneTransmitter_t285957085::get_offset_of_HearSelf_28(),
	MicrophoneTransmitter_t285957085::get_offset_of_testCircularBuffer_29(),
	MicrophoneTransmitter_t285957085::get_offset_of_testSource_30(),
	MicrophoneTransmitter_t285957085::get_offset_of_TestClip_31(),
	MicrophoneTransmitter_t285957085::get_offset_of_SaveTestClip_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5472 = { sizeof (AutoJoinSessionAndRoom_t479681466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5472[2] = 
{
	AutoJoinSessionAndRoom_t479681466::get_offset_of_roomID_4(),
	AutoJoinSessionAndRoom_t479681466::get_offset_of_Timeout_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5473 = { sizeof (U3CAutoConnectU3Ed__8_t3408352236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5473[6] = 
{
	U3CAutoConnectU3Ed__8_t3408352236::get_offset_of_U3CU3E1__state_0(),
	U3CAutoConnectU3Ed__8_t3408352236::get_offset_of_U3CU3E2__current_1(),
	U3CAutoConnectU3Ed__8_t3408352236::get_offset_of_U3CU3E4__this_2(),
	U3CAutoConnectU3Ed__8_t3408352236::get_offset_of_U3CsessionExistsU3E5__1_3(),
	U3CAutoConnectU3Ed__8_t3408352236::get_offset_of_U3CiU3E5__2_4(),
	U3CAutoConnectU3Ed__8_t3408352236::get_offset_of_U3CiU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5474 = { sizeof (ConsoleLogWriter_t525723634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5474[1] = 
{
	ConsoleLogWriter_t525723634::get_offset_of_ShowDetailedLogs_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5475 = { sizeof (DirectPairing_t1147775817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5475[9] = 
{
	DirectPairing_t1147775817::get_offset_of_PairingRole_2(),
	DirectPairing_t1147775817::get_offset_of_RemoteAddress_3(),
	DirectPairing_t1147775817::get_offset_of_RemotePort_4(),
	DirectPairing_t1147775817::get_offset_of_LocalPort_5(),
	DirectPairing_t1147775817::get_offset_of_AutoReconnect_6(),
	DirectPairing_t1147775817::get_offset_of_sharingMgr_7(),
	DirectPairing_t1147775817::get_offset_of_pairMaker_8(),
	DirectPairing_t1147775817::get_offset_of_pairingAdapter_9(),
	DirectPairing_t1147775817::get_offset_of_connectionAdapter_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5476 = { sizeof (Role_t2521298479)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5476[3] = 
{
	Role_t2521298479::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5477 = { sizeof (ManualIpConfiguration_t1673208871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5477[10] = 
{
	0,
	ManualIpConfiguration_t1673208871::get_offset_of_HideWhenConnected_3(),
	ManualIpConfiguration_t1673208871::get_offset_of_HideAfterSeconds_4(),
	ManualIpConfiguration_t1673208871::get_offset_of_Timeout_5(),
	ManualIpConfiguration_t1673208871::get_offset_of_ipAddress_6(),
	ManualIpConfiguration_t1673208871::get_offset_of_connectionIndicator_7(),
	ManualIpConfiguration_t1673208871::get_offset_of_timerRunning_8(),
	ManualIpConfiguration_t1673208871::get_offset_of_timer_9(),
	ManualIpConfiguration_t1673208871::get_offset_of_isTryingToConnect_10(),
	ManualIpConfiguration_t1673208871::get_offset_of_firstRun_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5478 = { sizeof (U3CHideU3Ed__24_t2549062377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5478[3] = 
{
	U3CHideU3Ed__24_t2549062377::get_offset_of_U3CU3E1__state_0(),
	U3CHideU3Ed__24_t2549062377::get_offset_of_U3CU3E2__current_1(),
	U3CHideU3Ed__24_t2549062377::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5479 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5479[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5480 = { sizeof (SyncBool_t2415766883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5480[2] = 
{
	SyncBool_t2415766883::get_offset_of_element_3(),
	SyncBool_t2415766883::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5481 = { sizeof (SyncDataAttribute_t3499957947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5481[1] = 
{
	SyncDataAttribute_t3499957947::get_offset_of_CustomFieldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5482 = { sizeof (SyncDataClassAttribute_t3940588719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5482[1] = 
{
	SyncDataClassAttribute_t3940588719::get_offset_of_CustomClassName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5483 = { sizeof (SyncDouble_t3192565710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5483[2] = 
{
	SyncDouble_t3192565710::get_offset_of_element_3(),
	SyncDouble_t3192565710::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5484 = { sizeof (SyncFloat_t2189089750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5484[2] = 
{
	SyncFloat_t2189089750::get_offset_of_element_3(),
	SyncFloat_t2189089750::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5485 = { sizeof (SyncInteger_t2757910165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5485[2] = 
{
	SyncInteger_t2757910165::get_offset_of_element_3(),
	SyncInteger_t2757910165::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5486 = { sizeof (SyncLong_t1964838366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5486[2] = 
{
	SyncLong_t1964838366::get_offset_of_element_3(),
	SyncLong_t1964838366::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5487 = { sizeof (SyncObject_t2866352391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5487[7] = 
{
	SyncObject_t2866352391::get_offset_of_syncListener_3(),
	SyncObject_t2866352391::get_offset_of_primitiveMap_4(),
	SyncObject_t2866352391::get_offset_of_primitives_5(),
	SyncObject_t2866352391::get_offset_of_ObjectChanged_6(),
	SyncObject_t2866352391::get_offset_of_InitializationComplete_7(),
	SyncObject_t2866352391::get_offset_of_internalObjectElement_8(),
	SyncObject_t2866352391::get_offset_of_owner_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5488 = { sizeof (SyncPrimitive_t3664634715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5488[3] = 
{
	SyncPrimitive_t3664634715::get_offset_of_fieldName_0(),
	SyncPrimitive_t3664634715::get_offset_of_xStringFieldName_1(),
	SyncPrimitive_t3664634715::get_offset_of_internalElement_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5489 = { sizeof (SyncQuaternion_t228159934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5489[4] = 
{
	SyncQuaternion_t228159934::get_offset_of_x_10(),
	SyncQuaternion_t228159934::get_offset_of_y_11(),
	SyncQuaternion_t228159934::get_offset_of_z_12(),
	SyncQuaternion_t228159934::get_offset_of_w_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5490 = { sizeof (SyncString_t2813399380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5490[2] = 
{
	SyncString_t2813399380::get_offset_of_element_3(),
	SyncString_t2813399380::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5491 = { sizeof (SyncTransform_t4091961955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5491[6] = 
{
	SyncTransform_t4091961955::get_offset_of_Position_10(),
	SyncTransform_t4091961955::get_offset_of_Rotation_11(),
	SyncTransform_t4091961955::get_offset_of_Scale_12(),
	SyncTransform_t4091961955::get_offset_of_PositionChanged_13(),
	SyncTransform_t4091961955::get_offset_of_RotationChanged_14(),
	SyncTransform_t4091961955::get_offset_of_ScaleChanged_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5492 = { sizeof (SyncVector3_t1671304744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5492[3] = 
{
	SyncVector3_t1671304744::get_offset_of_x_10(),
	SyncVector3_t1671304744::get_offset_of_y_11(),
	SyncVector3_t1671304744::get_offset_of_z_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5493 = { sizeof (SyncSettings_t2375960512), -1, sizeof(SyncSettings_t2375960512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5493[3] = 
{
	SyncSettings_t2375960512::get_offset_of_dataModelTypeToName_0(),
	SyncSettings_t2375960512::get_offset_of_dataModelNameToType_1(),
	SyncSettings_t2375960512_StaticFields::get_offset_of_instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5494 = { sizeof (PrefabToDataModel_t1762303220)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5494[2] = 
{
	PrefabToDataModel_t1762303220::get_offset_of_DataModelClassName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PrefabToDataModel_t1762303220::get_offset_of_Prefab_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5495 = { sizeof (PrefabSpawnManager_t3856760055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5495[3] = 
{
	PrefabSpawnManager_t3856760055::get_offset_of_spawnablePrefabs_6(),
	PrefabSpawnManager_t3856760055::get_offset_of_typeToPrefab_7(),
	PrefabSpawnManager_t3856760055::get_offset_of_objectCreationCounter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5496 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5496[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5497 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5498 = { sizeof (SyncSpawnedObject_t3749163513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5498[5] = 
{
	SyncSpawnedObject_t3749163513::get_offset_of_Transform_10(),
	SyncSpawnedObject_t3749163513::get_offset_of_Name_11(),
	SyncSpawnedObject_t3749163513::get_offset_of_ParentPath_12(),
	SyncSpawnedObject_t3749163513::get_offset_of_ObjectPath_13(),
	SyncSpawnedObject_t3749163513::get_offset_of_U3CGameObjectU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5499 = { sizeof (CameraCache_t1177433023), -1, sizeof(CameraCache_t1177433023_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5499[1] = 
{
	CameraCache_t1177433023_StaticFields::get_offset_of_cachedCamera_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
