﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// MS.Internal.Xml.Cache.XPathNode[]
struct XPathNodeU5BU5D_t47339301;
// System.Xml.XPath.XPathDocument
struct XPathDocument_t1673143697;
// MS.Internal.Xml.Cache.XPathNodePageInfo
struct XPathNodePageInfo_t2343388010;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Threading.Tasks.Task
struct Task_t3187275312;
// System.Threading.Tasks.Task`1<System.Boolean>
struct Task_1_t1502828140;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t61518632;
// MS.Internal.Xml.XPath.XPathScanner
struct XPathScanner_t3283201025;
// System.Xml.XPath.XPathResultType[]
struct XPathResultTypeU5BU5D_t1515527577;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Text.EncoderFallback
struct EncoderFallback_t1188251036;
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t722666473;
// System.Text.EncoderNLS
struct EncoderNLS_t449404832;
// System.Char
struct Char_t3634460470;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// Mono.Security.ASN1
struct ASN1_t2114160833;
// Mono.Security.X509.X509Store
struct X509Store_t2777415284;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Security.Cryptography.CspParameters
struct CspParameters_t239852639;
// Mono.Math.BigInteger
struct BigInteger_t2902905090;
// Mono.Security.X509.X509Stores
struct X509Stores_t1373936238;
// System.Xml.XmlRawWriter
struct XmlRawWriter_t722320575;
// MS.Internal.Xml.Cache.XPathNodeInfoAtom
struct XPathNodeInfoAtom_t1760358141;
// System.Xml.XmlTextEncoder
struct XmlTextEncoder_t1632274355;
// System.Xml.XPath.XPathNavigatorKeyComparer
struct XPathNavigatorKeyComparer_t2518900029;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Xml.CharEntityEncoderFallbackBuffer
struct CharEntityEncoderFallbackBuffer_t4206548234;
// Mono.Security.X509.Extensions.GeneralNames
struct GeneralNames_t2702294159;
// System.Xml.CharEntityEncoderFallback
struct CharEntityEncoderFallback_t110445598;
// System.Xml.XmlRawWriterBase64Encoder
struct XmlRawWriterBase64Encoder_t1905751661;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t535375154;
// System.Void
struct Void_t1185182177;
// MS.Internal.Xml.XPath.AstNode
struct AstNode_t2514041814;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1432317219;
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
struct KeyGeneratedEventHandler_t3064139578;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IO.Stream
struct Stream_t1273022909;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Text.Encoder
struct Encoder_t2198218980;
// System.IO.TextWriter
struct TextWriter_t3478189236;
// MS.Internal.Xml.XPath.Operator/Op[]
struct OpU5BU5D_t2837398892;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Xml.ByteStack
struct ByteStack_t2013325889;
// System.Xml.TernaryTreeReadOnly
struct TernaryTreeReadOnly_t172569514;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.EventArgs
struct EventArgs_t3591816995;

struct XPathNode_t2208072876_marshaled_pinvoke;
struct XPathNode_t2208072876_marshaled_com;



#ifndef U3CMODULEU3E_T692745527_H
#define U3CMODULEU3E_T692745527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745527 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745527_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef XPATHNODEINFOATOM_T1760358141_H
#define XPATHNODEINFOATOM_T1760358141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodeInfoAtom
struct  XPathNodeInfoAtom_t1760358141  : public RuntimeObject
{
public:
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::localName
	String_t* ___localName_0;
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::namespaceUri
	String_t* ___namespaceUri_1;
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::prefix
	String_t* ___prefix_2;
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::baseUri
	String_t* ___baseUri_3;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageParent
	XPathNodeU5BU5D_t47339301* ___pageParent_4;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageSibling
	XPathNodeU5BU5D_t47339301* ___pageSibling_5;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageSimilar
	XPathNodeU5BU5D_t47339301* ___pageSimilar_6;
	// System.Xml.XPath.XPathDocument MS.Internal.Xml.Cache.XPathNodeInfoAtom::doc
	XPathDocument_t1673143697 * ___doc_7;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::lineNumBase
	int32_t ___lineNumBase_8;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::linePosBase
	int32_t ___linePosBase_9;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::hashCode
	int32_t ___hashCode_10;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::localNameHash
	int32_t ___localNameHash_11;
	// MS.Internal.Xml.Cache.XPathNodePageInfo MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageInfo
	XPathNodePageInfo_t2343388010 * ___pageInfo_12;

public:
	inline static int32_t get_offset_of_localName_0() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___localName_0)); }
	inline String_t* get_localName_0() const { return ___localName_0; }
	inline String_t** get_address_of_localName_0() { return &___localName_0; }
	inline void set_localName_0(String_t* value)
	{
		___localName_0 = value;
		Il2CppCodeGenWriteBarrier((&___localName_0), value);
	}

	inline static int32_t get_offset_of_namespaceUri_1() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___namespaceUri_1)); }
	inline String_t* get_namespaceUri_1() const { return ___namespaceUri_1; }
	inline String_t** get_address_of_namespaceUri_1() { return &___namespaceUri_1; }
	inline void set_namespaceUri_1(String_t* value)
	{
		___namespaceUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_1), value);
	}

	inline static int32_t get_offset_of_prefix_2() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___prefix_2)); }
	inline String_t* get_prefix_2() const { return ___prefix_2; }
	inline String_t** get_address_of_prefix_2() { return &___prefix_2; }
	inline void set_prefix_2(String_t* value)
	{
		___prefix_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_2), value);
	}

	inline static int32_t get_offset_of_baseUri_3() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___baseUri_3)); }
	inline String_t* get_baseUri_3() const { return ___baseUri_3; }
	inline String_t** get_address_of_baseUri_3() { return &___baseUri_3; }
	inline void set_baseUri_3(String_t* value)
	{
		___baseUri_3 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_3), value);
	}

	inline static int32_t get_offset_of_pageParent_4() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___pageParent_4)); }
	inline XPathNodeU5BU5D_t47339301* get_pageParent_4() const { return ___pageParent_4; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_pageParent_4() { return &___pageParent_4; }
	inline void set_pageParent_4(XPathNodeU5BU5D_t47339301* value)
	{
		___pageParent_4 = value;
		Il2CppCodeGenWriteBarrier((&___pageParent_4), value);
	}

	inline static int32_t get_offset_of_pageSibling_5() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___pageSibling_5)); }
	inline XPathNodeU5BU5D_t47339301* get_pageSibling_5() const { return ___pageSibling_5; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_pageSibling_5() { return &___pageSibling_5; }
	inline void set_pageSibling_5(XPathNodeU5BU5D_t47339301* value)
	{
		___pageSibling_5 = value;
		Il2CppCodeGenWriteBarrier((&___pageSibling_5), value);
	}

	inline static int32_t get_offset_of_pageSimilar_6() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___pageSimilar_6)); }
	inline XPathNodeU5BU5D_t47339301* get_pageSimilar_6() const { return ___pageSimilar_6; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_pageSimilar_6() { return &___pageSimilar_6; }
	inline void set_pageSimilar_6(XPathNodeU5BU5D_t47339301* value)
	{
		___pageSimilar_6 = value;
		Il2CppCodeGenWriteBarrier((&___pageSimilar_6), value);
	}

	inline static int32_t get_offset_of_doc_7() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___doc_7)); }
	inline XPathDocument_t1673143697 * get_doc_7() const { return ___doc_7; }
	inline XPathDocument_t1673143697 ** get_address_of_doc_7() { return &___doc_7; }
	inline void set_doc_7(XPathDocument_t1673143697 * value)
	{
		___doc_7 = value;
		Il2CppCodeGenWriteBarrier((&___doc_7), value);
	}

	inline static int32_t get_offset_of_lineNumBase_8() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___lineNumBase_8)); }
	inline int32_t get_lineNumBase_8() const { return ___lineNumBase_8; }
	inline int32_t* get_address_of_lineNumBase_8() { return &___lineNumBase_8; }
	inline void set_lineNumBase_8(int32_t value)
	{
		___lineNumBase_8 = value;
	}

	inline static int32_t get_offset_of_linePosBase_9() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___linePosBase_9)); }
	inline int32_t get_linePosBase_9() const { return ___linePosBase_9; }
	inline int32_t* get_address_of_linePosBase_9() { return &___linePosBase_9; }
	inline void set_linePosBase_9(int32_t value)
	{
		___linePosBase_9 = value;
	}

	inline static int32_t get_offset_of_hashCode_10() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___hashCode_10)); }
	inline int32_t get_hashCode_10() const { return ___hashCode_10; }
	inline int32_t* get_address_of_hashCode_10() { return &___hashCode_10; }
	inline void set_hashCode_10(int32_t value)
	{
		___hashCode_10 = value;
	}

	inline static int32_t get_offset_of_localNameHash_11() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___localNameHash_11)); }
	inline int32_t get_localNameHash_11() const { return ___localNameHash_11; }
	inline int32_t* get_address_of_localNameHash_11() { return &___localNameHash_11; }
	inline void set_localNameHash_11(int32_t value)
	{
		___localNameHash_11 = value;
	}

	inline static int32_t get_offset_of_pageInfo_12() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t1760358141, ___pageInfo_12)); }
	inline XPathNodePageInfo_t2343388010 * get_pageInfo_12() const { return ___pageInfo_12; }
	inline XPathNodePageInfo_t2343388010 ** get_address_of_pageInfo_12() { return &___pageInfo_12; }
	inline void set_pageInfo_12(XPathNodePageInfo_t2343388010 * value)
	{
		___pageInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___pageInfo_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODEINFOATOM_T1760358141_H
#ifndef XPATHNODEPAGEINFO_T2343388010_H
#define XPATHNODEPAGEINFO_T2343388010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodePageInfo
struct  XPathNodePageInfo_t2343388010  : public RuntimeObject
{
public:
	// System.Int32 MS.Internal.Xml.Cache.XPathNodePageInfo::pageNum
	int32_t ___pageNum_0;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodePageInfo::nodeCount
	int32_t ___nodeCount_1;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodePageInfo::pageNext
	XPathNodeU5BU5D_t47339301* ___pageNext_2;

public:
	inline static int32_t get_offset_of_pageNum_0() { return static_cast<int32_t>(offsetof(XPathNodePageInfo_t2343388010, ___pageNum_0)); }
	inline int32_t get_pageNum_0() const { return ___pageNum_0; }
	inline int32_t* get_address_of_pageNum_0() { return &___pageNum_0; }
	inline void set_pageNum_0(int32_t value)
	{
		___pageNum_0 = value;
	}

	inline static int32_t get_offset_of_nodeCount_1() { return static_cast<int32_t>(offsetof(XPathNodePageInfo_t2343388010, ___nodeCount_1)); }
	inline int32_t get_nodeCount_1() const { return ___nodeCount_1; }
	inline int32_t* get_address_of_nodeCount_1() { return &___nodeCount_1; }
	inline void set_nodeCount_1(int32_t value)
	{
		___nodeCount_1 = value;
	}

	inline static int32_t get_offset_of_pageNext_2() { return static_cast<int32_t>(offsetof(XPathNodePageInfo_t2343388010, ___pageNext_2)); }
	inline XPathNodeU5BU5D_t47339301* get_pageNext_2() const { return ___pageNext_2; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_pageNext_2() { return &___pageNext_2; }
	inline void set_pageNext_2(XPathNodeU5BU5D_t47339301* value)
	{
		___pageNext_2 = value;
		Il2CppCodeGenWriteBarrier((&___pageNext_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODEPAGEINFO_T2343388010_H
#ifndef BASE64ENCODER_T3938083961_H
#define BASE64ENCODER_T3938083961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Base64Encoder
struct  Base64Encoder_t3938083961  : public RuntimeObject
{
public:
	// System.Byte[] System.Xml.Base64Encoder::leftOverBytes
	ByteU5BU5D_t4116647657* ___leftOverBytes_0;
	// System.Int32 System.Xml.Base64Encoder::leftOverBytesCount
	int32_t ___leftOverBytesCount_1;
	// System.Char[] System.Xml.Base64Encoder::charsLine
	CharU5BU5D_t3528271667* ___charsLine_2;

public:
	inline static int32_t get_offset_of_leftOverBytes_0() { return static_cast<int32_t>(offsetof(Base64Encoder_t3938083961, ___leftOverBytes_0)); }
	inline ByteU5BU5D_t4116647657* get_leftOverBytes_0() const { return ___leftOverBytes_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_leftOverBytes_0() { return &___leftOverBytes_0; }
	inline void set_leftOverBytes_0(ByteU5BU5D_t4116647657* value)
	{
		___leftOverBytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___leftOverBytes_0), value);
	}

	inline static int32_t get_offset_of_leftOverBytesCount_1() { return static_cast<int32_t>(offsetof(Base64Encoder_t3938083961, ___leftOverBytesCount_1)); }
	inline int32_t get_leftOverBytesCount_1() const { return ___leftOverBytesCount_1; }
	inline int32_t* get_address_of_leftOverBytesCount_1() { return &___leftOverBytesCount_1; }
	inline void set_leftOverBytesCount_1(int32_t value)
	{
		___leftOverBytesCount_1 = value;
	}

	inline static int32_t get_offset_of_charsLine_2() { return static_cast<int32_t>(offsetof(Base64Encoder_t3938083961, ___charsLine_2)); }
	inline CharU5BU5D_t3528271667* get_charsLine_2() const { return ___charsLine_2; }
	inline CharU5BU5D_t3528271667** get_address_of_charsLine_2() { return &___charsLine_2; }
	inline void set_charsLine_2(CharU5BU5D_t3528271667* value)
	{
		___charsLine_2 = value;
		Il2CppCodeGenWriteBarrier((&___charsLine_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE64ENCODER_T3938083961_H
#ifndef ASYNCHELPER_T714762001_H
#define ASYNCHELPER_T714762001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.AsyncHelper
struct  AsyncHelper_t714762001  : public RuntimeObject
{
public:

public:
};

struct AsyncHelper_t714762001_StaticFields
{
public:
	// System.Threading.Tasks.Task System.Xml.AsyncHelper::DoneTask
	Task_t3187275312 * ___DoneTask_0;
	// System.Threading.Tasks.Task`1<System.Boolean> System.Xml.AsyncHelper::DoneTaskTrue
	Task_1_t1502828140 * ___DoneTaskTrue_1;
	// System.Threading.Tasks.Task`1<System.Boolean> System.Xml.AsyncHelper::DoneTaskFalse
	Task_1_t1502828140 * ___DoneTaskFalse_2;
	// System.Threading.Tasks.Task`1<System.Int32> System.Xml.AsyncHelper::DoneTaskZero
	Task_1_t61518632 * ___DoneTaskZero_3;

public:
	inline static int32_t get_offset_of_DoneTask_0() { return static_cast<int32_t>(offsetof(AsyncHelper_t714762001_StaticFields, ___DoneTask_0)); }
	inline Task_t3187275312 * get_DoneTask_0() const { return ___DoneTask_0; }
	inline Task_t3187275312 ** get_address_of_DoneTask_0() { return &___DoneTask_0; }
	inline void set_DoneTask_0(Task_t3187275312 * value)
	{
		___DoneTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___DoneTask_0), value);
	}

	inline static int32_t get_offset_of_DoneTaskTrue_1() { return static_cast<int32_t>(offsetof(AsyncHelper_t714762001_StaticFields, ___DoneTaskTrue_1)); }
	inline Task_1_t1502828140 * get_DoneTaskTrue_1() const { return ___DoneTaskTrue_1; }
	inline Task_1_t1502828140 ** get_address_of_DoneTaskTrue_1() { return &___DoneTaskTrue_1; }
	inline void set_DoneTaskTrue_1(Task_1_t1502828140 * value)
	{
		___DoneTaskTrue_1 = value;
		Il2CppCodeGenWriteBarrier((&___DoneTaskTrue_1), value);
	}

	inline static int32_t get_offset_of_DoneTaskFalse_2() { return static_cast<int32_t>(offsetof(AsyncHelper_t714762001_StaticFields, ___DoneTaskFalse_2)); }
	inline Task_1_t1502828140 * get_DoneTaskFalse_2() const { return ___DoneTaskFalse_2; }
	inline Task_1_t1502828140 ** get_address_of_DoneTaskFalse_2() { return &___DoneTaskFalse_2; }
	inline void set_DoneTaskFalse_2(Task_1_t1502828140 * value)
	{
		___DoneTaskFalse_2 = value;
		Il2CppCodeGenWriteBarrier((&___DoneTaskFalse_2), value);
	}

	inline static int32_t get_offset_of_DoneTaskZero_3() { return static_cast<int32_t>(offsetof(AsyncHelper_t714762001_StaticFields, ___DoneTaskZero_3)); }
	inline Task_1_t61518632 * get_DoneTaskZero_3() const { return ___DoneTaskZero_3; }
	inline Task_1_t61518632 ** get_address_of_DoneTaskZero_3() { return &___DoneTaskZero_3; }
	inline void set_DoneTaskZero_3(Task_1_t61518632 * value)
	{
		___DoneTaskZero_3 = value;
		Il2CppCodeGenWriteBarrier((&___DoneTaskZero_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCHELPER_T714762001_H
#ifndef RES_T3627928856_H
#define RES_T3627928856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Res
struct  Res_t3627928856  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RES_T3627928856_H
#ifndef LOCALAPPCONTEXTSWITCHES_T2827819611_H
#define LOCALAPPCONTEXTSWITCHES_T2827819611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.LocalAppContextSwitches
struct  LocalAppContextSwitches_t2827819611  : public RuntimeObject
{
public:

public:
};

struct LocalAppContextSwitches_t2827819611_StaticFields
{
public:
	// System.Boolean System.LocalAppContextSwitches::DontThrowOnInvalidSurrogatePairs
	bool ___DontThrowOnInvalidSurrogatePairs_0;

public:
	inline static int32_t get_offset_of_DontThrowOnInvalidSurrogatePairs_0() { return static_cast<int32_t>(offsetof(LocalAppContextSwitches_t2827819611_StaticFields, ___DontThrowOnInvalidSurrogatePairs_0)); }
	inline bool get_DontThrowOnInvalidSurrogatePairs_0() const { return ___DontThrowOnInvalidSurrogatePairs_0; }
	inline bool* get_address_of_DontThrowOnInvalidSurrogatePairs_0() { return &___DontThrowOnInvalidSurrogatePairs_0; }
	inline void set_DontThrowOnInvalidSurrogatePairs_0(bool value)
	{
		___DontThrowOnInvalidSurrogatePairs_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALAPPCONTEXTSWITCHES_T2827819611_H
#ifndef SR_T167583544_H
#define SR_T167583544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SR
struct  SR_t167583544  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SR_T167583544_H
#ifndef PRIMEGENERATORBASE_T446028867_H
#define PRIMEGENERATORBASE_T446028867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.Generator.PrimeGeneratorBase
struct  PrimeGeneratorBase_t446028867  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMEGENERATORBASE_T446028867_H
#ifndef PRIMALITYTESTS_T1538473976_H
#define PRIMALITYTESTS_T1538473976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.PrimalityTests
struct  PrimalityTests_t1538473976  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMALITYTESTS_T1538473976_H
#ifndef ASTNODE_T2514041814_H
#define ASTNODE_T2514041814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.AstNode
struct  AstNode_t2514041814  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTNODE_T2514041814_H
#ifndef XPATHNODEHELPER_T2230825274_H
#define XPATHNODEHELPER_T2230825274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodeHelper
struct  XPathNodeHelper_t2230825274  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODEHELPER_T2230825274_H
#ifndef XMLWRITER_T127905479_H
#define XMLWRITER_T127905479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWriter
struct  XmlWriter_t127905479  : public RuntimeObject
{
public:
	// System.Char[] System.Xml.XmlWriter::writeNodeBuffer
	CharU5BU5D_t3528271667* ___writeNodeBuffer_0;

public:
	inline static int32_t get_offset_of_writeNodeBuffer_0() { return static_cast<int32_t>(offsetof(XmlWriter_t127905479, ___writeNodeBuffer_0)); }
	inline CharU5BU5D_t3528271667* get_writeNodeBuffer_0() const { return ___writeNodeBuffer_0; }
	inline CharU5BU5D_t3528271667** get_address_of_writeNodeBuffer_0() { return &___writeNodeBuffer_0; }
	inline void set_writeNodeBuffer_0(CharU5BU5D_t3528271667* value)
	{
		___writeNodeBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___writeNodeBuffer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWRITER_T127905479_H
#ifndef XPATHPARSER_T618394529_H
#define XPATHPARSER_T618394529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.XPathParser
struct  XPathParser_t618394529  : public RuntimeObject
{
public:
	// MS.Internal.Xml.XPath.XPathScanner MS.Internal.Xml.XPath.XPathParser::scanner
	XPathScanner_t3283201025 * ___scanner_0;
	// System.Int32 MS.Internal.Xml.XPath.XPathParser::parseDepth
	int32_t ___parseDepth_1;

public:
	inline static int32_t get_offset_of_scanner_0() { return static_cast<int32_t>(offsetof(XPathParser_t618394529, ___scanner_0)); }
	inline XPathScanner_t3283201025 * get_scanner_0() const { return ___scanner_0; }
	inline XPathScanner_t3283201025 ** get_address_of_scanner_0() { return &___scanner_0; }
	inline void set_scanner_0(XPathScanner_t3283201025 * value)
	{
		___scanner_0 = value;
		Il2CppCodeGenWriteBarrier((&___scanner_0), value);
	}

	inline static int32_t get_offset_of_parseDepth_1() { return static_cast<int32_t>(offsetof(XPathParser_t618394529, ___parseDepth_1)); }
	inline int32_t get_parseDepth_1() const { return ___parseDepth_1; }
	inline int32_t* get_address_of_parseDepth_1() { return &___parseDepth_1; }
	inline void set_parseDepth_1(int32_t value)
	{
		___parseDepth_1 = value;
	}
};

struct XPathParser_t618394529_StaticFields
{
public:
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray1
	XPathResultTypeU5BU5D_t1515527577* ___temparray1_2;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray2
	XPathResultTypeU5BU5D_t1515527577* ___temparray2_3;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray3
	XPathResultTypeU5BU5D_t1515527577* ___temparray3_4;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray4
	XPathResultTypeU5BU5D_t1515527577* ___temparray4_5;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray5
	XPathResultTypeU5BU5D_t1515527577* ___temparray5_6;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray6
	XPathResultTypeU5BU5D_t1515527577* ___temparray6_7;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray7
	XPathResultTypeU5BU5D_t1515527577* ___temparray7_8;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray8
	XPathResultTypeU5BU5D_t1515527577* ___temparray8_9;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray9
	XPathResultTypeU5BU5D_t1515527577* ___temparray9_10;
	// System.Collections.Hashtable MS.Internal.Xml.XPath.XPathParser::functionTable
	Hashtable_t1853889766 * ___functionTable_11;
	// System.Collections.Hashtable MS.Internal.Xml.XPath.XPathParser::AxesTable
	Hashtable_t1853889766 * ___AxesTable_12;

public:
	inline static int32_t get_offset_of_temparray1_2() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray1_2)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray1_2() const { return ___temparray1_2; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray1_2() { return &___temparray1_2; }
	inline void set_temparray1_2(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray1_2 = value;
		Il2CppCodeGenWriteBarrier((&___temparray1_2), value);
	}

	inline static int32_t get_offset_of_temparray2_3() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray2_3)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray2_3() const { return ___temparray2_3; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray2_3() { return &___temparray2_3; }
	inline void set_temparray2_3(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray2_3 = value;
		Il2CppCodeGenWriteBarrier((&___temparray2_3), value);
	}

	inline static int32_t get_offset_of_temparray3_4() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray3_4)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray3_4() const { return ___temparray3_4; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray3_4() { return &___temparray3_4; }
	inline void set_temparray3_4(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray3_4 = value;
		Il2CppCodeGenWriteBarrier((&___temparray3_4), value);
	}

	inline static int32_t get_offset_of_temparray4_5() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray4_5)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray4_5() const { return ___temparray4_5; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray4_5() { return &___temparray4_5; }
	inline void set_temparray4_5(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray4_5 = value;
		Il2CppCodeGenWriteBarrier((&___temparray4_5), value);
	}

	inline static int32_t get_offset_of_temparray5_6() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray5_6)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray5_6() const { return ___temparray5_6; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray5_6() { return &___temparray5_6; }
	inline void set_temparray5_6(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray5_6 = value;
		Il2CppCodeGenWriteBarrier((&___temparray5_6), value);
	}

	inline static int32_t get_offset_of_temparray6_7() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray6_7)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray6_7() const { return ___temparray6_7; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray6_7() { return &___temparray6_7; }
	inline void set_temparray6_7(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray6_7 = value;
		Il2CppCodeGenWriteBarrier((&___temparray6_7), value);
	}

	inline static int32_t get_offset_of_temparray7_8() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray7_8)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray7_8() const { return ___temparray7_8; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray7_8() { return &___temparray7_8; }
	inline void set_temparray7_8(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray7_8 = value;
		Il2CppCodeGenWriteBarrier((&___temparray7_8), value);
	}

	inline static int32_t get_offset_of_temparray8_9() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray8_9)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray8_9() const { return ___temparray8_9; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray8_9() { return &___temparray8_9; }
	inline void set_temparray8_9(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray8_9 = value;
		Il2CppCodeGenWriteBarrier((&___temparray8_9), value);
	}

	inline static int32_t get_offset_of_temparray9_10() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___temparray9_10)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_temparray9_10() const { return ___temparray9_10; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_temparray9_10() { return &___temparray9_10; }
	inline void set_temparray9_10(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___temparray9_10 = value;
		Il2CppCodeGenWriteBarrier((&___temparray9_10), value);
	}

	inline static int32_t get_offset_of_functionTable_11() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___functionTable_11)); }
	inline Hashtable_t1853889766 * get_functionTable_11() const { return ___functionTable_11; }
	inline Hashtable_t1853889766 ** get_address_of_functionTable_11() { return &___functionTable_11; }
	inline void set_functionTable_11(Hashtable_t1853889766 * value)
	{
		___functionTable_11 = value;
		Il2CppCodeGenWriteBarrier((&___functionTable_11), value);
	}

	inline static int32_t get_offset_of_AxesTable_12() { return static_cast<int32_t>(offsetof(XPathParser_t618394529_StaticFields, ___AxesTable_12)); }
	inline Hashtable_t1853889766 * get_AxesTable_12() const { return ___AxesTable_12; }
	inline Hashtable_t1853889766 ** get_address_of_AxesTable_12() { return &___AxesTable_12; }
	inline void set_AxesTable_12(Hashtable_t1853889766 * value)
	{
		___AxesTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___AxesTable_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHPARSER_T618394529_H
#ifndef INCREMENTALREADDECODER_T3011954239_H
#define INCREMENTALREADDECODER_T3011954239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.IncrementalReadDecoder
struct  IncrementalReadDecoder_t3011954239  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INCREMENTALREADDECODER_T3011954239_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef HASHALGORITHM_T1432317219_H
#define HASHALGORITHM_T1432317219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HashAlgorithm
struct  HashAlgorithm_t1432317219  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.HashAlgorithm::HashSizeValue
	int32_t ___HashSizeValue_0;
	// System.Byte[] System.Security.Cryptography.HashAlgorithm::HashValue
	ByteU5BU5D_t4116647657* ___HashValue_1;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::State
	int32_t ___State_2;
	// System.Boolean System.Security.Cryptography.HashAlgorithm::m_bDisposed
	bool ___m_bDisposed_3;

public:
	inline static int32_t get_offset_of_HashSizeValue_0() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___HashSizeValue_0)); }
	inline int32_t get_HashSizeValue_0() const { return ___HashSizeValue_0; }
	inline int32_t* get_address_of_HashSizeValue_0() { return &___HashSizeValue_0; }
	inline void set_HashSizeValue_0(int32_t value)
	{
		___HashSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_HashValue_1() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___HashValue_1)); }
	inline ByteU5BU5D_t4116647657* get_HashValue_1() const { return ___HashValue_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_HashValue_1() { return &___HashValue_1; }
	inline void set_HashValue_1(ByteU5BU5D_t4116647657* value)
	{
		___HashValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___HashValue_1), value);
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___State_2)); }
	inline int32_t get_State_2() const { return ___State_2; }
	inline int32_t* get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(int32_t value)
	{
		___State_2 = value;
	}

	inline static int32_t get_offset_of_m_bDisposed_3() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___m_bDisposed_3)); }
	inline bool get_m_bDisposed_3() const { return ___m_bDisposed_3; }
	inline bool* get_address_of_m_bDisposed_3() { return &___m_bDisposed_3; }
	inline void set_m_bDisposed_3(bool value)
	{
		___m_bDisposed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHM_T1432317219_H
#ifndef ENCODERFALLBACK_T1188251036_H
#define ENCODERFALLBACK_T1188251036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.EncoderFallback
struct  EncoderFallback_t1188251036  : public RuntimeObject
{
public:
	// System.Boolean System.Text.EncoderFallback::bIsMicrosoftBestFitFallback
	bool ___bIsMicrosoftBestFitFallback_0;

public:
	inline static int32_t get_offset_of_bIsMicrosoftBestFitFallback_0() { return static_cast<int32_t>(offsetof(EncoderFallback_t1188251036, ___bIsMicrosoftBestFitFallback_0)); }
	inline bool get_bIsMicrosoftBestFitFallback_0() const { return ___bIsMicrosoftBestFitFallback_0; }
	inline bool* get_address_of_bIsMicrosoftBestFitFallback_0() { return &___bIsMicrosoftBestFitFallback_0; }
	inline void set_bIsMicrosoftBestFitFallback_0(bool value)
	{
		___bIsMicrosoftBestFitFallback_0 = value;
	}
};

struct EncoderFallback_t1188251036_StaticFields
{
public:
	// System.Text.EncoderFallback modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.EncoderFallback::replacementFallback
	EncoderFallback_t1188251036 * ___replacementFallback_1;
	// System.Text.EncoderFallback modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.EncoderFallback::exceptionFallback
	EncoderFallback_t1188251036 * ___exceptionFallback_2;
	// System.Object System.Text.EncoderFallback::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_3;

public:
	inline static int32_t get_offset_of_replacementFallback_1() { return static_cast<int32_t>(offsetof(EncoderFallback_t1188251036_StaticFields, ___replacementFallback_1)); }
	inline EncoderFallback_t1188251036 * get_replacementFallback_1() const { return ___replacementFallback_1; }
	inline EncoderFallback_t1188251036 ** get_address_of_replacementFallback_1() { return &___replacementFallback_1; }
	inline void set_replacementFallback_1(EncoderFallback_t1188251036 * value)
	{
		___replacementFallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___replacementFallback_1), value);
	}

	inline static int32_t get_offset_of_exceptionFallback_2() { return static_cast<int32_t>(offsetof(EncoderFallback_t1188251036_StaticFields, ___exceptionFallback_2)); }
	inline EncoderFallback_t1188251036 * get_exceptionFallback_2() const { return ___exceptionFallback_2; }
	inline EncoderFallback_t1188251036 ** get_address_of_exceptionFallback_2() { return &___exceptionFallback_2; }
	inline void set_exceptionFallback_2(EncoderFallback_t1188251036 * value)
	{
		___exceptionFallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___exceptionFallback_2), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_3() { return static_cast<int32_t>(offsetof(EncoderFallback_t1188251036_StaticFields, ___s_InternalSyncObject_3)); }
	inline RuntimeObject * get_s_InternalSyncObject_3() const { return ___s_InternalSyncObject_3; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_3() { return &___s_InternalSyncObject_3; }
	inline void set_s_InternalSyncObject_3(RuntimeObject * value)
	{
		___s_InternalSyncObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODERFALLBACK_T1188251036_H
#ifndef XPATHITEM_T4250588140_H
#define XPATHITEM_T4250588140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathItem
struct  XPathItem_t4250588140  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHITEM_T4250588140_H
#ifndef ASYMMETRICALGORITHM_T932037087_H
#define ASYMMETRICALGORITHM_T932037087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsymmetricAlgorithm
struct  AsymmetricAlgorithm_t932037087  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.AsymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_0;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.AsymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t722666473* ___LegalKeySizesValue_1;

public:
	inline static int32_t get_offset_of_KeySizeValue_0() { return static_cast<int32_t>(offsetof(AsymmetricAlgorithm_t932037087, ___KeySizeValue_0)); }
	inline int32_t get_KeySizeValue_0() const { return ___KeySizeValue_0; }
	inline int32_t* get_address_of_KeySizeValue_0() { return &___KeySizeValue_0; }
	inline void set_KeySizeValue_0(int32_t value)
	{
		___KeySizeValue_0 = value;
	}

	inline static int32_t get_offset_of_LegalKeySizesValue_1() { return static_cast<int32_t>(offsetof(AsymmetricAlgorithm_t932037087, ___LegalKeySizesValue_1)); }
	inline KeySizesU5BU5D_t722666473* get_LegalKeySizesValue_1() const { return ___LegalKeySizesValue_1; }
	inline KeySizesU5BU5D_t722666473** get_address_of_LegalKeySizesValue_1() { return &___LegalKeySizesValue_1; }
	inline void set_LegalKeySizesValue_1(KeySizesU5BU5D_t722666473* value)
	{
		___LegalKeySizesValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___LegalKeySizesValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICALGORITHM_T932037087_H
#ifndef ENCODERFALLBACKBUFFER_T3523102303_H
#define ENCODERFALLBACKBUFFER_T3523102303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.EncoderFallbackBuffer
struct  EncoderFallbackBuffer_t3523102303  : public RuntimeObject
{
public:
	// System.Char* System.Text.EncoderFallbackBuffer::charStart
	Il2CppChar* ___charStart_0;
	// System.Char* System.Text.EncoderFallbackBuffer::charEnd
	Il2CppChar* ___charEnd_1;
	// System.Text.EncoderNLS System.Text.EncoderFallbackBuffer::encoder
	EncoderNLS_t449404832 * ___encoder_2;
	// System.Boolean System.Text.EncoderFallbackBuffer::setEncoder
	bool ___setEncoder_3;
	// System.Boolean System.Text.EncoderFallbackBuffer::bUsedEncoder
	bool ___bUsedEncoder_4;
	// System.Boolean System.Text.EncoderFallbackBuffer::bFallingBack
	bool ___bFallingBack_5;
	// System.Int32 System.Text.EncoderFallbackBuffer::iRecursionCount
	int32_t ___iRecursionCount_6;

public:
	inline static int32_t get_offset_of_charStart_0() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_t3523102303, ___charStart_0)); }
	inline Il2CppChar* get_charStart_0() const { return ___charStart_0; }
	inline Il2CppChar** get_address_of_charStart_0() { return &___charStart_0; }
	inline void set_charStart_0(Il2CppChar* value)
	{
		___charStart_0 = value;
	}

	inline static int32_t get_offset_of_charEnd_1() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_t3523102303, ___charEnd_1)); }
	inline Il2CppChar* get_charEnd_1() const { return ___charEnd_1; }
	inline Il2CppChar** get_address_of_charEnd_1() { return &___charEnd_1; }
	inline void set_charEnd_1(Il2CppChar* value)
	{
		___charEnd_1 = value;
	}

	inline static int32_t get_offset_of_encoder_2() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_t3523102303, ___encoder_2)); }
	inline EncoderNLS_t449404832 * get_encoder_2() const { return ___encoder_2; }
	inline EncoderNLS_t449404832 ** get_address_of_encoder_2() { return &___encoder_2; }
	inline void set_encoder_2(EncoderNLS_t449404832 * value)
	{
		___encoder_2 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_2), value);
	}

	inline static int32_t get_offset_of_setEncoder_3() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_t3523102303, ___setEncoder_3)); }
	inline bool get_setEncoder_3() const { return ___setEncoder_3; }
	inline bool* get_address_of_setEncoder_3() { return &___setEncoder_3; }
	inline void set_setEncoder_3(bool value)
	{
		___setEncoder_3 = value;
	}

	inline static int32_t get_offset_of_bUsedEncoder_4() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_t3523102303, ___bUsedEncoder_4)); }
	inline bool get_bUsedEncoder_4() const { return ___bUsedEncoder_4; }
	inline bool* get_address_of_bUsedEncoder_4() { return &___bUsedEncoder_4; }
	inline void set_bUsedEncoder_4(bool value)
	{
		___bUsedEncoder_4 = value;
	}

	inline static int32_t get_offset_of_bFallingBack_5() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_t3523102303, ___bFallingBack_5)); }
	inline bool get_bFallingBack_5() const { return ___bFallingBack_5; }
	inline bool* get_address_of_bFallingBack_5() { return &___bFallingBack_5; }
	inline void set_bFallingBack_5(bool value)
	{
		___bFallingBack_5 = value;
	}

	inline static int32_t get_offset_of_iRecursionCount_6() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_t3523102303, ___iRecursionCount_6)); }
	inline int32_t get_iRecursionCount_6() const { return ___iRecursionCount_6; }
	inline int32_t* get_address_of_iRecursionCount_6() { return &___iRecursionCount_6; }
	inline void set_iRecursionCount_6(int32_t value)
	{
		___iRecursionCount_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODERFALLBACKBUFFER_T3523102303_H
#ifndef BITSTACK_T371189938_H
#define BITSTACK_T371189938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BitStack
struct  BitStack_t371189938  : public RuntimeObject
{
public:
	// System.UInt32[] System.Xml.BitStack::bitStack
	UInt32U5BU5D_t2770800703* ___bitStack_0;
	// System.Int32 System.Xml.BitStack::stackPos
	int32_t ___stackPos_1;
	// System.UInt32 System.Xml.BitStack::curr
	uint32_t ___curr_2;

public:
	inline static int32_t get_offset_of_bitStack_0() { return static_cast<int32_t>(offsetof(BitStack_t371189938, ___bitStack_0)); }
	inline UInt32U5BU5D_t2770800703* get_bitStack_0() const { return ___bitStack_0; }
	inline UInt32U5BU5D_t2770800703** get_address_of_bitStack_0() { return &___bitStack_0; }
	inline void set_bitStack_0(UInt32U5BU5D_t2770800703* value)
	{
		___bitStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___bitStack_0), value);
	}

	inline static int32_t get_offset_of_stackPos_1() { return static_cast<int32_t>(offsetof(BitStack_t371189938, ___stackPos_1)); }
	inline int32_t get_stackPos_1() const { return ___stackPos_1; }
	inline int32_t* get_address_of_stackPos_1() { return &___stackPos_1; }
	inline void set_stackPos_1(int32_t value)
	{
		___stackPos_1 = value;
	}

	inline static int32_t get_offset_of_curr_2() { return static_cast<int32_t>(offsetof(BitStack_t371189938, ___curr_2)); }
	inline uint32_t get_curr_2() const { return ___curr_2; }
	inline uint32_t* get_address_of_curr_2() { return &___curr_2; }
	inline void set_curr_2(uint32_t value)
	{
		___curr_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITSTACK_T371189938_H
#ifndef BITS_T3566938933_H
#define BITS_T3566938933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Bits
struct  Bits_t3566938933  : public RuntimeObject
{
public:

public:
};

struct Bits_t3566938933_StaticFields
{
public:
	// System.UInt32 System.Xml.Bits::MASK_0101010101010101
	uint32_t ___MASK_0101010101010101_0;
	// System.UInt32 System.Xml.Bits::MASK_0011001100110011
	uint32_t ___MASK_0011001100110011_1;
	// System.UInt32 System.Xml.Bits::MASK_0000111100001111
	uint32_t ___MASK_0000111100001111_2;
	// System.UInt32 System.Xml.Bits::MASK_0000000011111111
	uint32_t ___MASK_0000000011111111_3;
	// System.UInt32 System.Xml.Bits::MASK_1111111111111111
	uint32_t ___MASK_1111111111111111_4;

public:
	inline static int32_t get_offset_of_MASK_0101010101010101_0() { return static_cast<int32_t>(offsetof(Bits_t3566938933_StaticFields, ___MASK_0101010101010101_0)); }
	inline uint32_t get_MASK_0101010101010101_0() const { return ___MASK_0101010101010101_0; }
	inline uint32_t* get_address_of_MASK_0101010101010101_0() { return &___MASK_0101010101010101_0; }
	inline void set_MASK_0101010101010101_0(uint32_t value)
	{
		___MASK_0101010101010101_0 = value;
	}

	inline static int32_t get_offset_of_MASK_0011001100110011_1() { return static_cast<int32_t>(offsetof(Bits_t3566938933_StaticFields, ___MASK_0011001100110011_1)); }
	inline uint32_t get_MASK_0011001100110011_1() const { return ___MASK_0011001100110011_1; }
	inline uint32_t* get_address_of_MASK_0011001100110011_1() { return &___MASK_0011001100110011_1; }
	inline void set_MASK_0011001100110011_1(uint32_t value)
	{
		___MASK_0011001100110011_1 = value;
	}

	inline static int32_t get_offset_of_MASK_0000111100001111_2() { return static_cast<int32_t>(offsetof(Bits_t3566938933_StaticFields, ___MASK_0000111100001111_2)); }
	inline uint32_t get_MASK_0000111100001111_2() const { return ___MASK_0000111100001111_2; }
	inline uint32_t* get_address_of_MASK_0000111100001111_2() { return &___MASK_0000111100001111_2; }
	inline void set_MASK_0000111100001111_2(uint32_t value)
	{
		___MASK_0000111100001111_2 = value;
	}

	inline static int32_t get_offset_of_MASK_0000000011111111_3() { return static_cast<int32_t>(offsetof(Bits_t3566938933_StaticFields, ___MASK_0000000011111111_3)); }
	inline uint32_t get_MASK_0000000011111111_3() const { return ___MASK_0000000011111111_3; }
	inline uint32_t* get_address_of_MASK_0000000011111111_3() { return &___MASK_0000000011111111_3; }
	inline void set_MASK_0000000011111111_3(uint32_t value)
	{
		___MASK_0000000011111111_3 = value;
	}

	inline static int32_t get_offset_of_MASK_1111111111111111_4() { return static_cast<int32_t>(offsetof(Bits_t3566938933_StaticFields, ___MASK_1111111111111111_4)); }
	inline uint32_t get_MASK_1111111111111111_4() const { return ___MASK_1111111111111111_4; }
	inline uint32_t* get_address_of_MASK_1111111111111111_4() { return &___MASK_1111111111111111_4; }
	inline void set_MASK_1111111111111111_4(uint32_t value)
	{
		___MASK_1111111111111111_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITS_T3566938933_H
#ifndef BINHEXENCODER_T1687308627_H
#define BINHEXENCODER_T1687308627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinHexEncoder
struct  BinHexEncoder_t1687308627  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINHEXENCODER_T1687308627_H
#ifndef BYTESTACK_T2013325889_H
#define BYTESTACK_T2013325889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ByteStack
struct  ByteStack_t2013325889  : public RuntimeObject
{
public:
	// System.Byte[] System.Xml.ByteStack::stack
	ByteU5BU5D_t4116647657* ___stack_0;
	// System.Int32 System.Xml.ByteStack::growthRate
	int32_t ___growthRate_1;
	// System.Int32 System.Xml.ByteStack::top
	int32_t ___top_2;
	// System.Int32 System.Xml.ByteStack::size
	int32_t ___size_3;

public:
	inline static int32_t get_offset_of_stack_0() { return static_cast<int32_t>(offsetof(ByteStack_t2013325889, ___stack_0)); }
	inline ByteU5BU5D_t4116647657* get_stack_0() const { return ___stack_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_stack_0() { return &___stack_0; }
	inline void set_stack_0(ByteU5BU5D_t4116647657* value)
	{
		___stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___stack_0), value);
	}

	inline static int32_t get_offset_of_growthRate_1() { return static_cast<int32_t>(offsetof(ByteStack_t2013325889, ___growthRate_1)); }
	inline int32_t get_growthRate_1() const { return ___growthRate_1; }
	inline int32_t* get_address_of_growthRate_1() { return &___growthRate_1; }
	inline void set_growthRate_1(int32_t value)
	{
		___growthRate_1 = value;
	}

	inline static int32_t get_offset_of_top_2() { return static_cast<int32_t>(offsetof(ByteStack_t2013325889, ___top_2)); }
	inline int32_t get_top_2() const { return ___top_2; }
	inline int32_t* get_address_of_top_2() { return &___top_2; }
	inline void set_top_2(int32_t value)
	{
		___top_2 = value;
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(ByteStack_t2013325889, ___size_3)); }
	inline int32_t get_size_3() const { return ___size_3; }
	inline int32_t* get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(int32_t value)
	{
		___size_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTESTACK_T2013325889_H
#ifndef X509EXTENSION_T3173393653_H
#define X509EXTENSION_T3173393653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Extension
struct  X509Extension_t3173393653  : public RuntimeObject
{
public:
	// System.String Mono.Security.X509.X509Extension::extnOid
	String_t* ___extnOid_0;
	// System.Boolean Mono.Security.X509.X509Extension::extnCritical
	bool ___extnCritical_1;
	// Mono.Security.ASN1 Mono.Security.X509.X509Extension::extnValue
	ASN1_t2114160833 * ___extnValue_2;

public:
	inline static int32_t get_offset_of_extnOid_0() { return static_cast<int32_t>(offsetof(X509Extension_t3173393653, ___extnOid_0)); }
	inline String_t* get_extnOid_0() const { return ___extnOid_0; }
	inline String_t** get_address_of_extnOid_0() { return &___extnOid_0; }
	inline void set_extnOid_0(String_t* value)
	{
		___extnOid_0 = value;
		Il2CppCodeGenWriteBarrier((&___extnOid_0), value);
	}

	inline static int32_t get_offset_of_extnCritical_1() { return static_cast<int32_t>(offsetof(X509Extension_t3173393653, ___extnCritical_1)); }
	inline bool get_extnCritical_1() const { return ___extnCritical_1; }
	inline bool* get_address_of_extnCritical_1() { return &___extnCritical_1; }
	inline void set_extnCritical_1(bool value)
	{
		___extnCritical_1 = value;
	}

	inline static int32_t get_offset_of_extnValue_2() { return static_cast<int32_t>(offsetof(X509Extension_t3173393653, ___extnValue_2)); }
	inline ASN1_t2114160833 * get_extnValue_2() const { return ___extnValue_2; }
	inline ASN1_t2114160833 ** get_address_of_extnValue_2() { return &___extnValue_2; }
	inline void set_extnValue_2(ASN1_t2114160833 * value)
	{
		___extnValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___extnValue_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSION_T3173393653_H
#ifndef HTMLTERNARYTREE_T1625978624_H
#define HTMLTERNARYTREE_T1625978624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HtmlTernaryTree
struct  HtmlTernaryTree_t1625978624  : public RuntimeObject
{
public:

public:
};

struct HtmlTernaryTree_t1625978624_StaticFields
{
public:
	// System.Byte[] System.Xml.HtmlTernaryTree::htmlElements
	ByteU5BU5D_t4116647657* ___htmlElements_0;
	// System.Byte[] System.Xml.HtmlTernaryTree::htmlAttributes
	ByteU5BU5D_t4116647657* ___htmlAttributes_1;

public:
	inline static int32_t get_offset_of_htmlElements_0() { return static_cast<int32_t>(offsetof(HtmlTernaryTree_t1625978624_StaticFields, ___htmlElements_0)); }
	inline ByteU5BU5D_t4116647657* get_htmlElements_0() const { return ___htmlElements_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_htmlElements_0() { return &___htmlElements_0; }
	inline void set_htmlElements_0(ByteU5BU5D_t4116647657* value)
	{
		___htmlElements_0 = value;
		Il2CppCodeGenWriteBarrier((&___htmlElements_0), value);
	}

	inline static int32_t get_offset_of_htmlAttributes_1() { return static_cast<int32_t>(offsetof(HtmlTernaryTree_t1625978624_StaticFields, ___htmlAttributes_1)); }
	inline ByteU5BU5D_t4116647657* get_htmlAttributes_1() const { return ___htmlAttributes_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_htmlAttributes_1() { return &___htmlAttributes_1; }
	inline void set_htmlAttributes_1(ByteU5BU5D_t4116647657* value)
	{
		___htmlAttributes_1 = value;
		Il2CppCodeGenWriteBarrier((&___htmlAttributes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLTERNARYTREE_T1625978624_H
#ifndef BINARYCOMPATIBILITY_T2660327299_H
#define BINARYCOMPATIBILITY_T2660327299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinaryCompatibility
struct  BinaryCompatibility_t2660327299  : public RuntimeObject
{
public:

public:
};

struct BinaryCompatibility_t2660327299_StaticFields
{
public:
	// System.Boolean System.Xml.BinaryCompatibility::_targetsAtLeast_Desktop_V4_5_2
	bool ____targetsAtLeast_Desktop_V4_5_2_0;

public:
	inline static int32_t get_offset_of__targetsAtLeast_Desktop_V4_5_2_0() { return static_cast<int32_t>(offsetof(BinaryCompatibility_t2660327299_StaticFields, ____targetsAtLeast_Desktop_V4_5_2_0)); }
	inline bool get__targetsAtLeast_Desktop_V4_5_2_0() const { return ____targetsAtLeast_Desktop_V4_5_2_0; }
	inline bool* get_address_of__targetsAtLeast_Desktop_V4_5_2_0() { return &____targetsAtLeast_Desktop_V4_5_2_0; }
	inline void set__targetsAtLeast_Desktop_V4_5_2_0(bool value)
	{
		____targetsAtLeast_Desktop_V4_5_2_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYCOMPATIBILITY_T2660327299_H
#ifndef PKCS1_T1505584677_H
#define PKCS1_T1505584677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS1
struct  PKCS1_t1505584677  : public RuntimeObject
{
public:

public:
};

struct PKCS1_t1505584677_StaticFields
{
public:
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA1
	ByteU5BU5D_t4116647657* ___emptySHA1_0;
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA256
	ByteU5BU5D_t4116647657* ___emptySHA256_1;
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA384
	ByteU5BU5D_t4116647657* ___emptySHA384_2;
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA512
	ByteU5BU5D_t4116647657* ___emptySHA512_3;

public:
	inline static int32_t get_offset_of_emptySHA1_0() { return static_cast<int32_t>(offsetof(PKCS1_t1505584677_StaticFields, ___emptySHA1_0)); }
	inline ByteU5BU5D_t4116647657* get_emptySHA1_0() const { return ___emptySHA1_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_emptySHA1_0() { return &___emptySHA1_0; }
	inline void set_emptySHA1_0(ByteU5BU5D_t4116647657* value)
	{
		___emptySHA1_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA1_0), value);
	}

	inline static int32_t get_offset_of_emptySHA256_1() { return static_cast<int32_t>(offsetof(PKCS1_t1505584677_StaticFields, ___emptySHA256_1)); }
	inline ByteU5BU5D_t4116647657* get_emptySHA256_1() const { return ___emptySHA256_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_emptySHA256_1() { return &___emptySHA256_1; }
	inline void set_emptySHA256_1(ByteU5BU5D_t4116647657* value)
	{
		___emptySHA256_1 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA256_1), value);
	}

	inline static int32_t get_offset_of_emptySHA384_2() { return static_cast<int32_t>(offsetof(PKCS1_t1505584677_StaticFields, ___emptySHA384_2)); }
	inline ByteU5BU5D_t4116647657* get_emptySHA384_2() const { return ___emptySHA384_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_emptySHA384_2() { return &___emptySHA384_2; }
	inline void set_emptySHA384_2(ByteU5BU5D_t4116647657* value)
	{
		___emptySHA384_2 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA384_2), value);
	}

	inline static int32_t get_offset_of_emptySHA512_3() { return static_cast<int32_t>(offsetof(PKCS1_t1505584677_StaticFields, ___emptySHA512_3)); }
	inline ByteU5BU5D_t4116647657* get_emptySHA512_3() const { return ___emptySHA512_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_emptySHA512_3() { return &___emptySHA512_3; }
	inline void set_emptySHA512_3(ByteU5BU5D_t4116647657* value)
	{
		___emptySHA512_3 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA512_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS1_T1505584677_H
#ifndef X509STORES_T1373936238_H
#define X509STORES_T1373936238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Stores
struct  X509Stores_t1373936238  : public RuntimeObject
{
public:
	// System.String Mono.Security.X509.X509Stores::_storePath
	String_t* ____storePath_0;
	// System.Boolean Mono.Security.X509.X509Stores::_newFormat
	bool ____newFormat_1;
	// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::_trusted
	X509Store_t2777415284 * ____trusted_2;

public:
	inline static int32_t get_offset_of__storePath_0() { return static_cast<int32_t>(offsetof(X509Stores_t1373936238, ____storePath_0)); }
	inline String_t* get__storePath_0() const { return ____storePath_0; }
	inline String_t** get_address_of__storePath_0() { return &____storePath_0; }
	inline void set__storePath_0(String_t* value)
	{
		____storePath_0 = value;
		Il2CppCodeGenWriteBarrier((&____storePath_0), value);
	}

	inline static int32_t get_offset_of__newFormat_1() { return static_cast<int32_t>(offsetof(X509Stores_t1373936238, ____newFormat_1)); }
	inline bool get__newFormat_1() const { return ____newFormat_1; }
	inline bool* get_address_of__newFormat_1() { return &____newFormat_1; }
	inline void set__newFormat_1(bool value)
	{
		____newFormat_1 = value;
	}

	inline static int32_t get_offset_of__trusted_2() { return static_cast<int32_t>(offsetof(X509Stores_t1373936238, ____trusted_2)); }
	inline X509Store_t2777415284 * get__trusted_2() const { return ____trusted_2; }
	inline X509Store_t2777415284 ** get_address_of__trusted_2() { return &____trusted_2; }
	inline void set__trusted_2(X509Store_t2777415284 * value)
	{
		____trusted_2 = value;
		Il2CppCodeGenWriteBarrier((&____trusted_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STORES_T1373936238_H
#ifndef PRIVATEKEYINFO_T668027993_H
#define PRIVATEKEYINFO_T668027993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
struct  PrivateKeyInfo_t668027993  : public RuntimeObject
{
public:
	// System.Int32 Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::_version
	int32_t ____version_0;
	// System.String Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::_algorithm
	String_t* ____algorithm_1;
	// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::_key
	ByteU5BU5D_t4116647657* ____key_2;
	// System.Collections.ArrayList Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::_list
	ArrayList_t2718874744 * ____list_3;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t668027993, ____version_0)); }
	inline int32_t get__version_0() const { return ____version_0; }
	inline int32_t* get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(int32_t value)
	{
		____version_0 = value;
	}

	inline static int32_t get_offset_of__algorithm_1() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t668027993, ____algorithm_1)); }
	inline String_t* get__algorithm_1() const { return ____algorithm_1; }
	inline String_t** get_address_of__algorithm_1() { return &____algorithm_1; }
	inline void set__algorithm_1(String_t* value)
	{
		____algorithm_1 = value;
		Il2CppCodeGenWriteBarrier((&____algorithm_1), value);
	}

	inline static int32_t get_offset_of__key_2() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t668027993, ____key_2)); }
	inline ByteU5BU5D_t4116647657* get__key_2() const { return ____key_2; }
	inline ByteU5BU5D_t4116647657** get_address_of__key_2() { return &____key_2; }
	inline void set__key_2(ByteU5BU5D_t4116647657* value)
	{
		____key_2 = value;
		Il2CppCodeGenWriteBarrier((&____key_2), value);
	}

	inline static int32_t get_offset_of__list_3() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t668027993, ____list_3)); }
	inline ArrayList_t2718874744 * get__list_3() const { return ____list_3; }
	inline ArrayList_t2718874744 ** get_address_of__list_3() { return &____list_3; }
	inline void set__list_3(ArrayList_t2718874744 * value)
	{
		____list_3 = value;
		Il2CppCodeGenWriteBarrier((&____list_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIVATEKEYINFO_T668027993_H
#ifndef PKCS8_T696280613_H
#define PKCS8_T696280613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS8
struct  PKCS8_t696280613  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS8_T696280613_H
#ifndef CRYPTOCONVERT_T610933157_H
#define CRYPTOCONVERT_T610933157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.CryptoConvert
struct  CryptoConvert_t610933157  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOCONVERT_T610933157_H
#ifndef BIGINTEGER_T2902905090_H
#define BIGINTEGER_T2902905090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.BigInteger
struct  BigInteger_t2902905090  : public RuntimeObject
{
public:
	// System.UInt32 Mono.Math.BigInteger::length
	uint32_t ___length_0;
	// System.UInt32[] Mono.Math.BigInteger::data
	UInt32U5BU5D_t2770800703* ___data_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(BigInteger_t2902905090, ___length_0)); }
	inline uint32_t get_length_0() const { return ___length_0; }
	inline uint32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(uint32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(BigInteger_t2902905090, ___data_1)); }
	inline UInt32U5BU5D_t2770800703* get_data_1() const { return ___data_1; }
	inline UInt32U5BU5D_t2770800703** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(UInt32U5BU5D_t2770800703* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

struct BigInteger_t2902905090_StaticFields
{
public:
	// System.UInt32[] Mono.Math.BigInteger::smallPrimes
	UInt32U5BU5D_t2770800703* ___smallPrimes_2;
	// System.Security.Cryptography.RandomNumberGenerator Mono.Math.BigInteger::rng
	RandomNumberGenerator_t386037858 * ___rng_3;

public:
	inline static int32_t get_offset_of_smallPrimes_2() { return static_cast<int32_t>(offsetof(BigInteger_t2902905090_StaticFields, ___smallPrimes_2)); }
	inline UInt32U5BU5D_t2770800703* get_smallPrimes_2() const { return ___smallPrimes_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of_smallPrimes_2() { return &___smallPrimes_2; }
	inline void set_smallPrimes_2(UInt32U5BU5D_t2770800703* value)
	{
		___smallPrimes_2 = value;
		Il2CppCodeGenWriteBarrier((&___smallPrimes_2), value);
	}

	inline static int32_t get_offset_of_rng_3() { return static_cast<int32_t>(offsetof(BigInteger_t2902905090_StaticFields, ___rng_3)); }
	inline RandomNumberGenerator_t386037858 * get_rng_3() const { return ___rng_3; }
	inline RandomNumberGenerator_t386037858 ** get_address_of_rng_3() { return &___rng_3; }
	inline void set_rng_3(RandomNumberGenerator_t386037858 * value)
	{
		___rng_3 = value;
		Il2CppCodeGenWriteBarrier((&___rng_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BIGINTEGER_T2902905090_H
#ifndef KEYPAIRPERSISTENCE_T2094547462_H
#define KEYPAIRPERSISTENCE_T2094547462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.KeyPairPersistence
struct  KeyPairPersistence_t2094547462  : public RuntimeObject
{
public:
	// System.Security.Cryptography.CspParameters Mono.Security.Cryptography.KeyPairPersistence::_params
	CspParameters_t239852639 * ____params_4;
	// System.String Mono.Security.Cryptography.KeyPairPersistence::_keyvalue
	String_t* ____keyvalue_5;
	// System.String Mono.Security.Cryptography.KeyPairPersistence::_filename
	String_t* ____filename_6;
	// System.String Mono.Security.Cryptography.KeyPairPersistence::_container
	String_t* ____container_7;

public:
	inline static int32_t get_offset_of__params_4() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462, ____params_4)); }
	inline CspParameters_t239852639 * get__params_4() const { return ____params_4; }
	inline CspParameters_t239852639 ** get_address_of__params_4() { return &____params_4; }
	inline void set__params_4(CspParameters_t239852639 * value)
	{
		____params_4 = value;
		Il2CppCodeGenWriteBarrier((&____params_4), value);
	}

	inline static int32_t get_offset_of__keyvalue_5() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462, ____keyvalue_5)); }
	inline String_t* get__keyvalue_5() const { return ____keyvalue_5; }
	inline String_t** get_address_of__keyvalue_5() { return &____keyvalue_5; }
	inline void set__keyvalue_5(String_t* value)
	{
		____keyvalue_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyvalue_5), value);
	}

	inline static int32_t get_offset_of__filename_6() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462, ____filename_6)); }
	inline String_t* get__filename_6() const { return ____filename_6; }
	inline String_t** get_address_of__filename_6() { return &____filename_6; }
	inline void set__filename_6(String_t* value)
	{
		____filename_6 = value;
		Il2CppCodeGenWriteBarrier((&____filename_6), value);
	}

	inline static int32_t get_offset_of__container_7() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462, ____container_7)); }
	inline String_t* get__container_7() const { return ____container_7; }
	inline String_t** get_address_of__container_7() { return &____container_7; }
	inline void set__container_7(String_t* value)
	{
		____container_7 = value;
		Il2CppCodeGenWriteBarrier((&____container_7), value);
	}
};

struct KeyPairPersistence_t2094547462_StaticFields
{
public:
	// System.Boolean Mono.Security.Cryptography.KeyPairPersistence::_userPathExists
	bool ____userPathExists_0;
	// System.String Mono.Security.Cryptography.KeyPairPersistence::_userPath
	String_t* ____userPath_1;
	// System.Boolean Mono.Security.Cryptography.KeyPairPersistence::_machinePathExists
	bool ____machinePathExists_2;
	// System.String Mono.Security.Cryptography.KeyPairPersistence::_machinePath
	String_t* ____machinePath_3;
	// System.Object Mono.Security.Cryptography.KeyPairPersistence::lockobj
	RuntimeObject * ___lockobj_8;

public:
	inline static int32_t get_offset_of__userPathExists_0() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462_StaticFields, ____userPathExists_0)); }
	inline bool get__userPathExists_0() const { return ____userPathExists_0; }
	inline bool* get_address_of__userPathExists_0() { return &____userPathExists_0; }
	inline void set__userPathExists_0(bool value)
	{
		____userPathExists_0 = value;
	}

	inline static int32_t get_offset_of__userPath_1() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462_StaticFields, ____userPath_1)); }
	inline String_t* get__userPath_1() const { return ____userPath_1; }
	inline String_t** get_address_of__userPath_1() { return &____userPath_1; }
	inline void set__userPath_1(String_t* value)
	{
		____userPath_1 = value;
		Il2CppCodeGenWriteBarrier((&____userPath_1), value);
	}

	inline static int32_t get_offset_of__machinePathExists_2() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462_StaticFields, ____machinePathExists_2)); }
	inline bool get__machinePathExists_2() const { return ____machinePathExists_2; }
	inline bool* get_address_of__machinePathExists_2() { return &____machinePathExists_2; }
	inline void set__machinePathExists_2(bool value)
	{
		____machinePathExists_2 = value;
	}

	inline static int32_t get_offset_of__machinePath_3() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462_StaticFields, ____machinePath_3)); }
	inline String_t* get__machinePath_3() const { return ____machinePath_3; }
	inline String_t** get_address_of__machinePath_3() { return &____machinePath_3; }
	inline void set__machinePath_3(String_t* value)
	{
		____machinePath_3 = value;
		Il2CppCodeGenWriteBarrier((&____machinePath_3), value);
	}

	inline static int32_t get_offset_of_lockobj_8() { return static_cast<int32_t>(offsetof(KeyPairPersistence_t2094547462_StaticFields, ___lockobj_8)); }
	inline RuntimeObject * get_lockobj_8() const { return ___lockobj_8; }
	inline RuntimeObject ** get_address_of_lockobj_8() { return &___lockobj_8; }
	inline void set_lockobj_8(RuntimeObject * value)
	{
		___lockobj_8 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYPAIRPERSISTENCE_T2094547462_H
#ifndef KEYBUILDER_T2049230355_H
#define KEYBUILDER_T2049230355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.KeyBuilder
struct  KeyBuilder_t2049230355  : public RuntimeObject
{
public:

public:
};

struct KeyBuilder_t2049230355_StaticFields
{
public:
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.KeyBuilder::rng
	RandomNumberGenerator_t386037858 * ___rng_0;

public:
	inline static int32_t get_offset_of_rng_0() { return static_cast<int32_t>(offsetof(KeyBuilder_t2049230355_StaticFields, ___rng_0)); }
	inline RandomNumberGenerator_t386037858 * get_rng_0() const { return ___rng_0; }
	inline RandomNumberGenerator_t386037858 ** get_address_of_rng_0() { return &___rng_0; }
	inline void set_rng_0(RandomNumberGenerator_t386037858 * value)
	{
		___rng_0 = value;
		Il2CppCodeGenWriteBarrier((&___rng_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBUILDER_T2049230355_H
#ifndef MODULUSRING_T596511505_H
#define MODULUSRING_T596511505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.BigInteger/ModulusRing
struct  ModulusRing_t596511505  : public RuntimeObject
{
public:
	// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::mod
	BigInteger_t2902905090 * ___mod_0;
	// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::constant
	BigInteger_t2902905090 * ___constant_1;

public:
	inline static int32_t get_offset_of_mod_0() { return static_cast<int32_t>(offsetof(ModulusRing_t596511505, ___mod_0)); }
	inline BigInteger_t2902905090 * get_mod_0() const { return ___mod_0; }
	inline BigInteger_t2902905090 ** get_address_of_mod_0() { return &___mod_0; }
	inline void set_mod_0(BigInteger_t2902905090 * value)
	{
		___mod_0 = value;
		Il2CppCodeGenWriteBarrier((&___mod_0), value);
	}

	inline static int32_t get_offset_of_constant_1() { return static_cast<int32_t>(offsetof(ModulusRing_t596511505, ___constant_1)); }
	inline BigInteger_t2902905090 * get_constant_1() const { return ___constant_1; }
	inline BigInteger_t2902905090 ** get_address_of_constant_1() { return &___constant_1; }
	inline void set_constant_1(BigInteger_t2902905090 * value)
	{
		___constant_1 = value;
		Il2CppCodeGenWriteBarrier((&___constant_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODULUSRING_T596511505_H
#ifndef GENERALNAMES_T2702294159_H
#define GENERALNAMES_T2702294159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.GeneralNames
struct  GeneralNames_t2702294159  : public RuntimeObject
{
public:
	// System.Collections.ArrayList Mono.Security.X509.Extensions.GeneralNames::rfc822Name
	ArrayList_t2718874744 * ___rfc822Name_0;
	// System.Collections.ArrayList Mono.Security.X509.Extensions.GeneralNames::dnsName
	ArrayList_t2718874744 * ___dnsName_1;
	// System.Collections.ArrayList Mono.Security.X509.Extensions.GeneralNames::directoryNames
	ArrayList_t2718874744 * ___directoryNames_2;
	// System.Collections.ArrayList Mono.Security.X509.Extensions.GeneralNames::uris
	ArrayList_t2718874744 * ___uris_3;
	// System.Collections.ArrayList Mono.Security.X509.Extensions.GeneralNames::ipAddr
	ArrayList_t2718874744 * ___ipAddr_4;

public:
	inline static int32_t get_offset_of_rfc822Name_0() { return static_cast<int32_t>(offsetof(GeneralNames_t2702294159, ___rfc822Name_0)); }
	inline ArrayList_t2718874744 * get_rfc822Name_0() const { return ___rfc822Name_0; }
	inline ArrayList_t2718874744 ** get_address_of_rfc822Name_0() { return &___rfc822Name_0; }
	inline void set_rfc822Name_0(ArrayList_t2718874744 * value)
	{
		___rfc822Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___rfc822Name_0), value);
	}

	inline static int32_t get_offset_of_dnsName_1() { return static_cast<int32_t>(offsetof(GeneralNames_t2702294159, ___dnsName_1)); }
	inline ArrayList_t2718874744 * get_dnsName_1() const { return ___dnsName_1; }
	inline ArrayList_t2718874744 ** get_address_of_dnsName_1() { return &___dnsName_1; }
	inline void set_dnsName_1(ArrayList_t2718874744 * value)
	{
		___dnsName_1 = value;
		Il2CppCodeGenWriteBarrier((&___dnsName_1), value);
	}

	inline static int32_t get_offset_of_directoryNames_2() { return static_cast<int32_t>(offsetof(GeneralNames_t2702294159, ___directoryNames_2)); }
	inline ArrayList_t2718874744 * get_directoryNames_2() const { return ___directoryNames_2; }
	inline ArrayList_t2718874744 ** get_address_of_directoryNames_2() { return &___directoryNames_2; }
	inline void set_directoryNames_2(ArrayList_t2718874744 * value)
	{
		___directoryNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___directoryNames_2), value);
	}

	inline static int32_t get_offset_of_uris_3() { return static_cast<int32_t>(offsetof(GeneralNames_t2702294159, ___uris_3)); }
	inline ArrayList_t2718874744 * get_uris_3() const { return ___uris_3; }
	inline ArrayList_t2718874744 ** get_address_of_uris_3() { return &___uris_3; }
	inline void set_uris_3(ArrayList_t2718874744 * value)
	{
		___uris_3 = value;
		Il2CppCodeGenWriteBarrier((&___uris_3), value);
	}

	inline static int32_t get_offset_of_ipAddr_4() { return static_cast<int32_t>(offsetof(GeneralNames_t2702294159, ___ipAddr_4)); }
	inline ArrayList_t2718874744 * get_ipAddr_4() const { return ___ipAddr_4; }
	inline ArrayList_t2718874744 ** get_address_of_ipAddr_4() { return &___ipAddr_4; }
	inline void set_ipAddr_4(ArrayList_t2718874744 * value)
	{
		___ipAddr_4 = value;
		Il2CppCodeGenWriteBarrier((&___ipAddr_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALNAMES_T2702294159_H
#ifndef KERNEL_T1402667220_H
#define KERNEL_T1402667220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.BigInteger/Kernel
struct  Kernel_t1402667220  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNEL_T1402667220_H
#ifndef ENCRYPTEDPRIVATEKEYINFO_T862116836_H
#define ENCRYPTEDPRIVATEKEYINFO_T862116836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo
struct  EncryptedPrivateKeyInfo_t862116836  : public RuntimeObject
{
public:
	// System.String Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo::_algorithm
	String_t* ____algorithm_0;
	// System.Byte[] Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo::_salt
	ByteU5BU5D_t4116647657* ____salt_1;
	// System.Int32 Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo::_iterations
	int32_t ____iterations_2;
	// System.Byte[] Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo::_data
	ByteU5BU5D_t4116647657* ____data_3;

public:
	inline static int32_t get_offset_of__algorithm_0() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_t862116836, ____algorithm_0)); }
	inline String_t* get__algorithm_0() const { return ____algorithm_0; }
	inline String_t** get_address_of__algorithm_0() { return &____algorithm_0; }
	inline void set__algorithm_0(String_t* value)
	{
		____algorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&____algorithm_0), value);
	}

	inline static int32_t get_offset_of__salt_1() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_t862116836, ____salt_1)); }
	inline ByteU5BU5D_t4116647657* get__salt_1() const { return ____salt_1; }
	inline ByteU5BU5D_t4116647657** get_address_of__salt_1() { return &____salt_1; }
	inline void set__salt_1(ByteU5BU5D_t4116647657* value)
	{
		____salt_1 = value;
		Il2CppCodeGenWriteBarrier((&____salt_1), value);
	}

	inline static int32_t get_offset_of__iterations_2() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_t862116836, ____iterations_2)); }
	inline int32_t get__iterations_2() const { return ____iterations_2; }
	inline int32_t* get_address_of__iterations_2() { return &____iterations_2; }
	inline void set__iterations_2(int32_t value)
	{
		____iterations_2 = value;
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_t862116836, ____data_3)); }
	inline ByteU5BU5D_t4116647657* get__data_3() const { return ____data_3; }
	inline ByteU5BU5D_t4116647657** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(ByteU5BU5D_t4116647657* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTEDPRIVATEKEYINFO_T862116836_H
#ifndef X509STOREMANAGER_T1046782376_H
#define X509STOREMANAGER_T1046782376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509StoreManager
struct  X509StoreManager_t1046782376  : public RuntimeObject
{
public:

public:
};

struct X509StoreManager_t1046782376_StaticFields
{
public:
	// System.String Mono.Security.X509.X509StoreManager::_userPath
	String_t* ____userPath_0;
	// System.String Mono.Security.X509.X509StoreManager::_localMachinePath
	String_t* ____localMachinePath_1;
	// System.String Mono.Security.X509.X509StoreManager::_newLocalMachinePath
	String_t* ____newLocalMachinePath_2;
	// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::_userStore
	X509Stores_t1373936238 * ____userStore_3;
	// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::_machineStore
	X509Stores_t1373936238 * ____machineStore_4;

public:
	inline static int32_t get_offset_of__userPath_0() { return static_cast<int32_t>(offsetof(X509StoreManager_t1046782376_StaticFields, ____userPath_0)); }
	inline String_t* get__userPath_0() const { return ____userPath_0; }
	inline String_t** get_address_of__userPath_0() { return &____userPath_0; }
	inline void set__userPath_0(String_t* value)
	{
		____userPath_0 = value;
		Il2CppCodeGenWriteBarrier((&____userPath_0), value);
	}

	inline static int32_t get_offset_of__localMachinePath_1() { return static_cast<int32_t>(offsetof(X509StoreManager_t1046782376_StaticFields, ____localMachinePath_1)); }
	inline String_t* get__localMachinePath_1() const { return ____localMachinePath_1; }
	inline String_t** get_address_of__localMachinePath_1() { return &____localMachinePath_1; }
	inline void set__localMachinePath_1(String_t* value)
	{
		____localMachinePath_1 = value;
		Il2CppCodeGenWriteBarrier((&____localMachinePath_1), value);
	}

	inline static int32_t get_offset_of__newLocalMachinePath_2() { return static_cast<int32_t>(offsetof(X509StoreManager_t1046782376_StaticFields, ____newLocalMachinePath_2)); }
	inline String_t* get__newLocalMachinePath_2() const { return ____newLocalMachinePath_2; }
	inline String_t** get_address_of__newLocalMachinePath_2() { return &____newLocalMachinePath_2; }
	inline void set__newLocalMachinePath_2(String_t* value)
	{
		____newLocalMachinePath_2 = value;
		Il2CppCodeGenWriteBarrier((&____newLocalMachinePath_2), value);
	}

	inline static int32_t get_offset_of__userStore_3() { return static_cast<int32_t>(offsetof(X509StoreManager_t1046782376_StaticFields, ____userStore_3)); }
	inline X509Stores_t1373936238 * get__userStore_3() const { return ____userStore_3; }
	inline X509Stores_t1373936238 ** get_address_of__userStore_3() { return &____userStore_3; }
	inline void set__userStore_3(X509Stores_t1373936238 * value)
	{
		____userStore_3 = value;
		Il2CppCodeGenWriteBarrier((&____userStore_3), value);
	}

	inline static int32_t get_offset_of__machineStore_4() { return static_cast<int32_t>(offsetof(X509StoreManager_t1046782376_StaticFields, ____machineStore_4)); }
	inline X509Stores_t1373936238 * get__machineStore_4() const { return ____machineStore_4; }
	inline X509Stores_t1373936238 ** get_address_of__machineStore_4() { return &____machineStore_4; }
	inline void set__machineStore_4(X509Stores_t1373936238 * value)
	{
		____machineStore_4 = value;
		Il2CppCodeGenWriteBarrier((&____machineStore_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STOREMANAGER_T1046782376_H
#ifndef XMLRAWWRITERBASE64ENCODER_T1905751661_H
#define XMLRAWWRITERBASE64ENCODER_T1905751661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlRawWriterBase64Encoder
struct  XmlRawWriterBase64Encoder_t1905751661  : public Base64Encoder_t3938083961
{
public:
	// System.Xml.XmlRawWriter System.Xml.XmlRawWriterBase64Encoder::rawWriter
	XmlRawWriter_t722320575 * ___rawWriter_3;

public:
	inline static int32_t get_offset_of_rawWriter_3() { return static_cast<int32_t>(offsetof(XmlRawWriterBase64Encoder_t1905751661, ___rawWriter_3)); }
	inline XmlRawWriter_t722320575 * get_rawWriter_3() const { return ___rawWriter_3; }
	inline XmlRawWriter_t722320575 ** get_address_of_rawWriter_3() { return &___rawWriter_3; }
	inline void set_rawWriter_3(XmlRawWriter_t722320575 * value)
	{
		___rawWriter_3 = value;
		Il2CppCodeGenWriteBarrier((&___rawWriter_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLRAWWRITERBASE64ENCODER_T1905751661_H
#ifndef XPATHNODE_T2208072876_H
#define XPATHNODE_T2208072876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNode
struct  XPathNode_t2208072876 
{
public:
	// MS.Internal.Xml.Cache.XPathNodeInfoAtom MS.Internal.Xml.Cache.XPathNode::info
	XPathNodeInfoAtom_t1760358141 * ___info_0;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::idxSibling
	uint16_t ___idxSibling_1;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::idxParent
	uint16_t ___idxParent_2;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::idxSimilar
	uint16_t ___idxSimilar_3;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::posOffset
	uint16_t ___posOffset_4;
	// System.UInt32 MS.Internal.Xml.Cache.XPathNode::props
	uint32_t ___props_5;
	// System.String MS.Internal.Xml.Cache.XPathNode::value
	String_t* ___value_6;

public:
	inline static int32_t get_offset_of_info_0() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___info_0)); }
	inline XPathNodeInfoAtom_t1760358141 * get_info_0() const { return ___info_0; }
	inline XPathNodeInfoAtom_t1760358141 ** get_address_of_info_0() { return &___info_0; }
	inline void set_info_0(XPathNodeInfoAtom_t1760358141 * value)
	{
		___info_0 = value;
		Il2CppCodeGenWriteBarrier((&___info_0), value);
	}

	inline static int32_t get_offset_of_idxSibling_1() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___idxSibling_1)); }
	inline uint16_t get_idxSibling_1() const { return ___idxSibling_1; }
	inline uint16_t* get_address_of_idxSibling_1() { return &___idxSibling_1; }
	inline void set_idxSibling_1(uint16_t value)
	{
		___idxSibling_1 = value;
	}

	inline static int32_t get_offset_of_idxParent_2() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___idxParent_2)); }
	inline uint16_t get_idxParent_2() const { return ___idxParent_2; }
	inline uint16_t* get_address_of_idxParent_2() { return &___idxParent_2; }
	inline void set_idxParent_2(uint16_t value)
	{
		___idxParent_2 = value;
	}

	inline static int32_t get_offset_of_idxSimilar_3() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___idxSimilar_3)); }
	inline uint16_t get_idxSimilar_3() const { return ___idxSimilar_3; }
	inline uint16_t* get_address_of_idxSimilar_3() { return &___idxSimilar_3; }
	inline void set_idxSimilar_3(uint16_t value)
	{
		___idxSimilar_3 = value;
	}

	inline static int32_t get_offset_of_posOffset_4() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___posOffset_4)); }
	inline uint16_t get_posOffset_4() const { return ___posOffset_4; }
	inline uint16_t* get_address_of_posOffset_4() { return &___posOffset_4; }
	inline void set_posOffset_4(uint16_t value)
	{
		___posOffset_4 = value;
	}

	inline static int32_t get_offset_of_props_5() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___props_5)); }
	inline uint32_t get_props_5() const { return ___props_5; }
	inline uint32_t* get_address_of_props_5() { return &___props_5; }
	inline void set_props_5(uint32_t value)
	{
		___props_5 = value;
	}

	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(XPathNode_t2208072876, ___value_6)); }
	inline String_t* get_value_6() const { return ___value_6; }
	inline String_t** get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(String_t* value)
	{
		___value_6 = value;
		Il2CppCodeGenWriteBarrier((&___value_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MS.Internal.Xml.Cache.XPathNode
struct XPathNode_t2208072876_marshaled_pinvoke
{
	XPathNodeInfoAtom_t1760358141 * ___info_0;
	uint16_t ___idxSibling_1;
	uint16_t ___idxParent_2;
	uint16_t ___idxSimilar_3;
	uint16_t ___posOffset_4;
	uint32_t ___props_5;
	char* ___value_6;
};
// Native definition for COM marshalling of MS.Internal.Xml.Cache.XPathNode
struct XPathNode_t2208072876_marshaled_com
{
	XPathNodeInfoAtom_t1760358141 * ___info_0;
	uint16_t ___idxSibling_1;
	uint16_t ___idxParent_2;
	uint16_t ___idxSimilar_3;
	uint16_t ___posOffset_4;
	uint32_t ___props_5;
	Il2CppChar* ___value_6;
};
#endif // XPATHNODE_T2208072876_H
#ifndef XMLTEXTWRITERBASE64ENCODER_T4259465041_H
#define XMLTEXTWRITERBASE64ENCODER_T4259465041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriterBase64Encoder
struct  XmlTextWriterBase64Encoder_t4259465041  : public Base64Encoder_t3938083961
{
public:
	// System.Xml.XmlTextEncoder System.Xml.XmlTextWriterBase64Encoder::xmlTextEncoder
	XmlTextEncoder_t1632274355 * ___xmlTextEncoder_3;

public:
	inline static int32_t get_offset_of_xmlTextEncoder_3() { return static_cast<int32_t>(offsetof(XmlTextWriterBase64Encoder_t4259465041, ___xmlTextEncoder_3)); }
	inline XmlTextEncoder_t1632274355 * get_xmlTextEncoder_3() const { return ___xmlTextEncoder_3; }
	inline XmlTextEncoder_t1632274355 ** get_address_of_xmlTextEncoder_3() { return &___xmlTextEncoder_3; }
	inline void set_xmlTextEncoder_3(XmlTextEncoder_t1632274355 * value)
	{
		___xmlTextEncoder_3 = value;
		Il2CppCodeGenWriteBarrier((&___xmlTextEncoder_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTWRITERBASE64ENCODER_T4259465041_H
#ifndef BASICCONSTRAINTSEXTENSION_T2462195279_H
#define BASICCONSTRAINTSEXTENSION_T2462195279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.BasicConstraintsExtension
struct  BasicConstraintsExtension_t2462195279  : public X509Extension_t3173393653
{
public:
	// System.Boolean Mono.Security.X509.Extensions.BasicConstraintsExtension::cA
	bool ___cA_3;
	// System.Int32 Mono.Security.X509.Extensions.BasicConstraintsExtension::pathLenConstraint
	int32_t ___pathLenConstraint_4;

public:
	inline static int32_t get_offset_of_cA_3() { return static_cast<int32_t>(offsetof(BasicConstraintsExtension_t2462195279, ___cA_3)); }
	inline bool get_cA_3() const { return ___cA_3; }
	inline bool* get_address_of_cA_3() { return &___cA_3; }
	inline void set_cA_3(bool value)
	{
		___cA_3 = value;
	}

	inline static int32_t get_offset_of_pathLenConstraint_4() { return static_cast<int32_t>(offsetof(BasicConstraintsExtension_t2462195279, ___pathLenConstraint_4)); }
	inline int32_t get_pathLenConstraint_4() const { return ___pathLenConstraint_4; }
	inline int32_t* get_address_of_pathLenConstraint_4() { return &___pathLenConstraint_4; }
	inline void set_pathLenConstraint_4(int32_t value)
	{
		___pathLenConstraint_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCONSTRAINTSEXTENSION_T2462195279_H
#ifndef XPATHNODEREF_T3498189018_H
#define XPATHNODEREF_T3498189018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodeRef
struct  XPathNodeRef_t3498189018 
{
public:
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeRef::page
	XPathNodeU5BU5D_t47339301* ___page_0;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeRef::idx
	int32_t ___idx_1;

public:
	inline static int32_t get_offset_of_page_0() { return static_cast<int32_t>(offsetof(XPathNodeRef_t3498189018, ___page_0)); }
	inline XPathNodeU5BU5D_t47339301* get_page_0() const { return ___page_0; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_page_0() { return &___page_0; }
	inline void set_page_0(XPathNodeU5BU5D_t47339301* value)
	{
		___page_0 = value;
		Il2CppCodeGenWriteBarrier((&___page_0), value);
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(XPathNodeRef_t3498189018, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MS.Internal.Xml.Cache.XPathNodeRef
struct XPathNodeRef_t3498189018_marshaled_pinvoke
{
	XPathNode_t2208072876_marshaled_pinvoke* ___page_0;
	int32_t ___idx_1;
};
// Native definition for COM marshalling of MS.Internal.Xml.Cache.XPathNodeRef
struct XPathNodeRef_t3498189018_marshaled_com
{
	XPathNode_t2208072876_marshaled_com* ___page_0;
	int32_t ___idx_1;
};
#endif // XPATHNODEREF_T3498189018_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef AUTHORITYKEYIDENTIFIEREXTENSION_T1122691429_H
#define AUTHORITYKEYIDENTIFIEREXTENSION_T1122691429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension
struct  AuthorityKeyIdentifierExtension_t1122691429  : public X509Extension_t3173393653
{
public:
	// System.Byte[] Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension::aki
	ByteU5BU5D_t4116647657* ___aki_3;

public:
	inline static int32_t get_offset_of_aki_3() { return static_cast<int32_t>(offsetof(AuthorityKeyIdentifierExtension_t1122691429, ___aki_3)); }
	inline ByteU5BU5D_t4116647657* get_aki_3() const { return ___aki_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_aki_3() { return &___aki_3; }
	inline void set_aki_3(ByteU5BU5D_t4116647657* value)
	{
		___aki_3 = value;
		Il2CppCodeGenWriteBarrier((&___aki_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHORITYKEYIDENTIFIEREXTENSION_T1122691429_H
#ifndef RSA_T2385438082_H
#define RSA_T2385438082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSA
struct  RSA_t2385438082  : public AsymmetricAlgorithm_t932037087
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSA_T2385438082_H
#ifndef XPATHNAVIGATOR_T787956054_H
#define XPATHNAVIGATOR_T787956054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigator
struct  XPathNavigator_t787956054  : public XPathItem_t4250588140
{
public:

public:
};

struct XPathNavigator_t787956054_StaticFields
{
public:
	// System.Xml.XPath.XPathNavigatorKeyComparer System.Xml.XPath.XPathNavigator::comparer
	XPathNavigatorKeyComparer_t2518900029 * ___comparer_0;
	// System.Char[] System.Xml.XPath.XPathNavigator::NodeTypeLetter
	CharU5BU5D_t3528271667* ___NodeTypeLetter_1;
	// System.Char[] System.Xml.XPath.XPathNavigator::UniqueIdTbl
	CharU5BU5D_t3528271667* ___UniqueIdTbl_2;
	// System.Int32[] System.Xml.XPath.XPathNavigator::ContentKindMasks
	Int32U5BU5D_t385246372* ___ContentKindMasks_3;

public:
	inline static int32_t get_offset_of_comparer_0() { return static_cast<int32_t>(offsetof(XPathNavigator_t787956054_StaticFields, ___comparer_0)); }
	inline XPathNavigatorKeyComparer_t2518900029 * get_comparer_0() const { return ___comparer_0; }
	inline XPathNavigatorKeyComparer_t2518900029 ** get_address_of_comparer_0() { return &___comparer_0; }
	inline void set_comparer_0(XPathNavigatorKeyComparer_t2518900029 * value)
	{
		___comparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_0), value);
	}

	inline static int32_t get_offset_of_NodeTypeLetter_1() { return static_cast<int32_t>(offsetof(XPathNavigator_t787956054_StaticFields, ___NodeTypeLetter_1)); }
	inline CharU5BU5D_t3528271667* get_NodeTypeLetter_1() const { return ___NodeTypeLetter_1; }
	inline CharU5BU5D_t3528271667** get_address_of_NodeTypeLetter_1() { return &___NodeTypeLetter_1; }
	inline void set_NodeTypeLetter_1(CharU5BU5D_t3528271667* value)
	{
		___NodeTypeLetter_1 = value;
		Il2CppCodeGenWriteBarrier((&___NodeTypeLetter_1), value);
	}

	inline static int32_t get_offset_of_UniqueIdTbl_2() { return static_cast<int32_t>(offsetof(XPathNavigator_t787956054_StaticFields, ___UniqueIdTbl_2)); }
	inline CharU5BU5D_t3528271667* get_UniqueIdTbl_2() const { return ___UniqueIdTbl_2; }
	inline CharU5BU5D_t3528271667** get_address_of_UniqueIdTbl_2() { return &___UniqueIdTbl_2; }
	inline void set_UniqueIdTbl_2(CharU5BU5D_t3528271667* value)
	{
		___UniqueIdTbl_2 = value;
		Il2CppCodeGenWriteBarrier((&___UniqueIdTbl_2), value);
	}

	inline static int32_t get_offset_of_ContentKindMasks_3() { return static_cast<int32_t>(offsetof(XPathNavigator_t787956054_StaticFields, ___ContentKindMasks_3)); }
	inline Int32U5BU5D_t385246372* get_ContentKindMasks_3() const { return ___ContentKindMasks_3; }
	inline Int32U5BU5D_t385246372** get_address_of_ContentKindMasks_3() { return &___ContentKindMasks_3; }
	inline void set_ContentKindMasks_3(Int32U5BU5D_t385246372* value)
	{
		___ContentKindMasks_3 = value;
		Il2CppCodeGenWriteBarrier((&___ContentKindMasks_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAVIGATOR_T787956054_H
#ifndef XMLCHARTYPE_T2277243275_H
#define XMLCHARTYPE_T2277243275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharType
struct  XmlCharType_t2277243275 
{
public:
	// System.Byte[] System.Xml.XmlCharType::charProperties
	ByteU5BU5D_t4116647657* ___charProperties_2;

public:
	inline static int32_t get_offset_of_charProperties_2() { return static_cast<int32_t>(offsetof(XmlCharType_t2277243275, ___charProperties_2)); }
	inline ByteU5BU5D_t4116647657* get_charProperties_2() const { return ___charProperties_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_charProperties_2() { return &___charProperties_2; }
	inline void set_charProperties_2(ByteU5BU5D_t4116647657* value)
	{
		___charProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___charProperties_2), value);
	}
};

struct XmlCharType_t2277243275_StaticFields
{
public:
	// System.Object System.Xml.XmlCharType::s_Lock
	RuntimeObject * ___s_Lock_0;
	// System.Byte[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlCharType::s_CharProperties
	ByteU5BU5D_t4116647657* ___s_CharProperties_1;

public:
	inline static int32_t get_offset_of_s_Lock_0() { return static_cast<int32_t>(offsetof(XmlCharType_t2277243275_StaticFields, ___s_Lock_0)); }
	inline RuntimeObject * get_s_Lock_0() const { return ___s_Lock_0; }
	inline RuntimeObject ** get_address_of_s_Lock_0() { return &___s_Lock_0; }
	inline void set_s_Lock_0(RuntimeObject * value)
	{
		___s_Lock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Lock_0), value);
	}

	inline static int32_t get_offset_of_s_CharProperties_1() { return static_cast<int32_t>(offsetof(XmlCharType_t2277243275_StaticFields, ___s_CharProperties_1)); }
	inline ByteU5BU5D_t4116647657* get_s_CharProperties_1() const { return ___s_CharProperties_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_s_CharProperties_1() { return &___s_CharProperties_1; }
	inline void set_s_CharProperties_1(ByteU5BU5D_t4116647657* value)
	{
		___s_CharProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CharProperties_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlCharType
struct XmlCharType_t2277243275_marshaled_pinvoke
{
	uint8_t* ___charProperties_2;
};
// Native definition for COM marshalling of System.Xml.XmlCharType
struct XmlCharType_t2277243275_marshaled_com
{
	uint8_t* ___charProperties_2;
};
#endif // XMLCHARTYPE_T2277243275_H
#ifndef KEYEDHASHALGORITHM_T112861511_H
#define KEYEDHASHALGORITHM_T112861511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.KeyedHashAlgorithm
struct  KeyedHashAlgorithm_t112861511  : public HashAlgorithm_t1432317219
{
public:
	// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::KeyValue
	ByteU5BU5D_t4116647657* ___KeyValue_4;

public:
	inline static int32_t get_offset_of_KeyValue_4() { return static_cast<int32_t>(offsetof(KeyedHashAlgorithm_t112861511, ___KeyValue_4)); }
	inline ByteU5BU5D_t4116647657* get_KeyValue_4() const { return ___KeyValue_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_KeyValue_4() { return &___KeyValue_4; }
	inline void set_KeyValue_4(ByteU5BU5D_t4116647657* value)
	{
		___KeyValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEDHASHALGORITHM_T112861511_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef CHARENTITYENCODERFALLBACK_T110445598_H
#define CHARENTITYENCODERFALLBACK_T110445598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.CharEntityEncoderFallback
struct  CharEntityEncoderFallback_t110445598  : public EncoderFallback_t1188251036
{
public:
	// System.Xml.CharEntityEncoderFallbackBuffer System.Xml.CharEntityEncoderFallback::fallbackBuffer
	CharEntityEncoderFallbackBuffer_t4206548234 * ___fallbackBuffer_4;
	// System.Int32[] System.Xml.CharEntityEncoderFallback::textContentMarks
	Int32U5BU5D_t385246372* ___textContentMarks_5;
	// System.Int32 System.Xml.CharEntityEncoderFallback::endMarkPos
	int32_t ___endMarkPos_6;
	// System.Int32 System.Xml.CharEntityEncoderFallback::curMarkPos
	int32_t ___curMarkPos_7;
	// System.Int32 System.Xml.CharEntityEncoderFallback::startOffset
	int32_t ___startOffset_8;

public:
	inline static int32_t get_offset_of_fallbackBuffer_4() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallback_t110445598, ___fallbackBuffer_4)); }
	inline CharEntityEncoderFallbackBuffer_t4206548234 * get_fallbackBuffer_4() const { return ___fallbackBuffer_4; }
	inline CharEntityEncoderFallbackBuffer_t4206548234 ** get_address_of_fallbackBuffer_4() { return &___fallbackBuffer_4; }
	inline void set_fallbackBuffer_4(CharEntityEncoderFallbackBuffer_t4206548234 * value)
	{
		___fallbackBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackBuffer_4), value);
	}

	inline static int32_t get_offset_of_textContentMarks_5() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallback_t110445598, ___textContentMarks_5)); }
	inline Int32U5BU5D_t385246372* get_textContentMarks_5() const { return ___textContentMarks_5; }
	inline Int32U5BU5D_t385246372** get_address_of_textContentMarks_5() { return &___textContentMarks_5; }
	inline void set_textContentMarks_5(Int32U5BU5D_t385246372* value)
	{
		___textContentMarks_5 = value;
		Il2CppCodeGenWriteBarrier((&___textContentMarks_5), value);
	}

	inline static int32_t get_offset_of_endMarkPos_6() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallback_t110445598, ___endMarkPos_6)); }
	inline int32_t get_endMarkPos_6() const { return ___endMarkPos_6; }
	inline int32_t* get_address_of_endMarkPos_6() { return &___endMarkPos_6; }
	inline void set_endMarkPos_6(int32_t value)
	{
		___endMarkPos_6 = value;
	}

	inline static int32_t get_offset_of_curMarkPos_7() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallback_t110445598, ___curMarkPos_7)); }
	inline int32_t get_curMarkPos_7() const { return ___curMarkPos_7; }
	inline int32_t* get_address_of_curMarkPos_7() { return &___curMarkPos_7; }
	inline void set_curMarkPos_7(int32_t value)
	{
		___curMarkPos_7 = value;
	}

	inline static int32_t get_offset_of_startOffset_8() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallback_t110445598, ___startOffset_8)); }
	inline int32_t get_startOffset_8() const { return ___startOffset_8; }
	inline int32_t* get_address_of_startOffset_8() { return &___startOffset_8; }
	inline void set_startOffset_8(int32_t value)
	{
		___startOffset_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARENTITYENCODERFALLBACK_T110445598_H
#ifndef SUBJECTALTNAMEEXTENSION_T1536937677_H
#define SUBJECTALTNAMEEXTENSION_T1536937677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.SubjectAltNameExtension
struct  SubjectAltNameExtension_t1536937677  : public X509Extension_t3173393653
{
public:
	// Mono.Security.X509.Extensions.GeneralNames Mono.Security.X509.Extensions.SubjectAltNameExtension::_names
	GeneralNames_t2702294159 * ____names_3;

public:
	inline static int32_t get_offset_of__names_3() { return static_cast<int32_t>(offsetof(SubjectAltNameExtension_t1536937677, ____names_3)); }
	inline GeneralNames_t2702294159 * get__names_3() const { return ____names_3; }
	inline GeneralNames_t2702294159 ** get_address_of__names_3() { return &____names_3; }
	inline void set__names_3(GeneralNames_t2702294159 * value)
	{
		____names_3 = value;
		Il2CppCodeGenWriteBarrier((&____names_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBJECTALTNAMEEXTENSION_T1536937677_H
#ifndef EXTENDEDKEYUSAGEEXTENSION_T3929363080_H
#define EXTENDEDKEYUSAGEEXTENSION_T3929363080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.ExtendedKeyUsageExtension
struct  ExtendedKeyUsageExtension_t3929363080  : public X509Extension_t3173393653
{
public:
	// System.Collections.ArrayList Mono.Security.X509.Extensions.ExtendedKeyUsageExtension::keyPurpose
	ArrayList_t2718874744 * ___keyPurpose_3;

public:
	inline static int32_t get_offset_of_keyPurpose_3() { return static_cast<int32_t>(offsetof(ExtendedKeyUsageExtension_t3929363080, ___keyPurpose_3)); }
	inline ArrayList_t2718874744 * get_keyPurpose_3() const { return ___keyPurpose_3; }
	inline ArrayList_t2718874744 ** get_address_of_keyPurpose_3() { return &___keyPurpose_3; }
	inline void set_keyPurpose_3(ArrayList_t2718874744 * value)
	{
		___keyPurpose_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyPurpose_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENDEDKEYUSAGEEXTENSION_T3929363080_H
#ifndef KEYUSAGEEXTENSION_T1795615912_H
#define KEYUSAGEEXTENSION_T1795615912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.KeyUsageExtension
struct  KeyUsageExtension_t1795615912  : public X509Extension_t3173393653
{
public:
	// System.Int32 Mono.Security.X509.Extensions.KeyUsageExtension::kubits
	int32_t ___kubits_3;

public:
	inline static int32_t get_offset_of_kubits_3() { return static_cast<int32_t>(offsetof(KeyUsageExtension_t1795615912, ___kubits_3)); }
	inline int32_t get_kubits_3() const { return ___kubits_3; }
	inline int32_t* get_address_of_kubits_3() { return &___kubits_3; }
	inline void set_kubits_3(int32_t value)
	{
		___kubits_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYUSAGEEXTENSION_T1795615912_H
#ifndef NETSCAPECERTTYPEEXTENSION_T1524296876_H
#define NETSCAPECERTTYPEEXTENSION_T1524296876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.NetscapeCertTypeExtension
struct  NetscapeCertTypeExtension_t1524296876  : public X509Extension_t3173393653
{
public:
	// System.Int32 Mono.Security.X509.Extensions.NetscapeCertTypeExtension::ctbits
	int32_t ___ctbits_3;

public:
	inline static int32_t get_offset_of_ctbits_3() { return static_cast<int32_t>(offsetof(NetscapeCertTypeExtension_t1524296876, ___ctbits_3)); }
	inline int32_t get_ctbits_3() const { return ___ctbits_3; }
	inline int32_t* get_address_of_ctbits_3() { return &___ctbits_3; }
	inline void set_ctbits_3(int32_t value)
	{
		___ctbits_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETSCAPECERTTYPEEXTENSION_T1524296876_H
#ifndef CHARENTITYENCODERFALLBACKBUFFER_T4206548234_H
#define CHARENTITYENCODERFALLBACKBUFFER_T4206548234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.CharEntityEncoderFallbackBuffer
struct  CharEntityEncoderFallbackBuffer_t4206548234  : public EncoderFallbackBuffer_t3523102303
{
public:
	// System.Xml.CharEntityEncoderFallback System.Xml.CharEntityEncoderFallbackBuffer::parent
	CharEntityEncoderFallback_t110445598 * ___parent_7;
	// System.String System.Xml.CharEntityEncoderFallbackBuffer::charEntity
	String_t* ___charEntity_8;
	// System.Int32 System.Xml.CharEntityEncoderFallbackBuffer::charEntityIndex
	int32_t ___charEntityIndex_9;

public:
	inline static int32_t get_offset_of_parent_7() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallbackBuffer_t4206548234, ___parent_7)); }
	inline CharEntityEncoderFallback_t110445598 * get_parent_7() const { return ___parent_7; }
	inline CharEntityEncoderFallback_t110445598 ** get_address_of_parent_7() { return &___parent_7; }
	inline void set_parent_7(CharEntityEncoderFallback_t110445598 * value)
	{
		___parent_7 = value;
		Il2CppCodeGenWriteBarrier((&___parent_7), value);
	}

	inline static int32_t get_offset_of_charEntity_8() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallbackBuffer_t4206548234, ___charEntity_8)); }
	inline String_t* get_charEntity_8() const { return ___charEntity_8; }
	inline String_t** get_address_of_charEntity_8() { return &___charEntity_8; }
	inline void set_charEntity_8(String_t* value)
	{
		___charEntity_8 = value;
		Il2CppCodeGenWriteBarrier((&___charEntity_8), value);
	}

	inline static int32_t get_offset_of_charEntityIndex_9() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallbackBuffer_t4206548234, ___charEntityIndex_9)); }
	inline int32_t get_charEntityIndex_9() const { return ___charEntityIndex_9; }
	inline int32_t* get_address_of_charEntityIndex_9() { return &___charEntityIndex_9; }
	inline void set_charEntityIndex_9(int32_t value)
	{
		___charEntityIndex_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARENTITYENCODERFALLBACKBUFFER_T4206548234_H
#ifndef XMLRAWWRITER_T722320575_H
#define XMLRAWWRITER_T722320575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlRawWriter
struct  XmlRawWriter_t722320575  : public XmlWriter_t127905479
{
public:
	// System.Xml.XmlRawWriterBase64Encoder System.Xml.XmlRawWriter::base64Encoder
	XmlRawWriterBase64Encoder_t1905751661 * ___base64Encoder_1;
	// System.Xml.IXmlNamespaceResolver System.Xml.XmlRawWriter::resolver
	RuntimeObject* ___resolver_2;

public:
	inline static int32_t get_offset_of_base64Encoder_1() { return static_cast<int32_t>(offsetof(XmlRawWriter_t722320575, ___base64Encoder_1)); }
	inline XmlRawWriterBase64Encoder_t1905751661 * get_base64Encoder_1() const { return ___base64Encoder_1; }
	inline XmlRawWriterBase64Encoder_t1905751661 ** get_address_of_base64Encoder_1() { return &___base64Encoder_1; }
	inline void set_base64Encoder_1(XmlRawWriterBase64Encoder_t1905751661 * value)
	{
		___base64Encoder_1 = value;
		Il2CppCodeGenWriteBarrier((&___base64Encoder_1), value);
	}

	inline static int32_t get_offset_of_resolver_2() { return static_cast<int32_t>(offsetof(XmlRawWriter_t722320575, ___resolver_2)); }
	inline RuntimeObject* get_resolver_2() const { return ___resolver_2; }
	inline RuntimeObject** get_address_of_resolver_2() { return &___resolver_2; }
	inline void set_resolver_2(RuntimeObject* value)
	{
		___resolver_2 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLRAWWRITER_T722320575_H
#ifndef __STATICARRAYINITTYPESIZEU3D9_T3218278899_H
#define __STATICARRAYINITTYPESIZEU3D9_T3218278899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=9
struct  __StaticArrayInitTypeSizeU3D9_t3218278899 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D9_t3218278899__padding[9];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D9_T3218278899_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef __STATICARRAYINITTYPESIZEU3D64_T3517497836_H
#define __STATICARRAYINITTYPESIZEU3D64_T3517497836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64
struct  __StaticArrayInitTypeSizeU3D64_t3517497836 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D64_t3517497836__padding[64];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D64_T3517497836_H
#ifndef __STATICARRAYINITTYPESIZEU3D32_T2711125390_H
#define __STATICARRAYINITTYPESIZEU3D32_T2711125390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32
struct  __StaticArrayInitTypeSizeU3D32_t2711125390 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t2711125390__padding[32];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D32_T2711125390_H
#ifndef __STATICARRAYINITTYPESIZEU3D3_T3217885683_H
#define __STATICARRAYINITTYPESIZEU3D3_T3217885683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3
struct  __StaticArrayInitTypeSizeU3D3_t3217885683 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D3_t3217885683__padding[3];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D3_T3217885683_H
#ifndef __STATICARRAYINITTYPESIZEU3D3132_T3825993976_H
#define __STATICARRAYINITTYPESIZEU3D3132_T3825993976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3132
struct  __StaticArrayInitTypeSizeU3D3132_t3825993976 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D3132_t3825993976__padding[3132];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D3132_T3825993976_H
#ifndef __STATICARRAYINITTYPESIZEU3D524_T4061560205_H
#define __STATICARRAYINITTYPESIZEU3D524_T4061560205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=524
struct  __StaticArrayInitTypeSizeU3D524_t4061560205 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D524_t4061560205__padding[524];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D524_T4061560205_H
#ifndef __STATICARRAYINITTYPESIZEU3D20_T1548391512_H
#define __STATICARRAYINITTYPESIZEU3D20_T1548391512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20
struct  __StaticArrayInitTypeSizeU3D20_t1548391512 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_t1548391512__padding[20];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D20_T1548391512_H
#ifndef __STATICARRAYINITTYPESIZEU3D10_T1548194904_H
#define __STATICARRAYINITTYPESIZEU3D10_T1548194904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10
struct  __StaticArrayInitTypeSizeU3D10_t1548194904 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D10_t1548194904__padding[10];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D10_T1548194904_H
#ifndef __STATICARRAYINITTYPESIZEU3D14_T3517563372_H
#define __STATICARRAYINITTYPESIZEU3D14_T3517563372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=14
struct  __StaticArrayInitTypeSizeU3D14_t3517563372 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D14_t3517563372__padding[14];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D14_T3517563372_H
#ifndef GROUP_T100818710_H
#define GROUP_T100818710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Group
struct  Group_t100818710  : public AstNode_t2514041814
{
public:
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Group::groupNode
	AstNode_t2514041814 * ___groupNode_0;

public:
	inline static int32_t get_offset_of_groupNode_0() { return static_cast<int32_t>(offsetof(Group_t100818710, ___groupNode_0)); }
	inline AstNode_t2514041814 * get_groupNode_0() const { return ___groupNode_0; }
	inline AstNode_t2514041814 ** get_address_of_groupNode_0() { return &___groupNode_0; }
	inline void set_groupNode_0(AstNode_t2514041814 * value)
	{
		___groupNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___groupNode_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUP_T100818710_H
#ifndef MD5SHA1_T723838944_H
#define MD5SHA1_T723838944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.MD5SHA1
struct  MD5SHA1_t723838944  : public HashAlgorithm_t1432317219
{
public:
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Cryptography.MD5SHA1::md5
	HashAlgorithm_t1432317219 * ___md5_4;
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Cryptography.MD5SHA1::sha
	HashAlgorithm_t1432317219 * ___sha_5;
	// System.Boolean Mono.Security.Cryptography.MD5SHA1::hashing
	bool ___hashing_6;

public:
	inline static int32_t get_offset_of_md5_4() { return static_cast<int32_t>(offsetof(MD5SHA1_t723838944, ___md5_4)); }
	inline HashAlgorithm_t1432317219 * get_md5_4() const { return ___md5_4; }
	inline HashAlgorithm_t1432317219 ** get_address_of_md5_4() { return &___md5_4; }
	inline void set_md5_4(HashAlgorithm_t1432317219 * value)
	{
		___md5_4 = value;
		Il2CppCodeGenWriteBarrier((&___md5_4), value);
	}

	inline static int32_t get_offset_of_sha_5() { return static_cast<int32_t>(offsetof(MD5SHA1_t723838944, ___sha_5)); }
	inline HashAlgorithm_t1432317219 * get_sha_5() const { return ___sha_5; }
	inline HashAlgorithm_t1432317219 ** get_address_of_sha_5() { return &___sha_5; }
	inline void set_sha_5(HashAlgorithm_t1432317219 * value)
	{
		___sha_5 = value;
		Il2CppCodeGenWriteBarrier((&___sha_5), value);
	}

	inline static int32_t get_offset_of_hashing_6() { return static_cast<int32_t>(offsetof(MD5SHA1_t723838944, ___hashing_6)); }
	inline bool get_hashing_6() const { return ___hashing_6; }
	inline bool* get_address_of_hashing_6() { return &___hashing_6; }
	inline void set_hashing_6(bool value)
	{
		___hashing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5SHA1_T723838944_H
#ifndef VARIABLE_T262588068_H
#define VARIABLE_T262588068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Variable
struct  Variable_t262588068  : public AstNode_t2514041814
{
public:
	// System.String MS.Internal.Xml.XPath.Variable::localname
	String_t* ___localname_0;
	// System.String MS.Internal.Xml.XPath.Variable::prefix
	String_t* ___prefix_1;

public:
	inline static int32_t get_offset_of_localname_0() { return static_cast<int32_t>(offsetof(Variable_t262588068, ___localname_0)); }
	inline String_t* get_localname_0() const { return ___localname_0; }
	inline String_t** get_address_of_localname_0() { return &___localname_0; }
	inline void set_localname_0(String_t* value)
	{
		___localname_0 = value;
		Il2CppCodeGenWriteBarrier((&___localname_0), value);
	}

	inline static int32_t get_offset_of_prefix_1() { return static_cast<int32_t>(offsetof(Variable_t262588068, ___prefix_1)); }
	inline String_t* get_prefix_1() const { return ___prefix_1; }
	inline String_t** get_address_of_prefix_1() { return &___prefix_1; }
	inline void set_prefix_1(String_t* value)
	{
		___prefix_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLE_T262588068_H
#ifndef FILTER_T1571657935_H
#define FILTER_T1571657935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Filter
struct  Filter_t1571657935  : public AstNode_t2514041814
{
public:
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Filter::input
	AstNode_t2514041814 * ___input_0;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Filter::condition
	AstNode_t2514041814 * ___condition_1;

public:
	inline static int32_t get_offset_of_input_0() { return static_cast<int32_t>(offsetof(Filter_t1571657935, ___input_0)); }
	inline AstNode_t2514041814 * get_input_0() const { return ___input_0; }
	inline AstNode_t2514041814 ** get_address_of_input_0() { return &___input_0; }
	inline void set_input_0(AstNode_t2514041814 * value)
	{
		___input_0 = value;
		Il2CppCodeGenWriteBarrier((&___input_0), value);
	}

	inline static int32_t get_offset_of_condition_1() { return static_cast<int32_t>(offsetof(Filter_t1571657935, ___condition_1)); }
	inline AstNode_t2514041814 * get_condition_1() const { return ___condition_1; }
	inline AstNode_t2514041814 ** get_address_of_condition_1() { return &___condition_1; }
	inline void set_condition_1(AstNode_t2514041814 * value)
	{
		___condition_1 = value;
		Il2CppCodeGenWriteBarrier((&___condition_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTER_T1571657935_H
#ifndef BINHEXDECODER_T1474272384_H
#define BINHEXDECODER_T1474272384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinHexDecoder
struct  BinHexDecoder_t1474272384  : public IncrementalReadDecoder_t3011954239
{
public:
	// System.Byte[] System.Xml.BinHexDecoder::buffer
	ByteU5BU5D_t4116647657* ___buffer_0;
	// System.Int32 System.Xml.BinHexDecoder::curIndex
	int32_t ___curIndex_1;
	// System.Int32 System.Xml.BinHexDecoder::endIndex
	int32_t ___endIndex_2;
	// System.Boolean System.Xml.BinHexDecoder::hasHalfByteCached
	bool ___hasHalfByteCached_3;
	// System.Byte System.Xml.BinHexDecoder::cachedHalfByte
	uint8_t ___cachedHalfByte_4;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(BinHexDecoder_t1474272384, ___buffer_0)); }
	inline ByteU5BU5D_t4116647657* get_buffer_0() const { return ___buffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(ByteU5BU5D_t4116647657* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_curIndex_1() { return static_cast<int32_t>(offsetof(BinHexDecoder_t1474272384, ___curIndex_1)); }
	inline int32_t get_curIndex_1() const { return ___curIndex_1; }
	inline int32_t* get_address_of_curIndex_1() { return &___curIndex_1; }
	inline void set_curIndex_1(int32_t value)
	{
		___curIndex_1 = value;
	}

	inline static int32_t get_offset_of_endIndex_2() { return static_cast<int32_t>(offsetof(BinHexDecoder_t1474272384, ___endIndex_2)); }
	inline int32_t get_endIndex_2() const { return ___endIndex_2; }
	inline int32_t* get_address_of_endIndex_2() { return &___endIndex_2; }
	inline void set_endIndex_2(int32_t value)
	{
		___endIndex_2 = value;
	}

	inline static int32_t get_offset_of_hasHalfByteCached_3() { return static_cast<int32_t>(offsetof(BinHexDecoder_t1474272384, ___hasHalfByteCached_3)); }
	inline bool get_hasHalfByteCached_3() const { return ___hasHalfByteCached_3; }
	inline bool* get_address_of_hasHalfByteCached_3() { return &___hasHalfByteCached_3; }
	inline void set_hasHalfByteCached_3(bool value)
	{
		___hasHalfByteCached_3 = value;
	}

	inline static int32_t get_offset_of_cachedHalfByte_4() { return static_cast<int32_t>(offsetof(BinHexDecoder_t1474272384, ___cachedHalfByte_4)); }
	inline uint8_t get_cachedHalfByte_4() const { return ___cachedHalfByte_4; }
	inline uint8_t* get_address_of_cachedHalfByte_4() { return &___cachedHalfByte_4; }
	inline void set_cachedHalfByte_4(uint8_t value)
	{
		___cachedHalfByte_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINHEXDECODER_T1474272384_H
#ifndef ROOT_T720671714_H
#define ROOT_T720671714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Root
struct  Root_t720671714  : public AstNode_t2514041814
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOT_T720671714_H
#ifndef __STATICARRAYINITTYPESIZEU3D48_T1904228656_H
#define __STATICARRAYINITTYPESIZEU3D48_T1904228656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=48
struct  __StaticArrayInitTypeSizeU3D48_t1904228656 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D48_t1904228656__padding[48];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D48_T1904228656_H
#ifndef SEQUENTIALSEARCHPRIMEGENERATORBASE_T2996090509_H
#define SEQUENTIALSEARCHPRIMEGENERATORBASE_T2996090509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase
struct  SequentialSearchPrimeGeneratorBase_t2996090509  : public PrimeGeneratorBase_t446028867
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENTIALSEARCHPRIMEGENERATORBASE_T2996090509_H
#ifndef CERTTYPES_T3317701015_H
#define CERTTYPES_T3317701015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes
struct  CertTypes_t3317701015 
{
public:
	// System.Int32 Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CertTypes_t3317701015, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTTYPES_T3317701015_H
#ifndef ELEMENTPROPERTIES_T469398153_H
#define ELEMENTPROPERTIES_T469398153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ElementProperties
struct  ElementProperties_t469398153 
{
public:
	// System.UInt32 System.Xml.ElementProperties::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ElementProperties_t469398153, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTPROPERTIES_T469398153_H
#ifndef ATTRIBUTEPROPERTIES_T3715092149_H
#define ATTRIBUTEPROPERTIES_T3715092149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.AttributeProperties
struct  AttributeProperties_t3715092149 
{
public:
	// System.UInt32 System.Xml.AttributeProperties::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeProperties_t3715092149, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEPROPERTIES_T3715092149_H
#ifndef XPATHNODETYPE_T3031007223_H
#define XPATHNODETYPE_T3031007223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNodeType
struct  XPathNodeType_t3031007223 
{
public:
	// System.Int32 System.Xml.XPath.XPathNodeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XPathNodeType_t3031007223, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODETYPE_T3031007223_H
#ifndef XPATHRESULTTYPE_T2828988488_H
#define XPATHRESULTTYPE_T2828988488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathResultType
struct  XPathResultType_t2828988488 
{
public:
	// System.Int32 System.Xml.XPath.XPathResultType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XPathResultType_t2828988488, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHRESULTTYPE_T2828988488_H
#ifndef CONFIDENCEFACTOR_T2516000286_H
#define CONFIDENCEFACTOR_T2516000286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.ConfidenceFactor
struct  ConfidenceFactor_t2516000286 
{
public:
	// System.Int32 Mono.Math.Prime.ConfidenceFactor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConfidenceFactor_t2516000286, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIDENCEFACTOR_T2516000286_H
#ifndef RSAMANAGED_T1757093820_H
#define RSAMANAGED_T1757093820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.RSAManaged
struct  RSAManaged_t1757093820  : public RSA_t2385438082
{
public:
	// System.Boolean Mono.Security.Cryptography.RSAManaged::isCRTpossible
	bool ___isCRTpossible_2;
	// System.Boolean Mono.Security.Cryptography.RSAManaged::keyBlinding
	bool ___keyBlinding_3;
	// System.Boolean Mono.Security.Cryptography.RSAManaged::keypairGenerated
	bool ___keypairGenerated_4;
	// System.Boolean Mono.Security.Cryptography.RSAManaged::m_disposed
	bool ___m_disposed_5;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::d
	BigInteger_t2902905090 * ___d_6;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::p
	BigInteger_t2902905090 * ___p_7;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::q
	BigInteger_t2902905090 * ___q_8;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::dp
	BigInteger_t2902905090 * ___dp_9;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::dq
	BigInteger_t2902905090 * ___dq_10;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::qInv
	BigInteger_t2902905090 * ___qInv_11;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::n
	BigInteger_t2902905090 * ___n_12;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::e
	BigInteger_t2902905090 * ___e_13;
	// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler Mono.Security.Cryptography.RSAManaged::KeyGenerated
	KeyGeneratedEventHandler_t3064139578 * ___KeyGenerated_14;

public:
	inline static int32_t get_offset_of_isCRTpossible_2() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___isCRTpossible_2)); }
	inline bool get_isCRTpossible_2() const { return ___isCRTpossible_2; }
	inline bool* get_address_of_isCRTpossible_2() { return &___isCRTpossible_2; }
	inline void set_isCRTpossible_2(bool value)
	{
		___isCRTpossible_2 = value;
	}

	inline static int32_t get_offset_of_keyBlinding_3() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___keyBlinding_3)); }
	inline bool get_keyBlinding_3() const { return ___keyBlinding_3; }
	inline bool* get_address_of_keyBlinding_3() { return &___keyBlinding_3; }
	inline void set_keyBlinding_3(bool value)
	{
		___keyBlinding_3 = value;
	}

	inline static int32_t get_offset_of_keypairGenerated_4() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___keypairGenerated_4)); }
	inline bool get_keypairGenerated_4() const { return ___keypairGenerated_4; }
	inline bool* get_address_of_keypairGenerated_4() { return &___keypairGenerated_4; }
	inline void set_keypairGenerated_4(bool value)
	{
		___keypairGenerated_4 = value;
	}

	inline static int32_t get_offset_of_m_disposed_5() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___m_disposed_5)); }
	inline bool get_m_disposed_5() const { return ___m_disposed_5; }
	inline bool* get_address_of_m_disposed_5() { return &___m_disposed_5; }
	inline void set_m_disposed_5(bool value)
	{
		___m_disposed_5 = value;
	}

	inline static int32_t get_offset_of_d_6() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___d_6)); }
	inline BigInteger_t2902905090 * get_d_6() const { return ___d_6; }
	inline BigInteger_t2902905090 ** get_address_of_d_6() { return &___d_6; }
	inline void set_d_6(BigInteger_t2902905090 * value)
	{
		___d_6 = value;
		Il2CppCodeGenWriteBarrier((&___d_6), value);
	}

	inline static int32_t get_offset_of_p_7() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___p_7)); }
	inline BigInteger_t2902905090 * get_p_7() const { return ___p_7; }
	inline BigInteger_t2902905090 ** get_address_of_p_7() { return &___p_7; }
	inline void set_p_7(BigInteger_t2902905090 * value)
	{
		___p_7 = value;
		Il2CppCodeGenWriteBarrier((&___p_7), value);
	}

	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___q_8)); }
	inline BigInteger_t2902905090 * get_q_8() const { return ___q_8; }
	inline BigInteger_t2902905090 ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_t2902905090 * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}

	inline static int32_t get_offset_of_dp_9() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___dp_9)); }
	inline BigInteger_t2902905090 * get_dp_9() const { return ___dp_9; }
	inline BigInteger_t2902905090 ** get_address_of_dp_9() { return &___dp_9; }
	inline void set_dp_9(BigInteger_t2902905090 * value)
	{
		___dp_9 = value;
		Il2CppCodeGenWriteBarrier((&___dp_9), value);
	}

	inline static int32_t get_offset_of_dq_10() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___dq_10)); }
	inline BigInteger_t2902905090 * get_dq_10() const { return ___dq_10; }
	inline BigInteger_t2902905090 ** get_address_of_dq_10() { return &___dq_10; }
	inline void set_dq_10(BigInteger_t2902905090 * value)
	{
		___dq_10 = value;
		Il2CppCodeGenWriteBarrier((&___dq_10), value);
	}

	inline static int32_t get_offset_of_qInv_11() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___qInv_11)); }
	inline BigInteger_t2902905090 * get_qInv_11() const { return ___qInv_11; }
	inline BigInteger_t2902905090 ** get_address_of_qInv_11() { return &___qInv_11; }
	inline void set_qInv_11(BigInteger_t2902905090 * value)
	{
		___qInv_11 = value;
		Il2CppCodeGenWriteBarrier((&___qInv_11), value);
	}

	inline static int32_t get_offset_of_n_12() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___n_12)); }
	inline BigInteger_t2902905090 * get_n_12() const { return ___n_12; }
	inline BigInteger_t2902905090 ** get_address_of_n_12() { return &___n_12; }
	inline void set_n_12(BigInteger_t2902905090 * value)
	{
		___n_12 = value;
		Il2CppCodeGenWriteBarrier((&___n_12), value);
	}

	inline static int32_t get_offset_of_e_13() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___e_13)); }
	inline BigInteger_t2902905090 * get_e_13() const { return ___e_13; }
	inline BigInteger_t2902905090 ** get_address_of_e_13() { return &___e_13; }
	inline void set_e_13(BigInteger_t2902905090 * value)
	{
		___e_13 = value;
		Il2CppCodeGenWriteBarrier((&___e_13), value);
	}

	inline static int32_t get_offset_of_KeyGenerated_14() { return static_cast<int32_t>(offsetof(RSAManaged_t1757093820, ___KeyGenerated_14)); }
	inline KeyGeneratedEventHandler_t3064139578 * get_KeyGenerated_14() const { return ___KeyGenerated_14; }
	inline KeyGeneratedEventHandler_t3064139578 ** get_address_of_KeyGenerated_14() { return &___KeyGenerated_14; }
	inline void set_KeyGenerated_14(KeyGeneratedEventHandler_t3064139578 * value)
	{
		___KeyGenerated_14 = value;
		Il2CppCodeGenWriteBarrier((&___KeyGenerated_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAMANAGED_T1757093820_H
#ifndef KEYUSAGES_T820456313_H
#define KEYUSAGES_T820456313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.KeyUsages
struct  KeyUsages_t820456313 
{
public:
	// System.Int32 Mono.Security.X509.Extensions.KeyUsages::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyUsages_t820456313, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYUSAGES_T820456313_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255362_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255362  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::12D04472A8285260EA12FD3813CDFA9F2D2B548C
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___12D04472A8285260EA12FD3813CDFA9F2D2B548C_0;
	// System.Int32 <PrivateImplementationDetails>::1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C
	int32_t ___1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::13A35EF1A549297C70E2AD46045BBD2ECA17852D
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___13A35EF1A549297C70E2AD46045BBD2ECA17852D_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::1A84029C80CB5518379F199F53FF08A7B764F8FD
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___1A84029C80CB5518379F199F53FF08A7B764F8FD_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::235D99572263B22ADFEE10FDA0C25E12F4D94FFC
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=14 <PrivateImplementationDetails>::2D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130
	__StaticArrayInitTypeSizeU3D14_t3517563372  ___2D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5;
	// System.Int32 <PrivateImplementationDetails>::31D8729F7377B44017C0A2395A582C9CA4163277
	int32_t ___31D8729F7377B44017C0A2395A582C9CA4163277_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::320B018758ECE3752FFEDBAEB1A6DB67C80B9359
	__StaticArrayInitTypeSizeU3D64_t3517497836  ___320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::3E3442C7396F3F2BB4C7348F4A2074C7DC677D68
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___3E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=48 <PrivateImplementationDetails>::4E3B533C39447AAEB59A8E48FABD7E15B5B5D195
	__StaticArrayInitTypeSizeU3D48_t1904228656  ___4E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10 <PrivateImplementationDetails>::56DFA5053B3131883637F53219E7D88CCEF35949
	__StaticArrayInitTypeSizeU3D10_t1548194904  ___56DFA5053B3131883637F53219E7D88CCEF35949_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=9 <PrivateImplementationDetails>::6D49C9D487D7AD3491ECE08732D68A593CC2038D
	__StaticArrayInitTypeSizeU3D9_t3218278899  ___6D49C9D487D7AD3491ECE08732D68A593CC2038D_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3132 <PrivateImplementationDetails>::6E5DC824F803F8565AF31B42199DAE39FE7F4EA9
	__StaticArrayInitTypeSizeU3D3132_t3825993976  ___6E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::736D39815215889F11249D9958F6ED12D37B9F57
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___736D39815215889F11249D9958F6ED12D37B9F57_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::86F4F563FA2C61798AE6238D789139739428463A
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___86F4F563FA2C61798AE6238D789139739428463A_14;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::97FB30C84FF4A41CD4625B44B2940BFC8DB43003
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___97FB30C84FF4A41CD4625B44B2940BFC8DB43003_15;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5
	__StaticArrayInitTypeSizeU3D64_t3517497836  ___9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::9BB00D1FCCBAF03165447FC8028E7CA07CA9FE88
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___9BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::A323DB0813C4D072957BA6FDA79D9776674CD06B
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___A323DB0813C4D072957BA6FDA79D9776674CD06B_18;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::BE1BDEC0AA74B4DCB079943E70528096CCA985F8
	__StaticArrayInitTypeSizeU3D20_t1548391512  ___BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::BF477463CE2F5EF38FC4C644BBBF4DF109E7670A
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::CF0B42666EF5E37EDEA0AB8E173E42C196D03814
	__StaticArrayInitTypeSizeU3D64_t3517497836  ___CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE
	__StaticArrayInitTypeSizeU3D32_t2711125390  ___D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=524 <PrivateImplementationDetails>::D693078666967609CB4A2B840921F9E052031847
	__StaticArrayInitTypeSizeU3D524_t4061560205  ___D693078666967609CB4A2B840921F9E052031847_23;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::E75835D001C843F156FBA01B001DFE1B8029AC17
	__StaticArrayInitTypeSizeU3D64_t3517497836  ___E75835D001C843F156FBA01B001DFE1B8029AC17_24;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10 <PrivateImplementationDetails>::EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11
	__StaticArrayInitTypeSizeU3D10_t1548194904  ___EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::EC83FB16C20052BEE2B4025159BC2ED45C9C70C3
	__StaticArrayInitTypeSizeU3D3_t3217885683  ___EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26;

public:
	inline static int32_t get_offset_of_U312D04472A8285260EA12FD3813CDFA9F2D2B548C_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___12D04472A8285260EA12FD3813CDFA9F2D2B548C_0)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U312D04472A8285260EA12FD3813CDFA9F2D2B548C_0() const { return ___12D04472A8285260EA12FD3813CDFA9F2D2B548C_0; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U312D04472A8285260EA12FD3813CDFA9F2D2B548C_0() { return &___12D04472A8285260EA12FD3813CDFA9F2D2B548C_0; }
	inline void set_U312D04472A8285260EA12FD3813CDFA9F2D2B548C_0(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___12D04472A8285260EA12FD3813CDFA9F2D2B548C_0 = value;
	}

	inline static int32_t get_offset_of_U31397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1)); }
	inline int32_t get_U31397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1() const { return ___1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1; }
	inline int32_t* get_address_of_U31397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1() { return &___1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1; }
	inline void set_U31397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1(int32_t value)
	{
		___1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1 = value;
	}

	inline static int32_t get_offset_of_U313A35EF1A549297C70E2AD46045BBD2ECA17852D_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___13A35EF1A549297C70E2AD46045BBD2ECA17852D_2)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U313A35EF1A549297C70E2AD46045BBD2ECA17852D_2() const { return ___13A35EF1A549297C70E2AD46045BBD2ECA17852D_2; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U313A35EF1A549297C70E2AD46045BBD2ECA17852D_2() { return &___13A35EF1A549297C70E2AD46045BBD2ECA17852D_2; }
	inline void set_U313A35EF1A549297C70E2AD46045BBD2ECA17852D_2(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___13A35EF1A549297C70E2AD46045BBD2ECA17852D_2 = value;
	}

	inline static int32_t get_offset_of_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___1A84029C80CB5518379F199F53FF08A7B764F8FD_3)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3() const { return ___1A84029C80CB5518379F199F53FF08A7B764F8FD_3; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3() { return &___1A84029C80CB5518379F199F53FF08A7B764F8FD_3; }
	inline void set_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___1A84029C80CB5518379F199F53FF08A7B764F8FD_3 = value;
	}

	inline static int32_t get_offset_of_U3235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U3235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4() const { return ___235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U3235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4() { return &___235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4; }
	inline void set_U3235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4 = value;
	}

	inline static int32_t get_offset_of_U32D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___2D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5)); }
	inline __StaticArrayInitTypeSizeU3D14_t3517563372  get_U32D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5() const { return ___2D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5; }
	inline __StaticArrayInitTypeSizeU3D14_t3517563372 * get_address_of_U32D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5() { return &___2D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5; }
	inline void set_U32D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5(__StaticArrayInitTypeSizeU3D14_t3517563372  value)
	{
		___2D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5 = value;
	}

	inline static int32_t get_offset_of_U331D8729F7377B44017C0A2395A582C9CA4163277_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___31D8729F7377B44017C0A2395A582C9CA4163277_6)); }
	inline int32_t get_U331D8729F7377B44017C0A2395A582C9CA4163277_6() const { return ___31D8729F7377B44017C0A2395A582C9CA4163277_6; }
	inline int32_t* get_address_of_U331D8729F7377B44017C0A2395A582C9CA4163277_6() { return &___31D8729F7377B44017C0A2395A582C9CA4163277_6; }
	inline void set_U331D8729F7377B44017C0A2395A582C9CA4163277_6(int32_t value)
	{
		___31D8729F7377B44017C0A2395A582C9CA4163277_6 = value;
	}

	inline static int32_t get_offset_of_U3320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7)); }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836  get_U3320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7() const { return ___320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7; }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836 * get_address_of_U3320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7() { return &___320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7; }
	inline void set_U3320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7(__StaticArrayInitTypeSizeU3D64_t3517497836  value)
	{
		___320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7 = value;
	}

	inline static int32_t get_offset_of_U33E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___3E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U33E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8() const { return ___3E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U33E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8() { return &___3E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8; }
	inline void set_U33E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___3E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8 = value;
	}

	inline static int32_t get_offset_of_U34E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___4E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9)); }
	inline __StaticArrayInitTypeSizeU3D48_t1904228656  get_U34E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9() const { return ___4E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9; }
	inline __StaticArrayInitTypeSizeU3D48_t1904228656 * get_address_of_U34E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9() { return &___4E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9; }
	inline void set_U34E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9(__StaticArrayInitTypeSizeU3D48_t1904228656  value)
	{
		___4E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9 = value;
	}

	inline static int32_t get_offset_of_U356DFA5053B3131883637F53219E7D88CCEF35949_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___56DFA5053B3131883637F53219E7D88CCEF35949_10)); }
	inline __StaticArrayInitTypeSizeU3D10_t1548194904  get_U356DFA5053B3131883637F53219E7D88CCEF35949_10() const { return ___56DFA5053B3131883637F53219E7D88CCEF35949_10; }
	inline __StaticArrayInitTypeSizeU3D10_t1548194904 * get_address_of_U356DFA5053B3131883637F53219E7D88CCEF35949_10() { return &___56DFA5053B3131883637F53219E7D88CCEF35949_10; }
	inline void set_U356DFA5053B3131883637F53219E7D88CCEF35949_10(__StaticArrayInitTypeSizeU3D10_t1548194904  value)
	{
		___56DFA5053B3131883637F53219E7D88CCEF35949_10 = value;
	}

	inline static int32_t get_offset_of_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___6D49C9D487D7AD3491ECE08732D68A593CC2038D_11)); }
	inline __StaticArrayInitTypeSizeU3D9_t3218278899  get_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_11() const { return ___6D49C9D487D7AD3491ECE08732D68A593CC2038D_11; }
	inline __StaticArrayInitTypeSizeU3D9_t3218278899 * get_address_of_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_11() { return &___6D49C9D487D7AD3491ECE08732D68A593CC2038D_11; }
	inline void set_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_11(__StaticArrayInitTypeSizeU3D9_t3218278899  value)
	{
		___6D49C9D487D7AD3491ECE08732D68A593CC2038D_11 = value;
	}

	inline static int32_t get_offset_of_U36E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___6E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12)); }
	inline __StaticArrayInitTypeSizeU3D3132_t3825993976  get_U36E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12() const { return ___6E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12; }
	inline __StaticArrayInitTypeSizeU3D3132_t3825993976 * get_address_of_U36E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12() { return &___6E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12; }
	inline void set_U36E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12(__StaticArrayInitTypeSizeU3D3132_t3825993976  value)
	{
		___6E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12 = value;
	}

	inline static int32_t get_offset_of_U3736D39815215889F11249D9958F6ED12D37B9F57_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___736D39815215889F11249D9958F6ED12D37B9F57_13)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U3736D39815215889F11249D9958F6ED12D37B9F57_13() const { return ___736D39815215889F11249D9958F6ED12D37B9F57_13; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U3736D39815215889F11249D9958F6ED12D37B9F57_13() { return &___736D39815215889F11249D9958F6ED12D37B9F57_13; }
	inline void set_U3736D39815215889F11249D9958F6ED12D37B9F57_13(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___736D39815215889F11249D9958F6ED12D37B9F57_13 = value;
	}

	inline static int32_t get_offset_of_U386F4F563FA2C61798AE6238D789139739428463A_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___86F4F563FA2C61798AE6238D789139739428463A_14)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U386F4F563FA2C61798AE6238D789139739428463A_14() const { return ___86F4F563FA2C61798AE6238D789139739428463A_14; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U386F4F563FA2C61798AE6238D789139739428463A_14() { return &___86F4F563FA2C61798AE6238D789139739428463A_14; }
	inline void set_U386F4F563FA2C61798AE6238D789139739428463A_14(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___86F4F563FA2C61798AE6238D789139739428463A_14 = value;
	}

	inline static int32_t get_offset_of_U397FB30C84FF4A41CD4625B44B2940BFC8DB43003_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___97FB30C84FF4A41CD4625B44B2940BFC8DB43003_15)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U397FB30C84FF4A41CD4625B44B2940BFC8DB43003_15() const { return ___97FB30C84FF4A41CD4625B44B2940BFC8DB43003_15; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U397FB30C84FF4A41CD4625B44B2940BFC8DB43003_15() { return &___97FB30C84FF4A41CD4625B44B2940BFC8DB43003_15; }
	inline void set_U397FB30C84FF4A41CD4625B44B2940BFC8DB43003_15(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___97FB30C84FF4A41CD4625B44B2940BFC8DB43003_15 = value;
	}

	inline static int32_t get_offset_of_U39A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16)); }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836  get_U39A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16() const { return ___9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16; }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836 * get_address_of_U39A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16() { return &___9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16; }
	inline void set_U39A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16(__StaticArrayInitTypeSizeU3D64_t3517497836  value)
	{
		___9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16 = value;
	}

	inline static int32_t get_offset_of_U39BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___9BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_U39BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17() const { return ___9BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_U39BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17() { return &___9BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17; }
	inline void set_U39BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___9BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17 = value;
	}

	inline static int32_t get_offset_of_A323DB0813C4D072957BA6FDA79D9776674CD06B_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___A323DB0813C4D072957BA6FDA79D9776674CD06B_18)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_A323DB0813C4D072957BA6FDA79D9776674CD06B_18() const { return ___A323DB0813C4D072957BA6FDA79D9776674CD06B_18; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_A323DB0813C4D072957BA6FDA79D9776674CD06B_18() { return &___A323DB0813C4D072957BA6FDA79D9776674CD06B_18; }
	inline void set_A323DB0813C4D072957BA6FDA79D9776674CD06B_18(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___A323DB0813C4D072957BA6FDA79D9776674CD06B_18 = value;
	}

	inline static int32_t get_offset_of_BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19)); }
	inline __StaticArrayInitTypeSizeU3D20_t1548391512  get_BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19() const { return ___BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19; }
	inline __StaticArrayInitTypeSizeU3D20_t1548391512 * get_address_of_BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19() { return &___BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19; }
	inline void set_BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19(__StaticArrayInitTypeSizeU3D20_t1548391512  value)
	{
		___BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19 = value;
	}

	inline static int32_t get_offset_of_BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20() const { return ___BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20() { return &___BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20; }
	inline void set_BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20 = value;
	}

	inline static int32_t get_offset_of_CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21)); }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836  get_CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21() const { return ___CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21; }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836 * get_address_of_CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21() { return &___CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21; }
	inline void set_CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21(__StaticArrayInitTypeSizeU3D64_t3517497836  value)
	{
		___CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21 = value;
	}

	inline static int32_t get_offset_of_D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22)); }
	inline __StaticArrayInitTypeSizeU3D32_t2711125390  get_D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22() const { return ___D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22; }
	inline __StaticArrayInitTypeSizeU3D32_t2711125390 * get_address_of_D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22() { return &___D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22; }
	inline void set_D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22(__StaticArrayInitTypeSizeU3D32_t2711125390  value)
	{
		___D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22 = value;
	}

	inline static int32_t get_offset_of_D693078666967609CB4A2B840921F9E052031847_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___D693078666967609CB4A2B840921F9E052031847_23)); }
	inline __StaticArrayInitTypeSizeU3D524_t4061560205  get_D693078666967609CB4A2B840921F9E052031847_23() const { return ___D693078666967609CB4A2B840921F9E052031847_23; }
	inline __StaticArrayInitTypeSizeU3D524_t4061560205 * get_address_of_D693078666967609CB4A2B840921F9E052031847_23() { return &___D693078666967609CB4A2B840921F9E052031847_23; }
	inline void set_D693078666967609CB4A2B840921F9E052031847_23(__StaticArrayInitTypeSizeU3D524_t4061560205  value)
	{
		___D693078666967609CB4A2B840921F9E052031847_23 = value;
	}

	inline static int32_t get_offset_of_E75835D001C843F156FBA01B001DFE1B8029AC17_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___E75835D001C843F156FBA01B001DFE1B8029AC17_24)); }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836  get_E75835D001C843F156FBA01B001DFE1B8029AC17_24() const { return ___E75835D001C843F156FBA01B001DFE1B8029AC17_24; }
	inline __StaticArrayInitTypeSizeU3D64_t3517497836 * get_address_of_E75835D001C843F156FBA01B001DFE1B8029AC17_24() { return &___E75835D001C843F156FBA01B001DFE1B8029AC17_24; }
	inline void set_E75835D001C843F156FBA01B001DFE1B8029AC17_24(__StaticArrayInitTypeSizeU3D64_t3517497836  value)
	{
		___E75835D001C843F156FBA01B001DFE1B8029AC17_24 = value;
	}

	inline static int32_t get_offset_of_EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25)); }
	inline __StaticArrayInitTypeSizeU3D10_t1548194904  get_EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25() const { return ___EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25; }
	inline __StaticArrayInitTypeSizeU3D10_t1548194904 * get_address_of_EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25() { return &___EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25; }
	inline void set_EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25(__StaticArrayInitTypeSizeU3D10_t1548194904  value)
	{
		___EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25 = value;
	}

	inline static int32_t get_offset_of_EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields, ___EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683  get_EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26() const { return ___EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885683 * get_address_of_EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26() { return &___EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26; }
	inline void set_EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26(__StaticArrayInitTypeSizeU3D3_t3217885683  value)
	{
		___EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255362_H
#ifndef FUNCTIONTYPE_T3319434782_H
#define FUNCTIONTYPE_T3319434782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Function/FunctionType
struct  FunctionType_t3319434782 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.Function/FunctionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FunctionType_t3319434782, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONTYPE_T3319434782_H
#ifndef XMLSTANDALONE_T2027388713_H
#define XMLSTANDALONE_T2027388713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlStandalone
struct  XmlStandalone_t2027388713 
{
public:
	// System.Int32 System.Xml.XmlStandalone::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlStandalone_t2027388713, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSTANDALONE_T2027388713_H
#ifndef HMAC_T3689525210_H
#define HMAC_T3689525210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.HMAC
struct  HMAC_t3689525210  : public KeyedHashAlgorithm_t112861511
{
public:
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Cryptography.HMAC::hash
	HashAlgorithm_t1432317219 * ___hash_5;
	// System.Boolean Mono.Security.Cryptography.HMAC::hashing
	bool ___hashing_6;
	// System.Byte[] Mono.Security.Cryptography.HMAC::innerPad
	ByteU5BU5D_t4116647657* ___innerPad_7;
	// System.Byte[] Mono.Security.Cryptography.HMAC::outerPad
	ByteU5BU5D_t4116647657* ___outerPad_8;

public:
	inline static int32_t get_offset_of_hash_5() { return static_cast<int32_t>(offsetof(HMAC_t3689525210, ___hash_5)); }
	inline HashAlgorithm_t1432317219 * get_hash_5() const { return ___hash_5; }
	inline HashAlgorithm_t1432317219 ** get_address_of_hash_5() { return &___hash_5; }
	inline void set_hash_5(HashAlgorithm_t1432317219 * value)
	{
		___hash_5 = value;
		Il2CppCodeGenWriteBarrier((&___hash_5), value);
	}

	inline static int32_t get_offset_of_hashing_6() { return static_cast<int32_t>(offsetof(HMAC_t3689525210, ___hashing_6)); }
	inline bool get_hashing_6() const { return ___hashing_6; }
	inline bool* get_address_of_hashing_6() { return &___hashing_6; }
	inline void set_hashing_6(bool value)
	{
		___hashing_6 = value;
	}

	inline static int32_t get_offset_of_innerPad_7() { return static_cast<int32_t>(offsetof(HMAC_t3689525210, ___innerPad_7)); }
	inline ByteU5BU5D_t4116647657* get_innerPad_7() const { return ___innerPad_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_innerPad_7() { return &___innerPad_7; }
	inline void set_innerPad_7(ByteU5BU5D_t4116647657* value)
	{
		___innerPad_7 = value;
		Il2CppCodeGenWriteBarrier((&___innerPad_7), value);
	}

	inline static int32_t get_offset_of_outerPad_8() { return static_cast<int32_t>(offsetof(HMAC_t3689525210, ___outerPad_8)); }
	inline ByteU5BU5D_t4116647657* get_outerPad_8() const { return ___outerPad_8; }
	inline ByteU5BU5D_t4116647657** get_address_of_outerPad_8() { return &___outerPad_8; }
	inline void set_outerPad_8(ByteU5BU5D_t4116647657* value)
	{
		___outerPad_8 = value;
		Il2CppCodeGenWriteBarrier((&___outerPad_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HMAC_T3689525210_H
#ifndef XMLOUTPUTMETHOD_T2185361861_H
#define XMLOUTPUTMETHOD_T2185361861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlOutputMethod
struct  XmlOutputMethod_t2185361861 
{
public:
	// System.Int32 System.Xml.XmlOutputMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlOutputMethod_t2185361861, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLOUTPUTMETHOD_T2185361861_H
#ifndef NEWLINEHANDLING_T850339274_H
#define NEWLINEHANDLING_T850339274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NewLineHandling
struct  NewLineHandling_t850339274 
{
public:
	// System.Int32 System.Xml.NewLineHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NewLineHandling_t850339274, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWLINEHANDLING_T850339274_H
#ifndef XPATHDOCUMENTNAVIGATOR_T2457178823_H
#define XPATHDOCUMENTNAVIGATOR_T2457178823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathDocumentNavigator
struct  XPathDocumentNavigator_t2457178823  : public XPathNavigator_t787956054
{
public:
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathDocumentNavigator::pageCurrent
	XPathNodeU5BU5D_t47339301* ___pageCurrent_4;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathDocumentNavigator::pageParent
	XPathNodeU5BU5D_t47339301* ___pageParent_5;
	// System.Int32 MS.Internal.Xml.Cache.XPathDocumentNavigator::idxCurrent
	int32_t ___idxCurrent_6;
	// System.Int32 MS.Internal.Xml.Cache.XPathDocumentNavigator::idxParent
	int32_t ___idxParent_7;

public:
	inline static int32_t get_offset_of_pageCurrent_4() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t2457178823, ___pageCurrent_4)); }
	inline XPathNodeU5BU5D_t47339301* get_pageCurrent_4() const { return ___pageCurrent_4; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_pageCurrent_4() { return &___pageCurrent_4; }
	inline void set_pageCurrent_4(XPathNodeU5BU5D_t47339301* value)
	{
		___pageCurrent_4 = value;
		Il2CppCodeGenWriteBarrier((&___pageCurrent_4), value);
	}

	inline static int32_t get_offset_of_pageParent_5() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t2457178823, ___pageParent_5)); }
	inline XPathNodeU5BU5D_t47339301* get_pageParent_5() const { return ___pageParent_5; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_pageParent_5() { return &___pageParent_5; }
	inline void set_pageParent_5(XPathNodeU5BU5D_t47339301* value)
	{
		___pageParent_5 = value;
		Il2CppCodeGenWriteBarrier((&___pageParent_5), value);
	}

	inline static int32_t get_offset_of_idxCurrent_6() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t2457178823, ___idxCurrent_6)); }
	inline int32_t get_idxCurrent_6() const { return ___idxCurrent_6; }
	inline int32_t* get_address_of_idxCurrent_6() { return &___idxCurrent_6; }
	inline void set_idxCurrent_6(int32_t value)
	{
		___idxCurrent_6 = value;
	}

	inline static int32_t get_offset_of_idxParent_7() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t2457178823, ___idxParent_7)); }
	inline int32_t get_idxParent_7() const { return ___idxParent_7; }
	inline int32_t* get_address_of_idxParent_7() { return &___idxParent_7; }
	inline void set_idxParent_7(int32_t value)
	{
		___idxParent_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHDOCUMENTNAVIGATOR_T2457178823_H
#ifndef LEXKIND_T864578899_H
#define LEXKIND_T864578899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.XPathScanner/LexKind
struct  LexKind_t864578899 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.XPathScanner/LexKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LexKind_t864578899, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEXKIND_T864578899_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef OP_T2046805169_H
#define OP_T2046805169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Operator/Op
struct  Op_t2046805169 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.Operator/Op::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Op_t2046805169, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OP_T2046805169_H
#ifndef ENTITYHANDLING_T1047276436_H
#define ENTITYHANDLING_T1047276436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.EntityHandling
struct  EntityHandling_t1047276436 
{
public:
	// System.Int32 System.Xml.EntityHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EntityHandling_t1047276436, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYHANDLING_T1047276436_H
#ifndef DTDPROCESSING_T1163997051_H
#define DTDPROCESSING_T1163997051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdProcessing
struct  DtdProcessing_t1163997051 
{
public:
	// System.Int32 System.Xml.DtdProcessing::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DtdProcessing_t1163997051, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPROCESSING_T1163997051_H
#ifndef AXISTYPE_T3322599580_H
#define AXISTYPE_T3322599580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Axis/AxisType
struct  AxisType_t3322599580 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.Axis/AxisType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisType_t3322599580, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTYPE_T3322599580_H
#ifndef CIPHERMODE_T84635067_H
#define CIPHERMODE_T84635067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CipherMode
struct  CipherMode_t84635067 
{
public:
	// System.Int32 System.Security.Cryptography.CipherMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherMode_t84635067, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERMODE_T84635067_H
#ifndef CONFORMANCELEVEL_T3899847875_H
#define CONFORMANCELEVEL_T3899847875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ConformanceLevel
struct  ConformanceLevel_t3899847875 
{
public:
	// System.Int32 System.Xml.ConformanceLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConformanceLevel_t3899847875, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFORMANCELEVEL_T3899847875_H
#ifndef SIGN_T3338384039_H
#define SIGN_T3338384039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.BigInteger/Sign
struct  Sign_t3338384039 
{
public:
	// System.Int32 Mono.Math.BigInteger/Sign::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Sign_t3338384039, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGN_T3338384039_H
#ifndef ASTTYPE_T3854428833_H
#define ASTTYPE_T3854428833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.AstNode/AstType
struct  AstType_t3854428833 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.AstNode/AstType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AstType_t3854428833, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTTYPE_T3854428833_H
#ifndef PADDINGMODE_T2546806710_H
#define PADDINGMODE_T2546806710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.PaddingMode
struct  PaddingMode_t2546806710 
{
public:
	// System.Int32 System.Security.Cryptography.PaddingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PaddingMode_t2546806710, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADDINGMODE_T2546806710_H
#ifndef XMLUTF8RAWTEXTWRITER_T2752398654_H
#define XMLUTF8RAWTEXTWRITER_T2752398654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUtf8RawTextWriter
struct  XmlUtf8RawTextWriter_t2752398654  : public XmlRawWriter_t722320575
{
public:
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::useAsync
	bool ___useAsync_3;
	// System.Byte[] System.Xml.XmlUtf8RawTextWriter::bufBytes
	ByteU5BU5D_t4116647657* ___bufBytes_4;
	// System.IO.Stream System.Xml.XmlUtf8RawTextWriter::stream
	Stream_t1273022909 * ___stream_5;
	// System.Text.Encoding System.Xml.XmlUtf8RawTextWriter::encoding
	Encoding_t1523322056 * ___encoding_6;
	// System.Xml.XmlCharType System.Xml.XmlUtf8RawTextWriter::xmlCharType
	XmlCharType_t2277243275  ___xmlCharType_7;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::bufPos
	int32_t ___bufPos_8;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::textPos
	int32_t ___textPos_9;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::contentPos
	int32_t ___contentPos_10;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::cdataPos
	int32_t ___cdataPos_11;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::attrEndPos
	int32_t ___attrEndPos_12;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::bufLen
	int32_t ___bufLen_13;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::writeToNull
	bool ___writeToNull_14;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::hadDoubleBracket
	bool ___hadDoubleBracket_15;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::inAttributeValue
	bool ___inAttributeValue_16;
	// System.Xml.NewLineHandling System.Xml.XmlUtf8RawTextWriter::newLineHandling
	int32_t ___newLineHandling_17;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::closeOutput
	bool ___closeOutput_18;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::omitXmlDeclaration
	bool ___omitXmlDeclaration_19;
	// System.String System.Xml.XmlUtf8RawTextWriter::newLineChars
	String_t* ___newLineChars_20;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::checkCharacters
	bool ___checkCharacters_21;
	// System.Xml.XmlStandalone System.Xml.XmlUtf8RawTextWriter::standalone
	int32_t ___standalone_22;
	// System.Xml.XmlOutputMethod System.Xml.XmlUtf8RawTextWriter::outputMethod
	int32_t ___outputMethod_23;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::autoXmlDeclaration
	bool ___autoXmlDeclaration_24;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::mergeCDataSections
	bool ___mergeCDataSections_25;

public:
	inline static int32_t get_offset_of_useAsync_3() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___useAsync_3)); }
	inline bool get_useAsync_3() const { return ___useAsync_3; }
	inline bool* get_address_of_useAsync_3() { return &___useAsync_3; }
	inline void set_useAsync_3(bool value)
	{
		___useAsync_3 = value;
	}

	inline static int32_t get_offset_of_bufBytes_4() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___bufBytes_4)); }
	inline ByteU5BU5D_t4116647657* get_bufBytes_4() const { return ___bufBytes_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_bufBytes_4() { return &___bufBytes_4; }
	inline void set_bufBytes_4(ByteU5BU5D_t4116647657* value)
	{
		___bufBytes_4 = value;
		Il2CppCodeGenWriteBarrier((&___bufBytes_4), value);
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___stream_5)); }
	inline Stream_t1273022909 * get_stream_5() const { return ___stream_5; }
	inline Stream_t1273022909 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_t1273022909 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___encoding_6)); }
	inline Encoding_t1523322056 * get_encoding_6() const { return ___encoding_6; }
	inline Encoding_t1523322056 ** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(Encoding_t1523322056 * value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_6), value);
	}

	inline static int32_t get_offset_of_xmlCharType_7() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___xmlCharType_7)); }
	inline XmlCharType_t2277243275  get_xmlCharType_7() const { return ___xmlCharType_7; }
	inline XmlCharType_t2277243275 * get_address_of_xmlCharType_7() { return &___xmlCharType_7; }
	inline void set_xmlCharType_7(XmlCharType_t2277243275  value)
	{
		___xmlCharType_7 = value;
	}

	inline static int32_t get_offset_of_bufPos_8() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___bufPos_8)); }
	inline int32_t get_bufPos_8() const { return ___bufPos_8; }
	inline int32_t* get_address_of_bufPos_8() { return &___bufPos_8; }
	inline void set_bufPos_8(int32_t value)
	{
		___bufPos_8 = value;
	}

	inline static int32_t get_offset_of_textPos_9() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___textPos_9)); }
	inline int32_t get_textPos_9() const { return ___textPos_9; }
	inline int32_t* get_address_of_textPos_9() { return &___textPos_9; }
	inline void set_textPos_9(int32_t value)
	{
		___textPos_9 = value;
	}

	inline static int32_t get_offset_of_contentPos_10() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___contentPos_10)); }
	inline int32_t get_contentPos_10() const { return ___contentPos_10; }
	inline int32_t* get_address_of_contentPos_10() { return &___contentPos_10; }
	inline void set_contentPos_10(int32_t value)
	{
		___contentPos_10 = value;
	}

	inline static int32_t get_offset_of_cdataPos_11() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___cdataPos_11)); }
	inline int32_t get_cdataPos_11() const { return ___cdataPos_11; }
	inline int32_t* get_address_of_cdataPos_11() { return &___cdataPos_11; }
	inline void set_cdataPos_11(int32_t value)
	{
		___cdataPos_11 = value;
	}

	inline static int32_t get_offset_of_attrEndPos_12() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___attrEndPos_12)); }
	inline int32_t get_attrEndPos_12() const { return ___attrEndPos_12; }
	inline int32_t* get_address_of_attrEndPos_12() { return &___attrEndPos_12; }
	inline void set_attrEndPos_12(int32_t value)
	{
		___attrEndPos_12 = value;
	}

	inline static int32_t get_offset_of_bufLen_13() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___bufLen_13)); }
	inline int32_t get_bufLen_13() const { return ___bufLen_13; }
	inline int32_t* get_address_of_bufLen_13() { return &___bufLen_13; }
	inline void set_bufLen_13(int32_t value)
	{
		___bufLen_13 = value;
	}

	inline static int32_t get_offset_of_writeToNull_14() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___writeToNull_14)); }
	inline bool get_writeToNull_14() const { return ___writeToNull_14; }
	inline bool* get_address_of_writeToNull_14() { return &___writeToNull_14; }
	inline void set_writeToNull_14(bool value)
	{
		___writeToNull_14 = value;
	}

	inline static int32_t get_offset_of_hadDoubleBracket_15() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___hadDoubleBracket_15)); }
	inline bool get_hadDoubleBracket_15() const { return ___hadDoubleBracket_15; }
	inline bool* get_address_of_hadDoubleBracket_15() { return &___hadDoubleBracket_15; }
	inline void set_hadDoubleBracket_15(bool value)
	{
		___hadDoubleBracket_15 = value;
	}

	inline static int32_t get_offset_of_inAttributeValue_16() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___inAttributeValue_16)); }
	inline bool get_inAttributeValue_16() const { return ___inAttributeValue_16; }
	inline bool* get_address_of_inAttributeValue_16() { return &___inAttributeValue_16; }
	inline void set_inAttributeValue_16(bool value)
	{
		___inAttributeValue_16 = value;
	}

	inline static int32_t get_offset_of_newLineHandling_17() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___newLineHandling_17)); }
	inline int32_t get_newLineHandling_17() const { return ___newLineHandling_17; }
	inline int32_t* get_address_of_newLineHandling_17() { return &___newLineHandling_17; }
	inline void set_newLineHandling_17(int32_t value)
	{
		___newLineHandling_17 = value;
	}

	inline static int32_t get_offset_of_closeOutput_18() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___closeOutput_18)); }
	inline bool get_closeOutput_18() const { return ___closeOutput_18; }
	inline bool* get_address_of_closeOutput_18() { return &___closeOutput_18; }
	inline void set_closeOutput_18(bool value)
	{
		___closeOutput_18 = value;
	}

	inline static int32_t get_offset_of_omitXmlDeclaration_19() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___omitXmlDeclaration_19)); }
	inline bool get_omitXmlDeclaration_19() const { return ___omitXmlDeclaration_19; }
	inline bool* get_address_of_omitXmlDeclaration_19() { return &___omitXmlDeclaration_19; }
	inline void set_omitXmlDeclaration_19(bool value)
	{
		___omitXmlDeclaration_19 = value;
	}

	inline static int32_t get_offset_of_newLineChars_20() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___newLineChars_20)); }
	inline String_t* get_newLineChars_20() const { return ___newLineChars_20; }
	inline String_t** get_address_of_newLineChars_20() { return &___newLineChars_20; }
	inline void set_newLineChars_20(String_t* value)
	{
		___newLineChars_20 = value;
		Il2CppCodeGenWriteBarrier((&___newLineChars_20), value);
	}

	inline static int32_t get_offset_of_checkCharacters_21() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___checkCharacters_21)); }
	inline bool get_checkCharacters_21() const { return ___checkCharacters_21; }
	inline bool* get_address_of_checkCharacters_21() { return &___checkCharacters_21; }
	inline void set_checkCharacters_21(bool value)
	{
		___checkCharacters_21 = value;
	}

	inline static int32_t get_offset_of_standalone_22() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___standalone_22)); }
	inline int32_t get_standalone_22() const { return ___standalone_22; }
	inline int32_t* get_address_of_standalone_22() { return &___standalone_22; }
	inline void set_standalone_22(int32_t value)
	{
		___standalone_22 = value;
	}

	inline static int32_t get_offset_of_outputMethod_23() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___outputMethod_23)); }
	inline int32_t get_outputMethod_23() const { return ___outputMethod_23; }
	inline int32_t* get_address_of_outputMethod_23() { return &___outputMethod_23; }
	inline void set_outputMethod_23(int32_t value)
	{
		___outputMethod_23 = value;
	}

	inline static int32_t get_offset_of_autoXmlDeclaration_24() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___autoXmlDeclaration_24)); }
	inline bool get_autoXmlDeclaration_24() const { return ___autoXmlDeclaration_24; }
	inline bool* get_address_of_autoXmlDeclaration_24() { return &___autoXmlDeclaration_24; }
	inline void set_autoXmlDeclaration_24(bool value)
	{
		___autoXmlDeclaration_24 = value;
	}

	inline static int32_t get_offset_of_mergeCDataSections_25() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___mergeCDataSections_25)); }
	inline bool get_mergeCDataSections_25() const { return ___mergeCDataSections_25; }
	inline bool* get_address_of_mergeCDataSections_25() { return &___mergeCDataSections_25; }
	inline void set_mergeCDataSections_25(bool value)
	{
		___mergeCDataSections_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLUTF8RAWTEXTWRITER_T2752398654_H
#ifndef XMLENCODEDRAWTEXTWRITER_T977350554_H
#define XMLENCODEDRAWTEXTWRITER_T977350554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEncodedRawTextWriter
struct  XmlEncodedRawTextWriter_t977350554  : public XmlRawWriter_t722320575
{
public:
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::useAsync
	bool ___useAsync_3;
	// System.Byte[] System.Xml.XmlEncodedRawTextWriter::bufBytes
	ByteU5BU5D_t4116647657* ___bufBytes_4;
	// System.IO.Stream System.Xml.XmlEncodedRawTextWriter::stream
	Stream_t1273022909 * ___stream_5;
	// System.Text.Encoding System.Xml.XmlEncodedRawTextWriter::encoding
	Encoding_t1523322056 * ___encoding_6;
	// System.Xml.XmlCharType System.Xml.XmlEncodedRawTextWriter::xmlCharType
	XmlCharType_t2277243275  ___xmlCharType_7;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::bufPos
	int32_t ___bufPos_8;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::textPos
	int32_t ___textPos_9;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::contentPos
	int32_t ___contentPos_10;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::cdataPos
	int32_t ___cdataPos_11;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::attrEndPos
	int32_t ___attrEndPos_12;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::bufLen
	int32_t ___bufLen_13;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::writeToNull
	bool ___writeToNull_14;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::hadDoubleBracket
	bool ___hadDoubleBracket_15;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::inAttributeValue
	bool ___inAttributeValue_16;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::bufBytesUsed
	int32_t ___bufBytesUsed_17;
	// System.Char[] System.Xml.XmlEncodedRawTextWriter::bufChars
	CharU5BU5D_t3528271667* ___bufChars_18;
	// System.Text.Encoder System.Xml.XmlEncodedRawTextWriter::encoder
	Encoder_t2198218980 * ___encoder_19;
	// System.IO.TextWriter System.Xml.XmlEncodedRawTextWriter::writer
	TextWriter_t3478189236 * ___writer_20;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::trackTextContent
	bool ___trackTextContent_21;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::inTextContent
	bool ___inTextContent_22;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::lastMarkPos
	int32_t ___lastMarkPos_23;
	// System.Int32[] System.Xml.XmlEncodedRawTextWriter::textContentMarks
	Int32U5BU5D_t385246372* ___textContentMarks_24;
	// System.Xml.CharEntityEncoderFallback System.Xml.XmlEncodedRawTextWriter::charEntityFallback
	CharEntityEncoderFallback_t110445598 * ___charEntityFallback_25;
	// System.Xml.NewLineHandling System.Xml.XmlEncodedRawTextWriter::newLineHandling
	int32_t ___newLineHandling_26;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::closeOutput
	bool ___closeOutput_27;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::omitXmlDeclaration
	bool ___omitXmlDeclaration_28;
	// System.String System.Xml.XmlEncodedRawTextWriter::newLineChars
	String_t* ___newLineChars_29;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::checkCharacters
	bool ___checkCharacters_30;
	// System.Xml.XmlStandalone System.Xml.XmlEncodedRawTextWriter::standalone
	int32_t ___standalone_31;
	// System.Xml.XmlOutputMethod System.Xml.XmlEncodedRawTextWriter::outputMethod
	int32_t ___outputMethod_32;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::autoXmlDeclaration
	bool ___autoXmlDeclaration_33;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::mergeCDataSections
	bool ___mergeCDataSections_34;

public:
	inline static int32_t get_offset_of_useAsync_3() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___useAsync_3)); }
	inline bool get_useAsync_3() const { return ___useAsync_3; }
	inline bool* get_address_of_useAsync_3() { return &___useAsync_3; }
	inline void set_useAsync_3(bool value)
	{
		___useAsync_3 = value;
	}

	inline static int32_t get_offset_of_bufBytes_4() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___bufBytes_4)); }
	inline ByteU5BU5D_t4116647657* get_bufBytes_4() const { return ___bufBytes_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_bufBytes_4() { return &___bufBytes_4; }
	inline void set_bufBytes_4(ByteU5BU5D_t4116647657* value)
	{
		___bufBytes_4 = value;
		Il2CppCodeGenWriteBarrier((&___bufBytes_4), value);
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___stream_5)); }
	inline Stream_t1273022909 * get_stream_5() const { return ___stream_5; }
	inline Stream_t1273022909 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_t1273022909 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___encoding_6)); }
	inline Encoding_t1523322056 * get_encoding_6() const { return ___encoding_6; }
	inline Encoding_t1523322056 ** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(Encoding_t1523322056 * value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_6), value);
	}

	inline static int32_t get_offset_of_xmlCharType_7() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___xmlCharType_7)); }
	inline XmlCharType_t2277243275  get_xmlCharType_7() const { return ___xmlCharType_7; }
	inline XmlCharType_t2277243275 * get_address_of_xmlCharType_7() { return &___xmlCharType_7; }
	inline void set_xmlCharType_7(XmlCharType_t2277243275  value)
	{
		___xmlCharType_7 = value;
	}

	inline static int32_t get_offset_of_bufPos_8() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___bufPos_8)); }
	inline int32_t get_bufPos_8() const { return ___bufPos_8; }
	inline int32_t* get_address_of_bufPos_8() { return &___bufPos_8; }
	inline void set_bufPos_8(int32_t value)
	{
		___bufPos_8 = value;
	}

	inline static int32_t get_offset_of_textPos_9() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___textPos_9)); }
	inline int32_t get_textPos_9() const { return ___textPos_9; }
	inline int32_t* get_address_of_textPos_9() { return &___textPos_9; }
	inline void set_textPos_9(int32_t value)
	{
		___textPos_9 = value;
	}

	inline static int32_t get_offset_of_contentPos_10() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___contentPos_10)); }
	inline int32_t get_contentPos_10() const { return ___contentPos_10; }
	inline int32_t* get_address_of_contentPos_10() { return &___contentPos_10; }
	inline void set_contentPos_10(int32_t value)
	{
		___contentPos_10 = value;
	}

	inline static int32_t get_offset_of_cdataPos_11() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___cdataPos_11)); }
	inline int32_t get_cdataPos_11() const { return ___cdataPos_11; }
	inline int32_t* get_address_of_cdataPos_11() { return &___cdataPos_11; }
	inline void set_cdataPos_11(int32_t value)
	{
		___cdataPos_11 = value;
	}

	inline static int32_t get_offset_of_attrEndPos_12() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___attrEndPos_12)); }
	inline int32_t get_attrEndPos_12() const { return ___attrEndPos_12; }
	inline int32_t* get_address_of_attrEndPos_12() { return &___attrEndPos_12; }
	inline void set_attrEndPos_12(int32_t value)
	{
		___attrEndPos_12 = value;
	}

	inline static int32_t get_offset_of_bufLen_13() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___bufLen_13)); }
	inline int32_t get_bufLen_13() const { return ___bufLen_13; }
	inline int32_t* get_address_of_bufLen_13() { return &___bufLen_13; }
	inline void set_bufLen_13(int32_t value)
	{
		___bufLen_13 = value;
	}

	inline static int32_t get_offset_of_writeToNull_14() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___writeToNull_14)); }
	inline bool get_writeToNull_14() const { return ___writeToNull_14; }
	inline bool* get_address_of_writeToNull_14() { return &___writeToNull_14; }
	inline void set_writeToNull_14(bool value)
	{
		___writeToNull_14 = value;
	}

	inline static int32_t get_offset_of_hadDoubleBracket_15() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___hadDoubleBracket_15)); }
	inline bool get_hadDoubleBracket_15() const { return ___hadDoubleBracket_15; }
	inline bool* get_address_of_hadDoubleBracket_15() { return &___hadDoubleBracket_15; }
	inline void set_hadDoubleBracket_15(bool value)
	{
		___hadDoubleBracket_15 = value;
	}

	inline static int32_t get_offset_of_inAttributeValue_16() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___inAttributeValue_16)); }
	inline bool get_inAttributeValue_16() const { return ___inAttributeValue_16; }
	inline bool* get_address_of_inAttributeValue_16() { return &___inAttributeValue_16; }
	inline void set_inAttributeValue_16(bool value)
	{
		___inAttributeValue_16 = value;
	}

	inline static int32_t get_offset_of_bufBytesUsed_17() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___bufBytesUsed_17)); }
	inline int32_t get_bufBytesUsed_17() const { return ___bufBytesUsed_17; }
	inline int32_t* get_address_of_bufBytesUsed_17() { return &___bufBytesUsed_17; }
	inline void set_bufBytesUsed_17(int32_t value)
	{
		___bufBytesUsed_17 = value;
	}

	inline static int32_t get_offset_of_bufChars_18() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___bufChars_18)); }
	inline CharU5BU5D_t3528271667* get_bufChars_18() const { return ___bufChars_18; }
	inline CharU5BU5D_t3528271667** get_address_of_bufChars_18() { return &___bufChars_18; }
	inline void set_bufChars_18(CharU5BU5D_t3528271667* value)
	{
		___bufChars_18 = value;
		Il2CppCodeGenWriteBarrier((&___bufChars_18), value);
	}

	inline static int32_t get_offset_of_encoder_19() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___encoder_19)); }
	inline Encoder_t2198218980 * get_encoder_19() const { return ___encoder_19; }
	inline Encoder_t2198218980 ** get_address_of_encoder_19() { return &___encoder_19; }
	inline void set_encoder_19(Encoder_t2198218980 * value)
	{
		___encoder_19 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_19), value);
	}

	inline static int32_t get_offset_of_writer_20() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___writer_20)); }
	inline TextWriter_t3478189236 * get_writer_20() const { return ___writer_20; }
	inline TextWriter_t3478189236 ** get_address_of_writer_20() { return &___writer_20; }
	inline void set_writer_20(TextWriter_t3478189236 * value)
	{
		___writer_20 = value;
		Il2CppCodeGenWriteBarrier((&___writer_20), value);
	}

	inline static int32_t get_offset_of_trackTextContent_21() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___trackTextContent_21)); }
	inline bool get_trackTextContent_21() const { return ___trackTextContent_21; }
	inline bool* get_address_of_trackTextContent_21() { return &___trackTextContent_21; }
	inline void set_trackTextContent_21(bool value)
	{
		___trackTextContent_21 = value;
	}

	inline static int32_t get_offset_of_inTextContent_22() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___inTextContent_22)); }
	inline bool get_inTextContent_22() const { return ___inTextContent_22; }
	inline bool* get_address_of_inTextContent_22() { return &___inTextContent_22; }
	inline void set_inTextContent_22(bool value)
	{
		___inTextContent_22 = value;
	}

	inline static int32_t get_offset_of_lastMarkPos_23() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___lastMarkPos_23)); }
	inline int32_t get_lastMarkPos_23() const { return ___lastMarkPos_23; }
	inline int32_t* get_address_of_lastMarkPos_23() { return &___lastMarkPos_23; }
	inline void set_lastMarkPos_23(int32_t value)
	{
		___lastMarkPos_23 = value;
	}

	inline static int32_t get_offset_of_textContentMarks_24() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___textContentMarks_24)); }
	inline Int32U5BU5D_t385246372* get_textContentMarks_24() const { return ___textContentMarks_24; }
	inline Int32U5BU5D_t385246372** get_address_of_textContentMarks_24() { return &___textContentMarks_24; }
	inline void set_textContentMarks_24(Int32U5BU5D_t385246372* value)
	{
		___textContentMarks_24 = value;
		Il2CppCodeGenWriteBarrier((&___textContentMarks_24), value);
	}

	inline static int32_t get_offset_of_charEntityFallback_25() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___charEntityFallback_25)); }
	inline CharEntityEncoderFallback_t110445598 * get_charEntityFallback_25() const { return ___charEntityFallback_25; }
	inline CharEntityEncoderFallback_t110445598 ** get_address_of_charEntityFallback_25() { return &___charEntityFallback_25; }
	inline void set_charEntityFallback_25(CharEntityEncoderFallback_t110445598 * value)
	{
		___charEntityFallback_25 = value;
		Il2CppCodeGenWriteBarrier((&___charEntityFallback_25), value);
	}

	inline static int32_t get_offset_of_newLineHandling_26() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___newLineHandling_26)); }
	inline int32_t get_newLineHandling_26() const { return ___newLineHandling_26; }
	inline int32_t* get_address_of_newLineHandling_26() { return &___newLineHandling_26; }
	inline void set_newLineHandling_26(int32_t value)
	{
		___newLineHandling_26 = value;
	}

	inline static int32_t get_offset_of_closeOutput_27() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___closeOutput_27)); }
	inline bool get_closeOutput_27() const { return ___closeOutput_27; }
	inline bool* get_address_of_closeOutput_27() { return &___closeOutput_27; }
	inline void set_closeOutput_27(bool value)
	{
		___closeOutput_27 = value;
	}

	inline static int32_t get_offset_of_omitXmlDeclaration_28() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___omitXmlDeclaration_28)); }
	inline bool get_omitXmlDeclaration_28() const { return ___omitXmlDeclaration_28; }
	inline bool* get_address_of_omitXmlDeclaration_28() { return &___omitXmlDeclaration_28; }
	inline void set_omitXmlDeclaration_28(bool value)
	{
		___omitXmlDeclaration_28 = value;
	}

	inline static int32_t get_offset_of_newLineChars_29() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___newLineChars_29)); }
	inline String_t* get_newLineChars_29() const { return ___newLineChars_29; }
	inline String_t** get_address_of_newLineChars_29() { return &___newLineChars_29; }
	inline void set_newLineChars_29(String_t* value)
	{
		___newLineChars_29 = value;
		Il2CppCodeGenWriteBarrier((&___newLineChars_29), value);
	}

	inline static int32_t get_offset_of_checkCharacters_30() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___checkCharacters_30)); }
	inline bool get_checkCharacters_30() const { return ___checkCharacters_30; }
	inline bool* get_address_of_checkCharacters_30() { return &___checkCharacters_30; }
	inline void set_checkCharacters_30(bool value)
	{
		___checkCharacters_30 = value;
	}

	inline static int32_t get_offset_of_standalone_31() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___standalone_31)); }
	inline int32_t get_standalone_31() const { return ___standalone_31; }
	inline int32_t* get_address_of_standalone_31() { return &___standalone_31; }
	inline void set_standalone_31(int32_t value)
	{
		___standalone_31 = value;
	}

	inline static int32_t get_offset_of_outputMethod_32() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___outputMethod_32)); }
	inline int32_t get_outputMethod_32() const { return ___outputMethod_32; }
	inline int32_t* get_address_of_outputMethod_32() { return &___outputMethod_32; }
	inline void set_outputMethod_32(int32_t value)
	{
		___outputMethod_32 = value;
	}

	inline static int32_t get_offset_of_autoXmlDeclaration_33() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___autoXmlDeclaration_33)); }
	inline bool get_autoXmlDeclaration_33() const { return ___autoXmlDeclaration_33; }
	inline bool* get_address_of_autoXmlDeclaration_33() { return &___autoXmlDeclaration_33; }
	inline void set_autoXmlDeclaration_33(bool value)
	{
		___autoXmlDeclaration_33 = value;
	}

	inline static int32_t get_offset_of_mergeCDataSections_34() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___mergeCDataSections_34)); }
	inline bool get_mergeCDataSections_34() const { return ___mergeCDataSections_34; }
	inline bool* get_address_of_mergeCDataSections_34() { return &___mergeCDataSections_34; }
	inline void set_mergeCDataSections_34(bool value)
	{
		___mergeCDataSections_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENCODEDRAWTEXTWRITER_T977350554_H
#ifndef OPERATOR_T966760113_H
#define OPERATOR_T966760113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Operator
struct  Operator_t966760113  : public AstNode_t2514041814
{
public:
	// MS.Internal.Xml.XPath.Operator/Op MS.Internal.Xml.XPath.Operator::opType
	int32_t ___opType_1;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Operator::opnd1
	AstNode_t2514041814 * ___opnd1_2;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Operator::opnd2
	AstNode_t2514041814 * ___opnd2_3;

public:
	inline static int32_t get_offset_of_opType_1() { return static_cast<int32_t>(offsetof(Operator_t966760113, ___opType_1)); }
	inline int32_t get_opType_1() const { return ___opType_1; }
	inline int32_t* get_address_of_opType_1() { return &___opType_1; }
	inline void set_opType_1(int32_t value)
	{
		___opType_1 = value;
	}

	inline static int32_t get_offset_of_opnd1_2() { return static_cast<int32_t>(offsetof(Operator_t966760113, ___opnd1_2)); }
	inline AstNode_t2514041814 * get_opnd1_2() const { return ___opnd1_2; }
	inline AstNode_t2514041814 ** get_address_of_opnd1_2() { return &___opnd1_2; }
	inline void set_opnd1_2(AstNode_t2514041814 * value)
	{
		___opnd1_2 = value;
		Il2CppCodeGenWriteBarrier((&___opnd1_2), value);
	}

	inline static int32_t get_offset_of_opnd2_3() { return static_cast<int32_t>(offsetof(Operator_t966760113, ___opnd2_3)); }
	inline AstNode_t2514041814 * get_opnd2_3() const { return ___opnd2_3; }
	inline AstNode_t2514041814 ** get_address_of_opnd2_3() { return &___opnd2_3; }
	inline void set_opnd2_3(AstNode_t2514041814 * value)
	{
		___opnd2_3 = value;
		Il2CppCodeGenWriteBarrier((&___opnd2_3), value);
	}
};

struct Operator_t966760113_StaticFields
{
public:
	// MS.Internal.Xml.XPath.Operator/Op[] MS.Internal.Xml.XPath.Operator::invertOp
	OpU5BU5D_t2837398892* ___invertOp_0;

public:
	inline static int32_t get_offset_of_invertOp_0() { return static_cast<int32_t>(offsetof(Operator_t966760113_StaticFields, ___invertOp_0)); }
	inline OpU5BU5D_t2837398892* get_invertOp_0() const { return ___invertOp_0; }
	inline OpU5BU5D_t2837398892** get_address_of_invertOp_0() { return &___invertOp_0; }
	inline void set_invertOp_0(OpU5BU5D_t2837398892* value)
	{
		___invertOp_0 = value;
		Il2CppCodeGenWriteBarrier((&___invertOp_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOR_T966760113_H
#ifndef PARAMINFO_T1233379796_H
#define PARAMINFO_T1233379796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.XPathParser/ParamInfo
struct  ParamInfo_t1233379796  : public RuntimeObject
{
public:
	// MS.Internal.Xml.XPath.Function/FunctionType MS.Internal.Xml.XPath.XPathParser/ParamInfo::ftype
	int32_t ___ftype_0;
	// System.Int32 MS.Internal.Xml.XPath.XPathParser/ParamInfo::minargs
	int32_t ___minargs_1;
	// System.Int32 MS.Internal.Xml.XPath.XPathParser/ParamInfo::maxargs
	int32_t ___maxargs_2;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser/ParamInfo::argTypes
	XPathResultTypeU5BU5D_t1515527577* ___argTypes_3;

public:
	inline static int32_t get_offset_of_ftype_0() { return static_cast<int32_t>(offsetof(ParamInfo_t1233379796, ___ftype_0)); }
	inline int32_t get_ftype_0() const { return ___ftype_0; }
	inline int32_t* get_address_of_ftype_0() { return &___ftype_0; }
	inline void set_ftype_0(int32_t value)
	{
		___ftype_0 = value;
	}

	inline static int32_t get_offset_of_minargs_1() { return static_cast<int32_t>(offsetof(ParamInfo_t1233379796, ___minargs_1)); }
	inline int32_t get_minargs_1() const { return ___minargs_1; }
	inline int32_t* get_address_of_minargs_1() { return &___minargs_1; }
	inline void set_minargs_1(int32_t value)
	{
		___minargs_1 = value;
	}

	inline static int32_t get_offset_of_maxargs_2() { return static_cast<int32_t>(offsetof(ParamInfo_t1233379796, ___maxargs_2)); }
	inline int32_t get_maxargs_2() const { return ___maxargs_2; }
	inline int32_t* get_address_of_maxargs_2() { return &___maxargs_2; }
	inline void set_maxargs_2(int32_t value)
	{
		___maxargs_2 = value;
	}

	inline static int32_t get_offset_of_argTypes_3() { return static_cast<int32_t>(offsetof(ParamInfo_t1233379796, ___argTypes_3)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_argTypes_3() const { return ___argTypes_3; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_argTypes_3() { return &___argTypes_3; }
	inline void set_argTypes_3(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___argTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___argTypes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMINFO_T1233379796_H
#ifndef XPATHSCANNER_T3283201025_H
#define XPATHSCANNER_T3283201025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.XPathScanner
struct  XPathScanner_t3283201025  : public RuntimeObject
{
public:
	// System.String MS.Internal.Xml.XPath.XPathScanner::xpathExpr
	String_t* ___xpathExpr_0;
	// System.Int32 MS.Internal.Xml.XPath.XPathScanner::xpathExprIndex
	int32_t ___xpathExprIndex_1;
	// MS.Internal.Xml.XPath.XPathScanner/LexKind MS.Internal.Xml.XPath.XPathScanner::kind
	int32_t ___kind_2;
	// System.Char MS.Internal.Xml.XPath.XPathScanner::currentChar
	Il2CppChar ___currentChar_3;
	// System.String MS.Internal.Xml.XPath.XPathScanner::name
	String_t* ___name_4;
	// System.String MS.Internal.Xml.XPath.XPathScanner::prefix
	String_t* ___prefix_5;
	// System.String MS.Internal.Xml.XPath.XPathScanner::stringValue
	String_t* ___stringValue_6;
	// System.Double MS.Internal.Xml.XPath.XPathScanner::numberValue
	double ___numberValue_7;
	// System.Boolean MS.Internal.Xml.XPath.XPathScanner::canBeFunction
	bool ___canBeFunction_8;
	// System.Xml.XmlCharType MS.Internal.Xml.XPath.XPathScanner::xmlCharType
	XmlCharType_t2277243275  ___xmlCharType_9;

public:
	inline static int32_t get_offset_of_xpathExpr_0() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___xpathExpr_0)); }
	inline String_t* get_xpathExpr_0() const { return ___xpathExpr_0; }
	inline String_t** get_address_of_xpathExpr_0() { return &___xpathExpr_0; }
	inline void set_xpathExpr_0(String_t* value)
	{
		___xpathExpr_0 = value;
		Il2CppCodeGenWriteBarrier((&___xpathExpr_0), value);
	}

	inline static int32_t get_offset_of_xpathExprIndex_1() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___xpathExprIndex_1)); }
	inline int32_t get_xpathExprIndex_1() const { return ___xpathExprIndex_1; }
	inline int32_t* get_address_of_xpathExprIndex_1() { return &___xpathExprIndex_1; }
	inline void set_xpathExprIndex_1(int32_t value)
	{
		___xpathExprIndex_1 = value;
	}

	inline static int32_t get_offset_of_kind_2() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___kind_2)); }
	inline int32_t get_kind_2() const { return ___kind_2; }
	inline int32_t* get_address_of_kind_2() { return &___kind_2; }
	inline void set_kind_2(int32_t value)
	{
		___kind_2 = value;
	}

	inline static int32_t get_offset_of_currentChar_3() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___currentChar_3)); }
	inline Il2CppChar get_currentChar_3() const { return ___currentChar_3; }
	inline Il2CppChar* get_address_of_currentChar_3() { return &___currentChar_3; }
	inline void set_currentChar_3(Il2CppChar value)
	{
		___currentChar_3 = value;
	}

	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier((&___name_4), value);
	}

	inline static int32_t get_offset_of_prefix_5() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___prefix_5)); }
	inline String_t* get_prefix_5() const { return ___prefix_5; }
	inline String_t** get_address_of_prefix_5() { return &___prefix_5; }
	inline void set_prefix_5(String_t* value)
	{
		___prefix_5 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_5), value);
	}

	inline static int32_t get_offset_of_stringValue_6() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___stringValue_6)); }
	inline String_t* get_stringValue_6() const { return ___stringValue_6; }
	inline String_t** get_address_of_stringValue_6() { return &___stringValue_6; }
	inline void set_stringValue_6(String_t* value)
	{
		___stringValue_6 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_6), value);
	}

	inline static int32_t get_offset_of_numberValue_7() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___numberValue_7)); }
	inline double get_numberValue_7() const { return ___numberValue_7; }
	inline double* get_address_of_numberValue_7() { return &___numberValue_7; }
	inline void set_numberValue_7(double value)
	{
		___numberValue_7 = value;
	}

	inline static int32_t get_offset_of_canBeFunction_8() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___canBeFunction_8)); }
	inline bool get_canBeFunction_8() const { return ___canBeFunction_8; }
	inline bool* get_address_of_canBeFunction_8() { return &___canBeFunction_8; }
	inline void set_canBeFunction_8(bool value)
	{
		___canBeFunction_8 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_9() { return static_cast<int32_t>(offsetof(XPathScanner_t3283201025, ___xmlCharType_9)); }
	inline XmlCharType_t2277243275  get_xmlCharType_9() const { return ___xmlCharType_9; }
	inline XmlCharType_t2277243275 * get_address_of_xmlCharType_9() { return &___xmlCharType_9; }
	inline void set_xmlCharType_9(XmlCharType_t2277243275  value)
	{
		___xmlCharType_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHSCANNER_T3283201025_H
#ifndef AXIS_T4207104559_H
#define AXIS_T4207104559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Axis
struct  Axis_t4207104559  : public AstNode_t2514041814
{
public:
	// MS.Internal.Xml.XPath.Axis/AxisType MS.Internal.Xml.XPath.Axis::axisType
	int32_t ___axisType_0;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Axis::input
	AstNode_t2514041814 * ___input_1;
	// System.String MS.Internal.Xml.XPath.Axis::prefix
	String_t* ___prefix_2;
	// System.String MS.Internal.Xml.XPath.Axis::name
	String_t* ___name_3;
	// System.Xml.XPath.XPathNodeType MS.Internal.Xml.XPath.Axis::nodeType
	int32_t ___nodeType_4;
	// System.Boolean MS.Internal.Xml.XPath.Axis::abbrAxis
	bool ___abbrAxis_5;
	// System.String MS.Internal.Xml.XPath.Axis::urn
	String_t* ___urn_6;

public:
	inline static int32_t get_offset_of_axisType_0() { return static_cast<int32_t>(offsetof(Axis_t4207104559, ___axisType_0)); }
	inline int32_t get_axisType_0() const { return ___axisType_0; }
	inline int32_t* get_address_of_axisType_0() { return &___axisType_0; }
	inline void set_axisType_0(int32_t value)
	{
		___axisType_0 = value;
	}

	inline static int32_t get_offset_of_input_1() { return static_cast<int32_t>(offsetof(Axis_t4207104559, ___input_1)); }
	inline AstNode_t2514041814 * get_input_1() const { return ___input_1; }
	inline AstNode_t2514041814 ** get_address_of_input_1() { return &___input_1; }
	inline void set_input_1(AstNode_t2514041814 * value)
	{
		___input_1 = value;
		Il2CppCodeGenWriteBarrier((&___input_1), value);
	}

	inline static int32_t get_offset_of_prefix_2() { return static_cast<int32_t>(offsetof(Axis_t4207104559, ___prefix_2)); }
	inline String_t* get_prefix_2() const { return ___prefix_2; }
	inline String_t** get_address_of_prefix_2() { return &___prefix_2; }
	inline void set_prefix_2(String_t* value)
	{
		___prefix_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_2), value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(Axis_t4207104559, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_nodeType_4() { return static_cast<int32_t>(offsetof(Axis_t4207104559, ___nodeType_4)); }
	inline int32_t get_nodeType_4() const { return ___nodeType_4; }
	inline int32_t* get_address_of_nodeType_4() { return &___nodeType_4; }
	inline void set_nodeType_4(int32_t value)
	{
		___nodeType_4 = value;
	}

	inline static int32_t get_offset_of_abbrAxis_5() { return static_cast<int32_t>(offsetof(Axis_t4207104559, ___abbrAxis_5)); }
	inline bool get_abbrAxis_5() const { return ___abbrAxis_5; }
	inline bool* get_address_of_abbrAxis_5() { return &___abbrAxis_5; }
	inline void set_abbrAxis_5(bool value)
	{
		___abbrAxis_5 = value;
	}

	inline static int32_t get_offset_of_urn_6() { return static_cast<int32_t>(offsetof(Axis_t4207104559, ___urn_6)); }
	inline String_t* get_urn_6() const { return ___urn_6; }
	inline String_t** get_address_of_urn_6() { return &___urn_6; }
	inline void set_urn_6(String_t* value)
	{
		___urn_6 = value;
		Il2CppCodeGenWriteBarrier((&___urn_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T4207104559_H
#ifndef FUNCTION_T1283990952_H
#define FUNCTION_T1283990952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Function
struct  Function_t1283990952  : public AstNode_t2514041814
{
public:
	// MS.Internal.Xml.XPath.Function/FunctionType MS.Internal.Xml.XPath.Function::functionType
	int32_t ___functionType_0;
	// System.Collections.ArrayList MS.Internal.Xml.XPath.Function::argumentList
	ArrayList_t2718874744 * ___argumentList_1;
	// System.String MS.Internal.Xml.XPath.Function::name
	String_t* ___name_2;
	// System.String MS.Internal.Xml.XPath.Function::prefix
	String_t* ___prefix_3;

public:
	inline static int32_t get_offset_of_functionType_0() { return static_cast<int32_t>(offsetof(Function_t1283990952, ___functionType_0)); }
	inline int32_t get_functionType_0() const { return ___functionType_0; }
	inline int32_t* get_address_of_functionType_0() { return &___functionType_0; }
	inline void set_functionType_0(int32_t value)
	{
		___functionType_0 = value;
	}

	inline static int32_t get_offset_of_argumentList_1() { return static_cast<int32_t>(offsetof(Function_t1283990952, ___argumentList_1)); }
	inline ArrayList_t2718874744 * get_argumentList_1() const { return ___argumentList_1; }
	inline ArrayList_t2718874744 ** get_address_of_argumentList_1() { return &___argumentList_1; }
	inline void set_argumentList_1(ArrayList_t2718874744 * value)
	{
		___argumentList_1 = value;
		Il2CppCodeGenWriteBarrier((&___argumentList_1), value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Function_t1283990952, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_prefix_3() { return static_cast<int32_t>(offsetof(Function_t1283990952, ___prefix_3)); }
	inline String_t* get_prefix_3() const { return ___prefix_3; }
	inline String_t** get_address_of_prefix_3() { return &___prefix_3; }
	inline void set_prefix_3(String_t* value)
	{
		___prefix_3 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_3), value);
	}
};

struct Function_t1283990952_StaticFields
{
public:
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.Function::ReturnTypes
	XPathResultTypeU5BU5D_t1515527577* ___ReturnTypes_4;

public:
	inline static int32_t get_offset_of_ReturnTypes_4() { return static_cast<int32_t>(offsetof(Function_t1283990952_StaticFields, ___ReturnTypes_4)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_ReturnTypes_4() const { return ___ReturnTypes_4; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_ReturnTypes_4() { return &___ReturnTypes_4; }
	inline void set_ReturnTypes_4(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___ReturnTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___ReturnTypes_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTION_T1283990952_H
#ifndef OPERAND_T3355154092_H
#define OPERAND_T3355154092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Operand
struct  Operand_t3355154092  : public AstNode_t2514041814
{
public:
	// System.Xml.XPath.XPathResultType MS.Internal.Xml.XPath.Operand::type
	int32_t ___type_0;
	// System.Object MS.Internal.Xml.XPath.Operand::val
	RuntimeObject * ___val_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(Operand_t3355154092, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_val_1() { return static_cast<int32_t>(offsetof(Operand_t3355154092, ___val_1)); }
	inline RuntimeObject * get_val_1() const { return ___val_1; }
	inline RuntimeObject ** get_address_of_val_1() { return &___val_1; }
	inline void set_val_1(RuntimeObject * value)
	{
		___val_1 = value;
		Il2CppCodeGenWriteBarrier((&___val_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERAND_T3355154092_H
#ifndef MULTICASTDELEGATE_T157516450_H
#define MULTICASTDELEGATE_T157516450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t157516450  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t157516450, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T157516450_H
#ifndef SYMMETRICALGORITHM_T4254223087_H
#define SYMMETRICALGORITHM_T4254223087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SymmetricAlgorithm
struct  SymmetricAlgorithm_t4254223087  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::BlockSizeValue
	int32_t ___BlockSizeValue_0;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::FeedbackSizeValue
	int32_t ___FeedbackSizeValue_1;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::IVValue
	ByteU5BU5D_t4116647657* ___IVValue_2;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::KeyValue
	ByteU5BU5D_t4116647657* ___KeyValue_3;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalBlockSizesValue
	KeySizesU5BU5D_t722666473* ___LegalBlockSizesValue_4;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t722666473* ___LegalKeySizesValue_5;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_6;
	// System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::ModeValue
	int32_t ___ModeValue_7;
	// System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::PaddingValue
	int32_t ___PaddingValue_8;

public:
	inline static int32_t get_offset_of_BlockSizeValue_0() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___BlockSizeValue_0)); }
	inline int32_t get_BlockSizeValue_0() const { return ___BlockSizeValue_0; }
	inline int32_t* get_address_of_BlockSizeValue_0() { return &___BlockSizeValue_0; }
	inline void set_BlockSizeValue_0(int32_t value)
	{
		___BlockSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_FeedbackSizeValue_1() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___FeedbackSizeValue_1)); }
	inline int32_t get_FeedbackSizeValue_1() const { return ___FeedbackSizeValue_1; }
	inline int32_t* get_address_of_FeedbackSizeValue_1() { return &___FeedbackSizeValue_1; }
	inline void set_FeedbackSizeValue_1(int32_t value)
	{
		___FeedbackSizeValue_1 = value;
	}

	inline static int32_t get_offset_of_IVValue_2() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___IVValue_2)); }
	inline ByteU5BU5D_t4116647657* get_IVValue_2() const { return ___IVValue_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_IVValue_2() { return &___IVValue_2; }
	inline void set_IVValue_2(ByteU5BU5D_t4116647657* value)
	{
		___IVValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___IVValue_2), value);
	}

	inline static int32_t get_offset_of_KeyValue_3() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___KeyValue_3)); }
	inline ByteU5BU5D_t4116647657* get_KeyValue_3() const { return ___KeyValue_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_KeyValue_3() { return &___KeyValue_3; }
	inline void set_KeyValue_3(ByteU5BU5D_t4116647657* value)
	{
		___KeyValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_3), value);
	}

	inline static int32_t get_offset_of_LegalBlockSizesValue_4() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___LegalBlockSizesValue_4)); }
	inline KeySizesU5BU5D_t722666473* get_LegalBlockSizesValue_4() const { return ___LegalBlockSizesValue_4; }
	inline KeySizesU5BU5D_t722666473** get_address_of_LegalBlockSizesValue_4() { return &___LegalBlockSizesValue_4; }
	inline void set_LegalBlockSizesValue_4(KeySizesU5BU5D_t722666473* value)
	{
		___LegalBlockSizesValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___LegalBlockSizesValue_4), value);
	}

	inline static int32_t get_offset_of_LegalKeySizesValue_5() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___LegalKeySizesValue_5)); }
	inline KeySizesU5BU5D_t722666473* get_LegalKeySizesValue_5() const { return ___LegalKeySizesValue_5; }
	inline KeySizesU5BU5D_t722666473** get_address_of_LegalKeySizesValue_5() { return &___LegalKeySizesValue_5; }
	inline void set_LegalKeySizesValue_5(KeySizesU5BU5D_t722666473* value)
	{
		___LegalKeySizesValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___LegalKeySizesValue_5), value);
	}

	inline static int32_t get_offset_of_KeySizeValue_6() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___KeySizeValue_6)); }
	inline int32_t get_KeySizeValue_6() const { return ___KeySizeValue_6; }
	inline int32_t* get_address_of_KeySizeValue_6() { return &___KeySizeValue_6; }
	inline void set_KeySizeValue_6(int32_t value)
	{
		___KeySizeValue_6 = value;
	}

	inline static int32_t get_offset_of_ModeValue_7() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___ModeValue_7)); }
	inline int32_t get_ModeValue_7() const { return ___ModeValue_7; }
	inline int32_t* get_address_of_ModeValue_7() { return &___ModeValue_7; }
	inline void set_ModeValue_7(int32_t value)
	{
		___ModeValue_7 = value;
	}

	inline static int32_t get_offset_of_PaddingValue_8() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___PaddingValue_8)); }
	inline int32_t get_PaddingValue_8() const { return ___PaddingValue_8; }
	inline int32_t* get_address_of_PaddingValue_8() { return &___PaddingValue_8; }
	inline void set_PaddingValue_8(int32_t value)
	{
		___PaddingValue_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMMETRICALGORITHM_T4254223087_H
#ifndef HTMLUTF8RAWTEXTWRITER_T4061238569_H
#define HTMLUTF8RAWTEXTWRITER_T4061238569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HtmlUtf8RawTextWriter
struct  HtmlUtf8RawTextWriter_t4061238569  : public XmlUtf8RawTextWriter_t2752398654
{
public:
	// System.Xml.ByteStack System.Xml.HtmlUtf8RawTextWriter::elementScope
	ByteStack_t2013325889 * ___elementScope_26;
	// System.Xml.ElementProperties System.Xml.HtmlUtf8RawTextWriter::currentElementProperties
	uint32_t ___currentElementProperties_27;
	// System.Xml.AttributeProperties System.Xml.HtmlUtf8RawTextWriter::currentAttributeProperties
	uint32_t ___currentAttributeProperties_28;
	// System.Boolean System.Xml.HtmlUtf8RawTextWriter::endsWithAmpersand
	bool ___endsWithAmpersand_29;
	// System.Byte[] System.Xml.HtmlUtf8RawTextWriter::uriEscapingBuffer
	ByteU5BU5D_t4116647657* ___uriEscapingBuffer_30;
	// System.String System.Xml.HtmlUtf8RawTextWriter::mediaType
	String_t* ___mediaType_31;
	// System.Boolean System.Xml.HtmlUtf8RawTextWriter::doNotEscapeUriAttributes
	bool ___doNotEscapeUriAttributes_32;

public:
	inline static int32_t get_offset_of_elementScope_26() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t4061238569, ___elementScope_26)); }
	inline ByteStack_t2013325889 * get_elementScope_26() const { return ___elementScope_26; }
	inline ByteStack_t2013325889 ** get_address_of_elementScope_26() { return &___elementScope_26; }
	inline void set_elementScope_26(ByteStack_t2013325889 * value)
	{
		___elementScope_26 = value;
		Il2CppCodeGenWriteBarrier((&___elementScope_26), value);
	}

	inline static int32_t get_offset_of_currentElementProperties_27() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t4061238569, ___currentElementProperties_27)); }
	inline uint32_t get_currentElementProperties_27() const { return ___currentElementProperties_27; }
	inline uint32_t* get_address_of_currentElementProperties_27() { return &___currentElementProperties_27; }
	inline void set_currentElementProperties_27(uint32_t value)
	{
		___currentElementProperties_27 = value;
	}

	inline static int32_t get_offset_of_currentAttributeProperties_28() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t4061238569, ___currentAttributeProperties_28)); }
	inline uint32_t get_currentAttributeProperties_28() const { return ___currentAttributeProperties_28; }
	inline uint32_t* get_address_of_currentAttributeProperties_28() { return &___currentAttributeProperties_28; }
	inline void set_currentAttributeProperties_28(uint32_t value)
	{
		___currentAttributeProperties_28 = value;
	}

	inline static int32_t get_offset_of_endsWithAmpersand_29() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t4061238569, ___endsWithAmpersand_29)); }
	inline bool get_endsWithAmpersand_29() const { return ___endsWithAmpersand_29; }
	inline bool* get_address_of_endsWithAmpersand_29() { return &___endsWithAmpersand_29; }
	inline void set_endsWithAmpersand_29(bool value)
	{
		___endsWithAmpersand_29 = value;
	}

	inline static int32_t get_offset_of_uriEscapingBuffer_30() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t4061238569, ___uriEscapingBuffer_30)); }
	inline ByteU5BU5D_t4116647657* get_uriEscapingBuffer_30() const { return ___uriEscapingBuffer_30; }
	inline ByteU5BU5D_t4116647657** get_address_of_uriEscapingBuffer_30() { return &___uriEscapingBuffer_30; }
	inline void set_uriEscapingBuffer_30(ByteU5BU5D_t4116647657* value)
	{
		___uriEscapingBuffer_30 = value;
		Il2CppCodeGenWriteBarrier((&___uriEscapingBuffer_30), value);
	}

	inline static int32_t get_offset_of_mediaType_31() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t4061238569, ___mediaType_31)); }
	inline String_t* get_mediaType_31() const { return ___mediaType_31; }
	inline String_t** get_address_of_mediaType_31() { return &___mediaType_31; }
	inline void set_mediaType_31(String_t* value)
	{
		___mediaType_31 = value;
		Il2CppCodeGenWriteBarrier((&___mediaType_31), value);
	}

	inline static int32_t get_offset_of_doNotEscapeUriAttributes_32() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t4061238569, ___doNotEscapeUriAttributes_32)); }
	inline bool get_doNotEscapeUriAttributes_32() const { return ___doNotEscapeUriAttributes_32; }
	inline bool* get_address_of_doNotEscapeUriAttributes_32() { return &___doNotEscapeUriAttributes_32; }
	inline void set_doNotEscapeUriAttributes_32(bool value)
	{
		___doNotEscapeUriAttributes_32 = value;
	}
};

struct HtmlUtf8RawTextWriter_t4061238569_StaticFields
{
public:
	// System.Xml.TernaryTreeReadOnly System.Xml.HtmlUtf8RawTextWriter::elementPropertySearch
	TernaryTreeReadOnly_t172569514 * ___elementPropertySearch_33;
	// System.Xml.TernaryTreeReadOnly System.Xml.HtmlUtf8RawTextWriter::attributePropertySearch
	TernaryTreeReadOnly_t172569514 * ___attributePropertySearch_34;

public:
	inline static int32_t get_offset_of_elementPropertySearch_33() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t4061238569_StaticFields, ___elementPropertySearch_33)); }
	inline TernaryTreeReadOnly_t172569514 * get_elementPropertySearch_33() const { return ___elementPropertySearch_33; }
	inline TernaryTreeReadOnly_t172569514 ** get_address_of_elementPropertySearch_33() { return &___elementPropertySearch_33; }
	inline void set_elementPropertySearch_33(TernaryTreeReadOnly_t172569514 * value)
	{
		___elementPropertySearch_33 = value;
		Il2CppCodeGenWriteBarrier((&___elementPropertySearch_33), value);
	}

	inline static int32_t get_offset_of_attributePropertySearch_34() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t4061238569_StaticFields, ___attributePropertySearch_34)); }
	inline TernaryTreeReadOnly_t172569514 * get_attributePropertySearch_34() const { return ___attributePropertySearch_34; }
	inline TernaryTreeReadOnly_t172569514 ** get_address_of_attributePropertySearch_34() { return &___attributePropertySearch_34; }
	inline void set_attributePropertySearch_34(TernaryTreeReadOnly_t172569514 * value)
	{
		___attributePropertySearch_34 = value;
		Il2CppCodeGenWriteBarrier((&___attributePropertySearch_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLUTF8RAWTEXTWRITER_T4061238569_H
#ifndef PRIMALITYTEST_T1539325944_H
#define PRIMALITYTEST_T1539325944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.PrimalityTest
struct  PrimalityTest_t1539325944  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMALITYTEST_T1539325944_H
#ifndef RC4_T2752556436_H
#define RC4_T2752556436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.RC4
struct  RC4_t2752556436  : public SymmetricAlgorithm_t4254223087
{
public:

public:
};

struct RC4_t2752556436_StaticFields
{
public:
	// System.Security.Cryptography.KeySizes[] Mono.Security.Cryptography.RC4::s_legalBlockSizes
	KeySizesU5BU5D_t722666473* ___s_legalBlockSizes_9;
	// System.Security.Cryptography.KeySizes[] Mono.Security.Cryptography.RC4::s_legalKeySizes
	KeySizesU5BU5D_t722666473* ___s_legalKeySizes_10;

public:
	inline static int32_t get_offset_of_s_legalBlockSizes_9() { return static_cast<int32_t>(offsetof(RC4_t2752556436_StaticFields, ___s_legalBlockSizes_9)); }
	inline KeySizesU5BU5D_t722666473* get_s_legalBlockSizes_9() const { return ___s_legalBlockSizes_9; }
	inline KeySizesU5BU5D_t722666473** get_address_of_s_legalBlockSizes_9() { return &___s_legalBlockSizes_9; }
	inline void set_s_legalBlockSizes_9(KeySizesU5BU5D_t722666473* value)
	{
		___s_legalBlockSizes_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_legalBlockSizes_9), value);
	}

	inline static int32_t get_offset_of_s_legalKeySizes_10() { return static_cast<int32_t>(offsetof(RC4_t2752556436_StaticFields, ___s_legalKeySizes_10)); }
	inline KeySizesU5BU5D_t722666473* get_s_legalKeySizes_10() const { return ___s_legalKeySizes_10; }
	inline KeySizesU5BU5D_t722666473** get_address_of_s_legalKeySizes_10() { return &___s_legalKeySizes_10; }
	inline void set_s_legalKeySizes_10(KeySizesU5BU5D_t722666473* value)
	{
		___s_legalKeySizes_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_legalKeySizes_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RC4_T2752556436_H
#ifndef HTMLENCODEDRAWTEXTWRITER_T1444789934_H
#define HTMLENCODEDRAWTEXTWRITER_T1444789934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HtmlEncodedRawTextWriter
struct  HtmlEncodedRawTextWriter_t1444789934  : public XmlEncodedRawTextWriter_t977350554
{
public:
	// System.Xml.ByteStack System.Xml.HtmlEncodedRawTextWriter::elementScope
	ByteStack_t2013325889 * ___elementScope_35;
	// System.Xml.ElementProperties System.Xml.HtmlEncodedRawTextWriter::currentElementProperties
	uint32_t ___currentElementProperties_36;
	// System.Xml.AttributeProperties System.Xml.HtmlEncodedRawTextWriter::currentAttributeProperties
	uint32_t ___currentAttributeProperties_37;
	// System.Boolean System.Xml.HtmlEncodedRawTextWriter::endsWithAmpersand
	bool ___endsWithAmpersand_38;
	// System.Byte[] System.Xml.HtmlEncodedRawTextWriter::uriEscapingBuffer
	ByteU5BU5D_t4116647657* ___uriEscapingBuffer_39;
	// System.String System.Xml.HtmlEncodedRawTextWriter::mediaType
	String_t* ___mediaType_40;
	// System.Boolean System.Xml.HtmlEncodedRawTextWriter::doNotEscapeUriAttributes
	bool ___doNotEscapeUriAttributes_41;

public:
	inline static int32_t get_offset_of_elementScope_35() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_t1444789934, ___elementScope_35)); }
	inline ByteStack_t2013325889 * get_elementScope_35() const { return ___elementScope_35; }
	inline ByteStack_t2013325889 ** get_address_of_elementScope_35() { return &___elementScope_35; }
	inline void set_elementScope_35(ByteStack_t2013325889 * value)
	{
		___elementScope_35 = value;
		Il2CppCodeGenWriteBarrier((&___elementScope_35), value);
	}

	inline static int32_t get_offset_of_currentElementProperties_36() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_t1444789934, ___currentElementProperties_36)); }
	inline uint32_t get_currentElementProperties_36() const { return ___currentElementProperties_36; }
	inline uint32_t* get_address_of_currentElementProperties_36() { return &___currentElementProperties_36; }
	inline void set_currentElementProperties_36(uint32_t value)
	{
		___currentElementProperties_36 = value;
	}

	inline static int32_t get_offset_of_currentAttributeProperties_37() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_t1444789934, ___currentAttributeProperties_37)); }
	inline uint32_t get_currentAttributeProperties_37() const { return ___currentAttributeProperties_37; }
	inline uint32_t* get_address_of_currentAttributeProperties_37() { return &___currentAttributeProperties_37; }
	inline void set_currentAttributeProperties_37(uint32_t value)
	{
		___currentAttributeProperties_37 = value;
	}

	inline static int32_t get_offset_of_endsWithAmpersand_38() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_t1444789934, ___endsWithAmpersand_38)); }
	inline bool get_endsWithAmpersand_38() const { return ___endsWithAmpersand_38; }
	inline bool* get_address_of_endsWithAmpersand_38() { return &___endsWithAmpersand_38; }
	inline void set_endsWithAmpersand_38(bool value)
	{
		___endsWithAmpersand_38 = value;
	}

	inline static int32_t get_offset_of_uriEscapingBuffer_39() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_t1444789934, ___uriEscapingBuffer_39)); }
	inline ByteU5BU5D_t4116647657* get_uriEscapingBuffer_39() const { return ___uriEscapingBuffer_39; }
	inline ByteU5BU5D_t4116647657** get_address_of_uriEscapingBuffer_39() { return &___uriEscapingBuffer_39; }
	inline void set_uriEscapingBuffer_39(ByteU5BU5D_t4116647657* value)
	{
		___uriEscapingBuffer_39 = value;
		Il2CppCodeGenWriteBarrier((&___uriEscapingBuffer_39), value);
	}

	inline static int32_t get_offset_of_mediaType_40() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_t1444789934, ___mediaType_40)); }
	inline String_t* get_mediaType_40() const { return ___mediaType_40; }
	inline String_t** get_address_of_mediaType_40() { return &___mediaType_40; }
	inline void set_mediaType_40(String_t* value)
	{
		___mediaType_40 = value;
		Il2CppCodeGenWriteBarrier((&___mediaType_40), value);
	}

	inline static int32_t get_offset_of_doNotEscapeUriAttributes_41() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_t1444789934, ___doNotEscapeUriAttributes_41)); }
	inline bool get_doNotEscapeUriAttributes_41() const { return ___doNotEscapeUriAttributes_41; }
	inline bool* get_address_of_doNotEscapeUriAttributes_41() { return &___doNotEscapeUriAttributes_41; }
	inline void set_doNotEscapeUriAttributes_41(bool value)
	{
		___doNotEscapeUriAttributes_41 = value;
	}
};

struct HtmlEncodedRawTextWriter_t1444789934_StaticFields
{
public:
	// System.Xml.TernaryTreeReadOnly System.Xml.HtmlEncodedRawTextWriter::elementPropertySearch
	TernaryTreeReadOnly_t172569514 * ___elementPropertySearch_42;
	// System.Xml.TernaryTreeReadOnly System.Xml.HtmlEncodedRawTextWriter::attributePropertySearch
	TernaryTreeReadOnly_t172569514 * ___attributePropertySearch_43;

public:
	inline static int32_t get_offset_of_elementPropertySearch_42() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_t1444789934_StaticFields, ___elementPropertySearch_42)); }
	inline TernaryTreeReadOnly_t172569514 * get_elementPropertySearch_42() const { return ___elementPropertySearch_42; }
	inline TernaryTreeReadOnly_t172569514 ** get_address_of_elementPropertySearch_42() { return &___elementPropertySearch_42; }
	inline void set_elementPropertySearch_42(TernaryTreeReadOnly_t172569514 * value)
	{
		___elementPropertySearch_42 = value;
		Il2CppCodeGenWriteBarrier((&___elementPropertySearch_42), value);
	}

	inline static int32_t get_offset_of_attributePropertySearch_43() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_t1444789934_StaticFields, ___attributePropertySearch_43)); }
	inline TernaryTreeReadOnly_t172569514 * get_attributePropertySearch_43() const { return ___attributePropertySearch_43; }
	inline TernaryTreeReadOnly_t172569514 ** get_address_of_attributePropertySearch_43() { return &___attributePropertySearch_43; }
	inline void set_attributePropertySearch_43(TernaryTreeReadOnly_t172569514 * value)
	{
		___attributePropertySearch_43 = value;
		Il2CppCodeGenWriteBarrier((&___attributePropertySearch_43), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLENCODEDRAWTEXTWRITER_T1444789934_H
#ifndef KEYGENERATEDEVENTHANDLER_T3064139578_H
#define KEYGENERATEDEVENTHANDLER_T3064139578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
struct  KeyGeneratedEventHandler_t3064139578  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYGENERATEDEVENTHANDLER_T3064139578_H
#ifndef ARC4MANAGED_T2641858452_H
#define ARC4MANAGED_T2641858452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.ARC4Managed
struct  ARC4Managed_t2641858452  : public RC4_t2752556436
{
public:
	// System.Byte[] Mono.Security.Cryptography.ARC4Managed::key
	ByteU5BU5D_t4116647657* ___key_11;
	// System.Byte[] Mono.Security.Cryptography.ARC4Managed::state
	ByteU5BU5D_t4116647657* ___state_12;
	// System.Byte Mono.Security.Cryptography.ARC4Managed::x
	uint8_t ___x_13;
	// System.Byte Mono.Security.Cryptography.ARC4Managed::y
	uint8_t ___y_14;
	// System.Boolean Mono.Security.Cryptography.ARC4Managed::m_disposed
	bool ___m_disposed_15;

public:
	inline static int32_t get_offset_of_key_11() { return static_cast<int32_t>(offsetof(ARC4Managed_t2641858452, ___key_11)); }
	inline ByteU5BU5D_t4116647657* get_key_11() const { return ___key_11; }
	inline ByteU5BU5D_t4116647657** get_address_of_key_11() { return &___key_11; }
	inline void set_key_11(ByteU5BU5D_t4116647657* value)
	{
		___key_11 = value;
		Il2CppCodeGenWriteBarrier((&___key_11), value);
	}

	inline static int32_t get_offset_of_state_12() { return static_cast<int32_t>(offsetof(ARC4Managed_t2641858452, ___state_12)); }
	inline ByteU5BU5D_t4116647657* get_state_12() const { return ___state_12; }
	inline ByteU5BU5D_t4116647657** get_address_of_state_12() { return &___state_12; }
	inline void set_state_12(ByteU5BU5D_t4116647657* value)
	{
		___state_12 = value;
		Il2CppCodeGenWriteBarrier((&___state_12), value);
	}

	inline static int32_t get_offset_of_x_13() { return static_cast<int32_t>(offsetof(ARC4Managed_t2641858452, ___x_13)); }
	inline uint8_t get_x_13() const { return ___x_13; }
	inline uint8_t* get_address_of_x_13() { return &___x_13; }
	inline void set_x_13(uint8_t value)
	{
		___x_13 = value;
	}

	inline static int32_t get_offset_of_y_14() { return static_cast<int32_t>(offsetof(ARC4Managed_t2641858452, ___y_14)); }
	inline uint8_t get_y_14() const { return ___y_14; }
	inline uint8_t* get_address_of_y_14() { return &___y_14; }
	inline void set_y_14(uint8_t value)
	{
		___y_14 = value;
	}

	inline static int32_t get_offset_of_m_disposed_15() { return static_cast<int32_t>(offsetof(ARC4Managed_t2641858452, ___m_disposed_15)); }
	inline bool get_m_disposed_15() const { return ___m_disposed_15; }
	inline bool* get_address_of_m_disposed_15() { return &___m_disposed_15; }
	inline void set_m_disposed_15(bool value)
	{
		___m_disposed_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARC4MANAGED_T2641858452_H
#ifndef HTMLENCODEDRAWTEXTWRITERINDENT_T2550277658_H
#define HTMLENCODEDRAWTEXTWRITERINDENT_T2550277658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HtmlEncodedRawTextWriterIndent
struct  HtmlEncodedRawTextWriterIndent_t2550277658  : public HtmlEncodedRawTextWriter_t1444789934
{
public:
	// System.Int32 System.Xml.HtmlEncodedRawTextWriterIndent::indentLevel
	int32_t ___indentLevel_44;
	// System.Int32 System.Xml.HtmlEncodedRawTextWriterIndent::endBlockPos
	int32_t ___endBlockPos_45;
	// System.String System.Xml.HtmlEncodedRawTextWriterIndent::indentChars
	String_t* ___indentChars_46;
	// System.Boolean System.Xml.HtmlEncodedRawTextWriterIndent::newLineOnAttributes
	bool ___newLineOnAttributes_47;

public:
	inline static int32_t get_offset_of_indentLevel_44() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriterIndent_t2550277658, ___indentLevel_44)); }
	inline int32_t get_indentLevel_44() const { return ___indentLevel_44; }
	inline int32_t* get_address_of_indentLevel_44() { return &___indentLevel_44; }
	inline void set_indentLevel_44(int32_t value)
	{
		___indentLevel_44 = value;
	}

	inline static int32_t get_offset_of_endBlockPos_45() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriterIndent_t2550277658, ___endBlockPos_45)); }
	inline int32_t get_endBlockPos_45() const { return ___endBlockPos_45; }
	inline int32_t* get_address_of_endBlockPos_45() { return &___endBlockPos_45; }
	inline void set_endBlockPos_45(int32_t value)
	{
		___endBlockPos_45 = value;
	}

	inline static int32_t get_offset_of_indentChars_46() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriterIndent_t2550277658, ___indentChars_46)); }
	inline String_t* get_indentChars_46() const { return ___indentChars_46; }
	inline String_t** get_address_of_indentChars_46() { return &___indentChars_46; }
	inline void set_indentChars_46(String_t* value)
	{
		___indentChars_46 = value;
		Il2CppCodeGenWriteBarrier((&___indentChars_46), value);
	}

	inline static int32_t get_offset_of_newLineOnAttributes_47() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriterIndent_t2550277658, ___newLineOnAttributes_47)); }
	inline bool get_newLineOnAttributes_47() const { return ___newLineOnAttributes_47; }
	inline bool* get_address_of_newLineOnAttributes_47() { return &___newLineOnAttributes_47; }
	inline void set_newLineOnAttributes_47(bool value)
	{
		___newLineOnAttributes_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLENCODEDRAWTEXTWRITERINDENT_T2550277658_H
#ifndef HTMLUTF8RAWTEXTWRITERINDENT_T1826254648_H
#define HTMLUTF8RAWTEXTWRITERINDENT_T1826254648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HtmlUtf8RawTextWriterIndent
struct  HtmlUtf8RawTextWriterIndent_t1826254648  : public HtmlUtf8RawTextWriter_t4061238569
{
public:
	// System.Int32 System.Xml.HtmlUtf8RawTextWriterIndent::indentLevel
	int32_t ___indentLevel_35;
	// System.Int32 System.Xml.HtmlUtf8RawTextWriterIndent::endBlockPos
	int32_t ___endBlockPos_36;
	// System.String System.Xml.HtmlUtf8RawTextWriterIndent::indentChars
	String_t* ___indentChars_37;
	// System.Boolean System.Xml.HtmlUtf8RawTextWriterIndent::newLineOnAttributes
	bool ___newLineOnAttributes_38;

public:
	inline static int32_t get_offset_of_indentLevel_35() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriterIndent_t1826254648, ___indentLevel_35)); }
	inline int32_t get_indentLevel_35() const { return ___indentLevel_35; }
	inline int32_t* get_address_of_indentLevel_35() { return &___indentLevel_35; }
	inline void set_indentLevel_35(int32_t value)
	{
		___indentLevel_35 = value;
	}

	inline static int32_t get_offset_of_endBlockPos_36() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriterIndent_t1826254648, ___endBlockPos_36)); }
	inline int32_t get_endBlockPos_36() const { return ___endBlockPos_36; }
	inline int32_t* get_address_of_endBlockPos_36() { return &___endBlockPos_36; }
	inline void set_endBlockPos_36(int32_t value)
	{
		___endBlockPos_36 = value;
	}

	inline static int32_t get_offset_of_indentChars_37() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriterIndent_t1826254648, ___indentChars_37)); }
	inline String_t* get_indentChars_37() const { return ___indentChars_37; }
	inline String_t** get_address_of_indentChars_37() { return &___indentChars_37; }
	inline void set_indentChars_37(String_t* value)
	{
		___indentChars_37 = value;
		Il2CppCodeGenWriteBarrier((&___indentChars_37), value);
	}

	inline static int32_t get_offset_of_newLineOnAttributes_38() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriterIndent_t1826254648, ___newLineOnAttributes_38)); }
	inline bool get_newLineOnAttributes_38() const { return ___newLineOnAttributes_38; }
	inline bool* get_address_of_newLineOnAttributes_38() { return &___newLineOnAttributes_38; }
	inline void set_newLineOnAttributes_38(bool value)
	{
		___newLineOnAttributes_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLUTF8RAWTEXTWRITERINDENT_T1826254648_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (X509StoreManager_t1046782376), -1, sizeof(X509StoreManager_t1046782376_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1800[5] = 
{
	X509StoreManager_t1046782376_StaticFields::get_offset_of__userPath_0(),
	X509StoreManager_t1046782376_StaticFields::get_offset_of__localMachinePath_1(),
	X509StoreManager_t1046782376_StaticFields::get_offset_of__newLocalMachinePath_2(),
	X509StoreManager_t1046782376_StaticFields::get_offset_of__userStore_3(),
	X509StoreManager_t1046782376_StaticFields::get_offset_of__machineStore_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (X509Stores_t1373936238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[3] = 
{
	X509Stores_t1373936238::get_offset_of__storePath_0(),
	X509Stores_t1373936238::get_offset_of__newFormat_1(),
	X509Stores_t1373936238::get_offset_of__trusted_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (AuthorityKeyIdentifierExtension_t1122691429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[1] = 
{
	AuthorityKeyIdentifierExtension_t1122691429::get_offset_of_aki_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (BasicConstraintsExtension_t2462195279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[2] = 
{
	BasicConstraintsExtension_t2462195279::get_offset_of_cA_3(),
	BasicConstraintsExtension_t2462195279::get_offset_of_pathLenConstraint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (ExtendedKeyUsageExtension_t3929363080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[1] = 
{
	ExtendedKeyUsageExtension_t3929363080::get_offset_of_keyPurpose_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (GeneralNames_t2702294159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[5] = 
{
	GeneralNames_t2702294159::get_offset_of_rfc822Name_0(),
	GeneralNames_t2702294159::get_offset_of_dnsName_1(),
	GeneralNames_t2702294159::get_offset_of_directoryNames_2(),
	GeneralNames_t2702294159::get_offset_of_uris_3(),
	GeneralNames_t2702294159::get_offset_of_ipAddr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (KeyUsages_t820456313)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1806[11] = 
{
	KeyUsages_t820456313::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (KeyUsageExtension_t1795615912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[1] = 
{
	KeyUsageExtension_t1795615912::get_offset_of_kubits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (NetscapeCertTypeExtension_t1524296876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[1] = 
{
	NetscapeCertTypeExtension_t1524296876::get_offset_of_ctbits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (CertTypes_t3317701015)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1809[8] = 
{
	CertTypes_t3317701015::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (SubjectAltNameExtension_t1536937677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[1] = 
{
	SubjectAltNameExtension_t1536937677::get_offset_of__names_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (ARC4Managed_t2641858452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1811[5] = 
{
	ARC4Managed_t2641858452::get_offset_of_key_11(),
	ARC4Managed_t2641858452::get_offset_of_state_12(),
	ARC4Managed_t2641858452::get_offset_of_x_13(),
	ARC4Managed_t2641858452::get_offset_of_y_14(),
	ARC4Managed_t2641858452::get_offset_of_m_disposed_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (CryptoConvert_t610933157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (KeyBuilder_t2049230355), -1, sizeof(KeyBuilder_t2049230355_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1813[1] = 
{
	KeyBuilder_t2049230355_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (KeyPairPersistence_t2094547462), -1, sizeof(KeyPairPersistence_t2094547462_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1814[9] = 
{
	KeyPairPersistence_t2094547462_StaticFields::get_offset_of__userPathExists_0(),
	KeyPairPersistence_t2094547462_StaticFields::get_offset_of__userPath_1(),
	KeyPairPersistence_t2094547462_StaticFields::get_offset_of__machinePathExists_2(),
	KeyPairPersistence_t2094547462_StaticFields::get_offset_of__machinePath_3(),
	KeyPairPersistence_t2094547462::get_offset_of__params_4(),
	KeyPairPersistence_t2094547462::get_offset_of__keyvalue_5(),
	KeyPairPersistence_t2094547462::get_offset_of__filename_6(),
	KeyPairPersistence_t2094547462::get_offset_of__container_7(),
	KeyPairPersistence_t2094547462_StaticFields::get_offset_of_lockobj_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (PKCS1_t1505584677), -1, sizeof(PKCS1_t1505584677_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1815[4] = 
{
	PKCS1_t1505584677_StaticFields::get_offset_of_emptySHA1_0(),
	PKCS1_t1505584677_StaticFields::get_offset_of_emptySHA256_1(),
	PKCS1_t1505584677_StaticFields::get_offset_of_emptySHA384_2(),
	PKCS1_t1505584677_StaticFields::get_offset_of_emptySHA512_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (PKCS8_t696280613), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (PrivateKeyInfo_t668027993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[4] = 
{
	PrivateKeyInfo_t668027993::get_offset_of__version_0(),
	PrivateKeyInfo_t668027993::get_offset_of__algorithm_1(),
	PrivateKeyInfo_t668027993::get_offset_of__key_2(),
	PrivateKeyInfo_t668027993::get_offset_of__list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (EncryptedPrivateKeyInfo_t862116836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[4] = 
{
	EncryptedPrivateKeyInfo_t862116836::get_offset_of__algorithm_0(),
	EncryptedPrivateKeyInfo_t862116836::get_offset_of__salt_1(),
	EncryptedPrivateKeyInfo_t862116836::get_offset_of__iterations_2(),
	EncryptedPrivateKeyInfo_t862116836::get_offset_of__data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (RC4_t2752556436), -1, sizeof(RC4_t2752556436_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1819[2] = 
{
	RC4_t2752556436_StaticFields::get_offset_of_s_legalBlockSizes_9(),
	RC4_t2752556436_StaticFields::get_offset_of_s_legalKeySizes_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (RSAManaged_t1757093820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[13] = 
{
	RSAManaged_t1757093820::get_offset_of_isCRTpossible_2(),
	RSAManaged_t1757093820::get_offset_of_keyBlinding_3(),
	RSAManaged_t1757093820::get_offset_of_keypairGenerated_4(),
	RSAManaged_t1757093820::get_offset_of_m_disposed_5(),
	RSAManaged_t1757093820::get_offset_of_d_6(),
	RSAManaged_t1757093820::get_offset_of_p_7(),
	RSAManaged_t1757093820::get_offset_of_q_8(),
	RSAManaged_t1757093820::get_offset_of_dp_9(),
	RSAManaged_t1757093820::get_offset_of_dq_10(),
	RSAManaged_t1757093820::get_offset_of_qInv_11(),
	RSAManaged_t1757093820::get_offset_of_n_12(),
	RSAManaged_t1757093820::get_offset_of_e_13(),
	RSAManaged_t1757093820::get_offset_of_KeyGenerated_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (KeyGeneratedEventHandler_t3064139578), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (HMAC_t3689525210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[4] = 
{
	HMAC_t3689525210::get_offset_of_hash_5(),
	HMAC_t3689525210::get_offset_of_hashing_6(),
	HMAC_t3689525210::get_offset_of_innerPad_7(),
	HMAC_t3689525210::get_offset_of_outerPad_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (MD5SHA1_t723838944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[3] = 
{
	MD5SHA1_t723838944::get_offset_of_md5_4(),
	MD5SHA1_t723838944::get_offset_of_sha_5(),
	MD5SHA1_t723838944::get_offset_of_hashing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (BigInteger_t2902905090), -1, sizeof(BigInteger_t2902905090_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1824[4] = 
{
	BigInteger_t2902905090::get_offset_of_length_0(),
	BigInteger_t2902905090::get_offset_of_data_1(),
	BigInteger_t2902905090_StaticFields::get_offset_of_smallPrimes_2(),
	BigInteger_t2902905090_StaticFields::get_offset_of_rng_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (Sign_t3338384039)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1825[4] = 
{
	Sign_t3338384039::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (ModulusRing_t596511505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[2] = 
{
	ModulusRing_t596511505::get_offset_of_mod_0(),
	ModulusRing_t596511505::get_offset_of_constant_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (Kernel_t1402667220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (ConfidenceFactor_t2516000286)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1828[7] = 
{
	ConfidenceFactor_t2516000286::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (PrimalityTest_t1539325944), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (PrimalityTests_t1538473976), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (PrimeGeneratorBase_t446028867), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (SequentialSearchPrimeGeneratorBase_t2996090509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255362), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1833[27] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U312D04472A8285260EA12FD3813CDFA9F2D2B548C_0(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U31397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_1(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U313A35EF1A549297C70E2AD46045BBD2ECA17852D_2(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U3235D99572263B22ADFEE10FDA0C25E12F4D94FFC_4(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U32D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130_5(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U331D8729F7377B44017C0A2395A582C9CA4163277_6(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U3320B018758ECE3752FFEDBAEB1A6DB67C80B9359_7(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U33E3442C7396F3F2BB4C7348F4A2074C7DC677D68_8(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U34E3B533C39447AAEB59A8E48FABD7E15B5B5D195_9(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U356DFA5053B3131883637F53219E7D88CCEF35949_10(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_11(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U36E5DC824F803F8565AF31B42199DAE39FE7F4EA9_12(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U3736D39815215889F11249D9958F6ED12D37B9F57_13(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U386F4F563FA2C61798AE6238D789139739428463A_14(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U397FB30C84FF4A41CD4625B44B2940BFC8DB43003_15(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U39A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_16(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U39BB00D1FCCBAF03165447FC8028E7CA07CA9FE88_17(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_A323DB0813C4D072957BA6FDA79D9776674CD06B_18(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_BE1BDEC0AA74B4DCB079943E70528096CCA985F8_19(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_BF477463CE2F5EF38FC4C644BBBF4DF109E7670A_20(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_CF0B42666EF5E37EDEA0AB8E173E42C196D03814_21(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_22(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_D693078666967609CB4A2B840921F9E052031847_23(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_E75835D001C843F156FBA01B001DFE1B8029AC17_24(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11_25(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_EC83FB16C20052BEE2B4025159BC2ED45C9C70C3_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (__StaticArrayInitTypeSizeU3D3_t3217885683)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D3_t3217885683 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (__StaticArrayInitTypeSizeU3D9_t3218278899)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D9_t3218278899 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (__StaticArrayInitTypeSizeU3D10_t1548194904)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D10_t1548194904 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (__StaticArrayInitTypeSizeU3D14_t3517563372)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D14_t3517563372 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (__StaticArrayInitTypeSizeU3D20_t1548391512)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D20_t1548391512 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (__StaticArrayInitTypeSizeU3D32_t2711125390)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_t2711125390 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (__StaticArrayInitTypeSizeU3D48_t1904228656)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D48_t1904228656 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (__StaticArrayInitTypeSizeU3D64_t3517497836)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D64_t3517497836 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (__StaticArrayInitTypeSizeU3D524_t4061560205)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D524_t4061560205 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (__StaticArrayInitTypeSizeU3D3132_t3825993976)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D3132_t3825993976 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (U3CModuleU3E_t692745527), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (SR_t167583544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (AstNode_t2514041814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (AstType_t3854428833)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1847[10] = 
{
	AstType_t3854428833::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (Axis_t4207104559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[7] = 
{
	Axis_t4207104559::get_offset_of_axisType_0(),
	Axis_t4207104559::get_offset_of_input_1(),
	Axis_t4207104559::get_offset_of_prefix_2(),
	Axis_t4207104559::get_offset_of_name_3(),
	Axis_t4207104559::get_offset_of_nodeType_4(),
	Axis_t4207104559::get_offset_of_abbrAxis_5(),
	Axis_t4207104559::get_offset_of_urn_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (AxisType_t3322599580)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1849[15] = 
{
	AxisType_t3322599580::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (Filter_t1571657935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[2] = 
{
	Filter_t1571657935::get_offset_of_input_0(),
	Filter_t1571657935::get_offset_of_condition_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (Function_t1283990952), -1, sizeof(Function_t1283990952_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1851[5] = 
{
	Function_t1283990952::get_offset_of_functionType_0(),
	Function_t1283990952::get_offset_of_argumentList_1(),
	Function_t1283990952::get_offset_of_name_2(),
	Function_t1283990952::get_offset_of_prefix_3(),
	Function_t1283990952_StaticFields::get_offset_of_ReturnTypes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (FunctionType_t3319434782)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1852[29] = 
{
	FunctionType_t3319434782::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (Group_t100818710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[1] = 
{
	Group_t100818710::get_offset_of_groupNode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (Operand_t3355154092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[2] = 
{
	Operand_t3355154092::get_offset_of_type_0(),
	Operand_t3355154092::get_offset_of_val_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (Operator_t966760113), -1, sizeof(Operator_t966760113_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1855[4] = 
{
	Operator_t966760113_StaticFields::get_offset_of_invertOp_0(),
	Operator_t966760113::get_offset_of_opType_1(),
	Operator_t966760113::get_offset_of_opnd1_2(),
	Operator_t966760113::get_offset_of_opnd2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (Op_t2046805169)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1856[16] = 
{
	Op_t2046805169::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (Root_t720671714), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (Variable_t262588068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[2] = 
{
	Variable_t262588068::get_offset_of_localname_0(),
	Variable_t262588068::get_offset_of_prefix_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (XPathParser_t618394529), -1, sizeof(XPathParser_t618394529_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1859[13] = 
{
	XPathParser_t618394529::get_offset_of_scanner_0(),
	XPathParser_t618394529::get_offset_of_parseDepth_1(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray1_2(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray2_3(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray3_4(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray4_5(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray5_6(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray6_7(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray7_8(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray8_9(),
	XPathParser_t618394529_StaticFields::get_offset_of_temparray9_10(),
	XPathParser_t618394529_StaticFields::get_offset_of_functionTable_11(),
	XPathParser_t618394529_StaticFields::get_offset_of_AxesTable_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (ParamInfo_t1233379796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1860[4] = 
{
	ParamInfo_t1233379796::get_offset_of_ftype_0(),
	ParamInfo_t1233379796::get_offset_of_minargs_1(),
	ParamInfo_t1233379796::get_offset_of_maxargs_2(),
	ParamInfo_t1233379796::get_offset_of_argTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (XPathScanner_t3283201025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1861[10] = 
{
	XPathScanner_t3283201025::get_offset_of_xpathExpr_0(),
	XPathScanner_t3283201025::get_offset_of_xpathExprIndex_1(),
	XPathScanner_t3283201025::get_offset_of_kind_2(),
	XPathScanner_t3283201025::get_offset_of_currentChar_3(),
	XPathScanner_t3283201025::get_offset_of_name_4(),
	XPathScanner_t3283201025::get_offset_of_prefix_5(),
	XPathScanner_t3283201025::get_offset_of_stringValue_6(),
	XPathScanner_t3283201025::get_offset_of_numberValue_7(),
	XPathScanner_t3283201025::get_offset_of_canBeFunction_8(),
	XPathScanner_t3283201025::get_offset_of_xmlCharType_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (LexKind_t864578899)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1862[32] = 
{
	LexKind_t864578899::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (XPathDocumentNavigator_t2457178823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1863[4] = 
{
	XPathDocumentNavigator_t2457178823::get_offset_of_pageCurrent_4(),
	XPathDocumentNavigator_t2457178823::get_offset_of_pageParent_5(),
	XPathDocumentNavigator_t2457178823::get_offset_of_idxCurrent_6(),
	XPathDocumentNavigator_t2457178823::get_offset_of_idxParent_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (XPathNode_t2208072876)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1864[7] = 
{
	XPathNode_t2208072876::get_offset_of_info_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_t2208072876::get_offset_of_idxSibling_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_t2208072876::get_offset_of_idxParent_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_t2208072876::get_offset_of_idxSimilar_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_t2208072876::get_offset_of_posOffset_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_t2208072876::get_offset_of_props_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNode_t2208072876::get_offset_of_value_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (XPathNodeRef_t3498189018)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1865[2] = 
{
	XPathNodeRef_t3498189018::get_offset_of_page_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XPathNodeRef_t3498189018::get_offset_of_idx_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (XPathNodeHelper_t2230825274), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (XPathNodePageInfo_t2343388010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1867[3] = 
{
	XPathNodePageInfo_t2343388010::get_offset_of_pageNum_0(),
	XPathNodePageInfo_t2343388010::get_offset_of_nodeCount_1(),
	XPathNodePageInfo_t2343388010::get_offset_of_pageNext_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (XPathNodeInfoAtom_t1760358141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1868[13] = 
{
	XPathNodeInfoAtom_t1760358141::get_offset_of_localName_0(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_namespaceUri_1(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_prefix_2(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_baseUri_3(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_pageParent_4(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_pageSibling_5(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_pageSimilar_6(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_doc_7(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_lineNumBase_8(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_linePosBase_9(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_hashCode_10(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_localNameHash_11(),
	XPathNodeInfoAtom_t1760358141::get_offset_of_pageInfo_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (LocalAppContextSwitches_t2827819611), -1, sizeof(LocalAppContextSwitches_t2827819611_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1869[1] = 
{
	LocalAppContextSwitches_t2827819611_StaticFields::get_offset_of_DontThrowOnInvalidSurrogatePairs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (Res_t3627928856), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (AsyncHelper_t714762001), -1, sizeof(AsyncHelper_t714762001_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1871[4] = 
{
	AsyncHelper_t714762001_StaticFields::get_offset_of_DoneTask_0(),
	AsyncHelper_t714762001_StaticFields::get_offset_of_DoneTaskTrue_1(),
	AsyncHelper_t714762001_StaticFields::get_offset_of_DoneTaskFalse_2(),
	AsyncHelper_t714762001_StaticFields::get_offset_of_DoneTaskZero_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (Base64Encoder_t3938083961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1872[3] = 
{
	Base64Encoder_t3938083961::get_offset_of_leftOverBytes_0(),
	Base64Encoder_t3938083961::get_offset_of_leftOverBytesCount_1(),
	Base64Encoder_t3938083961::get_offset_of_charsLine_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (XmlTextWriterBase64Encoder_t4259465041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[1] = 
{
	XmlTextWriterBase64Encoder_t4259465041::get_offset_of_xmlTextEncoder_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (XmlRawWriterBase64Encoder_t1905751661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1874[1] = 
{
	XmlRawWriterBase64Encoder_t1905751661::get_offset_of_rawWriter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (BinHexDecoder_t1474272384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1875[5] = 
{
	BinHexDecoder_t1474272384::get_offset_of_buffer_0(),
	BinHexDecoder_t1474272384::get_offset_of_curIndex_1(),
	BinHexDecoder_t1474272384::get_offset_of_endIndex_2(),
	BinHexDecoder_t1474272384::get_offset_of_hasHalfByteCached_3(),
	BinHexDecoder_t1474272384::get_offset_of_cachedHalfByte_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (BinHexEncoder_t1687308627), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (Bits_t3566938933), -1, sizeof(Bits_t3566938933_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1877[5] = 
{
	Bits_t3566938933_StaticFields::get_offset_of_MASK_0101010101010101_0(),
	Bits_t3566938933_StaticFields::get_offset_of_MASK_0011001100110011_1(),
	Bits_t3566938933_StaticFields::get_offset_of_MASK_0000111100001111_2(),
	Bits_t3566938933_StaticFields::get_offset_of_MASK_0000000011111111_3(),
	Bits_t3566938933_StaticFields::get_offset_of_MASK_1111111111111111_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (BitStack_t371189938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1878[3] = 
{
	BitStack_t371189938::get_offset_of_bitStack_0(),
	BitStack_t371189938::get_offset_of_stackPos_1(),
	BitStack_t371189938::get_offset_of_curr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (ByteStack_t2013325889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[4] = 
{
	ByteStack_t2013325889::get_offset_of_stack_0(),
	ByteStack_t2013325889::get_offset_of_growthRate_1(),
	ByteStack_t2013325889::get_offset_of_top_2(),
	ByteStack_t2013325889::get_offset_of_size_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (BinaryCompatibility_t2660327299), -1, sizeof(BinaryCompatibility_t2660327299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1880[1] = 
{
	BinaryCompatibility_t2660327299_StaticFields::get_offset_of__targetsAtLeast_Desktop_V4_5_2_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (CharEntityEncoderFallback_t110445598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1881[5] = 
{
	CharEntityEncoderFallback_t110445598::get_offset_of_fallbackBuffer_4(),
	CharEntityEncoderFallback_t110445598::get_offset_of_textContentMarks_5(),
	CharEntityEncoderFallback_t110445598::get_offset_of_endMarkPos_6(),
	CharEntityEncoderFallback_t110445598::get_offset_of_curMarkPos_7(),
	CharEntityEncoderFallback_t110445598::get_offset_of_startOffset_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (CharEntityEncoderFallbackBuffer_t4206548234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1882[3] = 
{
	CharEntityEncoderFallbackBuffer_t4206548234::get_offset_of_parent_7(),
	CharEntityEncoderFallbackBuffer_t4206548234::get_offset_of_charEntity_8(),
	CharEntityEncoderFallbackBuffer_t4206548234::get_offset_of_charEntityIndex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (ConformanceLevel_t3899847875)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1883[4] = 
{
	ConformanceLevel_t3899847875::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (DtdProcessing_t1163997051)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1884[4] = 
{
	DtdProcessing_t1163997051::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (EntityHandling_t1047276436)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1885[3] = 
{
	EntityHandling_t1047276436::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (HtmlEncodedRawTextWriter_t1444789934), -1, sizeof(HtmlEncodedRawTextWriter_t1444789934_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1886[9] = 
{
	HtmlEncodedRawTextWriter_t1444789934::get_offset_of_elementScope_35(),
	HtmlEncodedRawTextWriter_t1444789934::get_offset_of_currentElementProperties_36(),
	HtmlEncodedRawTextWriter_t1444789934::get_offset_of_currentAttributeProperties_37(),
	HtmlEncodedRawTextWriter_t1444789934::get_offset_of_endsWithAmpersand_38(),
	HtmlEncodedRawTextWriter_t1444789934::get_offset_of_uriEscapingBuffer_39(),
	HtmlEncodedRawTextWriter_t1444789934::get_offset_of_mediaType_40(),
	HtmlEncodedRawTextWriter_t1444789934::get_offset_of_doNotEscapeUriAttributes_41(),
	HtmlEncodedRawTextWriter_t1444789934_StaticFields::get_offset_of_elementPropertySearch_42(),
	HtmlEncodedRawTextWriter_t1444789934_StaticFields::get_offset_of_attributePropertySearch_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (HtmlEncodedRawTextWriterIndent_t2550277658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[4] = 
{
	HtmlEncodedRawTextWriterIndent_t2550277658::get_offset_of_indentLevel_44(),
	HtmlEncodedRawTextWriterIndent_t2550277658::get_offset_of_endBlockPos_45(),
	HtmlEncodedRawTextWriterIndent_t2550277658::get_offset_of_indentChars_46(),
	HtmlEncodedRawTextWriterIndent_t2550277658::get_offset_of_newLineOnAttributes_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (HtmlTernaryTree_t1625978624), -1, sizeof(HtmlTernaryTree_t1625978624_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1888[2] = 
{
	HtmlTernaryTree_t1625978624_StaticFields::get_offset_of_htmlElements_0(),
	HtmlTernaryTree_t1625978624_StaticFields::get_offset_of_htmlAttributes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (HtmlUtf8RawTextWriter_t4061238569), -1, sizeof(HtmlUtf8RawTextWriter_t4061238569_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1889[9] = 
{
	HtmlUtf8RawTextWriter_t4061238569::get_offset_of_elementScope_26(),
	HtmlUtf8RawTextWriter_t4061238569::get_offset_of_currentElementProperties_27(),
	HtmlUtf8RawTextWriter_t4061238569::get_offset_of_currentAttributeProperties_28(),
	HtmlUtf8RawTextWriter_t4061238569::get_offset_of_endsWithAmpersand_29(),
	HtmlUtf8RawTextWriter_t4061238569::get_offset_of_uriEscapingBuffer_30(),
	HtmlUtf8RawTextWriter_t4061238569::get_offset_of_mediaType_31(),
	HtmlUtf8RawTextWriter_t4061238569::get_offset_of_doNotEscapeUriAttributes_32(),
	HtmlUtf8RawTextWriter_t4061238569_StaticFields::get_offset_of_elementPropertySearch_33(),
	HtmlUtf8RawTextWriter_t4061238569_StaticFields::get_offset_of_attributePropertySearch_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (HtmlUtf8RawTextWriterIndent_t1826254648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[4] = 
{
	HtmlUtf8RawTextWriterIndent_t1826254648::get_offset_of_indentLevel_35(),
	HtmlUtf8RawTextWriterIndent_t1826254648::get_offset_of_endBlockPos_36(),
	HtmlUtf8RawTextWriterIndent_t1826254648::get_offset_of_indentChars_37(),
	HtmlUtf8RawTextWriterIndent_t1826254648::get_offset_of_newLineOnAttributes_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
