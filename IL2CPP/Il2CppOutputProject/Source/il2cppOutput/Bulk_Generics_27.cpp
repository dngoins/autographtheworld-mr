﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventRegistrationTokenListWithCount>
struct ConditionalWeakTable_2_t3044373657;
// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/TokenListCount
struct TokenListCount_t1606756367;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// MS.Internal.Xml.Cache.XPathNode[]
struct XPathNodeU5BU5D_t47339301;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// HoloToolkit.Unity.TimerScheduler/Callback
struct Callback_t2663646540;
// System.Collections.Generic.List`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct List_1_t1790965530;

extern Il2CppCodeGenString* _stringLiteral3450517380;
extern const uint32_t KeyValuePair_2_ToString_m875328790_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m3784847782_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m3475667597_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m4258372981_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1728257272_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1167308912_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m2584609649_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m65692449_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1238786018_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m3013572783_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m2983173998_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1045680960_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m3201086771_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m490808492_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m675279023_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1774591758_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m2480962023_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m4231614106_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m3021632350_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m510648957_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m225390547_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m80164506_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m3941865661_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m601171314_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m816416158_MetadataUsageId;
struct XPathNode_t2208072876_marshaled_pinvoke;
struct XPathNode_t2208072876_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef STRINGBUILDER_T1712802186_H
#define STRINGBUILDER_T1712802186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t1712802186  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t3528271667* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t1712802186 * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t1712802186, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t3528271667* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t3528271667** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t3528271667* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChunkChars_0), value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t1712802186, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t1712802186 * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t1712802186 ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t1712802186 * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChunkPrevious_1), value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t1712802186, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t1712802186, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t1712802186, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T1712802186_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef KEYVALUEPAIR_2_T3842366416_H
#define KEYVALUEPAIR_2_T3842366416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
struct  KeyValuePair_2_t3842366416 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	bool ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3842366416, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3842366416, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3842366416_H
#ifndef TYPEDCONSTANT_T714020897_H
#define TYPEDCONSTANT_T714020897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Compiler.BoundConstants/TypedConstant
struct  TypedConstant_t714020897 
{
public:
	// System.Object System.Linq.Expressions.Compiler.BoundConstants/TypedConstant::Value
	RuntimeObject * ___Value_0;
	// System.Type System.Linq.Expressions.Compiler.BoundConstants/TypedConstant::Type
	Type_t * ___Type_1;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(TypedConstant_t714020897, ___Value_0)); }
	inline RuntimeObject * get_Value_0() const { return ___Value_0; }
	inline RuntimeObject ** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(RuntimeObject * value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((&___Value_0), value);
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(TypedConstant_t714020897, ___Type_1)); }
	inline Type_t * get_Type_1() const { return ___Type_1; }
	inline Type_t ** get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(Type_t * value)
	{
		___Type_1 = value;
		Il2CppCodeGenWriteBarrier((&___Type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Linq.Expressions.Compiler.BoundConstants/TypedConstant
struct TypedConstant_t714020897_marshaled_pinvoke
{
	Il2CppIUnknown* ___Value_0;
	Type_t * ___Type_1;
};
// Native definition for COM marshalling of System.Linq.Expressions.Compiler.BoundConstants/TypedConstant
struct TypedConstant_t714020897_marshaled_com
{
	Il2CppIUnknown* ___Value_0;
	Type_t * ___Type_1;
};
#endif // TYPEDCONSTANT_T714020897_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef KEYVALUEPAIR_2_T2401056908_H
#define KEYVALUEPAIR_2_T2401056908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
struct  KeyValuePair_2_t2401056908 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2401056908, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2401056908, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2401056908_H
#ifndef KEYVALUEPAIR_2_T71524366_H
#define KEYVALUEPAIR_2_T71524366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
struct  KeyValuePair_2_t71524366 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t71524366, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t71524366, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T71524366_H
#ifndef KEYVALUEPAIR_2_T2245450819_H
#define KEYVALUEPAIR_2_T2245450819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>
struct  KeyValuePair_2_t2245450819 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int64_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2245450819, ___key_0)); }
	inline int64_t get_key_0() const { return ___key_0; }
	inline int64_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int64_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2245450819, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2245450819_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef KEYVALUEPAIR_2_T2530217319_H
#define KEYVALUEPAIR_2_T2530217319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t2530217319 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2530217319_H
#ifndef KEYVALUEPAIR_2_T2457078697_H
#define KEYVALUEPAIR_2_T2457078697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>
struct  KeyValuePair_2_t2457078697 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	uint32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2457078697, ___key_0)); }
	inline uint32_t get_key_0() const { return ___key_0; }
	inline uint32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(uint32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2457078697, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2457078697_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef EVENTREGISTRATIONTOKEN_T318890788_H
#define EVENTREGISTRATIONTOKEN_T318890788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken
struct  EventRegistrationToken_t318890788 
{
public:
	// System.UInt64 System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(EventRegistrationToken_t318890788, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTREGISTRATIONTOKEN_T318890788_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef EVENTCACHEKEY_T3133620722_H
#define EVENTCACHEKEY_T3133620722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey
struct  EventCacheKey_t3133620722 
{
public:
	// System.Object System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey::target
	RuntimeObject * ___target_0;
	// System.Reflection.MethodInfo System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey::method
	MethodInfo_t * ___method_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(EventCacheKey_t3133620722, ___target_0)); }
	inline RuntimeObject * get_target_0() const { return ___target_0; }
	inline RuntimeObject ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RuntimeObject * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(EventCacheKey_t3133620722, ___method_1)); }
	inline MethodInfo_t * get_method_1() const { return ___method_1; }
	inline MethodInfo_t ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(MethodInfo_t * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier((&___method_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey
struct EventCacheKey_t3133620722_marshaled_pinvoke
{
	Il2CppIUnknown* ___target_0;
	MethodInfo_t * ___method_1;
};
// Native definition for COM marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey
struct EventCacheKey_t3133620722_marshaled_com
{
	Il2CppIUnknown* ___target_0;
	MethodInfo_t * ___method_1;
};
#endif // EVENTCACHEKEY_T3133620722_H
#ifndef RESOURCELOCATOR_T3723970807_H
#define RESOURCELOCATOR_T3723970807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResourceLocator
struct  ResourceLocator_t3723970807 
{
public:
	// System.Object System.Resources.ResourceLocator::_value
	RuntimeObject * ____value_0;
	// System.Int32 System.Resources.ResourceLocator::_dataPos
	int32_t ____dataPos_1;

public:
	inline static int32_t get_offset_of__value_0() { return static_cast<int32_t>(offsetof(ResourceLocator_t3723970807, ____value_0)); }
	inline RuntimeObject * get__value_0() const { return ____value_0; }
	inline RuntimeObject ** get_address_of__value_0() { return &____value_0; }
	inline void set__value_0(RuntimeObject * value)
	{
		____value_0 = value;
		Il2CppCodeGenWriteBarrier((&____value_0), value);
	}

	inline static int32_t get_offset_of__dataPos_1() { return static_cast<int32_t>(offsetof(ResourceLocator_t3723970807, ____dataPos_1)); }
	inline int32_t get__dataPos_1() const { return ____dataPos_1; }
	inline int32_t* get_address_of__dataPos_1() { return &____dataPos_1; }
	inline void set__dataPos_1(int32_t value)
	{
		____dataPos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Resources.ResourceLocator
struct ResourceLocator_t3723970807_marshaled_pinvoke
{
	Il2CppIUnknown* ____value_0;
	int32_t ____dataPos_1;
};
// Native definition for COM marshalling of System.Resources.ResourceLocator
struct ResourceLocator_t3723970807_marshaled_com
{
	Il2CppIUnknown* ____value_0;
	int32_t ____dataPos_1;
};
#endif // RESOURCELOCATOR_T3723970807_H
#ifndef XBOXCONTROLLERDATA_T1920221146_H
#define XBOXCONTROLLERDATA_T1920221146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.XboxControllerData
struct  XboxControllerData_t1920221146 
{
public:
	// System.String HoloToolkit.Unity.InputModule.XboxControllerData::<GamePadName>k__BackingField
	String_t* ___U3CGamePadNameU3Ek__BackingField_0;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftStickHorizontalAxis>k__BackingField
	float ___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftStickVerticalAxis>k__BackingField
	float ___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightStickHorizontalAxis>k__BackingField
	float ___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightStickVerticalAxis>k__BackingField
	float ___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxDpadHorizontalAxis>k__BackingField
	float ___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxDpadVerticalAxis>k__BackingField
	float ___U3CXboxDpadVerticalAxisU3Ek__BackingField_6;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftTriggerAxis>k__BackingField
	float ___U3CXboxLeftTriggerAxisU3Ek__BackingField_7;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightTriggerAxis>k__BackingField
	float ___U3CXboxRightTriggerAxisU3Ek__BackingField_8;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxSharedTriggerAxis>k__BackingField
	float ___U3CXboxSharedTriggerAxisU3Ek__BackingField_9;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxA_Pressed>k__BackingField
	bool ___U3CXboxA_PressedU3Ek__BackingField_10;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxB_Pressed>k__BackingField
	bool ___U3CXboxB_PressedU3Ek__BackingField_11;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxX_Pressed>k__BackingField
	bool ___U3CXboxX_PressedU3Ek__BackingField_12;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxY_Pressed>k__BackingField
	bool ___U3CXboxY_PressedU3Ek__BackingField_13;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftBumper_Pressed>k__BackingField
	bool ___U3CXboxLeftBumper_PressedU3Ek__BackingField_14;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightBumper_Pressed>k__BackingField
	bool ___U3CXboxRightBumper_PressedU3Ek__BackingField_15;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftStick_Pressed>k__BackingField
	bool ___U3CXboxLeftStick_PressedU3Ek__BackingField_16;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightStick_Pressed>k__BackingField
	bool ___U3CXboxRightStick_PressedU3Ek__BackingField_17;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxView_Pressed>k__BackingField
	bool ___U3CXboxView_PressedU3Ek__BackingField_18;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxMenu_Pressed>k__BackingField
	bool ___U3CXboxMenu_PressedU3Ek__BackingField_19;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxA_Up>k__BackingField
	bool ___U3CXboxA_UpU3Ek__BackingField_20;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxB_Up>k__BackingField
	bool ___U3CXboxB_UpU3Ek__BackingField_21;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxX_Up>k__BackingField
	bool ___U3CXboxX_UpU3Ek__BackingField_22;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxY_Up>k__BackingField
	bool ___U3CXboxY_UpU3Ek__BackingField_23;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftBumper_Up>k__BackingField
	bool ___U3CXboxLeftBumper_UpU3Ek__BackingField_24;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightBumper_Up>k__BackingField
	bool ___U3CXboxRightBumper_UpU3Ek__BackingField_25;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftStick_Up>k__BackingField
	bool ___U3CXboxLeftStick_UpU3Ek__BackingField_26;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightStick_Up>k__BackingField
	bool ___U3CXboxRightStick_UpU3Ek__BackingField_27;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxView_Up>k__BackingField
	bool ___U3CXboxView_UpU3Ek__BackingField_28;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxMenu_Up>k__BackingField
	bool ___U3CXboxMenu_UpU3Ek__BackingField_29;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxA_Down>k__BackingField
	bool ___U3CXboxA_DownU3Ek__BackingField_30;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxB_Down>k__BackingField
	bool ___U3CXboxB_DownU3Ek__BackingField_31;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxX_Down>k__BackingField
	bool ___U3CXboxX_DownU3Ek__BackingField_32;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxY_Down>k__BackingField
	bool ___U3CXboxY_DownU3Ek__BackingField_33;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftBumper_Down>k__BackingField
	bool ___U3CXboxLeftBumper_DownU3Ek__BackingField_34;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightBumper_Down>k__BackingField
	bool ___U3CXboxRightBumper_DownU3Ek__BackingField_35;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftStick_Down>k__BackingField
	bool ___U3CXboxLeftStick_DownU3Ek__BackingField_36;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightStick_Down>k__BackingField
	bool ___U3CXboxRightStick_DownU3Ek__BackingField_37;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxView_Down>k__BackingField
	bool ___U3CXboxView_DownU3Ek__BackingField_38;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxMenu_Down>k__BackingField
	bool ___U3CXboxMenu_DownU3Ek__BackingField_39;

public:
	inline static int32_t get_offset_of_U3CGamePadNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CGamePadNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CGamePadNameU3Ek__BackingField_0() const { return ___U3CGamePadNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CGamePadNameU3Ek__BackingField_0() { return &___U3CGamePadNameU3Ek__BackingField_0; }
	inline void set_U3CGamePadNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CGamePadNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGamePadNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1)); }
	inline float get_U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1() const { return ___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1; }
	inline float* get_address_of_U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1() { return &___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1; }
	inline void set_U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1(float value)
	{
		___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2)); }
	inline float get_U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2() const { return ___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2; }
	inline float* get_address_of_U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2() { return &___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2; }
	inline void set_U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2(float value)
	{
		___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3)); }
	inline float get_U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3() const { return ___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3; }
	inline float* get_address_of_U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3() { return &___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3; }
	inline void set_U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3(float value)
	{
		___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightStickVerticalAxisU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4)); }
	inline float get_U3CXboxRightStickVerticalAxisU3Ek__BackingField_4() const { return ___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4; }
	inline float* get_address_of_U3CXboxRightStickVerticalAxisU3Ek__BackingField_4() { return &___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4; }
	inline void set_U3CXboxRightStickVerticalAxisU3Ek__BackingField_4(float value)
	{
		___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CXboxDpadHorizontalAxisU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5)); }
	inline float get_U3CXboxDpadHorizontalAxisU3Ek__BackingField_5() const { return ___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5; }
	inline float* get_address_of_U3CXboxDpadHorizontalAxisU3Ek__BackingField_5() { return &___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5; }
	inline void set_U3CXboxDpadHorizontalAxisU3Ek__BackingField_5(float value)
	{
		___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CXboxDpadVerticalAxisU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxDpadVerticalAxisU3Ek__BackingField_6)); }
	inline float get_U3CXboxDpadVerticalAxisU3Ek__BackingField_6() const { return ___U3CXboxDpadVerticalAxisU3Ek__BackingField_6; }
	inline float* get_address_of_U3CXboxDpadVerticalAxisU3Ek__BackingField_6() { return &___U3CXboxDpadVerticalAxisU3Ek__BackingField_6; }
	inline void set_U3CXboxDpadVerticalAxisU3Ek__BackingField_6(float value)
	{
		___U3CXboxDpadVerticalAxisU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftTriggerAxisU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftTriggerAxisU3Ek__BackingField_7)); }
	inline float get_U3CXboxLeftTriggerAxisU3Ek__BackingField_7() const { return ___U3CXboxLeftTriggerAxisU3Ek__BackingField_7; }
	inline float* get_address_of_U3CXboxLeftTriggerAxisU3Ek__BackingField_7() { return &___U3CXboxLeftTriggerAxisU3Ek__BackingField_7; }
	inline void set_U3CXboxLeftTriggerAxisU3Ek__BackingField_7(float value)
	{
		___U3CXboxLeftTriggerAxisU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightTriggerAxisU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightTriggerAxisU3Ek__BackingField_8)); }
	inline float get_U3CXboxRightTriggerAxisU3Ek__BackingField_8() const { return ___U3CXboxRightTriggerAxisU3Ek__BackingField_8; }
	inline float* get_address_of_U3CXboxRightTriggerAxisU3Ek__BackingField_8() { return &___U3CXboxRightTriggerAxisU3Ek__BackingField_8; }
	inline void set_U3CXboxRightTriggerAxisU3Ek__BackingField_8(float value)
	{
		___U3CXboxRightTriggerAxisU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CXboxSharedTriggerAxisU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxSharedTriggerAxisU3Ek__BackingField_9)); }
	inline float get_U3CXboxSharedTriggerAxisU3Ek__BackingField_9() const { return ___U3CXboxSharedTriggerAxisU3Ek__BackingField_9; }
	inline float* get_address_of_U3CXboxSharedTriggerAxisU3Ek__BackingField_9() { return &___U3CXboxSharedTriggerAxisU3Ek__BackingField_9; }
	inline void set_U3CXboxSharedTriggerAxisU3Ek__BackingField_9(float value)
	{
		___U3CXboxSharedTriggerAxisU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CXboxA_PressedU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxA_PressedU3Ek__BackingField_10)); }
	inline bool get_U3CXboxA_PressedU3Ek__BackingField_10() const { return ___U3CXboxA_PressedU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CXboxA_PressedU3Ek__BackingField_10() { return &___U3CXboxA_PressedU3Ek__BackingField_10; }
	inline void set_U3CXboxA_PressedU3Ek__BackingField_10(bool value)
	{
		___U3CXboxA_PressedU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CXboxB_PressedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxB_PressedU3Ek__BackingField_11)); }
	inline bool get_U3CXboxB_PressedU3Ek__BackingField_11() const { return ___U3CXboxB_PressedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CXboxB_PressedU3Ek__BackingField_11() { return &___U3CXboxB_PressedU3Ek__BackingField_11; }
	inline void set_U3CXboxB_PressedU3Ek__BackingField_11(bool value)
	{
		___U3CXboxB_PressedU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CXboxX_PressedU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxX_PressedU3Ek__BackingField_12)); }
	inline bool get_U3CXboxX_PressedU3Ek__BackingField_12() const { return ___U3CXboxX_PressedU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CXboxX_PressedU3Ek__BackingField_12() { return &___U3CXboxX_PressedU3Ek__BackingField_12; }
	inline void set_U3CXboxX_PressedU3Ek__BackingField_12(bool value)
	{
		___U3CXboxX_PressedU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CXboxY_PressedU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxY_PressedU3Ek__BackingField_13)); }
	inline bool get_U3CXboxY_PressedU3Ek__BackingField_13() const { return ___U3CXboxY_PressedU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CXboxY_PressedU3Ek__BackingField_13() { return &___U3CXboxY_PressedU3Ek__BackingField_13; }
	inline void set_U3CXboxY_PressedU3Ek__BackingField_13(bool value)
	{
		___U3CXboxY_PressedU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftBumper_PressedU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftBumper_PressedU3Ek__BackingField_14)); }
	inline bool get_U3CXboxLeftBumper_PressedU3Ek__BackingField_14() const { return ___U3CXboxLeftBumper_PressedU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CXboxLeftBumper_PressedU3Ek__BackingField_14() { return &___U3CXboxLeftBumper_PressedU3Ek__BackingField_14; }
	inline void set_U3CXboxLeftBumper_PressedU3Ek__BackingField_14(bool value)
	{
		___U3CXboxLeftBumper_PressedU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightBumper_PressedU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightBumper_PressedU3Ek__BackingField_15)); }
	inline bool get_U3CXboxRightBumper_PressedU3Ek__BackingField_15() const { return ___U3CXboxRightBumper_PressedU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CXboxRightBumper_PressedU3Ek__BackingField_15() { return &___U3CXboxRightBumper_PressedU3Ek__BackingField_15; }
	inline void set_U3CXboxRightBumper_PressedU3Ek__BackingField_15(bool value)
	{
		___U3CXboxRightBumper_PressedU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftStick_PressedU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftStick_PressedU3Ek__BackingField_16)); }
	inline bool get_U3CXboxLeftStick_PressedU3Ek__BackingField_16() const { return ___U3CXboxLeftStick_PressedU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CXboxLeftStick_PressedU3Ek__BackingField_16() { return &___U3CXboxLeftStick_PressedU3Ek__BackingField_16; }
	inline void set_U3CXboxLeftStick_PressedU3Ek__BackingField_16(bool value)
	{
		___U3CXboxLeftStick_PressedU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightStick_PressedU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightStick_PressedU3Ek__BackingField_17)); }
	inline bool get_U3CXboxRightStick_PressedU3Ek__BackingField_17() const { return ___U3CXboxRightStick_PressedU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CXboxRightStick_PressedU3Ek__BackingField_17() { return &___U3CXboxRightStick_PressedU3Ek__BackingField_17; }
	inline void set_U3CXboxRightStick_PressedU3Ek__BackingField_17(bool value)
	{
		___U3CXboxRightStick_PressedU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CXboxView_PressedU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxView_PressedU3Ek__BackingField_18)); }
	inline bool get_U3CXboxView_PressedU3Ek__BackingField_18() const { return ___U3CXboxView_PressedU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CXboxView_PressedU3Ek__BackingField_18() { return &___U3CXboxView_PressedU3Ek__BackingField_18; }
	inline void set_U3CXboxView_PressedU3Ek__BackingField_18(bool value)
	{
		___U3CXboxView_PressedU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CXboxMenu_PressedU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxMenu_PressedU3Ek__BackingField_19)); }
	inline bool get_U3CXboxMenu_PressedU3Ek__BackingField_19() const { return ___U3CXboxMenu_PressedU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CXboxMenu_PressedU3Ek__BackingField_19() { return &___U3CXboxMenu_PressedU3Ek__BackingField_19; }
	inline void set_U3CXboxMenu_PressedU3Ek__BackingField_19(bool value)
	{
		___U3CXboxMenu_PressedU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CXboxA_UpU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxA_UpU3Ek__BackingField_20)); }
	inline bool get_U3CXboxA_UpU3Ek__BackingField_20() const { return ___U3CXboxA_UpU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CXboxA_UpU3Ek__BackingField_20() { return &___U3CXboxA_UpU3Ek__BackingField_20; }
	inline void set_U3CXboxA_UpU3Ek__BackingField_20(bool value)
	{
		___U3CXboxA_UpU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CXboxB_UpU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxB_UpU3Ek__BackingField_21)); }
	inline bool get_U3CXboxB_UpU3Ek__BackingField_21() const { return ___U3CXboxB_UpU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CXboxB_UpU3Ek__BackingField_21() { return &___U3CXboxB_UpU3Ek__BackingField_21; }
	inline void set_U3CXboxB_UpU3Ek__BackingField_21(bool value)
	{
		___U3CXboxB_UpU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CXboxX_UpU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxX_UpU3Ek__BackingField_22)); }
	inline bool get_U3CXboxX_UpU3Ek__BackingField_22() const { return ___U3CXboxX_UpU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CXboxX_UpU3Ek__BackingField_22() { return &___U3CXboxX_UpU3Ek__BackingField_22; }
	inline void set_U3CXboxX_UpU3Ek__BackingField_22(bool value)
	{
		___U3CXboxX_UpU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CXboxY_UpU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxY_UpU3Ek__BackingField_23)); }
	inline bool get_U3CXboxY_UpU3Ek__BackingField_23() const { return ___U3CXboxY_UpU3Ek__BackingField_23; }
	inline bool* get_address_of_U3CXboxY_UpU3Ek__BackingField_23() { return &___U3CXboxY_UpU3Ek__BackingField_23; }
	inline void set_U3CXboxY_UpU3Ek__BackingField_23(bool value)
	{
		___U3CXboxY_UpU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftBumper_UpU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftBumper_UpU3Ek__BackingField_24)); }
	inline bool get_U3CXboxLeftBumper_UpU3Ek__BackingField_24() const { return ___U3CXboxLeftBumper_UpU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CXboxLeftBumper_UpU3Ek__BackingField_24() { return &___U3CXboxLeftBumper_UpU3Ek__BackingField_24; }
	inline void set_U3CXboxLeftBumper_UpU3Ek__BackingField_24(bool value)
	{
		___U3CXboxLeftBumper_UpU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightBumper_UpU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightBumper_UpU3Ek__BackingField_25)); }
	inline bool get_U3CXboxRightBumper_UpU3Ek__BackingField_25() const { return ___U3CXboxRightBumper_UpU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CXboxRightBumper_UpU3Ek__BackingField_25() { return &___U3CXboxRightBumper_UpU3Ek__BackingField_25; }
	inline void set_U3CXboxRightBumper_UpU3Ek__BackingField_25(bool value)
	{
		___U3CXboxRightBumper_UpU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftStick_UpU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftStick_UpU3Ek__BackingField_26)); }
	inline bool get_U3CXboxLeftStick_UpU3Ek__BackingField_26() const { return ___U3CXboxLeftStick_UpU3Ek__BackingField_26; }
	inline bool* get_address_of_U3CXboxLeftStick_UpU3Ek__BackingField_26() { return &___U3CXboxLeftStick_UpU3Ek__BackingField_26; }
	inline void set_U3CXboxLeftStick_UpU3Ek__BackingField_26(bool value)
	{
		___U3CXboxLeftStick_UpU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightStick_UpU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightStick_UpU3Ek__BackingField_27)); }
	inline bool get_U3CXboxRightStick_UpU3Ek__BackingField_27() const { return ___U3CXboxRightStick_UpU3Ek__BackingField_27; }
	inline bool* get_address_of_U3CXboxRightStick_UpU3Ek__BackingField_27() { return &___U3CXboxRightStick_UpU3Ek__BackingField_27; }
	inline void set_U3CXboxRightStick_UpU3Ek__BackingField_27(bool value)
	{
		___U3CXboxRightStick_UpU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CXboxView_UpU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxView_UpU3Ek__BackingField_28)); }
	inline bool get_U3CXboxView_UpU3Ek__BackingField_28() const { return ___U3CXboxView_UpU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CXboxView_UpU3Ek__BackingField_28() { return &___U3CXboxView_UpU3Ek__BackingField_28; }
	inline void set_U3CXboxView_UpU3Ek__BackingField_28(bool value)
	{
		___U3CXboxView_UpU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CXboxMenu_UpU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxMenu_UpU3Ek__BackingField_29)); }
	inline bool get_U3CXboxMenu_UpU3Ek__BackingField_29() const { return ___U3CXboxMenu_UpU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CXboxMenu_UpU3Ek__BackingField_29() { return &___U3CXboxMenu_UpU3Ek__BackingField_29; }
	inline void set_U3CXboxMenu_UpU3Ek__BackingField_29(bool value)
	{
		___U3CXboxMenu_UpU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CXboxA_DownU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxA_DownU3Ek__BackingField_30)); }
	inline bool get_U3CXboxA_DownU3Ek__BackingField_30() const { return ___U3CXboxA_DownU3Ek__BackingField_30; }
	inline bool* get_address_of_U3CXboxA_DownU3Ek__BackingField_30() { return &___U3CXboxA_DownU3Ek__BackingField_30; }
	inline void set_U3CXboxA_DownU3Ek__BackingField_30(bool value)
	{
		___U3CXboxA_DownU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CXboxB_DownU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxB_DownU3Ek__BackingField_31)); }
	inline bool get_U3CXboxB_DownU3Ek__BackingField_31() const { return ___U3CXboxB_DownU3Ek__BackingField_31; }
	inline bool* get_address_of_U3CXboxB_DownU3Ek__BackingField_31() { return &___U3CXboxB_DownU3Ek__BackingField_31; }
	inline void set_U3CXboxB_DownU3Ek__BackingField_31(bool value)
	{
		___U3CXboxB_DownU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CXboxX_DownU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxX_DownU3Ek__BackingField_32)); }
	inline bool get_U3CXboxX_DownU3Ek__BackingField_32() const { return ___U3CXboxX_DownU3Ek__BackingField_32; }
	inline bool* get_address_of_U3CXboxX_DownU3Ek__BackingField_32() { return &___U3CXboxX_DownU3Ek__BackingField_32; }
	inline void set_U3CXboxX_DownU3Ek__BackingField_32(bool value)
	{
		___U3CXboxX_DownU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CXboxY_DownU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxY_DownU3Ek__BackingField_33)); }
	inline bool get_U3CXboxY_DownU3Ek__BackingField_33() const { return ___U3CXboxY_DownU3Ek__BackingField_33; }
	inline bool* get_address_of_U3CXboxY_DownU3Ek__BackingField_33() { return &___U3CXboxY_DownU3Ek__BackingField_33; }
	inline void set_U3CXboxY_DownU3Ek__BackingField_33(bool value)
	{
		___U3CXboxY_DownU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftBumper_DownU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftBumper_DownU3Ek__BackingField_34)); }
	inline bool get_U3CXboxLeftBumper_DownU3Ek__BackingField_34() const { return ___U3CXboxLeftBumper_DownU3Ek__BackingField_34; }
	inline bool* get_address_of_U3CXboxLeftBumper_DownU3Ek__BackingField_34() { return &___U3CXboxLeftBumper_DownU3Ek__BackingField_34; }
	inline void set_U3CXboxLeftBumper_DownU3Ek__BackingField_34(bool value)
	{
		___U3CXboxLeftBumper_DownU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightBumper_DownU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightBumper_DownU3Ek__BackingField_35)); }
	inline bool get_U3CXboxRightBumper_DownU3Ek__BackingField_35() const { return ___U3CXboxRightBumper_DownU3Ek__BackingField_35; }
	inline bool* get_address_of_U3CXboxRightBumper_DownU3Ek__BackingField_35() { return &___U3CXboxRightBumper_DownU3Ek__BackingField_35; }
	inline void set_U3CXboxRightBumper_DownU3Ek__BackingField_35(bool value)
	{
		___U3CXboxRightBumper_DownU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftStick_DownU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftStick_DownU3Ek__BackingField_36)); }
	inline bool get_U3CXboxLeftStick_DownU3Ek__BackingField_36() const { return ___U3CXboxLeftStick_DownU3Ek__BackingField_36; }
	inline bool* get_address_of_U3CXboxLeftStick_DownU3Ek__BackingField_36() { return &___U3CXboxLeftStick_DownU3Ek__BackingField_36; }
	inline void set_U3CXboxLeftStick_DownU3Ek__BackingField_36(bool value)
	{
		___U3CXboxLeftStick_DownU3Ek__BackingField_36 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightStick_DownU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightStick_DownU3Ek__BackingField_37)); }
	inline bool get_U3CXboxRightStick_DownU3Ek__BackingField_37() const { return ___U3CXboxRightStick_DownU3Ek__BackingField_37; }
	inline bool* get_address_of_U3CXboxRightStick_DownU3Ek__BackingField_37() { return &___U3CXboxRightStick_DownU3Ek__BackingField_37; }
	inline void set_U3CXboxRightStick_DownU3Ek__BackingField_37(bool value)
	{
		___U3CXboxRightStick_DownU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_U3CXboxView_DownU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxView_DownU3Ek__BackingField_38)); }
	inline bool get_U3CXboxView_DownU3Ek__BackingField_38() const { return ___U3CXboxView_DownU3Ek__BackingField_38; }
	inline bool* get_address_of_U3CXboxView_DownU3Ek__BackingField_38() { return &___U3CXboxView_DownU3Ek__BackingField_38; }
	inline void set_U3CXboxView_DownU3Ek__BackingField_38(bool value)
	{
		___U3CXboxView_DownU3Ek__BackingField_38 = value;
	}

	inline static int32_t get_offset_of_U3CXboxMenu_DownU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxMenu_DownU3Ek__BackingField_39)); }
	inline bool get_U3CXboxMenu_DownU3Ek__BackingField_39() const { return ___U3CXboxMenu_DownU3Ek__BackingField_39; }
	inline bool* get_address_of_U3CXboxMenu_DownU3Ek__BackingField_39() { return &___U3CXboxMenu_DownU3Ek__BackingField_39; }
	inline void set_U3CXboxMenu_DownU3Ek__BackingField_39(bool value)
	{
		___U3CXboxMenu_DownU3Ek__BackingField_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.InputModule.XboxControllerData
struct XboxControllerData_t1920221146_marshaled_pinvoke
{
	char* ___U3CGamePadNameU3Ek__BackingField_0;
	float ___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1;
	float ___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2;
	float ___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3;
	float ___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4;
	float ___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5;
	float ___U3CXboxDpadVerticalAxisU3Ek__BackingField_6;
	float ___U3CXboxLeftTriggerAxisU3Ek__BackingField_7;
	float ___U3CXboxRightTriggerAxisU3Ek__BackingField_8;
	float ___U3CXboxSharedTriggerAxisU3Ek__BackingField_9;
	int32_t ___U3CXboxA_PressedU3Ek__BackingField_10;
	int32_t ___U3CXboxB_PressedU3Ek__BackingField_11;
	int32_t ___U3CXboxX_PressedU3Ek__BackingField_12;
	int32_t ___U3CXboxY_PressedU3Ek__BackingField_13;
	int32_t ___U3CXboxLeftBumper_PressedU3Ek__BackingField_14;
	int32_t ___U3CXboxRightBumper_PressedU3Ek__BackingField_15;
	int32_t ___U3CXboxLeftStick_PressedU3Ek__BackingField_16;
	int32_t ___U3CXboxRightStick_PressedU3Ek__BackingField_17;
	int32_t ___U3CXboxView_PressedU3Ek__BackingField_18;
	int32_t ___U3CXboxMenu_PressedU3Ek__BackingField_19;
	int32_t ___U3CXboxA_UpU3Ek__BackingField_20;
	int32_t ___U3CXboxB_UpU3Ek__BackingField_21;
	int32_t ___U3CXboxX_UpU3Ek__BackingField_22;
	int32_t ___U3CXboxY_UpU3Ek__BackingField_23;
	int32_t ___U3CXboxLeftBumper_UpU3Ek__BackingField_24;
	int32_t ___U3CXboxRightBumper_UpU3Ek__BackingField_25;
	int32_t ___U3CXboxLeftStick_UpU3Ek__BackingField_26;
	int32_t ___U3CXboxRightStick_UpU3Ek__BackingField_27;
	int32_t ___U3CXboxView_UpU3Ek__BackingField_28;
	int32_t ___U3CXboxMenu_UpU3Ek__BackingField_29;
	int32_t ___U3CXboxA_DownU3Ek__BackingField_30;
	int32_t ___U3CXboxB_DownU3Ek__BackingField_31;
	int32_t ___U3CXboxX_DownU3Ek__BackingField_32;
	int32_t ___U3CXboxY_DownU3Ek__BackingField_33;
	int32_t ___U3CXboxLeftBumper_DownU3Ek__BackingField_34;
	int32_t ___U3CXboxRightBumper_DownU3Ek__BackingField_35;
	int32_t ___U3CXboxLeftStick_DownU3Ek__BackingField_36;
	int32_t ___U3CXboxRightStick_DownU3Ek__BackingField_37;
	int32_t ___U3CXboxView_DownU3Ek__BackingField_38;
	int32_t ___U3CXboxMenu_DownU3Ek__BackingField_39;
};
// Native definition for COM marshalling of HoloToolkit.Unity.InputModule.XboxControllerData
struct XboxControllerData_t1920221146_marshaled_com
{
	Il2CppChar* ___U3CGamePadNameU3Ek__BackingField_0;
	float ___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1;
	float ___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2;
	float ___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3;
	float ___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4;
	float ___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5;
	float ___U3CXboxDpadVerticalAxisU3Ek__BackingField_6;
	float ___U3CXboxLeftTriggerAxisU3Ek__BackingField_7;
	float ___U3CXboxRightTriggerAxisU3Ek__BackingField_8;
	float ___U3CXboxSharedTriggerAxisU3Ek__BackingField_9;
	int32_t ___U3CXboxA_PressedU3Ek__BackingField_10;
	int32_t ___U3CXboxB_PressedU3Ek__BackingField_11;
	int32_t ___U3CXboxX_PressedU3Ek__BackingField_12;
	int32_t ___U3CXboxY_PressedU3Ek__BackingField_13;
	int32_t ___U3CXboxLeftBumper_PressedU3Ek__BackingField_14;
	int32_t ___U3CXboxRightBumper_PressedU3Ek__BackingField_15;
	int32_t ___U3CXboxLeftStick_PressedU3Ek__BackingField_16;
	int32_t ___U3CXboxRightStick_PressedU3Ek__BackingField_17;
	int32_t ___U3CXboxView_PressedU3Ek__BackingField_18;
	int32_t ___U3CXboxMenu_PressedU3Ek__BackingField_19;
	int32_t ___U3CXboxA_UpU3Ek__BackingField_20;
	int32_t ___U3CXboxB_UpU3Ek__BackingField_21;
	int32_t ___U3CXboxX_UpU3Ek__BackingField_22;
	int32_t ___U3CXboxY_UpU3Ek__BackingField_23;
	int32_t ___U3CXboxLeftBumper_UpU3Ek__BackingField_24;
	int32_t ___U3CXboxRightBumper_UpU3Ek__BackingField_25;
	int32_t ___U3CXboxLeftStick_UpU3Ek__BackingField_26;
	int32_t ___U3CXboxRightStick_UpU3Ek__BackingField_27;
	int32_t ___U3CXboxView_UpU3Ek__BackingField_28;
	int32_t ___U3CXboxMenu_UpU3Ek__BackingField_29;
	int32_t ___U3CXboxA_DownU3Ek__BackingField_30;
	int32_t ___U3CXboxB_DownU3Ek__BackingField_31;
	int32_t ___U3CXboxX_DownU3Ek__BackingField_32;
	int32_t ___U3CXboxY_DownU3Ek__BackingField_33;
	int32_t ___U3CXboxLeftBumper_DownU3Ek__BackingField_34;
	int32_t ___U3CXboxRightBumper_DownU3Ek__BackingField_35;
	int32_t ___U3CXboxLeftStick_DownU3Ek__BackingField_36;
	int32_t ___U3CXboxRightStick_DownU3Ek__BackingField_37;
	int32_t ___U3CXboxView_DownU3Ek__BackingField_38;
	int32_t ___U3CXboxMenu_DownU3Ek__BackingField_39;
};
#endif // XBOXCONTROLLERDATA_T1920221146_H
#ifndef EVENTCACHEENTRY_T156445199_H
#define EVENTCACHEENTRY_T156445199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry
struct  EventCacheEntry_t156445199 
{
public:
	// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventRegistrationTokenListWithCount> System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry::registrationTable
	ConditionalWeakTable_2_t3044373657 * ___registrationTable_0;
	// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/TokenListCount System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry::tokenListCount
	TokenListCount_t1606756367 * ___tokenListCount_1;

public:
	inline static int32_t get_offset_of_registrationTable_0() { return static_cast<int32_t>(offsetof(EventCacheEntry_t156445199, ___registrationTable_0)); }
	inline ConditionalWeakTable_2_t3044373657 * get_registrationTable_0() const { return ___registrationTable_0; }
	inline ConditionalWeakTable_2_t3044373657 ** get_address_of_registrationTable_0() { return &___registrationTable_0; }
	inline void set_registrationTable_0(ConditionalWeakTable_2_t3044373657 * value)
	{
		___registrationTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___registrationTable_0), value);
	}

	inline static int32_t get_offset_of_tokenListCount_1() { return static_cast<int32_t>(offsetof(EventCacheEntry_t156445199, ___tokenListCount_1)); }
	inline TokenListCount_t1606756367 * get_tokenListCount_1() const { return ___tokenListCount_1; }
	inline TokenListCount_t1606756367 ** get_address_of_tokenListCount_1() { return &___tokenListCount_1; }
	inline void set_tokenListCount_1(TokenListCount_t1606756367 * value)
	{
		___tokenListCount_1 = value;
		Il2CppCodeGenWriteBarrier((&___tokenListCount_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry
struct EventCacheEntry_t156445199_marshaled_pinvoke
{
	ConditionalWeakTable_2_t3044373657 * ___registrationTable_0;
	TokenListCount_t1606756367 * ___tokenListCount_1;
};
// Native definition for COM marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry
struct EventCacheEntry_t156445199_marshaled_com
{
	ConditionalWeakTable_2_t3044373657 * ___registrationTable_0;
	TokenListCount_t1606756367 * ___tokenListCount_1;
};
#endif // EVENTCACHEENTRY_T156445199_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef RESOLVERCONTRACTKEY_T3292851287_H
#define RESOLVERCONTRACTKEY_T3292851287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ResolverContractKey
struct  ResolverContractKey_t3292851287 
{
public:
	// System.Type Newtonsoft.Json.Serialization.ResolverContractKey::_resolverType
	Type_t * ____resolverType_0;
	// System.Type Newtonsoft.Json.Serialization.ResolverContractKey::_contractType
	Type_t * ____contractType_1;

public:
	inline static int32_t get_offset_of__resolverType_0() { return static_cast<int32_t>(offsetof(ResolverContractKey_t3292851287, ____resolverType_0)); }
	inline Type_t * get__resolverType_0() const { return ____resolverType_0; }
	inline Type_t ** get_address_of__resolverType_0() { return &____resolverType_0; }
	inline void set__resolverType_0(Type_t * value)
	{
		____resolverType_0 = value;
		Il2CppCodeGenWriteBarrier((&____resolverType_0), value);
	}

	inline static int32_t get_offset_of__contractType_1() { return static_cast<int32_t>(offsetof(ResolverContractKey_t3292851287, ____contractType_1)); }
	inline Type_t * get__contractType_1() const { return ____contractType_1; }
	inline Type_t ** get_address_of__contractType_1() { return &____contractType_1; }
	inline void set__contractType_1(Type_t * value)
	{
		____contractType_1 = value;
		Il2CppCodeGenWriteBarrier((&____contractType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Serialization.ResolverContractKey
struct ResolverContractKey_t3292851287_marshaled_pinvoke
{
	Type_t * ____resolverType_0;
	Type_t * ____contractType_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Serialization.ResolverContractKey
struct ResolverContractKey_t3292851287_marshaled_com
{
	Type_t * ____resolverType_0;
	Type_t * ____contractType_1;
};
#endif // RESOLVERCONTRACTKEY_T3292851287_H
#ifndef TYPECONVERTKEY_T285306760_H
#define TYPECONVERTKEY_T285306760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct  TypeConvertKey_t285306760 
{
public:
	// System.Type Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey::_initialType
	Type_t * ____initialType_0;
	// System.Type Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey::_targetType
	Type_t * ____targetType_1;

public:
	inline static int32_t get_offset_of__initialType_0() { return static_cast<int32_t>(offsetof(TypeConvertKey_t285306760, ____initialType_0)); }
	inline Type_t * get__initialType_0() const { return ____initialType_0; }
	inline Type_t ** get_address_of__initialType_0() { return &____initialType_0; }
	inline void set__initialType_0(Type_t * value)
	{
		____initialType_0 = value;
		Il2CppCodeGenWriteBarrier((&____initialType_0), value);
	}

	inline static int32_t get_offset_of__targetType_1() { return static_cast<int32_t>(offsetof(TypeConvertKey_t285306760, ____targetType_1)); }
	inline Type_t * get__targetType_1() const { return ____targetType_1; }
	inline Type_t ** get_address_of__targetType_1() { return &____targetType_1; }
	inline void set__targetType_1(Type_t * value)
	{
		____targetType_1 = value;
		Il2CppCodeGenWriteBarrier((&____targetType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t285306760_marshaled_pinvoke
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t285306760_marshaled_com
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
#endif // TYPECONVERTKEY_T285306760_H
#ifndef TYPENAMEKEY_T2985541961_H
#define TYPENAMEKEY_T2985541961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey
struct  TypeNameKey_t2985541961 
{
public:
	// System.String Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::AssemblyName
	String_t* ___AssemblyName_0;
	// System.String Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::TypeName
	String_t* ___TypeName_1;

public:
	inline static int32_t get_offset_of_AssemblyName_0() { return static_cast<int32_t>(offsetof(TypeNameKey_t2985541961, ___AssemblyName_0)); }
	inline String_t* get_AssemblyName_0() const { return ___AssemblyName_0; }
	inline String_t** get_address_of_AssemblyName_0() { return &___AssemblyName_0; }
	inline void set_AssemblyName_0(String_t* value)
	{
		___AssemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___AssemblyName_0), value);
	}

	inline static int32_t get_offset_of_TypeName_1() { return static_cast<int32_t>(offsetof(TypeNameKey_t2985541961, ___TypeName_1)); }
	inline String_t* get_TypeName_1() const { return ___TypeName_1; }
	inline String_t** get_address_of_TypeName_1() { return &___TypeName_1; }
	inline void set_TypeName_1(String_t* value)
	{
		___TypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey
struct TypeNameKey_t2985541961_marshaled_pinvoke
{
	char* ___AssemblyName_0;
	char* ___TypeName_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey
struct TypeNameKey_t2985541961_marshaled_com
{
	Il2CppChar* ___AssemblyName_0;
	Il2CppChar* ___TypeName_1;
};
#endif // TYPENAMEKEY_T2985541961_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_t4116647657* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_t4116647657* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_t4116647657* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((&___categoryForLatin1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef XPATHNODEREF_T3498189018_H
#define XPATHNODEREF_T3498189018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodeRef
struct  XPathNodeRef_t3498189018 
{
public:
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeRef::page
	XPathNodeU5BU5D_t47339301* ___page_0;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeRef::idx
	int32_t ___idx_1;

public:
	inline static int32_t get_offset_of_page_0() { return static_cast<int32_t>(offsetof(XPathNodeRef_t3498189018, ___page_0)); }
	inline XPathNodeU5BU5D_t47339301* get_page_0() const { return ___page_0; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_page_0() { return &___page_0; }
	inline void set_page_0(XPathNodeU5BU5D_t47339301* value)
	{
		___page_0 = value;
		Il2CppCodeGenWriteBarrier((&___page_0), value);
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(XPathNodeRef_t3498189018, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MS.Internal.Xml.Cache.XPathNodeRef
struct XPathNodeRef_t3498189018_marshaled_pinvoke
{
	XPathNode_t2208072876_marshaled_pinvoke* ___page_0;
	int32_t ___idx_1;
};
// Native definition for COM marshalling of MS.Internal.Xml.Cache.XPathNodeRef
struct XPathNodeRef_t3498189018_marshaled_com
{
	XPathNode_t2208072876_marshaled_com* ___page_0;
	int32_t ___idx_1;
};
#endif // XPATHNODEREF_T3498189018_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_4;
	// System.Int16 System.Guid::_b
	int16_t ____b_5;
	// System.Int16 System.Guid::_c
	int16_t ____c_6;
	// System.Byte System.Guid::_d
	uint8_t ____d_7;
	// System.Byte System.Guid::_e
	uint8_t ____e_8;
	// System.Byte System.Guid::_f
	uint8_t ____f_9;
	// System.Byte System.Guid::_g
	uint8_t ____g_10;
	// System.Byte System.Guid::_h
	uint8_t ____h_11;
	// System.Byte System.Guid::_i
	uint8_t ____i_12;
	// System.Byte System.Guid::_j
	uint8_t ____j_13;
	// System.Byte System.Guid::_k
	uint8_t ____k_14;

public:
	inline static int32_t get_offset_of__a_4() { return static_cast<int32_t>(offsetof(Guid_t, ____a_4)); }
	inline int32_t get__a_4() const { return ____a_4; }
	inline int32_t* get_address_of__a_4() { return &____a_4; }
	inline void set__a_4(int32_t value)
	{
		____a_4 = value;
	}

	inline static int32_t get_offset_of__b_5() { return static_cast<int32_t>(offsetof(Guid_t, ____b_5)); }
	inline int16_t get__b_5() const { return ____b_5; }
	inline int16_t* get_address_of__b_5() { return &____b_5; }
	inline void set__b_5(int16_t value)
	{
		____b_5 = value;
	}

	inline static int32_t get_offset_of__c_6() { return static_cast<int32_t>(offsetof(Guid_t, ____c_6)); }
	inline int16_t get__c_6() const { return ____c_6; }
	inline int16_t* get_address_of__c_6() { return &____c_6; }
	inline void set__c_6(int16_t value)
	{
		____c_6 = value;
	}

	inline static int32_t get_offset_of__d_7() { return static_cast<int32_t>(offsetof(Guid_t, ____d_7)); }
	inline uint8_t get__d_7() const { return ____d_7; }
	inline uint8_t* get_address_of__d_7() { return &____d_7; }
	inline void set__d_7(uint8_t value)
	{
		____d_7 = value;
	}

	inline static int32_t get_offset_of__e_8() { return static_cast<int32_t>(offsetof(Guid_t, ____e_8)); }
	inline uint8_t get__e_8() const { return ____e_8; }
	inline uint8_t* get_address_of__e_8() { return &____e_8; }
	inline void set__e_8(uint8_t value)
	{
		____e_8 = value;
	}

	inline static int32_t get_offset_of__f_9() { return static_cast<int32_t>(offsetof(Guid_t, ____f_9)); }
	inline uint8_t get__f_9() const { return ____f_9; }
	inline uint8_t* get_address_of__f_9() { return &____f_9; }
	inline void set__f_9(uint8_t value)
	{
		____f_9 = value;
	}

	inline static int32_t get_offset_of__g_10() { return static_cast<int32_t>(offsetof(Guid_t, ____g_10)); }
	inline uint8_t get__g_10() const { return ____g_10; }
	inline uint8_t* get_address_of__g_10() { return &____g_10; }
	inline void set__g_10(uint8_t value)
	{
		____g_10 = value;
	}

	inline static int32_t get_offset_of__h_11() { return static_cast<int32_t>(offsetof(Guid_t, ____h_11)); }
	inline uint8_t get__h_11() const { return ____h_11; }
	inline uint8_t* get_address_of__h_11() { return &____h_11; }
	inline void set__h_11(uint8_t value)
	{
		____h_11 = value;
	}

	inline static int32_t get_offset_of__i_12() { return static_cast<int32_t>(offsetof(Guid_t, ____i_12)); }
	inline uint8_t get__i_12() const { return ____i_12; }
	inline uint8_t* get_address_of__i_12() { return &____i_12; }
	inline void set__i_12(uint8_t value)
	{
		____i_12 = value;
	}

	inline static int32_t get_offset_of__j_13() { return static_cast<int32_t>(offsetof(Guid_t, ____j_13)); }
	inline uint8_t get__j_13() const { return ____j_13; }
	inline uint8_t* get_address_of__j_13() { return &____j_13; }
	inline void set__j_13(uint8_t value)
	{
		____j_13 = value;
	}

	inline static int32_t get_offset_of__k_14() { return static_cast<int32_t>(offsetof(Guid_t, ____k_14)); }
	inline uint8_t get__k_14() const { return ____k_14; }
	inline uint8_t* get_address_of__k_14() { return &____k_14; }
	inline void set__k_14(uint8_t value)
	{
		____k_14 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_0;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_1;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_2;
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_3;

public:
	inline static int32_t get_offset_of__rngAccess_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_0)); }
	inline RuntimeObject * get__rngAccess_0() const { return ____rngAccess_0; }
	inline RuntimeObject ** get_address_of__rngAccess_0() { return &____rngAccess_0; }
	inline void set__rngAccess_0(RuntimeObject * value)
	{
		____rngAccess_0 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_0), value);
	}

	inline static int32_t get_offset_of__rng_1() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_1)); }
	inline RandomNumberGenerator_t386037858 * get__rng_1() const { return ____rng_1; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_1() { return &____rng_1; }
	inline void set__rng_1(RandomNumberGenerator_t386037858 * value)
	{
		____rng_1 = value;
		Il2CppCodeGenWriteBarrier((&____rng_1), value);
	}

	inline static int32_t get_offset_of__fastRng_2() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_2)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_2() const { return ____fastRng_2; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_2() { return &____fastRng_2; }
	inline void set__fastRng_2(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_2 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_2), value);
	}

	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_3)); }
	inline Guid_t  get_Empty_3() const { return ___Empty_3; }
	inline Guid_t * get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(Guid_t  value)
	{
		___Empty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef TIMERDATA_T4056715490_H
#define TIMERDATA_T4056715490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TimerScheduler/TimerData
struct  TimerData_t4056715490 
{
public:
	// HoloToolkit.Unity.TimerScheduler/Callback HoloToolkit.Unity.TimerScheduler/TimerData::Callback
	Callback_t2663646540 * ___Callback_0;
	// System.Single HoloToolkit.Unity.TimerScheduler/TimerData::Duration
	float ___Duration_1;
	// System.Boolean HoloToolkit.Unity.TimerScheduler/TimerData::Loop
	bool ___Loop_2;
	// System.Int32 HoloToolkit.Unity.TimerScheduler/TimerData::Id
	int32_t ___Id_3;

public:
	inline static int32_t get_offset_of_Callback_0() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Callback_0)); }
	inline Callback_t2663646540 * get_Callback_0() const { return ___Callback_0; }
	inline Callback_t2663646540 ** get_address_of_Callback_0() { return &___Callback_0; }
	inline void set_Callback_0(Callback_t2663646540 * value)
	{
		___Callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___Callback_0), value);
	}

	inline static int32_t get_offset_of_Duration_1() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Duration_1)); }
	inline float get_Duration_1() const { return ___Duration_1; }
	inline float* get_address_of_Duration_1() { return &___Duration_1; }
	inline void set_Duration_1(float value)
	{
		___Duration_1 = value;
	}

	inline static int32_t get_offset_of_Loop_2() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Loop_2)); }
	inline bool get_Loop_2() const { return ___Loop_2; }
	inline bool* get_address_of_Loop_2() { return &___Loop_2; }
	inline void set_Loop_2(bool value)
	{
		___Loop_2 = value;
	}

	inline static int32_t get_offset_of_Id_3() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Id_3)); }
	inline int32_t get_Id_3() const { return ___Id_3; }
	inline int32_t* get_address_of_Id_3() { return &___Id_3; }
	inline void set_Id_3(int32_t value)
	{
		___Id_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.TimerScheduler/TimerData
struct TimerData_t4056715490_marshaled_pinvoke
{
	Il2CppMethodPointer ___Callback_0;
	float ___Duration_1;
	int32_t ___Loop_2;
	int32_t ___Id_3;
};
// Native definition for COM marshalling of HoloToolkit.Unity.TimerScheduler/TimerData
struct TimerData_t4056715490_marshaled_com
{
	Il2CppMethodPointer ___Callback_0;
	float ___Duration_1;
	int32_t ___Loop_2;
	int32_t ___Id_3;
};
#endif // TIMERDATA_T4056715490_H
#ifndef KEYVALUEPAIR_2_T4237331251_H
#define KEYVALUEPAIR_2_T4237331251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
struct  KeyValuePair_2_t4237331251 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4237331251, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4237331251, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T4237331251_H
#ifndef EVENTREGISTRATIONTOKENLIST_T3288506496_H
#define EVENTREGISTRATIONTOKENLIST_T3288506496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList
struct  EventRegistrationTokenList_t3288506496 
{
public:
	// System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList::firstToken
	EventRegistrationToken_t318890788  ___firstToken_0;
	// System.Collections.Generic.List`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken> System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList::restTokens
	List_1_t1790965530 * ___restTokens_1;

public:
	inline static int32_t get_offset_of_firstToken_0() { return static_cast<int32_t>(offsetof(EventRegistrationTokenList_t3288506496, ___firstToken_0)); }
	inline EventRegistrationToken_t318890788  get_firstToken_0() const { return ___firstToken_0; }
	inline EventRegistrationToken_t318890788 * get_address_of_firstToken_0() { return &___firstToken_0; }
	inline void set_firstToken_0(EventRegistrationToken_t318890788  value)
	{
		___firstToken_0 = value;
	}

	inline static int32_t get_offset_of_restTokens_1() { return static_cast<int32_t>(offsetof(EventRegistrationTokenList_t3288506496, ___restTokens_1)); }
	inline List_1_t1790965530 * get_restTokens_1() const { return ___restTokens_1; }
	inline List_1_t1790965530 ** get_address_of_restTokens_1() { return &___restTokens_1; }
	inline void set_restTokens_1(List_1_t1790965530 * value)
	{
		___restTokens_1 = value;
		Il2CppCodeGenWriteBarrier((&___restTokens_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList
struct EventRegistrationTokenList_t3288506496_marshaled_pinvoke
{
	EventRegistrationToken_t318890788  ___firstToken_0;
	List_1_t1790965530 * ___restTokens_1;
};
// Native definition for COM marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList
struct EventRegistrationTokenList_t3288506496_marshaled_com
{
	EventRegistrationToken_t318890788  ___firstToken_0;
	List_1_t1790965530 * ___restTokens_1;
};
#endif // EVENTREGISTRATIONTOKENLIST_T3288506496_H
#ifndef KEYVALUEPAIR_2_T1048133692_H
#define KEYVALUEPAIR_2_T1048133692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>
struct  KeyValuePair_2_t1048133692 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	TimerData_t4056715490  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1048133692, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1048133692, ___value_1)); }
	inline TimerData_t4056715490  get_value_1() const { return ___value_1; }
	inline TimerData_t4056715490 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(TimerData_t4056715490  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1048133692_H
#ifndef KEYVALUEPAIR_2_T3174081962_H
#define KEYVALUEPAIR_2_T3174081962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>
struct  KeyValuePair_2_t3174081962 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	ResourceLocator_t3723970807  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3174081962, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3174081962, ___value_1)); }
	inline ResourceLocator_t3723970807  get_value_1() const { return ___value_1; }
	inline ResourceLocator_t3723970807 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(ResourceLocator_t3723970807  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3174081962_H
#ifndef KEYVALUEPAIR_2_T3699644050_H
#define KEYVALUEPAIR_2_T3699644050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>
struct  KeyValuePair_2_t3699644050 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	intptr_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3699644050, ___key_0)); }
	inline intptr_t get_key_0() const { return ___key_0; }
	inline intptr_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(intptr_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3699644050, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3699644050_H
#ifndef KEYVALUEPAIR_2_T2872605199_H
#define KEYVALUEPAIR_2_T2872605199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>
struct  KeyValuePair_2_t2872605199 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	XPathNodeRef_t3498189018  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	XPathNodeRef_t3498189018  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2872605199, ___key_0)); }
	inline XPathNodeRef_t3498189018  get_key_0() const { return ___key_0; }
	inline XPathNodeRef_t3498189018 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(XPathNodeRef_t3498189018  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2872605199, ___value_1)); }
	inline XPathNodeRef_t3498189018  get_value_1() const { return ___value_1; }
	inline XPathNodeRef_t3498189018 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(XPathNodeRef_t3498189018  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2872605199_H
#ifndef KEYVALUEPAIR_2_T255164838_H
#define KEYVALUEPAIR_2_T255164838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>
struct  KeyValuePair_2_t255164838 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	TypedConstant_t714020897  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t255164838, ___key_0)); }
	inline TypedConstant_t714020897  get_key_0() const { return ___key_0; }
	inline TypedConstant_t714020897 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(TypedConstant_t714020897  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t255164838, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T255164838_H
#ifndef KEYVALUEPAIR_2_T1297193679_H
#define KEYVALUEPAIR_2_T1297193679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>
struct  KeyValuePair_2_t1297193679 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	uint32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	XboxControllerData_t1920221146  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1297193679, ___key_0)); }
	inline uint32_t get_key_0() const { return ___key_0; }
	inline uint32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(uint32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1297193679, ___value_1)); }
	inline XboxControllerData_t1920221146  get_value_1() const { return ___value_1; }
	inline XboxControllerData_t1920221146 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(XboxControllerData_t1920221146  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1297193679_H
#ifndef KEYVALUEPAIR_2_T4253942476_H
#define KEYVALUEPAIR_2_T4253942476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>
struct  KeyValuePair_2_t4253942476 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	EventCacheKey_t3133620722  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	EventCacheEntry_t156445199  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4253942476, ___key_0)); }
	inline EventCacheKey_t3133620722  get_key_0() const { return ___key_0; }
	inline EventCacheKey_t3133620722 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(EventCacheKey_t3133620722  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4253942476, ___value_1)); }
	inline EventCacheEntry_t156445199  get_value_1() const { return ___value_1; }
	inline EventCacheEntry_t156445199 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(EventCacheEntry_t156445199  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T4253942476_H
#ifndef KEYVALUEPAIR_2_T644712158_H
#define KEYVALUEPAIR_2_T644712158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct  KeyValuePair_2_t644712158 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	TypeNameKey_t2985541961  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t644712158, ___key_0)); }
	inline TypeNameKey_t2985541961  get_key_0() const { return ___key_0; }
	inline TypeNameKey_t2985541961 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(TypeNameKey_t2985541961  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t644712158, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T644712158_H
#ifndef KEYVALUEPAIR_2_T3267901400_H
#define KEYVALUEPAIR_2_T3267901400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>
struct  KeyValuePair_2_t3267901400 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	ResolverContractKey_t3292851287  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3267901400, ___key_0)); }
	inline ResolverContractKey_t3292851287  get_key_0() const { return ___key_0; }
	inline ResolverContractKey_t3292851287 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(ResolverContractKey_t3292851287  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3267901400, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3267901400_H
#ifndef PRIMITIVETYPECODE_T798949904_H
#define PRIMITIVETYPECODE_T798949904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PrimitiveTypeCode
struct  PrimitiveTypeCode_t798949904 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PrimitiveTypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PrimitiveTypeCode_t798949904, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPECODE_T798949904_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef READTYPE_T340786580_H
#define READTYPE_T340786580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReadType
struct  ReadType_t340786580 
{
public:
	// System.Int32 Newtonsoft.Json.ReadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReadType_t340786580, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READTYPE_T340786580_H
#ifndef KEYEVENT_T245959883_H
#define KEYEVENT_T245959883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.KeyboardManager/KeyEvent
struct  KeyEvent_t245959883 
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.KeyboardManager/KeyEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyEvent_t245959883, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEVENT_T245959883_H
#ifndef SWITCHVALUESTATE_T2805251467_H
#define SWITCHVALUESTATE_T2805251467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AppContext/SwitchValueState
struct  SwitchValueState_t2805251467 
{
public:
	// System.Int32 System.AppContext/SwitchValueState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SwitchValueState_t2805251467, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCHVALUESTATE_T2805251467_H
#ifndef KEYVALUEPAIR_2_T126004427_H
#define KEYVALUEPAIR_2_T126004427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>
struct  KeyValuePair_2_t126004427 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	TypedConstant_t714020897  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t126004427, ___key_0)); }
	inline TypedConstant_t714020897  get_key_0() const { return ___key_0; }
	inline TypedConstant_t714020897 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(TypedConstant_t714020897  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t126004427, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T126004427_H
#ifndef VARIABLESTORAGEKIND_T3280289589_H
#define VARIABLESTORAGEKIND_T3280289589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Compiler.VariableStorageKind
struct  VariableStorageKind_t3280289589 
{
public:
	// System.Int32 System.Linq.Expressions.Compiler.VariableStorageKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VariableStorageKind_t3280289589, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLESTORAGEKIND_T3280289589_H
#ifndef KEYVALUEPAIR_2_T231828568_H
#define KEYVALUEPAIR_2_T231828568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>
struct  KeyValuePair_2_t231828568 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Guid_t  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t231828568, ___key_0)); }
	inline Guid_t  get_key_0() const { return ___key_0; }
	inline Guid_t * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Guid_t  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t231828568, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T231828568_H
#ifndef KEYVALUEPAIR_2_T753628355_H
#define KEYVALUEPAIR_2_T753628355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct  KeyValuePair_2_t753628355 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	TypeConvertKey_t285306760  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t753628355, ___key_0)); }
	inline TypeConvertKey_t285306760  get_key_0() const { return ___key_0; }
	inline TypeConvertKey_t285306760 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(TypeConvertKey_t285306760  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t753628355, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T753628355_H
#ifndef KEYCODEEVENTPAIR_T1510105498_H
#define KEYCODEEVENTPAIR_T1510105498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair
struct  KeyCodeEventPair_t1510105498 
{
public:
	// UnityEngine.KeyCode HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair::KeyCode
	int32_t ___KeyCode_0;
	// HoloToolkit.Unity.InputModule.KeyboardManager/KeyEvent HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair::KeyEvent
	int32_t ___KeyEvent_1;

public:
	inline static int32_t get_offset_of_KeyCode_0() { return static_cast<int32_t>(offsetof(KeyCodeEventPair_t1510105498, ___KeyCode_0)); }
	inline int32_t get_KeyCode_0() const { return ___KeyCode_0; }
	inline int32_t* get_address_of_KeyCode_0() { return &___KeyCode_0; }
	inline void set_KeyCode_0(int32_t value)
	{
		___KeyCode_0 = value;
	}

	inline static int32_t get_offset_of_KeyEvent_1() { return static_cast<int32_t>(offsetof(KeyCodeEventPair_t1510105498, ___KeyEvent_1)); }
	inline int32_t get_KeyEvent_1() const { return ___KeyEvent_1; }
	inline int32_t* get_address_of_KeyEvent_1() { return &___KeyEvent_1; }
	inline void set_KeyEvent_1(int32_t value)
	{
		___KeyEvent_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODEEVENTPAIR_T1510105498_H
#ifndef KEYVALUEPAIR_2_T249061059_H
#define KEYVALUEPAIR_2_T249061059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct  KeyValuePair_2_t249061059 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t249061059, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t249061059, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T249061059_H
#ifndef KEYVALUEPAIR_2_T4085865031_H
#define KEYVALUEPAIR_2_T4085865031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>
struct  KeyValuePair_2_t4085865031 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4085865031, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4085865031, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T4085865031_H
#ifndef KEYVALUEPAIR_2_T2255362622_H
#define KEYVALUEPAIR_2_T2255362622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>
struct  KeyValuePair_2_t2255362622 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2255362622, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2255362622, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2255362622_H
#ifndef KEYVALUEPAIR_2_T2738617651_H
#define KEYVALUEPAIR_2_T2738617651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>
struct  KeyValuePair_2_t2738617651 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	EventRegistrationTokenList_t3288506496  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2738617651, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2738617651, ___value_1)); }
	inline EventRegistrationTokenList_t3288506496  get_value_1() const { return ___value_1; }
	inline EventRegistrationTokenList_t3288506496 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(EventRegistrationTokenList_t3288506496  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2738617651_H
#ifndef KEYVALUEPAIR_2_T2730400744_H
#define KEYVALUEPAIR_2_T2730400744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>
struct  KeyValuePair_2_t2730400744 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2730400744, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2730400744, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2730400744_H
#ifndef KEYVALUEPAIR_2_T2541612585_H
#define KEYVALUEPAIR_2_T2541612585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>
struct  KeyValuePair_2_t2541612585 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	KeyCodeEventPair_t1510105498  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2541612585, ___key_0)); }
	inline KeyCodeEventPair_t1510105498  get_key_0() const { return ___key_0; }
	inline KeyCodeEventPair_t1510105498 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(KeyCodeEventPair_t1510105498  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2541612585, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2541612585_H


// System.Void System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2475282467_gshared (KeyValuePair_2_t2541612585 * __this, KeyCodeEventPair_t1510105498  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>::get_Key()
extern "C"  KeyCodeEventPair_t1510105498  KeyValuePair_2_get_Key_m2199460899_gshared (KeyValuePair_2_t2541612585 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m2892759683_gshared (KeyValuePair_2_t2541612585 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m875328790_gshared (KeyValuePair_2_t2541612585 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3854325871_gshared (KeyValuePair_2_t2872605199 * __this, XPathNodeRef_t3498189018  ___key0, XPathNodeRef_t3498189018  ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>::get_Key()
extern "C"  XPathNodeRef_t3498189018  KeyValuePair_2_get_Key_m3002864413_gshared (KeyValuePair_2_t2872605199 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>::get_Value()
extern "C"  XPathNodeRef_t3498189018  KeyValuePair_2_get_Value_m3903919478_gshared (KeyValuePair_2_t2872605199 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3784847782_gshared (KeyValuePair_2_t2872605199 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3669030337_gshared (KeyValuePair_2_t644712158 * __this, TypeNameKey_t2985541961  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Key()
extern "C"  TypeNameKey_t2985541961  KeyValuePair_2_get_Key_m3246206588_gshared (KeyValuePair_2_t644712158 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m831194216_gshared (KeyValuePair_2_t644712158 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3475667597_gshared (KeyValuePair_2_t644712158 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m613684380_gshared (KeyValuePair_2_t3267901400 * __this, ResolverContractKey_t3292851287  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Key()
extern "C"  ResolverContractKey_t3292851287  KeyValuePair_2_get_Key_m3927600899_gshared (KeyValuePair_2_t3267901400 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m3308705333_gshared (KeyValuePair_2_t3267901400 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4258372981_gshared (KeyValuePair_2_t3267901400 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2870587947_gshared (KeyValuePair_2_t753628355 * __this, TypeConvertKey_t285306760  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Key()
extern "C"  TypeConvertKey_t285306760  KeyValuePair_2_get_Key_m2096859168_gshared (KeyValuePair_2_t753628355 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m2171951189_gshared (KeyValuePair_2_t753628355 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1728257272_gshared (KeyValuePair_2_t753628355 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1315482284_gshared (KeyValuePair_2_t231828568 * __this, Guid_t  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>::get_Key()
extern "C"  Guid_t  KeyValuePair_2_get_Key_m4294704491_gshared (KeyValuePair_2_t231828568 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m2666064688_gshared (KeyValuePair_2_t231828568 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1167308912_gshared (KeyValuePair_2_t231828568 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m205375688_gshared (KeyValuePair_2_t1048133692 * __this, int32_t ___key0, TimerData_t4056715490  ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1183315111_gshared (KeyValuePair_2_t1048133692 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>::get_Value()
extern "C"  TimerData_t4056715490  KeyValuePair_2_get_Value_m513163858_gshared (KeyValuePair_2_t1048133692 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2584609649_gshared (KeyValuePair_2_t1048133692 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m932279514_gshared (KeyValuePair_2_t4237331251 * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2635782095_gshared (KeyValuePair_2_t4237331251 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1804680087_gshared (KeyValuePair_2_t4237331251 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m65692449_gshared (KeyValuePair_2_t4237331251 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2118224448_gshared (KeyValuePair_2_t71524366 * __this, int32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1839753989_gshared (KeyValuePair_2_t71524366 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m3495598764_gshared (KeyValuePair_2_t71524366 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1238786018_gshared (KeyValuePair_2_t71524366 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2056996813_gshared (KeyValuePair_2_t2245450819 * __this, int64_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Key()
extern "C"  int64_t KeyValuePair_2_get_Key_m635992374_gshared (KeyValuePair_2_t2245450819 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m392474074_gshared (KeyValuePair_2_t2245450819 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3013572783_gshared (KeyValuePair_2_t2245450819 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3953574590_gshared (KeyValuePair_2_t3699644050 * __this, intptr_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Key()
extern "C"  intptr_t KeyValuePair_2_get_Key_m1204087822_gshared (KeyValuePair_2_t3699644050 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1339120122_gshared (KeyValuePair_2_t3699644050 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2983173998_gshared (KeyValuePair_2_t3699644050 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3967312370_gshared (KeyValuePair_2_t126004427 * __this, TypedConstant_t714020897  ___key0, int32_t ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>::get_Key()
extern "C"  TypedConstant_t714020897  KeyValuePair_2_get_Key_m2623710737_gshared (KeyValuePair_2_t126004427 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3418326370_gshared (KeyValuePair_2_t126004427 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1045680960_gshared (KeyValuePair_2_t126004427 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m51719720_gshared (KeyValuePair_2_t255164838 * __this, TypedConstant_t714020897  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>::get_Key()
extern "C"  TypedConstant_t714020897  KeyValuePair_2_get_Key_m3539669632_gshared (KeyValuePair_2_t255164838 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1745831214_gshared (KeyValuePair_2_t255164838 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3201086771_gshared (KeyValuePair_2_t255164838 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1860763818_gshared (KeyValuePair_2_t4085865031 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m2837550314_gshared (KeyValuePair_2_t4085865031 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1430811576_gshared (KeyValuePair_2_t4085865031 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m490808492_gshared (KeyValuePair_2_t4085865031 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m198529365_gshared (KeyValuePair_2_t249061059 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m3093971998_gshared (KeyValuePair_2_t249061059 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m551864407_gshared (KeyValuePair_2_t249061059 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m675279023_gshared (KeyValuePair_2_t249061059 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1927988026_gshared (KeyValuePair_2_t2255362622 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m3745423128_gshared (KeyValuePair_2_t2255362622 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m963734252_gshared (KeyValuePair_2_t2255362622 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1774591758_gshared (KeyValuePair_2_t2255362622 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m23191374_gshared (KeyValuePair_2_t3842366416 * __this, RuntimeObject * ___key0, bool ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m2106922848_gshared (KeyValuePair_2_t3842366416 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m1669764045_gshared (KeyValuePair_2_t3842366416 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2480962023_gshared (KeyValuePair_2_t3842366416 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m880186442_gshared (KeyValuePair_2_t2401056908 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1055012466_gshared (KeyValuePair_2_t2401056908 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1308554439_gshared (KeyValuePair_2_t2401056908 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4231614106_gshared (KeyValuePair_2_t2401056908 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3716993270_gshared (KeyValuePair_2_t2730400744 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m4110334143_gshared (KeyValuePair_2_t2730400744 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3734748706_gshared (KeyValuePair_2_t2730400744 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3021632350_gshared (KeyValuePair_2_t2730400744 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m727165395_gshared (KeyValuePair_2_t2530217319 * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m4184817181_gshared (KeyValuePair_2_t2530217319 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1132502692_gshared (KeyValuePair_2_t2530217319 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m510648957_gshared (KeyValuePair_2_t2530217319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m877231462_gshared (KeyValuePair_2_t3174081962 * __this, RuntimeObject * ___key0, ResourceLocator_t3723970807  ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1914465694_gshared (KeyValuePair_2_t3174081962 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>::get_Value()
extern "C"  ResourceLocator_t3723970807  KeyValuePair_2_get_Value_m1048976747_gshared (KeyValuePair_2_t3174081962 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m225390547_gshared (KeyValuePair_2_t3174081962 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m52543417_gshared (KeyValuePair_2_t2738617651 * __this, RuntimeObject * ___key0, EventRegistrationTokenList_t3288506496  ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m312793161_gshared (KeyValuePair_2_t2738617651 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>::get_Value()
extern "C"  EventRegistrationTokenList_t3288506496  KeyValuePair_2_get_Value_m80964760_gshared (KeyValuePair_2_t2738617651 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m80164506_gshared (KeyValuePair_2_t2738617651 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1054831967_gshared (KeyValuePair_2_t4253942476 * __this, EventCacheKey_t3133620722  ___key0, EventCacheEntry_t156445199  ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>::get_Key()
extern "C"  EventCacheKey_t3133620722  KeyValuePair_2_get_Key_m1029407593_gshared (KeyValuePair_2_t4253942476 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>::get_Value()
extern "C"  EventCacheEntry_t156445199  KeyValuePair_2_get_Value_m497513548_gshared (KeyValuePair_2_t4253942476 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3941865661_gshared (KeyValuePair_2_t4253942476 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2398257575_gshared (KeyValuePair_2_t1297193679 * __this, uint32_t ___key0, XboxControllerData_t1920221146  ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>::get_Key()
extern "C"  uint32_t KeyValuePair_2_get_Key_m784662759_gshared (KeyValuePair_2_t1297193679 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>::get_Value()
extern "C"  XboxControllerData_t1920221146  KeyValuePair_2_get_Value_m3814993531_gshared (KeyValuePair_2_t1297193679 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m601171314_gshared (KeyValuePair_2_t1297193679 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1509212969_gshared (KeyValuePair_2_t2457078697 * __this, uint32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::get_Key()
extern "C"  uint32_t KeyValuePair_2_get_Key_m1662296104_gshared (KeyValuePair_2_t2457078697 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1484005982_gshared (KeyValuePair_2_t2457078697 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m816416158_gshared (KeyValuePair_2_t2457078697 * __this, const RuntimeMethod* method);

// System.Void System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2475282467(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2541612585 *, KeyCodeEventPair_t1510105498 , RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m2475282467_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m2199460899(__this, method) ((  KeyCodeEventPair_t1510105498  (*) (KeyValuePair_2_t2541612585 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m2199460899_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m2892759683(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t2541612585 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m2892759683_gshared)(__this, method)
// System.Text.StringBuilder System.Text.StringBuilderCache::Acquire(System.Int32)
extern "C"  StringBuilder_t1712802186 * StringBuilderCache_Acquire_m4169564332 (RuntimeObject * __this /* static, unused */, int32_t ___capacity0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
extern "C"  StringBuilder_t1712802186 * StringBuilder_Append_m2383614642 (StringBuilder_t1712802186 * __this, Il2CppChar ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C"  StringBuilder_t1712802186 * StringBuilder_Append_m1965104174 (StringBuilder_t1712802186 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.StringBuilderCache::GetStringAndRelease(System.Text.StringBuilder)
extern "C"  String_t* StringBuilderCache_GetStringAndRelease_m1110745745 (RuntimeObject * __this /* static, unused */, StringBuilder_t1712802186 * ___sb0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>::ToString()
#define KeyValuePair_2_ToString_m875328790(__this, method) ((  String_t* (*) (KeyValuePair_2_t2541612585 *, const RuntimeMethod*))KeyValuePair_2_ToString_m875328790_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3854325871(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2872605199 *, XPathNodeRef_t3498189018 , XPathNodeRef_t3498189018 , const RuntimeMethod*))KeyValuePair_2__ctor_m3854325871_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>::get_Key()
#define KeyValuePair_2_get_Key_m3002864413(__this, method) ((  XPathNodeRef_t3498189018  (*) (KeyValuePair_2_t2872605199 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m3002864413_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>::get_Value()
#define KeyValuePair_2_get_Value_m3903919478(__this, method) ((  XPathNodeRef_t3498189018  (*) (KeyValuePair_2_t2872605199 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3903919478_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>::ToString()
#define KeyValuePair_2_ToString_m3784847782(__this, method) ((  String_t* (*) (KeyValuePair_2_t2872605199 *, const RuntimeMethod*))KeyValuePair_2_ToString_m3784847782_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3669030337(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t644712158 *, TypeNameKey_t2985541961 , RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m3669030337_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m3246206588(__this, method) ((  TypeNameKey_t2985541961  (*) (KeyValuePair_2_t644712158 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m3246206588_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m831194216(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t644712158 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m831194216_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::ToString()
#define KeyValuePair_2_ToString_m3475667597(__this, method) ((  String_t* (*) (KeyValuePair_2_t644712158 *, const RuntimeMethod*))KeyValuePair_2_ToString_m3475667597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m613684380(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3267901400 *, ResolverContractKey_t3292851287 , RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m613684380_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m3927600899(__this, method) ((  ResolverContractKey_t3292851287  (*) (KeyValuePair_2_t3267901400 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m3927600899_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m3308705333(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t3267901400 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3308705333_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::ToString()
#define KeyValuePair_2_ToString_m4258372981(__this, method) ((  String_t* (*) (KeyValuePair_2_t3267901400 *, const RuntimeMethod*))KeyValuePair_2_ToString_m4258372981_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2870587947(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t753628355 *, TypeConvertKey_t285306760 , RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m2870587947_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m2096859168(__this, method) ((  TypeConvertKey_t285306760  (*) (KeyValuePair_2_t753628355 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m2096859168_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m2171951189(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t753628355 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m2171951189_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::ToString()
#define KeyValuePair_2_ToString_m1728257272(__this, method) ((  String_t* (*) (KeyValuePair_2_t753628355 *, const RuntimeMethod*))KeyValuePair_2_ToString_m1728257272_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1315482284(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t231828568 *, Guid_t , RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m1315482284_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m4294704491(__this, method) ((  Guid_t  (*) (KeyValuePair_2_t231828568 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m4294704491_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m2666064688(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t231828568 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m2666064688_gshared)(__this, method)
// System.String System.Guid::ToString()
extern "C"  String_t* Guid_ToString_m3279186591 (Guid_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>::ToString()
#define KeyValuePair_2_ToString_m1167308912(__this, method) ((  String_t* (*) (KeyValuePair_2_t231828568 *, const RuntimeMethod*))KeyValuePair_2_ToString_m1167308912_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m205375688(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1048133692 *, int32_t, TimerData_t4056715490 , const RuntimeMethod*))KeyValuePair_2__ctor_m205375688_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>::get_Key()
#define KeyValuePair_2_get_Key_m1183315111(__this, method) ((  int32_t (*) (KeyValuePair_2_t1048133692 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1183315111_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>::get_Value()
#define KeyValuePair_2_get_Value_m513163858(__this, method) ((  TimerData_t4056715490  (*) (KeyValuePair_2_t1048133692 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m513163858_gshared)(__this, method)
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m141394615 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>::ToString()
#define KeyValuePair_2_ToString_m2584609649(__this, method) ((  String_t* (*) (KeyValuePair_2_t1048133692 *, const RuntimeMethod*))KeyValuePair_2_ToString_m2584609649_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m932279514(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4237331251 *, int32_t, int32_t, const RuntimeMethod*))KeyValuePair_2__ctor_m932279514_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m2635782095(__this, method) ((  int32_t (*) (KeyValuePair_2_t4237331251 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m2635782095_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m1804680087(__this, method) ((  int32_t (*) (KeyValuePair_2_t4237331251 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1804680087_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m65692449(__this, method) ((  String_t* (*) (KeyValuePair_2_t4237331251 *, const RuntimeMethod*))KeyValuePair_2_ToString_m65692449_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2118224448(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t71524366 *, int32_t, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m2118224448_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m1839753989(__this, method) ((  int32_t (*) (KeyValuePair_2_t71524366 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1839753989_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m3495598764(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t71524366 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3495598764_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
#define KeyValuePair_2_ToString_m1238786018(__this, method) ((  String_t* (*) (KeyValuePair_2_t71524366 *, const RuntimeMethod*))KeyValuePair_2_ToString_m1238786018_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2056996813(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2245450819 *, int64_t, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m2056996813_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m635992374(__this, method) ((  int64_t (*) (KeyValuePair_2_t2245450819 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m635992374_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m392474074(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t2245450819 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m392474074_gshared)(__this, method)
// System.String System.Int64::ToString()
extern "C"  String_t* Int64_ToString_m2986581816 (int64_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::ToString()
#define KeyValuePair_2_ToString_m3013572783(__this, method) ((  String_t* (*) (KeyValuePair_2_t2245450819 *, const RuntimeMethod*))KeyValuePair_2_ToString_m3013572783_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3953574590(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3699644050 *, intptr_t, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m3953574590_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m1204087822(__this, method) ((  intptr_t (*) (KeyValuePair_2_t3699644050 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1204087822_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m1339120122(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t3699644050 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1339120122_gshared)(__this, method)
// System.String System.IntPtr::ToString()
extern "C"  String_t* IntPtr_ToString_m1831665121 (intptr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::ToString()
#define KeyValuePair_2_ToString_m2983173998(__this, method) ((  String_t* (*) (KeyValuePair_2_t3699644050 *, const RuntimeMethod*))KeyValuePair_2_ToString_m2983173998_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3967312370(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t126004427 *, TypedConstant_t714020897 , int32_t, const RuntimeMethod*))KeyValuePair_2__ctor_m3967312370_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m2623710737(__this, method) ((  TypedConstant_t714020897  (*) (KeyValuePair_2_t126004427 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m2623710737_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m3418326370(__this, method) ((  int32_t (*) (KeyValuePair_2_t126004427 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3418326370_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m1045680960(__this, method) ((  String_t* (*) (KeyValuePair_2_t126004427 *, const RuntimeMethod*))KeyValuePair_2_ToString_m1045680960_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m51719720(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t255164838 *, TypedConstant_t714020897 , RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m51719720_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m3539669632(__this, method) ((  TypedConstant_t714020897  (*) (KeyValuePair_2_t255164838 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m3539669632_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m1745831214(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t255164838 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1745831214_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>::ToString()
#define KeyValuePair_2_ToString_m3201086771(__this, method) ((  String_t* (*) (KeyValuePair_2_t255164838 *, const RuntimeMethod*))KeyValuePair_2_ToString_m3201086771_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1860763818(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4085865031 *, RuntimeObject *, int32_t, const RuntimeMethod*))KeyValuePair_2__ctor_m1860763818_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::get_Key()
#define KeyValuePair_2_get_Key_m2837550314(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t4085865031 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m2837550314_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::get_Value()
#define KeyValuePair_2_get_Value_m1430811576(__this, method) ((  int32_t (*) (KeyValuePair_2_t4085865031 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1430811576_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::ToString()
#define KeyValuePair_2_ToString_m490808492(__this, method) ((  String_t* (*) (KeyValuePair_2_t4085865031 *, const RuntimeMethod*))KeyValuePair_2_ToString_m490808492_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m198529365(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t249061059 *, RuntimeObject *, int32_t, const RuntimeMethod*))KeyValuePair_2__ctor_m198529365_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Key()
#define KeyValuePair_2_get_Key_m3093971998(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t249061059 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m3093971998_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Value()
#define KeyValuePair_2_get_Value_m551864407(__this, method) ((  int32_t (*) (KeyValuePair_2_t249061059 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m551864407_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::ToString()
#define KeyValuePair_2_ToString_m675279023(__this, method) ((  String_t* (*) (KeyValuePair_2_t249061059 *, const RuntimeMethod*))KeyValuePair_2_ToString_m675279023_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1927988026(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2255362622 *, RuntimeObject *, int32_t, const RuntimeMethod*))KeyValuePair_2__ctor_m1927988026_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>::get_Key()
#define KeyValuePair_2_get_Key_m3745423128(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t2255362622 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m3745423128_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>::get_Value()
#define KeyValuePair_2_get_Value_m963734252(__this, method) ((  int32_t (*) (KeyValuePair_2_t2255362622 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m963734252_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>::ToString()
#define KeyValuePair_2_ToString_m1774591758(__this, method) ((  String_t* (*) (KeyValuePair_2_t2255362622 *, const RuntimeMethod*))KeyValuePair_2_ToString_m1774591758_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m23191374(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3842366416 *, RuntimeObject *, bool, const RuntimeMethod*))KeyValuePair_2__ctor_m23191374_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m2106922848(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t3842366416 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m2106922848_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m1669764045(__this, method) ((  bool (*) (KeyValuePair_2_t3842366416 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1669764045_gshared)(__this, method)
// System.String System.Boolean::ToString()
extern "C"  String_t* Boolean_ToString_m2664721875 (bool* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m2480962023(__this, method) ((  String_t* (*) (KeyValuePair_2_t3842366416 *, const RuntimeMethod*))KeyValuePair_2_ToString_m2480962023_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m880186442(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2401056908 *, RuntimeObject *, int32_t, const RuntimeMethod*))KeyValuePair_2__ctor_m880186442_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m1055012466(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t2401056908 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1055012466_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m1308554439(__this, method) ((  int32_t (*) (KeyValuePair_2_t2401056908 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1308554439_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m4231614106(__this, method) ((  String_t* (*) (KeyValuePair_2_t2401056908 *, const RuntimeMethod*))KeyValuePair_2_ToString_m4231614106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3716993270(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2730400744 *, RuntimeObject *, int32_t, const RuntimeMethod*))KeyValuePair_2__ctor_m3716993270_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>::get_Key()
#define KeyValuePair_2_get_Key_m4110334143(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t2730400744 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m4110334143_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>::get_Value()
#define KeyValuePair_2_get_Value_m3734748706(__this, method) ((  int32_t (*) (KeyValuePair_2_t2730400744 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3734748706_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>::ToString()
#define KeyValuePair_2_ToString_m3021632350(__this, method) ((  String_t* (*) (KeyValuePair_2_t2730400744 *, const RuntimeMethod*))KeyValuePair_2_ToString_m3021632350_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m727165395(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2530217319 *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m727165395_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m4184817181(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t2530217319 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m4184817181_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m1132502692(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t2530217319 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1132502692_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
#define KeyValuePair_2_ToString_m510648957(__this, method) ((  String_t* (*) (KeyValuePair_2_t2530217319 *, const RuntimeMethod*))KeyValuePair_2_ToString_m510648957_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m877231462(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3174081962 *, RuntimeObject *, ResourceLocator_t3723970807 , const RuntimeMethod*))KeyValuePair_2__ctor_m877231462_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>::get_Key()
#define KeyValuePair_2_get_Key_m1914465694(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t3174081962 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1914465694_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>::get_Value()
#define KeyValuePair_2_get_Value_m1048976747(__this, method) ((  ResourceLocator_t3723970807  (*) (KeyValuePair_2_t3174081962 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1048976747_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>::ToString()
#define KeyValuePair_2_ToString_m225390547(__this, method) ((  String_t* (*) (KeyValuePair_2_t3174081962 *, const RuntimeMethod*))KeyValuePair_2_ToString_m225390547_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m52543417(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2738617651 *, RuntimeObject *, EventRegistrationTokenList_t3288506496 , const RuntimeMethod*))KeyValuePair_2__ctor_m52543417_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>::get_Key()
#define KeyValuePair_2_get_Key_m312793161(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t2738617651 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m312793161_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>::get_Value()
#define KeyValuePair_2_get_Value_m80964760(__this, method) ((  EventRegistrationTokenList_t3288506496  (*) (KeyValuePair_2_t2738617651 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m80964760_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>::ToString()
#define KeyValuePair_2_ToString_m80164506(__this, method) ((  String_t* (*) (KeyValuePair_2_t2738617651 *, const RuntimeMethod*))KeyValuePair_2_ToString_m80164506_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1054831967(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4253942476 *, EventCacheKey_t3133620722 , EventCacheEntry_t156445199 , const RuntimeMethod*))KeyValuePair_2__ctor_m1054831967_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>::get_Key()
#define KeyValuePair_2_get_Key_m1029407593(__this, method) ((  EventCacheKey_t3133620722  (*) (KeyValuePair_2_t4253942476 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1029407593_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>::get_Value()
#define KeyValuePair_2_get_Value_m497513548(__this, method) ((  EventCacheEntry_t156445199  (*) (KeyValuePair_2_t4253942476 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m497513548_gshared)(__this, method)
// System.String System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey::ToString()
extern "C"  String_t* EventCacheKey_ToString_m875627681 (EventCacheKey_t3133620722 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>::ToString()
#define KeyValuePair_2_ToString_m3941865661(__this, method) ((  String_t* (*) (KeyValuePair_2_t4253942476 *, const RuntimeMethod*))KeyValuePair_2_ToString_m3941865661_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2398257575(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1297193679 *, uint32_t, XboxControllerData_t1920221146 , const RuntimeMethod*))KeyValuePair_2__ctor_m2398257575_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>::get_Key()
#define KeyValuePair_2_get_Key_m784662759(__this, method) ((  uint32_t (*) (KeyValuePair_2_t1297193679 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m784662759_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>::get_Value()
#define KeyValuePair_2_get_Value_m3814993531(__this, method) ((  XboxControllerData_t1920221146  (*) (KeyValuePair_2_t1297193679 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3814993531_gshared)(__this, method)
// System.String System.UInt32::ToString()
extern "C"  String_t* UInt32_ToString_m2574561716 (uint32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>::ToString()
#define KeyValuePair_2_ToString_m601171314(__this, method) ((  String_t* (*) (KeyValuePair_2_t1297193679 *, const RuntimeMethod*))KeyValuePair_2_ToString_m601171314_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1509212969(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2457078697 *, uint32_t, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m1509212969_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m1662296104(__this, method) ((  uint32_t (*) (KeyValuePair_2_t2457078697 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1662296104_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m1484005982(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t2457078697 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1484005982_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::ToString()
#define KeyValuePair_2_ToString_m816416158(__this, method) ((  String_t* (*) (KeyValuePair_2_t2457078697 *, const RuntimeMethod*))KeyValuePair_2_ToString_m816416158_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2475282467_gshared (KeyValuePair_2_t2541612585 * __this, KeyCodeEventPair_t1510105498  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		KeyCodeEventPair_t1510105498  L_0 = ___key0;
		__this->set_key_0(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2475282467_AdjustorThunk (RuntimeObject * __this, KeyCodeEventPair_t1510105498  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t2541612585 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2541612585 *>(__this + 1);
	KeyValuePair_2__ctor_m2475282467(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>::get_Key()
extern "C"  KeyCodeEventPair_t1510105498  KeyValuePair_2_get_Key_m2199460899_gshared (KeyValuePair_2_t2541612585 * __this, const RuntimeMethod* method)
{
	{
		KeyCodeEventPair_t1510105498  L_0 = (KeyCodeEventPair_t1510105498 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  KeyCodeEventPair_t1510105498  KeyValuePair_2_get_Key_m2199460899_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2541612585 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2541612585 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2199460899(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m2892759683_gshared (KeyValuePair_2_t2541612585 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m2892759683_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2541612585 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2541612585 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2892759683(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m875328790_gshared (KeyValuePair_2_t2541612585 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m875328790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	KeyCodeEventPair_t1510105498  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RuntimeObject * V_2 = NULL;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		KeyCodeEventPair_t1510105498  L_2 = KeyValuePair_2_get_Key_m2199460899((KeyValuePair_2_t2541612585 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		KeyCodeEventPair_t1510105498  L_4 = KeyValuePair_2_get_Key_m2199460899((KeyValuePair_2_t2541612585 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (KeyCodeEventPair_t1510105498 )L_4;
		RuntimeObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (&V_1));
		NullCheck((RuntimeObject *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_5);
		V_1 = *(KeyCodeEventPair_t1510105498 *)UnBox(L_5);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_6, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_7 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_7);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_7, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		RuntimeObject * L_8 = KeyValuePair_2_get_Value_m2892759683((KeyValuePair_2_t2541612585 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_8)
		{
			goto IL_006d;
		}
	}
	{
		StringBuilder_t1712802186 * L_9 = V_0;
		RuntimeObject * L_10 = KeyValuePair_2_get_Value_m2892759683((KeyValuePair_2_t2541612585 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (RuntimeObject *)L_10;
		NullCheck((RuntimeObject *)(V_2));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_2));
		NullCheck((StringBuilder_t1712802186 *)L_9);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_9, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_12);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_12, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		String_t* L_14 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m875328790_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2541612585 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2541612585 *>(__this + 1);
	return KeyValuePair_2_ToString_m875328790(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3854325871_gshared (KeyValuePair_2_t2872605199 * __this, XPathNodeRef_t3498189018  ___key0, XPathNodeRef_t3498189018  ___value1, const RuntimeMethod* method)
{
	{
		XPathNodeRef_t3498189018  L_0 = ___key0;
		__this->set_key_0(L_0);
		XPathNodeRef_t3498189018  L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3854325871_AdjustorThunk (RuntimeObject * __this, XPathNodeRef_t3498189018  ___key0, XPathNodeRef_t3498189018  ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t2872605199 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2872605199 *>(__this + 1);
	KeyValuePair_2__ctor_m3854325871(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>::get_Key()
extern "C"  XPathNodeRef_t3498189018  KeyValuePair_2_get_Key_m3002864413_gshared (KeyValuePair_2_t2872605199 * __this, const RuntimeMethod* method)
{
	{
		XPathNodeRef_t3498189018  L_0 = (XPathNodeRef_t3498189018 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  XPathNodeRef_t3498189018  KeyValuePair_2_get_Key_m3002864413_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2872605199 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2872605199 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3002864413(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>::get_Value()
extern "C"  XPathNodeRef_t3498189018  KeyValuePair_2_get_Value_m3903919478_gshared (KeyValuePair_2_t2872605199 * __this, const RuntimeMethod* method)
{
	{
		XPathNodeRef_t3498189018  L_0 = (XPathNodeRef_t3498189018 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  XPathNodeRef_t3498189018  KeyValuePair_2_get_Value_m3903919478_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2872605199 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2872605199 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3903919478(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3784847782_gshared (KeyValuePair_2_t2872605199 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3784847782_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	XPathNodeRef_t3498189018  V_1;
	memset(&V_1, 0, sizeof(V_1));
	XPathNodeRef_t3498189018  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		XPathNodeRef_t3498189018  L_2 = KeyValuePair_2_get_Key_m3002864413((KeyValuePair_2_t2872605199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		XPathNodeRef_t3498189018  L_4 = KeyValuePair_2_get_Key_m3002864413((KeyValuePair_2_t2872605199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (XPathNodeRef_t3498189018 )L_4;
		RuntimeObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (&V_1));
		NullCheck((RuntimeObject *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_5);
		V_1 = *(XPathNodeRef_t3498189018 *)UnBox(L_5);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_6, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_7 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_7);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_7, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		XPathNodeRef_t3498189018  L_8 = KeyValuePair_2_get_Value_m3903919478((KeyValuePair_2_t2872605199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		StringBuilder_t1712802186 * L_9 = V_0;
		XPathNodeRef_t3498189018  L_10 = KeyValuePair_2_get_Value_m3903919478((KeyValuePair_2_t2872605199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (XPathNodeRef_t3498189018 )L_10;
		RuntimeObject * L_11 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_2));
		NullCheck((RuntimeObject *)L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_11);
		V_2 = *(XPathNodeRef_t3498189018 *)UnBox(L_11);
		NullCheck((StringBuilder_t1712802186 *)L_9);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_9, (String_t*)L_12, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_13 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_13);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_13, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_14 = V_0;
		String_t* L_15 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3784847782_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2872605199 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2872605199 *>(__this + 1);
	return KeyValuePair_2_ToString_m3784847782(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3669030337_gshared (KeyValuePair_2_t644712158 * __this, TypeNameKey_t2985541961  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		TypeNameKey_t2985541961  L_0 = ___key0;
		__this->set_key_0(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3669030337_AdjustorThunk (RuntimeObject * __this, TypeNameKey_t2985541961  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t644712158 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t644712158 *>(__this + 1);
	KeyValuePair_2__ctor_m3669030337(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Key()
extern "C"  TypeNameKey_t2985541961  KeyValuePair_2_get_Key_m3246206588_gshared (KeyValuePair_2_t644712158 * __this, const RuntimeMethod* method)
{
	{
		TypeNameKey_t2985541961  L_0 = (TypeNameKey_t2985541961 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  TypeNameKey_t2985541961  KeyValuePair_2_get_Key_m3246206588_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t644712158 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t644712158 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3246206588(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m831194216_gshared (KeyValuePair_2_t644712158 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m831194216_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t644712158 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t644712158 *>(__this + 1);
	return KeyValuePair_2_get_Value_m831194216(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3475667597_gshared (KeyValuePair_2_t644712158 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3475667597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	TypeNameKey_t2985541961  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RuntimeObject * V_2 = NULL;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		TypeNameKey_t2985541961  L_2 = KeyValuePair_2_get_Key_m3246206588((KeyValuePair_2_t644712158 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		TypeNameKey_t2985541961  L_4 = KeyValuePair_2_get_Key_m3246206588((KeyValuePair_2_t644712158 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (TypeNameKey_t2985541961 )L_4;
		RuntimeObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (&V_1));
		NullCheck((RuntimeObject *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_5);
		V_1 = *(TypeNameKey_t2985541961 *)UnBox(L_5);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_6, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_7 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_7);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_7, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		RuntimeObject * L_8 = KeyValuePair_2_get_Value_m831194216((KeyValuePair_2_t644712158 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_8)
		{
			goto IL_006d;
		}
	}
	{
		StringBuilder_t1712802186 * L_9 = V_0;
		RuntimeObject * L_10 = KeyValuePair_2_get_Value_m831194216((KeyValuePair_2_t644712158 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (RuntimeObject *)L_10;
		NullCheck((RuntimeObject *)(V_2));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_2));
		NullCheck((StringBuilder_t1712802186 *)L_9);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_9, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_12);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_12, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		String_t* L_14 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3475667597_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t644712158 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t644712158 *>(__this + 1);
	return KeyValuePair_2_ToString_m3475667597(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m613684380_gshared (KeyValuePair_2_t3267901400 * __this, ResolverContractKey_t3292851287  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		ResolverContractKey_t3292851287  L_0 = ___key0;
		__this->set_key_0(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m613684380_AdjustorThunk (RuntimeObject * __this, ResolverContractKey_t3292851287  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t3267901400 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3267901400 *>(__this + 1);
	KeyValuePair_2__ctor_m613684380(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Key()
extern "C"  ResolverContractKey_t3292851287  KeyValuePair_2_get_Key_m3927600899_gshared (KeyValuePair_2_t3267901400 * __this, const RuntimeMethod* method)
{
	{
		ResolverContractKey_t3292851287  L_0 = (ResolverContractKey_t3292851287 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  ResolverContractKey_t3292851287  KeyValuePair_2_get_Key_m3927600899_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3267901400 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3267901400 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3927600899(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m3308705333_gshared (KeyValuePair_2_t3267901400 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m3308705333_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3267901400 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3267901400 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3308705333(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4258372981_gshared (KeyValuePair_2_t3267901400 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m4258372981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	ResolverContractKey_t3292851287  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RuntimeObject * V_2 = NULL;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		ResolverContractKey_t3292851287  L_2 = KeyValuePair_2_get_Key_m3927600899((KeyValuePair_2_t3267901400 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		ResolverContractKey_t3292851287  L_4 = KeyValuePair_2_get_Key_m3927600899((KeyValuePair_2_t3267901400 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (ResolverContractKey_t3292851287 )L_4;
		RuntimeObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (&V_1));
		NullCheck((RuntimeObject *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_5);
		V_1 = *(ResolverContractKey_t3292851287 *)UnBox(L_5);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_6, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_7 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_7);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_7, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		RuntimeObject * L_8 = KeyValuePair_2_get_Value_m3308705333((KeyValuePair_2_t3267901400 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_8)
		{
			goto IL_006d;
		}
	}
	{
		StringBuilder_t1712802186 * L_9 = V_0;
		RuntimeObject * L_10 = KeyValuePair_2_get_Value_m3308705333((KeyValuePair_2_t3267901400 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (RuntimeObject *)L_10;
		NullCheck((RuntimeObject *)(V_2));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_2));
		NullCheck((StringBuilder_t1712802186 *)L_9);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_9, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_12);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_12, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		String_t* L_14 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m4258372981_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3267901400 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3267901400 *>(__this + 1);
	return KeyValuePair_2_ToString_m4258372981(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2870587947_gshared (KeyValuePair_2_t753628355 * __this, TypeConvertKey_t285306760  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		TypeConvertKey_t285306760  L_0 = ___key0;
		__this->set_key_0(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2870587947_AdjustorThunk (RuntimeObject * __this, TypeConvertKey_t285306760  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t753628355 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t753628355 *>(__this + 1);
	KeyValuePair_2__ctor_m2870587947(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Key()
extern "C"  TypeConvertKey_t285306760  KeyValuePair_2_get_Key_m2096859168_gshared (KeyValuePair_2_t753628355 * __this, const RuntimeMethod* method)
{
	{
		TypeConvertKey_t285306760  L_0 = (TypeConvertKey_t285306760 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  TypeConvertKey_t285306760  KeyValuePair_2_get_Key_m2096859168_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t753628355 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t753628355 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2096859168(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m2171951189_gshared (KeyValuePair_2_t753628355 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m2171951189_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t753628355 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t753628355 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2171951189(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1728257272_gshared (KeyValuePair_2_t753628355 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1728257272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	TypeConvertKey_t285306760  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RuntimeObject * V_2 = NULL;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		TypeConvertKey_t285306760  L_2 = KeyValuePair_2_get_Key_m2096859168((KeyValuePair_2_t753628355 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		TypeConvertKey_t285306760  L_4 = KeyValuePair_2_get_Key_m2096859168((KeyValuePair_2_t753628355 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (TypeConvertKey_t285306760 )L_4;
		RuntimeObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (&V_1));
		NullCheck((RuntimeObject *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_5);
		V_1 = *(TypeConvertKey_t285306760 *)UnBox(L_5);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_6, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_7 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_7);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_7, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		RuntimeObject * L_8 = KeyValuePair_2_get_Value_m2171951189((KeyValuePair_2_t753628355 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_8)
		{
			goto IL_006d;
		}
	}
	{
		StringBuilder_t1712802186 * L_9 = V_0;
		RuntimeObject * L_10 = KeyValuePair_2_get_Value_m2171951189((KeyValuePair_2_t753628355 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (RuntimeObject *)L_10;
		NullCheck((RuntimeObject *)(V_2));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_2));
		NullCheck((StringBuilder_t1712802186 *)L_9);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_9, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_12);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_12, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		String_t* L_14 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1728257272_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t753628355 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t753628355 *>(__this + 1);
	return KeyValuePair_2_ToString_m1728257272(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1315482284_gshared (KeyValuePair_2_t231828568 * __this, Guid_t  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		Guid_t  L_0 = ___key0;
		__this->set_key_0(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1315482284_AdjustorThunk (RuntimeObject * __this, Guid_t  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t231828568 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t231828568 *>(__this + 1);
	KeyValuePair_2__ctor_m1315482284(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>::get_Key()
extern "C"  Guid_t  KeyValuePair_2_get_Key_m4294704491_gshared (KeyValuePair_2_t231828568 * __this, const RuntimeMethod* method)
{
	{
		Guid_t  L_0 = (Guid_t )__this->get_key_0();
		return L_0;
	}
}
extern "C"  Guid_t  KeyValuePair_2_get_Key_m4294704491_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t231828568 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t231828568 *>(__this + 1);
	return KeyValuePair_2_get_Key_m4294704491(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m2666064688_gshared (KeyValuePair_2_t231828568 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m2666064688_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t231828568 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t231828568 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2666064688(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1167308912_gshared (KeyValuePair_2_t231828568 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1167308912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	Guid_t  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RuntimeObject * V_2 = NULL;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		Guid_t  L_2 = KeyValuePair_2_get_Key_m4294704491((KeyValuePair_2_t231828568 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		Guid_t  L_4 = KeyValuePair_2_get_Key_m4294704491((KeyValuePair_2_t231828568 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (Guid_t )L_4;
		String_t* L_5 = Guid_ToString_m3279186591((Guid_t *)(&V_1), /*hidden argument*/NULL);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		RuntimeObject * L_7 = KeyValuePair_2_get_Value_m2666064688((KeyValuePair_2_t231828568 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		RuntimeObject * L_9 = KeyValuePair_2_get_Value_m2666064688((KeyValuePair_2_t231828568 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (RuntimeObject *)L_9;
		NullCheck((RuntimeObject *)(V_2));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_2));
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_10, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_11 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_11);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_11, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_12 = V_0;
		String_t* L_13 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1167308912_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t231828568 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t231828568 *>(__this + 1);
	return KeyValuePair_2_ToString_m1167308912(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m205375688_gshared (KeyValuePair_2_t1048133692 * __this, int32_t ___key0, TimerData_t4056715490  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___key0;
		__this->set_key_0(L_0);
		TimerData_t4056715490  L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m205375688_AdjustorThunk (RuntimeObject * __this, int32_t ___key0, TimerData_t4056715490  ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t1048133692 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1048133692 *>(__this + 1);
	KeyValuePair_2__ctor_m205375688(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1183315111_gshared (KeyValuePair_2_t1048133692 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1183315111_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t1048133692 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1048133692 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1183315111(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>::get_Value()
extern "C"  TimerData_t4056715490  KeyValuePair_2_get_Value_m513163858_gshared (KeyValuePair_2_t1048133692 * __this, const RuntimeMethod* method)
{
	{
		TimerData_t4056715490  L_0 = (TimerData_t4056715490 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  TimerData_t4056715490  KeyValuePair_2_get_Value_m513163858_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t1048133692 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1048133692 *>(__this + 1);
	return KeyValuePair_2_get_Value_m513163858(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2584609649_gshared (KeyValuePair_2_t1048133692 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2584609649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	int32_t V_1 = 0;
	TimerData_t4056715490  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		int32_t L_2 = KeyValuePair_2_get_Key_m1183315111((KeyValuePair_2_t1048133692 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		int32_t L_4 = KeyValuePair_2_get_Key_m1183315111((KeyValuePair_2_t1048133692 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (int32_t)L_4;
		String_t* L_5 = Int32_ToString_m141394615((int32_t*)(&V_1), /*hidden argument*/NULL);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		TimerData_t4056715490  L_7 = KeyValuePair_2_get_Value_m513163858((KeyValuePair_2_t1048133692 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		TimerData_t4056715490  L_9 = KeyValuePair_2_get_Value_m513163858((KeyValuePair_2_t1048133692 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (TimerData_t4056715490 )L_9;
		RuntimeObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_2));
		NullCheck((RuntimeObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_10);
		V_2 = *(TimerData_t4056715490 *)UnBox(L_10);
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_12);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_12, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		String_t* L_14 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2584609649_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t1048133692 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1048133692 *>(__this + 1);
	return KeyValuePair_2_ToString_m2584609649(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m932279514_gshared (KeyValuePair_2_t4237331251 * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___key0;
		__this->set_key_0(L_0);
		int32_t L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m932279514_AdjustorThunk (RuntimeObject * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t4237331251 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4237331251 *>(__this + 1);
	KeyValuePair_2__ctor_m932279514(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2635782095_gshared (KeyValuePair_2_t4237331251 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m2635782095_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t4237331251 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4237331251 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2635782095(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1804680087_gshared (KeyValuePair_2_t4237331251 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m1804680087_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t4237331251 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4237331251 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1804680087(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m65692449_gshared (KeyValuePair_2_t4237331251 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m65692449_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		int32_t L_2 = KeyValuePair_2_get_Key_m2635782095((KeyValuePair_2_t4237331251 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		int32_t L_4 = KeyValuePair_2_get_Key_m2635782095((KeyValuePair_2_t4237331251 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (int32_t)L_4;
		String_t* L_5 = Int32_ToString_m141394615((int32_t*)(&V_1), /*hidden argument*/NULL);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		int32_t L_7 = KeyValuePair_2_get_Value_m1804680087((KeyValuePair_2_t4237331251 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		int32_t L_9 = KeyValuePair_2_get_Value_m1804680087((KeyValuePair_2_t4237331251 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m141394615((int32_t*)(&V_2), /*hidden argument*/NULL);
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_10, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_11 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_11);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_11, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_12 = V_0;
		String_t* L_13 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m65692449_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t4237331251 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4237331251 *>(__this + 1);
	return KeyValuePair_2_ToString_m65692449(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2118224448_gshared (KeyValuePair_2_t71524366 * __this, int32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___key0;
		__this->set_key_0(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2118224448_AdjustorThunk (RuntimeObject * __this, int32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t71524366 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t71524366 *>(__this + 1);
	KeyValuePair_2__ctor_m2118224448(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1839753989_gshared (KeyValuePair_2_t71524366 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1839753989_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t71524366 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t71524366 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1839753989(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m3495598764_gshared (KeyValuePair_2_t71524366 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m3495598764_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t71524366 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t71524366 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3495598764(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1238786018_gshared (KeyValuePair_2_t71524366 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1238786018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	int32_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		int32_t L_2 = KeyValuePair_2_get_Key_m1839753989((KeyValuePair_2_t71524366 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		int32_t L_4 = KeyValuePair_2_get_Key_m1839753989((KeyValuePair_2_t71524366 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (int32_t)L_4;
		String_t* L_5 = Int32_ToString_m141394615((int32_t*)(&V_1), /*hidden argument*/NULL);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		RuntimeObject * L_7 = KeyValuePair_2_get_Value_m3495598764((KeyValuePair_2_t71524366 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		RuntimeObject * L_9 = KeyValuePair_2_get_Value_m3495598764((KeyValuePair_2_t71524366 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (RuntimeObject *)L_9;
		NullCheck((RuntimeObject *)(V_2));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_2));
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_10, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_11 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_11);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_11, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_12 = V_0;
		String_t* L_13 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1238786018_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t71524366 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t71524366 *>(__this + 1);
	return KeyValuePair_2_ToString_m1238786018(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2056996813_gshared (KeyValuePair_2_t2245450819 * __this, int64_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		int64_t L_0 = ___key0;
		__this->set_key_0(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2056996813_AdjustorThunk (RuntimeObject * __this, int64_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t2245450819 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2245450819 *>(__this + 1);
	KeyValuePair_2__ctor_m2056996813(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Key()
extern "C"  int64_t KeyValuePair_2_get_Key_m635992374_gshared (KeyValuePair_2_t2245450819 * __this, const RuntimeMethod* method)
{
	{
		int64_t L_0 = (int64_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int64_t KeyValuePair_2_get_Key_m635992374_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2245450819 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2245450819 *>(__this + 1);
	return KeyValuePair_2_get_Key_m635992374(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m392474074_gshared (KeyValuePair_2_t2245450819 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m392474074_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2245450819 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2245450819 *>(__this + 1);
	return KeyValuePair_2_get_Value_m392474074(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3013572783_gshared (KeyValuePair_2_t2245450819 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3013572783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	int64_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		int64_t L_2 = KeyValuePair_2_get_Key_m635992374((KeyValuePair_2_t2245450819 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		int64_t L_4 = KeyValuePair_2_get_Key_m635992374((KeyValuePair_2_t2245450819 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (int64_t)L_4;
		String_t* L_5 = Int64_ToString_m2986581816((int64_t*)(&V_1), /*hidden argument*/NULL);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		RuntimeObject * L_7 = KeyValuePair_2_get_Value_m392474074((KeyValuePair_2_t2245450819 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		RuntimeObject * L_9 = KeyValuePair_2_get_Value_m392474074((KeyValuePair_2_t2245450819 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (RuntimeObject *)L_9;
		NullCheck((RuntimeObject *)(V_2));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_2));
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_10, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_11 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_11);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_11, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_12 = V_0;
		String_t* L_13 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3013572783_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2245450819 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2245450819 *>(__this + 1);
	return KeyValuePair_2_ToString_m3013572783(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3953574590_gshared (KeyValuePair_2_t3699644050 * __this, intptr_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___key0;
		__this->set_key_0(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3953574590_AdjustorThunk (RuntimeObject * __this, intptr_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t3699644050 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3699644050 *>(__this + 1);
	KeyValuePair_2__ctor_m3953574590(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Key()
extern "C"  intptr_t KeyValuePair_2_get_Key_m1204087822_gshared (KeyValuePair_2_t3699644050 * __this, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = (intptr_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  intptr_t KeyValuePair_2_get_Key_m1204087822_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3699644050 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3699644050 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1204087822(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1339120122_gshared (KeyValuePair_2_t3699644050 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1339120122_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3699644050 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3699644050 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1339120122(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2983173998_gshared (KeyValuePair_2_t3699644050 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2983173998_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	intptr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	RuntimeObject * V_2 = NULL;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		intptr_t L_2 = KeyValuePair_2_get_Key_m1204087822((KeyValuePair_2_t3699644050 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		intptr_t L_4 = KeyValuePair_2_get_Key_m1204087822((KeyValuePair_2_t3699644050 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (intptr_t)L_4;
		String_t* L_5 = IntPtr_ToString_m1831665121((intptr_t*)(&V_1), /*hidden argument*/NULL);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		RuntimeObject * L_7 = KeyValuePair_2_get_Value_m1339120122((KeyValuePair_2_t3699644050 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		RuntimeObject * L_9 = KeyValuePair_2_get_Value_m1339120122((KeyValuePair_2_t3699644050 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (RuntimeObject *)L_9;
		NullCheck((RuntimeObject *)(V_2));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_2));
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_10, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_11 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_11);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_11, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_12 = V_0;
		String_t* L_13 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2983173998_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3699644050 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3699644050 *>(__this + 1);
	return KeyValuePair_2_ToString_m2983173998(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3967312370_gshared (KeyValuePair_2_t126004427 * __this, TypedConstant_t714020897  ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		TypedConstant_t714020897  L_0 = ___key0;
		__this->set_key_0(L_0);
		int32_t L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3967312370_AdjustorThunk (RuntimeObject * __this, TypedConstant_t714020897  ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t126004427 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t126004427 *>(__this + 1);
	KeyValuePair_2__ctor_m3967312370(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>::get_Key()
extern "C"  TypedConstant_t714020897  KeyValuePair_2_get_Key_m2623710737_gshared (KeyValuePair_2_t126004427 * __this, const RuntimeMethod* method)
{
	{
		TypedConstant_t714020897  L_0 = (TypedConstant_t714020897 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  TypedConstant_t714020897  KeyValuePair_2_get_Key_m2623710737_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t126004427 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t126004427 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2623710737(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3418326370_gshared (KeyValuePair_2_t126004427 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m3418326370_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t126004427 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t126004427 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3418326370(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1045680960_gshared (KeyValuePair_2_t126004427 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1045680960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	TypedConstant_t714020897  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		TypedConstant_t714020897  L_2 = KeyValuePair_2_get_Key_m2623710737((KeyValuePair_2_t126004427 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		TypedConstant_t714020897  L_4 = KeyValuePair_2_get_Key_m2623710737((KeyValuePair_2_t126004427 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (TypedConstant_t714020897 )L_4;
		RuntimeObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (&V_1));
		NullCheck((RuntimeObject *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_5);
		V_1 = *(TypedConstant_t714020897 *)UnBox(L_5);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_6, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_7 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_7);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_7, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		int32_t L_8 = KeyValuePair_2_get_Value_m3418326370((KeyValuePair_2_t126004427 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		StringBuilder_t1712802186 * L_9 = V_0;
		int32_t L_10 = KeyValuePair_2_get_Value_m3418326370((KeyValuePair_2_t126004427 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (int32_t)L_10;
		String_t* L_11 = Int32_ToString_m141394615((int32_t*)(&V_2), /*hidden argument*/NULL);
		NullCheck((StringBuilder_t1712802186 *)L_9);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_9, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_12);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_12, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		String_t* L_14 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1045680960_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t126004427 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t126004427 *>(__this + 1);
	return KeyValuePair_2_ToString_m1045680960(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m51719720_gshared (KeyValuePair_2_t255164838 * __this, TypedConstant_t714020897  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		TypedConstant_t714020897  L_0 = ___key0;
		__this->set_key_0(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m51719720_AdjustorThunk (RuntimeObject * __this, TypedConstant_t714020897  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t255164838 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t255164838 *>(__this + 1);
	KeyValuePair_2__ctor_m51719720(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>::get_Key()
extern "C"  TypedConstant_t714020897  KeyValuePair_2_get_Key_m3539669632_gshared (KeyValuePair_2_t255164838 * __this, const RuntimeMethod* method)
{
	{
		TypedConstant_t714020897  L_0 = (TypedConstant_t714020897 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  TypedConstant_t714020897  KeyValuePair_2_get_Key_m3539669632_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t255164838 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t255164838 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3539669632(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1745831214_gshared (KeyValuePair_2_t255164838 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1745831214_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t255164838 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t255164838 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1745831214(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3201086771_gshared (KeyValuePair_2_t255164838 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3201086771_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	TypedConstant_t714020897  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RuntimeObject * V_2 = NULL;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		TypedConstant_t714020897  L_2 = KeyValuePair_2_get_Key_m3539669632((KeyValuePair_2_t255164838 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		TypedConstant_t714020897  L_4 = KeyValuePair_2_get_Key_m3539669632((KeyValuePair_2_t255164838 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (TypedConstant_t714020897 )L_4;
		RuntimeObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (&V_1));
		NullCheck((RuntimeObject *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_5);
		V_1 = *(TypedConstant_t714020897 *)UnBox(L_5);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_6, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_7 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_7);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_7, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		RuntimeObject * L_8 = KeyValuePair_2_get_Value_m1745831214((KeyValuePair_2_t255164838 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_8)
		{
			goto IL_006d;
		}
	}
	{
		StringBuilder_t1712802186 * L_9 = V_0;
		RuntimeObject * L_10 = KeyValuePair_2_get_Value_m1745831214((KeyValuePair_2_t255164838 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (RuntimeObject *)L_10;
		NullCheck((RuntimeObject *)(V_2));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_2));
		NullCheck((StringBuilder_t1712802186 *)L_9);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_9, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_12);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_12, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		String_t* L_14 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3201086771_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t255164838 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t255164838 *>(__this + 1);
	return KeyValuePair_2_ToString_m3201086771(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1860763818_gshared (KeyValuePair_2_t4085865031 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		__this->set_key_0(L_0);
		int32_t L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1860763818_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t4085865031 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4085865031 *>(__this + 1);
	KeyValuePair_2__ctor_m1860763818(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m2837550314_gshared (KeyValuePair_2_t4085865031 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m2837550314_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t4085865031 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4085865031 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2837550314(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1430811576_gshared (KeyValuePair_2_t4085865031 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m1430811576_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t4085865031 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4085865031 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1430811576(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m490808492_gshared (KeyValuePair_2_t4085865031 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m490808492_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	int32_t V_2 = 0;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m2837550314((KeyValuePair_2_t4085865031 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		RuntimeObject * L_4 = KeyValuePair_2_get_Key_m2837550314((KeyValuePair_2_t4085865031 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (RuntimeObject *)L_4;
		NullCheck((RuntimeObject *)(V_1));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_1));
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		int32_t L_7 = KeyValuePair_2_get_Value_m1430811576((KeyValuePair_2_t4085865031 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		int32_t L_9 = KeyValuePair_2_get_Value_m1430811576((KeyValuePair_2_t4085865031 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (int32_t)L_9;
		RuntimeObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_2));
		NullCheck((RuntimeObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_10);
		V_2 = *(int32_t*)UnBox(L_10);
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_12);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_12, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		String_t* L_14 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m490808492_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t4085865031 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4085865031 *>(__this + 1);
	return KeyValuePair_2_ToString_m490808492(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m198529365_gshared (KeyValuePair_2_t249061059 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		__this->set_key_0(L_0);
		int32_t L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m198529365_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t249061059 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t249061059 *>(__this + 1);
	KeyValuePair_2__ctor_m198529365(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m3093971998_gshared (KeyValuePair_2_t249061059 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m3093971998_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t249061059 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t249061059 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3093971998(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m551864407_gshared (KeyValuePair_2_t249061059 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m551864407_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t249061059 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t249061059 *>(__this + 1);
	return KeyValuePair_2_get_Value_m551864407(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m675279023_gshared (KeyValuePair_2_t249061059 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m675279023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	int32_t V_2 = 0;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m3093971998((KeyValuePair_2_t249061059 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		RuntimeObject * L_4 = KeyValuePair_2_get_Key_m3093971998((KeyValuePair_2_t249061059 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (RuntimeObject *)L_4;
		NullCheck((RuntimeObject *)(V_1));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_1));
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		int32_t L_7 = KeyValuePair_2_get_Value_m551864407((KeyValuePair_2_t249061059 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		int32_t L_9 = KeyValuePair_2_get_Value_m551864407((KeyValuePair_2_t249061059 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (int32_t)L_9;
		RuntimeObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_2));
		NullCheck((RuntimeObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_10);
		V_2 = *(int32_t*)UnBox(L_10);
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_12);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_12, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		String_t* L_14 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m675279023_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t249061059 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t249061059 *>(__this + 1);
	return KeyValuePair_2_ToString_m675279023(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1927988026_gshared (KeyValuePair_2_t2255362622 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		__this->set_key_0(L_0);
		int32_t L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1927988026_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t2255362622 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2255362622 *>(__this + 1);
	KeyValuePair_2__ctor_m1927988026(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m3745423128_gshared (KeyValuePair_2_t2255362622 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m3745423128_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2255362622 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2255362622 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3745423128(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m963734252_gshared (KeyValuePair_2_t2255362622 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m963734252_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2255362622 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2255362622 *>(__this + 1);
	return KeyValuePair_2_get_Value_m963734252(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1774591758_gshared (KeyValuePair_2_t2255362622 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1774591758_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	int32_t V_2 = 0;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m3745423128((KeyValuePair_2_t2255362622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		RuntimeObject * L_4 = KeyValuePair_2_get_Key_m3745423128((KeyValuePair_2_t2255362622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (RuntimeObject *)L_4;
		NullCheck((RuntimeObject *)(V_1));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_1));
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		int32_t L_7 = KeyValuePair_2_get_Value_m963734252((KeyValuePair_2_t2255362622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		int32_t L_9 = KeyValuePair_2_get_Value_m963734252((KeyValuePair_2_t2255362622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (int32_t)L_9;
		RuntimeObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_2));
		NullCheck((RuntimeObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_10);
		V_2 = *(int32_t*)UnBox(L_10);
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_12);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_12, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		String_t* L_14 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1774591758_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2255362622 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2255362622 *>(__this + 1);
	return KeyValuePair_2_ToString_m1774591758(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m23191374_gshared (KeyValuePair_2_t3842366416 * __this, RuntimeObject * ___key0, bool ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		__this->set_key_0(L_0);
		bool L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m23191374_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, bool ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t3842366416 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3842366416 *>(__this + 1);
	KeyValuePair_2__ctor_m23191374(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m2106922848_gshared (KeyValuePair_2_t3842366416 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m2106922848_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3842366416 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3842366416 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2106922848(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m1669764045_gshared (KeyValuePair_2_t3842366416 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_value_1();
		return L_0;
	}
}
extern "C"  bool KeyValuePair_2_get_Value_m1669764045_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3842366416 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3842366416 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1669764045(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2480962023_gshared (KeyValuePair_2_t3842366416 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2480962023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	bool V_2 = false;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m2106922848((KeyValuePair_2_t3842366416 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		RuntimeObject * L_4 = KeyValuePair_2_get_Key_m2106922848((KeyValuePair_2_t3842366416 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (RuntimeObject *)L_4;
		NullCheck((RuntimeObject *)(V_1));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_1));
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		bool L_7 = KeyValuePair_2_get_Value_m1669764045((KeyValuePair_2_t3842366416 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		bool L_9 = KeyValuePair_2_get_Value_m1669764045((KeyValuePair_2_t3842366416 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (bool)L_9;
		String_t* L_10 = Boolean_ToString_m2664721875((bool*)(&V_2), /*hidden argument*/NULL);
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_10, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_11 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_11);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_11, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_12 = V_0;
		String_t* L_13 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2480962023_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3842366416 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3842366416 *>(__this + 1);
	return KeyValuePair_2_ToString_m2480962023(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m880186442_gshared (KeyValuePair_2_t2401056908 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		__this->set_key_0(L_0);
		int32_t L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m880186442_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t2401056908 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2401056908 *>(__this + 1);
	KeyValuePair_2__ctor_m880186442(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1055012466_gshared (KeyValuePair_2_t2401056908 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1055012466_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2401056908 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2401056908 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1055012466(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1308554439_gshared (KeyValuePair_2_t2401056908 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m1308554439_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2401056908 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2401056908 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1308554439(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4231614106_gshared (KeyValuePair_2_t2401056908 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m4231614106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	int32_t V_2 = 0;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m1055012466((KeyValuePair_2_t2401056908 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		RuntimeObject * L_4 = KeyValuePair_2_get_Key_m1055012466((KeyValuePair_2_t2401056908 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (RuntimeObject *)L_4;
		NullCheck((RuntimeObject *)(V_1));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_1));
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		int32_t L_7 = KeyValuePair_2_get_Value_m1308554439((KeyValuePair_2_t2401056908 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		int32_t L_9 = KeyValuePair_2_get_Value_m1308554439((KeyValuePair_2_t2401056908 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m141394615((int32_t*)(&V_2), /*hidden argument*/NULL);
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_10, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_11 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_11);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_11, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_12 = V_0;
		String_t* L_13 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m4231614106_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2401056908 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2401056908 *>(__this + 1);
	return KeyValuePair_2_ToString_m4231614106(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3716993270_gshared (KeyValuePair_2_t2730400744 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		__this->set_key_0(L_0);
		int32_t L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3716993270_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t2730400744 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2730400744 *>(__this + 1);
	KeyValuePair_2__ctor_m3716993270(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m4110334143_gshared (KeyValuePair_2_t2730400744 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m4110334143_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2730400744 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2730400744 *>(__this + 1);
	return KeyValuePair_2_get_Key_m4110334143(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3734748706_gshared (KeyValuePair_2_t2730400744 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m3734748706_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2730400744 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2730400744 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3734748706(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3021632350_gshared (KeyValuePair_2_t2730400744 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3021632350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	int32_t V_2 = 0;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m4110334143((KeyValuePair_2_t2730400744 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		RuntimeObject * L_4 = KeyValuePair_2_get_Key_m4110334143((KeyValuePair_2_t2730400744 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (RuntimeObject *)L_4;
		NullCheck((RuntimeObject *)(V_1));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_1));
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		int32_t L_7 = KeyValuePair_2_get_Value_m3734748706((KeyValuePair_2_t2730400744 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		int32_t L_9 = KeyValuePair_2_get_Value_m3734748706((KeyValuePair_2_t2730400744 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (int32_t)L_9;
		RuntimeObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_2));
		NullCheck((RuntimeObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_10);
		V_2 = *(int32_t*)UnBox(L_10);
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_12);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_12, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		String_t* L_14 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3021632350_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2730400744 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2730400744 *>(__this + 1);
	return KeyValuePair_2_ToString_m3021632350(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m727165395_gshared (KeyValuePair_2_t2530217319 * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		__this->set_key_0(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m727165395_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t2530217319 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2530217319 *>(__this + 1);
	KeyValuePair_2__ctor_m727165395(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m4184817181_gshared (KeyValuePair_2_t2530217319 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m4184817181_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2530217319 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2530217319 *>(__this + 1);
	return KeyValuePair_2_get_Key_m4184817181(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1132502692_gshared (KeyValuePair_2_t2530217319 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1132502692_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2530217319 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2530217319 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1132502692(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m510648957_gshared (KeyValuePair_2_t2530217319 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m510648957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m4184817181((KeyValuePair_2_t2530217319 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		RuntimeObject * L_4 = KeyValuePair_2_get_Key_m4184817181((KeyValuePair_2_t2530217319 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (RuntimeObject *)L_4;
		NullCheck((RuntimeObject *)(V_1));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_1));
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		RuntimeObject * L_7 = KeyValuePair_2_get_Value_m1132502692((KeyValuePair_2_t2530217319 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		RuntimeObject * L_9 = KeyValuePair_2_get_Value_m1132502692((KeyValuePair_2_t2530217319 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (RuntimeObject *)L_9;
		NullCheck((RuntimeObject *)(V_2));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_2));
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_10, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_11 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_11);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_11, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_12 = V_0;
		String_t* L_13 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m510648957_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2530217319 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2530217319 *>(__this + 1);
	return KeyValuePair_2_ToString_m510648957(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m877231462_gshared (KeyValuePair_2_t3174081962 * __this, RuntimeObject * ___key0, ResourceLocator_t3723970807  ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		__this->set_key_0(L_0);
		ResourceLocator_t3723970807  L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m877231462_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, ResourceLocator_t3723970807  ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t3174081962 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3174081962 *>(__this + 1);
	KeyValuePair_2__ctor_m877231462(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1914465694_gshared (KeyValuePair_2_t3174081962 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1914465694_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3174081962 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3174081962 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1914465694(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>::get_Value()
extern "C"  ResourceLocator_t3723970807  KeyValuePair_2_get_Value_m1048976747_gshared (KeyValuePair_2_t3174081962 * __this, const RuntimeMethod* method)
{
	{
		ResourceLocator_t3723970807  L_0 = (ResourceLocator_t3723970807 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  ResourceLocator_t3723970807  KeyValuePair_2_get_Value_m1048976747_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3174081962 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3174081962 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1048976747(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m225390547_gshared (KeyValuePair_2_t3174081962 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m225390547_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	ResourceLocator_t3723970807  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m1914465694((KeyValuePair_2_t3174081962 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		RuntimeObject * L_4 = KeyValuePair_2_get_Key_m1914465694((KeyValuePair_2_t3174081962 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (RuntimeObject *)L_4;
		NullCheck((RuntimeObject *)(V_1));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_1));
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		ResourceLocator_t3723970807  L_7 = KeyValuePair_2_get_Value_m1048976747((KeyValuePair_2_t3174081962 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		ResourceLocator_t3723970807  L_9 = KeyValuePair_2_get_Value_m1048976747((KeyValuePair_2_t3174081962 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (ResourceLocator_t3723970807 )L_9;
		RuntimeObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_2));
		NullCheck((RuntimeObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_10);
		V_2 = *(ResourceLocator_t3723970807 *)UnBox(L_10);
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_12);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_12, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		String_t* L_14 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m225390547_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3174081962 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3174081962 *>(__this + 1);
	return KeyValuePair_2_ToString_m225390547(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m52543417_gshared (KeyValuePair_2_t2738617651 * __this, RuntimeObject * ___key0, EventRegistrationTokenList_t3288506496  ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		__this->set_key_0(L_0);
		EventRegistrationTokenList_t3288506496  L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m52543417_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, EventRegistrationTokenList_t3288506496  ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t2738617651 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2738617651 *>(__this + 1);
	KeyValuePair_2__ctor_m52543417(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m312793161_gshared (KeyValuePair_2_t2738617651 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m312793161_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2738617651 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2738617651 *>(__this + 1);
	return KeyValuePair_2_get_Key_m312793161(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>::get_Value()
extern "C"  EventRegistrationTokenList_t3288506496  KeyValuePair_2_get_Value_m80964760_gshared (KeyValuePair_2_t2738617651 * __this, const RuntimeMethod* method)
{
	{
		EventRegistrationTokenList_t3288506496  L_0 = (EventRegistrationTokenList_t3288506496 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  EventRegistrationTokenList_t3288506496  KeyValuePair_2_get_Value_m80964760_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2738617651 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2738617651 *>(__this + 1);
	return KeyValuePair_2_get_Value_m80964760(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m80164506_gshared (KeyValuePair_2_t2738617651 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m80164506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	EventRegistrationTokenList_t3288506496  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m312793161((KeyValuePair_2_t2738617651 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		RuntimeObject * L_4 = KeyValuePair_2_get_Key_m312793161((KeyValuePair_2_t2738617651 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (RuntimeObject *)L_4;
		NullCheck((RuntimeObject *)(V_1));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_1));
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		EventRegistrationTokenList_t3288506496  L_7 = KeyValuePair_2_get_Value_m80964760((KeyValuePair_2_t2738617651 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		EventRegistrationTokenList_t3288506496  L_9 = KeyValuePair_2_get_Value_m80964760((KeyValuePair_2_t2738617651 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (EventRegistrationTokenList_t3288506496 )L_9;
		RuntimeObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_2));
		NullCheck((RuntimeObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_10);
		V_2 = *(EventRegistrationTokenList_t3288506496 *)UnBox(L_10);
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_12);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_12, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		String_t* L_14 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m80164506_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2738617651 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2738617651 *>(__this + 1);
	return KeyValuePair_2_ToString_m80164506(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1054831967_gshared (KeyValuePair_2_t4253942476 * __this, EventCacheKey_t3133620722  ___key0, EventCacheEntry_t156445199  ___value1, const RuntimeMethod* method)
{
	{
		EventCacheKey_t3133620722  L_0 = ___key0;
		__this->set_key_0(L_0);
		EventCacheEntry_t156445199  L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1054831967_AdjustorThunk (RuntimeObject * __this, EventCacheKey_t3133620722  ___key0, EventCacheEntry_t156445199  ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t4253942476 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4253942476 *>(__this + 1);
	KeyValuePair_2__ctor_m1054831967(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>::get_Key()
extern "C"  EventCacheKey_t3133620722  KeyValuePair_2_get_Key_m1029407593_gshared (KeyValuePair_2_t4253942476 * __this, const RuntimeMethod* method)
{
	{
		EventCacheKey_t3133620722  L_0 = (EventCacheKey_t3133620722 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  EventCacheKey_t3133620722  KeyValuePair_2_get_Key_m1029407593_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t4253942476 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4253942476 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1029407593(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>::get_Value()
extern "C"  EventCacheEntry_t156445199  KeyValuePair_2_get_Value_m497513548_gshared (KeyValuePair_2_t4253942476 * __this, const RuntimeMethod* method)
{
	{
		EventCacheEntry_t156445199  L_0 = (EventCacheEntry_t156445199 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  EventCacheEntry_t156445199  KeyValuePair_2_get_Value_m497513548_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t4253942476 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4253942476 *>(__this + 1);
	return KeyValuePair_2_get_Value_m497513548(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3941865661_gshared (KeyValuePair_2_t4253942476 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3941865661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	EventCacheKey_t3133620722  V_1;
	memset(&V_1, 0, sizeof(V_1));
	EventCacheEntry_t156445199  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		EventCacheKey_t3133620722  L_2 = KeyValuePair_2_get_Key_m1029407593((KeyValuePair_2_t4253942476 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		EventCacheKey_t3133620722  L_4 = KeyValuePair_2_get_Key_m1029407593((KeyValuePair_2_t4253942476 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (EventCacheKey_t3133620722 )L_4;
		String_t* L_5 = EventCacheKey_ToString_m875627681((EventCacheKey_t3133620722 *)(&V_1), /*hidden argument*/NULL);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		EventCacheEntry_t156445199  L_7 = KeyValuePair_2_get_Value_m497513548((KeyValuePair_2_t4253942476 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		EventCacheEntry_t156445199  L_9 = KeyValuePair_2_get_Value_m497513548((KeyValuePair_2_t4253942476 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (EventCacheEntry_t156445199 )L_9;
		RuntimeObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_2));
		NullCheck((RuntimeObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_10);
		V_2 = *(EventCacheEntry_t156445199 *)UnBox(L_10);
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_12);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_12, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		String_t* L_14 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3941865661_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t4253942476 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4253942476 *>(__this + 1);
	return KeyValuePair_2_ToString_m3941865661(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2398257575_gshared (KeyValuePair_2_t1297193679 * __this, uint32_t ___key0, XboxControllerData_t1920221146  ___value1, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___key0;
		__this->set_key_0(L_0);
		XboxControllerData_t1920221146  L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2398257575_AdjustorThunk (RuntimeObject * __this, uint32_t ___key0, XboxControllerData_t1920221146  ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t1297193679 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1297193679 *>(__this + 1);
	KeyValuePair_2__ctor_m2398257575(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>::get_Key()
extern "C"  uint32_t KeyValuePair_2_get_Key_m784662759_gshared (KeyValuePair_2_t1297193679 * __this, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  uint32_t KeyValuePair_2_get_Key_m784662759_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t1297193679 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1297193679 *>(__this + 1);
	return KeyValuePair_2_get_Key_m784662759(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>::get_Value()
extern "C"  XboxControllerData_t1920221146  KeyValuePair_2_get_Value_m3814993531_gshared (KeyValuePair_2_t1297193679 * __this, const RuntimeMethod* method)
{
	{
		XboxControllerData_t1920221146  L_0 = (XboxControllerData_t1920221146 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  XboxControllerData_t1920221146  KeyValuePair_2_get_Value_m3814993531_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t1297193679 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1297193679 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3814993531(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m601171314_gshared (KeyValuePair_2_t1297193679 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m601171314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	uint32_t V_1 = 0;
	XboxControllerData_t1920221146  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		uint32_t L_2 = KeyValuePair_2_get_Key_m784662759((KeyValuePair_2_t1297193679 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		uint32_t L_4 = KeyValuePair_2_get_Key_m784662759((KeyValuePair_2_t1297193679 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (uint32_t)L_4;
		String_t* L_5 = UInt32_ToString_m2574561716((uint32_t*)(&V_1), /*hidden argument*/NULL);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		XboxControllerData_t1920221146  L_7 = KeyValuePair_2_get_Value_m3814993531((KeyValuePair_2_t1297193679 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		XboxControllerData_t1920221146  L_9 = KeyValuePair_2_get_Value_m3814993531((KeyValuePair_2_t1297193679 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (XboxControllerData_t1920221146 )L_9;
		RuntimeObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_2));
		NullCheck((RuntimeObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_10);
		V_2 = *(XboxControllerData_t1920221146 *)UnBox(L_10);
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_12);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_12, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		String_t* L_14 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m601171314_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t1297193679 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1297193679 *>(__this + 1);
	return KeyValuePair_2_ToString_m601171314(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1509212969_gshared (KeyValuePair_2_t2457078697 * __this, uint32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___key0;
		__this->set_key_0(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_value_1(L_1);
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1509212969_AdjustorThunk (RuntimeObject * __this, uint32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t2457078697 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2457078697 *>(__this + 1);
	KeyValuePair_2__ctor_m1509212969(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::get_Key()
extern "C"  uint32_t KeyValuePair_2_get_Key_m1662296104_gshared (KeyValuePair_2_t2457078697 * __this, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  uint32_t KeyValuePair_2_get_Key_m1662296104_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2457078697 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2457078697 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1662296104(_thisAdjusted, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1484005982_gshared (KeyValuePair_2_t2457078697 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1484005982_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2457078697 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2457078697 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1484005982(_thisAdjusted, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m816416158_gshared (KeyValuePair_2_t2457078697 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m816416158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	uint32_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	{
		StringBuilder_t1712802186 * L_0 = StringBuilderCache_Acquire_m4169564332(NULL /*static, unused*/, (int32_t)((int32_t)16), /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1712802186 *)L_0;
		StringBuilder_t1712802186 * L_1 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_1);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_1, (Il2CppChar)((int32_t)91), /*hidden argument*/NULL);
		uint32_t L_2 = KeyValuePair_2_get_Key_m1662296104((KeyValuePair_2_t2457078697 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}
	{
		StringBuilder_t1712802186 * L_3 = V_0;
		uint32_t L_4 = KeyValuePair_2_get_Key_m1662296104((KeyValuePair_2_t2457078697 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_1 = (uint32_t)L_4;
		String_t* L_5 = UInt32_ToString_m2574561716((uint32_t*)(&V_1), /*hidden argument*/NULL);
		NullCheck((StringBuilder_t1712802186 *)L_3);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_3, (String_t*)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_6);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_6, (String_t*)_stringLiteral3450517380, /*hidden argument*/NULL);
		RuntimeObject * L_7 = KeyValuePair_2_get_Value_m1484005982((KeyValuePair_2_t2457078697 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		RuntimeObject * L_9 = KeyValuePair_2_get_Value_m1484005982((KeyValuePair_2_t2457078697 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (RuntimeObject *)L_9;
		NullCheck((RuntimeObject *)(V_2));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(V_2));
		NullCheck((StringBuilder_t1712802186 *)L_8);
		StringBuilder_Append_m1965104174((StringBuilder_t1712802186 *)L_8, (String_t*)L_10, /*hidden argument*/NULL);
	}

IL_006d:
	{
		StringBuilder_t1712802186 * L_11 = V_0;
		NullCheck((StringBuilder_t1712802186 *)L_11);
		StringBuilder_Append_m2383614642((StringBuilder_t1712802186 *)L_11, (Il2CppChar)((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_12 = V_0;
		String_t* L_13 = StringBuilderCache_GetStringAndRelease_m1110745745(NULL /*static, unused*/, (StringBuilder_t1712802186 *)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m816416158_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2457078697 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2457078697 *>(__this + 1);
	return KeyValuePair_2_ToString_m816416158(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
