﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityGLTF.GLTFComponent
struct GLTFComponent_t238219215;
// UnityGLTF.GLTFSceneImporter
struct GLTFSceneImporter_t274320441;
// System.String
struct String_t;
// UnityGLTF.AsyncAction
struct AsyncAction_t3334821446;
// GLTF.Schema.Scene
struct Scene_t1955661005;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// GLTF.Schema.Buffer
struct Buffer_t869705777;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// GLTF.Schema.Image
struct Image_t782620675;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// System.Text.RegularExpressions.Match
struct Match_t3408321083;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// System.IO.FileStream
struct FileStream_t4292183065;
// GLTF.Schema.BufferView
struct BufferView_t2650099126;
// System.Action
struct Action_t1264377477;
// UnityGLTF.AsyncAction/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t4123515534;
// System.Double[]
struct DoubleU5BU5D_t3413330114;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3365920845;
// FastSimplexNoise/Contribution2[]
struct Contribution2U5BU5D_t2459528002;
// FastSimplexNoise/Contribution3[]
struct Contribution3U5BU5D_t896822082;
// FastSimplexNoise/Contribution4[]
struct Contribution4U5BU5D_t3245828930;
// HoloToolkit.Unity.InputModule.KeyboardManager
struct KeyboardManager_t1786564518;
// MicrophoneManager
struct MicrophoneManager_t2580314274;
// System.Exception
struct Exception_t1436737249;
// UnityEngine.Material
struct Material_t340375123;
// GLTF.Schema.Material
struct Material_t2790518843;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t149664596;
// UnityEngine.Texture[]
struct TextureU5BU5D_t908295702;
// UnityGLTF.Cache.MaterialCacheData[]
struct MaterialCacheDataU5BU5D_t484490365;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]>
struct Dictionary_2_t3005360988;
// System.Collections.Generic.List`1<UnityGLTF.Cache.MeshCacheData[]>
struct List_1_t695246165;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// System.Collections.Generic.Dictionary`2<System.String,GLTF.AttributeAccessor>
struct Dictionary_2_t691531204;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t2285235057;
// System.Text.EncoderFallback
struct EncoderFallback_t1188251036;
// System.Text.DecoderFallback
struct DecoderFallback_t3123823036;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.Dictionary`2<UnityGLTF.GLTFSceneImporter/MaterialType,UnityEngine.Shader>
struct Dictionary_2_t2973298224;
// System.IO.Stream
struct Stream_t1273022909;
// GLTF.Schema.GLTFRoot
struct GLTFRoot_t676886847;
// UnityGLTF.Cache.AssetCache
struct AssetCache_t783611398;
// System.Security.Cryptography.HMACSHA1
struct HMACSHA1_t1952596188;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Windows.Speech.DictationRecognizer
struct DictationRecognizer_t838322161;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;
// UnityEngine.Shader
struct Shader_t4151988712;

struct Exception_t1436737249_marshaled_pinvoke;
struct Exception_t1436737249_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745588_H
#define U3CMODULEU3E_T692745588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745588 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745588_H
#ifndef U3CMODULEU3E_T692745589_H
#define U3CMODULEU3E_T692745589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745589 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745589_H
#ifndef U3CSTARTU3ED__13_T203077579_H
#define U3CSTARTU3ED__13_T203077579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFComponent/<Start>d__13
struct  U3CStartU3Ed__13_t203077579  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.GLTFComponent/<Start>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.GLTFComponent/<Start>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityGLTF.GLTFComponent UnityGLTF.GLTFComponent/<Start>d__13::<>4__this
	GLTFComponent_t238219215 * ___U3CU3E4__this_2;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFComponent/<Start>d__13::<loader>5__1
	GLTFSceneImporter_t274320441 * ___U3CloaderU3E5__1_3;
	// System.String UnityGLTF.GLTFComponent/<Start>d__13::<fullPath>5__2
	String_t* ___U3CfullPathU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__13_t203077579, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__13_t203077579, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__13_t203077579, ___U3CU3E4__this_2)); }
	inline GLTFComponent_t238219215 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFComponent_t238219215 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFComponent_t238219215 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CloaderU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__13_t203077579, ___U3CloaderU3E5__1_3)); }
	inline GLTFSceneImporter_t274320441 * get_U3CloaderU3E5__1_3() const { return ___U3CloaderU3E5__1_3; }
	inline GLTFSceneImporter_t274320441 ** get_address_of_U3CloaderU3E5__1_3() { return &___U3CloaderU3E5__1_3; }
	inline void set_U3CloaderU3E5__1_3(GLTFSceneImporter_t274320441 * value)
	{
		___U3CloaderU3E5__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloaderU3E5__1_3), value);
	}

	inline static int32_t get_offset_of_U3CfullPathU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__13_t203077579, ___U3CfullPathU3E5__2_4)); }
	inline String_t* get_U3CfullPathU3E5__2_4() const { return ___U3CfullPathU3E5__2_4; }
	inline String_t** get_address_of_U3CfullPathU3E5__2_4() { return &___U3CfullPathU3E5__2_4; }
	inline void set_U3CfullPathU3E5__2_4(String_t* value)
	{
		___U3CfullPathU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfullPathU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__13_T203077579_H
#ifndef U3CWAITU3ED__3_T2236800517_H
#define U3CWAITU3ED__3_T2236800517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.AsyncAction/<Wait>d__3
struct  U3CWaitU3Ed__3_t2236800517  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.AsyncAction/<Wait>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.AsyncAction/<Wait>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityGLTF.AsyncAction UnityGLTF.AsyncAction/<Wait>d__3::<>4__this
	AsyncAction_t3334821446 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__3_t2236800517, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__3_t2236800517, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__3_t2236800517, ___U3CU3E4__this_2)); }
	inline AsyncAction_t3334821446 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AsyncAction_t3334821446 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AsyncAction_t3334821446 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITU3ED__3_T2236800517_H
#ifndef CONSTS_T1749595338_H
#define CONSTS_T1749595338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Consts
struct  Consts_t1749595338  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTS_T1749595338_H
#ifndef U3CIMPORTSCENEU3ED__22_T599600113_H
#define U3CIMPORTSCENEU3ED__22_T599600113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<ImportScene>d__22
struct  U3CImportSceneU3Ed__22_t599600113  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ImportScene>d__22::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.GLTFSceneImporter/<ImportScene>d__22::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ImportScene>d__22::sceneIndex
	int32_t ___sceneIndex_2;
	// System.Boolean UnityGLTF.GLTFSceneImporter/<ImportScene>d__22::isMultithreaded
	bool ___isMultithreaded_3;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<ImportScene>d__22::<>4__this
	GLTFSceneImporter_t274320441 * ___U3CU3E4__this_4;
	// GLTF.Schema.Scene UnityGLTF.GLTFSceneImporter/<ImportScene>d__22::<scene>5__1
	Scene_t1955661005 * ___U3CsceneU3E5__1_5;
	// UnityEngine.GameObject UnityGLTF.GLTFSceneImporter/<ImportScene>d__22::<sceneObj>5__2
	GameObject_t1113636619 * ___U3CsceneObjU3E5__2_6;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ImportScene>d__22::<i>5__3
	int32_t ___U3CiU3E5__3_7;
	// GLTF.Schema.Buffer UnityGLTF.GLTFSceneImporter/<ImportScene>d__22::<buffer>5__4
	Buffer_t869705777 * ___U3CbufferU3E5__4_8;
	// System.Byte[] UnityGLTF.GLTFSceneImporter/<ImportScene>d__22::<glbBuffer>5__5
	ByteU5BU5D_t4116647657* ___U3CglbBufferU3E5__5_9;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<ImportScene>d__22::<i>5__6
	int32_t ___U3CiU3E5__6_10;
	// GLTF.Schema.Image UnityGLTF.GLTFSceneImporter/<ImportScene>d__22::<image>5__7
	Image_t782620675 * ___U3CimageU3E5__7_11;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t599600113, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t599600113, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_sceneIndex_2() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t599600113, ___sceneIndex_2)); }
	inline int32_t get_sceneIndex_2() const { return ___sceneIndex_2; }
	inline int32_t* get_address_of_sceneIndex_2() { return &___sceneIndex_2; }
	inline void set_sceneIndex_2(int32_t value)
	{
		___sceneIndex_2 = value;
	}

	inline static int32_t get_offset_of_isMultithreaded_3() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t599600113, ___isMultithreaded_3)); }
	inline bool get_isMultithreaded_3() const { return ___isMultithreaded_3; }
	inline bool* get_address_of_isMultithreaded_3() { return &___isMultithreaded_3; }
	inline void set_isMultithreaded_3(bool value)
	{
		___isMultithreaded_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t599600113, ___U3CU3E4__this_4)); }
	inline GLTFSceneImporter_t274320441 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline GLTFSceneImporter_t274320441 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(GLTFSceneImporter_t274320441 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_U3CsceneU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t599600113, ___U3CsceneU3E5__1_5)); }
	inline Scene_t1955661005 * get_U3CsceneU3E5__1_5() const { return ___U3CsceneU3E5__1_5; }
	inline Scene_t1955661005 ** get_address_of_U3CsceneU3E5__1_5() { return &___U3CsceneU3E5__1_5; }
	inline void set_U3CsceneU3E5__1_5(Scene_t1955661005 * value)
	{
		___U3CsceneU3E5__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsceneU3E5__1_5), value);
	}

	inline static int32_t get_offset_of_U3CsceneObjU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t599600113, ___U3CsceneObjU3E5__2_6)); }
	inline GameObject_t1113636619 * get_U3CsceneObjU3E5__2_6() const { return ___U3CsceneObjU3E5__2_6; }
	inline GameObject_t1113636619 ** get_address_of_U3CsceneObjU3E5__2_6() { return &___U3CsceneObjU3E5__2_6; }
	inline void set_U3CsceneObjU3E5__2_6(GameObject_t1113636619 * value)
	{
		___U3CsceneObjU3E5__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsceneObjU3E5__2_6), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_7() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t599600113, ___U3CiU3E5__3_7)); }
	inline int32_t get_U3CiU3E5__3_7() const { return ___U3CiU3E5__3_7; }
	inline int32_t* get_address_of_U3CiU3E5__3_7() { return &___U3CiU3E5__3_7; }
	inline void set_U3CiU3E5__3_7(int32_t value)
	{
		___U3CiU3E5__3_7 = value;
	}

	inline static int32_t get_offset_of_U3CbufferU3E5__4_8() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t599600113, ___U3CbufferU3E5__4_8)); }
	inline Buffer_t869705777 * get_U3CbufferU3E5__4_8() const { return ___U3CbufferU3E5__4_8; }
	inline Buffer_t869705777 ** get_address_of_U3CbufferU3E5__4_8() { return &___U3CbufferU3E5__4_8; }
	inline void set_U3CbufferU3E5__4_8(Buffer_t869705777 * value)
	{
		___U3CbufferU3E5__4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbufferU3E5__4_8), value);
	}

	inline static int32_t get_offset_of_U3CglbBufferU3E5__5_9() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t599600113, ___U3CglbBufferU3E5__5_9)); }
	inline ByteU5BU5D_t4116647657* get_U3CglbBufferU3E5__5_9() const { return ___U3CglbBufferU3E5__5_9; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CglbBufferU3E5__5_9() { return &___U3CglbBufferU3E5__5_9; }
	inline void set_U3CglbBufferU3E5__5_9(ByteU5BU5D_t4116647657* value)
	{
		___U3CglbBufferU3E5__5_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CglbBufferU3E5__5_9), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__6_10() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t599600113, ___U3CiU3E5__6_10)); }
	inline int32_t get_U3CiU3E5__6_10() const { return ___U3CiU3E5__6_10; }
	inline int32_t* get_address_of_U3CiU3E5__6_10() { return &___U3CiU3E5__6_10; }
	inline void set_U3CiU3E5__6_10(int32_t value)
	{
		___U3CiU3E5__6_10 = value;
	}

	inline static int32_t get_offset_of_U3CimageU3E5__7_11() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t599600113, ___U3CimageU3E5__7_11)); }
	inline Image_t782620675 * get_U3CimageU3E5__7_11() const { return ___U3CimageU3E5__7_11; }
	inline Image_t782620675 ** get_address_of_U3CimageU3E5__7_11() { return &___U3CimageU3E5__7_11; }
	inline void set_U3CimageU3E5__7_11(Image_t782620675 * value)
	{
		___U3CimageU3E5__7_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageU3E5__7_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CIMPORTSCENEU3ED__22_T599600113_H
#ifndef U3CLOADIMAGEU3ED__33_T1921818520_H
#define U3CLOADIMAGEU3ED__33_T1921818520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<LoadImage>d__33
struct  U3CLoadImageU3Ed__33_t1921818520  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::rootPath
	String_t* ___rootPath_2;
	// GLTF.Schema.Image UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::image
	Image_t782620675 * ___image_3;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::imageID
	int32_t ___imageID_4;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<>4__this
	GLTFSceneImporter_t274320441 * ___U3CU3E4__this_5;
	// UnityEngine.Texture2D UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<texture>5__1
	Texture2D_t3840446185 * ___U3CtextureU3E5__1_6;
	// System.String UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<uri>5__2
	String_t* ___U3CuriU3E5__2_7;
	// System.Text.RegularExpressions.Regex UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<regex>5__3
	Regex_t3657309853 * ___U3CregexU3E5__3_8;
	// System.Text.RegularExpressions.Match UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<match>5__4
	Match_t3408321083 * ___U3CmatchU3E5__4_9;
	// System.String UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<base64Data>5__5
	String_t* ___U3Cbase64DataU3E5__5_10;
	// System.Byte[] UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<textureData>5__6
	ByteU5BU5D_t4116647657* ___U3CtextureDataU3E5__6_11;
	// UnityEngine.Networking.UnityWebRequest UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<www>5__7
	UnityWebRequest_t463507806 * ___U3CwwwU3E5__7_12;
	// UnityEngine.Texture2D UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<tempTexture>5__8
	Texture2D_t3840446185 * ___U3CtempTextureU3E5__8_13;
	// System.String UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<pathToLoad>5__9
	String_t* ___U3CpathToLoadU3E5__9_14;
	// System.IO.FileStream UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<file>5__10
	FileStream_t4292183065 * ___U3CfileU3E5__10_15;
	// System.Byte[] UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<bufferData>5__11
	ByteU5BU5D_t4116647657* ___U3CbufferDataU3E5__11_16;
	// GLTF.Schema.BufferView UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<bufferView>5__12
	BufferView_t2650099126 * ___U3CbufferViewU3E5__12_17;
	// GLTF.Schema.Buffer UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<buffer>5__13
	Buffer_t869705777 * ___U3CbufferU3E5__13_18;
	// System.Byte[] UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<data>5__14
	ByteU5BU5D_t4116647657* ___U3CdataU3E5__14_19;
	// System.Byte[] UnityGLTF.GLTFSceneImporter/<LoadImage>d__33::<bufferContents>5__15
	ByteU5BU5D_t4116647657* ___U3CbufferContentsU3E5__15_20;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_rootPath_2() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___rootPath_2)); }
	inline String_t* get_rootPath_2() const { return ___rootPath_2; }
	inline String_t** get_address_of_rootPath_2() { return &___rootPath_2; }
	inline void set_rootPath_2(String_t* value)
	{
		___rootPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___rootPath_2), value);
	}

	inline static int32_t get_offset_of_image_3() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___image_3)); }
	inline Image_t782620675 * get_image_3() const { return ___image_3; }
	inline Image_t782620675 ** get_address_of_image_3() { return &___image_3; }
	inline void set_image_3(Image_t782620675 * value)
	{
		___image_3 = value;
		Il2CppCodeGenWriteBarrier((&___image_3), value);
	}

	inline static int32_t get_offset_of_imageID_4() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___imageID_4)); }
	inline int32_t get_imageID_4() const { return ___imageID_4; }
	inline int32_t* get_address_of_imageID_4() { return &___imageID_4; }
	inline void set_imageID_4(int32_t value)
	{
		___imageID_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CU3E4__this_5)); }
	inline GLTFSceneImporter_t274320441 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline GLTFSceneImporter_t274320441 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(GLTFSceneImporter_t274320441 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CtextureU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CtextureU3E5__1_6)); }
	inline Texture2D_t3840446185 * get_U3CtextureU3E5__1_6() const { return ___U3CtextureU3E5__1_6; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtextureU3E5__1_6() { return &___U3CtextureU3E5__1_6; }
	inline void set_U3CtextureU3E5__1_6(Texture2D_t3840446185 * value)
	{
		___U3CtextureU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextureU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3CuriU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CuriU3E5__2_7)); }
	inline String_t* get_U3CuriU3E5__2_7() const { return ___U3CuriU3E5__2_7; }
	inline String_t** get_address_of_U3CuriU3E5__2_7() { return &___U3CuriU3E5__2_7; }
	inline void set_U3CuriU3E5__2_7(String_t* value)
	{
		___U3CuriU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuriU3E5__2_7), value);
	}

	inline static int32_t get_offset_of_U3CregexU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CregexU3E5__3_8)); }
	inline Regex_t3657309853 * get_U3CregexU3E5__3_8() const { return ___U3CregexU3E5__3_8; }
	inline Regex_t3657309853 ** get_address_of_U3CregexU3E5__3_8() { return &___U3CregexU3E5__3_8; }
	inline void set_U3CregexU3E5__3_8(Regex_t3657309853 * value)
	{
		___U3CregexU3E5__3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CregexU3E5__3_8), value);
	}

	inline static int32_t get_offset_of_U3CmatchU3E5__4_9() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CmatchU3E5__4_9)); }
	inline Match_t3408321083 * get_U3CmatchU3E5__4_9() const { return ___U3CmatchU3E5__4_9; }
	inline Match_t3408321083 ** get_address_of_U3CmatchU3E5__4_9() { return &___U3CmatchU3E5__4_9; }
	inline void set_U3CmatchU3E5__4_9(Match_t3408321083 * value)
	{
		___U3CmatchU3E5__4_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmatchU3E5__4_9), value);
	}

	inline static int32_t get_offset_of_U3Cbase64DataU3E5__5_10() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3Cbase64DataU3E5__5_10)); }
	inline String_t* get_U3Cbase64DataU3E5__5_10() const { return ___U3Cbase64DataU3E5__5_10; }
	inline String_t** get_address_of_U3Cbase64DataU3E5__5_10() { return &___U3Cbase64DataU3E5__5_10; }
	inline void set_U3Cbase64DataU3E5__5_10(String_t* value)
	{
		___U3Cbase64DataU3E5__5_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cbase64DataU3E5__5_10), value);
	}

	inline static int32_t get_offset_of_U3CtextureDataU3E5__6_11() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CtextureDataU3E5__6_11)); }
	inline ByteU5BU5D_t4116647657* get_U3CtextureDataU3E5__6_11() const { return ___U3CtextureDataU3E5__6_11; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CtextureDataU3E5__6_11() { return &___U3CtextureDataU3E5__6_11; }
	inline void set_U3CtextureDataU3E5__6_11(ByteU5BU5D_t4116647657* value)
	{
		___U3CtextureDataU3E5__6_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextureDataU3E5__6_11), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__7_12() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CwwwU3E5__7_12)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E5__7_12() const { return ___U3CwwwU3E5__7_12; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E5__7_12() { return &___U3CwwwU3E5__7_12; }
	inline void set_U3CwwwU3E5__7_12(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E5__7_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__7_12), value);
	}

	inline static int32_t get_offset_of_U3CtempTextureU3E5__8_13() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CtempTextureU3E5__8_13)); }
	inline Texture2D_t3840446185 * get_U3CtempTextureU3E5__8_13() const { return ___U3CtempTextureU3E5__8_13; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtempTextureU3E5__8_13() { return &___U3CtempTextureU3E5__8_13; }
	inline void set_U3CtempTextureU3E5__8_13(Texture2D_t3840446185 * value)
	{
		___U3CtempTextureU3E5__8_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtempTextureU3E5__8_13), value);
	}

	inline static int32_t get_offset_of_U3CpathToLoadU3E5__9_14() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CpathToLoadU3E5__9_14)); }
	inline String_t* get_U3CpathToLoadU3E5__9_14() const { return ___U3CpathToLoadU3E5__9_14; }
	inline String_t** get_address_of_U3CpathToLoadU3E5__9_14() { return &___U3CpathToLoadU3E5__9_14; }
	inline void set_U3CpathToLoadU3E5__9_14(String_t* value)
	{
		___U3CpathToLoadU3E5__9_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpathToLoadU3E5__9_14), value);
	}

	inline static int32_t get_offset_of_U3CfileU3E5__10_15() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CfileU3E5__10_15)); }
	inline FileStream_t4292183065 * get_U3CfileU3E5__10_15() const { return ___U3CfileU3E5__10_15; }
	inline FileStream_t4292183065 ** get_address_of_U3CfileU3E5__10_15() { return &___U3CfileU3E5__10_15; }
	inline void set_U3CfileU3E5__10_15(FileStream_t4292183065 * value)
	{
		___U3CfileU3E5__10_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileU3E5__10_15), value);
	}

	inline static int32_t get_offset_of_U3CbufferDataU3E5__11_16() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CbufferDataU3E5__11_16)); }
	inline ByteU5BU5D_t4116647657* get_U3CbufferDataU3E5__11_16() const { return ___U3CbufferDataU3E5__11_16; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CbufferDataU3E5__11_16() { return &___U3CbufferDataU3E5__11_16; }
	inline void set_U3CbufferDataU3E5__11_16(ByteU5BU5D_t4116647657* value)
	{
		___U3CbufferDataU3E5__11_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbufferDataU3E5__11_16), value);
	}

	inline static int32_t get_offset_of_U3CbufferViewU3E5__12_17() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CbufferViewU3E5__12_17)); }
	inline BufferView_t2650099126 * get_U3CbufferViewU3E5__12_17() const { return ___U3CbufferViewU3E5__12_17; }
	inline BufferView_t2650099126 ** get_address_of_U3CbufferViewU3E5__12_17() { return &___U3CbufferViewU3E5__12_17; }
	inline void set_U3CbufferViewU3E5__12_17(BufferView_t2650099126 * value)
	{
		___U3CbufferViewU3E5__12_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbufferViewU3E5__12_17), value);
	}

	inline static int32_t get_offset_of_U3CbufferU3E5__13_18() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CbufferU3E5__13_18)); }
	inline Buffer_t869705777 * get_U3CbufferU3E5__13_18() const { return ___U3CbufferU3E5__13_18; }
	inline Buffer_t869705777 ** get_address_of_U3CbufferU3E5__13_18() { return &___U3CbufferU3E5__13_18; }
	inline void set_U3CbufferU3E5__13_18(Buffer_t869705777 * value)
	{
		___U3CbufferU3E5__13_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbufferU3E5__13_18), value);
	}

	inline static int32_t get_offset_of_U3CdataU3E5__14_19() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CdataU3E5__14_19)); }
	inline ByteU5BU5D_t4116647657* get_U3CdataU3E5__14_19() const { return ___U3CdataU3E5__14_19; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CdataU3E5__14_19() { return &___U3CdataU3E5__14_19; }
	inline void set_U3CdataU3E5__14_19(ByteU5BU5D_t4116647657* value)
	{
		___U3CdataU3E5__14_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3E5__14_19), value);
	}

	inline static int32_t get_offset_of_U3CbufferContentsU3E5__15_20() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__33_t1921818520, ___U3CbufferContentsU3E5__15_20)); }
	inline ByteU5BU5D_t4116647657* get_U3CbufferContentsU3E5__15_20() const { return ___U3CbufferContentsU3E5__15_20; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CbufferContentsU3E5__15_20() { return &___U3CbufferContentsU3E5__15_20; }
	inline void set_U3CbufferContentsU3E5__15_20(ByteU5BU5D_t4116647657* value)
	{
		___U3CbufferContentsU3E5__15_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbufferContentsU3E5__15_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADIMAGEU3ED__33_T1921818520_H
#ifndef U3CWAITFORMODELLOADU3ED__14_T282584680_H
#define U3CWAITFORMODELLOADU3ED__14_T282584680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFComponent/<WaitForModelLoad>d__14
struct  U3CWaitForModelLoadU3Ed__14_t282584680  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.GLTFComponent/<WaitForModelLoad>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.GLTFComponent/<WaitForModelLoad>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityGLTF.GLTFComponent UnityGLTF.GLTFComponent/<WaitForModelLoad>d__14::<>4__this
	GLTFComponent_t238219215 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForModelLoadU3Ed__14_t282584680, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForModelLoadU3Ed__14_t282584680, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForModelLoadU3Ed__14_t282584680, ___U3CU3E4__this_2)); }
	inline GLTFComponent_t238219215 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFComponent_t238219215 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFComponent_t238219215 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORMODELLOADU3ED__14_T282584680_H
#ifndef U3CLOADU3ED__21_T2072795234_H
#define U3CLOADU3ED__21_T2072795234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<Load>d__21
struct  U3CLoadU3Ed__21_t2072795234  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<Load>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.GLTFSceneImporter/<Load>d__21::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<Load>d__21::sceneIndex
	int32_t ___sceneIndex_2;
	// System.Boolean UnityGLTF.GLTFSceneImporter/<Load>d__21::isMultithreaded
	bool ___isMultithreaded_3;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<Load>d__21::<>4__this
	GLTFSceneImporter_t274320441 * ___U3CU3E4__this_4;
	// UnityEngine.Networking.UnityWebRequest UnityGLTF.GLTFSceneImporter/<Load>d__21::<www>5__1
	UnityWebRequest_t463507806 * ___U3CwwwU3E5__1_5;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<Load>d__21::<streamLength>5__2
	int32_t ___U3CstreamLengthU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__21_t2072795234, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__21_t2072795234, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_sceneIndex_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__21_t2072795234, ___sceneIndex_2)); }
	inline int32_t get_sceneIndex_2() const { return ___sceneIndex_2; }
	inline int32_t* get_address_of_sceneIndex_2() { return &___sceneIndex_2; }
	inline void set_sceneIndex_2(int32_t value)
	{
		___sceneIndex_2 = value;
	}

	inline static int32_t get_offset_of_isMultithreaded_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__21_t2072795234, ___isMultithreaded_3)); }
	inline bool get_isMultithreaded_3() const { return ___isMultithreaded_3; }
	inline bool* get_address_of_isMultithreaded_3() { return &___isMultithreaded_3; }
	inline void set_isMultithreaded_3(bool value)
	{
		___isMultithreaded_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__21_t2072795234, ___U3CU3E4__this_4)); }
	inline GLTFSceneImporter_t274320441 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline GLTFSceneImporter_t274320441 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(GLTFSceneImporter_t274320441 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__21_t2072795234, ___U3CwwwU3E5__1_5)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E5__1_5() const { return ___U3CwwwU3E5__1_5; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E5__1_5() { return &___U3CwwwU3E5__1_5; }
	inline void set_U3CwwwU3E5__1_5(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E5__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__1_5), value);
	}

	inline static int32_t get_offset_of_U3CstreamLengthU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__21_t2072795234, ___U3CstreamLengthU3E5__2_6)); }
	inline int32_t get_U3CstreamLengthU3E5__2_6() const { return ___U3CstreamLengthU3E5__2_6; }
	inline int32_t* get_address_of_U3CstreamLengthU3E5__2_6() { return &___U3CstreamLengthU3E5__2_6; }
	inline void set_U3CstreamLengthU3E5__2_6(int32_t value)
	{
		___U3CstreamLengthU3E5__2_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3ED__21_T2072795234_H
#ifndef U3CRUNONWORKERTHREADU3ED__2_T1197827754_H
#define U3CRUNONWORKERTHREADU3ED__2_T1197827754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.AsyncAction/<RunOnWorkerThread>d__2
struct  U3CRunOnWorkerThreadU3Ed__2_t1197827754  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.AsyncAction/<RunOnWorkerThread>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.AsyncAction/<RunOnWorkerThread>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Action UnityGLTF.AsyncAction/<RunOnWorkerThread>d__2::action
	Action_t1264377477 * ___action_2;
	// UnityGLTF.AsyncAction UnityGLTF.AsyncAction/<RunOnWorkerThread>d__2::<>4__this
	AsyncAction_t3334821446 * ___U3CU3E4__this_3;
	// UnityGLTF.AsyncAction/<>c__DisplayClass2_0 UnityGLTF.AsyncAction/<RunOnWorkerThread>d__2::<>8__1
	U3CU3Ec__DisplayClass2_0_t4123515534 * ___U3CU3E8__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRunOnWorkerThreadU3Ed__2_t1197827754, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRunOnWorkerThreadU3Ed__2_t1197827754, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CRunOnWorkerThreadU3Ed__2_t1197827754, ___action_2)); }
	inline Action_t1264377477 * get_action_2() const { return ___action_2; }
	inline Action_t1264377477 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t1264377477 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier((&___action_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CRunOnWorkerThreadU3Ed__2_t1197827754, ___U3CU3E4__this_3)); }
	inline AsyncAction_t3334821446 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline AsyncAction_t3334821446 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(AsyncAction_t3334821446 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_4() { return static_cast<int32_t>(offsetof(U3CRunOnWorkerThreadU3Ed__2_t1197827754, ___U3CU3E8__1_4)); }
	inline U3CU3Ec__DisplayClass2_0_t4123515534 * get_U3CU3E8__1_4() const { return ___U3CU3E8__1_4; }
	inline U3CU3Ec__DisplayClass2_0_t4123515534 ** get_address_of_U3CU3E8__1_4() { return &___U3CU3E8__1_4; }
	inline void set_U3CU3E8__1_4(U3CU3Ec__DisplayClass2_0_t4123515534 * value)
	{
		___U3CU3E8__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNONWORKERTHREADU3ED__2_T1197827754_H
#ifndef CONTRIBUTION3_T1917226771_H
#define CONTRIBUTION3_T1917226771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FastSimplexNoise/Contribution3
struct  Contribution3_t1917226771  : public RuntimeObject
{
public:
	// System.Double FastSimplexNoise/Contribution3::dx
	double ___dx_0;
	// System.Double FastSimplexNoise/Contribution3::dy
	double ___dy_1;
	// System.Double FastSimplexNoise/Contribution3::dz
	double ___dz_2;
	// System.Int32 FastSimplexNoise/Contribution3::xsb
	int32_t ___xsb_3;
	// System.Int32 FastSimplexNoise/Contribution3::ysb
	int32_t ___ysb_4;
	// System.Int32 FastSimplexNoise/Contribution3::zsb
	int32_t ___zsb_5;
	// FastSimplexNoise/Contribution3 FastSimplexNoise/Contribution3::Next
	Contribution3_t1917226771 * ___Next_6;

public:
	inline static int32_t get_offset_of_dx_0() { return static_cast<int32_t>(offsetof(Contribution3_t1917226771, ___dx_0)); }
	inline double get_dx_0() const { return ___dx_0; }
	inline double* get_address_of_dx_0() { return &___dx_0; }
	inline void set_dx_0(double value)
	{
		___dx_0 = value;
	}

	inline static int32_t get_offset_of_dy_1() { return static_cast<int32_t>(offsetof(Contribution3_t1917226771, ___dy_1)); }
	inline double get_dy_1() const { return ___dy_1; }
	inline double* get_address_of_dy_1() { return &___dy_1; }
	inline void set_dy_1(double value)
	{
		___dy_1 = value;
	}

	inline static int32_t get_offset_of_dz_2() { return static_cast<int32_t>(offsetof(Contribution3_t1917226771, ___dz_2)); }
	inline double get_dz_2() const { return ___dz_2; }
	inline double* get_address_of_dz_2() { return &___dz_2; }
	inline void set_dz_2(double value)
	{
		___dz_2 = value;
	}

	inline static int32_t get_offset_of_xsb_3() { return static_cast<int32_t>(offsetof(Contribution3_t1917226771, ___xsb_3)); }
	inline int32_t get_xsb_3() const { return ___xsb_3; }
	inline int32_t* get_address_of_xsb_3() { return &___xsb_3; }
	inline void set_xsb_3(int32_t value)
	{
		___xsb_3 = value;
	}

	inline static int32_t get_offset_of_ysb_4() { return static_cast<int32_t>(offsetof(Contribution3_t1917226771, ___ysb_4)); }
	inline int32_t get_ysb_4() const { return ___ysb_4; }
	inline int32_t* get_address_of_ysb_4() { return &___ysb_4; }
	inline void set_ysb_4(int32_t value)
	{
		___ysb_4 = value;
	}

	inline static int32_t get_offset_of_zsb_5() { return static_cast<int32_t>(offsetof(Contribution3_t1917226771, ___zsb_5)); }
	inline int32_t get_zsb_5() const { return ___zsb_5; }
	inline int32_t* get_address_of_zsb_5() { return &___zsb_5; }
	inline void set_zsb_5(int32_t value)
	{
		___zsb_5 = value;
	}

	inline static int32_t get_offset_of_Next_6() { return static_cast<int32_t>(offsetof(Contribution3_t1917226771, ___Next_6)); }
	inline Contribution3_t1917226771 * get_Next_6() const { return ___Next_6; }
	inline Contribution3_t1917226771 ** get_address_of_Next_6() { return &___Next_6; }
	inline void set_Next_6(Contribution3_t1917226771 * value)
	{
		___Next_6 = value;
		Il2CppCodeGenWriteBarrier((&___Next_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRIBUTION3_T1917226771_H
#ifndef CONTRIBUTION4_T1916768019_H
#define CONTRIBUTION4_T1916768019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FastSimplexNoise/Contribution4
struct  Contribution4_t1916768019  : public RuntimeObject
{
public:
	// System.Double FastSimplexNoise/Contribution4::dx
	double ___dx_0;
	// System.Double FastSimplexNoise/Contribution4::dy
	double ___dy_1;
	// System.Double FastSimplexNoise/Contribution4::dz
	double ___dz_2;
	// System.Double FastSimplexNoise/Contribution4::dw
	double ___dw_3;
	// System.Int32 FastSimplexNoise/Contribution4::xsb
	int32_t ___xsb_4;
	// System.Int32 FastSimplexNoise/Contribution4::ysb
	int32_t ___ysb_5;
	// System.Int32 FastSimplexNoise/Contribution4::zsb
	int32_t ___zsb_6;
	// System.Int32 FastSimplexNoise/Contribution4::wsb
	int32_t ___wsb_7;
	// FastSimplexNoise/Contribution4 FastSimplexNoise/Contribution4::Next
	Contribution4_t1916768019 * ___Next_8;

public:
	inline static int32_t get_offset_of_dx_0() { return static_cast<int32_t>(offsetof(Contribution4_t1916768019, ___dx_0)); }
	inline double get_dx_0() const { return ___dx_0; }
	inline double* get_address_of_dx_0() { return &___dx_0; }
	inline void set_dx_0(double value)
	{
		___dx_0 = value;
	}

	inline static int32_t get_offset_of_dy_1() { return static_cast<int32_t>(offsetof(Contribution4_t1916768019, ___dy_1)); }
	inline double get_dy_1() const { return ___dy_1; }
	inline double* get_address_of_dy_1() { return &___dy_1; }
	inline void set_dy_1(double value)
	{
		___dy_1 = value;
	}

	inline static int32_t get_offset_of_dz_2() { return static_cast<int32_t>(offsetof(Contribution4_t1916768019, ___dz_2)); }
	inline double get_dz_2() const { return ___dz_2; }
	inline double* get_address_of_dz_2() { return &___dz_2; }
	inline void set_dz_2(double value)
	{
		___dz_2 = value;
	}

	inline static int32_t get_offset_of_dw_3() { return static_cast<int32_t>(offsetof(Contribution4_t1916768019, ___dw_3)); }
	inline double get_dw_3() const { return ___dw_3; }
	inline double* get_address_of_dw_3() { return &___dw_3; }
	inline void set_dw_3(double value)
	{
		___dw_3 = value;
	}

	inline static int32_t get_offset_of_xsb_4() { return static_cast<int32_t>(offsetof(Contribution4_t1916768019, ___xsb_4)); }
	inline int32_t get_xsb_4() const { return ___xsb_4; }
	inline int32_t* get_address_of_xsb_4() { return &___xsb_4; }
	inline void set_xsb_4(int32_t value)
	{
		___xsb_4 = value;
	}

	inline static int32_t get_offset_of_ysb_5() { return static_cast<int32_t>(offsetof(Contribution4_t1916768019, ___ysb_5)); }
	inline int32_t get_ysb_5() const { return ___ysb_5; }
	inline int32_t* get_address_of_ysb_5() { return &___ysb_5; }
	inline void set_ysb_5(int32_t value)
	{
		___ysb_5 = value;
	}

	inline static int32_t get_offset_of_zsb_6() { return static_cast<int32_t>(offsetof(Contribution4_t1916768019, ___zsb_6)); }
	inline int32_t get_zsb_6() const { return ___zsb_6; }
	inline int32_t* get_address_of_zsb_6() { return &___zsb_6; }
	inline void set_zsb_6(int32_t value)
	{
		___zsb_6 = value;
	}

	inline static int32_t get_offset_of_wsb_7() { return static_cast<int32_t>(offsetof(Contribution4_t1916768019, ___wsb_7)); }
	inline int32_t get_wsb_7() const { return ___wsb_7; }
	inline int32_t* get_address_of_wsb_7() { return &___wsb_7; }
	inline void set_wsb_7(int32_t value)
	{
		___wsb_7 = value;
	}

	inline static int32_t get_offset_of_Next_8() { return static_cast<int32_t>(offsetof(Contribution4_t1916768019, ___Next_8)); }
	inline Contribution4_t1916768019 * get_Next_8() const { return ___Next_8; }
	inline Contribution4_t1916768019 ** get_address_of_Next_8() { return &___Next_8; }
	inline void set_Next_8(Contribution4_t1916768019 * value)
	{
		___Next_8 = value;
		Il2CppCodeGenWriteBarrier((&___Next_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRIBUTION4_T1916768019_H
#ifndef FASTSIMPLEXNOISE_T743524795_H
#define FASTSIMPLEXNOISE_T743524795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FastSimplexNoise
struct  FastSimplexNoise_t743524795  : public RuntimeObject
{
public:
	// System.Byte[] FastSimplexNoise::perm
	ByteU5BU5D_t4116647657* ___perm_11;
	// System.Byte[] FastSimplexNoise::perm2D
	ByteU5BU5D_t4116647657* ___perm2D_12;
	// System.Byte[] FastSimplexNoise::perm3D
	ByteU5BU5D_t4116647657* ___perm3D_13;
	// System.Byte[] FastSimplexNoise::perm4D
	ByteU5BU5D_t4116647657* ___perm4D_14;

public:
	inline static int32_t get_offset_of_perm_11() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795, ___perm_11)); }
	inline ByteU5BU5D_t4116647657* get_perm_11() const { return ___perm_11; }
	inline ByteU5BU5D_t4116647657** get_address_of_perm_11() { return &___perm_11; }
	inline void set_perm_11(ByteU5BU5D_t4116647657* value)
	{
		___perm_11 = value;
		Il2CppCodeGenWriteBarrier((&___perm_11), value);
	}

	inline static int32_t get_offset_of_perm2D_12() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795, ___perm2D_12)); }
	inline ByteU5BU5D_t4116647657* get_perm2D_12() const { return ___perm2D_12; }
	inline ByteU5BU5D_t4116647657** get_address_of_perm2D_12() { return &___perm2D_12; }
	inline void set_perm2D_12(ByteU5BU5D_t4116647657* value)
	{
		___perm2D_12 = value;
		Il2CppCodeGenWriteBarrier((&___perm2D_12), value);
	}

	inline static int32_t get_offset_of_perm3D_13() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795, ___perm3D_13)); }
	inline ByteU5BU5D_t4116647657* get_perm3D_13() const { return ___perm3D_13; }
	inline ByteU5BU5D_t4116647657** get_address_of_perm3D_13() { return &___perm3D_13; }
	inline void set_perm3D_13(ByteU5BU5D_t4116647657* value)
	{
		___perm3D_13 = value;
		Il2CppCodeGenWriteBarrier((&___perm3D_13), value);
	}

	inline static int32_t get_offset_of_perm4D_14() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795, ___perm4D_14)); }
	inline ByteU5BU5D_t4116647657* get_perm4D_14() const { return ___perm4D_14; }
	inline ByteU5BU5D_t4116647657** get_address_of_perm4D_14() { return &___perm4D_14; }
	inline void set_perm4D_14(ByteU5BU5D_t4116647657* value)
	{
		___perm4D_14 = value;
		Il2CppCodeGenWriteBarrier((&___perm4D_14), value);
	}
};

struct FastSimplexNoise_t743524795_StaticFields
{
public:
	// System.Double[] FastSimplexNoise::gradients2D
	DoubleU5BU5D_t3413330114* ___gradients2D_15;
	// System.Double[] FastSimplexNoise::gradients3D
	DoubleU5BU5D_t3413330114* ___gradients3D_16;
	// System.Double[] FastSimplexNoise::gradients4D
	DoubleU5BU5D_t3413330114* ___gradients4D_17;
	// System.Int32[] FastSimplexNoise::p2D
	Int32U5BU5D_t385246372* ___p2D_18;
	// System.Int32[] FastSimplexNoise::p3D
	Int32U5BU5D_t385246372* ___p3D_19;
	// System.Int32[] FastSimplexNoise::p4D
	Int32U5BU5D_t385246372* ___p4D_20;
	// System.Int32[] FastSimplexNoise::lookupPairs2D
	Int32U5BU5D_t385246372* ___lookupPairs2D_21;
	// System.Int32[] FastSimplexNoise::lookupPairs3D
	Int32U5BU5D_t385246372* ___lookupPairs3D_22;
	// System.Int32[] FastSimplexNoise::lookupPairs4D
	Int32U5BU5D_t385246372* ___lookupPairs4D_23;
	// System.Int32[][] FastSimplexNoise::base2D
	Int32U5BU5DU5BU5D_t3365920845* ___base2D_24;
	// System.Int32[][] FastSimplexNoise::base3D
	Int32U5BU5DU5BU5D_t3365920845* ___base3D_25;
	// System.Int32[][] FastSimplexNoise::base4D
	Int32U5BU5DU5BU5D_t3365920845* ___base4D_26;
	// FastSimplexNoise/Contribution2[] FastSimplexNoise::lookup2D
	Contribution2U5BU5D_t2459528002* ___lookup2D_27;
	// FastSimplexNoise/Contribution3[] FastSimplexNoise::lookup3D
	Contribution3U5BU5D_t896822082* ___lookup3D_28;
	// FastSimplexNoise/Contribution4[] FastSimplexNoise::lookup4D
	Contribution4U5BU5D_t3245828930* ___lookup4D_29;

public:
	inline static int32_t get_offset_of_gradients2D_15() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795_StaticFields, ___gradients2D_15)); }
	inline DoubleU5BU5D_t3413330114* get_gradients2D_15() const { return ___gradients2D_15; }
	inline DoubleU5BU5D_t3413330114** get_address_of_gradients2D_15() { return &___gradients2D_15; }
	inline void set_gradients2D_15(DoubleU5BU5D_t3413330114* value)
	{
		___gradients2D_15 = value;
		Il2CppCodeGenWriteBarrier((&___gradients2D_15), value);
	}

	inline static int32_t get_offset_of_gradients3D_16() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795_StaticFields, ___gradients3D_16)); }
	inline DoubleU5BU5D_t3413330114* get_gradients3D_16() const { return ___gradients3D_16; }
	inline DoubleU5BU5D_t3413330114** get_address_of_gradients3D_16() { return &___gradients3D_16; }
	inline void set_gradients3D_16(DoubleU5BU5D_t3413330114* value)
	{
		___gradients3D_16 = value;
		Il2CppCodeGenWriteBarrier((&___gradients3D_16), value);
	}

	inline static int32_t get_offset_of_gradients4D_17() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795_StaticFields, ___gradients4D_17)); }
	inline DoubleU5BU5D_t3413330114* get_gradients4D_17() const { return ___gradients4D_17; }
	inline DoubleU5BU5D_t3413330114** get_address_of_gradients4D_17() { return &___gradients4D_17; }
	inline void set_gradients4D_17(DoubleU5BU5D_t3413330114* value)
	{
		___gradients4D_17 = value;
		Il2CppCodeGenWriteBarrier((&___gradients4D_17), value);
	}

	inline static int32_t get_offset_of_p2D_18() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795_StaticFields, ___p2D_18)); }
	inline Int32U5BU5D_t385246372* get_p2D_18() const { return ___p2D_18; }
	inline Int32U5BU5D_t385246372** get_address_of_p2D_18() { return &___p2D_18; }
	inline void set_p2D_18(Int32U5BU5D_t385246372* value)
	{
		___p2D_18 = value;
		Il2CppCodeGenWriteBarrier((&___p2D_18), value);
	}

	inline static int32_t get_offset_of_p3D_19() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795_StaticFields, ___p3D_19)); }
	inline Int32U5BU5D_t385246372* get_p3D_19() const { return ___p3D_19; }
	inline Int32U5BU5D_t385246372** get_address_of_p3D_19() { return &___p3D_19; }
	inline void set_p3D_19(Int32U5BU5D_t385246372* value)
	{
		___p3D_19 = value;
		Il2CppCodeGenWriteBarrier((&___p3D_19), value);
	}

	inline static int32_t get_offset_of_p4D_20() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795_StaticFields, ___p4D_20)); }
	inline Int32U5BU5D_t385246372* get_p4D_20() const { return ___p4D_20; }
	inline Int32U5BU5D_t385246372** get_address_of_p4D_20() { return &___p4D_20; }
	inline void set_p4D_20(Int32U5BU5D_t385246372* value)
	{
		___p4D_20 = value;
		Il2CppCodeGenWriteBarrier((&___p4D_20), value);
	}

	inline static int32_t get_offset_of_lookupPairs2D_21() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795_StaticFields, ___lookupPairs2D_21)); }
	inline Int32U5BU5D_t385246372* get_lookupPairs2D_21() const { return ___lookupPairs2D_21; }
	inline Int32U5BU5D_t385246372** get_address_of_lookupPairs2D_21() { return &___lookupPairs2D_21; }
	inline void set_lookupPairs2D_21(Int32U5BU5D_t385246372* value)
	{
		___lookupPairs2D_21 = value;
		Il2CppCodeGenWriteBarrier((&___lookupPairs2D_21), value);
	}

	inline static int32_t get_offset_of_lookupPairs3D_22() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795_StaticFields, ___lookupPairs3D_22)); }
	inline Int32U5BU5D_t385246372* get_lookupPairs3D_22() const { return ___lookupPairs3D_22; }
	inline Int32U5BU5D_t385246372** get_address_of_lookupPairs3D_22() { return &___lookupPairs3D_22; }
	inline void set_lookupPairs3D_22(Int32U5BU5D_t385246372* value)
	{
		___lookupPairs3D_22 = value;
		Il2CppCodeGenWriteBarrier((&___lookupPairs3D_22), value);
	}

	inline static int32_t get_offset_of_lookupPairs4D_23() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795_StaticFields, ___lookupPairs4D_23)); }
	inline Int32U5BU5D_t385246372* get_lookupPairs4D_23() const { return ___lookupPairs4D_23; }
	inline Int32U5BU5D_t385246372** get_address_of_lookupPairs4D_23() { return &___lookupPairs4D_23; }
	inline void set_lookupPairs4D_23(Int32U5BU5D_t385246372* value)
	{
		___lookupPairs4D_23 = value;
		Il2CppCodeGenWriteBarrier((&___lookupPairs4D_23), value);
	}

	inline static int32_t get_offset_of_base2D_24() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795_StaticFields, ___base2D_24)); }
	inline Int32U5BU5DU5BU5D_t3365920845* get_base2D_24() const { return ___base2D_24; }
	inline Int32U5BU5DU5BU5D_t3365920845** get_address_of_base2D_24() { return &___base2D_24; }
	inline void set_base2D_24(Int32U5BU5DU5BU5D_t3365920845* value)
	{
		___base2D_24 = value;
		Il2CppCodeGenWriteBarrier((&___base2D_24), value);
	}

	inline static int32_t get_offset_of_base3D_25() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795_StaticFields, ___base3D_25)); }
	inline Int32U5BU5DU5BU5D_t3365920845* get_base3D_25() const { return ___base3D_25; }
	inline Int32U5BU5DU5BU5D_t3365920845** get_address_of_base3D_25() { return &___base3D_25; }
	inline void set_base3D_25(Int32U5BU5DU5BU5D_t3365920845* value)
	{
		___base3D_25 = value;
		Il2CppCodeGenWriteBarrier((&___base3D_25), value);
	}

	inline static int32_t get_offset_of_base4D_26() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795_StaticFields, ___base4D_26)); }
	inline Int32U5BU5DU5BU5D_t3365920845* get_base4D_26() const { return ___base4D_26; }
	inline Int32U5BU5DU5BU5D_t3365920845** get_address_of_base4D_26() { return &___base4D_26; }
	inline void set_base4D_26(Int32U5BU5DU5BU5D_t3365920845* value)
	{
		___base4D_26 = value;
		Il2CppCodeGenWriteBarrier((&___base4D_26), value);
	}

	inline static int32_t get_offset_of_lookup2D_27() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795_StaticFields, ___lookup2D_27)); }
	inline Contribution2U5BU5D_t2459528002* get_lookup2D_27() const { return ___lookup2D_27; }
	inline Contribution2U5BU5D_t2459528002** get_address_of_lookup2D_27() { return &___lookup2D_27; }
	inline void set_lookup2D_27(Contribution2U5BU5D_t2459528002* value)
	{
		___lookup2D_27 = value;
		Il2CppCodeGenWriteBarrier((&___lookup2D_27), value);
	}

	inline static int32_t get_offset_of_lookup3D_28() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795_StaticFields, ___lookup3D_28)); }
	inline Contribution3U5BU5D_t896822082* get_lookup3D_28() const { return ___lookup3D_28; }
	inline Contribution3U5BU5D_t896822082** get_address_of_lookup3D_28() { return &___lookup3D_28; }
	inline void set_lookup3D_28(Contribution3U5BU5D_t896822082* value)
	{
		___lookup3D_28 = value;
		Il2CppCodeGenWriteBarrier((&___lookup3D_28), value);
	}

	inline static int32_t get_offset_of_lookup4D_29() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t743524795_StaticFields, ___lookup4D_29)); }
	inline Contribution4U5BU5D_t3245828930* get_lookup4D_29() const { return ___lookup4D_29; }
	inline Contribution4U5BU5D_t3245828930** get_address_of_lookup4D_29() { return &___lookup4D_29; }
	inline void set_lookup4D_29(Contribution4U5BU5D_t3245828930* value)
	{
		___lookup4D_29 = value;
		Il2CppCodeGenWriteBarrier((&___lookup4D_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTSIMPLEXNOISE_T743524795_H
#ifndef CONTRIBUTION2_T1917161235_H
#define CONTRIBUTION2_T1917161235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FastSimplexNoise/Contribution2
struct  Contribution2_t1917161235  : public RuntimeObject
{
public:
	// System.Double FastSimplexNoise/Contribution2::dx
	double ___dx_0;
	// System.Double FastSimplexNoise/Contribution2::dy
	double ___dy_1;
	// System.Int32 FastSimplexNoise/Contribution2::xsb
	int32_t ___xsb_2;
	// System.Int32 FastSimplexNoise/Contribution2::ysb
	int32_t ___ysb_3;
	// FastSimplexNoise/Contribution2 FastSimplexNoise/Contribution2::Next
	Contribution2_t1917161235 * ___Next_4;

public:
	inline static int32_t get_offset_of_dx_0() { return static_cast<int32_t>(offsetof(Contribution2_t1917161235, ___dx_0)); }
	inline double get_dx_0() const { return ___dx_0; }
	inline double* get_address_of_dx_0() { return &___dx_0; }
	inline void set_dx_0(double value)
	{
		___dx_0 = value;
	}

	inline static int32_t get_offset_of_dy_1() { return static_cast<int32_t>(offsetof(Contribution2_t1917161235, ___dy_1)); }
	inline double get_dy_1() const { return ___dy_1; }
	inline double* get_address_of_dy_1() { return &___dy_1; }
	inline void set_dy_1(double value)
	{
		___dy_1 = value;
	}

	inline static int32_t get_offset_of_xsb_2() { return static_cast<int32_t>(offsetof(Contribution2_t1917161235, ___xsb_2)); }
	inline int32_t get_xsb_2() const { return ___xsb_2; }
	inline int32_t* get_address_of_xsb_2() { return &___xsb_2; }
	inline void set_xsb_2(int32_t value)
	{
		___xsb_2 = value;
	}

	inline static int32_t get_offset_of_ysb_3() { return static_cast<int32_t>(offsetof(Contribution2_t1917161235, ___ysb_3)); }
	inline int32_t get_ysb_3() const { return ___ysb_3; }
	inline int32_t* get_address_of_ysb_3() { return &___ysb_3; }
	inline void set_ysb_3(int32_t value)
	{
		___ysb_3 = value;
	}

	inline static int32_t get_offset_of_Next_4() { return static_cast<int32_t>(offsetof(Contribution2_t1917161235, ___Next_4)); }
	inline Contribution2_t1917161235 * get_Next_4() const { return ___Next_4; }
	inline Contribution2_t1917161235 ** get_address_of_Next_4() { return &___Next_4; }
	inline void set_Next_4(Contribution2_t1917161235 * value)
	{
		___Next_4 = value;
		Il2CppCodeGenWriteBarrier((&___Next_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRIBUTION2_T1917161235_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T4123515534_H
#define U3CU3EC__DISPLAYCLASS2_0_T4123515534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.AsyncAction/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t4123515534  : public RuntimeObject
{
public:
	// System.Action UnityGLTF.AsyncAction/<>c__DisplayClass2_0::action
	Action_t1264377477 * ___action_0;
	// UnityGLTF.AsyncAction UnityGLTF.AsyncAction/<>c__DisplayClass2_0::<>4__this
	AsyncAction_t3334821446 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t4123515534, ___action_0)); }
	inline Action_t1264377477 * get_action_0() const { return ___action_0; }
	inline Action_t1264377477 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t1264377477 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t4123515534, ___U3CU3E4__this_1)); }
	inline AsyncAction_t3334821446 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline AsyncAction_t3334821446 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(AsyncAction_t3334821446 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T4123515534_H
#ifndef BOUNDSEXTENSIONS_T3769520328_H
#define BOUNDSEXTENSIONS_T3769520328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoundsExtensions
struct  BoundsExtensions_t3769520328  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDSEXTENSIONS_T3769520328_H
#ifndef U3CRESTARTSPEECHSYSTEMU3ED__19_T3166134508_H
#define U3CRESTARTSPEECHSYSTEMU3ED__19_T3166134508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MicrophoneManager/<RestartSpeechSystem>d__19
struct  U3CRestartSpeechSystemU3Ed__19_t3166134508  : public RuntimeObject
{
public:
	// System.Int32 MicrophoneManager/<RestartSpeechSystem>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MicrophoneManager/<RestartSpeechSystem>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.InputModule.KeyboardManager MicrophoneManager/<RestartSpeechSystem>d__19::keywordToStart
	KeyboardManager_t1786564518 * ___keywordToStart_2;
	// MicrophoneManager MicrophoneManager/<RestartSpeechSystem>d__19::<>4__this
	MicrophoneManager_t2580314274 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRestartSpeechSystemU3Ed__19_t3166134508, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRestartSpeechSystemU3Ed__19_t3166134508, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_keywordToStart_2() { return static_cast<int32_t>(offsetof(U3CRestartSpeechSystemU3Ed__19_t3166134508, ___keywordToStart_2)); }
	inline KeyboardManager_t1786564518 * get_keywordToStart_2() const { return ___keywordToStart_2; }
	inline KeyboardManager_t1786564518 ** get_address_of_keywordToStart_2() { return &___keywordToStart_2; }
	inline void set_keywordToStart_2(KeyboardManager_t1786564518 * value)
	{
		___keywordToStart_2 = value;
		Il2CppCodeGenWriteBarrier((&___keywordToStart_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CRestartSpeechSystemU3Ed__19_t3166134508, ___U3CU3E4__this_3)); }
	inline MicrophoneManager_t2580314274 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline MicrophoneManager_t2580314274 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(MicrophoneManager_t2580314274 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESTARTSPEECHSYSTEMU3ED__19_T3166134508_H
#ifndef ASYNCACTION_T3334821446_H
#define ASYNCACTION_T3334821446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.AsyncAction
struct  AsyncAction_t3334821446  : public RuntimeObject
{
public:
	// System.Boolean UnityGLTF.AsyncAction::_workerThreadRunning
	bool ____workerThreadRunning_0;
	// System.Exception UnityGLTF.AsyncAction::_savedException
	Exception_t1436737249 * ____savedException_1;

public:
	inline static int32_t get_offset_of__workerThreadRunning_0() { return static_cast<int32_t>(offsetof(AsyncAction_t3334821446, ____workerThreadRunning_0)); }
	inline bool get__workerThreadRunning_0() const { return ____workerThreadRunning_0; }
	inline bool* get_address_of__workerThreadRunning_0() { return &____workerThreadRunning_0; }
	inline void set__workerThreadRunning_0(bool value)
	{
		____workerThreadRunning_0 = value;
	}

	inline static int32_t get_offset_of__savedException_1() { return static_cast<int32_t>(offsetof(AsyncAction_t3334821446, ____savedException_1)); }
	inline Exception_t1436737249 * get__savedException_1() const { return ____savedException_1; }
	inline Exception_t1436737249 ** get_address_of__savedException_1() { return &____savedException_1; }
	inline void set__savedException_1(Exception_t1436737249 * value)
	{
		____savedException_1 = value;
		Il2CppCodeGenWriteBarrier((&____savedException_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCACTION_T3334821446_H
#ifndef MATERIALCACHEDATA_T2754836532_H
#define MATERIALCACHEDATA_T2754836532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Cache.MaterialCacheData
struct  MaterialCacheData_t2754836532  : public RuntimeObject
{
public:
	// UnityEngine.Material UnityGLTF.Cache.MaterialCacheData::<UnityMaterial>k__BackingField
	Material_t340375123 * ___U3CUnityMaterialU3Ek__BackingField_0;
	// UnityEngine.Material UnityGLTF.Cache.MaterialCacheData::<UnityMaterialWithVertexColor>k__BackingField
	Material_t340375123 * ___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1;
	// GLTF.Schema.Material UnityGLTF.Cache.MaterialCacheData::<GLTFMaterial>k__BackingField
	Material_t2790518843 * ___U3CGLTFMaterialU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CUnityMaterialU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MaterialCacheData_t2754836532, ___U3CUnityMaterialU3Ek__BackingField_0)); }
	inline Material_t340375123 * get_U3CUnityMaterialU3Ek__BackingField_0() const { return ___U3CUnityMaterialU3Ek__BackingField_0; }
	inline Material_t340375123 ** get_address_of_U3CUnityMaterialU3Ek__BackingField_0() { return &___U3CUnityMaterialU3Ek__BackingField_0; }
	inline void set_U3CUnityMaterialU3Ek__BackingField_0(Material_t340375123 * value)
	{
		___U3CUnityMaterialU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnityMaterialU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CUnityMaterialWithVertexColorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MaterialCacheData_t2754836532, ___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1)); }
	inline Material_t340375123 * get_U3CUnityMaterialWithVertexColorU3Ek__BackingField_1() const { return ___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1; }
	inline Material_t340375123 ** get_address_of_U3CUnityMaterialWithVertexColorU3Ek__BackingField_1() { return &___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1; }
	inline void set_U3CUnityMaterialWithVertexColorU3Ek__BackingField_1(Material_t340375123 * value)
	{
		___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CGLTFMaterialU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MaterialCacheData_t2754836532, ___U3CGLTFMaterialU3Ek__BackingField_2)); }
	inline Material_t2790518843 * get_U3CGLTFMaterialU3Ek__BackingField_2() const { return ___U3CGLTFMaterialU3Ek__BackingField_2; }
	inline Material_t2790518843 ** get_address_of_U3CGLTFMaterialU3Ek__BackingField_2() { return &___U3CGLTFMaterialU3Ek__BackingField_2; }
	inline void set_U3CGLTFMaterialU3Ek__BackingField_2(Material_t2790518843 * value)
	{
		___U3CGLTFMaterialU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGLTFMaterialU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALCACHEDATA_T2754836532_H
#ifndef ASSETCACHE_T783611398_H
#define ASSETCACHE_T783611398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Cache.AssetCache
struct  AssetCache_t783611398  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D[] UnityGLTF.Cache.AssetCache::<ImageCache>k__BackingField
	Texture2DU5BU5D_t149664596* ___U3CImageCacheU3Ek__BackingField_0;
	// UnityEngine.Texture[] UnityGLTF.Cache.AssetCache::<TextureCache>k__BackingField
	TextureU5BU5D_t908295702* ___U3CTextureCacheU3Ek__BackingField_1;
	// UnityGLTF.Cache.MaterialCacheData[] UnityGLTF.Cache.AssetCache::<MaterialCache>k__BackingField
	MaterialCacheDataU5BU5D_t484490365* ___U3CMaterialCacheU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]> UnityGLTF.Cache.AssetCache::<BufferCache>k__BackingField
	Dictionary_2_t3005360988 * ___U3CBufferCacheU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<UnityGLTF.Cache.MeshCacheData[]> UnityGLTF.Cache.AssetCache::<MeshCache>k__BackingField
	List_1_t695246165 * ___U3CMeshCacheU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CImageCacheU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AssetCache_t783611398, ___U3CImageCacheU3Ek__BackingField_0)); }
	inline Texture2DU5BU5D_t149664596* get_U3CImageCacheU3Ek__BackingField_0() const { return ___U3CImageCacheU3Ek__BackingField_0; }
	inline Texture2DU5BU5D_t149664596** get_address_of_U3CImageCacheU3Ek__BackingField_0() { return &___U3CImageCacheU3Ek__BackingField_0; }
	inline void set_U3CImageCacheU3Ek__BackingField_0(Texture2DU5BU5D_t149664596* value)
	{
		___U3CImageCacheU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageCacheU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTextureCacheU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AssetCache_t783611398, ___U3CTextureCacheU3Ek__BackingField_1)); }
	inline TextureU5BU5D_t908295702* get_U3CTextureCacheU3Ek__BackingField_1() const { return ___U3CTextureCacheU3Ek__BackingField_1; }
	inline TextureU5BU5D_t908295702** get_address_of_U3CTextureCacheU3Ek__BackingField_1() { return &___U3CTextureCacheU3Ek__BackingField_1; }
	inline void set_U3CTextureCacheU3Ek__BackingField_1(TextureU5BU5D_t908295702* value)
	{
		___U3CTextureCacheU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextureCacheU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CMaterialCacheU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AssetCache_t783611398, ___U3CMaterialCacheU3Ek__BackingField_2)); }
	inline MaterialCacheDataU5BU5D_t484490365* get_U3CMaterialCacheU3Ek__BackingField_2() const { return ___U3CMaterialCacheU3Ek__BackingField_2; }
	inline MaterialCacheDataU5BU5D_t484490365** get_address_of_U3CMaterialCacheU3Ek__BackingField_2() { return &___U3CMaterialCacheU3Ek__BackingField_2; }
	inline void set_U3CMaterialCacheU3Ek__BackingField_2(MaterialCacheDataU5BU5D_t484490365* value)
	{
		___U3CMaterialCacheU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaterialCacheU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CBufferCacheU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AssetCache_t783611398, ___U3CBufferCacheU3Ek__BackingField_3)); }
	inline Dictionary_2_t3005360988 * get_U3CBufferCacheU3Ek__BackingField_3() const { return ___U3CBufferCacheU3Ek__BackingField_3; }
	inline Dictionary_2_t3005360988 ** get_address_of_U3CBufferCacheU3Ek__BackingField_3() { return &___U3CBufferCacheU3Ek__BackingField_3; }
	inline void set_U3CBufferCacheU3Ek__BackingField_3(Dictionary_2_t3005360988 * value)
	{
		___U3CBufferCacheU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBufferCacheU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CMeshCacheU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AssetCache_t783611398, ___U3CMeshCacheU3Ek__BackingField_4)); }
	inline List_1_t695246165 * get_U3CMeshCacheU3Ek__BackingField_4() const { return ___U3CMeshCacheU3Ek__BackingField_4; }
	inline List_1_t695246165 ** get_address_of_U3CMeshCacheU3Ek__BackingField_4() { return &___U3CMeshCacheU3Ek__BackingField_4; }
	inline void set_U3CMeshCacheU3Ek__BackingField_4(List_1_t695246165 * value)
	{
		___U3CMeshCacheU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMeshCacheU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETCACHE_T783611398_H
#ifndef EXCEPTION_T1436737249_H
#define EXCEPTION_T1436737249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1436737249  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t1436737249 * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____innerException_4)); }
	inline Exception_t1436737249 * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t1436737249 ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t1436737249 * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t1436737249_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t1436737249_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t1436737249_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1436737249_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t1436737249_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1436737249_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T1436737249_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef MESHCACHEDATA_T1944180602_H
#define MESHCACHEDATA_T1944180602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Cache.MeshCacheData
struct  MeshCacheData_t1944180602  : public RuntimeObject
{
public:
	// UnityEngine.Mesh UnityGLTF.Cache.MeshCacheData::<LoadedMesh>k__BackingField
	Mesh_t3648964284 * ___U3CLoadedMeshU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,GLTF.AttributeAccessor> UnityGLTF.Cache.MeshCacheData::<MeshAttributes>k__BackingField
	Dictionary_2_t691531204 * ___U3CMeshAttributesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CLoadedMeshU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MeshCacheData_t1944180602, ___U3CLoadedMeshU3Ek__BackingField_0)); }
	inline Mesh_t3648964284 * get_U3CLoadedMeshU3Ek__BackingField_0() const { return ___U3CLoadedMeshU3Ek__BackingField_0; }
	inline Mesh_t3648964284 ** get_address_of_U3CLoadedMeshU3Ek__BackingField_0() { return &___U3CLoadedMeshU3Ek__BackingField_0; }
	inline void set_U3CLoadedMeshU3Ek__BackingField_0(Mesh_t3648964284 * value)
	{
		___U3CLoadedMeshU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadedMeshU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CMeshAttributesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MeshCacheData_t1944180602, ___U3CMeshAttributesU3Ek__BackingField_1)); }
	inline Dictionary_2_t691531204 * get_U3CMeshAttributesU3Ek__BackingField_1() const { return ___U3CMeshAttributesU3Ek__BackingField_1; }
	inline Dictionary_2_t691531204 ** get_address_of_U3CMeshAttributesU3Ek__BackingField_1() { return &___U3CMeshAttributesU3Ek__BackingField_1; }
	inline void set_U3CMeshAttributesU3Ek__BackingField_1(Dictionary_2_t691531204 * value)
	{
		___U3CMeshAttributesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMeshAttributesU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHCACHEDATA_T1944180602_H
#ifndef U3CLOADBUFFERU3ED__34_T2608884096_H
#define U3CLOADBUFFERU3ED__34_T2608884096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/<LoadBuffer>d__34
struct  U3CLoadBufferU3Ed__34_t2608884096  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/<LoadBuffer>d__34::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.GLTFSceneImporter/<LoadBuffer>d__34::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String UnityGLTF.GLTFSceneImporter/<LoadBuffer>d__34::sourceUri
	String_t* ___sourceUri_2;
	// GLTF.Schema.Buffer UnityGLTF.GLTFSceneImporter/<LoadBuffer>d__34::buffer
	Buffer_t869705777 * ___buffer_3;
	// System.Int32 UnityGLTF.GLTFSceneImporter/<LoadBuffer>d__34::bufferIndex
	int32_t ___bufferIndex_4;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter/<LoadBuffer>d__34::<>4__this
	GLTFSceneImporter_t274320441 * ___U3CU3E4__this_5;
	// System.Byte[] UnityGLTF.GLTFSceneImporter/<LoadBuffer>d__34::<bufferData>5__1
	ByteU5BU5D_t4116647657* ___U3CbufferDataU3E5__1_6;
	// System.String UnityGLTF.GLTFSceneImporter/<LoadBuffer>d__34::<uri>5__2
	String_t* ___U3CuriU3E5__2_7;
	// System.Text.RegularExpressions.Regex UnityGLTF.GLTFSceneImporter/<LoadBuffer>d__34::<regex>5__3
	Regex_t3657309853 * ___U3CregexU3E5__3_8;
	// System.Text.RegularExpressions.Match UnityGLTF.GLTFSceneImporter/<LoadBuffer>d__34::<match>5__4
	Match_t3408321083 * ___U3CmatchU3E5__4_9;
	// System.String UnityGLTF.GLTFSceneImporter/<LoadBuffer>d__34::<base64Data>5__5
	String_t* ___U3Cbase64DataU3E5__5_10;
	// UnityEngine.Networking.UnityWebRequest UnityGLTF.GLTFSceneImporter/<LoadBuffer>d__34::<www>5__6
	UnityWebRequest_t463507806 * ___U3CwwwU3E5__6_11;
	// System.String UnityGLTF.GLTFSceneImporter/<LoadBuffer>d__34::<pathToLoad>5__7
	String_t* ___U3CpathToLoadU3E5__7_12;
	// System.IO.FileStream UnityGLTF.GLTFSceneImporter/<LoadBuffer>d__34::<file>5__8
	FileStream_t4292183065 * ___U3CfileU3E5__8_13;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__34_t2608884096, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__34_t2608884096, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_sourceUri_2() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__34_t2608884096, ___sourceUri_2)); }
	inline String_t* get_sourceUri_2() const { return ___sourceUri_2; }
	inline String_t** get_address_of_sourceUri_2() { return &___sourceUri_2; }
	inline void set_sourceUri_2(String_t* value)
	{
		___sourceUri_2 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_2), value);
	}

	inline static int32_t get_offset_of_buffer_3() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__34_t2608884096, ___buffer_3)); }
	inline Buffer_t869705777 * get_buffer_3() const { return ___buffer_3; }
	inline Buffer_t869705777 ** get_address_of_buffer_3() { return &___buffer_3; }
	inline void set_buffer_3(Buffer_t869705777 * value)
	{
		___buffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_3), value);
	}

	inline static int32_t get_offset_of_bufferIndex_4() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__34_t2608884096, ___bufferIndex_4)); }
	inline int32_t get_bufferIndex_4() const { return ___bufferIndex_4; }
	inline int32_t* get_address_of_bufferIndex_4() { return &___bufferIndex_4; }
	inline void set_bufferIndex_4(int32_t value)
	{
		___bufferIndex_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__34_t2608884096, ___U3CU3E4__this_5)); }
	inline GLTFSceneImporter_t274320441 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline GLTFSceneImporter_t274320441 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(GLTFSceneImporter_t274320441 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CbufferDataU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__34_t2608884096, ___U3CbufferDataU3E5__1_6)); }
	inline ByteU5BU5D_t4116647657* get_U3CbufferDataU3E5__1_6() const { return ___U3CbufferDataU3E5__1_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CbufferDataU3E5__1_6() { return &___U3CbufferDataU3E5__1_6; }
	inline void set_U3CbufferDataU3E5__1_6(ByteU5BU5D_t4116647657* value)
	{
		___U3CbufferDataU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbufferDataU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3CuriU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__34_t2608884096, ___U3CuriU3E5__2_7)); }
	inline String_t* get_U3CuriU3E5__2_7() const { return ___U3CuriU3E5__2_7; }
	inline String_t** get_address_of_U3CuriU3E5__2_7() { return &___U3CuriU3E5__2_7; }
	inline void set_U3CuriU3E5__2_7(String_t* value)
	{
		___U3CuriU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuriU3E5__2_7), value);
	}

	inline static int32_t get_offset_of_U3CregexU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__34_t2608884096, ___U3CregexU3E5__3_8)); }
	inline Regex_t3657309853 * get_U3CregexU3E5__3_8() const { return ___U3CregexU3E5__3_8; }
	inline Regex_t3657309853 ** get_address_of_U3CregexU3E5__3_8() { return &___U3CregexU3E5__3_8; }
	inline void set_U3CregexU3E5__3_8(Regex_t3657309853 * value)
	{
		___U3CregexU3E5__3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CregexU3E5__3_8), value);
	}

	inline static int32_t get_offset_of_U3CmatchU3E5__4_9() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__34_t2608884096, ___U3CmatchU3E5__4_9)); }
	inline Match_t3408321083 * get_U3CmatchU3E5__4_9() const { return ___U3CmatchU3E5__4_9; }
	inline Match_t3408321083 ** get_address_of_U3CmatchU3E5__4_9() { return &___U3CmatchU3E5__4_9; }
	inline void set_U3CmatchU3E5__4_9(Match_t3408321083 * value)
	{
		___U3CmatchU3E5__4_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmatchU3E5__4_9), value);
	}

	inline static int32_t get_offset_of_U3Cbase64DataU3E5__5_10() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__34_t2608884096, ___U3Cbase64DataU3E5__5_10)); }
	inline String_t* get_U3Cbase64DataU3E5__5_10() const { return ___U3Cbase64DataU3E5__5_10; }
	inline String_t** get_address_of_U3Cbase64DataU3E5__5_10() { return &___U3Cbase64DataU3E5__5_10; }
	inline void set_U3Cbase64DataU3E5__5_10(String_t* value)
	{
		___U3Cbase64DataU3E5__5_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cbase64DataU3E5__5_10), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__6_11() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__34_t2608884096, ___U3CwwwU3E5__6_11)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E5__6_11() const { return ___U3CwwwU3E5__6_11; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E5__6_11() { return &___U3CwwwU3E5__6_11; }
	inline void set_U3CwwwU3E5__6_11(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E5__6_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__6_11), value);
	}

	inline static int32_t get_offset_of_U3CpathToLoadU3E5__7_12() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__34_t2608884096, ___U3CpathToLoadU3E5__7_12)); }
	inline String_t* get_U3CpathToLoadU3E5__7_12() const { return ___U3CpathToLoadU3E5__7_12; }
	inline String_t** get_address_of_U3CpathToLoadU3E5__7_12() { return &___U3CpathToLoadU3E5__7_12; }
	inline void set_U3CpathToLoadU3E5__7_12(String_t* value)
	{
		___U3CpathToLoadU3E5__7_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpathToLoadU3E5__7_12), value);
	}

	inline static int32_t get_offset_of_U3CfileU3E5__8_13() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__34_t2608884096, ___U3CfileU3E5__8_13)); }
	inline FileStream_t4292183065 * get_U3CfileU3E5__8_13() const { return ___U3CfileU3E5__8_13; }
	inline FileStream_t4292183065 ** get_address_of_U3CfileU3E5__8_13() { return &___U3CfileU3E5__8_13; }
	inline void set_U3CfileU3E5__8_13(FileStream_t4292183065 * value)
	{
		___U3CfileU3E5__8_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileU3E5__8_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADBUFFERU3ED__34_T2608884096_H
#ifndef GLTFUNITYHELPERS_T1495654367_H
#define GLTFUNITYHELPERS_T1495654367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFUnityHelpers
struct  GLTFUnityHelpers_t1495654367  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFUNITYHELPERS_T1495654367_H
#ifndef ENCODING_T1523322056_H
#define ENCODING_T1523322056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t1523322056  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t2285235057 * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_t1188251036 * ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t3123823036 * ___decoderFallback_14;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___dataItem_10)); }
	inline CodePageDataItem_t2285235057 * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t2285235057 ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t2285235057 * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier((&___dataItem_10), value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_11() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___m_deserializedFromEverett_11)); }
	inline bool get_m_deserializedFromEverett_11() const { return ___m_deserializedFromEverett_11; }
	inline bool* get_address_of_m_deserializedFromEverett_11() { return &___m_deserializedFromEverett_11; }
	inline void set_m_deserializedFromEverett_11(bool value)
	{
		___m_deserializedFromEverett_11 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_12() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___m_isReadOnly_12)); }
	inline bool get_m_isReadOnly_12() const { return ___m_isReadOnly_12; }
	inline bool* get_address_of_m_isReadOnly_12() { return &___m_isReadOnly_12; }
	inline void set_m_isReadOnly_12(bool value)
	{
		___m_isReadOnly_12 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___encoderFallback_13)); }
	inline EncoderFallback_t1188251036 * get_encoderFallback_13() const { return ___encoderFallback_13; }
	inline EncoderFallback_t1188251036 ** get_address_of_encoderFallback_13() { return &___encoderFallback_13; }
	inline void set_encoderFallback_13(EncoderFallback_t1188251036 * value)
	{
		___encoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___encoderFallback_13), value);
	}

	inline static int32_t get_offset_of_decoderFallback_14() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___decoderFallback_14)); }
	inline DecoderFallback_t3123823036 * get_decoderFallback_14() const { return ___decoderFallback_14; }
	inline DecoderFallback_t3123823036 ** get_address_of_decoderFallback_14() { return &___decoderFallback_14; }
	inline void set_decoderFallback_14(DecoderFallback_t3123823036 * value)
	{
		___decoderFallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___decoderFallback_14), value);
	}
};

struct Encoding_t1523322056_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t1523322056 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t1523322056 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t1523322056 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t1523322056 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t1523322056 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t1523322056 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t1523322056 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t1523322056 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t1853889766 * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_15;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_t1523322056 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_t1523322056 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_t1523322056 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_0), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_t1523322056 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_t1523322056 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_t1523322056 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_1), value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_t1523322056 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_t1523322056 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_t1523322056 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUnicode_2), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_t1523322056 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_t1523322056 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_t1523322056 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_3), value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_t1523322056 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_t1523322056 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_t1523322056 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((&___utf8Encoding_4), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_t1523322056 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_t1523322056 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_t1523322056 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_5), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_t1523322056 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_t1523322056 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_t1523322056 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_6), value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_t1523322056 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_t1523322056 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_t1523322056 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((&___latin1Encoding_7), value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___encodings_8)); }
	inline Hashtable_t1853889766 * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t1853889766 ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t1853889766 * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_8), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_15() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___s_InternalSyncObject_15)); }
	inline RuntimeObject * get_s_InternalSyncObject_15() const { return ___s_InternalSyncObject_15; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_15() { return &___s_InternalSyncObject_15; }
	inline void set_s_InternalSyncObject_15(RuntimeObject * value)
	{
		___s_InternalSyncObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T1523322056_H
#ifndef SCHEMAEXTENSIONS_T3846062055_H
#define SCHEMAEXTENSIONS_T3846062055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Extensions.SchemaExtensions
struct  SchemaExtensions_t3846062055  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMAEXTENSIONS_T3846062055_H
#ifndef MONOENCODING_T666837952_H
#define MONOENCODING_T666837952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.MonoEncoding
struct  MonoEncoding_t666837952  : public Encoding_t1523322056
{
public:
	// System.Int32 I18N.Common.MonoEncoding::win_code_page
	int32_t ___win_code_page_16;

public:
	inline static int32_t get_offset_of_win_code_page_16() { return static_cast<int32_t>(offsetof(MonoEncoding_t666837952, ___win_code_page_16)); }
	inline int32_t get_win_code_page_16() const { return ___win_code_page_16; }
	inline int32_t* get_address_of_win_code_page_16() { return &___win_code_page_16; }
	inline void set_win_code_page_16(int32_t value)
	{
		___win_code_page_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOENCODING_T666837952_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef __STATICARRAYINITTYPESIZEU3D512_T3317833663_H
#define __STATICARRAYINITTYPESIZEU3D512_T3317833663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512
struct  __StaticArrayInitTypeSizeU3D512_t3317833663 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D512_t3317833663__padding[512];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D512_T3317833663_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t385246372* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t385246372* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_31)); }
	inline DateTime_t3738529785  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t3738529785 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t3738529785  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_32)); }
	inline DateTime_t3738529785  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t3738529785  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef __STATICARRAYINITTYPESIZEU3D512_T3317833662_H
#define __STATICARRAYINITTYPESIZEU3D512_T3317833662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512
struct  __StaticArrayInitTypeSizeU3D512_t3317833662 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D512_t3317833662__padding[512];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D512_T3317833662_H
#ifndef WEBREQUESTEXCEPTION_T1410780468_H
#define WEBREQUESTEXCEPTION_T1410780468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.WebRequestException
struct  WebRequestException_t1410780468  : public Exception_t1436737249
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTEXCEPTION_T1410780468_H
#ifndef SYSTEMEXCEPTION_T176217641_H
#define SYSTEMEXCEPTION_T176217641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217641  : public Exception_t1436737249
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217641_H
#ifndef AXIS_T3661294207_H
#define AXIS_T3661294207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoundsExtensions/Axis
struct  Axis_t3661294207 
{
public:
	// System.Int32 BoundsExtensions/Axis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Axis_t3661294207, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T3661294207_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255375_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255375  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::05C64B887A4D766EEB5842345D6B609A0E3A91C9
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___05C64B887A4D766EEB5842345D6B609A0E3A91C9_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::148F346330E294B4157ED412259A7E7F373E26A8
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___148F346330E294B4157ED412259A7E7F373E26A8_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::1F867F0E96DB3A56943B8CB2662D1DA624023FCD
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___1F867F0E96DB3A56943B8CB2662D1DA624023FCD_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::3220DE2BE9769737429E0DE2C6D837CB7C5A02AD
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___3220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::356CE585046545CD2D0B109FF8A85DB88EE29074
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___356CE585046545CD2D0B109FF8A85DB88EE29074_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::4FEA2A6324C0192B29C90830F5D578051A4B1B16
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___4FEA2A6324C0192B29C90830F5D578051A4B1B16_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::598D9433A53523A59D462896B803E416AB0B1901
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___598D9433A53523A59D462896B803E416AB0B1901_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::6E6F169B075CACDE575F1FEA42B1376D31D5A7E5
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___6E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::7089F9820A6F9CC830909BCD1FAF2EF0A540F348
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___7089F9820A6F9CC830909BCD1FAF2EF0A540F348_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::D5DB24A984219B0001B4B86CDE1E0DF54572D83A
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::E11338F644FF140C0E23D8B7526DB4D2D73FC3F7
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14;

public:
	inline static int32_t get_offset_of_U305C64B887A4D766EEB5842345D6B609A0E3A91C9_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___05C64B887A4D766EEB5842345D6B609A0E3A91C9_0)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U305C64B887A4D766EEB5842345D6B609A0E3A91C9_0() const { return ___05C64B887A4D766EEB5842345D6B609A0E3A91C9_0; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U305C64B887A4D766EEB5842345D6B609A0E3A91C9_0() { return &___05C64B887A4D766EEB5842345D6B609A0E3A91C9_0; }
	inline void set_U305C64B887A4D766EEB5842345D6B609A0E3A91C9_0(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___05C64B887A4D766EEB5842345D6B609A0E3A91C9_0 = value;
	}

	inline static int32_t get_offset_of_U3148F346330E294B4157ED412259A7E7F373E26A8_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___148F346330E294B4157ED412259A7E7F373E26A8_1)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U3148F346330E294B4157ED412259A7E7F373E26A8_1() const { return ___148F346330E294B4157ED412259A7E7F373E26A8_1; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U3148F346330E294B4157ED412259A7E7F373E26A8_1() { return &___148F346330E294B4157ED412259A7E7F373E26A8_1; }
	inline void set_U3148F346330E294B4157ED412259A7E7F373E26A8_1(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___148F346330E294B4157ED412259A7E7F373E26A8_1 = value;
	}

	inline static int32_t get_offset_of_U31F867F0E96DB3A56943B8CB2662D1DA624023FCD_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___1F867F0E96DB3A56943B8CB2662D1DA624023FCD_2)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U31F867F0E96DB3A56943B8CB2662D1DA624023FCD_2() const { return ___1F867F0E96DB3A56943B8CB2662D1DA624023FCD_2; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U31F867F0E96DB3A56943B8CB2662D1DA624023FCD_2() { return &___1F867F0E96DB3A56943B8CB2662D1DA624023FCD_2; }
	inline void set_U31F867F0E96DB3A56943B8CB2662D1DA624023FCD_2(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___1F867F0E96DB3A56943B8CB2662D1DA624023FCD_2 = value;
	}

	inline static int32_t get_offset_of_U33220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___3220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U33220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3() const { return ___3220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U33220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3() { return &___3220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3; }
	inline void set_U33220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___3220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3 = value;
	}

	inline static int32_t get_offset_of_U3356CE585046545CD2D0B109FF8A85DB88EE29074_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___356CE585046545CD2D0B109FF8A85DB88EE29074_4)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U3356CE585046545CD2D0B109FF8A85DB88EE29074_4() const { return ___356CE585046545CD2D0B109FF8A85DB88EE29074_4; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U3356CE585046545CD2D0B109FF8A85DB88EE29074_4() { return &___356CE585046545CD2D0B109FF8A85DB88EE29074_4; }
	inline void set_U3356CE585046545CD2D0B109FF8A85DB88EE29074_4(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___356CE585046545CD2D0B109FF8A85DB88EE29074_4 = value;
	}

	inline static int32_t get_offset_of_U34FEA2A6324C0192B29C90830F5D578051A4B1B16_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___4FEA2A6324C0192B29C90830F5D578051A4B1B16_5)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U34FEA2A6324C0192B29C90830F5D578051A4B1B16_5() const { return ___4FEA2A6324C0192B29C90830F5D578051A4B1B16_5; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U34FEA2A6324C0192B29C90830F5D578051A4B1B16_5() { return &___4FEA2A6324C0192B29C90830F5D578051A4B1B16_5; }
	inline void set_U34FEA2A6324C0192B29C90830F5D578051A4B1B16_5(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___4FEA2A6324C0192B29C90830F5D578051A4B1B16_5 = value;
	}

	inline static int32_t get_offset_of_U3598D9433A53523A59D462896B803E416AB0B1901_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___598D9433A53523A59D462896B803E416AB0B1901_6)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U3598D9433A53523A59D462896B803E416AB0B1901_6() const { return ___598D9433A53523A59D462896B803E416AB0B1901_6; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U3598D9433A53523A59D462896B803E416AB0B1901_6() { return &___598D9433A53523A59D462896B803E416AB0B1901_6; }
	inline void set_U3598D9433A53523A59D462896B803E416AB0B1901_6(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___598D9433A53523A59D462896B803E416AB0B1901_6 = value;
	}

	inline static int32_t get_offset_of_U36E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___6E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U36E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7() const { return ___6E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U36E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7() { return &___6E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7; }
	inline void set_U36E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___6E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7 = value;
	}

	inline static int32_t get_offset_of_U37089F9820A6F9CC830909BCD1FAF2EF0A540F348_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___7089F9820A6F9CC830909BCD1FAF2EF0A540F348_8)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U37089F9820A6F9CC830909BCD1FAF2EF0A540F348_8() const { return ___7089F9820A6F9CC830909BCD1FAF2EF0A540F348_8; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U37089F9820A6F9CC830909BCD1FAF2EF0A540F348_8() { return &___7089F9820A6F9CC830909BCD1FAF2EF0A540F348_8; }
	inline void set_U37089F9820A6F9CC830909BCD1FAF2EF0A540F348_8(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___7089F9820A6F9CC830909BCD1FAF2EF0A540F348_8 = value;
	}

	inline static int32_t get_offset_of_U39ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U39ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9() const { return ___9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U39ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9() { return &___9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9; }
	inline void set_U39ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9 = value;
	}

	inline static int32_t get_offset_of_A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10() const { return ___A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10() { return &___A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10; }
	inline void set_A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10 = value;
	}

	inline static int32_t get_offset_of_B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11() const { return ___B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11() { return &___B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11; }
	inline void set_B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11 = value;
	}

	inline static int32_t get_offset_of_D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12() const { return ___D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12() { return &___D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12; }
	inline void set_D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12 = value;
	}

	inline static int32_t get_offset_of_E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13() const { return ___E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13() { return &___E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13; }
	inline void set_E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13 = value;
	}

	inline static int32_t get_offset_of_FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14() const { return ___FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14() { return &___FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14; }
	inline void set_FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255375_H
#ifndef LOADTYPE_T225521639_H
#define LOADTYPE_T225521639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/LoadType
struct  LoadType_t225521639 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/LoadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadType_t225521639, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADTYPE_T225521639_H
#ifndef MATERIALTYPE_T597773032_H
#define MATERIALTYPE_T597773032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/MaterialType
struct  MaterialType_t597773032 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/MaterialType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaterialType_t597773032, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALTYPE_T597773032_H
#ifndef BYTEENCODING_T1371924561_H
#define BYTEENCODING_T1371924561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.ByteEncoding
struct  ByteEncoding_t1371924561  : public MonoEncoding_t666837952
{
public:
	// System.Char[] I18N.Common.ByteEncoding::toChars
	CharU5BU5D_t3528271667* ___toChars_17;
	// System.String I18N.Common.ByteEncoding::encodingName
	String_t* ___encodingName_18;
	// System.String I18N.Common.ByteEncoding::bodyName
	String_t* ___bodyName_19;
	// System.String I18N.Common.ByteEncoding::headerName
	String_t* ___headerName_20;
	// System.String I18N.Common.ByteEncoding::webName
	String_t* ___webName_21;
	// System.Boolean I18N.Common.ByteEncoding::isBrowserDisplay
	bool ___isBrowserDisplay_22;
	// System.Boolean I18N.Common.ByteEncoding::isBrowserSave
	bool ___isBrowserSave_23;
	// System.Boolean I18N.Common.ByteEncoding::isMailNewsDisplay
	bool ___isMailNewsDisplay_24;
	// System.Boolean I18N.Common.ByteEncoding::isMailNewsSave
	bool ___isMailNewsSave_25;
	// System.Int32 I18N.Common.ByteEncoding::windowsCodePage
	int32_t ___windowsCodePage_26;

public:
	inline static int32_t get_offset_of_toChars_17() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___toChars_17)); }
	inline CharU5BU5D_t3528271667* get_toChars_17() const { return ___toChars_17; }
	inline CharU5BU5D_t3528271667** get_address_of_toChars_17() { return &___toChars_17; }
	inline void set_toChars_17(CharU5BU5D_t3528271667* value)
	{
		___toChars_17 = value;
		Il2CppCodeGenWriteBarrier((&___toChars_17), value);
	}

	inline static int32_t get_offset_of_encodingName_18() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___encodingName_18)); }
	inline String_t* get_encodingName_18() const { return ___encodingName_18; }
	inline String_t** get_address_of_encodingName_18() { return &___encodingName_18; }
	inline void set_encodingName_18(String_t* value)
	{
		___encodingName_18 = value;
		Il2CppCodeGenWriteBarrier((&___encodingName_18), value);
	}

	inline static int32_t get_offset_of_bodyName_19() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___bodyName_19)); }
	inline String_t* get_bodyName_19() const { return ___bodyName_19; }
	inline String_t** get_address_of_bodyName_19() { return &___bodyName_19; }
	inline void set_bodyName_19(String_t* value)
	{
		___bodyName_19 = value;
		Il2CppCodeGenWriteBarrier((&___bodyName_19), value);
	}

	inline static int32_t get_offset_of_headerName_20() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___headerName_20)); }
	inline String_t* get_headerName_20() const { return ___headerName_20; }
	inline String_t** get_address_of_headerName_20() { return &___headerName_20; }
	inline void set_headerName_20(String_t* value)
	{
		___headerName_20 = value;
		Il2CppCodeGenWriteBarrier((&___headerName_20), value);
	}

	inline static int32_t get_offset_of_webName_21() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___webName_21)); }
	inline String_t* get_webName_21() const { return ___webName_21; }
	inline String_t** get_address_of_webName_21() { return &___webName_21; }
	inline void set_webName_21(String_t* value)
	{
		___webName_21 = value;
		Il2CppCodeGenWriteBarrier((&___webName_21), value);
	}

	inline static int32_t get_offset_of_isBrowserDisplay_22() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___isBrowserDisplay_22)); }
	inline bool get_isBrowserDisplay_22() const { return ___isBrowserDisplay_22; }
	inline bool* get_address_of_isBrowserDisplay_22() { return &___isBrowserDisplay_22; }
	inline void set_isBrowserDisplay_22(bool value)
	{
		___isBrowserDisplay_22 = value;
	}

	inline static int32_t get_offset_of_isBrowserSave_23() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___isBrowserSave_23)); }
	inline bool get_isBrowserSave_23() const { return ___isBrowserSave_23; }
	inline bool* get_address_of_isBrowserSave_23() { return &___isBrowserSave_23; }
	inline void set_isBrowserSave_23(bool value)
	{
		___isBrowserSave_23 = value;
	}

	inline static int32_t get_offset_of_isMailNewsDisplay_24() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___isMailNewsDisplay_24)); }
	inline bool get_isMailNewsDisplay_24() const { return ___isMailNewsDisplay_24; }
	inline bool* get_address_of_isMailNewsDisplay_24() { return &___isMailNewsDisplay_24; }
	inline void set_isMailNewsDisplay_24(bool value)
	{
		___isMailNewsDisplay_24 = value;
	}

	inline static int32_t get_offset_of_isMailNewsSave_25() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___isMailNewsSave_25)); }
	inline bool get_isMailNewsSave_25() const { return ___isMailNewsSave_25; }
	inline bool* get_address_of_isMailNewsSave_25() { return &___isMailNewsSave_25; }
	inline void set_isMailNewsSave_25(bool value)
	{
		___isMailNewsSave_25 = value;
	}

	inline static int32_t get_offset_of_windowsCodePage_26() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___windowsCodePage_26)); }
	inline int32_t get_windowsCodePage_26() const { return ___windowsCodePage_26; }
	inline int32_t* get_address_of_windowsCodePage_26() { return &___windowsCodePage_26; }
	inline void set_windowsCodePage_26(int32_t value)
	{
		___windowsCodePage_26 = value;
	}
};

struct ByteEncoding_t1371924561_StaticFields
{
public:
	// System.Byte[] I18N.Common.ByteEncoding::isNormalized
	ByteU5BU5D_t4116647657* ___isNormalized_27;
	// System.Byte[] I18N.Common.ByteEncoding::isNormalizedComputed
	ByteU5BU5D_t4116647657* ___isNormalizedComputed_28;
	// System.Byte[] I18N.Common.ByteEncoding::normalization_bytes
	ByteU5BU5D_t4116647657* ___normalization_bytes_29;

public:
	inline static int32_t get_offset_of_isNormalized_27() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561_StaticFields, ___isNormalized_27)); }
	inline ByteU5BU5D_t4116647657* get_isNormalized_27() const { return ___isNormalized_27; }
	inline ByteU5BU5D_t4116647657** get_address_of_isNormalized_27() { return &___isNormalized_27; }
	inline void set_isNormalized_27(ByteU5BU5D_t4116647657* value)
	{
		___isNormalized_27 = value;
		Il2CppCodeGenWriteBarrier((&___isNormalized_27), value);
	}

	inline static int32_t get_offset_of_isNormalizedComputed_28() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561_StaticFields, ___isNormalizedComputed_28)); }
	inline ByteU5BU5D_t4116647657* get_isNormalizedComputed_28() const { return ___isNormalizedComputed_28; }
	inline ByteU5BU5D_t4116647657** get_address_of_isNormalizedComputed_28() { return &___isNormalizedComputed_28; }
	inline void set_isNormalizedComputed_28(ByteU5BU5D_t4116647657* value)
	{
		___isNormalizedComputed_28 = value;
		Il2CppCodeGenWriteBarrier((&___isNormalizedComputed_28), value);
	}

	inline static int32_t get_offset_of_normalization_bytes_29() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561_StaticFields, ___normalization_bytes_29)); }
	inline ByteU5BU5D_t4116647657* get_normalization_bytes_29() const { return ___normalization_bytes_29; }
	inline ByteU5BU5D_t4116647657** get_address_of_normalization_bytes_29() { return &___normalization_bytes_29; }
	inline void set_normalization_bytes_29(ByteU5BU5D_t4116647657* value)
	{
		___normalization_bytes_29 = value;
		Il2CppCodeGenWriteBarrier((&___normalization_bytes_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEENCODING_T1371924561_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255374_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255374  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::05B1E1067273E188A50BA4342B4BC09ABEE669CD
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___05B1E1067273E188A50BA4342B4BC09ABEE669CD_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::09885C8B0840E863B3A14261999895D896677D5A
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___09885C8B0840E863B3A14261999895D896677D5A_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::271EF3D1C1F8136E08DBCAB443CE02538A4156F0
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::2E1C78D5F4666324672DFACDC9307935CF14E98F
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___2E1C78D5F4666324672DFACDC9307935CF14E98F_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::3551F16F8A11003B8289B76B9E3D97969B68E7D9
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___3551F16F8A11003B8289B76B9E3D97969B68E7D9_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::36A0A1CE2E522AB1D22668B38F6126D2F6E17D44
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___36A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::37C03949C69BDDA042205E1A573B349E75F693B8
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___37C03949C69BDDA042205E1A573B349E75F693B8_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::47DCC01E928630EBCC018C5E0285145985F92532
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___47DCC01E928630EBCC018C5E0285145985F92532_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::52623ED7F33E8C1C541F1C3101FA981B173D795E
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___52623ED7F33E8C1C541F1C3101FA981B173D795E_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::659C24C855D0E50CBD366B2774AF841E29202E70
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___659C24C855D0E50CBD366B2774AF841E29202E70_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::82273BF74F02F3FDFABEED76BCA4B82DDB2C2761
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___82273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::8E958C1847104F390F9C9B64F0F39C98ED4AC63F
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___8E958C1847104F390F9C9B64F0F39C98ED4AC63F_18;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::94F89F72CC0508D891A0C71DD7B57B703869A3FB
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___94F89F72CC0508D891A0C71DD7B57B703869A3FB_19;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::9EF46C677634CB82C0BD475E372C71C5F68C981A
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___9EF46C677634CB82C0BD475E372C71C5F68C981A_21;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::A5A5BE77BD3D095389FABC21D18BB648787E6618
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___A5A5BE77BD3D095389FABC21D18BB648787E6618_22;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::B1D1CEE08985760776A35065014EAF3D4D3CDE8D
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::B2574B82126B684C0CB3CF93BF7473F0FBB34400
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___B2574B82126B684C0CB3CF93BF7473F0FBB34400_25;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::B6BD9A82204938ABFE97CF3FAB5A47824080EAA0
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::BC1CEF8131E7F0D8206029373157806126E21026
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___BC1CEF8131E7F0D8206029373157806126E21026_27;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::C14817C33562352073848F1A8EA633C19CF1A4F5
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___C14817C33562352073848F1A8EA633C19CF1A4F5_28;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::C392E993B9E622A36C30E0BB997A9F41293CD9EF
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___C392E993B9E622A36C30E0BB997A9F41293CD9EF_29;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::CFB85A64A0D1FAB523C3DE56096F8803CDFEA674
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::D257291FBF3DA6F5C38B3275534902BC9EDE92EA
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::D4795500A3D0F00D8EE85626648C3FBAFA4F70C3
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::DF4C38A5E59685BBEC109623762B6914BE00E866
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___DF4C38A5E59685BBEC109623762B6914BE00E866_35;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::F632BE2E03B27F265F779A5D3D217E5C14AB6CB5
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36;

public:
	inline static int32_t get_offset_of_U305B1E1067273E188A50BA4342B4BC09ABEE669CD_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___05B1E1067273E188A50BA4342B4BC09ABEE669CD_0)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U305B1E1067273E188A50BA4342B4BC09ABEE669CD_0() const { return ___05B1E1067273E188A50BA4342B4BC09ABEE669CD_0; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U305B1E1067273E188A50BA4342B4BC09ABEE669CD_0() { return &___05B1E1067273E188A50BA4342B4BC09ABEE669CD_0; }
	inline void set_U305B1E1067273E188A50BA4342B4BC09ABEE669CD_0(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___05B1E1067273E188A50BA4342B4BC09ABEE669CD_0 = value;
	}

	inline static int32_t get_offset_of_U309885C8B0840E863B3A14261999895D896677D5A_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___09885C8B0840E863B3A14261999895D896677D5A_1)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U309885C8B0840E863B3A14261999895D896677D5A_1() const { return ___09885C8B0840E863B3A14261999895D896677D5A_1; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U309885C8B0840E863B3A14261999895D896677D5A_1() { return &___09885C8B0840E863B3A14261999895D896677D5A_1; }
	inline void set_U309885C8B0840E863B3A14261999895D896677D5A_1(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___09885C8B0840E863B3A14261999895D896677D5A_1 = value;
	}

	inline static int32_t get_offset_of_U30BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U30BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2() const { return ___0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U30BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2() { return &___0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2; }
	inline void set_U30BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2 = value;
	}

	inline static int32_t get_offset_of_U3271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U3271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3() const { return ___271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U3271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3() { return &___271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3; }
	inline void set_U3271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3 = value;
	}

	inline static int32_t get_offset_of_U32E1C78D5F4666324672DFACDC9307935CF14E98F_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___2E1C78D5F4666324672DFACDC9307935CF14E98F_4)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U32E1C78D5F4666324672DFACDC9307935CF14E98F_4() const { return ___2E1C78D5F4666324672DFACDC9307935CF14E98F_4; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U32E1C78D5F4666324672DFACDC9307935CF14E98F_4() { return &___2E1C78D5F4666324672DFACDC9307935CF14E98F_4; }
	inline void set_U32E1C78D5F4666324672DFACDC9307935CF14E98F_4(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___2E1C78D5F4666324672DFACDC9307935CF14E98F_4 = value;
	}

	inline static int32_t get_offset_of_U33551F16F8A11003B8289B76B9E3D97969B68E7D9_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___3551F16F8A11003B8289B76B9E3D97969B68E7D9_5)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U33551F16F8A11003B8289B76B9E3D97969B68E7D9_5() const { return ___3551F16F8A11003B8289B76B9E3D97969B68E7D9_5; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U33551F16F8A11003B8289B76B9E3D97969B68E7D9_5() { return &___3551F16F8A11003B8289B76B9E3D97969B68E7D9_5; }
	inline void set_U33551F16F8A11003B8289B76B9E3D97969B68E7D9_5(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___3551F16F8A11003B8289B76B9E3D97969B68E7D9_5 = value;
	}

	inline static int32_t get_offset_of_U336A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___36A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U336A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6() const { return ___36A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U336A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6() { return &___36A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6; }
	inline void set_U336A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___36A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6 = value;
	}

	inline static int32_t get_offset_of_U337C03949C69BDDA042205E1A573B349E75F693B8_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___37C03949C69BDDA042205E1A573B349E75F693B8_7)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U337C03949C69BDDA042205E1A573B349E75F693B8_7() const { return ___37C03949C69BDDA042205E1A573B349E75F693B8_7; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U337C03949C69BDDA042205E1A573B349E75F693B8_7() { return &___37C03949C69BDDA042205E1A573B349E75F693B8_7; }
	inline void set_U337C03949C69BDDA042205E1A573B349E75F693B8_7(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___37C03949C69BDDA042205E1A573B349E75F693B8_7 = value;
	}

	inline static int32_t get_offset_of_U347DCC01E928630EBCC018C5E0285145985F92532_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___47DCC01E928630EBCC018C5E0285145985F92532_8)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U347DCC01E928630EBCC018C5E0285145985F92532_8() const { return ___47DCC01E928630EBCC018C5E0285145985F92532_8; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U347DCC01E928630EBCC018C5E0285145985F92532_8() { return &___47DCC01E928630EBCC018C5E0285145985F92532_8; }
	inline void set_U347DCC01E928630EBCC018C5E0285145985F92532_8(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___47DCC01E928630EBCC018C5E0285145985F92532_8 = value;
	}

	inline static int32_t get_offset_of_U352623ED7F33E8C1C541F1C3101FA981B173D795E_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___52623ED7F33E8C1C541F1C3101FA981B173D795E_9)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U352623ED7F33E8C1C541F1C3101FA981B173D795E_9() const { return ___52623ED7F33E8C1C541F1C3101FA981B173D795E_9; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U352623ED7F33E8C1C541F1C3101FA981B173D795E_9() { return &___52623ED7F33E8C1C541F1C3101FA981B173D795E_9; }
	inline void set_U352623ED7F33E8C1C541F1C3101FA981B173D795E_9(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___52623ED7F33E8C1C541F1C3101FA981B173D795E_9 = value;
	}

	inline static int32_t get_offset_of_U35EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U35EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10() const { return ___5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U35EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10() { return &___5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10; }
	inline void set_U35EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10 = value;
	}

	inline static int32_t get_offset_of_U3659C24C855D0E50CBD366B2774AF841E29202E70_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___659C24C855D0E50CBD366B2774AF841E29202E70_11)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U3659C24C855D0E50CBD366B2774AF841E29202E70_11() const { return ___659C24C855D0E50CBD366B2774AF841E29202E70_11; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U3659C24C855D0E50CBD366B2774AF841E29202E70_11() { return &___659C24C855D0E50CBD366B2774AF841E29202E70_11; }
	inline void set_U3659C24C855D0E50CBD366B2774AF841E29202E70_11(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___659C24C855D0E50CBD366B2774AF841E29202E70_11 = value;
	}

	inline static int32_t get_offset_of_U36B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U36B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12() const { return ___6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U36B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12() { return &___6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12; }
	inline void set_U36B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12 = value;
	}

	inline static int32_t get_offset_of_U371D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U371D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13() const { return ___71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U371D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13() { return &___71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13; }
	inline void set_U371D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13 = value;
	}

	inline static int32_t get_offset_of_U37A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U37A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14() const { return ___7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U37A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14() { return &___7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14; }
	inline void set_U37A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14 = value;
	}

	inline static int32_t get_offset_of_U37AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U37AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15() const { return ___7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U37AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15() { return &___7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15; }
	inline void set_U37AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15 = value;
	}

	inline static int32_t get_offset_of_U382273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___82273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U382273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16() const { return ___82273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U382273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16() { return &___82273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16; }
	inline void set_U382273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___82273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16 = value;
	}

	inline static int32_t get_offset_of_U38C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U38C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17() const { return ___8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U38C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17() { return &___8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17; }
	inline void set_U38C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17 = value;
	}

	inline static int32_t get_offset_of_U38E958C1847104F390F9C9B64F0F39C98ED4AC63F_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___8E958C1847104F390F9C9B64F0F39C98ED4AC63F_18)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U38E958C1847104F390F9C9B64F0F39C98ED4AC63F_18() const { return ___8E958C1847104F390F9C9B64F0F39C98ED4AC63F_18; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U38E958C1847104F390F9C9B64F0F39C98ED4AC63F_18() { return &___8E958C1847104F390F9C9B64F0F39C98ED4AC63F_18; }
	inline void set_U38E958C1847104F390F9C9B64F0F39C98ED4AC63F_18(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___8E958C1847104F390F9C9B64F0F39C98ED4AC63F_18 = value;
	}

	inline static int32_t get_offset_of_U394F89F72CC0508D891A0C71DD7B57B703869A3FB_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___94F89F72CC0508D891A0C71DD7B57B703869A3FB_19)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U394F89F72CC0508D891A0C71DD7B57B703869A3FB_19() const { return ___94F89F72CC0508D891A0C71DD7B57B703869A3FB_19; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U394F89F72CC0508D891A0C71DD7B57B703869A3FB_19() { return &___94F89F72CC0508D891A0C71DD7B57B703869A3FB_19; }
	inline void set_U394F89F72CC0508D891A0C71DD7B57B703869A3FB_19(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___94F89F72CC0508D891A0C71DD7B57B703869A3FB_19 = value;
	}

	inline static int32_t get_offset_of_U39E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U39E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20() const { return ___9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U39E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20() { return &___9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20; }
	inline void set_U39E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20 = value;
	}

	inline static int32_t get_offset_of_U39EF46C677634CB82C0BD475E372C71C5F68C981A_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___9EF46C677634CB82C0BD475E372C71C5F68C981A_21)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U39EF46C677634CB82C0BD475E372C71C5F68C981A_21() const { return ___9EF46C677634CB82C0BD475E372C71C5F68C981A_21; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U39EF46C677634CB82C0BD475E372C71C5F68C981A_21() { return &___9EF46C677634CB82C0BD475E372C71C5F68C981A_21; }
	inline void set_U39EF46C677634CB82C0BD475E372C71C5F68C981A_21(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___9EF46C677634CB82C0BD475E372C71C5F68C981A_21 = value;
	}

	inline static int32_t get_offset_of_A5A5BE77BD3D095389FABC21D18BB648787E6618_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___A5A5BE77BD3D095389FABC21D18BB648787E6618_22)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_A5A5BE77BD3D095389FABC21D18BB648787E6618_22() const { return ___A5A5BE77BD3D095389FABC21D18BB648787E6618_22; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_A5A5BE77BD3D095389FABC21D18BB648787E6618_22() { return &___A5A5BE77BD3D095389FABC21D18BB648787E6618_22; }
	inline void set_A5A5BE77BD3D095389FABC21D18BB648787E6618_22(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___A5A5BE77BD3D095389FABC21D18BB648787E6618_22 = value;
	}

	inline static int32_t get_offset_of_B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23() const { return ___B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23() { return &___B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23; }
	inline void set_B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23 = value;
	}

	inline static int32_t get_offset_of_B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24() const { return ___B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24() { return &___B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24; }
	inline void set_B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24 = value;
	}

	inline static int32_t get_offset_of_B2574B82126B684C0CB3CF93BF7473F0FBB34400_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___B2574B82126B684C0CB3CF93BF7473F0FBB34400_25)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_B2574B82126B684C0CB3CF93BF7473F0FBB34400_25() const { return ___B2574B82126B684C0CB3CF93BF7473F0FBB34400_25; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_B2574B82126B684C0CB3CF93BF7473F0FBB34400_25() { return &___B2574B82126B684C0CB3CF93BF7473F0FBB34400_25; }
	inline void set_B2574B82126B684C0CB3CF93BF7473F0FBB34400_25(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___B2574B82126B684C0CB3CF93BF7473F0FBB34400_25 = value;
	}

	inline static int32_t get_offset_of_B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26() const { return ___B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26() { return &___B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26; }
	inline void set_B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26 = value;
	}

	inline static int32_t get_offset_of_BC1CEF8131E7F0D8206029373157806126E21026_27() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___BC1CEF8131E7F0D8206029373157806126E21026_27)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_BC1CEF8131E7F0D8206029373157806126E21026_27() const { return ___BC1CEF8131E7F0D8206029373157806126E21026_27; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_BC1CEF8131E7F0D8206029373157806126E21026_27() { return &___BC1CEF8131E7F0D8206029373157806126E21026_27; }
	inline void set_BC1CEF8131E7F0D8206029373157806126E21026_27(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___BC1CEF8131E7F0D8206029373157806126E21026_27 = value;
	}

	inline static int32_t get_offset_of_C14817C33562352073848F1A8EA633C19CF1A4F5_28() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___C14817C33562352073848F1A8EA633C19CF1A4F5_28)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_C14817C33562352073848F1A8EA633C19CF1A4F5_28() const { return ___C14817C33562352073848F1A8EA633C19CF1A4F5_28; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_C14817C33562352073848F1A8EA633C19CF1A4F5_28() { return &___C14817C33562352073848F1A8EA633C19CF1A4F5_28; }
	inline void set_C14817C33562352073848F1A8EA633C19CF1A4F5_28(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___C14817C33562352073848F1A8EA633C19CF1A4F5_28 = value;
	}

	inline static int32_t get_offset_of_C392E993B9E622A36C30E0BB997A9F41293CD9EF_29() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___C392E993B9E622A36C30E0BB997A9F41293CD9EF_29)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_C392E993B9E622A36C30E0BB997A9F41293CD9EF_29() const { return ___C392E993B9E622A36C30E0BB997A9F41293CD9EF_29; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_C392E993B9E622A36C30E0BB997A9F41293CD9EF_29() { return &___C392E993B9E622A36C30E0BB997A9F41293CD9EF_29; }
	inline void set_C392E993B9E622A36C30E0BB997A9F41293CD9EF_29(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___C392E993B9E622A36C30E0BB997A9F41293CD9EF_29 = value;
	}

	inline static int32_t get_offset_of_C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30() const { return ___C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30() { return &___C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30; }
	inline void set_C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30 = value;
	}

	inline static int32_t get_offset_of_C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31() const { return ___C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31() { return &___C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31; }
	inline void set_C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31 = value;
	}

	inline static int32_t get_offset_of_CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32() const { return ___CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32() { return &___CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32; }
	inline void set_CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32 = value;
	}

	inline static int32_t get_offset_of_D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33() const { return ___D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33() { return &___D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33; }
	inline void set_D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33 = value;
	}

	inline static int32_t get_offset_of_D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34() const { return ___D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34() { return &___D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34; }
	inline void set_D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34 = value;
	}

	inline static int32_t get_offset_of_DF4C38A5E59685BBEC109623762B6914BE00E866_35() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___DF4C38A5E59685BBEC109623762B6914BE00E866_35)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_DF4C38A5E59685BBEC109623762B6914BE00E866_35() const { return ___DF4C38A5E59685BBEC109623762B6914BE00E866_35; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_DF4C38A5E59685BBEC109623762B6914BE00E866_35() { return &___DF4C38A5E59685BBEC109623762B6914BE00E866_35; }
	inline void set_DF4C38A5E59685BBEC109623762B6914BE00E866_35(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___DF4C38A5E59685BBEC109623762B6914BE00E866_35 = value;
	}

	inline static int32_t get_offset_of_F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36() const { return ___F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36() { return &___F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36; }
	inline void set_F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255374_H
#ifndef HANDLEREF_T3745784363_H
#define HANDLEREF_T3745784363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.HandleRef
struct  HandleRef_t3745784363 
{
public:
	// System.Object System.Runtime.InteropServices.HandleRef::wrapper
	RuntimeObject * ___wrapper_0;
	// System.IntPtr System.Runtime.InteropServices.HandleRef::handle
	intptr_t ___handle_1;

public:
	inline static int32_t get_offset_of_wrapper_0() { return static_cast<int32_t>(offsetof(HandleRef_t3745784363, ___wrapper_0)); }
	inline RuntimeObject * get_wrapper_0() const { return ___wrapper_0; }
	inline RuntimeObject ** get_address_of_wrapper_0() { return &___wrapper_0; }
	inline void set_wrapper_0(RuntimeObject * value)
	{
		___wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___wrapper_0), value);
	}

	inline static int32_t get_offset_of_handle_1() { return static_cast<int32_t>(offsetof(HandleRef_t3745784363, ___handle_1)); }
	inline intptr_t get_handle_1() const { return ___handle_1; }
	inline intptr_t* get_address_of_handle_1() { return &___handle_1; }
	inline void set_handle_1(intptr_t value)
	{
		___handle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.InteropServices.HandleRef
struct HandleRef_t3745784363_marshaled_pinvoke
{
	Il2CppIUnknown* ___wrapper_0;
	intptr_t ___handle_1;
};
// Native definition for COM marshalling of System.Runtime.InteropServices.HandleRef
struct HandleRef_t3745784363_marshaled_com
{
	Il2CppIUnknown* ___wrapper_0;
	intptr_t ___handle_1;
};
#endif // HANDLEREF_T3745784363_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef CP861_T2877197398_H
#define CP861_T2877197398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP861
struct  CP861_t2877197398  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP861_t2877197398_StaticFields
{
public:
	// System.Char[] I18N.West.CP861::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP861_t2877197398_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP861_T2877197398_H
#ifndef CP860_T1311113457_H
#define CP860_T1311113457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP860
struct  CP860_t1311113457  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP860_t1311113457_StaticFields
{
public:
	// System.Char[] I18N.West.CP860::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP860_t1311113457_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP860_T1311113457_H
#ifndef CP850_T1311178993_H
#define CP850_T1311178993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP850
struct  CP850_t1311178993  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP850_t1311178993_StaticFields
{
public:
	// System.Char[] I18N.West.CP850::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP850_t1311178993_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP850_T1311178993_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef GLTFSCENEIMPORTER_T274320441_H
#define GLTFSCENEIMPORTER_T274320441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter
struct  GLTFSceneImporter_t274320441  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityGLTF.GLTFSceneImporter::_lastLoadedScene
	GameObject_t1113636619 * ____lastLoadedScene_0;
	// UnityEngine.Transform UnityGLTF.GLTFSceneImporter::_sceneParent
	Transform_t3600365921 * ____sceneParent_1;
	// System.Collections.Generic.Dictionary`2<UnityGLTF.GLTFSceneImporter/MaterialType,UnityEngine.Shader> UnityGLTF.GLTFSceneImporter::_shaderCache
	Dictionary_2_t2973298224 * ____shaderCache_2;
	// System.Int32 UnityGLTF.GLTFSceneImporter::MaximumLod
	int32_t ___MaximumLod_3;
	// GLTF.Schema.Material UnityGLTF.GLTFSceneImporter::DefaultMaterial
	Material_t2790518843 * ___DefaultMaterial_4;
	// System.String UnityGLTF.GLTFSceneImporter::_gltfUrl
	String_t* ____gltfUrl_5;
	// System.String UnityGLTF.GLTFSceneImporter::_gltfDirectoryPath
	String_t* ____gltfDirectoryPath_6;
	// System.IO.Stream UnityGLTF.GLTFSceneImporter::_gltfStream
	Stream_t1273022909 * ____gltfStream_7;
	// GLTF.Schema.GLTFRoot UnityGLTF.GLTFSceneImporter::_root
	GLTFRoot_t676886847 * ____root_8;
	// UnityGLTF.Cache.AssetCache UnityGLTF.GLTFSceneImporter::_assetCache
	AssetCache_t783611398 * ____assetCache_9;
	// UnityGLTF.AsyncAction UnityGLTF.GLTFSceneImporter::_asyncAction
	AsyncAction_t3334821446 * ____asyncAction_10;
	// System.Boolean UnityGLTF.GLTFSceneImporter::_addColliders
	bool ____addColliders_11;
	// System.Byte[] UnityGLTF.GLTFSceneImporter::_gltfData
	ByteU5BU5D_t4116647657* ____gltfData_12;
	// UnityGLTF.GLTFSceneImporter/LoadType UnityGLTF.GLTFSceneImporter::_loadType
	int32_t ____loadType_13;

public:
	inline static int32_t get_offset_of__lastLoadedScene_0() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_t274320441, ____lastLoadedScene_0)); }
	inline GameObject_t1113636619 * get__lastLoadedScene_0() const { return ____lastLoadedScene_0; }
	inline GameObject_t1113636619 ** get_address_of__lastLoadedScene_0() { return &____lastLoadedScene_0; }
	inline void set__lastLoadedScene_0(GameObject_t1113636619 * value)
	{
		____lastLoadedScene_0 = value;
		Il2CppCodeGenWriteBarrier((&____lastLoadedScene_0), value);
	}

	inline static int32_t get_offset_of__sceneParent_1() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_t274320441, ____sceneParent_1)); }
	inline Transform_t3600365921 * get__sceneParent_1() const { return ____sceneParent_1; }
	inline Transform_t3600365921 ** get_address_of__sceneParent_1() { return &____sceneParent_1; }
	inline void set__sceneParent_1(Transform_t3600365921 * value)
	{
		____sceneParent_1 = value;
		Il2CppCodeGenWriteBarrier((&____sceneParent_1), value);
	}

	inline static int32_t get_offset_of__shaderCache_2() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_t274320441, ____shaderCache_2)); }
	inline Dictionary_2_t2973298224 * get__shaderCache_2() const { return ____shaderCache_2; }
	inline Dictionary_2_t2973298224 ** get_address_of__shaderCache_2() { return &____shaderCache_2; }
	inline void set__shaderCache_2(Dictionary_2_t2973298224 * value)
	{
		____shaderCache_2 = value;
		Il2CppCodeGenWriteBarrier((&____shaderCache_2), value);
	}

	inline static int32_t get_offset_of_MaximumLod_3() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_t274320441, ___MaximumLod_3)); }
	inline int32_t get_MaximumLod_3() const { return ___MaximumLod_3; }
	inline int32_t* get_address_of_MaximumLod_3() { return &___MaximumLod_3; }
	inline void set_MaximumLod_3(int32_t value)
	{
		___MaximumLod_3 = value;
	}

	inline static int32_t get_offset_of_DefaultMaterial_4() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_t274320441, ___DefaultMaterial_4)); }
	inline Material_t2790518843 * get_DefaultMaterial_4() const { return ___DefaultMaterial_4; }
	inline Material_t2790518843 ** get_address_of_DefaultMaterial_4() { return &___DefaultMaterial_4; }
	inline void set_DefaultMaterial_4(Material_t2790518843 * value)
	{
		___DefaultMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultMaterial_4), value);
	}

	inline static int32_t get_offset_of__gltfUrl_5() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_t274320441, ____gltfUrl_5)); }
	inline String_t* get__gltfUrl_5() const { return ____gltfUrl_5; }
	inline String_t** get_address_of__gltfUrl_5() { return &____gltfUrl_5; }
	inline void set__gltfUrl_5(String_t* value)
	{
		____gltfUrl_5 = value;
		Il2CppCodeGenWriteBarrier((&____gltfUrl_5), value);
	}

	inline static int32_t get_offset_of__gltfDirectoryPath_6() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_t274320441, ____gltfDirectoryPath_6)); }
	inline String_t* get__gltfDirectoryPath_6() const { return ____gltfDirectoryPath_6; }
	inline String_t** get_address_of__gltfDirectoryPath_6() { return &____gltfDirectoryPath_6; }
	inline void set__gltfDirectoryPath_6(String_t* value)
	{
		____gltfDirectoryPath_6 = value;
		Il2CppCodeGenWriteBarrier((&____gltfDirectoryPath_6), value);
	}

	inline static int32_t get_offset_of__gltfStream_7() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_t274320441, ____gltfStream_7)); }
	inline Stream_t1273022909 * get__gltfStream_7() const { return ____gltfStream_7; }
	inline Stream_t1273022909 ** get_address_of__gltfStream_7() { return &____gltfStream_7; }
	inline void set__gltfStream_7(Stream_t1273022909 * value)
	{
		____gltfStream_7 = value;
		Il2CppCodeGenWriteBarrier((&____gltfStream_7), value);
	}

	inline static int32_t get_offset_of__root_8() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_t274320441, ____root_8)); }
	inline GLTFRoot_t676886847 * get__root_8() const { return ____root_8; }
	inline GLTFRoot_t676886847 ** get_address_of__root_8() { return &____root_8; }
	inline void set__root_8(GLTFRoot_t676886847 * value)
	{
		____root_8 = value;
		Il2CppCodeGenWriteBarrier((&____root_8), value);
	}

	inline static int32_t get_offset_of__assetCache_9() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_t274320441, ____assetCache_9)); }
	inline AssetCache_t783611398 * get__assetCache_9() const { return ____assetCache_9; }
	inline AssetCache_t783611398 ** get_address_of__assetCache_9() { return &____assetCache_9; }
	inline void set__assetCache_9(AssetCache_t783611398 * value)
	{
		____assetCache_9 = value;
		Il2CppCodeGenWriteBarrier((&____assetCache_9), value);
	}

	inline static int32_t get_offset_of__asyncAction_10() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_t274320441, ____asyncAction_10)); }
	inline AsyncAction_t3334821446 * get__asyncAction_10() const { return ____asyncAction_10; }
	inline AsyncAction_t3334821446 ** get_address_of__asyncAction_10() { return &____asyncAction_10; }
	inline void set__asyncAction_10(AsyncAction_t3334821446 * value)
	{
		____asyncAction_10 = value;
		Il2CppCodeGenWriteBarrier((&____asyncAction_10), value);
	}

	inline static int32_t get_offset_of__addColliders_11() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_t274320441, ____addColliders_11)); }
	inline bool get__addColliders_11() const { return ____addColliders_11; }
	inline bool* get_address_of__addColliders_11() { return &____addColliders_11; }
	inline void set__addColliders_11(bool value)
	{
		____addColliders_11 = value;
	}

	inline static int32_t get_offset_of__gltfData_12() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_t274320441, ____gltfData_12)); }
	inline ByteU5BU5D_t4116647657* get__gltfData_12() const { return ____gltfData_12; }
	inline ByteU5BU5D_t4116647657** get_address_of__gltfData_12() { return &____gltfData_12; }
	inline void set__gltfData_12(ByteU5BU5D_t4116647657* value)
	{
		____gltfData_12 = value;
		Il2CppCodeGenWriteBarrier((&____gltfData_12), value);
	}

	inline static int32_t get_offset_of__loadType_13() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_t274320441, ____loadType_13)); }
	inline int32_t get__loadType_13() const { return ____loadType_13; }
	inline int32_t* get_address_of__loadType_13() { return &____loadType_13; }
	inline void set__loadType_13(int32_t value)
	{
		____loadType_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFSCENEIMPORTER_T274320441_H
#ifndef CP863_T1714397984_H
#define CP863_T1714397984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP863
struct  CP863_t1714397984  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP863_t1714397984_StaticFields
{
public:
	// System.Char[] I18N.West.CP863::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP863_t1714397984_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP863_T1714397984_H
#ifndef CP865_T551598570_H
#define CP865_T551598570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP865
struct  CP865_t551598570  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP865_t551598570_StaticFields
{
public:
	// System.Char[] I18N.West.CP865::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP865_t551598570_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP865_T551598570_H
#ifndef CP1250_T1787607523_H
#define CP1250_T1787607523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP1250
struct  CP1250_t1787607523  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP1250_t1787607523_StaticFields
{
public:
	// System.Char[] I18N.West.CP1250::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP1250_t1787607523_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP1250_T1787607523_H
#ifndef CP500_T2026640899_H
#define CP500_T2026640899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP500
struct  CP500_t2026640899  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP500_t2026640899_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP500::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP500_t2026640899_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP500_T2026640899_H
#ifndef CP10079_T3879478589_H
#define CP10079_T3879478589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP10079
struct  CP10079_t3879478589  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP10079_t3879478589_StaticFields
{
public:
	// System.Char[] I18N.West.CP10079::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP10079_t3879478589_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP10079_T3879478589_H
#ifndef CP37_T108829002_H
#define CP37_T108829002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP37
struct  CP37_t108829002  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP37_t108829002_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP37::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP37_t108829002_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP37_T108829002_H
#ifndef CP1253_T4126259683_H
#define CP1253_T4126259683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP1253
struct  CP1253_t4126259683  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP1253_t4126259683_StaticFields
{
public:
	// System.Char[] I18N.West.CP1253::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP1253_t4126259683_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP1253_T4126259683_H
#ifndef CP21025_T3367402627_H
#define CP21025_T3367402627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP21025
struct  CP21025_t3367402627  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP21025_t3367402627_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP21025::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP21025_t3367402627_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP21025_T3367402627_H
#ifndef CP1252_T2169944547_H
#define CP1252_T2169944547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP1252
struct  CP1252_t2169944547  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP1252_t2169944547_StaticFields
{
public:
	// System.Char[] I18N.West.CP1252::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP1252_t2169944547_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP1252_T2169944547_H
#ifndef CP855_T1623684055_H
#define CP855_T1623684055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP855
struct  CP855_t1623684055  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP855_t1623684055_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP855::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP855_t1623684055_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP855_T1623684055_H
#ifndef CP858_T2383198942_H
#define CP858_T2383198942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP858
struct  CP858_t2383198942  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP858_t2383198942_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP858::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP858_t2383198942_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP858_T2383198942_H
#ifndef CP857_T460884641_H
#define CP857_T460884641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP857
struct  CP857_t460884641  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP857_t460884641_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP857::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP857_t460884641_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP857_T460884641_H
#ifndef CP852_T864169168_H
#define CP852_T864169168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP852
struct  CP852_t864169168  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP852_t864169168_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP852::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP852_t864169168_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP852_T864169168_H
#ifndef CP10000_T776152390_H
#define CP10000_T776152390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP10000
struct  CP10000_t776152390  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP10000_t776152390_StaticFields
{
public:
	// System.Char[] I18N.West.CP10000::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP10000_t776152390_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP10000_T776152390_H
#ifndef CP708_T2382871261_H
#define CP708_T2382871261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP708
struct  CP708_t2382871261  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP708_t2382871261_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP708::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP708_t2382871261_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP708_T2382871261_H
#ifndef CP864_T57665650_H
#define CP864_T57665650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP864
struct  CP864_t57665650  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP864_t57665650_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP864::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP864_t57665650_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP864_T57665650_H
#ifndef CP20871_T2147492183_H
#define CP20871_T2147492183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP20871
struct  CP20871_t2147492183  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP20871_t2147492183_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP20871::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP20871_t2147492183_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP20871_T2147492183_H
#ifndef CP20297_T1010799439_H
#define CP20297_T1010799439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP20297
struct  CP20297_t1010799439  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP20297_t1010799439_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP20297::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP20297_t1010799439_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP20297_T1010799439_H
#ifndef CP20424_T1395871470_H
#define CP20424_T1395871470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP20424
struct  CP20424_t1395871470  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP20424_t1395871470_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP20424::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP20424_t1395871470_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP20424_T1395871470_H
#ifndef CP437_T3683438768_H
#define CP437_T3683438768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP437
struct  CP437_t3683438768  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP437_t3683438768_StaticFields
{
public:
	// System.Char[] I18N.West.CP437::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP437_t3683438768_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP437_T3683438768_H
#ifndef CP20420_T1395871466_H
#define CP20420_T1395871466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP20420
struct  CP20420_t1395871466  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP20420_t1395871466_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP20420::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP20420_t1395871466_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP20420_T1395871466_H
#ifndef CP28597_T3519602203_H
#define CP28597_T3519602203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP28597
struct  CP28597_t3519602203  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP28597_t3519602203_StaticFields
{
public:
	// System.Char[] I18N.West.CP28597::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP28597_t3519602203_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP28597_T3519602203_H
#ifndef CP862_T864234704_H
#define CP862_T864234704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP862
struct  CP862_t864234704  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP862_t864234704_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP862::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP862_t864234704_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP862_T864234704_H
#ifndef CP28592_T3519602206_H
#define CP28592_T3519602206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP28592
struct  CP28592_t3519602206  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP28592_t3519602206_StaticFields
{
public:
	// System.Char[] I18N.West.CP28592::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP28592_t3519602206_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP28592_T3519602206_H
#ifndef CP28605_T1600022914_H
#define CP28605_T1600022914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP28605
struct  CP28605_t1600022914  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP28605_t1600022914_StaticFields
{
public:
	// System.Char[] I18N.West.CP28605::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP28605_t1600022914_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP28605_T1600022914_H
#ifndef CP28593_T3519602207_H
#define CP28593_T3519602207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP28593
struct  CP28593_t3519602207  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP28593_t3519602207_StaticFields
{
public:
	// System.Char[] I18N.West.CP28593::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP28593_t3519602207_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP28593_T3519602207_H
#ifndef ENCIBM297_T2940010377_H
#define ENCIBM297_T2940010377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm297
struct  ENCibm297_t2940010377  : public CP20297_t1010799439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM297_T2940010377_H
#ifndef ENCIBM852_T2537512288_H
#define ENCIBM852_T2537512288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm852
struct  ENCibm852_t2537512288  : public CP852_t864169168
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM852_T2537512288_H
#ifndef ENCIBM871_T4103727301_H
#define ENCIBM871_T4103727301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm871
struct  ENCibm871_t4103727301  : public CP20871_t2147492183
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM871_T4103727301_H
#ifndef ENCIBM855_T1777997401_H
#define ENCIBM855_T1777997401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm855
struct  ENCibm855_t1777997401  : public CP855_t1623684055
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM855_T1777997401_H
#ifndef ENCIBM861_T2247939372_H
#define ENCIBM861_T2247939372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm861
struct  ENCibm861_t2247939372  : public CP861_t2877197398
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM861_T2247939372_H
#ifndef ENCIBM420_T1374516262_H
#define ENCIBM420_T1374516262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm420
struct  ENCibm420_t1374516262  : public CP20420_t1395871466
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM420_T1374516262_H
#ifndef ENCIBM1025_T670129495_H
#define ENCIBM1025_T670129495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm1025
struct  ENCibm1025_t670129495  : public CP21025_t3367402627
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM1025_T670129495_H
#ifndef ENCIBM424_T3343884730_H
#define ENCIBM424_T3343884730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm424
struct  ENCibm424_t3343884730  : public CP20424_t1395871470
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM424_T3343884730_H
#ifndef ENCIBM037_T2940665735_H
#define ENCIBM037_T2940665735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm037
struct  ENCibm037_t2940665735  : public CP37_t108829002
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM037_T2940665735_H
#ifndef ENCASMO_708_T562121192_H
#define ENCASMO_708_T562121192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCasmo_708
struct  ENCasmo_708_t562121192  : public CP708_t2382871261
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCASMO_708_T562121192_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ENCIBM500_T1374385189_H
#define ENCIBM500_T1374385189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm500
struct  ENCibm500_t1374385189  : public CP500_t2026640899
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM500_T1374385189_H
#ifndef ENCISO_8859_2_T815391552_H
#define ENCISO_8859_2_T815391552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCiso_8859_2
struct  ENCiso_8859_2_t815391552  : public CP28592_t3519602206
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCISO_8859_2_T815391552_H
#ifndef ENCISO_8859_3_T2381475493_H
#define ENCISO_8859_3_T2381475493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCiso_8859_3
struct  ENCiso_8859_3_t2381475493  : public CP28593_t3519602207
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCISO_8859_3_T2381475493_H
#ifndef ENCWINDOWS_1253_T3241285643_H
#define ENCWINDOWS_1253_T3241285643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCwindows_1253
struct  ENCwindows_1253_t3241285643  : public CP1253_t4126259683
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCWINDOWS_1253_T3241285643_H
#ifndef ENCWINDOWS_1250_T3241285640_H
#define ENCWINDOWS_1250_T3241285640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCwindows_1250
struct  ENCwindows_1250_t3241285640  : public CP1250_t1787607523
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCWINDOWS_1250_T3241285640_H
#ifndef ENCWINDOWS_1252_T3241285642_H
#define ENCWINDOWS_1252_T3241285642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCwindows_1252
struct  ENCwindows_1252_t3241285642  : public CP1252_t2169944547
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCWINDOWS_1252_T3241285642_H
#ifndef ENCIBM865_T278570904_H
#define ENCIBM865_T278570904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm865
struct  ENCibm865_t278570904  : public CP865_t551598570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM865_T278570904_H
#ifndef ENCIBM860_T3814023313_H
#define ENCIBM860_T3814023313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm860
struct  ENCibm860_t3814023313  : public CP860_t1311113457
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM860_T3814023313_H
#ifndef ENCIBM850_T3813826705_H
#define ENCIBM850_T3813826705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm850
struct  ENCibm850_t3813826705  : public CP850_t1311178993
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM850_T3813826705_H
#ifndef ENCISO_8859_15_T3881981915_H
#define ENCISO_8859_15_T3881981915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCiso_8859_15
struct  ENCiso_8859_15_t3881981915  : public CP28605_t1600022914
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCISO_8859_15_T3881981915_H
#ifndef ENCIBM863_T1085139958_H
#define ENCIBM863_T1085139958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm863
struct  ENCibm863_t1085139958  : public CP863_t1714397984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM863_T1085139958_H
#ifndef ENCISO_8859_7_T412107025_H
#define ENCISO_8859_7_T412107025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCiso_8859_7
struct  ENCiso_8859_7_t412107025  : public CP28597_t3519602203
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCISO_8859_7_T412107025_H
#ifndef ENCIBM864_T3344146878_H
#define ENCIBM864_T3344146878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm864
struct  ENCibm864_t3344146878  : public CP864_t57665650
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM864_T3344146878_H
#ifndef ENCMACINTOSH_T1087697298_H
#define ENCMACINTOSH_T1087697298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCmacintosh
struct  ENCmacintosh_t1087697298  : public CP10000_t776152390
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCMACINTOSH_T1087697298_H
#ifndef ENCIBM857_T2940796815_H
#define ENCIBM857_T2940796815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm857
struct  ENCibm857_t2940796815  : public CP857_t460884641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM857_T2940796815_H
#ifndef ENCIBM00858_T2458074995_H
#define ENCIBM00858_T2458074995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm00858
struct  ENCibm00858_t2458074995  : public CP858_t2383198942
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM00858_T2458074995_H
#ifndef ENCIBM437_T3410411110_H
#define ENCIBM437_T3410411110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm437
struct  ENCibm437_t3410411110  : public CP437_t3683438768
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM437_T3410411110_H
#ifndef ENCIBM862_T2537577824_H
#define ENCIBM862_T2537577824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm862
struct  ENCibm862_t2537577824  : public CP862_t864234704
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM862_T2537577824_H
#ifndef TWEET_T251520785_H
#define TWEET_T251520785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tweet
struct  Tweet_t251520785  : public ScriptableObject_t2528358522
{
public:
	// System.String Tweet::consumerKey
	String_t* ___consumerKey_3;
	// System.String Tweet::consumerKeySecret
	String_t* ___consumerKeySecret_4;
	// System.String Tweet::accessToken
	String_t* ___accessToken_5;
	// System.String Tweet::accessTokenSecret
	String_t* ___accessTokenSecret_6;
	// System.Security.Cryptography.HMACSHA1 Tweet::sigHasher
	HMACSHA1_t1952596188 * ___sigHasher_7;
	// System.DateTime Tweet::epochUtc
	DateTime_t3738529785  ___epochUtc_8;

public:
	inline static int32_t get_offset_of_consumerKey_3() { return static_cast<int32_t>(offsetof(Tweet_t251520785, ___consumerKey_3)); }
	inline String_t* get_consumerKey_3() const { return ___consumerKey_3; }
	inline String_t** get_address_of_consumerKey_3() { return &___consumerKey_3; }
	inline void set_consumerKey_3(String_t* value)
	{
		___consumerKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___consumerKey_3), value);
	}

	inline static int32_t get_offset_of_consumerKeySecret_4() { return static_cast<int32_t>(offsetof(Tweet_t251520785, ___consumerKeySecret_4)); }
	inline String_t* get_consumerKeySecret_4() const { return ___consumerKeySecret_4; }
	inline String_t** get_address_of_consumerKeySecret_4() { return &___consumerKeySecret_4; }
	inline void set_consumerKeySecret_4(String_t* value)
	{
		___consumerKeySecret_4 = value;
		Il2CppCodeGenWriteBarrier((&___consumerKeySecret_4), value);
	}

	inline static int32_t get_offset_of_accessToken_5() { return static_cast<int32_t>(offsetof(Tweet_t251520785, ___accessToken_5)); }
	inline String_t* get_accessToken_5() const { return ___accessToken_5; }
	inline String_t** get_address_of_accessToken_5() { return &___accessToken_5; }
	inline void set_accessToken_5(String_t* value)
	{
		___accessToken_5 = value;
		Il2CppCodeGenWriteBarrier((&___accessToken_5), value);
	}

	inline static int32_t get_offset_of_accessTokenSecret_6() { return static_cast<int32_t>(offsetof(Tweet_t251520785, ___accessTokenSecret_6)); }
	inline String_t* get_accessTokenSecret_6() const { return ___accessTokenSecret_6; }
	inline String_t** get_address_of_accessTokenSecret_6() { return &___accessTokenSecret_6; }
	inline void set_accessTokenSecret_6(String_t* value)
	{
		___accessTokenSecret_6 = value;
		Il2CppCodeGenWriteBarrier((&___accessTokenSecret_6), value);
	}

	inline static int32_t get_offset_of_sigHasher_7() { return static_cast<int32_t>(offsetof(Tweet_t251520785, ___sigHasher_7)); }
	inline HMACSHA1_t1952596188 * get_sigHasher_7() const { return ___sigHasher_7; }
	inline HMACSHA1_t1952596188 ** get_address_of_sigHasher_7() { return &___sigHasher_7; }
	inline void set_sigHasher_7(HMACSHA1_t1952596188 * value)
	{
		___sigHasher_7 = value;
		Il2CppCodeGenWriteBarrier((&___sigHasher_7), value);
	}

	inline static int32_t get_offset_of_epochUtc_8() { return static_cast<int32_t>(offsetof(Tweet_t251520785, ___epochUtc_8)); }
	inline DateTime_t3738529785  get_epochUtc_8() const { return ___epochUtc_8; }
	inline DateTime_t3738529785 * get_address_of_epochUtc_8() { return &___epochUtc_8; }
	inline void set_epochUtc_8(DateTime_t3738529785  value)
	{
		___epochUtc_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEET_T251520785_H
#ifndef ENCX_MAC_ICELANDIC_T2411177152_H
#define ENCX_MAC_ICELANDIC_T2411177152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCx_mac_icelandic
struct  ENCx_mac_icelandic_t2411177152  : public CP10079_t3879478589
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCX_MAC_ICELANDIC_T2411177152_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef MICROPHONEMANAGER_T2580314274_H
#define MICROPHONEMANAGER_T2580314274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MicrophoneManager
struct  MicrophoneManager_t2580314274  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text MicrophoneManager::DictationDisplay
	Text_t1901882714 * ___DictationDisplay_2;
	// UnityEngine.Windows.Speech.DictationRecognizer MicrophoneManager::dictationRecognizer
	DictationRecognizer_t838322161 * ___dictationRecognizer_3;
	// System.Text.StringBuilder MicrophoneManager::textSoFar
	StringBuilder_t1712802186 * ___textSoFar_4;
	// System.Int32 MicrophoneManager::samplingRate
	int32_t ___samplingRate_6;
	// System.Boolean MicrophoneManager::hasRecordingStarted
	bool ___hasRecordingStarted_8;
	// System.Text.RegularExpressions.Regex MicrophoneManager::regex
	Regex_t3657309853 * ___regex_9;

public:
	inline static int32_t get_offset_of_DictationDisplay_2() { return static_cast<int32_t>(offsetof(MicrophoneManager_t2580314274, ___DictationDisplay_2)); }
	inline Text_t1901882714 * get_DictationDisplay_2() const { return ___DictationDisplay_2; }
	inline Text_t1901882714 ** get_address_of_DictationDisplay_2() { return &___DictationDisplay_2; }
	inline void set_DictationDisplay_2(Text_t1901882714 * value)
	{
		___DictationDisplay_2 = value;
		Il2CppCodeGenWriteBarrier((&___DictationDisplay_2), value);
	}

	inline static int32_t get_offset_of_dictationRecognizer_3() { return static_cast<int32_t>(offsetof(MicrophoneManager_t2580314274, ___dictationRecognizer_3)); }
	inline DictationRecognizer_t838322161 * get_dictationRecognizer_3() const { return ___dictationRecognizer_3; }
	inline DictationRecognizer_t838322161 ** get_address_of_dictationRecognizer_3() { return &___dictationRecognizer_3; }
	inline void set_dictationRecognizer_3(DictationRecognizer_t838322161 * value)
	{
		___dictationRecognizer_3 = value;
		Il2CppCodeGenWriteBarrier((&___dictationRecognizer_3), value);
	}

	inline static int32_t get_offset_of_textSoFar_4() { return static_cast<int32_t>(offsetof(MicrophoneManager_t2580314274, ___textSoFar_4)); }
	inline StringBuilder_t1712802186 * get_textSoFar_4() const { return ___textSoFar_4; }
	inline StringBuilder_t1712802186 ** get_address_of_textSoFar_4() { return &___textSoFar_4; }
	inline void set_textSoFar_4(StringBuilder_t1712802186 * value)
	{
		___textSoFar_4 = value;
		Il2CppCodeGenWriteBarrier((&___textSoFar_4), value);
	}

	inline static int32_t get_offset_of_samplingRate_6() { return static_cast<int32_t>(offsetof(MicrophoneManager_t2580314274, ___samplingRate_6)); }
	inline int32_t get_samplingRate_6() const { return ___samplingRate_6; }
	inline int32_t* get_address_of_samplingRate_6() { return &___samplingRate_6; }
	inline void set_samplingRate_6(int32_t value)
	{
		___samplingRate_6 = value;
	}

	inline static int32_t get_offset_of_hasRecordingStarted_8() { return static_cast<int32_t>(offsetof(MicrophoneManager_t2580314274, ___hasRecordingStarted_8)); }
	inline bool get_hasRecordingStarted_8() const { return ___hasRecordingStarted_8; }
	inline bool* get_address_of_hasRecordingStarted_8() { return &___hasRecordingStarted_8; }
	inline void set_hasRecordingStarted_8(bool value)
	{
		___hasRecordingStarted_8 = value;
	}

	inline static int32_t get_offset_of_regex_9() { return static_cast<int32_t>(offsetof(MicrophoneManager_t2580314274, ___regex_9)); }
	inline Regex_t3657309853 * get_regex_9() const { return ___regex_9; }
	inline Regex_t3657309853 ** get_address_of_regex_9() { return &___regex_9; }
	inline void set_regex_9(Regex_t3657309853 * value)
	{
		___regex_9 = value;
		Il2CppCodeGenWriteBarrier((&___regex_9), value);
	}
};

struct MicrophoneManager_t2580314274_StaticFields
{
public:
	// System.String MicrophoneManager::deviceName
	String_t* ___deviceName_5;

public:
	inline static int32_t get_offset_of_deviceName_5() { return static_cast<int32_t>(offsetof(MicrophoneManager_t2580314274_StaticFields, ___deviceName_5)); }
	inline String_t* get_deviceName_5() const { return ___deviceName_5; }
	inline String_t** get_address_of_deviceName_5() { return &___deviceName_5; }
	inline void set_deviceName_5(String_t* value)
	{
		___deviceName_5 = value;
		Il2CppCodeGenWriteBarrier((&___deviceName_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROPHONEMANAGER_T2580314274_H
#ifndef MOVESCRIPT_T4183579406_H
#define MOVESCRIPT_T4183579406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.Earth.MoveScript
struct  MoveScript_t4183579406  : public MonoBehaviour_t3962482529
{
public:
	// System.Single DigitalRuby.Earth.MoveScript::Speed
	float ___Speed_2;

public:
	inline static int32_t get_offset_of_Speed_2() { return static_cast<int32_t>(offsetof(MoveScript_t4183579406, ___Speed_2)); }
	inline float get_Speed_2() const { return ___Speed_2; }
	inline float* get_address_of_Speed_2() { return &___Speed_2; }
	inline void set_Speed_2(float value)
	{
		___Speed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVESCRIPT_T4183579406_H
#ifndef GLTFCOMPONENT_T238219215_H
#define GLTFCOMPONENT_T238219215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFComponent
struct  GLTFComponent_t238219215  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityGLTF.GLTFComponent::Url
	String_t* ___Url_2;
	// System.Boolean UnityGLTF.GLTFComponent::Multithreaded
	bool ___Multithreaded_3;
	// System.Boolean UnityGLTF.GLTFComponent::UseStream
	bool ___UseStream_4;
	// System.Int32 UnityGLTF.GLTFComponent::MaximumLod
	int32_t ___MaximumLod_5;
	// UnityEngine.Shader UnityGLTF.GLTFComponent::GLTFStandard
	Shader_t4151988712 * ___GLTFStandard_6;
	// UnityEngine.Shader UnityGLTF.GLTFComponent::GLTFStandardSpecular
	Shader_t4151988712 * ___GLTFStandardSpecular_7;
	// UnityEngine.Shader UnityGLTF.GLTFComponent::GLTFConstant
	Shader_t4151988712 * ___GLTFConstant_8;
	// System.Boolean UnityGLTF.GLTFComponent::addColliders
	bool ___addColliders_9;
	// System.IO.Stream UnityGLTF.GLTFComponent::GLTFStream
	Stream_t1273022909 * ___GLTFStream_10;
	// System.Boolean UnityGLTF.GLTFComponent::<IsLoaded>k__BackingField
	bool ___U3CIsLoadedU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_Url_2() { return static_cast<int32_t>(offsetof(GLTFComponent_t238219215, ___Url_2)); }
	inline String_t* get_Url_2() const { return ___Url_2; }
	inline String_t** get_address_of_Url_2() { return &___Url_2; }
	inline void set_Url_2(String_t* value)
	{
		___Url_2 = value;
		Il2CppCodeGenWriteBarrier((&___Url_2), value);
	}

	inline static int32_t get_offset_of_Multithreaded_3() { return static_cast<int32_t>(offsetof(GLTFComponent_t238219215, ___Multithreaded_3)); }
	inline bool get_Multithreaded_3() const { return ___Multithreaded_3; }
	inline bool* get_address_of_Multithreaded_3() { return &___Multithreaded_3; }
	inline void set_Multithreaded_3(bool value)
	{
		___Multithreaded_3 = value;
	}

	inline static int32_t get_offset_of_UseStream_4() { return static_cast<int32_t>(offsetof(GLTFComponent_t238219215, ___UseStream_4)); }
	inline bool get_UseStream_4() const { return ___UseStream_4; }
	inline bool* get_address_of_UseStream_4() { return &___UseStream_4; }
	inline void set_UseStream_4(bool value)
	{
		___UseStream_4 = value;
	}

	inline static int32_t get_offset_of_MaximumLod_5() { return static_cast<int32_t>(offsetof(GLTFComponent_t238219215, ___MaximumLod_5)); }
	inline int32_t get_MaximumLod_5() const { return ___MaximumLod_5; }
	inline int32_t* get_address_of_MaximumLod_5() { return &___MaximumLod_5; }
	inline void set_MaximumLod_5(int32_t value)
	{
		___MaximumLod_5 = value;
	}

	inline static int32_t get_offset_of_GLTFStandard_6() { return static_cast<int32_t>(offsetof(GLTFComponent_t238219215, ___GLTFStandard_6)); }
	inline Shader_t4151988712 * get_GLTFStandard_6() const { return ___GLTFStandard_6; }
	inline Shader_t4151988712 ** get_address_of_GLTFStandard_6() { return &___GLTFStandard_6; }
	inline void set_GLTFStandard_6(Shader_t4151988712 * value)
	{
		___GLTFStandard_6 = value;
		Il2CppCodeGenWriteBarrier((&___GLTFStandard_6), value);
	}

	inline static int32_t get_offset_of_GLTFStandardSpecular_7() { return static_cast<int32_t>(offsetof(GLTFComponent_t238219215, ___GLTFStandardSpecular_7)); }
	inline Shader_t4151988712 * get_GLTFStandardSpecular_7() const { return ___GLTFStandardSpecular_7; }
	inline Shader_t4151988712 ** get_address_of_GLTFStandardSpecular_7() { return &___GLTFStandardSpecular_7; }
	inline void set_GLTFStandardSpecular_7(Shader_t4151988712 * value)
	{
		___GLTFStandardSpecular_7 = value;
		Il2CppCodeGenWriteBarrier((&___GLTFStandardSpecular_7), value);
	}

	inline static int32_t get_offset_of_GLTFConstant_8() { return static_cast<int32_t>(offsetof(GLTFComponent_t238219215, ___GLTFConstant_8)); }
	inline Shader_t4151988712 * get_GLTFConstant_8() const { return ___GLTFConstant_8; }
	inline Shader_t4151988712 ** get_address_of_GLTFConstant_8() { return &___GLTFConstant_8; }
	inline void set_GLTFConstant_8(Shader_t4151988712 * value)
	{
		___GLTFConstant_8 = value;
		Il2CppCodeGenWriteBarrier((&___GLTFConstant_8), value);
	}

	inline static int32_t get_offset_of_addColliders_9() { return static_cast<int32_t>(offsetof(GLTFComponent_t238219215, ___addColliders_9)); }
	inline bool get_addColliders_9() const { return ___addColliders_9; }
	inline bool* get_address_of_addColliders_9() { return &___addColliders_9; }
	inline void set_addColliders_9(bool value)
	{
		___addColliders_9 = value;
	}

	inline static int32_t get_offset_of_GLTFStream_10() { return static_cast<int32_t>(offsetof(GLTFComponent_t238219215, ___GLTFStream_10)); }
	inline Stream_t1273022909 * get_GLTFStream_10() const { return ___GLTFStream_10; }
	inline Stream_t1273022909 ** get_address_of_GLTFStream_10() { return &___GLTFStream_10; }
	inline void set_GLTFStream_10(Stream_t1273022909 * value)
	{
		___GLTFStream_10 = value;
		Il2CppCodeGenWriteBarrier((&___GLTFStream_10), value);
	}

	inline static int32_t get_offset_of_U3CIsLoadedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GLTFComponent_t238219215, ___U3CIsLoadedU3Ek__BackingField_11)); }
	inline bool get_U3CIsLoadedU3Ek__BackingField_11() const { return ___U3CIsLoadedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsLoadedU3Ek__BackingField_11() { return &___U3CIsLoadedU3Ek__BackingField_11; }
	inline void set_U3CIsLoadedU3Ek__BackingField_11(bool value)
	{
		___U3CIsLoadedU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFCOMPONENT_T238219215_H
#ifndef EARTHROOTSCRIPT_T2136688071_H
#define EARTHROOTSCRIPT_T2136688071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.Earth.EarthRootScript
struct  EarthRootScript_t2136688071  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EARTHROOTSCRIPT_T2136688071_H
#ifndef DISPLAYDEPENDENTOBJECTACTIVATOR_T701741189_H
#define DISPLAYDEPENDENTOBJECTACTIVATOR_T701741189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisplayDependentObjectActivator
struct  DisplayDependentObjectActivator_t701741189  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean DisplayDependentObjectActivator::OpaqueDisplay
	bool ___OpaqueDisplay_2;
	// System.Boolean DisplayDependentObjectActivator::TransparentDisplay
	bool ___TransparentDisplay_3;

public:
	inline static int32_t get_offset_of_OpaqueDisplay_2() { return static_cast<int32_t>(offsetof(DisplayDependentObjectActivator_t701741189, ___OpaqueDisplay_2)); }
	inline bool get_OpaqueDisplay_2() const { return ___OpaqueDisplay_2; }
	inline bool* get_address_of_OpaqueDisplay_2() { return &___OpaqueDisplay_2; }
	inline void set_OpaqueDisplay_2(bool value)
	{
		___OpaqueDisplay_2 = value;
	}

	inline static int32_t get_offset_of_TransparentDisplay_3() { return static_cast<int32_t>(offsetof(DisplayDependentObjectActivator_t701741189, ___TransparentDisplay_3)); }
	inline bool get_TransparentDisplay_3() const { return ___TransparentDisplay_3; }
	inline bool* get_address_of_TransparentDisplay_3() { return &___TransparentDisplay_3; }
	inline void set_TransparentDisplay_3(bool value)
	{
		___TransparentDisplay_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYDEPENDENTOBJECTACTIVATOR_T701741189_H
#ifndef SPHERESCRIPT_T2867235001_H
#define SPHERESCRIPT_T2867235001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.Earth.SphereScript
struct  SphereScript_t2867235001  : public MonoBehaviour_t3962482529
{
public:
	// System.Single DigitalRuby.Earth.SphereScript::Radius
	float ___Radius_2;
	// System.Int32 DigitalRuby.Earth.SphereScript::Detail
	int32_t ___Detail_3;
	// System.Boolean DigitalRuby.Earth.SphereScript::IcoSphere
	bool ___IcoSphere_4;
	// System.Single DigitalRuby.Earth.SphereScript::lastRadius
	float ___lastRadius_5;
	// System.Int32 DigitalRuby.Earth.SphereScript::lastDetail
	int32_t ___lastDetail_6;
	// System.Boolean DigitalRuby.Earth.SphereScript::lastIcoSphere
	bool ___lastIcoSphere_7;
	// System.Boolean DigitalRuby.Earth.SphereScript::dirty
	bool ___dirty_8;

public:
	inline static int32_t get_offset_of_Radius_2() { return static_cast<int32_t>(offsetof(SphereScript_t2867235001, ___Radius_2)); }
	inline float get_Radius_2() const { return ___Radius_2; }
	inline float* get_address_of_Radius_2() { return &___Radius_2; }
	inline void set_Radius_2(float value)
	{
		___Radius_2 = value;
	}

	inline static int32_t get_offset_of_Detail_3() { return static_cast<int32_t>(offsetof(SphereScript_t2867235001, ___Detail_3)); }
	inline int32_t get_Detail_3() const { return ___Detail_3; }
	inline int32_t* get_address_of_Detail_3() { return &___Detail_3; }
	inline void set_Detail_3(int32_t value)
	{
		___Detail_3 = value;
	}

	inline static int32_t get_offset_of_IcoSphere_4() { return static_cast<int32_t>(offsetof(SphereScript_t2867235001, ___IcoSphere_4)); }
	inline bool get_IcoSphere_4() const { return ___IcoSphere_4; }
	inline bool* get_address_of_IcoSphere_4() { return &___IcoSphere_4; }
	inline void set_IcoSphere_4(bool value)
	{
		___IcoSphere_4 = value;
	}

	inline static int32_t get_offset_of_lastRadius_5() { return static_cast<int32_t>(offsetof(SphereScript_t2867235001, ___lastRadius_5)); }
	inline float get_lastRadius_5() const { return ___lastRadius_5; }
	inline float* get_address_of_lastRadius_5() { return &___lastRadius_5; }
	inline void set_lastRadius_5(float value)
	{
		___lastRadius_5 = value;
	}

	inline static int32_t get_offset_of_lastDetail_6() { return static_cast<int32_t>(offsetof(SphereScript_t2867235001, ___lastDetail_6)); }
	inline int32_t get_lastDetail_6() const { return ___lastDetail_6; }
	inline int32_t* get_address_of_lastDetail_6() { return &___lastDetail_6; }
	inline void set_lastDetail_6(int32_t value)
	{
		___lastDetail_6 = value;
	}

	inline static int32_t get_offset_of_lastIcoSphere_7() { return static_cast<int32_t>(offsetof(SphereScript_t2867235001, ___lastIcoSphere_7)); }
	inline bool get_lastIcoSphere_7() const { return ___lastIcoSphere_7; }
	inline bool* get_address_of_lastIcoSphere_7() { return &___lastIcoSphere_7; }
	inline void set_lastIcoSphere_7(bool value)
	{
		___lastIcoSphere_7 = value;
	}

	inline static int32_t get_offset_of_dirty_8() { return static_cast<int32_t>(offsetof(SphereScript_t2867235001, ___dirty_8)); }
	inline bool get_dirty_8() const { return ___dirty_8; }
	inline bool* get_address_of_dirty_8() { return &___dirty_8; }
	inline void set_dirty_8(bool value)
	{
		___dirty_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPHERESCRIPT_T2867235001_H
#ifndef EARTHSCRIPT_T2062648451_H
#define EARTHSCRIPT_T2062648451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.Earth.EarthScript
struct  EarthScript_t2062648451  : public SphereScript_t2867235001
{
public:
	// System.Boolean DigitalRuby.Earth.EarthScript::Rotate
	bool ___Rotate_9;
	// System.Single DigitalRuby.Earth.EarthScript::RotationSpeedX
	float ___RotationSpeedX_10;
	// System.Single DigitalRuby.Earth.EarthScript::RotationSpeedY
	float ___RotationSpeedY_11;
	// System.Single DigitalRuby.Earth.EarthScript::RotationSpeedZ
	float ___RotationSpeedZ_12;

public:
	inline static int32_t get_offset_of_Rotate_9() { return static_cast<int32_t>(offsetof(EarthScript_t2062648451, ___Rotate_9)); }
	inline bool get_Rotate_9() const { return ___Rotate_9; }
	inline bool* get_address_of_Rotate_9() { return &___Rotate_9; }
	inline void set_Rotate_9(bool value)
	{
		___Rotate_9 = value;
	}

	inline static int32_t get_offset_of_RotationSpeedX_10() { return static_cast<int32_t>(offsetof(EarthScript_t2062648451, ___RotationSpeedX_10)); }
	inline float get_RotationSpeedX_10() const { return ___RotationSpeedX_10; }
	inline float* get_address_of_RotationSpeedX_10() { return &___RotationSpeedX_10; }
	inline void set_RotationSpeedX_10(float value)
	{
		___RotationSpeedX_10 = value;
	}

	inline static int32_t get_offset_of_RotationSpeedY_11() { return static_cast<int32_t>(offsetof(EarthScript_t2062648451, ___RotationSpeedY_11)); }
	inline float get_RotationSpeedY_11() const { return ___RotationSpeedY_11; }
	inline float* get_address_of_RotationSpeedY_11() { return &___RotationSpeedY_11; }
	inline void set_RotationSpeedY_11(float value)
	{
		___RotationSpeedY_11 = value;
	}

	inline static int32_t get_offset_of_RotationSpeedZ_12() { return static_cast<int32_t>(offsetof(EarthScript_t2062648451, ___RotationSpeedZ_12)); }
	inline float get_RotationSpeedZ_12() const { return ___RotationSpeedZ_12; }
	inline float* get_address_of_RotationSpeedZ_12() { return &___RotationSpeedZ_12; }
	inline void set_RotationSpeedZ_12(float value)
	{
		___RotationSpeedZ_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EARTHSCRIPT_T2062648451_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5200 = { sizeof (CP20297_t1010799439), -1, sizeof(CP20297_t1010799439_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5200[1] = 
{
	CP20297_t1010799439_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5201 = { sizeof (ENCibm297_t2940010377), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5202 = { sizeof (CP20420_t1395871466), -1, sizeof(CP20420_t1395871466_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5202[1] = 
{
	CP20420_t1395871466_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5203 = { sizeof (ENCibm420_t1374516262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5204 = { sizeof (CP20424_t1395871470), -1, sizeof(CP20424_t1395871470_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5204[1] = 
{
	CP20424_t1395871470_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5205 = { sizeof (ENCibm424_t3343884730), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5206 = { sizeof (CP20871_t2147492183), -1, sizeof(CP20871_t2147492183_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5206[1] = 
{
	CP20871_t2147492183_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5207 = { sizeof (ENCibm871_t4103727301), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5208 = { sizeof (CP21025_t3367402627), -1, sizeof(CP21025_t3367402627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5208[1] = 
{
	CP21025_t3367402627_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5209 = { sizeof (ENCibm1025_t670129495), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5210 = { sizeof (CP37_t108829002), -1, sizeof(CP37_t108829002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5210[1] = 
{
	CP37_t108829002_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5211 = { sizeof (ENCibm037_t2940665735), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5212 = { sizeof (CP500_t2026640899), -1, sizeof(CP500_t2026640899_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5212[1] = 
{
	CP500_t2026640899_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5213 = { sizeof (ENCibm500_t1374385189), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5214 = { sizeof (CP708_t2382871261), -1, sizeof(CP708_t2382871261_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5214[1] = 
{
	CP708_t2382871261_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5215 = { sizeof (ENCasmo_708_t562121192), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5216 = { sizeof (CP852_t864169168), -1, sizeof(CP852_t864169168_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5216[1] = 
{
	CP852_t864169168_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5217 = { sizeof (ENCibm852_t2537512288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5218 = { sizeof (CP855_t1623684055), -1, sizeof(CP855_t1623684055_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5218[1] = 
{
	CP855_t1623684055_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5219 = { sizeof (ENCibm855_t1777997401), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5220 = { sizeof (CP857_t460884641), -1, sizeof(CP857_t460884641_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5220[1] = 
{
	CP857_t460884641_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5221 = { sizeof (ENCibm857_t2940796815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5222 = { sizeof (CP858_t2383198942), -1, sizeof(CP858_t2383198942_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5222[1] = 
{
	CP858_t2383198942_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5223 = { sizeof (ENCibm00858_t2458074995), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5224 = { sizeof (CP862_t864234704), -1, sizeof(CP862_t864234704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5224[1] = 
{
	CP862_t864234704_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5225 = { sizeof (ENCibm862_t2537577824), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5226 = { sizeof (CP864_t57665650), -1, sizeof(CP864_t57665650_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5226[1] = 
{
	CP864_t57665650_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5227 = { sizeof (ENCibm864_t3344146878), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5228 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255374), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5228[37] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U305B1E1067273E188A50BA4342B4BC09ABEE669CD_0(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U309885C8B0840E863B3A14261999895D896677D5A_1(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U30BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U3271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U32E1C78D5F4666324672DFACDC9307935CF14E98F_4(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U33551F16F8A11003B8289B76B9E3D97969B68E7D9_5(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U336A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U337C03949C69BDDA042205E1A573B349E75F693B8_7(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U347DCC01E928630EBCC018C5E0285145985F92532_8(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U352623ED7F33E8C1C541F1C3101FA981B173D795E_9(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U35EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U3659C24C855D0E50CBD366B2774AF841E29202E70_11(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U36B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U371D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U37A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U37AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U382273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U38C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U38E958C1847104F390F9C9B64F0F39C98ED4AC63F_18(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U394F89F72CC0508D891A0C71DD7B57B703869A3FB_19(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U39E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U39EF46C677634CB82C0BD475E372C71C5F68C981A_21(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_A5A5BE77BD3D095389FABC21D18BB648787E6618_22(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_B2574B82126B684C0CB3CF93BF7473F0FBB34400_25(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_BC1CEF8131E7F0D8206029373157806126E21026_27(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_C14817C33562352073848F1A8EA633C19CF1A4F5_28(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_C392E993B9E622A36C30E0BB997A9F41293CD9EF_29(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_DF4C38A5E59685BBEC109623762B6914BE00E866_35(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5229 = { sizeof (__StaticArrayInitTypeSizeU3D512_t3317833662)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D512_t3317833662 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5230 = { sizeof (U3CModuleU3E_t692745588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5231 = { sizeof (Consts_t1749595338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5231[41] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5232 = { sizeof (CP10000_t776152390), -1, sizeof(CP10000_t776152390_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5232[1] = 
{
	CP10000_t776152390_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5233 = { sizeof (ENCmacintosh_t1087697298), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5234 = { sizeof (CP10079_t3879478589), -1, sizeof(CP10079_t3879478589_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5234[1] = 
{
	CP10079_t3879478589_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5235 = { sizeof (ENCx_mac_icelandic_t2411177152), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5236 = { sizeof (CP1250_t1787607523), -1, sizeof(CP1250_t1787607523_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5236[1] = 
{
	CP1250_t1787607523_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5237 = { sizeof (ENCwindows_1250_t3241285640), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5238 = { sizeof (CP1252_t2169944547), -1, sizeof(CP1252_t2169944547_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5238[1] = 
{
	CP1252_t2169944547_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5239 = { sizeof (ENCwindows_1252_t3241285642), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5240 = { sizeof (CP1253_t4126259683), -1, sizeof(CP1253_t4126259683_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5240[1] = 
{
	CP1253_t4126259683_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5241 = { sizeof (ENCwindows_1253_t3241285643), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5242 = { sizeof (CP28592_t3519602206), -1, sizeof(CP28592_t3519602206_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5242[1] = 
{
	CP28592_t3519602206_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5243 = { sizeof (ENCiso_8859_2_t815391552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5244 = { sizeof (CP28593_t3519602207), -1, sizeof(CP28593_t3519602207_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5244[1] = 
{
	CP28593_t3519602207_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5245 = { sizeof (ENCiso_8859_3_t2381475493), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5246 = { sizeof (CP28597_t3519602203), -1, sizeof(CP28597_t3519602203_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5246[1] = 
{
	CP28597_t3519602203_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5247 = { sizeof (ENCiso_8859_7_t412107025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5248 = { sizeof (CP28605_t1600022914), -1, sizeof(CP28605_t1600022914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5248[1] = 
{
	CP28605_t1600022914_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5249 = { sizeof (ENCiso_8859_15_t3881981915), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5250 = { sizeof (CP437_t3683438768), -1, sizeof(CP437_t3683438768_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5250[1] = 
{
	CP437_t3683438768_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5251 = { sizeof (ENCibm437_t3410411110), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5252 = { sizeof (CP850_t1311178993), -1, sizeof(CP850_t1311178993_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5252[1] = 
{
	CP850_t1311178993_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5253 = { sizeof (ENCibm850_t3813826705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5254 = { sizeof (CP860_t1311113457), -1, sizeof(CP860_t1311113457_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5254[1] = 
{
	CP860_t1311113457_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5255 = { sizeof (ENCibm860_t3814023313), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5256 = { sizeof (CP861_t2877197398), -1, sizeof(CP861_t2877197398_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5256[1] = 
{
	CP861_t2877197398_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5257 = { sizeof (ENCibm861_t2247939372), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5258 = { sizeof (CP863_t1714397984), -1, sizeof(CP863_t1714397984_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5258[1] = 
{
	CP863_t1714397984_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5259 = { sizeof (ENCibm863_t1085139958), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5260 = { sizeof (CP865_t551598570), -1, sizeof(CP865_t551598570_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5260[1] = 
{
	CP865_t551598570_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5261 = { sizeof (ENCibm865_t278570904), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5262 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255375), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5262[15] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_U305C64B887A4D766EEB5842345D6B609A0E3A91C9_0(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_U3148F346330E294B4157ED412259A7E7F373E26A8_1(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_U31F867F0E96DB3A56943B8CB2662D1DA624023FCD_2(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_U33220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_U3356CE585046545CD2D0B109FF8A85DB88EE29074_4(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_U34FEA2A6324C0192B29C90830F5D578051A4B1B16_5(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_U3598D9433A53523A59D462896B803E416AB0B1901_6(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_U36E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_U37089F9820A6F9CC830909BCD1FAF2EF0A540F348_8(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_U39ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5263 = { sizeof (__StaticArrayInitTypeSizeU3D512_t3317833663)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D512_t3317833663 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5264 = { sizeof (U3CModuleU3E_t692745589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5265 = { sizeof (BoundsExtensions_t3769520328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5265[34] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5266 = { sizeof (Axis_t3661294207)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5266[4] = 
{
	Axis_t3661294207::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5267 = { sizeof (DisplayDependentObjectActivator_t701741189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5267[2] = 
{
	DisplayDependentObjectActivator_t701741189::get_offset_of_OpaqueDisplay_2(),
	DisplayDependentObjectActivator_t701741189::get_offset_of_TransparentDisplay_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5268 = { sizeof (FastSimplexNoise_t743524795), -1, sizeof(FastSimplexNoise_t743524795_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5268[30] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	FastSimplexNoise_t743524795::get_offset_of_perm_11(),
	FastSimplexNoise_t743524795::get_offset_of_perm2D_12(),
	FastSimplexNoise_t743524795::get_offset_of_perm3D_13(),
	FastSimplexNoise_t743524795::get_offset_of_perm4D_14(),
	FastSimplexNoise_t743524795_StaticFields::get_offset_of_gradients2D_15(),
	FastSimplexNoise_t743524795_StaticFields::get_offset_of_gradients3D_16(),
	FastSimplexNoise_t743524795_StaticFields::get_offset_of_gradients4D_17(),
	FastSimplexNoise_t743524795_StaticFields::get_offset_of_p2D_18(),
	FastSimplexNoise_t743524795_StaticFields::get_offset_of_p3D_19(),
	FastSimplexNoise_t743524795_StaticFields::get_offset_of_p4D_20(),
	FastSimplexNoise_t743524795_StaticFields::get_offset_of_lookupPairs2D_21(),
	FastSimplexNoise_t743524795_StaticFields::get_offset_of_lookupPairs3D_22(),
	FastSimplexNoise_t743524795_StaticFields::get_offset_of_lookupPairs4D_23(),
	FastSimplexNoise_t743524795_StaticFields::get_offset_of_base2D_24(),
	FastSimplexNoise_t743524795_StaticFields::get_offset_of_base3D_25(),
	FastSimplexNoise_t743524795_StaticFields::get_offset_of_base4D_26(),
	FastSimplexNoise_t743524795_StaticFields::get_offset_of_lookup2D_27(),
	FastSimplexNoise_t743524795_StaticFields::get_offset_of_lookup3D_28(),
	FastSimplexNoise_t743524795_StaticFields::get_offset_of_lookup4D_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5269 = { sizeof (Contribution2_t1917161235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5269[5] = 
{
	Contribution2_t1917161235::get_offset_of_dx_0(),
	Contribution2_t1917161235::get_offset_of_dy_1(),
	Contribution2_t1917161235::get_offset_of_xsb_2(),
	Contribution2_t1917161235::get_offset_of_ysb_3(),
	Contribution2_t1917161235::get_offset_of_Next_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5270 = { sizeof (Contribution3_t1917226771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5270[7] = 
{
	Contribution3_t1917226771::get_offset_of_dx_0(),
	Contribution3_t1917226771::get_offset_of_dy_1(),
	Contribution3_t1917226771::get_offset_of_dz_2(),
	Contribution3_t1917226771::get_offset_of_xsb_3(),
	Contribution3_t1917226771::get_offset_of_ysb_4(),
	Contribution3_t1917226771::get_offset_of_zsb_5(),
	Contribution3_t1917226771::get_offset_of_Next_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5271 = { sizeof (Contribution4_t1916768019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5271[9] = 
{
	Contribution4_t1916768019::get_offset_of_dx_0(),
	Contribution4_t1916768019::get_offset_of_dy_1(),
	Contribution4_t1916768019::get_offset_of_dz_2(),
	Contribution4_t1916768019::get_offset_of_dw_3(),
	Contribution4_t1916768019::get_offset_of_xsb_4(),
	Contribution4_t1916768019::get_offset_of_ysb_5(),
	Contribution4_t1916768019::get_offset_of_zsb_6(),
	Contribution4_t1916768019::get_offset_of_wsb_7(),
	Contribution4_t1916768019::get_offset_of_Next_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5272 = { sizeof (MicrophoneManager_t2580314274), -1, sizeof(MicrophoneManager_t2580314274_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5272[8] = 
{
	MicrophoneManager_t2580314274::get_offset_of_DictationDisplay_2(),
	MicrophoneManager_t2580314274::get_offset_of_dictationRecognizer_3(),
	MicrophoneManager_t2580314274::get_offset_of_textSoFar_4(),
	MicrophoneManager_t2580314274_StaticFields::get_offset_of_deviceName_5(),
	MicrophoneManager_t2580314274::get_offset_of_samplingRate_6(),
	0,
	MicrophoneManager_t2580314274::get_offset_of_hasRecordingStarted_8(),
	MicrophoneManager_t2580314274::get_offset_of_regex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5273 = { sizeof (U3CRestartSpeechSystemU3Ed__19_t3166134508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5273[4] = 
{
	U3CRestartSpeechSystemU3Ed__19_t3166134508::get_offset_of_U3CU3E1__state_0(),
	U3CRestartSpeechSystemU3Ed__19_t3166134508::get_offset_of_U3CU3E2__current_1(),
	U3CRestartSpeechSystemU3Ed__19_t3166134508::get_offset_of_keywordToStart_2(),
	U3CRestartSpeechSystemU3Ed__19_t3166134508::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5274 = { sizeof (Tweet_t251520785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5274[7] = 
{
	0,
	Tweet_t251520785::get_offset_of_consumerKey_3(),
	Tweet_t251520785::get_offset_of_consumerKeySecret_4(),
	Tweet_t251520785::get_offset_of_accessToken_5(),
	Tweet_t251520785::get_offset_of_accessTokenSecret_6(),
	Tweet_t251520785::get_offset_of_sigHasher_7(),
	Tweet_t251520785::get_offset_of_epochUtc_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5275 = { sizeof (AsyncAction_t3334821446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5275[2] = 
{
	AsyncAction_t3334821446::get_offset_of__workerThreadRunning_0(),
	AsyncAction_t3334821446::get_offset_of__savedException_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5276 = { sizeof (U3CU3Ec__DisplayClass2_0_t4123515534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5276[2] = 
{
	U3CU3Ec__DisplayClass2_0_t4123515534::get_offset_of_action_0(),
	U3CU3Ec__DisplayClass2_0_t4123515534::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5277 = { sizeof (U3CRunOnWorkerThreadU3Ed__2_t1197827754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5277[5] = 
{
	U3CRunOnWorkerThreadU3Ed__2_t1197827754::get_offset_of_U3CU3E1__state_0(),
	U3CRunOnWorkerThreadU3Ed__2_t1197827754::get_offset_of_U3CU3E2__current_1(),
	U3CRunOnWorkerThreadU3Ed__2_t1197827754::get_offset_of_action_2(),
	U3CRunOnWorkerThreadU3Ed__2_t1197827754::get_offset_of_U3CU3E4__this_3(),
	U3CRunOnWorkerThreadU3Ed__2_t1197827754::get_offset_of_U3CU3E8__1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5278 = { sizeof (U3CWaitU3Ed__3_t2236800517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5278[3] = 
{
	U3CWaitU3Ed__3_t2236800517::get_offset_of_U3CU3E1__state_0(),
	U3CWaitU3Ed__3_t2236800517::get_offset_of_U3CU3E2__current_1(),
	U3CWaitU3Ed__3_t2236800517::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5279 = { sizeof (WebRequestException_t1410780468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5280 = { sizeof (GLTFComponent_t238219215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5280[10] = 
{
	GLTFComponent_t238219215::get_offset_of_Url_2(),
	GLTFComponent_t238219215::get_offset_of_Multithreaded_3(),
	GLTFComponent_t238219215::get_offset_of_UseStream_4(),
	GLTFComponent_t238219215::get_offset_of_MaximumLod_5(),
	GLTFComponent_t238219215::get_offset_of_GLTFStandard_6(),
	GLTFComponent_t238219215::get_offset_of_GLTFStandardSpecular_7(),
	GLTFComponent_t238219215::get_offset_of_GLTFConstant_8(),
	GLTFComponent_t238219215::get_offset_of_addColliders_9(),
	GLTFComponent_t238219215::get_offset_of_GLTFStream_10(),
	GLTFComponent_t238219215::get_offset_of_U3CIsLoadedU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5281 = { sizeof (U3CStartU3Ed__13_t203077579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5281[5] = 
{
	U3CStartU3Ed__13_t203077579::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__13_t203077579::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__13_t203077579::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__13_t203077579::get_offset_of_U3CloaderU3E5__1_3(),
	U3CStartU3Ed__13_t203077579::get_offset_of_U3CfullPathU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5282 = { sizeof (U3CWaitForModelLoadU3Ed__14_t282584680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5282[3] = 
{
	U3CWaitForModelLoadU3Ed__14_t282584680::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForModelLoadU3Ed__14_t282584680::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForModelLoadU3Ed__14_t282584680::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5283 = { sizeof (GLTFSceneImporter_t274320441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5283[15] = 
{
	GLTFSceneImporter_t274320441::get_offset_of__lastLoadedScene_0(),
	GLTFSceneImporter_t274320441::get_offset_of__sceneParent_1(),
	GLTFSceneImporter_t274320441::get_offset_of__shaderCache_2(),
	GLTFSceneImporter_t274320441::get_offset_of_MaximumLod_3(),
	GLTFSceneImporter_t274320441::get_offset_of_DefaultMaterial_4(),
	GLTFSceneImporter_t274320441::get_offset_of__gltfUrl_5(),
	GLTFSceneImporter_t274320441::get_offset_of__gltfDirectoryPath_6(),
	GLTFSceneImporter_t274320441::get_offset_of__gltfStream_7(),
	GLTFSceneImporter_t274320441::get_offset_of__root_8(),
	GLTFSceneImporter_t274320441::get_offset_of__assetCache_9(),
	GLTFSceneImporter_t274320441::get_offset_of__asyncAction_10(),
	GLTFSceneImporter_t274320441::get_offset_of__addColliders_11(),
	GLTFSceneImporter_t274320441::get_offset_of__gltfData_12(),
	GLTFSceneImporter_t274320441::get_offset_of__loadType_13(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5284 = { sizeof (MaterialType_t597773032)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5284[7] = 
{
	MaterialType_t597773032::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5285 = { sizeof (LoadType_t225521639)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5285[3] = 
{
	LoadType_t225521639::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5286 = { sizeof (U3CLoadU3Ed__21_t2072795234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5286[7] = 
{
	U3CLoadU3Ed__21_t2072795234::get_offset_of_U3CU3E1__state_0(),
	U3CLoadU3Ed__21_t2072795234::get_offset_of_U3CU3E2__current_1(),
	U3CLoadU3Ed__21_t2072795234::get_offset_of_sceneIndex_2(),
	U3CLoadU3Ed__21_t2072795234::get_offset_of_isMultithreaded_3(),
	U3CLoadU3Ed__21_t2072795234::get_offset_of_U3CU3E4__this_4(),
	U3CLoadU3Ed__21_t2072795234::get_offset_of_U3CwwwU3E5__1_5(),
	U3CLoadU3Ed__21_t2072795234::get_offset_of_U3CstreamLengthU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5287 = { sizeof (U3CImportSceneU3Ed__22_t599600113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5287[12] = 
{
	U3CImportSceneU3Ed__22_t599600113::get_offset_of_U3CU3E1__state_0(),
	U3CImportSceneU3Ed__22_t599600113::get_offset_of_U3CU3E2__current_1(),
	U3CImportSceneU3Ed__22_t599600113::get_offset_of_sceneIndex_2(),
	U3CImportSceneU3Ed__22_t599600113::get_offset_of_isMultithreaded_3(),
	U3CImportSceneU3Ed__22_t599600113::get_offset_of_U3CU3E4__this_4(),
	U3CImportSceneU3Ed__22_t599600113::get_offset_of_U3CsceneU3E5__1_5(),
	U3CImportSceneU3Ed__22_t599600113::get_offset_of_U3CsceneObjU3E5__2_6(),
	U3CImportSceneU3Ed__22_t599600113::get_offset_of_U3CiU3E5__3_7(),
	U3CImportSceneU3Ed__22_t599600113::get_offset_of_U3CbufferU3E5__4_8(),
	U3CImportSceneU3Ed__22_t599600113::get_offset_of_U3CglbBufferU3E5__5_9(),
	U3CImportSceneU3Ed__22_t599600113::get_offset_of_U3CiU3E5__6_10(),
	U3CImportSceneU3Ed__22_t599600113::get_offset_of_U3CimageU3E5__7_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5288 = { sizeof (U3CLoadImageU3Ed__33_t1921818520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5288[21] = 
{
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CU3E1__state_0(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CU3E2__current_1(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_rootPath_2(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_image_3(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_imageID_4(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CU3E4__this_5(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CtextureU3E5__1_6(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CuriU3E5__2_7(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CregexU3E5__3_8(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CmatchU3E5__4_9(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3Cbase64DataU3E5__5_10(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CtextureDataU3E5__6_11(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CwwwU3E5__7_12(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CtempTextureU3E5__8_13(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CpathToLoadU3E5__9_14(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CfileU3E5__10_15(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CbufferDataU3E5__11_16(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CbufferViewU3E5__12_17(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CbufferU3E5__13_18(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CdataU3E5__14_19(),
	U3CLoadImageU3Ed__33_t1921818520::get_offset_of_U3CbufferContentsU3E5__15_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5289 = { sizeof (U3CLoadBufferU3Ed__34_t2608884096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5289[14] = 
{
	U3CLoadBufferU3Ed__34_t2608884096::get_offset_of_U3CU3E1__state_0(),
	U3CLoadBufferU3Ed__34_t2608884096::get_offset_of_U3CU3E2__current_1(),
	U3CLoadBufferU3Ed__34_t2608884096::get_offset_of_sourceUri_2(),
	U3CLoadBufferU3Ed__34_t2608884096::get_offset_of_buffer_3(),
	U3CLoadBufferU3Ed__34_t2608884096::get_offset_of_bufferIndex_4(),
	U3CLoadBufferU3Ed__34_t2608884096::get_offset_of_U3CU3E4__this_5(),
	U3CLoadBufferU3Ed__34_t2608884096::get_offset_of_U3CbufferDataU3E5__1_6(),
	U3CLoadBufferU3Ed__34_t2608884096::get_offset_of_U3CuriU3E5__2_7(),
	U3CLoadBufferU3Ed__34_t2608884096::get_offset_of_U3CregexU3E5__3_8(),
	U3CLoadBufferU3Ed__34_t2608884096::get_offset_of_U3CmatchU3E5__4_9(),
	U3CLoadBufferU3Ed__34_t2608884096::get_offset_of_U3Cbase64DataU3E5__5_10(),
	U3CLoadBufferU3Ed__34_t2608884096::get_offset_of_U3CwwwU3E5__6_11(),
	U3CLoadBufferU3Ed__34_t2608884096::get_offset_of_U3CpathToLoadU3E5__7_12(),
	U3CLoadBufferU3Ed__34_t2608884096::get_offset_of_U3CfileU3E5__8_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5290 = { sizeof (GLTFUnityHelpers_t1495654367), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5291 = { sizeof (SchemaExtensions_t3846062055), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5292 = { sizeof (AssetCache_t783611398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5292[5] = 
{
	AssetCache_t783611398::get_offset_of_U3CImageCacheU3Ek__BackingField_0(),
	AssetCache_t783611398::get_offset_of_U3CTextureCacheU3Ek__BackingField_1(),
	AssetCache_t783611398::get_offset_of_U3CMaterialCacheU3Ek__BackingField_2(),
	AssetCache_t783611398::get_offset_of_U3CBufferCacheU3Ek__BackingField_3(),
	AssetCache_t783611398::get_offset_of_U3CMeshCacheU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5293 = { sizeof (MaterialCacheData_t2754836532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5293[3] = 
{
	MaterialCacheData_t2754836532::get_offset_of_U3CUnityMaterialU3Ek__BackingField_0(),
	MaterialCacheData_t2754836532::get_offset_of_U3CUnityMaterialWithVertexColorU3Ek__BackingField_1(),
	MaterialCacheData_t2754836532::get_offset_of_U3CGLTFMaterialU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5294 = { sizeof (MeshCacheData_t1944180602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5294[2] = 
{
	MeshCacheData_t1944180602::get_offset_of_U3CLoadedMeshU3Ek__BackingField_0(),
	MeshCacheData_t1944180602::get_offset_of_U3CMeshAttributesU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5295 = { sizeof (SystemException_t176217641), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5296 = { sizeof (HandleRef_t3745784363)+ sizeof (RuntimeObject), sizeof(HandleRef_t3745784363_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5296[2] = 
{
	HandleRef_t3745784363::get_offset_of_wrapper_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HandleRef_t3745784363::get_offset_of_handle_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5297 = { sizeof (EarthRootScript_t2136688071), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5298 = { sizeof (EarthScript_t2062648451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5298[4] = 
{
	EarthScript_t2062648451::get_offset_of_Rotate_9(),
	EarthScript_t2062648451::get_offset_of_RotationSpeedX_10(),
	EarthScript_t2062648451::get_offset_of_RotationSpeedY_11(),
	EarthScript_t2062648451::get_offset_of_RotationSpeedZ_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5299 = { sizeof (MoveScript_t4183579406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5299[1] = 
{
	MoveScript_t4183579406::get_offset_of_Speed_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
