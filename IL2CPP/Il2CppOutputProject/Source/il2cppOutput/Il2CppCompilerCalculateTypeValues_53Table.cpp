﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.EventHandler
struct EventHandler_t1348719766;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Func`2<System.Reflection.ParameterInfo,System.Type>
struct Func_2_t3692615456;
// System.String
struct String_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_0
struct SwigDelegatePairMaker_0_t3088859200;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_1
struct SwigDelegatePairMaker_1_t3088859199;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_2
struct SwigDelegatePairMaker_2_t3088859202;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_3
struct SwigDelegatePairMaker_3_t3088859201;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_4
struct SwigDelegatePairMaker_4_t3088859196;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_5
struct SwigDelegatePairMaker_5_t3088859195;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_6
struct SwigDelegatePairMaker_6_t3088859198;
// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_7
struct SwigDelegatePairMaker_7_t3088859197;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// HoloToolkit.Sharing.LogWriter/SwigDelegateLogWriter_0
struct SwigDelegateLogWriter_0_t50445701;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// HoloToolkit.Sharing.PairingListener/SwigDelegatePairingListener_0
struct SwigDelegatePairingListener_0_t4183180789;
// HoloToolkit.Sharing.PairingListener/SwigDelegatePairingListener_1
struct SwigDelegatePairingListener_1_t4183180788;
// HoloToolkit.Sharing.IntArrayListener/SwigDelegateIntArrayListener_0
struct SwigDelegateIntArrayListener_0_t3309985819;
// HoloToolkit.Sharing.IntArrayListener/SwigDelegateIntArrayListener_1
struct SwigDelegateIntArrayListener_1_t1743901878;
// HoloToolkit.Sharing.IntArrayListener/SwigDelegateIntArrayListener_2
struct SwigDelegateIntArrayListener_2_t177817937;
// HoloToolkit.Sharing.DiscoveryClientListener/SwigDelegateDiscoveryClientListener_0
struct SwigDelegateDiscoveryClientListener_0_t3614305483;
// HoloToolkit.Sharing.DiscoveryClientListener/SwigDelegateDiscoveryClientListener_1
struct SwigDelegateDiscoveryClientListener_1_t3614305484;
// HoloToolkit.Sharing.FloatArrayListener/SwigDelegateFloatArrayListener_0
struct SwigDelegateFloatArrayListener_0_t2300273156;
// HoloToolkit.Sharing.FloatArrayListener/SwigDelegateFloatArrayListener_1
struct SwigDelegateFloatArrayListener_1_t734189215;
// HoloToolkit.Sharing.FloatArrayListener/SwigDelegateFloatArrayListener_2
struct SwigDelegateFloatArrayListener_2_t1137473742;
// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_0
struct SwigDelegateObjectElementListener_0_t271806937;
// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_1
struct SwigDelegateObjectElementListener_1_t271806938;
// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_2
struct SwigDelegateObjectElementListener_2_t271806935;
// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_3
struct SwigDelegateObjectElementListener_3_t271806936;
// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_4
struct SwigDelegateObjectElementListener_4_t271806941;
// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_5
struct SwigDelegateObjectElementListener_5_t271806942;
// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_6
struct SwigDelegateObjectElementListener_6_t271806939;
// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_7
struct SwigDelegateObjectElementListener_7_t271806940;
// HoloToolkit.Sharing.NetworkConnectionListener/SwigDelegateNetworkConnectionListener_0
struct SwigDelegateNetworkConnectionListener_0_t4021727018;
// HoloToolkit.Sharing.NetworkConnectionListener/SwigDelegateNetworkConnectionListener_1
struct SwigDelegateNetworkConnectionListener_1_t4021727017;
// HoloToolkit.Sharing.NetworkConnectionListener/SwigDelegateNetworkConnectionListener_2
struct SwigDelegateNetworkConnectionListener_2_t4021727020;
// HoloToolkit.Sharing.NetworkConnectionListener/SwigDelegateNetworkConnectionListener_3
struct SwigDelegateNetworkConnectionListener_3_t4021727019;
// System.Action`1<HoloToolkit.Sharing.DiscoveredSystem>
struct Action_1_t1236426205;
// System.Action
struct Action_t1264377477;
// System.Action`1<HoloToolkit.Sharing.PairingResult>
struct Action_1_t1658156947;
// System.Action`2<System.Int64,System.Boolean>
struct Action_2_t3497391435;
// System.Action`2<System.Int64,System.Int32>
struct Action_2_t2056081927;
// System.Action`2<System.Int64,System.Int64>
struct Action_2_t2841703478;
// System.Action`2<System.Int64,System.Single>
struct Action_2_t502402948;
// System.Action`2<System.Int64,System.Double>
struct Action_2_t3994768833;
// System.Action`2<System.Int64,HoloToolkit.Sharing.XString>
struct Action_2_t45974928;
// System.Action`1<HoloToolkit.Sharing.Element>
struct Action_1_t827361636;
// System.Action`1<HoloToolkit.Sharing.NetworkConnection>
struct Action_1_t1272347286;
// System.Action`2<HoloToolkit.Sharing.NetworkConnection,HoloToolkit.Sharing.NetworkInMessage>
struct Action_2_t2041518244;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Image
struct Image_t2670269651;
// HoloToolkit.UI.Keyboard.Keyboard
struct Keyboard_t4047957915;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t881764471;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// HoloToolkit.UI.Keyboard.AxisSlider
struct AxisSlider_t300566400;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t731888065;
// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t648412432;
// UnityEngine.UI.InputField/OnChangeEvent
struct OnChangeEvent_t467195904;
// UnityEngine.UI.InputField/OnValidateInput
struct OnValidateInput_t2355412304;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.Event
struct Event_t2956885303;
// UnityEngine.Transform
struct Transform_t3600365921;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef WINDOWSAPICHECKER_T3065255428_H
#define WINDOWSAPICHECKER_T3065255428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.WindowsApiChecker
struct  WindowsApiChecker_t3065255428  : public RuntimeObject
{
public:

public:
};

struct WindowsApiChecker_t3065255428_StaticFields
{
public:
	// System.Boolean HoloToolkit.WindowsApiChecker::<UniversalApiContractV5_IsAvailable>k__BackingField
	bool ___U3CUniversalApiContractV5_IsAvailableU3Ek__BackingField_0;
	// System.Boolean HoloToolkit.WindowsApiChecker::<UniversalApiContractV4_IsAvailable>k__BackingField
	bool ___U3CUniversalApiContractV4_IsAvailableU3Ek__BackingField_1;
	// System.Boolean HoloToolkit.WindowsApiChecker::<UniversalApiContractV3_IsAvailable>k__BackingField
	bool ___U3CUniversalApiContractV3_IsAvailableU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CUniversalApiContractV5_IsAvailableU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WindowsApiChecker_t3065255428_StaticFields, ___U3CUniversalApiContractV5_IsAvailableU3Ek__BackingField_0)); }
	inline bool get_U3CUniversalApiContractV5_IsAvailableU3Ek__BackingField_0() const { return ___U3CUniversalApiContractV5_IsAvailableU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CUniversalApiContractV5_IsAvailableU3Ek__BackingField_0() { return &___U3CUniversalApiContractV5_IsAvailableU3Ek__BackingField_0; }
	inline void set_U3CUniversalApiContractV5_IsAvailableU3Ek__BackingField_0(bool value)
	{
		___U3CUniversalApiContractV5_IsAvailableU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CUniversalApiContractV4_IsAvailableU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WindowsApiChecker_t3065255428_StaticFields, ___U3CUniversalApiContractV4_IsAvailableU3Ek__BackingField_1)); }
	inline bool get_U3CUniversalApiContractV4_IsAvailableU3Ek__BackingField_1() const { return ___U3CUniversalApiContractV4_IsAvailableU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CUniversalApiContractV4_IsAvailableU3Ek__BackingField_1() { return &___U3CUniversalApiContractV4_IsAvailableU3Ek__BackingField_1; }
	inline void set_U3CUniversalApiContractV4_IsAvailableU3Ek__BackingField_1(bool value)
	{
		___U3CUniversalApiContractV4_IsAvailableU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CUniversalApiContractV3_IsAvailableU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WindowsApiChecker_t3065255428_StaticFields, ___U3CUniversalApiContractV3_IsAvailableU3Ek__BackingField_2)); }
	inline bool get_U3CUniversalApiContractV3_IsAvailableU3Ek__BackingField_2() const { return ___U3CUniversalApiContractV3_IsAvailableU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CUniversalApiContractV3_IsAvailableU3Ek__BackingField_2() { return &___U3CUniversalApiContractV3_IsAvailableU3Ek__BackingField_2; }
	inline void set_U3CUniversalApiContractV3_IsAvailableU3Ek__BackingField_2(bool value)
	{
		___U3CUniversalApiContractV3_IsAvailableU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWSAPICHECKER_T3065255428_H
#ifndef TYPEUTILS_T3188311306_H
#define TYPEUTILS_T3188311306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.TypeUtils
struct  TypeUtils_t3188311306  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEUTILS_T3188311306_H
#ifndef U3CU3EC_T1768260159_H
#define U3CU3EC_T1768260159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.UI.Keyboard.Keyboard/<>c
struct  U3CU3Ec_t1768260159  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1768260159_StaticFields
{
public:
	// HoloToolkit.UI.Keyboard.Keyboard/<>c HoloToolkit.UI.Keyboard.Keyboard/<>c::<>9
	U3CU3Ec_t1768260159 * ___U3CU3E9_0;
	// System.EventHandler HoloToolkit.UI.Keyboard.Keyboard/<>c::<>9__101_0
	EventHandler_t1348719766 * ___U3CU3E9__101_0_1;
	// System.Action`1<System.String> HoloToolkit.UI.Keyboard.Keyboard/<>c::<>9__101_1
	Action_1_t2019918284 * ___U3CU3E9__101_1_2;
	// System.EventHandler HoloToolkit.UI.Keyboard.Keyboard/<>c::<>9__101_2
	EventHandler_t1348719766 * ___U3CU3E9__101_2_3;
	// System.EventHandler HoloToolkit.UI.Keyboard.Keyboard/<>c::<>9__101_3
	EventHandler_t1348719766 * ___U3CU3E9__101_3_4;
	// System.EventHandler HoloToolkit.UI.Keyboard.Keyboard/<>c::<>9__101_4
	EventHandler_t1348719766 * ___U3CU3E9__101_4_5;
	// System.EventHandler HoloToolkit.UI.Keyboard.Keyboard/<>c::<>9__101_5
	EventHandler_t1348719766 * ___U3CU3E9__101_5_6;
	// System.Action`1<System.Boolean> HoloToolkit.UI.Keyboard.Keyboard/<>c::<>9__101_6
	Action_1_t269755560 * ___U3CU3E9__101_6_7;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1768260159_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1768260159 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1768260159 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1768260159 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__101_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1768260159_StaticFields, ___U3CU3E9__101_0_1)); }
	inline EventHandler_t1348719766 * get_U3CU3E9__101_0_1() const { return ___U3CU3E9__101_0_1; }
	inline EventHandler_t1348719766 ** get_address_of_U3CU3E9__101_0_1() { return &___U3CU3E9__101_0_1; }
	inline void set_U3CU3E9__101_0_1(EventHandler_t1348719766 * value)
	{
		___U3CU3E9__101_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__101_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__101_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1768260159_StaticFields, ___U3CU3E9__101_1_2)); }
	inline Action_1_t2019918284 * get_U3CU3E9__101_1_2() const { return ___U3CU3E9__101_1_2; }
	inline Action_1_t2019918284 ** get_address_of_U3CU3E9__101_1_2() { return &___U3CU3E9__101_1_2; }
	inline void set_U3CU3E9__101_1_2(Action_1_t2019918284 * value)
	{
		___U3CU3E9__101_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__101_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__101_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1768260159_StaticFields, ___U3CU3E9__101_2_3)); }
	inline EventHandler_t1348719766 * get_U3CU3E9__101_2_3() const { return ___U3CU3E9__101_2_3; }
	inline EventHandler_t1348719766 ** get_address_of_U3CU3E9__101_2_3() { return &___U3CU3E9__101_2_3; }
	inline void set_U3CU3E9__101_2_3(EventHandler_t1348719766 * value)
	{
		___U3CU3E9__101_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__101_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__101_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1768260159_StaticFields, ___U3CU3E9__101_3_4)); }
	inline EventHandler_t1348719766 * get_U3CU3E9__101_3_4() const { return ___U3CU3E9__101_3_4; }
	inline EventHandler_t1348719766 ** get_address_of_U3CU3E9__101_3_4() { return &___U3CU3E9__101_3_4; }
	inline void set_U3CU3E9__101_3_4(EventHandler_t1348719766 * value)
	{
		___U3CU3E9__101_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__101_3_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__101_4_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1768260159_StaticFields, ___U3CU3E9__101_4_5)); }
	inline EventHandler_t1348719766 * get_U3CU3E9__101_4_5() const { return ___U3CU3E9__101_4_5; }
	inline EventHandler_t1348719766 ** get_address_of_U3CU3E9__101_4_5() { return &___U3CU3E9__101_4_5; }
	inline void set_U3CU3E9__101_4_5(EventHandler_t1348719766 * value)
	{
		___U3CU3E9__101_4_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__101_4_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__101_5_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1768260159_StaticFields, ___U3CU3E9__101_5_6)); }
	inline EventHandler_t1348719766 * get_U3CU3E9__101_5_6() const { return ___U3CU3E9__101_5_6; }
	inline EventHandler_t1348719766 ** get_address_of_U3CU3E9__101_5_6() { return &___U3CU3E9__101_5_6; }
	inline void set_U3CU3E9__101_5_6(EventHandler_t1348719766 * value)
	{
		___U3CU3E9__101_5_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__101_5_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__101_6_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1768260159_StaticFields, ___U3CU3E9__101_6_7)); }
	inline Action_1_t269755560 * get_U3CU3E9__101_6_7() const { return ___U3CU3E9__101_6_7; }
	inline Action_1_t269755560 ** get_address_of_U3CU3E9__101_6_7() { return &___U3CU3E9__101_6_7; }
	inline void set_U3CU3E9__101_6_7(Action_1_t269755560 * value)
	{
		___U3CU3E9__101_6_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__101_6_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1768260159_H
#ifndef SESSIONEXTENSIONS_T1740927046_H
#define SESSIONEXTENSIONS_T1740927046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionExtensions
struct  SessionExtensions_t1740927046  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONEXTENSIONS_T1740927046_H
#ifndef U3CU3EC_T1266415778_H
#define U3CU3EC_T1266415778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.ReflectionExtensions/<>c
struct  U3CU3Ec_t1266415778  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1266415778_StaticFields
{
public:
	// HoloToolkit.ReflectionExtensions/<>c HoloToolkit.ReflectionExtensions/<>c::<>9
	U3CU3Ec_t1266415778 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.ParameterInfo,System.Type> HoloToolkit.ReflectionExtensions/<>c::<>9__4_2
	Func_2_t3692615456 * ___U3CU3E9__4_2_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1266415778_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1266415778 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1266415778 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1266415778 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1266415778_StaticFields, ___U3CU3E9__4_2_1)); }
	inline Func_2_t3692615456 * get_U3CU3E9__4_2_1() const { return ___U3CU3E9__4_2_1; }
	inline Func_2_t3692615456 ** get_address_of_U3CU3E9__4_2_1() { return &___U3CU3E9__4_2_1; }
	inline void set_U3CU3E9__4_2_1(Func_2_t3692615456 * value)
	{
		___U3CU3E9__4_2_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1266415778_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef REFLECTIONEXTENSIONS_T4058370115_H
#define REFLECTIONEXTENSIONS_T4058370115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.ReflectionExtensions
struct  ReflectionExtensions_t4058370115  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONEXTENSIONS_T4058370115_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T1345556967_H
#define U3CU3EC__DISPLAYCLASS4_0_T1345556967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.ReflectionExtensions/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t1345556967  : public RuntimeObject
{
public:
	// System.String HoloToolkit.ReflectionExtensions/<>c__DisplayClass4_0::methodName
	String_t* ___methodName_0;
	// System.Type[] HoloToolkit.ReflectionExtensions/<>c__DisplayClass4_0::parameters
	TypeU5BU5D_t3940880105* ___parameters_1;

public:
	inline static int32_t get_offset_of_methodName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t1345556967, ___methodName_0)); }
	inline String_t* get_methodName_0() const { return ___methodName_0; }
	inline String_t** get_address_of_methodName_0() { return &___methodName_0; }
	inline void set_methodName_0(String_t* value)
	{
		___methodName_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodName_0), value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t1345556967, ___parameters_1)); }
	inline TypeU5BU5D_t3940880105* get_parameters_1() const { return ___parameters_1; }
	inline TypeU5BU5D_t3940880105** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(TypeU5BU5D_t3940880105* value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T1345556967_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef TRIANGLEINDICES_T1683999176_H
#define TRIANGLEINDICES_T1683999176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.Earth.SphereScript/TriangleIndices
struct  TriangleIndices_t1683999176 
{
public:
	// System.Int32 DigitalRuby.Earth.SphereScript/TriangleIndices::v1
	int32_t ___v1_0;
	// System.Int32 DigitalRuby.Earth.SphereScript/TriangleIndices::v2
	int32_t ___v2_1;
	// System.Int32 DigitalRuby.Earth.SphereScript/TriangleIndices::v3
	int32_t ___v3_2;

public:
	inline static int32_t get_offset_of_v1_0() { return static_cast<int32_t>(offsetof(TriangleIndices_t1683999176, ___v1_0)); }
	inline int32_t get_v1_0() const { return ___v1_0; }
	inline int32_t* get_address_of_v1_0() { return &___v1_0; }
	inline void set_v1_0(int32_t value)
	{
		___v1_0 = value;
	}

	inline static int32_t get_offset_of_v2_1() { return static_cast<int32_t>(offsetof(TriangleIndices_t1683999176, ___v2_1)); }
	inline int32_t get_v2_1() const { return ___v2_1; }
	inline int32_t* get_address_of_v2_1() { return &___v2_1; }
	inline void set_v2_1(int32_t value)
	{
		___v2_1 = value;
	}

	inline static int32_t get_offset_of_v3_2() { return static_cast<int32_t>(offsetof(TriangleIndices_t1683999176, ___v3_2)); }
	inline int32_t get_v3_2() const { return ___v3_2; }
	inline int32_t* get_address_of_v3_2() { return &___v3_2; }
	inline void set_v3_2(int32_t value)
	{
		___v3_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGLEINDICES_T1683999176_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef MIDDLEPOINTCACHEKEY_T1252889446_H
#define MIDDLEPOINTCACHEKEY_T1252889446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.Earth.SphereScript/MiddlePointCacheKey
struct  MiddlePointCacheKey_t1252889446 
{
public:
	// System.Int32 DigitalRuby.Earth.SphereScript/MiddlePointCacheKey::Key1
	int32_t ___Key1_0;
	// System.Int32 DigitalRuby.Earth.SphereScript/MiddlePointCacheKey::Key2
	int32_t ___Key2_1;

public:
	inline static int32_t get_offset_of_Key1_0() { return static_cast<int32_t>(offsetof(MiddlePointCacheKey_t1252889446, ___Key1_0)); }
	inline int32_t get_Key1_0() const { return ___Key1_0; }
	inline int32_t* get_address_of_Key1_0() { return &___Key1_0; }
	inline void set_Key1_0(int32_t value)
	{
		___Key1_0 = value;
	}

	inline static int32_t get_offset_of_Key2_1() { return static_cast<int32_t>(offsetof(MiddlePointCacheKey_t1252889446, ___Key2_1)); }
	inline int32_t get_Key2_1() const { return ___Key2_1; }
	inline int32_t* get_address_of_Key2_1() { return &___Key2_1; }
	inline void set_Key2_1(int32_t value)
	{
		___Key2_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIDDLEPOINTCACHEKEY_T1252889446_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t594665363_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t594665363_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef ELEMENTTYPE_T2760602766_H
#define ELEMENTTYPE_T2760602766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ElementType
struct  ElementType_t2760602766 
{
public:
	// System.Int32 HoloToolkit.Sharing.ElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ElementType_t2760602766, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTTYPE_T2760602766_H
#ifndef MACHINESESSIONSTATE_T745471285_H
#define MACHINESESSIONSTATE_T745471285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.MachineSessionState
struct  MachineSessionState_t745471285 
{
public:
	// System.Int32 HoloToolkit.Sharing.MachineSessionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MachineSessionState_t745471285, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MACHINESESSIONSTATE_T745471285_H
#ifndef PAIRINGRESULT_T1485689352_H
#define PAIRINGRESULT_T1485689352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairingResult
struct  PairingResult_t1485689352 
{
public:
	// System.Int32 HoloToolkit.Sharing.PairingResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PairingResult_t1485689352, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIRINGRESULT_T1485689352_H
#ifndef MESSAGEID_T254005563_H
#define MESSAGEID_T254005563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.MessageID
struct  MessageID_t254005563 
{
public:
	// System.Int32 HoloToolkit.Sharing.MessageID::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MessageID_t254005563, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEID_T254005563_H
#ifndef MESSAGECHANNEL_T22737989_H
#define MESSAGECHANNEL_T22737989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.MessageChannel
struct  MessageChannel_t22737989 
{
public:
	// System.Int32 HoloToolkit.Sharing.MessageChannel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MessageChannel_t22737989, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGECHANNEL_T22737989_H
#ifndef LOGSEVERITY_T4123842851_H
#define LOGSEVERITY_T4123842851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.LogSeverity
struct  LogSeverity_t4123842851 
{
public:
	// System.Int32 HoloToolkit.Sharing.LogSeverity::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogSeverity_t4123842851, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGSEVERITY_T4123842851_H
#ifndef MESSAGERELIABILITY_T2687606723_H
#define MESSAGERELIABILITY_T2687606723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.MessageReliability
struct  MessageReliability_t2687606723 
{
public:
	// System.Int32 HoloToolkit.Sharing.MessageReliability::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MessageReliability_t2687606723, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGERELIABILITY_T2687606723_H
#ifndef HANDLEREF_T3745784363_H
#define HANDLEREF_T3745784363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.HandleRef
struct  HandleRef_t3745784363 
{
public:
	// System.Object System.Runtime.InteropServices.HandleRef::wrapper
	RuntimeObject * ___wrapper_0;
	// System.IntPtr System.Runtime.InteropServices.HandleRef::handle
	intptr_t ___handle_1;

public:
	inline static int32_t get_offset_of_wrapper_0() { return static_cast<int32_t>(offsetof(HandleRef_t3745784363, ___wrapper_0)); }
	inline RuntimeObject * get_wrapper_0() const { return ___wrapper_0; }
	inline RuntimeObject ** get_address_of_wrapper_0() { return &___wrapper_0; }
	inline void set_wrapper_0(RuntimeObject * value)
	{
		___wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___wrapper_0), value);
	}

	inline static int32_t get_offset_of_handle_1() { return static_cast<int32_t>(offsetof(HandleRef_t3745784363, ___handle_1)); }
	inline intptr_t get_handle_1() const { return ___handle_1; }
	inline intptr_t* get_address_of_handle_1() { return &___handle_1; }
	inline void set_handle_1(intptr_t value)
	{
		___handle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.InteropServices.HandleRef
struct HandleRef_t3745784363_marshaled_pinvoke
{
	Il2CppIUnknown* ___wrapper_0;
	intptr_t ___handle_1;
};
// Native definition for COM marshalling of System.Runtime.InteropServices.HandleRef
struct HandleRef_t3745784363_marshaled_com
{
	Il2CppIUnknown* ___wrapper_0;
	intptr_t ___handle_1;
};
#endif // HANDLEREF_T3745784363_H
#ifndef MESSAGEPRIORITY_T2166322227_H
#define MESSAGEPRIORITY_T2166322227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.MessagePriority
struct  MessagePriority_t2166322227 
{
public:
	// System.Int32 HoloToolkit.Sharing.MessagePriority::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MessagePriority_t2166322227, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEPRIORITY_T2166322227_H
#ifndef LAYOUTTYPE_T1707310462_H
#define LAYOUTTYPE_T1707310462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.UI.Keyboard.Keyboard/LayoutType
struct  LayoutType_t1707310462 
{
public:
	// System.Int32 HoloToolkit.UI.Keyboard.Keyboard/LayoutType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LayoutType_t1707310462, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTTYPE_T1707310462_H
#ifndef TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#define TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboardType
struct  TouchScreenKeyboardType_t1530597702 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_t1530597702, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef EAXIS_T17314133_H
#define EAXIS_T17314133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.UI.Keyboard.AxisSlider/EAxis
struct  EAxis_t17314133 
{
public:
	// System.Int32 HoloToolkit.UI.Keyboard.AxisSlider/EAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EAxis_t17314133, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAXIS_T17314133_H
#ifndef CHARACTERVALIDATION_T4051914437_H
#define CHARACTERVALIDATION_T4051914437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/CharacterValidation
struct  CharacterValidation_t4051914437 
{
public:
	// System.Int32 UnityEngine.UI.InputField/CharacterValidation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CharacterValidation_t4051914437, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERVALIDATION_T4051914437_H
#ifndef LINETYPE_T4214648469_H
#define LINETYPE_T4214648469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/LineType
struct  LineType_t4214648469 
{
public:
	// System.Int32 UnityEngine.UI.InputField/LineType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LineType_t4214648469, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETYPE_T4214648469_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef FUNCTION_T3824847731_H
#define FUNCTION_T3824847731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.UI.Keyboard.KeyboardKeyFunc/Function
struct  Function_t3824847731 
{
public:
	// System.Int32 HoloToolkit.UI.Keyboard.KeyboardKeyFunc/Function::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Function_t3824847731, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTION_T3824847731_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef CONTENTTYPE_T1787303396_H
#define CONTENTTYPE_T1787303396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/ContentType
struct  ContentType_t1787303396 
{
public:
	// System.Int32 UnityEngine.UI.InputField/ContentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ContentType_t1787303396, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T1787303396_H
#ifndef CLIENTROLE_T373356971_H
#define CLIENTROLE_T373356971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ClientRole
struct  ClientRole_t373356971 
{
public:
	// System.Int32 HoloToolkit.Sharing.ClientRole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClientRole_t373356971, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTROLE_T373356971_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef INPUTTYPE_T1770400679_H
#define INPUTTYPE_T1770400679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/InputType
struct  InputType_t1770400679 
{
public:
	// System.Int32 UnityEngine.UI.InputField/InputType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputType_t1770400679, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTYPE_T1770400679_H
#ifndef LOGMANAGER_T3740877303_H
#define LOGMANAGER_T3740877303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.LogManager
struct  LogManager_t3740877303  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.LogManager::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.LogManager::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(LogManager_t3740877303, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(LogManager_t3740877303, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGMANAGER_T3740877303_H
#ifndef NETWORKOUTMESSAGE_T1497576987_H
#define NETWORKOUTMESSAGE_T1497576987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.NetworkOutMessage
struct  NetworkOutMessage_t1497576987  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.NetworkOutMessage::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.NetworkOutMessage::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(NetworkOutMessage_t1497576987, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(NetworkOutMessage_t1497576987, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKOUTMESSAGE_T1497576987_H
#ifndef NETWORKINMESSAGE_T2396015845_H
#define NETWORKINMESSAGE_T2396015845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.NetworkInMessage
struct  NetworkInMessage_t2396015845  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.NetworkInMessage::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.NetworkInMessage::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(NetworkInMessage_t2396015845, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(NetworkInMessage_t2396015845, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKINMESSAGE_T2396015845_H
#ifndef PAIRMAKER_T2569523026_H
#define PAIRMAKER_T2569523026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairMaker
struct  PairMaker_t2569523026  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.PairMaker::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.PairMaker::swigCMemOwn
	bool ___swigCMemOwn_1;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_0 HoloToolkit.Sharing.PairMaker::swigDelegate0
	SwigDelegatePairMaker_0_t3088859200 * ___swigDelegate0_2;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_1 HoloToolkit.Sharing.PairMaker::swigDelegate1
	SwigDelegatePairMaker_1_t3088859199 * ___swigDelegate1_3;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_2 HoloToolkit.Sharing.PairMaker::swigDelegate2
	SwigDelegatePairMaker_2_t3088859202 * ___swigDelegate2_4;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_3 HoloToolkit.Sharing.PairMaker::swigDelegate3
	SwigDelegatePairMaker_3_t3088859201 * ___swigDelegate3_5;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_4 HoloToolkit.Sharing.PairMaker::swigDelegate4
	SwigDelegatePairMaker_4_t3088859196 * ___swigDelegate4_6;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_5 HoloToolkit.Sharing.PairMaker::swigDelegate5
	SwigDelegatePairMaker_5_t3088859195 * ___swigDelegate5_7;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_6 HoloToolkit.Sharing.PairMaker::swigDelegate6
	SwigDelegatePairMaker_6_t3088859198 * ___swigDelegate6_8;
	// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_7 HoloToolkit.Sharing.PairMaker::swigDelegate7
	SwigDelegatePairMaker_7_t3088859197 * ___swigDelegate7_9;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_2() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate0_2)); }
	inline SwigDelegatePairMaker_0_t3088859200 * get_swigDelegate0_2() const { return ___swigDelegate0_2; }
	inline SwigDelegatePairMaker_0_t3088859200 ** get_address_of_swigDelegate0_2() { return &___swigDelegate0_2; }
	inline void set_swigDelegate0_2(SwigDelegatePairMaker_0_t3088859200 * value)
	{
		___swigDelegate0_2 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_2), value);
	}

	inline static int32_t get_offset_of_swigDelegate1_3() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate1_3)); }
	inline SwigDelegatePairMaker_1_t3088859199 * get_swigDelegate1_3() const { return ___swigDelegate1_3; }
	inline SwigDelegatePairMaker_1_t3088859199 ** get_address_of_swigDelegate1_3() { return &___swigDelegate1_3; }
	inline void set_swigDelegate1_3(SwigDelegatePairMaker_1_t3088859199 * value)
	{
		___swigDelegate1_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate1_3), value);
	}

	inline static int32_t get_offset_of_swigDelegate2_4() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate2_4)); }
	inline SwigDelegatePairMaker_2_t3088859202 * get_swigDelegate2_4() const { return ___swigDelegate2_4; }
	inline SwigDelegatePairMaker_2_t3088859202 ** get_address_of_swigDelegate2_4() { return &___swigDelegate2_4; }
	inline void set_swigDelegate2_4(SwigDelegatePairMaker_2_t3088859202 * value)
	{
		___swigDelegate2_4 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate2_4), value);
	}

	inline static int32_t get_offset_of_swigDelegate3_5() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate3_5)); }
	inline SwigDelegatePairMaker_3_t3088859201 * get_swigDelegate3_5() const { return ___swigDelegate3_5; }
	inline SwigDelegatePairMaker_3_t3088859201 ** get_address_of_swigDelegate3_5() { return &___swigDelegate3_5; }
	inline void set_swigDelegate3_5(SwigDelegatePairMaker_3_t3088859201 * value)
	{
		___swigDelegate3_5 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate3_5), value);
	}

	inline static int32_t get_offset_of_swigDelegate4_6() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate4_6)); }
	inline SwigDelegatePairMaker_4_t3088859196 * get_swigDelegate4_6() const { return ___swigDelegate4_6; }
	inline SwigDelegatePairMaker_4_t3088859196 ** get_address_of_swigDelegate4_6() { return &___swigDelegate4_6; }
	inline void set_swigDelegate4_6(SwigDelegatePairMaker_4_t3088859196 * value)
	{
		___swigDelegate4_6 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate4_6), value);
	}

	inline static int32_t get_offset_of_swigDelegate5_7() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate5_7)); }
	inline SwigDelegatePairMaker_5_t3088859195 * get_swigDelegate5_7() const { return ___swigDelegate5_7; }
	inline SwigDelegatePairMaker_5_t3088859195 ** get_address_of_swigDelegate5_7() { return &___swigDelegate5_7; }
	inline void set_swigDelegate5_7(SwigDelegatePairMaker_5_t3088859195 * value)
	{
		___swigDelegate5_7 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate5_7), value);
	}

	inline static int32_t get_offset_of_swigDelegate6_8() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate6_8)); }
	inline SwigDelegatePairMaker_6_t3088859198 * get_swigDelegate6_8() const { return ___swigDelegate6_8; }
	inline SwigDelegatePairMaker_6_t3088859198 ** get_address_of_swigDelegate6_8() { return &___swigDelegate6_8; }
	inline void set_swigDelegate6_8(SwigDelegatePairMaker_6_t3088859198 * value)
	{
		___swigDelegate6_8 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate6_8), value);
	}

	inline static int32_t get_offset_of_swigDelegate7_9() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026, ___swigDelegate7_9)); }
	inline SwigDelegatePairMaker_7_t3088859197 * get_swigDelegate7_9() const { return ___swigDelegate7_9; }
	inline SwigDelegatePairMaker_7_t3088859197 ** get_address_of_swigDelegate7_9() { return &___swigDelegate7_9; }
	inline void set_swigDelegate7_9(SwigDelegatePairMaker_7_t3088859197 * value)
	{
		___swigDelegate7_9 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate7_9), value);
	}
};

struct PairMaker_t2569523026_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_10;
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes1
	TypeU5BU5D_t3940880105* ___swigMethodTypes1_11;
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes2
	TypeU5BU5D_t3940880105* ___swigMethodTypes2_12;
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes3
	TypeU5BU5D_t3940880105* ___swigMethodTypes3_13;
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes4
	TypeU5BU5D_t3940880105* ___swigMethodTypes4_14;
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes5
	TypeU5BU5D_t3940880105* ___swigMethodTypes5_15;
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes6
	TypeU5BU5D_t3940880105* ___swigMethodTypes6_16;
	// System.Type[] HoloToolkit.Sharing.PairMaker::swigMethodTypes7
	TypeU5BU5D_t3940880105* ___swigMethodTypes7_17;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_10() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes0_10)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_10() const { return ___swigMethodTypes0_10; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_10() { return &___swigMethodTypes0_10; }
	inline void set_swigMethodTypes0_10(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_10 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_10), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes1_11() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes1_11)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes1_11() const { return ___swigMethodTypes1_11; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes1_11() { return &___swigMethodTypes1_11; }
	inline void set_swigMethodTypes1_11(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes1_11 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes1_11), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes2_12() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes2_12)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes2_12() const { return ___swigMethodTypes2_12; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes2_12() { return &___swigMethodTypes2_12; }
	inline void set_swigMethodTypes2_12(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes2_12 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes2_12), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes3_13() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes3_13)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes3_13() const { return ___swigMethodTypes3_13; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes3_13() { return &___swigMethodTypes3_13; }
	inline void set_swigMethodTypes3_13(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes3_13 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes3_13), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes4_14() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes4_14)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes4_14() const { return ___swigMethodTypes4_14; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes4_14() { return &___swigMethodTypes4_14; }
	inline void set_swigMethodTypes4_14(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes4_14 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes4_14), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes5_15() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes5_15)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes5_15() const { return ___swigMethodTypes5_15; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes5_15() { return &___swigMethodTypes5_15; }
	inline void set_swigMethodTypes5_15(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes5_15 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes5_15), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes6_16() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes6_16)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes6_16() const { return ___swigMethodTypes6_16; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes6_16() { return &___swigMethodTypes6_16; }
	inline void set_swigMethodTypes6_16(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes6_16 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes6_16), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes7_17() { return static_cast<int32_t>(offsetof(PairMaker_t2569523026_StaticFields, ___swigMethodTypes7_17)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes7_17() const { return ___swigMethodTypes7_17; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes7_17() { return &___swigMethodTypes7_17; }
	inline void set_swigMethodTypes7_17(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes7_17 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes7_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIRMAKER_T2569523026_H
#ifndef NETWORKCONNECTION_T1099879691_H
#define NETWORKCONNECTION_T1099879691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.NetworkConnection
struct  NetworkConnection_t1099879691  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.NetworkConnection::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.NetworkConnection::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(NetworkConnection_t1099879691, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(NetworkConnection_t1099879691, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCONNECTION_T1099879691_H
#ifndef PAIRINGMANAGER_T3824021151_H
#define PAIRINGMANAGER_T3824021151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairingManager
struct  PairingManager_t3824021151  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.PairingManager::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.PairingManager::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(PairingManager_t3824021151, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(PairingManager_t3824021151, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIRINGMANAGER_T3824021151_H
#ifndef LOG_T986242981_H
#define LOG_T986242981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Log
struct  Log_t986242981  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.Log::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.Log::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(Log_t986242981, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(Log_t986242981, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOG_T986242981_H
#ifndef AUDIOMANAGER_T2087294725_H
#define AUDIOMANAGER_T2087294725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.AudioManager
struct  AudioManager_t2087294725  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.AudioManager::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.AudioManager::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(AudioManager_t2087294725, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(AudioManager_t2087294725, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMANAGER_T2087294725_H
#ifndef CLIENTCONFIG_T2300563953_H
#define CLIENTCONFIG_T2300563953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ClientConfig
struct  ClientConfig_t2300563953  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.ClientConfig::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.ClientConfig::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(ClientConfig_t2300563953, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(ClientConfig_t2300563953, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTCONFIG_T2300563953_H
#ifndef ANCHORDOWNLOADREQUEST_T1250893622_H
#define ANCHORDOWNLOADREQUEST_T1250893622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.AnchorDownloadRequest
struct  AnchorDownloadRequest_t1250893622  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.AnchorDownloadRequest::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.AnchorDownloadRequest::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(AnchorDownloadRequest_t1250893622, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(AnchorDownloadRequest_t1250893622, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORDOWNLOADREQUEST_T1250893622_H
#ifndef MULTICASTDELEGATE_T157516450_H
#define MULTICASTDELEGATE_T157516450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t157516450  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t157516450, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T157516450_H
#ifndef DISCOVERYCLIENT_T1530907974_H
#define DISCOVERYCLIENT_T1530907974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.DiscoveryClient
struct  DiscoveryClient_t1530907974  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.DiscoveryClient::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.DiscoveryClient::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(DiscoveryClient_t1530907974, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(DiscoveryClient_t1530907974, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCOVERYCLIENT_T1530907974_H
#ifndef LOGWRITER_T3550065378_H
#define LOGWRITER_T3550065378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.LogWriter
struct  LogWriter_t3550065378  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.LogWriter::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.LogWriter::swigCMemOwn
	bool ___swigCMemOwn_1;
	// HoloToolkit.Sharing.LogWriter/SwigDelegateLogWriter_0 HoloToolkit.Sharing.LogWriter::swigDelegate0
	SwigDelegateLogWriter_0_t50445701 * ___swigDelegate0_2;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(LogWriter_t3550065378, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(LogWriter_t3550065378, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_2() { return static_cast<int32_t>(offsetof(LogWriter_t3550065378, ___swigDelegate0_2)); }
	inline SwigDelegateLogWriter_0_t50445701 * get_swigDelegate0_2() const { return ___swigDelegate0_2; }
	inline SwigDelegateLogWriter_0_t50445701 ** get_address_of_swigDelegate0_2() { return &___swigDelegate0_2; }
	inline void set_swigDelegate0_2(SwigDelegateLogWriter_0_t50445701 * value)
	{
		___swigDelegate0_2 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_2), value);
	}
};

struct LogWriter_t3550065378_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.LogWriter::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_3;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_3() { return static_cast<int32_t>(offsetof(LogWriter_t3550065378_StaticFields, ___swigMethodTypes0_3)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_3() const { return ___swigMethodTypes0_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_3() { return &___swigMethodTypes0_3; }
	inline void set_swigMethodTypes0_3(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGWRITER_T3550065378_H
#ifndef ELEMENT_T654894041_H
#define ELEMENT_T654894041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Element
struct  Element_t654894041  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.Element::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.Element::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(Element_t654894041, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(Element_t654894041, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_T654894041_H
#ifndef DISCOVEREDSYSTEM_T1063958610_H
#define DISCOVEREDSYSTEM_T1063958610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.DiscoveredSystem
struct  DiscoveredSystem_t1063958610  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.DiscoveredSystem::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.DiscoveredSystem::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(DiscoveredSystem_t1063958610, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(DiscoveredSystem_t1063958610, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCOVEREDSYSTEM_T1063958610_H
#ifndef LISTENER_T2209039043_H
#define LISTENER_T2209039043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Listener
struct  Listener_t2209039043  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.Listener::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.Listener::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(Listener_t2209039043, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(Listener_t2209039043, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTENER_T2209039043_H
#ifndef PROFILE_T1856509255_H
#define PROFILE_T1856509255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Profile
struct  Profile_t1856509255  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.Profile::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.Profile::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(Profile_t1856509255, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(Profile_t1856509255, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILE_T1856509255_H
#ifndef PROFILEMANAGER_T129985400_H
#define PROFILEMANAGER_T129985400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ProfileManager
struct  ProfileManager_t129985400  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.ProfileManager::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.ProfileManager::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(ProfileManager_t129985400, ___swigCPtr_0)); }
	inline HandleRef_t3745784363  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784363  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(ProfileManager_t129985400, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEMANAGER_T129985400_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef SWIGDELEGATEOBJECTELEMENTLISTENER_6_T271806939_H
#define SWIGDELEGATEOBJECTELEMENTLISTENER_6_T271806939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_6
struct  SwigDelegateObjectElementListener_6_t271806939  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEOBJECTELEMENTLISTENER_6_T271806939_H
#ifndef SWIGDELEGATEPAIRINGLISTENER_1_T4183180788_H
#define SWIGDELEGATEPAIRINGLISTENER_1_T4183180788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairingListener/SwigDelegatePairingListener_1
struct  SwigDelegatePairingListener_1_t4183180788  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEPAIRINGLISTENER_1_T4183180788_H
#ifndef SWIGDELEGATEPAIRINGLISTENER_0_T4183180789_H
#define SWIGDELEGATEPAIRINGLISTENER_0_T4183180789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairingListener/SwigDelegatePairingListener_0
struct  SwigDelegatePairingListener_0_t4183180789  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEPAIRINGLISTENER_0_T4183180789_H
#ifndef PAIRINGLISTENER_T2464634798_H
#define PAIRINGLISTENER_T2464634798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairingListener
struct  PairingListener_t2464634798  : public Listener_t2209039043
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.PairingListener::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;
	// HoloToolkit.Sharing.PairingListener/SwigDelegatePairingListener_0 HoloToolkit.Sharing.PairingListener::swigDelegate0
	SwigDelegatePairingListener_0_t4183180789 * ___swigDelegate0_3;
	// HoloToolkit.Sharing.PairingListener/SwigDelegatePairingListener_1 HoloToolkit.Sharing.PairingListener::swigDelegate1
	SwigDelegatePairingListener_1_t4183180788 * ___swigDelegate1_4;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(PairingListener_t2464634798, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_3() { return static_cast<int32_t>(offsetof(PairingListener_t2464634798, ___swigDelegate0_3)); }
	inline SwigDelegatePairingListener_0_t4183180789 * get_swigDelegate0_3() const { return ___swigDelegate0_3; }
	inline SwigDelegatePairingListener_0_t4183180789 ** get_address_of_swigDelegate0_3() { return &___swigDelegate0_3; }
	inline void set_swigDelegate0_3(SwigDelegatePairingListener_0_t4183180789 * value)
	{
		___swigDelegate0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_3), value);
	}

	inline static int32_t get_offset_of_swigDelegate1_4() { return static_cast<int32_t>(offsetof(PairingListener_t2464634798, ___swigDelegate1_4)); }
	inline SwigDelegatePairingListener_1_t4183180788 * get_swigDelegate1_4() const { return ___swigDelegate1_4; }
	inline SwigDelegatePairingListener_1_t4183180788 ** get_address_of_swigDelegate1_4() { return &___swigDelegate1_4; }
	inline void set_swigDelegate1_4(SwigDelegatePairingListener_1_t4183180788 * value)
	{
		___swigDelegate1_4 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate1_4), value);
	}
};

struct PairingListener_t2464634798_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.PairingListener::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_5;
	// System.Type[] HoloToolkit.Sharing.PairingListener::swigMethodTypes1
	TypeU5BU5D_t3940880105* ___swigMethodTypes1_6;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_5() { return static_cast<int32_t>(offsetof(PairingListener_t2464634798_StaticFields, ___swigMethodTypes0_5)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_5() const { return ___swigMethodTypes0_5; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_5() { return &___swigMethodTypes0_5; }
	inline void set_swigMethodTypes0_5(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_5 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_5), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes1_6() { return static_cast<int32_t>(offsetof(PairingListener_t2464634798_StaticFields, ___swigMethodTypes1_6)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes1_6() const { return ___swigMethodTypes1_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes1_6() { return &___swigMethodTypes1_6; }
	inline void set_swigMethodTypes1_6(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes1_6 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIRINGLISTENER_T2464634798_H
#ifndef SWIGDELEGATEOBJECTELEMENTLISTENER_7_T271806940_H
#define SWIGDELEGATEOBJECTELEMENTLISTENER_7_T271806940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_7
struct  SwigDelegateObjectElementListener_7_t271806940  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEOBJECTELEMENTLISTENER_7_T271806940_H
#ifndef SWIGDELEGATEPAIRMAKER_6_T3088859198_H
#define SWIGDELEGATEPAIRMAKER_6_T3088859198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_6
struct  SwigDelegatePairMaker_6_t3088859198  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEPAIRMAKER_6_T3088859198_H
#ifndef SWIGDELEGATEPAIRMAKER_5_T3088859195_H
#define SWIGDELEGATEPAIRMAKER_5_T3088859195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_5
struct  SwigDelegatePairMaker_5_t3088859195  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEPAIRMAKER_5_T3088859195_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef SWIGDELEGATEPAIRMAKER_7_T3088859197_H
#define SWIGDELEGATEPAIRMAKER_7_T3088859197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_7
struct  SwigDelegatePairMaker_7_t3088859197  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEPAIRMAKER_7_T3088859197_H
#ifndef SWIGDELEGATEPAIRMAKER_4_T3088859196_H
#define SWIGDELEGATEPAIRMAKER_4_T3088859196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_4
struct  SwigDelegatePairMaker_4_t3088859196  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEPAIRMAKER_4_T3088859196_H
#ifndef SWIGDELEGATEPAIRMAKER_1_T3088859199_H
#define SWIGDELEGATEPAIRMAKER_1_T3088859199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_1
struct  SwigDelegatePairMaker_1_t3088859199  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEPAIRMAKER_1_T3088859199_H
#ifndef SWIGDELEGATEPAIRMAKER_0_T3088859200_H
#define SWIGDELEGATEPAIRMAKER_0_T3088859200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_0
struct  SwigDelegatePairMaker_0_t3088859200  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEPAIRMAKER_0_T3088859200_H
#ifndef SWIGDELEGATEPAIRMAKER_3_T3088859201_H
#define SWIGDELEGATEPAIRMAKER_3_T3088859201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_3
struct  SwigDelegatePairMaker_3_t3088859201  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEPAIRMAKER_3_T3088859201_H
#ifndef SWIGDELEGATEPAIRMAKER_2_T3088859202_H
#define SWIGDELEGATEPAIRMAKER_2_T3088859202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairMaker/SwigDelegatePairMaker_2
struct  SwigDelegatePairMaker_2_t3088859202  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEPAIRMAKER_2_T3088859202_H
#ifndef SWIGDELEGATEOBJECTELEMENTLISTENER_5_T271806942_H
#define SWIGDELEGATEOBJECTELEMENTLISTENER_5_T271806942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_5
struct  SwigDelegateObjectElementListener_5_t271806942  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEOBJECTELEMENTLISTENER_5_T271806942_H
#ifndef SWIGDELEGATEFLOATARRAYLISTENER_2_T1137473742_H
#define SWIGDELEGATEFLOATARRAYLISTENER_2_T1137473742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.FloatArrayListener/SwigDelegateFloatArrayListener_2
struct  SwigDelegateFloatArrayListener_2_t1137473742  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEFLOATARRAYLISTENER_2_T1137473742_H
#ifndef FLOATELEMENT_T4176382651_H
#define FLOATELEMENT_T4176382651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.FloatElement
struct  FloatElement_t4176382651  : public Element_t654894041
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.FloatElement::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(FloatElement_t4176382651, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATELEMENT_T4176382651_H
#ifndef SWIGDELEGATEFLOATARRAYLISTENER_0_T2300273156_H
#define SWIGDELEGATEFLOATARRAYLISTENER_0_T2300273156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.FloatArrayListener/SwigDelegateFloatArrayListener_0
struct  SwigDelegateFloatArrayListener_0_t2300273156  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEFLOATARRAYLISTENER_0_T2300273156_H
#ifndef SWIGDELEGATEFLOATARRAYLISTENER_1_T734189215_H
#define SWIGDELEGATEFLOATARRAYLISTENER_1_T734189215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.FloatArrayListener/SwigDelegateFloatArrayListener_1
struct  SwigDelegateFloatArrayListener_1_t734189215  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEFLOATARRAYLISTENER_1_T734189215_H
#ifndef INTARRAYELEMENT_T3225799893_H
#define INTARRAYELEMENT_T3225799893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.IntArrayElement
struct  IntArrayElement_t3225799893  : public Element_t654894041
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.IntArrayElement::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(IntArrayElement_t3225799893, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTARRAYELEMENT_T3225799893_H
#ifndef SWIGDELEGATEINTARRAYLISTENER_1_T1743901878_H
#define SWIGDELEGATEINTARRAYLISTENER_1_T1743901878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.IntArrayListener/SwigDelegateIntArrayListener_1
struct  SwigDelegateIntArrayListener_1_t1743901878  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEINTARRAYLISTENER_1_T1743901878_H
#ifndef SWIGDELEGATEINTARRAYLISTENER_2_T177817937_H
#define SWIGDELEGATEINTARRAYLISTENER_2_T177817937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.IntArrayListener/SwigDelegateIntArrayListener_2
struct  SwigDelegateIntArrayListener_2_t177817937  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEINTARRAYLISTENER_2_T177817937_H
#ifndef INTARRAYLISTENER_T4199203735_H
#define INTARRAYLISTENER_T4199203735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.IntArrayListener
struct  IntArrayListener_t4199203735  : public Listener_t2209039043
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.IntArrayListener::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;
	// HoloToolkit.Sharing.IntArrayListener/SwigDelegateIntArrayListener_0 HoloToolkit.Sharing.IntArrayListener::swigDelegate0
	SwigDelegateIntArrayListener_0_t3309985819 * ___swigDelegate0_3;
	// HoloToolkit.Sharing.IntArrayListener/SwigDelegateIntArrayListener_1 HoloToolkit.Sharing.IntArrayListener::swigDelegate1
	SwigDelegateIntArrayListener_1_t1743901878 * ___swigDelegate1_4;
	// HoloToolkit.Sharing.IntArrayListener/SwigDelegateIntArrayListener_2 HoloToolkit.Sharing.IntArrayListener::swigDelegate2
	SwigDelegateIntArrayListener_2_t177817937 * ___swigDelegate2_5;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(IntArrayListener_t4199203735, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_3() { return static_cast<int32_t>(offsetof(IntArrayListener_t4199203735, ___swigDelegate0_3)); }
	inline SwigDelegateIntArrayListener_0_t3309985819 * get_swigDelegate0_3() const { return ___swigDelegate0_3; }
	inline SwigDelegateIntArrayListener_0_t3309985819 ** get_address_of_swigDelegate0_3() { return &___swigDelegate0_3; }
	inline void set_swigDelegate0_3(SwigDelegateIntArrayListener_0_t3309985819 * value)
	{
		___swigDelegate0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_3), value);
	}

	inline static int32_t get_offset_of_swigDelegate1_4() { return static_cast<int32_t>(offsetof(IntArrayListener_t4199203735, ___swigDelegate1_4)); }
	inline SwigDelegateIntArrayListener_1_t1743901878 * get_swigDelegate1_4() const { return ___swigDelegate1_4; }
	inline SwigDelegateIntArrayListener_1_t1743901878 ** get_address_of_swigDelegate1_4() { return &___swigDelegate1_4; }
	inline void set_swigDelegate1_4(SwigDelegateIntArrayListener_1_t1743901878 * value)
	{
		___swigDelegate1_4 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate1_4), value);
	}

	inline static int32_t get_offset_of_swigDelegate2_5() { return static_cast<int32_t>(offsetof(IntArrayListener_t4199203735, ___swigDelegate2_5)); }
	inline SwigDelegateIntArrayListener_2_t177817937 * get_swigDelegate2_5() const { return ___swigDelegate2_5; }
	inline SwigDelegateIntArrayListener_2_t177817937 ** get_address_of_swigDelegate2_5() { return &___swigDelegate2_5; }
	inline void set_swigDelegate2_5(SwigDelegateIntArrayListener_2_t177817937 * value)
	{
		___swigDelegate2_5 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate2_5), value);
	}
};

struct IntArrayListener_t4199203735_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.IntArrayListener::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_6;
	// System.Type[] HoloToolkit.Sharing.IntArrayListener::swigMethodTypes1
	TypeU5BU5D_t3940880105* ___swigMethodTypes1_7;
	// System.Type[] HoloToolkit.Sharing.IntArrayListener::swigMethodTypes2
	TypeU5BU5D_t3940880105* ___swigMethodTypes2_8;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_6() { return static_cast<int32_t>(offsetof(IntArrayListener_t4199203735_StaticFields, ___swigMethodTypes0_6)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_6() const { return ___swigMethodTypes0_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_6() { return &___swigMethodTypes0_6; }
	inline void set_swigMethodTypes0_6(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_6 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_6), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes1_7() { return static_cast<int32_t>(offsetof(IntArrayListener_t4199203735_StaticFields, ___swigMethodTypes1_7)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes1_7() const { return ___swigMethodTypes1_7; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes1_7() { return &___swigMethodTypes1_7; }
	inline void set_swigMethodTypes1_7(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes1_7 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes1_7), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes2_8() { return static_cast<int32_t>(offsetof(IntArrayListener_t4199203735_StaticFields, ___swigMethodTypes2_8)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes2_8() const { return ___swigMethodTypes2_8; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes2_8() { return &___swigMethodTypes2_8; }
	inline void set_swigMethodTypes2_8(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes2_8 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes2_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTARRAYLISTENER_T4199203735_H
#ifndef SWIGDELEGATEINTARRAYLISTENER_0_T3309985819_H
#define SWIGDELEGATEINTARRAYLISTENER_0_T3309985819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.IntArrayListener/SwigDelegateIntArrayListener_0
struct  SwigDelegateIntArrayListener_0_t3309985819  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEINTARRAYLISTENER_0_T3309985819_H
#ifndef DIRECTPAIRRECEIVER_T2582757322_H
#define DIRECTPAIRRECEIVER_T2582757322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.DirectPairReceiver
struct  DirectPairReceiver_t2582757322  : public PairMaker_t2569523026
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.DirectPairReceiver::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_18;

public:
	inline static int32_t get_offset_of_swigCPtr_18() { return static_cast<int32_t>(offsetof(DirectPairReceiver_t2582757322, ___swigCPtr_18)); }
	inline HandleRef_t3745784363  get_swigCPtr_18() const { return ___swigCPtr_18; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_18() { return &___swigCPtr_18; }
	inline void set_swigCPtr_18(HandleRef_t3745784363  value)
	{
		___swigCPtr_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTPAIRRECEIVER_T2582757322_H
#ifndef DISCOVERYCLIENTLISTENER_T1752642716_H
#define DISCOVERYCLIENTLISTENER_T1752642716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.DiscoveryClientListener
struct  DiscoveryClientListener_t1752642716  : public Listener_t2209039043
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.DiscoveryClientListener::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;
	// HoloToolkit.Sharing.DiscoveryClientListener/SwigDelegateDiscoveryClientListener_0 HoloToolkit.Sharing.DiscoveryClientListener::swigDelegate0
	SwigDelegateDiscoveryClientListener_0_t3614305483 * ___swigDelegate0_3;
	// HoloToolkit.Sharing.DiscoveryClientListener/SwigDelegateDiscoveryClientListener_1 HoloToolkit.Sharing.DiscoveryClientListener::swigDelegate1
	SwigDelegateDiscoveryClientListener_1_t3614305484 * ___swigDelegate1_4;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(DiscoveryClientListener_t1752642716, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_3() { return static_cast<int32_t>(offsetof(DiscoveryClientListener_t1752642716, ___swigDelegate0_3)); }
	inline SwigDelegateDiscoveryClientListener_0_t3614305483 * get_swigDelegate0_3() const { return ___swigDelegate0_3; }
	inline SwigDelegateDiscoveryClientListener_0_t3614305483 ** get_address_of_swigDelegate0_3() { return &___swigDelegate0_3; }
	inline void set_swigDelegate0_3(SwigDelegateDiscoveryClientListener_0_t3614305483 * value)
	{
		___swigDelegate0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_3), value);
	}

	inline static int32_t get_offset_of_swigDelegate1_4() { return static_cast<int32_t>(offsetof(DiscoveryClientListener_t1752642716, ___swigDelegate1_4)); }
	inline SwigDelegateDiscoveryClientListener_1_t3614305484 * get_swigDelegate1_4() const { return ___swigDelegate1_4; }
	inline SwigDelegateDiscoveryClientListener_1_t3614305484 ** get_address_of_swigDelegate1_4() { return &___swigDelegate1_4; }
	inline void set_swigDelegate1_4(SwigDelegateDiscoveryClientListener_1_t3614305484 * value)
	{
		___swigDelegate1_4 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate1_4), value);
	}
};

struct DiscoveryClientListener_t1752642716_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.DiscoveryClientListener::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_5;
	// System.Type[] HoloToolkit.Sharing.DiscoveryClientListener::swigMethodTypes1
	TypeU5BU5D_t3940880105* ___swigMethodTypes1_6;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_5() { return static_cast<int32_t>(offsetof(DiscoveryClientListener_t1752642716_StaticFields, ___swigMethodTypes0_5)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_5() const { return ___swigMethodTypes0_5; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_5() { return &___swigMethodTypes0_5; }
	inline void set_swigMethodTypes0_5(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_5 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_5), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes1_6() { return static_cast<int32_t>(offsetof(DiscoveryClientListener_t1752642716_StaticFields, ___swigMethodTypes1_6)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes1_6() const { return ___swigMethodTypes1_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes1_6() { return &___swigMethodTypes1_6; }
	inline void set_swigMethodTypes1_6(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes1_6 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCOVERYCLIENTLISTENER_T1752642716_H
#ifndef BOOLELEMENT_T553673861_H
#define BOOLELEMENT_T553673861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.BoolElement
struct  BoolElement_t553673861  : public Element_t654894041
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.BoolElement::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(BoolElement_t553673861, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLELEMENT_T553673861_H
#ifndef DIRECTPAIRCONNECTOR_T2072515004_H
#define DIRECTPAIRCONNECTOR_T2072515004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.DirectPairConnector
struct  DirectPairConnector_t2072515004  : public PairMaker_t2569523026
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.DirectPairConnector::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_18;

public:
	inline static int32_t get_offset_of_swigCPtr_18() { return static_cast<int32_t>(offsetof(DirectPairConnector_t2072515004, ___swigCPtr_18)); }
	inline HandleRef_t3745784363  get_swigCPtr_18() const { return ___swigCPtr_18; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_18() { return &___swigCPtr_18; }
	inline void set_swigCPtr_18(HandleRef_t3745784363  value)
	{
		___swigCPtr_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTPAIRCONNECTOR_T2072515004_H
#ifndef SWIGDELEGATEDISCOVERYCLIENTLISTENER_0_T3614305483_H
#define SWIGDELEGATEDISCOVERYCLIENTLISTENER_0_T3614305483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.DiscoveryClientListener/SwigDelegateDiscoveryClientListener_0
struct  SwigDelegateDiscoveryClientListener_0_t3614305483  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEDISCOVERYCLIENTLISTENER_0_T3614305483_H
#ifndef FLOATARRAYELEMENT_T2118589074_H
#define FLOATARRAYELEMENT_T2118589074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.FloatArrayElement
struct  FloatArrayElement_t2118589074  : public Element_t654894041
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.FloatArrayElement::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(FloatArrayElement_t2118589074, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATARRAYELEMENT_T2118589074_H
#ifndef FLOATARRAYLISTENER_T612841106_H
#define FLOATARRAYLISTENER_T612841106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.FloatArrayListener
struct  FloatArrayListener_t612841106  : public Listener_t2209039043
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.FloatArrayListener::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;
	// HoloToolkit.Sharing.FloatArrayListener/SwigDelegateFloatArrayListener_0 HoloToolkit.Sharing.FloatArrayListener::swigDelegate0
	SwigDelegateFloatArrayListener_0_t2300273156 * ___swigDelegate0_3;
	// HoloToolkit.Sharing.FloatArrayListener/SwigDelegateFloatArrayListener_1 HoloToolkit.Sharing.FloatArrayListener::swigDelegate1
	SwigDelegateFloatArrayListener_1_t734189215 * ___swigDelegate1_4;
	// HoloToolkit.Sharing.FloatArrayListener/SwigDelegateFloatArrayListener_2 HoloToolkit.Sharing.FloatArrayListener::swigDelegate2
	SwigDelegateFloatArrayListener_2_t1137473742 * ___swigDelegate2_5;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(FloatArrayListener_t612841106, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_3() { return static_cast<int32_t>(offsetof(FloatArrayListener_t612841106, ___swigDelegate0_3)); }
	inline SwigDelegateFloatArrayListener_0_t2300273156 * get_swigDelegate0_3() const { return ___swigDelegate0_3; }
	inline SwigDelegateFloatArrayListener_0_t2300273156 ** get_address_of_swigDelegate0_3() { return &___swigDelegate0_3; }
	inline void set_swigDelegate0_3(SwigDelegateFloatArrayListener_0_t2300273156 * value)
	{
		___swigDelegate0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_3), value);
	}

	inline static int32_t get_offset_of_swigDelegate1_4() { return static_cast<int32_t>(offsetof(FloatArrayListener_t612841106, ___swigDelegate1_4)); }
	inline SwigDelegateFloatArrayListener_1_t734189215 * get_swigDelegate1_4() const { return ___swigDelegate1_4; }
	inline SwigDelegateFloatArrayListener_1_t734189215 ** get_address_of_swigDelegate1_4() { return &___swigDelegate1_4; }
	inline void set_swigDelegate1_4(SwigDelegateFloatArrayListener_1_t734189215 * value)
	{
		___swigDelegate1_4 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate1_4), value);
	}

	inline static int32_t get_offset_of_swigDelegate2_5() { return static_cast<int32_t>(offsetof(FloatArrayListener_t612841106, ___swigDelegate2_5)); }
	inline SwigDelegateFloatArrayListener_2_t1137473742 * get_swigDelegate2_5() const { return ___swigDelegate2_5; }
	inline SwigDelegateFloatArrayListener_2_t1137473742 ** get_address_of_swigDelegate2_5() { return &___swigDelegate2_5; }
	inline void set_swigDelegate2_5(SwigDelegateFloatArrayListener_2_t1137473742 * value)
	{
		___swigDelegate2_5 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate2_5), value);
	}
};

struct FloatArrayListener_t612841106_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.FloatArrayListener::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_6;
	// System.Type[] HoloToolkit.Sharing.FloatArrayListener::swigMethodTypes1
	TypeU5BU5D_t3940880105* ___swigMethodTypes1_7;
	// System.Type[] HoloToolkit.Sharing.FloatArrayListener::swigMethodTypes2
	TypeU5BU5D_t3940880105* ___swigMethodTypes2_8;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_6() { return static_cast<int32_t>(offsetof(FloatArrayListener_t612841106_StaticFields, ___swigMethodTypes0_6)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_6() const { return ___swigMethodTypes0_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_6() { return &___swigMethodTypes0_6; }
	inline void set_swigMethodTypes0_6(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_6 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_6), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes1_7() { return static_cast<int32_t>(offsetof(FloatArrayListener_t612841106_StaticFields, ___swigMethodTypes1_7)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes1_7() const { return ___swigMethodTypes1_7; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes1_7() { return &___swigMethodTypes1_7; }
	inline void set_swigMethodTypes1_7(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes1_7 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes1_7), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes2_8() { return static_cast<int32_t>(offsetof(FloatArrayListener_t612841106_StaticFields, ___swigMethodTypes2_8)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes2_8() const { return ___swigMethodTypes2_8; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes2_8() { return &___swigMethodTypes2_8; }
	inline void set_swigMethodTypes2_8(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes2_8 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes2_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATARRAYLISTENER_T612841106_H
#ifndef SWIGDELEGATEDISCOVERYCLIENTLISTENER_1_T3614305484_H
#define SWIGDELEGATEDISCOVERYCLIENTLISTENER_1_T3614305484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.DiscoveryClientListener/SwigDelegateDiscoveryClientListener_1
struct  SwigDelegateDiscoveryClientListener_1_t3614305484  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEDISCOVERYCLIENTLISTENER_1_T3614305484_H
#ifndef DOUBLEELEMENT_T3393924332_H
#define DOUBLEELEMENT_T3393924332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.DoubleElement
struct  DoubleElement_t3393924332  : public Element_t654894041
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.DoubleElement::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(DoubleElement_t3393924332, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEELEMENT_T3393924332_H
#ifndef OBJECTELEMENT_T3710848331_H
#define OBJECTELEMENT_T3710848331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ObjectElement
struct  ObjectElement_t3710848331  : public Element_t654894041
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.ObjectElement::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(ObjectElement_t3710848331, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTELEMENT_T3710848331_H
#ifndef OBJECTELEMENTLISTENER_T389328343_H
#define OBJECTELEMENTLISTENER_T389328343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ObjectElementListener
struct  ObjectElementListener_t389328343  : public Listener_t2209039043
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.ObjectElementListener::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;
	// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_0 HoloToolkit.Sharing.ObjectElementListener::swigDelegate0
	SwigDelegateObjectElementListener_0_t271806937 * ___swigDelegate0_3;
	// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_1 HoloToolkit.Sharing.ObjectElementListener::swigDelegate1
	SwigDelegateObjectElementListener_1_t271806938 * ___swigDelegate1_4;
	// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_2 HoloToolkit.Sharing.ObjectElementListener::swigDelegate2
	SwigDelegateObjectElementListener_2_t271806935 * ___swigDelegate2_5;
	// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_3 HoloToolkit.Sharing.ObjectElementListener::swigDelegate3
	SwigDelegateObjectElementListener_3_t271806936 * ___swigDelegate3_6;
	// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_4 HoloToolkit.Sharing.ObjectElementListener::swigDelegate4
	SwigDelegateObjectElementListener_4_t271806941 * ___swigDelegate4_7;
	// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_5 HoloToolkit.Sharing.ObjectElementListener::swigDelegate5
	SwigDelegateObjectElementListener_5_t271806942 * ___swigDelegate5_8;
	// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_6 HoloToolkit.Sharing.ObjectElementListener::swigDelegate6
	SwigDelegateObjectElementListener_6_t271806939 * ___swigDelegate6_9;
	// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_7 HoloToolkit.Sharing.ObjectElementListener::swigDelegate7
	SwigDelegateObjectElementListener_7_t271806940 * ___swigDelegate7_10;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_3() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343, ___swigDelegate0_3)); }
	inline SwigDelegateObjectElementListener_0_t271806937 * get_swigDelegate0_3() const { return ___swigDelegate0_3; }
	inline SwigDelegateObjectElementListener_0_t271806937 ** get_address_of_swigDelegate0_3() { return &___swigDelegate0_3; }
	inline void set_swigDelegate0_3(SwigDelegateObjectElementListener_0_t271806937 * value)
	{
		___swigDelegate0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_3), value);
	}

	inline static int32_t get_offset_of_swigDelegate1_4() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343, ___swigDelegate1_4)); }
	inline SwigDelegateObjectElementListener_1_t271806938 * get_swigDelegate1_4() const { return ___swigDelegate1_4; }
	inline SwigDelegateObjectElementListener_1_t271806938 ** get_address_of_swigDelegate1_4() { return &___swigDelegate1_4; }
	inline void set_swigDelegate1_4(SwigDelegateObjectElementListener_1_t271806938 * value)
	{
		___swigDelegate1_4 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate1_4), value);
	}

	inline static int32_t get_offset_of_swigDelegate2_5() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343, ___swigDelegate2_5)); }
	inline SwigDelegateObjectElementListener_2_t271806935 * get_swigDelegate2_5() const { return ___swigDelegate2_5; }
	inline SwigDelegateObjectElementListener_2_t271806935 ** get_address_of_swigDelegate2_5() { return &___swigDelegate2_5; }
	inline void set_swigDelegate2_5(SwigDelegateObjectElementListener_2_t271806935 * value)
	{
		___swigDelegate2_5 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate2_5), value);
	}

	inline static int32_t get_offset_of_swigDelegate3_6() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343, ___swigDelegate3_6)); }
	inline SwigDelegateObjectElementListener_3_t271806936 * get_swigDelegate3_6() const { return ___swigDelegate3_6; }
	inline SwigDelegateObjectElementListener_3_t271806936 ** get_address_of_swigDelegate3_6() { return &___swigDelegate3_6; }
	inline void set_swigDelegate3_6(SwigDelegateObjectElementListener_3_t271806936 * value)
	{
		___swigDelegate3_6 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate3_6), value);
	}

	inline static int32_t get_offset_of_swigDelegate4_7() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343, ___swigDelegate4_7)); }
	inline SwigDelegateObjectElementListener_4_t271806941 * get_swigDelegate4_7() const { return ___swigDelegate4_7; }
	inline SwigDelegateObjectElementListener_4_t271806941 ** get_address_of_swigDelegate4_7() { return &___swigDelegate4_7; }
	inline void set_swigDelegate4_7(SwigDelegateObjectElementListener_4_t271806941 * value)
	{
		___swigDelegate4_7 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate4_7), value);
	}

	inline static int32_t get_offset_of_swigDelegate5_8() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343, ___swigDelegate5_8)); }
	inline SwigDelegateObjectElementListener_5_t271806942 * get_swigDelegate5_8() const { return ___swigDelegate5_8; }
	inline SwigDelegateObjectElementListener_5_t271806942 ** get_address_of_swigDelegate5_8() { return &___swigDelegate5_8; }
	inline void set_swigDelegate5_8(SwigDelegateObjectElementListener_5_t271806942 * value)
	{
		___swigDelegate5_8 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate5_8), value);
	}

	inline static int32_t get_offset_of_swigDelegate6_9() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343, ___swigDelegate6_9)); }
	inline SwigDelegateObjectElementListener_6_t271806939 * get_swigDelegate6_9() const { return ___swigDelegate6_9; }
	inline SwigDelegateObjectElementListener_6_t271806939 ** get_address_of_swigDelegate6_9() { return &___swigDelegate6_9; }
	inline void set_swigDelegate6_9(SwigDelegateObjectElementListener_6_t271806939 * value)
	{
		___swigDelegate6_9 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate6_9), value);
	}

	inline static int32_t get_offset_of_swigDelegate7_10() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343, ___swigDelegate7_10)); }
	inline SwigDelegateObjectElementListener_7_t271806940 * get_swigDelegate7_10() const { return ___swigDelegate7_10; }
	inline SwigDelegateObjectElementListener_7_t271806940 ** get_address_of_swigDelegate7_10() { return &___swigDelegate7_10; }
	inline void set_swigDelegate7_10(SwigDelegateObjectElementListener_7_t271806940 * value)
	{
		___swigDelegate7_10 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate7_10), value);
	}
};

struct ObjectElementListener_t389328343_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.ObjectElementListener::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_11;
	// System.Type[] HoloToolkit.Sharing.ObjectElementListener::swigMethodTypes1
	TypeU5BU5D_t3940880105* ___swigMethodTypes1_12;
	// System.Type[] HoloToolkit.Sharing.ObjectElementListener::swigMethodTypes2
	TypeU5BU5D_t3940880105* ___swigMethodTypes2_13;
	// System.Type[] HoloToolkit.Sharing.ObjectElementListener::swigMethodTypes3
	TypeU5BU5D_t3940880105* ___swigMethodTypes3_14;
	// System.Type[] HoloToolkit.Sharing.ObjectElementListener::swigMethodTypes4
	TypeU5BU5D_t3940880105* ___swigMethodTypes4_15;
	// System.Type[] HoloToolkit.Sharing.ObjectElementListener::swigMethodTypes5
	TypeU5BU5D_t3940880105* ___swigMethodTypes5_16;
	// System.Type[] HoloToolkit.Sharing.ObjectElementListener::swigMethodTypes6
	TypeU5BU5D_t3940880105* ___swigMethodTypes6_17;
	// System.Type[] HoloToolkit.Sharing.ObjectElementListener::swigMethodTypes7
	TypeU5BU5D_t3940880105* ___swigMethodTypes7_18;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_11() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343_StaticFields, ___swigMethodTypes0_11)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_11() const { return ___swigMethodTypes0_11; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_11() { return &___swigMethodTypes0_11; }
	inline void set_swigMethodTypes0_11(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_11 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_11), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes1_12() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343_StaticFields, ___swigMethodTypes1_12)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes1_12() const { return ___swigMethodTypes1_12; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes1_12() { return &___swigMethodTypes1_12; }
	inline void set_swigMethodTypes1_12(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes1_12 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes1_12), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes2_13() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343_StaticFields, ___swigMethodTypes2_13)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes2_13() const { return ___swigMethodTypes2_13; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes2_13() { return &___swigMethodTypes2_13; }
	inline void set_swigMethodTypes2_13(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes2_13 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes2_13), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes3_14() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343_StaticFields, ___swigMethodTypes3_14)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes3_14() const { return ___swigMethodTypes3_14; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes3_14() { return &___swigMethodTypes3_14; }
	inline void set_swigMethodTypes3_14(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes3_14 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes3_14), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes4_15() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343_StaticFields, ___swigMethodTypes4_15)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes4_15() const { return ___swigMethodTypes4_15; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes4_15() { return &___swigMethodTypes4_15; }
	inline void set_swigMethodTypes4_15(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes4_15 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes4_15), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes5_16() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343_StaticFields, ___swigMethodTypes5_16)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes5_16() const { return ___swigMethodTypes5_16; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes5_16() { return &___swigMethodTypes5_16; }
	inline void set_swigMethodTypes5_16(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes5_16 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes5_16), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes6_17() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343_StaticFields, ___swigMethodTypes6_17)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes6_17() const { return ___swigMethodTypes6_17; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes6_17() { return &___swigMethodTypes6_17; }
	inline void set_swigMethodTypes6_17(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes6_17 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes6_17), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes7_18() { return static_cast<int32_t>(offsetof(ObjectElementListener_t389328343_StaticFields, ___swigMethodTypes7_18)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes7_18() const { return ___swigMethodTypes7_18; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes7_18() { return &___swigMethodTypes7_18; }
	inline void set_swigMethodTypes7_18(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes7_18 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes7_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTELEMENTLISTENER_T389328343_H
#ifndef SWIGDELEGATENETWORKCONNECTIONLISTENER_2_T4021727020_H
#define SWIGDELEGATENETWORKCONNECTIONLISTENER_2_T4021727020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.NetworkConnectionListener/SwigDelegateNetworkConnectionListener_2
struct  SwigDelegateNetworkConnectionListener_2_t4021727020  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATENETWORKCONNECTIONLISTENER_2_T4021727020_H
#ifndef SWIGDELEGATENETWORKCONNECTIONLISTENER_3_T4021727019_H
#define SWIGDELEGATENETWORKCONNECTIONLISTENER_3_T4021727019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.NetworkConnectionListener/SwigDelegateNetworkConnectionListener_3
struct  SwigDelegateNetworkConnectionListener_3_t4021727019  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATENETWORKCONNECTIONLISTENER_3_T4021727019_H
#ifndef SWIGDELEGATEOBJECTELEMENTLISTENER_0_T271806937_H
#define SWIGDELEGATEOBJECTELEMENTLISTENER_0_T271806937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_0
struct  SwigDelegateObjectElementListener_0_t271806937  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEOBJECTELEMENTLISTENER_0_T271806937_H
#ifndef SWIGDELEGATEOBJECTELEMENTLISTENER_3_T271806936_H
#define SWIGDELEGATEOBJECTELEMENTLISTENER_3_T271806936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_3
struct  SwigDelegateObjectElementListener_3_t271806936  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEOBJECTELEMENTLISTENER_3_T271806936_H
#ifndef SWIGDELEGATEOBJECTELEMENTLISTENER_4_T271806941_H
#define SWIGDELEGATEOBJECTELEMENTLISTENER_4_T271806941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_4
struct  SwigDelegateObjectElementListener_4_t271806941  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEOBJECTELEMENTLISTENER_4_T271806941_H
#ifndef SWIGDELEGATEOBJECTELEMENTLISTENER_1_T271806938_H
#define SWIGDELEGATEOBJECTELEMENTLISTENER_1_T271806938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_1
struct  SwigDelegateObjectElementListener_1_t271806938  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEOBJECTELEMENTLISTENER_1_T271806938_H
#ifndef SWIGDELEGATEOBJECTELEMENTLISTENER_2_T271806935_H
#define SWIGDELEGATEOBJECTELEMENTLISTENER_2_T271806935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ObjectElementListener/SwigDelegateObjectElementListener_2
struct  SwigDelegateObjectElementListener_2_t271806935  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATEOBJECTELEMENTLISTENER_2_T271806935_H
#ifndef SWIGDELEGATENETWORKCONNECTIONLISTENER_1_T4021727017_H
#define SWIGDELEGATENETWORKCONNECTIONLISTENER_1_T4021727017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.NetworkConnectionListener/SwigDelegateNetworkConnectionListener_1
struct  SwigDelegateNetworkConnectionListener_1_t4021727017  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATENETWORKCONNECTIONLISTENER_1_T4021727017_H
#ifndef LONGELEMENT_T2161894770_H
#define LONGELEMENT_T2161894770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.LongElement
struct  LongElement_t2161894770  : public Element_t654894041
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.LongElement::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(LongElement_t2161894770, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGELEMENT_T2161894770_H
#ifndef NETWORKCONNECTIONLISTENER_T2919586752_H
#define NETWORKCONNECTIONLISTENER_T2919586752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.NetworkConnectionListener
struct  NetworkConnectionListener_t2919586752  : public Listener_t2209039043
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.NetworkConnectionListener::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;
	// HoloToolkit.Sharing.NetworkConnectionListener/SwigDelegateNetworkConnectionListener_0 HoloToolkit.Sharing.NetworkConnectionListener::swigDelegate0
	SwigDelegateNetworkConnectionListener_0_t4021727018 * ___swigDelegate0_3;
	// HoloToolkit.Sharing.NetworkConnectionListener/SwigDelegateNetworkConnectionListener_1 HoloToolkit.Sharing.NetworkConnectionListener::swigDelegate1
	SwigDelegateNetworkConnectionListener_1_t4021727017 * ___swigDelegate1_4;
	// HoloToolkit.Sharing.NetworkConnectionListener/SwigDelegateNetworkConnectionListener_2 HoloToolkit.Sharing.NetworkConnectionListener::swigDelegate2
	SwigDelegateNetworkConnectionListener_2_t4021727020 * ___swigDelegate2_5;
	// HoloToolkit.Sharing.NetworkConnectionListener/SwigDelegateNetworkConnectionListener_3 HoloToolkit.Sharing.NetworkConnectionListener::swigDelegate3
	SwigDelegateNetworkConnectionListener_3_t4021727019 * ___swigDelegate3_6;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(NetworkConnectionListener_t2919586752, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_3() { return static_cast<int32_t>(offsetof(NetworkConnectionListener_t2919586752, ___swigDelegate0_3)); }
	inline SwigDelegateNetworkConnectionListener_0_t4021727018 * get_swigDelegate0_3() const { return ___swigDelegate0_3; }
	inline SwigDelegateNetworkConnectionListener_0_t4021727018 ** get_address_of_swigDelegate0_3() { return &___swigDelegate0_3; }
	inline void set_swigDelegate0_3(SwigDelegateNetworkConnectionListener_0_t4021727018 * value)
	{
		___swigDelegate0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_3), value);
	}

	inline static int32_t get_offset_of_swigDelegate1_4() { return static_cast<int32_t>(offsetof(NetworkConnectionListener_t2919586752, ___swigDelegate1_4)); }
	inline SwigDelegateNetworkConnectionListener_1_t4021727017 * get_swigDelegate1_4() const { return ___swigDelegate1_4; }
	inline SwigDelegateNetworkConnectionListener_1_t4021727017 ** get_address_of_swigDelegate1_4() { return &___swigDelegate1_4; }
	inline void set_swigDelegate1_4(SwigDelegateNetworkConnectionListener_1_t4021727017 * value)
	{
		___swigDelegate1_4 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate1_4), value);
	}

	inline static int32_t get_offset_of_swigDelegate2_5() { return static_cast<int32_t>(offsetof(NetworkConnectionListener_t2919586752, ___swigDelegate2_5)); }
	inline SwigDelegateNetworkConnectionListener_2_t4021727020 * get_swigDelegate2_5() const { return ___swigDelegate2_5; }
	inline SwigDelegateNetworkConnectionListener_2_t4021727020 ** get_address_of_swigDelegate2_5() { return &___swigDelegate2_5; }
	inline void set_swigDelegate2_5(SwigDelegateNetworkConnectionListener_2_t4021727020 * value)
	{
		___swigDelegate2_5 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate2_5), value);
	}

	inline static int32_t get_offset_of_swigDelegate3_6() { return static_cast<int32_t>(offsetof(NetworkConnectionListener_t2919586752, ___swigDelegate3_6)); }
	inline SwigDelegateNetworkConnectionListener_3_t4021727019 * get_swigDelegate3_6() const { return ___swigDelegate3_6; }
	inline SwigDelegateNetworkConnectionListener_3_t4021727019 ** get_address_of_swigDelegate3_6() { return &___swigDelegate3_6; }
	inline void set_swigDelegate3_6(SwigDelegateNetworkConnectionListener_3_t4021727019 * value)
	{
		___swigDelegate3_6 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate3_6), value);
	}
};

struct NetworkConnectionListener_t2919586752_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.NetworkConnectionListener::swigMethodTypes0
	TypeU5BU5D_t3940880105* ___swigMethodTypes0_7;
	// System.Type[] HoloToolkit.Sharing.NetworkConnectionListener::swigMethodTypes1
	TypeU5BU5D_t3940880105* ___swigMethodTypes1_8;
	// System.Type[] HoloToolkit.Sharing.NetworkConnectionListener::swigMethodTypes2
	TypeU5BU5D_t3940880105* ___swigMethodTypes2_9;
	// System.Type[] HoloToolkit.Sharing.NetworkConnectionListener::swigMethodTypes3
	TypeU5BU5D_t3940880105* ___swigMethodTypes3_10;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_7() { return static_cast<int32_t>(offsetof(NetworkConnectionListener_t2919586752_StaticFields, ___swigMethodTypes0_7)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes0_7() const { return ___swigMethodTypes0_7; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes0_7() { return &___swigMethodTypes0_7; }
	inline void set_swigMethodTypes0_7(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes0_7 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_7), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes1_8() { return static_cast<int32_t>(offsetof(NetworkConnectionListener_t2919586752_StaticFields, ___swigMethodTypes1_8)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes1_8() const { return ___swigMethodTypes1_8; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes1_8() { return &___swigMethodTypes1_8; }
	inline void set_swigMethodTypes1_8(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes1_8 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes1_8), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes2_9() { return static_cast<int32_t>(offsetof(NetworkConnectionListener_t2919586752_StaticFields, ___swigMethodTypes2_9)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes2_9() const { return ___swigMethodTypes2_9; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes2_9() { return &___swigMethodTypes2_9; }
	inline void set_swigMethodTypes2_9(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes2_9 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes2_9), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes3_10() { return static_cast<int32_t>(offsetof(NetworkConnectionListener_t2919586752_StaticFields, ___swigMethodTypes3_10)); }
	inline TypeU5BU5D_t3940880105* get_swigMethodTypes3_10() const { return ___swigMethodTypes3_10; }
	inline TypeU5BU5D_t3940880105** get_address_of_swigMethodTypes3_10() { return &___swigMethodTypes3_10; }
	inline void set_swigMethodTypes3_10(TypeU5BU5D_t3940880105* value)
	{
		___swigMethodTypes3_10 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes3_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCONNECTIONLISTENER_T2919586752_H
#ifndef INTELEMENT_T687264645_H
#define INTELEMENT_T687264645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.IntElement
struct  IntElement_t687264645  : public Element_t654894041
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.IntElement::swigCPtr
	HandleRef_t3745784363  ___swigCPtr_2;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(IntElement_t687264645, ___swigCPtr_2)); }
	inline HandleRef_t3745784363  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784363 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784363  value)
	{
		___swigCPtr_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTELEMENT_T687264645_H
#ifndef SWIGDELEGATELOGWRITER_0_T50445701_H
#define SWIGDELEGATELOGWRITER_0_T50445701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.LogWriter/SwigDelegateLogWriter_0
struct  SwigDelegateLogWriter_0_t50445701  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATELOGWRITER_0_T50445701_H
#ifndef SWIGDELEGATENETWORKCONNECTIONLISTENER_0_T4021727018_H
#define SWIGDELEGATENETWORKCONNECTIONLISTENER_0_T4021727018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.NetworkConnectionListener/SwigDelegateNetworkConnectionListener_0
struct  SwigDelegateNetworkConnectionListener_0_t4021727018  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGDELEGATENETWORKCONNECTIONLISTENER_0_T4021727018_H
#ifndef DISCOVERYCLIENTADAPTER_T99005982_H
#define DISCOVERYCLIENTADAPTER_T99005982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.DiscoveryClientAdapter
struct  DiscoveryClientAdapter_t99005982  : public DiscoveryClientListener_t1752642716
{
public:
	// System.Action`1<HoloToolkit.Sharing.DiscoveredSystem> HoloToolkit.Sharing.DiscoveryClientAdapter::DiscoveredEvent
	Action_1_t1236426205 * ___DiscoveredEvent_7;
	// System.Action`1<HoloToolkit.Sharing.DiscoveredSystem> HoloToolkit.Sharing.DiscoveryClientAdapter::LostEvent
	Action_1_t1236426205 * ___LostEvent_8;

public:
	inline static int32_t get_offset_of_DiscoveredEvent_7() { return static_cast<int32_t>(offsetof(DiscoveryClientAdapter_t99005982, ___DiscoveredEvent_7)); }
	inline Action_1_t1236426205 * get_DiscoveredEvent_7() const { return ___DiscoveredEvent_7; }
	inline Action_1_t1236426205 ** get_address_of_DiscoveredEvent_7() { return &___DiscoveredEvent_7; }
	inline void set_DiscoveredEvent_7(Action_1_t1236426205 * value)
	{
		___DiscoveredEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___DiscoveredEvent_7), value);
	}

	inline static int32_t get_offset_of_LostEvent_8() { return static_cast<int32_t>(offsetof(DiscoveryClientAdapter_t99005982, ___LostEvent_8)); }
	inline Action_1_t1236426205 * get_LostEvent_8() const { return ___LostEvent_8; }
	inline Action_1_t1236426205 ** get_address_of_LostEvent_8() { return &___LostEvent_8; }
	inline void set_LostEvent_8(Action_1_t1236426205 * value)
	{
		___LostEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&___LostEvent_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCOVERYCLIENTADAPTER_T99005982_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef PAIRINGADAPTER_T2261856171_H
#define PAIRINGADAPTER_T2261856171_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.PairingAdapter
struct  PairingAdapter_t2261856171  : public PairingListener_t2464634798
{
public:
	// System.Action HoloToolkit.Sharing.PairingAdapter::SuccessEvent
	Action_t1264377477 * ___SuccessEvent_7;
	// System.Action`1<HoloToolkit.Sharing.PairingResult> HoloToolkit.Sharing.PairingAdapter::FailureEvent
	Action_1_t1658156947 * ___FailureEvent_8;

public:
	inline static int32_t get_offset_of_SuccessEvent_7() { return static_cast<int32_t>(offsetof(PairingAdapter_t2261856171, ___SuccessEvent_7)); }
	inline Action_t1264377477 * get_SuccessEvent_7() const { return ___SuccessEvent_7; }
	inline Action_t1264377477 ** get_address_of_SuccessEvent_7() { return &___SuccessEvent_7; }
	inline void set_SuccessEvent_7(Action_t1264377477 * value)
	{
		___SuccessEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___SuccessEvent_7), value);
	}

	inline static int32_t get_offset_of_FailureEvent_8() { return static_cast<int32_t>(offsetof(PairingAdapter_t2261856171, ___FailureEvent_8)); }
	inline Action_1_t1658156947 * get_FailureEvent_8() const { return ___FailureEvent_8; }
	inline Action_1_t1658156947 ** get_address_of_FailureEvent_8() { return &___FailureEvent_8; }
	inline void set_FailureEvent_8(Action_1_t1658156947 * value)
	{
		___FailureEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&___FailureEvent_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIRINGADAPTER_T2261856171_H
#ifndef OBJECTELEMENTADAPTER_T1935870410_H
#define OBJECTELEMENTADAPTER_T1935870410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ObjectElementAdapter
struct  ObjectElementAdapter_t1935870410  : public ObjectElementListener_t389328343
{
public:
	// System.Action`2<System.Int64,System.Boolean> HoloToolkit.Sharing.ObjectElementAdapter::BoolChangedEvent
	Action_2_t3497391435 * ___BoolChangedEvent_19;
	// System.Action`2<System.Int64,System.Int32> HoloToolkit.Sharing.ObjectElementAdapter::IntChangedEvent
	Action_2_t2056081927 * ___IntChangedEvent_20;
	// System.Action`2<System.Int64,System.Int64> HoloToolkit.Sharing.ObjectElementAdapter::LongChangedEvent
	Action_2_t2841703478 * ___LongChangedEvent_21;
	// System.Action`2<System.Int64,System.Single> HoloToolkit.Sharing.ObjectElementAdapter::FloatChangedEvent
	Action_2_t502402948 * ___FloatChangedEvent_22;
	// System.Action`2<System.Int64,System.Double> HoloToolkit.Sharing.ObjectElementAdapter::DoubleChangedEvent
	Action_2_t3994768833 * ___DoubleChangedEvent_23;
	// System.Action`2<System.Int64,HoloToolkit.Sharing.XString> HoloToolkit.Sharing.ObjectElementAdapter::StringChangedEvent
	Action_2_t45974928 * ___StringChangedEvent_24;
	// System.Action`1<HoloToolkit.Sharing.Element> HoloToolkit.Sharing.ObjectElementAdapter::ElementAddedEvent
	Action_1_t827361636 * ___ElementAddedEvent_25;
	// System.Action`1<HoloToolkit.Sharing.Element> HoloToolkit.Sharing.ObjectElementAdapter::ElementDeletedEvent
	Action_1_t827361636 * ___ElementDeletedEvent_26;

public:
	inline static int32_t get_offset_of_BoolChangedEvent_19() { return static_cast<int32_t>(offsetof(ObjectElementAdapter_t1935870410, ___BoolChangedEvent_19)); }
	inline Action_2_t3497391435 * get_BoolChangedEvent_19() const { return ___BoolChangedEvent_19; }
	inline Action_2_t3497391435 ** get_address_of_BoolChangedEvent_19() { return &___BoolChangedEvent_19; }
	inline void set_BoolChangedEvent_19(Action_2_t3497391435 * value)
	{
		___BoolChangedEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___BoolChangedEvent_19), value);
	}

	inline static int32_t get_offset_of_IntChangedEvent_20() { return static_cast<int32_t>(offsetof(ObjectElementAdapter_t1935870410, ___IntChangedEvent_20)); }
	inline Action_2_t2056081927 * get_IntChangedEvent_20() const { return ___IntChangedEvent_20; }
	inline Action_2_t2056081927 ** get_address_of_IntChangedEvent_20() { return &___IntChangedEvent_20; }
	inline void set_IntChangedEvent_20(Action_2_t2056081927 * value)
	{
		___IntChangedEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___IntChangedEvent_20), value);
	}

	inline static int32_t get_offset_of_LongChangedEvent_21() { return static_cast<int32_t>(offsetof(ObjectElementAdapter_t1935870410, ___LongChangedEvent_21)); }
	inline Action_2_t2841703478 * get_LongChangedEvent_21() const { return ___LongChangedEvent_21; }
	inline Action_2_t2841703478 ** get_address_of_LongChangedEvent_21() { return &___LongChangedEvent_21; }
	inline void set_LongChangedEvent_21(Action_2_t2841703478 * value)
	{
		___LongChangedEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___LongChangedEvent_21), value);
	}

	inline static int32_t get_offset_of_FloatChangedEvent_22() { return static_cast<int32_t>(offsetof(ObjectElementAdapter_t1935870410, ___FloatChangedEvent_22)); }
	inline Action_2_t502402948 * get_FloatChangedEvent_22() const { return ___FloatChangedEvent_22; }
	inline Action_2_t502402948 ** get_address_of_FloatChangedEvent_22() { return &___FloatChangedEvent_22; }
	inline void set_FloatChangedEvent_22(Action_2_t502402948 * value)
	{
		___FloatChangedEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___FloatChangedEvent_22), value);
	}

	inline static int32_t get_offset_of_DoubleChangedEvent_23() { return static_cast<int32_t>(offsetof(ObjectElementAdapter_t1935870410, ___DoubleChangedEvent_23)); }
	inline Action_2_t3994768833 * get_DoubleChangedEvent_23() const { return ___DoubleChangedEvent_23; }
	inline Action_2_t3994768833 ** get_address_of_DoubleChangedEvent_23() { return &___DoubleChangedEvent_23; }
	inline void set_DoubleChangedEvent_23(Action_2_t3994768833 * value)
	{
		___DoubleChangedEvent_23 = value;
		Il2CppCodeGenWriteBarrier((&___DoubleChangedEvent_23), value);
	}

	inline static int32_t get_offset_of_StringChangedEvent_24() { return static_cast<int32_t>(offsetof(ObjectElementAdapter_t1935870410, ___StringChangedEvent_24)); }
	inline Action_2_t45974928 * get_StringChangedEvent_24() const { return ___StringChangedEvent_24; }
	inline Action_2_t45974928 ** get_address_of_StringChangedEvent_24() { return &___StringChangedEvent_24; }
	inline void set_StringChangedEvent_24(Action_2_t45974928 * value)
	{
		___StringChangedEvent_24 = value;
		Il2CppCodeGenWriteBarrier((&___StringChangedEvent_24), value);
	}

	inline static int32_t get_offset_of_ElementAddedEvent_25() { return static_cast<int32_t>(offsetof(ObjectElementAdapter_t1935870410, ___ElementAddedEvent_25)); }
	inline Action_1_t827361636 * get_ElementAddedEvent_25() const { return ___ElementAddedEvent_25; }
	inline Action_1_t827361636 ** get_address_of_ElementAddedEvent_25() { return &___ElementAddedEvent_25; }
	inline void set_ElementAddedEvent_25(Action_1_t827361636 * value)
	{
		___ElementAddedEvent_25 = value;
		Il2CppCodeGenWriteBarrier((&___ElementAddedEvent_25), value);
	}

	inline static int32_t get_offset_of_ElementDeletedEvent_26() { return static_cast<int32_t>(offsetof(ObjectElementAdapter_t1935870410, ___ElementDeletedEvent_26)); }
	inline Action_1_t827361636 * get_ElementDeletedEvent_26() const { return ___ElementDeletedEvent_26; }
	inline Action_1_t827361636 ** get_address_of_ElementDeletedEvent_26() { return &___ElementDeletedEvent_26; }
	inline void set_ElementDeletedEvent_26(Action_1_t827361636 * value)
	{
		___ElementDeletedEvent_26 = value;
		Il2CppCodeGenWriteBarrier((&___ElementDeletedEvent_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTELEMENTADAPTER_T1935870410_H
#ifndef NETWORKCONNECTIONADAPTER_T239632668_H
#define NETWORKCONNECTIONADAPTER_T239632668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.NetworkConnectionAdapter
struct  NetworkConnectionAdapter_t239632668  : public NetworkConnectionListener_t2919586752
{
public:
	// System.Action`1<HoloToolkit.Sharing.NetworkConnection> HoloToolkit.Sharing.NetworkConnectionAdapter::ConnectedCallback
	Action_1_t1272347286 * ___ConnectedCallback_11;
	// System.Action`1<HoloToolkit.Sharing.NetworkConnection> HoloToolkit.Sharing.NetworkConnectionAdapter::ConnectionFailedCallback
	Action_1_t1272347286 * ___ConnectionFailedCallback_12;
	// System.Action`1<HoloToolkit.Sharing.NetworkConnection> HoloToolkit.Sharing.NetworkConnectionAdapter::DisconnectedCallback
	Action_1_t1272347286 * ___DisconnectedCallback_13;
	// System.Action`2<HoloToolkit.Sharing.NetworkConnection,HoloToolkit.Sharing.NetworkInMessage> HoloToolkit.Sharing.NetworkConnectionAdapter::MessageReceivedCallback
	Action_2_t2041518244 * ___MessageReceivedCallback_14;

public:
	inline static int32_t get_offset_of_ConnectedCallback_11() { return static_cast<int32_t>(offsetof(NetworkConnectionAdapter_t239632668, ___ConnectedCallback_11)); }
	inline Action_1_t1272347286 * get_ConnectedCallback_11() const { return ___ConnectedCallback_11; }
	inline Action_1_t1272347286 ** get_address_of_ConnectedCallback_11() { return &___ConnectedCallback_11; }
	inline void set_ConnectedCallback_11(Action_1_t1272347286 * value)
	{
		___ConnectedCallback_11 = value;
		Il2CppCodeGenWriteBarrier((&___ConnectedCallback_11), value);
	}

	inline static int32_t get_offset_of_ConnectionFailedCallback_12() { return static_cast<int32_t>(offsetof(NetworkConnectionAdapter_t239632668, ___ConnectionFailedCallback_12)); }
	inline Action_1_t1272347286 * get_ConnectionFailedCallback_12() const { return ___ConnectionFailedCallback_12; }
	inline Action_1_t1272347286 ** get_address_of_ConnectionFailedCallback_12() { return &___ConnectionFailedCallback_12; }
	inline void set_ConnectionFailedCallback_12(Action_1_t1272347286 * value)
	{
		___ConnectionFailedCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___ConnectionFailedCallback_12), value);
	}

	inline static int32_t get_offset_of_DisconnectedCallback_13() { return static_cast<int32_t>(offsetof(NetworkConnectionAdapter_t239632668, ___DisconnectedCallback_13)); }
	inline Action_1_t1272347286 * get_DisconnectedCallback_13() const { return ___DisconnectedCallback_13; }
	inline Action_1_t1272347286 ** get_address_of_DisconnectedCallback_13() { return &___DisconnectedCallback_13; }
	inline void set_DisconnectedCallback_13(Action_1_t1272347286 * value)
	{
		___DisconnectedCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___DisconnectedCallback_13), value);
	}

	inline static int32_t get_offset_of_MessageReceivedCallback_14() { return static_cast<int32_t>(offsetof(NetworkConnectionAdapter_t239632668, ___MessageReceivedCallback_14)); }
	inline Action_2_t2041518244 * get_MessageReceivedCallback_14() const { return ___MessageReceivedCallback_14; }
	inline Action_2_t2041518244 ** get_address_of_MessageReceivedCallback_14() { return &___MessageReceivedCallback_14; }
	inline void set_MessageReceivedCallback_14(Action_2_t2041518244 * value)
	{
		___MessageReceivedCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___MessageReceivedCallback_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCONNECTIONADAPTER_T239632668_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef KEYBOARDVALUEKEY_T175000019_H
#define KEYBOARDVALUEKEY_T175000019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.UI.Keyboard.KeyboardValueKey
struct  KeyboardValueKey_t175000019  : public MonoBehaviour_t3962482529
{
public:
	// System.String HoloToolkit.UI.Keyboard.KeyboardValueKey::Value
	String_t* ___Value_2;
	// System.String HoloToolkit.UI.Keyboard.KeyboardValueKey::ShiftValue
	String_t* ___ShiftValue_3;
	// UnityEngine.UI.Text HoloToolkit.UI.Keyboard.KeyboardValueKey::m_Text
	Text_t1901882714 * ___m_Text_4;
	// UnityEngine.UI.Button HoloToolkit.UI.Keyboard.KeyboardValueKey::m_Button
	Button_t4055032469 * ___m_Button_5;

public:
	inline static int32_t get_offset_of_Value_2() { return static_cast<int32_t>(offsetof(KeyboardValueKey_t175000019, ___Value_2)); }
	inline String_t* get_Value_2() const { return ___Value_2; }
	inline String_t** get_address_of_Value_2() { return &___Value_2; }
	inline void set_Value_2(String_t* value)
	{
		___Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___Value_2), value);
	}

	inline static int32_t get_offset_of_ShiftValue_3() { return static_cast<int32_t>(offsetof(KeyboardValueKey_t175000019, ___ShiftValue_3)); }
	inline String_t* get_ShiftValue_3() const { return ___ShiftValue_3; }
	inline String_t** get_address_of_ShiftValue_3() { return &___ShiftValue_3; }
	inline void set_ShiftValue_3(String_t* value)
	{
		___ShiftValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___ShiftValue_3), value);
	}

	inline static int32_t get_offset_of_m_Text_4() { return static_cast<int32_t>(offsetof(KeyboardValueKey_t175000019, ___m_Text_4)); }
	inline Text_t1901882714 * get_m_Text_4() const { return ___m_Text_4; }
	inline Text_t1901882714 ** get_address_of_m_Text_4() { return &___m_Text_4; }
	inline void set_m_Text_4(Text_t1901882714 * value)
	{
		___m_Text_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_4), value);
	}

	inline static int32_t get_offset_of_m_Button_5() { return static_cast<int32_t>(offsetof(KeyboardValueKey_t175000019, ___m_Button_5)); }
	inline Button_t4055032469 * get_m_Button_5() const { return ___m_Button_5; }
	inline Button_t4055032469 ** get_address_of_m_Button_5() { return &___m_Button_5; }
	inline void set_m_Button_5(Button_t4055032469 * value)
	{
		___m_Button_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Button_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDVALUEKEY_T175000019_H
#ifndef AXISSLIDER_T300566400_H
#define AXISSLIDER_T300566400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.UI.Keyboard.AxisSlider
struct  AxisSlider_t300566400  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.UI.Keyboard.AxisSlider/EAxis HoloToolkit.UI.Keyboard.AxisSlider::Axis
	int32_t ___Axis_2;
	// System.Single HoloToolkit.UI.Keyboard.AxisSlider::currentPos
	float ___currentPos_3;
	// System.Single HoloToolkit.UI.Keyboard.AxisSlider::slideVel
	float ___slideVel_4;
	// System.Single HoloToolkit.UI.Keyboard.AxisSlider::slideAccel
	float ___slideAccel_5;
	// System.Single HoloToolkit.UI.Keyboard.AxisSlider::slideFriction
	float ___slideFriction_6;
	// System.Single HoloToolkit.UI.Keyboard.AxisSlider::deadZone
	float ___deadZone_7;
	// System.Single HoloToolkit.UI.Keyboard.AxisSlider::clampDistance
	float ___clampDistance_8;
	// System.Single HoloToolkit.UI.Keyboard.AxisSlider::bounce
	float ___bounce_9;
	// UnityEngine.Vector3 HoloToolkit.UI.Keyboard.AxisSlider::TargetPoint
	Vector3_t3722313464  ___TargetPoint_10;

public:
	inline static int32_t get_offset_of_Axis_2() { return static_cast<int32_t>(offsetof(AxisSlider_t300566400, ___Axis_2)); }
	inline int32_t get_Axis_2() const { return ___Axis_2; }
	inline int32_t* get_address_of_Axis_2() { return &___Axis_2; }
	inline void set_Axis_2(int32_t value)
	{
		___Axis_2 = value;
	}

	inline static int32_t get_offset_of_currentPos_3() { return static_cast<int32_t>(offsetof(AxisSlider_t300566400, ___currentPos_3)); }
	inline float get_currentPos_3() const { return ___currentPos_3; }
	inline float* get_address_of_currentPos_3() { return &___currentPos_3; }
	inline void set_currentPos_3(float value)
	{
		___currentPos_3 = value;
	}

	inline static int32_t get_offset_of_slideVel_4() { return static_cast<int32_t>(offsetof(AxisSlider_t300566400, ___slideVel_4)); }
	inline float get_slideVel_4() const { return ___slideVel_4; }
	inline float* get_address_of_slideVel_4() { return &___slideVel_4; }
	inline void set_slideVel_4(float value)
	{
		___slideVel_4 = value;
	}

	inline static int32_t get_offset_of_slideAccel_5() { return static_cast<int32_t>(offsetof(AxisSlider_t300566400, ___slideAccel_5)); }
	inline float get_slideAccel_5() const { return ___slideAccel_5; }
	inline float* get_address_of_slideAccel_5() { return &___slideAccel_5; }
	inline void set_slideAccel_5(float value)
	{
		___slideAccel_5 = value;
	}

	inline static int32_t get_offset_of_slideFriction_6() { return static_cast<int32_t>(offsetof(AxisSlider_t300566400, ___slideFriction_6)); }
	inline float get_slideFriction_6() const { return ___slideFriction_6; }
	inline float* get_address_of_slideFriction_6() { return &___slideFriction_6; }
	inline void set_slideFriction_6(float value)
	{
		___slideFriction_6 = value;
	}

	inline static int32_t get_offset_of_deadZone_7() { return static_cast<int32_t>(offsetof(AxisSlider_t300566400, ___deadZone_7)); }
	inline float get_deadZone_7() const { return ___deadZone_7; }
	inline float* get_address_of_deadZone_7() { return &___deadZone_7; }
	inline void set_deadZone_7(float value)
	{
		___deadZone_7 = value;
	}

	inline static int32_t get_offset_of_clampDistance_8() { return static_cast<int32_t>(offsetof(AxisSlider_t300566400, ___clampDistance_8)); }
	inline float get_clampDistance_8() const { return ___clampDistance_8; }
	inline float* get_address_of_clampDistance_8() { return &___clampDistance_8; }
	inline void set_clampDistance_8(float value)
	{
		___clampDistance_8 = value;
	}

	inline static int32_t get_offset_of_bounce_9() { return static_cast<int32_t>(offsetof(AxisSlider_t300566400, ___bounce_9)); }
	inline float get_bounce_9() const { return ___bounce_9; }
	inline float* get_address_of_bounce_9() { return &___bounce_9; }
	inline void set_bounce_9(float value)
	{
		___bounce_9 = value;
	}

	inline static int32_t get_offset_of_TargetPoint_10() { return static_cast<int32_t>(offsetof(AxisSlider_t300566400, ___TargetPoint_10)); }
	inline Vector3_t3722313464  get_TargetPoint_10() const { return ___TargetPoint_10; }
	inline Vector3_t3722313464 * get_address_of_TargetPoint_10() { return &___TargetPoint_10; }
	inline void set_TargetPoint_10(Vector3_t3722313464  value)
	{
		___TargetPoint_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSLIDER_T300566400_H
#ifndef CAPSLOCKHIGHLIGHT_T2579730708_H
#define CAPSLOCKHIGHLIGHT_T2579730708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.UI.Keyboard.CapsLockHighlight
struct  CapsLockHighlight_t2579730708  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image HoloToolkit.UI.Keyboard.CapsLockHighlight::m_Highlight
	Image_t2670269651 * ___m_Highlight_2;
	// HoloToolkit.UI.Keyboard.Keyboard HoloToolkit.UI.Keyboard.CapsLockHighlight::m_Keyboard
	Keyboard_t4047957915 * ___m_Keyboard_3;

public:
	inline static int32_t get_offset_of_m_Highlight_2() { return static_cast<int32_t>(offsetof(CapsLockHighlight_t2579730708, ___m_Highlight_2)); }
	inline Image_t2670269651 * get_m_Highlight_2() const { return ___m_Highlight_2; }
	inline Image_t2670269651 ** get_address_of_m_Highlight_2() { return &___m_Highlight_2; }
	inline void set_m_Highlight_2(Image_t2670269651 * value)
	{
		___m_Highlight_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Highlight_2), value);
	}

	inline static int32_t get_offset_of_m_Keyboard_3() { return static_cast<int32_t>(offsetof(CapsLockHighlight_t2579730708, ___m_Keyboard_3)); }
	inline Keyboard_t4047957915 * get_m_Keyboard_3() const { return ___m_Keyboard_3; }
	inline Keyboard_t4047957915 ** get_address_of_m_Keyboard_3() { return &___m_Keyboard_3; }
	inline void set_m_Keyboard_3(Keyboard_t4047957915 * value)
	{
		___m_Keyboard_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Keyboard_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPSLOCKHIGHLIGHT_T2579730708_H
#ifndef KEYBOARDKEYFUNC_T2343856591_H
#define KEYBOARDKEYFUNC_T2343856591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.UI.Keyboard.KeyboardKeyFunc
struct  KeyboardKeyFunc_t2343856591  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.UI.Keyboard.KeyboardKeyFunc/Function HoloToolkit.UI.Keyboard.KeyboardKeyFunc::m_ButtonFunction
	int32_t ___m_ButtonFunction_2;
	// UnityEngine.UI.Button HoloToolkit.UI.Keyboard.KeyboardKeyFunc::m_Button
	Button_t4055032469 * ___m_Button_3;

public:
	inline static int32_t get_offset_of_m_ButtonFunction_2() { return static_cast<int32_t>(offsetof(KeyboardKeyFunc_t2343856591, ___m_ButtonFunction_2)); }
	inline int32_t get_m_ButtonFunction_2() const { return ___m_ButtonFunction_2; }
	inline int32_t* get_address_of_m_ButtonFunction_2() { return &___m_ButtonFunction_2; }
	inline void set_m_ButtonFunction_2(int32_t value)
	{
		___m_ButtonFunction_2 = value;
	}

	inline static int32_t get_offset_of_m_Button_3() { return static_cast<int32_t>(offsetof(KeyboardKeyFunc_t2343856591, ___m_Button_3)); }
	inline Button_t4055032469 * get_m_Button_3() const { return ___m_Button_3; }
	inline Button_t4055032469 ** get_address_of_m_Button_3() { return &___m_Button_3; }
	inline void set_m_Button_3(Button_t4055032469 * value)
	{
		___m_Button_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Button_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDKEYFUNC_T2343856591_H
#ifndef SYMBOLDISABLEHIGHLIGHT_T3411979291_H
#define SYMBOLDISABLEHIGHLIGHT_T3411979291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.UI.Keyboard.SymbolDisableHighlight
struct  SymbolDisableHighlight_t3411979291  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text HoloToolkit.UI.Keyboard.SymbolDisableHighlight::m_TextField
	Text_t1901882714 * ___m_TextField_2;
	// UnityEngine.UI.Image HoloToolkit.UI.Keyboard.SymbolDisableHighlight::m_ImageField
	Image_t2670269651 * ___m_ImageField_3;
	// UnityEngine.Color HoloToolkit.UI.Keyboard.SymbolDisableHighlight::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_4;
	// UnityEngine.Color HoloToolkit.UI.Keyboard.SymbolDisableHighlight::m_StartingColor
	Color_t2555686324  ___m_StartingColor_5;
	// UnityEngine.UI.Button HoloToolkit.UI.Keyboard.SymbolDisableHighlight::m_Button
	Button_t4055032469 * ___m_Button_6;

public:
	inline static int32_t get_offset_of_m_TextField_2() { return static_cast<int32_t>(offsetof(SymbolDisableHighlight_t3411979291, ___m_TextField_2)); }
	inline Text_t1901882714 * get_m_TextField_2() const { return ___m_TextField_2; }
	inline Text_t1901882714 ** get_address_of_m_TextField_2() { return &___m_TextField_2; }
	inline void set_m_TextField_2(Text_t1901882714 * value)
	{
		___m_TextField_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextField_2), value);
	}

	inline static int32_t get_offset_of_m_ImageField_3() { return static_cast<int32_t>(offsetof(SymbolDisableHighlight_t3411979291, ___m_ImageField_3)); }
	inline Image_t2670269651 * get_m_ImageField_3() const { return ___m_ImageField_3; }
	inline Image_t2670269651 ** get_address_of_m_ImageField_3() { return &___m_ImageField_3; }
	inline void set_m_ImageField_3(Image_t2670269651 * value)
	{
		___m_ImageField_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ImageField_3), value);
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(SymbolDisableHighlight_t3411979291, ___m_DisabledColor_4)); }
	inline Color_t2555686324  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_t2555686324  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_StartingColor_5() { return static_cast<int32_t>(offsetof(SymbolDisableHighlight_t3411979291, ___m_StartingColor_5)); }
	inline Color_t2555686324  get_m_StartingColor_5() const { return ___m_StartingColor_5; }
	inline Color_t2555686324 * get_address_of_m_StartingColor_5() { return &___m_StartingColor_5; }
	inline void set_m_StartingColor_5(Color_t2555686324  value)
	{
		___m_StartingColor_5 = value;
	}

	inline static int32_t get_offset_of_m_Button_6() { return static_cast<int32_t>(offsetof(SymbolDisableHighlight_t3411979291, ___m_Button_6)); }
	inline Button_t4055032469 * get_m_Button_6() const { return ___m_Button_6; }
	inline Button_t4055032469 ** get_address_of_m_Button_6() { return &___m_Button_6; }
	inline void set_m_Button_6(Button_t4055032469 * value)
	{
		___m_Button_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Button_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMBOLDISABLEHIGHLIGHT_T3411979291_H
#ifndef UICOLLECTION_T2513404254_H
#define UICOLLECTION_T2513404254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.UI.Keyboard.UICollection
struct  UICollection_t2513404254  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HoloToolkit.UI.Keyboard.UICollection::MaxWidth
	float ___MaxWidth_2;
	// System.Single HoloToolkit.UI.Keyboard.UICollection::MaxHeight
	float ___MaxHeight_3;
	// System.Single HoloToolkit.UI.Keyboard.UICollection::HorizontalSpacing
	float ___HorizontalSpacing_4;
	// System.Single HoloToolkit.UI.Keyboard.UICollection::VerticalSpacing
	float ___VerticalSpacing_5;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> HoloToolkit.UI.Keyboard.UICollection::<Items>k__BackingField
	List_1_t881764471 * ___U3CItemsU3Ek__BackingField_6;
	// UnityEngine.RectTransform HoloToolkit.UI.Keyboard.UICollection::rectTransform
	RectTransform_t3704657025 * ___rectTransform_7;

public:
	inline static int32_t get_offset_of_MaxWidth_2() { return static_cast<int32_t>(offsetof(UICollection_t2513404254, ___MaxWidth_2)); }
	inline float get_MaxWidth_2() const { return ___MaxWidth_2; }
	inline float* get_address_of_MaxWidth_2() { return &___MaxWidth_2; }
	inline void set_MaxWidth_2(float value)
	{
		___MaxWidth_2 = value;
	}

	inline static int32_t get_offset_of_MaxHeight_3() { return static_cast<int32_t>(offsetof(UICollection_t2513404254, ___MaxHeight_3)); }
	inline float get_MaxHeight_3() const { return ___MaxHeight_3; }
	inline float* get_address_of_MaxHeight_3() { return &___MaxHeight_3; }
	inline void set_MaxHeight_3(float value)
	{
		___MaxHeight_3 = value;
	}

	inline static int32_t get_offset_of_HorizontalSpacing_4() { return static_cast<int32_t>(offsetof(UICollection_t2513404254, ___HorizontalSpacing_4)); }
	inline float get_HorizontalSpacing_4() const { return ___HorizontalSpacing_4; }
	inline float* get_address_of_HorizontalSpacing_4() { return &___HorizontalSpacing_4; }
	inline void set_HorizontalSpacing_4(float value)
	{
		___HorizontalSpacing_4 = value;
	}

	inline static int32_t get_offset_of_VerticalSpacing_5() { return static_cast<int32_t>(offsetof(UICollection_t2513404254, ___VerticalSpacing_5)); }
	inline float get_VerticalSpacing_5() const { return ___VerticalSpacing_5; }
	inline float* get_address_of_VerticalSpacing_5() { return &___VerticalSpacing_5; }
	inline void set_VerticalSpacing_5(float value)
	{
		___VerticalSpacing_5 = value;
	}

	inline static int32_t get_offset_of_U3CItemsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UICollection_t2513404254, ___U3CItemsU3Ek__BackingField_6)); }
	inline List_1_t881764471 * get_U3CItemsU3Ek__BackingField_6() const { return ___U3CItemsU3Ek__BackingField_6; }
	inline List_1_t881764471 ** get_address_of_U3CItemsU3Ek__BackingField_6() { return &___U3CItemsU3Ek__BackingField_6; }
	inline void set_U3CItemsU3Ek__BackingField_6(List_1_t881764471 * value)
	{
		___U3CItemsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemsU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_rectTransform_7() { return static_cast<int32_t>(offsetof(UICollection_t2513404254, ___rectTransform_7)); }
	inline RectTransform_t3704657025 * get_rectTransform_7() const { return ___rectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_7() { return &___rectTransform_7; }
	inline void set_rectTransform_7(RectTransform_t3704657025 * value)
	{
		___rectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICOLLECTION_T2513404254_H
#ifndef SPHERESCRIPT_T2867235001_H
#define SPHERESCRIPT_T2867235001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.Earth.SphereScript
struct  SphereScript_t2867235001  : public MonoBehaviour_t3962482529
{
public:
	// System.Single DigitalRuby.Earth.SphereScript::Radius
	float ___Radius_2;
	// System.Int32 DigitalRuby.Earth.SphereScript::Detail
	int32_t ___Detail_3;
	// System.Boolean DigitalRuby.Earth.SphereScript::IcoSphere
	bool ___IcoSphere_4;
	// System.Single DigitalRuby.Earth.SphereScript::lastRadius
	float ___lastRadius_5;
	// System.Int32 DigitalRuby.Earth.SphereScript::lastDetail
	int32_t ___lastDetail_6;
	// System.Boolean DigitalRuby.Earth.SphereScript::lastIcoSphere
	bool ___lastIcoSphere_7;
	// System.Boolean DigitalRuby.Earth.SphereScript::dirty
	bool ___dirty_8;

public:
	inline static int32_t get_offset_of_Radius_2() { return static_cast<int32_t>(offsetof(SphereScript_t2867235001, ___Radius_2)); }
	inline float get_Radius_2() const { return ___Radius_2; }
	inline float* get_address_of_Radius_2() { return &___Radius_2; }
	inline void set_Radius_2(float value)
	{
		___Radius_2 = value;
	}

	inline static int32_t get_offset_of_Detail_3() { return static_cast<int32_t>(offsetof(SphereScript_t2867235001, ___Detail_3)); }
	inline int32_t get_Detail_3() const { return ___Detail_3; }
	inline int32_t* get_address_of_Detail_3() { return &___Detail_3; }
	inline void set_Detail_3(int32_t value)
	{
		___Detail_3 = value;
	}

	inline static int32_t get_offset_of_IcoSphere_4() { return static_cast<int32_t>(offsetof(SphereScript_t2867235001, ___IcoSphere_4)); }
	inline bool get_IcoSphere_4() const { return ___IcoSphere_4; }
	inline bool* get_address_of_IcoSphere_4() { return &___IcoSphere_4; }
	inline void set_IcoSphere_4(bool value)
	{
		___IcoSphere_4 = value;
	}

	inline static int32_t get_offset_of_lastRadius_5() { return static_cast<int32_t>(offsetof(SphereScript_t2867235001, ___lastRadius_5)); }
	inline float get_lastRadius_5() const { return ___lastRadius_5; }
	inline float* get_address_of_lastRadius_5() { return &___lastRadius_5; }
	inline void set_lastRadius_5(float value)
	{
		___lastRadius_5 = value;
	}

	inline static int32_t get_offset_of_lastDetail_6() { return static_cast<int32_t>(offsetof(SphereScript_t2867235001, ___lastDetail_6)); }
	inline int32_t get_lastDetail_6() const { return ___lastDetail_6; }
	inline int32_t* get_address_of_lastDetail_6() { return &___lastDetail_6; }
	inline void set_lastDetail_6(int32_t value)
	{
		___lastDetail_6 = value;
	}

	inline static int32_t get_offset_of_lastIcoSphere_7() { return static_cast<int32_t>(offsetof(SphereScript_t2867235001, ___lastIcoSphere_7)); }
	inline bool get_lastIcoSphere_7() const { return ___lastIcoSphere_7; }
	inline bool* get_address_of_lastIcoSphere_7() { return &___lastIcoSphere_7; }
	inline void set_lastIcoSphere_7(bool value)
	{
		___lastIcoSphere_7 = value;
	}

	inline static int32_t get_offset_of_dirty_8() { return static_cast<int32_t>(offsetof(SphereScript_t2867235001, ___dirty_8)); }
	inline bool get_dirty_8() const { return ___dirty_8; }
	inline bool* get_address_of_dirty_8() { return &___dirty_8; }
	inline void set_dirty_8(bool value)
	{
		___dirty_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPHERESCRIPT_T2867235001_H
#ifndef SINGLETON_1_T153215073_H
#define SINGLETON_1_T153215073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.UI.Keyboard.Keyboard>
struct  Singleton_1_t153215073  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t153215073_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	Keyboard_t4047957915 * ___instance_2;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t153215073_StaticFields, ___instance_2)); }
	inline Keyboard_t4047957915 * get_instance_2() const { return ___instance_2; }
	inline Keyboard_t4047957915 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(Keyboard_t4047957915 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_searchForInstance_3() { return static_cast<int32_t>(offsetof(Singleton_1_t153215073_StaticFields, ___searchForInstance_3)); }
	inline bool get_searchForInstance_3() const { return ___searchForInstance_3; }
	inline bool* get_address_of_searchForInstance_3() { return &___searchForInstance_3; }
	inline void set_searchForInstance_3(bool value)
	{
		___searchForInstance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T153215073_H
#ifndef SYMBOLKEYBOARD_T847850322_H
#define SYMBOLKEYBOARD_T847850322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.UI.Keyboard.SymbolKeyboard
struct  SymbolKeyboard_t847850322  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button HoloToolkit.UI.Keyboard.SymbolKeyboard::m_PageBck
	Button_t4055032469 * ___m_PageBck_2;
	// UnityEngine.UI.Button HoloToolkit.UI.Keyboard.SymbolKeyboard::m_PageFwd
	Button_t4055032469 * ___m_PageFwd_3;

public:
	inline static int32_t get_offset_of_m_PageBck_2() { return static_cast<int32_t>(offsetof(SymbolKeyboard_t847850322, ___m_PageBck_2)); }
	inline Button_t4055032469 * get_m_PageBck_2() const { return ___m_PageBck_2; }
	inline Button_t4055032469 ** get_address_of_m_PageBck_2() { return &___m_PageBck_2; }
	inline void set_m_PageBck_2(Button_t4055032469 * value)
	{
		___m_PageBck_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PageBck_2), value);
	}

	inline static int32_t get_offset_of_m_PageFwd_3() { return static_cast<int32_t>(offsetof(SymbolKeyboard_t847850322, ___m_PageFwd_3)); }
	inline Button_t4055032469 * get_m_PageFwd_3() const { return ___m_PageFwd_3; }
	inline Button_t4055032469 ** get_address_of_m_PageFwd_3() { return &___m_PageFwd_3; }
	inline void set_m_PageFwd_3(Button_t4055032469 * value)
	{
		___m_PageFwd_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PageFwd_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMBOLKEYBOARD_T847850322_H
#ifndef KEYBOARD_T4047957915_H
#define KEYBOARD_T4047957915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.UI.Keyboard.Keyboard
struct  Keyboard_t4047957915  : public Singleton_1_t153215073
{
public:
	// System.EventHandler HoloToolkit.UI.Keyboard.Keyboard::OnTextSubmitted
	EventHandler_t1348719766 * ___OnTextSubmitted_4;
	// System.Action`1<System.String> HoloToolkit.UI.Keyboard.Keyboard::OnTextUpdated
	Action_1_t2019918284 * ___OnTextUpdated_5;
	// System.EventHandler HoloToolkit.UI.Keyboard.Keyboard::OnClosed
	EventHandler_t1348719766 * ___OnClosed_6;
	// System.EventHandler HoloToolkit.UI.Keyboard.Keyboard::OnPrevious
	EventHandler_t1348719766 * ___OnPrevious_7;
	// System.EventHandler HoloToolkit.UI.Keyboard.Keyboard::OnNext
	EventHandler_t1348719766 * ___OnNext_8;
	// System.EventHandler HoloToolkit.UI.Keyboard.Keyboard::OnPlacement
	EventHandler_t1348719766 * ___OnPlacement_9;
	// UnityEngine.UI.InputField HoloToolkit.UI.Keyboard.Keyboard::InputField
	InputField_t3762917431 * ___InputField_10;
	// HoloToolkit.UI.Keyboard.AxisSlider HoloToolkit.UI.Keyboard.Keyboard::InputFieldSlide
	AxisSlider_t300566400 * ___InputFieldSlide_11;
	// System.Boolean HoloToolkit.UI.Keyboard.Keyboard::SliderEnabled
	bool ___SliderEnabled_12;
	// System.Boolean HoloToolkit.UI.Keyboard.Keyboard::SubmitOnEnter
	bool ___SubmitOnEnter_13;
	// UnityEngine.UI.Image HoloToolkit.UI.Keyboard.Keyboard::AlphaKeyboard
	Image_t2670269651 * ___AlphaKeyboard_14;
	// UnityEngine.UI.Image HoloToolkit.UI.Keyboard.Keyboard::SymbolKeyboard
	Image_t2670269651 * ___SymbolKeyboard_15;
	// UnityEngine.UI.Image HoloToolkit.UI.Keyboard.Keyboard::AlphaSubKeys
	Image_t2670269651 * ___AlphaSubKeys_16;
	// UnityEngine.UI.Image HoloToolkit.UI.Keyboard.Keyboard::AlphaWebKeys
	Image_t2670269651 * ___AlphaWebKeys_17;
	// UnityEngine.UI.Image HoloToolkit.UI.Keyboard.Keyboard::AlphaMailKeys
	Image_t2670269651 * ___AlphaMailKeys_18;
	// HoloToolkit.UI.Keyboard.Keyboard/LayoutType HoloToolkit.UI.Keyboard.Keyboard::m_LastKeyboardLayout
	int32_t ___m_LastKeyboardLayout_19;
	// System.Single HoloToolkit.UI.Keyboard.Keyboard::m_MaxScale
	float ___m_MaxScale_20;
	// System.Single HoloToolkit.UI.Keyboard.Keyboard::m_MinScale
	float ___m_MinScale_21;
	// System.Single HoloToolkit.UI.Keyboard.Keyboard::m_MaxDistance
	float ___m_MaxDistance_22;
	// System.Single HoloToolkit.UI.Keyboard.Keyboard::m_MinDistance
	float ___m_MinDistance_23;
	// System.Boolean HoloToolkit.UI.Keyboard.Keyboard::CloseOnInactivity
	bool ___CloseOnInactivity_24;
	// System.Single HoloToolkit.UI.Keyboard.Keyboard::CloseOnInactivityTime
	float ___CloseOnInactivityTime_25;
	// System.Single HoloToolkit.UI.Keyboard.Keyboard::_closingTime
	float ____closingTime_26;
	// System.Action`1<System.Boolean> HoloToolkit.UI.Keyboard.Keyboard::OnKeyboardShifted
	Action_1_t269755560 * ___OnKeyboardShifted_27;
	// System.Boolean HoloToolkit.UI.Keyboard.Keyboard::m_IsShifted
	bool ___m_IsShifted_28;
	// System.Boolean HoloToolkit.UI.Keyboard.Keyboard::m_IsCapslocked
	bool ___m_IsCapslocked_29;
	// System.Int32 HoloToolkit.UI.Keyboard.Keyboard::m_CaretPosition
	int32_t ___m_CaretPosition_30;
	// UnityEngine.Vector3 HoloToolkit.UI.Keyboard.Keyboard::m_StartingScale
	Vector3_t3722313464  ___m_StartingScale_31;
	// UnityEngine.Vector3 HoloToolkit.UI.Keyboard.Keyboard::m_ObjectBounds
	Vector3_t3722313464  ___m_ObjectBounds_32;
	// UnityEngine.Color HoloToolkit.UI.Keyboard.Keyboard::_defaultColor
	Color_t2555686324  ____defaultColor_33;
	// UnityEngine.UI.Image HoloToolkit.UI.Keyboard.Keyboard::_recordImage
	Image_t2670269651 * ____recordImage_34;
	// UnityEngine.AudioSource HoloToolkit.UI.Keyboard.Keyboard::_audioSource
	AudioSource_t3935305588 * ____audioSource_35;

public:
	inline static int32_t get_offset_of_OnTextSubmitted_4() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___OnTextSubmitted_4)); }
	inline EventHandler_t1348719766 * get_OnTextSubmitted_4() const { return ___OnTextSubmitted_4; }
	inline EventHandler_t1348719766 ** get_address_of_OnTextSubmitted_4() { return &___OnTextSubmitted_4; }
	inline void set_OnTextSubmitted_4(EventHandler_t1348719766 * value)
	{
		___OnTextSubmitted_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnTextSubmitted_4), value);
	}

	inline static int32_t get_offset_of_OnTextUpdated_5() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___OnTextUpdated_5)); }
	inline Action_1_t2019918284 * get_OnTextUpdated_5() const { return ___OnTextUpdated_5; }
	inline Action_1_t2019918284 ** get_address_of_OnTextUpdated_5() { return &___OnTextUpdated_5; }
	inline void set_OnTextUpdated_5(Action_1_t2019918284 * value)
	{
		___OnTextUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnTextUpdated_5), value);
	}

	inline static int32_t get_offset_of_OnClosed_6() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___OnClosed_6)); }
	inline EventHandler_t1348719766 * get_OnClosed_6() const { return ___OnClosed_6; }
	inline EventHandler_t1348719766 ** get_address_of_OnClosed_6() { return &___OnClosed_6; }
	inline void set_OnClosed_6(EventHandler_t1348719766 * value)
	{
		___OnClosed_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnClosed_6), value);
	}

	inline static int32_t get_offset_of_OnPrevious_7() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___OnPrevious_7)); }
	inline EventHandler_t1348719766 * get_OnPrevious_7() const { return ___OnPrevious_7; }
	inline EventHandler_t1348719766 ** get_address_of_OnPrevious_7() { return &___OnPrevious_7; }
	inline void set_OnPrevious_7(EventHandler_t1348719766 * value)
	{
		___OnPrevious_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnPrevious_7), value);
	}

	inline static int32_t get_offset_of_OnNext_8() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___OnNext_8)); }
	inline EventHandler_t1348719766 * get_OnNext_8() const { return ___OnNext_8; }
	inline EventHandler_t1348719766 ** get_address_of_OnNext_8() { return &___OnNext_8; }
	inline void set_OnNext_8(EventHandler_t1348719766 * value)
	{
		___OnNext_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnNext_8), value);
	}

	inline static int32_t get_offset_of_OnPlacement_9() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___OnPlacement_9)); }
	inline EventHandler_t1348719766 * get_OnPlacement_9() const { return ___OnPlacement_9; }
	inline EventHandler_t1348719766 ** get_address_of_OnPlacement_9() { return &___OnPlacement_9; }
	inline void set_OnPlacement_9(EventHandler_t1348719766 * value)
	{
		___OnPlacement_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnPlacement_9), value);
	}

	inline static int32_t get_offset_of_InputField_10() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___InputField_10)); }
	inline InputField_t3762917431 * get_InputField_10() const { return ___InputField_10; }
	inline InputField_t3762917431 ** get_address_of_InputField_10() { return &___InputField_10; }
	inline void set_InputField_10(InputField_t3762917431 * value)
	{
		___InputField_10 = value;
		Il2CppCodeGenWriteBarrier((&___InputField_10), value);
	}

	inline static int32_t get_offset_of_InputFieldSlide_11() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___InputFieldSlide_11)); }
	inline AxisSlider_t300566400 * get_InputFieldSlide_11() const { return ___InputFieldSlide_11; }
	inline AxisSlider_t300566400 ** get_address_of_InputFieldSlide_11() { return &___InputFieldSlide_11; }
	inline void set_InputFieldSlide_11(AxisSlider_t300566400 * value)
	{
		___InputFieldSlide_11 = value;
		Il2CppCodeGenWriteBarrier((&___InputFieldSlide_11), value);
	}

	inline static int32_t get_offset_of_SliderEnabled_12() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___SliderEnabled_12)); }
	inline bool get_SliderEnabled_12() const { return ___SliderEnabled_12; }
	inline bool* get_address_of_SliderEnabled_12() { return &___SliderEnabled_12; }
	inline void set_SliderEnabled_12(bool value)
	{
		___SliderEnabled_12 = value;
	}

	inline static int32_t get_offset_of_SubmitOnEnter_13() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___SubmitOnEnter_13)); }
	inline bool get_SubmitOnEnter_13() const { return ___SubmitOnEnter_13; }
	inline bool* get_address_of_SubmitOnEnter_13() { return &___SubmitOnEnter_13; }
	inline void set_SubmitOnEnter_13(bool value)
	{
		___SubmitOnEnter_13 = value;
	}

	inline static int32_t get_offset_of_AlphaKeyboard_14() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___AlphaKeyboard_14)); }
	inline Image_t2670269651 * get_AlphaKeyboard_14() const { return ___AlphaKeyboard_14; }
	inline Image_t2670269651 ** get_address_of_AlphaKeyboard_14() { return &___AlphaKeyboard_14; }
	inline void set_AlphaKeyboard_14(Image_t2670269651 * value)
	{
		___AlphaKeyboard_14 = value;
		Il2CppCodeGenWriteBarrier((&___AlphaKeyboard_14), value);
	}

	inline static int32_t get_offset_of_SymbolKeyboard_15() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___SymbolKeyboard_15)); }
	inline Image_t2670269651 * get_SymbolKeyboard_15() const { return ___SymbolKeyboard_15; }
	inline Image_t2670269651 ** get_address_of_SymbolKeyboard_15() { return &___SymbolKeyboard_15; }
	inline void set_SymbolKeyboard_15(Image_t2670269651 * value)
	{
		___SymbolKeyboard_15 = value;
		Il2CppCodeGenWriteBarrier((&___SymbolKeyboard_15), value);
	}

	inline static int32_t get_offset_of_AlphaSubKeys_16() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___AlphaSubKeys_16)); }
	inline Image_t2670269651 * get_AlphaSubKeys_16() const { return ___AlphaSubKeys_16; }
	inline Image_t2670269651 ** get_address_of_AlphaSubKeys_16() { return &___AlphaSubKeys_16; }
	inline void set_AlphaSubKeys_16(Image_t2670269651 * value)
	{
		___AlphaSubKeys_16 = value;
		Il2CppCodeGenWriteBarrier((&___AlphaSubKeys_16), value);
	}

	inline static int32_t get_offset_of_AlphaWebKeys_17() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___AlphaWebKeys_17)); }
	inline Image_t2670269651 * get_AlphaWebKeys_17() const { return ___AlphaWebKeys_17; }
	inline Image_t2670269651 ** get_address_of_AlphaWebKeys_17() { return &___AlphaWebKeys_17; }
	inline void set_AlphaWebKeys_17(Image_t2670269651 * value)
	{
		___AlphaWebKeys_17 = value;
		Il2CppCodeGenWriteBarrier((&___AlphaWebKeys_17), value);
	}

	inline static int32_t get_offset_of_AlphaMailKeys_18() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___AlphaMailKeys_18)); }
	inline Image_t2670269651 * get_AlphaMailKeys_18() const { return ___AlphaMailKeys_18; }
	inline Image_t2670269651 ** get_address_of_AlphaMailKeys_18() { return &___AlphaMailKeys_18; }
	inline void set_AlphaMailKeys_18(Image_t2670269651 * value)
	{
		___AlphaMailKeys_18 = value;
		Il2CppCodeGenWriteBarrier((&___AlphaMailKeys_18), value);
	}

	inline static int32_t get_offset_of_m_LastKeyboardLayout_19() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___m_LastKeyboardLayout_19)); }
	inline int32_t get_m_LastKeyboardLayout_19() const { return ___m_LastKeyboardLayout_19; }
	inline int32_t* get_address_of_m_LastKeyboardLayout_19() { return &___m_LastKeyboardLayout_19; }
	inline void set_m_LastKeyboardLayout_19(int32_t value)
	{
		___m_LastKeyboardLayout_19 = value;
	}

	inline static int32_t get_offset_of_m_MaxScale_20() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___m_MaxScale_20)); }
	inline float get_m_MaxScale_20() const { return ___m_MaxScale_20; }
	inline float* get_address_of_m_MaxScale_20() { return &___m_MaxScale_20; }
	inline void set_m_MaxScale_20(float value)
	{
		___m_MaxScale_20 = value;
	}

	inline static int32_t get_offset_of_m_MinScale_21() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___m_MinScale_21)); }
	inline float get_m_MinScale_21() const { return ___m_MinScale_21; }
	inline float* get_address_of_m_MinScale_21() { return &___m_MinScale_21; }
	inline void set_m_MinScale_21(float value)
	{
		___m_MinScale_21 = value;
	}

	inline static int32_t get_offset_of_m_MaxDistance_22() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___m_MaxDistance_22)); }
	inline float get_m_MaxDistance_22() const { return ___m_MaxDistance_22; }
	inline float* get_address_of_m_MaxDistance_22() { return &___m_MaxDistance_22; }
	inline void set_m_MaxDistance_22(float value)
	{
		___m_MaxDistance_22 = value;
	}

	inline static int32_t get_offset_of_m_MinDistance_23() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___m_MinDistance_23)); }
	inline float get_m_MinDistance_23() const { return ___m_MinDistance_23; }
	inline float* get_address_of_m_MinDistance_23() { return &___m_MinDistance_23; }
	inline void set_m_MinDistance_23(float value)
	{
		___m_MinDistance_23 = value;
	}

	inline static int32_t get_offset_of_CloseOnInactivity_24() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___CloseOnInactivity_24)); }
	inline bool get_CloseOnInactivity_24() const { return ___CloseOnInactivity_24; }
	inline bool* get_address_of_CloseOnInactivity_24() { return &___CloseOnInactivity_24; }
	inline void set_CloseOnInactivity_24(bool value)
	{
		___CloseOnInactivity_24 = value;
	}

	inline static int32_t get_offset_of_CloseOnInactivityTime_25() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___CloseOnInactivityTime_25)); }
	inline float get_CloseOnInactivityTime_25() const { return ___CloseOnInactivityTime_25; }
	inline float* get_address_of_CloseOnInactivityTime_25() { return &___CloseOnInactivityTime_25; }
	inline void set_CloseOnInactivityTime_25(float value)
	{
		___CloseOnInactivityTime_25 = value;
	}

	inline static int32_t get_offset_of__closingTime_26() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ____closingTime_26)); }
	inline float get__closingTime_26() const { return ____closingTime_26; }
	inline float* get_address_of__closingTime_26() { return &____closingTime_26; }
	inline void set__closingTime_26(float value)
	{
		____closingTime_26 = value;
	}

	inline static int32_t get_offset_of_OnKeyboardShifted_27() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___OnKeyboardShifted_27)); }
	inline Action_1_t269755560 * get_OnKeyboardShifted_27() const { return ___OnKeyboardShifted_27; }
	inline Action_1_t269755560 ** get_address_of_OnKeyboardShifted_27() { return &___OnKeyboardShifted_27; }
	inline void set_OnKeyboardShifted_27(Action_1_t269755560 * value)
	{
		___OnKeyboardShifted_27 = value;
		Il2CppCodeGenWriteBarrier((&___OnKeyboardShifted_27), value);
	}

	inline static int32_t get_offset_of_m_IsShifted_28() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___m_IsShifted_28)); }
	inline bool get_m_IsShifted_28() const { return ___m_IsShifted_28; }
	inline bool* get_address_of_m_IsShifted_28() { return &___m_IsShifted_28; }
	inline void set_m_IsShifted_28(bool value)
	{
		___m_IsShifted_28 = value;
	}

	inline static int32_t get_offset_of_m_IsCapslocked_29() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___m_IsCapslocked_29)); }
	inline bool get_m_IsCapslocked_29() const { return ___m_IsCapslocked_29; }
	inline bool* get_address_of_m_IsCapslocked_29() { return &___m_IsCapslocked_29; }
	inline void set_m_IsCapslocked_29(bool value)
	{
		___m_IsCapslocked_29 = value;
	}

	inline static int32_t get_offset_of_m_CaretPosition_30() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___m_CaretPosition_30)); }
	inline int32_t get_m_CaretPosition_30() const { return ___m_CaretPosition_30; }
	inline int32_t* get_address_of_m_CaretPosition_30() { return &___m_CaretPosition_30; }
	inline void set_m_CaretPosition_30(int32_t value)
	{
		___m_CaretPosition_30 = value;
	}

	inline static int32_t get_offset_of_m_StartingScale_31() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___m_StartingScale_31)); }
	inline Vector3_t3722313464  get_m_StartingScale_31() const { return ___m_StartingScale_31; }
	inline Vector3_t3722313464 * get_address_of_m_StartingScale_31() { return &___m_StartingScale_31; }
	inline void set_m_StartingScale_31(Vector3_t3722313464  value)
	{
		___m_StartingScale_31 = value;
	}

	inline static int32_t get_offset_of_m_ObjectBounds_32() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ___m_ObjectBounds_32)); }
	inline Vector3_t3722313464  get_m_ObjectBounds_32() const { return ___m_ObjectBounds_32; }
	inline Vector3_t3722313464 * get_address_of_m_ObjectBounds_32() { return &___m_ObjectBounds_32; }
	inline void set_m_ObjectBounds_32(Vector3_t3722313464  value)
	{
		___m_ObjectBounds_32 = value;
	}

	inline static int32_t get_offset_of__defaultColor_33() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ____defaultColor_33)); }
	inline Color_t2555686324  get__defaultColor_33() const { return ____defaultColor_33; }
	inline Color_t2555686324 * get_address_of__defaultColor_33() { return &____defaultColor_33; }
	inline void set__defaultColor_33(Color_t2555686324  value)
	{
		____defaultColor_33 = value;
	}

	inline static int32_t get_offset_of__recordImage_34() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ____recordImage_34)); }
	inline Image_t2670269651 * get__recordImage_34() const { return ____recordImage_34; }
	inline Image_t2670269651 ** get_address_of__recordImage_34() { return &____recordImage_34; }
	inline void set__recordImage_34(Image_t2670269651 * value)
	{
		____recordImage_34 = value;
		Il2CppCodeGenWriteBarrier((&____recordImage_34), value);
	}

	inline static int32_t get_offset_of__audioSource_35() { return static_cast<int32_t>(offsetof(Keyboard_t4047957915, ____audioSource_35)); }
	inline AudioSource_t3935305588 * get__audioSource_35() const { return ____audioSource_35; }
	inline AudioSource_t3935305588 ** get_address_of__audioSource_35() { return &____audioSource_35; }
	inline void set__audioSource_35(AudioSource_t3935305588 * value)
	{
		____audioSource_35 = value;
		Il2CppCodeGenWriteBarrier((&____audioSource_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARD_T4047957915_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_3)); }
	inline Navigation_t3049316579  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t3049316579  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_5)); }
	inline ColorBlock_t2139031574  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2139031574  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_6)); }
	inline SpriteState_t1362986479  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1362986479  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_9)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_15)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_2)); }
	inline List_1_t427135887 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t427135887 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t427135887 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef INPUTFIELD_T3762917431_H
#define INPUTFIELD_T3762917431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField
struct  InputField_t3762917431  : public Selectable_t3250028441
{
public:
	// UnityEngine.TouchScreenKeyboard UnityEngine.UI.InputField::m_Keyboard
	TouchScreenKeyboard_t731888065 * ___m_Keyboard_16;
	// UnityEngine.UI.Text UnityEngine.UI.InputField::m_TextComponent
	Text_t1901882714 * ___m_TextComponent_18;
	// UnityEngine.UI.Graphic UnityEngine.UI.InputField::m_Placeholder
	Graphic_t1660335611 * ___m_Placeholder_19;
	// UnityEngine.UI.InputField/ContentType UnityEngine.UI.InputField::m_ContentType
	int32_t ___m_ContentType_20;
	// UnityEngine.UI.InputField/InputType UnityEngine.UI.InputField::m_InputType
	int32_t ___m_InputType_21;
	// System.Char UnityEngine.UI.InputField::m_AsteriskChar
	Il2CppChar ___m_AsteriskChar_22;
	// UnityEngine.TouchScreenKeyboardType UnityEngine.UI.InputField::m_KeyboardType
	int32_t ___m_KeyboardType_23;
	// UnityEngine.UI.InputField/LineType UnityEngine.UI.InputField::m_LineType
	int32_t ___m_LineType_24;
	// System.Boolean UnityEngine.UI.InputField::m_HideMobileInput
	bool ___m_HideMobileInput_25;
	// UnityEngine.UI.InputField/CharacterValidation UnityEngine.UI.InputField::m_CharacterValidation
	int32_t ___m_CharacterValidation_26;
	// System.Int32 UnityEngine.UI.InputField::m_CharacterLimit
	int32_t ___m_CharacterLimit_27;
	// UnityEngine.UI.InputField/SubmitEvent UnityEngine.UI.InputField::m_OnEndEdit
	SubmitEvent_t648412432 * ___m_OnEndEdit_28;
	// UnityEngine.UI.InputField/OnChangeEvent UnityEngine.UI.InputField::m_OnValueChanged
	OnChangeEvent_t467195904 * ___m_OnValueChanged_29;
	// UnityEngine.UI.InputField/OnValidateInput UnityEngine.UI.InputField::m_OnValidateInput
	OnValidateInput_t2355412304 * ___m_OnValidateInput_30;
	// UnityEngine.Color UnityEngine.UI.InputField::m_CaretColor
	Color_t2555686324  ___m_CaretColor_31;
	// System.Boolean UnityEngine.UI.InputField::m_CustomCaretColor
	bool ___m_CustomCaretColor_32;
	// UnityEngine.Color UnityEngine.UI.InputField::m_SelectionColor
	Color_t2555686324  ___m_SelectionColor_33;
	// System.String UnityEngine.UI.InputField::m_Text
	String_t* ___m_Text_34;
	// System.Single UnityEngine.UI.InputField::m_CaretBlinkRate
	float ___m_CaretBlinkRate_35;
	// System.Int32 UnityEngine.UI.InputField::m_CaretWidth
	int32_t ___m_CaretWidth_36;
	// System.Boolean UnityEngine.UI.InputField::m_ReadOnly
	bool ___m_ReadOnly_37;
	// System.Int32 UnityEngine.UI.InputField::m_CaretPosition
	int32_t ___m_CaretPosition_38;
	// System.Int32 UnityEngine.UI.InputField::m_CaretSelectPosition
	int32_t ___m_CaretSelectPosition_39;
	// UnityEngine.RectTransform UnityEngine.UI.InputField::caretRectTrans
	RectTransform_t3704657025 * ___caretRectTrans_40;
	// UnityEngine.UIVertex[] UnityEngine.UI.InputField::m_CursorVerts
	UIVertexU5BU5D_t1981460040* ___m_CursorVerts_41;
	// UnityEngine.TextGenerator UnityEngine.UI.InputField::m_InputTextCache
	TextGenerator_t3211863866 * ___m_InputTextCache_42;
	// UnityEngine.CanvasRenderer UnityEngine.UI.InputField::m_CachedInputRenderer
	CanvasRenderer_t2598313366 * ___m_CachedInputRenderer_43;
	// System.Boolean UnityEngine.UI.InputField::m_PreventFontCallback
	bool ___m_PreventFontCallback_44;
	// UnityEngine.Mesh UnityEngine.UI.InputField::m_Mesh
	Mesh_t3648964284 * ___m_Mesh_45;
	// System.Boolean UnityEngine.UI.InputField::m_AllowInput
	bool ___m_AllowInput_46;
	// System.Boolean UnityEngine.UI.InputField::m_ShouldActivateNextUpdate
	bool ___m_ShouldActivateNextUpdate_47;
	// System.Boolean UnityEngine.UI.InputField::m_UpdateDrag
	bool ___m_UpdateDrag_48;
	// System.Boolean UnityEngine.UI.InputField::m_DragPositionOutOfBounds
	bool ___m_DragPositionOutOfBounds_49;
	// System.Boolean UnityEngine.UI.InputField::m_CaretVisible
	bool ___m_CaretVisible_52;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_BlinkCoroutine
	Coroutine_t3829159415 * ___m_BlinkCoroutine_53;
	// System.Single UnityEngine.UI.InputField::m_BlinkStartTime
	float ___m_BlinkStartTime_54;
	// System.Int32 UnityEngine.UI.InputField::m_DrawStart
	int32_t ___m_DrawStart_55;
	// System.Int32 UnityEngine.UI.InputField::m_DrawEnd
	int32_t ___m_DrawEnd_56;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_DragCoroutine
	Coroutine_t3829159415 * ___m_DragCoroutine_57;
	// System.String UnityEngine.UI.InputField::m_OriginalText
	String_t* ___m_OriginalText_58;
	// System.Boolean UnityEngine.UI.InputField::m_WasCanceled
	bool ___m_WasCanceled_59;
	// System.Boolean UnityEngine.UI.InputField::m_HasDoneFocusTransition
	bool ___m_HasDoneFocusTransition_60;
	// UnityEngine.Event UnityEngine.UI.InputField::m_ProcessingEvent
	Event_t2956885303 * ___m_ProcessingEvent_62;

public:
	inline static int32_t get_offset_of_m_Keyboard_16() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Keyboard_16)); }
	inline TouchScreenKeyboard_t731888065 * get_m_Keyboard_16() const { return ___m_Keyboard_16; }
	inline TouchScreenKeyboard_t731888065 ** get_address_of_m_Keyboard_16() { return &___m_Keyboard_16; }
	inline void set_m_Keyboard_16(TouchScreenKeyboard_t731888065 * value)
	{
		___m_Keyboard_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Keyboard_16), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_18() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_TextComponent_18)); }
	inline Text_t1901882714 * get_m_TextComponent_18() const { return ___m_TextComponent_18; }
	inline Text_t1901882714 ** get_address_of_m_TextComponent_18() { return &___m_TextComponent_18; }
	inline void set_m_TextComponent_18(Text_t1901882714 * value)
	{
		___m_TextComponent_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_18), value);
	}

	inline static int32_t get_offset_of_m_Placeholder_19() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Placeholder_19)); }
	inline Graphic_t1660335611 * get_m_Placeholder_19() const { return ___m_Placeholder_19; }
	inline Graphic_t1660335611 ** get_address_of_m_Placeholder_19() { return &___m_Placeholder_19; }
	inline void set_m_Placeholder_19(Graphic_t1660335611 * value)
	{
		___m_Placeholder_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_Placeholder_19), value);
	}

	inline static int32_t get_offset_of_m_ContentType_20() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ContentType_20)); }
	inline int32_t get_m_ContentType_20() const { return ___m_ContentType_20; }
	inline int32_t* get_address_of_m_ContentType_20() { return &___m_ContentType_20; }
	inline void set_m_ContentType_20(int32_t value)
	{
		___m_ContentType_20 = value;
	}

	inline static int32_t get_offset_of_m_InputType_21() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_InputType_21)); }
	inline int32_t get_m_InputType_21() const { return ___m_InputType_21; }
	inline int32_t* get_address_of_m_InputType_21() { return &___m_InputType_21; }
	inline void set_m_InputType_21(int32_t value)
	{
		___m_InputType_21 = value;
	}

	inline static int32_t get_offset_of_m_AsteriskChar_22() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_AsteriskChar_22)); }
	inline Il2CppChar get_m_AsteriskChar_22() const { return ___m_AsteriskChar_22; }
	inline Il2CppChar* get_address_of_m_AsteriskChar_22() { return &___m_AsteriskChar_22; }
	inline void set_m_AsteriskChar_22(Il2CppChar value)
	{
		___m_AsteriskChar_22 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardType_23() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_KeyboardType_23)); }
	inline int32_t get_m_KeyboardType_23() const { return ___m_KeyboardType_23; }
	inline int32_t* get_address_of_m_KeyboardType_23() { return &___m_KeyboardType_23; }
	inline void set_m_KeyboardType_23(int32_t value)
	{
		___m_KeyboardType_23 = value;
	}

	inline static int32_t get_offset_of_m_LineType_24() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_LineType_24)); }
	inline int32_t get_m_LineType_24() const { return ___m_LineType_24; }
	inline int32_t* get_address_of_m_LineType_24() { return &___m_LineType_24; }
	inline void set_m_LineType_24(int32_t value)
	{
		___m_LineType_24 = value;
	}

	inline static int32_t get_offset_of_m_HideMobileInput_25() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_HideMobileInput_25)); }
	inline bool get_m_HideMobileInput_25() const { return ___m_HideMobileInput_25; }
	inline bool* get_address_of_m_HideMobileInput_25() { return &___m_HideMobileInput_25; }
	inline void set_m_HideMobileInput_25(bool value)
	{
		___m_HideMobileInput_25 = value;
	}

	inline static int32_t get_offset_of_m_CharacterValidation_26() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CharacterValidation_26)); }
	inline int32_t get_m_CharacterValidation_26() const { return ___m_CharacterValidation_26; }
	inline int32_t* get_address_of_m_CharacterValidation_26() { return &___m_CharacterValidation_26; }
	inline void set_m_CharacterValidation_26(int32_t value)
	{
		___m_CharacterValidation_26 = value;
	}

	inline static int32_t get_offset_of_m_CharacterLimit_27() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CharacterLimit_27)); }
	inline int32_t get_m_CharacterLimit_27() const { return ___m_CharacterLimit_27; }
	inline int32_t* get_address_of_m_CharacterLimit_27() { return &___m_CharacterLimit_27; }
	inline void set_m_CharacterLimit_27(int32_t value)
	{
		___m_CharacterLimit_27 = value;
	}

	inline static int32_t get_offset_of_m_OnEndEdit_28() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OnEndEdit_28)); }
	inline SubmitEvent_t648412432 * get_m_OnEndEdit_28() const { return ___m_OnEndEdit_28; }
	inline SubmitEvent_t648412432 ** get_address_of_m_OnEndEdit_28() { return &___m_OnEndEdit_28; }
	inline void set_m_OnEndEdit_28(SubmitEvent_t648412432 * value)
	{
		___m_OnEndEdit_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnEndEdit_28), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_29() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OnValueChanged_29)); }
	inline OnChangeEvent_t467195904 * get_m_OnValueChanged_29() const { return ___m_OnValueChanged_29; }
	inline OnChangeEvent_t467195904 ** get_address_of_m_OnValueChanged_29() { return &___m_OnValueChanged_29; }
	inline void set_m_OnValueChanged_29(OnChangeEvent_t467195904 * value)
	{
		___m_OnValueChanged_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_29), value);
	}

	inline static int32_t get_offset_of_m_OnValidateInput_30() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OnValidateInput_30)); }
	inline OnValidateInput_t2355412304 * get_m_OnValidateInput_30() const { return ___m_OnValidateInput_30; }
	inline OnValidateInput_t2355412304 ** get_address_of_m_OnValidateInput_30() { return &___m_OnValidateInput_30; }
	inline void set_m_OnValidateInput_30(OnValidateInput_t2355412304 * value)
	{
		___m_OnValidateInput_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValidateInput_30), value);
	}

	inline static int32_t get_offset_of_m_CaretColor_31() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretColor_31)); }
	inline Color_t2555686324  get_m_CaretColor_31() const { return ___m_CaretColor_31; }
	inline Color_t2555686324 * get_address_of_m_CaretColor_31() { return &___m_CaretColor_31; }
	inline void set_m_CaretColor_31(Color_t2555686324  value)
	{
		___m_CaretColor_31 = value;
	}

	inline static int32_t get_offset_of_m_CustomCaretColor_32() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CustomCaretColor_32)); }
	inline bool get_m_CustomCaretColor_32() const { return ___m_CustomCaretColor_32; }
	inline bool* get_address_of_m_CustomCaretColor_32() { return &___m_CustomCaretColor_32; }
	inline void set_m_CustomCaretColor_32(bool value)
	{
		___m_CustomCaretColor_32 = value;
	}

	inline static int32_t get_offset_of_m_SelectionColor_33() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_SelectionColor_33)); }
	inline Color_t2555686324  get_m_SelectionColor_33() const { return ___m_SelectionColor_33; }
	inline Color_t2555686324 * get_address_of_m_SelectionColor_33() { return &___m_SelectionColor_33; }
	inline void set_m_SelectionColor_33(Color_t2555686324  value)
	{
		___m_SelectionColor_33 = value;
	}

	inline static int32_t get_offset_of_m_Text_34() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Text_34)); }
	inline String_t* get_m_Text_34() const { return ___m_Text_34; }
	inline String_t** get_address_of_m_Text_34() { return &___m_Text_34; }
	inline void set_m_Text_34(String_t* value)
	{
		___m_Text_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_34), value);
	}

	inline static int32_t get_offset_of_m_CaretBlinkRate_35() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretBlinkRate_35)); }
	inline float get_m_CaretBlinkRate_35() const { return ___m_CaretBlinkRate_35; }
	inline float* get_address_of_m_CaretBlinkRate_35() { return &___m_CaretBlinkRate_35; }
	inline void set_m_CaretBlinkRate_35(float value)
	{
		___m_CaretBlinkRate_35 = value;
	}

	inline static int32_t get_offset_of_m_CaretWidth_36() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretWidth_36)); }
	inline int32_t get_m_CaretWidth_36() const { return ___m_CaretWidth_36; }
	inline int32_t* get_address_of_m_CaretWidth_36() { return &___m_CaretWidth_36; }
	inline void set_m_CaretWidth_36(int32_t value)
	{
		___m_CaretWidth_36 = value;
	}

	inline static int32_t get_offset_of_m_ReadOnly_37() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ReadOnly_37)); }
	inline bool get_m_ReadOnly_37() const { return ___m_ReadOnly_37; }
	inline bool* get_address_of_m_ReadOnly_37() { return &___m_ReadOnly_37; }
	inline void set_m_ReadOnly_37(bool value)
	{
		___m_ReadOnly_37 = value;
	}

	inline static int32_t get_offset_of_m_CaretPosition_38() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretPosition_38)); }
	inline int32_t get_m_CaretPosition_38() const { return ___m_CaretPosition_38; }
	inline int32_t* get_address_of_m_CaretPosition_38() { return &___m_CaretPosition_38; }
	inline void set_m_CaretPosition_38(int32_t value)
	{
		___m_CaretPosition_38 = value;
	}

	inline static int32_t get_offset_of_m_CaretSelectPosition_39() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretSelectPosition_39)); }
	inline int32_t get_m_CaretSelectPosition_39() const { return ___m_CaretSelectPosition_39; }
	inline int32_t* get_address_of_m_CaretSelectPosition_39() { return &___m_CaretSelectPosition_39; }
	inline void set_m_CaretSelectPosition_39(int32_t value)
	{
		___m_CaretSelectPosition_39 = value;
	}

	inline static int32_t get_offset_of_caretRectTrans_40() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___caretRectTrans_40)); }
	inline RectTransform_t3704657025 * get_caretRectTrans_40() const { return ___caretRectTrans_40; }
	inline RectTransform_t3704657025 ** get_address_of_caretRectTrans_40() { return &___caretRectTrans_40; }
	inline void set_caretRectTrans_40(RectTransform_t3704657025 * value)
	{
		___caretRectTrans_40 = value;
		Il2CppCodeGenWriteBarrier((&___caretRectTrans_40), value);
	}

	inline static int32_t get_offset_of_m_CursorVerts_41() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CursorVerts_41)); }
	inline UIVertexU5BU5D_t1981460040* get_m_CursorVerts_41() const { return ___m_CursorVerts_41; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_CursorVerts_41() { return &___m_CursorVerts_41; }
	inline void set_m_CursorVerts_41(UIVertexU5BU5D_t1981460040* value)
	{
		___m_CursorVerts_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_CursorVerts_41), value);
	}

	inline static int32_t get_offset_of_m_InputTextCache_42() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_InputTextCache_42)); }
	inline TextGenerator_t3211863866 * get_m_InputTextCache_42() const { return ___m_InputTextCache_42; }
	inline TextGenerator_t3211863866 ** get_address_of_m_InputTextCache_42() { return &___m_InputTextCache_42; }
	inline void set_m_InputTextCache_42(TextGenerator_t3211863866 * value)
	{
		___m_InputTextCache_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputTextCache_42), value);
	}

	inline static int32_t get_offset_of_m_CachedInputRenderer_43() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CachedInputRenderer_43)); }
	inline CanvasRenderer_t2598313366 * get_m_CachedInputRenderer_43() const { return ___m_CachedInputRenderer_43; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CachedInputRenderer_43() { return &___m_CachedInputRenderer_43; }
	inline void set_m_CachedInputRenderer_43(CanvasRenderer_t2598313366 * value)
	{
		___m_CachedInputRenderer_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedInputRenderer_43), value);
	}

	inline static int32_t get_offset_of_m_PreventFontCallback_44() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_PreventFontCallback_44)); }
	inline bool get_m_PreventFontCallback_44() const { return ___m_PreventFontCallback_44; }
	inline bool* get_address_of_m_PreventFontCallback_44() { return &___m_PreventFontCallback_44; }
	inline void set_m_PreventFontCallback_44(bool value)
	{
		___m_PreventFontCallback_44 = value;
	}

	inline static int32_t get_offset_of_m_Mesh_45() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Mesh_45)); }
	inline Mesh_t3648964284 * get_m_Mesh_45() const { return ___m_Mesh_45; }
	inline Mesh_t3648964284 ** get_address_of_m_Mesh_45() { return &___m_Mesh_45; }
	inline void set_m_Mesh_45(Mesh_t3648964284 * value)
	{
		___m_Mesh_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mesh_45), value);
	}

	inline static int32_t get_offset_of_m_AllowInput_46() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_AllowInput_46)); }
	inline bool get_m_AllowInput_46() const { return ___m_AllowInput_46; }
	inline bool* get_address_of_m_AllowInput_46() { return &___m_AllowInput_46; }
	inline void set_m_AllowInput_46(bool value)
	{
		___m_AllowInput_46 = value;
	}

	inline static int32_t get_offset_of_m_ShouldActivateNextUpdate_47() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ShouldActivateNextUpdate_47)); }
	inline bool get_m_ShouldActivateNextUpdate_47() const { return ___m_ShouldActivateNextUpdate_47; }
	inline bool* get_address_of_m_ShouldActivateNextUpdate_47() { return &___m_ShouldActivateNextUpdate_47; }
	inline void set_m_ShouldActivateNextUpdate_47(bool value)
	{
		___m_ShouldActivateNextUpdate_47 = value;
	}

	inline static int32_t get_offset_of_m_UpdateDrag_48() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_UpdateDrag_48)); }
	inline bool get_m_UpdateDrag_48() const { return ___m_UpdateDrag_48; }
	inline bool* get_address_of_m_UpdateDrag_48() { return &___m_UpdateDrag_48; }
	inline void set_m_UpdateDrag_48(bool value)
	{
		___m_UpdateDrag_48 = value;
	}

	inline static int32_t get_offset_of_m_DragPositionOutOfBounds_49() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DragPositionOutOfBounds_49)); }
	inline bool get_m_DragPositionOutOfBounds_49() const { return ___m_DragPositionOutOfBounds_49; }
	inline bool* get_address_of_m_DragPositionOutOfBounds_49() { return &___m_DragPositionOutOfBounds_49; }
	inline void set_m_DragPositionOutOfBounds_49(bool value)
	{
		___m_DragPositionOutOfBounds_49 = value;
	}

	inline static int32_t get_offset_of_m_CaretVisible_52() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretVisible_52)); }
	inline bool get_m_CaretVisible_52() const { return ___m_CaretVisible_52; }
	inline bool* get_address_of_m_CaretVisible_52() { return &___m_CaretVisible_52; }
	inline void set_m_CaretVisible_52(bool value)
	{
		___m_CaretVisible_52 = value;
	}

	inline static int32_t get_offset_of_m_BlinkCoroutine_53() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_BlinkCoroutine_53)); }
	inline Coroutine_t3829159415 * get_m_BlinkCoroutine_53() const { return ___m_BlinkCoroutine_53; }
	inline Coroutine_t3829159415 ** get_address_of_m_BlinkCoroutine_53() { return &___m_BlinkCoroutine_53; }
	inline void set_m_BlinkCoroutine_53(Coroutine_t3829159415 * value)
	{
		___m_BlinkCoroutine_53 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlinkCoroutine_53), value);
	}

	inline static int32_t get_offset_of_m_BlinkStartTime_54() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_BlinkStartTime_54)); }
	inline float get_m_BlinkStartTime_54() const { return ___m_BlinkStartTime_54; }
	inline float* get_address_of_m_BlinkStartTime_54() { return &___m_BlinkStartTime_54; }
	inline void set_m_BlinkStartTime_54(float value)
	{
		___m_BlinkStartTime_54 = value;
	}

	inline static int32_t get_offset_of_m_DrawStart_55() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DrawStart_55)); }
	inline int32_t get_m_DrawStart_55() const { return ___m_DrawStart_55; }
	inline int32_t* get_address_of_m_DrawStart_55() { return &___m_DrawStart_55; }
	inline void set_m_DrawStart_55(int32_t value)
	{
		___m_DrawStart_55 = value;
	}

	inline static int32_t get_offset_of_m_DrawEnd_56() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DrawEnd_56)); }
	inline int32_t get_m_DrawEnd_56() const { return ___m_DrawEnd_56; }
	inline int32_t* get_address_of_m_DrawEnd_56() { return &___m_DrawEnd_56; }
	inline void set_m_DrawEnd_56(int32_t value)
	{
		___m_DrawEnd_56 = value;
	}

	inline static int32_t get_offset_of_m_DragCoroutine_57() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DragCoroutine_57)); }
	inline Coroutine_t3829159415 * get_m_DragCoroutine_57() const { return ___m_DragCoroutine_57; }
	inline Coroutine_t3829159415 ** get_address_of_m_DragCoroutine_57() { return &___m_DragCoroutine_57; }
	inline void set_m_DragCoroutine_57(Coroutine_t3829159415 * value)
	{
		___m_DragCoroutine_57 = value;
		Il2CppCodeGenWriteBarrier((&___m_DragCoroutine_57), value);
	}

	inline static int32_t get_offset_of_m_OriginalText_58() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OriginalText_58)); }
	inline String_t* get_m_OriginalText_58() const { return ___m_OriginalText_58; }
	inline String_t** get_address_of_m_OriginalText_58() { return &___m_OriginalText_58; }
	inline void set_m_OriginalText_58(String_t* value)
	{
		___m_OriginalText_58 = value;
		Il2CppCodeGenWriteBarrier((&___m_OriginalText_58), value);
	}

	inline static int32_t get_offset_of_m_WasCanceled_59() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_WasCanceled_59)); }
	inline bool get_m_WasCanceled_59() const { return ___m_WasCanceled_59; }
	inline bool* get_address_of_m_WasCanceled_59() { return &___m_WasCanceled_59; }
	inline void set_m_WasCanceled_59(bool value)
	{
		___m_WasCanceled_59 = value;
	}

	inline static int32_t get_offset_of_m_HasDoneFocusTransition_60() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_HasDoneFocusTransition_60)); }
	inline bool get_m_HasDoneFocusTransition_60() const { return ___m_HasDoneFocusTransition_60; }
	inline bool* get_address_of_m_HasDoneFocusTransition_60() { return &___m_HasDoneFocusTransition_60; }
	inline void set_m_HasDoneFocusTransition_60(bool value)
	{
		___m_HasDoneFocusTransition_60 = value;
	}

	inline static int32_t get_offset_of_m_ProcessingEvent_62() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ProcessingEvent_62)); }
	inline Event_t2956885303 * get_m_ProcessingEvent_62() const { return ___m_ProcessingEvent_62; }
	inline Event_t2956885303 ** get_address_of_m_ProcessingEvent_62() { return &___m_ProcessingEvent_62; }
	inline void set_m_ProcessingEvent_62(Event_t2956885303 * value)
	{
		___m_ProcessingEvent_62 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProcessingEvent_62), value);
	}
};

struct InputField_t3762917431_StaticFields
{
public:
	// System.Char[] UnityEngine.UI.InputField::kSeparators
	CharU5BU5D_t3528271667* ___kSeparators_17;

public:
	inline static int32_t get_offset_of_kSeparators_17() { return static_cast<int32_t>(offsetof(InputField_t3762917431_StaticFields, ___kSeparators_17)); }
	inline CharU5BU5D_t3528271667* get_kSeparators_17() const { return ___kSeparators_17; }
	inline CharU5BU5D_t3528271667** get_address_of_kSeparators_17() { return &___kSeparators_17; }
	inline void set_kSeparators_17(CharU5BU5D_t3528271667* value)
	{
		___kSeparators_17 = value;
		Il2CppCodeGenWriteBarrier((&___kSeparators_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTFIELD_T3762917431_H
#ifndef SLIDERINPUTFIELD_T4235481994_H
#define SLIDERINPUTFIELD_T4235481994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.UI.Keyboard.SliderInputField
struct  SliderInputField_t4235481994  : public InputField_t3762917431
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIDERINPUTFIELD_T4235481994_H
#ifndef KEYBOARDINPUTFIELD_T487708905_H
#define KEYBOARDINPUTFIELD_T487708905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.UI.Keyboard.KeyboardInputField
struct  KeyboardInputField_t487708905  : public InputField_t3762917431
{
public:
	// UnityEngine.Transform HoloToolkit.UI.Keyboard.KeyboardInputField::KeyboardSpawnPoint
	Transform_t3600365921 * ___KeyboardSpawnPoint_64;
	// HoloToolkit.UI.Keyboard.Keyboard/LayoutType HoloToolkit.UI.Keyboard.KeyboardInputField::KeyboardLayout
	int32_t ___KeyboardLayout_65;

public:
	inline static int32_t get_offset_of_KeyboardSpawnPoint_64() { return static_cast<int32_t>(offsetof(KeyboardInputField_t487708905, ___KeyboardSpawnPoint_64)); }
	inline Transform_t3600365921 * get_KeyboardSpawnPoint_64() const { return ___KeyboardSpawnPoint_64; }
	inline Transform_t3600365921 ** get_address_of_KeyboardSpawnPoint_64() { return &___KeyboardSpawnPoint_64; }
	inline void set_KeyboardSpawnPoint_64(Transform_t3600365921 * value)
	{
		___KeyboardSpawnPoint_64 = value;
		Il2CppCodeGenWriteBarrier((&___KeyboardSpawnPoint_64), value);
	}

	inline static int32_t get_offset_of_KeyboardLayout_65() { return static_cast<int32_t>(offsetof(KeyboardInputField_t487708905, ___KeyboardLayout_65)); }
	inline int32_t get_KeyboardLayout_65() const { return ___KeyboardLayout_65; }
	inline int32_t* get_address_of_KeyboardLayout_65() { return &___KeyboardLayout_65; }
	inline void set_KeyboardLayout_65(int32_t value)
	{
		___KeyboardLayout_65 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDINPUTFIELD_T487708905_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5300 = { sizeof (SphereScript_t2867235001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5300[7] = 
{
	SphereScript_t2867235001::get_offset_of_Radius_2(),
	SphereScript_t2867235001::get_offset_of_Detail_3(),
	SphereScript_t2867235001::get_offset_of_IcoSphere_4(),
	SphereScript_t2867235001::get_offset_of_lastRadius_5(),
	SphereScript_t2867235001::get_offset_of_lastDetail_6(),
	SphereScript_t2867235001::get_offset_of_lastIcoSphere_7(),
	SphereScript_t2867235001::get_offset_of_dirty_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5301 = { sizeof (MiddlePointCacheKey_t1252889446)+ sizeof (RuntimeObject), sizeof(MiddlePointCacheKey_t1252889446 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5301[2] = 
{
	MiddlePointCacheKey_t1252889446::get_offset_of_Key1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MiddlePointCacheKey_t1252889446::get_offset_of_Key2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5302 = { sizeof (TriangleIndices_t1683999176)+ sizeof (RuntimeObject), sizeof(TriangleIndices_t1683999176 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5302[3] = 
{
	TriangleIndices_t1683999176::get_offset_of_v1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TriangleIndices_t1683999176::get_offset_of_v2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TriangleIndices_t1683999176::get_offset_of_v3_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5303 = { sizeof (ReflectionExtensions_t4058370115), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5304 = { sizeof (U3CU3Ec__DisplayClass4_0_t1345556967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5304[2] = 
{
	U3CU3Ec__DisplayClass4_0_t1345556967::get_offset_of_methodName_0(),
	U3CU3Ec__DisplayClass4_0_t1345556967::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5305 = { sizeof (U3CU3Ec_t1266415778), -1, sizeof(U3CU3Ec_t1266415778_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5305[2] = 
{
	U3CU3Ec_t1266415778_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1266415778_StaticFields::get_offset_of_U3CU3E9__4_2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5306 = { sizeof (TypeUtils_t3188311306), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5307 = { sizeof (WindowsApiChecker_t3065255428), -1, sizeof(WindowsApiChecker_t3065255428_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5307[3] = 
{
	WindowsApiChecker_t3065255428_StaticFields::get_offset_of_U3CUniversalApiContractV5_IsAvailableU3Ek__BackingField_0(),
	WindowsApiChecker_t3065255428_StaticFields::get_offset_of_U3CUniversalApiContractV4_IsAvailableU3Ek__BackingField_1(),
	WindowsApiChecker_t3065255428_StaticFields::get_offset_of_U3CUniversalApiContractV3_IsAvailableU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5308 = { sizeof (AxisSlider_t300566400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5308[9] = 
{
	AxisSlider_t300566400::get_offset_of_Axis_2(),
	AxisSlider_t300566400::get_offset_of_currentPos_3(),
	AxisSlider_t300566400::get_offset_of_slideVel_4(),
	AxisSlider_t300566400::get_offset_of_slideAccel_5(),
	AxisSlider_t300566400::get_offset_of_slideFriction_6(),
	AxisSlider_t300566400::get_offset_of_deadZone_7(),
	AxisSlider_t300566400::get_offset_of_clampDistance_8(),
	AxisSlider_t300566400::get_offset_of_bounce_9(),
	AxisSlider_t300566400::get_offset_of_TargetPoint_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5309 = { sizeof (EAxis_t17314133)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5309[4] = 
{
	EAxis_t17314133::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5310 = { sizeof (CapsLockHighlight_t2579730708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5310[2] = 
{
	CapsLockHighlight_t2579730708::get_offset_of_m_Highlight_2(),
	CapsLockHighlight_t2579730708::get_offset_of_m_Keyboard_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5311 = { sizeof (Keyboard_t4047957915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5311[32] = 
{
	Keyboard_t4047957915::get_offset_of_OnTextSubmitted_4(),
	Keyboard_t4047957915::get_offset_of_OnTextUpdated_5(),
	Keyboard_t4047957915::get_offset_of_OnClosed_6(),
	Keyboard_t4047957915::get_offset_of_OnPrevious_7(),
	Keyboard_t4047957915::get_offset_of_OnNext_8(),
	Keyboard_t4047957915::get_offset_of_OnPlacement_9(),
	Keyboard_t4047957915::get_offset_of_InputField_10(),
	Keyboard_t4047957915::get_offset_of_InputFieldSlide_11(),
	Keyboard_t4047957915::get_offset_of_SliderEnabled_12(),
	Keyboard_t4047957915::get_offset_of_SubmitOnEnter_13(),
	Keyboard_t4047957915::get_offset_of_AlphaKeyboard_14(),
	Keyboard_t4047957915::get_offset_of_SymbolKeyboard_15(),
	Keyboard_t4047957915::get_offset_of_AlphaSubKeys_16(),
	Keyboard_t4047957915::get_offset_of_AlphaWebKeys_17(),
	Keyboard_t4047957915::get_offset_of_AlphaMailKeys_18(),
	Keyboard_t4047957915::get_offset_of_m_LastKeyboardLayout_19(),
	Keyboard_t4047957915::get_offset_of_m_MaxScale_20(),
	Keyboard_t4047957915::get_offset_of_m_MinScale_21(),
	Keyboard_t4047957915::get_offset_of_m_MaxDistance_22(),
	Keyboard_t4047957915::get_offset_of_m_MinDistance_23(),
	Keyboard_t4047957915::get_offset_of_CloseOnInactivity_24(),
	Keyboard_t4047957915::get_offset_of_CloseOnInactivityTime_25(),
	Keyboard_t4047957915::get_offset_of__closingTime_26(),
	Keyboard_t4047957915::get_offset_of_OnKeyboardShifted_27(),
	Keyboard_t4047957915::get_offset_of_m_IsShifted_28(),
	Keyboard_t4047957915::get_offset_of_m_IsCapslocked_29(),
	Keyboard_t4047957915::get_offset_of_m_CaretPosition_30(),
	Keyboard_t4047957915::get_offset_of_m_StartingScale_31(),
	Keyboard_t4047957915::get_offset_of_m_ObjectBounds_32(),
	Keyboard_t4047957915::get_offset_of__defaultColor_33(),
	Keyboard_t4047957915::get_offset_of__recordImage_34(),
	Keyboard_t4047957915::get_offset_of__audioSource_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5312 = { sizeof (LayoutType_t1707310462)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5312[5] = 
{
	LayoutType_t1707310462::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5313 = { sizeof (U3CU3Ec_t1768260159), -1, sizeof(U3CU3Ec_t1768260159_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5313[8] = 
{
	U3CU3Ec_t1768260159_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1768260159_StaticFields::get_offset_of_U3CU3E9__101_0_1(),
	U3CU3Ec_t1768260159_StaticFields::get_offset_of_U3CU3E9__101_1_2(),
	U3CU3Ec_t1768260159_StaticFields::get_offset_of_U3CU3E9__101_2_3(),
	U3CU3Ec_t1768260159_StaticFields::get_offset_of_U3CU3E9__101_3_4(),
	U3CU3Ec_t1768260159_StaticFields::get_offset_of_U3CU3E9__101_4_5(),
	U3CU3Ec_t1768260159_StaticFields::get_offset_of_U3CU3E9__101_5_6(),
	U3CU3Ec_t1768260159_StaticFields::get_offset_of_U3CU3E9__101_6_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5314 = { sizeof (KeyboardInputField_t487708905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5314[3] = 
{
	KeyboardInputField_t487708905::get_offset_of_KeyboardSpawnPoint_64(),
	KeyboardInputField_t487708905::get_offset_of_KeyboardLayout_65(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5315 = { sizeof (KeyboardKeyFunc_t2343856591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5315[2] = 
{
	KeyboardKeyFunc_t2343856591::get_offset_of_m_ButtonFunction_2(),
	KeyboardKeyFunc_t2343856591::get_offset_of_m_Button_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5316 = { sizeof (Function_t3824847731)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5316[14] = 
{
	Function_t3824847731::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5317 = { sizeof (KeyboardValueKey_t175000019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5317[4] = 
{
	KeyboardValueKey_t175000019::get_offset_of_Value_2(),
	KeyboardValueKey_t175000019::get_offset_of_ShiftValue_3(),
	KeyboardValueKey_t175000019::get_offset_of_m_Text_4(),
	KeyboardValueKey_t175000019::get_offset_of_m_Button_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5318 = { sizeof (SliderInputField_t4235481994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5319 = { sizeof (SymbolDisableHighlight_t3411979291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5319[5] = 
{
	SymbolDisableHighlight_t3411979291::get_offset_of_m_TextField_2(),
	SymbolDisableHighlight_t3411979291::get_offset_of_m_ImageField_3(),
	SymbolDisableHighlight_t3411979291::get_offset_of_m_DisabledColor_4(),
	SymbolDisableHighlight_t3411979291::get_offset_of_m_StartingColor_5(),
	SymbolDisableHighlight_t3411979291::get_offset_of_m_Button_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5320 = { sizeof (SymbolKeyboard_t847850322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5320[2] = 
{
	SymbolKeyboard_t847850322::get_offset_of_m_PageBck_2(),
	SymbolKeyboard_t847850322::get_offset_of_m_PageFwd_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5321 = { sizeof (UICollection_t2513404254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5321[6] = 
{
	UICollection_t2513404254::get_offset_of_MaxWidth_2(),
	UICollection_t2513404254::get_offset_of_MaxHeight_3(),
	UICollection_t2513404254::get_offset_of_HorizontalSpacing_4(),
	UICollection_t2513404254::get_offset_of_VerticalSpacing_5(),
	UICollection_t2513404254::get_offset_of_U3CItemsU3Ek__BackingField_6(),
	UICollection_t2513404254::get_offset_of_rectTransform_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5322 = { sizeof (SessionExtensions_t1740927046), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5323 = { sizeof (AnchorDownloadRequest_t1250893622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5323[2] = 
{
	AnchorDownloadRequest_t1250893622::get_offset_of_swigCPtr_0(),
	AnchorDownloadRequest_t1250893622::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5324 = { sizeof (AudioManager_t2087294725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5324[2] = 
{
	AudioManager_t2087294725::get_offset_of_swigCPtr_0(),
	AudioManager_t2087294725::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5325 = { sizeof (BoolElement_t553673861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5325[1] = 
{
	BoolElement_t553673861::get_offset_of_swigCPtr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5326 = { sizeof (ClientConfig_t2300563953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5326[2] = 
{
	ClientConfig_t2300563953::get_offset_of_swigCPtr_0(),
	ClientConfig_t2300563953::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5327 = { sizeof (ClientRole_t373356971)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5327[4] = 
{
	ClientRole_t373356971::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5328 = { sizeof (DirectPairConnector_t2072515004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5328[1] = 
{
	DirectPairConnector_t2072515004::get_offset_of_swigCPtr_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5329 = { sizeof (DirectPairReceiver_t2582757322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5329[1] = 
{
	DirectPairReceiver_t2582757322::get_offset_of_swigCPtr_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5330 = { sizeof (DiscoveredSystem_t1063958610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5330[2] = 
{
	DiscoveredSystem_t1063958610::get_offset_of_swigCPtr_0(),
	DiscoveredSystem_t1063958610::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5331 = { sizeof (DiscoveryClient_t1530907974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5331[2] = 
{
	DiscoveryClient_t1530907974::get_offset_of_swigCPtr_0(),
	DiscoveryClient_t1530907974::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5332 = { sizeof (DiscoveryClientAdapter_t99005982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5332[2] = 
{
	DiscoveryClientAdapter_t99005982::get_offset_of_DiscoveredEvent_7(),
	DiscoveryClientAdapter_t99005982::get_offset_of_LostEvent_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5333 = { sizeof (DiscoveryClientListener_t1752642716), -1, sizeof(DiscoveryClientListener_t1752642716_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5333[5] = 
{
	DiscoveryClientListener_t1752642716::get_offset_of_swigCPtr_2(),
	DiscoveryClientListener_t1752642716::get_offset_of_swigDelegate0_3(),
	DiscoveryClientListener_t1752642716::get_offset_of_swigDelegate1_4(),
	DiscoveryClientListener_t1752642716_StaticFields::get_offset_of_swigMethodTypes0_5(),
	DiscoveryClientListener_t1752642716_StaticFields::get_offset_of_swigMethodTypes1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5334 = { sizeof (SwigDelegateDiscoveryClientListener_0_t3614305483), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5335 = { sizeof (SwigDelegateDiscoveryClientListener_1_t3614305484), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5336 = { sizeof (DoubleElement_t3393924332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5336[1] = 
{
	DoubleElement_t3393924332::get_offset_of_swigCPtr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5337 = { sizeof (Element_t654894041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5337[2] = 
{
	Element_t654894041::get_offset_of_swigCPtr_0(),
	Element_t654894041::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5338 = { sizeof (ElementType_t2760602766)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5338[13] = 
{
	ElementType_t2760602766::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5339 = { sizeof (FloatArrayElement_t2118589074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5339[1] = 
{
	FloatArrayElement_t2118589074::get_offset_of_swigCPtr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5340 = { sizeof (FloatArrayListener_t612841106), -1, sizeof(FloatArrayListener_t612841106_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5340[7] = 
{
	FloatArrayListener_t612841106::get_offset_of_swigCPtr_2(),
	FloatArrayListener_t612841106::get_offset_of_swigDelegate0_3(),
	FloatArrayListener_t612841106::get_offset_of_swigDelegate1_4(),
	FloatArrayListener_t612841106::get_offset_of_swigDelegate2_5(),
	FloatArrayListener_t612841106_StaticFields::get_offset_of_swigMethodTypes0_6(),
	FloatArrayListener_t612841106_StaticFields::get_offset_of_swigMethodTypes1_7(),
	FloatArrayListener_t612841106_StaticFields::get_offset_of_swigMethodTypes2_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5341 = { sizeof (SwigDelegateFloatArrayListener_0_t2300273156), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5342 = { sizeof (SwigDelegateFloatArrayListener_1_t734189215), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5343 = { sizeof (SwigDelegateFloatArrayListener_2_t1137473742), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5344 = { sizeof (FloatElement_t4176382651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5344[1] = 
{
	FloatElement_t4176382651::get_offset_of_swigCPtr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5345 = { sizeof (IntArrayElement_t3225799893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5345[1] = 
{
	IntArrayElement_t3225799893::get_offset_of_swigCPtr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5346 = { sizeof (IntArrayListener_t4199203735), -1, sizeof(IntArrayListener_t4199203735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5346[7] = 
{
	IntArrayListener_t4199203735::get_offset_of_swigCPtr_2(),
	IntArrayListener_t4199203735::get_offset_of_swigDelegate0_3(),
	IntArrayListener_t4199203735::get_offset_of_swigDelegate1_4(),
	IntArrayListener_t4199203735::get_offset_of_swigDelegate2_5(),
	IntArrayListener_t4199203735_StaticFields::get_offset_of_swigMethodTypes0_6(),
	IntArrayListener_t4199203735_StaticFields::get_offset_of_swigMethodTypes1_7(),
	IntArrayListener_t4199203735_StaticFields::get_offset_of_swigMethodTypes2_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5347 = { sizeof (SwigDelegateIntArrayListener_0_t3309985819), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5348 = { sizeof (SwigDelegateIntArrayListener_1_t1743901878), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5349 = { sizeof (SwigDelegateIntArrayListener_2_t177817937), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5350 = { sizeof (IntElement_t687264645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5350[1] = 
{
	IntElement_t687264645::get_offset_of_swigCPtr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5351 = { sizeof (Listener_t2209039043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5351[2] = 
{
	Listener_t2209039043::get_offset_of_swigCPtr_0(),
	Listener_t2209039043::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5352 = { sizeof (Log_t986242981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5352[2] = 
{
	Log_t986242981::get_offset_of_swigCPtr_0(),
	Log_t986242981::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5353 = { sizeof (LogManager_t3740877303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5353[2] = 
{
	LogManager_t3740877303::get_offset_of_swigCPtr_0(),
	LogManager_t3740877303::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5354 = { sizeof (LogSeverity_t4123842851)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5354[4] = 
{
	LogSeverity_t4123842851::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5355 = { sizeof (LogWriter_t3550065378), -1, sizeof(LogWriter_t3550065378_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5355[4] = 
{
	LogWriter_t3550065378::get_offset_of_swigCPtr_0(),
	LogWriter_t3550065378::get_offset_of_swigCMemOwn_1(),
	LogWriter_t3550065378::get_offset_of_swigDelegate0_2(),
	LogWriter_t3550065378_StaticFields::get_offset_of_swigMethodTypes0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5356 = { sizeof (SwigDelegateLogWriter_0_t50445701), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5357 = { sizeof (LongElement_t2161894770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5357[1] = 
{
	LongElement_t2161894770::get_offset_of_swigCPtr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5358 = { sizeof (MachineSessionState_t745471285)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5358[4] = 
{
	MachineSessionState_t745471285::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5359 = { sizeof (MessageChannel_t22737989)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5359[9] = 
{
	MessageChannel_t22737989::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5360 = { sizeof (MessageID_t254005563)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5360[20] = 
{
	MessageID_t254005563::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5361 = { sizeof (MessagePriority_t2166322227)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5361[5] = 
{
	MessagePriority_t2166322227::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5362 = { sizeof (MessageReliability_t2687606723)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5362[6] = 
{
	MessageReliability_t2687606723::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5363 = { sizeof (NetworkConnection_t1099879691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5363[2] = 
{
	NetworkConnection_t1099879691::get_offset_of_swigCPtr_0(),
	NetworkConnection_t1099879691::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5364 = { sizeof (NetworkConnectionAdapter_t239632668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5364[4] = 
{
	NetworkConnectionAdapter_t239632668::get_offset_of_ConnectedCallback_11(),
	NetworkConnectionAdapter_t239632668::get_offset_of_ConnectionFailedCallback_12(),
	NetworkConnectionAdapter_t239632668::get_offset_of_DisconnectedCallback_13(),
	NetworkConnectionAdapter_t239632668::get_offset_of_MessageReceivedCallback_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5365 = { sizeof (NetworkConnectionListener_t2919586752), -1, sizeof(NetworkConnectionListener_t2919586752_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5365[9] = 
{
	NetworkConnectionListener_t2919586752::get_offset_of_swigCPtr_2(),
	NetworkConnectionListener_t2919586752::get_offset_of_swigDelegate0_3(),
	NetworkConnectionListener_t2919586752::get_offset_of_swigDelegate1_4(),
	NetworkConnectionListener_t2919586752::get_offset_of_swigDelegate2_5(),
	NetworkConnectionListener_t2919586752::get_offset_of_swigDelegate3_6(),
	NetworkConnectionListener_t2919586752_StaticFields::get_offset_of_swigMethodTypes0_7(),
	NetworkConnectionListener_t2919586752_StaticFields::get_offset_of_swigMethodTypes1_8(),
	NetworkConnectionListener_t2919586752_StaticFields::get_offset_of_swigMethodTypes2_9(),
	NetworkConnectionListener_t2919586752_StaticFields::get_offset_of_swigMethodTypes3_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5366 = { sizeof (SwigDelegateNetworkConnectionListener_0_t4021727018), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5367 = { sizeof (SwigDelegateNetworkConnectionListener_1_t4021727017), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5368 = { sizeof (SwigDelegateNetworkConnectionListener_2_t4021727020), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5369 = { sizeof (SwigDelegateNetworkConnectionListener_3_t4021727019), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5370 = { sizeof (NetworkInMessage_t2396015845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5370[2] = 
{
	NetworkInMessage_t2396015845::get_offset_of_swigCPtr_0(),
	NetworkInMessage_t2396015845::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5371 = { sizeof (NetworkOutMessage_t1497576987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5371[2] = 
{
	NetworkOutMessage_t1497576987::get_offset_of_swigCPtr_0(),
	NetworkOutMessage_t1497576987::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5372 = { sizeof (ObjectElement_t3710848331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5372[1] = 
{
	ObjectElement_t3710848331::get_offset_of_swigCPtr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5373 = { sizeof (ObjectElementAdapter_t1935870410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5373[8] = 
{
	ObjectElementAdapter_t1935870410::get_offset_of_BoolChangedEvent_19(),
	ObjectElementAdapter_t1935870410::get_offset_of_IntChangedEvent_20(),
	ObjectElementAdapter_t1935870410::get_offset_of_LongChangedEvent_21(),
	ObjectElementAdapter_t1935870410::get_offset_of_FloatChangedEvent_22(),
	ObjectElementAdapter_t1935870410::get_offset_of_DoubleChangedEvent_23(),
	ObjectElementAdapter_t1935870410::get_offset_of_StringChangedEvent_24(),
	ObjectElementAdapter_t1935870410::get_offset_of_ElementAddedEvent_25(),
	ObjectElementAdapter_t1935870410::get_offset_of_ElementDeletedEvent_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5374 = { sizeof (ObjectElementListener_t389328343), -1, sizeof(ObjectElementListener_t389328343_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5374[17] = 
{
	ObjectElementListener_t389328343::get_offset_of_swigCPtr_2(),
	ObjectElementListener_t389328343::get_offset_of_swigDelegate0_3(),
	ObjectElementListener_t389328343::get_offset_of_swigDelegate1_4(),
	ObjectElementListener_t389328343::get_offset_of_swigDelegate2_5(),
	ObjectElementListener_t389328343::get_offset_of_swigDelegate3_6(),
	ObjectElementListener_t389328343::get_offset_of_swigDelegate4_7(),
	ObjectElementListener_t389328343::get_offset_of_swigDelegate5_8(),
	ObjectElementListener_t389328343::get_offset_of_swigDelegate6_9(),
	ObjectElementListener_t389328343::get_offset_of_swigDelegate7_10(),
	ObjectElementListener_t389328343_StaticFields::get_offset_of_swigMethodTypes0_11(),
	ObjectElementListener_t389328343_StaticFields::get_offset_of_swigMethodTypes1_12(),
	ObjectElementListener_t389328343_StaticFields::get_offset_of_swigMethodTypes2_13(),
	ObjectElementListener_t389328343_StaticFields::get_offset_of_swigMethodTypes3_14(),
	ObjectElementListener_t389328343_StaticFields::get_offset_of_swigMethodTypes4_15(),
	ObjectElementListener_t389328343_StaticFields::get_offset_of_swigMethodTypes5_16(),
	ObjectElementListener_t389328343_StaticFields::get_offset_of_swigMethodTypes6_17(),
	ObjectElementListener_t389328343_StaticFields::get_offset_of_swigMethodTypes7_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5375 = { sizeof (SwigDelegateObjectElementListener_0_t271806937), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5376 = { sizeof (SwigDelegateObjectElementListener_1_t271806938), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5377 = { sizeof (SwigDelegateObjectElementListener_2_t271806935), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5378 = { sizeof (SwigDelegateObjectElementListener_3_t271806936), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5379 = { sizeof (SwigDelegateObjectElementListener_4_t271806941), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5380 = { sizeof (SwigDelegateObjectElementListener_5_t271806942), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5381 = { sizeof (SwigDelegateObjectElementListener_6_t271806939), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5382 = { sizeof (SwigDelegateObjectElementListener_7_t271806940), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5383 = { sizeof (PairingAdapter_t2261856171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5383[2] = 
{
	PairingAdapter_t2261856171::get_offset_of_SuccessEvent_7(),
	PairingAdapter_t2261856171::get_offset_of_FailureEvent_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5384 = { sizeof (PairingListener_t2464634798), -1, sizeof(PairingListener_t2464634798_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5384[5] = 
{
	PairingListener_t2464634798::get_offset_of_swigCPtr_2(),
	PairingListener_t2464634798::get_offset_of_swigDelegate0_3(),
	PairingListener_t2464634798::get_offset_of_swigDelegate1_4(),
	PairingListener_t2464634798_StaticFields::get_offset_of_swigMethodTypes0_5(),
	PairingListener_t2464634798_StaticFields::get_offset_of_swigMethodTypes1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5385 = { sizeof (SwigDelegatePairingListener_0_t4183180789), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5386 = { sizeof (SwigDelegatePairingListener_1_t4183180788), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5387 = { sizeof (PairingManager_t3824021151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5387[2] = 
{
	PairingManager_t3824021151::get_offset_of_swigCPtr_0(),
	PairingManager_t3824021151::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5388 = { sizeof (PairingResult_t1485689352)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5388[8] = 
{
	PairingResult_t1485689352::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5389 = { sizeof (PairMaker_t2569523026), -1, sizeof(PairMaker_t2569523026_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5389[18] = 
{
	PairMaker_t2569523026::get_offset_of_swigCPtr_0(),
	PairMaker_t2569523026::get_offset_of_swigCMemOwn_1(),
	PairMaker_t2569523026::get_offset_of_swigDelegate0_2(),
	PairMaker_t2569523026::get_offset_of_swigDelegate1_3(),
	PairMaker_t2569523026::get_offset_of_swigDelegate2_4(),
	PairMaker_t2569523026::get_offset_of_swigDelegate3_5(),
	PairMaker_t2569523026::get_offset_of_swigDelegate4_6(),
	PairMaker_t2569523026::get_offset_of_swigDelegate5_7(),
	PairMaker_t2569523026::get_offset_of_swigDelegate6_8(),
	PairMaker_t2569523026::get_offset_of_swigDelegate7_9(),
	PairMaker_t2569523026_StaticFields::get_offset_of_swigMethodTypes0_10(),
	PairMaker_t2569523026_StaticFields::get_offset_of_swigMethodTypes1_11(),
	PairMaker_t2569523026_StaticFields::get_offset_of_swigMethodTypes2_12(),
	PairMaker_t2569523026_StaticFields::get_offset_of_swigMethodTypes3_13(),
	PairMaker_t2569523026_StaticFields::get_offset_of_swigMethodTypes4_14(),
	PairMaker_t2569523026_StaticFields::get_offset_of_swigMethodTypes5_15(),
	PairMaker_t2569523026_StaticFields::get_offset_of_swigMethodTypes6_16(),
	PairMaker_t2569523026_StaticFields::get_offset_of_swigMethodTypes7_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5390 = { sizeof (SwigDelegatePairMaker_0_t3088859200), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5391 = { sizeof (SwigDelegatePairMaker_1_t3088859199), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5392 = { sizeof (SwigDelegatePairMaker_2_t3088859202), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5393 = { sizeof (SwigDelegatePairMaker_3_t3088859201), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5394 = { sizeof (SwigDelegatePairMaker_4_t3088859196), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5395 = { sizeof (SwigDelegatePairMaker_5_t3088859195), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5396 = { sizeof (SwigDelegatePairMaker_6_t3088859198), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5397 = { sizeof (SwigDelegatePairMaker_7_t3088859197), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5398 = { sizeof (Profile_t1856509255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5398[2] = 
{
	Profile_t1856509255::get_offset_of_swigCPtr_0(),
	Profile_t1856509255::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5399 = { sizeof (ProfileManager_t129985400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5399[2] = 
{
	ProfileManager_t129985400::get_offset_of_swigCPtr_0(),
	ProfileManager_t129985400::get_offset_of_swigCMemOwn_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
