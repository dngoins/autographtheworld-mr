﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Threading.Thread
struct Thread_t2300836069;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1493878338;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.IO.SearchPattern2/Op
struct Op_t3134810481;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.IO.IFileWatcher
struct IFileWatcher_t3899097327;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.IO.KqueueMonitor
struct KqueueMonitor_t3818269198;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.IO.FileSystemWatcher
struct FileSystemWatcher_t416760199;
// System.IO.InotifyData
struct InotifyData_t2533354870;
// System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry
struct NameObjectEntry_t4224248211;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t1318642398;
// System.StringComparer
struct StringComparer_t3301955079;
// System.Collections.Specialized.ListDictionary/DictionaryNode
struct DictionaryNode_t417719465;
// System.Collections.IComparer
struct IComparer_t1540313114;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_t1624492310;
// System.Collections.Specialized.NameObjectCollectionBase
struct NameObjectCollectionBase_t2091847364;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Collections.ICollection
struct ICollection_t3904884886;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t2342208608;
// System.Collections.IHashCodeProvider
struct IHashCodeProvider_t267601189;
// System.Void
struct Void_t1185182177;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t156472862;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2974092902;
// System.ComponentModel.ISite
struct ISite_t4006303512;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1108123056;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Exception
struct Exception_t1436737249;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IO.kevent[]
struct keventU5BU5D_t21523777;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;
// System.Collections.Generic.Dictionary`2<System.String,System.IO.PathData>
struct Dictionary_2_t3002451328;
// System.Collections.Generic.Dictionary`2<System.Int32,System.IO.PathData>
struct Dictionary_2_t2105908360;
// System.IO.Compression.DeflateStream
struct DeflateStream_t4175168077;
// System.IO.Compression.DeflateStreamNative/UnmanagedReadOrWrite
struct UnmanagedReadOrWrite_t1975956110;
// System.IO.Stream
struct Stream_t1273022909;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.IList
struct IList_t2094931216;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.IO.Compression.DeflateStreamNative
struct DeflateStreamNative_t1405046456;
// System.IO.RenamedEventArgs
struct RenamedEventArgs_t2350765466;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.ComponentModel.ISynchronizeInvoke
struct ISynchronizeInvoke_t1357618335;
// System.IO.SearchPattern2
struct SearchPattern2_t2824637351;
// System.IO.FileSystemEventHandler
struct FileSystemEventHandler_t1806121106;
// System.IO.ErrorEventHandler
struct ErrorEventHandler_t2621677363;
// System.IO.RenamedEventHandler
struct RenamedEventHandler_t3047461033;
// System.Collections.Specialized.NotifyCollectionChangedEventArgs
struct NotifyCollectionChangedEventArgs_t9239872;
// System.IO.FileSystemEventArgs
struct FileSystemEventArgs_t1603777841;
// System.IO.ErrorEventArgs
struct ErrorEventArgs_t1584858912;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DEFAULTWATCHER_T2229106420_H
#define DEFAULTWATCHER_T2229106420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.DefaultWatcher
struct  DefaultWatcher_t2229106420  : public RuntimeObject
{
public:

public:
};

struct DefaultWatcher_t2229106420_StaticFields
{
public:
	// System.IO.DefaultWatcher System.IO.DefaultWatcher::instance
	DefaultWatcher_t2229106420 * ___instance_0;
	// System.Threading.Thread System.IO.DefaultWatcher::thread
	Thread_t2300836069 * ___thread_1;
	// System.Collections.Hashtable System.IO.DefaultWatcher::watches
	Hashtable_t1853889766 * ___watches_2;
	// System.String[] System.IO.DefaultWatcher::NoStringsArray
	StringU5BU5D_t1281789340* ___NoStringsArray_3;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(DefaultWatcher_t2229106420_StaticFields, ___instance_0)); }
	inline DefaultWatcher_t2229106420 * get_instance_0() const { return ___instance_0; }
	inline DefaultWatcher_t2229106420 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(DefaultWatcher_t2229106420 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}

	inline static int32_t get_offset_of_thread_1() { return static_cast<int32_t>(offsetof(DefaultWatcher_t2229106420_StaticFields, ___thread_1)); }
	inline Thread_t2300836069 * get_thread_1() const { return ___thread_1; }
	inline Thread_t2300836069 ** get_address_of_thread_1() { return &___thread_1; }
	inline void set_thread_1(Thread_t2300836069 * value)
	{
		___thread_1 = value;
		Il2CppCodeGenWriteBarrier((&___thread_1), value);
	}

	inline static int32_t get_offset_of_watches_2() { return static_cast<int32_t>(offsetof(DefaultWatcher_t2229106420_StaticFields, ___watches_2)); }
	inline Hashtable_t1853889766 * get_watches_2() const { return ___watches_2; }
	inline Hashtable_t1853889766 ** get_address_of_watches_2() { return &___watches_2; }
	inline void set_watches_2(Hashtable_t1853889766 * value)
	{
		___watches_2 = value;
		Il2CppCodeGenWriteBarrier((&___watches_2), value);
	}

	inline static int32_t get_offset_of_NoStringsArray_3() { return static_cast<int32_t>(offsetof(DefaultWatcher_t2229106420_StaticFields, ___NoStringsArray_3)); }
	inline StringU5BU5D_t1281789340* get_NoStringsArray_3() const { return ___NoStringsArray_3; }
	inline StringU5BU5D_t1281789340** get_address_of_NoStringsArray_3() { return &___NoStringsArray_3; }
	inline void set_NoStringsArray_3(StringU5BU5D_t1281789340* value)
	{
		___NoStringsArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___NoStringsArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTWATCHER_T2229106420_H
#ifndef PATHDATA_T3217195029_H
#define PATHDATA_T3217195029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.PathData
struct  PathData_t3217195029  : public RuntimeObject
{
public:
	// System.String System.IO.PathData::Path
	String_t* ___Path_0;
	// System.Boolean System.IO.PathData::IsDirectory
	bool ___IsDirectory_1;
	// System.Int32 System.IO.PathData::Fd
	int32_t ___Fd_2;

public:
	inline static int32_t get_offset_of_Path_0() { return static_cast<int32_t>(offsetof(PathData_t3217195029, ___Path_0)); }
	inline String_t* get_Path_0() const { return ___Path_0; }
	inline String_t** get_address_of_Path_0() { return &___Path_0; }
	inline void set_Path_0(String_t* value)
	{
		___Path_0 = value;
		Il2CppCodeGenWriteBarrier((&___Path_0), value);
	}

	inline static int32_t get_offset_of_IsDirectory_1() { return static_cast<int32_t>(offsetof(PathData_t3217195029, ___IsDirectory_1)); }
	inline bool get_IsDirectory_1() const { return ___IsDirectory_1; }
	inline bool* get_address_of_IsDirectory_1() { return &___IsDirectory_1; }
	inline void set_IsDirectory_1(bool value)
	{
		___IsDirectory_1 = value;
	}

	inline static int32_t get_offset_of_Fd_2() { return static_cast<int32_t>(offsetof(PathData_t3217195029, ___Fd_2)); }
	inline int32_t get_Fd_2() const { return ___Fd_2; }
	inline int32_t* get_address_of_Fd_2() { return &___Fd_2; }
	inline void set_Fd_2(int32_t value)
	{
		___Fd_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHDATA_T3217195029_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef STRINGDICTIONARY_T120437468_H
#define STRINGDICTIONARY_T120437468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.StringDictionary
struct  StringDictionary_t120437468  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Collections.Specialized.StringDictionary::contents
	Hashtable_t1853889766 * ___contents_0;

public:
	inline static int32_t get_offset_of_contents_0() { return static_cast<int32_t>(offsetof(StringDictionary_t120437468, ___contents_0)); }
	inline Hashtable_t1853889766 * get_contents_0() const { return ___contents_0; }
	inline Hashtable_t1853889766 ** get_address_of_contents_0() { return &___contents_0; }
	inline void set_contents_0(Hashtable_t1853889766 * value)
	{
		___contents_0 = value;
		Il2CppCodeGenWriteBarrier((&___contents_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGDICTIONARY_T120437468_H
#ifndef ORDEREDDICTIONARYENUMERATOR_T1215437281_H
#define ORDEREDDICTIONARYENUMERATOR_T1215437281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.OrderedDictionary/OrderedDictionaryEnumerator
struct  OrderedDictionaryEnumerator_t1215437281  : public RuntimeObject
{
public:
	// System.Int32 System.Collections.Specialized.OrderedDictionary/OrderedDictionaryEnumerator::_objectReturnType
	int32_t ____objectReturnType_0;
	// System.Collections.IEnumerator System.Collections.Specialized.OrderedDictionary/OrderedDictionaryEnumerator::arrayEnumerator
	RuntimeObject* ___arrayEnumerator_1;

public:
	inline static int32_t get_offset_of__objectReturnType_0() { return static_cast<int32_t>(offsetof(OrderedDictionaryEnumerator_t1215437281, ____objectReturnType_0)); }
	inline int32_t get__objectReturnType_0() const { return ____objectReturnType_0; }
	inline int32_t* get_address_of__objectReturnType_0() { return &____objectReturnType_0; }
	inline void set__objectReturnType_0(int32_t value)
	{
		____objectReturnType_0 = value;
	}

	inline static int32_t get_offset_of_arrayEnumerator_1() { return static_cast<int32_t>(offsetof(OrderedDictionaryEnumerator_t1215437281, ___arrayEnumerator_1)); }
	inline RuntimeObject* get_arrayEnumerator_1() const { return ___arrayEnumerator_1; }
	inline RuntimeObject** get_address_of_arrayEnumerator_1() { return &___arrayEnumerator_1; }
	inline void set_arrayEnumerator_1(RuntimeObject* value)
	{
		___arrayEnumerator_1 = value;
		Il2CppCodeGenWriteBarrier((&___arrayEnumerator_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORDEREDDICTIONARYENUMERATOR_T1215437281_H
#ifndef ORDEREDDICTIONARY_T2617496293_H
#define ORDEREDDICTIONARY_T2617496293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.OrderedDictionary
struct  OrderedDictionary_t2617496293  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.Specialized.OrderedDictionary::_objectsArray
	ArrayList_t2718874744 * ____objectsArray_0;
	// System.Collections.Hashtable System.Collections.Specialized.OrderedDictionary::_objectsTable
	Hashtable_t1853889766 * ____objectsTable_1;
	// System.Int32 System.Collections.Specialized.OrderedDictionary::_initialCapacity
	int32_t ____initialCapacity_2;
	// System.Collections.IEqualityComparer System.Collections.Specialized.OrderedDictionary::_comparer
	RuntimeObject* ____comparer_3;
	// System.Boolean System.Collections.Specialized.OrderedDictionary::_readOnly
	bool ____readOnly_4;
	// System.Object System.Collections.Specialized.OrderedDictionary::_syncRoot
	RuntimeObject * ____syncRoot_5;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.OrderedDictionary::_siInfo
	SerializationInfo_t950877179 * ____siInfo_6;

public:
	inline static int32_t get_offset_of__objectsArray_0() { return static_cast<int32_t>(offsetof(OrderedDictionary_t2617496293, ____objectsArray_0)); }
	inline ArrayList_t2718874744 * get__objectsArray_0() const { return ____objectsArray_0; }
	inline ArrayList_t2718874744 ** get_address_of__objectsArray_0() { return &____objectsArray_0; }
	inline void set__objectsArray_0(ArrayList_t2718874744 * value)
	{
		____objectsArray_0 = value;
		Il2CppCodeGenWriteBarrier((&____objectsArray_0), value);
	}

	inline static int32_t get_offset_of__objectsTable_1() { return static_cast<int32_t>(offsetof(OrderedDictionary_t2617496293, ____objectsTable_1)); }
	inline Hashtable_t1853889766 * get__objectsTable_1() const { return ____objectsTable_1; }
	inline Hashtable_t1853889766 ** get_address_of__objectsTable_1() { return &____objectsTable_1; }
	inline void set__objectsTable_1(Hashtable_t1853889766 * value)
	{
		____objectsTable_1 = value;
		Il2CppCodeGenWriteBarrier((&____objectsTable_1), value);
	}

	inline static int32_t get_offset_of__initialCapacity_2() { return static_cast<int32_t>(offsetof(OrderedDictionary_t2617496293, ____initialCapacity_2)); }
	inline int32_t get__initialCapacity_2() const { return ____initialCapacity_2; }
	inline int32_t* get_address_of__initialCapacity_2() { return &____initialCapacity_2; }
	inline void set__initialCapacity_2(int32_t value)
	{
		____initialCapacity_2 = value;
	}

	inline static int32_t get_offset_of__comparer_3() { return static_cast<int32_t>(offsetof(OrderedDictionary_t2617496293, ____comparer_3)); }
	inline RuntimeObject* get__comparer_3() const { return ____comparer_3; }
	inline RuntimeObject** get_address_of__comparer_3() { return &____comparer_3; }
	inline void set__comparer_3(RuntimeObject* value)
	{
		____comparer_3 = value;
		Il2CppCodeGenWriteBarrier((&____comparer_3), value);
	}

	inline static int32_t get_offset_of__readOnly_4() { return static_cast<int32_t>(offsetof(OrderedDictionary_t2617496293, ____readOnly_4)); }
	inline bool get__readOnly_4() const { return ____readOnly_4; }
	inline bool* get_address_of__readOnly_4() { return &____readOnly_4; }
	inline void set__readOnly_4(bool value)
	{
		____readOnly_4 = value;
	}

	inline static int32_t get_offset_of__syncRoot_5() { return static_cast<int32_t>(offsetof(OrderedDictionary_t2617496293, ____syncRoot_5)); }
	inline RuntimeObject * get__syncRoot_5() const { return ____syncRoot_5; }
	inline RuntimeObject ** get_address_of__syncRoot_5() { return &____syncRoot_5; }
	inline void set__syncRoot_5(RuntimeObject * value)
	{
		____syncRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_5), value);
	}

	inline static int32_t get_offset_of__siInfo_6() { return static_cast<int32_t>(offsetof(OrderedDictionary_t2617496293, ____siInfo_6)); }
	inline SerializationInfo_t950877179 * get__siInfo_6() const { return ____siInfo_6; }
	inline SerializationInfo_t950877179 ** get_address_of__siInfo_6() { return &____siInfo_6; }
	inline void set__siInfo_6(SerializationInfo_t950877179 * value)
	{
		____siInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&____siInfo_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORDEREDDICTIONARY_T2617496293_H
#ifndef STRINGCOLLECTION_T167406615_H
#define STRINGCOLLECTION_T167406615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.StringCollection
struct  StringCollection_t167406615  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.Specialized.StringCollection::data
	ArrayList_t2718874744 * ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(StringCollection_t167406615, ___data_0)); }
	inline ArrayList_t2718874744 * get_data_0() const { return ___data_0; }
	inline ArrayList_t2718874744 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ArrayList_t2718874744 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGCOLLECTION_T167406615_H
#ifndef ORDEREDDICTIONARYKEYVALUECOLLECTION_T1788601968_H
#define ORDEREDDICTIONARYKEYVALUECOLLECTION_T1788601968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.OrderedDictionary/OrderedDictionaryKeyValueCollection
struct  OrderedDictionaryKeyValueCollection_t1788601968  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.Specialized.OrderedDictionary/OrderedDictionaryKeyValueCollection::_objects
	ArrayList_t2718874744 * ____objects_0;
	// System.Boolean System.Collections.Specialized.OrderedDictionary/OrderedDictionaryKeyValueCollection::isKeys
	bool ___isKeys_1;

public:
	inline static int32_t get_offset_of__objects_0() { return static_cast<int32_t>(offsetof(OrderedDictionaryKeyValueCollection_t1788601968, ____objects_0)); }
	inline ArrayList_t2718874744 * get__objects_0() const { return ____objects_0; }
	inline ArrayList_t2718874744 ** get_address_of__objects_0() { return &____objects_0; }
	inline void set__objects_0(ArrayList_t2718874744 * value)
	{
		____objects_0 = value;
		Il2CppCodeGenWriteBarrier((&____objects_0), value);
	}

	inline static int32_t get_offset_of_isKeys_1() { return static_cast<int32_t>(offsetof(OrderedDictionaryKeyValueCollection_t1788601968, ___isKeys_1)); }
	inline bool get_isKeys_1() const { return ___isKeys_1; }
	inline bool* get_address_of_isKeys_1() { return &___isKeys_1; }
	inline void set_isKeys_1(bool value)
	{
		___isKeys_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORDEREDDICTIONARYKEYVALUECOLLECTION_T1788601968_H
#ifndef SEARCHPATTERN2_T2824637351_H
#define SEARCHPATTERN2_T2824637351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.SearchPattern2
struct  SearchPattern2_t2824637351  : public RuntimeObject
{
public:
	// System.IO.SearchPattern2/Op System.IO.SearchPattern2::ops
	Op_t3134810481 * ___ops_0;
	// System.Boolean System.IO.SearchPattern2::ignore
	bool ___ignore_1;
	// System.Boolean System.IO.SearchPattern2::hasWildcard
	bool ___hasWildcard_2;
	// System.String System.IO.SearchPattern2::pattern
	String_t* ___pattern_3;

public:
	inline static int32_t get_offset_of_ops_0() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351, ___ops_0)); }
	inline Op_t3134810481 * get_ops_0() const { return ___ops_0; }
	inline Op_t3134810481 ** get_address_of_ops_0() { return &___ops_0; }
	inline void set_ops_0(Op_t3134810481 * value)
	{
		___ops_0 = value;
		Il2CppCodeGenWriteBarrier((&___ops_0), value);
	}

	inline static int32_t get_offset_of_ignore_1() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351, ___ignore_1)); }
	inline bool get_ignore_1() const { return ___ignore_1; }
	inline bool* get_address_of_ignore_1() { return &___ignore_1; }
	inline void set_ignore_1(bool value)
	{
		___ignore_1 = value;
	}

	inline static int32_t get_offset_of_hasWildcard_2() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351, ___hasWildcard_2)); }
	inline bool get_hasWildcard_2() const { return ___hasWildcard_2; }
	inline bool* get_address_of_hasWildcard_2() { return &___hasWildcard_2; }
	inline void set_hasWildcard_2(bool value)
	{
		___hasWildcard_2 = value;
	}

	inline static int32_t get_offset_of_pattern_3() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351, ___pattern_3)); }
	inline String_t* get_pattern_3() const { return ___pattern_3; }
	inline String_t** get_address_of_pattern_3() { return &___pattern_3; }
	inline void set_pattern_3(String_t* value)
	{
		___pattern_3 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_3), value);
	}
};

struct SearchPattern2_t2824637351_StaticFields
{
public:
	// System.Char[] System.IO.SearchPattern2::WildcardChars
	CharU5BU5D_t3528271667* ___WildcardChars_4;
	// System.Char[] System.IO.SearchPattern2::InvalidChars
	CharU5BU5D_t3528271667* ___InvalidChars_5;

public:
	inline static int32_t get_offset_of_WildcardChars_4() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351_StaticFields, ___WildcardChars_4)); }
	inline CharU5BU5D_t3528271667* get_WildcardChars_4() const { return ___WildcardChars_4; }
	inline CharU5BU5D_t3528271667** get_address_of_WildcardChars_4() { return &___WildcardChars_4; }
	inline void set_WildcardChars_4(CharU5BU5D_t3528271667* value)
	{
		___WildcardChars_4 = value;
		Il2CppCodeGenWriteBarrier((&___WildcardChars_4), value);
	}

	inline static int32_t get_offset_of_InvalidChars_5() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351_StaticFields, ___InvalidChars_5)); }
	inline CharU5BU5D_t3528271667* get_InvalidChars_5() const { return ___InvalidChars_5; }
	inline CharU5BU5D_t3528271667** get_address_of_InvalidChars_5() { return &___InvalidChars_5; }
	inline void set_InvalidChars_5(CharU5BU5D_t3528271667* value)
	{
		___InvalidChars_5 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidChars_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHPATTERN2_T2824637351_H
#ifndef WINDOWSWATCHER_T1958179572_H
#define WINDOWSWATCHER_T1958179572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.WindowsWatcher
struct  WindowsWatcher_t1958179572  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWSWATCHER_T1958179572_H
#ifndef KEVENTWATCHER_T3355132332_H
#define KEVENTWATCHER_T3355132332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.KeventWatcher
struct  KeventWatcher_t3355132332  : public RuntimeObject
{
public:

public:
};

struct KeventWatcher_t3355132332_StaticFields
{
public:
	// System.Boolean System.IO.KeventWatcher::failed
	bool ___failed_0;
	// System.IO.KeventWatcher System.IO.KeventWatcher::instance
	KeventWatcher_t3355132332 * ___instance_1;
	// System.Collections.Hashtable System.IO.KeventWatcher::watches
	Hashtable_t1853889766 * ___watches_2;

public:
	inline static int32_t get_offset_of_failed_0() { return static_cast<int32_t>(offsetof(KeventWatcher_t3355132332_StaticFields, ___failed_0)); }
	inline bool get_failed_0() const { return ___failed_0; }
	inline bool* get_address_of_failed_0() { return &___failed_0; }
	inline void set_failed_0(bool value)
	{
		___failed_0 = value;
	}

	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(KeventWatcher_t3355132332_StaticFields, ___instance_1)); }
	inline KeventWatcher_t3355132332 * get_instance_1() const { return ___instance_1; }
	inline KeventWatcher_t3355132332 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(KeventWatcher_t3355132332 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___instance_1), value);
	}

	inline static int32_t get_offset_of_watches_2() { return static_cast<int32_t>(offsetof(KeventWatcher_t3355132332_StaticFields, ___watches_2)); }
	inline Hashtable_t1853889766 * get_watches_2() const { return ___watches_2; }
	inline Hashtable_t1853889766 ** get_address_of_watches_2() { return &___watches_2; }
	inline void set_watches_2(Hashtable_t1853889766 * value)
	{
		___watches_2 = value;
		Il2CppCodeGenWriteBarrier((&___watches_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEVENTWATCHER_T3355132332_H
#ifndef NULLFILEWATCHER_T1718214284_H
#define NULLFILEWATCHER_T1718214284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.NullFileWatcher
struct  NullFileWatcher_t1718214284  : public RuntimeObject
{
public:

public:
};

struct NullFileWatcher_t1718214284_StaticFields
{
public:
	// System.IO.IFileWatcher System.IO.NullFileWatcher::instance
	RuntimeObject* ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(NullFileWatcher_t1718214284_StaticFields, ___instance_0)); }
	inline RuntimeObject* get_instance_0() const { return ___instance_0; }
	inline RuntimeObject** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(RuntimeObject* value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLFILEWATCHER_T1718214284_H
#ifndef U3CGETENUMERATORU3ED__18_T986872067_H
#define U3CGETENUMERATORU3ED__18_T986872067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.InotifyWatcher/<GetEnumerator>d__18
struct  U3CGetEnumeratorU3Ed__18_t986872067  : public RuntimeObject
{
public:
	// System.Int32 System.IO.InotifyWatcher/<GetEnumerator>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object System.IO.InotifyWatcher/<GetEnumerator>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 System.IO.InotifyWatcher/<GetEnumerator>d__18::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Object System.IO.InotifyWatcher/<GetEnumerator>d__18::source
	RuntimeObject * ___source_3;
	// System.Object System.IO.InotifyWatcher/<GetEnumerator>d__18::<>3__source
	RuntimeObject * ___U3CU3E3__source_4;
	// System.Collections.ArrayList System.IO.InotifyWatcher/<GetEnumerator>d__18::<list>5__1
	ArrayList_t2718874744 * ___U3ClistU3E5__1_5;
	// System.Int32 System.IO.InotifyWatcher/<GetEnumerator>d__18::<i>5__2
	int32_t ___U3CiU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_t986872067, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_t986872067, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_t986872067, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_t986872067, ___source_3)); }
	inline RuntimeObject * get_source_3() const { return ___source_3; }
	inline RuntimeObject ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((&___source_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__source_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_t986872067, ___U3CU3E3__source_4)); }
	inline RuntimeObject * get_U3CU3E3__source_4() const { return ___U3CU3E3__source_4; }
	inline RuntimeObject ** get_address_of_U3CU3E3__source_4() { return &___U3CU3E3__source_4; }
	inline void set_U3CU3E3__source_4(RuntimeObject * value)
	{
		___U3CU3E3__source_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__source_4), value);
	}

	inline static int32_t get_offset_of_U3ClistU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_t986872067, ___U3ClistU3E5__1_5)); }
	inline ArrayList_t2718874744 * get_U3ClistU3E5__1_5() const { return ___U3ClistU3E5__1_5; }
	inline ArrayList_t2718874744 ** get_address_of_U3ClistU3E5__1_5() { return &___U3ClistU3E5__1_5; }
	inline void set_U3ClistU3E5__1_5(ArrayList_t2718874744 * value)
	{
		___U3ClistU3E5__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClistU3E5__1_5), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__18_t986872067, ___U3CiU3E5__2_6)); }
	inline int32_t get_U3CiU3E5__2_6() const { return ___U3CiU3E5__2_6; }
	inline int32_t* get_address_of_U3CiU3E5__2_6() { return &___U3CiU3E5__2_6; }
	inline void set_U3CiU3E5__2_6(int32_t value)
	{
		___U3CiU3E5__2_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__18_T986872067_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T2361320332_H
#define U3CU3EC__DISPLAYCLASS11_0_T2361320332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.KqueueMonitor/<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t2361320332  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Int32> System.IO.KqueueMonitor/<>c__DisplayClass11_0::newFds
	List_1_t128053199 * ___newFds_0;
	// System.IO.KqueueMonitor System.IO.KqueueMonitor/<>c__DisplayClass11_0::<>4__this
	KqueueMonitor_t3818269198 * ___U3CU3E4__this_1;
	// System.Action`1<System.String> System.IO.KqueueMonitor/<>c__DisplayClass11_0::<>9__0
	Action_1_t2019918284 * ___U3CU3E9__0_2;

public:
	inline static int32_t get_offset_of_newFds_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t2361320332, ___newFds_0)); }
	inline List_1_t128053199 * get_newFds_0() const { return ___newFds_0; }
	inline List_1_t128053199 ** get_address_of_newFds_0() { return &___newFds_0; }
	inline void set_newFds_0(List_1_t128053199 * value)
	{
		___newFds_0 = value;
		Il2CppCodeGenWriteBarrier((&___newFds_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t2361320332, ___U3CU3E4__this_1)); }
	inline KqueueMonitor_t3818269198 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline KqueueMonitor_t3818269198 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(KqueueMonitor_t3818269198 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t2361320332, ___U3CU3E9__0_2)); }
	inline Action_1_t2019918284 * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Action_1_t2019918284 ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Action_1_t2019918284 * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T2361320332_H
#ifndef INOTIFYDATA_T2533354870_H
#define INOTIFYDATA_T2533354870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.InotifyData
struct  InotifyData_t2533354870  : public RuntimeObject
{
public:
	// System.IO.FileSystemWatcher System.IO.InotifyData::FSW
	FileSystemWatcher_t416760199 * ___FSW_0;
	// System.String System.IO.InotifyData::Directory
	String_t* ___Directory_1;
	// System.Int32 System.IO.InotifyData::Watch
	int32_t ___Watch_2;

public:
	inline static int32_t get_offset_of_FSW_0() { return static_cast<int32_t>(offsetof(InotifyData_t2533354870, ___FSW_0)); }
	inline FileSystemWatcher_t416760199 * get_FSW_0() const { return ___FSW_0; }
	inline FileSystemWatcher_t416760199 ** get_address_of_FSW_0() { return &___FSW_0; }
	inline void set_FSW_0(FileSystemWatcher_t416760199 * value)
	{
		___FSW_0 = value;
		Il2CppCodeGenWriteBarrier((&___FSW_0), value);
	}

	inline static int32_t get_offset_of_Directory_1() { return static_cast<int32_t>(offsetof(InotifyData_t2533354870, ___Directory_1)); }
	inline String_t* get_Directory_1() const { return ___Directory_1; }
	inline String_t** get_address_of_Directory_1() { return &___Directory_1; }
	inline void set_Directory_1(String_t* value)
	{
		___Directory_1 = value;
		Il2CppCodeGenWriteBarrier((&___Directory_1), value);
	}

	inline static int32_t get_offset_of_Watch_2() { return static_cast<int32_t>(offsetof(InotifyData_t2533354870, ___Watch_2)); }
	inline int32_t get_Watch_2() const { return ___Watch_2; }
	inline int32_t* get_address_of_Watch_2() { return &___Watch_2; }
	inline void set_Watch_2(int32_t value)
	{
		___Watch_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INOTIFYDATA_T2533354870_H
#ifndef PARENTINOTIFYDATA_T1149622319_H
#define PARENTINOTIFYDATA_T1149622319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.ParentInotifyData
struct  ParentInotifyData_t1149622319  : public RuntimeObject
{
public:
	// System.Boolean System.IO.ParentInotifyData::IncludeSubdirs
	bool ___IncludeSubdirs_0;
	// System.Boolean System.IO.ParentInotifyData::Enabled
	bool ___Enabled_1;
	// System.Collections.ArrayList System.IO.ParentInotifyData::children
	ArrayList_t2718874744 * ___children_2;
	// System.IO.InotifyData System.IO.ParentInotifyData::data
	InotifyData_t2533354870 * ___data_3;

public:
	inline static int32_t get_offset_of_IncludeSubdirs_0() { return static_cast<int32_t>(offsetof(ParentInotifyData_t1149622319, ___IncludeSubdirs_0)); }
	inline bool get_IncludeSubdirs_0() const { return ___IncludeSubdirs_0; }
	inline bool* get_address_of_IncludeSubdirs_0() { return &___IncludeSubdirs_0; }
	inline void set_IncludeSubdirs_0(bool value)
	{
		___IncludeSubdirs_0 = value;
	}

	inline static int32_t get_offset_of_Enabled_1() { return static_cast<int32_t>(offsetof(ParentInotifyData_t1149622319, ___Enabled_1)); }
	inline bool get_Enabled_1() const { return ___Enabled_1; }
	inline bool* get_address_of_Enabled_1() { return &___Enabled_1; }
	inline void set_Enabled_1(bool value)
	{
		___Enabled_1 = value;
	}

	inline static int32_t get_offset_of_children_2() { return static_cast<int32_t>(offsetof(ParentInotifyData_t1149622319, ___children_2)); }
	inline ArrayList_t2718874744 * get_children_2() const { return ___children_2; }
	inline ArrayList_t2718874744 ** get_address_of_children_2() { return &___children_2; }
	inline void set_children_2(ArrayList_t2718874744 * value)
	{
		___children_2 = value;
		Il2CppCodeGenWriteBarrier((&___children_2), value);
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(ParentInotifyData_t1149622319, ___data_3)); }
	inline InotifyData_t2533354870 * get_data_3() const { return ___data_3; }
	inline InotifyData_t2533354870 ** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(InotifyData_t2533354870 * value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((&___data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARENTINOTIFYDATA_T1149622319_H
#ifndef NAMEOBJECTCOLLECTIONBASE_T2091847364_H
#define NAMEOBJECTCOLLECTIONBASE_T2091847364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase
struct  NameObjectCollectionBase_t2091847364  : public RuntimeObject
{
public:
	// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::_readOnly
	bool ____readOnly_0;
	// System.Collections.ArrayList System.Collections.Specialized.NameObjectCollectionBase::_entriesArray
	ArrayList_t2718874744 * ____entriesArray_1;
	// System.Collections.IEqualityComparer System.Collections.Specialized.NameObjectCollectionBase::_keyComparer
	RuntimeObject* ____keyComparer_2;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.NameObjectCollectionBase::_entriesTable
	Hashtable_t1853889766 * ____entriesTable_3;
	// System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.NameObjectCollectionBase::_nullKeyEntry
	NameObjectEntry_t4224248211 * ____nullKeyEntry_4;
	// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection System.Collections.Specialized.NameObjectCollectionBase::_keys
	KeysCollection_t1318642398 * ____keys_5;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.NameObjectCollectionBase::_serializationInfo
	SerializationInfo_t950877179 * ____serializationInfo_6;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::_version
	int32_t ____version_7;
	// System.Object System.Collections.Specialized.NameObjectCollectionBase::_syncRoot
	RuntimeObject * ____syncRoot_8;

public:
	inline static int32_t get_offset_of__readOnly_0() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ____readOnly_0)); }
	inline bool get__readOnly_0() const { return ____readOnly_0; }
	inline bool* get_address_of__readOnly_0() { return &____readOnly_0; }
	inline void set__readOnly_0(bool value)
	{
		____readOnly_0 = value;
	}

	inline static int32_t get_offset_of__entriesArray_1() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ____entriesArray_1)); }
	inline ArrayList_t2718874744 * get__entriesArray_1() const { return ____entriesArray_1; }
	inline ArrayList_t2718874744 ** get_address_of__entriesArray_1() { return &____entriesArray_1; }
	inline void set__entriesArray_1(ArrayList_t2718874744 * value)
	{
		____entriesArray_1 = value;
		Il2CppCodeGenWriteBarrier((&____entriesArray_1), value);
	}

	inline static int32_t get_offset_of__keyComparer_2() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ____keyComparer_2)); }
	inline RuntimeObject* get__keyComparer_2() const { return ____keyComparer_2; }
	inline RuntimeObject** get_address_of__keyComparer_2() { return &____keyComparer_2; }
	inline void set__keyComparer_2(RuntimeObject* value)
	{
		____keyComparer_2 = value;
		Il2CppCodeGenWriteBarrier((&____keyComparer_2), value);
	}

	inline static int32_t get_offset_of__entriesTable_3() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ____entriesTable_3)); }
	inline Hashtable_t1853889766 * get__entriesTable_3() const { return ____entriesTable_3; }
	inline Hashtable_t1853889766 ** get_address_of__entriesTable_3() { return &____entriesTable_3; }
	inline void set__entriesTable_3(Hashtable_t1853889766 * value)
	{
		____entriesTable_3 = value;
		Il2CppCodeGenWriteBarrier((&____entriesTable_3), value);
	}

	inline static int32_t get_offset_of__nullKeyEntry_4() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ____nullKeyEntry_4)); }
	inline NameObjectEntry_t4224248211 * get__nullKeyEntry_4() const { return ____nullKeyEntry_4; }
	inline NameObjectEntry_t4224248211 ** get_address_of__nullKeyEntry_4() { return &____nullKeyEntry_4; }
	inline void set__nullKeyEntry_4(NameObjectEntry_t4224248211 * value)
	{
		____nullKeyEntry_4 = value;
		Il2CppCodeGenWriteBarrier((&____nullKeyEntry_4), value);
	}

	inline static int32_t get_offset_of__keys_5() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ____keys_5)); }
	inline KeysCollection_t1318642398 * get__keys_5() const { return ____keys_5; }
	inline KeysCollection_t1318642398 ** get_address_of__keys_5() { return &____keys_5; }
	inline void set__keys_5(KeysCollection_t1318642398 * value)
	{
		____keys_5 = value;
		Il2CppCodeGenWriteBarrier((&____keys_5), value);
	}

	inline static int32_t get_offset_of__serializationInfo_6() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ____serializationInfo_6)); }
	inline SerializationInfo_t950877179 * get__serializationInfo_6() const { return ____serializationInfo_6; }
	inline SerializationInfo_t950877179 ** get_address_of__serializationInfo_6() { return &____serializationInfo_6; }
	inline void set__serializationInfo_6(SerializationInfo_t950877179 * value)
	{
		____serializationInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&____serializationInfo_6), value);
	}

	inline static int32_t get_offset_of__version_7() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ____version_7)); }
	inline int32_t get__version_7() const { return ____version_7; }
	inline int32_t* get_address_of__version_7() { return &____version_7; }
	inline void set__version_7(int32_t value)
	{
		____version_7 = value;
	}

	inline static int32_t get_offset_of__syncRoot_8() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ____syncRoot_8)); }
	inline RuntimeObject * get__syncRoot_8() const { return ____syncRoot_8; }
	inline RuntimeObject ** get_address_of__syncRoot_8() { return &____syncRoot_8; }
	inline void set__syncRoot_8(RuntimeObject * value)
	{
		____syncRoot_8 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_8), value);
	}
};

struct NameObjectCollectionBase_t2091847364_StaticFields
{
public:
	// System.StringComparer System.Collections.Specialized.NameObjectCollectionBase::defaultComparer
	StringComparer_t3301955079 * ___defaultComparer_9;

public:
	inline static int32_t get_offset_of_defaultComparer_9() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364_StaticFields, ___defaultComparer_9)); }
	inline StringComparer_t3301955079 * get_defaultComparer_9() const { return ___defaultComparer_9; }
	inline StringComparer_t3301955079 ** get_address_of_defaultComparer_9() { return &___defaultComparer_9; }
	inline void set_defaultComparer_9(StringComparer_t3301955079 * value)
	{
		___defaultComparer_9 = value;
		Il2CppCodeGenWriteBarrier((&___defaultComparer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTCOLLECTIONBASE_T2091847364_H
#ifndef LISTDICTIONARY_T1624492310_H
#define LISTDICTIONARY_T1624492310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.ListDictionary
struct  ListDictionary_t1624492310  : public RuntimeObject
{
public:
	// System.Collections.Specialized.ListDictionary/DictionaryNode System.Collections.Specialized.ListDictionary::head
	DictionaryNode_t417719465 * ___head_0;
	// System.Int32 System.Collections.Specialized.ListDictionary::version
	int32_t ___version_1;
	// System.Int32 System.Collections.Specialized.ListDictionary::count
	int32_t ___count_2;
	// System.Collections.IComparer System.Collections.Specialized.ListDictionary::comparer
	RuntimeObject* ___comparer_3;
	// System.Object System.Collections.Specialized.ListDictionary::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of_head_0() { return static_cast<int32_t>(offsetof(ListDictionary_t1624492310, ___head_0)); }
	inline DictionaryNode_t417719465 * get_head_0() const { return ___head_0; }
	inline DictionaryNode_t417719465 ** get_address_of_head_0() { return &___head_0; }
	inline void set_head_0(DictionaryNode_t417719465 * value)
	{
		___head_0 = value;
		Il2CppCodeGenWriteBarrier((&___head_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(ListDictionary_t1624492310, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(ListDictionary_t1624492310, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_comparer_3() { return static_cast<int32_t>(offsetof(ListDictionary_t1624492310, ___comparer_3)); }
	inline RuntimeObject* get_comparer_3() const { return ___comparer_3; }
	inline RuntimeObject** get_address_of_comparer_3() { return &___comparer_3; }
	inline void set_comparer_3(RuntimeObject* value)
	{
		___comparer_3 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_3), value);
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(ListDictionary_t1624492310, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTDICTIONARY_T1624492310_H
#ifndef NAMEOBJECTENTRY_T4224248211_H
#define NAMEOBJECTENTRY_T4224248211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry
struct  NameObjectEntry_t4224248211  : public RuntimeObject
{
public:
	// System.String System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry::Key
	String_t* ___Key_0;
	// System.Object System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(NameObjectEntry_t4224248211, ___Key_0)); }
	inline String_t* get_Key_0() const { return ___Key_0; }
	inline String_t** get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(String_t* value)
	{
		___Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___Key_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(NameObjectEntry_t4224248211, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTENTRY_T4224248211_H
#ifndef NODEKEYVALUECOLLECTION_T1279341543_H
#define NODEKEYVALUECOLLECTION_T1279341543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.ListDictionary/NodeKeyValueCollection
struct  NodeKeyValueCollection_t1279341543  : public RuntimeObject
{
public:
	// System.Collections.Specialized.ListDictionary System.Collections.Specialized.ListDictionary/NodeKeyValueCollection::list
	ListDictionary_t1624492310 * ___list_0;
	// System.Boolean System.Collections.Specialized.ListDictionary/NodeKeyValueCollection::isKeys
	bool ___isKeys_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(NodeKeyValueCollection_t1279341543, ___list_0)); }
	inline ListDictionary_t1624492310 * get_list_0() const { return ___list_0; }
	inline ListDictionary_t1624492310 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ListDictionary_t1624492310 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_isKeys_1() { return static_cast<int32_t>(offsetof(NodeKeyValueCollection_t1279341543, ___isKeys_1)); }
	inline bool get_isKeys_1() const { return ___isKeys_1; }
	inline bool* get_address_of_isKeys_1() { return &___isKeys_1; }
	inline void set_isKeys_1(bool value)
	{
		___isKeys_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODEKEYVALUECOLLECTION_T1279341543_H
#ifndef NODEENUMERATOR_T3248827953_H
#define NODEENUMERATOR_T3248827953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.ListDictionary/NodeEnumerator
struct  NodeEnumerator_t3248827953  : public RuntimeObject
{
public:
	// System.Collections.Specialized.ListDictionary System.Collections.Specialized.ListDictionary/NodeEnumerator::list
	ListDictionary_t1624492310 * ___list_0;
	// System.Collections.Specialized.ListDictionary/DictionaryNode System.Collections.Specialized.ListDictionary/NodeEnumerator::current
	DictionaryNode_t417719465 * ___current_1;
	// System.Int32 System.Collections.Specialized.ListDictionary/NodeEnumerator::version
	int32_t ___version_2;
	// System.Boolean System.Collections.Specialized.ListDictionary/NodeEnumerator::start
	bool ___start_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(NodeEnumerator_t3248827953, ___list_0)); }
	inline ListDictionary_t1624492310 * get_list_0() const { return ___list_0; }
	inline ListDictionary_t1624492310 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ListDictionary_t1624492310 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(NodeEnumerator_t3248827953, ___current_1)); }
	inline DictionaryNode_t417719465 * get_current_1() const { return ___current_1; }
	inline DictionaryNode_t417719465 ** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(DictionaryNode_t417719465 * value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier((&___current_1), value);
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(NodeEnumerator_t3248827953, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_start_3() { return static_cast<int32_t>(offsetof(NodeEnumerator_t3248827953, ___start_3)); }
	inline bool get_start_3() const { return ___start_3; }
	inline bool* get_address_of_start_3() { return &___start_3; }
	inline void set_start_3(bool value)
	{
		___start_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODEENUMERATOR_T3248827953_H
#ifndef NODEKEYVALUEENUMERATOR_T642906510_H
#define NODEKEYVALUEENUMERATOR_T642906510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.ListDictionary/NodeKeyValueCollection/NodeKeyValueEnumerator
struct  NodeKeyValueEnumerator_t642906510  : public RuntimeObject
{
public:
	// System.Collections.Specialized.ListDictionary System.Collections.Specialized.ListDictionary/NodeKeyValueCollection/NodeKeyValueEnumerator::list
	ListDictionary_t1624492310 * ___list_0;
	// System.Collections.Specialized.ListDictionary/DictionaryNode System.Collections.Specialized.ListDictionary/NodeKeyValueCollection/NodeKeyValueEnumerator::current
	DictionaryNode_t417719465 * ___current_1;
	// System.Int32 System.Collections.Specialized.ListDictionary/NodeKeyValueCollection/NodeKeyValueEnumerator::version
	int32_t ___version_2;
	// System.Boolean System.Collections.Specialized.ListDictionary/NodeKeyValueCollection/NodeKeyValueEnumerator::isKeys
	bool ___isKeys_3;
	// System.Boolean System.Collections.Specialized.ListDictionary/NodeKeyValueCollection/NodeKeyValueEnumerator::start
	bool ___start_4;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(NodeKeyValueEnumerator_t642906510, ___list_0)); }
	inline ListDictionary_t1624492310 * get_list_0() const { return ___list_0; }
	inline ListDictionary_t1624492310 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ListDictionary_t1624492310 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(NodeKeyValueEnumerator_t642906510, ___current_1)); }
	inline DictionaryNode_t417719465 * get_current_1() const { return ___current_1; }
	inline DictionaryNode_t417719465 ** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(DictionaryNode_t417719465 * value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier((&___current_1), value);
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(NodeKeyValueEnumerator_t642906510, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_isKeys_3() { return static_cast<int32_t>(offsetof(NodeKeyValueEnumerator_t642906510, ___isKeys_3)); }
	inline bool get_isKeys_3() const { return ___isKeys_3; }
	inline bool* get_address_of_isKeys_3() { return &___isKeys_3; }
	inline void set_isKeys_3(bool value)
	{
		___isKeys_3 = value;
	}

	inline static int32_t get_offset_of_start_4() { return static_cast<int32_t>(offsetof(NodeKeyValueEnumerator_t642906510, ___start_4)); }
	inline bool get_start_4() const { return ___start_4; }
	inline bool* get_address_of_start_4() { return &___start_4; }
	inline void set_start_4(bool value)
	{
		___start_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODEKEYVALUEENUMERATOR_T642906510_H
#ifndef DICTIONARYNODE_T417719465_H
#define DICTIONARYNODE_T417719465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.ListDictionary/DictionaryNode
struct  DictionaryNode_t417719465  : public RuntimeObject
{
public:
	// System.Object System.Collections.Specialized.ListDictionary/DictionaryNode::key
	RuntimeObject * ___key_0;
	// System.Object System.Collections.Specialized.ListDictionary/DictionaryNode::value
	RuntimeObject * ___value_1;
	// System.Collections.Specialized.ListDictionary/DictionaryNode System.Collections.Specialized.ListDictionary/DictionaryNode::next
	DictionaryNode_t417719465 * ___next_2;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(DictionaryNode_t417719465, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(DictionaryNode_t417719465, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}

	inline static int32_t get_offset_of_next_2() { return static_cast<int32_t>(offsetof(DictionaryNode_t417719465, ___next_2)); }
	inline DictionaryNode_t417719465 * get_next_2() const { return ___next_2; }
	inline DictionaryNode_t417719465 ** get_address_of_next_2() { return &___next_2; }
	inline void set_next_2(DictionaryNode_t417719465 * value)
	{
		___next_2 = value;
		Il2CppCodeGenWriteBarrier((&___next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYNODE_T417719465_H
#ifndef NAMEOBJECTKEYSENUMERATOR_T3824388371_H
#define NAMEOBJECTKEYSENUMERATOR_T3824388371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase/NameObjectKeysEnumerator
struct  NameObjectKeysEnumerator_t3824388371  : public RuntimeObject
{
public:
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase/NameObjectKeysEnumerator::_pos
	int32_t ____pos_0;
	// System.Collections.Specialized.NameObjectCollectionBase System.Collections.Specialized.NameObjectCollectionBase/NameObjectKeysEnumerator::_coll
	NameObjectCollectionBase_t2091847364 * ____coll_1;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase/NameObjectKeysEnumerator::_version
	int32_t ____version_2;

public:
	inline static int32_t get_offset_of__pos_0() { return static_cast<int32_t>(offsetof(NameObjectKeysEnumerator_t3824388371, ____pos_0)); }
	inline int32_t get__pos_0() const { return ____pos_0; }
	inline int32_t* get_address_of__pos_0() { return &____pos_0; }
	inline void set__pos_0(int32_t value)
	{
		____pos_0 = value;
	}

	inline static int32_t get_offset_of__coll_1() { return static_cast<int32_t>(offsetof(NameObjectKeysEnumerator_t3824388371, ____coll_1)); }
	inline NameObjectCollectionBase_t2091847364 * get__coll_1() const { return ____coll_1; }
	inline NameObjectCollectionBase_t2091847364 ** get_address_of__coll_1() { return &____coll_1; }
	inline void set__coll_1(NameObjectCollectionBase_t2091847364 * value)
	{
		____coll_1 = value;
		Il2CppCodeGenWriteBarrier((&____coll_1), value);
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(NameObjectKeysEnumerator_t3824388371, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTKEYSENUMERATOR_T3824388371_H
#ifndef INSTANCEDESCRIPTOR_T657473484_H
#define INSTANCEDESCRIPTOR_T657473484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Design.Serialization.InstanceDescriptor
struct  InstanceDescriptor_t657473484  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo System.ComponentModel.Design.Serialization.InstanceDescriptor::member
	MemberInfo_t * ___member_0;
	// System.Collections.ICollection System.ComponentModel.Design.Serialization.InstanceDescriptor::arguments
	RuntimeObject* ___arguments_1;
	// System.Boolean System.ComponentModel.Design.Serialization.InstanceDescriptor::isComplete
	bool ___isComplete_2;

public:
	inline static int32_t get_offset_of_member_0() { return static_cast<int32_t>(offsetof(InstanceDescriptor_t657473484, ___member_0)); }
	inline MemberInfo_t * get_member_0() const { return ___member_0; }
	inline MemberInfo_t ** get_address_of_member_0() { return &___member_0; }
	inline void set_member_0(MemberInfo_t * value)
	{
		___member_0 = value;
		Il2CppCodeGenWriteBarrier((&___member_0), value);
	}

	inline static int32_t get_offset_of_arguments_1() { return static_cast<int32_t>(offsetof(InstanceDescriptor_t657473484, ___arguments_1)); }
	inline RuntimeObject* get_arguments_1() const { return ___arguments_1; }
	inline RuntimeObject** get_address_of_arguments_1() { return &___arguments_1; }
	inline void set_arguments_1(RuntimeObject* value)
	{
		___arguments_1 = value;
		Il2CppCodeGenWriteBarrier((&___arguments_1), value);
	}

	inline static int32_t get_offset_of_isComplete_2() { return static_cast<int32_t>(offsetof(InstanceDescriptor_t657473484, ___isComplete_2)); }
	inline bool get_isComplete_2() const { return ___isComplete_2; }
	inline bool* get_address_of_isComplete_2() { return &___isComplete_2; }
	inline void set_isComplete_2(bool value)
	{
		___isComplete_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEDESCRIPTOR_T657473484_H
#ifndef MARSHALBYREFOBJECT_T2760389100_H
#define MARSHALBYREFOBJECT_T2760389100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t2760389100  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t2342208608 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t2760389100, ____identity_0)); }
	inline ServerIdentity_t2342208608 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t2342208608 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t2342208608 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_pinvoke
{
	ServerIdentity_t2342208608 * ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_com
{
	ServerIdentity_t2342208608 * ____identity_0;
};
#endif // MARSHALBYREFOBJECT_T2760389100_H
#ifndef HYBRIDDICTIONARY_T4070033136_H
#define HYBRIDDICTIONARY_T4070033136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.HybridDictionary
struct  HybridDictionary_t4070033136  : public RuntimeObject
{
public:
	// System.Collections.Specialized.ListDictionary System.Collections.Specialized.HybridDictionary::list
	ListDictionary_t1624492310 * ___list_0;
	// System.Collections.Hashtable System.Collections.Specialized.HybridDictionary::hashtable
	Hashtable_t1853889766 * ___hashtable_1;
	// System.Boolean System.Collections.Specialized.HybridDictionary::caseInsensitive
	bool ___caseInsensitive_2;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(HybridDictionary_t4070033136, ___list_0)); }
	inline ListDictionary_t1624492310 * get_list_0() const { return ___list_0; }
	inline ListDictionary_t1624492310 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ListDictionary_t1624492310 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_hashtable_1() { return static_cast<int32_t>(offsetof(HybridDictionary_t4070033136, ___hashtable_1)); }
	inline Hashtable_t1853889766 * get_hashtable_1() const { return ___hashtable_1; }
	inline Hashtable_t1853889766 ** get_address_of_hashtable_1() { return &___hashtable_1; }
	inline void set_hashtable_1(Hashtable_t1853889766 * value)
	{
		___hashtable_1 = value;
		Il2CppCodeGenWriteBarrier((&___hashtable_1), value);
	}

	inline static int32_t get_offset_of_caseInsensitive_2() { return static_cast<int32_t>(offsetof(HybridDictionary_t4070033136, ___caseInsensitive_2)); }
	inline bool get_caseInsensitive_2() const { return ___caseInsensitive_2; }
	inline bool* get_address_of_caseInsensitive_2() { return &___caseInsensitive_2; }
	inline void set_caseInsensitive_2(bool value)
	{
		___caseInsensitive_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HYBRIDDICTIONARY_T4070033136_H
#ifndef COMPATIBLECOMPARER_T4154576053_H
#define COMPATIBLECOMPARER_T4154576053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.CompatibleComparer
struct  CompatibleComparer_t4154576053  : public RuntimeObject
{
public:
	// System.Collections.IComparer System.Collections.Specialized.CompatibleComparer::_comparer
	RuntimeObject* ____comparer_0;
	// System.Collections.IHashCodeProvider System.Collections.Specialized.CompatibleComparer::_hcp
	RuntimeObject* ____hcp_2;

public:
	inline static int32_t get_offset_of__comparer_0() { return static_cast<int32_t>(offsetof(CompatibleComparer_t4154576053, ____comparer_0)); }
	inline RuntimeObject* get__comparer_0() const { return ____comparer_0; }
	inline RuntimeObject** get_address_of__comparer_0() { return &____comparer_0; }
	inline void set__comparer_0(RuntimeObject* value)
	{
		____comparer_0 = value;
		Il2CppCodeGenWriteBarrier((&____comparer_0), value);
	}

	inline static int32_t get_offset_of__hcp_2() { return static_cast<int32_t>(offsetof(CompatibleComparer_t4154576053, ____hcp_2)); }
	inline RuntimeObject* get__hcp_2() const { return ____hcp_2; }
	inline RuntimeObject** get_address_of__hcp_2() { return &____hcp_2; }
	inline void set__hcp_2(RuntimeObject* value)
	{
		____hcp_2 = value;
		Il2CppCodeGenWriteBarrier((&____hcp_2), value);
	}
};

struct CompatibleComparer_t4154576053_StaticFields
{
public:
	// System.Collections.IComparer modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.CompatibleComparer::defaultComparer
	RuntimeObject* ___defaultComparer_1;
	// System.Collections.IHashCodeProvider modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.CompatibleComparer::defaultHashProvider
	RuntimeObject* ___defaultHashProvider_3;

public:
	inline static int32_t get_offset_of_defaultComparer_1() { return static_cast<int32_t>(offsetof(CompatibleComparer_t4154576053_StaticFields, ___defaultComparer_1)); }
	inline RuntimeObject* get_defaultComparer_1() const { return ___defaultComparer_1; }
	inline RuntimeObject** get_address_of_defaultComparer_1() { return &___defaultComparer_1; }
	inline void set_defaultComparer_1(RuntimeObject* value)
	{
		___defaultComparer_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultComparer_1), value);
	}

	inline static int32_t get_offset_of_defaultHashProvider_3() { return static_cast<int32_t>(offsetof(CompatibleComparer_t4154576053_StaticFields, ___defaultHashProvider_3)); }
	inline RuntimeObject* get_defaultHashProvider_3() const { return ___defaultHashProvider_3; }
	inline RuntimeObject** get_address_of_defaultHashProvider_3() { return &___defaultHashProvider_3; }
	inline void set_defaultHashProvider_3(RuntimeObject* value)
	{
		___defaultHashProvider_3 = value;
		Il2CppCodeGenWriteBarrier((&___defaultHashProvider_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPATIBLECOMPARER_T4154576053_H
#ifndef KEYSCOLLECTION_T1318642398_H
#define KEYSCOLLECTION_T1318642398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct  KeysCollection_t1318642398  : public RuntimeObject
{
public:
	// System.Collections.Specialized.NameObjectCollectionBase System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::_coll
	NameObjectCollectionBase_t2091847364 * ____coll_0;

public:
	inline static int32_t get_offset_of__coll_0() { return static_cast<int32_t>(offsetof(KeysCollection_t1318642398, ____coll_0)); }
	inline NameObjectCollectionBase_t2091847364 * get__coll_0() const { return ____coll_0; }
	inline NameObjectCollectionBase_t2091847364 ** get_address_of__coll_0() { return &____coll_0; }
	inline void set__coll_0(NameObjectCollectionBase_t2091847364 * value)
	{
		____coll_0 = value;
		Il2CppCodeGenWriteBarrier((&____coll_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYSCOLLECTION_T1318642398_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T2710994319_H
#define __STATICARRAYINITTYPESIZEU3D12_T2710994319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_t2710994319 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t2710994319__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T2710994319_H
#ifndef __STATICARRAYINITTYPESIZEU3D6_T3217689076_H
#define __STATICARRAYINITTYPESIZEU3D6_T3217689076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6
struct  __StaticArrayInitTypeSizeU3D6_t3217689076 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D6_t3217689076__padding[6];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D6_T3217689076_H
#ifndef __STATICARRAYINITTYPESIZEU3D9_T3218278900_H
#define __STATICARRAYINITTYPESIZEU3D9_T3218278900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=9
struct  __StaticArrayInitTypeSizeU3D9_t3218278900 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D9_t3218278900__padding[9];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D9_T3218278900_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef GCHANDLE_T3351438187_H
#define GCHANDLE_T3351438187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t3351438187 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t3351438187, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T3351438187_H
#ifndef __STATICARRAYINITTYPESIZEU3D3_T3217885684_H
#define __STATICARRAYINITTYPESIZEU3D3_T3217885684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3
struct  __StaticArrayInitTypeSizeU3D3_t3217885684 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D3_t3217885684__padding[3];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D3_T3217885684_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public MarshalByRefObject_t2760389100
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t156472862 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2974092902 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_t1273022909, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_t156472862 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_t156472862 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_t156472862 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_t1273022909, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2974092902 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2974092902 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2974092902 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_1)); }
	inline Stream_t1273022909 * get_Null_1() const { return ___Null_1; }
	inline Stream_t1273022909 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t1273022909 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef COMPONENT_T3620823400_H
#define COMPONENT_T3620823400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Component
struct  Component_t3620823400  : public MarshalByRefObject_t2760389100
{
public:
	// System.ComponentModel.ISite System.ComponentModel.Component::site
	RuntimeObject* ___site_2;
	// System.ComponentModel.EventHandlerList System.ComponentModel.Component::events
	EventHandlerList_t1108123056 * ___events_3;

public:
	inline static int32_t get_offset_of_site_2() { return static_cast<int32_t>(offsetof(Component_t3620823400, ___site_2)); }
	inline RuntimeObject* get_site_2() const { return ___site_2; }
	inline RuntimeObject** get_address_of_site_2() { return &___site_2; }
	inline void set_site_2(RuntimeObject* value)
	{
		___site_2 = value;
		Il2CppCodeGenWriteBarrier((&___site_2), value);
	}

	inline static int32_t get_offset_of_events_3() { return static_cast<int32_t>(offsetof(Component_t3620823400, ___events_3)); }
	inline EventHandlerList_t1108123056 * get_events_3() const { return ___events_3; }
	inline EventHandlerList_t1108123056 ** get_address_of_events_3() { return &___events_3; }
	inline void set_events_3(EventHandlerList_t1108123056 * value)
	{
		___events_3 = value;
		Il2CppCodeGenWriteBarrier((&___events_3), value);
	}
};

struct Component_t3620823400_StaticFields
{
public:
	// System.Object System.ComponentModel.Component::EventDisposed
	RuntimeObject * ___EventDisposed_1;

public:
	inline static int32_t get_offset_of_EventDisposed_1() { return static_cast<int32_t>(offsetof(Component_t3620823400_StaticFields, ___EventDisposed_1)); }
	inline RuntimeObject * get_EventDisposed_1() const { return ___EventDisposed_1; }
	inline RuntimeObject ** get_address_of_EventDisposed_1() { return &___EventDisposed_1; }
	inline void set_EventDisposed_1(RuntimeObject * value)
	{
		___EventDisposed_1 = value;
		Il2CppCodeGenWriteBarrier((&___EventDisposed_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3620823400_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t385246372* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t385246372* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_31)); }
	inline DateTime_t3738529785  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t3738529785 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t3738529785  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_32)); }
	inline DateTime_t3738529785  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t3738529785  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef DESCRIPTIONATTRIBUTE_T874390736_H
#define DESCRIPTIONATTRIBUTE_T874390736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DescriptionAttribute
struct  DescriptionAttribute_t874390736  : public Attribute_t861562559
{
public:
	// System.String System.ComponentModel.DescriptionAttribute::description
	String_t* ___description_1;

public:
	inline static int32_t get_offset_of_description_1() { return static_cast<int32_t>(offsetof(DescriptionAttribute_t874390736, ___description_1)); }
	inline String_t* get_description_1() const { return ___description_1; }
	inline String_t** get_address_of_description_1() { return &___description_1; }
	inline void set_description_1(String_t* value)
	{
		___description_1 = value;
		Il2CppCodeGenWriteBarrier((&___description_1), value);
	}
};

struct DescriptionAttribute_t874390736_StaticFields
{
public:
	// System.ComponentModel.DescriptionAttribute System.ComponentModel.DescriptionAttribute::Default
	DescriptionAttribute_t874390736 * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(DescriptionAttribute_t874390736_StaticFields, ___Default_0)); }
	inline DescriptionAttribute_t874390736 * get_Default_0() const { return ___Default_0; }
	inline DescriptionAttribute_t874390736 ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(DescriptionAttribute_t874390736 * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((&___Default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESCRIPTIONATTRIBUTE_T874390736_H
#ifndef UINTPTR_T_H
#define UINTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UIntPtr
struct  UIntPtr_t 
{
public:
	// System.Void* System.UIntPtr::_pointer
	void* ____pointer_1;

public:
	inline static int32_t get_offset_of__pointer_1() { return static_cast<int32_t>(offsetof(UIntPtr_t, ____pointer_1)); }
	inline void* get__pointer_1() const { return ____pointer_1; }
	inline void** get_address_of__pointer_1() { return &____pointer_1; }
	inline void set__pointer_1(void* value)
	{
		____pointer_1 = value;
	}
};

struct UIntPtr_t_StaticFields
{
public:
	// System.UIntPtr System.UIntPtr::Zero
	uintptr_t ___Zero_0;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(UIntPtr_t_StaticFields, ___Zero_0)); }
	inline uintptr_t get_Zero_0() const { return ___Zero_0; }
	inline uintptr_t* get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(uintptr_t value)
	{
		___Zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINTPTR_T_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef __STATICARRAYINITTYPESIZEU3D44_T3517366765_H
#define __STATICARRAYINITTYPESIZEU3D44_T3517366765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44
struct  __StaticArrayInitTypeSizeU3D44_t3517366765 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D44_t3517366765__padding[44];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D44_T3517366765_H
#ifndef __STATICARRAYINITTYPESIZEU3D32_T2711125392_H
#define __STATICARRAYINITTYPESIZEU3D32_T2711125392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32
struct  __StaticArrayInitTypeSizeU3D32_t2711125392 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t2711125392__padding[32];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D32_T2711125392_H
#ifndef __STATICARRAYINITTYPESIZEU3D14_T3517563373_H
#define __STATICARRAYINITTYPESIZEU3D14_T3517563373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=14
struct  __StaticArrayInitTypeSizeU3D14_t3517563373 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D14_t3517563373__padding[14];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D14_T3517563373_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef __STATICARRAYINITTYPESIZEU3D256_T1757367633_H
#define __STATICARRAYINITTYPESIZEU3D256_T1757367633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256
struct  __StaticArrayInitTypeSizeU3D256_t1757367633 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D256_t1757367633__padding[256];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D256_T1757367633_H
#ifndef __STATICARRAYINITTYPESIZEU3D128_T531529102_H
#define __STATICARRAYINITTYPESIZEU3D128_T531529102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128
struct  __StaticArrayInitTypeSizeU3D128_t531529102 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D128_t531529102__padding[128];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D128_T531529102_H
#ifndef CASESENSITIVESTRINGDICTIONARY_T553067329_H
#define CASESENSITIVESTRINGDICTIONARY_T553067329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.CaseSensitiveStringDictionary
struct  CaseSensitiveStringDictionary_t553067329  : public StringDictionary_t120437468
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASESENSITIVESTRINGDICTIONARY_T553067329_H
#ifndef FAMREQUEST_T3578860103_H
#define FAMREQUEST_T3578860103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FAMRequest
struct  FAMRequest_t3578860103 
{
public:
	// System.Int32 System.IO.FAMRequest::ReqNum
	int32_t ___ReqNum_0;

public:
	inline static int32_t get_offset_of_ReqNum_0() { return static_cast<int32_t>(offsetof(FAMRequest_t3578860103, ___ReqNum_0)); }
	inline int32_t get_ReqNum_0() const { return ___ReqNum_0; }
	inline int32_t* get_address_of_ReqNum_0() { return &___ReqNum_0; }
	inline void set_ReqNum_0(int32_t value)
	{
		___ReqNum_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAMREQUEST_T3578860103_H
#ifndef ERROREVENTARGS_T1584858912_H
#define ERROREVENTARGS_T1584858912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.ErrorEventArgs
struct  ErrorEventArgs_t1584858912  : public EventArgs_t3591816995
{
public:
	// System.Exception System.IO.ErrorEventArgs::exception
	Exception_t1436737249 * ___exception_1;

public:
	inline static int32_t get_offset_of_exception_1() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t1584858912, ___exception_1)); }
	inline Exception_t1436737249 * get_exception_1() const { return ___exception_1; }
	inline Exception_t1436737249 ** get_address_of_exception_1() { return &___exception_1; }
	inline void set_exception_1(Exception_t1436737249 * value)
	{
		___exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___exception_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTARGS_T1584858912_H
#ifndef ROOTDESIGNERSERIALIZERATTRIBUTE_T3074689342_H
#define ROOTDESIGNERSERIALIZERATTRIBUTE_T3074689342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Design.Serialization.RootDesignerSerializerAttribute
struct  RootDesignerSerializerAttribute_t3074689342  : public Attribute_t861562559
{
public:
	// System.Boolean System.ComponentModel.Design.Serialization.RootDesignerSerializerAttribute::reloadable
	bool ___reloadable_0;
	// System.String System.ComponentModel.Design.Serialization.RootDesignerSerializerAttribute::serializerTypeName
	String_t* ___serializerTypeName_1;
	// System.String System.ComponentModel.Design.Serialization.RootDesignerSerializerAttribute::serializerBaseTypeName
	String_t* ___serializerBaseTypeName_2;
	// System.String System.ComponentModel.Design.Serialization.RootDesignerSerializerAttribute::typeId
	String_t* ___typeId_3;

public:
	inline static int32_t get_offset_of_reloadable_0() { return static_cast<int32_t>(offsetof(RootDesignerSerializerAttribute_t3074689342, ___reloadable_0)); }
	inline bool get_reloadable_0() const { return ___reloadable_0; }
	inline bool* get_address_of_reloadable_0() { return &___reloadable_0; }
	inline void set_reloadable_0(bool value)
	{
		___reloadable_0 = value;
	}

	inline static int32_t get_offset_of_serializerTypeName_1() { return static_cast<int32_t>(offsetof(RootDesignerSerializerAttribute_t3074689342, ___serializerTypeName_1)); }
	inline String_t* get_serializerTypeName_1() const { return ___serializerTypeName_1; }
	inline String_t** get_address_of_serializerTypeName_1() { return &___serializerTypeName_1; }
	inline void set_serializerTypeName_1(String_t* value)
	{
		___serializerTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___serializerTypeName_1), value);
	}

	inline static int32_t get_offset_of_serializerBaseTypeName_2() { return static_cast<int32_t>(offsetof(RootDesignerSerializerAttribute_t3074689342, ___serializerBaseTypeName_2)); }
	inline String_t* get_serializerBaseTypeName_2() const { return ___serializerBaseTypeName_2; }
	inline String_t** get_address_of_serializerBaseTypeName_2() { return &___serializerBaseTypeName_2; }
	inline void set_serializerBaseTypeName_2(String_t* value)
	{
		___serializerBaseTypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___serializerBaseTypeName_2), value);
	}

	inline static int32_t get_offset_of_typeId_3() { return static_cast<int32_t>(offsetof(RootDesignerSerializerAttribute_t3074689342, ___typeId_3)); }
	inline String_t* get_typeId_3() const { return ___typeId_3; }
	inline String_t** get_address_of_typeId_3() { return &___typeId_3; }
	inline void set_typeId_3(String_t* value)
	{
		___typeId_3 = value;
		Il2CppCodeGenWriteBarrier((&___typeId_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOTDESIGNERSERIALIZERATTRIBUTE_T3074689342_H
#ifndef DESIGNERSERIALIZERATTRIBUTE_T1570548024_H
#define DESIGNERSERIALIZERATTRIBUTE_T1570548024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Design.Serialization.DesignerSerializerAttribute
struct  DesignerSerializerAttribute_t1570548024  : public Attribute_t861562559
{
public:
	// System.String System.ComponentModel.Design.Serialization.DesignerSerializerAttribute::serializerTypeName
	String_t* ___serializerTypeName_0;
	// System.String System.ComponentModel.Design.Serialization.DesignerSerializerAttribute::serializerBaseTypeName
	String_t* ___serializerBaseTypeName_1;
	// System.String System.ComponentModel.Design.Serialization.DesignerSerializerAttribute::typeId
	String_t* ___typeId_2;

public:
	inline static int32_t get_offset_of_serializerTypeName_0() { return static_cast<int32_t>(offsetof(DesignerSerializerAttribute_t1570548024, ___serializerTypeName_0)); }
	inline String_t* get_serializerTypeName_0() const { return ___serializerTypeName_0; }
	inline String_t** get_address_of_serializerTypeName_0() { return &___serializerTypeName_0; }
	inline void set_serializerTypeName_0(String_t* value)
	{
		___serializerTypeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___serializerTypeName_0), value);
	}

	inline static int32_t get_offset_of_serializerBaseTypeName_1() { return static_cast<int32_t>(offsetof(DesignerSerializerAttribute_t1570548024, ___serializerBaseTypeName_1)); }
	inline String_t* get_serializerBaseTypeName_1() const { return ___serializerBaseTypeName_1; }
	inline String_t** get_address_of_serializerBaseTypeName_1() { return &___serializerBaseTypeName_1; }
	inline void set_serializerBaseTypeName_1(String_t* value)
	{
		___serializerBaseTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___serializerBaseTypeName_1), value);
	}

	inline static int32_t get_offset_of_typeId_2() { return static_cast<int32_t>(offsetof(DesignerSerializerAttribute_t1570548024, ___typeId_2)); }
	inline String_t* get_typeId_2() const { return ___typeId_2; }
	inline String_t** get_address_of_typeId_2() { return &___typeId_2; }
	inline void set_typeId_2(String_t* value)
	{
		___typeId_2 = value;
		Il2CppCodeGenWriteBarrier((&___typeId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESIGNERSERIALIZERATTRIBUTE_T1570548024_H
#ifndef NAMEVALUECOLLECTION_T407452768_H
#define NAMEVALUECOLLECTION_T407452768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameValueCollection
struct  NameValueCollection_t407452768  : public NameObjectCollectionBase_t2091847364
{
public:
	// System.String[] System.Collections.Specialized.NameValueCollection::_all
	StringU5BU5D_t1281789340* ____all_10;
	// System.String[] System.Collections.Specialized.NameValueCollection::_allKeys
	StringU5BU5D_t1281789340* ____allKeys_11;

public:
	inline static int32_t get_offset_of__all_10() { return static_cast<int32_t>(offsetof(NameValueCollection_t407452768, ____all_10)); }
	inline StringU5BU5D_t1281789340* get__all_10() const { return ____all_10; }
	inline StringU5BU5D_t1281789340** get_address_of__all_10() { return &____all_10; }
	inline void set__all_10(StringU5BU5D_t1281789340* value)
	{
		____all_10 = value;
		Il2CppCodeGenWriteBarrier((&____all_10), value);
	}

	inline static int32_t get_offset_of__allKeys_11() { return static_cast<int32_t>(offsetof(NameValueCollection_t407452768, ____allKeys_11)); }
	inline StringU5BU5D_t1281789340* get__allKeys_11() const { return ____allKeys_11; }
	inline StringU5BU5D_t1281789340** get_address_of__allKeys_11() { return &____allKeys_11; }
	inline void set__allKeys_11(StringU5BU5D_t1281789340* value)
	{
		____allKeys_11 = value;
		Il2CppCodeGenWriteBarrier((&____allKeys_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUECOLLECTION_T407452768_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255365  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=14 <PrivateImplementationDetails>::0283A6AF88802AB45989B29549915BEA0F6CD515
	__StaticArrayInitTypeSizeU3D14_t3517563373  ___0283A6AF88802AB45989B29549915BEA0F6CD515_0;
	// System.Int64 <PrivateImplementationDetails>::03F4297FCC30D0FD5E420E5D26E7FA711167C7EF
	int64_t ___03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=9 <PrivateImplementationDetails>::1A39764B112685485A5BA7B2880D878B858C1A7A
	__StaticArrayInitTypeSizeU3D9_t3218278900  ___1A39764B112685485A5BA7B2880D878B858C1A7A_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::1A84029C80CB5518379F199F53FF08A7B764F8FD
	__StaticArrayInitTypeSizeU3D3_t3217885684  ___1A84029C80CB5518379F199F53FF08A7B764F8FD_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::3BE77BF818331C2D8400FFFFF9FADD3F16AD89AC
	__StaticArrayInitTypeSizeU3D12_t2710994319  ___3BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::59F5BD34B6C013DEACC784F69C67E95150033A84
	__StaticArrayInitTypeSizeU3D32_t2711125392  ___59F5BD34B6C013DEACC784F69C67E95150033A84_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::5BC3486B05BA8CF4689C7BDB198B3F477BB4E20C
	__StaticArrayInitTypeSizeU3D6_t3217689076  ___5BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=9 <PrivateImplementationDetails>::6D49C9D487D7AD3491ECE08732D68A593CC2038D
	__StaticArrayInitTypeSizeU3D9_t3218278900  ___6D49C9D487D7AD3491ECE08732D68A593CC2038D_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128 <PrivateImplementationDetails>::6F3AD3DC3AF8047587C4C9D696EB68A01FEF796E
	__StaticArrayInitTypeSizeU3D128_t531529102  ___6F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44 <PrivateImplementationDetails>::8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3
	__StaticArrayInitTypeSizeU3D44_t3517366765  ___8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9;
	// System.Int64 <PrivateImplementationDetails>::98A44A6F8606AE6F23FE230286C1D6FBCC407226
	int64_t ___98A44A6F8606AE6F23FE230286C1D6FBCC407226_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536
	__StaticArrayInitTypeSizeU3D32_t2711125392  ___C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128 <PrivateImplementationDetails>::CCEEADA43268372341F81AE0C9208C6856441C04
	__StaticArrayInitTypeSizeU3D128_t531529102  ___CCEEADA43268372341F81AE0C9208C6856441C04_12;
	// System.Int64 <PrivateImplementationDetails>::E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78
	int64_t ___E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::EC5842B3154E1AF94500B57220EB9F684BCCC42A
	__StaticArrayInitTypeSizeU3D32_t2711125392  ___EC5842B3154E1AF94500B57220EB9F684BCCC42A_14;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256 <PrivateImplementationDetails>::EEAFE8C6E1AB017237567305EE925C976CDB6458
	__StaticArrayInitTypeSizeU3D256_t1757367633  ___EEAFE8C6E1AB017237567305EE925C976CDB6458_15;

public:
	inline static int32_t get_offset_of_U30283A6AF88802AB45989B29549915BEA0F6CD515_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___0283A6AF88802AB45989B29549915BEA0F6CD515_0)); }
	inline __StaticArrayInitTypeSizeU3D14_t3517563373  get_U30283A6AF88802AB45989B29549915BEA0F6CD515_0() const { return ___0283A6AF88802AB45989B29549915BEA0F6CD515_0; }
	inline __StaticArrayInitTypeSizeU3D14_t3517563373 * get_address_of_U30283A6AF88802AB45989B29549915BEA0F6CD515_0() { return &___0283A6AF88802AB45989B29549915BEA0F6CD515_0; }
	inline void set_U30283A6AF88802AB45989B29549915BEA0F6CD515_0(__StaticArrayInitTypeSizeU3D14_t3517563373  value)
	{
		___0283A6AF88802AB45989B29549915BEA0F6CD515_0 = value;
	}

	inline static int32_t get_offset_of_U303F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1)); }
	inline int64_t get_U303F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1() const { return ___03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1; }
	inline int64_t* get_address_of_U303F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1() { return &___03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1; }
	inline void set_U303F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1(int64_t value)
	{
		___03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1 = value;
	}

	inline static int32_t get_offset_of_U31A39764B112685485A5BA7B2880D878B858C1A7A_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___1A39764B112685485A5BA7B2880D878B858C1A7A_2)); }
	inline __StaticArrayInitTypeSizeU3D9_t3218278900  get_U31A39764B112685485A5BA7B2880D878B858C1A7A_2() const { return ___1A39764B112685485A5BA7B2880D878B858C1A7A_2; }
	inline __StaticArrayInitTypeSizeU3D9_t3218278900 * get_address_of_U31A39764B112685485A5BA7B2880D878B858C1A7A_2() { return &___1A39764B112685485A5BA7B2880D878B858C1A7A_2; }
	inline void set_U31A39764B112685485A5BA7B2880D878B858C1A7A_2(__StaticArrayInitTypeSizeU3D9_t3218278900  value)
	{
		___1A39764B112685485A5BA7B2880D878B858C1A7A_2 = value;
	}

	inline static int32_t get_offset_of_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___1A84029C80CB5518379F199F53FF08A7B764F8FD_3)); }
	inline __StaticArrayInitTypeSizeU3D3_t3217885684  get_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3() const { return ___1A84029C80CB5518379F199F53FF08A7B764F8FD_3; }
	inline __StaticArrayInitTypeSizeU3D3_t3217885684 * get_address_of_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3() { return &___1A84029C80CB5518379F199F53FF08A7B764F8FD_3; }
	inline void set_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3(__StaticArrayInitTypeSizeU3D3_t3217885684  value)
	{
		___1A84029C80CB5518379F199F53FF08A7B764F8FD_3 = value;
	}

	inline static int32_t get_offset_of_U33BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___3BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4)); }
	inline __StaticArrayInitTypeSizeU3D12_t2710994319  get_U33BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4() const { return ___3BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4; }
	inline __StaticArrayInitTypeSizeU3D12_t2710994319 * get_address_of_U33BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4() { return &___3BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4; }
	inline void set_U33BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4(__StaticArrayInitTypeSizeU3D12_t2710994319  value)
	{
		___3BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4 = value;
	}

	inline static int32_t get_offset_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___59F5BD34B6C013DEACC784F69C67E95150033A84_5)); }
	inline __StaticArrayInitTypeSizeU3D32_t2711125392  get_U359F5BD34B6C013DEACC784F69C67E95150033A84_5() const { return ___59F5BD34B6C013DEACC784F69C67E95150033A84_5; }
	inline __StaticArrayInitTypeSizeU3D32_t2711125392 * get_address_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_5() { return &___59F5BD34B6C013DEACC784F69C67E95150033A84_5; }
	inline void set_U359F5BD34B6C013DEACC784F69C67E95150033A84_5(__StaticArrayInitTypeSizeU3D32_t2711125392  value)
	{
		___59F5BD34B6C013DEACC784F69C67E95150033A84_5 = value;
	}

	inline static int32_t get_offset_of_U35BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___5BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6)); }
	inline __StaticArrayInitTypeSizeU3D6_t3217689076  get_U35BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6() const { return ___5BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6; }
	inline __StaticArrayInitTypeSizeU3D6_t3217689076 * get_address_of_U35BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6() { return &___5BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6; }
	inline void set_U35BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6(__StaticArrayInitTypeSizeU3D6_t3217689076  value)
	{
		___5BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6 = value;
	}

	inline static int32_t get_offset_of_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___6D49C9D487D7AD3491ECE08732D68A593CC2038D_7)); }
	inline __StaticArrayInitTypeSizeU3D9_t3218278900  get_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_7() const { return ___6D49C9D487D7AD3491ECE08732D68A593CC2038D_7; }
	inline __StaticArrayInitTypeSizeU3D9_t3218278900 * get_address_of_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_7() { return &___6D49C9D487D7AD3491ECE08732D68A593CC2038D_7; }
	inline void set_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_7(__StaticArrayInitTypeSizeU3D9_t3218278900  value)
	{
		___6D49C9D487D7AD3491ECE08732D68A593CC2038D_7 = value;
	}

	inline static int32_t get_offset_of_U36F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___6F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8)); }
	inline __StaticArrayInitTypeSizeU3D128_t531529102  get_U36F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8() const { return ___6F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8; }
	inline __StaticArrayInitTypeSizeU3D128_t531529102 * get_address_of_U36F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8() { return &___6F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8; }
	inline void set_U36F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8(__StaticArrayInitTypeSizeU3D128_t531529102  value)
	{
		___6F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8 = value;
	}

	inline static int32_t get_offset_of_U38E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9)); }
	inline __StaticArrayInitTypeSizeU3D44_t3517366765  get_U38E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9() const { return ___8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9; }
	inline __StaticArrayInitTypeSizeU3D44_t3517366765 * get_address_of_U38E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9() { return &___8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9; }
	inline void set_U38E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9(__StaticArrayInitTypeSizeU3D44_t3517366765  value)
	{
		___8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9 = value;
	}

	inline static int32_t get_offset_of_U398A44A6F8606AE6F23FE230286C1D6FBCC407226_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___98A44A6F8606AE6F23FE230286C1D6FBCC407226_10)); }
	inline int64_t get_U398A44A6F8606AE6F23FE230286C1D6FBCC407226_10() const { return ___98A44A6F8606AE6F23FE230286C1D6FBCC407226_10; }
	inline int64_t* get_address_of_U398A44A6F8606AE6F23FE230286C1D6FBCC407226_10() { return &___98A44A6F8606AE6F23FE230286C1D6FBCC407226_10; }
	inline void set_U398A44A6F8606AE6F23FE230286C1D6FBCC407226_10(int64_t value)
	{
		___98A44A6F8606AE6F23FE230286C1D6FBCC407226_10 = value;
	}

	inline static int32_t get_offset_of_C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11)); }
	inline __StaticArrayInitTypeSizeU3D32_t2711125392  get_C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11() const { return ___C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11; }
	inline __StaticArrayInitTypeSizeU3D32_t2711125392 * get_address_of_C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11() { return &___C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11; }
	inline void set_C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11(__StaticArrayInitTypeSizeU3D32_t2711125392  value)
	{
		___C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11 = value;
	}

	inline static int32_t get_offset_of_CCEEADA43268372341F81AE0C9208C6856441C04_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___CCEEADA43268372341F81AE0C9208C6856441C04_12)); }
	inline __StaticArrayInitTypeSizeU3D128_t531529102  get_CCEEADA43268372341F81AE0C9208C6856441C04_12() const { return ___CCEEADA43268372341F81AE0C9208C6856441C04_12; }
	inline __StaticArrayInitTypeSizeU3D128_t531529102 * get_address_of_CCEEADA43268372341F81AE0C9208C6856441C04_12() { return &___CCEEADA43268372341F81AE0C9208C6856441C04_12; }
	inline void set_CCEEADA43268372341F81AE0C9208C6856441C04_12(__StaticArrayInitTypeSizeU3D128_t531529102  value)
	{
		___CCEEADA43268372341F81AE0C9208C6856441C04_12 = value;
	}

	inline static int32_t get_offset_of_E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13)); }
	inline int64_t get_E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13() const { return ___E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13; }
	inline int64_t* get_address_of_E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13() { return &___E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13; }
	inline void set_E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13(int64_t value)
	{
		___E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13 = value;
	}

	inline static int32_t get_offset_of_EC5842B3154E1AF94500B57220EB9F684BCCC42A_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___EC5842B3154E1AF94500B57220EB9F684BCCC42A_14)); }
	inline __StaticArrayInitTypeSizeU3D32_t2711125392  get_EC5842B3154E1AF94500B57220EB9F684BCCC42A_14() const { return ___EC5842B3154E1AF94500B57220EB9F684BCCC42A_14; }
	inline __StaticArrayInitTypeSizeU3D32_t2711125392 * get_address_of_EC5842B3154E1AF94500B57220EB9F684BCCC42A_14() { return &___EC5842B3154E1AF94500B57220EB9F684BCCC42A_14; }
	inline void set_EC5842B3154E1AF94500B57220EB9F684BCCC42A_14(__StaticArrayInitTypeSizeU3D32_t2711125392  value)
	{
		___EC5842B3154E1AF94500B57220EB9F684BCCC42A_14 = value;
	}

	inline static int32_t get_offset_of_EEAFE8C6E1AB017237567305EE925C976CDB6458_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___EEAFE8C6E1AB017237567305EE925C976CDB6458_15)); }
	inline __StaticArrayInitTypeSizeU3D256_t1757367633  get_EEAFE8C6E1AB017237567305EE925C976CDB6458_15() const { return ___EEAFE8C6E1AB017237567305EE925C976CDB6458_15; }
	inline __StaticArrayInitTypeSizeU3D256_t1757367633 * get_address_of_EEAFE8C6E1AB017237567305EE925C976CDB6458_15() { return &___EEAFE8C6E1AB017237567305EE925C976CDB6458_15; }
	inline void set_EEAFE8C6E1AB017237567305EE925C976CDB6458_15(__StaticArrayInitTypeSizeU3D256_t1757367633  value)
	{
		___EEAFE8C6E1AB017237567305EE925C976CDB6458_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifndef FILEACTION_T3839877343_H
#define FILEACTION_T3839877343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAction
struct  FileAction_t3839877343 
{
public:
	// System.Int32 System.IO.FileAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAction_t3839877343, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEACTION_T3839877343_H
#ifndef FAMDATA_T1817296356_H
#define FAMDATA_T1817296356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FAMData
struct  FAMData_t1817296356  : public RuntimeObject
{
public:
	// System.IO.FileSystemWatcher System.IO.FAMData::FSW
	FileSystemWatcher_t416760199 * ___FSW_0;
	// System.String System.IO.FAMData::Directory
	String_t* ___Directory_1;
	// System.String System.IO.FAMData::FileMask
	String_t* ___FileMask_2;
	// System.Boolean System.IO.FAMData::IncludeSubdirs
	bool ___IncludeSubdirs_3;
	// System.Boolean System.IO.FAMData::Enabled
	bool ___Enabled_4;
	// System.IO.FAMRequest System.IO.FAMData::Request
	FAMRequest_t3578860103  ___Request_5;
	// System.Collections.Hashtable System.IO.FAMData::SubDirs
	Hashtable_t1853889766 * ___SubDirs_6;

public:
	inline static int32_t get_offset_of_FSW_0() { return static_cast<int32_t>(offsetof(FAMData_t1817296356, ___FSW_0)); }
	inline FileSystemWatcher_t416760199 * get_FSW_0() const { return ___FSW_0; }
	inline FileSystemWatcher_t416760199 ** get_address_of_FSW_0() { return &___FSW_0; }
	inline void set_FSW_0(FileSystemWatcher_t416760199 * value)
	{
		___FSW_0 = value;
		Il2CppCodeGenWriteBarrier((&___FSW_0), value);
	}

	inline static int32_t get_offset_of_Directory_1() { return static_cast<int32_t>(offsetof(FAMData_t1817296356, ___Directory_1)); }
	inline String_t* get_Directory_1() const { return ___Directory_1; }
	inline String_t** get_address_of_Directory_1() { return &___Directory_1; }
	inline void set_Directory_1(String_t* value)
	{
		___Directory_1 = value;
		Il2CppCodeGenWriteBarrier((&___Directory_1), value);
	}

	inline static int32_t get_offset_of_FileMask_2() { return static_cast<int32_t>(offsetof(FAMData_t1817296356, ___FileMask_2)); }
	inline String_t* get_FileMask_2() const { return ___FileMask_2; }
	inline String_t** get_address_of_FileMask_2() { return &___FileMask_2; }
	inline void set_FileMask_2(String_t* value)
	{
		___FileMask_2 = value;
		Il2CppCodeGenWriteBarrier((&___FileMask_2), value);
	}

	inline static int32_t get_offset_of_IncludeSubdirs_3() { return static_cast<int32_t>(offsetof(FAMData_t1817296356, ___IncludeSubdirs_3)); }
	inline bool get_IncludeSubdirs_3() const { return ___IncludeSubdirs_3; }
	inline bool* get_address_of_IncludeSubdirs_3() { return &___IncludeSubdirs_3; }
	inline void set_IncludeSubdirs_3(bool value)
	{
		___IncludeSubdirs_3 = value;
	}

	inline static int32_t get_offset_of_Enabled_4() { return static_cast<int32_t>(offsetof(FAMData_t1817296356, ___Enabled_4)); }
	inline bool get_Enabled_4() const { return ___Enabled_4; }
	inline bool* get_address_of_Enabled_4() { return &___Enabled_4; }
	inline void set_Enabled_4(bool value)
	{
		___Enabled_4 = value;
	}

	inline static int32_t get_offset_of_Request_5() { return static_cast<int32_t>(offsetof(FAMData_t1817296356, ___Request_5)); }
	inline FAMRequest_t3578860103  get_Request_5() const { return ___Request_5; }
	inline FAMRequest_t3578860103 * get_address_of_Request_5() { return &___Request_5; }
	inline void set_Request_5(FAMRequest_t3578860103  value)
	{
		___Request_5 = value;
	}

	inline static int32_t get_offset_of_SubDirs_6() { return static_cast<int32_t>(offsetof(FAMData_t1817296356, ___SubDirs_6)); }
	inline Hashtable_t1853889766 * get_SubDirs_6() const { return ___SubDirs_6; }
	inline Hashtable_t1853889766 ** get_address_of_SubDirs_6() { return &___SubDirs_6; }
	inline void set_SubDirs_6(Hashtable_t1853889766 * value)
	{
		___SubDirs_6 = value;
		Il2CppCodeGenWriteBarrier((&___SubDirs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAMDATA_T1817296356_H
#ifndef INOTIFYMASK_T3323194736_H
#define INOTIFYMASK_T3323194736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.InotifyMask
struct  InotifyMask_t3323194736 
{
public:
	// System.UInt32 System.IO.InotifyMask::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InotifyMask_t3323194736, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INOTIFYMASK_T3323194736_H
#ifndef IODESCRIPTIONATTRIBUTE_T2336130459_H
#define IODESCRIPTIONATTRIBUTE_T2336130459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.IODescriptionAttribute
struct  IODescriptionAttribute_t2336130459  : public DescriptionAttribute_t874390736
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IODESCRIPTIONATTRIBUTE_T2336130459_H
#ifndef EVENTTYPE_T2132206657_H
#define EVENTTYPE_T2132206657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemWatcher/EventType
struct  EventType_t2132206657 
{
public:
	// System.Int32 System.IO.FileSystemWatcher/EventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventType_t2132206657, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTYPE_T2132206657_H
#ifndef FAMCODES_T1537683418_H
#define FAMCODES_T1537683418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FAMCodes
struct  FAMCodes_t1537683418 
{
public:
	// System.Int32 System.IO.FAMCodes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FAMCodes_t1537683418, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAMCODES_T1537683418_H
#ifndef NOTIFYCOLLECTIONCHANGEDACTION_T1056453382_H
#define NOTIFYCOLLECTIONCHANGEDACTION_T1056453382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NotifyCollectionChangedAction
struct  NotifyCollectionChangedAction_t1056453382 
{
public:
	// System.Int32 System.Collections.Specialized.NotifyCollectionChangedAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedAction_t1056453382, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFYCOLLECTIONCHANGEDACTION_T1056453382_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef TIMESPEC_T893749660_H
#define TIMESPEC_T893749660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.timespec
struct  timespec_t893749660 
{
public:
	// System.IntPtr System.IO.timespec::tv_sec
	intptr_t ___tv_sec_0;
	// System.IntPtr System.IO.timespec::tv_nsec
	intptr_t ___tv_nsec_1;

public:
	inline static int32_t get_offset_of_tv_sec_0() { return static_cast<int32_t>(offsetof(timespec_t893749660, ___tv_sec_0)); }
	inline intptr_t get_tv_sec_0() const { return ___tv_sec_0; }
	inline intptr_t* get_address_of_tv_sec_0() { return &___tv_sec_0; }
	inline void set_tv_sec_0(intptr_t value)
	{
		___tv_sec_0 = value;
	}

	inline static int32_t get_offset_of_tv_nsec_1() { return static_cast<int32_t>(offsetof(timespec_t893749660, ___tv_nsec_1)); }
	inline intptr_t get_tv_nsec_1() const { return ___tv_nsec_1; }
	inline intptr_t* get_address_of_tv_nsec_1() { return &___tv_nsec_1; }
	inline void set_tv_nsec_1(intptr_t value)
	{
		___tv_nsec_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPEC_T893749660_H
#ifndef FAMCONNECTION_T1678998633_H
#define FAMCONNECTION_T1678998633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FAMConnection
struct  FAMConnection_t1678998633 
{
public:
	// System.Int32 System.IO.FAMConnection::FD
	int32_t ___FD_0;
	// System.IntPtr System.IO.FAMConnection::opaque
	intptr_t ___opaque_1;

public:
	inline static int32_t get_offset_of_FD_0() { return static_cast<int32_t>(offsetof(FAMConnection_t1678998633, ___FD_0)); }
	inline int32_t get_FD_0() const { return ___FD_0; }
	inline int32_t* get_address_of_FD_0() { return &___FD_0; }
	inline void set_FD_0(int32_t value)
	{
		___FD_0 = value;
	}

	inline static int32_t get_offset_of_opaque_1() { return static_cast<int32_t>(offsetof(FAMConnection_t1678998633, ___opaque_1)); }
	inline intptr_t get_opaque_1() const { return ___opaque_1; }
	inline intptr_t* get_address_of_opaque_1() { return &___opaque_1; }
	inline void set_opaque_1(intptr_t value)
	{
		___opaque_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAMCONNECTION_T1678998633_H
#ifndef FILEATTRIBUTES_T3417205536_H
#define FILEATTRIBUTES_T3417205536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAttributes
struct  FileAttributes_t3417205536 
{
public:
	// System.Int32 System.IO.FileAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAttributes_t3417205536, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEATTRIBUTES_T3417205536_H
#ifndef DEFAULTWATCHERDATA_T3310779826_H
#define DEFAULTWATCHERDATA_T3310779826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.DefaultWatcherData
struct  DefaultWatcherData_t3310779826  : public RuntimeObject
{
public:
	// System.IO.FileSystemWatcher System.IO.DefaultWatcherData::FSW
	FileSystemWatcher_t416760199 * ___FSW_0;
	// System.String System.IO.DefaultWatcherData::Directory
	String_t* ___Directory_1;
	// System.String System.IO.DefaultWatcherData::FileMask
	String_t* ___FileMask_2;
	// System.Boolean System.IO.DefaultWatcherData::IncludeSubdirs
	bool ___IncludeSubdirs_3;
	// System.Boolean System.IO.DefaultWatcherData::Enabled
	bool ___Enabled_4;
	// System.Boolean System.IO.DefaultWatcherData::NoWildcards
	bool ___NoWildcards_5;
	// System.DateTime System.IO.DefaultWatcherData::DisabledTime
	DateTime_t3738529785  ___DisabledTime_6;
	// System.Object System.IO.DefaultWatcherData::FilesLock
	RuntimeObject * ___FilesLock_7;
	// System.Collections.Hashtable System.IO.DefaultWatcherData::Files
	Hashtable_t1853889766 * ___Files_8;

public:
	inline static int32_t get_offset_of_FSW_0() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___FSW_0)); }
	inline FileSystemWatcher_t416760199 * get_FSW_0() const { return ___FSW_0; }
	inline FileSystemWatcher_t416760199 ** get_address_of_FSW_0() { return &___FSW_0; }
	inline void set_FSW_0(FileSystemWatcher_t416760199 * value)
	{
		___FSW_0 = value;
		Il2CppCodeGenWriteBarrier((&___FSW_0), value);
	}

	inline static int32_t get_offset_of_Directory_1() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___Directory_1)); }
	inline String_t* get_Directory_1() const { return ___Directory_1; }
	inline String_t** get_address_of_Directory_1() { return &___Directory_1; }
	inline void set_Directory_1(String_t* value)
	{
		___Directory_1 = value;
		Il2CppCodeGenWriteBarrier((&___Directory_1), value);
	}

	inline static int32_t get_offset_of_FileMask_2() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___FileMask_2)); }
	inline String_t* get_FileMask_2() const { return ___FileMask_2; }
	inline String_t** get_address_of_FileMask_2() { return &___FileMask_2; }
	inline void set_FileMask_2(String_t* value)
	{
		___FileMask_2 = value;
		Il2CppCodeGenWriteBarrier((&___FileMask_2), value);
	}

	inline static int32_t get_offset_of_IncludeSubdirs_3() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___IncludeSubdirs_3)); }
	inline bool get_IncludeSubdirs_3() const { return ___IncludeSubdirs_3; }
	inline bool* get_address_of_IncludeSubdirs_3() { return &___IncludeSubdirs_3; }
	inline void set_IncludeSubdirs_3(bool value)
	{
		___IncludeSubdirs_3 = value;
	}

	inline static int32_t get_offset_of_Enabled_4() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___Enabled_4)); }
	inline bool get_Enabled_4() const { return ___Enabled_4; }
	inline bool* get_address_of_Enabled_4() { return &___Enabled_4; }
	inline void set_Enabled_4(bool value)
	{
		___Enabled_4 = value;
	}

	inline static int32_t get_offset_of_NoWildcards_5() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___NoWildcards_5)); }
	inline bool get_NoWildcards_5() const { return ___NoWildcards_5; }
	inline bool* get_address_of_NoWildcards_5() { return &___NoWildcards_5; }
	inline void set_NoWildcards_5(bool value)
	{
		___NoWildcards_5 = value;
	}

	inline static int32_t get_offset_of_DisabledTime_6() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___DisabledTime_6)); }
	inline DateTime_t3738529785  get_DisabledTime_6() const { return ___DisabledTime_6; }
	inline DateTime_t3738529785 * get_address_of_DisabledTime_6() { return &___DisabledTime_6; }
	inline void set_DisabledTime_6(DateTime_t3738529785  value)
	{
		___DisabledTime_6 = value;
	}

	inline static int32_t get_offset_of_FilesLock_7() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___FilesLock_7)); }
	inline RuntimeObject * get_FilesLock_7() const { return ___FilesLock_7; }
	inline RuntimeObject ** get_address_of_FilesLock_7() { return &___FilesLock_7; }
	inline void set_FilesLock_7(RuntimeObject * value)
	{
		___FilesLock_7 = value;
		Il2CppCodeGenWriteBarrier((&___FilesLock_7), value);
	}

	inline static int32_t get_offset_of_Files_8() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___Files_8)); }
	inline Hashtable_t1853889766 * get_Files_8() const { return ___Files_8; }
	inline Hashtable_t1853889766 ** get_address_of_Files_8() { return &___Files_8; }
	inline void set_Files_8(Hashtable_t1853889766 * value)
	{
		___Files_8 = value;
		Il2CppCodeGenWriteBarrier((&___Files_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTWATCHERDATA_T3310779826_H
#ifndef EVENTFLAGS_T1670942752_H
#define EVENTFLAGS_T1670942752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.EventFlags
struct  EventFlags_t1670942752 
{
public:
	// System.UInt16 System.IO.EventFlags::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventFlags_t1670942752, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFLAGS_T1670942752_H
#ifndef KQUEUEMONITOR_T3818269198_H
#define KQUEUEMONITOR_T3818269198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.KqueueMonitor
struct  KqueueMonitor_t3818269198  : public RuntimeObject
{
public:
	// System.Int32 System.IO.KqueueMonitor::maxFds
	int32_t ___maxFds_2;
	// System.IO.FileSystemWatcher System.IO.KqueueMonitor::fsw
	FileSystemWatcher_t416760199 * ___fsw_3;
	// System.Int32 System.IO.KqueueMonitor::conn
	int32_t ___conn_4;
	// System.Threading.Thread System.IO.KqueueMonitor::thread
	Thread_t2300836069 * ___thread_5;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.IO.KqueueMonitor::requestStop
	bool ___requestStop_6;
	// System.Threading.AutoResetEvent System.IO.KqueueMonitor::startedEvent
	AutoResetEvent_t1333520283 * ___startedEvent_7;
	// System.Boolean System.IO.KqueueMonitor::started
	bool ___started_8;
	// System.Boolean System.IO.KqueueMonitor::inDispatch
	bool ___inDispatch_9;
	// System.Exception System.IO.KqueueMonitor::exc
	Exception_t1436737249 * ___exc_10;
	// System.Object System.IO.KqueueMonitor::stateLock
	RuntimeObject * ___stateLock_11;
	// System.Object System.IO.KqueueMonitor::connLock
	RuntimeObject * ___connLock_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.IO.PathData> System.IO.KqueueMonitor::pathsDict
	Dictionary_2_t3002451328 * ___pathsDict_13;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.IO.PathData> System.IO.KqueueMonitor::fdsDict
	Dictionary_2_t2105908360 * ___fdsDict_14;
	// System.String System.IO.KqueueMonitor::fixupPath
	String_t* ___fixupPath_15;
	// System.String System.IO.KqueueMonitor::fullPathNoLastSlash
	String_t* ___fullPathNoLastSlash_16;

public:
	inline static int32_t get_offset_of_maxFds_2() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___maxFds_2)); }
	inline int32_t get_maxFds_2() const { return ___maxFds_2; }
	inline int32_t* get_address_of_maxFds_2() { return &___maxFds_2; }
	inline void set_maxFds_2(int32_t value)
	{
		___maxFds_2 = value;
	}

	inline static int32_t get_offset_of_fsw_3() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___fsw_3)); }
	inline FileSystemWatcher_t416760199 * get_fsw_3() const { return ___fsw_3; }
	inline FileSystemWatcher_t416760199 ** get_address_of_fsw_3() { return &___fsw_3; }
	inline void set_fsw_3(FileSystemWatcher_t416760199 * value)
	{
		___fsw_3 = value;
		Il2CppCodeGenWriteBarrier((&___fsw_3), value);
	}

	inline static int32_t get_offset_of_conn_4() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___conn_4)); }
	inline int32_t get_conn_4() const { return ___conn_4; }
	inline int32_t* get_address_of_conn_4() { return &___conn_4; }
	inline void set_conn_4(int32_t value)
	{
		___conn_4 = value;
	}

	inline static int32_t get_offset_of_thread_5() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___thread_5)); }
	inline Thread_t2300836069 * get_thread_5() const { return ___thread_5; }
	inline Thread_t2300836069 ** get_address_of_thread_5() { return &___thread_5; }
	inline void set_thread_5(Thread_t2300836069 * value)
	{
		___thread_5 = value;
		Il2CppCodeGenWriteBarrier((&___thread_5), value);
	}

	inline static int32_t get_offset_of_requestStop_6() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___requestStop_6)); }
	inline bool get_requestStop_6() const { return ___requestStop_6; }
	inline bool* get_address_of_requestStop_6() { return &___requestStop_6; }
	inline void set_requestStop_6(bool value)
	{
		___requestStop_6 = value;
	}

	inline static int32_t get_offset_of_startedEvent_7() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___startedEvent_7)); }
	inline AutoResetEvent_t1333520283 * get_startedEvent_7() const { return ___startedEvent_7; }
	inline AutoResetEvent_t1333520283 ** get_address_of_startedEvent_7() { return &___startedEvent_7; }
	inline void set_startedEvent_7(AutoResetEvent_t1333520283 * value)
	{
		___startedEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___startedEvent_7), value);
	}

	inline static int32_t get_offset_of_started_8() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___started_8)); }
	inline bool get_started_8() const { return ___started_8; }
	inline bool* get_address_of_started_8() { return &___started_8; }
	inline void set_started_8(bool value)
	{
		___started_8 = value;
	}

	inline static int32_t get_offset_of_inDispatch_9() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___inDispatch_9)); }
	inline bool get_inDispatch_9() const { return ___inDispatch_9; }
	inline bool* get_address_of_inDispatch_9() { return &___inDispatch_9; }
	inline void set_inDispatch_9(bool value)
	{
		___inDispatch_9 = value;
	}

	inline static int32_t get_offset_of_exc_10() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___exc_10)); }
	inline Exception_t1436737249 * get_exc_10() const { return ___exc_10; }
	inline Exception_t1436737249 ** get_address_of_exc_10() { return &___exc_10; }
	inline void set_exc_10(Exception_t1436737249 * value)
	{
		___exc_10 = value;
		Il2CppCodeGenWriteBarrier((&___exc_10), value);
	}

	inline static int32_t get_offset_of_stateLock_11() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___stateLock_11)); }
	inline RuntimeObject * get_stateLock_11() const { return ___stateLock_11; }
	inline RuntimeObject ** get_address_of_stateLock_11() { return &___stateLock_11; }
	inline void set_stateLock_11(RuntimeObject * value)
	{
		___stateLock_11 = value;
		Il2CppCodeGenWriteBarrier((&___stateLock_11), value);
	}

	inline static int32_t get_offset_of_connLock_12() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___connLock_12)); }
	inline RuntimeObject * get_connLock_12() const { return ___connLock_12; }
	inline RuntimeObject ** get_address_of_connLock_12() { return &___connLock_12; }
	inline void set_connLock_12(RuntimeObject * value)
	{
		___connLock_12 = value;
		Il2CppCodeGenWriteBarrier((&___connLock_12), value);
	}

	inline static int32_t get_offset_of_pathsDict_13() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___pathsDict_13)); }
	inline Dictionary_2_t3002451328 * get_pathsDict_13() const { return ___pathsDict_13; }
	inline Dictionary_2_t3002451328 ** get_address_of_pathsDict_13() { return &___pathsDict_13; }
	inline void set_pathsDict_13(Dictionary_2_t3002451328 * value)
	{
		___pathsDict_13 = value;
		Il2CppCodeGenWriteBarrier((&___pathsDict_13), value);
	}

	inline static int32_t get_offset_of_fdsDict_14() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___fdsDict_14)); }
	inline Dictionary_2_t2105908360 * get_fdsDict_14() const { return ___fdsDict_14; }
	inline Dictionary_2_t2105908360 ** get_address_of_fdsDict_14() { return &___fdsDict_14; }
	inline void set_fdsDict_14(Dictionary_2_t2105908360 * value)
	{
		___fdsDict_14 = value;
		Il2CppCodeGenWriteBarrier((&___fdsDict_14), value);
	}

	inline static int32_t get_offset_of_fixupPath_15() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___fixupPath_15)); }
	inline String_t* get_fixupPath_15() const { return ___fixupPath_15; }
	inline String_t** get_address_of_fixupPath_15() { return &___fixupPath_15; }
	inline void set_fixupPath_15(String_t* value)
	{
		___fixupPath_15 = value;
		Il2CppCodeGenWriteBarrier((&___fixupPath_15), value);
	}

	inline static int32_t get_offset_of_fullPathNoLastSlash_16() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198, ___fullPathNoLastSlash_16)); }
	inline String_t* get_fullPathNoLastSlash_16() const { return ___fullPathNoLastSlash_16; }
	inline String_t** get_address_of_fullPathNoLastSlash_16() { return &___fullPathNoLastSlash_16; }
	inline void set_fullPathNoLastSlash_16(String_t* value)
	{
		___fullPathNoLastSlash_16 = value;
		Il2CppCodeGenWriteBarrier((&___fullPathNoLastSlash_16), value);
	}
};

struct KqueueMonitor_t3818269198_StaticFields
{
public:
	// System.Boolean System.IO.KqueueMonitor::initialized
	bool ___initialized_0;
	// System.IO.kevent[] System.IO.KqueueMonitor::emptyEventList
	keventU5BU5D_t21523777* ___emptyEventList_1;

public:
	inline static int32_t get_offset_of_initialized_0() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198_StaticFields, ___initialized_0)); }
	inline bool get_initialized_0() const { return ___initialized_0; }
	inline bool* get_address_of_initialized_0() { return &___initialized_0; }
	inline void set_initialized_0(bool value)
	{
		___initialized_0 = value;
	}

	inline static int32_t get_offset_of_emptyEventList_1() { return static_cast<int32_t>(offsetof(KqueueMonitor_t3818269198_StaticFields, ___emptyEventList_1)); }
	inline keventU5BU5D_t21523777* get_emptyEventList_1() const { return ___emptyEventList_1; }
	inline keventU5BU5D_t21523777** get_address_of_emptyEventList_1() { return &___emptyEventList_1; }
	inline void set_emptyEventList_1(keventU5BU5D_t21523777* value)
	{
		___emptyEventList_1 = value;
		Il2CppCodeGenWriteBarrier((&___emptyEventList_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KQUEUEMONITOR_T3818269198_H
#ifndef GZIPSTREAM_T3417139389_H
#define GZIPSTREAM_T3417139389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.GZipStream
struct  GZipStream_t3417139389  : public Stream_t1273022909
{
public:
	// System.IO.Compression.DeflateStream System.IO.Compression.GZipStream::_deflateStream
	DeflateStream_t4175168077 * ____deflateStream_5;

public:
	inline static int32_t get_offset_of__deflateStream_5() { return static_cast<int32_t>(offsetof(GZipStream_t3417139389, ____deflateStream_5)); }
	inline DeflateStream_t4175168077 * get__deflateStream_5() const { return ____deflateStream_5; }
	inline DeflateStream_t4175168077 ** get_address_of__deflateStream_5() { return &____deflateStream_5; }
	inline void set__deflateStream_5(DeflateStream_t4175168077 * value)
	{
		____deflateStream_5 = value;
		Il2CppCodeGenWriteBarrier((&____deflateStream_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GZIPSTREAM_T3417139389_H
#ifndef COMPRESSIONMODE_T3714291783_H
#define COMPRESSIONMODE_T3714291783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.CompressionMode
struct  CompressionMode_t3714291783 
{
public:
	// System.Int32 System.IO.Compression.CompressionMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompressionMode_t3714291783, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONMODE_T3714291783_H
#ifndef FILTERFLAGS_T1388790893_H
#define FILTERFLAGS_T1388790893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FilterFlags
struct  FilterFlags_t1388790893 
{
public:
	// System.UInt32 System.IO.FilterFlags::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterFlags_t1388790893, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERFLAGS_T1388790893_H
#ifndef NOTIFYFILTERS_T3825601669_H
#define NOTIFYFILTERS_T3825601669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.NotifyFilters
struct  NotifyFilters_t3825601669 
{
public:
	// System.Int32 System.IO.NotifyFilters::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NotifyFilters_t3825601669, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFYFILTERS_T3825601669_H
#ifndef OPCODE_T3581930920_H
#define OPCODE_T3581930920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.SearchPattern2/OpCode
struct  OpCode_t3581930920 
{
public:
	// System.Int32 System.IO.SearchPattern2/OpCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OpCode_t3581930920, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPCODE_T3581930920_H
#ifndef EVENTFILTER_T2065690828_H
#define EVENTFILTER_T2065690828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.EventFilter
struct  EventFilter_t2065690828 
{
public:
	// System.Int16 System.IO.EventFilter::value__
	int16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventFilter_t2065690828, ___value___2)); }
	inline int16_t get_value___2() const { return ___value___2; }
	inline int16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFILTER_T2065690828_H
#ifndef DEFLATESTREAMNATIVE_T1405046456_H
#define DEFLATESTREAMNATIVE_T1405046456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.DeflateStreamNative
struct  DeflateStreamNative_t1405046456  : public RuntimeObject
{
public:
	// System.IO.Compression.DeflateStreamNative/UnmanagedReadOrWrite System.IO.Compression.DeflateStreamNative::feeder
	UnmanagedReadOrWrite_t1975956110 * ___feeder_0;
	// System.IO.Stream System.IO.Compression.DeflateStreamNative::base_stream
	Stream_t1273022909 * ___base_stream_1;
	// System.IntPtr System.IO.Compression.DeflateStreamNative::z_stream
	intptr_t ___z_stream_2;
	// System.Runtime.InteropServices.GCHandle System.IO.Compression.DeflateStreamNative::data
	GCHandle_t3351438187  ___data_3;
	// System.Boolean System.IO.Compression.DeflateStreamNative::disposed
	bool ___disposed_4;
	// System.Byte[] System.IO.Compression.DeflateStreamNative::io_buffer
	ByteU5BU5D_t4116647657* ___io_buffer_5;

public:
	inline static int32_t get_offset_of_feeder_0() { return static_cast<int32_t>(offsetof(DeflateStreamNative_t1405046456, ___feeder_0)); }
	inline UnmanagedReadOrWrite_t1975956110 * get_feeder_0() const { return ___feeder_0; }
	inline UnmanagedReadOrWrite_t1975956110 ** get_address_of_feeder_0() { return &___feeder_0; }
	inline void set_feeder_0(UnmanagedReadOrWrite_t1975956110 * value)
	{
		___feeder_0 = value;
		Il2CppCodeGenWriteBarrier((&___feeder_0), value);
	}

	inline static int32_t get_offset_of_base_stream_1() { return static_cast<int32_t>(offsetof(DeflateStreamNative_t1405046456, ___base_stream_1)); }
	inline Stream_t1273022909 * get_base_stream_1() const { return ___base_stream_1; }
	inline Stream_t1273022909 ** get_address_of_base_stream_1() { return &___base_stream_1; }
	inline void set_base_stream_1(Stream_t1273022909 * value)
	{
		___base_stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___base_stream_1), value);
	}

	inline static int32_t get_offset_of_z_stream_2() { return static_cast<int32_t>(offsetof(DeflateStreamNative_t1405046456, ___z_stream_2)); }
	inline intptr_t get_z_stream_2() const { return ___z_stream_2; }
	inline intptr_t* get_address_of_z_stream_2() { return &___z_stream_2; }
	inline void set_z_stream_2(intptr_t value)
	{
		___z_stream_2 = value;
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(DeflateStreamNative_t1405046456, ___data_3)); }
	inline GCHandle_t3351438187  get_data_3() const { return ___data_3; }
	inline GCHandle_t3351438187 * get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(GCHandle_t3351438187  value)
	{
		___data_3 = value;
	}

	inline static int32_t get_offset_of_disposed_4() { return static_cast<int32_t>(offsetof(DeflateStreamNative_t1405046456, ___disposed_4)); }
	inline bool get_disposed_4() const { return ___disposed_4; }
	inline bool* get_address_of_disposed_4() { return &___disposed_4; }
	inline void set_disposed_4(bool value)
	{
		___disposed_4 = value;
	}

	inline static int32_t get_offset_of_io_buffer_5() { return static_cast<int32_t>(offsetof(DeflateStreamNative_t1405046456, ___io_buffer_5)); }
	inline ByteU5BU5D_t4116647657* get_io_buffer_5() const { return ___io_buffer_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_io_buffer_5() { return &___io_buffer_5; }
	inline void set_io_buffer_5(ByteU5BU5D_t4116647657* value)
	{
		___io_buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___io_buffer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATESTREAMNATIVE_T1405046456_H
#ifndef WATCHERCHANGETYPES_T673177441_H
#define WATCHERCHANGETYPES_T673177441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.WatcherChangeTypes
struct  WatcherChangeTypes_t673177441 
{
public:
	// System.Int32 System.IO.WatcherChangeTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WatcherChangeTypes_t673177441, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATCHERCHANGETYPES_T673177441_H
#ifndef NOTIFYCOLLECTIONCHANGEDEVENTARGS_T9239872_H
#define NOTIFYCOLLECTIONCHANGEDEVENTARGS_T9239872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NotifyCollectionChangedEventArgs
struct  NotifyCollectionChangedEventArgs_t9239872  : public EventArgs_t3591816995
{
public:
	// System.Collections.Specialized.NotifyCollectionChangedAction System.Collections.Specialized.NotifyCollectionChangedEventArgs::_action
	int32_t ____action_1;
	// System.Collections.IList System.Collections.Specialized.NotifyCollectionChangedEventArgs::_newItems
	RuntimeObject* ____newItems_2;
	// System.Collections.IList System.Collections.Specialized.NotifyCollectionChangedEventArgs::_oldItems
	RuntimeObject* ____oldItems_3;
	// System.Int32 System.Collections.Specialized.NotifyCollectionChangedEventArgs::_newStartingIndex
	int32_t ____newStartingIndex_4;
	// System.Int32 System.Collections.Specialized.NotifyCollectionChangedEventArgs::_oldStartingIndex
	int32_t ____oldStartingIndex_5;

public:
	inline static int32_t get_offset_of__action_1() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedEventArgs_t9239872, ____action_1)); }
	inline int32_t get__action_1() const { return ____action_1; }
	inline int32_t* get_address_of__action_1() { return &____action_1; }
	inline void set__action_1(int32_t value)
	{
		____action_1 = value;
	}

	inline static int32_t get_offset_of__newItems_2() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedEventArgs_t9239872, ____newItems_2)); }
	inline RuntimeObject* get__newItems_2() const { return ____newItems_2; }
	inline RuntimeObject** get_address_of__newItems_2() { return &____newItems_2; }
	inline void set__newItems_2(RuntimeObject* value)
	{
		____newItems_2 = value;
		Il2CppCodeGenWriteBarrier((&____newItems_2), value);
	}

	inline static int32_t get_offset_of__oldItems_3() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedEventArgs_t9239872, ____oldItems_3)); }
	inline RuntimeObject* get__oldItems_3() const { return ____oldItems_3; }
	inline RuntimeObject** get_address_of__oldItems_3() { return &____oldItems_3; }
	inline void set__oldItems_3(RuntimeObject* value)
	{
		____oldItems_3 = value;
		Il2CppCodeGenWriteBarrier((&____oldItems_3), value);
	}

	inline static int32_t get_offset_of__newStartingIndex_4() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedEventArgs_t9239872, ____newStartingIndex_4)); }
	inline int32_t get__newStartingIndex_4() const { return ____newStartingIndex_4; }
	inline int32_t* get_address_of__newStartingIndex_4() { return &____newStartingIndex_4; }
	inline void set__newStartingIndex_4(int32_t value)
	{
		____newStartingIndex_4 = value;
	}

	inline static int32_t get_offset_of__oldStartingIndex_5() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedEventArgs_t9239872, ____oldStartingIndex_5)); }
	inline int32_t get__oldStartingIndex_5() const { return ____oldStartingIndex_5; }
	inline int32_t* get_address_of__oldStartingIndex_5() { return &____oldStartingIndex_5; }
	inline void set__oldStartingIndex_5(int32_t value)
	{
		____oldStartingIndex_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFYCOLLECTIONCHANGEDEVENTARGS_T9239872_H
#ifndef WAITFORCHANGEDRESULT_T3377826936_H
#define WAITFORCHANGEDRESULT_T3377826936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.WaitForChangedResult
struct  WaitForChangedResult_t3377826936 
{
public:
	// System.IO.WatcherChangeTypes System.IO.WaitForChangedResult::changeType
	int32_t ___changeType_0;
	// System.String System.IO.WaitForChangedResult::name
	String_t* ___name_1;
	// System.String System.IO.WaitForChangedResult::oldName
	String_t* ___oldName_2;
	// System.Boolean System.IO.WaitForChangedResult::timedOut
	bool ___timedOut_3;

public:
	inline static int32_t get_offset_of_changeType_0() { return static_cast<int32_t>(offsetof(WaitForChangedResult_t3377826936, ___changeType_0)); }
	inline int32_t get_changeType_0() const { return ___changeType_0; }
	inline int32_t* get_address_of_changeType_0() { return &___changeType_0; }
	inline void set_changeType_0(int32_t value)
	{
		___changeType_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(WaitForChangedResult_t3377826936, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_oldName_2() { return static_cast<int32_t>(offsetof(WaitForChangedResult_t3377826936, ___oldName_2)); }
	inline String_t* get_oldName_2() const { return ___oldName_2; }
	inline String_t** get_address_of_oldName_2() { return &___oldName_2; }
	inline void set_oldName_2(String_t* value)
	{
		___oldName_2 = value;
		Il2CppCodeGenWriteBarrier((&___oldName_2), value);
	}

	inline static int32_t get_offset_of_timedOut_3() { return static_cast<int32_t>(offsetof(WaitForChangedResult_t3377826936, ___timedOut_3)); }
	inline bool get_timedOut_3() const { return ___timedOut_3; }
	inline bool* get_address_of_timedOut_3() { return &___timedOut_3; }
	inline void set_timedOut_3(bool value)
	{
		___timedOut_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IO.WaitForChangedResult
struct WaitForChangedResult_t3377826936_marshaled_pinvoke
{
	int32_t ___changeType_0;
	char* ___name_1;
	char* ___oldName_2;
	int32_t ___timedOut_3;
};
// Native definition for COM marshalling of System.IO.WaitForChangedResult
struct WaitForChangedResult_t3377826936_marshaled_com
{
	int32_t ___changeType_0;
	Il2CppChar* ___name_1;
	Il2CppChar* ___oldName_2;
	int32_t ___timedOut_3;
};
#endif // WAITFORCHANGEDRESULT_T3377826936_H
#ifndef INOTIFYWATCHER_T2392648639_H
#define INOTIFYWATCHER_T2392648639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.InotifyWatcher
struct  InotifyWatcher_t2392648639  : public RuntimeObject
{
public:

public:
};

struct InotifyWatcher_t2392648639_StaticFields
{
public:
	// System.Boolean System.IO.InotifyWatcher::failed
	bool ___failed_0;
	// System.IO.InotifyWatcher System.IO.InotifyWatcher::instance
	InotifyWatcher_t2392648639 * ___instance_1;
	// System.Collections.Hashtable System.IO.InotifyWatcher::watches
	Hashtable_t1853889766 * ___watches_2;
	// System.Collections.Hashtable System.IO.InotifyWatcher::requests
	Hashtable_t1853889766 * ___requests_3;
	// System.IntPtr System.IO.InotifyWatcher::FD
	intptr_t ___FD_4;
	// System.Threading.Thread System.IO.InotifyWatcher::thread
	Thread_t2300836069 * ___thread_5;
	// System.Boolean System.IO.InotifyWatcher::stop
	bool ___stop_6;
	// System.IO.InotifyMask System.IO.InotifyWatcher::Interesting
	uint32_t ___Interesting_7;

public:
	inline static int32_t get_offset_of_failed_0() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___failed_0)); }
	inline bool get_failed_0() const { return ___failed_0; }
	inline bool* get_address_of_failed_0() { return &___failed_0; }
	inline void set_failed_0(bool value)
	{
		___failed_0 = value;
	}

	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___instance_1)); }
	inline InotifyWatcher_t2392648639 * get_instance_1() const { return ___instance_1; }
	inline InotifyWatcher_t2392648639 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(InotifyWatcher_t2392648639 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___instance_1), value);
	}

	inline static int32_t get_offset_of_watches_2() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___watches_2)); }
	inline Hashtable_t1853889766 * get_watches_2() const { return ___watches_2; }
	inline Hashtable_t1853889766 ** get_address_of_watches_2() { return &___watches_2; }
	inline void set_watches_2(Hashtable_t1853889766 * value)
	{
		___watches_2 = value;
		Il2CppCodeGenWriteBarrier((&___watches_2), value);
	}

	inline static int32_t get_offset_of_requests_3() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___requests_3)); }
	inline Hashtable_t1853889766 * get_requests_3() const { return ___requests_3; }
	inline Hashtable_t1853889766 ** get_address_of_requests_3() { return &___requests_3; }
	inline void set_requests_3(Hashtable_t1853889766 * value)
	{
		___requests_3 = value;
		Il2CppCodeGenWriteBarrier((&___requests_3), value);
	}

	inline static int32_t get_offset_of_FD_4() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___FD_4)); }
	inline intptr_t get_FD_4() const { return ___FD_4; }
	inline intptr_t* get_address_of_FD_4() { return &___FD_4; }
	inline void set_FD_4(intptr_t value)
	{
		___FD_4 = value;
	}

	inline static int32_t get_offset_of_thread_5() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___thread_5)); }
	inline Thread_t2300836069 * get_thread_5() const { return ___thread_5; }
	inline Thread_t2300836069 ** get_address_of_thread_5() { return &___thread_5; }
	inline void set_thread_5(Thread_t2300836069 * value)
	{
		___thread_5 = value;
		Il2CppCodeGenWriteBarrier((&___thread_5), value);
	}

	inline static int32_t get_offset_of_stop_6() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___stop_6)); }
	inline bool get_stop_6() const { return ___stop_6; }
	inline bool* get_address_of_stop_6() { return &___stop_6; }
	inline void set_stop_6(bool value)
	{
		___stop_6 = value;
	}

	inline static int32_t get_offset_of_Interesting_7() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___Interesting_7)); }
	inline uint32_t get_Interesting_7() const { return ___Interesting_7; }
	inline uint32_t* get_address_of_Interesting_7() { return &___Interesting_7; }
	inline void set_Interesting_7(uint32_t value)
	{
		___Interesting_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INOTIFYWATCHER_T2392648639_H
#ifndef FILEDATA_T355659623_H
#define FILEDATA_T355659623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileData
struct  FileData_t355659623  : public RuntimeObject
{
public:
	// System.String System.IO.FileData::Directory
	String_t* ___Directory_0;
	// System.IO.FileAttributes System.IO.FileData::Attributes
	int32_t ___Attributes_1;
	// System.Boolean System.IO.FileData::NotExists
	bool ___NotExists_2;
	// System.DateTime System.IO.FileData::CreationTime
	DateTime_t3738529785  ___CreationTime_3;
	// System.DateTime System.IO.FileData::LastWriteTime
	DateTime_t3738529785  ___LastWriteTime_4;

public:
	inline static int32_t get_offset_of_Directory_0() { return static_cast<int32_t>(offsetof(FileData_t355659623, ___Directory_0)); }
	inline String_t* get_Directory_0() const { return ___Directory_0; }
	inline String_t** get_address_of_Directory_0() { return &___Directory_0; }
	inline void set_Directory_0(String_t* value)
	{
		___Directory_0 = value;
		Il2CppCodeGenWriteBarrier((&___Directory_0), value);
	}

	inline static int32_t get_offset_of_Attributes_1() { return static_cast<int32_t>(offsetof(FileData_t355659623, ___Attributes_1)); }
	inline int32_t get_Attributes_1() const { return ___Attributes_1; }
	inline int32_t* get_address_of_Attributes_1() { return &___Attributes_1; }
	inline void set_Attributes_1(int32_t value)
	{
		___Attributes_1 = value;
	}

	inline static int32_t get_offset_of_NotExists_2() { return static_cast<int32_t>(offsetof(FileData_t355659623, ___NotExists_2)); }
	inline bool get_NotExists_2() const { return ___NotExists_2; }
	inline bool* get_address_of_NotExists_2() { return &___NotExists_2; }
	inline void set_NotExists_2(bool value)
	{
		___NotExists_2 = value;
	}

	inline static int32_t get_offset_of_CreationTime_3() { return static_cast<int32_t>(offsetof(FileData_t355659623, ___CreationTime_3)); }
	inline DateTime_t3738529785  get_CreationTime_3() const { return ___CreationTime_3; }
	inline DateTime_t3738529785 * get_address_of_CreationTime_3() { return &___CreationTime_3; }
	inline void set_CreationTime_3(DateTime_t3738529785  value)
	{
		___CreationTime_3 = value;
	}

	inline static int32_t get_offset_of_LastWriteTime_4() { return static_cast<int32_t>(offsetof(FileData_t355659623, ___LastWriteTime_4)); }
	inline DateTime_t3738529785  get_LastWriteTime_4() const { return ___LastWriteTime_4; }
	inline DateTime_t3738529785 * get_address_of_LastWriteTime_4() { return &___LastWriteTime_4; }
	inline void set_LastWriteTime_4(DateTime_t3738529785  value)
	{
		___LastWriteTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEDATA_T355659623_H
#ifndef OP_T3134810481_H
#define OP_T3134810481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.SearchPattern2/Op
struct  Op_t3134810481  : public RuntimeObject
{
public:
	// System.IO.SearchPattern2/OpCode System.IO.SearchPattern2/Op::Code
	int32_t ___Code_0;
	// System.String System.IO.SearchPattern2/Op::Argument
	String_t* ___Argument_1;
	// System.IO.SearchPattern2/Op System.IO.SearchPattern2/Op::Next
	Op_t3134810481 * ___Next_2;

public:
	inline static int32_t get_offset_of_Code_0() { return static_cast<int32_t>(offsetof(Op_t3134810481, ___Code_0)); }
	inline int32_t get_Code_0() const { return ___Code_0; }
	inline int32_t* get_address_of_Code_0() { return &___Code_0; }
	inline void set_Code_0(int32_t value)
	{
		___Code_0 = value;
	}

	inline static int32_t get_offset_of_Argument_1() { return static_cast<int32_t>(offsetof(Op_t3134810481, ___Argument_1)); }
	inline String_t* get_Argument_1() const { return ___Argument_1; }
	inline String_t** get_address_of_Argument_1() { return &___Argument_1; }
	inline void set_Argument_1(String_t* value)
	{
		___Argument_1 = value;
		Il2CppCodeGenWriteBarrier((&___Argument_1), value);
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(Op_t3134810481, ___Next_2)); }
	inline Op_t3134810481 * get_Next_2() const { return ___Next_2; }
	inline Op_t3134810481 ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(Op_t3134810481 * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier((&___Next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OP_T3134810481_H
#ifndef MULTICASTDELEGATE_T157516450_H
#define MULTICASTDELEGATE_T157516450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t157516450  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t157516450, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T157516450_H
#ifndef FAMWATCHER_T3228827479_H
#define FAMWATCHER_T3228827479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FAMWatcher
struct  FAMWatcher_t3228827479  : public RuntimeObject
{
public:

public:
};

struct FAMWatcher_t3228827479_StaticFields
{
public:
	// System.Boolean System.IO.FAMWatcher::failed
	bool ___failed_0;
	// System.IO.FAMWatcher System.IO.FAMWatcher::instance
	FAMWatcher_t3228827479 * ___instance_1;
	// System.Collections.Hashtable System.IO.FAMWatcher::watches
	Hashtable_t1853889766 * ___watches_2;
	// System.Collections.Hashtable System.IO.FAMWatcher::requests
	Hashtable_t1853889766 * ___requests_3;
	// System.IO.FAMConnection System.IO.FAMWatcher::conn
	FAMConnection_t1678998633  ___conn_4;
	// System.Threading.Thread System.IO.FAMWatcher::thread
	Thread_t2300836069 * ___thread_5;
	// System.Boolean System.IO.FAMWatcher::stop
	bool ___stop_6;
	// System.Boolean System.IO.FAMWatcher::use_gamin
	bool ___use_gamin_7;

public:
	inline static int32_t get_offset_of_failed_0() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___failed_0)); }
	inline bool get_failed_0() const { return ___failed_0; }
	inline bool* get_address_of_failed_0() { return &___failed_0; }
	inline void set_failed_0(bool value)
	{
		___failed_0 = value;
	}

	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___instance_1)); }
	inline FAMWatcher_t3228827479 * get_instance_1() const { return ___instance_1; }
	inline FAMWatcher_t3228827479 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(FAMWatcher_t3228827479 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___instance_1), value);
	}

	inline static int32_t get_offset_of_watches_2() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___watches_2)); }
	inline Hashtable_t1853889766 * get_watches_2() const { return ___watches_2; }
	inline Hashtable_t1853889766 ** get_address_of_watches_2() { return &___watches_2; }
	inline void set_watches_2(Hashtable_t1853889766 * value)
	{
		___watches_2 = value;
		Il2CppCodeGenWriteBarrier((&___watches_2), value);
	}

	inline static int32_t get_offset_of_requests_3() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___requests_3)); }
	inline Hashtable_t1853889766 * get_requests_3() const { return ___requests_3; }
	inline Hashtable_t1853889766 ** get_address_of_requests_3() { return &___requests_3; }
	inline void set_requests_3(Hashtable_t1853889766 * value)
	{
		___requests_3 = value;
		Il2CppCodeGenWriteBarrier((&___requests_3), value);
	}

	inline static int32_t get_offset_of_conn_4() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___conn_4)); }
	inline FAMConnection_t1678998633  get_conn_4() const { return ___conn_4; }
	inline FAMConnection_t1678998633 * get_address_of_conn_4() { return &___conn_4; }
	inline void set_conn_4(FAMConnection_t1678998633  value)
	{
		___conn_4 = value;
	}

	inline static int32_t get_offset_of_thread_5() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___thread_5)); }
	inline Thread_t2300836069 * get_thread_5() const { return ___thread_5; }
	inline Thread_t2300836069 ** get_address_of_thread_5() { return &___thread_5; }
	inline void set_thread_5(Thread_t2300836069 * value)
	{
		___thread_5 = value;
		Il2CppCodeGenWriteBarrier((&___thread_5), value);
	}

	inline static int32_t get_offset_of_stop_6() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___stop_6)); }
	inline bool get_stop_6() const { return ___stop_6; }
	inline bool* get_address_of_stop_6() { return &___stop_6; }
	inline void set_stop_6(bool value)
	{
		___stop_6 = value;
	}

	inline static int32_t get_offset_of_use_gamin_7() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___use_gamin_7)); }
	inline bool get_use_gamin_7() const { return ___use_gamin_7; }
	inline bool* get_address_of_use_gamin_7() { return &___use_gamin_7; }
	inline void set_use_gamin_7(bool value)
	{
		___use_gamin_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAMWATCHER_T3228827479_H
#ifndef FILESYSTEMEVENTARGS_T1603777841_H
#define FILESYSTEMEVENTARGS_T1603777841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemEventArgs
struct  FileSystemEventArgs_t1603777841  : public EventArgs_t3591816995
{
public:
	// System.IO.WatcherChangeTypes System.IO.FileSystemEventArgs::changeType
	int32_t ___changeType_1;
	// System.String System.IO.FileSystemEventArgs::directory
	String_t* ___directory_2;
	// System.String System.IO.FileSystemEventArgs::name
	String_t* ___name_3;

public:
	inline static int32_t get_offset_of_changeType_1() { return static_cast<int32_t>(offsetof(FileSystemEventArgs_t1603777841, ___changeType_1)); }
	inline int32_t get_changeType_1() const { return ___changeType_1; }
	inline int32_t* get_address_of_changeType_1() { return &___changeType_1; }
	inline void set_changeType_1(int32_t value)
	{
		___changeType_1 = value;
	}

	inline static int32_t get_offset_of_directory_2() { return static_cast<int32_t>(offsetof(FileSystemEventArgs_t1603777841, ___directory_2)); }
	inline String_t* get_directory_2() const { return ___directory_2; }
	inline String_t** get_address_of_directory_2() { return &___directory_2; }
	inline void set_directory_2(String_t* value)
	{
		___directory_2 = value;
		Il2CppCodeGenWriteBarrier((&___directory_2), value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(FileSystemEventArgs_t1603777841, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESYSTEMEVENTARGS_T1603777841_H
#ifndef INOTIFYEVENT_T2637353984_H
#define INOTIFYEVENT_T2637353984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.InotifyEvent
struct  InotifyEvent_t2637353984 
{
public:
	// System.Int32 System.IO.InotifyEvent::WatchDescriptor
	int32_t ___WatchDescriptor_0;
	// System.IO.InotifyMask System.IO.InotifyEvent::Mask
	uint32_t ___Mask_1;
	// System.String System.IO.InotifyEvent::Name
	String_t* ___Name_2;

public:
	inline static int32_t get_offset_of_WatchDescriptor_0() { return static_cast<int32_t>(offsetof(InotifyEvent_t2637353984, ___WatchDescriptor_0)); }
	inline int32_t get_WatchDescriptor_0() const { return ___WatchDescriptor_0; }
	inline int32_t* get_address_of_WatchDescriptor_0() { return &___WatchDescriptor_0; }
	inline void set_WatchDescriptor_0(int32_t value)
	{
		___WatchDescriptor_0 = value;
	}

	inline static int32_t get_offset_of_Mask_1() { return static_cast<int32_t>(offsetof(InotifyEvent_t2637353984, ___Mask_1)); }
	inline uint32_t get_Mask_1() const { return ___Mask_1; }
	inline uint32_t* get_address_of_Mask_1() { return &___Mask_1; }
	inline void set_Mask_1(uint32_t value)
	{
		___Mask_1 = value;
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(InotifyEvent_t2637353984, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IO.InotifyEvent
struct InotifyEvent_t2637353984_marshaled_pinvoke
{
	int32_t ___WatchDescriptor_0;
	uint32_t ___Mask_1;
	char* ___Name_2;
};
// Native definition for COM marshalling of System.IO.InotifyEvent
struct InotifyEvent_t2637353984_marshaled_com
{
	int32_t ___WatchDescriptor_0;
	uint32_t ___Mask_1;
	Il2CppChar* ___Name_2;
};
#endif // INOTIFYEVENT_T2637353984_H
#ifndef KEVENT_T2406656960_H
#define KEVENT_T2406656960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.kevent
struct  kevent_t2406656960 
{
public:
	// System.UIntPtr System.IO.kevent::ident
	uintptr_t ___ident_0;
	// System.IO.EventFilter System.IO.kevent::filter
	int16_t ___filter_1;
	// System.IO.EventFlags System.IO.kevent::flags
	uint16_t ___flags_2;
	// System.IO.FilterFlags System.IO.kevent::fflags
	uint32_t ___fflags_3;
	// System.IntPtr System.IO.kevent::data
	intptr_t ___data_4;
	// System.IntPtr System.IO.kevent::udata
	intptr_t ___udata_5;

public:
	inline static int32_t get_offset_of_ident_0() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___ident_0)); }
	inline uintptr_t get_ident_0() const { return ___ident_0; }
	inline uintptr_t* get_address_of_ident_0() { return &___ident_0; }
	inline void set_ident_0(uintptr_t value)
	{
		___ident_0 = value;
	}

	inline static int32_t get_offset_of_filter_1() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___filter_1)); }
	inline int16_t get_filter_1() const { return ___filter_1; }
	inline int16_t* get_address_of_filter_1() { return &___filter_1; }
	inline void set_filter_1(int16_t value)
	{
		___filter_1 = value;
	}

	inline static int32_t get_offset_of_flags_2() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___flags_2)); }
	inline uint16_t get_flags_2() const { return ___flags_2; }
	inline uint16_t* get_address_of_flags_2() { return &___flags_2; }
	inline void set_flags_2(uint16_t value)
	{
		___flags_2 = value;
	}

	inline static int32_t get_offset_of_fflags_3() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___fflags_3)); }
	inline uint32_t get_fflags_3() const { return ___fflags_3; }
	inline uint32_t* get_address_of_fflags_3() { return &___fflags_3; }
	inline void set_fflags_3(uint32_t value)
	{
		___fflags_3 = value;
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___data_4)); }
	inline intptr_t get_data_4() const { return ___data_4; }
	inline intptr_t* get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(intptr_t value)
	{
		___data_4 = value;
	}

	inline static int32_t get_offset_of_udata_5() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___udata_5)); }
	inline intptr_t get_udata_5() const { return ___udata_5; }
	inline intptr_t* get_address_of_udata_5() { return &___udata_5; }
	inline void set_udata_5(intptr_t value)
	{
		___udata_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEVENT_T2406656960_H
#ifndef DEFLATESTREAM_T4175168077_H
#define DEFLATESTREAM_T4175168077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.DeflateStream
struct  DeflateStream_t4175168077  : public Stream_t1273022909
{
public:
	// System.IO.Stream System.IO.Compression.DeflateStream::base_stream
	Stream_t1273022909 * ___base_stream_5;
	// System.IO.Compression.CompressionMode System.IO.Compression.DeflateStream::mode
	int32_t ___mode_6;
	// System.Boolean System.IO.Compression.DeflateStream::leaveOpen
	bool ___leaveOpen_7;
	// System.Boolean System.IO.Compression.DeflateStream::disposed
	bool ___disposed_8;
	// System.IO.Compression.DeflateStreamNative System.IO.Compression.DeflateStream::native
	DeflateStreamNative_t1405046456 * ___native_9;

public:
	inline static int32_t get_offset_of_base_stream_5() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___base_stream_5)); }
	inline Stream_t1273022909 * get_base_stream_5() const { return ___base_stream_5; }
	inline Stream_t1273022909 ** get_address_of_base_stream_5() { return &___base_stream_5; }
	inline void set_base_stream_5(Stream_t1273022909 * value)
	{
		___base_stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___base_stream_5), value);
	}

	inline static int32_t get_offset_of_mode_6() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___mode_6)); }
	inline int32_t get_mode_6() const { return ___mode_6; }
	inline int32_t* get_address_of_mode_6() { return &___mode_6; }
	inline void set_mode_6(int32_t value)
	{
		___mode_6 = value;
	}

	inline static int32_t get_offset_of_leaveOpen_7() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___leaveOpen_7)); }
	inline bool get_leaveOpen_7() const { return ___leaveOpen_7; }
	inline bool* get_address_of_leaveOpen_7() { return &___leaveOpen_7; }
	inline void set_leaveOpen_7(bool value)
	{
		___leaveOpen_7 = value;
	}

	inline static int32_t get_offset_of_disposed_8() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___disposed_8)); }
	inline bool get_disposed_8() const { return ___disposed_8; }
	inline bool* get_address_of_disposed_8() { return &___disposed_8; }
	inline void set_disposed_8(bool value)
	{
		___disposed_8 = value;
	}

	inline static int32_t get_offset_of_native_9() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___native_9)); }
	inline DeflateStreamNative_t1405046456 * get_native_9() const { return ___native_9; }
	inline DeflateStreamNative_t1405046456 ** get_address_of_native_9() { return &___native_9; }
	inline void set_native_9(DeflateStreamNative_t1405046456 * value)
	{
		___native_9 = value;
		Il2CppCodeGenWriteBarrier((&___native_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATESTREAM_T4175168077_H
#ifndef RENAMEDEVENTARGS_T2350765466_H
#define RENAMEDEVENTARGS_T2350765466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.RenamedEventArgs
struct  RenamedEventArgs_t2350765466  : public FileSystemEventArgs_t1603777841
{
public:
	// System.String System.IO.RenamedEventArgs::oldName
	String_t* ___oldName_4;
	// System.String System.IO.RenamedEventArgs::oldFullPath
	String_t* ___oldFullPath_5;

public:
	inline static int32_t get_offset_of_oldName_4() { return static_cast<int32_t>(offsetof(RenamedEventArgs_t2350765466, ___oldName_4)); }
	inline String_t* get_oldName_4() const { return ___oldName_4; }
	inline String_t** get_address_of_oldName_4() { return &___oldName_4; }
	inline void set_oldName_4(String_t* value)
	{
		___oldName_4 = value;
		Il2CppCodeGenWriteBarrier((&___oldName_4), value);
	}

	inline static int32_t get_offset_of_oldFullPath_5() { return static_cast<int32_t>(offsetof(RenamedEventArgs_t2350765466, ___oldFullPath_5)); }
	inline String_t* get_oldFullPath_5() const { return ___oldFullPath_5; }
	inline String_t** get_address_of_oldFullPath_5() { return &___oldFullPath_5; }
	inline void set_oldFullPath_5(String_t* value)
	{
		___oldFullPath_5 = value;
		Il2CppCodeGenWriteBarrier((&___oldFullPath_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENAMEDEVENTARGS_T2350765466_H
#ifndef RENAMEDEVENTHANDLER_T3047461033_H
#define RENAMEDEVENTHANDLER_T3047461033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.RenamedEventHandler
struct  RenamedEventHandler_t3047461033  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENAMEDEVENTHANDLER_T3047461033_H
#ifndef UNMANAGEDREADORWRITE_T1975956110_H
#define UNMANAGEDREADORWRITE_T1975956110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.DeflateStreamNative/UnmanagedReadOrWrite
struct  UnmanagedReadOrWrite_t1975956110  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMANAGEDREADORWRITE_T1975956110_H
#ifndef WRITEMETHOD_T2538911768_H
#define WRITEMETHOD_T2538911768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.DeflateStream/WriteMethod
struct  WriteMethod_t2538911768  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEMETHOD_T2538911768_H
#ifndef READMETHOD_T893206259_H
#define READMETHOD_T893206259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.DeflateStream/ReadMethod
struct  ReadMethod_t893206259  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READMETHOD_T893206259_H
#ifndef FILESYSTEMWATCHER_T416760199_H
#define FILESYSTEMWATCHER_T416760199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemWatcher
struct  FileSystemWatcher_t416760199  : public Component_t3620823400
{
public:
	// System.Boolean System.IO.FileSystemWatcher::enableRaisingEvents
	bool ___enableRaisingEvents_4;
	// System.String System.IO.FileSystemWatcher::filter
	String_t* ___filter_5;
	// System.Boolean System.IO.FileSystemWatcher::includeSubdirectories
	bool ___includeSubdirectories_6;
	// System.Int32 System.IO.FileSystemWatcher::internalBufferSize
	int32_t ___internalBufferSize_7;
	// System.IO.NotifyFilters System.IO.FileSystemWatcher::notifyFilter
	int32_t ___notifyFilter_8;
	// System.String System.IO.FileSystemWatcher::path
	String_t* ___path_9;
	// System.String System.IO.FileSystemWatcher::fullpath
	String_t* ___fullpath_10;
	// System.ComponentModel.ISynchronizeInvoke System.IO.FileSystemWatcher::synchronizingObject
	RuntimeObject* ___synchronizingObject_11;
	// System.IO.WaitForChangedResult System.IO.FileSystemWatcher::lastData
	WaitForChangedResult_t3377826936  ___lastData_12;
	// System.Boolean System.IO.FileSystemWatcher::waiting
	bool ___waiting_13;
	// System.IO.SearchPattern2 System.IO.FileSystemWatcher::pattern
	SearchPattern2_t2824637351 * ___pattern_14;
	// System.Boolean System.IO.FileSystemWatcher::disposed
	bool ___disposed_15;
	// System.String System.IO.FileSystemWatcher::mangledFilter
	String_t* ___mangledFilter_16;
	// System.IO.FileSystemEventHandler System.IO.FileSystemWatcher::Changed
	FileSystemEventHandler_t1806121106 * ___Changed_19;
	// System.IO.FileSystemEventHandler System.IO.FileSystemWatcher::Created
	FileSystemEventHandler_t1806121106 * ___Created_20;
	// System.IO.FileSystemEventHandler System.IO.FileSystemWatcher::Deleted
	FileSystemEventHandler_t1806121106 * ___Deleted_21;
	// System.IO.ErrorEventHandler System.IO.FileSystemWatcher::Error
	ErrorEventHandler_t2621677363 * ___Error_22;
	// System.IO.RenamedEventHandler System.IO.FileSystemWatcher::Renamed
	RenamedEventHandler_t3047461033 * ___Renamed_23;

public:
	inline static int32_t get_offset_of_enableRaisingEvents_4() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___enableRaisingEvents_4)); }
	inline bool get_enableRaisingEvents_4() const { return ___enableRaisingEvents_4; }
	inline bool* get_address_of_enableRaisingEvents_4() { return &___enableRaisingEvents_4; }
	inline void set_enableRaisingEvents_4(bool value)
	{
		___enableRaisingEvents_4 = value;
	}

	inline static int32_t get_offset_of_filter_5() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___filter_5)); }
	inline String_t* get_filter_5() const { return ___filter_5; }
	inline String_t** get_address_of_filter_5() { return &___filter_5; }
	inline void set_filter_5(String_t* value)
	{
		___filter_5 = value;
		Il2CppCodeGenWriteBarrier((&___filter_5), value);
	}

	inline static int32_t get_offset_of_includeSubdirectories_6() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___includeSubdirectories_6)); }
	inline bool get_includeSubdirectories_6() const { return ___includeSubdirectories_6; }
	inline bool* get_address_of_includeSubdirectories_6() { return &___includeSubdirectories_6; }
	inline void set_includeSubdirectories_6(bool value)
	{
		___includeSubdirectories_6 = value;
	}

	inline static int32_t get_offset_of_internalBufferSize_7() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___internalBufferSize_7)); }
	inline int32_t get_internalBufferSize_7() const { return ___internalBufferSize_7; }
	inline int32_t* get_address_of_internalBufferSize_7() { return &___internalBufferSize_7; }
	inline void set_internalBufferSize_7(int32_t value)
	{
		___internalBufferSize_7 = value;
	}

	inline static int32_t get_offset_of_notifyFilter_8() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___notifyFilter_8)); }
	inline int32_t get_notifyFilter_8() const { return ___notifyFilter_8; }
	inline int32_t* get_address_of_notifyFilter_8() { return &___notifyFilter_8; }
	inline void set_notifyFilter_8(int32_t value)
	{
		___notifyFilter_8 = value;
	}

	inline static int32_t get_offset_of_path_9() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___path_9)); }
	inline String_t* get_path_9() const { return ___path_9; }
	inline String_t** get_address_of_path_9() { return &___path_9; }
	inline void set_path_9(String_t* value)
	{
		___path_9 = value;
		Il2CppCodeGenWriteBarrier((&___path_9), value);
	}

	inline static int32_t get_offset_of_fullpath_10() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___fullpath_10)); }
	inline String_t* get_fullpath_10() const { return ___fullpath_10; }
	inline String_t** get_address_of_fullpath_10() { return &___fullpath_10; }
	inline void set_fullpath_10(String_t* value)
	{
		___fullpath_10 = value;
		Il2CppCodeGenWriteBarrier((&___fullpath_10), value);
	}

	inline static int32_t get_offset_of_synchronizingObject_11() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___synchronizingObject_11)); }
	inline RuntimeObject* get_synchronizingObject_11() const { return ___synchronizingObject_11; }
	inline RuntimeObject** get_address_of_synchronizingObject_11() { return &___synchronizingObject_11; }
	inline void set_synchronizingObject_11(RuntimeObject* value)
	{
		___synchronizingObject_11 = value;
		Il2CppCodeGenWriteBarrier((&___synchronizingObject_11), value);
	}

	inline static int32_t get_offset_of_lastData_12() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___lastData_12)); }
	inline WaitForChangedResult_t3377826936  get_lastData_12() const { return ___lastData_12; }
	inline WaitForChangedResult_t3377826936 * get_address_of_lastData_12() { return &___lastData_12; }
	inline void set_lastData_12(WaitForChangedResult_t3377826936  value)
	{
		___lastData_12 = value;
	}

	inline static int32_t get_offset_of_waiting_13() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___waiting_13)); }
	inline bool get_waiting_13() const { return ___waiting_13; }
	inline bool* get_address_of_waiting_13() { return &___waiting_13; }
	inline void set_waiting_13(bool value)
	{
		___waiting_13 = value;
	}

	inline static int32_t get_offset_of_pattern_14() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___pattern_14)); }
	inline SearchPattern2_t2824637351 * get_pattern_14() const { return ___pattern_14; }
	inline SearchPattern2_t2824637351 ** get_address_of_pattern_14() { return &___pattern_14; }
	inline void set_pattern_14(SearchPattern2_t2824637351 * value)
	{
		___pattern_14 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_14), value);
	}

	inline static int32_t get_offset_of_disposed_15() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___disposed_15)); }
	inline bool get_disposed_15() const { return ___disposed_15; }
	inline bool* get_address_of_disposed_15() { return &___disposed_15; }
	inline void set_disposed_15(bool value)
	{
		___disposed_15 = value;
	}

	inline static int32_t get_offset_of_mangledFilter_16() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___mangledFilter_16)); }
	inline String_t* get_mangledFilter_16() const { return ___mangledFilter_16; }
	inline String_t** get_address_of_mangledFilter_16() { return &___mangledFilter_16; }
	inline void set_mangledFilter_16(String_t* value)
	{
		___mangledFilter_16 = value;
		Il2CppCodeGenWriteBarrier((&___mangledFilter_16), value);
	}

	inline static int32_t get_offset_of_Changed_19() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___Changed_19)); }
	inline FileSystemEventHandler_t1806121106 * get_Changed_19() const { return ___Changed_19; }
	inline FileSystemEventHandler_t1806121106 ** get_address_of_Changed_19() { return &___Changed_19; }
	inline void set_Changed_19(FileSystemEventHandler_t1806121106 * value)
	{
		___Changed_19 = value;
		Il2CppCodeGenWriteBarrier((&___Changed_19), value);
	}

	inline static int32_t get_offset_of_Created_20() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___Created_20)); }
	inline FileSystemEventHandler_t1806121106 * get_Created_20() const { return ___Created_20; }
	inline FileSystemEventHandler_t1806121106 ** get_address_of_Created_20() { return &___Created_20; }
	inline void set_Created_20(FileSystemEventHandler_t1806121106 * value)
	{
		___Created_20 = value;
		Il2CppCodeGenWriteBarrier((&___Created_20), value);
	}

	inline static int32_t get_offset_of_Deleted_21() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___Deleted_21)); }
	inline FileSystemEventHandler_t1806121106 * get_Deleted_21() const { return ___Deleted_21; }
	inline FileSystemEventHandler_t1806121106 ** get_address_of_Deleted_21() { return &___Deleted_21; }
	inline void set_Deleted_21(FileSystemEventHandler_t1806121106 * value)
	{
		___Deleted_21 = value;
		Il2CppCodeGenWriteBarrier((&___Deleted_21), value);
	}

	inline static int32_t get_offset_of_Error_22() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___Error_22)); }
	inline ErrorEventHandler_t2621677363 * get_Error_22() const { return ___Error_22; }
	inline ErrorEventHandler_t2621677363 ** get_address_of_Error_22() { return &___Error_22; }
	inline void set_Error_22(ErrorEventHandler_t2621677363 * value)
	{
		___Error_22 = value;
		Il2CppCodeGenWriteBarrier((&___Error_22), value);
	}

	inline static int32_t get_offset_of_Renamed_23() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___Renamed_23)); }
	inline RenamedEventHandler_t3047461033 * get_Renamed_23() const { return ___Renamed_23; }
	inline RenamedEventHandler_t3047461033 ** get_address_of_Renamed_23() { return &___Renamed_23; }
	inline void set_Renamed_23(RenamedEventHandler_t3047461033 * value)
	{
		___Renamed_23 = value;
		Il2CppCodeGenWriteBarrier((&___Renamed_23), value);
	}
};

struct FileSystemWatcher_t416760199_StaticFields
{
public:
	// System.IO.IFileWatcher System.IO.FileSystemWatcher::watcher
	RuntimeObject* ___watcher_17;
	// System.Object System.IO.FileSystemWatcher::lockobj
	RuntimeObject * ___lockobj_18;

public:
	inline static int32_t get_offset_of_watcher_17() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199_StaticFields, ___watcher_17)); }
	inline RuntimeObject* get_watcher_17() const { return ___watcher_17; }
	inline RuntimeObject** get_address_of_watcher_17() { return &___watcher_17; }
	inline void set_watcher_17(RuntimeObject* value)
	{
		___watcher_17 = value;
		Il2CppCodeGenWriteBarrier((&___watcher_17), value);
	}

	inline static int32_t get_offset_of_lockobj_18() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199_StaticFields, ___lockobj_18)); }
	inline RuntimeObject * get_lockobj_18() const { return ___lockobj_18; }
	inline RuntimeObject ** get_address_of_lockobj_18() { return &___lockobj_18; }
	inline void set_lockobj_18(RuntimeObject * value)
	{
		___lockobj_18 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESYSTEMWATCHER_T416760199_H
#ifndef NOTIFYCOLLECTIONCHANGEDEVENTHANDLER_T2749712960_H
#define NOTIFYCOLLECTIONCHANGEDEVENTHANDLER_T2749712960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NotifyCollectionChangedEventHandler
struct  NotifyCollectionChangedEventHandler_t2749712960  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// COM Callable Wrapper interface definition for System.Collections.Specialized.NotifyCollectionChangedEventHandler
struct INotifyCollectionChangedEventHandler_t2749712960_ComCallableWrapper : Il2CppIUnknown
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL Invoke(Il2CppIInspectable* ___sender0, NotifyCollectionChangedEventArgs_t9239872 * ___e1) = 0;
};

#endif // NOTIFYCOLLECTIONCHANGEDEVENTHANDLER_T2749712960_H
#ifndef FILESYSTEMEVENTHANDLER_T1806121106_H
#define FILESYSTEMEVENTHANDLER_T1806121106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemEventHandler
struct  FileSystemEventHandler_t1806121106  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESYSTEMEVENTHANDLER_T1806121106_H
#ifndef ERROREVENTHANDLER_T2621677363_H
#define ERROREVENTHANDLER_T2621677363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.ErrorEventHandler
struct  ErrorEventHandler_t2621677363  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTHANDLER_T2621677363_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (DesignerSerializerAttribute_t1570548024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3104[3] = 
{
	DesignerSerializerAttribute_t1570548024::get_offset_of_serializerTypeName_0(),
	DesignerSerializerAttribute_t1570548024::get_offset_of_serializerBaseTypeName_1(),
	DesignerSerializerAttribute_t1570548024::get_offset_of_typeId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (InstanceDescriptor_t657473484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3105[3] = 
{
	InstanceDescriptor_t657473484::get_offset_of_member_0(),
	InstanceDescriptor_t657473484::get_offset_of_arguments_1(),
	InstanceDescriptor_t657473484::get_offset_of_isComplete_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (RootDesignerSerializerAttribute_t3074689342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3106[4] = 
{
	RootDesignerSerializerAttribute_t3074689342::get_offset_of_reloadable_0(),
	RootDesignerSerializerAttribute_t3074689342::get_offset_of_serializerTypeName_1(),
	RootDesignerSerializerAttribute_t3074689342::get_offset_of_serializerBaseTypeName_2(),
	RootDesignerSerializerAttribute_t3074689342::get_offset_of_typeId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (CaseSensitiveStringDictionary_t553067329), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (HybridDictionary_t4070033136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3108[3] = 
{
	HybridDictionary_t4070033136::get_offset_of_list_0(),
	HybridDictionary_t4070033136::get_offset_of_hashtable_1(),
	HybridDictionary_t4070033136::get_offset_of_caseInsensitive_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (ListDictionary_t1624492310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3111[5] = 
{
	ListDictionary_t1624492310::get_offset_of_head_0(),
	ListDictionary_t1624492310::get_offset_of_version_1(),
	ListDictionary_t1624492310::get_offset_of_count_2(),
	ListDictionary_t1624492310::get_offset_of_comparer_3(),
	ListDictionary_t1624492310::get_offset_of__syncRoot_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (NodeEnumerator_t3248827953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3112[4] = 
{
	NodeEnumerator_t3248827953::get_offset_of_list_0(),
	NodeEnumerator_t3248827953::get_offset_of_current_1(),
	NodeEnumerator_t3248827953::get_offset_of_version_2(),
	NodeEnumerator_t3248827953::get_offset_of_start_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (NodeKeyValueCollection_t1279341543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3113[2] = 
{
	NodeKeyValueCollection_t1279341543::get_offset_of_list_0(),
	NodeKeyValueCollection_t1279341543::get_offset_of_isKeys_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (NodeKeyValueEnumerator_t642906510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3114[5] = 
{
	NodeKeyValueEnumerator_t642906510::get_offset_of_list_0(),
	NodeKeyValueEnumerator_t642906510::get_offset_of_current_1(),
	NodeKeyValueEnumerator_t642906510::get_offset_of_version_2(),
	NodeKeyValueEnumerator_t642906510::get_offset_of_isKeys_3(),
	NodeKeyValueEnumerator_t642906510::get_offset_of_start_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (DictionaryNode_t417719465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3115[3] = 
{
	DictionaryNode_t417719465::get_offset_of_key_0(),
	DictionaryNode_t417719465::get_offset_of_value_1(),
	DictionaryNode_t417719465::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (NameObjectCollectionBase_t2091847364), -1, sizeof(NameObjectCollectionBase_t2091847364_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3116[10] = 
{
	NameObjectCollectionBase_t2091847364::get_offset_of__readOnly_0(),
	NameObjectCollectionBase_t2091847364::get_offset_of__entriesArray_1(),
	NameObjectCollectionBase_t2091847364::get_offset_of__keyComparer_2(),
	NameObjectCollectionBase_t2091847364::get_offset_of__entriesTable_3(),
	NameObjectCollectionBase_t2091847364::get_offset_of__nullKeyEntry_4(),
	NameObjectCollectionBase_t2091847364::get_offset_of__keys_5(),
	NameObjectCollectionBase_t2091847364::get_offset_of__serializationInfo_6(),
	NameObjectCollectionBase_t2091847364::get_offset_of__version_7(),
	NameObjectCollectionBase_t2091847364::get_offset_of__syncRoot_8(),
	NameObjectCollectionBase_t2091847364_StaticFields::get_offset_of_defaultComparer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (NameObjectEntry_t4224248211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3117[2] = 
{
	NameObjectEntry_t4224248211::get_offset_of_Key_0(),
	NameObjectEntry_t4224248211::get_offset_of_Value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (NameObjectKeysEnumerator_t3824388371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3118[3] = 
{
	NameObjectKeysEnumerator_t3824388371::get_offset_of__pos_0(),
	NameObjectKeysEnumerator_t3824388371::get_offset_of__coll_1(),
	NameObjectKeysEnumerator_t3824388371::get_offset_of__version_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (KeysCollection_t1318642398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3119[1] = 
{
	KeysCollection_t1318642398::get_offset_of__coll_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (CompatibleComparer_t4154576053), -1, sizeof(CompatibleComparer_t4154576053_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3120[4] = 
{
	CompatibleComparer_t4154576053::get_offset_of__comparer_0(),
	CompatibleComparer_t4154576053_StaticFields::get_offset_of_defaultComparer_1(),
	CompatibleComparer_t4154576053::get_offset_of__hcp_2(),
	CompatibleComparer_t4154576053_StaticFields::get_offset_of_defaultHashProvider_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (NameValueCollection_t407452768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3121[2] = 
{
	NameValueCollection_t407452768::get_offset_of__all_10(),
	NameValueCollection_t407452768::get_offset_of__allKeys_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (NotifyCollectionChangedAction_t1056453382)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3122[6] = 
{
	NotifyCollectionChangedAction_t1056453382::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (NotifyCollectionChangedEventArgs_t9239872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3123[5] = 
{
	NotifyCollectionChangedEventArgs_t9239872::get_offset_of__action_1(),
	NotifyCollectionChangedEventArgs_t9239872::get_offset_of__newItems_2(),
	NotifyCollectionChangedEventArgs_t9239872::get_offset_of__oldItems_3(),
	NotifyCollectionChangedEventArgs_t9239872::get_offset_of__newStartingIndex_4(),
	NotifyCollectionChangedEventArgs_t9239872::get_offset_of__oldStartingIndex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (NotifyCollectionChangedEventHandler_t2749712960), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (OrderedDictionary_t2617496293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3125[7] = 
{
	OrderedDictionary_t2617496293::get_offset_of__objectsArray_0(),
	OrderedDictionary_t2617496293::get_offset_of__objectsTable_1(),
	OrderedDictionary_t2617496293::get_offset_of__initialCapacity_2(),
	OrderedDictionary_t2617496293::get_offset_of__comparer_3(),
	OrderedDictionary_t2617496293::get_offset_of__readOnly_4(),
	OrderedDictionary_t2617496293::get_offset_of__syncRoot_5(),
	OrderedDictionary_t2617496293::get_offset_of__siInfo_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (OrderedDictionaryEnumerator_t1215437281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3126[2] = 
{
	OrderedDictionaryEnumerator_t1215437281::get_offset_of__objectReturnType_0(),
	OrderedDictionaryEnumerator_t1215437281::get_offset_of_arrayEnumerator_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (OrderedDictionaryKeyValueCollection_t1788601968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3127[2] = 
{
	OrderedDictionaryKeyValueCollection_t1788601968::get_offset_of__objects_0(),
	OrderedDictionaryKeyValueCollection_t1788601968::get_offset_of_isKeys_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (StringCollection_t167406615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3128[1] = 
{
	StringCollection_t167406615::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (StringDictionary_t120437468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3129[1] = 
{
	StringDictionary_t120437468::get_offset_of_contents_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3134[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3135[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3136[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3137[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3138[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3139[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3140[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (DefaultWatcherData_t3310779826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3141[9] = 
{
	DefaultWatcherData_t3310779826::get_offset_of_FSW_0(),
	DefaultWatcherData_t3310779826::get_offset_of_Directory_1(),
	DefaultWatcherData_t3310779826::get_offset_of_FileMask_2(),
	DefaultWatcherData_t3310779826::get_offset_of_IncludeSubdirs_3(),
	DefaultWatcherData_t3310779826::get_offset_of_Enabled_4(),
	DefaultWatcherData_t3310779826::get_offset_of_NoWildcards_5(),
	DefaultWatcherData_t3310779826::get_offset_of_DisabledTime_6(),
	DefaultWatcherData_t3310779826::get_offset_of_FilesLock_7(),
	DefaultWatcherData_t3310779826::get_offset_of_Files_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (FileData_t355659623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3142[5] = 
{
	FileData_t355659623::get_offset_of_Directory_0(),
	FileData_t355659623::get_offset_of_Attributes_1(),
	FileData_t355659623::get_offset_of_NotExists_2(),
	FileData_t355659623::get_offset_of_CreationTime_3(),
	FileData_t355659623::get_offset_of_LastWriteTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (DefaultWatcher_t2229106420), -1, sizeof(DefaultWatcher_t2229106420_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3143[4] = 
{
	DefaultWatcher_t2229106420_StaticFields::get_offset_of_instance_0(),
	DefaultWatcher_t2229106420_StaticFields::get_offset_of_thread_1(),
	DefaultWatcher_t2229106420_StaticFields::get_offset_of_watches_2(),
	DefaultWatcher_t2229106420_StaticFields::get_offset_of_NoStringsArray_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (ErrorEventArgs_t1584858912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3144[1] = 
{
	ErrorEventArgs_t1584858912::get_offset_of_exception_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (ErrorEventHandler_t2621677363), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (FAMConnection_t1678998633)+ sizeof (RuntimeObject), sizeof(FAMConnection_t1678998633 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3146[2] = 
{
	FAMConnection_t1678998633::get_offset_of_FD_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FAMConnection_t1678998633::get_offset_of_opaque_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (FAMRequest_t3578860103)+ sizeof (RuntimeObject), sizeof(FAMRequest_t3578860103 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3147[1] = 
{
	FAMRequest_t3578860103::get_offset_of_ReqNum_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (FAMCodes_t1537683418)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3148[10] = 
{
	FAMCodes_t1537683418::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (FAMData_t1817296356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3149[7] = 
{
	FAMData_t1817296356::get_offset_of_FSW_0(),
	FAMData_t1817296356::get_offset_of_Directory_1(),
	FAMData_t1817296356::get_offset_of_FileMask_2(),
	FAMData_t1817296356::get_offset_of_IncludeSubdirs_3(),
	FAMData_t1817296356::get_offset_of_Enabled_4(),
	FAMData_t1817296356::get_offset_of_Request_5(),
	FAMData_t1817296356::get_offset_of_SubDirs_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (FAMWatcher_t3228827479), -1, sizeof(FAMWatcher_t3228827479_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3150[8] = 
{
	FAMWatcher_t3228827479_StaticFields::get_offset_of_failed_0(),
	FAMWatcher_t3228827479_StaticFields::get_offset_of_instance_1(),
	FAMWatcher_t3228827479_StaticFields::get_offset_of_watches_2(),
	FAMWatcher_t3228827479_StaticFields::get_offset_of_requests_3(),
	FAMWatcher_t3228827479_StaticFields::get_offset_of_conn_4(),
	FAMWatcher_t3228827479_StaticFields::get_offset_of_thread_5(),
	FAMWatcher_t3228827479_StaticFields::get_offset_of_stop_6(),
	FAMWatcher_t3228827479_StaticFields::get_offset_of_use_gamin_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (FileAction_t3839877343)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3151[6] = 
{
	FileAction_t3839877343::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (FileSystemEventArgs_t1603777841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3152[3] = 
{
	FileSystemEventArgs_t1603777841::get_offset_of_changeType_1(),
	FileSystemEventArgs_t1603777841::get_offset_of_directory_2(),
	FileSystemEventArgs_t1603777841::get_offset_of_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { sizeof (FileSystemEventHandler_t1806121106), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { sizeof (FileSystemWatcher_t416760199), -1, sizeof(FileSystemWatcher_t416760199_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3154[20] = 
{
	FileSystemWatcher_t416760199::get_offset_of_enableRaisingEvents_4(),
	FileSystemWatcher_t416760199::get_offset_of_filter_5(),
	FileSystemWatcher_t416760199::get_offset_of_includeSubdirectories_6(),
	FileSystemWatcher_t416760199::get_offset_of_internalBufferSize_7(),
	FileSystemWatcher_t416760199::get_offset_of_notifyFilter_8(),
	FileSystemWatcher_t416760199::get_offset_of_path_9(),
	FileSystemWatcher_t416760199::get_offset_of_fullpath_10(),
	FileSystemWatcher_t416760199::get_offset_of_synchronizingObject_11(),
	FileSystemWatcher_t416760199::get_offset_of_lastData_12(),
	FileSystemWatcher_t416760199::get_offset_of_waiting_13(),
	FileSystemWatcher_t416760199::get_offset_of_pattern_14(),
	FileSystemWatcher_t416760199::get_offset_of_disposed_15(),
	FileSystemWatcher_t416760199::get_offset_of_mangledFilter_16(),
	FileSystemWatcher_t416760199_StaticFields::get_offset_of_watcher_17(),
	FileSystemWatcher_t416760199_StaticFields::get_offset_of_lockobj_18(),
	FileSystemWatcher_t416760199::get_offset_of_Changed_19(),
	FileSystemWatcher_t416760199::get_offset_of_Created_20(),
	FileSystemWatcher_t416760199::get_offset_of_Deleted_21(),
	FileSystemWatcher_t416760199::get_offset_of_Error_22(),
	FileSystemWatcher_t416760199::get_offset_of_Renamed_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { sizeof (EventType_t2132206657)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3155[4] = 
{
	EventType_t2132206657::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { sizeof (IODescriptionAttribute_t2336130459), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { sizeof (InotifyMask_t3323194736)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3158[22] = 
{
	InotifyMask_t3323194736::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { sizeof (InotifyEvent_t2637353984)+ sizeof (RuntimeObject), sizeof(InotifyEvent_t2637353984_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3159[3] = 
{
	InotifyEvent_t2637353984::get_offset_of_WatchDescriptor_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InotifyEvent_t2637353984::get_offset_of_Mask_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InotifyEvent_t2637353984::get_offset_of_Name_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (ParentInotifyData_t1149622319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3160[4] = 
{
	ParentInotifyData_t1149622319::get_offset_of_IncludeSubdirs_0(),
	ParentInotifyData_t1149622319::get_offset_of_Enabled_1(),
	ParentInotifyData_t1149622319::get_offset_of_children_2(),
	ParentInotifyData_t1149622319::get_offset_of_data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (InotifyData_t2533354870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3161[3] = 
{
	InotifyData_t2533354870::get_offset_of_FSW_0(),
	InotifyData_t2533354870::get_offset_of_Directory_1(),
	InotifyData_t2533354870::get_offset_of_Watch_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (InotifyWatcher_t2392648639), -1, sizeof(InotifyWatcher_t2392648639_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3162[8] = 
{
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_failed_0(),
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_instance_1(),
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_watches_2(),
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_requests_3(),
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_FD_4(),
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_thread_5(),
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_stop_6(),
	InotifyWatcher_t2392648639_StaticFields::get_offset_of_Interesting_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { sizeof (U3CGetEnumeratorU3Ed__18_t986872067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3163[7] = 
{
	U3CGetEnumeratorU3Ed__18_t986872067::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__18_t986872067::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__18_t986872067::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetEnumeratorU3Ed__18_t986872067::get_offset_of_source_3(),
	U3CGetEnumeratorU3Ed__18_t986872067::get_offset_of_U3CU3E3__source_4(),
	U3CGetEnumeratorU3Ed__18_t986872067::get_offset_of_U3ClistU3E5__1_5(),
	U3CGetEnumeratorU3Ed__18_t986872067::get_offset_of_U3CiU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (EventFlags_t1670942752)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3164[14] = 
{
	EventFlags_t1670942752::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (EventFilter_t2065690828)+ sizeof (RuntimeObject), sizeof(int16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3165[12] = 
{
	EventFilter_t2065690828::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { sizeof (FilterFlags_t1388790893)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3166[42] = 
{
	FilterFlags_t1388790893::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { sizeof (kevent_t2406656960)+ sizeof (RuntimeObject), sizeof(kevent_t2406656960 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3167[6] = 
{
	kevent_t2406656960::get_offset_of_ident_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	kevent_t2406656960::get_offset_of_filter_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	kevent_t2406656960::get_offset_of_flags_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	kevent_t2406656960::get_offset_of_fflags_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	kevent_t2406656960::get_offset_of_data_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	kevent_t2406656960::get_offset_of_udata_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { sizeof (timespec_t893749660)+ sizeof (RuntimeObject), sizeof(timespec_t893749660 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3168[2] = 
{
	timespec_t893749660::get_offset_of_tv_sec_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	timespec_t893749660::get_offset_of_tv_nsec_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { sizeof (PathData_t3217195029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3169[3] = 
{
	PathData_t3217195029::get_offset_of_Path_0(),
	PathData_t3217195029::get_offset_of_IsDirectory_1(),
	PathData_t3217195029::get_offset_of_Fd_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { sizeof (KqueueMonitor_t3818269198), -1, sizeof(KqueueMonitor_t3818269198_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3170[17] = 
{
	KqueueMonitor_t3818269198_StaticFields::get_offset_of_initialized_0(),
	KqueueMonitor_t3818269198_StaticFields::get_offset_of_emptyEventList_1(),
	KqueueMonitor_t3818269198::get_offset_of_maxFds_2(),
	KqueueMonitor_t3818269198::get_offset_of_fsw_3(),
	KqueueMonitor_t3818269198::get_offset_of_conn_4(),
	KqueueMonitor_t3818269198::get_offset_of_thread_5(),
	KqueueMonitor_t3818269198::get_offset_of_requestStop_6(),
	KqueueMonitor_t3818269198::get_offset_of_startedEvent_7(),
	KqueueMonitor_t3818269198::get_offset_of_started_8(),
	KqueueMonitor_t3818269198::get_offset_of_inDispatch_9(),
	KqueueMonitor_t3818269198::get_offset_of_exc_10(),
	KqueueMonitor_t3818269198::get_offset_of_stateLock_11(),
	KqueueMonitor_t3818269198::get_offset_of_connLock_12(),
	KqueueMonitor_t3818269198::get_offset_of_pathsDict_13(),
	KqueueMonitor_t3818269198::get_offset_of_fdsDict_14(),
	KqueueMonitor_t3818269198::get_offset_of_fixupPath_15(),
	KqueueMonitor_t3818269198::get_offset_of_fullPathNoLastSlash_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { sizeof (U3CU3Ec__DisplayClass11_0_t2361320332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3171[3] = 
{
	U3CU3Ec__DisplayClass11_0_t2361320332::get_offset_of_newFds_0(),
	U3CU3Ec__DisplayClass11_0_t2361320332::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass11_0_t2361320332::get_offset_of_U3CU3E9__0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { sizeof (KeventWatcher_t3355132332), -1, sizeof(KeventWatcher_t3355132332_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3172[3] = 
{
	KeventWatcher_t3355132332_StaticFields::get_offset_of_failed_0(),
	KeventWatcher_t3355132332_StaticFields::get_offset_of_instance_1(),
	KeventWatcher_t3355132332_StaticFields::get_offset_of_watches_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (NotifyFilters_t3825601669)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3173[9] = 
{
	NotifyFilters_t3825601669::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (NullFileWatcher_t1718214284), -1, sizeof(NullFileWatcher_t1718214284_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3174[1] = 
{
	NullFileWatcher_t1718214284_StaticFields::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { sizeof (RenamedEventArgs_t2350765466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3175[2] = 
{
	RenamedEventArgs_t2350765466::get_offset_of_oldName_4(),
	RenamedEventArgs_t2350765466::get_offset_of_oldFullPath_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { sizeof (RenamedEventHandler_t3047461033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { sizeof (SearchPattern2_t2824637351), -1, sizeof(SearchPattern2_t2824637351_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3177[6] = 
{
	SearchPattern2_t2824637351::get_offset_of_ops_0(),
	SearchPattern2_t2824637351::get_offset_of_ignore_1(),
	SearchPattern2_t2824637351::get_offset_of_hasWildcard_2(),
	SearchPattern2_t2824637351::get_offset_of_pattern_3(),
	SearchPattern2_t2824637351_StaticFields::get_offset_of_WildcardChars_4(),
	SearchPattern2_t2824637351_StaticFields::get_offset_of_InvalidChars_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { sizeof (Op_t3134810481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3178[3] = 
{
	Op_t3134810481::get_offset_of_Code_0(),
	Op_t3134810481::get_offset_of_Argument_1(),
	Op_t3134810481::get_offset_of_Next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { sizeof (OpCode_t3581930920)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3179[6] = 
{
	OpCode_t3581930920::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { sizeof (WaitForChangedResult_t3377826936)+ sizeof (RuntimeObject), sizeof(WaitForChangedResult_t3377826936_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3180[4] = 
{
	WaitForChangedResult_t3377826936::get_offset_of_changeType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WaitForChangedResult_t3377826936::get_offset_of_name_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WaitForChangedResult_t3377826936::get_offset_of_oldName_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WaitForChangedResult_t3377826936::get_offset_of_timedOut_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { sizeof (WatcherChangeTypes_t673177441)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3181[6] = 
{
	WatcherChangeTypes_t673177441::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { sizeof (WindowsWatcher_t1958179572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { sizeof (CompressionMode_t3714291783)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3183[3] = 
{
	CompressionMode_t3714291783::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { sizeof (GZipStream_t3417139389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3184[1] = 
{
	GZipStream_t3417139389::get_offset_of__deflateStream_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { sizeof (DeflateStream_t4175168077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3185[5] = 
{
	DeflateStream_t4175168077::get_offset_of_base_stream_5(),
	DeflateStream_t4175168077::get_offset_of_mode_6(),
	DeflateStream_t4175168077::get_offset_of_leaveOpen_7(),
	DeflateStream_t4175168077::get_offset_of_disposed_8(),
	DeflateStream_t4175168077::get_offset_of_native_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { sizeof (ReadMethod_t893206259), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { sizeof (WriteMethod_t2538911768), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { sizeof (DeflateStreamNative_t1405046456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3188[6] = 
{
	DeflateStreamNative_t1405046456::get_offset_of_feeder_0(),
	DeflateStreamNative_t1405046456::get_offset_of_base_stream_1(),
	DeflateStreamNative_t1405046456::get_offset_of_z_stream_2(),
	DeflateStreamNative_t1405046456::get_offset_of_data_3(),
	DeflateStreamNative_t1405046456::get_offset_of_disposed_4(),
	DeflateStreamNative_t1405046456::get_offset_of_io_buffer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { sizeof (UnmanagedReadOrWrite_t1975956110), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255365), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3190[16] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U30283A6AF88802AB45989B29549915BEA0F6CD515_0(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U303F4297FCC30D0FD5E420E5D26E7FA711167C7EF_1(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U31A39764B112685485A5BA7B2880D878B858C1A7A_2(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U31A84029C80CB5518379F199F53FF08A7B764F8FD_3(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U33BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_4(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_5(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U35BC3486B05BA8CF4689C7BDB198B3F477BB4E20C_6(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U36D49C9D487D7AD3491ECE08732D68A593CC2038D_7(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U36F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_8(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U38E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_9(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U398A44A6F8606AE6F23FE230286C1D6FBCC407226_10(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_11(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_CCEEADA43268372341F81AE0C9208C6856441C04_12(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_13(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_EC5842B3154E1AF94500B57220EB9F684BCCC42A_14(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_EEAFE8C6E1AB017237567305EE925C976CDB6458_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { sizeof (__StaticArrayInitTypeSizeU3D3_t3217885684)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D3_t3217885684 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { sizeof (__StaticArrayInitTypeSizeU3D6_t3217689076)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D6_t3217689076 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { sizeof (__StaticArrayInitTypeSizeU3D9_t3218278900)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D9_t3218278900 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { sizeof (__StaticArrayInitTypeSizeU3D12_t2710994319)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t2710994319 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { sizeof (__StaticArrayInitTypeSizeU3D14_t3517563373)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D14_t3517563373 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { sizeof (__StaticArrayInitTypeSizeU3D32_t2711125392)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_t2711125392 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { sizeof (__StaticArrayInitTypeSizeU3D44_t3517366765)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D44_t3517366765 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { sizeof (__StaticArrayInitTypeSizeU3D128_t531529102)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D128_t531529102 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { sizeof (__StaticArrayInitTypeSizeU3D256_t1757367633)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D256_t1757367633 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
