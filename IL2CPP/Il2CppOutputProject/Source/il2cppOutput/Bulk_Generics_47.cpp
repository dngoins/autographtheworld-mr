﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>>
struct IIterableToIEnumerableAdapter_1_t2812313252;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>>
struct IEnumerator_1_t946956468;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.UInt32,System.Object>>
struct IIterableToIEnumerableAdapter_1_t3972198270;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.UInt32,System.Object>>
struct IEnumerator_1_t2106841486;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<UnityEngine.Vector3,System.Object>>
struct IIterableToIEnumerableAdapter_1_t2920148328;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<UnityEngine.Vector3,System.Object>>
struct IEnumerator_1_t1054791544;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>>
struct IIterableToIEnumerableAdapter_1_t1519240120;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>>
struct IEnumerator_1_t3948850632;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.Decimal>>
struct IIterableToIEnumerableAdapter_1_t1917209929;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.HashSet`1/Slot<System.Decimal>>
struct IEnumerator_1_t51853145;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.Int32>>
struct IIterableToIEnumerableAdapter_1_t1919896302;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.HashSet`1/Slot<System.Int32>>
struct IEnumerator_1_t54539518;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.Object>>
struct IIterableToIEnumerableAdapter_1_t2049056713;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.HashSet`1/Slot<System.Object>>
struct IEnumerator_1_t183699929;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.UInt32>>
struct IIterableToIEnumerableAdapter_1_t1529012527;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.HashSet`1/Slot<System.UInt32>>
struct IEnumerator_1_t3958623039;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>>
struct IIterableToIEnumerableAdapter_1_t544572541;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>>
struct IEnumerator_1_t2974183053;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>
struct IIterableToIEnumerableAdapter_1_t875565155;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>
struct IEnumerator_1_t3305175667;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>
struct IIterableToIEnumerableAdapter_1_t2942639410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>
struct IEnumerator_1_t1077282626;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>
struct IIterableToIEnumerableAdapter_1_t1270861356;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>
struct IEnumerator_1_t3700471868;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>
struct IIterableToIEnumerableAdapter_1_t3051555607;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>
struct IEnumerator_1_t1186198823;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>
struct IIterableToIEnumerableAdapter_1_t2529755820;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>
struct IEnumerator_1_t664399036;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>>
struct IIterableToIEnumerableAdapter_1_t3346060944;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>>
struct IEnumerator_1_t1480704160;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IIterableToIEnumerableAdapter_1_t2369451618;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t504094834;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct IIterableToIEnumerableAdapter_1_t248410775;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct IEnumerator_1_t2678021287;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>
struct IIterableToIEnumerableAdapter_1_t1702604006;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>
struct IEnumerator_1_t4132214518;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>>
struct IIterableToIEnumerableAdapter_1_t2423931679;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>>
struct IEnumerator_1_t558574895;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>>
struct IIterableToIEnumerableAdapter_1_t2553092090;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>>
struct IEnumerator_1_t687735306;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>
struct IIterableToIEnumerableAdapter_1_t2088824987;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>
struct IEnumerator_1_t223468203;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>
struct IIterableToIEnumerableAdapter_1_t2546988311;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>
struct IEnumerator_1_t681631527;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>>
struct IIterableToIEnumerableAdapter_1_t258322578;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>>
struct IEnumerator_1_t2687933090;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct IIterableToIEnumerableAdapter_1_t1845326372;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct IEnumerator_1_t4274936884;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IIterableToIEnumerableAdapter_1_t404016864;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IEnumerator_1_t2833627376;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>>
struct IIterableToIEnumerableAdapter_1_t733360700;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>>
struct IEnumerator_1_t3162971212;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IIterableToIEnumerableAdapter_1_t533177275;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t2962787787;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>
struct IIterableToIEnumerableAdapter_1_t1177041918;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>
struct IEnumerator_1_t3606652430;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>>
struct IIterableToIEnumerableAdapter_1_t741577607;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>>
struct IEnumerator_1_t3171188119;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>>
struct IIterableToIEnumerableAdapter_1_t2256902432;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>>
struct IEnumerator_1_t391545648;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>>
struct IIterableToIEnumerableAdapter_1_t3595120931;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>>
struct IEnumerator_1_t1729764147;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>
struct IIterableToIEnumerableAdapter_1_t460038653;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>
struct IEnumerator_1_t2889649165;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>
struct IIterableToIEnumerableAdapter_1_t3702956007;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>
struct IEnumerator_1_t1837599223;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>>
struct IIterableToIEnumerableAdapter_1_t2302047799;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>>
struct IEnumerator_1_t436691015;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Marker>
struct IIterableToIEnumerableAdapter_1_t897737733;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Marker>
struct IEnumerator_1_t3327348245;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Hashtable/bucket>
struct IIterableToIEnumerableAdapter_1_t3056058956;
// System.Collections.Generic.IEnumerator`1<System.Collections.Hashtable/bucket>
struct IEnumerator_1_t1190702172;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.ComponentModel.AttributeCollection/AttributeEntry>
struct IIterableToIEnumerableAdapter_1_t3298938115;
// System.Collections.Generic.IEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>
struct IEnumerator_1_t1433581331;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.DateTime>
struct IIterableToIEnumerableAdapter_1_t1741489741;
// System.Collections.Generic.IEnumerator`1<System.DateTime>
struct IEnumerator_1_t4171100253;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.DateTimeOffset>
struct IIterableToIEnumerableAdapter_1_t1232247463;
// System.Collections.Generic.IEnumerator`1<System.DateTimeOffset>
struct IEnumerator_1_t3661857975;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.DateTimeParse/DS>
struct IIterableToIEnumerableAdapter_1_t235230326;
// System.Collections.Generic.IEnumerator`1<System.DateTimeParse/DS>
struct IEnumerator_1_t2664840838;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Decimal>
struct IIterableToIEnumerableAdapter_1_t951219336;
// System.Collections.Generic.IEnumerator`1<System.Decimal>
struct IEnumerator_1_t3380829848;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Double>
struct IIterableToIEnumerableAdapter_1_t2892592615;
// System.Collections.Generic.IEnumerator`1<System.Double>
struct IEnumerator_1_t1027235831;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Globalization.HebrewNumber/HS>
struct IIterableToIEnumerableAdapter_1_t1342732972;
// System.Collections.Generic.IEnumerator`1<System.Globalization.HebrewNumber/HS>
struct IEnumerator_1_t3772343484;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Globalization.InternalCodePageDataItem>
struct IIterableToIEnumerableAdapter_1_t578492889;
// System.Collections.Generic.IEnumerator`1<System.Globalization.InternalCodePageDataItem>
struct IEnumerator_1_t3008103401;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Globalization.InternalEncodingDataItem>
struct IIterableToIEnumerableAdapter_1_t1161819773;
// System.Collections.Generic.IEnumerator`1<System.Globalization.InternalEncodingDataItem>
struct IEnumerator_1_t3591430285;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Globalization.TimeSpanParse/TimeSpanToken>
struct IIterableToIEnumerableAdapter_1_t3291274626;
// System.Collections.Generic.IEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>
struct IEnumerator_1_t1425917842;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Guid>
struct IIterableToIEnumerableAdapter_1_t1196492843;
// System.Collections.Generic.IEnumerator`1<System.Guid>
struct IEnumerator_1_t3626103355;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Int16>
struct IIterableToIEnumerableAdapter_1_t555780343;
// System.Collections.Generic.IEnumerator`1<System.Int16>
struct IEnumerator_1_t2985390855;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Int32>
struct IIterableToIEnumerableAdapter_1_t953905709;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3383516221;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Int64>
struct IIterableToIEnumerableAdapter_1_t1739527260;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t4169137772;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.IntPtr>
struct IIterableToIEnumerableAdapter_1_t3138077433;
// System.Collections.Generic.IEnumerator`1<System.IntPtr>
struct IEnumerator_1_t1272720649;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.IO.kevent>
struct IIterableToIEnumerableAdapter_1_t409616916;
// System.Collections.Generic.IEnumerator`1<System.IO.kevent>
struct IEnumerator_1_t2839227428;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant>
struct IIterableToIEnumerableAdapter_1_t3011948149;
// System.Collections.Generic.IEnumerator`1<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant>
struct IEnumerator_1_t1146591365;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Linq.Expressions.Compiler.VariableStorageKind>
struct IIterableToIEnumerableAdapter_1_t1283249545;
// System.Collections.Generic.IEnumerator`1<System.Linq.Expressions.Compiler.VariableStorageKind>
struct IEnumerator_1_t3712860057;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Linq.Set`1/Slot<System.Char>>
struct IIterableToIEnumerableAdapter_1_t1192369078;
// System.Collections.Generic.IEnumerator`1<System.Linq.Set`1/Slot<System.Char>>
struct IEnumerator_1_t3621979590;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Linq.Set`1/Slot<System.Object>>
struct IIterableToIEnumerableAdapter_1_t638014772;
// System.Collections.Generic.IEnumerator`1<System.Linq.Set`1/Slot<System.Object>>
struct IEnumerator_1_t3067625284;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Net.CookieTokenizer/RecognizedAttribute>
struct IIterableToIEnumerableAdapter_1_t2930001472;
// System.Collections.Generic.IEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>
struct IEnumerator_1_t1064644688;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Net.HeaderVariantInfo>
struct IIterableToIEnumerableAdapter_1_t4233612853;
// System.Collections.Generic.IEnumerator`1<System.Net.HeaderVariantInfo>
struct IEnumerator_1_t2368256069;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>
struct IIterableToIEnumerableAdapter_1_t1466486284;
// System.Collections.Generic.IEnumerator`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>
struct IEnumerator_1_t3896096796;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Net.Sockets.Socket/WSABUF>
struct IIterableToIEnumerableAdapter_1_t1019346;
// System.Collections.Generic.IEnumerator`1<System.Net.Sockets.Socket/WSABUF>
struct IEnumerator_1_t2430629858;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Net.WebHeaderCollection/RfcChar>
struct IIterableToIEnumerableAdapter_1_t908369886;
// System.Collections.Generic.IEnumerator`1<System.Net.WebHeaderCollection/RfcChar>
struct IEnumerator_1_t3337980398;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Object>
struct IIterableToIEnumerableAdapter_1_t1083066120;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t3512676632;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.ParameterizedStrings/FormatParam>
struct IIterableToIEnumerableAdapter_1_t2197434038;
// System.Collections.Generic.IEnumerator`1<System.ParameterizedStrings/FormatParam>
struct IEnumerator_1_t332077254;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.CustomAttributeNamedArgument>
struct IIterableToIEnumerableAdapter_1_t2585792962;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t720436178;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.CustomAttributeTypedArgument>
struct IIterableToIEnumerableAdapter_1_t726110113;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t3155720625;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.ILExceptionBlock>
struct IIterableToIEnumerableAdapter_1_t1964834922;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ILExceptionBlock>
struct IEnumerator_1_t99478138;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.ILExceptionInfo>
struct IIterableToIEnumerableAdapter_1_t2535783262;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ILExceptionInfo>
struct IEnumerator_1_t670426478;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.ILGenerator/LabelData>
struct IIterableToIEnumerableAdapter_1_t2658094643;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>
struct IEnumerator_1_t792737859;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.ILGenerator/LabelFixup>
struct IIterableToIEnumerableAdapter_1_t3156429306;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>
struct IEnumerator_1_t1291072522;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.ILTokenInfo>
struct IIterableToIEnumerableAdapter_1_t328735070;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ILTokenInfo>
struct IEnumerator_1_t2758345582;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.Label>
struct IIterableToIEnumerableAdapter_1_t284621599;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.Label>
struct IEnumerator_1_t2714232111;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.MonoResource>
struct IIterableToIEnumerableAdapter_1_t2106389965;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.MonoResource>
struct IEnumerator_1_t241033181;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.MonoWin32Resource>
struct IIterableToIEnumerableAdapter_1_t4202156735;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.MonoWin32Resource>
struct IEnumerator_1_t2336799951;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.RefEmitPermissionSet>
struct IIterableToIEnumerableAdapter_1_t2782318239;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>
struct IEnumerator_1_t916961455;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.SequencePoint>
struct IIterableToIEnumerableAdapter_1_t68225812;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.SequencePoint>
struct IEnumerator_1_t2497836324;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.ParameterModifier>
struct IIterableToIEnumerableAdapter_1_t3759621718;
// System.Collections.Generic.IEnumerator`1<System.Reflection.ParameterModifier>
struct IEnumerator_1_t1894264934;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Resources.ResourceLocator>
struct IIterableToIEnumerableAdapter_1_t1726930763;
// System.Collections.Generic.IEnumerator`1<System.Resources.ResourceLocator>
struct IEnumerator_1_t4156541275;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.CompilerServices.Ephemeron>
struct IIterableToIEnumerableAdapter_1_t3900523614;
// System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.Ephemeron>
struct IEnumerator_1_t2035166830;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.InteropServices.GCHandle>
struct IIterableToIEnumerableAdapter_1_t1354398143;
// System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.GCHandle>
struct IEnumerator_1_t3784008655;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct IIterableToIEnumerableAdapter_1_t2616818040;
// System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct IEnumerator_1_t751461256;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>
struct IIterableToIEnumerableAdapter_1_t1291466452;
// System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>
struct IEnumerator_1_t3721076964;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>
struct IIterableToIEnumerableAdapter_1_t2454372451;
// System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>
struct IEnumerator_1_t589015667;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey>
struct IIterableToIEnumerableAdapter_1_t1136580678;
// System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey>
struct IEnumerator_1_t3566191190;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>
struct IIterableToIEnumerableAdapter_1_t1488396410;
// System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>
struct IEnumerator_1_t3918006922;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>
struct IIterableToIEnumerableAdapter_1_t2096008933;
// System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>
struct IEnumerator_1_t230652149;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.SByte>
struct IIterableToIEnumerableAdapter_1_t3967504914;
// System.Collections.Generic.IEnumerator`1<System.SByte>
struct IEnumerator_1_t2102148130;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>
struct IIterableToIEnumerableAdapter_1_t2431529966;
// System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>
struct IEnumerator_1_t566173182;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Single>
struct IIterableToIEnumerableAdapter_1_t3695194026;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t1829837242;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.TermInfoStrings>
struct IIterableToIEnumerableAdapter_1_t2588207207;
// System.Collections.Generic.IEnumerator`1<System.TermInfoStrings>
struct IEnumerator_1_t722850423;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>
struct IIterableToIEnumerableAdapter_1_t913277531;
// System.Collections.Generic.IEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>
struct IEnumerator_1_t3342888043;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Text.RegularExpressions.RegexOptions>
struct IIterableToIEnumerableAdapter_1_t2390772847;
// System.Collections.Generic.IEnumerator`1<System.Text.RegularExpressions.RegexOptions>
struct IEnumerator_1_t525416063;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Threading.CancellationTokenRegistration>
struct IIterableToIEnumerableAdapter_1_t816384860;
// System.Collections.Generic.IEnumerator`1<System.Threading.CancellationTokenRegistration>
struct IEnumerator_1_t3245995372;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.TimeSpan>
struct IIterableToIEnumerableAdapter_1_t3179086501;
// System.Collections.Generic.IEnumerator`1<System.TimeSpan>
struct IEnumerator_1_t1313729717;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.TimeZoneInfo/TZifType>
struct IIterableToIEnumerableAdapter_1_t114737315;
// System.Collections.Generic.IEnumerator`1<System.TimeZoneInfo/TZifType>
struct IEnumerator_1_t2544347827;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.TypeCode>
struct IIterableToIEnumerableAdapter_1_t990184043;
// System.Collections.Generic.IEnumerator`1<System.TypeCode>
struct IEnumerator_1_t3419794555;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.UInt16>
struct IIterableToIEnumerableAdapter_1_t180684914;
// System.Collections.Generic.IEnumerator`1<System.UInt16>
struct IEnumerator_1_t2610295426;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.UInt32>
struct IIterableToIEnumerableAdapter_1_t563021934;
// System.Collections.Generic.IEnumerator`1<System.UInt32>
struct IEnumerator_1_t2992632446;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.UInt64>
struct IIterableToIEnumerableAdapter_1_t2137000048;
// System.Collections.Generic.IEnumerator`1<System.UInt64>
struct IEnumerator_1_t271643264;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Xml.Linq.XHashtable`1/XHashtableState/Entry<System.Object>>
struct IIterableToIEnumerableAdapter_1_t741461246;
// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XHashtable`1/XHashtableState/Entry<System.Object>>
struct IEnumerator_1_t3171071758;
// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Xml.Schema.FacetsChecker/FacetsCompiler/Map>
struct IIterableToIEnumerableAdapter_1_t3628971679;
// System.Collections.Generic.IEnumerator`1<System.Xml.Schema.FacetsChecker/FacetsCompiler/Map>
struct IEnumerator_1_t1763614895;
// Windows.Foundation.Collections.IIterator`1<System.Object>
struct IIterator_1_t954249637;
// Windows.Foundation.Collections.IIterator`1<System.Int64>
struct IIterator_1_t1610710777;
// Windows.Foundation.Collections.IIterator`1<System.Int16>
struct IIterator_1_t426963860;
// Windows.Foundation.Collections.IIterator`1<System.Int32>
struct IIterator_1_t825089226;
// Windows.Foundation.Collections.IIterator`1<System.SByte>
struct IIterator_1_t3838688431;
// Windows.Foundation.Collections.IIterator`1<System.Single>
struct IIterator_1_t3566377543;
// Windows.Foundation.Collections.IIterator`1<System.UInt64>
struct IIterator_1_t2008183565;
// Windows.Foundation.Collections.IIterator`1<System.UInt16>
struct IIterator_1_t51868431;
// Windows.Foundation.Collections.IIterator`1<System.UInt32>
struct IIterator_1_t434205451;
// Windows.Foundation.Collections.IIterator`1<System.Double>
struct IIterator_1_t2763776132;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Type
struct Type_t;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Threading.SparselyPopulatedArrayFragment`1<System.Threading.CancellationCallbackInfo>
struct SparselyPopulatedArrayFragment_1_t4161250538;
// MS.Internal.Xml.Cache.XPathNode[]
struct XPathNodeU5BU5D_t47339301;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// HoloToolkit.Unity.TimerScheduler/Callback
struct Callback_t2663646540;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventRegistrationTokenListWithCount>
struct ConditionalWeakTable_2_t3044373657;
// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/TokenListCount
struct TokenListCount_t1606756367;
// System.Collections.Generic.List`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct List_1_t1790965530;
// System.Threading.CancellationCallbackInfo
struct CancellationCallbackInfo_t322720759;
// System.Reflection.Emit.ILExceptionBlock[]
struct ILExceptionBlockU5BU5D_t2996808915;
// Windows.Foundation.Collections.IIterator`1<System.Reflection.Emit.ILTokenInfo>
struct IIterator_1_t199918587;
// Windows.Foundation.Collections.IIterator`1<System.Reflection.Emit.Label>
struct IIterator_1_t155805116;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IIterator_1_t404360792;
// Windows.Foundation.Collections.IIterator`1<System.Reflection.Emit.ILGenerator/LabelFixup>
struct IIterator_1_t3027612823;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>
struct IIterator_1_t331222170;
// Windows.Foundation.Collections.IIterator`1<System.Reflection.Emit.ILExceptionBlock>
struct IIterator_1_t1836018439;
// Windows.Foundation.Collections.IIterator`1<System.Reflection.Emit.ILGenerator/LabelData>
struct IIterator_1_t2529278160;
// Windows.Foundation.Collections.IIterator`1<System.Resources.ResourceLocator>
struct IIterator_1_t1598114280;
// Windows.Foundation.Collections.IIterator`1<System.Runtime.CompilerServices.Ephemeron>
struct IIterator_1_t3771707131;
// Windows.Foundation.Collections.IIterator`1<System.Runtime.InteropServices.GCHandle>
struct IIterator_1_t1225581660;
// Windows.Foundation.Collections.IIterator`1<System.Reflection.ParameterModifier>
struct IIterator_1_t3630805235;
// Windows.Foundation.Collections.IIterator`1<System.Reflection.Emit.MonoWin32Resource>
struct IIterator_1_t4073340252;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IIterator_1_t275200381;
// Windows.Foundation.Collections.IIterator`1<System.Reflection.Emit.SequencePoint>
struct IIterator_1_t4234376625;
// Windows.Foundation.Collections.IIterator`1<System.Reflection.CustomAttributeTypedArgument>
struct IIterator_1_t597293630;
// Windows.Foundation.Collections.IIterator`1<System.IntPtr>
struct IIterator_1_t3009260950;
// Windows.Foundation.Collections.IIterator`1<System.Decimal>
struct IIterator_1_t822402853;
// Windows.Foundation.Collections.IIterator`1<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant>
struct IIterator_1_t2883131666;
// Windows.Foundation.Collections.IIterator`1<System.Guid>
struct IIterator_1_t1067676360;
// Windows.Foundation.Collections.IIterator`1<System.Globalization.InternalEncodingDataItem>
struct IIterator_1_t1033003290;
// Windows.Foundation.Collections.IIterator`1<System.Globalization.InternalCodePageDataItem>
struct IIterator_1_t449676406;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Hashtable/bucket>
struct IIterator_1_t2927242473;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Marker>
struct IIterator_1_t768921250;
// Windows.Foundation.Collections.IIterator`1<System.ParameterizedStrings/FormatParam>
struct IIterator_1_t2068617555;
// Windows.Foundation.Collections.IIterator`1<System.ComponentModel.AttributeCollection/AttributeEntry>
struct IIterator_1_t3170121632;
// Windows.Foundation.Collections.IIterator`1<System.Linq.Set`1/Slot<System.Object>>
struct IIterator_1_t509198289;
// Windows.Foundation.Collections.IIterator`1<System.DateTime>
struct IIterator_1_t1612673258;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.HashSet`1/Slot<System.Object>>
struct IIterator_1_t1920240230;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.HashSet`1/Slot<System.Int32>>
struct IIterator_1_t1791079819;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.HashSet`1/Slot<System.UInt32>>
struct IIterator_1_t1400196044;
// Windows.Foundation.Collections.IIterator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>
struct IIterator_1_t784461048;
// Windows.Foundation.Collections.IIterator`1<System.Linq.Set`1/Slot<System.Char>>
struct IIterator_1_t1063552595;
// Windows.Foundation.Collections.IIterator`1<System.Xml.Linq.XHashtable`1/XHashtableState/Entry<System.Object>>
struct IIterator_1_t612644763;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.UInt32,System.Object>>
struct IIterator_1_t3843381787;
// Windows.Foundation.Collections.IIterator`1<System.Xml.Schema.FacetsChecker/FacetsCompiler/Map>
struct IIterator_1_t3500155196;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IIterator_1_t2240635135;
// Windows.Foundation.Collections.IIterator`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey>
struct IIterator_1_t1007764195;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct IIterator_1_t119594292;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct IIterator_1_t1716509889;
// Windows.Foundation.Collections.IIterator`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>
struct IIterator_1_t2325555968;
// Windows.Foundation.Collections.IIterator`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct IIterator_1_t2488001557;
// Windows.Foundation.Collections.IIterator`1<System.DateTimeOffset>
struct IIterator_1_t1103430980;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.HashSet`1/Slot<System.Decimal>>
struct IIterator_1_t1788393446;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<UnityEngine.Vector3,System.Object>>
struct IIterator_1_t2791331845;
// Windows.Foundation.Collections.IIterator`1<System.Globalization.HebrewNumber/HS>
struct IIterator_1_t1213916489;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>>
struct IIterator_1_t2424275607;
// Windows.Foundation.Collections.IIterator`1<System.DateTimeParse/DS>
struct IIterator_1_t106413843;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>
struct IIterator_1_t1142044873;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>>
struct IIterator_1_t2128085949;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>>
struct IIterator_1_t3466304448;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>
struct IIterator_1_t2922739124;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>>
struct IIterator_1_t3217244461;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>
struct IIterator_1_t2400939337;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>
struct IIterator_1_t3574139524;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>
struct IIterator_1_t1048225435;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>
struct IIterator_1_t1573787523;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>>
struct IIterator_1_t2295115196;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>
struct IIterator_1_t2813822927;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>
struct IIterator_1_t746748672;
// Windows.Foundation.Collections.IIterator`1<System.Threading.CancellationTokenRegistration>
struct IIterator_1_t687568377;
// Windows.Foundation.Collections.IIterator`1<System.TimeSpan>
struct IIterator_1_t3050270018;
// Windows.Foundation.Collections.IIterator`1<System.Reflection.Emit.ILExceptionInfo>
struct IIterator_1_t2406966779;
// Windows.Foundation.Collections.IIterator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>
struct IIterator_1_t1967192450;
// Windows.Foundation.Collections.IIterator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>
struct IIterator_1_t1359579927;
// Windows.Foundation.Collections.IIterator`1<System.TermInfoStrings>
struct IIterator_1_t2459390724;
// Windows.Foundation.Collections.IIterator`1<System.Text.RegularExpressions.RegexOptions>
struct IIterator_1_t2261956364;
// Windows.Foundation.Collections.IIterator`1<System.TypeCode>
struct IIterator_1_t861367560;
// Windows.Foundation.Collections.IIterator`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>
struct IIterator_1_t1162649969;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>>
struct IIterator_1_t2683496769;
// System.IO.Stream
struct Stream_t1273022909;
// Windows.Foundation.Collections.IIterator`1<System.Linq.Expressions.Compiler.VariableStorageKind>
struct IIterator_1_t1154433062;
// Windows.Foundation.Collections.IIterator`1<System.Net.Sockets.Socket/WSABUF>
struct IIterator_1_t4167170159;
// Windows.Foundation.Collections.IIterator`1<System.Reflection.CustomAttributeNamedArgument>
struct IIterator_1_t2456976479;
// Windows.Foundation.Collections.IIterator`1<System.Net.WebHeaderCollection/RfcChar>
struct IIterator_1_t779553403;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.Dictionary`2/Entry<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>>
struct IIterator_1_t1390423637;
// Windows.Foundation.Collections.IIterator`1<System.TimeZoneInfo/TZifType>
struct IIterator_1_t4280888128;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>>
struct IIterator_1_t2173231316;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>>
struct IIterator_1_t612761124;
// Windows.Foundation.Collections.IIterator`1<System.Reflection.Emit.MonoResource>
struct IIterator_1_t1977573482;
// Windows.Foundation.Collections.IIterator`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>
struct IIterator_1_t1337669801;
// Windows.Foundation.Collections.IIterator`1<System.IO.kevent>
struct IIterator_1_t280800433;
// Windows.Foundation.Collections.IIterator`1<System.Net.CookieTokenizer/RecognizedAttribute>
struct IIterator_1_t2801184989;
// Windows.Foundation.Collections.IIterator`1<System.Net.HeaderVariantInfo>
struct IIterator_1_t4104796370;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>>
struct IIterator_1_t604544217;
// Windows.Foundation.Collections.IIterator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>
struct IIterator_1_t2302713483;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>
struct IIterator_1_t2418171828;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>
struct IIterator_1_t1960008504;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>>
struct IIterator_1_t129506095;
// Windows.Foundation.Collections.IIterator`1<System.Reflection.Emit.RefEmitPermissionSet>
struct IIterator_1_t2653501756;
// Windows.Foundation.Collections.IIterator`1<System.Globalization.TimeSpanParse/TimeSpanToken>
struct IIterator_1_t3162458143;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>>
struct IIterator_1_t415756058;

struct IKeyValuePair_2_t1096243984;
struct IIterator_1_t1610710777;
struct IIterator_1_t2240635135;
struct IIterator_1_t119594292;
struct IKeyValuePair_2_t3270170437;
struct IIterator_1_t2400939337;
struct IIterator_1_t825089226;
struct IIterator_1_t3566377543;
struct IKeyValuePair_2_t1256548186;
struct IIterator_1_t404360792;
struct IIterator_1_t1716509889;
struct IKeyValuePair_2_t3425776526;
struct IKeyValuePair_2_t3554936937;
struct IIterator_1_t275200381;
struct IKeyValuePair_2_t572118738;
struct IIterator_1_t331222170;
struct IIterator_1_t2488001557;
struct IKeyValuePair_2_t3481798315;
struct IIterator_1_t954249637;
struct IIterator_1_t434205451;
struct IIterator_1_t51868431;
struct IIterator_1_t2763776132;
struct IIterator_1_t1067676360;
struct IIterator_1_t2008183565;
struct IIterator_1_t3050270018;
struct IIterator_1_t426963860;
struct IIterator_1_t1103430980;
struct XPathNode_t2208072876_marshaled_pinvoke;
struct XPathNode_t2208072876_marshaled_com;
struct ILExceptionBlock_t3961874966_marshaled_pinvoke;
struct ILExceptionBlock_t3961874966_marshaled_com;
struct Guid_t ;
struct DateTime_t1679451545 ;
struct EventRegistrationToken_t318890788 ;
struct TimeSpan_t881159249 ;



// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct NOVTABLE IIterator_1_t2240635135 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2947568524(IKeyValuePair_2_t1096243984** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3859218363(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2670346608(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3900158597(uint32_t ___items0ArraySize, IKeyValuePair_2_t1096243984** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Int64>
struct NOVTABLE IIterable_1_t1357964764 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3916705692(IIterator_1_t1610710777** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct NOVTABLE IIterable_1_t1987889122 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m513098253(IIterator_1_t2240635135** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct NOVTABLE IIterable_1_t4161815575 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1699614814(IIterator_1_t119594292** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct NOVTABLE IIterator_1_t119594292 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3547052056(IKeyValuePair_2_t3270170437** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m344842528(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1308250629(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3058286030(uint32_t ___items0ArraySize, IKeyValuePair_2_t3270170437** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>
struct NOVTABLE IIterable_1_t2148193324 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m597753835(IIterator_1_t2400939337** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Int32>
struct NOVTABLE IIterable_1_t572343213 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1280577591(IIterator_1_t825089226** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Int32>
struct NOVTABLE IIterator_1_t825089226 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3230580114(int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3822991096(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1179569845(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2626728204(uint32_t ___items0ArraySize, int32_t* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Single>
struct NOVTABLE IIterable_1_t3313631530 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m4078210676(IIterator_1_t3566377543** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>
struct NOVTABLE IIterator_1_t2400939337 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m74209353(IKeyValuePair_2_t1256548186** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2249475151(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2303149837(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3197996383(uint32_t ___items0ArraySize, IKeyValuePair_2_t1256548186** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Single>
struct NOVTABLE IIterator_1_t3566377543 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1146799328(float* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2822908534(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2523572727(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3822961495(uint32_t ___items0ArraySize, float* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct NOVTABLE IIterable_1_t151614779 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m4041697847(IIterator_1_t404360792** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct NOVTABLE IIterable_1_t1463763876 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m202819341(IIterator_1_t1716509889** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct NOVTABLE IIterator_1_t275200381 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3813727452(IKeyValuePair_2_t3425776526** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m538388502(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2855647886(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3959935491(uint32_t ___items0ArraySize, IKeyValuePair_2_t3425776526** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct NOVTABLE IIterator_1_t404360792 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3426234107(IKeyValuePair_2_t3554936937** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2174897718(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m106249972(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3542193621(uint32_t ___items0ArraySize, IKeyValuePair_2_t3554936937** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct NOVTABLE IIterable_1_t22454368 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m2644326382(IIterator_1_t275200381** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct NOVTABLE IIterator_1_t1716509889 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3308861090(IKeyValuePair_2_t572118738** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m583209192(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3227612391(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3064915941(uint32_t ___items0ArraySize, IKeyValuePair_2_t572118738** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>
struct NOVTABLE IIterable_1_t78476157 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m817385671(IIterator_1_t331222170** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct NOVTABLE IIterable_1_t2235255544 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m4102057096(IIterator_1_t2488001557** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>
struct NOVTABLE IIterator_1_t331222170 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3355334578(IKeyValuePair_2_t3481798315** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m940628351(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1278101282(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m253528417(uint32_t ___items0ArraySize, IKeyValuePair_2_t3481798315** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Object>
struct NOVTABLE IIterable_1_t701503624 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3341821894(IIterator_1_t954249637** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Object>
struct NOVTABLE IIterator_1_t954249637 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m4033148858(Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2403098075(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3126031461(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1443615899(uint32_t ___items0ArraySize, Il2CppIInspectable** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.UInt32>
struct NOVTABLE IIterable_1_t181459438 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3240740629(IIterator_1_t434205451** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.UInt16>
struct NOVTABLE IIterable_1_t4094089714 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1090735028(IIterator_1_t51868431** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.UInt16>
struct NOVTABLE IIterator_1_t51868431 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2601848623(uint16_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1464880407(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3625227941(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m965407816(uint32_t ___items0ArraySize, uint16_t* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Double>
struct NOVTABLE IIterable_1_t2511030119 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m2037907598(IIterator_1_t2763776132** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Guid>
struct NOVTABLE IIterable_1_t814930347 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m401588167(IIterator_1_t1067676360** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.UInt32>
struct NOVTABLE IIterator_1_t434205451 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3564117589(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1240475462(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3827789177(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1580073288(uint32_t ___items0ArraySize, uint32_t* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.UInt64>
struct NOVTABLE IIterable_1_t1755437552 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m2773234478(IIterator_1_t2008183565** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.UInt64>
struct NOVTABLE IIterator_1_t2008183565 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m4214781887(uint64_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1386923519(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m282066159(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m188438814(uint32_t ___items0ArraySize, uint64_t* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Int16>
struct NOVTABLE IIterator_1_t426963860 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3967028616(int16_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3077644356(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3913675470(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2584956007(uint32_t ___items0ArraySize, int16_t* ___items0, uint32_t* comReturnValue) = 0;
};
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
// Windows.Foundation.Collections.IIterable`1<System.TimeSpan>
struct NOVTABLE IIterable_1_t2797524005 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1083239098(IIterator_1_t3050270018** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Double>
struct NOVTABLE IIterator_1_t2763776132 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m354509700(double* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3297171499(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3160156876(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2319078079(uint32_t ___items0ArraySize, double* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Int64>
struct NOVTABLE IIterator_1_t1610710777 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m384601225(int64_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3484924956(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m997059212(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m779360946(uint32_t ___items0ArraySize, int64_t* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Int16>
struct NOVTABLE IIterable_1_t174217847 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m502408437(IIterator_1_t426963860** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.DateTimeOffset>
struct NOVTABLE IIterable_1_t850684967 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m2198371330(IIterator_1_t1103430980** comReturnValue) = 0;
};
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1019346_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1019346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Net.Sockets.Socket/WSABUF>
struct  IIterableToIEnumerableAdapter_1_t1019346  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1019346_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1466486284_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1466486284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>
struct  IIterableToIEnumerableAdapter_1_t1466486284  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1466486284_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T908369886_H
#define IITERABLETOIENUMERABLEADAPTER_1_T908369886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Net.WebHeaderCollection/RfcChar>
struct  IIterableToIEnumerableAdapter_1_t908369886  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T908369886_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T4233612853_H
#define IITERABLETOIENUMERABLEADAPTER_1_T4233612853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Net.HeaderVariantInfo>
struct  IIterableToIEnumerableAdapter_1_t4233612853  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T4233612853_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1083066120_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1083066120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Object>
struct  IIterableToIEnumerableAdapter_1_t1083066120  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1083066120_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1161819773_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1161819773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Globalization.InternalEncodingDataItem>
struct  IIterableToIEnumerableAdapter_1_t1161819773  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1161819773_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T578492889_H
#define IITERABLETOIENUMERABLEADAPTER_1_T578492889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Globalization.InternalCodePageDataItem>
struct  IIterableToIEnumerableAdapter_1_t578492889  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T578492889_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2197434038_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2197434038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.ParameterizedStrings/FormatParam>
struct  IIterableToIEnumerableAdapter_1_t2197434038  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2197434038_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T989169257_H
#define IITERATORTOIENUMERATORADAPTER_1_T989169257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Object>
struct  IIteratorToIEnumeratorAdapter_1_t989169257  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t989169257, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t989169257, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t989169257, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t989169257, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T989169257_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2585792962_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2585792962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.CustomAttributeNamedArgument>
struct  IIterableToIEnumerableAdapter_1_t2585792962  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2585792962_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1645630397_H
#define IITERATORTOIENUMERATORADAPTER_1_T1645630397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Int64>
struct  IIteratorToIEnumeratorAdapter_1_t1645630397  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int64_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1645630397, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1645630397, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1645630397, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1645630397, ___current_3)); }
	inline int64_t get_current_3() const { return ___current_3; }
	inline int64_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int64_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1645630397_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T409616916_H
#define IITERABLETOIENUMERABLEADAPTER_1_T409616916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.IO.kevent>
struct  IIterableToIEnumerableAdapter_1_t409616916  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T409616916_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3138077433_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3138077433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.IntPtr>
struct  IIterableToIEnumerableAdapter_1_t3138077433  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3138077433_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T461883480_H
#define IITERATORTOIENUMERATORADAPTER_1_T461883480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Int16>
struct  IIteratorToIEnumeratorAdapter_1_t461883480  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int16_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t461883480, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t461883480, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t461883480, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t461883480, ___current_3)); }
	inline int16_t get_current_3() const { return ___current_3; }
	inline int16_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int16_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T461883480_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T953905709_H
#define IITERABLETOIENUMERABLEADAPTER_1_T953905709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Int32>
struct  IIterableToIEnumerableAdapter_1_t953905709  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T953905709_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1739527260_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1739527260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Int64>
struct  IIterableToIEnumerableAdapter_1_t1739527260  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1739527260_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T860008846_H
#define IITERATORTOIENUMERATORADAPTER_1_T860008846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Int32>
struct  IIteratorToIEnumeratorAdapter_1_t860008846  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t860008846, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t860008846, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t860008846, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t860008846, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T860008846_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1196492843_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1196492843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Guid>
struct  IIterableToIEnumerableAdapter_1_t1196492843  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1196492843_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T638014772_H
#define IITERABLETOIENUMERABLEADAPTER_1_T638014772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Linq.Set`1/Slot<System.Object>>
struct  IIterableToIEnumerableAdapter_1_t638014772  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T638014772_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2930001472_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2930001472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Net.CookieTokenizer/RecognizedAttribute>
struct  IIterableToIEnumerableAdapter_1_t2930001472  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2930001472_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3291274626_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3291274626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Globalization.TimeSpanParse/TimeSpanToken>
struct  IIterableToIEnumerableAdapter_1_t3291274626  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3291274626_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3011948149_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3011948149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant>
struct  IIterableToIEnumerableAdapter_1_t3011948149  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3011948149_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T555780343_H
#define IITERABLETOIENUMERABLEADAPTER_1_T555780343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Int16>
struct  IIterableToIEnumerableAdapter_1_t555780343  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T555780343_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1192369078_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1192369078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Linq.Set`1/Slot<System.Char>>
struct  IIterableToIEnumerableAdapter_1_t1192369078  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1192369078_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1283249545_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1283249545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Linq.Expressions.Compiler.VariableStorageKind>
struct  IIterableToIEnumerableAdapter_1_t1283249545  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1283249545_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T726110113_H
#define IITERABLETOIENUMERABLEADAPTER_1_T726110113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.CustomAttributeTypedArgument>
struct  IIterableToIEnumerableAdapter_1_t726110113  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T726110113_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2390772847_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2390772847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Text.RegularExpressions.RegexOptions>
struct  IIterableToIEnumerableAdapter_1_t2390772847  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2390772847_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T913277531_H
#define IITERABLETOIENUMERABLEADAPTER_1_T913277531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>
struct  IIterableToIEnumerableAdapter_1_t913277531  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T913277531_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T816384860_H
#define IITERABLETOIENUMERABLEADAPTER_1_T816384860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Threading.CancellationTokenRegistration>
struct  IIterableToIEnumerableAdapter_1_t816384860  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T816384860_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T114737315_H
#define IITERABLETOIENUMERABLEADAPTER_1_T114737315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.TimeZoneInfo/TZifType>
struct  IIterableToIEnumerableAdapter_1_t114737315  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T114737315_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3179086501_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3179086501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.TimeSpan>
struct  IIterableToIEnumerableAdapter_1_t3179086501  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3179086501_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2431529966_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2431529966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>
struct  IIterableToIEnumerableAdapter_1_t2431529966  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2431529966_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3873608051_H
#define IITERATORTOIENUMERATORADAPTER_1_T3873608051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.SByte>
struct  IIteratorToIEnumeratorAdapter_1_t3873608051  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int8_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3873608051, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3873608051, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3873608051, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3873608051, ___current_3)); }
	inline int8_t get_current_3() const { return ___current_3; }
	inline int8_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int8_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3873608051_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3695194026_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3695194026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Single>
struct  IIterableToIEnumerableAdapter_1_t3695194026  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3695194026_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2588207207_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2588207207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.TermInfoStrings>
struct  IIterableToIEnumerableAdapter_1_t2588207207  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2588207207_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3601297163_H
#define IITERATORTOIENUMERATORADAPTER_1_T3601297163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Single>
struct  IIteratorToIEnumeratorAdapter_1_t3601297163  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	float ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3601297163, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3601297163, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3601297163, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3601297163, ___current_3)); }
	inline float get_current_3() const { return ___current_3; }
	inline float* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(float value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3601297163_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2043103185_H
#define IITERATORTOIENUMERATORADAPTER_1_T2043103185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.UInt64>
struct  IIteratorToIEnumeratorAdapter_1_t2043103185  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	uint64_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2043103185, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2043103185, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2043103185, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2043103185, ___current_3)); }
	inline uint64_t get_current_3() const { return ___current_3; }
	inline uint64_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(uint64_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2043103185_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2137000048_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2137000048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.UInt64>
struct  IIterableToIEnumerableAdapter_1_t2137000048  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2137000048_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T741461246_H
#define IITERABLETOIENUMERABLEADAPTER_1_T741461246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Xml.Linq.XHashtable`1/XHashtableState/Entry<System.Object>>
struct  IIterableToIEnumerableAdapter_1_t741461246  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T741461246_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3628971679_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3628971679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Xml.Schema.FacetsChecker/FacetsCompiler/Map>
struct  IIterableToIEnumerableAdapter_1_t3628971679  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3628971679_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T180684914_H
#define IITERABLETOIENUMERABLEADAPTER_1_T180684914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.UInt16>
struct  IIterableToIEnumerableAdapter_1_t180684914  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T180684914_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T990184043_H
#define IITERABLETOIENUMERABLEADAPTER_1_T990184043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.TypeCode>
struct  IIterableToIEnumerableAdapter_1_t990184043  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T990184043_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T86788051_H
#define IITERATORTOIENUMERATORADAPTER_1_T86788051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.UInt16>
struct  IIteratorToIEnumeratorAdapter_1_t86788051  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	uint16_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t86788051, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t86788051, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t86788051, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t86788051, ___current_3)); }
	inline uint16_t get_current_3() const { return ___current_3; }
	inline uint16_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(uint16_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T86788051_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T469125071_H
#define IITERATORTOIENUMERATORADAPTER_1_T469125071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.UInt32>
struct  IIteratorToIEnumeratorAdapter_1_t469125071  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	uint32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t469125071, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t469125071, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t469125071, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t469125071, ___current_3)); }
	inline uint32_t get_current_3() const { return ___current_3; }
	inline uint32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(uint32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T469125071_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T563021934_H
#define IITERABLETOIENUMERABLEADAPTER_1_T563021934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.UInt32>
struct  IIterableToIEnumerableAdapter_1_t563021934  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T563021934_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3967504914_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3967504914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.SByte>
struct  IIterableToIEnumerableAdapter_1_t3967504914  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3967504914_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2106389965_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2106389965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.MonoResource>
struct  IIterableToIEnumerableAdapter_1_t2106389965  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2106389965_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T284621599_H
#define IITERABLETOIENUMERABLEADAPTER_1_T284621599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.Label>
struct  IIterableToIEnumerableAdapter_1_t284621599  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T284621599_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T4202156735_H
#define IITERABLETOIENUMERABLEADAPTER_1_T4202156735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.MonoWin32Resource>
struct  IIterableToIEnumerableAdapter_1_t4202156735  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T4202156735_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T68225812_H
#define IITERABLETOIENUMERABLEADAPTER_1_T68225812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.SequencePoint>
struct  IIterableToIEnumerableAdapter_1_t68225812  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T68225812_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2782318239_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2782318239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.RefEmitPermissionSet>
struct  IIterableToIEnumerableAdapter_1_t2782318239  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2782318239_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2535783262_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2535783262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.ILExceptionInfo>
struct  IIterableToIEnumerableAdapter_1_t2535783262  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2535783262_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1964834922_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1964834922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.ILExceptionBlock>
struct  IIterableToIEnumerableAdapter_1_t1964834922  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1964834922_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2658094643_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2658094643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.ILGenerator/LabelData>
struct  IIterableToIEnumerableAdapter_1_t2658094643  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2658094643_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T328735070_H
#define IITERABLETOIENUMERABLEADAPTER_1_T328735070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.ILTokenInfo>
struct  IIterableToIEnumerableAdapter_1_t328735070  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T328735070_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3156429306_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3156429306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.ILGenerator/LabelFixup>
struct  IIterableToIEnumerableAdapter_1_t3156429306  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3156429306_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2454372451_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2454372451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>
struct  IIterableToIEnumerableAdapter_1_t2454372451  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2454372451_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1291466452_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1291466452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>
struct  IIterableToIEnumerableAdapter_1_t1291466452  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1291466452_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1136580678_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1136580678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey>
struct  IIterableToIEnumerableAdapter_1_t1136580678  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1136580678_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2096008933_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2096008933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>
struct  IIterableToIEnumerableAdapter_1_t2096008933  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2096008933_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1488396410_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1488396410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>
struct  IIterableToIEnumerableAdapter_1_t1488396410  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1488396410_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1726930763_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1726930763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Resources.ResourceLocator>
struct  IIterableToIEnumerableAdapter_1_t1726930763  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1726930763_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3759621718_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3759621718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.ParameterModifier>
struct  IIterableToIEnumerableAdapter_1_t3759621718  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3759621718_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3900523614_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3900523614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.CompilerServices.Ephemeron>
struct  IIterableToIEnumerableAdapter_1_t3900523614  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3900523614_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2616818040_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2616818040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct  IIterableToIEnumerableAdapter_1_t2616818040  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2616818040_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1354398143_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1354398143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.InteropServices.GCHandle>
struct  IIterableToIEnumerableAdapter_1_t1354398143  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1354398143_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3702956007_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3702956007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t3702956007  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3702956007_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T875565155_H
#define IITERABLETOIENUMERABLEADAPTER_1_T875565155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>
struct  IIterableToIEnumerableAdapter_1_t875565155  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T875565155_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1177041918_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1177041918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>
struct  IIterableToIEnumerableAdapter_1_t1177041918  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1177041918_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T248410775_H
#define IITERABLETOIENUMERABLEADAPTER_1_T248410775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t248410775  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T248410775_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2942639410_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2942639410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t2942639410  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2942639410_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T404016864_H
#define IITERABLETOIENUMERABLEADAPTER_1_T404016864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct  IIterableToIEnumerableAdapter_1_t404016864  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T404016864_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2088824987_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2088824987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>
struct  IIterableToIEnumerableAdapter_1_t2088824987  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2088824987_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T897737733_H
#define IITERABLETOIENUMERABLEADAPTER_1_T897737733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Marker>
struct  IIterableToIEnumerableAdapter_1_t897737733  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T897737733_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2553092090_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2553092090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t2553092090  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2553092090_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T544572541_H
#define IITERABLETOIENUMERABLEADAPTER_1_T544572541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t544572541  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T544572541_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2302047799_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2302047799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t2302047799  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2302047799_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3346060944_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3346060944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>>
struct  IIterableToIEnumerableAdapter_1_t3346060944  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3346060944_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2529755820_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2529755820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t2529755820  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2529755820_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2256902432_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2256902432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>>
struct  IIterableToIEnumerableAdapter_1_t2256902432  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2256902432_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T741577607_H
#define IITERABLETOIENUMERABLEADAPTER_1_T741577607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>>
struct  IIterableToIEnumerableAdapter_1_t741577607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T741577607_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2369451618_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2369451618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t2369451618  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2369451618_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3051555607_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3051555607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t3051555607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3051555607_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2423931679_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2423931679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>>
struct  IIterableToIEnumerableAdapter_1_t2423931679  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2423931679_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T460038653_H
#define IITERABLETOIENUMERABLEADAPTER_1_T460038653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t460038653  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T460038653_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1270861356_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1270861356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t1270861356  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1270861356_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3595120931_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3595120931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>>
struct  IIterableToIEnumerableAdapter_1_t3595120931  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3595120931_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T733360700_H
#define IITERABLETOIENUMERABLEADAPTER_1_T733360700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>>
struct  IIterableToIEnumerableAdapter_1_t733360700  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T733360700_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1529012527_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1529012527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.UInt32>>
struct  IIterableToIEnumerableAdapter_1_t1529012527  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1529012527_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3972198270_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3972198270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.UInt32,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t3972198270  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3972198270_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2812313252_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2812313252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>>
struct  IIterableToIEnumerableAdapter_1_t2812313252  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2812313252_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T951219336_H
#define IITERABLETOIENUMERABLEADAPTER_1_T951219336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Decimal>
struct  IIterableToIEnumerableAdapter_1_t951219336  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T951219336_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1702604006_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1702604006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t1702604006  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1702604006_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2920148328_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2920148328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<UnityEngine.Vector3,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t2920148328  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2920148328_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2892592615_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2892592615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Double>
struct  IIterableToIEnumerableAdapter_1_t2892592615  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2892592615_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1342732972_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1342732972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Globalization.HebrewNumber/HS>
struct  IIterableToIEnumerableAdapter_1_t1342732972  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1342732972_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T533177275_H
#define IITERABLETOIENUMERABLEADAPTER_1_T533177275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t533177275  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T533177275_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T258322578_H
#define IITERABLETOIENUMERABLEADAPTER_1_T258322578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>>
struct  IIterableToIEnumerableAdapter_1_t258322578  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T258322578_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2546988311_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2546988311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>
struct  IIterableToIEnumerableAdapter_1_t2546988311  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2546988311_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2798695752_H
#define IITERATORTOIENUMERATORADAPTER_1_T2798695752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Double>
struct  IIteratorToIEnumeratorAdapter_1_t2798695752  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	double ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2798695752, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2798695752, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2798695752, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2798695752, ___current_3)); }
	inline double get_current_3() const { return ___current_3; }
	inline double* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(double value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2798695752_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1919896302_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1919896302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.Int32>>
struct  IIterableToIEnumerableAdapter_1_t1919896302  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1919896302_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1741489741_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1741489741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.DateTime>
struct  IIterableToIEnumerableAdapter_1_t1741489741  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1741489741_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3298938115_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3298938115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.ComponentModel.AttributeCollection/AttributeEntry>
struct  IIterableToIEnumerableAdapter_1_t3298938115  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3298938115_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T3056058956_H
#define IITERABLETOIENUMERABLEADAPTER_1_T3056058956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Hashtable/bucket>
struct  IIterableToIEnumerableAdapter_1_t3056058956  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T3056058956_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T2049056713_H
#define IITERABLETOIENUMERABLEADAPTER_1_T2049056713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.Object>>
struct  IIterableToIEnumerableAdapter_1_t2049056713  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T2049056713_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1917209929_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1917209929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.Decimal>>
struct  IIterableToIEnumerableAdapter_1_t1917209929  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1917209929_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T235230326_H
#define IITERABLETOIENUMERABLEADAPTER_1_T235230326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.DateTimeParse/DS>
struct  IIterableToIEnumerableAdapter_1_t235230326  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T235230326_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1519240120_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1519240120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>>
struct  IIterableToIEnumerableAdapter_1_t1519240120  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1519240120_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1845326372_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1845326372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct  IIterableToIEnumerableAdapter_1_t1845326372  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1845326372_H
#ifndef IITERABLETOIENUMERABLEADAPTER_1_T1232247463_H
#define IITERABLETOIENUMERABLEADAPTER_1_T1232247463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.DateTimeOffset>
struct  IIterableToIEnumerableAdapter_1_t1232247463  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERABLETOIENUMERABLEADAPTER_1_T1232247463_H
#ifndef INTERNALCODEPAGEDATAITEM_T2575532933_H
#define INTERNALCODEPAGEDATAITEM_T2575532933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.InternalCodePageDataItem
struct  InternalCodePageDataItem_t2575532933 
{
public:
	// System.UInt16 System.Globalization.InternalCodePageDataItem::codePage
	uint16_t ___codePage_0;
	// System.UInt16 System.Globalization.InternalCodePageDataItem::uiFamilyCodePage
	uint16_t ___uiFamilyCodePage_1;
	// System.UInt32 System.Globalization.InternalCodePageDataItem::flags
	uint32_t ___flags_2;
	// System.String System.Globalization.InternalCodePageDataItem::Names
	String_t* ___Names_3;

public:
	inline static int32_t get_offset_of_codePage_0() { return static_cast<int32_t>(offsetof(InternalCodePageDataItem_t2575532933, ___codePage_0)); }
	inline uint16_t get_codePage_0() const { return ___codePage_0; }
	inline uint16_t* get_address_of_codePage_0() { return &___codePage_0; }
	inline void set_codePage_0(uint16_t value)
	{
		___codePage_0 = value;
	}

	inline static int32_t get_offset_of_uiFamilyCodePage_1() { return static_cast<int32_t>(offsetof(InternalCodePageDataItem_t2575532933, ___uiFamilyCodePage_1)); }
	inline uint16_t get_uiFamilyCodePage_1() const { return ___uiFamilyCodePage_1; }
	inline uint16_t* get_address_of_uiFamilyCodePage_1() { return &___uiFamilyCodePage_1; }
	inline void set_uiFamilyCodePage_1(uint16_t value)
	{
		___uiFamilyCodePage_1 = value;
	}

	inline static int32_t get_offset_of_flags_2() { return static_cast<int32_t>(offsetof(InternalCodePageDataItem_t2575532933, ___flags_2)); }
	inline uint32_t get_flags_2() const { return ___flags_2; }
	inline uint32_t* get_address_of_flags_2() { return &___flags_2; }
	inline void set_flags_2(uint32_t value)
	{
		___flags_2 = value;
	}

	inline static int32_t get_offset_of_Names_3() { return static_cast<int32_t>(offsetof(InternalCodePageDataItem_t2575532933, ___Names_3)); }
	inline String_t* get_Names_3() const { return ___Names_3; }
	inline String_t** get_address_of_Names_3() { return &___Names_3; }
	inline void set_Names_3(String_t* value)
	{
		___Names_3 = value;
		Il2CppCodeGenWriteBarrier((&___Names_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Globalization.InternalCodePageDataItem
struct InternalCodePageDataItem_t2575532933_marshaled_pinvoke
{
	uint16_t ___codePage_0;
	uint16_t ___uiFamilyCodePage_1;
	uint32_t ___flags_2;
	char* ___Names_3;
};
// Native definition for COM marshalling of System.Globalization.InternalCodePageDataItem
struct InternalCodePageDataItem_t2575532933_marshaled_com
{
	uint16_t ___codePage_0;
	uint16_t ___uiFamilyCodePage_1;
	uint32_t ___flags_2;
	Il2CppChar* ___Names_3;
};
#endif // INTERNALCODEPAGEDATAITEM_T2575532933_H
#ifndef UINTPTR_T_H
#define UINTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UIntPtr
struct  UIntPtr_t 
{
public:
	// System.Void* System.UIntPtr::_pointer
	void* ____pointer_1;

public:
	inline static int32_t get_offset_of__pointer_1() { return static_cast<int32_t>(offsetof(UIntPtr_t, ____pointer_1)); }
	inline void* get__pointer_1() const { return ____pointer_1; }
	inline void** get_address_of__pointer_1() { return &____pointer_1; }
	inline void set__pointer_1(void* value)
	{
		____pointer_1 = value;
	}
};

struct UIntPtr_t_StaticFields
{
public:
	// System.UIntPtr System.UIntPtr::Zero
	uintptr_t ___Zero_0;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(UIntPtr_t_StaticFields, ___Zero_0)); }
	inline uintptr_t get_Zero_0() const { return ___Zero_0; }
	inline uintptr_t* get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(uintptr_t value)
	{
		___Zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINTPTR_T_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef DECIMAL_T2948259380_H
#define DECIMAL_T2948259380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t2948259380 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t2948259380_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t2770800703* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t2948259380  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t2948259380  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t2948259380  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t2948259380  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t2948259380  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t2948259380  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t2948259380  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t2770800703* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t2770800703** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t2770800703* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier((&___Powers10_6), value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___Zero_7)); }
	inline Decimal_t2948259380  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t2948259380 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t2948259380  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___One_8)); }
	inline Decimal_t2948259380  get_One_8() const { return ___One_8; }
	inline Decimal_t2948259380 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t2948259380  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MinusOne_9)); }
	inline Decimal_t2948259380  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t2948259380 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t2948259380  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MaxValue_10)); }
	inline Decimal_t2948259380  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t2948259380 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t2948259380  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MinValue_11)); }
	inline Decimal_t2948259380  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t2948259380 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t2948259380  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t2948259380  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t2948259380 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t2948259380  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t2948259380  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t2948259380 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t2948259380  value)
	{
		___NearPositiveZero_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T2948259380_H
#ifndef SLOT_T3526052571_H
#define SLOT_T3526052571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1/Slot<System.UInt32>
struct  Slot_t3526052571 
{
public:
	// System.Int32 System.Collections.Generic.HashSet`1/Slot::hashCode
	int32_t ___hashCode_0;
	// T System.Collections.Generic.HashSet`1/Slot::value
	uint32_t ___value_1;
	// System.Int32 System.Collections.Generic.HashSet`1/Slot::next
	int32_t ___next_2;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Slot_t3526052571, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(Slot_t3526052571, ___value_1)); }
	inline uint32_t get_value_1() const { return ___value_1; }
	inline uint32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(uint32_t value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_next_2() { return static_cast<int32_t>(offsetof(Slot_t3526052571, ___next_2)); }
	inline int32_t get_next_2() const { return ___next_2; }
	inline int32_t* get_address_of_next_2() { return &___next_2; }
	inline void set_next_2(int32_t value)
	{
		___next_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOT_T3526052571_H
#ifndef ATTRIBUTEENTRY_T1001010863_H
#define ATTRIBUTEENTRY_T1001010863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AttributeCollection/AttributeEntry
struct  AttributeEntry_t1001010863 
{
public:
	// System.Type System.ComponentModel.AttributeCollection/AttributeEntry::type
	Type_t * ___type_0;
	// System.Int32 System.ComponentModel.AttributeCollection/AttributeEntry::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(AttributeEntry_t1001010863, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(AttributeEntry_t1001010863, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ComponentModel.AttributeCollection/AttributeEntry
struct AttributeEntry_t1001010863_marshaled_pinvoke
{
	Type_t * ___type_0;
	int32_t ___index_1;
};
// Native definition for COM marshalling of System.ComponentModel.AttributeCollection/AttributeEntry
struct AttributeEntry_t1001010863_marshaled_com
{
	Type_t * ___type_0;
	int32_t ___index_1;
};
#endif // ATTRIBUTEENTRY_T1001010863_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t385246372* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t385246372* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_31)); }
	inline DateTime_t3738529785  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t3738529785 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t3738529785  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_32)); }
	inline DateTime_t3738529785  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t3738529785  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef DATETIME_T1679451545_H
#define DATETIME_T1679451545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.DateTime
struct  DateTime_t1679451545 
{
public:
	// System.Int64 Windows.Foundation.DateTime::UniversalTime
	int64_t ___UniversalTime_0;

public:
	inline static int32_t get_offset_of_UniversalTime_0() { return static_cast<int32_t>(offsetof(DateTime_t1679451545, ___UniversalTime_0)); }
	inline int64_t get_UniversalTime_0() const { return ___UniversalTime_0; }
	inline int64_t* get_address_of_UniversalTime_0() { return &___UniversalTime_0; }
	inline void set_UniversalTime_0(int64_t value)
	{
		___UniversalTime_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T1679451545_H
#ifndef BUCKET_T758131704_H
#define BUCKET_T758131704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Hashtable/bucket
struct  bucket_t758131704 
{
public:
	// System.Object System.Collections.Hashtable/bucket::key
	RuntimeObject * ___key_0;
	// System.Object System.Collections.Hashtable/bucket::val
	RuntimeObject * ___val_1;
	// System.Int32 System.Collections.Hashtable/bucket::hash_coll
	int32_t ___hash_coll_2;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(bucket_t758131704, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_val_1() { return static_cast<int32_t>(offsetof(bucket_t758131704, ___val_1)); }
	inline RuntimeObject * get_val_1() const { return ___val_1; }
	inline RuntimeObject ** get_address_of_val_1() { return &___val_1; }
	inline void set_val_1(RuntimeObject * value)
	{
		___val_1 = value;
		Il2CppCodeGenWriteBarrier((&___val_1), value);
	}

	inline static int32_t get_offset_of_hash_coll_2() { return static_cast<int32_t>(offsetof(bucket_t758131704, ___hash_coll_2)); }
	inline int32_t get_hash_coll_2() const { return ___hash_coll_2; }
	inline int32_t* get_address_of_hash_coll_2() { return &___hash_coll_2; }
	inline void set_hash_coll_2(int32_t value)
	{
		___hash_coll_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Collections.Hashtable/bucket
struct bucket_t758131704_marshaled_pinvoke
{
	Il2CppIUnknown* ___key_0;
	Il2CppIUnknown* ___val_1;
	int32_t ___hash_coll_2;
};
// Native definition for COM marshalling of System.Collections.Hashtable/bucket
struct bucket_t758131704_marshaled_com
{
	Il2CppIUnknown* ___key_0;
	Il2CppIUnknown* ___val_1;
	int32_t ___hash_coll_2;
};
#endif // BUCKET_T758131704_H
#ifndef SPARSELYPOPULATEDARRAYADDINFO_1_T223515617_H
#define SPARSELYPOPULATEDARRAYADDINFO_1_T223515617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SparselyPopulatedArrayAddInfo`1<System.Threading.CancellationCallbackInfo>
struct  SparselyPopulatedArrayAddInfo_1_t223515617 
{
public:
	// System.Threading.SparselyPopulatedArrayFragment`1<T> System.Threading.SparselyPopulatedArrayAddInfo`1::m_source
	SparselyPopulatedArrayFragment_1_t4161250538 * ___m_source_0;
	// System.Int32 System.Threading.SparselyPopulatedArrayAddInfo`1::m_index
	int32_t ___m_index_1;

public:
	inline static int32_t get_offset_of_m_source_0() { return static_cast<int32_t>(offsetof(SparselyPopulatedArrayAddInfo_1_t223515617, ___m_source_0)); }
	inline SparselyPopulatedArrayFragment_1_t4161250538 * get_m_source_0() const { return ___m_source_0; }
	inline SparselyPopulatedArrayFragment_1_t4161250538 ** get_address_of_m_source_0() { return &___m_source_0; }
	inline void set_m_source_0(SparselyPopulatedArrayFragment_1_t4161250538 * value)
	{
		___m_source_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_source_0), value);
	}

	inline static int32_t get_offset_of_m_index_1() { return static_cast<int32_t>(offsetof(SparselyPopulatedArrayAddInfo_1_t223515617, ___m_index_1)); }
	inline int32_t get_m_index_1() const { return ___m_index_1; }
	inline int32_t* get_address_of_m_index_1() { return &___m_index_1; }
	inline void set_m_index_1(int32_t value)
	{
		___m_index_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPARSELYPOPULATEDARRAYADDINFO_1_T223515617_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef KEYVALUEPAIR_2_T71524366_H
#define KEYVALUEPAIR_2_T71524366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
struct  KeyValuePair_2_t71524366 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t71524366, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t71524366, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T71524366_H
#ifndef SLOT_T3916936346_H
#define SLOT_T3916936346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1/Slot<System.Int32>
struct  Slot_t3916936346 
{
public:
	// System.Int32 System.Collections.Generic.HashSet`1/Slot::hashCode
	int32_t ___hashCode_0;
	// T System.Collections.Generic.HashSet`1/Slot::value
	int32_t ___value_1;
	// System.Int32 System.Collections.Generic.HashSet`1/Slot::next
	int32_t ___next_2;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Slot_t3916936346, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(Slot_t3916936346, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_next_2() { return static_cast<int32_t>(offsetof(Slot_t3916936346, ___next_2)); }
	inline int32_t get_next_2() const { return ___next_2; }
	inline int32_t* get_address_of_next_2() { return &___next_2; }
	inline void set_next_2(int32_t value)
	{
		___next_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOT_T3916936346_H
#ifndef SLOT_T4046096757_H
#define SLOT_T4046096757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1/Slot<System.Object>
struct  Slot_t4046096757 
{
public:
	// System.Int32 System.Collections.Generic.HashSet`1/Slot::hashCode
	int32_t ___hashCode_0;
	// T System.Collections.Generic.HashSet`1/Slot::value
	RuntimeObject * ___value_1;
	// System.Int32 System.Collections.Generic.HashSet`1/Slot::next
	int32_t ___next_2;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Slot_t4046096757, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(Slot_t4046096757, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}

	inline static int32_t get_offset_of_next_2() { return static_cast<int32_t>(offsetof(Slot_t4046096757, ___next_2)); }
	inline int32_t get_next_2() const { return ___next_2; }
	inline int32_t* get_address_of_next_2() { return &___next_2; }
	inline void set_next_2(int32_t value)
	{
		___next_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOT_T4046096757_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ENTRY_T1674271018_H
#define ENTRY_T1674271018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.UInt32,System.Object>
struct  Entry_t1674271018 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	uint32_t ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	RuntimeObject * ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t1674271018, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t1674271018, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t1674271018, ___key_2)); }
	inline uint32_t get_key_2() const { return ___key_2; }
	inline uint32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(uint32_t value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t1674271018, ___value_3)); }
	inline RuntimeObject * get_value_3() const { return ___value_3; }
	inline RuntimeObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(RuntimeObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T1674271018_H
#ifndef ALIGNMENTUNION_T208902285_H
#define ALIGNMENTUNION_T208902285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkInformation.AlignmentUnion
struct  AlignmentUnion_t208902285 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt64 System.Net.NetworkInformation.AlignmentUnion::Alignment
			uint64_t ___Alignment_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint64_t ___Alignment_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 System.Net.NetworkInformation.AlignmentUnion::Length
			int32_t ___Length_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___Length_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___IfIndex_2_OffsetPadding[4];
			// System.Int32 System.Net.NetworkInformation.AlignmentUnion::IfIndex
			int32_t ___IfIndex_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___IfIndex_2_OffsetPadding_forAlignmentOnly[4];
			int32_t ___IfIndex_2_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_Alignment_0() { return static_cast<int32_t>(offsetof(AlignmentUnion_t208902285, ___Alignment_0)); }
	inline uint64_t get_Alignment_0() const { return ___Alignment_0; }
	inline uint64_t* get_address_of_Alignment_0() { return &___Alignment_0; }
	inline void set_Alignment_0(uint64_t value)
	{
		___Alignment_0 = value;
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(AlignmentUnion_t208902285, ___Length_1)); }
	inline int32_t get_Length_1() const { return ___Length_1; }
	inline int32_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(int32_t value)
	{
		___Length_1 = value;
	}

	inline static int32_t get_offset_of_IfIndex_2() { return static_cast<int32_t>(offsetof(AlignmentUnion_t208902285, ___IfIndex_2)); }
	inline int32_t get_IfIndex_2() const { return ___IfIndex_2; }
	inline int32_t* get_address_of_IfIndex_2() { return &___IfIndex_2; }
	inline void set_IfIndex_2(int32_t value)
	{
		___IfIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNMENTUNION_T208902285_H
#ifndef KEYVALUEPAIR_2_T2530217319_H
#define KEYVALUEPAIR_2_T2530217319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t2530217319 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2530217319_H
#ifndef MARKER_T2894777777_H
#define MARKER_T2894777777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Marker
struct  Marker_t2894777777 
{
public:
	// System.Int32 System.Collections.Generic.Marker::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_0;
	// System.Int32 System.Collections.Generic.Marker::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Marker_t2894777777, ___U3CCountU3Ek__BackingField_0)); }
	inline int32_t get_U3CCountU3Ek__BackingField_0() const { return ___U3CCountU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_0() { return &___U3CCountU3Ek__BackingField_0; }
	inline void set_U3CCountU3Ek__BackingField_0(int32_t value)
	{
		___U3CCountU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIndexU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Marker_t2894777777, ___U3CIndexU3Ek__BackingField_1)); }
	inline int32_t get_U3CIndexU3Ek__BackingField_1() const { return ___U3CIndexU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CIndexU3Ek__BackingField_1() { return &___U3CIndexU3Ek__BackingField_1; }
	inline void set_U3CIndexU3Ek__BackingField_1(int32_t value)
	{
		___U3CIndexU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKER_T2894777777_H
#ifndef KEYVALUEPAIR_2_T2457078697_H
#define KEYVALUEPAIR_2_T2457078697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>
struct  KeyValuePair_2_t2457078697 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	uint32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2457078697, ___key_0)); }
	inline uint32_t get_key_0() const { return ___key_0; }
	inline uint32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(uint32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2457078697, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2457078697_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef KEYVALUEPAIR_2_T2245450819_H
#define KEYVALUEPAIR_2_T2245450819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>
struct  KeyValuePair_2_t2245450819 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int64_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2245450819, ___key_0)); }
	inline int64_t get_key_0() const { return ___key_0; }
	inline int64_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int64_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2245450819, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2245450819_H
#ifndef KEYVALUEPAIR_2_T2401056908_H
#define KEYVALUEPAIR_2_T2401056908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
struct  KeyValuePair_2_t2401056908 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2401056908, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2401056908, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2401056908_H
#ifndef KEYVALUEPAIR_2_T3842366416_H
#define KEYVALUEPAIR_2_T3842366416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
struct  KeyValuePair_2_t3842366416 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	bool ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3842366416, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3842366416, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3842366416_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef CUSTOMATTRIBUTETYPEDARGUMENT_T2723150157_H
#define CUSTOMATTRIBUTETYPEDARGUMENT_T2723150157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeTypedArgument
struct  CustomAttributeTypedArgument_t2723150157 
{
public:
	// System.Type System.Reflection.CustomAttributeTypedArgument::argumentType
	Type_t * ___argumentType_0;
	// System.Object System.Reflection.CustomAttributeTypedArgument::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_argumentType_0() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t2723150157, ___argumentType_0)); }
	inline Type_t * get_argumentType_0() const { return ___argumentType_0; }
	inline Type_t ** get_address_of_argumentType_0() { return &___argumentType_0; }
	inline void set_argumentType_0(Type_t * value)
	{
		___argumentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___argumentType_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t2723150157, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t2723150157_marshaled_pinvoke
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t2723150157_marshaled_com
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
#endif // CUSTOMATTRIBUTETYPEDARGUMENT_T2723150157_H
#ifndef ILEXCEPTIONBLOCK_T3961874966_H
#define ILEXCEPTIONBLOCK_T3961874966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.ILExceptionBlock
struct  ILExceptionBlock_t3961874966 
{
public:
	// System.Type System.Reflection.Emit.ILExceptionBlock::extype
	Type_t * ___extype_5;
	// System.Int32 System.Reflection.Emit.ILExceptionBlock::type
	int32_t ___type_6;
	// System.Int32 System.Reflection.Emit.ILExceptionBlock::start
	int32_t ___start_7;
	// System.Int32 System.Reflection.Emit.ILExceptionBlock::len
	int32_t ___len_8;
	// System.Int32 System.Reflection.Emit.ILExceptionBlock::filter_offset
	int32_t ___filter_offset_9;

public:
	inline static int32_t get_offset_of_extype_5() { return static_cast<int32_t>(offsetof(ILExceptionBlock_t3961874966, ___extype_5)); }
	inline Type_t * get_extype_5() const { return ___extype_5; }
	inline Type_t ** get_address_of_extype_5() { return &___extype_5; }
	inline void set_extype_5(Type_t * value)
	{
		___extype_5 = value;
		Il2CppCodeGenWriteBarrier((&___extype_5), value);
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(ILExceptionBlock_t3961874966, ___type_6)); }
	inline int32_t get_type_6() const { return ___type_6; }
	inline int32_t* get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(int32_t value)
	{
		___type_6 = value;
	}

	inline static int32_t get_offset_of_start_7() { return static_cast<int32_t>(offsetof(ILExceptionBlock_t3961874966, ___start_7)); }
	inline int32_t get_start_7() const { return ___start_7; }
	inline int32_t* get_address_of_start_7() { return &___start_7; }
	inline void set_start_7(int32_t value)
	{
		___start_7 = value;
	}

	inline static int32_t get_offset_of_len_8() { return static_cast<int32_t>(offsetof(ILExceptionBlock_t3961874966, ___len_8)); }
	inline int32_t get_len_8() const { return ___len_8; }
	inline int32_t* get_address_of_len_8() { return &___len_8; }
	inline void set_len_8(int32_t value)
	{
		___len_8 = value;
	}

	inline static int32_t get_offset_of_filter_offset_9() { return static_cast<int32_t>(offsetof(ILExceptionBlock_t3961874966, ___filter_offset_9)); }
	inline int32_t get_filter_offset_9() const { return ___filter_offset_9; }
	inline int32_t* get_address_of_filter_offset_9() { return &___filter_offset_9; }
	inline void set_filter_offset_9(int32_t value)
	{
		___filter_offset_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Emit.ILExceptionBlock
struct ILExceptionBlock_t3961874966_marshaled_pinvoke
{
	Type_t * ___extype_5;
	int32_t ___type_6;
	int32_t ___start_7;
	int32_t ___len_8;
	int32_t ___filter_offset_9;
};
// Native definition for COM marshalling of System.Reflection.Emit.ILExceptionBlock
struct ILExceptionBlock_t3961874966_marshaled_com
{
	Type_t * ___extype_5;
	int32_t ___type_6;
	int32_t ___start_7;
	int32_t ___len_8;
	int32_t ___filter_offset_9;
};
#endif // ILEXCEPTIONBLOCK_T3961874966_H
#ifndef LOWERCASEMAPPING_T2910317575_H
#define LOWERCASEMAPPING_T2910317575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping
struct  LowerCaseMapping_t2910317575 
{
public:
	// System.Char System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_chMin
	Il2CppChar ____chMin_0;
	// System.Char System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_chMax
	Il2CppChar ____chMax_1;
	// System.Int32 System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_lcOp
	int32_t ____lcOp_2;
	// System.Int32 System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_data
	int32_t ____data_3;

public:
	inline static int32_t get_offset_of__chMin_0() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2910317575, ____chMin_0)); }
	inline Il2CppChar get__chMin_0() const { return ____chMin_0; }
	inline Il2CppChar* get_address_of__chMin_0() { return &____chMin_0; }
	inline void set__chMin_0(Il2CppChar value)
	{
		____chMin_0 = value;
	}

	inline static int32_t get_offset_of__chMax_1() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2910317575, ____chMax_1)); }
	inline Il2CppChar get__chMax_1() const { return ____chMax_1; }
	inline Il2CppChar* get_address_of__chMax_1() { return &____chMax_1; }
	inline void set__chMax_1(Il2CppChar value)
	{
		____chMax_1 = value;
	}

	inline static int32_t get_offset_of__lcOp_2() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2910317575, ____lcOp_2)); }
	inline int32_t get__lcOp_2() const { return ____lcOp_2; }
	inline int32_t* get_address_of__lcOp_2() { return &____lcOp_2; }
	inline void set__lcOp_2(int32_t value)
	{
		____lcOp_2 = value;
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2910317575, ____data_3)); }
	inline int32_t get__data_3() const { return ____data_3; }
	inline int32_t* get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(int32_t value)
	{
		____data_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping
struct LowerCaseMapping_t2910317575_marshaled_pinvoke
{
	uint8_t ____chMin_0;
	uint8_t ____chMax_1;
	int32_t ____lcOp_2;
	int32_t ____data_3;
};
// Native definition for COM marshalling of System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping
struct LowerCaseMapping_t2910317575_marshaled_com
{
	uint8_t ____chMin_0;
	uint8_t ____chMax_1;
	int32_t ____lcOp_2;
	int32_t ____data_3;
};
#endif // LOWERCASEMAPPING_T2910317575_H
#ifndef RESOURCELOCATOR_T3723970807_H
#define RESOURCELOCATOR_T3723970807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResourceLocator
struct  ResourceLocator_t3723970807 
{
public:
	// System.Object System.Resources.ResourceLocator::_value
	RuntimeObject * ____value_0;
	// System.Int32 System.Resources.ResourceLocator::_dataPos
	int32_t ____dataPos_1;

public:
	inline static int32_t get_offset_of__value_0() { return static_cast<int32_t>(offsetof(ResourceLocator_t3723970807, ____value_0)); }
	inline RuntimeObject * get__value_0() const { return ____value_0; }
	inline RuntimeObject ** get_address_of__value_0() { return &____value_0; }
	inline void set__value_0(RuntimeObject * value)
	{
		____value_0 = value;
		Il2CppCodeGenWriteBarrier((&____value_0), value);
	}

	inline static int32_t get_offset_of__dataPos_1() { return static_cast<int32_t>(offsetof(ResourceLocator_t3723970807, ____dataPos_1)); }
	inline int32_t get__dataPos_1() const { return ____dataPos_1; }
	inline int32_t* get_address_of__dataPos_1() { return &____dataPos_1; }
	inline void set__dataPos_1(int32_t value)
	{
		____dataPos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Resources.ResourceLocator
struct ResourceLocator_t3723970807_marshaled_pinvoke
{
	Il2CppIUnknown* ____value_0;
	int32_t ____dataPos_1;
};
// Native definition for COM marshalling of System.Resources.ResourceLocator
struct ResourceLocator_t3723970807_marshaled_com
{
	Il2CppIUnknown* ____value_0;
	int32_t ____dataPos_1;
};
#endif // RESOURCELOCATOR_T3723970807_H
#ifndef XPATHNODEREF_T3498189018_H
#define XPATHNODEREF_T3498189018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodeRef
struct  XPathNodeRef_t3498189018 
{
public:
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeRef::page
	XPathNodeU5BU5D_t47339301* ___page_0;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeRef::idx
	int32_t ___idx_1;

public:
	inline static int32_t get_offset_of_page_0() { return static_cast<int32_t>(offsetof(XPathNodeRef_t3498189018, ___page_0)); }
	inline XPathNodeU5BU5D_t47339301* get_page_0() const { return ___page_0; }
	inline XPathNodeU5BU5D_t47339301** get_address_of_page_0() { return &___page_0; }
	inline void set_page_0(XPathNodeU5BU5D_t47339301* value)
	{
		___page_0 = value;
		Il2CppCodeGenWriteBarrier((&___page_0), value);
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(XPathNodeRef_t3498189018, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MS.Internal.Xml.Cache.XPathNodeRef
struct XPathNodeRef_t3498189018_marshaled_pinvoke
{
	XPathNode_t2208072876_marshaled_pinvoke* ___page_0;
	int32_t ___idx_1;
};
// Native definition for COM marshalling of MS.Internal.Xml.Cache.XPathNodeRef
struct XPathNodeRef_t3498189018_marshaled_com
{
	XPathNode_t2208072876_marshaled_com* ___page_0;
	int32_t ___idx_1;
};
#endif // XPATHNODEREF_T3498189018_H
#ifndef TYPENAMEKEY_T2985541961_H
#define TYPENAMEKEY_T2985541961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey
struct  TypeNameKey_t2985541961 
{
public:
	// System.String Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::AssemblyName
	String_t* ___AssemblyName_0;
	// System.String Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::TypeName
	String_t* ___TypeName_1;

public:
	inline static int32_t get_offset_of_AssemblyName_0() { return static_cast<int32_t>(offsetof(TypeNameKey_t2985541961, ___AssemblyName_0)); }
	inline String_t* get_AssemblyName_0() const { return ___AssemblyName_0; }
	inline String_t** get_address_of_AssemblyName_0() { return &___AssemblyName_0; }
	inline void set_AssemblyName_0(String_t* value)
	{
		___AssemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___AssemblyName_0), value);
	}

	inline static int32_t get_offset_of_TypeName_1() { return static_cast<int32_t>(offsetof(TypeNameKey_t2985541961, ___TypeName_1)); }
	inline String_t* get_TypeName_1() const { return ___TypeName_1; }
	inline String_t** get_address_of_TypeName_1() { return &___TypeName_1; }
	inline void set_TypeName_1(String_t* value)
	{
		___TypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey
struct TypeNameKey_t2985541961_marshaled_pinvoke
{
	char* ___AssemblyName_0;
	char* ___TypeName_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey
struct TypeNameKey_t2985541961_marshaled_com
{
	Il2CppChar* ___AssemblyName_0;
	Il2CppChar* ___TypeName_1;
};
#endif // TYPENAMEKEY_T2985541961_H
#ifndef PARAMETERMODIFIER_T1461694466_H
#define PARAMETERMODIFIER_T1461694466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterModifier
struct  ParameterModifier_t1461694466 
{
public:
	// System.Boolean[] System.Reflection.ParameterModifier::_byRef
	BooleanU5BU5D_t2897418192* ____byRef_0;

public:
	inline static int32_t get_offset_of__byRef_0() { return static_cast<int32_t>(offsetof(ParameterModifier_t1461694466, ____byRef_0)); }
	inline BooleanU5BU5D_t2897418192* get__byRef_0() const { return ____byRef_0; }
	inline BooleanU5BU5D_t2897418192** get_address_of__byRef_0() { return &____byRef_0; }
	inline void set__byRef_0(BooleanU5BU5D_t2897418192* value)
	{
		____byRef_0 = value;
		Il2CppCodeGenWriteBarrier((&____byRef_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t1461694466_marshaled_pinvoke
{
	int32_t* ____byRef_0;
};
// Native definition for COM marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t1461694466_marshaled_com
{
	int32_t* ____byRef_0;
};
#endif // PARAMETERMODIFIER_T1461694466_H
#ifndef EVENTCACHEKEY_T3133620722_H
#define EVENTCACHEKEY_T3133620722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey
struct  EventCacheKey_t3133620722 
{
public:
	// System.Object System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey::target
	RuntimeObject * ___target_0;
	// System.Reflection.MethodInfo System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey::method
	MethodInfo_t * ___method_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(EventCacheKey_t3133620722, ___target_0)); }
	inline RuntimeObject * get_target_0() const { return ___target_0; }
	inline RuntimeObject ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RuntimeObject * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(EventCacheKey_t3133620722, ___method_1)); }
	inline MethodInfo_t * get_method_1() const { return ___method_1; }
	inline MethodInfo_t ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(MethodInfo_t * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier((&___method_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey
struct EventCacheKey_t3133620722_marshaled_pinvoke
{
	Il2CppIUnknown* ___target_0;
	MethodInfo_t * ___method_1;
};
// Native definition for COM marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey
struct EventCacheKey_t3133620722_marshaled_com
{
	Il2CppIUnknown* ___target_0;
	MethodInfo_t * ___method_1;
};
#endif // EVENTCACHEKEY_T3133620722_H
#ifndef ENTRY_T2738501290_H
#define ENTRY_T2738501290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XHashtable`1/XHashtableState/Entry<System.Object>
struct  Entry_t2738501290 
{
public:
	// TValue System.Xml.Linq.XHashtable`1/XHashtableState/Entry::Value
	RuntimeObject * ___Value_0;
	// System.Int32 System.Xml.Linq.XHashtable`1/XHashtableState/Entry::HashCode
	int32_t ___HashCode_1;
	// System.Int32 System.Xml.Linq.XHashtable`1/XHashtableState/Entry::Next
	int32_t ___Next_2;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(Entry_t2738501290, ___Value_0)); }
	inline RuntimeObject * get_Value_0() const { return ___Value_0; }
	inline RuntimeObject ** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(RuntimeObject * value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((&___Value_0), value);
	}

	inline static int32_t get_offset_of_HashCode_1() { return static_cast<int32_t>(offsetof(Entry_t2738501290, ___HashCode_1)); }
	inline int32_t get_HashCode_1() const { return ___HashCode_1; }
	inline int32_t* get_address_of_HashCode_1() { return &___HashCode_1; }
	inline void set_HashCode_1(int32_t value)
	{
		___HashCode_1 = value;
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(Entry_t2738501290, ___Next_2)); }
	inline int32_t get_Next_2() const { return ___Next_2; }
	inline int32_t* get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(int32_t value)
	{
		___Next_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T2738501290_H
#ifndef LABEL_T2281661643_H
#define LABEL_T2281661643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.Label
struct  Label_t2281661643 
{
public:
	// System.Int32 System.Reflection.Emit.Label::label
	int32_t ___label_0;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(Label_t2281661643, ___label_0)); }
	inline int32_t get_label_0() const { return ___label_0; }
	inline int32_t* get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(int32_t value)
	{
		___label_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LABEL_T2281661643_H
#ifndef SEQUENCEPOINT_T2065265856_H
#define SEQUENCEPOINT_T2065265856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.SequencePoint
struct  SequencePoint_t2065265856 
{
public:
	// System.Int32 System.Reflection.Emit.SequencePoint::Offset
	int32_t ___Offset_0;
	// System.Int32 System.Reflection.Emit.SequencePoint::Line
	int32_t ___Line_1;
	// System.Int32 System.Reflection.Emit.SequencePoint::Col
	int32_t ___Col_2;
	// System.Int32 System.Reflection.Emit.SequencePoint::EndLine
	int32_t ___EndLine_3;
	// System.Int32 System.Reflection.Emit.SequencePoint::EndCol
	int32_t ___EndCol_4;

public:
	inline static int32_t get_offset_of_Offset_0() { return static_cast<int32_t>(offsetof(SequencePoint_t2065265856, ___Offset_0)); }
	inline int32_t get_Offset_0() const { return ___Offset_0; }
	inline int32_t* get_address_of_Offset_0() { return &___Offset_0; }
	inline void set_Offset_0(int32_t value)
	{
		___Offset_0 = value;
	}

	inline static int32_t get_offset_of_Line_1() { return static_cast<int32_t>(offsetof(SequencePoint_t2065265856, ___Line_1)); }
	inline int32_t get_Line_1() const { return ___Line_1; }
	inline int32_t* get_address_of_Line_1() { return &___Line_1; }
	inline void set_Line_1(int32_t value)
	{
		___Line_1 = value;
	}

	inline static int32_t get_offset_of_Col_2() { return static_cast<int32_t>(offsetof(SequencePoint_t2065265856, ___Col_2)); }
	inline int32_t get_Col_2() const { return ___Col_2; }
	inline int32_t* get_address_of_Col_2() { return &___Col_2; }
	inline void set_Col_2(int32_t value)
	{
		___Col_2 = value;
	}

	inline static int32_t get_offset_of_EndLine_3() { return static_cast<int32_t>(offsetof(SequencePoint_t2065265856, ___EndLine_3)); }
	inline int32_t get_EndLine_3() const { return ___EndLine_3; }
	inline int32_t* get_address_of_EndLine_3() { return &___EndLine_3; }
	inline void set_EndLine_3(int32_t value)
	{
		___EndLine_3 = value;
	}

	inline static int32_t get_offset_of_EndCol_4() { return static_cast<int32_t>(offsetof(SequencePoint_t2065265856, ___EndCol_4)); }
	inline int32_t get_EndCol_4() const { return ___EndCol_4; }
	inline int32_t* get_address_of_EndCol_4() { return &___EndCol_4; }
	inline void set_EndCol_4(int32_t value)
	{
		___EndCol_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENCEPOINT_T2065265856_H
#ifndef MONOWIN32RESOURCE_T1904229483_H
#define MONOWIN32RESOURCE_T1904229483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.MonoWin32Resource
struct  MonoWin32Resource_t1904229483 
{
public:
	// System.Int32 System.Reflection.Emit.MonoWin32Resource::res_type
	int32_t ___res_type_0;
	// System.Int32 System.Reflection.Emit.MonoWin32Resource::res_id
	int32_t ___res_id_1;
	// System.Int32 System.Reflection.Emit.MonoWin32Resource::lang_id
	int32_t ___lang_id_2;
	// System.Byte[] System.Reflection.Emit.MonoWin32Resource::data
	ByteU5BU5D_t4116647657* ___data_3;

public:
	inline static int32_t get_offset_of_res_type_0() { return static_cast<int32_t>(offsetof(MonoWin32Resource_t1904229483, ___res_type_0)); }
	inline int32_t get_res_type_0() const { return ___res_type_0; }
	inline int32_t* get_address_of_res_type_0() { return &___res_type_0; }
	inline void set_res_type_0(int32_t value)
	{
		___res_type_0 = value;
	}

	inline static int32_t get_offset_of_res_id_1() { return static_cast<int32_t>(offsetof(MonoWin32Resource_t1904229483, ___res_id_1)); }
	inline int32_t get_res_id_1() const { return ___res_id_1; }
	inline int32_t* get_address_of_res_id_1() { return &___res_id_1; }
	inline void set_res_id_1(int32_t value)
	{
		___res_id_1 = value;
	}

	inline static int32_t get_offset_of_lang_id_2() { return static_cast<int32_t>(offsetof(MonoWin32Resource_t1904229483, ___lang_id_2)); }
	inline int32_t get_lang_id_2() const { return ___lang_id_2; }
	inline int32_t* get_address_of_lang_id_2() { return &___lang_id_2; }
	inline void set_lang_id_2(int32_t value)
	{
		___lang_id_2 = value;
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(MonoWin32Resource_t1904229483, ___data_3)); }
	inline ByteU5BU5D_t4116647657* get_data_3() const { return ___data_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(ByteU5BU5D_t4116647657* value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((&___data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Emit.MonoWin32Resource
struct MonoWin32Resource_t1904229483_marshaled_pinvoke
{
	int32_t ___res_type_0;
	int32_t ___res_id_1;
	int32_t ___lang_id_2;
	uint8_t* ___data_3;
};
// Native definition for COM marshalling of System.Reflection.Emit.MonoWin32Resource
struct MonoWin32Resource_t1904229483_marshaled_com
{
	int32_t ___res_type_0;
	int32_t ___res_id_1;
	int32_t ___lang_id_2;
	uint8_t* ___data_3;
};
#endif // MONOWIN32RESOURCE_T1904229483_H
#ifndef ILTOKENINFO_T2325775114_H
#define ILTOKENINFO_T2325775114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.ILTokenInfo
struct  ILTokenInfo_t2325775114 
{
public:
	// System.Reflection.MemberInfo System.Reflection.Emit.ILTokenInfo::member
	MemberInfo_t * ___member_0;
	// System.Int32 System.Reflection.Emit.ILTokenInfo::code_pos
	int32_t ___code_pos_1;

public:
	inline static int32_t get_offset_of_member_0() { return static_cast<int32_t>(offsetof(ILTokenInfo_t2325775114, ___member_0)); }
	inline MemberInfo_t * get_member_0() const { return ___member_0; }
	inline MemberInfo_t ** get_address_of_member_0() { return &___member_0; }
	inline void set_member_0(MemberInfo_t * value)
	{
		___member_0 = value;
		Il2CppCodeGenWriteBarrier((&___member_0), value);
	}

	inline static int32_t get_offset_of_code_pos_1() { return static_cast<int32_t>(offsetof(ILTokenInfo_t2325775114, ___code_pos_1)); }
	inline int32_t get_code_pos_1() const { return ___code_pos_1; }
	inline int32_t* get_address_of_code_pos_1() { return &___code_pos_1; }
	inline void set_code_pos_1(int32_t value)
	{
		___code_pos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Emit.ILTokenInfo
struct ILTokenInfo_t2325775114_marshaled_pinvoke
{
	MemberInfo_t * ___member_0;
	int32_t ___code_pos_1;
};
// Native definition for COM marshalling of System.Reflection.Emit.ILTokenInfo
struct ILTokenInfo_t2325775114_marshaled_com
{
	MemberInfo_t * ___member_0;
	int32_t ___code_pos_1;
};
#endif // ILTOKENINFO_T2325775114_H
#ifndef LABELDATA_T360167391_H
#define LABELDATA_T360167391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.ILGenerator/LabelData
struct  LabelData_t360167391 
{
public:
	// System.Int32 System.Reflection.Emit.ILGenerator/LabelData::addr
	int32_t ___addr_0;
	// System.Int32 System.Reflection.Emit.ILGenerator/LabelData::maxStack
	int32_t ___maxStack_1;

public:
	inline static int32_t get_offset_of_addr_0() { return static_cast<int32_t>(offsetof(LabelData_t360167391, ___addr_0)); }
	inline int32_t get_addr_0() const { return ___addr_0; }
	inline int32_t* get_address_of_addr_0() { return &___addr_0; }
	inline void set_addr_0(int32_t value)
	{
		___addr_0 = value;
	}

	inline static int32_t get_offset_of_maxStack_1() { return static_cast<int32_t>(offsetof(LabelData_t360167391, ___maxStack_1)); }
	inline int32_t get_maxStack_1() const { return ___maxStack_1; }
	inline int32_t* get_address_of_maxStack_1() { return &___maxStack_1; }
	inline void set_maxStack_1(int32_t value)
	{
		___maxStack_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LABELDATA_T360167391_H
#ifndef XBOXCONTROLLERDATA_T1920221146_H
#define XBOXCONTROLLERDATA_T1920221146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.XboxControllerData
struct  XboxControllerData_t1920221146 
{
public:
	// System.String HoloToolkit.Unity.InputModule.XboxControllerData::<GamePadName>k__BackingField
	String_t* ___U3CGamePadNameU3Ek__BackingField_0;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftStickHorizontalAxis>k__BackingField
	float ___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftStickVerticalAxis>k__BackingField
	float ___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightStickHorizontalAxis>k__BackingField
	float ___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightStickVerticalAxis>k__BackingField
	float ___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxDpadHorizontalAxis>k__BackingField
	float ___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxDpadVerticalAxis>k__BackingField
	float ___U3CXboxDpadVerticalAxisU3Ek__BackingField_6;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftTriggerAxis>k__BackingField
	float ___U3CXboxLeftTriggerAxisU3Ek__BackingField_7;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightTriggerAxis>k__BackingField
	float ___U3CXboxRightTriggerAxisU3Ek__BackingField_8;
	// System.Single HoloToolkit.Unity.InputModule.XboxControllerData::<XboxSharedTriggerAxis>k__BackingField
	float ___U3CXboxSharedTriggerAxisU3Ek__BackingField_9;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxA_Pressed>k__BackingField
	bool ___U3CXboxA_PressedU3Ek__BackingField_10;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxB_Pressed>k__BackingField
	bool ___U3CXboxB_PressedU3Ek__BackingField_11;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxX_Pressed>k__BackingField
	bool ___U3CXboxX_PressedU3Ek__BackingField_12;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxY_Pressed>k__BackingField
	bool ___U3CXboxY_PressedU3Ek__BackingField_13;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftBumper_Pressed>k__BackingField
	bool ___U3CXboxLeftBumper_PressedU3Ek__BackingField_14;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightBumper_Pressed>k__BackingField
	bool ___U3CXboxRightBumper_PressedU3Ek__BackingField_15;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftStick_Pressed>k__BackingField
	bool ___U3CXboxLeftStick_PressedU3Ek__BackingField_16;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightStick_Pressed>k__BackingField
	bool ___U3CXboxRightStick_PressedU3Ek__BackingField_17;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxView_Pressed>k__BackingField
	bool ___U3CXboxView_PressedU3Ek__BackingField_18;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxMenu_Pressed>k__BackingField
	bool ___U3CXboxMenu_PressedU3Ek__BackingField_19;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxA_Up>k__BackingField
	bool ___U3CXboxA_UpU3Ek__BackingField_20;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxB_Up>k__BackingField
	bool ___U3CXboxB_UpU3Ek__BackingField_21;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxX_Up>k__BackingField
	bool ___U3CXboxX_UpU3Ek__BackingField_22;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxY_Up>k__BackingField
	bool ___U3CXboxY_UpU3Ek__BackingField_23;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftBumper_Up>k__BackingField
	bool ___U3CXboxLeftBumper_UpU3Ek__BackingField_24;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightBumper_Up>k__BackingField
	bool ___U3CXboxRightBumper_UpU3Ek__BackingField_25;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftStick_Up>k__BackingField
	bool ___U3CXboxLeftStick_UpU3Ek__BackingField_26;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightStick_Up>k__BackingField
	bool ___U3CXboxRightStick_UpU3Ek__BackingField_27;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxView_Up>k__BackingField
	bool ___U3CXboxView_UpU3Ek__BackingField_28;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxMenu_Up>k__BackingField
	bool ___U3CXboxMenu_UpU3Ek__BackingField_29;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxA_Down>k__BackingField
	bool ___U3CXboxA_DownU3Ek__BackingField_30;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxB_Down>k__BackingField
	bool ___U3CXboxB_DownU3Ek__BackingField_31;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxX_Down>k__BackingField
	bool ___U3CXboxX_DownU3Ek__BackingField_32;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxY_Down>k__BackingField
	bool ___U3CXboxY_DownU3Ek__BackingField_33;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftBumper_Down>k__BackingField
	bool ___U3CXboxLeftBumper_DownU3Ek__BackingField_34;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightBumper_Down>k__BackingField
	bool ___U3CXboxRightBumper_DownU3Ek__BackingField_35;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxLeftStick_Down>k__BackingField
	bool ___U3CXboxLeftStick_DownU3Ek__BackingField_36;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxRightStick_Down>k__BackingField
	bool ___U3CXboxRightStick_DownU3Ek__BackingField_37;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxView_Down>k__BackingField
	bool ___U3CXboxView_DownU3Ek__BackingField_38;
	// System.Boolean HoloToolkit.Unity.InputModule.XboxControllerData::<XboxMenu_Down>k__BackingField
	bool ___U3CXboxMenu_DownU3Ek__BackingField_39;

public:
	inline static int32_t get_offset_of_U3CGamePadNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CGamePadNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CGamePadNameU3Ek__BackingField_0() const { return ___U3CGamePadNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CGamePadNameU3Ek__BackingField_0() { return &___U3CGamePadNameU3Ek__BackingField_0; }
	inline void set_U3CGamePadNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CGamePadNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGamePadNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1)); }
	inline float get_U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1() const { return ___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1; }
	inline float* get_address_of_U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1() { return &___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1; }
	inline void set_U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1(float value)
	{
		___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2)); }
	inline float get_U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2() const { return ___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2; }
	inline float* get_address_of_U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2() { return &___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2; }
	inline void set_U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2(float value)
	{
		___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3)); }
	inline float get_U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3() const { return ___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3; }
	inline float* get_address_of_U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3() { return &___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3; }
	inline void set_U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3(float value)
	{
		___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightStickVerticalAxisU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4)); }
	inline float get_U3CXboxRightStickVerticalAxisU3Ek__BackingField_4() const { return ___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4; }
	inline float* get_address_of_U3CXboxRightStickVerticalAxisU3Ek__BackingField_4() { return &___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4; }
	inline void set_U3CXboxRightStickVerticalAxisU3Ek__BackingField_4(float value)
	{
		___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CXboxDpadHorizontalAxisU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5)); }
	inline float get_U3CXboxDpadHorizontalAxisU3Ek__BackingField_5() const { return ___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5; }
	inline float* get_address_of_U3CXboxDpadHorizontalAxisU3Ek__BackingField_5() { return &___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5; }
	inline void set_U3CXboxDpadHorizontalAxisU3Ek__BackingField_5(float value)
	{
		___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CXboxDpadVerticalAxisU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxDpadVerticalAxisU3Ek__BackingField_6)); }
	inline float get_U3CXboxDpadVerticalAxisU3Ek__BackingField_6() const { return ___U3CXboxDpadVerticalAxisU3Ek__BackingField_6; }
	inline float* get_address_of_U3CXboxDpadVerticalAxisU3Ek__BackingField_6() { return &___U3CXboxDpadVerticalAxisU3Ek__BackingField_6; }
	inline void set_U3CXboxDpadVerticalAxisU3Ek__BackingField_6(float value)
	{
		___U3CXboxDpadVerticalAxisU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftTriggerAxisU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftTriggerAxisU3Ek__BackingField_7)); }
	inline float get_U3CXboxLeftTriggerAxisU3Ek__BackingField_7() const { return ___U3CXboxLeftTriggerAxisU3Ek__BackingField_7; }
	inline float* get_address_of_U3CXboxLeftTriggerAxisU3Ek__BackingField_7() { return &___U3CXboxLeftTriggerAxisU3Ek__BackingField_7; }
	inline void set_U3CXboxLeftTriggerAxisU3Ek__BackingField_7(float value)
	{
		___U3CXboxLeftTriggerAxisU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightTriggerAxisU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightTriggerAxisU3Ek__BackingField_8)); }
	inline float get_U3CXboxRightTriggerAxisU3Ek__BackingField_8() const { return ___U3CXboxRightTriggerAxisU3Ek__BackingField_8; }
	inline float* get_address_of_U3CXboxRightTriggerAxisU3Ek__BackingField_8() { return &___U3CXboxRightTriggerAxisU3Ek__BackingField_8; }
	inline void set_U3CXboxRightTriggerAxisU3Ek__BackingField_8(float value)
	{
		___U3CXboxRightTriggerAxisU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CXboxSharedTriggerAxisU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxSharedTriggerAxisU3Ek__BackingField_9)); }
	inline float get_U3CXboxSharedTriggerAxisU3Ek__BackingField_9() const { return ___U3CXboxSharedTriggerAxisU3Ek__BackingField_9; }
	inline float* get_address_of_U3CXboxSharedTriggerAxisU3Ek__BackingField_9() { return &___U3CXboxSharedTriggerAxisU3Ek__BackingField_9; }
	inline void set_U3CXboxSharedTriggerAxisU3Ek__BackingField_9(float value)
	{
		___U3CXboxSharedTriggerAxisU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CXboxA_PressedU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxA_PressedU3Ek__BackingField_10)); }
	inline bool get_U3CXboxA_PressedU3Ek__BackingField_10() const { return ___U3CXboxA_PressedU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CXboxA_PressedU3Ek__BackingField_10() { return &___U3CXboxA_PressedU3Ek__BackingField_10; }
	inline void set_U3CXboxA_PressedU3Ek__BackingField_10(bool value)
	{
		___U3CXboxA_PressedU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CXboxB_PressedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxB_PressedU3Ek__BackingField_11)); }
	inline bool get_U3CXboxB_PressedU3Ek__BackingField_11() const { return ___U3CXboxB_PressedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CXboxB_PressedU3Ek__BackingField_11() { return &___U3CXboxB_PressedU3Ek__BackingField_11; }
	inline void set_U3CXboxB_PressedU3Ek__BackingField_11(bool value)
	{
		___U3CXboxB_PressedU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CXboxX_PressedU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxX_PressedU3Ek__BackingField_12)); }
	inline bool get_U3CXboxX_PressedU3Ek__BackingField_12() const { return ___U3CXboxX_PressedU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CXboxX_PressedU3Ek__BackingField_12() { return &___U3CXboxX_PressedU3Ek__BackingField_12; }
	inline void set_U3CXboxX_PressedU3Ek__BackingField_12(bool value)
	{
		___U3CXboxX_PressedU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CXboxY_PressedU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxY_PressedU3Ek__BackingField_13)); }
	inline bool get_U3CXboxY_PressedU3Ek__BackingField_13() const { return ___U3CXboxY_PressedU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CXboxY_PressedU3Ek__BackingField_13() { return &___U3CXboxY_PressedU3Ek__BackingField_13; }
	inline void set_U3CXboxY_PressedU3Ek__BackingField_13(bool value)
	{
		___U3CXboxY_PressedU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftBumper_PressedU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftBumper_PressedU3Ek__BackingField_14)); }
	inline bool get_U3CXboxLeftBumper_PressedU3Ek__BackingField_14() const { return ___U3CXboxLeftBumper_PressedU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CXboxLeftBumper_PressedU3Ek__BackingField_14() { return &___U3CXboxLeftBumper_PressedU3Ek__BackingField_14; }
	inline void set_U3CXboxLeftBumper_PressedU3Ek__BackingField_14(bool value)
	{
		___U3CXboxLeftBumper_PressedU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightBumper_PressedU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightBumper_PressedU3Ek__BackingField_15)); }
	inline bool get_U3CXboxRightBumper_PressedU3Ek__BackingField_15() const { return ___U3CXboxRightBumper_PressedU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CXboxRightBumper_PressedU3Ek__BackingField_15() { return &___U3CXboxRightBumper_PressedU3Ek__BackingField_15; }
	inline void set_U3CXboxRightBumper_PressedU3Ek__BackingField_15(bool value)
	{
		___U3CXboxRightBumper_PressedU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftStick_PressedU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftStick_PressedU3Ek__BackingField_16)); }
	inline bool get_U3CXboxLeftStick_PressedU3Ek__BackingField_16() const { return ___U3CXboxLeftStick_PressedU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CXboxLeftStick_PressedU3Ek__BackingField_16() { return &___U3CXboxLeftStick_PressedU3Ek__BackingField_16; }
	inline void set_U3CXboxLeftStick_PressedU3Ek__BackingField_16(bool value)
	{
		___U3CXboxLeftStick_PressedU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightStick_PressedU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightStick_PressedU3Ek__BackingField_17)); }
	inline bool get_U3CXboxRightStick_PressedU3Ek__BackingField_17() const { return ___U3CXboxRightStick_PressedU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CXboxRightStick_PressedU3Ek__BackingField_17() { return &___U3CXboxRightStick_PressedU3Ek__BackingField_17; }
	inline void set_U3CXboxRightStick_PressedU3Ek__BackingField_17(bool value)
	{
		___U3CXboxRightStick_PressedU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CXboxView_PressedU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxView_PressedU3Ek__BackingField_18)); }
	inline bool get_U3CXboxView_PressedU3Ek__BackingField_18() const { return ___U3CXboxView_PressedU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CXboxView_PressedU3Ek__BackingField_18() { return &___U3CXboxView_PressedU3Ek__BackingField_18; }
	inline void set_U3CXboxView_PressedU3Ek__BackingField_18(bool value)
	{
		___U3CXboxView_PressedU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CXboxMenu_PressedU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxMenu_PressedU3Ek__BackingField_19)); }
	inline bool get_U3CXboxMenu_PressedU3Ek__BackingField_19() const { return ___U3CXboxMenu_PressedU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CXboxMenu_PressedU3Ek__BackingField_19() { return &___U3CXboxMenu_PressedU3Ek__BackingField_19; }
	inline void set_U3CXboxMenu_PressedU3Ek__BackingField_19(bool value)
	{
		___U3CXboxMenu_PressedU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CXboxA_UpU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxA_UpU3Ek__BackingField_20)); }
	inline bool get_U3CXboxA_UpU3Ek__BackingField_20() const { return ___U3CXboxA_UpU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CXboxA_UpU3Ek__BackingField_20() { return &___U3CXboxA_UpU3Ek__BackingField_20; }
	inline void set_U3CXboxA_UpU3Ek__BackingField_20(bool value)
	{
		___U3CXboxA_UpU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CXboxB_UpU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxB_UpU3Ek__BackingField_21)); }
	inline bool get_U3CXboxB_UpU3Ek__BackingField_21() const { return ___U3CXboxB_UpU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CXboxB_UpU3Ek__BackingField_21() { return &___U3CXboxB_UpU3Ek__BackingField_21; }
	inline void set_U3CXboxB_UpU3Ek__BackingField_21(bool value)
	{
		___U3CXboxB_UpU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CXboxX_UpU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxX_UpU3Ek__BackingField_22)); }
	inline bool get_U3CXboxX_UpU3Ek__BackingField_22() const { return ___U3CXboxX_UpU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CXboxX_UpU3Ek__BackingField_22() { return &___U3CXboxX_UpU3Ek__BackingField_22; }
	inline void set_U3CXboxX_UpU3Ek__BackingField_22(bool value)
	{
		___U3CXboxX_UpU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CXboxY_UpU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxY_UpU3Ek__BackingField_23)); }
	inline bool get_U3CXboxY_UpU3Ek__BackingField_23() const { return ___U3CXboxY_UpU3Ek__BackingField_23; }
	inline bool* get_address_of_U3CXboxY_UpU3Ek__BackingField_23() { return &___U3CXboxY_UpU3Ek__BackingField_23; }
	inline void set_U3CXboxY_UpU3Ek__BackingField_23(bool value)
	{
		___U3CXboxY_UpU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftBumper_UpU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftBumper_UpU3Ek__BackingField_24)); }
	inline bool get_U3CXboxLeftBumper_UpU3Ek__BackingField_24() const { return ___U3CXboxLeftBumper_UpU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CXboxLeftBumper_UpU3Ek__BackingField_24() { return &___U3CXboxLeftBumper_UpU3Ek__BackingField_24; }
	inline void set_U3CXboxLeftBumper_UpU3Ek__BackingField_24(bool value)
	{
		___U3CXboxLeftBumper_UpU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightBumper_UpU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightBumper_UpU3Ek__BackingField_25)); }
	inline bool get_U3CXboxRightBumper_UpU3Ek__BackingField_25() const { return ___U3CXboxRightBumper_UpU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CXboxRightBumper_UpU3Ek__BackingField_25() { return &___U3CXboxRightBumper_UpU3Ek__BackingField_25; }
	inline void set_U3CXboxRightBumper_UpU3Ek__BackingField_25(bool value)
	{
		___U3CXboxRightBumper_UpU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftStick_UpU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftStick_UpU3Ek__BackingField_26)); }
	inline bool get_U3CXboxLeftStick_UpU3Ek__BackingField_26() const { return ___U3CXboxLeftStick_UpU3Ek__BackingField_26; }
	inline bool* get_address_of_U3CXboxLeftStick_UpU3Ek__BackingField_26() { return &___U3CXboxLeftStick_UpU3Ek__BackingField_26; }
	inline void set_U3CXboxLeftStick_UpU3Ek__BackingField_26(bool value)
	{
		___U3CXboxLeftStick_UpU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightStick_UpU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightStick_UpU3Ek__BackingField_27)); }
	inline bool get_U3CXboxRightStick_UpU3Ek__BackingField_27() const { return ___U3CXboxRightStick_UpU3Ek__BackingField_27; }
	inline bool* get_address_of_U3CXboxRightStick_UpU3Ek__BackingField_27() { return &___U3CXboxRightStick_UpU3Ek__BackingField_27; }
	inline void set_U3CXboxRightStick_UpU3Ek__BackingField_27(bool value)
	{
		___U3CXboxRightStick_UpU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CXboxView_UpU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxView_UpU3Ek__BackingField_28)); }
	inline bool get_U3CXboxView_UpU3Ek__BackingField_28() const { return ___U3CXboxView_UpU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CXboxView_UpU3Ek__BackingField_28() { return &___U3CXboxView_UpU3Ek__BackingField_28; }
	inline void set_U3CXboxView_UpU3Ek__BackingField_28(bool value)
	{
		___U3CXboxView_UpU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CXboxMenu_UpU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxMenu_UpU3Ek__BackingField_29)); }
	inline bool get_U3CXboxMenu_UpU3Ek__BackingField_29() const { return ___U3CXboxMenu_UpU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CXboxMenu_UpU3Ek__BackingField_29() { return &___U3CXboxMenu_UpU3Ek__BackingField_29; }
	inline void set_U3CXboxMenu_UpU3Ek__BackingField_29(bool value)
	{
		___U3CXboxMenu_UpU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CXboxA_DownU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxA_DownU3Ek__BackingField_30)); }
	inline bool get_U3CXboxA_DownU3Ek__BackingField_30() const { return ___U3CXboxA_DownU3Ek__BackingField_30; }
	inline bool* get_address_of_U3CXboxA_DownU3Ek__BackingField_30() { return &___U3CXboxA_DownU3Ek__BackingField_30; }
	inline void set_U3CXboxA_DownU3Ek__BackingField_30(bool value)
	{
		___U3CXboxA_DownU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CXboxB_DownU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxB_DownU3Ek__BackingField_31)); }
	inline bool get_U3CXboxB_DownU3Ek__BackingField_31() const { return ___U3CXboxB_DownU3Ek__BackingField_31; }
	inline bool* get_address_of_U3CXboxB_DownU3Ek__BackingField_31() { return &___U3CXboxB_DownU3Ek__BackingField_31; }
	inline void set_U3CXboxB_DownU3Ek__BackingField_31(bool value)
	{
		___U3CXboxB_DownU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CXboxX_DownU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxX_DownU3Ek__BackingField_32)); }
	inline bool get_U3CXboxX_DownU3Ek__BackingField_32() const { return ___U3CXboxX_DownU3Ek__BackingField_32; }
	inline bool* get_address_of_U3CXboxX_DownU3Ek__BackingField_32() { return &___U3CXboxX_DownU3Ek__BackingField_32; }
	inline void set_U3CXboxX_DownU3Ek__BackingField_32(bool value)
	{
		___U3CXboxX_DownU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CXboxY_DownU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxY_DownU3Ek__BackingField_33)); }
	inline bool get_U3CXboxY_DownU3Ek__BackingField_33() const { return ___U3CXboxY_DownU3Ek__BackingField_33; }
	inline bool* get_address_of_U3CXboxY_DownU3Ek__BackingField_33() { return &___U3CXboxY_DownU3Ek__BackingField_33; }
	inline void set_U3CXboxY_DownU3Ek__BackingField_33(bool value)
	{
		___U3CXboxY_DownU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftBumper_DownU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftBumper_DownU3Ek__BackingField_34)); }
	inline bool get_U3CXboxLeftBumper_DownU3Ek__BackingField_34() const { return ___U3CXboxLeftBumper_DownU3Ek__BackingField_34; }
	inline bool* get_address_of_U3CXboxLeftBumper_DownU3Ek__BackingField_34() { return &___U3CXboxLeftBumper_DownU3Ek__BackingField_34; }
	inline void set_U3CXboxLeftBumper_DownU3Ek__BackingField_34(bool value)
	{
		___U3CXboxLeftBumper_DownU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightBumper_DownU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightBumper_DownU3Ek__BackingField_35)); }
	inline bool get_U3CXboxRightBumper_DownU3Ek__BackingField_35() const { return ___U3CXboxRightBumper_DownU3Ek__BackingField_35; }
	inline bool* get_address_of_U3CXboxRightBumper_DownU3Ek__BackingField_35() { return &___U3CXboxRightBumper_DownU3Ek__BackingField_35; }
	inline void set_U3CXboxRightBumper_DownU3Ek__BackingField_35(bool value)
	{
		___U3CXboxRightBumper_DownU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_U3CXboxLeftStick_DownU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxLeftStick_DownU3Ek__BackingField_36)); }
	inline bool get_U3CXboxLeftStick_DownU3Ek__BackingField_36() const { return ___U3CXboxLeftStick_DownU3Ek__BackingField_36; }
	inline bool* get_address_of_U3CXboxLeftStick_DownU3Ek__BackingField_36() { return &___U3CXboxLeftStick_DownU3Ek__BackingField_36; }
	inline void set_U3CXboxLeftStick_DownU3Ek__BackingField_36(bool value)
	{
		___U3CXboxLeftStick_DownU3Ek__BackingField_36 = value;
	}

	inline static int32_t get_offset_of_U3CXboxRightStick_DownU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxRightStick_DownU3Ek__BackingField_37)); }
	inline bool get_U3CXboxRightStick_DownU3Ek__BackingField_37() const { return ___U3CXboxRightStick_DownU3Ek__BackingField_37; }
	inline bool* get_address_of_U3CXboxRightStick_DownU3Ek__BackingField_37() { return &___U3CXboxRightStick_DownU3Ek__BackingField_37; }
	inline void set_U3CXboxRightStick_DownU3Ek__BackingField_37(bool value)
	{
		___U3CXboxRightStick_DownU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_U3CXboxView_DownU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxView_DownU3Ek__BackingField_38)); }
	inline bool get_U3CXboxView_DownU3Ek__BackingField_38() const { return ___U3CXboxView_DownU3Ek__BackingField_38; }
	inline bool* get_address_of_U3CXboxView_DownU3Ek__BackingField_38() { return &___U3CXboxView_DownU3Ek__BackingField_38; }
	inline void set_U3CXboxView_DownU3Ek__BackingField_38(bool value)
	{
		___U3CXboxView_DownU3Ek__BackingField_38 = value;
	}

	inline static int32_t get_offset_of_U3CXboxMenu_DownU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(XboxControllerData_t1920221146, ___U3CXboxMenu_DownU3Ek__BackingField_39)); }
	inline bool get_U3CXboxMenu_DownU3Ek__BackingField_39() const { return ___U3CXboxMenu_DownU3Ek__BackingField_39; }
	inline bool* get_address_of_U3CXboxMenu_DownU3Ek__BackingField_39() { return &___U3CXboxMenu_DownU3Ek__BackingField_39; }
	inline void set_U3CXboxMenu_DownU3Ek__BackingField_39(bool value)
	{
		___U3CXboxMenu_DownU3Ek__BackingField_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.InputModule.XboxControllerData
struct XboxControllerData_t1920221146_marshaled_pinvoke
{
	char* ___U3CGamePadNameU3Ek__BackingField_0;
	float ___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1;
	float ___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2;
	float ___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3;
	float ___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4;
	float ___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5;
	float ___U3CXboxDpadVerticalAxisU3Ek__BackingField_6;
	float ___U3CXboxLeftTriggerAxisU3Ek__BackingField_7;
	float ___U3CXboxRightTriggerAxisU3Ek__BackingField_8;
	float ___U3CXboxSharedTriggerAxisU3Ek__BackingField_9;
	int32_t ___U3CXboxA_PressedU3Ek__BackingField_10;
	int32_t ___U3CXboxB_PressedU3Ek__BackingField_11;
	int32_t ___U3CXboxX_PressedU3Ek__BackingField_12;
	int32_t ___U3CXboxY_PressedU3Ek__BackingField_13;
	int32_t ___U3CXboxLeftBumper_PressedU3Ek__BackingField_14;
	int32_t ___U3CXboxRightBumper_PressedU3Ek__BackingField_15;
	int32_t ___U3CXboxLeftStick_PressedU3Ek__BackingField_16;
	int32_t ___U3CXboxRightStick_PressedU3Ek__BackingField_17;
	int32_t ___U3CXboxView_PressedU3Ek__BackingField_18;
	int32_t ___U3CXboxMenu_PressedU3Ek__BackingField_19;
	int32_t ___U3CXboxA_UpU3Ek__BackingField_20;
	int32_t ___U3CXboxB_UpU3Ek__BackingField_21;
	int32_t ___U3CXboxX_UpU3Ek__BackingField_22;
	int32_t ___U3CXboxY_UpU3Ek__BackingField_23;
	int32_t ___U3CXboxLeftBumper_UpU3Ek__BackingField_24;
	int32_t ___U3CXboxRightBumper_UpU3Ek__BackingField_25;
	int32_t ___U3CXboxLeftStick_UpU3Ek__BackingField_26;
	int32_t ___U3CXboxRightStick_UpU3Ek__BackingField_27;
	int32_t ___U3CXboxView_UpU3Ek__BackingField_28;
	int32_t ___U3CXboxMenu_UpU3Ek__BackingField_29;
	int32_t ___U3CXboxA_DownU3Ek__BackingField_30;
	int32_t ___U3CXboxB_DownU3Ek__BackingField_31;
	int32_t ___U3CXboxX_DownU3Ek__BackingField_32;
	int32_t ___U3CXboxY_DownU3Ek__BackingField_33;
	int32_t ___U3CXboxLeftBumper_DownU3Ek__BackingField_34;
	int32_t ___U3CXboxRightBumper_DownU3Ek__BackingField_35;
	int32_t ___U3CXboxLeftStick_DownU3Ek__BackingField_36;
	int32_t ___U3CXboxRightStick_DownU3Ek__BackingField_37;
	int32_t ___U3CXboxView_DownU3Ek__BackingField_38;
	int32_t ___U3CXboxMenu_DownU3Ek__BackingField_39;
};
// Native definition for COM marshalling of HoloToolkit.Unity.InputModule.XboxControllerData
struct XboxControllerData_t1920221146_marshaled_com
{
	Il2CppChar* ___U3CGamePadNameU3Ek__BackingField_0;
	float ___U3CXboxLeftStickHorizontalAxisU3Ek__BackingField_1;
	float ___U3CXboxLeftStickVerticalAxisU3Ek__BackingField_2;
	float ___U3CXboxRightStickHorizontalAxisU3Ek__BackingField_3;
	float ___U3CXboxRightStickVerticalAxisU3Ek__BackingField_4;
	float ___U3CXboxDpadHorizontalAxisU3Ek__BackingField_5;
	float ___U3CXboxDpadVerticalAxisU3Ek__BackingField_6;
	float ___U3CXboxLeftTriggerAxisU3Ek__BackingField_7;
	float ___U3CXboxRightTriggerAxisU3Ek__BackingField_8;
	float ___U3CXboxSharedTriggerAxisU3Ek__BackingField_9;
	int32_t ___U3CXboxA_PressedU3Ek__BackingField_10;
	int32_t ___U3CXboxB_PressedU3Ek__BackingField_11;
	int32_t ___U3CXboxX_PressedU3Ek__BackingField_12;
	int32_t ___U3CXboxY_PressedU3Ek__BackingField_13;
	int32_t ___U3CXboxLeftBumper_PressedU3Ek__BackingField_14;
	int32_t ___U3CXboxRightBumper_PressedU3Ek__BackingField_15;
	int32_t ___U3CXboxLeftStick_PressedU3Ek__BackingField_16;
	int32_t ___U3CXboxRightStick_PressedU3Ek__BackingField_17;
	int32_t ___U3CXboxView_PressedU3Ek__BackingField_18;
	int32_t ___U3CXboxMenu_PressedU3Ek__BackingField_19;
	int32_t ___U3CXboxA_UpU3Ek__BackingField_20;
	int32_t ___U3CXboxB_UpU3Ek__BackingField_21;
	int32_t ___U3CXboxX_UpU3Ek__BackingField_22;
	int32_t ___U3CXboxY_UpU3Ek__BackingField_23;
	int32_t ___U3CXboxLeftBumper_UpU3Ek__BackingField_24;
	int32_t ___U3CXboxRightBumper_UpU3Ek__BackingField_25;
	int32_t ___U3CXboxLeftStick_UpU3Ek__BackingField_26;
	int32_t ___U3CXboxRightStick_UpU3Ek__BackingField_27;
	int32_t ___U3CXboxView_UpU3Ek__BackingField_28;
	int32_t ___U3CXboxMenu_UpU3Ek__BackingField_29;
	int32_t ___U3CXboxA_DownU3Ek__BackingField_30;
	int32_t ___U3CXboxB_DownU3Ek__BackingField_31;
	int32_t ___U3CXboxX_DownU3Ek__BackingField_32;
	int32_t ___U3CXboxY_DownU3Ek__BackingField_33;
	int32_t ___U3CXboxLeftBumper_DownU3Ek__BackingField_34;
	int32_t ___U3CXboxRightBumper_DownU3Ek__BackingField_35;
	int32_t ___U3CXboxLeftStick_DownU3Ek__BackingField_36;
	int32_t ___U3CXboxRightStick_DownU3Ek__BackingField_37;
	int32_t ___U3CXboxView_DownU3Ek__BackingField_38;
	int32_t ___U3CXboxMenu_DownU3Ek__BackingField_39;
};
#endif // XBOXCONTROLLERDATA_T1920221146_H
#ifndef MAP_T1331044427_H
#define MAP_T1331044427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.FacetsChecker/FacetsCompiler/Map
struct  Map_t1331044427 
{
public:
	// System.Char System.Xml.Schema.FacetsChecker/FacetsCompiler/Map::match
	Il2CppChar ___match_0;
	// System.String System.Xml.Schema.FacetsChecker/FacetsCompiler/Map::replacement
	String_t* ___replacement_1;

public:
	inline static int32_t get_offset_of_match_0() { return static_cast<int32_t>(offsetof(Map_t1331044427, ___match_0)); }
	inline Il2CppChar get_match_0() const { return ___match_0; }
	inline Il2CppChar* get_address_of_match_0() { return &___match_0; }
	inline void set_match_0(Il2CppChar value)
	{
		___match_0 = value;
	}

	inline static int32_t get_offset_of_replacement_1() { return static_cast<int32_t>(offsetof(Map_t1331044427, ___replacement_1)); }
	inline String_t* get_replacement_1() const { return ___replacement_1; }
	inline String_t** get_address_of_replacement_1() { return &___replacement_1; }
	inline void set_replacement_1(String_t* value)
	{
		___replacement_1 = value;
		Il2CppCodeGenWriteBarrier((&___replacement_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.FacetsChecker/FacetsCompiler/Map
struct Map_t1331044427_marshaled_pinvoke
{
	uint8_t ___match_0;
	char* ___replacement_1;
};
// Native definition for COM marshalling of System.Xml.Schema.FacetsChecker/FacetsCompiler/Map
struct Map_t1331044427_marshaled_com
{
	uint8_t ___match_0;
	Il2CppChar* ___replacement_1;
};
#endif // MAP_T1331044427_H
#ifndef LABELFIXUP_T858502054_H
#define LABELFIXUP_T858502054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.ILGenerator/LabelFixup
struct  LabelFixup_t858502054 
{
public:
	// System.Int32 System.Reflection.Emit.ILGenerator/LabelFixup::offset
	int32_t ___offset_0;
	// System.Int32 System.Reflection.Emit.ILGenerator/LabelFixup::pos
	int32_t ___pos_1;
	// System.Int32 System.Reflection.Emit.ILGenerator/LabelFixup::label_idx
	int32_t ___label_idx_2;

public:
	inline static int32_t get_offset_of_offset_0() { return static_cast<int32_t>(offsetof(LabelFixup_t858502054, ___offset_0)); }
	inline int32_t get_offset_0() const { return ___offset_0; }
	inline int32_t* get_address_of_offset_0() { return &___offset_0; }
	inline void set_offset_0(int32_t value)
	{
		___offset_0 = value;
	}

	inline static int32_t get_offset_of_pos_1() { return static_cast<int32_t>(offsetof(LabelFixup_t858502054, ___pos_1)); }
	inline int32_t get_pos_1() const { return ___pos_1; }
	inline int32_t* get_address_of_pos_1() { return &___pos_1; }
	inline void set_pos_1(int32_t value)
	{
		___pos_1 = value;
	}

	inline static int32_t get_offset_of_label_idx_2() { return static_cast<int32_t>(offsetof(LabelFixup_t858502054, ___label_idx_2)); }
	inline int32_t get_label_idx_2() const { return ___label_idx_2; }
	inline int32_t* get_address_of_label_idx_2() { return &___label_idx_2; }
	inline void set_label_idx_2(int32_t value)
	{
		___label_idx_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LABELFIXUP_T858502054_H
#ifndef FORMATPARAM_T4194474082_H
#define FORMATPARAM_T4194474082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ParameterizedStrings/FormatParam
struct  FormatParam_t4194474082 
{
public:
	// System.Int32 System.ParameterizedStrings/FormatParam::_int32
	int32_t ____int32_0;
	// System.String System.ParameterizedStrings/FormatParam::_string
	String_t* ____string_1;

public:
	inline static int32_t get_offset_of__int32_0() { return static_cast<int32_t>(offsetof(FormatParam_t4194474082, ____int32_0)); }
	inline int32_t get__int32_0() const { return ____int32_0; }
	inline int32_t* get_address_of__int32_0() { return &____int32_0; }
	inline void set__int32_0(int32_t value)
	{
		____int32_0 = value;
	}

	inline static int32_t get_offset_of__string_1() { return static_cast<int32_t>(offsetof(FormatParam_t4194474082, ____string_1)); }
	inline String_t* get__string_1() const { return ____string_1; }
	inline String_t** get_address_of__string_1() { return &____string_1; }
	inline void set__string_1(String_t* value)
	{
		____string_1 = value;
		Il2CppCodeGenWriteBarrier((&____string_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ParameterizedStrings/FormatParam
struct FormatParam_t4194474082_marshaled_pinvoke
{
	int32_t ____int32_0;
	char* ____string_1;
};
// Native definition for COM marshalling of System.ParameterizedStrings/FormatParam
struct FormatParam_t4194474082_marshaled_com
{
	int32_t ____int32_0;
	Il2CppChar* ____string_1;
};
#endif // FORMATPARAM_T4194474082_H
#ifndef TIMERDATA_T4056715490_H
#define TIMERDATA_T4056715490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TimerScheduler/TimerData
struct  TimerData_t4056715490 
{
public:
	// HoloToolkit.Unity.TimerScheduler/Callback HoloToolkit.Unity.TimerScheduler/TimerData::Callback
	Callback_t2663646540 * ___Callback_0;
	// System.Single HoloToolkit.Unity.TimerScheduler/TimerData::Duration
	float ___Duration_1;
	// System.Boolean HoloToolkit.Unity.TimerScheduler/TimerData::Loop
	bool ___Loop_2;
	// System.Int32 HoloToolkit.Unity.TimerScheduler/TimerData::Id
	int32_t ___Id_3;

public:
	inline static int32_t get_offset_of_Callback_0() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Callback_0)); }
	inline Callback_t2663646540 * get_Callback_0() const { return ___Callback_0; }
	inline Callback_t2663646540 ** get_address_of_Callback_0() { return &___Callback_0; }
	inline void set_Callback_0(Callback_t2663646540 * value)
	{
		___Callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___Callback_0), value);
	}

	inline static int32_t get_offset_of_Duration_1() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Duration_1)); }
	inline float get_Duration_1() const { return ___Duration_1; }
	inline float* get_address_of_Duration_1() { return &___Duration_1; }
	inline void set_Duration_1(float value)
	{
		___Duration_1 = value;
	}

	inline static int32_t get_offset_of_Loop_2() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Loop_2)); }
	inline bool get_Loop_2() const { return ___Loop_2; }
	inline bool* get_address_of_Loop_2() { return &___Loop_2; }
	inline void set_Loop_2(bool value)
	{
		___Loop_2 = value;
	}

	inline static int32_t get_offset_of_Id_3() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Id_3)); }
	inline int32_t get_Id_3() const { return ___Id_3; }
	inline int32_t* get_address_of_Id_3() { return &___Id_3; }
	inline void set_Id_3(int32_t value)
	{
		___Id_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.TimerScheduler/TimerData
struct TimerData_t4056715490_marshaled_pinvoke
{
	Il2CppMethodPointer ___Callback_0;
	float ___Duration_1;
	int32_t ___Loop_2;
	int32_t ___Id_3;
};
// Native definition for COM marshalling of HoloToolkit.Unity.TimerScheduler/TimerData
struct TimerData_t4056715490_marshaled_com
{
	Il2CppMethodPointer ___Callback_0;
	float ___Duration_1;
	int32_t ___Loop_2;
	int32_t ___Id_3;
};
#endif // TIMERDATA_T4056715490_H
#ifndef TYPECONVERTKEY_T285306760_H
#define TYPECONVERTKEY_T285306760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct  TypeConvertKey_t285306760 
{
public:
	// System.Type Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey::_initialType
	Type_t * ____initialType_0;
	// System.Type Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey::_targetType
	Type_t * ____targetType_1;

public:
	inline static int32_t get_offset_of__initialType_0() { return static_cast<int32_t>(offsetof(TypeConvertKey_t285306760, ____initialType_0)); }
	inline Type_t * get__initialType_0() const { return ____initialType_0; }
	inline Type_t ** get_address_of__initialType_0() { return &____initialType_0; }
	inline void set__initialType_0(Type_t * value)
	{
		____initialType_0 = value;
		Il2CppCodeGenWriteBarrier((&____initialType_0), value);
	}

	inline static int32_t get_offset_of__targetType_1() { return static_cast<int32_t>(offsetof(TypeConvertKey_t285306760, ____targetType_1)); }
	inline Type_t * get__targetType_1() const { return ____targetType_1; }
	inline Type_t ** get_address_of__targetType_1() { return &____targetType_1; }
	inline void set__targetType_1(Type_t * value)
	{
		____targetType_1 = value;
		Il2CppCodeGenWriteBarrier((&____targetType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t285306760_marshaled_pinvoke
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t285306760_marshaled_com
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
#endif // TYPECONVERTKEY_T285306760_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_4;
	// System.Int16 System.Guid::_b
	int16_t ____b_5;
	// System.Int16 System.Guid::_c
	int16_t ____c_6;
	// System.Byte System.Guid::_d
	uint8_t ____d_7;
	// System.Byte System.Guid::_e
	uint8_t ____e_8;
	// System.Byte System.Guid::_f
	uint8_t ____f_9;
	// System.Byte System.Guid::_g
	uint8_t ____g_10;
	// System.Byte System.Guid::_h
	uint8_t ____h_11;
	// System.Byte System.Guid::_i
	uint8_t ____i_12;
	// System.Byte System.Guid::_j
	uint8_t ____j_13;
	// System.Byte System.Guid::_k
	uint8_t ____k_14;

public:
	inline static int32_t get_offset_of__a_4() { return static_cast<int32_t>(offsetof(Guid_t, ____a_4)); }
	inline int32_t get__a_4() const { return ____a_4; }
	inline int32_t* get_address_of__a_4() { return &____a_4; }
	inline void set__a_4(int32_t value)
	{
		____a_4 = value;
	}

	inline static int32_t get_offset_of__b_5() { return static_cast<int32_t>(offsetof(Guid_t, ____b_5)); }
	inline int16_t get__b_5() const { return ____b_5; }
	inline int16_t* get_address_of__b_5() { return &____b_5; }
	inline void set__b_5(int16_t value)
	{
		____b_5 = value;
	}

	inline static int32_t get_offset_of__c_6() { return static_cast<int32_t>(offsetof(Guid_t, ____c_6)); }
	inline int16_t get__c_6() const { return ____c_6; }
	inline int16_t* get_address_of__c_6() { return &____c_6; }
	inline void set__c_6(int16_t value)
	{
		____c_6 = value;
	}

	inline static int32_t get_offset_of__d_7() { return static_cast<int32_t>(offsetof(Guid_t, ____d_7)); }
	inline uint8_t get__d_7() const { return ____d_7; }
	inline uint8_t* get_address_of__d_7() { return &____d_7; }
	inline void set__d_7(uint8_t value)
	{
		____d_7 = value;
	}

	inline static int32_t get_offset_of__e_8() { return static_cast<int32_t>(offsetof(Guid_t, ____e_8)); }
	inline uint8_t get__e_8() const { return ____e_8; }
	inline uint8_t* get_address_of__e_8() { return &____e_8; }
	inline void set__e_8(uint8_t value)
	{
		____e_8 = value;
	}

	inline static int32_t get_offset_of__f_9() { return static_cast<int32_t>(offsetof(Guid_t, ____f_9)); }
	inline uint8_t get__f_9() const { return ____f_9; }
	inline uint8_t* get_address_of__f_9() { return &____f_9; }
	inline void set__f_9(uint8_t value)
	{
		____f_9 = value;
	}

	inline static int32_t get_offset_of__g_10() { return static_cast<int32_t>(offsetof(Guid_t, ____g_10)); }
	inline uint8_t get__g_10() const { return ____g_10; }
	inline uint8_t* get_address_of__g_10() { return &____g_10; }
	inline void set__g_10(uint8_t value)
	{
		____g_10 = value;
	}

	inline static int32_t get_offset_of__h_11() { return static_cast<int32_t>(offsetof(Guid_t, ____h_11)); }
	inline uint8_t get__h_11() const { return ____h_11; }
	inline uint8_t* get_address_of__h_11() { return &____h_11; }
	inline void set__h_11(uint8_t value)
	{
		____h_11 = value;
	}

	inline static int32_t get_offset_of__i_12() { return static_cast<int32_t>(offsetof(Guid_t, ____i_12)); }
	inline uint8_t get__i_12() const { return ____i_12; }
	inline uint8_t* get_address_of__i_12() { return &____i_12; }
	inline void set__i_12(uint8_t value)
	{
		____i_12 = value;
	}

	inline static int32_t get_offset_of__j_13() { return static_cast<int32_t>(offsetof(Guid_t, ____j_13)); }
	inline uint8_t get__j_13() const { return ____j_13; }
	inline uint8_t* get_address_of__j_13() { return &____j_13; }
	inline void set__j_13(uint8_t value)
	{
		____j_13 = value;
	}

	inline static int32_t get_offset_of__k_14() { return static_cast<int32_t>(offsetof(Guid_t, ____k_14)); }
	inline uint8_t get__k_14() const { return ____k_14; }
	inline uint8_t* get_address_of__k_14() { return &____k_14; }
	inline void set__k_14(uint8_t value)
	{
		____k_14 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_0;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_1;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_2;
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_3;

public:
	inline static int32_t get_offset_of__rngAccess_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_0)); }
	inline RuntimeObject * get__rngAccess_0() const { return ____rngAccess_0; }
	inline RuntimeObject ** get_address_of__rngAccess_0() { return &____rngAccess_0; }
	inline void set__rngAccess_0(RuntimeObject * value)
	{
		____rngAccess_0 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_0), value);
	}

	inline static int32_t get_offset_of__rng_1() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_1)); }
	inline RandomNumberGenerator_t386037858 * get__rng_1() const { return ____rng_1; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_1() { return &____rng_1; }
	inline void set__rng_1(RandomNumberGenerator_t386037858 * value)
	{
		____rng_1 = value;
		Il2CppCodeGenWriteBarrier((&____rng_1), value);
	}

	inline static int32_t get_offset_of__fastRng_2() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_2)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_2() const { return ____fastRng_2; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_2() { return &____fastRng_2; }
	inline void set__fastRng_2(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_2 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_2), value);
	}

	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_3)); }
	inline Guid_t  get_Empty_3() const { return ___Empty_3; }
	inline Guid_t * get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(Guid_t  value)
	{
		___Empty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef EVENTREGISTRATIONTOKEN_T318890788_H
#define EVENTREGISTRATIONTOKEN_T318890788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken
struct  EventRegistrationToken_t318890788 
{
public:
	// System.UInt64 System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(EventRegistrationToken_t318890788, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTREGISTRATIONTOKEN_T318890788_H
#ifndef SLOT_T3189409122_H
#define SLOT_T3189409122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Set`1/Slot<System.Char>
struct  Slot_t3189409122 
{
public:
	// System.Int32 System.Linq.Set`1/Slot::_hashCode
	int32_t ____hashCode_0;
	// System.Int32 System.Linq.Set`1/Slot::_next
	int32_t ____next_1;
	// TElement System.Linq.Set`1/Slot::_value
	Il2CppChar ____value_2;

public:
	inline static int32_t get_offset_of__hashCode_0() { return static_cast<int32_t>(offsetof(Slot_t3189409122, ____hashCode_0)); }
	inline int32_t get__hashCode_0() const { return ____hashCode_0; }
	inline int32_t* get_address_of__hashCode_0() { return &____hashCode_0; }
	inline void set__hashCode_0(int32_t value)
	{
		____hashCode_0 = value;
	}

	inline static int32_t get_offset_of__next_1() { return static_cast<int32_t>(offsetof(Slot_t3189409122, ____next_1)); }
	inline int32_t get__next_1() const { return ____next_1; }
	inline int32_t* get_address_of__next_1() { return &____next_1; }
	inline void set__next_1(int32_t value)
	{
		____next_1 = value;
	}

	inline static int32_t get_offset_of__value_2() { return static_cast<int32_t>(offsetof(Slot_t3189409122, ____value_2)); }
	inline Il2CppChar get__value_2() const { return ____value_2; }
	inline Il2CppChar* get_address_of__value_2() { return &____value_2; }
	inline void set__value_2(Il2CppChar value)
	{
		____value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOT_T3189409122_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef EVENTCACHEENTRY_T156445199_H
#define EVENTCACHEENTRY_T156445199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry
struct  EventCacheEntry_t156445199 
{
public:
	// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventRegistrationTokenListWithCount> System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry::registrationTable
	ConditionalWeakTable_2_t3044373657 * ___registrationTable_0;
	// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/TokenListCount System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry::tokenListCount
	TokenListCount_t1606756367 * ___tokenListCount_1;

public:
	inline static int32_t get_offset_of_registrationTable_0() { return static_cast<int32_t>(offsetof(EventCacheEntry_t156445199, ___registrationTable_0)); }
	inline ConditionalWeakTable_2_t3044373657 * get_registrationTable_0() const { return ___registrationTable_0; }
	inline ConditionalWeakTable_2_t3044373657 ** get_address_of_registrationTable_0() { return &___registrationTable_0; }
	inline void set_registrationTable_0(ConditionalWeakTable_2_t3044373657 * value)
	{
		___registrationTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___registrationTable_0), value);
	}

	inline static int32_t get_offset_of_tokenListCount_1() { return static_cast<int32_t>(offsetof(EventCacheEntry_t156445199, ___tokenListCount_1)); }
	inline TokenListCount_t1606756367 * get_tokenListCount_1() const { return ___tokenListCount_1; }
	inline TokenListCount_t1606756367 ** get_address_of_tokenListCount_1() { return &___tokenListCount_1; }
	inline void set_tokenListCount_1(TokenListCount_t1606756367 * value)
	{
		___tokenListCount_1 = value;
		Il2CppCodeGenWriteBarrier((&___tokenListCount_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry
struct EventCacheEntry_t156445199_marshaled_pinvoke
{
	ConditionalWeakTable_2_t3044373657 * ___registrationTable_0;
	TokenListCount_t1606756367 * ___tokenListCount_1;
};
// Native definition for COM marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry
struct EventCacheEntry_t156445199_marshaled_com
{
	ConditionalWeakTable_2_t3044373657 * ___registrationTable_0;
	TokenListCount_t1606756367 * ___tokenListCount_1;
};
#endif // EVENTCACHEENTRY_T156445199_H
#ifndef INTERNALENCODINGDATAITEM_T3158859817_H
#define INTERNALENCODINGDATAITEM_T3158859817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.InternalEncodingDataItem
struct  InternalEncodingDataItem_t3158859817 
{
public:
	// System.String System.Globalization.InternalEncodingDataItem::webName
	String_t* ___webName_0;
	// System.UInt16 System.Globalization.InternalEncodingDataItem::codePage
	uint16_t ___codePage_1;

public:
	inline static int32_t get_offset_of_webName_0() { return static_cast<int32_t>(offsetof(InternalEncodingDataItem_t3158859817, ___webName_0)); }
	inline String_t* get_webName_0() const { return ___webName_0; }
	inline String_t** get_address_of_webName_0() { return &___webName_0; }
	inline void set_webName_0(String_t* value)
	{
		___webName_0 = value;
		Il2CppCodeGenWriteBarrier((&___webName_0), value);
	}

	inline static int32_t get_offset_of_codePage_1() { return static_cast<int32_t>(offsetof(InternalEncodingDataItem_t3158859817, ___codePage_1)); }
	inline uint16_t get_codePage_1() const { return ___codePage_1; }
	inline uint16_t* get_address_of_codePage_1() { return &___codePage_1; }
	inline void set_codePage_1(uint16_t value)
	{
		___codePage_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Globalization.InternalEncodingDataItem
struct InternalEncodingDataItem_t3158859817_marshaled_pinvoke
{
	char* ___webName_0;
	uint16_t ___codePage_1;
};
// Native definition for COM marshalling of System.Globalization.InternalEncodingDataItem
struct InternalEncodingDataItem_t3158859817_marshaled_com
{
	Il2CppChar* ___webName_0;
	uint16_t ___codePage_1;
};
#endif // INTERNALENCODINGDATAITEM_T3158859817_H
#ifndef EPHEMERON_T1602596362_H
#define EPHEMERON_T1602596362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.Ephemeron
struct  Ephemeron_t1602596362 
{
public:
	// System.Object System.Runtime.CompilerServices.Ephemeron::key
	RuntimeObject * ___key_0;
	// System.Object System.Runtime.CompilerServices.Ephemeron::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(Ephemeron_t1602596362, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(Ephemeron_t1602596362, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.Ephemeron
struct Ephemeron_t1602596362_marshaled_pinvoke
{
	Il2CppIUnknown* ___key_0;
	Il2CppIUnknown* ___value_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.Ephemeron
struct Ephemeron_t1602596362_marshaled_com
{
	Il2CppIUnknown* ___key_0;
	Il2CppIUnknown* ___value_1;
};
#endif // EPHEMERON_T1602596362_H
#ifndef TYPEDCONSTANT_T714020897_H
#define TYPEDCONSTANT_T714020897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Compiler.BoundConstants/TypedConstant
struct  TypedConstant_t714020897 
{
public:
	// System.Object System.Linq.Expressions.Compiler.BoundConstants/TypedConstant::Value
	RuntimeObject * ___Value_0;
	// System.Type System.Linq.Expressions.Compiler.BoundConstants/TypedConstant::Type
	Type_t * ___Type_1;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(TypedConstant_t714020897, ___Value_0)); }
	inline RuntimeObject * get_Value_0() const { return ___Value_0; }
	inline RuntimeObject ** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(RuntimeObject * value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((&___Value_0), value);
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(TypedConstant_t714020897, ___Type_1)); }
	inline Type_t * get_Type_1() const { return ___Type_1; }
	inline Type_t ** get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(Type_t * value)
	{
		___Type_1 = value;
		Il2CppCodeGenWriteBarrier((&___Type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Linq.Expressions.Compiler.BoundConstants/TypedConstant
struct TypedConstant_t714020897_marshaled_pinvoke
{
	Il2CppIUnknown* ___Value_0;
	Type_t * ___Type_1;
};
// Native definition for COM marshalling of System.Linq.Expressions.Compiler.BoundConstants/TypedConstant
struct TypedConstant_t714020897_marshaled_com
{
	Il2CppIUnknown* ___Value_0;
	Type_t * ___Type_1;
};
#endif // TYPEDCONSTANT_T714020897_H
#ifndef RESOLVERCONTRACTKEY_T3292851287_H
#define RESOLVERCONTRACTKEY_T3292851287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ResolverContractKey
struct  ResolverContractKey_t3292851287 
{
public:
	// System.Type Newtonsoft.Json.Serialization.ResolverContractKey::_resolverType
	Type_t * ____resolverType_0;
	// System.Type Newtonsoft.Json.Serialization.ResolverContractKey::_contractType
	Type_t * ____contractType_1;

public:
	inline static int32_t get_offset_of__resolverType_0() { return static_cast<int32_t>(offsetof(ResolverContractKey_t3292851287, ____resolverType_0)); }
	inline Type_t * get__resolverType_0() const { return ____resolverType_0; }
	inline Type_t ** get_address_of__resolverType_0() { return &____resolverType_0; }
	inline void set__resolverType_0(Type_t * value)
	{
		____resolverType_0 = value;
		Il2CppCodeGenWriteBarrier((&____resolverType_0), value);
	}

	inline static int32_t get_offset_of__contractType_1() { return static_cast<int32_t>(offsetof(ResolverContractKey_t3292851287, ____contractType_1)); }
	inline Type_t * get__contractType_1() const { return ____contractType_1; }
	inline Type_t ** get_address_of__contractType_1() { return &____contractType_1; }
	inline void set__contractType_1(Type_t * value)
	{
		____contractType_1 = value;
		Il2CppCodeGenWriteBarrier((&____contractType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Serialization.ResolverContractKey
struct ResolverContractKey_t3292851287_marshaled_pinvoke
{
	Type_t * ____resolverType_0;
	Type_t * ____contractType_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Serialization.ResolverContractKey
struct ResolverContractKey_t3292851287_marshaled_com
{
	Type_t * ____resolverType_0;
	Type_t * ____contractType_1;
};
#endif // RESOLVERCONTRACTKEY_T3292851287_H
#ifndef SLOT_T2635054816_H
#define SLOT_T2635054816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Set`1/Slot<System.Object>
struct  Slot_t2635054816 
{
public:
	// System.Int32 System.Linq.Set`1/Slot::_hashCode
	int32_t ____hashCode_0;
	// System.Int32 System.Linq.Set`1/Slot::_next
	int32_t ____next_1;
	// TElement System.Linq.Set`1/Slot::_value
	RuntimeObject * ____value_2;

public:
	inline static int32_t get_offset_of__hashCode_0() { return static_cast<int32_t>(offsetof(Slot_t2635054816, ____hashCode_0)); }
	inline int32_t get__hashCode_0() const { return ____hashCode_0; }
	inline int32_t* get_address_of__hashCode_0() { return &____hashCode_0; }
	inline void set__hashCode_0(int32_t value)
	{
		____hashCode_0 = value;
	}

	inline static int32_t get_offset_of__next_1() { return static_cast<int32_t>(offsetof(Slot_t2635054816, ____next_1)); }
	inline int32_t get__next_1() const { return ____next_1; }
	inline int32_t* get_address_of__next_1() { return &____next_1; }
	inline void set__next_1(int32_t value)
	{
		____next_1 = value;
	}

	inline static int32_t get_offset_of__value_2() { return static_cast<int32_t>(offsetof(Slot_t2635054816, ____value_2)); }
	inline RuntimeObject * get__value_2() const { return ____value_2; }
	inline RuntimeObject ** get_address_of__value_2() { return &____value_2; }
	inline void set__value_2(RuntimeObject * value)
	{
		____value_2 = value;
		Il2CppCodeGenWriteBarrier((&____value_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOT_T2635054816_H
#ifndef GCHANDLE_T3351438187_H
#define GCHANDLE_T3351438187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t3351438187 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t3351438187, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T3351438187_H
#ifndef EVENTREGISTRATIONTOKENLIST_T3288506496_H
#define EVENTREGISTRATIONTOKENLIST_T3288506496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList
struct  EventRegistrationTokenList_t3288506496 
{
public:
	// System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList::firstToken
	EventRegistrationToken_t318890788  ___firstToken_0;
	// System.Collections.Generic.List`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken> System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList::restTokens
	List_1_t1790965530 * ___restTokens_1;

public:
	inline static int32_t get_offset_of_firstToken_0() { return static_cast<int32_t>(offsetof(EventRegistrationTokenList_t3288506496, ___firstToken_0)); }
	inline EventRegistrationToken_t318890788  get_firstToken_0() const { return ___firstToken_0; }
	inline EventRegistrationToken_t318890788 * get_address_of_firstToken_0() { return &___firstToken_0; }
	inline void set_firstToken_0(EventRegistrationToken_t318890788  value)
	{
		___firstToken_0 = value;
	}

	inline static int32_t get_offset_of_restTokens_1() { return static_cast<int32_t>(offsetof(EventRegistrationTokenList_t3288506496, ___restTokens_1)); }
	inline List_1_t1790965530 * get_restTokens_1() const { return ___restTokens_1; }
	inline List_1_t1790965530 ** get_address_of_restTokens_1() { return &___restTokens_1; }
	inline void set_restTokens_1(List_1_t1790965530 * value)
	{
		___restTokens_1 = value;
		Il2CppCodeGenWriteBarrier((&___restTokens_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList
struct EventRegistrationTokenList_t3288506496_marshaled_pinvoke
{
	EventRegistrationToken_t318890788  ___firstToken_0;
	List_1_t1790965530 * ___restTokens_1;
};
// Native definition for COM marshalling of System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList
struct EventRegistrationTokenList_t3288506496_marshaled_com
{
	EventRegistrationToken_t318890788  ___firstToken_0;
	List_1_t1790965530 * ___restTokens_1;
};
#endif // EVENTREGISTRATIONTOKENLIST_T3288506496_H
#ifndef SWITCHVALUESTATE_T2805251467_H
#define SWITCHVALUESTATE_T2805251467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AppContext/SwitchValueState
struct  SwitchValueState_t2805251467 
{
public:
	// System.Int32 System.AppContext/SwitchValueState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SwitchValueState_t2805251467, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCHVALUESTATE_T2805251467_H
#ifndef RESOURCEATTRIBUTES_T3997964906_H
#define RESOURCEATTRIBUTES_T3997964906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ResourceAttributes
struct  ResourceAttributes_t3997964906 
{
public:
	// System.Int32 System.Reflection.ResourceAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ResourceAttributes_t3997964906, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCEATTRIBUTES_T3997964906_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_19)); }
	inline TimeSpan_t881159249  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_t881159249 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_t881159249  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_t881159249  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_t881159249  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_21)); }
	inline TimeSpan_t881159249  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_t881159249  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef SECURITYACTION_T569814076_H
#define SECURITYACTION_T569814076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Permissions.SecurityAction
struct  SecurityAction_t569814076 
{
public:
	// System.Int32 System.Security.Permissions.SecurityAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SecurityAction_t569814076, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYACTION_T569814076_H
#ifndef CANCELLATIONTOKENREGISTRATION_T2813424904_H
#define CANCELLATIONTOKENREGISTRATION_T2813424904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.CancellationTokenRegistration
struct  CancellationTokenRegistration_t2813424904 
{
public:
	// System.Threading.CancellationCallbackInfo System.Threading.CancellationTokenRegistration::m_callbackInfo
	CancellationCallbackInfo_t322720759 * ___m_callbackInfo_0;
	// System.Threading.SparselyPopulatedArrayAddInfo`1<System.Threading.CancellationCallbackInfo> System.Threading.CancellationTokenRegistration::m_registrationInfo
	SparselyPopulatedArrayAddInfo_1_t223515617  ___m_registrationInfo_1;

public:
	inline static int32_t get_offset_of_m_callbackInfo_0() { return static_cast<int32_t>(offsetof(CancellationTokenRegistration_t2813424904, ___m_callbackInfo_0)); }
	inline CancellationCallbackInfo_t322720759 * get_m_callbackInfo_0() const { return ___m_callbackInfo_0; }
	inline CancellationCallbackInfo_t322720759 ** get_address_of_m_callbackInfo_0() { return &___m_callbackInfo_0; }
	inline void set_m_callbackInfo_0(CancellationCallbackInfo_t322720759 * value)
	{
		___m_callbackInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_callbackInfo_0), value);
	}

	inline static int32_t get_offset_of_m_registrationInfo_1() { return static_cast<int32_t>(offsetof(CancellationTokenRegistration_t2813424904, ___m_registrationInfo_1)); }
	inline SparselyPopulatedArrayAddInfo_1_t223515617  get_m_registrationInfo_1() const { return ___m_registrationInfo_1; }
	inline SparselyPopulatedArrayAddInfo_1_t223515617 * get_address_of_m_registrationInfo_1() { return &___m_registrationInfo_1; }
	inline void set_m_registrationInfo_1(SparselyPopulatedArrayAddInfo_1_t223515617  value)
	{
		___m_registrationInfo_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Threading.CancellationTokenRegistration
struct CancellationTokenRegistration_t2813424904_marshaled_pinvoke
{
	CancellationCallbackInfo_t322720759 * ___m_callbackInfo_0;
	SparselyPopulatedArrayAddInfo_1_t223515617  ___m_registrationInfo_1;
};
// Native definition for COM marshalling of System.Threading.CancellationTokenRegistration
struct CancellationTokenRegistration_t2813424904_marshaled_com
{
	CancellationCallbackInfo_t322720759 * ___m_callbackInfo_0;
	SparselyPopulatedArrayAddInfo_1_t223515617  ___m_registrationInfo_1;
};
#endif // CANCELLATIONTOKENREGISTRATION_T2813424904_H
#ifndef REGEXOPTIONS_T92845595_H
#define REGEXOPTIONS_T92845595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexOptions
struct  RegexOptions_t92845595 
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RegexOptions_t92845595, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXOPTIONS_T92845595_H
#ifndef KEYVALUEPAIR_2_T1048133692_H
#define KEYVALUEPAIR_2_T1048133692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>
struct  KeyValuePair_2_t1048133692 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	TimerData_t4056715490  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1048133692, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1048133692, ___value_1)); }
	inline TimerData_t4056715490  get_value_1() const { return ___value_1; }
	inline TimerData_t4056715490 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(TimerData_t4056715490  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1048133692_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef BINARYTYPEENUM_T3485436454_H
#define BINARYTYPEENUM_T3485436454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum
struct  BinaryTypeEnum_t3485436454 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BinaryTypeEnum_t3485436454, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYTYPEENUM_T3485436454_H
#ifndef KEYVALUEPAIR_2_T753628355_H
#define KEYVALUEPAIR_2_T753628355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct  KeyValuePair_2_t753628355 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	TypeConvertKey_t285306760  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t753628355, ___key_0)); }
	inline TypeConvertKey_t285306760  get_key_0() const { return ___key_0; }
	inline TypeConvertKey_t285306760 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(TypeConvertKey_t285306760  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t753628355, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T753628355_H
#ifndef KEYEVENT_T245959883_H
#define KEYEVENT_T245959883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.KeyboardManager/KeyEvent
struct  KeyEvent_t245959883 
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.KeyboardManager/KeyEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyEvent_t245959883, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEVENT_T245959883_H
#ifndef KEYVALUEPAIR_2_T231828568_H
#define KEYVALUEPAIR_2_T231828568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>
struct  KeyValuePair_2_t231828568 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Guid_t  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t231828568, ___key_0)); }
	inline Guid_t  get_key_0() const { return ___key_0; }
	inline Guid_t * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Guid_t  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t231828568, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T231828568_H
#ifndef KEYVALUEPAIR_2_T644712158_H
#define KEYVALUEPAIR_2_T644712158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct  KeyValuePair_2_t644712158 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	TypeNameKey_t2985541961  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t644712158, ___key_0)); }
	inline TypeNameKey_t2985541961  get_key_0() const { return ___key_0; }
	inline TypeNameKey_t2985541961 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(TypeNameKey_t2985541961  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t644712158, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T644712158_H
#ifndef X509CHAINSTATUSFLAGS_T1026973125_H
#define X509CHAINSTATUSFLAGS_T1026973125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
struct  X509ChainStatusFlags_t1026973125 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509ChainStatusFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509ChainStatusFlags_t1026973125, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINSTATUSFLAGS_T1026973125_H
#ifndef KEYVALUEPAIR_2_T2872605199_H
#define KEYVALUEPAIR_2_T2872605199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>
struct  KeyValuePair_2_t2872605199 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	XPathNodeRef_t3498189018  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	XPathNodeRef_t3498189018  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2872605199, ___key_0)); }
	inline XPathNodeRef_t3498189018  get_key_0() const { return ___key_0; }
	inline XPathNodeRef_t3498189018 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(XPathNodeRef_t3498189018  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2872605199, ___value_1)); }
	inline XPathNodeRef_t3498189018  get_value_1() const { return ___value_1; }
	inline XPathNodeRef_t3498189018 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(XPathNodeRef_t3498189018  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2872605199_H
#ifndef INTERNALPRIMITIVETYPEE_T4093048977_H
#define INTERNALPRIMITIVETYPEE_T4093048977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE
struct  InternalPrimitiveTypeE_t4093048977 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InternalPrimitiveTypeE_t4093048977, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALPRIMITIVETYPEE_T4093048977_H
#ifndef TERMINFOSTRINGS_T290279955_H
#define TERMINFOSTRINGS_T290279955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TermInfoStrings
struct  TermInfoStrings_t290279955 
{
public:
	// System.Int32 System.TermInfoStrings::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TermInfoStrings_t290279955, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERMINFOSTRINGS_T290279955_H
#ifndef KEYVALUEPAIR_2_T3267901400_H
#define KEYVALUEPAIR_2_T3267901400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>
struct  KeyValuePair_2_t3267901400 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	ResolverContractKey_t3292851287  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3267901400, ___key_0)); }
	inline ResolverContractKey_t3292851287  get_key_0() const { return ___key_0; }
	inline ResolverContractKey_t3292851287 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(ResolverContractKey_t3292851287  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3267901400, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3267901400_H
#ifndef EVENTFILTER_T2065690828_H
#define EVENTFILTER_T2065690828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.EventFilter
struct  EventFilter_t2065690828 
{
public:
	// System.Int16 System.IO.EventFilter::value__
	int16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventFilter_t2065690828, ___value___2)); }
	inline int16_t get_value___2() const { return ___value___2; }
	inline int16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFILTER_T2065690828_H
#ifndef RFCCHAR_T2905409930_H
#define RFCCHAR_T2905409930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebHeaderCollection/RfcChar
struct  RfcChar_t2905409930 
{
public:
	// System.Byte System.Net.WebHeaderCollection/RfcChar::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RfcChar_t2905409930, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RFCCHAR_T2905409930_H
#ifndef WSABUF_T1998059390_H
#define WSABUF_T1998059390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.Socket/WSABUF
struct  WSABUF_t1998059390 
{
public:
	// System.Int32 System.Net.Sockets.Socket/WSABUF::len
	int32_t ___len_0;
	// System.IntPtr System.Net.Sockets.Socket/WSABUF::buf
	intptr_t ___buf_1;

public:
	inline static int32_t get_offset_of_len_0() { return static_cast<int32_t>(offsetof(WSABUF_t1998059390, ___len_0)); }
	inline int32_t get_len_0() const { return ___len_0; }
	inline int32_t* get_address_of_len_0() { return &___len_0; }
	inline void set_len_0(int32_t value)
	{
		___len_0 = value;
	}

	inline static int32_t get_offset_of_buf_1() { return static_cast<int32_t>(offsetof(WSABUF_t1998059390, ___buf_1)); }
	inline intptr_t get_buf_1() const { return ___buf_1; }
	inline intptr_t* get_address_of_buf_1() { return &___buf_1; }
	inline void set_buf_1(intptr_t value)
	{
		___buf_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WSABUF_T1998059390_H
#ifndef EVENTFLAGS_T1670942752_H
#define EVENTFLAGS_T1670942752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.EventFlags
struct  EventFlags_t1670942752 
{
public:
	// System.UInt16 System.IO.EventFlags::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventFlags_t1670942752, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFLAGS_T1670942752_H
#ifndef MATERIALTYPE_T597773032_H
#define MATERIALTYPE_T597773032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter/MaterialType
struct  MaterialType_t597773032 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter/MaterialType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaterialType_t597773032, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALTYPE_T597773032_H
#ifndef KEYVALUEPAIR_2_T1405028755_H
#define KEYVALUEPAIR_2_T1405028755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>
struct  KeyValuePair_2_t1405028755 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Vector3_t3722313464  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1405028755, ___key_0)); }
	inline Vector3_t3722313464  get_key_0() const { return ___key_0; }
	inline Vector3_t3722313464 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Vector3_t3722313464  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1405028755, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1405028755_H
#ifndef TTT_T2628677491_H
#define TTT_T2628677491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.TimeSpanParse/TTT
struct  TTT_t2628677491 
{
public:
	// System.Int32 System.Globalization.TimeSpanParse/TTT::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TTT_t2628677491, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TTT_T2628677491_H
#ifndef READTYPE_T340786580_H
#define READTYPE_T340786580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReadType
struct  ReadType_t340786580 
{
public:
	// System.Int32 Newtonsoft.Json.ReadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReadType_t340786580, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READTYPE_T340786580_H
#ifndef HS_T3339773016_H
#define HS_T3339773016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.HebrewNumber/HS
struct  HS_t3339773016 
{
public:
	// System.Int32 System.Globalization.HebrewNumber/HS::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HS_t3339773016, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HS_T3339773016_H
#ifndef PRIMITIVETYPECODE_T798949904_H
#define PRIMITIVETYPECODE_T798949904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PrimitiveTypeCode
struct  PrimitiveTypeCode_t798949904 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PrimitiveTypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PrimitiveTypeCode_t798949904, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPECODE_T798949904_H
#ifndef VARIABLESTORAGEKIND_T3280289589_H
#define VARIABLESTORAGEKIND_T3280289589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Compiler.VariableStorageKind
struct  VariableStorageKind_t3280289589 
{
public:
	// System.Int32 System.Linq.Expressions.Compiler.VariableStorageKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VariableStorageKind_t3280289589, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLESTORAGEKIND_T3280289589_H
#ifndef DATETIMEOFFSET_T3229287507_H
#define DATETIMEOFFSET_T3229287507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeOffset
struct  DateTimeOffset_t3229287507 
{
public:
	// System.DateTime System.DateTimeOffset::m_dateTime
	DateTime_t3738529785  ___m_dateTime_2;
	// System.Int16 System.DateTimeOffset::m_offsetMinutes
	int16_t ___m_offsetMinutes_3;

public:
	inline static int32_t get_offset_of_m_dateTime_2() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507, ___m_dateTime_2)); }
	inline DateTime_t3738529785  get_m_dateTime_2() const { return ___m_dateTime_2; }
	inline DateTime_t3738529785 * get_address_of_m_dateTime_2() { return &___m_dateTime_2; }
	inline void set_m_dateTime_2(DateTime_t3738529785  value)
	{
		___m_dateTime_2 = value;
	}

	inline static int32_t get_offset_of_m_offsetMinutes_3() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507, ___m_offsetMinutes_3)); }
	inline int16_t get_m_offsetMinutes_3() const { return ___m_offsetMinutes_3; }
	inline int16_t* get_address_of_m_offsetMinutes_3() { return &___m_offsetMinutes_3; }
	inline void set_m_offsetMinutes_3(int16_t value)
	{
		___m_offsetMinutes_3 = value;
	}
};

struct DateTimeOffset_t3229287507_StaticFields
{
public:
	// System.DateTimeOffset System.DateTimeOffset::MinValue
	DateTimeOffset_t3229287507  ___MinValue_0;
	// System.DateTimeOffset System.DateTimeOffset::MaxValue
	DateTimeOffset_t3229287507  ___MaxValue_1;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507_StaticFields, ___MinValue_0)); }
	inline DateTimeOffset_t3229287507  get_MinValue_0() const { return ___MinValue_0; }
	inline DateTimeOffset_t3229287507 * get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(DateTimeOffset_t3229287507  value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507_StaticFields, ___MaxValue_1)); }
	inline DateTimeOffset_t3229287507  get_MaxValue_1() const { return ___MaxValue_1; }
	inline DateTimeOffset_t3229287507 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(DateTimeOffset_t3229287507  value)
	{
		___MaxValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEOFFSET_T3229287507_H
#ifndef DS_T2232270370_H
#define DS_T2232270370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeParse/DS
struct  DS_t2232270370 
{
public:
	// System.Int32 System.DateTimeParse/DS::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DS_t2232270370, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DS_T2232270370_H
#ifndef KEYVALUEPAIR_2_T1297193679_H
#define KEYVALUEPAIR_2_T1297193679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>
struct  KeyValuePair_2_t1297193679 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	uint32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	XboxControllerData_t1920221146  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1297193679, ___key_0)); }
	inline uint32_t get_key_0() const { return ___key_0; }
	inline uint32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(uint32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1297193679, ___value_1)); }
	inline XboxControllerData_t1920221146  get_value_1() const { return ___value_1; }
	inline XboxControllerData_t1920221146 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(XboxControllerData_t1920221146  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1297193679_H
#ifndef KEYVALUEPAIR_2_T255164838_H
#define KEYVALUEPAIR_2_T255164838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>
struct  KeyValuePair_2_t255164838 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	TypedConstant_t714020897  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t255164838, ___key_0)); }
	inline TypedConstant_t714020897  get_key_0() const { return ___key_0; }
	inline TypedConstant_t714020897 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(TypedConstant_t714020897  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t255164838, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T255164838_H
#ifndef OPERATIONALSTATUS_T2709089529_H
#define OPERATIONALSTATUS_T2709089529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkInformation.OperationalStatus
struct  OperationalStatus_t2709089529 
{
public:
	// System.Int32 System.Net.NetworkInformation.OperationalStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OperationalStatus_t2709089529, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATIONALSTATUS_T2709089529_H
#ifndef NETWORKINTERFACETYPE_T616418749_H
#define NETWORKINTERFACETYPE_T616418749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkInformation.NetworkInterfaceType
struct  NetworkInterfaceType_t616418749 
{
public:
	// System.Int32 System.Net.NetworkInformation.NetworkInterfaceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NetworkInterfaceType_t616418749, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKINTERFACETYPE_T616418749_H
#ifndef TYPECODE_T2987224087_H
#define TYPECODE_T2987224087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TypeCode
struct  TypeCode_t2987224087 
{
public:
	// System.Int32 System.TypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeCode_t2987224087, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPECODE_T2987224087_H
#ifndef KEYVALUEPAIR_2_T3699644050_H
#define KEYVALUEPAIR_2_T3699644050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>
struct  KeyValuePair_2_t3699644050 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	intptr_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3699644050, ___key_0)); }
	inline intptr_t get_key_0() const { return ___key_0; }
	inline intptr_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(intptr_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3699644050, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3699644050_H
#ifndef KEYVALUEPAIR_2_T126004427_H
#define KEYVALUEPAIR_2_T126004427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>
struct  KeyValuePair_2_t126004427 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	TypedConstant_t714020897  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t126004427, ___key_0)); }
	inline TypedConstant_t714020897  get_key_0() const { return ___key_0; }
	inline TypedConstant_t714020897 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(TypedConstant_t714020897  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t126004427, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T126004427_H
#ifndef ILEXCEPTIONINFO_T237856010_H
#define ILEXCEPTIONINFO_T237856010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.ILExceptionInfo
struct  ILExceptionInfo_t237856010 
{
public:
	// System.Reflection.Emit.ILExceptionBlock[] System.Reflection.Emit.ILExceptionInfo::handlers
	ILExceptionBlockU5BU5D_t2996808915* ___handlers_0;
	// System.Int32 System.Reflection.Emit.ILExceptionInfo::start
	int32_t ___start_1;
	// System.Int32 System.Reflection.Emit.ILExceptionInfo::len
	int32_t ___len_2;
	// System.Reflection.Emit.Label System.Reflection.Emit.ILExceptionInfo::end
	Label_t2281661643  ___end_3;

public:
	inline static int32_t get_offset_of_handlers_0() { return static_cast<int32_t>(offsetof(ILExceptionInfo_t237856010, ___handlers_0)); }
	inline ILExceptionBlockU5BU5D_t2996808915* get_handlers_0() const { return ___handlers_0; }
	inline ILExceptionBlockU5BU5D_t2996808915** get_address_of_handlers_0() { return &___handlers_0; }
	inline void set_handlers_0(ILExceptionBlockU5BU5D_t2996808915* value)
	{
		___handlers_0 = value;
		Il2CppCodeGenWriteBarrier((&___handlers_0), value);
	}

	inline static int32_t get_offset_of_start_1() { return static_cast<int32_t>(offsetof(ILExceptionInfo_t237856010, ___start_1)); }
	inline int32_t get_start_1() const { return ___start_1; }
	inline int32_t* get_address_of_start_1() { return &___start_1; }
	inline void set_start_1(int32_t value)
	{
		___start_1 = value;
	}

	inline static int32_t get_offset_of_len_2() { return static_cast<int32_t>(offsetof(ILExceptionInfo_t237856010, ___len_2)); }
	inline int32_t get_len_2() const { return ___len_2; }
	inline int32_t* get_address_of_len_2() { return &___len_2; }
	inline void set_len_2(int32_t value)
	{
		___len_2 = value;
	}

	inline static int32_t get_offset_of_end_3() { return static_cast<int32_t>(offsetof(ILExceptionInfo_t237856010, ___end_3)); }
	inline Label_t2281661643  get_end_3() const { return ___end_3; }
	inline Label_t2281661643 * get_address_of_end_3() { return &___end_3; }
	inline void set_end_3(Label_t2281661643  value)
	{
		___end_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Emit.ILExceptionInfo
struct ILExceptionInfo_t237856010_marshaled_pinvoke
{
	ILExceptionBlock_t3961874966_marshaled_pinvoke* ___handlers_0;
	int32_t ___start_1;
	int32_t ___len_2;
	Label_t2281661643  ___end_3;
};
// Native definition for COM marshalling of System.Reflection.Emit.ILExceptionInfo
struct ILExceptionInfo_t237856010_marshaled_com
{
	ILExceptionBlock_t3961874966_marshaled_com* ___handlers_0;
	int32_t ___start_1;
	int32_t ___len_2;
	Label_t2281661643  ___end_3;
};
#endif // ILEXCEPTIONINFO_T237856010_H
#ifndef FILTERFLAGS_T1388790893_H
#define FILTERFLAGS_T1388790893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FilterFlags
struct  FilterFlags_t1388790893 
{
public:
	// System.UInt32 System.IO.FilterFlags::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterFlags_t1388790893, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERFLAGS_T1388790893_H
#ifndef KEYVALUEPAIR_2_T4253942476_H
#define KEYVALUEPAIR_2_T4253942476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>
struct  KeyValuePair_2_t4253942476 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	EventCacheKey_t3133620722  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	EventCacheEntry_t156445199  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4253942476, ___key_0)); }
	inline EventCacheKey_t3133620722  get_key_0() const { return ___key_0; }
	inline EventCacheKey_t3133620722 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(EventCacheKey_t3133620722  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4253942476, ___value_1)); }
	inline EventCacheEntry_t156445199  get_value_1() const { return ___value_1; }
	inline EventCacheEntry_t156445199 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(EventCacheEntry_t156445199  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T4253942476_H
#ifndef CUSTOMATTRIBUTENAMEDARGUMENT_T287865710_H
#define CUSTOMATTRIBUTENAMEDARGUMENT_T287865710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeNamedArgument
struct  CustomAttributeNamedArgument_t287865710 
{
public:
	// System.Reflection.CustomAttributeTypedArgument System.Reflection.CustomAttributeNamedArgument::typedArgument
	CustomAttributeTypedArgument_t2723150157  ___typedArgument_0;
	// System.Reflection.MemberInfo System.Reflection.CustomAttributeNamedArgument::memberInfo
	MemberInfo_t * ___memberInfo_1;

public:
	inline static int32_t get_offset_of_typedArgument_0() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t287865710, ___typedArgument_0)); }
	inline CustomAttributeTypedArgument_t2723150157  get_typedArgument_0() const { return ___typedArgument_0; }
	inline CustomAttributeTypedArgument_t2723150157 * get_address_of_typedArgument_0() { return &___typedArgument_0; }
	inline void set_typedArgument_0(CustomAttributeTypedArgument_t2723150157  value)
	{
		___typedArgument_0 = value;
	}

	inline static int32_t get_offset_of_memberInfo_1() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t287865710, ___memberInfo_1)); }
	inline MemberInfo_t * get_memberInfo_1() const { return ___memberInfo_1; }
	inline MemberInfo_t ** get_address_of_memberInfo_1() { return &___memberInfo_1; }
	inline void set_memberInfo_1(MemberInfo_t * value)
	{
		___memberInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___memberInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t287865710_marshaled_pinvoke
{
	CustomAttributeTypedArgument_t2723150157_marshaled_pinvoke ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t287865710_marshaled_com
{
	CustomAttributeTypedArgument_t2723150157_marshaled_com ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
#endif // CUSTOMATTRIBUTENAMEDARGUMENT_T287865710_H
#ifndef COOKIEVARIANT_T1245073431_H
#define COOKIEVARIANT_T1245073431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieVariant
struct  CookieVariant_t1245073431 
{
public:
	// System.Int32 System.Net.CookieVariant::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CookieVariant_t1245073431, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIEVARIANT_T1245073431_H
#ifndef KEYVALUEPAIR_2_T3174081962_H
#define KEYVALUEPAIR_2_T3174081962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>
struct  KeyValuePair_2_t3174081962 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	ResourceLocator_t3723970807  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3174081962, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3174081962, ___value_1)); }
	inline ResourceLocator_t3723970807  get_value_1() const { return ___value_1; }
	inline ResourceLocator_t3723970807 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(ResourceLocator_t3723970807  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3174081962_H
#ifndef COOKIETOKEN_T385055808_H
#define COOKIETOKEN_T385055808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieToken
struct  CookieToken_t385055808 
{
public:
	// System.Int32 System.Net.CookieToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CookieToken_t385055808, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIETOKEN_T385055808_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T234838207_H
#define IITERATORTOIENUMERATORADAPTER_1_T234838207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Reflection.Emit.ILTokenInfo>
struct  IIteratorToIEnumeratorAdapter_1_t234838207  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	ILTokenInfo_t2325775114  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t234838207, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t234838207, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t234838207, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t234838207, ___current_3)); }
	inline ILTokenInfo_t2325775114  get_current_3() const { return ___current_3; }
	inline ILTokenInfo_t2325775114 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ILTokenInfo_t2325775114  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T234838207_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T190724736_H
#define IITERATORTOIENUMERATORADAPTER_1_T190724736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Reflection.Emit.Label>
struct  IIteratorToIEnumeratorAdapter_1_t190724736  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Label_t2281661643  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t190724736, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t190724736, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t190724736, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t190724736, ___current_3)); }
	inline Label_t2281661643  get_current_3() const { return ___current_3; }
	inline Label_t2281661643 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Label_t2281661643  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T190724736_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T439280412_H
#define IITERATORTOIENUMERATORADAPTER_1_T439280412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t439280412  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t2530217319  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t439280412, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t439280412, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t439280412, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t439280412, ___current_3)); }
	inline KeyValuePair_2_t2530217319  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2530217319 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2530217319  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T439280412_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3062532443_H
#define IITERATORTOIENUMERATORADAPTER_1_T3062532443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Reflection.Emit.ILGenerator/LabelFixup>
struct  IIteratorToIEnumeratorAdapter_1_t3062532443  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	LabelFixup_t858502054  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3062532443, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3062532443, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3062532443, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3062532443, ___current_3)); }
	inline LabelFixup_t858502054  get_current_3() const { return ___current_3; }
	inline LabelFixup_t858502054 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(LabelFixup_t858502054  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3062532443_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T366141790_H
#define IITERATORTOIENUMERATORADAPTER_1_T366141790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t366141790  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t2457078697  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t366141790, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t366141790, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t366141790, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t366141790, ___current_3)); }
	inline KeyValuePair_2_t2457078697  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2457078697 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2457078697  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T366141790_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1870938059_H
#define IITERATORTOIENUMERATORADAPTER_1_T1870938059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Reflection.Emit.ILExceptionBlock>
struct  IIteratorToIEnumeratorAdapter_1_t1870938059  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	ILExceptionBlock_t3961874966  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1870938059, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1870938059, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1870938059, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1870938059, ___current_3)); }
	inline ILExceptionBlock_t3961874966  get_current_3() const { return ___current_3; }
	inline ILExceptionBlock_t3961874966 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ILExceptionBlock_t3961874966  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1870938059_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2564197780_H
#define IITERATORTOIENUMERATORADAPTER_1_T2564197780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Reflection.Emit.ILGenerator/LabelData>
struct  IIteratorToIEnumeratorAdapter_1_t2564197780  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	LabelData_t360167391  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2564197780, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2564197780, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2564197780, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2564197780, ___current_3)); }
	inline LabelData_t360167391  get_current_3() const { return ___current_3; }
	inline LabelData_t360167391 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(LabelData_t360167391  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2564197780_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1633033900_H
#define IITERATORTOIENUMERATORADAPTER_1_T1633033900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Resources.ResourceLocator>
struct  IIteratorToIEnumeratorAdapter_1_t1633033900  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	ResourceLocator_t3723970807  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1633033900, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1633033900, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1633033900, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1633033900, ___current_3)); }
	inline ResourceLocator_t3723970807  get_current_3() const { return ___current_3; }
	inline ResourceLocator_t3723970807 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ResourceLocator_t3723970807  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1633033900_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3806626751_H
#define IITERATORTOIENUMERATORADAPTER_1_T3806626751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Runtime.CompilerServices.Ephemeron>
struct  IIteratorToIEnumeratorAdapter_1_t3806626751  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Ephemeron_t1602596362  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3806626751, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3806626751, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3806626751, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3806626751, ___current_3)); }
	inline Ephemeron_t1602596362  get_current_3() const { return ___current_3; }
	inline Ephemeron_t1602596362 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Ephemeron_t1602596362  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3806626751_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1260501280_H
#define IITERATORTOIENUMERATORADAPTER_1_T1260501280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Runtime.InteropServices.GCHandle>
struct  IIteratorToIEnumeratorAdapter_1_t1260501280  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	GCHandle_t3351438187  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1260501280, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1260501280, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1260501280, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1260501280, ___current_3)); }
	inline GCHandle_t3351438187  get_current_3() const { return ___current_3; }
	inline GCHandle_t3351438187 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(GCHandle_t3351438187  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1260501280_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3665724855_H
#define IITERATORTOIENUMERATORADAPTER_1_T3665724855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Reflection.ParameterModifier>
struct  IIteratorToIEnumeratorAdapter_1_t3665724855  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	ParameterModifier_t1461694466  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3665724855, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3665724855, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3665724855, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3665724855, ___current_3)); }
	inline ParameterModifier_t1461694466  get_current_3() const { return ___current_3; }
	inline ParameterModifier_t1461694466 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ParameterModifier_t1461694466  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3665724855_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T4108259872_H
#define IITERATORTOIENUMERATORADAPTER_1_T4108259872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Reflection.Emit.MonoWin32Resource>
struct  IIteratorToIEnumeratorAdapter_1_t4108259872  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	MonoWin32Resource_t1904229483  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4108259872, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4108259872, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4108259872, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4108259872, ___current_3)); }
	inline MonoWin32Resource_t1904229483  get_current_3() const { return ___current_3; }
	inline MonoWin32Resource_t1904229483 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(MonoWin32Resource_t1904229483  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T4108259872_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T310120001_H
#define IITERATORTOIENUMERATORADAPTER_1_T310120001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct  IIteratorToIEnumeratorAdapter_1_t310120001  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t2401056908  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t310120001, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t310120001, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t310120001, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t310120001, ___current_3)); }
	inline KeyValuePair_2_t2401056908  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2401056908 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2401056908  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T310120001_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T4269296245_H
#define IITERATORTOIENUMERATORADAPTER_1_T4269296245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Reflection.Emit.SequencePoint>
struct  IIteratorToIEnumeratorAdapter_1_t4269296245  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	SequencePoint_t2065265856  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4269296245, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4269296245, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4269296245, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4269296245, ___current_3)); }
	inline SequencePoint_t2065265856  get_current_3() const { return ___current_3; }
	inline SequencePoint_t2065265856 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(SequencePoint_t2065265856  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T4269296245_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T632213250_H
#define IITERATORTOIENUMERATORADAPTER_1_T632213250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Reflection.CustomAttributeTypedArgument>
struct  IIteratorToIEnumeratorAdapter_1_t632213250  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	CustomAttributeTypedArgument_t2723150157  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t632213250, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t632213250, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t632213250, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t632213250, ___current_3)); }
	inline CustomAttributeTypedArgument_t2723150157  get_current_3() const { return ___current_3; }
	inline CustomAttributeTypedArgument_t2723150157 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(CustomAttributeTypedArgument_t2723150157  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T632213250_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3044180570_H
#define IITERATORTOIENUMERATORADAPTER_1_T3044180570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.IntPtr>
struct  IIteratorToIEnumeratorAdapter_1_t3044180570  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	intptr_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3044180570, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3044180570, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3044180570, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3044180570, ___current_3)); }
	inline intptr_t get_current_3() const { return ___current_3; }
	inline intptr_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(intptr_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3044180570_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T857322473_H
#define IITERATORTOIENUMERATORADAPTER_1_T857322473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Decimal>
struct  IIteratorToIEnumeratorAdapter_1_t857322473  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Decimal_t2948259380  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t857322473, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t857322473, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t857322473, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t857322473, ___current_3)); }
	inline Decimal_t2948259380  get_current_3() const { return ___current_3; }
	inline Decimal_t2948259380 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Decimal_t2948259380  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T857322473_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2918051286_H
#define IITERATORTOIENUMERATORADAPTER_1_T2918051286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant>
struct  IIteratorToIEnumeratorAdapter_1_t2918051286  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	TypedConstant_t714020897  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2918051286, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2918051286, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2918051286, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2918051286, ___current_3)); }
	inline TypedConstant_t714020897  get_current_3() const { return ___current_3; }
	inline TypedConstant_t714020897 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(TypedConstant_t714020897  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2918051286_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1102595980_H
#define IITERATORTOIENUMERATORADAPTER_1_T1102595980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Guid>
struct  IIteratorToIEnumeratorAdapter_1_t1102595980  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Guid_t  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1102595980, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1102595980, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1102595980, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1102595980, ___current_3)); }
	inline Guid_t  get_current_3() const { return ___current_3; }
	inline Guid_t * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Guid_t  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1102595980_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1067922910_H
#define IITERATORTOIENUMERATORADAPTER_1_T1067922910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Globalization.InternalEncodingDataItem>
struct  IIteratorToIEnumeratorAdapter_1_t1067922910  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	InternalEncodingDataItem_t3158859817  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1067922910, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1067922910, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1067922910, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1067922910, ___current_3)); }
	inline InternalEncodingDataItem_t3158859817  get_current_3() const { return ___current_3; }
	inline InternalEncodingDataItem_t3158859817 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(InternalEncodingDataItem_t3158859817  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1067922910_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T484596026_H
#define IITERATORTOIENUMERATORADAPTER_1_T484596026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Globalization.InternalCodePageDataItem>
struct  IIteratorToIEnumeratorAdapter_1_t484596026  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	InternalCodePageDataItem_t2575532933  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t484596026, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t484596026, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t484596026, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t484596026, ___current_3)); }
	inline InternalCodePageDataItem_t2575532933  get_current_3() const { return ___current_3; }
	inline InternalCodePageDataItem_t2575532933 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(InternalCodePageDataItem_t2575532933  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T484596026_H
// Windows.Foundation.Collections.IIterator`1<System.Guid>
struct NOVTABLE IIterator_1_t1067676360 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m5372693(Guid_t * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3692598929(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2924594529(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3031797434(uint32_t ___items0ArraySize, Guid_t * ___items0, uint32_t* comReturnValue) = 0;
};
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2962162093_H
#define IITERATORTOIENUMERATORADAPTER_1_T2962162093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Hashtable/bucket>
struct  IIteratorToIEnumeratorAdapter_1_t2962162093  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	bucket_t758131704  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2962162093, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2962162093, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2962162093, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2962162093, ___current_3)); }
	inline bucket_t758131704  get_current_3() const { return ___current_3; }
	inline bucket_t758131704 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(bucket_t758131704  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2962162093_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T803840870_H
#define IITERATORTOIENUMERATORADAPTER_1_T803840870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Marker>
struct  IIteratorToIEnumeratorAdapter_1_t803840870  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Marker_t2894777777  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t803840870, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t803840870, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t803840870, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t803840870, ___current_3)); }
	inline Marker_t2894777777  get_current_3() const { return ___current_3; }
	inline Marker_t2894777777 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Marker_t2894777777  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T803840870_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2103537175_H
#define IITERATORTOIENUMERATORADAPTER_1_T2103537175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.ParameterizedStrings/FormatParam>
struct  IIteratorToIEnumeratorAdapter_1_t2103537175  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	FormatParam_t4194474082  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2103537175, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2103537175, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2103537175, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2103537175, ___current_3)); }
	inline FormatParam_t4194474082  get_current_3() const { return ___current_3; }
	inline FormatParam_t4194474082 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(FormatParam_t4194474082  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2103537175_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3205041252_H
#define IITERATORTOIENUMERATORADAPTER_1_T3205041252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.ComponentModel.AttributeCollection/AttributeEntry>
struct  IIteratorToIEnumeratorAdapter_1_t3205041252  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	AttributeEntry_t1001010863  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3205041252, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3205041252, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3205041252, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3205041252, ___current_3)); }
	inline AttributeEntry_t1001010863  get_current_3() const { return ___current_3; }
	inline AttributeEntry_t1001010863 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(AttributeEntry_t1001010863  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3205041252_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T544117909_H
#define IITERATORTOIENUMERATORADAPTER_1_T544117909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Linq.Set`1/Slot<System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t544117909  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Slot_t2635054816  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t544117909, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t544117909, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t544117909, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t544117909, ___current_3)); }
	inline Slot_t2635054816  get_current_3() const { return ___current_3; }
	inline Slot_t2635054816 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Slot_t2635054816  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T544117909_H
// Windows.Foundation.Collections.IIterator`1<System.DateTimeOffset>
struct NOVTABLE IIterator_1_t1103430980 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3227006710(DateTime_t1679451545 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m57224880(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1172097404(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m875085322(uint32_t ___items0ArraySize, DateTime_t1679451545 * ___items0, uint32_t* comReturnValue) = 0;
};
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1647592878_H
#define IITERATORTOIENUMERATORADAPTER_1_T1647592878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.DateTime>
struct  IIteratorToIEnumeratorAdapter_1_t1647592878  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	DateTime_t3738529785  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1647592878, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1647592878, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1647592878, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1647592878, ___current_3)); }
	inline DateTime_t3738529785  get_current_3() const { return ___current_3; }
	inline DateTime_t3738529785 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(DateTime_t3738529785  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1647592878_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1955159850_H
#define IITERATORTOIENUMERATORADAPTER_1_T1955159850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t1955159850  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Slot_t4046096757  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1955159850, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1955159850, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1955159850, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1955159850, ___current_3)); }
	inline Slot_t4046096757  get_current_3() const { return ___current_3; }
	inline Slot_t4046096757 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Slot_t4046096757  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1955159850_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1825999439_H
#define IITERATORTOIENUMERATORADAPTER_1_T1825999439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.Int32>>
struct  IIteratorToIEnumeratorAdapter_1_t1825999439  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Slot_t3916936346  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1825999439, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1825999439, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1825999439, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1825999439, ___current_3)); }
	inline Slot_t3916936346  get_current_3() const { return ___current_3; }
	inline Slot_t3916936346 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Slot_t3916936346  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1825999439_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1435115664_H
#define IITERATORTOIENUMERATORADAPTER_1_T1435115664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.UInt32>>
struct  IIteratorToIEnumeratorAdapter_1_t1435115664  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Slot_t3526052571  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1435115664, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1435115664, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1435115664, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1435115664, ___current_3)); }
	inline Slot_t3526052571  get_current_3() const { return ___current_3; }
	inline Slot_t3526052571 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Slot_t3526052571  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1435115664_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T819380668_H
#define IITERATORTOIENUMERATORADAPTER_1_T819380668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>
struct  IIteratorToIEnumeratorAdapter_1_t819380668  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	LowerCaseMapping_t2910317575  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t819380668, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t819380668, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t819380668, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t819380668, ___current_3)); }
	inline LowerCaseMapping_t2910317575  get_current_3() const { return ___current_3; }
	inline LowerCaseMapping_t2910317575 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(LowerCaseMapping_t2910317575  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T819380668_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1098472215_H
#define IITERATORTOIENUMERATORADAPTER_1_T1098472215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Linq.Set`1/Slot<System.Char>>
struct  IIteratorToIEnumeratorAdapter_1_t1098472215  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Slot_t3189409122  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1098472215, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1098472215, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1098472215, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1098472215, ___current_3)); }
	inline Slot_t3189409122  get_current_3() const { return ___current_3; }
	inline Slot_t3189409122 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Slot_t3189409122  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1098472215_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T647564383_H
#define IITERATORTOIENUMERATORADAPTER_1_T647564383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Xml.Linq.XHashtable`1/XHashtableState/Entry<System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t647564383  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t2738501290  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t647564383, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t647564383, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t647564383, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t647564383, ___current_3)); }
	inline Entry_t2738501290  get_current_3() const { return ___current_3; }
	inline Entry_t2738501290 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t2738501290  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T647564383_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3878301407_H
#define IITERATORTOIENUMERATORADAPTER_1_T3878301407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.UInt32,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t3878301407  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t1674271018  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3878301407, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3878301407, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3878301407, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3878301407, ___current_3)); }
	inline Entry_t1674271018  get_current_3() const { return ___current_3; }
	inline Entry_t1674271018 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t1674271018  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3878301407_H
#ifndef SLOT_T3914249973_H
#define SLOT_T3914249973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1/Slot<System.Decimal>
struct  Slot_t3914249973 
{
public:
	// System.Int32 System.Collections.Generic.HashSet`1/Slot::hashCode
	int32_t ___hashCode_0;
	// T System.Collections.Generic.HashSet`1/Slot::value
	Decimal_t2948259380  ___value_1;
	// System.Int32 System.Collections.Generic.HashSet`1/Slot::next
	int32_t ___next_2;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Slot_t3914249973, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(Slot_t3914249973, ___value_1)); }
	inline Decimal_t2948259380  get_value_1() const { return ___value_1; }
	inline Decimal_t2948259380 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Decimal_t2948259380  value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_next_2() { return static_cast<int32_t>(offsetof(Slot_t3914249973, ___next_2)); }
	inline int32_t get_next_2() const { return ___next_2; }
	inline int32_t* get_address_of_next_2() { return &___next_2; }
	inline void set_next_2(int32_t value)
	{
		___next_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOT_T3914249973_H
#ifndef ENTRY_T622221076_H
#define ENTRY_T622221076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<UnityEngine.Vector3,System.Object>
struct  Entry_t622221076 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	Vector3_t3722313464  ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	RuntimeObject * ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t622221076, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t622221076, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t622221076, ___key_2)); }
	inline Vector3_t3722313464  get_key_2() const { return ___key_2; }
	inline Vector3_t3722313464 * get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(Vector3_t3722313464  value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t622221076, ___value_3)); }
	inline RuntimeObject * get_value_3() const { return ___value_3; }
	inline RuntimeObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(RuntimeObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T622221076_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3535074816_H
#define IITERATORTOIENUMERATORADAPTER_1_T3535074816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Xml.Schema.FacetsChecker/FacetsCompiler/Map>
struct  IIteratorToIEnumeratorAdapter_1_t3535074816  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Map_t1331044427  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3535074816, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3535074816, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3535074816, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3535074816, ___current_3)); }
	inline Map_t1331044427  get_current_3() const { return ___current_3; }
	inline Map_t1331044427 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Map_t1331044427  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3535074816_H
#ifndef ENTRY_T514386000_H
#define ENTRY_T514386000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>
struct  Entry_t514386000 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	uint32_t ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	XboxControllerData_t1920221146  ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t514386000, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t514386000, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t514386000, ___key_2)); }
	inline uint32_t get_key_2() const { return ___key_2; }
	inline uint32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(uint32_t value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t514386000, ___value_3)); }
	inline XboxControllerData_t1920221146  get_value_3() const { return ___value_3; }
	inline XboxControllerData_t1920221146 * get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(XboxControllerData_t1920221146  value)
	{
		___value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T514386000_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2275554755_H
#define IITERATORTOIENUMERATORADAPTER_1_T2275554755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t2275554755  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t71524366  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2275554755, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2275554755, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2275554755, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2275554755, ___current_3)); }
	inline KeyValuePair_2_t71524366  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t71524366 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t71524366  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2275554755_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1042683815_H
#define IITERATORTOIENUMERATORADAPTER_1_T1042683815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey>
struct  IIteratorToIEnumeratorAdapter_1_t1042683815  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	EventCacheKey_t3133620722  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1042683815, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1042683815, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1042683815, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1042683815, ___current_3)); }
	inline EventCacheKey_t3133620722  get_current_3() const { return ___current_3; }
	inline EventCacheKey_t3133620722 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(EventCacheKey_t3133620722  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1042683815_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T154513912_H
#define IITERATORTOIENUMERATORADAPTER_1_T154513912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t154513912  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t2245450819  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t154513912, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t154513912, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t154513912, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t154513912, ___current_3)); }
	inline KeyValuePair_2_t2245450819  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2245450819 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2245450819  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T154513912_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1751429509_H
#define IITERATORTOIENUMERATORADAPTER_1_T1751429509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct  IIteratorToIEnumeratorAdapter_1_t1751429509  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t3842366416  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1751429509, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1751429509, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1751429509, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1751429509, ___current_3)); }
	inline KeyValuePair_2_t3842366416  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3842366416 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3842366416  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1751429509_H
// Windows.Foundation.Collections.IIterator`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct NOVTABLE IIterator_1_t2488001557 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2952276126(EventRegistrationToken_t318890788 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1513168069(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m467192383(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m620125635(uint32_t ___items0ArraySize, EventRegistrationToken_t318890788 * ___items0, uint32_t* comReturnValue) = 0;
};
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2360475588_H
#define IITERATORTOIENUMERATORADAPTER_1_T2360475588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>
struct  IIteratorToIEnumeratorAdapter_1_t2360475588  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	EventCacheEntry_t156445199  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2360475588, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2360475588, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2360475588, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2360475588, ___current_3)); }
	inline EventCacheEntry_t156445199  get_current_3() const { return ___current_3; }
	inline EventCacheEntry_t156445199 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(EventCacheEntry_t156445199  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2360475588_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2522921177_H
#define IITERATORTOIENUMERATORADAPTER_1_T2522921177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct  IIteratorToIEnumeratorAdapter_1_t2522921177  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	EventRegistrationToken_t318890788  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2522921177, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2522921177, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2522921177, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2522921177, ___current_3)); }
	inline EventRegistrationToken_t318890788  get_current_3() const { return ___current_3; }
	inline EventRegistrationToken_t318890788 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(EventRegistrationToken_t318890788  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2522921177_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1138350600_H
#define IITERATORTOIENUMERATORADAPTER_1_T1138350600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.DateTimeOffset>
struct  IIteratorToIEnumeratorAdapter_1_t1138350600  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	DateTimeOffset_t3229287507  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1138350600, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1138350600, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1138350600, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1138350600, ___current_3)); }
	inline DateTimeOffset_t3229287507  get_current_3() const { return ___current_3; }
	inline DateTimeOffset_t3229287507 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(DateTimeOffset_t3229287507  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1138350600_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1823313066_H
#define IITERATORTOIENUMERATORADAPTER_1_T1823313066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.Decimal>>
struct  IIteratorToIEnumeratorAdapter_1_t1823313066  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Slot_t3914249973  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1823313066, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1823313066, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1823313066, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1823313066, ___current_3)); }
	inline Slot_t3914249973  get_current_3() const { return ___current_3; }
	inline Slot_t3914249973 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Slot_t3914249973  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1823313066_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2826251465_H
#define IITERATORTOIENUMERATORADAPTER_1_T2826251465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<UnityEngine.Vector3,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t2826251465  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t622221076  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2826251465, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2826251465, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2826251465, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2826251465, ___current_3)); }
	inline Entry_t622221076  get_current_3() const { return ___current_3; }
	inline Entry_t622221076 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t622221076  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2826251465_H
#ifndef X509CHAINSTATUS_T133602714_H
#define X509CHAINSTATUS_T133602714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainStatus
struct  X509ChainStatus_t133602714 
{
public:
	// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags System.Security.Cryptography.X509Certificates.X509ChainStatus::status
	int32_t ___status_0;
	// System.String System.Security.Cryptography.X509Certificates.X509ChainStatus::info
	String_t* ___info_1;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(X509ChainStatus_t133602714, ___status_0)); }
	inline int32_t get_status_0() const { return ___status_0; }
	inline int32_t* get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(int32_t value)
	{
		___status_0 = value;
	}

	inline static int32_t get_offset_of_info_1() { return static_cast<int32_t>(offsetof(X509ChainStatus_t133602714, ___info_1)); }
	inline String_t* get_info_1() const { return ___info_1; }
	inline String_t** get_address_of_info_1() { return &___info_1; }
	inline void set_info_1(String_t* value)
	{
		___info_1 = value;
		Il2CppCodeGenWriteBarrier((&___info_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Security.Cryptography.X509Certificates.X509ChainStatus
struct X509ChainStatus_t133602714_marshaled_pinvoke
{
	int32_t ___status_0;
	char* ___info_1;
};
// Native definition for COM marshalling of System.Security.Cryptography.X509Certificates.X509ChainStatus
struct X509ChainStatus_t133602714_marshaled_com
{
	int32_t ___status_0;
	Il2CppChar* ___info_1;
};
#endif // X509CHAINSTATUS_T133602714_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1248836109_H
#define IITERATORTOIENUMERATORADAPTER_1_T1248836109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Globalization.HebrewNumber/HS>
struct  IIteratorToIEnumeratorAdapter_1_t1248836109  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1248836109, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1248836109, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1248836109, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1248836109, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1248836109_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2459195227_H
#define IITERATORTOIENUMERATORADAPTER_1_T2459195227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t2459195227  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t255164838  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2459195227, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2459195227, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2459195227, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2459195227, ___current_3)); }
	inline KeyValuePair_2_t255164838  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t255164838 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t255164838  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2459195227_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T141333463_H
#define IITERATORTOIENUMERATORADAPTER_1_T141333463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.DateTimeParse/DS>
struct  IIteratorToIEnumeratorAdapter_1_t141333463  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t141333463, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t141333463, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t141333463, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t141333463, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T141333463_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1176964493_H
#define IITERATORTOIENUMERATORADAPTER_1_T1176964493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t1176964493  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t3267901400  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1176964493, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1176964493, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1176964493, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1176964493, ___current_3)); }
	inline KeyValuePair_2_t3267901400  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3267901400 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3267901400  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1176964493_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2163005569_H
#define IITERATORTOIENUMERATORADAPTER_1_T2163005569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>>
struct  IIteratorToIEnumeratorAdapter_1_t2163005569  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t4253942476  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2163005569, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2163005569, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2163005569, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2163005569, ___current_3)); }
	inline KeyValuePair_2_t4253942476  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t4253942476 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t4253942476  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2163005569_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3501224068_H
#define IITERATORTOIENUMERATORADAPTER_1_T3501224068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>>
struct  IIteratorToIEnumeratorAdapter_1_t3501224068  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t1297193679  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3501224068, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3501224068, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3501224068, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3501224068, ___current_3)); }
	inline KeyValuePair_2_t1297193679  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1297193679 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1297193679  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3501224068_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2957658744_H
#define IITERATORTOIENUMERATORADAPTER_1_T2957658744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t2957658744  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t753628355  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2957658744, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2957658744, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2957658744, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2957658744, ___current_3)); }
	inline KeyValuePair_2_t753628355  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t753628355 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t753628355  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2957658744_H
#ifndef KEYCODEEVENTPAIR_T1510105498_H
#define KEYCODEEVENTPAIR_T1510105498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair
struct  KeyCodeEventPair_t1510105498 
{
public:
	// UnityEngine.KeyCode HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair::KeyCode
	int32_t ___KeyCode_0;
	// HoloToolkit.Unity.InputModule.KeyboardManager/KeyEvent HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair::KeyEvent
	int32_t ___KeyEvent_1;

public:
	inline static int32_t get_offset_of_KeyCode_0() { return static_cast<int32_t>(offsetof(KeyCodeEventPair_t1510105498, ___KeyCode_0)); }
	inline int32_t get_KeyCode_0() const { return ___KeyCode_0; }
	inline int32_t* get_address_of_KeyCode_0() { return &___KeyCode_0; }
	inline void set_KeyCode_0(int32_t value)
	{
		___KeyCode_0 = value;
	}

	inline static int32_t get_offset_of_KeyEvent_1() { return static_cast<int32_t>(offsetof(KeyCodeEventPair_t1510105498, ___KeyEvent_1)); }
	inline int32_t get_KeyEvent_1() const { return ___KeyEvent_1; }
	inline int32_t* get_address_of_KeyEvent_1() { return &___KeyEvent_1; }
	inline void set_KeyEvent_1(int32_t value)
	{
		___KeyEvent_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODEEVENTPAIR_T1510105498_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3252164081_H
#define IITERATORTOIENUMERATORADAPTER_1_T3252164081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>>
struct  IIteratorToIEnumeratorAdapter_1_t3252164081  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t1048133692  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3252164081, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3252164081, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3252164081, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3252164081, ___current_3)); }
	inline KeyValuePair_2_t1048133692  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1048133692 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1048133692  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3252164081_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2435858957_H
#define IITERATORTOIENUMERATORADAPTER_1_T2435858957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t2435858957  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t231828568  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2435858957, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2435858957, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2435858957, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2435858957, ___current_3)); }
	inline KeyValuePair_2_t231828568  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t231828568 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t231828568  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2435858957_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3609059144_H
#define IITERATORTOIENUMERATORADAPTER_1_T3609059144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t3609059144  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t1405028755  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3609059144, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3609059144, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3609059144, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3609059144, ___current_3)); }
	inline KeyValuePair_2_t1405028755  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1405028755 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1405028755  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3609059144_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1083145055_H
#define IITERATORTOIENUMERATORADAPTER_1_T1083145055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>
struct  IIteratorToIEnumeratorAdapter_1_t1083145055  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t3174081962  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1083145055, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1083145055, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1083145055, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1083145055, ___current_3)); }
	inline KeyValuePair_2_t3174081962  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3174081962 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3174081962  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1083145055_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1608707143_H
#define IITERATORTOIENUMERATORADAPTER_1_T1608707143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t1608707143  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t3699644050  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1608707143, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1608707143, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1608707143, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1608707143, ___current_3)); }
	inline KeyValuePair_2_t3699644050  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3699644050 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3699644050  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1608707143_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2330034816_H
#define IITERATORTOIENUMERATORADAPTER_1_T2330034816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>>
struct  IIteratorToIEnumeratorAdapter_1_t2330034816  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t126004427  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2330034816, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2330034816, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2330034816, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2330034816, ___current_3)); }
	inline KeyValuePair_2_t126004427  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t126004427 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t126004427  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2330034816_H
#ifndef TZIFTYPE_T2111777359_H
#define TZIFTYPE_T2111777359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeZoneInfo/TZifType
struct  TZifType_t2111777359 
{
public:
	// System.TimeSpan System.TimeZoneInfo/TZifType::UtcOffset
	TimeSpan_t881159249  ___UtcOffset_0;
	// System.Boolean System.TimeZoneInfo/TZifType::IsDst
	bool ___IsDst_1;
	// System.Byte System.TimeZoneInfo/TZifType::AbbreviationIndex
	uint8_t ___AbbreviationIndex_2;

public:
	inline static int32_t get_offset_of_UtcOffset_0() { return static_cast<int32_t>(offsetof(TZifType_t2111777359, ___UtcOffset_0)); }
	inline TimeSpan_t881159249  get_UtcOffset_0() const { return ___UtcOffset_0; }
	inline TimeSpan_t881159249 * get_address_of_UtcOffset_0() { return &___UtcOffset_0; }
	inline void set_UtcOffset_0(TimeSpan_t881159249  value)
	{
		___UtcOffset_0 = value;
	}

	inline static int32_t get_offset_of_IsDst_1() { return static_cast<int32_t>(offsetof(TZifType_t2111777359, ___IsDst_1)); }
	inline bool get_IsDst_1() const { return ___IsDst_1; }
	inline bool* get_address_of_IsDst_1() { return &___IsDst_1; }
	inline void set_IsDst_1(bool value)
	{
		___IsDst_1 = value;
	}

	inline static int32_t get_offset_of_AbbreviationIndex_2() { return static_cast<int32_t>(offsetof(TZifType_t2111777359, ___AbbreviationIndex_2)); }
	inline uint8_t get_AbbreviationIndex_2() const { return ___AbbreviationIndex_2; }
	inline uint8_t* get_address_of_AbbreviationIndex_2() { return &___AbbreviationIndex_2; }
	inline void set_AbbreviationIndex_2(uint8_t value)
	{
		___AbbreviationIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.TimeZoneInfo/TZifType
struct TZifType_t2111777359_marshaled_pinvoke
{
	TimeSpan_t881159249  ___UtcOffset_0;
	int32_t ___IsDst_1;
	uint8_t ___AbbreviationIndex_2;
};
// Native definition for COM marshalling of System.TimeZoneInfo/TZifType
struct TZifType_t2111777359_marshaled_com
{
	TimeSpan_t881159249  ___UtcOffset_0;
	int32_t ___IsDst_1;
	uint8_t ___AbbreviationIndex_2;
};
#endif // TZIFTYPE_T2111777359_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2848742547_H
#define IITERATORTOIENUMERATORADAPTER_1_T2848742547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t2848742547  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t644712158  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2848742547, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2848742547, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2848742547, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2848742547, ___current_3)); }
	inline KeyValuePair_2_t644712158  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t644712158 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t644712158  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2848742547_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T781668292_H
#define IITERATORTOIENUMERATORADAPTER_1_T781668292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>
struct  IIteratorToIEnumeratorAdapter_1_t781668292  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t2872605199  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t781668292, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t781668292, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t781668292, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t781668292, ___current_3)); }
	inline KeyValuePair_2_t2872605199  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2872605199 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2872605199  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T781668292_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T722487997_H
#define IITERATORTOIENUMERATORADAPTER_1_T722487997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Threading.CancellationTokenRegistration>
struct  IIteratorToIEnumeratorAdapter_1_t722487997  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	CancellationTokenRegistration_t2813424904  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t722487997, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t722487997, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t722487997, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t722487997, ___current_3)); }
	inline CancellationTokenRegistration_t2813424904  get_current_3() const { return ___current_3; }
	inline CancellationTokenRegistration_t2813424904 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(CancellationTokenRegistration_t2813424904  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T722487997_H
// Windows.Foundation.Collections.IIterator`1<System.TimeSpan>
struct NOVTABLE IIterator_1_t3050270018 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3779463845(TimeSpan_t881159249 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m575406462(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1603012770(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m4131394294(uint32_t ___items0ArraySize, TimeSpan_t881159249 * ___items0, uint32_t* comReturnValue) = 0;
};
#ifndef KEYVALUEPAIR_2_T249061059_H
#define KEYVALUEPAIR_2_T249061059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct  KeyValuePair_2_t249061059 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t249061059, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t249061059, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T249061059_H
#ifndef KEYVALUEPAIR_2_T4085865031_H
#define KEYVALUEPAIR_2_T4085865031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>
struct  KeyValuePair_2_t4085865031 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4085865031, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4085865031, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T4085865031_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3085189638_H
#define IITERATORTOIENUMERATORADAPTER_1_T3085189638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.TimeSpan>
struct  IIteratorToIEnumeratorAdapter_1_t3085189638  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	TimeSpan_t881159249  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3085189638, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3085189638, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3085189638, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3085189638, ___current_3)); }
	inline TimeSpan_t881159249  get_current_3() const { return ___current_3; }
	inline TimeSpan_t881159249 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(TimeSpan_t881159249  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3085189638_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2441886399_H
#define IITERATORTOIENUMERATORADAPTER_1_T2441886399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Reflection.Emit.ILExceptionInfo>
struct  IIteratorToIEnumeratorAdapter_1_t2441886399  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	ILExceptionInfo_t237856010  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2441886399, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2441886399, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2441886399, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2441886399, ___current_3)); }
	inline ILExceptionInfo_t237856010  get_current_3() const { return ___current_3; }
	inline ILExceptionInfo_t237856010 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ILExceptionInfo_t237856010  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2441886399_H
#ifndef TIMESPANTOKEN_T993347374_H
#define TIMESPANTOKEN_T993347374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.TimeSpanParse/TimeSpanToken
struct  TimeSpanToken_t993347374 
{
public:
	// System.Globalization.TimeSpanParse/TTT System.Globalization.TimeSpanParse/TimeSpanToken::ttt
	int32_t ___ttt_0;
	// System.Int32 System.Globalization.TimeSpanParse/TimeSpanToken::num
	int32_t ___num_1;
	// System.Int32 System.Globalization.TimeSpanParse/TimeSpanToken::zeroes
	int32_t ___zeroes_2;
	// System.String System.Globalization.TimeSpanParse/TimeSpanToken::sep
	String_t* ___sep_3;

public:
	inline static int32_t get_offset_of_ttt_0() { return static_cast<int32_t>(offsetof(TimeSpanToken_t993347374, ___ttt_0)); }
	inline int32_t get_ttt_0() const { return ___ttt_0; }
	inline int32_t* get_address_of_ttt_0() { return &___ttt_0; }
	inline void set_ttt_0(int32_t value)
	{
		___ttt_0 = value;
	}

	inline static int32_t get_offset_of_num_1() { return static_cast<int32_t>(offsetof(TimeSpanToken_t993347374, ___num_1)); }
	inline int32_t get_num_1() const { return ___num_1; }
	inline int32_t* get_address_of_num_1() { return &___num_1; }
	inline void set_num_1(int32_t value)
	{
		___num_1 = value;
	}

	inline static int32_t get_offset_of_zeroes_2() { return static_cast<int32_t>(offsetof(TimeSpanToken_t993347374, ___zeroes_2)); }
	inline int32_t get_zeroes_2() const { return ___zeroes_2; }
	inline int32_t* get_address_of_zeroes_2() { return &___zeroes_2; }
	inline void set_zeroes_2(int32_t value)
	{
		___zeroes_2 = value;
	}

	inline static int32_t get_offset_of_sep_3() { return static_cast<int32_t>(offsetof(TimeSpanToken_t993347374, ___sep_3)); }
	inline String_t* get_sep_3() const { return ___sep_3; }
	inline String_t** get_address_of_sep_3() { return &___sep_3; }
	inline void set_sep_3(String_t* value)
	{
		___sep_3 = value;
		Il2CppCodeGenWriteBarrier((&___sep_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Globalization.TimeSpanParse/TimeSpanToken
struct TimeSpanToken_t993347374_marshaled_pinvoke
{
	int32_t ___ttt_0;
	int32_t ___num_1;
	int32_t ___zeroes_2;
	char* ___sep_3;
};
// Native definition for COM marshalling of System.Globalization.TimeSpanParse/TimeSpanToken
struct TimeSpanToken_t993347374_marshaled_com
{
	int32_t ___ttt_0;
	int32_t ___num_1;
	int32_t ___zeroes_2;
	Il2CppChar* ___sep_3;
};
#endif // TIMESPANTOKEN_T993347374_H
#ifndef KEVENT_T2406656960_H
#define KEVENT_T2406656960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.kevent
struct  kevent_t2406656960 
{
public:
	// System.UIntPtr System.IO.kevent::ident
	uintptr_t ___ident_0;
	// System.IO.EventFilter System.IO.kevent::filter
	int16_t ___filter_1;
	// System.IO.EventFlags System.IO.kevent::flags
	uint16_t ___flags_2;
	// System.IO.FilterFlags System.IO.kevent::fflags
	uint32_t ___fflags_3;
	// System.IntPtr System.IO.kevent::data
	intptr_t ___data_4;
	// System.IntPtr System.IO.kevent::udata
	intptr_t ___udata_5;

public:
	inline static int32_t get_offset_of_ident_0() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___ident_0)); }
	inline uintptr_t get_ident_0() const { return ___ident_0; }
	inline uintptr_t* get_address_of_ident_0() { return &___ident_0; }
	inline void set_ident_0(uintptr_t value)
	{
		___ident_0 = value;
	}

	inline static int32_t get_offset_of_filter_1() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___filter_1)); }
	inline int16_t get_filter_1() const { return ___filter_1; }
	inline int16_t* get_address_of_filter_1() { return &___filter_1; }
	inline void set_filter_1(int16_t value)
	{
		___filter_1 = value;
	}

	inline static int32_t get_offset_of_flags_2() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___flags_2)); }
	inline uint16_t get_flags_2() const { return ___flags_2; }
	inline uint16_t* get_address_of_flags_2() { return &___flags_2; }
	inline void set_flags_2(uint16_t value)
	{
		___flags_2 = value;
	}

	inline static int32_t get_offset_of_fflags_3() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___fflags_3)); }
	inline uint32_t get_fflags_3() const { return ___fflags_3; }
	inline uint32_t* get_address_of_fflags_3() { return &___fflags_3; }
	inline void set_fflags_3(uint32_t value)
	{
		___fflags_3 = value;
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___data_4)); }
	inline intptr_t get_data_4() const { return ___data_4; }
	inline intptr_t* get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(intptr_t value)
	{
		___data_4 = value;
	}

	inline static int32_t get_offset_of_udata_5() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___udata_5)); }
	inline intptr_t get_udata_5() const { return ___udata_5; }
	inline intptr_t* get_address_of_udata_5() { return &___udata_5; }
	inline void set_udata_5(intptr_t value)
	{
		___udata_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEVENT_T2406656960_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2002112070_H
#define IITERATORTOIENUMERATORADAPTER_1_T2002112070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>
struct  IIteratorToIEnumeratorAdapter_1_t2002112070  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2002112070, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2002112070, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2002112070, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2002112070, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2002112070_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1394499547_H
#define IITERATORTOIENUMERATORADAPTER_1_T1394499547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>
struct  IIteratorToIEnumeratorAdapter_1_t1394499547  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1394499547, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1394499547, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1394499547, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1394499547, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1394499547_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2494310344_H
#define IITERATORTOIENUMERATORADAPTER_1_T2494310344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.TermInfoStrings>
struct  IIteratorToIEnumeratorAdapter_1_t2494310344  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2494310344, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2494310344, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2494310344, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2494310344, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2494310344_H
#ifndef KEYVALUEPAIR_2_T2738617651_H
#define KEYVALUEPAIR_2_T2738617651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>
struct  KeyValuePair_2_t2738617651 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	EventRegistrationTokenList_t3288506496  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2738617651, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2738617651, ___value_1)); }
	inline EventRegistrationTokenList_t3288506496  get_value_1() const { return ___value_1; }
	inline EventRegistrationTokenList_t3288506496 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(EventRegistrationTokenList_t3288506496  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2738617651_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2296875984_H
#define IITERATORTOIENUMERATORADAPTER_1_T2296875984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Text.RegularExpressions.RegexOptions>
struct  IIteratorToIEnumeratorAdapter_1_t2296875984  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2296875984, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2296875984, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2296875984, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2296875984, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2296875984_H
#ifndef KEYVALUEPAIR_2_T2255362622_H
#define KEYVALUEPAIR_2_T2255362622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>
struct  KeyValuePair_2_t2255362622 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2255362622, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2255362622, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2255362622_H
#ifndef KEYVALUEPAIR_2_T4120547_H
#define KEYVALUEPAIR_2_T4120547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>
struct  KeyValuePair_2_t4120547 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4120547, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4120547, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T4120547_H
#ifndef KEYVALUEPAIR_2_T2730400744_H
#define KEYVALUEPAIR_2_T2730400744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>
struct  KeyValuePair_2_t2730400744 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2730400744, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2730400744, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2730400744_H
#ifndef ENTRY_T3516280164_H
#define ENTRY_T3516280164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>
struct  Entry_t3516280164 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	int32_t ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	RuntimeObject * ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t3516280164, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t3516280164, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t3516280164, ___key_2)); }
	inline int32_t get_key_2() const { return ___key_2; }
	inline int32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int32_t value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t3516280164, ___value_3)); }
	inline RuntimeObject * get_value_3() const { return ___value_3; }
	inline RuntimeObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(RuntimeObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3516280164_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T896287180_H
#define IITERATORTOIENUMERATORADAPTER_1_T896287180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.TypeCode>
struct  IIteratorToIEnumeratorAdapter_1_t896287180  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t896287180, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t896287180, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t896287180, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t896287180, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T896287180_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1197569589_H
#define IITERATORTOIENUMERATORADAPTER_1_T1197569589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>
struct  IIteratorToIEnumeratorAdapter_1_t1197569589  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	EventRegistrationTokenList_t3288506496  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1197569589, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1197569589, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1197569589, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1197569589, ___current_3)); }
	inline EventRegistrationTokenList_t3288506496  get_current_3() const { return ___current_3; }
	inline EventRegistrationTokenList_t3288506496 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(EventRegistrationTokenList_t3288506496  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1197569589_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2718416389_H
#define IITERATORTOIENUMERATORADAPTER_1_T2718416389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>>
struct  IIteratorToIEnumeratorAdapter_1_t2718416389  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t514386000  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2718416389, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2718416389, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2718416389, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2718416389, ___current_3)); }
	inline Entry_t514386000  get_current_3() const { return ___current_3; }
	inline Entry_t514386000 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t514386000  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2718416389_H
#ifndef REFEMITPERMISSIONSET_T484390987_H
#define REFEMITPERMISSIONSET_T484390987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.RefEmitPermissionSet
struct  RefEmitPermissionSet_t484390987 
{
public:
	// System.Security.Permissions.SecurityAction System.Reflection.Emit.RefEmitPermissionSet::action
	int32_t ___action_0;
	// System.String System.Reflection.Emit.RefEmitPermissionSet::pset
	String_t* ___pset_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(RefEmitPermissionSet_t484390987, ___action_0)); }
	inline int32_t get_action_0() const { return ___action_0; }
	inline int32_t* get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(int32_t value)
	{
		___action_0 = value;
	}

	inline static int32_t get_offset_of_pset_1() { return static_cast<int32_t>(offsetof(RefEmitPermissionSet_t484390987, ___pset_1)); }
	inline String_t* get_pset_1() const { return ___pset_1; }
	inline String_t** get_address_of_pset_1() { return &___pset_1; }
	inline void set_pset_1(String_t* value)
	{
		___pset_1 = value;
		Il2CppCodeGenWriteBarrier((&___pset_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Emit.RefEmitPermissionSet
struct RefEmitPermissionSet_t484390987_marshaled_pinvoke
{
	int32_t ___action_0;
	char* ___pset_1;
};
// Native definition for COM marshalling of System.Reflection.Emit.RefEmitPermissionSet
struct RefEmitPermissionSet_t484390987_marshaled_com
{
	int32_t ___action_0;
	Il2CppChar* ___pset_1;
};
#endif // REFEMITPERMISSIONSET_T484390987_H
#ifndef MONORESOURCE_T4103430009_H
#define MONORESOURCE_T4103430009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.MonoResource
struct  MonoResource_t4103430009 
{
public:
	// System.Byte[] System.Reflection.Emit.MonoResource::data
	ByteU5BU5D_t4116647657* ___data_0;
	// System.String System.Reflection.Emit.MonoResource::name
	String_t* ___name_1;
	// System.String System.Reflection.Emit.MonoResource::filename
	String_t* ___filename_2;
	// System.Reflection.ResourceAttributes System.Reflection.Emit.MonoResource::attrs
	int32_t ___attrs_3;
	// System.Int32 System.Reflection.Emit.MonoResource::offset
	int32_t ___offset_4;
	// System.IO.Stream System.Reflection.Emit.MonoResource::stream
	Stream_t1273022909 * ___stream_5;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(MonoResource_t4103430009, ___data_0)); }
	inline ByteU5BU5D_t4116647657* get_data_0() const { return ___data_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ByteU5BU5D_t4116647657* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(MonoResource_t4103430009, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_filename_2() { return static_cast<int32_t>(offsetof(MonoResource_t4103430009, ___filename_2)); }
	inline String_t* get_filename_2() const { return ___filename_2; }
	inline String_t** get_address_of_filename_2() { return &___filename_2; }
	inline void set_filename_2(String_t* value)
	{
		___filename_2 = value;
		Il2CppCodeGenWriteBarrier((&___filename_2), value);
	}

	inline static int32_t get_offset_of_attrs_3() { return static_cast<int32_t>(offsetof(MonoResource_t4103430009, ___attrs_3)); }
	inline int32_t get_attrs_3() const { return ___attrs_3; }
	inline int32_t* get_address_of_attrs_3() { return &___attrs_3; }
	inline void set_attrs_3(int32_t value)
	{
		___attrs_3 = value;
	}

	inline static int32_t get_offset_of_offset_4() { return static_cast<int32_t>(offsetof(MonoResource_t4103430009, ___offset_4)); }
	inline int32_t get_offset_4() const { return ___offset_4; }
	inline int32_t* get_address_of_offset_4() { return &___offset_4; }
	inline void set_offset_4(int32_t value)
	{
		___offset_4 = value;
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(MonoResource_t4103430009, ___stream_5)); }
	inline Stream_t1273022909 * get_stream_5() const { return ___stream_5; }
	inline Stream_t1273022909 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_t1273022909 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Emit.MonoResource
struct MonoResource_t4103430009_marshaled_pinvoke
{
	uint8_t* ___data_0;
	char* ___name_1;
	char* ___filename_2;
	int32_t ___attrs_3;
	int32_t ___offset_4;
	Stream_t1273022909 * ___stream_5;
};
// Native definition for COM marshalling of System.Reflection.Emit.MonoResource
struct MonoResource_t4103430009_marshaled_com
{
	uint8_t* ___data_0;
	Il2CppChar* ___name_1;
	Il2CppChar* ___filename_2;
	int32_t ___attrs_3;
	int32_t ___offset_4;
	Stream_t1273022909 * ___stream_5;
};
#endif // MONORESOURCE_T4103430009_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1189352682_H
#define IITERATORTOIENUMERATORADAPTER_1_T1189352682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Linq.Expressions.Compiler.VariableStorageKind>
struct  IIteratorToIEnumeratorAdapter_1_t1189352682  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1189352682, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1189352682, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1189352682, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1189352682, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1189352682_H
#ifndef WIN32_IP_ADAPTER_ADDRESSES_T3463526328_H
#define WIN32_IP_ADAPTER_ADDRESSES_T3463526328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES
struct  Win32_IP_ADAPTER_ADDRESSES_t3463526328 
{
public:
	// System.Net.NetworkInformation.AlignmentUnion System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::Alignment
	AlignmentUnion_t208902285  ___Alignment_0;
	// System.IntPtr System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::Next
	intptr_t ___Next_1;
	// System.String System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::AdapterName
	String_t* ___AdapterName_2;
	// System.IntPtr System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::FirstUnicastAddress
	intptr_t ___FirstUnicastAddress_3;
	// System.IntPtr System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::FirstAnycastAddress
	intptr_t ___FirstAnycastAddress_4;
	// System.IntPtr System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::FirstMulticastAddress
	intptr_t ___FirstMulticastAddress_5;
	// System.IntPtr System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::FirstDnsServerAddress
	intptr_t ___FirstDnsServerAddress_6;
	// System.String System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::DnsSuffix
	String_t* ___DnsSuffix_7;
	// System.String System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::Description
	String_t* ___Description_8;
	// System.String System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::FriendlyName
	String_t* ___FriendlyName_9;
	// System.Byte[] System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::PhysicalAddress
	ByteU5BU5D_t4116647657* ___PhysicalAddress_10;
	// System.UInt32 System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::PhysicalAddressLength
	uint32_t ___PhysicalAddressLength_11;
	// System.UInt32 System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::Flags
	uint32_t ___Flags_12;
	// System.UInt32 System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::Mtu
	uint32_t ___Mtu_13;
	// System.Net.NetworkInformation.NetworkInterfaceType System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::IfType
	int32_t ___IfType_14;
	// System.Net.NetworkInformation.OperationalStatus System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::OperStatus
	int32_t ___OperStatus_15;
	// System.Int32 System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::Ipv6IfIndex
	int32_t ___Ipv6IfIndex_16;
	// System.UInt32[] System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES::ZoneIndices
	UInt32U5BU5D_t2770800703* ___ZoneIndices_17;

public:
	inline static int32_t get_offset_of_Alignment_0() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___Alignment_0)); }
	inline AlignmentUnion_t208902285  get_Alignment_0() const { return ___Alignment_0; }
	inline AlignmentUnion_t208902285 * get_address_of_Alignment_0() { return &___Alignment_0; }
	inline void set_Alignment_0(AlignmentUnion_t208902285  value)
	{
		___Alignment_0 = value;
	}

	inline static int32_t get_offset_of_Next_1() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___Next_1)); }
	inline intptr_t get_Next_1() const { return ___Next_1; }
	inline intptr_t* get_address_of_Next_1() { return &___Next_1; }
	inline void set_Next_1(intptr_t value)
	{
		___Next_1 = value;
	}

	inline static int32_t get_offset_of_AdapterName_2() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___AdapterName_2)); }
	inline String_t* get_AdapterName_2() const { return ___AdapterName_2; }
	inline String_t** get_address_of_AdapterName_2() { return &___AdapterName_2; }
	inline void set_AdapterName_2(String_t* value)
	{
		___AdapterName_2 = value;
		Il2CppCodeGenWriteBarrier((&___AdapterName_2), value);
	}

	inline static int32_t get_offset_of_FirstUnicastAddress_3() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___FirstUnicastAddress_3)); }
	inline intptr_t get_FirstUnicastAddress_3() const { return ___FirstUnicastAddress_3; }
	inline intptr_t* get_address_of_FirstUnicastAddress_3() { return &___FirstUnicastAddress_3; }
	inline void set_FirstUnicastAddress_3(intptr_t value)
	{
		___FirstUnicastAddress_3 = value;
	}

	inline static int32_t get_offset_of_FirstAnycastAddress_4() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___FirstAnycastAddress_4)); }
	inline intptr_t get_FirstAnycastAddress_4() const { return ___FirstAnycastAddress_4; }
	inline intptr_t* get_address_of_FirstAnycastAddress_4() { return &___FirstAnycastAddress_4; }
	inline void set_FirstAnycastAddress_4(intptr_t value)
	{
		___FirstAnycastAddress_4 = value;
	}

	inline static int32_t get_offset_of_FirstMulticastAddress_5() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___FirstMulticastAddress_5)); }
	inline intptr_t get_FirstMulticastAddress_5() const { return ___FirstMulticastAddress_5; }
	inline intptr_t* get_address_of_FirstMulticastAddress_5() { return &___FirstMulticastAddress_5; }
	inline void set_FirstMulticastAddress_5(intptr_t value)
	{
		___FirstMulticastAddress_5 = value;
	}

	inline static int32_t get_offset_of_FirstDnsServerAddress_6() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___FirstDnsServerAddress_6)); }
	inline intptr_t get_FirstDnsServerAddress_6() const { return ___FirstDnsServerAddress_6; }
	inline intptr_t* get_address_of_FirstDnsServerAddress_6() { return &___FirstDnsServerAddress_6; }
	inline void set_FirstDnsServerAddress_6(intptr_t value)
	{
		___FirstDnsServerAddress_6 = value;
	}

	inline static int32_t get_offset_of_DnsSuffix_7() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___DnsSuffix_7)); }
	inline String_t* get_DnsSuffix_7() const { return ___DnsSuffix_7; }
	inline String_t** get_address_of_DnsSuffix_7() { return &___DnsSuffix_7; }
	inline void set_DnsSuffix_7(String_t* value)
	{
		___DnsSuffix_7 = value;
		Il2CppCodeGenWriteBarrier((&___DnsSuffix_7), value);
	}

	inline static int32_t get_offset_of_Description_8() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___Description_8)); }
	inline String_t* get_Description_8() const { return ___Description_8; }
	inline String_t** get_address_of_Description_8() { return &___Description_8; }
	inline void set_Description_8(String_t* value)
	{
		___Description_8 = value;
		Il2CppCodeGenWriteBarrier((&___Description_8), value);
	}

	inline static int32_t get_offset_of_FriendlyName_9() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___FriendlyName_9)); }
	inline String_t* get_FriendlyName_9() const { return ___FriendlyName_9; }
	inline String_t** get_address_of_FriendlyName_9() { return &___FriendlyName_9; }
	inline void set_FriendlyName_9(String_t* value)
	{
		___FriendlyName_9 = value;
		Il2CppCodeGenWriteBarrier((&___FriendlyName_9), value);
	}

	inline static int32_t get_offset_of_PhysicalAddress_10() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___PhysicalAddress_10)); }
	inline ByteU5BU5D_t4116647657* get_PhysicalAddress_10() const { return ___PhysicalAddress_10; }
	inline ByteU5BU5D_t4116647657** get_address_of_PhysicalAddress_10() { return &___PhysicalAddress_10; }
	inline void set_PhysicalAddress_10(ByteU5BU5D_t4116647657* value)
	{
		___PhysicalAddress_10 = value;
		Il2CppCodeGenWriteBarrier((&___PhysicalAddress_10), value);
	}

	inline static int32_t get_offset_of_PhysicalAddressLength_11() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___PhysicalAddressLength_11)); }
	inline uint32_t get_PhysicalAddressLength_11() const { return ___PhysicalAddressLength_11; }
	inline uint32_t* get_address_of_PhysicalAddressLength_11() { return &___PhysicalAddressLength_11; }
	inline void set_PhysicalAddressLength_11(uint32_t value)
	{
		___PhysicalAddressLength_11 = value;
	}

	inline static int32_t get_offset_of_Flags_12() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___Flags_12)); }
	inline uint32_t get_Flags_12() const { return ___Flags_12; }
	inline uint32_t* get_address_of_Flags_12() { return &___Flags_12; }
	inline void set_Flags_12(uint32_t value)
	{
		___Flags_12 = value;
	}

	inline static int32_t get_offset_of_Mtu_13() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___Mtu_13)); }
	inline uint32_t get_Mtu_13() const { return ___Mtu_13; }
	inline uint32_t* get_address_of_Mtu_13() { return &___Mtu_13; }
	inline void set_Mtu_13(uint32_t value)
	{
		___Mtu_13 = value;
	}

	inline static int32_t get_offset_of_IfType_14() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___IfType_14)); }
	inline int32_t get_IfType_14() const { return ___IfType_14; }
	inline int32_t* get_address_of_IfType_14() { return &___IfType_14; }
	inline void set_IfType_14(int32_t value)
	{
		___IfType_14 = value;
	}

	inline static int32_t get_offset_of_OperStatus_15() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___OperStatus_15)); }
	inline int32_t get_OperStatus_15() const { return ___OperStatus_15; }
	inline int32_t* get_address_of_OperStatus_15() { return &___OperStatus_15; }
	inline void set_OperStatus_15(int32_t value)
	{
		___OperStatus_15 = value;
	}

	inline static int32_t get_offset_of_Ipv6IfIndex_16() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___Ipv6IfIndex_16)); }
	inline int32_t get_Ipv6IfIndex_16() const { return ___Ipv6IfIndex_16; }
	inline int32_t* get_address_of_Ipv6IfIndex_16() { return &___Ipv6IfIndex_16; }
	inline void set_Ipv6IfIndex_16(int32_t value)
	{
		___Ipv6IfIndex_16 = value;
	}

	inline static int32_t get_offset_of_ZoneIndices_17() { return static_cast<int32_t>(offsetof(Win32_IP_ADAPTER_ADDRESSES_t3463526328, ___ZoneIndices_17)); }
	inline UInt32U5BU5D_t2770800703* get_ZoneIndices_17() const { return ___ZoneIndices_17; }
	inline UInt32U5BU5D_t2770800703** get_address_of_ZoneIndices_17() { return &___ZoneIndices_17; }
	inline void set_ZoneIndices_17(UInt32U5BU5D_t2770800703* value)
	{
		___ZoneIndices_17 = value;
		Il2CppCodeGenWriteBarrier((&___ZoneIndices_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES
struct Win32_IP_ADAPTER_ADDRESSES_t3463526328_marshaled_pinvoke
{
	AlignmentUnion_t208902285  ___Alignment_0;
	intptr_t ___Next_1;
	char* ___AdapterName_2;
	intptr_t ___FirstUnicastAddress_3;
	intptr_t ___FirstAnycastAddress_4;
	intptr_t ___FirstMulticastAddress_5;
	intptr_t ___FirstDnsServerAddress_6;
	Il2CppChar* ___DnsSuffix_7;
	Il2CppChar* ___Description_8;
	Il2CppChar* ___FriendlyName_9;
	uint8_t ___PhysicalAddress_10[8];
	uint32_t ___PhysicalAddressLength_11;
	uint32_t ___Flags_12;
	uint32_t ___Mtu_13;
	int32_t ___IfType_14;
	int32_t ___OperStatus_15;
	int32_t ___Ipv6IfIndex_16;
	uint32_t ___ZoneIndices_17[64];
};
// Native definition for COM marshalling of System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES
struct Win32_IP_ADAPTER_ADDRESSES_t3463526328_marshaled_com
{
	AlignmentUnion_t208902285  ___Alignment_0;
	intptr_t ___Next_1;
	char* ___AdapterName_2;
	intptr_t ___FirstUnicastAddress_3;
	intptr_t ___FirstAnycastAddress_4;
	intptr_t ___FirstMulticastAddress_5;
	intptr_t ___FirstDnsServerAddress_6;
	Il2CppChar* ___DnsSuffix_7;
	Il2CppChar* ___Description_8;
	Il2CppChar* ___FriendlyName_9;
	uint8_t ___PhysicalAddress_10[8];
	uint32_t ___PhysicalAddressLength_11;
	uint32_t ___Flags_12;
	uint32_t ___Mtu_13;
	int32_t ___IfType_14;
	int32_t ___OperStatus_15;
	int32_t ___Ipv6IfIndex_16;
	uint32_t ___ZoneIndices_17[64];
};
#endif // WIN32_IP_ADAPTER_ADDRESSES_T3463526328_H
#ifndef HEADERVARIANTINFO_T1935685601_H
#define HEADERVARIANTINFO_T1935685601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HeaderVariantInfo
struct  HeaderVariantInfo_t1935685601 
{
public:
	// System.String System.Net.HeaderVariantInfo::m_name
	String_t* ___m_name_0;
	// System.Net.CookieVariant System.Net.HeaderVariantInfo::m_variant
	int32_t ___m_variant_1;

public:
	inline static int32_t get_offset_of_m_name_0() { return static_cast<int32_t>(offsetof(HeaderVariantInfo_t1935685601, ___m_name_0)); }
	inline String_t* get_m_name_0() const { return ___m_name_0; }
	inline String_t** get_address_of_m_name_0() { return &___m_name_0; }
	inline void set_m_name_0(String_t* value)
	{
		___m_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_0), value);
	}

	inline static int32_t get_offset_of_m_variant_1() { return static_cast<int32_t>(offsetof(HeaderVariantInfo_t1935685601, ___m_variant_1)); }
	inline int32_t get_m_variant_1() const { return ___m_variant_1; }
	inline int32_t* get_address_of_m_variant_1() { return &___m_variant_1; }
	inline void set_m_variant_1(int32_t value)
	{
		___m_variant_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Net.HeaderVariantInfo
struct HeaderVariantInfo_t1935685601_marshaled_pinvoke
{
	char* ___m_name_0;
	int32_t ___m_variant_1;
};
// Native definition for COM marshalling of System.Net.HeaderVariantInfo
struct HeaderVariantInfo_t1935685601_marshaled_com
{
	Il2CppChar* ___m_name_0;
	int32_t ___m_variant_1;
};
#endif // HEADERVARIANTINFO_T1935685601_H
#ifndef RECOGNIZEDATTRIBUTE_T632074220_H
#define RECOGNIZEDATTRIBUTE_T632074220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieTokenizer/RecognizedAttribute
struct  RecognizedAttribute_t632074220 
{
public:
	// System.String System.Net.CookieTokenizer/RecognizedAttribute::m_name
	String_t* ___m_name_0;
	// System.Net.CookieToken System.Net.CookieTokenizer/RecognizedAttribute::m_token
	int32_t ___m_token_1;

public:
	inline static int32_t get_offset_of_m_name_0() { return static_cast<int32_t>(offsetof(RecognizedAttribute_t632074220, ___m_name_0)); }
	inline String_t* get_m_name_0() const { return ___m_name_0; }
	inline String_t** get_address_of_m_name_0() { return &___m_name_0; }
	inline void set_m_name_0(String_t* value)
	{
		___m_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_0), value);
	}

	inline static int32_t get_offset_of_m_token_1() { return static_cast<int32_t>(offsetof(RecognizedAttribute_t632074220, ___m_token_1)); }
	inline int32_t get_m_token_1() const { return ___m_token_1; }
	inline int32_t* get_address_of_m_token_1() { return &___m_token_1; }
	inline void set_m_token_1(int32_t value)
	{
		___m_token_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Net.CookieTokenizer/RecognizedAttribute
struct RecognizedAttribute_t632074220_marshaled_pinvoke
{
	char* ___m_name_0;
	int32_t ___m_token_1;
};
// Native definition for COM marshalling of System.Net.CookieTokenizer/RecognizedAttribute
struct RecognizedAttribute_t632074220_marshaled_com
{
	Il2CppChar* ___m_name_0;
	int32_t ___m_token_1;
};
#endif // RECOGNIZEDATTRIBUTE_T632074220_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T4202089779_H
#define IITERATORTOIENUMERATORADAPTER_1_T4202089779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Net.Sockets.Socket/WSABUF>
struct  IIteratorToIEnumeratorAdapter_1_t4202089779  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	WSABUF_t1998059390  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4202089779, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4202089779, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4202089779, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4202089779, ___current_3)); }
	inline WSABUF_t1998059390  get_current_3() const { return ___current_3; }
	inline WSABUF_t1998059390 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(WSABUF_t1998059390  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T4202089779_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2491896099_H
#define IITERATORTOIENUMERATORADAPTER_1_T2491896099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Reflection.CustomAttributeNamedArgument>
struct  IIteratorToIEnumeratorAdapter_1_t2491896099  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	CustomAttributeNamedArgument_t287865710  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2491896099, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2491896099, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2491896099, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2491896099, ___current_3)); }
	inline CustomAttributeNamedArgument_t287865710  get_current_3() const { return ___current_3; }
	inline CustomAttributeNamedArgument_t287865710 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(CustomAttributeNamedArgument_t287865710  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2491896099_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T814473023_H
#define IITERATORTOIENUMERATORADAPTER_1_T814473023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Net.WebHeaderCollection/RfcChar>
struct  IIteratorToIEnumeratorAdapter_1_t814473023  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	uint8_t ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t814473023, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t814473023, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t814473023, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t814473023, ___current_3)); }
	inline uint8_t get_current_3() const { return ___current_3; }
	inline uint8_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(uint8_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T814473023_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1425343257_H
#define IITERATORTOIENUMERATORADAPTER_1_T1425343257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.Dictionary`2/Entry<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t1425343257  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Entry_t3516280164  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1425343257, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1425343257, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1425343257, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1425343257, ___current_3)); }
	inline Entry_t3516280164  get_current_3() const { return ___current_3; }
	inline Entry_t3516280164 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Entry_t3516280164  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1425343257_H
#ifndef KEYVALUEPAIR_2_T2541612585_H
#define KEYVALUEPAIR_2_T2541612585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>
struct  KeyValuePair_2_t2541612585 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	KeyCodeEventPair_t1510105498  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2541612585, ___key_0)); }
	inline KeyCodeEventPair_t1510105498  get_key_0() const { return ___key_0; }
	inline KeyCodeEventPair_t1510105498 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(KeyCodeEventPair_t1510105498  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2541612585, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2541612585_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T20840452_H
#define IITERATORTOIENUMERATORADAPTER_1_T20840452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.TimeZoneInfo/TZifType>
struct  IIteratorToIEnumeratorAdapter_1_t20840452  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	TZifType_t2111777359  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t20840452, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t20840452, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t20840452, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t20840452, ___current_3)); }
	inline TZifType_t2111777359  get_current_3() const { return ___current_3; }
	inline TZifType_t2111777359 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(TZifType_t2111777359  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T20840452_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2208150936_H
#define IITERATORTOIENUMERATORADAPTER_1_T2208150936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t2208150936  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t4120547  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2208150936, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2208150936, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2208150936, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2208150936, ___current_3)); }
	inline KeyValuePair_2_t4120547  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t4120547 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t4120547  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2208150936_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T647680744_H
#define IITERATORTOIENUMERATORADAPTER_1_T647680744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>>
struct  IIteratorToIEnumeratorAdapter_1_t647680744  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t2738617651  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t647680744, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t647680744, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t647680744, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t647680744, ___current_3)); }
	inline KeyValuePair_2_t2738617651  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2738617651 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2738617651  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T647680744_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2012493102_H
#define IITERATORTOIENUMERATORADAPTER_1_T2012493102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Reflection.Emit.MonoResource>
struct  IIteratorToIEnumeratorAdapter_1_t2012493102  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	MonoResource_t4103430009  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2012493102, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2012493102, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2012493102, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2012493102, ___current_3)); }
	inline MonoResource_t4103430009  get_current_3() const { return ___current_3; }
	inline MonoResource_t4103430009 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(MonoResource_t4103430009  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2012493102_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1372589421_H
#define IITERATORTOIENUMERATORADAPTER_1_T1372589421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>
struct  IIteratorToIEnumeratorAdapter_1_t1372589421  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	Win32_IP_ADAPTER_ADDRESSES_t3463526328  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1372589421, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1372589421, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1372589421, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1372589421, ___current_3)); }
	inline Win32_IP_ADAPTER_ADDRESSES_t3463526328  get_current_3() const { return ___current_3; }
	inline Win32_IP_ADAPTER_ADDRESSES_t3463526328 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Win32_IP_ADAPTER_ADDRESSES_t3463526328  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1372589421_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T315720053_H
#define IITERATORTOIENUMERATORADAPTER_1_T315720053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.IO.kevent>
struct  IIteratorToIEnumeratorAdapter_1_t315720053  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	kevent_t2406656960  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t315720053, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t315720053, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t315720053, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t315720053, ___current_3)); }
	inline kevent_t2406656960  get_current_3() const { return ___current_3; }
	inline kevent_t2406656960 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(kevent_t2406656960  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T315720053_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2836104609_H
#define IITERATORTOIENUMERATORADAPTER_1_T2836104609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Net.CookieTokenizer/RecognizedAttribute>
struct  IIteratorToIEnumeratorAdapter_1_t2836104609  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	RecognizedAttribute_t632074220  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2836104609, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2836104609, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2836104609, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2836104609, ___current_3)); }
	inline RecognizedAttribute_t632074220  get_current_3() const { return ___current_3; }
	inline RecognizedAttribute_t632074220 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RecognizedAttribute_t632074220  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2836104609_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T4139715990_H
#define IITERATORTOIENUMERATORADAPTER_1_T4139715990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Net.HeaderVariantInfo>
struct  IIteratorToIEnumeratorAdapter_1_t4139715990  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	HeaderVariantInfo_t1935685601  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4139715990, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4139715990, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4139715990, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t4139715990, ___current_3)); }
	inline HeaderVariantInfo_t1935685601  get_current_3() const { return ___current_3; }
	inline HeaderVariantInfo_t1935685601 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(HeaderVariantInfo_t1935685601  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T4139715990_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T639463837_H
#define IITERATORTOIENUMERATORADAPTER_1_T639463837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>>
struct  IIteratorToIEnumeratorAdapter_1_t639463837  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t2730400744  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t639463837, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t639463837, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t639463837, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t639463837, ___current_3)); }
	inline KeyValuePair_2_t2730400744  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2730400744 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2730400744  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T639463837_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2337633103_H
#define IITERATORTOIENUMERATORADAPTER_1_T2337633103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>
struct  IIteratorToIEnumeratorAdapter_1_t2337633103  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	X509ChainStatus_t133602714  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2337633103, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2337633103, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2337633103, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2337633103, ___current_3)); }
	inline X509ChainStatus_t133602714  get_current_3() const { return ___current_3; }
	inline X509ChainStatus_t133602714 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(X509ChainStatus_t133602714  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2337633103_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2453091448_H
#define IITERATORTOIENUMERATORADAPTER_1_T2453091448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>
struct  IIteratorToIEnumeratorAdapter_1_t2453091448  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t249061059  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2453091448, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2453091448, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2453091448, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2453091448, ___current_3)); }
	inline KeyValuePair_2_t249061059  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t249061059 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t249061059  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2453091448_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T1994928124_H
#define IITERATORTOIENUMERATORADAPTER_1_T1994928124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>
struct  IIteratorToIEnumeratorAdapter_1_t1994928124  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t4085865031  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1994928124, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1994928124, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1994928124, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t1994928124, ___current_3)); }
	inline KeyValuePair_2_t4085865031  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t4085865031 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t4085865031  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T1994928124_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T164425715_H
#define IITERATORTOIENUMERATORADAPTER_1_T164425715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>>
struct  IIteratorToIEnumeratorAdapter_1_t164425715  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t2255362622  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t164425715, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t164425715, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t164425715, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t164425715, ___current_3)); }
	inline KeyValuePair_2_t2255362622  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2255362622 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2255362622  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T164425715_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T2688421376_H
#define IITERATORTOIENUMERATORADAPTER_1_T2688421376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Reflection.Emit.RefEmitPermissionSet>
struct  IIteratorToIEnumeratorAdapter_1_t2688421376  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	RefEmitPermissionSet_t484390987  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2688421376, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2688421376, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2688421376, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t2688421376, ___current_3)); }
	inline RefEmitPermissionSet_t484390987  get_current_3() const { return ___current_3; }
	inline RefEmitPermissionSet_t484390987 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RefEmitPermissionSet_t484390987  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T2688421376_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T3197377763_H
#define IITERATORTOIENUMERATORADAPTER_1_T3197377763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Globalization.TimeSpanParse/TimeSpanToken>
struct  IIteratorToIEnumeratorAdapter_1_t3197377763  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	TimeSpanToken_t993347374  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3197377763, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3197377763, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3197377763, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t3197377763, ___current_3)); }
	inline TimeSpanToken_t993347374  get_current_3() const { return ___current_3; }
	inline TimeSpanToken_t993347374 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(TimeSpanToken_t993347374  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T3197377763_H
#ifndef IITERATORTOIENUMERATORADAPTER_1_T450675678_H
#define IITERATORTOIENUMERATORADAPTER_1_T450675678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1<System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>>
struct  IIteratorToIEnumeratorAdapter_1_t450675678  : public RuntimeObject
{
public:
	// Windows.Foundation.Collections.IIterator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::iterator
	RuntimeObject* ___iterator_0;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::initialized
	bool ___initialized_1;
	// System.Boolean System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::hadCurrent
	bool ___hadCurrent_2;
	// T System.Runtime.InteropServices.WindowsRuntime.IIteratorToIEnumeratorAdapter`1::current
	KeyValuePair_2_t2541612585  ___current_3;

public:
	inline static int32_t get_offset_of_iterator_0() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t450675678, ___iterator_0)); }
	inline RuntimeObject* get_iterator_0() const { return ___iterator_0; }
	inline RuntimeObject** get_address_of_iterator_0() { return &___iterator_0; }
	inline void set_iterator_0(RuntimeObject* value)
	{
		___iterator_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterator_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t450675678, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_hadCurrent_2() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t450675678, ___hadCurrent_2)); }
	inline bool get_hadCurrent_2() const { return ___hadCurrent_2; }
	inline bool* get_address_of_hadCurrent_2() { return &___hadCurrent_2; }
	inline void set_hadCurrent_2(bool value)
	{
		___hadCurrent_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(IIteratorToIEnumeratorAdapter_1_t450675678, ___current_3)); }
	inline KeyValuePair_2_t2541612585  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2541612585 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2541612585  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IITERATORTOIENUMERATORADAPTER_1_T450675678_H



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3880315370_gshared (IIterableToIEnumerableAdapter_1_t2812313252 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2718416389 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2718416389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2718416389 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<System.UInt32,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3186051589_gshared (IIterableToIEnumerableAdapter_1_t3972198270 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<System.UInt32,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3878301407 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3878301407 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3878301407 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<UnityEngine.Vector3,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3217369057_gshared (IIterableToIEnumerableAdapter_1_t2920148328 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<UnityEngine.Vector3,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2826251465 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2826251465 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2826251465 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Dictionary`2/Entry<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3871306702_gshared (IIterableToIEnumerableAdapter_1_t1519240120 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Dictionary`2/Entry<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1425343257 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1425343257 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1425343257 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.Decimal>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3957899455_gshared (IIterableToIEnumerableAdapter_1_t1917209929 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.HashSet`1/Slot<System.Decimal>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1823313066 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1823313066 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1823313066 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.Int32>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m67756481_gshared (IIterableToIEnumerableAdapter_1_t1919896302 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.HashSet`1/Slot<System.Int32>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1825999439 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1825999439 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1825999439 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1118195850_gshared (IIterableToIEnumerableAdapter_1_t2049056713 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.HashSet`1/Slot<System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1955159850 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1955159850 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1955159850 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.HashSet`1/Slot<System.UInt32>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3025088194_gshared (IIterableToIEnumerableAdapter_1_t1529012527 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.HashSet`1/Slot<System.UInt32>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1435115664 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1435115664 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1435115664 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m4016588937_gshared (IIterableToIEnumerableAdapter_1_t544572541 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<HoloToolkit.Unity.InputModule.KeyboardManager/KeyCodeEventPair,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t450675678 * L_3 = (IIteratorToIEnumeratorAdapter_1_t450675678 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t450675678 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m643157309_gshared (IIterableToIEnumerableAdapter_1_t875565155 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t781668292 * L_3 = (IIteratorToIEnumeratorAdapter_1_t781668292 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t781668292 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2768953237_gshared (IIterableToIEnumerableAdapter_1_t2942639410 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2848742547 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2848742547 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2848742547 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3950484582_gshared (IIterableToIEnumerableAdapter_1_t1270861356 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1176964493 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1176964493 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1176964493 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m302565819_gshared (IIterableToIEnumerableAdapter_1_t3051555607 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2957658744 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2957658744 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2957658744 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1336902228_gshared (IIterableToIEnumerableAdapter_1_t2529755820 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2435858957 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2435858957 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2435858957 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2190587448_gshared (IIterableToIEnumerableAdapter_1_t3346060944 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3252164081 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3252164081 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3252164081 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3650736878_gshared (IIterableToIEnumerableAdapter_1_t2369451618 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2275554755 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2275554755 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2275554755 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3421083021_gshared (IIterableToIEnumerableAdapter_1_t248410775 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t154513912 * L_3 = (IIteratorToIEnumeratorAdapter_1_t154513912 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t154513912 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2629676406_gshared (IIterableToIEnumerableAdapter_1_t1702604006 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1608707143 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1608707143 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1608707143 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3195481867_gshared (IIterableToIEnumerableAdapter_1_t2423931679 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Int32>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2330034816 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2330034816 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2330034816 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3131832019_gshared (IIterableToIEnumerableAdapter_1_t2553092090 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2459195227 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2459195227 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2459195227 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3296933079_gshared (IIterableToIEnumerableAdapter_1_t2088824987 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1994928124 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1994928124 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1994928124 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3295683269_gshared (IIterableToIEnumerableAdapter_1_t2546988311 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2453091448 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2453091448 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2453091448 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m995361612_gshared (IIterableToIEnumerableAdapter_1_t258322578 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.AppContext/SwitchValueState>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t164425715 * L_3 = (IIteratorToIEnumeratorAdapter_1_t164425715 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t164425715 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m406388468_gshared (IIterableToIEnumerableAdapter_1_t1845326372 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1751429509 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1751429509 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1751429509 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3546213817_gshared (IIterableToIEnumerableAdapter_1_t404016864 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t310120001 * L_3 = (IIteratorToIEnumeratorAdapter_1_t310120001 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t310120001 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m4061866521_gshared (IIterableToIEnumerableAdapter_1_t733360700 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Linq.Expressions.Compiler.VariableStorageKind>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t639463837 * L_3 = (IIteratorToIEnumeratorAdapter_1_t639463837 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t639463837 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3138710310_gshared (IIterableToIEnumerableAdapter_1_t533177275 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t439280412 * L_3 = (IIteratorToIEnumeratorAdapter_1_t439280412 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t439280412 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m994282190_gshared (IIterableToIEnumerableAdapter_1_t1177041918 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1083145055 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1083145055 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1083145055 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2608376213_gshared (IIterableToIEnumerableAdapter_1_t741577607 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t647680744 * L_3 = (IIteratorToIEnumeratorAdapter_1_t647680744 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t647680744 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m78877214_gshared (IIterableToIEnumerableAdapter_1_t2256902432 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey,System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2163005569 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2163005569 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2163005569 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1727827754_gshared (IIterableToIEnumerableAdapter_1_t3595120931 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,HoloToolkit.Unity.InputModule.XboxControllerData>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3501224068 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3501224068 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3501224068 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2730470365_gshared (IIterableToIEnumerableAdapter_1_t460038653 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t366141790 * L_3 = (IIteratorToIEnumeratorAdapter_1_t366141790 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t366141790 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m570042697_gshared (IIterableToIEnumerableAdapter_1_t3702956007 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3609059144 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3609059144 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3609059144 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.KeyValuePair`2<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1357858861_gshared (IIterableToIEnumerableAdapter_1_t2302047799 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<UnityGLTF.GLTFSceneImporter/MaterialType,System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2208150936 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2208150936 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2208150936 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Generic.Marker>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m653427654_gshared (IIterableToIEnumerableAdapter_1_t897737733 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.Marker>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t803840870 * L_3 = (IIteratorToIEnumeratorAdapter_1_t803840870 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t803840870 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Collections.Hashtable/bucket>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2345593722_gshared (IIterableToIEnumerableAdapter_1_t3056058956 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Hashtable/bucket>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2962162093 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2962162093 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2962162093 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.ComponentModel.AttributeCollection/AttributeEntry>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1283170879_gshared (IIterableToIEnumerableAdapter_1_t3298938115 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.ComponentModel.AttributeCollection/AttributeEntry>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3205041252 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3205041252 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3205041252 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.DateTime>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1627813550_gshared (IIterableToIEnumerableAdapter_1_t1741489741 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.DateTime>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1647592878 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1647592878 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1647592878 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.DateTimeOffset>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2577570978_gshared (IIterableToIEnumerableAdapter_1_t1232247463 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.DateTimeOffset>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1138350600 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1138350600 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1138350600 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.DateTimeParse/DS>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3740657961_gshared (IIterableToIEnumerableAdapter_1_t235230326 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.DateTimeParse/DS>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t141333463 * L_3 = (IIteratorToIEnumeratorAdapter_1_t141333463 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t141333463 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Decimal>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m488708806_gshared (IIterableToIEnumerableAdapter_1_t951219336 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Decimal>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t857322473 * L_3 = (IIteratorToIEnumeratorAdapter_1_t857322473 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t857322473 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Double>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1983320754_gshared (IIterableToIEnumerableAdapter_1_t2892592615 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Double>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2798695752 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2798695752 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2798695752 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Globalization.HebrewNumber/HS>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2862469748_gshared (IIterableToIEnumerableAdapter_1_t1342732972 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Globalization.HebrewNumber/HS>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1248836109 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1248836109 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1248836109 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Globalization.InternalCodePageDataItem>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1979534948_gshared (IIterableToIEnumerableAdapter_1_t578492889 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Globalization.InternalCodePageDataItem>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t484596026 * L_3 = (IIteratorToIEnumeratorAdapter_1_t484596026 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t484596026 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Globalization.InternalEncodingDataItem>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1424265760_gshared (IIterableToIEnumerableAdapter_1_t1161819773 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Globalization.InternalEncodingDataItem>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1067922910 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1067922910 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1067922910 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Globalization.TimeSpanParse/TimeSpanToken>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1173873626_gshared (IIterableToIEnumerableAdapter_1_t3291274626 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Globalization.TimeSpanParse/TimeSpanToken>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3197377763 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3197377763 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3197377763 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Guid>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1709958283_gshared (IIterableToIEnumerableAdapter_1_t1196492843 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Guid>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1102595980 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1102595980 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1102595980 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Int16>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m4281332935_gshared (IIterableToIEnumerableAdapter_1_t555780343 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Int16>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t461883480 * L_3 = (IIteratorToIEnumeratorAdapter_1_t461883480 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t461883480 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Int32>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3594394509_gshared (IIterableToIEnumerableAdapter_1_t953905709 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Int32>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t860008846 * L_3 = (IIteratorToIEnumeratorAdapter_1_t860008846 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t860008846 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Int64>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3137431539_gshared (IIterableToIEnumerableAdapter_1_t1739527260 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Int64>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1645630397 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1645630397 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1645630397 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.IntPtr>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2553873855_gshared (IIterableToIEnumerableAdapter_1_t3138077433 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.IntPtr>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3044180570 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3044180570 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3044180570 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.IO.kevent>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1619838192_gshared (IIterableToIEnumerableAdapter_1_t409616916 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.IO.kevent>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t315720053 * L_3 = (IIteratorToIEnumeratorAdapter_1_t315720053 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t315720053 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m896793671_gshared (IIterableToIEnumerableAdapter_1_t3011948149 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Linq.Expressions.Compiler.BoundConstants/TypedConstant>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2918051286 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2918051286 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2918051286 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Linq.Expressions.Compiler.VariableStorageKind>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m580296690_gshared (IIterableToIEnumerableAdapter_1_t1283249545 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Linq.Expressions.Compiler.VariableStorageKind>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1189352682 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1189352682 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1189352682 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Linq.Set`1/Slot<System.Char>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1900115941_gshared (IIterableToIEnumerableAdapter_1_t1192369078 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Linq.Set`1/Slot<System.Char>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1098472215 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1098472215 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1098472215 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Linq.Set`1/Slot<System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1390680473_gshared (IIterableToIEnumerableAdapter_1_t638014772 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Linq.Set`1/Slot<System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t544117909 * L_3 = (IIteratorToIEnumeratorAdapter_1_t544117909 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t544117909 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Net.CookieTokenizer/RecognizedAttribute>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m607557727_gshared (IIterableToIEnumerableAdapter_1_t2930001472 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Net.CookieTokenizer/RecognizedAttribute>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2836104609 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2836104609 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2836104609 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Net.HeaderVariantInfo>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m152136960_gshared (IIterableToIEnumerableAdapter_1_t4233612853 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Net.HeaderVariantInfo>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t4139715990 * L_3 = (IIteratorToIEnumeratorAdapter_1_t4139715990 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t4139715990 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3377176124_gshared (IIterableToIEnumerableAdapter_1_t1466486284 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1372589421 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1372589421 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1372589421 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Net.Sockets.Socket/WSABUF>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3737110974_gshared (IIterableToIEnumerableAdapter_1_t1019346 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Net.Sockets.Socket/WSABUF>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t4202089779 * L_3 = (IIteratorToIEnumeratorAdapter_1_t4202089779 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t4202089779 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Net.WebHeaderCollection/RfcChar>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3415273645_gshared (IIterableToIEnumerableAdapter_1_t908369886 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Net.WebHeaderCollection/RfcChar>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t814473023 * L_3 = (IIteratorToIEnumeratorAdapter_1_t814473023 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t814473023 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Object>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1329154218_gshared (IIterableToIEnumerableAdapter_1_t1083066120 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Object>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t989169257 * L_3 = (IIteratorToIEnumeratorAdapter_1_t989169257 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t989169257 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.ParameterizedStrings/FormatParam>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m261601849_gshared (IIterableToIEnumerableAdapter_1_t2197434038 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.ParameterizedStrings/FormatParam>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2103537175 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2103537175 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2103537175 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3831501120_gshared (IIterableToIEnumerableAdapter_1_t2585792962 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Reflection.CustomAttributeNamedArgument>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2491896099 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2491896099 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2491896099 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m185344279_gshared (IIterableToIEnumerableAdapter_1_t726110113 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Reflection.CustomAttributeTypedArgument>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t632213250 * L_3 = (IIteratorToIEnumeratorAdapter_1_t632213250 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t632213250 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.ILExceptionBlock>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3928215225_gshared (IIterableToIEnumerableAdapter_1_t1964834922 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Reflection.Emit.ILExceptionBlock>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1870938059 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1870938059 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1870938059 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.ILExceptionInfo>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1257893405_gshared (IIterableToIEnumerableAdapter_1_t2535783262 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Reflection.Emit.ILExceptionInfo>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2441886399 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2441886399 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2441886399 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1613361695_gshared (IIterableToIEnumerableAdapter_1_t2658094643 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Reflection.Emit.ILGenerator/LabelData>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2564197780 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2564197780 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2564197780 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3606814124_gshared (IIterableToIEnumerableAdapter_1_t3156429306 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Reflection.Emit.ILGenerator/LabelFixup>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3062532443 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3062532443 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3062532443 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m4281336966_gshared (IIterableToIEnumerableAdapter_1_t328735070 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Reflection.Emit.ILTokenInfo>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t234838207 * L_3 = (IIteratorToIEnumeratorAdapter_1_t234838207 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t234838207 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.Label>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2914048958_gshared (IIterableToIEnumerableAdapter_1_t284621599 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Reflection.Emit.Label>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t190724736 * L_3 = (IIteratorToIEnumeratorAdapter_1_t190724736 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t190724736 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.MonoResource>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m328409597_gshared (IIterableToIEnumerableAdapter_1_t2106389965 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Reflection.Emit.MonoResource>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2012493102 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2012493102 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2012493102 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.MonoWin32Resource>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1703743584_gshared (IIterableToIEnumerableAdapter_1_t4202156735 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Reflection.Emit.MonoWin32Resource>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t4108259872 * L_3 = (IIteratorToIEnumeratorAdapter_1_t4108259872 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t4108259872 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.RefEmitPermissionSet>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2165861667_gshared (IIterableToIEnumerableAdapter_1_t2782318239 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Reflection.Emit.RefEmitPermissionSet>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2688421376 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2688421376 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2688421376 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.Emit.SequencePoint>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2806725003_gshared (IIterableToIEnumerableAdapter_1_t68225812 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Reflection.Emit.SequencePoint>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t4269296245 * L_3 = (IIteratorToIEnumeratorAdapter_1_t4269296245 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t4269296245 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Reflection.ParameterModifier>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m114032438_gshared (IIterableToIEnumerableAdapter_1_t3759621718 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Reflection.ParameterModifier>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3665724855 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3665724855 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3665724855 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Resources.ResourceLocator>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1920324117_gshared (IIterableToIEnumerableAdapter_1_t1726930763 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Resources.ResourceLocator>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1633033900 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1633033900 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1633033900 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.CompilerServices.Ephemeron>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2490194795_gshared (IIterableToIEnumerableAdapter_1_t3900523614 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Runtime.CompilerServices.Ephemeron>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3806626751 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3806626751 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3806626751 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.InteropServices.GCHandle>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m235809342_gshared (IIterableToIEnumerableAdapter_1_t1354398143 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Runtime.InteropServices.GCHandle>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1260501280 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1260501280 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1260501280 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1304222155_gshared (IIterableToIEnumerableAdapter_1_t2616818040 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2522921177 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2522921177 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2522921177 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m305392249_gshared (IIterableToIEnumerableAdapter_1_t1291466452 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/EventRegistrationTokenList>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1197569589 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1197569589 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1197569589 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m4259012000_gshared (IIterableToIEnumerableAdapter_1_t2454372451 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheEntry>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2360475588 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2360475588 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2360475588 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2871588923_gshared (IIterableToIEnumerableAdapter_1_t1136580678 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMarshal/NativeOrStaticEventRegistrationImpl/EventCacheKey>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1042683815 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1042683815 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1042683815 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m426698727_gshared (IIterableToIEnumerableAdapter_1_t1488396410 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t1394499547 * L_3 = (IIteratorToIEnumeratorAdapter_1_t1394499547 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t1394499547 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1553165176_gshared (IIterableToIEnumerableAdapter_1_t2096008933 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2002112070 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2002112070 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2002112070 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.SByte>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m514852131_gshared (IIterableToIEnumerableAdapter_1_t3967504914 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.SByte>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3873608051 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3873608051 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3873608051 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3289612109_gshared (IIterableToIEnumerableAdapter_1_t2431529966 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2337633103 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2337633103 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2337633103 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Single>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m4173907405_gshared (IIterableToIEnumerableAdapter_1_t3695194026 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Single>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3601297163 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3601297163 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3601297163 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.TermInfoStrings>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2363036536_gshared (IIterableToIEnumerableAdapter_1_t2588207207 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.TermInfoStrings>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2494310344 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2494310344 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2494310344 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1189501177_gshared (IIterableToIEnumerableAdapter_1_t913277531 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t819380668 * L_3 = (IIteratorToIEnumeratorAdapter_1_t819380668 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t819380668 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Text.RegularExpressions.RegexOptions>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m720032026_gshared (IIterableToIEnumerableAdapter_1_t2390772847 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Text.RegularExpressions.RegexOptions>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2296875984 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2296875984 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2296875984 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Threading.CancellationTokenRegistration>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m676134824_gshared (IIterableToIEnumerableAdapter_1_t816384860 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Threading.CancellationTokenRegistration>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t722487997 * L_3 = (IIteratorToIEnumeratorAdapter_1_t722487997 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t722487997 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.TimeSpan>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1730504053_gshared (IIterableToIEnumerableAdapter_1_t3179086501 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.TimeSpan>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3085189638 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3085189638 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3085189638 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.TimeZoneInfo/TZifType>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m967317763_gshared (IIterableToIEnumerableAdapter_1_t114737315 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.TimeZoneInfo/TZifType>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t20840452 * L_3 = (IIteratorToIEnumeratorAdapter_1_t20840452 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t20840452 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.TypeCode>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m509138743_gshared (IIterableToIEnumerableAdapter_1_t990184043 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.TypeCode>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t896287180 * L_3 = (IIteratorToIEnumeratorAdapter_1_t896287180 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t896287180 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.UInt16>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m3339367552_gshared (IIterableToIEnumerableAdapter_1_t180684914 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.UInt16>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t86788051 * L_3 = (IIteratorToIEnumeratorAdapter_1_t86788051 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t86788051 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.UInt32>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2262411128_gshared (IIterableToIEnumerableAdapter_1_t563021934 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.UInt32>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t469125071 * L_3 = (IIteratorToIEnumeratorAdapter_1_t469125071 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t469125071 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.UInt64>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1268175984_gshared (IIterableToIEnumerableAdapter_1_t2137000048 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.UInt64>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t2043103185 * L_3 = (IIteratorToIEnumeratorAdapter_1_t2043103185 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t2043103185 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Xml.Linq.XHashtable`1/XHashtableState/Entry<System.Object>>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m1311381820_gshared (IIterableToIEnumerableAdapter_1_t741461246 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Xml.Linq.XHashtable`1/XHashtableState/Entry<System.Object>>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t647564383 * L_3 = (IIteratorToIEnumeratorAdapter_1_t647564383 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t647564383 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.WindowsRuntime.IIterableToIEnumerableAdapter`1<System.Xml.Schema.FacetsChecker/FacetsCompiler/Map>::System.Collections.Generic.IEnumerable`1.GetEnumerator()
extern "C"  RuntimeObject* IIterableToIEnumerableAdapter_1_System_Collections_Generic_IEnumerable_1_GetEnumerator_m2077881340_gshared (IIterableToIEnumerableAdapter_1_t3628971679 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((RuntimeObject*)__this);
		RuntimeObject* L_0 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Xml.Schema.FacetsChecker/FacetsCompiler/Map>::First() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (RuntimeObject*)__this);
		RuntimeObject* L_1 = (RuntimeObject*)L_0;
		V_0 = (RuntimeObject*)L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (RuntimeObject*)NULL;
	}

IL_000c:
	{
		RuntimeObject* L_2 = V_0;
		IIteratorToIEnumeratorAdapter_1_t3535074816 * L_3 = (IIteratorToIEnumeratorAdapter_1_t3535074816 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (IIteratorToIEnumeratorAdapter_1_t3535074816 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
