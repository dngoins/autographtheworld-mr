﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Xml.XmlWellFormedWriter
struct XmlWellFormedWriter_t2234459422;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;
// System.String
struct String_t;
// System.Xml.XmlWellFormedWriter/AttributeValueCache/Item[]
struct ItemU5BU5D_t2509908728;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Xml.XmlTextReaderImpl
struct XmlTextReaderImpl_t178060594;
// System.Xml.XmlValidatingReaderImpl
struct XmlValidatingReaderImpl_t1817762384;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t791314227;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t39404347;
// System.Xml.XmlNamespaceManager/NamespaceDeclaration[]
struct NamespaceDeclarationU5BU5D_t3609802718;
// System.Xml.XmlNameTable
struct XmlNameTable_t71772148;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Xml.XmlName[]
struct XmlNameU5BU5D_t742213655;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.Xml.XmlNode
struct XmlNode_t3767805227;
// System.Xml.SecureStringHasher/HashCodeOfStringDelegate
struct HashCodeOfStringDelegate_t1614268366;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.IO.Stream
struct Stream_t1273022909;
// System.Text.Decoder
struct Decoder_t2204182725;
// System.IO.TextReader
struct TextReader_t283511965;
// System.Uri
struct Uri_t100236324;
// System.Xml.IDtdEntityInfo
struct IDtdEntityInfo_t3492232514;
// System.Void
struct Void_t1185182177;
// System.Xml.XmlName
struct XmlName_t4150142242;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t1437094927;
// System.Xml.XmlImplementation
struct XmlImplementation_t254178875;
// System.Xml.DomNameTable
struct DomNameTable_t751058560;
// System.Xml.XmlNamedNodeMap
struct XmlNamedNodeMap_t2821286253;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_t3347508623;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t266093086;
// System.Xml.XmlNodeChangedEventHandler
struct XmlNodeChangedEventHandler_t1533444722;
// System.Xml.XmlResolver
struct XmlResolver_t626023767;
// System.Xml.EmptyEnumerator
struct EmptyEnumerator_t1830168813;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t934654762;
// System.Xml.XmlRawWriterBase64Encoder
struct XmlRawWriterBase64Encoder_t1905751661;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t535375154;
// System.Xml.XmlWriter
struct XmlWriter_t127905479;
// System.Threading.Tasks.Task
struct Task_t3187275312;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Xml.XmlRawWriter
struct XmlRawWriter_t722320575;
// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Int32>
struct Dictionary_2_t2925943073;
// System.Xml.BitStack
struct BitStack_t371189938;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Xml.XmlAttributeCollection
struct XmlAttributeCollection_t2316283784;
// System.IO.TextWriter
struct TextWriter_t3478189236;
// System.Xml.OnRemoveWriter
struct OnRemoveWriter_t4019152709;
// System.Xml.XmlWriterSettings
struct XmlWriterSettings_t3314986516;
// System.Xml.XmlEventCache
struct XmlEventCache_t1546883120;
// System.Collections.Generic.List`1<System.Xml.XmlEventCache/XmlEvent[]>
struct List_1_t2211047657;
// System.Xml.XmlEventCache/XmlEvent[]
struct XmlEventU5BU5D_t738972915;
// System.Xml.XmlParserContext
struct XmlParserContext_t2544895291;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Threading.Tasks.Task`1<System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>>
struct Task_1_t1685771062;
// System.Xml.XmlTextReaderImpl/LaterInitParam
struct LaterInitParam_t1449395818;
// System.Xml.XmlTextReaderImpl/NodeData[]
struct NodeDataU5BU5D_t1309219640;
// System.Xml.XmlTextReaderImpl/NodeData
struct NodeData_t1817330133;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t418790500;
// System.Xml.XmlTextReaderImpl/XmlContext
struct XmlContext_t1618903103;
// System.Xml.XmlTextReaderImpl/ParsingState[]
struct ParsingStateU5BU5D_t1980313167;
// System.Xml.IDtdInfo
struct IDtdInfo_t4056024937;
// System.Xml.IncrementalReadDecoder
struct IncrementalReadDecoder_t3011954239;
// System.Xml.IValidationEventHandling
struct IValidationEventHandling_t3880195220;
// System.Xml.XmlTextReaderImpl/OnDefaultAttributeUseDelegate
struct OnDefaultAttributeUseDelegate_t2911570364;
// System.Collections.Generic.Dictionary`2<System.Xml.IDtdEntityInfo,System.Xml.IDtdEntityInfo>
struct Dictionary_2_t1740447880;
// System.Xml.XmlReader
struct XmlReader_t3121518892;
// System.Text.Encoder
struct Encoder_t2198218980;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Xml.CharEntityEncoderFallback
struct CharEntityEncoderFallback_t110445598;
// System.Collections.Generic.List`1<System.Xml.XmlQualifiedName>
struct List_1_t4232729054;
// System.Xml.Schema.BaseValidator
struct BaseValidator_t868759770;
// System.Xml.Schema.XmlSchemaCollection
struct XmlSchemaCollection_t3610399789;
// System.Xml.XmlValidatingReaderImpl/ValidationEventHandling
struct ValidationEventHandling_t3812646699;
// System.Xml.ReadContentAsBinaryHelper
struct ReadContentAsBinaryHelper_t3017207972;
// System.Xml.XmlWellFormedWriter/Namespace[]
struct NamespaceU5BU5D_t1923479799;
// System.Xml.XmlWellFormedWriter/ElementScope[]
struct ElementScopeU5BU5D_t4144864108;
// System.Xml.XmlWellFormedWriter/AttrName[]
struct AttrNameU5BU5D_t1251672482;
// System.Xml.XmlWellFormedWriter/AttributeValueCache
struct AttributeValueCache_t2432505004;
// System.Xml.XmlWellFormedWriter/State[]
struct StateU5BU5D_t2644572590;
// System.Xml.SecureStringHasher
struct SecureStringHasher_t95812985;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Xml.WriteState[]
struct WriteStateU5BU5D_t2967056742;
// System.Xml.XmlTextEncoder
struct XmlTextEncoder_t1632274355;
// System.Xml.XmlTextWriter/TagInfo[]
struct TagInfoU5BU5D_t2840723532;
// System.Xml.XmlTextWriter/State[]
struct StateU5BU5D_t428546178;
// System.Xml.XmlTextWriterBase64Encoder
struct XmlTextWriterBase64Encoder_t4259465041;
// System.Xml.XmlTextWriter/Namespace[]
struct NamespaceU5BU5D_t4259279085;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Xml.IDtdDefaultAttributeInfo
struct IDtdDefaultAttributeInfo_t2775364687;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef NAMESPACERESOLVERPROXY_T4291765537_H
#define NAMESPACERESOLVERPROXY_T4291765537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/NamespaceResolverProxy
struct  NamespaceResolverProxy_t4291765537  : public RuntimeObject
{
public:
	// System.Xml.XmlWellFormedWriter System.Xml.XmlWellFormedWriter/NamespaceResolverProxy::wfWriter
	XmlWellFormedWriter_t2234459422 * ___wfWriter_0;

public:
	inline static int32_t get_offset_of_wfWriter_0() { return static_cast<int32_t>(offsetof(NamespaceResolverProxy_t4291765537, ___wfWriter_0)); }
	inline XmlWellFormedWriter_t2234459422 * get_wfWriter_0() const { return ___wfWriter_0; }
	inline XmlWellFormedWriter_t2234459422 ** get_address_of_wfWriter_0() { return &___wfWriter_0; }
	inline void set_wfWriter_0(XmlWellFormedWriter_t2234459422 * value)
	{
		___wfWriter_0 = value;
		Il2CppCodeGenWriteBarrier((&___wfWriter_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACERESOLVERPROXY_T4291765537_H
#ifndef ATTRIBUTEVALUECACHE_T2432505004_H
#define ATTRIBUTEVALUECACHE_T2432505004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/AttributeValueCache
struct  AttributeValueCache_t2432505004  : public RuntimeObject
{
public:
	// System.Text.StringBuilder System.Xml.XmlWellFormedWriter/AttributeValueCache::stringValue
	StringBuilder_t1712802186 * ___stringValue_0;
	// System.String System.Xml.XmlWellFormedWriter/AttributeValueCache::singleStringValue
	String_t* ___singleStringValue_1;
	// System.Xml.XmlWellFormedWriter/AttributeValueCache/Item[] System.Xml.XmlWellFormedWriter/AttributeValueCache::items
	ItemU5BU5D_t2509908728* ___items_2;
	// System.Int32 System.Xml.XmlWellFormedWriter/AttributeValueCache::firstItem
	int32_t ___firstItem_3;
	// System.Int32 System.Xml.XmlWellFormedWriter/AttributeValueCache::lastItem
	int32_t ___lastItem_4;

public:
	inline static int32_t get_offset_of_stringValue_0() { return static_cast<int32_t>(offsetof(AttributeValueCache_t2432505004, ___stringValue_0)); }
	inline StringBuilder_t1712802186 * get_stringValue_0() const { return ___stringValue_0; }
	inline StringBuilder_t1712802186 ** get_address_of_stringValue_0() { return &___stringValue_0; }
	inline void set_stringValue_0(StringBuilder_t1712802186 * value)
	{
		___stringValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_0), value);
	}

	inline static int32_t get_offset_of_singleStringValue_1() { return static_cast<int32_t>(offsetof(AttributeValueCache_t2432505004, ___singleStringValue_1)); }
	inline String_t* get_singleStringValue_1() const { return ___singleStringValue_1; }
	inline String_t** get_address_of_singleStringValue_1() { return &___singleStringValue_1; }
	inline void set_singleStringValue_1(String_t* value)
	{
		___singleStringValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___singleStringValue_1), value);
	}

	inline static int32_t get_offset_of_items_2() { return static_cast<int32_t>(offsetof(AttributeValueCache_t2432505004, ___items_2)); }
	inline ItemU5BU5D_t2509908728* get_items_2() const { return ___items_2; }
	inline ItemU5BU5D_t2509908728** get_address_of_items_2() { return &___items_2; }
	inline void set_items_2(ItemU5BU5D_t2509908728* value)
	{
		___items_2 = value;
		Il2CppCodeGenWriteBarrier((&___items_2), value);
	}

	inline static int32_t get_offset_of_firstItem_3() { return static_cast<int32_t>(offsetof(AttributeValueCache_t2432505004, ___firstItem_3)); }
	inline int32_t get_firstItem_3() const { return ___firstItem_3; }
	inline int32_t* get_address_of_firstItem_3() { return &___firstItem_3; }
	inline void set_firstItem_3(int32_t value)
	{
		___firstItem_3 = value;
	}

	inline static int32_t get_offset_of_lastItem_4() { return static_cast<int32_t>(offsetof(AttributeValueCache_t2432505004, ___lastItem_4)); }
	inline int32_t get_lastItem_4() const { return ___lastItem_4; }
	inline int32_t* get_address_of_lastItem_4() { return &___lastItem_4; }
	inline void set_lastItem_4(int32_t value)
	{
		___lastItem_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEVALUECACHE_T2432505004_H
#ifndef BUFFERCHUNK_T1400678142_H
#define BUFFERCHUNK_T1400678142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/AttributeValueCache/BufferChunk
struct  BufferChunk_t1400678142  : public RuntimeObject
{
public:
	// System.Char[] System.Xml.XmlWellFormedWriter/AttributeValueCache/BufferChunk::buffer
	CharU5BU5D_t3528271667* ___buffer_0;
	// System.Int32 System.Xml.XmlWellFormedWriter/AttributeValueCache/BufferChunk::index
	int32_t ___index_1;
	// System.Int32 System.Xml.XmlWellFormedWriter/AttributeValueCache/BufferChunk::count
	int32_t ___count_2;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(BufferChunk_t1400678142, ___buffer_0)); }
	inline CharU5BU5D_t3528271667* get_buffer_0() const { return ___buffer_0; }
	inline CharU5BU5D_t3528271667** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(CharU5BU5D_t3528271667* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(BufferChunk_t1400678142, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(BufferChunk_t1400678142, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERCHUNK_T1400678142_H
#ifndef DTDPARSERPROXY_T3101460057_H
#define DTDPARSERPROXY_T3101460057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/DtdParserProxy
struct  DtdParserProxy_t3101460057  : public RuntimeObject
{
public:
	// System.Xml.XmlTextReaderImpl System.Xml.XmlTextReaderImpl/DtdParserProxy::reader
	XmlTextReaderImpl_t178060594 * ___reader_0;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(DtdParserProxy_t3101460057, ___reader_0)); }
	inline XmlTextReaderImpl_t178060594 * get_reader_0() const { return ___reader_0; }
	inline XmlTextReaderImpl_t178060594 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(XmlTextReaderImpl_t178060594 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier((&___reader_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPARSERPROXY_T3101460057_H
#ifndef XMLREADER_T3121518892_H
#define XMLREADER_T3121518892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_t3121518892  : public RuntimeObject
{
public:

public:
};

struct XmlReader_t3121518892_StaticFields
{
public:
	// System.UInt32 System.Xml.XmlReader::IsTextualNodeBitmap
	uint32_t ___IsTextualNodeBitmap_0;
	// System.UInt32 System.Xml.XmlReader::CanReadContentAsBitmap
	uint32_t ___CanReadContentAsBitmap_1;
	// System.UInt32 System.Xml.XmlReader::HasValueBitmap
	uint32_t ___HasValueBitmap_2;

public:
	inline static int32_t get_offset_of_IsTextualNodeBitmap_0() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892_StaticFields, ___IsTextualNodeBitmap_0)); }
	inline uint32_t get_IsTextualNodeBitmap_0() const { return ___IsTextualNodeBitmap_0; }
	inline uint32_t* get_address_of_IsTextualNodeBitmap_0() { return &___IsTextualNodeBitmap_0; }
	inline void set_IsTextualNodeBitmap_0(uint32_t value)
	{
		___IsTextualNodeBitmap_0 = value;
	}

	inline static int32_t get_offset_of_CanReadContentAsBitmap_1() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892_StaticFields, ___CanReadContentAsBitmap_1)); }
	inline uint32_t get_CanReadContentAsBitmap_1() const { return ___CanReadContentAsBitmap_1; }
	inline uint32_t* get_address_of_CanReadContentAsBitmap_1() { return &___CanReadContentAsBitmap_1; }
	inline void set_CanReadContentAsBitmap_1(uint32_t value)
	{
		___CanReadContentAsBitmap_1 = value;
	}

	inline static int32_t get_offset_of_HasValueBitmap_2() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892_StaticFields, ___HasValueBitmap_2)); }
	inline uint32_t get_HasValueBitmap_2() const { return ___HasValueBitmap_2; }
	inline uint32_t* get_address_of_HasValueBitmap_2() { return &___HasValueBitmap_2; }
	inline void set_HasValueBitmap_2(uint32_t value)
	{
		___HasValueBitmap_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_T3121518892_H
#ifndef VALIDATIONEVENTHANDLING_T3812646699_H
#define VALIDATIONEVENTHANDLING_T3812646699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlValidatingReaderImpl/ValidationEventHandling
struct  ValidationEventHandling_t3812646699  : public RuntimeObject
{
public:
	// System.Xml.XmlValidatingReaderImpl System.Xml.XmlValidatingReaderImpl/ValidationEventHandling::reader
	XmlValidatingReaderImpl_t1817762384 * ___reader_0;
	// System.Xml.Schema.ValidationEventHandler System.Xml.XmlValidatingReaderImpl/ValidationEventHandling::eventHandler
	ValidationEventHandler_t791314227 * ___eventHandler_1;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(ValidationEventHandling_t3812646699, ___reader_0)); }
	inline XmlValidatingReaderImpl_t1817762384 * get_reader_0() const { return ___reader_0; }
	inline XmlValidatingReaderImpl_t1817762384 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(XmlValidatingReaderImpl_t1817762384 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier((&___reader_0), value);
	}

	inline static int32_t get_offset_of_eventHandler_1() { return static_cast<int32_t>(offsetof(ValidationEventHandling_t3812646699, ___eventHandler_1)); }
	inline ValidationEventHandler_t791314227 * get_eventHandler_1() const { return ___eventHandler_1; }
	inline ValidationEventHandler_t791314227 ** get_address_of_eventHandler_1() { return &___eventHandler_1; }
	inline void set_eventHandler_1(ValidationEventHandler_t791314227 * value)
	{
		___eventHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandler_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONEVENTHANDLING_T3812646699_H
#ifndef DTDDEFAULTATTRIBUTEINFOTONODEDATACOMPARER_T1152788373_H
#define DTDDEFAULTATTRIBUTEINFOTONODEDATACOMPARER_T1152788373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/DtdDefaultAttributeInfoToNodeDataComparer
struct  DtdDefaultAttributeInfoToNodeDataComparer_t1152788373  : public RuntimeObject
{
public:

public:
};

struct DtdDefaultAttributeInfoToNodeDataComparer_t1152788373_StaticFields
{
public:
	// System.Collections.Generic.IComparer`1<System.Object> System.Xml.XmlTextReaderImpl/DtdDefaultAttributeInfoToNodeDataComparer::s_instance
	RuntimeObject* ___s_instance_0;

public:
	inline static int32_t get_offset_of_s_instance_0() { return static_cast<int32_t>(offsetof(DtdDefaultAttributeInfoToNodeDataComparer_t1152788373_StaticFields, ___s_instance_0)); }
	inline RuntimeObject* get_s_instance_0() const { return ___s_instance_0; }
	inline RuntimeObject** get_address_of_s_instance_0() { return &___s_instance_0; }
	inline void set_s_instance_0(RuntimeObject* value)
	{
		___s_instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDDEFAULTATTRIBUTEINFOTONODEDATACOMPARER_T1152788373_H
#ifndef XMLWRITER_T127905479_H
#define XMLWRITER_T127905479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWriter
struct  XmlWriter_t127905479  : public RuntimeObject
{
public:
	// System.Char[] System.Xml.XmlWriter::writeNodeBuffer
	CharU5BU5D_t3528271667* ___writeNodeBuffer_0;

public:
	inline static int32_t get_offset_of_writeNodeBuffer_0() { return static_cast<int32_t>(offsetof(XmlWriter_t127905479, ___writeNodeBuffer_0)); }
	inline CharU5BU5D_t3528271667* get_writeNodeBuffer_0() const { return ___writeNodeBuffer_0; }
	inline CharU5BU5D_t3528271667** get_address_of_writeNodeBuffer_0() { return &___writeNodeBuffer_0; }
	inline void set_writeNodeBuffer_0(CharU5BU5D_t3528271667* value)
	{
		___writeNodeBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___writeNodeBuffer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWRITER_T127905479_H
#ifndef XMLNAMESPACEMANAGER_T418790500_H
#define XMLNAMESPACEMANAGER_T418790500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager
struct  XmlNamespaceManager_t418790500  : public RuntimeObject
{
public:
	// System.Xml.XmlNamespaceManager/NamespaceDeclaration[] System.Xml.XmlNamespaceManager::nsdecls
	NamespaceDeclarationU5BU5D_t3609802718* ___nsdecls_0;
	// System.Int32 System.Xml.XmlNamespaceManager::lastDecl
	int32_t ___lastDecl_1;
	// System.Xml.XmlNameTable System.Xml.XmlNamespaceManager::nameTable
	XmlNameTable_t71772148 * ___nameTable_2;
	// System.Int32 System.Xml.XmlNamespaceManager::scopeId
	int32_t ___scopeId_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNamespaceManager::hashTable
	Dictionary_2_t2736202052 * ___hashTable_4;
	// System.Boolean System.Xml.XmlNamespaceManager::useHashtable
	bool ___useHashtable_5;
	// System.String System.Xml.XmlNamespaceManager::xml
	String_t* ___xml_6;
	// System.String System.Xml.XmlNamespaceManager::xmlNs
	String_t* ___xmlNs_7;

public:
	inline static int32_t get_offset_of_nsdecls_0() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___nsdecls_0)); }
	inline NamespaceDeclarationU5BU5D_t3609802718* get_nsdecls_0() const { return ___nsdecls_0; }
	inline NamespaceDeclarationU5BU5D_t3609802718** get_address_of_nsdecls_0() { return &___nsdecls_0; }
	inline void set_nsdecls_0(NamespaceDeclarationU5BU5D_t3609802718* value)
	{
		___nsdecls_0 = value;
		Il2CppCodeGenWriteBarrier((&___nsdecls_0), value);
	}

	inline static int32_t get_offset_of_lastDecl_1() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___lastDecl_1)); }
	inline int32_t get_lastDecl_1() const { return ___lastDecl_1; }
	inline int32_t* get_address_of_lastDecl_1() { return &___lastDecl_1; }
	inline void set_lastDecl_1(int32_t value)
	{
		___lastDecl_1 = value;
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___nameTable_2)); }
	inline XmlNameTable_t71772148 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t71772148 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_scopeId_3() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___scopeId_3)); }
	inline int32_t get_scopeId_3() const { return ___scopeId_3; }
	inline int32_t* get_address_of_scopeId_3() { return &___scopeId_3; }
	inline void set_scopeId_3(int32_t value)
	{
		___scopeId_3 = value;
	}

	inline static int32_t get_offset_of_hashTable_4() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___hashTable_4)); }
	inline Dictionary_2_t2736202052 * get_hashTable_4() const { return ___hashTable_4; }
	inline Dictionary_2_t2736202052 ** get_address_of_hashTable_4() { return &___hashTable_4; }
	inline void set_hashTable_4(Dictionary_2_t2736202052 * value)
	{
		___hashTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___hashTable_4), value);
	}

	inline static int32_t get_offset_of_useHashtable_5() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___useHashtable_5)); }
	inline bool get_useHashtable_5() const { return ___useHashtable_5; }
	inline bool* get_address_of_useHashtable_5() { return &___useHashtable_5; }
	inline void set_useHashtable_5(bool value)
	{
		___useHashtable_5 = value;
	}

	inline static int32_t get_offset_of_xml_6() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___xml_6)); }
	inline String_t* get_xml_6() const { return ___xml_6; }
	inline String_t** get_address_of_xml_6() { return &___xml_6; }
	inline void set_xml_6(String_t* value)
	{
		___xml_6 = value;
		Il2CppCodeGenWriteBarrier((&___xml_6), value);
	}

	inline static int32_t get_offset_of_xmlNs_7() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___xmlNs_7)); }
	inline String_t* get_xmlNs_7() const { return ___xmlNs_7; }
	inline String_t** get_address_of_xmlNs_7() { return &___xmlNs_7; }
	inline void set_xmlNs_7(String_t* value)
	{
		___xmlNs_7 = value;
		Il2CppCodeGenWriteBarrier((&___xmlNs_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEMANAGER_T418790500_H
#ifndef XMLNODE_T3767805227_H
#define XMLNODE_T3767805227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNode
struct  XmlNode_t3767805227  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNode::parentNode
	XmlNode_t3767805227 * ___parentNode_0;

public:
	inline static int32_t get_offset_of_parentNode_0() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227, ___parentNode_0)); }
	inline XmlNode_t3767805227 * get_parentNode_0() const { return ___parentNode_0; }
	inline XmlNode_t3767805227 ** get_address_of_parentNode_0() { return &___parentNode_0; }
	inline void set_parentNode_0(XmlNode_t3767805227 * value)
	{
		___parentNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___parentNode_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODE_T3767805227_H
#ifndef XMLNODELIST_T2551693786_H
#define XMLNODELIST_T2551693786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeList
struct  XmlNodeList_t2551693786  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODELIST_T2551693786_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef DOMNAMETABLE_T751058560_H
#define DOMNAMETABLE_T751058560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DomNameTable
struct  DomNameTable_t751058560  : public RuntimeObject
{
public:
	// System.Xml.XmlName[] System.Xml.DomNameTable::entries
	XmlNameU5BU5D_t742213655* ___entries_0;
	// System.Int32 System.Xml.DomNameTable::count
	int32_t ___count_1;
	// System.Int32 System.Xml.DomNameTable::mask
	int32_t ___mask_2;
	// System.Xml.XmlDocument System.Xml.DomNameTable::ownerDocument
	XmlDocument_t2837193595 * ___ownerDocument_3;
	// System.Xml.XmlNameTable System.Xml.DomNameTable::nameTable
	XmlNameTable_t71772148 * ___nameTable_4;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(DomNameTable_t751058560, ___entries_0)); }
	inline XmlNameU5BU5D_t742213655* get_entries_0() const { return ___entries_0; }
	inline XmlNameU5BU5D_t742213655** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(XmlNameU5BU5D_t742213655* value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier((&___entries_0), value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(DomNameTable_t751058560, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_mask_2() { return static_cast<int32_t>(offsetof(DomNameTable_t751058560, ___mask_2)); }
	inline int32_t get_mask_2() const { return ___mask_2; }
	inline int32_t* get_address_of_mask_2() { return &___mask_2; }
	inline void set_mask_2(int32_t value)
	{
		___mask_2 = value;
	}

	inline static int32_t get_offset_of_ownerDocument_3() { return static_cast<int32_t>(offsetof(DomNameTable_t751058560, ___ownerDocument_3)); }
	inline XmlDocument_t2837193595 * get_ownerDocument_3() const { return ___ownerDocument_3; }
	inline XmlDocument_t2837193595 ** get_address_of_ownerDocument_3() { return &___ownerDocument_3; }
	inline void set_ownerDocument_3(XmlDocument_t2837193595 * value)
	{
		___ownerDocument_3 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_3), value);
	}

	inline static int32_t get_offset_of_nameTable_4() { return static_cast<int32_t>(offsetof(DomNameTable_t751058560, ___nameTable_4)); }
	inline XmlNameTable_t71772148 * get_nameTable_4() const { return ___nameTable_4; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_4() { return &___nameTable_4; }
	inline void set_nameTable_4(XmlNameTable_t71772148 * value)
	{
		___nameTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOMNAMETABLE_T751058560_H
#ifndef XMLCHILDENUMERATOR_T4124507839_H
#define XMLCHILDENUMERATOR_T4124507839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlChildEnumerator
struct  XmlChildEnumerator_t4124507839  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlChildEnumerator::container
	XmlNode_t3767805227 * ___container_0;
	// System.Xml.XmlNode System.Xml.XmlChildEnumerator::child
	XmlNode_t3767805227 * ___child_1;
	// System.Boolean System.Xml.XmlChildEnumerator::isFirst
	bool ___isFirst_2;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(XmlChildEnumerator_t4124507839, ___container_0)); }
	inline XmlNode_t3767805227 * get_container_0() const { return ___container_0; }
	inline XmlNode_t3767805227 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(XmlNode_t3767805227 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_child_1() { return static_cast<int32_t>(offsetof(XmlChildEnumerator_t4124507839, ___child_1)); }
	inline XmlNode_t3767805227 * get_child_1() const { return ___child_1; }
	inline XmlNode_t3767805227 ** get_address_of_child_1() { return &___child_1; }
	inline void set_child_1(XmlNode_t3767805227 * value)
	{
		___child_1 = value;
		Il2CppCodeGenWriteBarrier((&___child_1), value);
	}

	inline static int32_t get_offset_of_isFirst_2() { return static_cast<int32_t>(offsetof(XmlChildEnumerator_t4124507839, ___isFirst_2)); }
	inline bool get_isFirst_2() const { return ___isFirst_2; }
	inline bool* get_address_of_isFirst_2() { return &___isFirst_2; }
	inline void set_isFirst_2(bool value)
	{
		___isFirst_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHILDENUMERATOR_T4124507839_H
#ifndef XMLIMPLEMENTATION_T254178875_H
#define XMLIMPLEMENTATION_T254178875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlImplementation
struct  XmlImplementation_t254178875  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.XmlImplementation::nameTable
	XmlNameTable_t71772148 * ___nameTable_0;

public:
	inline static int32_t get_offset_of_nameTable_0() { return static_cast<int32_t>(offsetof(XmlImplementation_t254178875, ___nameTable_0)); }
	inline XmlNameTable_t71772148 * get_nameTable_0() const { return ___nameTable_0; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_0() { return &___nameTable_0; }
	inline void set_nameTable_0(XmlNameTable_t71772148 * value)
	{
		___nameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLIMPLEMENTATION_T254178875_H
#ifndef SECURESTRINGHASHER_T95812985_H
#define SECURESTRINGHASHER_T95812985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.SecureStringHasher
struct  SecureStringHasher_t95812985  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.SecureStringHasher::hashCodeRandomizer
	int32_t ___hashCodeRandomizer_1;

public:
	inline static int32_t get_offset_of_hashCodeRandomizer_1() { return static_cast<int32_t>(offsetof(SecureStringHasher_t95812985, ___hashCodeRandomizer_1)); }
	inline int32_t get_hashCodeRandomizer_1() const { return ___hashCodeRandomizer_1; }
	inline int32_t* get_address_of_hashCodeRandomizer_1() { return &___hashCodeRandomizer_1; }
	inline void set_hashCodeRandomizer_1(int32_t value)
	{
		___hashCodeRandomizer_1 = value;
	}
};

struct SecureStringHasher_t95812985_StaticFields
{
public:
	// System.Xml.SecureStringHasher/HashCodeOfStringDelegate System.Xml.SecureStringHasher::hashCodeDelegate
	HashCodeOfStringDelegate_t1614268366 * ___hashCodeDelegate_0;

public:
	inline static int32_t get_offset_of_hashCodeDelegate_0() { return static_cast<int32_t>(offsetof(SecureStringHasher_t95812985_StaticFields, ___hashCodeDelegate_0)); }
	inline HashCodeOfStringDelegate_t1614268366 * get_hashCodeDelegate_0() const { return ___hashCodeDelegate_0; }
	inline HashCodeOfStringDelegate_t1614268366 ** get_address_of_hashCodeDelegate_0() { return &___hashCodeDelegate_0; }
	inline void set_hashCodeDelegate_0(HashCodeOfStringDelegate_t1614268366 * value)
	{
		___hashCodeDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___hashCodeDelegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURESTRINGHASHER_T95812985_H
#ifndef INCREMENTALREADDECODER_T3011954239_H
#define INCREMENTALREADDECODER_T3011954239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.IncrementalReadDecoder
struct  IncrementalReadDecoder_t3011954239  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INCREMENTALREADDECODER_T3011954239_H
#ifndef TERNARYTREEREADONLY_T172569514_H
#define TERNARYTREEREADONLY_T172569514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.TernaryTreeReadOnly
struct  TernaryTreeReadOnly_t172569514  : public RuntimeObject
{
public:
	// System.Byte[] System.Xml.TernaryTreeReadOnly::nodeBuffer
	ByteU5BU5D_t4116647657* ___nodeBuffer_0;

public:
	inline static int32_t get_offset_of_nodeBuffer_0() { return static_cast<int32_t>(offsetof(TernaryTreeReadOnly_t172569514, ___nodeBuffer_0)); }
	inline ByteU5BU5D_t4116647657* get_nodeBuffer_0() const { return ___nodeBuffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_nodeBuffer_0() { return &___nodeBuffer_0; }
	inline void set_nodeBuffer_0(ByteU5BU5D_t4116647657* value)
	{
		___nodeBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___nodeBuffer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERNARYTREEREADONLY_T172569514_H
#ifndef XMLLINKEDNODE_T1437094927_H
#define XMLLINKEDNODE_T1437094927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlLinkedNode
struct  XmlLinkedNode_t1437094927  : public XmlNode_t3767805227
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlLinkedNode::next
	XmlLinkedNode_t1437094927 * ___next_1;

public:
	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(XmlLinkedNode_t1437094927, ___next_1)); }
	inline XmlLinkedNode_t1437094927 * get_next_1() const { return ___next_1; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(XmlLinkedNode_t1437094927 * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((&___next_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLLINKEDNODE_T1437094927_H
#ifndef PARSINGSTATE_T1780334922_H
#define PARSINGSTATE_T1780334922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/ParsingState
struct  ParsingState_t1780334922 
{
public:
	// System.Char[] System.Xml.XmlTextReaderImpl/ParsingState::chars
	CharU5BU5D_t3528271667* ___chars_0;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::charPos
	int32_t ___charPos_1;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::charsUsed
	int32_t ___charsUsed_2;
	// System.Text.Encoding System.Xml.XmlTextReaderImpl/ParsingState::encoding
	Encoding_t1523322056 * ___encoding_3;
	// System.Boolean System.Xml.XmlTextReaderImpl/ParsingState::appendMode
	bool ___appendMode_4;
	// System.IO.Stream System.Xml.XmlTextReaderImpl/ParsingState::stream
	Stream_t1273022909 * ___stream_5;
	// System.Text.Decoder System.Xml.XmlTextReaderImpl/ParsingState::decoder
	Decoder_t2204182725 * ___decoder_6;
	// System.Byte[] System.Xml.XmlTextReaderImpl/ParsingState::bytes
	ByteU5BU5D_t4116647657* ___bytes_7;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::bytePos
	int32_t ___bytePos_8;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::bytesUsed
	int32_t ___bytesUsed_9;
	// System.IO.TextReader System.Xml.XmlTextReaderImpl/ParsingState::textReader
	TextReader_t283511965 * ___textReader_10;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::lineNo
	int32_t ___lineNo_11;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::lineStartPos
	int32_t ___lineStartPos_12;
	// System.String System.Xml.XmlTextReaderImpl/ParsingState::baseUriStr
	String_t* ___baseUriStr_13;
	// System.Uri System.Xml.XmlTextReaderImpl/ParsingState::baseUri
	Uri_t100236324 * ___baseUri_14;
	// System.Boolean System.Xml.XmlTextReaderImpl/ParsingState::isEof
	bool ___isEof_15;
	// System.Boolean System.Xml.XmlTextReaderImpl/ParsingState::isStreamEof
	bool ___isStreamEof_16;
	// System.Xml.IDtdEntityInfo System.Xml.XmlTextReaderImpl/ParsingState::entity
	RuntimeObject* ___entity_17;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::entityId
	int32_t ___entityId_18;
	// System.Boolean System.Xml.XmlTextReaderImpl/ParsingState::eolNormalized
	bool ___eolNormalized_19;
	// System.Boolean System.Xml.XmlTextReaderImpl/ParsingState::entityResolvedManually
	bool ___entityResolvedManually_20;

public:
	inline static int32_t get_offset_of_chars_0() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___chars_0)); }
	inline CharU5BU5D_t3528271667* get_chars_0() const { return ___chars_0; }
	inline CharU5BU5D_t3528271667** get_address_of_chars_0() { return &___chars_0; }
	inline void set_chars_0(CharU5BU5D_t3528271667* value)
	{
		___chars_0 = value;
		Il2CppCodeGenWriteBarrier((&___chars_0), value);
	}

	inline static int32_t get_offset_of_charPos_1() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___charPos_1)); }
	inline int32_t get_charPos_1() const { return ___charPos_1; }
	inline int32_t* get_address_of_charPos_1() { return &___charPos_1; }
	inline void set_charPos_1(int32_t value)
	{
		___charPos_1 = value;
	}

	inline static int32_t get_offset_of_charsUsed_2() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___charsUsed_2)); }
	inline int32_t get_charsUsed_2() const { return ___charsUsed_2; }
	inline int32_t* get_address_of_charsUsed_2() { return &___charsUsed_2; }
	inline void set_charsUsed_2(int32_t value)
	{
		___charsUsed_2 = value;
	}

	inline static int32_t get_offset_of_encoding_3() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___encoding_3)); }
	inline Encoding_t1523322056 * get_encoding_3() const { return ___encoding_3; }
	inline Encoding_t1523322056 ** get_address_of_encoding_3() { return &___encoding_3; }
	inline void set_encoding_3(Encoding_t1523322056 * value)
	{
		___encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_3), value);
	}

	inline static int32_t get_offset_of_appendMode_4() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___appendMode_4)); }
	inline bool get_appendMode_4() const { return ___appendMode_4; }
	inline bool* get_address_of_appendMode_4() { return &___appendMode_4; }
	inline void set_appendMode_4(bool value)
	{
		___appendMode_4 = value;
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___stream_5)); }
	inline Stream_t1273022909 * get_stream_5() const { return ___stream_5; }
	inline Stream_t1273022909 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_t1273022909 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_decoder_6() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___decoder_6)); }
	inline Decoder_t2204182725 * get_decoder_6() const { return ___decoder_6; }
	inline Decoder_t2204182725 ** get_address_of_decoder_6() { return &___decoder_6; }
	inline void set_decoder_6(Decoder_t2204182725 * value)
	{
		___decoder_6 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_6), value);
	}

	inline static int32_t get_offset_of_bytes_7() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___bytes_7)); }
	inline ByteU5BU5D_t4116647657* get_bytes_7() const { return ___bytes_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_bytes_7() { return &___bytes_7; }
	inline void set_bytes_7(ByteU5BU5D_t4116647657* value)
	{
		___bytes_7 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_7), value);
	}

	inline static int32_t get_offset_of_bytePos_8() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___bytePos_8)); }
	inline int32_t get_bytePos_8() const { return ___bytePos_8; }
	inline int32_t* get_address_of_bytePos_8() { return &___bytePos_8; }
	inline void set_bytePos_8(int32_t value)
	{
		___bytePos_8 = value;
	}

	inline static int32_t get_offset_of_bytesUsed_9() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___bytesUsed_9)); }
	inline int32_t get_bytesUsed_9() const { return ___bytesUsed_9; }
	inline int32_t* get_address_of_bytesUsed_9() { return &___bytesUsed_9; }
	inline void set_bytesUsed_9(int32_t value)
	{
		___bytesUsed_9 = value;
	}

	inline static int32_t get_offset_of_textReader_10() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___textReader_10)); }
	inline TextReader_t283511965 * get_textReader_10() const { return ___textReader_10; }
	inline TextReader_t283511965 ** get_address_of_textReader_10() { return &___textReader_10; }
	inline void set_textReader_10(TextReader_t283511965 * value)
	{
		___textReader_10 = value;
		Il2CppCodeGenWriteBarrier((&___textReader_10), value);
	}

	inline static int32_t get_offset_of_lineNo_11() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___lineNo_11)); }
	inline int32_t get_lineNo_11() const { return ___lineNo_11; }
	inline int32_t* get_address_of_lineNo_11() { return &___lineNo_11; }
	inline void set_lineNo_11(int32_t value)
	{
		___lineNo_11 = value;
	}

	inline static int32_t get_offset_of_lineStartPos_12() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___lineStartPos_12)); }
	inline int32_t get_lineStartPos_12() const { return ___lineStartPos_12; }
	inline int32_t* get_address_of_lineStartPos_12() { return &___lineStartPos_12; }
	inline void set_lineStartPos_12(int32_t value)
	{
		___lineStartPos_12 = value;
	}

	inline static int32_t get_offset_of_baseUriStr_13() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___baseUriStr_13)); }
	inline String_t* get_baseUriStr_13() const { return ___baseUriStr_13; }
	inline String_t** get_address_of_baseUriStr_13() { return &___baseUriStr_13; }
	inline void set_baseUriStr_13(String_t* value)
	{
		___baseUriStr_13 = value;
		Il2CppCodeGenWriteBarrier((&___baseUriStr_13), value);
	}

	inline static int32_t get_offset_of_baseUri_14() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___baseUri_14)); }
	inline Uri_t100236324 * get_baseUri_14() const { return ___baseUri_14; }
	inline Uri_t100236324 ** get_address_of_baseUri_14() { return &___baseUri_14; }
	inline void set_baseUri_14(Uri_t100236324 * value)
	{
		___baseUri_14 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_14), value);
	}

	inline static int32_t get_offset_of_isEof_15() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___isEof_15)); }
	inline bool get_isEof_15() const { return ___isEof_15; }
	inline bool* get_address_of_isEof_15() { return &___isEof_15; }
	inline void set_isEof_15(bool value)
	{
		___isEof_15 = value;
	}

	inline static int32_t get_offset_of_isStreamEof_16() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___isStreamEof_16)); }
	inline bool get_isStreamEof_16() const { return ___isStreamEof_16; }
	inline bool* get_address_of_isStreamEof_16() { return &___isStreamEof_16; }
	inline void set_isStreamEof_16(bool value)
	{
		___isStreamEof_16 = value;
	}

	inline static int32_t get_offset_of_entity_17() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___entity_17)); }
	inline RuntimeObject* get_entity_17() const { return ___entity_17; }
	inline RuntimeObject** get_address_of_entity_17() { return &___entity_17; }
	inline void set_entity_17(RuntimeObject* value)
	{
		___entity_17 = value;
		Il2CppCodeGenWriteBarrier((&___entity_17), value);
	}

	inline static int32_t get_offset_of_entityId_18() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___entityId_18)); }
	inline int32_t get_entityId_18() const { return ___entityId_18; }
	inline int32_t* get_address_of_entityId_18() { return &___entityId_18; }
	inline void set_entityId_18(int32_t value)
	{
		___entityId_18 = value;
	}

	inline static int32_t get_offset_of_eolNormalized_19() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___eolNormalized_19)); }
	inline bool get_eolNormalized_19() const { return ___eolNormalized_19; }
	inline bool* get_address_of_eolNormalized_19() { return &___eolNormalized_19; }
	inline void set_eolNormalized_19(bool value)
	{
		___eolNormalized_19 = value;
	}

	inline static int32_t get_offset_of_entityResolvedManually_20() { return static_cast<int32_t>(offsetof(ParsingState_t1780334922, ___entityResolvedManually_20)); }
	inline bool get_entityResolvedManually_20() const { return ___entityResolvedManually_20; }
	inline bool* get_address_of_entityResolvedManually_20() { return &___entityResolvedManually_20; }
	inline void set_entityResolvedManually_20(bool value)
	{
		___entityResolvedManually_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlTextReaderImpl/ParsingState
struct ParsingState_t1780334922_marshaled_pinvoke
{
	uint8_t* ___chars_0;
	int32_t ___charPos_1;
	int32_t ___charsUsed_2;
	Encoding_t1523322056 * ___encoding_3;
	int32_t ___appendMode_4;
	Stream_t1273022909 * ___stream_5;
	Decoder_t2204182725 * ___decoder_6;
	uint8_t* ___bytes_7;
	int32_t ___bytePos_8;
	int32_t ___bytesUsed_9;
	TextReader_t283511965 * ___textReader_10;
	int32_t ___lineNo_11;
	int32_t ___lineStartPos_12;
	char* ___baseUriStr_13;
	Uri_t100236324 * ___baseUri_14;
	int32_t ___isEof_15;
	int32_t ___isStreamEof_16;
	RuntimeObject* ___entity_17;
	int32_t ___entityId_18;
	int32_t ___eolNormalized_19;
	int32_t ___entityResolvedManually_20;
};
// Native definition for COM marshalling of System.Xml.XmlTextReaderImpl/ParsingState
struct ParsingState_t1780334922_marshaled_com
{
	uint8_t* ___chars_0;
	int32_t ___charPos_1;
	int32_t ___charsUsed_2;
	Encoding_t1523322056 * ___encoding_3;
	int32_t ___appendMode_4;
	Stream_t1273022909 * ___stream_5;
	Decoder_t2204182725 * ___decoder_6;
	uint8_t* ___bytes_7;
	int32_t ___bytePos_8;
	int32_t ___bytesUsed_9;
	TextReader_t283511965 * ___textReader_10;
	int32_t ___lineNo_11;
	int32_t ___lineStartPos_12;
	Il2CppChar* ___baseUriStr_13;
	Uri_t100236324 * ___baseUri_14;
	int32_t ___isEof_15;
	int32_t ___isStreamEof_16;
	RuntimeObject* ___entity_17;
	int32_t ___entityId_18;
	int32_t ___eolNormalized_19;
	int32_t ___entityResolvedManually_20;
};
#endif // PARSINGSTATE_T1780334922_H
#ifndef NONAMESPACEMANAGER_T2350683444_H
#define NONAMESPACEMANAGER_T2350683444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/NoNamespaceManager
struct  NoNamespaceManager_t2350683444  : public XmlNamespaceManager_t418790500
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONAMESPACEMANAGER_T2350683444_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef XMLATTRIBUTE_T1173852259_H
#define XMLATTRIBUTE_T1173852259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAttribute
struct  XmlAttribute_t1173852259  : public XmlNode_t3767805227
{
public:
	// System.Xml.XmlName System.Xml.XmlAttribute::name
	XmlName_t4150142242 * ___name_1;
	// System.Xml.XmlLinkedNode System.Xml.XmlAttribute::lastChild
	XmlLinkedNode_t1437094927 * ___lastChild_2;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(XmlAttribute_t1173852259, ___name_1)); }
	inline XmlName_t4150142242 * get_name_1() const { return ___name_1; }
	inline XmlName_t4150142242 ** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(XmlName_t4150142242 * value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_lastChild_2() { return static_cast<int32_t>(offsetof(XmlAttribute_t1173852259, ___lastChild_2)); }
	inline XmlLinkedNode_t1437094927 * get_lastChild_2() const { return ___lastChild_2; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastChild_2() { return &___lastChild_2; }
	inline void set_lastChild_2(XmlLinkedNode_t1437094927 * value)
	{
		___lastChild_2 = value;
		Il2CppCodeGenWriteBarrier((&___lastChild_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTE_T1173852259_H
#ifndef XMLCHILDNODES_T2906253626_H
#define XMLCHILDNODES_T2906253626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlChildNodes
struct  XmlChildNodes_t2906253626  : public XmlNodeList_t2551693786
{
public:
	// System.Xml.XmlNode System.Xml.XmlChildNodes::container
	XmlNode_t3767805227 * ___container_0;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(XmlChildNodes_t2906253626, ___container_0)); }
	inline XmlNode_t3767805227 * get_container_0() const { return ___container_0; }
	inline XmlNode_t3767805227 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(XmlNode_t3767805227 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHILDNODES_T2906253626_H
#ifndef ATTRNAME_T2097328179_H
#define ATTRNAME_T2097328179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/AttrName
struct  AttrName_t2097328179 
{
public:
	// System.String System.Xml.XmlWellFormedWriter/AttrName::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlWellFormedWriter/AttrName::namespaceUri
	String_t* ___namespaceUri_1;
	// System.String System.Xml.XmlWellFormedWriter/AttrName::localName
	String_t* ___localName_2;
	// System.Int32 System.Xml.XmlWellFormedWriter/AttrName::prev
	int32_t ___prev_3;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(AttrName_t2097328179, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_namespaceUri_1() { return static_cast<int32_t>(offsetof(AttrName_t2097328179, ___namespaceUri_1)); }
	inline String_t* get_namespaceUri_1() const { return ___namespaceUri_1; }
	inline String_t** get_address_of_namespaceUri_1() { return &___namespaceUri_1; }
	inline void set_namespaceUri_1(String_t* value)
	{
		___namespaceUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_1), value);
	}

	inline static int32_t get_offset_of_localName_2() { return static_cast<int32_t>(offsetof(AttrName_t2097328179, ___localName_2)); }
	inline String_t* get_localName_2() const { return ___localName_2; }
	inline String_t** get_address_of_localName_2() { return &___localName_2; }
	inline void set_localName_2(String_t* value)
	{
		___localName_2 = value;
		Il2CppCodeGenWriteBarrier((&___localName_2), value);
	}

	inline static int32_t get_offset_of_prev_3() { return static_cast<int32_t>(offsetof(AttrName_t2097328179, ___prev_3)); }
	inline int32_t get_prev_3() const { return ___prev_3; }
	inline int32_t* get_address_of_prev_3() { return &___prev_3; }
	inline void set_prev_3(int32_t value)
	{
		___prev_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlWellFormedWriter/AttrName
struct AttrName_t2097328179_marshaled_pinvoke
{
	char* ___prefix_0;
	char* ___namespaceUri_1;
	char* ___localName_2;
	int32_t ___prev_3;
};
// Native definition for COM marshalling of System.Xml.XmlWellFormedWriter/AttrName
struct AttrName_t2097328179_marshaled_com
{
	Il2CppChar* ___prefix_0;
	Il2CppChar* ___namespaceUri_1;
	Il2CppChar* ___localName_2;
	int32_t ___prev_3;
};
#endif // ATTRNAME_T2097328179_H
#ifndef XMLVALIDATINGREADER_T1719295192_H
#define XMLVALIDATINGREADER_T1719295192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlValidatingReader
struct  XmlValidatingReader_t1719295192  : public XmlReader_t3121518892
{
public:
	// System.Xml.XmlValidatingReaderImpl System.Xml.XmlValidatingReader::impl
	XmlValidatingReaderImpl_t1817762384 * ___impl_3;

public:
	inline static int32_t get_offset_of_impl_3() { return static_cast<int32_t>(offsetof(XmlValidatingReader_t1719295192, ___impl_3)); }
	inline XmlValidatingReaderImpl_t1817762384 * get_impl_3() const { return ___impl_3; }
	inline XmlValidatingReaderImpl_t1817762384 ** get_address_of_impl_3() { return &___impl_3; }
	inline void set_impl_3(XmlValidatingReaderImpl_t1817762384 * value)
	{
		___impl_3 = value;
		Il2CppCodeGenWriteBarrier((&___impl_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLVALIDATINGREADER_T1719295192_H
#ifndef XMLDOCUMENTFRAGMENT_T1323348855_H
#define XMLDOCUMENTFRAGMENT_T1323348855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocumentFragment
struct  XmlDocumentFragment_t1323348855  : public XmlNode_t3767805227
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlDocumentFragment::lastChild
	XmlLinkedNode_t1437094927 * ___lastChild_1;

public:
	inline static int32_t get_offset_of_lastChild_1() { return static_cast<int32_t>(offsetof(XmlDocumentFragment_t1323348855, ___lastChild_1)); }
	inline XmlLinkedNode_t1437094927 * get_lastChild_1() const { return ___lastChild_1; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastChild_1() { return &___lastChild_1; }
	inline void set_lastChild_1(XmlLinkedNode_t1437094927 * value)
	{
		___lastChild_1 = value;
		Il2CppCodeGenWriteBarrier((&___lastChild_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTFRAGMENT_T1323348855_H
#ifndef XMLENTITY_T3308518401_H
#define XMLENTITY_T3308518401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntity
struct  XmlEntity_t3308518401  : public XmlNode_t3767805227
{
public:
	// System.String System.Xml.XmlEntity::publicId
	String_t* ___publicId_1;
	// System.String System.Xml.XmlEntity::systemId
	String_t* ___systemId_2;
	// System.String System.Xml.XmlEntity::notationName
	String_t* ___notationName_3;
	// System.String System.Xml.XmlEntity::name
	String_t* ___name_4;
	// System.String System.Xml.XmlEntity::unparsedReplacementStr
	String_t* ___unparsedReplacementStr_5;
	// System.String System.Xml.XmlEntity::baseURI
	String_t* ___baseURI_6;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntity::lastChild
	XmlLinkedNode_t1437094927 * ___lastChild_7;
	// System.Boolean System.Xml.XmlEntity::childrenFoliating
	bool ___childrenFoliating_8;

public:
	inline static int32_t get_offset_of_publicId_1() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___publicId_1)); }
	inline String_t* get_publicId_1() const { return ___publicId_1; }
	inline String_t** get_address_of_publicId_1() { return &___publicId_1; }
	inline void set_publicId_1(String_t* value)
	{
		___publicId_1 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_1), value);
	}

	inline static int32_t get_offset_of_systemId_2() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___systemId_2)); }
	inline String_t* get_systemId_2() const { return ___systemId_2; }
	inline String_t** get_address_of_systemId_2() { return &___systemId_2; }
	inline void set_systemId_2(String_t* value)
	{
		___systemId_2 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_2), value);
	}

	inline static int32_t get_offset_of_notationName_3() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___notationName_3)); }
	inline String_t* get_notationName_3() const { return ___notationName_3; }
	inline String_t** get_address_of_notationName_3() { return &___notationName_3; }
	inline void set_notationName_3(String_t* value)
	{
		___notationName_3 = value;
		Il2CppCodeGenWriteBarrier((&___notationName_3), value);
	}

	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier((&___name_4), value);
	}

	inline static int32_t get_offset_of_unparsedReplacementStr_5() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___unparsedReplacementStr_5)); }
	inline String_t* get_unparsedReplacementStr_5() const { return ___unparsedReplacementStr_5; }
	inline String_t** get_address_of_unparsedReplacementStr_5() { return &___unparsedReplacementStr_5; }
	inline void set_unparsedReplacementStr_5(String_t* value)
	{
		___unparsedReplacementStr_5 = value;
		Il2CppCodeGenWriteBarrier((&___unparsedReplacementStr_5), value);
	}

	inline static int32_t get_offset_of_baseURI_6() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___baseURI_6)); }
	inline String_t* get_baseURI_6() const { return ___baseURI_6; }
	inline String_t** get_address_of_baseURI_6() { return &___baseURI_6; }
	inline void set_baseURI_6(String_t* value)
	{
		___baseURI_6 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_6), value);
	}

	inline static int32_t get_offset_of_lastChild_7() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___lastChild_7)); }
	inline XmlLinkedNode_t1437094927 * get_lastChild_7() const { return ___lastChild_7; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastChild_7() { return &___lastChild_7; }
	inline void set_lastChild_7(XmlLinkedNode_t1437094927 * value)
	{
		___lastChild_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastChild_7), value);
	}

	inline static int32_t get_offset_of_childrenFoliating_8() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___childrenFoliating_8)); }
	inline bool get_childrenFoliating_8() const { return ___childrenFoliating_8; }
	inline bool* get_address_of_childrenFoliating_8() { return &___childrenFoliating_8; }
	inline void set_childrenFoliating_8(bool value)
	{
		___childrenFoliating_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENTITY_T3308518401_H
#ifndef NAMESPACE_T2218256516_H
#define NAMESPACE_T2218256516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/Namespace
struct  Namespace_t2218256516 
{
public:
	// System.String System.Xml.XmlTextWriter/Namespace::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlTextWriter/Namespace::ns
	String_t* ___ns_1;
	// System.Boolean System.Xml.XmlTextWriter/Namespace::declared
	bool ___declared_2;
	// System.Int32 System.Xml.XmlTextWriter/Namespace::prevNsIndex
	int32_t ___prevNsIndex_3;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(Namespace_t2218256516, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(Namespace_t2218256516, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier((&___ns_1), value);
	}

	inline static int32_t get_offset_of_declared_2() { return static_cast<int32_t>(offsetof(Namespace_t2218256516, ___declared_2)); }
	inline bool get_declared_2() const { return ___declared_2; }
	inline bool* get_address_of_declared_2() { return &___declared_2; }
	inline void set_declared_2(bool value)
	{
		___declared_2 = value;
	}

	inline static int32_t get_offset_of_prevNsIndex_3() { return static_cast<int32_t>(offsetof(Namespace_t2218256516, ___prevNsIndex_3)); }
	inline int32_t get_prevNsIndex_3() const { return ___prevNsIndex_3; }
	inline int32_t* get_address_of_prevNsIndex_3() { return &___prevNsIndex_3; }
	inline void set_prevNsIndex_3(int32_t value)
	{
		___prevNsIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlTextWriter/Namespace
struct Namespace_t2218256516_marshaled_pinvoke
{
	char* ___prefix_0;
	char* ___ns_1;
	int32_t ___declared_2;
	int32_t ___prevNsIndex_3;
};
// Native definition for COM marshalling of System.Xml.XmlTextWriter/Namespace
struct Namespace_t2218256516_marshaled_com
{
	Il2CppChar* ___prefix_0;
	Il2CppChar* ___ns_1;
	int32_t ___declared_2;
	int32_t ___prevNsIndex_3;
};
#endif // NAMESPACE_T2218256516_H
#ifndef XMLDOCUMENT_T2837193595_H
#define XMLDOCUMENT_T2837193595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocument
struct  XmlDocument_t2837193595  : public XmlNode_t3767805227
{
public:
	// System.Xml.XmlImplementation System.Xml.XmlDocument::implementation
	XmlImplementation_t254178875 * ___implementation_1;
	// System.Xml.DomNameTable System.Xml.XmlDocument::domNameTable
	DomNameTable_t751058560 * ___domNameTable_2;
	// System.Xml.XmlLinkedNode System.Xml.XmlDocument::lastChild
	XmlLinkedNode_t1437094927 * ___lastChild_3;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocument::entities
	XmlNamedNodeMap_t2821286253 * ___entities_4;
	// System.Collections.Hashtable System.Xml.XmlDocument::htElementIdMap
	Hashtable_t1853889766 * ___htElementIdMap_5;
	// System.Collections.Hashtable System.Xml.XmlDocument::htElementIDAttrDecl
	Hashtable_t1853889766 * ___htElementIDAttrDecl_6;
	// System.Xml.Schema.SchemaInfo System.Xml.XmlDocument::schemaInfo
	SchemaInfo_t3347508623 * ___schemaInfo_7;
	// System.Xml.Schema.XmlSchemaSet System.Xml.XmlDocument::schemas
	XmlSchemaSet_t266093086 * ___schemas_8;
	// System.Boolean System.Xml.XmlDocument::reportValidity
	bool ___reportValidity_9;
	// System.Boolean System.Xml.XmlDocument::actualLoadingStatus
	bool ___actualLoadingStatus_10;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeInsertingDelegate
	XmlNodeChangedEventHandler_t1533444722 * ___onNodeInsertingDelegate_11;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeInsertedDelegate
	XmlNodeChangedEventHandler_t1533444722 * ___onNodeInsertedDelegate_12;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeRemovingDelegate
	XmlNodeChangedEventHandler_t1533444722 * ___onNodeRemovingDelegate_13;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeRemovedDelegate
	XmlNodeChangedEventHandler_t1533444722 * ___onNodeRemovedDelegate_14;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeChangingDelegate
	XmlNodeChangedEventHandler_t1533444722 * ___onNodeChangingDelegate_15;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeChangedDelegate
	XmlNodeChangedEventHandler_t1533444722 * ___onNodeChangedDelegate_16;
	// System.Boolean System.Xml.XmlDocument::fEntRefNodesPresent
	bool ___fEntRefNodesPresent_17;
	// System.Boolean System.Xml.XmlDocument::fCDataNodesPresent
	bool ___fCDataNodesPresent_18;
	// System.Boolean System.Xml.XmlDocument::preserveWhitespace
	bool ___preserveWhitespace_19;
	// System.Boolean System.Xml.XmlDocument::isLoading
	bool ___isLoading_20;
	// System.String System.Xml.XmlDocument::strDocumentName
	String_t* ___strDocumentName_21;
	// System.String System.Xml.XmlDocument::strDocumentFragmentName
	String_t* ___strDocumentFragmentName_22;
	// System.String System.Xml.XmlDocument::strCommentName
	String_t* ___strCommentName_23;
	// System.String System.Xml.XmlDocument::strTextName
	String_t* ___strTextName_24;
	// System.String System.Xml.XmlDocument::strCDataSectionName
	String_t* ___strCDataSectionName_25;
	// System.String System.Xml.XmlDocument::strEntityName
	String_t* ___strEntityName_26;
	// System.String System.Xml.XmlDocument::strID
	String_t* ___strID_27;
	// System.String System.Xml.XmlDocument::strXmlns
	String_t* ___strXmlns_28;
	// System.String System.Xml.XmlDocument::strXml
	String_t* ___strXml_29;
	// System.String System.Xml.XmlDocument::strSpace
	String_t* ___strSpace_30;
	// System.String System.Xml.XmlDocument::strLang
	String_t* ___strLang_31;
	// System.String System.Xml.XmlDocument::strEmpty
	String_t* ___strEmpty_32;
	// System.String System.Xml.XmlDocument::strNonSignificantWhitespaceName
	String_t* ___strNonSignificantWhitespaceName_33;
	// System.String System.Xml.XmlDocument::strSignificantWhitespaceName
	String_t* ___strSignificantWhitespaceName_34;
	// System.String System.Xml.XmlDocument::strReservedXmlns
	String_t* ___strReservedXmlns_35;
	// System.String System.Xml.XmlDocument::strReservedXml
	String_t* ___strReservedXml_36;
	// System.String System.Xml.XmlDocument::baseURI
	String_t* ___baseURI_37;
	// System.Xml.XmlResolver System.Xml.XmlDocument::resolver
	XmlResolver_t626023767 * ___resolver_38;
	// System.Boolean System.Xml.XmlDocument::bSetResolver
	bool ___bSetResolver_39;
	// System.Object System.Xml.XmlDocument::objLock
	RuntimeObject * ___objLock_40;

public:
	inline static int32_t get_offset_of_implementation_1() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___implementation_1)); }
	inline XmlImplementation_t254178875 * get_implementation_1() const { return ___implementation_1; }
	inline XmlImplementation_t254178875 ** get_address_of_implementation_1() { return &___implementation_1; }
	inline void set_implementation_1(XmlImplementation_t254178875 * value)
	{
		___implementation_1 = value;
		Il2CppCodeGenWriteBarrier((&___implementation_1), value);
	}

	inline static int32_t get_offset_of_domNameTable_2() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___domNameTable_2)); }
	inline DomNameTable_t751058560 * get_domNameTable_2() const { return ___domNameTable_2; }
	inline DomNameTable_t751058560 ** get_address_of_domNameTable_2() { return &___domNameTable_2; }
	inline void set_domNameTable_2(DomNameTable_t751058560 * value)
	{
		___domNameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___domNameTable_2), value);
	}

	inline static int32_t get_offset_of_lastChild_3() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___lastChild_3)); }
	inline XmlLinkedNode_t1437094927 * get_lastChild_3() const { return ___lastChild_3; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastChild_3() { return &___lastChild_3; }
	inline void set_lastChild_3(XmlLinkedNode_t1437094927 * value)
	{
		___lastChild_3 = value;
		Il2CppCodeGenWriteBarrier((&___lastChild_3), value);
	}

	inline static int32_t get_offset_of_entities_4() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___entities_4)); }
	inline XmlNamedNodeMap_t2821286253 * get_entities_4() const { return ___entities_4; }
	inline XmlNamedNodeMap_t2821286253 ** get_address_of_entities_4() { return &___entities_4; }
	inline void set_entities_4(XmlNamedNodeMap_t2821286253 * value)
	{
		___entities_4 = value;
		Il2CppCodeGenWriteBarrier((&___entities_4), value);
	}

	inline static int32_t get_offset_of_htElementIdMap_5() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___htElementIdMap_5)); }
	inline Hashtable_t1853889766 * get_htElementIdMap_5() const { return ___htElementIdMap_5; }
	inline Hashtable_t1853889766 ** get_address_of_htElementIdMap_5() { return &___htElementIdMap_5; }
	inline void set_htElementIdMap_5(Hashtable_t1853889766 * value)
	{
		___htElementIdMap_5 = value;
		Il2CppCodeGenWriteBarrier((&___htElementIdMap_5), value);
	}

	inline static int32_t get_offset_of_htElementIDAttrDecl_6() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___htElementIDAttrDecl_6)); }
	inline Hashtable_t1853889766 * get_htElementIDAttrDecl_6() const { return ___htElementIDAttrDecl_6; }
	inline Hashtable_t1853889766 ** get_address_of_htElementIDAttrDecl_6() { return &___htElementIDAttrDecl_6; }
	inline void set_htElementIDAttrDecl_6(Hashtable_t1853889766 * value)
	{
		___htElementIDAttrDecl_6 = value;
		Il2CppCodeGenWriteBarrier((&___htElementIDAttrDecl_6), value);
	}

	inline static int32_t get_offset_of_schemaInfo_7() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___schemaInfo_7)); }
	inline SchemaInfo_t3347508623 * get_schemaInfo_7() const { return ___schemaInfo_7; }
	inline SchemaInfo_t3347508623 ** get_address_of_schemaInfo_7() { return &___schemaInfo_7; }
	inline void set_schemaInfo_7(SchemaInfo_t3347508623 * value)
	{
		___schemaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_7), value);
	}

	inline static int32_t get_offset_of_schemas_8() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___schemas_8)); }
	inline XmlSchemaSet_t266093086 * get_schemas_8() const { return ___schemas_8; }
	inline XmlSchemaSet_t266093086 ** get_address_of_schemas_8() { return &___schemas_8; }
	inline void set_schemas_8(XmlSchemaSet_t266093086 * value)
	{
		___schemas_8 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_8), value);
	}

	inline static int32_t get_offset_of_reportValidity_9() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___reportValidity_9)); }
	inline bool get_reportValidity_9() const { return ___reportValidity_9; }
	inline bool* get_address_of_reportValidity_9() { return &___reportValidity_9; }
	inline void set_reportValidity_9(bool value)
	{
		___reportValidity_9 = value;
	}

	inline static int32_t get_offset_of_actualLoadingStatus_10() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___actualLoadingStatus_10)); }
	inline bool get_actualLoadingStatus_10() const { return ___actualLoadingStatus_10; }
	inline bool* get_address_of_actualLoadingStatus_10() { return &___actualLoadingStatus_10; }
	inline void set_actualLoadingStatus_10(bool value)
	{
		___actualLoadingStatus_10 = value;
	}

	inline static int32_t get_offset_of_onNodeInsertingDelegate_11() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___onNodeInsertingDelegate_11)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_onNodeInsertingDelegate_11() const { return ___onNodeInsertingDelegate_11; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_onNodeInsertingDelegate_11() { return &___onNodeInsertingDelegate_11; }
	inline void set_onNodeInsertingDelegate_11(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___onNodeInsertingDelegate_11 = value;
		Il2CppCodeGenWriteBarrier((&___onNodeInsertingDelegate_11), value);
	}

	inline static int32_t get_offset_of_onNodeInsertedDelegate_12() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___onNodeInsertedDelegate_12)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_onNodeInsertedDelegate_12() const { return ___onNodeInsertedDelegate_12; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_onNodeInsertedDelegate_12() { return &___onNodeInsertedDelegate_12; }
	inline void set_onNodeInsertedDelegate_12(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___onNodeInsertedDelegate_12 = value;
		Il2CppCodeGenWriteBarrier((&___onNodeInsertedDelegate_12), value);
	}

	inline static int32_t get_offset_of_onNodeRemovingDelegate_13() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___onNodeRemovingDelegate_13)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_onNodeRemovingDelegate_13() const { return ___onNodeRemovingDelegate_13; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_onNodeRemovingDelegate_13() { return &___onNodeRemovingDelegate_13; }
	inline void set_onNodeRemovingDelegate_13(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___onNodeRemovingDelegate_13 = value;
		Il2CppCodeGenWriteBarrier((&___onNodeRemovingDelegate_13), value);
	}

	inline static int32_t get_offset_of_onNodeRemovedDelegate_14() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___onNodeRemovedDelegate_14)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_onNodeRemovedDelegate_14() const { return ___onNodeRemovedDelegate_14; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_onNodeRemovedDelegate_14() { return &___onNodeRemovedDelegate_14; }
	inline void set_onNodeRemovedDelegate_14(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___onNodeRemovedDelegate_14 = value;
		Il2CppCodeGenWriteBarrier((&___onNodeRemovedDelegate_14), value);
	}

	inline static int32_t get_offset_of_onNodeChangingDelegate_15() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___onNodeChangingDelegate_15)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_onNodeChangingDelegate_15() const { return ___onNodeChangingDelegate_15; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_onNodeChangingDelegate_15() { return &___onNodeChangingDelegate_15; }
	inline void set_onNodeChangingDelegate_15(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___onNodeChangingDelegate_15 = value;
		Il2CppCodeGenWriteBarrier((&___onNodeChangingDelegate_15), value);
	}

	inline static int32_t get_offset_of_onNodeChangedDelegate_16() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___onNodeChangedDelegate_16)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_onNodeChangedDelegate_16() const { return ___onNodeChangedDelegate_16; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_onNodeChangedDelegate_16() { return &___onNodeChangedDelegate_16; }
	inline void set_onNodeChangedDelegate_16(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___onNodeChangedDelegate_16 = value;
		Il2CppCodeGenWriteBarrier((&___onNodeChangedDelegate_16), value);
	}

	inline static int32_t get_offset_of_fEntRefNodesPresent_17() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___fEntRefNodesPresent_17)); }
	inline bool get_fEntRefNodesPresent_17() const { return ___fEntRefNodesPresent_17; }
	inline bool* get_address_of_fEntRefNodesPresent_17() { return &___fEntRefNodesPresent_17; }
	inline void set_fEntRefNodesPresent_17(bool value)
	{
		___fEntRefNodesPresent_17 = value;
	}

	inline static int32_t get_offset_of_fCDataNodesPresent_18() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___fCDataNodesPresent_18)); }
	inline bool get_fCDataNodesPresent_18() const { return ___fCDataNodesPresent_18; }
	inline bool* get_address_of_fCDataNodesPresent_18() { return &___fCDataNodesPresent_18; }
	inline void set_fCDataNodesPresent_18(bool value)
	{
		___fCDataNodesPresent_18 = value;
	}

	inline static int32_t get_offset_of_preserveWhitespace_19() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___preserveWhitespace_19)); }
	inline bool get_preserveWhitespace_19() const { return ___preserveWhitespace_19; }
	inline bool* get_address_of_preserveWhitespace_19() { return &___preserveWhitespace_19; }
	inline void set_preserveWhitespace_19(bool value)
	{
		___preserveWhitespace_19 = value;
	}

	inline static int32_t get_offset_of_isLoading_20() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___isLoading_20)); }
	inline bool get_isLoading_20() const { return ___isLoading_20; }
	inline bool* get_address_of_isLoading_20() { return &___isLoading_20; }
	inline void set_isLoading_20(bool value)
	{
		___isLoading_20 = value;
	}

	inline static int32_t get_offset_of_strDocumentName_21() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strDocumentName_21)); }
	inline String_t* get_strDocumentName_21() const { return ___strDocumentName_21; }
	inline String_t** get_address_of_strDocumentName_21() { return &___strDocumentName_21; }
	inline void set_strDocumentName_21(String_t* value)
	{
		___strDocumentName_21 = value;
		Il2CppCodeGenWriteBarrier((&___strDocumentName_21), value);
	}

	inline static int32_t get_offset_of_strDocumentFragmentName_22() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strDocumentFragmentName_22)); }
	inline String_t* get_strDocumentFragmentName_22() const { return ___strDocumentFragmentName_22; }
	inline String_t** get_address_of_strDocumentFragmentName_22() { return &___strDocumentFragmentName_22; }
	inline void set_strDocumentFragmentName_22(String_t* value)
	{
		___strDocumentFragmentName_22 = value;
		Il2CppCodeGenWriteBarrier((&___strDocumentFragmentName_22), value);
	}

	inline static int32_t get_offset_of_strCommentName_23() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strCommentName_23)); }
	inline String_t* get_strCommentName_23() const { return ___strCommentName_23; }
	inline String_t** get_address_of_strCommentName_23() { return &___strCommentName_23; }
	inline void set_strCommentName_23(String_t* value)
	{
		___strCommentName_23 = value;
		Il2CppCodeGenWriteBarrier((&___strCommentName_23), value);
	}

	inline static int32_t get_offset_of_strTextName_24() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strTextName_24)); }
	inline String_t* get_strTextName_24() const { return ___strTextName_24; }
	inline String_t** get_address_of_strTextName_24() { return &___strTextName_24; }
	inline void set_strTextName_24(String_t* value)
	{
		___strTextName_24 = value;
		Il2CppCodeGenWriteBarrier((&___strTextName_24), value);
	}

	inline static int32_t get_offset_of_strCDataSectionName_25() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strCDataSectionName_25)); }
	inline String_t* get_strCDataSectionName_25() const { return ___strCDataSectionName_25; }
	inline String_t** get_address_of_strCDataSectionName_25() { return &___strCDataSectionName_25; }
	inline void set_strCDataSectionName_25(String_t* value)
	{
		___strCDataSectionName_25 = value;
		Il2CppCodeGenWriteBarrier((&___strCDataSectionName_25), value);
	}

	inline static int32_t get_offset_of_strEntityName_26() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strEntityName_26)); }
	inline String_t* get_strEntityName_26() const { return ___strEntityName_26; }
	inline String_t** get_address_of_strEntityName_26() { return &___strEntityName_26; }
	inline void set_strEntityName_26(String_t* value)
	{
		___strEntityName_26 = value;
		Il2CppCodeGenWriteBarrier((&___strEntityName_26), value);
	}

	inline static int32_t get_offset_of_strID_27() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strID_27)); }
	inline String_t* get_strID_27() const { return ___strID_27; }
	inline String_t** get_address_of_strID_27() { return &___strID_27; }
	inline void set_strID_27(String_t* value)
	{
		___strID_27 = value;
		Il2CppCodeGenWriteBarrier((&___strID_27), value);
	}

	inline static int32_t get_offset_of_strXmlns_28() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strXmlns_28)); }
	inline String_t* get_strXmlns_28() const { return ___strXmlns_28; }
	inline String_t** get_address_of_strXmlns_28() { return &___strXmlns_28; }
	inline void set_strXmlns_28(String_t* value)
	{
		___strXmlns_28 = value;
		Il2CppCodeGenWriteBarrier((&___strXmlns_28), value);
	}

	inline static int32_t get_offset_of_strXml_29() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strXml_29)); }
	inline String_t* get_strXml_29() const { return ___strXml_29; }
	inline String_t** get_address_of_strXml_29() { return &___strXml_29; }
	inline void set_strXml_29(String_t* value)
	{
		___strXml_29 = value;
		Il2CppCodeGenWriteBarrier((&___strXml_29), value);
	}

	inline static int32_t get_offset_of_strSpace_30() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strSpace_30)); }
	inline String_t* get_strSpace_30() const { return ___strSpace_30; }
	inline String_t** get_address_of_strSpace_30() { return &___strSpace_30; }
	inline void set_strSpace_30(String_t* value)
	{
		___strSpace_30 = value;
		Il2CppCodeGenWriteBarrier((&___strSpace_30), value);
	}

	inline static int32_t get_offset_of_strLang_31() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strLang_31)); }
	inline String_t* get_strLang_31() const { return ___strLang_31; }
	inline String_t** get_address_of_strLang_31() { return &___strLang_31; }
	inline void set_strLang_31(String_t* value)
	{
		___strLang_31 = value;
		Il2CppCodeGenWriteBarrier((&___strLang_31), value);
	}

	inline static int32_t get_offset_of_strEmpty_32() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strEmpty_32)); }
	inline String_t* get_strEmpty_32() const { return ___strEmpty_32; }
	inline String_t** get_address_of_strEmpty_32() { return &___strEmpty_32; }
	inline void set_strEmpty_32(String_t* value)
	{
		___strEmpty_32 = value;
		Il2CppCodeGenWriteBarrier((&___strEmpty_32), value);
	}

	inline static int32_t get_offset_of_strNonSignificantWhitespaceName_33() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strNonSignificantWhitespaceName_33)); }
	inline String_t* get_strNonSignificantWhitespaceName_33() const { return ___strNonSignificantWhitespaceName_33; }
	inline String_t** get_address_of_strNonSignificantWhitespaceName_33() { return &___strNonSignificantWhitespaceName_33; }
	inline void set_strNonSignificantWhitespaceName_33(String_t* value)
	{
		___strNonSignificantWhitespaceName_33 = value;
		Il2CppCodeGenWriteBarrier((&___strNonSignificantWhitespaceName_33), value);
	}

	inline static int32_t get_offset_of_strSignificantWhitespaceName_34() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strSignificantWhitespaceName_34)); }
	inline String_t* get_strSignificantWhitespaceName_34() const { return ___strSignificantWhitespaceName_34; }
	inline String_t** get_address_of_strSignificantWhitespaceName_34() { return &___strSignificantWhitespaceName_34; }
	inline void set_strSignificantWhitespaceName_34(String_t* value)
	{
		___strSignificantWhitespaceName_34 = value;
		Il2CppCodeGenWriteBarrier((&___strSignificantWhitespaceName_34), value);
	}

	inline static int32_t get_offset_of_strReservedXmlns_35() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strReservedXmlns_35)); }
	inline String_t* get_strReservedXmlns_35() const { return ___strReservedXmlns_35; }
	inline String_t** get_address_of_strReservedXmlns_35() { return &___strReservedXmlns_35; }
	inline void set_strReservedXmlns_35(String_t* value)
	{
		___strReservedXmlns_35 = value;
		Il2CppCodeGenWriteBarrier((&___strReservedXmlns_35), value);
	}

	inline static int32_t get_offset_of_strReservedXml_36() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___strReservedXml_36)); }
	inline String_t* get_strReservedXml_36() const { return ___strReservedXml_36; }
	inline String_t** get_address_of_strReservedXml_36() { return &___strReservedXml_36; }
	inline void set_strReservedXml_36(String_t* value)
	{
		___strReservedXml_36 = value;
		Il2CppCodeGenWriteBarrier((&___strReservedXml_36), value);
	}

	inline static int32_t get_offset_of_baseURI_37() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___baseURI_37)); }
	inline String_t* get_baseURI_37() const { return ___baseURI_37; }
	inline String_t** get_address_of_baseURI_37() { return &___baseURI_37; }
	inline void set_baseURI_37(String_t* value)
	{
		___baseURI_37 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_37), value);
	}

	inline static int32_t get_offset_of_resolver_38() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___resolver_38)); }
	inline XmlResolver_t626023767 * get_resolver_38() const { return ___resolver_38; }
	inline XmlResolver_t626023767 ** get_address_of_resolver_38() { return &___resolver_38; }
	inline void set_resolver_38(XmlResolver_t626023767 * value)
	{
		___resolver_38 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_38), value);
	}

	inline static int32_t get_offset_of_bSetResolver_39() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___bSetResolver_39)); }
	inline bool get_bSetResolver_39() const { return ___bSetResolver_39; }
	inline bool* get_address_of_bSetResolver_39() { return &___bSetResolver_39; }
	inline void set_bSetResolver_39(bool value)
	{
		___bSetResolver_39 = value;
	}

	inline static int32_t get_offset_of_objLock_40() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___objLock_40)); }
	inline RuntimeObject * get_objLock_40() const { return ___objLock_40; }
	inline RuntimeObject ** get_address_of_objLock_40() { return &___objLock_40; }
	inline void set_objLock_40(RuntimeObject * value)
	{
		___objLock_40 = value;
		Il2CppCodeGenWriteBarrier((&___objLock_40), value);
	}
};

struct XmlDocument_t2837193595_StaticFields
{
public:
	// System.Xml.EmptyEnumerator System.Xml.XmlDocument::EmptyEnumerator
	EmptyEnumerator_t1830168813 * ___EmptyEnumerator_41;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::NotKnownSchemaInfo
	RuntimeObject* ___NotKnownSchemaInfo_42;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::ValidSchemaInfo
	RuntimeObject* ___ValidSchemaInfo_43;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::InvalidSchemaInfo
	RuntimeObject* ___InvalidSchemaInfo_44;

public:
	inline static int32_t get_offset_of_EmptyEnumerator_41() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595_StaticFields, ___EmptyEnumerator_41)); }
	inline EmptyEnumerator_t1830168813 * get_EmptyEnumerator_41() const { return ___EmptyEnumerator_41; }
	inline EmptyEnumerator_t1830168813 ** get_address_of_EmptyEnumerator_41() { return &___EmptyEnumerator_41; }
	inline void set_EmptyEnumerator_41(EmptyEnumerator_t1830168813 * value)
	{
		___EmptyEnumerator_41 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyEnumerator_41), value);
	}

	inline static int32_t get_offset_of_NotKnownSchemaInfo_42() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595_StaticFields, ___NotKnownSchemaInfo_42)); }
	inline RuntimeObject* get_NotKnownSchemaInfo_42() const { return ___NotKnownSchemaInfo_42; }
	inline RuntimeObject** get_address_of_NotKnownSchemaInfo_42() { return &___NotKnownSchemaInfo_42; }
	inline void set_NotKnownSchemaInfo_42(RuntimeObject* value)
	{
		___NotKnownSchemaInfo_42 = value;
		Il2CppCodeGenWriteBarrier((&___NotKnownSchemaInfo_42), value);
	}

	inline static int32_t get_offset_of_ValidSchemaInfo_43() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595_StaticFields, ___ValidSchemaInfo_43)); }
	inline RuntimeObject* get_ValidSchemaInfo_43() const { return ___ValidSchemaInfo_43; }
	inline RuntimeObject** get_address_of_ValidSchemaInfo_43() { return &___ValidSchemaInfo_43; }
	inline void set_ValidSchemaInfo_43(RuntimeObject* value)
	{
		___ValidSchemaInfo_43 = value;
		Il2CppCodeGenWriteBarrier((&___ValidSchemaInfo_43), value);
	}

	inline static int32_t get_offset_of_InvalidSchemaInfo_44() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595_StaticFields, ___InvalidSchemaInfo_44)); }
	inline RuntimeObject* get_InvalidSchemaInfo_44() const { return ___InvalidSchemaInfo_44; }
	inline RuntimeObject** get_address_of_InvalidSchemaInfo_44() { return &___InvalidSchemaInfo_44; }
	inline void set_InvalidSchemaInfo_44(RuntimeObject* value)
	{
		___InvalidSchemaInfo_44 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidSchemaInfo_44), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENT_T2837193595_H
#ifndef LINEINFO_T3266778363_H
#define LINEINFO_T3266778363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.LineInfo
struct  LineInfo_t3266778363 
{
public:
	// System.Int32 System.Xml.LineInfo::lineNo
	int32_t ___lineNo_0;
	// System.Int32 System.Xml.LineInfo::linePos
	int32_t ___linePos_1;

public:
	inline static int32_t get_offset_of_lineNo_0() { return static_cast<int32_t>(offsetof(LineInfo_t3266778363, ___lineNo_0)); }
	inline int32_t get_lineNo_0() const { return ___lineNo_0; }
	inline int32_t* get_address_of_lineNo_0() { return &___lineNo_0; }
	inline void set_lineNo_0(int32_t value)
	{
		___lineNo_0 = value;
	}

	inline static int32_t get_offset_of_linePos_1() { return static_cast<int32_t>(offsetof(LineInfo_t3266778363, ___linePos_1)); }
	inline int32_t get_linePos_1() const { return ___linePos_1; }
	inline int32_t* get_address_of_linePos_1() { return &___linePos_1; }
	inline void set_linePos_1(int32_t value)
	{
		___linePos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEINFO_T3266778363_H
#ifndef XMLRAWWRITER_T722320575_H
#define XMLRAWWRITER_T722320575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlRawWriter
struct  XmlRawWriter_t722320575  : public XmlWriter_t127905479
{
public:
	// System.Xml.XmlRawWriterBase64Encoder System.Xml.XmlRawWriter::base64Encoder
	XmlRawWriterBase64Encoder_t1905751661 * ___base64Encoder_1;
	// System.Xml.IXmlNamespaceResolver System.Xml.XmlRawWriter::resolver
	RuntimeObject* ___resolver_2;

public:
	inline static int32_t get_offset_of_base64Encoder_1() { return static_cast<int32_t>(offsetof(XmlRawWriter_t722320575, ___base64Encoder_1)); }
	inline XmlRawWriterBase64Encoder_t1905751661 * get_base64Encoder_1() const { return ___base64Encoder_1; }
	inline XmlRawWriterBase64Encoder_t1905751661 ** get_address_of_base64Encoder_1() { return &___base64Encoder_1; }
	inline void set_base64Encoder_1(XmlRawWriterBase64Encoder_t1905751661 * value)
	{
		___base64Encoder_1 = value;
		Il2CppCodeGenWriteBarrier((&___base64Encoder_1), value);
	}

	inline static int32_t get_offset_of_resolver_2() { return static_cast<int32_t>(offsetof(XmlRawWriter_t722320575, ___resolver_2)); }
	inline RuntimeObject* get_resolver_2() const { return ___resolver_2; }
	inline RuntimeObject** get_address_of_resolver_2() { return &___resolver_2; }
	inline void set_resolver_2(RuntimeObject* value)
	{
		___resolver_2 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLRAWWRITER_T722320575_H
#ifndef NULLABLE_1_T1819850047_H
#define NULLABLE_1_T1819850047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t1819850047 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1819850047_H
#ifndef SMALLXMLNODELIST_T242297918_H
#define SMALLXMLNODELIST_T242297918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamedNodeMap/SmallXmlNodeList
struct  SmallXmlNodeList_t242297918 
{
public:
	// System.Object System.Xml.XmlNamedNodeMap/SmallXmlNodeList::field
	RuntimeObject * ___field_0;

public:
	inline static int32_t get_offset_of_field_0() { return static_cast<int32_t>(offsetof(SmallXmlNodeList_t242297918, ___field_0)); }
	inline RuntimeObject * get_field_0() const { return ___field_0; }
	inline RuntimeObject ** get_address_of_field_0() { return &___field_0; }
	inline void set_field_0(RuntimeObject * value)
	{
		___field_0 = value;
		Il2CppCodeGenWriteBarrier((&___field_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlNamedNodeMap/SmallXmlNodeList
struct SmallXmlNodeList_t242297918_marshaled_pinvoke
{
	Il2CppIUnknown* ___field_0;
};
// Native definition for COM marshalling of System.Xml.XmlNamedNodeMap/SmallXmlNodeList
struct SmallXmlNodeList_t242297918_marshaled_com
{
	Il2CppIUnknown* ___field_0;
};
#endif // SMALLXMLNODELIST_T242297918_H
#ifndef XMLASYNCCHECKWRITER_T4057126699_H
#define XMLASYNCCHECKWRITER_T4057126699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAsyncCheckWriter
struct  XmlAsyncCheckWriter_t4057126699  : public XmlWriter_t127905479
{
public:
	// System.Xml.XmlWriter System.Xml.XmlAsyncCheckWriter::coreWriter
	XmlWriter_t127905479 * ___coreWriter_1;
	// System.Threading.Tasks.Task System.Xml.XmlAsyncCheckWriter::lastTask
	Task_t3187275312 * ___lastTask_2;

public:
	inline static int32_t get_offset_of_coreWriter_1() { return static_cast<int32_t>(offsetof(XmlAsyncCheckWriter_t4057126699, ___coreWriter_1)); }
	inline XmlWriter_t127905479 * get_coreWriter_1() const { return ___coreWriter_1; }
	inline XmlWriter_t127905479 ** get_address_of_coreWriter_1() { return &___coreWriter_1; }
	inline void set_coreWriter_1(XmlWriter_t127905479 * value)
	{
		___coreWriter_1 = value;
		Il2CppCodeGenWriteBarrier((&___coreWriter_1), value);
	}

	inline static int32_t get_offset_of_lastTask_2() { return static_cast<int32_t>(offsetof(XmlAsyncCheckWriter_t4057126699, ___lastTask_2)); }
	inline Task_t3187275312 * get_lastTask_2() const { return ___lastTask_2; }
	inline Task_t3187275312 ** get_address_of_lastTask_2() { return &___lastTask_2; }
	inline void set_lastTask_2(Task_t3187275312 * value)
	{
		___lastTask_2 = value;
		Il2CppCodeGenWriteBarrier((&___lastTask_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLASYNCCHECKWRITER_T4057126699_H
#ifndef INCREMENTALREADDUMMYDECODER_T2572367147_H
#define INCREMENTALREADDUMMYDECODER_T2572367147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.IncrementalReadDummyDecoder
struct  IncrementalReadDummyDecoder_t2572367147  : public IncrementalReadDecoder_t3011954239
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INCREMENTALREADDUMMYDECODER_T2572367147_H
#ifndef XMLCHARTYPE_T2277243275_H
#define XMLCHARTYPE_T2277243275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharType
struct  XmlCharType_t2277243275 
{
public:
	// System.Byte[] System.Xml.XmlCharType::charProperties
	ByteU5BU5D_t4116647657* ___charProperties_2;

public:
	inline static int32_t get_offset_of_charProperties_2() { return static_cast<int32_t>(offsetof(XmlCharType_t2277243275, ___charProperties_2)); }
	inline ByteU5BU5D_t4116647657* get_charProperties_2() const { return ___charProperties_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_charProperties_2() { return &___charProperties_2; }
	inline void set_charProperties_2(ByteU5BU5D_t4116647657* value)
	{
		___charProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___charProperties_2), value);
	}
};

struct XmlCharType_t2277243275_StaticFields
{
public:
	// System.Object System.Xml.XmlCharType::s_Lock
	RuntimeObject * ___s_Lock_0;
	// System.Byte[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlCharType::s_CharProperties
	ByteU5BU5D_t4116647657* ___s_CharProperties_1;

public:
	inline static int32_t get_offset_of_s_Lock_0() { return static_cast<int32_t>(offsetof(XmlCharType_t2277243275_StaticFields, ___s_Lock_0)); }
	inline RuntimeObject * get_s_Lock_0() const { return ___s_Lock_0; }
	inline RuntimeObject ** get_address_of_s_Lock_0() { return &___s_Lock_0; }
	inline void set_s_Lock_0(RuntimeObject * value)
	{
		___s_Lock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Lock_0), value);
	}

	inline static int32_t get_offset_of_s_CharProperties_1() { return static_cast<int32_t>(offsetof(XmlCharType_t2277243275_StaticFields, ___s_CharProperties_1)); }
	inline ByteU5BU5D_t4116647657* get_s_CharProperties_1() const { return ___s_CharProperties_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_s_CharProperties_1() { return &___s_CharProperties_1; }
	inline void set_s_CharProperties_1(ByteU5BU5D_t4116647657* value)
	{
		___s_CharProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CharProperties_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlCharType
struct XmlCharType_t2277243275_marshaled_pinvoke
{
	uint8_t* ___charProperties_2;
};
// Native definition for COM marshalling of System.Xml.XmlCharType
struct XmlCharType_t2277243275_marshaled_com
{
	uint8_t* ___charProperties_2;
};
#endif // XMLCHARTYPE_T2277243275_H
#ifndef XMLTEXTREADER_T4233384356_H
#define XMLTEXTREADER_T4233384356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReader
struct  XmlTextReader_t4233384356  : public XmlReader_t3121518892
{
public:
	// System.Xml.XmlTextReaderImpl System.Xml.XmlTextReader::impl
	XmlTextReaderImpl_t178060594 * ___impl_3;

public:
	inline static int32_t get_offset_of_impl_3() { return static_cast<int32_t>(offsetof(XmlTextReader_t4233384356, ___impl_3)); }
	inline XmlTextReaderImpl_t178060594 * get_impl_3() const { return ___impl_3; }
	inline XmlTextReaderImpl_t178060594 ** get_address_of_impl_3() { return &___impl_3; }
	inline void set_impl_3(XmlTextReaderImpl_t178060594 * value)
	{
		___impl_3 = value;
		Il2CppCodeGenWriteBarrier((&___impl_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTREADER_T4233384356_H
#ifndef STRINGCONCAT_T2171099760_H
#define STRINGCONCAT_T2171099760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Xsl.Runtime.StringConcat
struct  StringConcat_t2171099760 
{
public:
	// System.String System.Xml.Xsl.Runtime.StringConcat::s1
	String_t* ___s1_0;
	// System.String System.Xml.Xsl.Runtime.StringConcat::s2
	String_t* ___s2_1;
	// System.String System.Xml.Xsl.Runtime.StringConcat::s3
	String_t* ___s3_2;
	// System.String System.Xml.Xsl.Runtime.StringConcat::s4
	String_t* ___s4_3;
	// System.String System.Xml.Xsl.Runtime.StringConcat::delimiter
	String_t* ___delimiter_4;
	// System.Collections.Generic.List`1<System.String> System.Xml.Xsl.Runtime.StringConcat::strList
	List_1_t3319525431 * ___strList_5;
	// System.Int32 System.Xml.Xsl.Runtime.StringConcat::idxStr
	int32_t ___idxStr_6;

public:
	inline static int32_t get_offset_of_s1_0() { return static_cast<int32_t>(offsetof(StringConcat_t2171099760, ___s1_0)); }
	inline String_t* get_s1_0() const { return ___s1_0; }
	inline String_t** get_address_of_s1_0() { return &___s1_0; }
	inline void set_s1_0(String_t* value)
	{
		___s1_0 = value;
		Il2CppCodeGenWriteBarrier((&___s1_0), value);
	}

	inline static int32_t get_offset_of_s2_1() { return static_cast<int32_t>(offsetof(StringConcat_t2171099760, ___s2_1)); }
	inline String_t* get_s2_1() const { return ___s2_1; }
	inline String_t** get_address_of_s2_1() { return &___s2_1; }
	inline void set_s2_1(String_t* value)
	{
		___s2_1 = value;
		Il2CppCodeGenWriteBarrier((&___s2_1), value);
	}

	inline static int32_t get_offset_of_s3_2() { return static_cast<int32_t>(offsetof(StringConcat_t2171099760, ___s3_2)); }
	inline String_t* get_s3_2() const { return ___s3_2; }
	inline String_t** get_address_of_s3_2() { return &___s3_2; }
	inline void set_s3_2(String_t* value)
	{
		___s3_2 = value;
		Il2CppCodeGenWriteBarrier((&___s3_2), value);
	}

	inline static int32_t get_offset_of_s4_3() { return static_cast<int32_t>(offsetof(StringConcat_t2171099760, ___s4_3)); }
	inline String_t* get_s4_3() const { return ___s4_3; }
	inline String_t** get_address_of_s4_3() { return &___s4_3; }
	inline void set_s4_3(String_t* value)
	{
		___s4_3 = value;
		Il2CppCodeGenWriteBarrier((&___s4_3), value);
	}

	inline static int32_t get_offset_of_delimiter_4() { return static_cast<int32_t>(offsetof(StringConcat_t2171099760, ___delimiter_4)); }
	inline String_t* get_delimiter_4() const { return ___delimiter_4; }
	inline String_t** get_address_of_delimiter_4() { return &___delimiter_4; }
	inline void set_delimiter_4(String_t* value)
	{
		___delimiter_4 = value;
		Il2CppCodeGenWriteBarrier((&___delimiter_4), value);
	}

	inline static int32_t get_offset_of_strList_5() { return static_cast<int32_t>(offsetof(StringConcat_t2171099760, ___strList_5)); }
	inline List_1_t3319525431 * get_strList_5() const { return ___strList_5; }
	inline List_1_t3319525431 ** get_address_of_strList_5() { return &___strList_5; }
	inline void set_strList_5(List_1_t3319525431 * value)
	{
		___strList_5 = value;
		Il2CppCodeGenWriteBarrier((&___strList_5), value);
	}

	inline static int32_t get_offset_of_idxStr_6() { return static_cast<int32_t>(offsetof(StringConcat_t2171099760, ___idxStr_6)); }
	inline int32_t get_idxStr_6() const { return ___idxStr_6; }
	inline int32_t* get_address_of_idxStr_6() { return &___idxStr_6; }
	inline void set_idxStr_6(int32_t value)
	{
		___idxStr_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Xsl.Runtime.StringConcat
struct StringConcat_t2171099760_marshaled_pinvoke
{
	char* ___s1_0;
	char* ___s2_1;
	char* ___s3_2;
	char* ___s4_3;
	char* ___delimiter_4;
	List_1_t3319525431 * ___strList_5;
	int32_t ___idxStr_6;
};
// Native definition for COM marshalling of System.Xml.Xsl.Runtime.StringConcat
struct StringConcat_t2171099760_marshaled_com
{
	Il2CppChar* ___s1_0;
	Il2CppChar* ___s2_1;
	Il2CppChar* ___s3_2;
	Il2CppChar* ___s4_3;
	Il2CppChar* ___delimiter_4;
	List_1_t3319525431 * ___strList_5;
	int32_t ___idxStr_6;
};
#endif // STRINGCONCAT_T2171099760_H
#ifndef XMLSTANDALONE_T2027388713_H
#define XMLSTANDALONE_T2027388713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlStandalone
struct  XmlStandalone_t2027388713 
{
public:
	// System.Int32 System.Xml.XmlStandalone::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlStandalone_t2027388713, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSTANDALONE_T2027388713_H
#ifndef TRISTATE_T50539540_H
#define TRISTATE_T50539540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.TriState
struct  TriState_t50539540 
{
public:
	// System.Int32 System.Xml.TriState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TriState_t50539540, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRISTATE_T50539540_H
#ifndef STATE_T1355373645_H
#define STATE_T1355373645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReadContentAsBinaryHelper/State
struct  State_t1355373645 
{
public:
	// System.Int32 System.Xml.ReadContentAsBinaryHelper/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t1355373645, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T1355373645_H
#ifndef QUERYOUTPUTWRITER_T1945554537_H
#define QUERYOUTPUTWRITER_T1945554537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.QueryOutputWriter
struct  QueryOutputWriter_t1945554537  : public XmlRawWriter_t722320575
{
public:
	// System.Xml.XmlRawWriter System.Xml.QueryOutputWriter::wrapped
	XmlRawWriter_t722320575 * ___wrapped_3;
	// System.Boolean System.Xml.QueryOutputWriter::inCDataSection
	bool ___inCDataSection_4;
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Int32> System.Xml.QueryOutputWriter::lookupCDataElems
	Dictionary_2_t2925943073 * ___lookupCDataElems_5;
	// System.Xml.BitStack System.Xml.QueryOutputWriter::bitsCData
	BitStack_t371189938 * ___bitsCData_6;
	// System.Xml.XmlQualifiedName System.Xml.QueryOutputWriter::qnameCData
	XmlQualifiedName_t2760654312 * ___qnameCData_7;
	// System.Boolean System.Xml.QueryOutputWriter::outputDocType
	bool ___outputDocType_8;
	// System.Boolean System.Xml.QueryOutputWriter::checkWellFormedDoc
	bool ___checkWellFormedDoc_9;
	// System.Boolean System.Xml.QueryOutputWriter::hasDocElem
	bool ___hasDocElem_10;
	// System.Boolean System.Xml.QueryOutputWriter::inAttr
	bool ___inAttr_11;
	// System.String System.Xml.QueryOutputWriter::systemId
	String_t* ___systemId_12;
	// System.String System.Xml.QueryOutputWriter::publicId
	String_t* ___publicId_13;
	// System.Int32 System.Xml.QueryOutputWriter::depth
	int32_t ___depth_14;

public:
	inline static int32_t get_offset_of_wrapped_3() { return static_cast<int32_t>(offsetof(QueryOutputWriter_t1945554537, ___wrapped_3)); }
	inline XmlRawWriter_t722320575 * get_wrapped_3() const { return ___wrapped_3; }
	inline XmlRawWriter_t722320575 ** get_address_of_wrapped_3() { return &___wrapped_3; }
	inline void set_wrapped_3(XmlRawWriter_t722320575 * value)
	{
		___wrapped_3 = value;
		Il2CppCodeGenWriteBarrier((&___wrapped_3), value);
	}

	inline static int32_t get_offset_of_inCDataSection_4() { return static_cast<int32_t>(offsetof(QueryOutputWriter_t1945554537, ___inCDataSection_4)); }
	inline bool get_inCDataSection_4() const { return ___inCDataSection_4; }
	inline bool* get_address_of_inCDataSection_4() { return &___inCDataSection_4; }
	inline void set_inCDataSection_4(bool value)
	{
		___inCDataSection_4 = value;
	}

	inline static int32_t get_offset_of_lookupCDataElems_5() { return static_cast<int32_t>(offsetof(QueryOutputWriter_t1945554537, ___lookupCDataElems_5)); }
	inline Dictionary_2_t2925943073 * get_lookupCDataElems_5() const { return ___lookupCDataElems_5; }
	inline Dictionary_2_t2925943073 ** get_address_of_lookupCDataElems_5() { return &___lookupCDataElems_5; }
	inline void set_lookupCDataElems_5(Dictionary_2_t2925943073 * value)
	{
		___lookupCDataElems_5 = value;
		Il2CppCodeGenWriteBarrier((&___lookupCDataElems_5), value);
	}

	inline static int32_t get_offset_of_bitsCData_6() { return static_cast<int32_t>(offsetof(QueryOutputWriter_t1945554537, ___bitsCData_6)); }
	inline BitStack_t371189938 * get_bitsCData_6() const { return ___bitsCData_6; }
	inline BitStack_t371189938 ** get_address_of_bitsCData_6() { return &___bitsCData_6; }
	inline void set_bitsCData_6(BitStack_t371189938 * value)
	{
		___bitsCData_6 = value;
		Il2CppCodeGenWriteBarrier((&___bitsCData_6), value);
	}

	inline static int32_t get_offset_of_qnameCData_7() { return static_cast<int32_t>(offsetof(QueryOutputWriter_t1945554537, ___qnameCData_7)); }
	inline XmlQualifiedName_t2760654312 * get_qnameCData_7() const { return ___qnameCData_7; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_qnameCData_7() { return &___qnameCData_7; }
	inline void set_qnameCData_7(XmlQualifiedName_t2760654312 * value)
	{
		___qnameCData_7 = value;
		Il2CppCodeGenWriteBarrier((&___qnameCData_7), value);
	}

	inline static int32_t get_offset_of_outputDocType_8() { return static_cast<int32_t>(offsetof(QueryOutputWriter_t1945554537, ___outputDocType_8)); }
	inline bool get_outputDocType_8() const { return ___outputDocType_8; }
	inline bool* get_address_of_outputDocType_8() { return &___outputDocType_8; }
	inline void set_outputDocType_8(bool value)
	{
		___outputDocType_8 = value;
	}

	inline static int32_t get_offset_of_checkWellFormedDoc_9() { return static_cast<int32_t>(offsetof(QueryOutputWriter_t1945554537, ___checkWellFormedDoc_9)); }
	inline bool get_checkWellFormedDoc_9() const { return ___checkWellFormedDoc_9; }
	inline bool* get_address_of_checkWellFormedDoc_9() { return &___checkWellFormedDoc_9; }
	inline void set_checkWellFormedDoc_9(bool value)
	{
		___checkWellFormedDoc_9 = value;
	}

	inline static int32_t get_offset_of_hasDocElem_10() { return static_cast<int32_t>(offsetof(QueryOutputWriter_t1945554537, ___hasDocElem_10)); }
	inline bool get_hasDocElem_10() const { return ___hasDocElem_10; }
	inline bool* get_address_of_hasDocElem_10() { return &___hasDocElem_10; }
	inline void set_hasDocElem_10(bool value)
	{
		___hasDocElem_10 = value;
	}

	inline static int32_t get_offset_of_inAttr_11() { return static_cast<int32_t>(offsetof(QueryOutputWriter_t1945554537, ___inAttr_11)); }
	inline bool get_inAttr_11() const { return ___inAttr_11; }
	inline bool* get_address_of_inAttr_11() { return &___inAttr_11; }
	inline void set_inAttr_11(bool value)
	{
		___inAttr_11 = value;
	}

	inline static int32_t get_offset_of_systemId_12() { return static_cast<int32_t>(offsetof(QueryOutputWriter_t1945554537, ___systemId_12)); }
	inline String_t* get_systemId_12() const { return ___systemId_12; }
	inline String_t** get_address_of_systemId_12() { return &___systemId_12; }
	inline void set_systemId_12(String_t* value)
	{
		___systemId_12 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_12), value);
	}

	inline static int32_t get_offset_of_publicId_13() { return static_cast<int32_t>(offsetof(QueryOutputWriter_t1945554537, ___publicId_13)); }
	inline String_t* get_publicId_13() const { return ___publicId_13; }
	inline String_t** get_address_of_publicId_13() { return &___publicId_13; }
	inline void set_publicId_13(String_t* value)
	{
		___publicId_13 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_13), value);
	}

	inline static int32_t get_offset_of_depth_14() { return static_cast<int32_t>(offsetof(QueryOutputWriter_t1945554537, ___depth_14)); }
	inline int32_t get_depth_14() const { return ___depth_14; }
	inline int32_t* get_address_of_depth_14() { return &___depth_14; }
	inline void set_depth_14(int32_t value)
	{
		___depth_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYOUTPUTWRITER_T1945554537_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef CONFORMANCELEVEL_T3899847875_H
#define CONFORMANCELEVEL_T3899847875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ConformanceLevel
struct  ConformanceLevel_t3899847875 
{
public:
	// System.Int32 System.Xml.ConformanceLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConformanceLevel_t3899847875, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFORMANCELEVEL_T3899847875_H
#ifndef SPECIALATTRIBUTE_T2983285139_H
#define SPECIALATTRIBUTE_T2983285139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/SpecialAttribute
struct  SpecialAttribute_t2983285139 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter/SpecialAttribute::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialAttribute_t2983285139, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALATTRIBUTE_T2983285139_H
#ifndef NAMESPACEKIND_T1063930693_H
#define NAMESPACEKIND_T1063930693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/NamespaceKind
struct  NamespaceKind_t1063930693 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter/NamespaceKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NamespaceKind_t1063930693, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEKIND_T1063930693_H
#ifndef ATTRIBUTEPROPERTIES_T3715092149_H
#define ATTRIBUTEPROPERTIES_T3715092149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.AttributeProperties
struct  AttributeProperties_t3715092149 
{
public:
	// System.UInt32 System.Xml.AttributeProperties::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeProperties_t3715092149, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEPROPERTIES_T3715092149_H
#ifndef XMLOUTPUTMETHOD_T2185361861_H
#define XMLOUTPUTMETHOD_T2185361861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlOutputMethod
struct  XmlOutputMethod_t2185361861 
{
public:
	// System.Int32 System.Xml.XmlOutputMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlOutputMethod_t2185361861, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLOUTPUTMETHOD_T2185361861_H
#ifndef WRITESTATE_T3983380671_H
#define WRITESTATE_T3983380671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WriteState
struct  WriteState_t3983380671 
{
public:
	// System.Int32 System.Xml.WriteState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WriteState_t3983380671, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATE_T3983380671_H
#ifndef ELEMENTPROPERTIES_T469398153_H
#define ELEMENTPROPERTIES_T469398153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ElementProperties
struct  ElementProperties_t469398153 
{
public:
	// System.UInt32 System.Xml.ElementProperties::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ElementProperties_t469398153, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTPROPERTIES_T469398153_H
#ifndef XMLNODECHANGEDACTION_T3227731597_H
#define XMLNODECHANGEDACTION_T3227731597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedAction
struct  XmlNodeChangedAction_t3227731597 
{
public:
	// System.Int32 System.Xml.XmlNodeChangedAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlNodeChangedAction_t3227731597, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECHANGEDACTION_T3227731597_H
#ifndef XMLENTITYREFERENCE_T1966808559_H
#define XMLENTITYREFERENCE_T1966808559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntityReference
struct  XmlEntityReference_t1966808559  : public XmlLinkedNode_t1437094927
{
public:
	// System.String System.Xml.XmlEntityReference::name
	String_t* ___name_2;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntityReference::lastChild
	XmlLinkedNode_t1437094927 * ___lastChild_3;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(XmlEntityReference_t1966808559, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_lastChild_3() { return static_cast<int32_t>(offsetof(XmlEntityReference_t1966808559, ___lastChild_3)); }
	inline XmlLinkedNode_t1437094927 * get_lastChild_3() const { return ___lastChild_3; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastChild_3() { return &___lastChild_3; }
	inline void set_lastChild_3(XmlLinkedNode_t1437094927 * value)
	{
		___lastChild_3 = value;
		Il2CppCodeGenWriteBarrier((&___lastChild_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENTITYREFERENCE_T1966808559_H
#ifndef NEWLINEHANDLING_T850339274_H
#define NEWLINEHANDLING_T850339274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NewLineHandling
struct  NewLineHandling_t850339274 
{
public:
	// System.Int32 System.Xml.NewLineHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NewLineHandling_t850339274, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWLINEHANDLING_T850339274_H
#ifndef DTDPROCESSING_T1163997051_H
#define DTDPROCESSING_T1163997051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdProcessing
struct  DtdProcessing_t1163997051 
{
public:
	// System.Int32 System.Xml.DtdProcessing::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DtdProcessing_t1163997051, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPROCESSING_T1163997051_H
#ifndef XMLSCHEMAVALIDATIONFLAGS_T877176585_H
#define XMLSCHEMAVALIDATIONFLAGS_T877176585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidationFlags
struct  XmlSchemaValidationFlags_t877176585 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidationFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaValidationFlags_t877176585, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATIONFLAGS_T877176585_H
#ifndef NAMESPACEHANDLING_T4087553436_H
#define NAMESPACEHANDLING_T4087553436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NamespaceHandling
struct  NamespaceHandling_t4087553436 
{
public:
	// System.Int32 System.Xml.NamespaceHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NamespaceHandling_t4087553436, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEHANDLING_T4087553436_H
#ifndef XMLELEMENT_T561603118_H
#define XMLELEMENT_T561603118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlElement
struct  XmlElement_t561603118  : public XmlLinkedNode_t1437094927
{
public:
	// System.Xml.XmlName System.Xml.XmlElement::name
	XmlName_t4150142242 * ___name_2;
	// System.Xml.XmlAttributeCollection System.Xml.XmlElement::attributes
	XmlAttributeCollection_t2316283784 * ___attributes_3;
	// System.Xml.XmlLinkedNode System.Xml.XmlElement::lastChild
	XmlLinkedNode_t1437094927 * ___lastChild_4;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(XmlElement_t561603118, ___name_2)); }
	inline XmlName_t4150142242 * get_name_2() const { return ___name_2; }
	inline XmlName_t4150142242 ** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(XmlName_t4150142242 * value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_attributes_3() { return static_cast<int32_t>(offsetof(XmlElement_t561603118, ___attributes_3)); }
	inline XmlAttributeCollection_t2316283784 * get_attributes_3() const { return ___attributes_3; }
	inline XmlAttributeCollection_t2316283784 ** get_address_of_attributes_3() { return &___attributes_3; }
	inline void set_attributes_3(XmlAttributeCollection_t2316283784 * value)
	{
		___attributes_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_3), value);
	}

	inline static int32_t get_offset_of_lastChild_4() { return static_cast<int32_t>(offsetof(XmlElement_t561603118, ___lastChild_4)); }
	inline XmlLinkedNode_t1437094927 * get_lastChild_4() const { return ___lastChild_4; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastChild_4() { return &___lastChild_4; }
	inline void set_lastChild_4(XmlLinkedNode_t1437094927 * value)
	{
		___lastChild_4 = value;
		Il2CppCodeGenWriteBarrier((&___lastChild_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENT_T561603118_H
#ifndef XMLDECLARATION_T679870411_H
#define XMLDECLARATION_T679870411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDeclaration
struct  XmlDeclaration_t679870411  : public XmlLinkedNode_t1437094927
{
public:
	// System.String System.Xml.XmlDeclaration::version
	String_t* ___version_2;
	// System.String System.Xml.XmlDeclaration::encoding
	String_t* ___encoding_3;
	// System.String System.Xml.XmlDeclaration::standalone
	String_t* ___standalone_4;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(XmlDeclaration_t679870411, ___version_2)); }
	inline String_t* get_version_2() const { return ___version_2; }
	inline String_t** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(String_t* value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_encoding_3() { return static_cast<int32_t>(offsetof(XmlDeclaration_t679870411, ___encoding_3)); }
	inline String_t* get_encoding_3() const { return ___encoding_3; }
	inline String_t** get_address_of_encoding_3() { return &___encoding_3; }
	inline void set_encoding_3(String_t* value)
	{
		___encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_3), value);
	}

	inline static int32_t get_offset_of_standalone_4() { return static_cast<int32_t>(offsetof(XmlDeclaration_t679870411, ___standalone_4)); }
	inline String_t* get_standalone_4() const { return ___standalone_4; }
	inline String_t** get_address_of_standalone_4() { return &___standalone_4; }
	inline void set_standalone_4(String_t* value)
	{
		___standalone_4 = value;
		Il2CppCodeGenWriteBarrier((&___standalone_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDECLARATION_T679870411_H
#ifndef XMLNAMEDNODEMAP_T2821286253_H
#define XMLNAMEDNODEMAP_T2821286253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamedNodeMap
struct  XmlNamedNodeMap_t2821286253  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNamedNodeMap::parent
	XmlNode_t3767805227 * ___parent_0;
	// System.Xml.XmlNamedNodeMap/SmallXmlNodeList System.Xml.XmlNamedNodeMap::nodes
	SmallXmlNodeList_t242297918  ___nodes_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t2821286253, ___parent_0)); }
	inline XmlNode_t3767805227 * get_parent_0() const { return ___parent_0; }
	inline XmlNode_t3767805227 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(XmlNode_t3767805227 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_nodes_1() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t2821286253, ___nodes_1)); }
	inline SmallXmlNodeList_t242297918  get_nodes_1() const { return ___nodes_1; }
	inline SmallXmlNodeList_t242297918 * get_address_of_nodes_1() { return &___nodes_1; }
	inline void set_nodes_1(SmallXmlNodeList_t242297918  value)
	{
		___nodes_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEDNODEMAP_T2821286253_H
#ifndef XMLCHARACTERDATA_T1167807131_H
#define XMLCHARACTERDATA_T1167807131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharacterData
struct  XmlCharacterData_t1167807131  : public XmlLinkedNode_t1437094927
{
public:
	// System.String System.Xml.XmlCharacterData::data
	String_t* ___data_2;

public:
	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(XmlCharacterData_t1167807131, ___data_2)); }
	inline String_t* get_data_2() const { return ___data_2; }
	inline String_t** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(String_t* value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHARACTERDATA_T1167807131_H
#ifndef XMLDOCUMENTTYPE_T4112370061_H
#define XMLDOCUMENTTYPE_T4112370061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocumentType
struct  XmlDocumentType_t4112370061  : public XmlLinkedNode_t1437094927
{
public:
	// System.String System.Xml.XmlDocumentType::name
	String_t* ___name_2;
	// System.String System.Xml.XmlDocumentType::publicId
	String_t* ___publicId_3;
	// System.String System.Xml.XmlDocumentType::systemId
	String_t* ___systemId_4;
	// System.String System.Xml.XmlDocumentType::internalSubset
	String_t* ___internalSubset_5;
	// System.Boolean System.Xml.XmlDocumentType::namespaces
	bool ___namespaces_6;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::entities
	XmlNamedNodeMap_t2821286253 * ___entities_7;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::notations
	XmlNamedNodeMap_t2821286253 * ___notations_8;
	// System.Xml.Schema.SchemaInfo System.Xml.XmlDocumentType::schemaInfo
	SchemaInfo_t3347508623 * ___schemaInfo_9;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(XmlDocumentType_t4112370061, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_publicId_3() { return static_cast<int32_t>(offsetof(XmlDocumentType_t4112370061, ___publicId_3)); }
	inline String_t* get_publicId_3() const { return ___publicId_3; }
	inline String_t** get_address_of_publicId_3() { return &___publicId_3; }
	inline void set_publicId_3(String_t* value)
	{
		___publicId_3 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_3), value);
	}

	inline static int32_t get_offset_of_systemId_4() { return static_cast<int32_t>(offsetof(XmlDocumentType_t4112370061, ___systemId_4)); }
	inline String_t* get_systemId_4() const { return ___systemId_4; }
	inline String_t** get_address_of_systemId_4() { return &___systemId_4; }
	inline void set_systemId_4(String_t* value)
	{
		___systemId_4 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_4), value);
	}

	inline static int32_t get_offset_of_internalSubset_5() { return static_cast<int32_t>(offsetof(XmlDocumentType_t4112370061, ___internalSubset_5)); }
	inline String_t* get_internalSubset_5() const { return ___internalSubset_5; }
	inline String_t** get_address_of_internalSubset_5() { return &___internalSubset_5; }
	inline void set_internalSubset_5(String_t* value)
	{
		___internalSubset_5 = value;
		Il2CppCodeGenWriteBarrier((&___internalSubset_5), value);
	}

	inline static int32_t get_offset_of_namespaces_6() { return static_cast<int32_t>(offsetof(XmlDocumentType_t4112370061, ___namespaces_6)); }
	inline bool get_namespaces_6() const { return ___namespaces_6; }
	inline bool* get_address_of_namespaces_6() { return &___namespaces_6; }
	inline void set_namespaces_6(bool value)
	{
		___namespaces_6 = value;
	}

	inline static int32_t get_offset_of_entities_7() { return static_cast<int32_t>(offsetof(XmlDocumentType_t4112370061, ___entities_7)); }
	inline XmlNamedNodeMap_t2821286253 * get_entities_7() const { return ___entities_7; }
	inline XmlNamedNodeMap_t2821286253 ** get_address_of_entities_7() { return &___entities_7; }
	inline void set_entities_7(XmlNamedNodeMap_t2821286253 * value)
	{
		___entities_7 = value;
		Il2CppCodeGenWriteBarrier((&___entities_7), value);
	}

	inline static int32_t get_offset_of_notations_8() { return static_cast<int32_t>(offsetof(XmlDocumentType_t4112370061, ___notations_8)); }
	inline XmlNamedNodeMap_t2821286253 * get_notations_8() const { return ___notations_8; }
	inline XmlNamedNodeMap_t2821286253 ** get_address_of_notations_8() { return &___notations_8; }
	inline void set_notations_8(XmlNamedNodeMap_t2821286253 * value)
	{
		___notations_8 = value;
		Il2CppCodeGenWriteBarrier((&___notations_8), value);
	}

	inline static int32_t get_offset_of_schemaInfo_9() { return static_cast<int32_t>(offsetof(XmlDocumentType_t4112370061, ___schemaInfo_9)); }
	inline SchemaInfo_t3347508623 * get_schemaInfo_9() const { return ___schemaInfo_9; }
	inline SchemaInfo_t3347508623 ** get_address_of_schemaInfo_9() { return &___schemaInfo_9; }
	inline void set_schemaInfo_9(SchemaInfo_t3347508623 * value)
	{
		___schemaInfo_9 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTTYPE_T4112370061_H
#ifndef ENTITYHANDLING_T1047276436_H
#define ENTITYHANDLING_T1047276436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.EntityHandling
struct  EntityHandling_t1047276436 
{
public:
	// System.Int32 System.Xml.EntityHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EntityHandling_t1047276436, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYHANDLING_T1047276436_H
#ifndef XMLNODETYPE_T1672767151_H
#define XMLNODETYPE_T1672767151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeType
struct  XmlNodeType_t1672767151 
{
public:
	// System.Int32 System.Xml.XmlNodeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlNodeType_t1672767151, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODETYPE_T1672767151_H
#ifndef STATE_T1792539347_H
#define STATE_T1792539347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/State
struct  State_t1792539347 
{
public:
	// System.Int32 System.Xml.XmlTextWriter/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t1792539347, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T1792539347_H
#ifndef SPECIALATTR_T319442854_H
#define SPECIALATTR_T319442854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/SpecialAttr
struct  SpecialAttr_t319442854 
{
public:
	// System.Int32 System.Xml.XmlTextWriter/SpecialAttr::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialAttr_t319442854, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALATTR_T319442854_H
#ifndef PARSINGFUNCTION_T2722146511_H
#define PARSINGFUNCTION_T2722146511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/ParsingFunction
struct  ParsingFunction_t2722146511 
{
public:
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingFunction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParsingFunction_t2722146511, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSINGFUNCTION_T2722146511_H
#ifndef XMLSPACE_T3324193251_H
#define XMLSPACE_T3324193251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSpace
struct  XmlSpace_t3324193251 
{
public:
	// System.Int32 System.Xml.XmlSpace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSpace_t3324193251, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSPACE_T3324193251_H
#ifndef TOKEN_T2430492165_H
#define TOKEN_T2430492165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/Token
struct  Token_t2430492165 
{
public:
	// System.Int32 System.Xml.XmlTextWriter/Token::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Token_t2430492165, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T2430492165_H
#ifndef XMLTEXTENCODER_T1632274355_H
#define XMLTEXTENCODER_T1632274355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextEncoder
struct  XmlTextEncoder_t1632274355  : public RuntimeObject
{
public:
	// System.IO.TextWriter System.Xml.XmlTextEncoder::textWriter
	TextWriter_t3478189236 * ___textWriter_0;
	// System.Boolean System.Xml.XmlTextEncoder::inAttribute
	bool ___inAttribute_1;
	// System.Char System.Xml.XmlTextEncoder::quoteChar
	Il2CppChar ___quoteChar_2;
	// System.Text.StringBuilder System.Xml.XmlTextEncoder::attrValue
	StringBuilder_t1712802186 * ___attrValue_3;
	// System.Boolean System.Xml.XmlTextEncoder::cacheAttrValue
	bool ___cacheAttrValue_4;
	// System.Xml.XmlCharType System.Xml.XmlTextEncoder::xmlCharType
	XmlCharType_t2277243275  ___xmlCharType_5;

public:
	inline static int32_t get_offset_of_textWriter_0() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t1632274355, ___textWriter_0)); }
	inline TextWriter_t3478189236 * get_textWriter_0() const { return ___textWriter_0; }
	inline TextWriter_t3478189236 ** get_address_of_textWriter_0() { return &___textWriter_0; }
	inline void set_textWriter_0(TextWriter_t3478189236 * value)
	{
		___textWriter_0 = value;
		Il2CppCodeGenWriteBarrier((&___textWriter_0), value);
	}

	inline static int32_t get_offset_of_inAttribute_1() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t1632274355, ___inAttribute_1)); }
	inline bool get_inAttribute_1() const { return ___inAttribute_1; }
	inline bool* get_address_of_inAttribute_1() { return &___inAttribute_1; }
	inline void set_inAttribute_1(bool value)
	{
		___inAttribute_1 = value;
	}

	inline static int32_t get_offset_of_quoteChar_2() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t1632274355, ___quoteChar_2)); }
	inline Il2CppChar get_quoteChar_2() const { return ___quoteChar_2; }
	inline Il2CppChar* get_address_of_quoteChar_2() { return &___quoteChar_2; }
	inline void set_quoteChar_2(Il2CppChar value)
	{
		___quoteChar_2 = value;
	}

	inline static int32_t get_offset_of_attrValue_3() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t1632274355, ___attrValue_3)); }
	inline StringBuilder_t1712802186 * get_attrValue_3() const { return ___attrValue_3; }
	inline StringBuilder_t1712802186 ** get_address_of_attrValue_3() { return &___attrValue_3; }
	inline void set_attrValue_3(StringBuilder_t1712802186 * value)
	{
		___attrValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___attrValue_3), value);
	}

	inline static int32_t get_offset_of_cacheAttrValue_4() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t1632274355, ___cacheAttrValue_4)); }
	inline bool get_cacheAttrValue_4() const { return ___cacheAttrValue_4; }
	inline bool* get_address_of_cacheAttrValue_4() { return &___cacheAttrValue_4; }
	inline void set_cacheAttrValue_4(bool value)
	{
		___cacheAttrValue_4 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_5() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t1632274355, ___xmlCharType_5)); }
	inline XmlCharType_t2277243275  get_xmlCharType_5() const { return ___xmlCharType_5; }
	inline XmlCharType_t2277243275 * get_address_of_xmlCharType_5() { return &___xmlCharType_5; }
	inline void set_xmlCharType_5(XmlCharType_t2277243275  value)
	{
		___xmlCharType_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTENCODER_T1632274355_H
#ifndef NAMESPACESTATE_T853084585_H
#define NAMESPACESTATE_T853084585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/NamespaceState
struct  NamespaceState_t853084585 
{
public:
	// System.Int32 System.Xml.XmlTextWriter/NamespaceState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NamespaceState_t853084585, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACESTATE_T853084585_H
#ifndef ENTITYEXPANDTYPE_T2794135677_H
#define ENTITYEXPANDTYPE_T2794135677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/EntityExpandType
struct  EntityExpandType_t2794135677 
{
public:
	// System.Int32 System.Xml.XmlTextReaderImpl/EntityExpandType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EntityExpandType_t2794135677, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYEXPANDTYPE_T2794135677_H
#ifndef INCREMENTALREADSTATE_T591482297_H
#define INCREMENTALREADSTATE_T591482297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/IncrementalReadState
struct  IncrementalReadState_t591482297 
{
public:
	// System.Int32 System.Xml.XmlTextReaderImpl/IncrementalReadState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IncrementalReadState_t591482297, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INCREMENTALREADSTATE_T591482297_H
#ifndef INITINPUTTYPE_T933223089_H
#define INITINPUTTYPE_T933223089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/InitInputType
struct  InitInputType_t933223089 
{
public:
	// System.Int32 System.Xml.XmlTextReaderImpl/InitInputType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InitInputType_t933223089, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITINPUTTYPE_T933223089_H
#ifndef PARSINGMODE_T1812726891_H
#define PARSINGMODE_T1812726891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/ParsingMode
struct  ParsingMode_t1812726891 
{
public:
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParsingMode_t1812726891, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSINGMODE_T1812726891_H
#ifndef FORMATTING_T1232942836_H
#define FORMATTING_T1232942836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Formatting
struct  Formatting_t1232942836 
{
public:
	// System.Int32 System.Xml.Formatting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Formatting_t1232942836, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_T1232942836_H
#ifndef ENTITYTYPE_T2463313529_H
#define ENTITYTYPE_T2463313529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/EntityType
struct  EntityType_t2463313529 
{
public:
	// System.Int32 System.Xml.XmlTextReaderImpl/EntityType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EntityType_t2463313529, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYTYPE_T2463313529_H
#ifndef XMLAUTODETECTWRITER_T3131353423_H
#define XMLAUTODETECTWRITER_T3131353423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAutoDetectWriter
struct  XmlAutoDetectWriter_t3131353423  : public XmlRawWriter_t722320575
{
public:
	// System.Xml.XmlRawWriter System.Xml.XmlAutoDetectWriter::wrapped
	XmlRawWriter_t722320575 * ___wrapped_3;
	// System.Xml.OnRemoveWriter System.Xml.XmlAutoDetectWriter::onRemove
	OnRemoveWriter_t4019152709 * ___onRemove_4;
	// System.Xml.XmlWriterSettings System.Xml.XmlAutoDetectWriter::writerSettings
	XmlWriterSettings_t3314986516 * ___writerSettings_5;
	// System.Xml.XmlEventCache System.Xml.XmlAutoDetectWriter::eventCache
	XmlEventCache_t1546883120 * ___eventCache_6;
	// System.IO.TextWriter System.Xml.XmlAutoDetectWriter::textWriter
	TextWriter_t3478189236 * ___textWriter_7;
	// System.IO.Stream System.Xml.XmlAutoDetectWriter::strm
	Stream_t1273022909 * ___strm_8;

public:
	inline static int32_t get_offset_of_wrapped_3() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_t3131353423, ___wrapped_3)); }
	inline XmlRawWriter_t722320575 * get_wrapped_3() const { return ___wrapped_3; }
	inline XmlRawWriter_t722320575 ** get_address_of_wrapped_3() { return &___wrapped_3; }
	inline void set_wrapped_3(XmlRawWriter_t722320575 * value)
	{
		___wrapped_3 = value;
		Il2CppCodeGenWriteBarrier((&___wrapped_3), value);
	}

	inline static int32_t get_offset_of_onRemove_4() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_t3131353423, ___onRemove_4)); }
	inline OnRemoveWriter_t4019152709 * get_onRemove_4() const { return ___onRemove_4; }
	inline OnRemoveWriter_t4019152709 ** get_address_of_onRemove_4() { return &___onRemove_4; }
	inline void set_onRemove_4(OnRemoveWriter_t4019152709 * value)
	{
		___onRemove_4 = value;
		Il2CppCodeGenWriteBarrier((&___onRemove_4), value);
	}

	inline static int32_t get_offset_of_writerSettings_5() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_t3131353423, ___writerSettings_5)); }
	inline XmlWriterSettings_t3314986516 * get_writerSettings_5() const { return ___writerSettings_5; }
	inline XmlWriterSettings_t3314986516 ** get_address_of_writerSettings_5() { return &___writerSettings_5; }
	inline void set_writerSettings_5(XmlWriterSettings_t3314986516 * value)
	{
		___writerSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&___writerSettings_5), value);
	}

	inline static int32_t get_offset_of_eventCache_6() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_t3131353423, ___eventCache_6)); }
	inline XmlEventCache_t1546883120 * get_eventCache_6() const { return ___eventCache_6; }
	inline XmlEventCache_t1546883120 ** get_address_of_eventCache_6() { return &___eventCache_6; }
	inline void set_eventCache_6(XmlEventCache_t1546883120 * value)
	{
		___eventCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___eventCache_6), value);
	}

	inline static int32_t get_offset_of_textWriter_7() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_t3131353423, ___textWriter_7)); }
	inline TextWriter_t3478189236 * get_textWriter_7() const { return ___textWriter_7; }
	inline TextWriter_t3478189236 ** get_address_of_textWriter_7() { return &___textWriter_7; }
	inline void set_textWriter_7(TextWriter_t3478189236 * value)
	{
		___textWriter_7 = value;
		Il2CppCodeGenWriteBarrier((&___textWriter_7), value);
	}

	inline static int32_t get_offset_of_strm_8() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_t3131353423, ___strm_8)); }
	inline Stream_t1273022909 * get_strm_8() const { return ___strm_8; }
	inline Stream_t1273022909 ** get_address_of_strm_8() { return &___strm_8; }
	inline void set_strm_8(Stream_t1273022909 * value)
	{
		___strm_8 = value;
		Il2CppCodeGenWriteBarrier((&___strm_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLAUTODETECTWRITER_T3131353423_H
#ifndef TOKEN_T678414789_H
#define TOKEN_T678414789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/Token
struct  Token_t678414789 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter/Token::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Token_t678414789, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T678414789_H
#ifndef VALIDATIONTYPE_T4049928607_H
#define VALIDATIONTYPE_T4049928607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ValidationType
struct  ValidationType_t4049928607 
{
public:
	// System.Int32 System.Xml.ValidationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ValidationType_t4049928607, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONTYPE_T4049928607_H
#ifndef ITEMTYPE_T208921270_H
#define ITEMTYPE_T208921270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/AttributeValueCache/ItemType
struct  ItemType_t208921270 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter/AttributeValueCache/ItemType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ItemType_t208921270, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMTYPE_T208921270_H
#ifndef READSTATE_T944984020_H
#define READSTATE_T944984020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReadState
struct  ReadState_t944984020 
{
public:
	// System.Int32 System.Xml.ReadState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReadState_t944984020, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTATE_T944984020_H
#ifndef WHITESPACEHANDLING_T784045650_H
#define WHITESPACEHANDLING_T784045650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WhitespaceHandling
struct  WhitespaceHandling_t784045650 
{
public:
	// System.Int32 System.Xml.WhitespaceHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WhitespaceHandling_t784045650, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHITESPACEHANDLING_T784045650_H
#ifndef PARSINGFUNCTION_T767331079_H
#define PARSINGFUNCTION_T767331079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlValidatingReaderImpl/ParsingFunction
struct  ParsingFunction_t767331079 
{
public:
	// System.Int32 System.Xml.XmlValidatingReaderImpl/ParsingFunction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParsingFunction_t767331079, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSINGFUNCTION_T767331079_H
#ifndef STATE_T3712518167_H
#define STATE_T3712518167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/State
struct  State_t3712518167 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t3712518167, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T3712518167_H
#ifndef XMLEVENTTYPE_T2232656300_H
#define XMLEVENTTYPE_T2232656300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEventCache/XmlEventType
struct  XmlEventType_t2232656300 
{
public:
	// System.Int32 System.Xml.XmlEventCache/XmlEventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlEventType_t2232656300, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLEVENTTYPE_T2232656300_H
#ifndef XMLEVENTCACHE_T1546883120_H
#define XMLEVENTCACHE_T1546883120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEventCache
struct  XmlEventCache_t1546883120  : public XmlRawWriter_t722320575
{
public:
	// System.Collections.Generic.List`1<System.Xml.XmlEventCache/XmlEvent[]> System.Xml.XmlEventCache::pages
	List_1_t2211047657 * ___pages_3;
	// System.Xml.XmlEventCache/XmlEvent[] System.Xml.XmlEventCache::pageCurr
	XmlEventU5BU5D_t738972915* ___pageCurr_4;
	// System.Int32 System.Xml.XmlEventCache::pageSize
	int32_t ___pageSize_5;
	// System.Boolean System.Xml.XmlEventCache::hasRootNode
	bool ___hasRootNode_6;
	// System.Xml.Xsl.Runtime.StringConcat System.Xml.XmlEventCache::singleText
	StringConcat_t2171099760  ___singleText_7;
	// System.String System.Xml.XmlEventCache::baseUri
	String_t* ___baseUri_8;

public:
	inline static int32_t get_offset_of_pages_3() { return static_cast<int32_t>(offsetof(XmlEventCache_t1546883120, ___pages_3)); }
	inline List_1_t2211047657 * get_pages_3() const { return ___pages_3; }
	inline List_1_t2211047657 ** get_address_of_pages_3() { return &___pages_3; }
	inline void set_pages_3(List_1_t2211047657 * value)
	{
		___pages_3 = value;
		Il2CppCodeGenWriteBarrier((&___pages_3), value);
	}

	inline static int32_t get_offset_of_pageCurr_4() { return static_cast<int32_t>(offsetof(XmlEventCache_t1546883120, ___pageCurr_4)); }
	inline XmlEventU5BU5D_t738972915* get_pageCurr_4() const { return ___pageCurr_4; }
	inline XmlEventU5BU5D_t738972915** get_address_of_pageCurr_4() { return &___pageCurr_4; }
	inline void set_pageCurr_4(XmlEventU5BU5D_t738972915* value)
	{
		___pageCurr_4 = value;
		Il2CppCodeGenWriteBarrier((&___pageCurr_4), value);
	}

	inline static int32_t get_offset_of_pageSize_5() { return static_cast<int32_t>(offsetof(XmlEventCache_t1546883120, ___pageSize_5)); }
	inline int32_t get_pageSize_5() const { return ___pageSize_5; }
	inline int32_t* get_address_of_pageSize_5() { return &___pageSize_5; }
	inline void set_pageSize_5(int32_t value)
	{
		___pageSize_5 = value;
	}

	inline static int32_t get_offset_of_hasRootNode_6() { return static_cast<int32_t>(offsetof(XmlEventCache_t1546883120, ___hasRootNode_6)); }
	inline bool get_hasRootNode_6() const { return ___hasRootNode_6; }
	inline bool* get_address_of_hasRootNode_6() { return &___hasRootNode_6; }
	inline void set_hasRootNode_6(bool value)
	{
		___hasRootNode_6 = value;
	}

	inline static int32_t get_offset_of_singleText_7() { return static_cast<int32_t>(offsetof(XmlEventCache_t1546883120, ___singleText_7)); }
	inline StringConcat_t2171099760  get_singleText_7() const { return ___singleText_7; }
	inline StringConcat_t2171099760 * get_address_of_singleText_7() { return &___singleText_7; }
	inline void set_singleText_7(StringConcat_t2171099760  value)
	{
		___singleText_7 = value;
	}

	inline static int32_t get_offset_of_baseUri_8() { return static_cast<int32_t>(offsetof(XmlEventCache_t1546883120, ___baseUri_8)); }
	inline String_t* get_baseUri_8() const { return ___baseUri_8; }
	inline String_t** get_address_of_baseUri_8() { return &___baseUri_8; }
	inline void set_baseUri_8(String_t* value)
	{
		___baseUri_8 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLEVENTCACHE_T1546883120_H
#ifndef XMLREADERSETTINGS_T2186285234_H
#define XMLREADERSETTINGS_T2186285234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderSettings
struct  XmlReaderSettings_t2186285234  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.XmlReaderSettings::useAsync
	bool ___useAsync_0;
	// System.Xml.XmlNameTable System.Xml.XmlReaderSettings::nameTable
	XmlNameTable_t71772148 * ___nameTable_1;
	// System.Xml.XmlResolver System.Xml.XmlReaderSettings::xmlResolver
	XmlResolver_t626023767 * ___xmlResolver_2;
	// System.Int32 System.Xml.XmlReaderSettings::lineNumberOffset
	int32_t ___lineNumberOffset_3;
	// System.Int32 System.Xml.XmlReaderSettings::linePositionOffset
	int32_t ___linePositionOffset_4;
	// System.Xml.ConformanceLevel System.Xml.XmlReaderSettings::conformanceLevel
	int32_t ___conformanceLevel_5;
	// System.Boolean System.Xml.XmlReaderSettings::checkCharacters
	bool ___checkCharacters_6;
	// System.Int64 System.Xml.XmlReaderSettings::maxCharactersInDocument
	int64_t ___maxCharactersInDocument_7;
	// System.Int64 System.Xml.XmlReaderSettings::maxCharactersFromEntities
	int64_t ___maxCharactersFromEntities_8;
	// System.Boolean System.Xml.XmlReaderSettings::ignoreWhitespace
	bool ___ignoreWhitespace_9;
	// System.Boolean System.Xml.XmlReaderSettings::ignorePIs
	bool ___ignorePIs_10;
	// System.Boolean System.Xml.XmlReaderSettings::ignoreComments
	bool ___ignoreComments_11;
	// System.Xml.DtdProcessing System.Xml.XmlReaderSettings::dtdProcessing
	int32_t ___dtdProcessing_12;
	// System.Xml.ValidationType System.Xml.XmlReaderSettings::validationType
	int32_t ___validationType_13;
	// System.Xml.Schema.XmlSchemaValidationFlags System.Xml.XmlReaderSettings::validationFlags
	int32_t ___validationFlags_14;
	// System.Xml.Schema.XmlSchemaSet System.Xml.XmlReaderSettings::schemas
	XmlSchemaSet_t266093086 * ___schemas_15;
	// System.Boolean System.Xml.XmlReaderSettings::closeInput
	bool ___closeInput_16;
	// System.Boolean System.Xml.XmlReaderSettings::isReadOnly
	bool ___isReadOnly_17;
	// System.Boolean System.Xml.XmlReaderSettings::<IsXmlResolverSet>k__BackingField
	bool ___U3CIsXmlResolverSetU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_useAsync_0() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___useAsync_0)); }
	inline bool get_useAsync_0() const { return ___useAsync_0; }
	inline bool* get_address_of_useAsync_0() { return &___useAsync_0; }
	inline void set_useAsync_0(bool value)
	{
		___useAsync_0 = value;
	}

	inline static int32_t get_offset_of_nameTable_1() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___nameTable_1)); }
	inline XmlNameTable_t71772148 * get_nameTable_1() const { return ___nameTable_1; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_1() { return &___nameTable_1; }
	inline void set_nameTable_1(XmlNameTable_t71772148 * value)
	{
		___nameTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_1), value);
	}

	inline static int32_t get_offset_of_xmlResolver_2() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___xmlResolver_2)); }
	inline XmlResolver_t626023767 * get_xmlResolver_2() const { return ___xmlResolver_2; }
	inline XmlResolver_t626023767 ** get_address_of_xmlResolver_2() { return &___xmlResolver_2; }
	inline void set_xmlResolver_2(XmlResolver_t626023767 * value)
	{
		___xmlResolver_2 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_2), value);
	}

	inline static int32_t get_offset_of_lineNumberOffset_3() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___lineNumberOffset_3)); }
	inline int32_t get_lineNumberOffset_3() const { return ___lineNumberOffset_3; }
	inline int32_t* get_address_of_lineNumberOffset_3() { return &___lineNumberOffset_3; }
	inline void set_lineNumberOffset_3(int32_t value)
	{
		___lineNumberOffset_3 = value;
	}

	inline static int32_t get_offset_of_linePositionOffset_4() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___linePositionOffset_4)); }
	inline int32_t get_linePositionOffset_4() const { return ___linePositionOffset_4; }
	inline int32_t* get_address_of_linePositionOffset_4() { return &___linePositionOffset_4; }
	inline void set_linePositionOffset_4(int32_t value)
	{
		___linePositionOffset_4 = value;
	}

	inline static int32_t get_offset_of_conformanceLevel_5() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___conformanceLevel_5)); }
	inline int32_t get_conformanceLevel_5() const { return ___conformanceLevel_5; }
	inline int32_t* get_address_of_conformanceLevel_5() { return &___conformanceLevel_5; }
	inline void set_conformanceLevel_5(int32_t value)
	{
		___conformanceLevel_5 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_6() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___checkCharacters_6)); }
	inline bool get_checkCharacters_6() const { return ___checkCharacters_6; }
	inline bool* get_address_of_checkCharacters_6() { return &___checkCharacters_6; }
	inline void set_checkCharacters_6(bool value)
	{
		___checkCharacters_6 = value;
	}

	inline static int32_t get_offset_of_maxCharactersInDocument_7() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___maxCharactersInDocument_7)); }
	inline int64_t get_maxCharactersInDocument_7() const { return ___maxCharactersInDocument_7; }
	inline int64_t* get_address_of_maxCharactersInDocument_7() { return &___maxCharactersInDocument_7; }
	inline void set_maxCharactersInDocument_7(int64_t value)
	{
		___maxCharactersInDocument_7 = value;
	}

	inline static int32_t get_offset_of_maxCharactersFromEntities_8() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___maxCharactersFromEntities_8)); }
	inline int64_t get_maxCharactersFromEntities_8() const { return ___maxCharactersFromEntities_8; }
	inline int64_t* get_address_of_maxCharactersFromEntities_8() { return &___maxCharactersFromEntities_8; }
	inline void set_maxCharactersFromEntities_8(int64_t value)
	{
		___maxCharactersFromEntities_8 = value;
	}

	inline static int32_t get_offset_of_ignoreWhitespace_9() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___ignoreWhitespace_9)); }
	inline bool get_ignoreWhitespace_9() const { return ___ignoreWhitespace_9; }
	inline bool* get_address_of_ignoreWhitespace_9() { return &___ignoreWhitespace_9; }
	inline void set_ignoreWhitespace_9(bool value)
	{
		___ignoreWhitespace_9 = value;
	}

	inline static int32_t get_offset_of_ignorePIs_10() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___ignorePIs_10)); }
	inline bool get_ignorePIs_10() const { return ___ignorePIs_10; }
	inline bool* get_address_of_ignorePIs_10() { return &___ignorePIs_10; }
	inline void set_ignorePIs_10(bool value)
	{
		___ignorePIs_10 = value;
	}

	inline static int32_t get_offset_of_ignoreComments_11() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___ignoreComments_11)); }
	inline bool get_ignoreComments_11() const { return ___ignoreComments_11; }
	inline bool* get_address_of_ignoreComments_11() { return &___ignoreComments_11; }
	inline void set_ignoreComments_11(bool value)
	{
		___ignoreComments_11 = value;
	}

	inline static int32_t get_offset_of_dtdProcessing_12() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___dtdProcessing_12)); }
	inline int32_t get_dtdProcessing_12() const { return ___dtdProcessing_12; }
	inline int32_t* get_address_of_dtdProcessing_12() { return &___dtdProcessing_12; }
	inline void set_dtdProcessing_12(int32_t value)
	{
		___dtdProcessing_12 = value;
	}

	inline static int32_t get_offset_of_validationType_13() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___validationType_13)); }
	inline int32_t get_validationType_13() const { return ___validationType_13; }
	inline int32_t* get_address_of_validationType_13() { return &___validationType_13; }
	inline void set_validationType_13(int32_t value)
	{
		___validationType_13 = value;
	}

	inline static int32_t get_offset_of_validationFlags_14() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___validationFlags_14)); }
	inline int32_t get_validationFlags_14() const { return ___validationFlags_14; }
	inline int32_t* get_address_of_validationFlags_14() { return &___validationFlags_14; }
	inline void set_validationFlags_14(int32_t value)
	{
		___validationFlags_14 = value;
	}

	inline static int32_t get_offset_of_schemas_15() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___schemas_15)); }
	inline XmlSchemaSet_t266093086 * get_schemas_15() const { return ___schemas_15; }
	inline XmlSchemaSet_t266093086 ** get_address_of_schemas_15() { return &___schemas_15; }
	inline void set_schemas_15(XmlSchemaSet_t266093086 * value)
	{
		___schemas_15 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_15), value);
	}

	inline static int32_t get_offset_of_closeInput_16() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___closeInput_16)); }
	inline bool get_closeInput_16() const { return ___closeInput_16; }
	inline bool* get_address_of_closeInput_16() { return &___closeInput_16; }
	inline void set_closeInput_16(bool value)
	{
		___closeInput_16 = value;
	}

	inline static int32_t get_offset_of_isReadOnly_17() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___isReadOnly_17)); }
	inline bool get_isReadOnly_17() const { return ___isReadOnly_17; }
	inline bool* get_address_of_isReadOnly_17() { return &___isReadOnly_17; }
	inline void set_isReadOnly_17(bool value)
	{
		___isReadOnly_17 = value;
	}

	inline static int32_t get_offset_of_U3CIsXmlResolverSetU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___U3CIsXmlResolverSetU3Ek__BackingField_18)); }
	inline bool get_U3CIsXmlResolverSetU3Ek__BackingField_18() const { return ___U3CIsXmlResolverSetU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CIsXmlResolverSetU3Ek__BackingField_18() { return &___U3CIsXmlResolverSetU3Ek__BackingField_18; }
	inline void set_U3CIsXmlResolverSetU3Ek__BackingField_18(bool value)
	{
		___U3CIsXmlResolverSetU3Ek__BackingField_18 = value;
	}
};

struct XmlReaderSettings_t2186285234_StaticFields
{
public:
	// System.Nullable`1<System.Boolean> System.Xml.XmlReaderSettings::s_enableLegacyXmlSettings
	Nullable_1_t1819850047  ___s_enableLegacyXmlSettings_19;

public:
	inline static int32_t get_offset_of_s_enableLegacyXmlSettings_19() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234_StaticFields, ___s_enableLegacyXmlSettings_19)); }
	inline Nullable_1_t1819850047  get_s_enableLegacyXmlSettings_19() const { return ___s_enableLegacyXmlSettings_19; }
	inline Nullable_1_t1819850047 * get_address_of_s_enableLegacyXmlSettings_19() { return &___s_enableLegacyXmlSettings_19; }
	inline void set_s_enableLegacyXmlSettings_19(Nullable_1_t1819850047  value)
	{
		___s_enableLegacyXmlSettings_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADERSETTINGS_T2186285234_H
#ifndef LATERINITPARAM_T1449395818_H
#define LATERINITPARAM_T1449395818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/LaterInitParam
struct  LaterInitParam_t1449395818  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.XmlTextReaderImpl/LaterInitParam::useAsync
	bool ___useAsync_0;
	// System.IO.Stream System.Xml.XmlTextReaderImpl/LaterInitParam::inputStream
	Stream_t1273022909 * ___inputStream_1;
	// System.Byte[] System.Xml.XmlTextReaderImpl/LaterInitParam::inputBytes
	ByteU5BU5D_t4116647657* ___inputBytes_2;
	// System.Int32 System.Xml.XmlTextReaderImpl/LaterInitParam::inputByteCount
	int32_t ___inputByteCount_3;
	// System.Uri System.Xml.XmlTextReaderImpl/LaterInitParam::inputbaseUri
	Uri_t100236324 * ___inputbaseUri_4;
	// System.String System.Xml.XmlTextReaderImpl/LaterInitParam::inputUriStr
	String_t* ___inputUriStr_5;
	// System.Xml.XmlResolver System.Xml.XmlTextReaderImpl/LaterInitParam::inputUriResolver
	XmlResolver_t626023767 * ___inputUriResolver_6;
	// System.Xml.XmlParserContext System.Xml.XmlTextReaderImpl/LaterInitParam::inputContext
	XmlParserContext_t2544895291 * ___inputContext_7;
	// System.IO.TextReader System.Xml.XmlTextReaderImpl/LaterInitParam::inputTextReader
	TextReader_t283511965 * ___inputTextReader_8;
	// System.Xml.XmlTextReaderImpl/InitInputType System.Xml.XmlTextReaderImpl/LaterInitParam::initType
	int32_t ___initType_9;

public:
	inline static int32_t get_offset_of_useAsync_0() { return static_cast<int32_t>(offsetof(LaterInitParam_t1449395818, ___useAsync_0)); }
	inline bool get_useAsync_0() const { return ___useAsync_0; }
	inline bool* get_address_of_useAsync_0() { return &___useAsync_0; }
	inline void set_useAsync_0(bool value)
	{
		___useAsync_0 = value;
	}

	inline static int32_t get_offset_of_inputStream_1() { return static_cast<int32_t>(offsetof(LaterInitParam_t1449395818, ___inputStream_1)); }
	inline Stream_t1273022909 * get_inputStream_1() const { return ___inputStream_1; }
	inline Stream_t1273022909 ** get_address_of_inputStream_1() { return &___inputStream_1; }
	inline void set_inputStream_1(Stream_t1273022909 * value)
	{
		___inputStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___inputStream_1), value);
	}

	inline static int32_t get_offset_of_inputBytes_2() { return static_cast<int32_t>(offsetof(LaterInitParam_t1449395818, ___inputBytes_2)); }
	inline ByteU5BU5D_t4116647657* get_inputBytes_2() const { return ___inputBytes_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_inputBytes_2() { return &___inputBytes_2; }
	inline void set_inputBytes_2(ByteU5BU5D_t4116647657* value)
	{
		___inputBytes_2 = value;
		Il2CppCodeGenWriteBarrier((&___inputBytes_2), value);
	}

	inline static int32_t get_offset_of_inputByteCount_3() { return static_cast<int32_t>(offsetof(LaterInitParam_t1449395818, ___inputByteCount_3)); }
	inline int32_t get_inputByteCount_3() const { return ___inputByteCount_3; }
	inline int32_t* get_address_of_inputByteCount_3() { return &___inputByteCount_3; }
	inline void set_inputByteCount_3(int32_t value)
	{
		___inputByteCount_3 = value;
	}

	inline static int32_t get_offset_of_inputbaseUri_4() { return static_cast<int32_t>(offsetof(LaterInitParam_t1449395818, ___inputbaseUri_4)); }
	inline Uri_t100236324 * get_inputbaseUri_4() const { return ___inputbaseUri_4; }
	inline Uri_t100236324 ** get_address_of_inputbaseUri_4() { return &___inputbaseUri_4; }
	inline void set_inputbaseUri_4(Uri_t100236324 * value)
	{
		___inputbaseUri_4 = value;
		Il2CppCodeGenWriteBarrier((&___inputbaseUri_4), value);
	}

	inline static int32_t get_offset_of_inputUriStr_5() { return static_cast<int32_t>(offsetof(LaterInitParam_t1449395818, ___inputUriStr_5)); }
	inline String_t* get_inputUriStr_5() const { return ___inputUriStr_5; }
	inline String_t** get_address_of_inputUriStr_5() { return &___inputUriStr_5; }
	inline void set_inputUriStr_5(String_t* value)
	{
		___inputUriStr_5 = value;
		Il2CppCodeGenWriteBarrier((&___inputUriStr_5), value);
	}

	inline static int32_t get_offset_of_inputUriResolver_6() { return static_cast<int32_t>(offsetof(LaterInitParam_t1449395818, ___inputUriResolver_6)); }
	inline XmlResolver_t626023767 * get_inputUriResolver_6() const { return ___inputUriResolver_6; }
	inline XmlResolver_t626023767 ** get_address_of_inputUriResolver_6() { return &___inputUriResolver_6; }
	inline void set_inputUriResolver_6(XmlResolver_t626023767 * value)
	{
		___inputUriResolver_6 = value;
		Il2CppCodeGenWriteBarrier((&___inputUriResolver_6), value);
	}

	inline static int32_t get_offset_of_inputContext_7() { return static_cast<int32_t>(offsetof(LaterInitParam_t1449395818, ___inputContext_7)); }
	inline XmlParserContext_t2544895291 * get_inputContext_7() const { return ___inputContext_7; }
	inline XmlParserContext_t2544895291 ** get_address_of_inputContext_7() { return &___inputContext_7; }
	inline void set_inputContext_7(XmlParserContext_t2544895291 * value)
	{
		___inputContext_7 = value;
		Il2CppCodeGenWriteBarrier((&___inputContext_7), value);
	}

	inline static int32_t get_offset_of_inputTextReader_8() { return static_cast<int32_t>(offsetof(LaterInitParam_t1449395818, ___inputTextReader_8)); }
	inline TextReader_t283511965 * get_inputTextReader_8() const { return ___inputTextReader_8; }
	inline TextReader_t283511965 ** get_address_of_inputTextReader_8() { return &___inputTextReader_8; }
	inline void set_inputTextReader_8(TextReader_t283511965 * value)
	{
		___inputTextReader_8 = value;
		Il2CppCodeGenWriteBarrier((&___inputTextReader_8), value);
	}

	inline static int32_t get_offset_of_initType_9() { return static_cast<int32_t>(offsetof(LaterInitParam_t1449395818, ___initType_9)); }
	inline int32_t get_initType_9() const { return ___initType_9; }
	inline int32_t* get_address_of_initType_9() { return &___initType_9; }
	inline void set_initType_9(int32_t value)
	{
		___initType_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATERINITPARAM_T1449395818_H
#ifndef MULTICASTDELEGATE_T157516450_H
#define MULTICASTDELEGATE_T157516450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t157516450  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t157516450, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T157516450_H
#ifndef XMLTEXTREADERIMPL_T178060594_H
#define XMLTEXTREADERIMPL_T178060594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl
struct  XmlTextReaderImpl_t178060594  : public XmlReader_t3121518892
{
public:
	// System.Threading.Tasks.Task`1<System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>> System.Xml.XmlTextReaderImpl::parseText_dummyTask
	Task_1_t1685771062 * ___parseText_dummyTask_3;
	// System.Xml.XmlTextReaderImpl/LaterInitParam System.Xml.XmlTextReaderImpl::laterInitParam
	LaterInitParam_t1449395818 * ___laterInitParam_4;
	// System.Xml.XmlCharType System.Xml.XmlTextReaderImpl::xmlCharType
	XmlCharType_t2277243275  ___xmlCharType_5;
	// System.Xml.XmlTextReaderImpl/ParsingState System.Xml.XmlTextReaderImpl::ps
	ParsingState_t1780334922  ___ps_6;
	// System.Xml.XmlTextReaderImpl/ParsingFunction System.Xml.XmlTextReaderImpl::parsingFunction
	int32_t ___parsingFunction_7;
	// System.Xml.XmlTextReaderImpl/ParsingFunction System.Xml.XmlTextReaderImpl::nextParsingFunction
	int32_t ___nextParsingFunction_8;
	// System.Xml.XmlTextReaderImpl/ParsingFunction System.Xml.XmlTextReaderImpl::nextNextParsingFunction
	int32_t ___nextNextParsingFunction_9;
	// System.Xml.XmlTextReaderImpl/NodeData[] System.Xml.XmlTextReaderImpl::nodes
	NodeDataU5BU5D_t1309219640* ___nodes_10;
	// System.Xml.XmlTextReaderImpl/NodeData System.Xml.XmlTextReaderImpl::curNode
	NodeData_t1817330133 * ___curNode_11;
	// System.Int32 System.Xml.XmlTextReaderImpl::index
	int32_t ___index_12;
	// System.Int32 System.Xml.XmlTextReaderImpl::curAttrIndex
	int32_t ___curAttrIndex_13;
	// System.Int32 System.Xml.XmlTextReaderImpl::attrCount
	int32_t ___attrCount_14;
	// System.Int32 System.Xml.XmlTextReaderImpl::attrHashtable
	int32_t ___attrHashtable_15;
	// System.Int32 System.Xml.XmlTextReaderImpl::attrDuplWalkCount
	int32_t ___attrDuplWalkCount_16;
	// System.Boolean System.Xml.XmlTextReaderImpl::attrNeedNamespaceLookup
	bool ___attrNeedNamespaceLookup_17;
	// System.Boolean System.Xml.XmlTextReaderImpl::fullAttrCleanup
	bool ___fullAttrCleanup_18;
	// System.Xml.XmlTextReaderImpl/NodeData[] System.Xml.XmlTextReaderImpl::attrDuplSortingArray
	NodeDataU5BU5D_t1309219640* ___attrDuplSortingArray_19;
	// System.Xml.XmlNameTable System.Xml.XmlTextReaderImpl::nameTable
	XmlNameTable_t71772148 * ___nameTable_20;
	// System.Boolean System.Xml.XmlTextReaderImpl::nameTableFromSettings
	bool ___nameTableFromSettings_21;
	// System.Xml.XmlResolver System.Xml.XmlTextReaderImpl::xmlResolver
	XmlResolver_t626023767 * ___xmlResolver_22;
	// System.String System.Xml.XmlTextReaderImpl::url
	String_t* ___url_23;
	// System.Boolean System.Xml.XmlTextReaderImpl::normalize
	bool ___normalize_24;
	// System.Boolean System.Xml.XmlTextReaderImpl::supportNamespaces
	bool ___supportNamespaces_25;
	// System.Xml.WhitespaceHandling System.Xml.XmlTextReaderImpl::whitespaceHandling
	int32_t ___whitespaceHandling_26;
	// System.Xml.DtdProcessing System.Xml.XmlTextReaderImpl::dtdProcessing
	int32_t ___dtdProcessing_27;
	// System.Xml.EntityHandling System.Xml.XmlTextReaderImpl::entityHandling
	int32_t ___entityHandling_28;
	// System.Boolean System.Xml.XmlTextReaderImpl::ignorePIs
	bool ___ignorePIs_29;
	// System.Boolean System.Xml.XmlTextReaderImpl::ignoreComments
	bool ___ignoreComments_30;
	// System.Boolean System.Xml.XmlTextReaderImpl::checkCharacters
	bool ___checkCharacters_31;
	// System.Int32 System.Xml.XmlTextReaderImpl::lineNumberOffset
	int32_t ___lineNumberOffset_32;
	// System.Int32 System.Xml.XmlTextReaderImpl::linePositionOffset
	int32_t ___linePositionOffset_33;
	// System.Boolean System.Xml.XmlTextReaderImpl::closeInput
	bool ___closeInput_34;
	// System.Int64 System.Xml.XmlTextReaderImpl::maxCharactersInDocument
	int64_t ___maxCharactersInDocument_35;
	// System.Int64 System.Xml.XmlTextReaderImpl::maxCharactersFromEntities
	int64_t ___maxCharactersFromEntities_36;
	// System.Boolean System.Xml.XmlTextReaderImpl::v1Compat
	bool ___v1Compat_37;
	// System.Xml.XmlNamespaceManager System.Xml.XmlTextReaderImpl::namespaceManager
	XmlNamespaceManager_t418790500 * ___namespaceManager_38;
	// System.String System.Xml.XmlTextReaderImpl::lastPrefix
	String_t* ___lastPrefix_39;
	// System.Xml.XmlTextReaderImpl/XmlContext System.Xml.XmlTextReaderImpl::xmlContext
	XmlContext_t1618903103 * ___xmlContext_40;
	// System.Xml.XmlTextReaderImpl/ParsingState[] System.Xml.XmlTextReaderImpl::parsingStatesStack
	ParsingStateU5BU5D_t1980313167* ___parsingStatesStack_41;
	// System.Int32 System.Xml.XmlTextReaderImpl::parsingStatesStackTop
	int32_t ___parsingStatesStackTop_42;
	// System.String System.Xml.XmlTextReaderImpl::reportedBaseUri
	String_t* ___reportedBaseUri_43;
	// System.Text.Encoding System.Xml.XmlTextReaderImpl::reportedEncoding
	Encoding_t1523322056 * ___reportedEncoding_44;
	// System.Xml.IDtdInfo System.Xml.XmlTextReaderImpl::dtdInfo
	RuntimeObject* ___dtdInfo_45;
	// System.Xml.XmlNodeType System.Xml.XmlTextReaderImpl::fragmentType
	int32_t ___fragmentType_46;
	// System.Xml.XmlParserContext System.Xml.XmlTextReaderImpl::fragmentParserContext
	XmlParserContext_t2544895291 * ___fragmentParserContext_47;
	// System.Boolean System.Xml.XmlTextReaderImpl::fragment
	bool ___fragment_48;
	// System.Xml.IncrementalReadDecoder System.Xml.XmlTextReaderImpl::incReadDecoder
	IncrementalReadDecoder_t3011954239 * ___incReadDecoder_49;
	// System.Xml.XmlTextReaderImpl/IncrementalReadState System.Xml.XmlTextReaderImpl::incReadState
	int32_t ___incReadState_50;
	// System.Xml.LineInfo System.Xml.XmlTextReaderImpl::incReadLineInfo
	LineInfo_t3266778363  ___incReadLineInfo_51;
	// System.Int32 System.Xml.XmlTextReaderImpl::incReadDepth
	int32_t ___incReadDepth_52;
	// System.Int32 System.Xml.XmlTextReaderImpl::incReadLeftStartPos
	int32_t ___incReadLeftStartPos_53;
	// System.Int32 System.Xml.XmlTextReaderImpl::incReadLeftEndPos
	int32_t ___incReadLeftEndPos_54;
	// System.Int32 System.Xml.XmlTextReaderImpl::attributeValueBaseEntityId
	int32_t ___attributeValueBaseEntityId_55;
	// System.Boolean System.Xml.XmlTextReaderImpl::emptyEntityInAttributeResolved
	bool ___emptyEntityInAttributeResolved_56;
	// System.Xml.IValidationEventHandling System.Xml.XmlTextReaderImpl::validationEventHandling
	RuntimeObject* ___validationEventHandling_57;
	// System.Xml.XmlTextReaderImpl/OnDefaultAttributeUseDelegate System.Xml.XmlTextReaderImpl::onDefaultAttributeUse
	OnDefaultAttributeUseDelegate_t2911570364 * ___onDefaultAttributeUse_58;
	// System.Boolean System.Xml.XmlTextReaderImpl::validatingReaderCompatFlag
	bool ___validatingReaderCompatFlag_59;
	// System.Boolean System.Xml.XmlTextReaderImpl::addDefaultAttributesAndNormalize
	bool ___addDefaultAttributesAndNormalize_60;
	// System.Text.StringBuilder System.Xml.XmlTextReaderImpl::stringBuilder
	StringBuilder_t1712802186 * ___stringBuilder_61;
	// System.Boolean System.Xml.XmlTextReaderImpl::rootElementParsed
	bool ___rootElementParsed_62;
	// System.Boolean System.Xml.XmlTextReaderImpl::standalone
	bool ___standalone_63;
	// System.Int32 System.Xml.XmlTextReaderImpl::nextEntityId
	int32_t ___nextEntityId_64;
	// System.Xml.XmlTextReaderImpl/ParsingMode System.Xml.XmlTextReaderImpl::parsingMode
	int32_t ___parsingMode_65;
	// System.Xml.ReadState System.Xml.XmlTextReaderImpl::readState
	int32_t ___readState_66;
	// System.Xml.IDtdEntityInfo System.Xml.XmlTextReaderImpl::lastEntity
	RuntimeObject* ___lastEntity_67;
	// System.Boolean System.Xml.XmlTextReaderImpl::afterResetState
	bool ___afterResetState_68;
	// System.Int32 System.Xml.XmlTextReaderImpl::documentStartBytePos
	int32_t ___documentStartBytePos_69;
	// System.Int32 System.Xml.XmlTextReaderImpl::readValueOffset
	int32_t ___readValueOffset_70;
	// System.Int64 System.Xml.XmlTextReaderImpl::charactersInDocument
	int64_t ___charactersInDocument_71;
	// System.Int64 System.Xml.XmlTextReaderImpl::charactersFromEntities
	int64_t ___charactersFromEntities_72;
	// System.Collections.Generic.Dictionary`2<System.Xml.IDtdEntityInfo,System.Xml.IDtdEntityInfo> System.Xml.XmlTextReaderImpl::currentEntities
	Dictionary_2_t1740447880 * ___currentEntities_73;
	// System.Boolean System.Xml.XmlTextReaderImpl::disableUndeclaredEntityCheck
	bool ___disableUndeclaredEntityCheck_74;
	// System.Xml.XmlReader System.Xml.XmlTextReaderImpl::outerReader
	XmlReader_t3121518892 * ___outerReader_75;
	// System.Boolean System.Xml.XmlTextReaderImpl::xmlResolverIsSet
	bool ___xmlResolverIsSet_76;
	// System.String System.Xml.XmlTextReaderImpl::Xml
	String_t* ___Xml_77;
	// System.String System.Xml.XmlTextReaderImpl::XmlNs
	String_t* ___XmlNs_78;

public:
	inline static int32_t get_offset_of_parseText_dummyTask_3() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___parseText_dummyTask_3)); }
	inline Task_1_t1685771062 * get_parseText_dummyTask_3() const { return ___parseText_dummyTask_3; }
	inline Task_1_t1685771062 ** get_address_of_parseText_dummyTask_3() { return &___parseText_dummyTask_3; }
	inline void set_parseText_dummyTask_3(Task_1_t1685771062 * value)
	{
		___parseText_dummyTask_3 = value;
		Il2CppCodeGenWriteBarrier((&___parseText_dummyTask_3), value);
	}

	inline static int32_t get_offset_of_laterInitParam_4() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___laterInitParam_4)); }
	inline LaterInitParam_t1449395818 * get_laterInitParam_4() const { return ___laterInitParam_4; }
	inline LaterInitParam_t1449395818 ** get_address_of_laterInitParam_4() { return &___laterInitParam_4; }
	inline void set_laterInitParam_4(LaterInitParam_t1449395818 * value)
	{
		___laterInitParam_4 = value;
		Il2CppCodeGenWriteBarrier((&___laterInitParam_4), value);
	}

	inline static int32_t get_offset_of_xmlCharType_5() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___xmlCharType_5)); }
	inline XmlCharType_t2277243275  get_xmlCharType_5() const { return ___xmlCharType_5; }
	inline XmlCharType_t2277243275 * get_address_of_xmlCharType_5() { return &___xmlCharType_5; }
	inline void set_xmlCharType_5(XmlCharType_t2277243275  value)
	{
		___xmlCharType_5 = value;
	}

	inline static int32_t get_offset_of_ps_6() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___ps_6)); }
	inline ParsingState_t1780334922  get_ps_6() const { return ___ps_6; }
	inline ParsingState_t1780334922 * get_address_of_ps_6() { return &___ps_6; }
	inline void set_ps_6(ParsingState_t1780334922  value)
	{
		___ps_6 = value;
	}

	inline static int32_t get_offset_of_parsingFunction_7() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___parsingFunction_7)); }
	inline int32_t get_parsingFunction_7() const { return ___parsingFunction_7; }
	inline int32_t* get_address_of_parsingFunction_7() { return &___parsingFunction_7; }
	inline void set_parsingFunction_7(int32_t value)
	{
		___parsingFunction_7 = value;
	}

	inline static int32_t get_offset_of_nextParsingFunction_8() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___nextParsingFunction_8)); }
	inline int32_t get_nextParsingFunction_8() const { return ___nextParsingFunction_8; }
	inline int32_t* get_address_of_nextParsingFunction_8() { return &___nextParsingFunction_8; }
	inline void set_nextParsingFunction_8(int32_t value)
	{
		___nextParsingFunction_8 = value;
	}

	inline static int32_t get_offset_of_nextNextParsingFunction_9() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___nextNextParsingFunction_9)); }
	inline int32_t get_nextNextParsingFunction_9() const { return ___nextNextParsingFunction_9; }
	inline int32_t* get_address_of_nextNextParsingFunction_9() { return &___nextNextParsingFunction_9; }
	inline void set_nextNextParsingFunction_9(int32_t value)
	{
		___nextNextParsingFunction_9 = value;
	}

	inline static int32_t get_offset_of_nodes_10() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___nodes_10)); }
	inline NodeDataU5BU5D_t1309219640* get_nodes_10() const { return ___nodes_10; }
	inline NodeDataU5BU5D_t1309219640** get_address_of_nodes_10() { return &___nodes_10; }
	inline void set_nodes_10(NodeDataU5BU5D_t1309219640* value)
	{
		___nodes_10 = value;
		Il2CppCodeGenWriteBarrier((&___nodes_10), value);
	}

	inline static int32_t get_offset_of_curNode_11() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___curNode_11)); }
	inline NodeData_t1817330133 * get_curNode_11() const { return ___curNode_11; }
	inline NodeData_t1817330133 ** get_address_of_curNode_11() { return &___curNode_11; }
	inline void set_curNode_11(NodeData_t1817330133 * value)
	{
		___curNode_11 = value;
		Il2CppCodeGenWriteBarrier((&___curNode_11), value);
	}

	inline static int32_t get_offset_of_index_12() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___index_12)); }
	inline int32_t get_index_12() const { return ___index_12; }
	inline int32_t* get_address_of_index_12() { return &___index_12; }
	inline void set_index_12(int32_t value)
	{
		___index_12 = value;
	}

	inline static int32_t get_offset_of_curAttrIndex_13() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___curAttrIndex_13)); }
	inline int32_t get_curAttrIndex_13() const { return ___curAttrIndex_13; }
	inline int32_t* get_address_of_curAttrIndex_13() { return &___curAttrIndex_13; }
	inline void set_curAttrIndex_13(int32_t value)
	{
		___curAttrIndex_13 = value;
	}

	inline static int32_t get_offset_of_attrCount_14() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___attrCount_14)); }
	inline int32_t get_attrCount_14() const { return ___attrCount_14; }
	inline int32_t* get_address_of_attrCount_14() { return &___attrCount_14; }
	inline void set_attrCount_14(int32_t value)
	{
		___attrCount_14 = value;
	}

	inline static int32_t get_offset_of_attrHashtable_15() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___attrHashtable_15)); }
	inline int32_t get_attrHashtable_15() const { return ___attrHashtable_15; }
	inline int32_t* get_address_of_attrHashtable_15() { return &___attrHashtable_15; }
	inline void set_attrHashtable_15(int32_t value)
	{
		___attrHashtable_15 = value;
	}

	inline static int32_t get_offset_of_attrDuplWalkCount_16() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___attrDuplWalkCount_16)); }
	inline int32_t get_attrDuplWalkCount_16() const { return ___attrDuplWalkCount_16; }
	inline int32_t* get_address_of_attrDuplWalkCount_16() { return &___attrDuplWalkCount_16; }
	inline void set_attrDuplWalkCount_16(int32_t value)
	{
		___attrDuplWalkCount_16 = value;
	}

	inline static int32_t get_offset_of_attrNeedNamespaceLookup_17() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___attrNeedNamespaceLookup_17)); }
	inline bool get_attrNeedNamespaceLookup_17() const { return ___attrNeedNamespaceLookup_17; }
	inline bool* get_address_of_attrNeedNamespaceLookup_17() { return &___attrNeedNamespaceLookup_17; }
	inline void set_attrNeedNamespaceLookup_17(bool value)
	{
		___attrNeedNamespaceLookup_17 = value;
	}

	inline static int32_t get_offset_of_fullAttrCleanup_18() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___fullAttrCleanup_18)); }
	inline bool get_fullAttrCleanup_18() const { return ___fullAttrCleanup_18; }
	inline bool* get_address_of_fullAttrCleanup_18() { return &___fullAttrCleanup_18; }
	inline void set_fullAttrCleanup_18(bool value)
	{
		___fullAttrCleanup_18 = value;
	}

	inline static int32_t get_offset_of_attrDuplSortingArray_19() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___attrDuplSortingArray_19)); }
	inline NodeDataU5BU5D_t1309219640* get_attrDuplSortingArray_19() const { return ___attrDuplSortingArray_19; }
	inline NodeDataU5BU5D_t1309219640** get_address_of_attrDuplSortingArray_19() { return &___attrDuplSortingArray_19; }
	inline void set_attrDuplSortingArray_19(NodeDataU5BU5D_t1309219640* value)
	{
		___attrDuplSortingArray_19 = value;
		Il2CppCodeGenWriteBarrier((&___attrDuplSortingArray_19), value);
	}

	inline static int32_t get_offset_of_nameTable_20() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___nameTable_20)); }
	inline XmlNameTable_t71772148 * get_nameTable_20() const { return ___nameTable_20; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_20() { return &___nameTable_20; }
	inline void set_nameTable_20(XmlNameTable_t71772148 * value)
	{
		___nameTable_20 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_20), value);
	}

	inline static int32_t get_offset_of_nameTableFromSettings_21() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___nameTableFromSettings_21)); }
	inline bool get_nameTableFromSettings_21() const { return ___nameTableFromSettings_21; }
	inline bool* get_address_of_nameTableFromSettings_21() { return &___nameTableFromSettings_21; }
	inline void set_nameTableFromSettings_21(bool value)
	{
		___nameTableFromSettings_21 = value;
	}

	inline static int32_t get_offset_of_xmlResolver_22() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___xmlResolver_22)); }
	inline XmlResolver_t626023767 * get_xmlResolver_22() const { return ___xmlResolver_22; }
	inline XmlResolver_t626023767 ** get_address_of_xmlResolver_22() { return &___xmlResolver_22; }
	inline void set_xmlResolver_22(XmlResolver_t626023767 * value)
	{
		___xmlResolver_22 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_22), value);
	}

	inline static int32_t get_offset_of_url_23() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___url_23)); }
	inline String_t* get_url_23() const { return ___url_23; }
	inline String_t** get_address_of_url_23() { return &___url_23; }
	inline void set_url_23(String_t* value)
	{
		___url_23 = value;
		Il2CppCodeGenWriteBarrier((&___url_23), value);
	}

	inline static int32_t get_offset_of_normalize_24() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___normalize_24)); }
	inline bool get_normalize_24() const { return ___normalize_24; }
	inline bool* get_address_of_normalize_24() { return &___normalize_24; }
	inline void set_normalize_24(bool value)
	{
		___normalize_24 = value;
	}

	inline static int32_t get_offset_of_supportNamespaces_25() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___supportNamespaces_25)); }
	inline bool get_supportNamespaces_25() const { return ___supportNamespaces_25; }
	inline bool* get_address_of_supportNamespaces_25() { return &___supportNamespaces_25; }
	inline void set_supportNamespaces_25(bool value)
	{
		___supportNamespaces_25 = value;
	}

	inline static int32_t get_offset_of_whitespaceHandling_26() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___whitespaceHandling_26)); }
	inline int32_t get_whitespaceHandling_26() const { return ___whitespaceHandling_26; }
	inline int32_t* get_address_of_whitespaceHandling_26() { return &___whitespaceHandling_26; }
	inline void set_whitespaceHandling_26(int32_t value)
	{
		___whitespaceHandling_26 = value;
	}

	inline static int32_t get_offset_of_dtdProcessing_27() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___dtdProcessing_27)); }
	inline int32_t get_dtdProcessing_27() const { return ___dtdProcessing_27; }
	inline int32_t* get_address_of_dtdProcessing_27() { return &___dtdProcessing_27; }
	inline void set_dtdProcessing_27(int32_t value)
	{
		___dtdProcessing_27 = value;
	}

	inline static int32_t get_offset_of_entityHandling_28() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___entityHandling_28)); }
	inline int32_t get_entityHandling_28() const { return ___entityHandling_28; }
	inline int32_t* get_address_of_entityHandling_28() { return &___entityHandling_28; }
	inline void set_entityHandling_28(int32_t value)
	{
		___entityHandling_28 = value;
	}

	inline static int32_t get_offset_of_ignorePIs_29() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___ignorePIs_29)); }
	inline bool get_ignorePIs_29() const { return ___ignorePIs_29; }
	inline bool* get_address_of_ignorePIs_29() { return &___ignorePIs_29; }
	inline void set_ignorePIs_29(bool value)
	{
		___ignorePIs_29 = value;
	}

	inline static int32_t get_offset_of_ignoreComments_30() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___ignoreComments_30)); }
	inline bool get_ignoreComments_30() const { return ___ignoreComments_30; }
	inline bool* get_address_of_ignoreComments_30() { return &___ignoreComments_30; }
	inline void set_ignoreComments_30(bool value)
	{
		___ignoreComments_30 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_31() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___checkCharacters_31)); }
	inline bool get_checkCharacters_31() const { return ___checkCharacters_31; }
	inline bool* get_address_of_checkCharacters_31() { return &___checkCharacters_31; }
	inline void set_checkCharacters_31(bool value)
	{
		___checkCharacters_31 = value;
	}

	inline static int32_t get_offset_of_lineNumberOffset_32() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___lineNumberOffset_32)); }
	inline int32_t get_lineNumberOffset_32() const { return ___lineNumberOffset_32; }
	inline int32_t* get_address_of_lineNumberOffset_32() { return &___lineNumberOffset_32; }
	inline void set_lineNumberOffset_32(int32_t value)
	{
		___lineNumberOffset_32 = value;
	}

	inline static int32_t get_offset_of_linePositionOffset_33() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___linePositionOffset_33)); }
	inline int32_t get_linePositionOffset_33() const { return ___linePositionOffset_33; }
	inline int32_t* get_address_of_linePositionOffset_33() { return &___linePositionOffset_33; }
	inline void set_linePositionOffset_33(int32_t value)
	{
		___linePositionOffset_33 = value;
	}

	inline static int32_t get_offset_of_closeInput_34() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___closeInput_34)); }
	inline bool get_closeInput_34() const { return ___closeInput_34; }
	inline bool* get_address_of_closeInput_34() { return &___closeInput_34; }
	inline void set_closeInput_34(bool value)
	{
		___closeInput_34 = value;
	}

	inline static int32_t get_offset_of_maxCharactersInDocument_35() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___maxCharactersInDocument_35)); }
	inline int64_t get_maxCharactersInDocument_35() const { return ___maxCharactersInDocument_35; }
	inline int64_t* get_address_of_maxCharactersInDocument_35() { return &___maxCharactersInDocument_35; }
	inline void set_maxCharactersInDocument_35(int64_t value)
	{
		___maxCharactersInDocument_35 = value;
	}

	inline static int32_t get_offset_of_maxCharactersFromEntities_36() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___maxCharactersFromEntities_36)); }
	inline int64_t get_maxCharactersFromEntities_36() const { return ___maxCharactersFromEntities_36; }
	inline int64_t* get_address_of_maxCharactersFromEntities_36() { return &___maxCharactersFromEntities_36; }
	inline void set_maxCharactersFromEntities_36(int64_t value)
	{
		___maxCharactersFromEntities_36 = value;
	}

	inline static int32_t get_offset_of_v1Compat_37() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___v1Compat_37)); }
	inline bool get_v1Compat_37() const { return ___v1Compat_37; }
	inline bool* get_address_of_v1Compat_37() { return &___v1Compat_37; }
	inline void set_v1Compat_37(bool value)
	{
		___v1Compat_37 = value;
	}

	inline static int32_t get_offset_of_namespaceManager_38() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___namespaceManager_38)); }
	inline XmlNamespaceManager_t418790500 * get_namespaceManager_38() const { return ___namespaceManager_38; }
	inline XmlNamespaceManager_t418790500 ** get_address_of_namespaceManager_38() { return &___namespaceManager_38; }
	inline void set_namespaceManager_38(XmlNamespaceManager_t418790500 * value)
	{
		___namespaceManager_38 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceManager_38), value);
	}

	inline static int32_t get_offset_of_lastPrefix_39() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___lastPrefix_39)); }
	inline String_t* get_lastPrefix_39() const { return ___lastPrefix_39; }
	inline String_t** get_address_of_lastPrefix_39() { return &___lastPrefix_39; }
	inline void set_lastPrefix_39(String_t* value)
	{
		___lastPrefix_39 = value;
		Il2CppCodeGenWriteBarrier((&___lastPrefix_39), value);
	}

	inline static int32_t get_offset_of_xmlContext_40() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___xmlContext_40)); }
	inline XmlContext_t1618903103 * get_xmlContext_40() const { return ___xmlContext_40; }
	inline XmlContext_t1618903103 ** get_address_of_xmlContext_40() { return &___xmlContext_40; }
	inline void set_xmlContext_40(XmlContext_t1618903103 * value)
	{
		___xmlContext_40 = value;
		Il2CppCodeGenWriteBarrier((&___xmlContext_40), value);
	}

	inline static int32_t get_offset_of_parsingStatesStack_41() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___parsingStatesStack_41)); }
	inline ParsingStateU5BU5D_t1980313167* get_parsingStatesStack_41() const { return ___parsingStatesStack_41; }
	inline ParsingStateU5BU5D_t1980313167** get_address_of_parsingStatesStack_41() { return &___parsingStatesStack_41; }
	inline void set_parsingStatesStack_41(ParsingStateU5BU5D_t1980313167* value)
	{
		___parsingStatesStack_41 = value;
		Il2CppCodeGenWriteBarrier((&___parsingStatesStack_41), value);
	}

	inline static int32_t get_offset_of_parsingStatesStackTop_42() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___parsingStatesStackTop_42)); }
	inline int32_t get_parsingStatesStackTop_42() const { return ___parsingStatesStackTop_42; }
	inline int32_t* get_address_of_parsingStatesStackTop_42() { return &___parsingStatesStackTop_42; }
	inline void set_parsingStatesStackTop_42(int32_t value)
	{
		___parsingStatesStackTop_42 = value;
	}

	inline static int32_t get_offset_of_reportedBaseUri_43() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___reportedBaseUri_43)); }
	inline String_t* get_reportedBaseUri_43() const { return ___reportedBaseUri_43; }
	inline String_t** get_address_of_reportedBaseUri_43() { return &___reportedBaseUri_43; }
	inline void set_reportedBaseUri_43(String_t* value)
	{
		___reportedBaseUri_43 = value;
		Il2CppCodeGenWriteBarrier((&___reportedBaseUri_43), value);
	}

	inline static int32_t get_offset_of_reportedEncoding_44() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___reportedEncoding_44)); }
	inline Encoding_t1523322056 * get_reportedEncoding_44() const { return ___reportedEncoding_44; }
	inline Encoding_t1523322056 ** get_address_of_reportedEncoding_44() { return &___reportedEncoding_44; }
	inline void set_reportedEncoding_44(Encoding_t1523322056 * value)
	{
		___reportedEncoding_44 = value;
		Il2CppCodeGenWriteBarrier((&___reportedEncoding_44), value);
	}

	inline static int32_t get_offset_of_dtdInfo_45() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___dtdInfo_45)); }
	inline RuntimeObject* get_dtdInfo_45() const { return ___dtdInfo_45; }
	inline RuntimeObject** get_address_of_dtdInfo_45() { return &___dtdInfo_45; }
	inline void set_dtdInfo_45(RuntimeObject* value)
	{
		___dtdInfo_45 = value;
		Il2CppCodeGenWriteBarrier((&___dtdInfo_45), value);
	}

	inline static int32_t get_offset_of_fragmentType_46() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___fragmentType_46)); }
	inline int32_t get_fragmentType_46() const { return ___fragmentType_46; }
	inline int32_t* get_address_of_fragmentType_46() { return &___fragmentType_46; }
	inline void set_fragmentType_46(int32_t value)
	{
		___fragmentType_46 = value;
	}

	inline static int32_t get_offset_of_fragmentParserContext_47() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___fragmentParserContext_47)); }
	inline XmlParserContext_t2544895291 * get_fragmentParserContext_47() const { return ___fragmentParserContext_47; }
	inline XmlParserContext_t2544895291 ** get_address_of_fragmentParserContext_47() { return &___fragmentParserContext_47; }
	inline void set_fragmentParserContext_47(XmlParserContext_t2544895291 * value)
	{
		___fragmentParserContext_47 = value;
		Il2CppCodeGenWriteBarrier((&___fragmentParserContext_47), value);
	}

	inline static int32_t get_offset_of_fragment_48() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___fragment_48)); }
	inline bool get_fragment_48() const { return ___fragment_48; }
	inline bool* get_address_of_fragment_48() { return &___fragment_48; }
	inline void set_fragment_48(bool value)
	{
		___fragment_48 = value;
	}

	inline static int32_t get_offset_of_incReadDecoder_49() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___incReadDecoder_49)); }
	inline IncrementalReadDecoder_t3011954239 * get_incReadDecoder_49() const { return ___incReadDecoder_49; }
	inline IncrementalReadDecoder_t3011954239 ** get_address_of_incReadDecoder_49() { return &___incReadDecoder_49; }
	inline void set_incReadDecoder_49(IncrementalReadDecoder_t3011954239 * value)
	{
		___incReadDecoder_49 = value;
		Il2CppCodeGenWriteBarrier((&___incReadDecoder_49), value);
	}

	inline static int32_t get_offset_of_incReadState_50() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___incReadState_50)); }
	inline int32_t get_incReadState_50() const { return ___incReadState_50; }
	inline int32_t* get_address_of_incReadState_50() { return &___incReadState_50; }
	inline void set_incReadState_50(int32_t value)
	{
		___incReadState_50 = value;
	}

	inline static int32_t get_offset_of_incReadLineInfo_51() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___incReadLineInfo_51)); }
	inline LineInfo_t3266778363  get_incReadLineInfo_51() const { return ___incReadLineInfo_51; }
	inline LineInfo_t3266778363 * get_address_of_incReadLineInfo_51() { return &___incReadLineInfo_51; }
	inline void set_incReadLineInfo_51(LineInfo_t3266778363  value)
	{
		___incReadLineInfo_51 = value;
	}

	inline static int32_t get_offset_of_incReadDepth_52() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___incReadDepth_52)); }
	inline int32_t get_incReadDepth_52() const { return ___incReadDepth_52; }
	inline int32_t* get_address_of_incReadDepth_52() { return &___incReadDepth_52; }
	inline void set_incReadDepth_52(int32_t value)
	{
		___incReadDepth_52 = value;
	}

	inline static int32_t get_offset_of_incReadLeftStartPos_53() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___incReadLeftStartPos_53)); }
	inline int32_t get_incReadLeftStartPos_53() const { return ___incReadLeftStartPos_53; }
	inline int32_t* get_address_of_incReadLeftStartPos_53() { return &___incReadLeftStartPos_53; }
	inline void set_incReadLeftStartPos_53(int32_t value)
	{
		___incReadLeftStartPos_53 = value;
	}

	inline static int32_t get_offset_of_incReadLeftEndPos_54() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___incReadLeftEndPos_54)); }
	inline int32_t get_incReadLeftEndPos_54() const { return ___incReadLeftEndPos_54; }
	inline int32_t* get_address_of_incReadLeftEndPos_54() { return &___incReadLeftEndPos_54; }
	inline void set_incReadLeftEndPos_54(int32_t value)
	{
		___incReadLeftEndPos_54 = value;
	}

	inline static int32_t get_offset_of_attributeValueBaseEntityId_55() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___attributeValueBaseEntityId_55)); }
	inline int32_t get_attributeValueBaseEntityId_55() const { return ___attributeValueBaseEntityId_55; }
	inline int32_t* get_address_of_attributeValueBaseEntityId_55() { return &___attributeValueBaseEntityId_55; }
	inline void set_attributeValueBaseEntityId_55(int32_t value)
	{
		___attributeValueBaseEntityId_55 = value;
	}

	inline static int32_t get_offset_of_emptyEntityInAttributeResolved_56() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___emptyEntityInAttributeResolved_56)); }
	inline bool get_emptyEntityInAttributeResolved_56() const { return ___emptyEntityInAttributeResolved_56; }
	inline bool* get_address_of_emptyEntityInAttributeResolved_56() { return &___emptyEntityInAttributeResolved_56; }
	inline void set_emptyEntityInAttributeResolved_56(bool value)
	{
		___emptyEntityInAttributeResolved_56 = value;
	}

	inline static int32_t get_offset_of_validationEventHandling_57() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___validationEventHandling_57)); }
	inline RuntimeObject* get_validationEventHandling_57() const { return ___validationEventHandling_57; }
	inline RuntimeObject** get_address_of_validationEventHandling_57() { return &___validationEventHandling_57; }
	inline void set_validationEventHandling_57(RuntimeObject* value)
	{
		___validationEventHandling_57 = value;
		Il2CppCodeGenWriteBarrier((&___validationEventHandling_57), value);
	}

	inline static int32_t get_offset_of_onDefaultAttributeUse_58() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___onDefaultAttributeUse_58)); }
	inline OnDefaultAttributeUseDelegate_t2911570364 * get_onDefaultAttributeUse_58() const { return ___onDefaultAttributeUse_58; }
	inline OnDefaultAttributeUseDelegate_t2911570364 ** get_address_of_onDefaultAttributeUse_58() { return &___onDefaultAttributeUse_58; }
	inline void set_onDefaultAttributeUse_58(OnDefaultAttributeUseDelegate_t2911570364 * value)
	{
		___onDefaultAttributeUse_58 = value;
		Il2CppCodeGenWriteBarrier((&___onDefaultAttributeUse_58), value);
	}

	inline static int32_t get_offset_of_validatingReaderCompatFlag_59() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___validatingReaderCompatFlag_59)); }
	inline bool get_validatingReaderCompatFlag_59() const { return ___validatingReaderCompatFlag_59; }
	inline bool* get_address_of_validatingReaderCompatFlag_59() { return &___validatingReaderCompatFlag_59; }
	inline void set_validatingReaderCompatFlag_59(bool value)
	{
		___validatingReaderCompatFlag_59 = value;
	}

	inline static int32_t get_offset_of_addDefaultAttributesAndNormalize_60() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___addDefaultAttributesAndNormalize_60)); }
	inline bool get_addDefaultAttributesAndNormalize_60() const { return ___addDefaultAttributesAndNormalize_60; }
	inline bool* get_address_of_addDefaultAttributesAndNormalize_60() { return &___addDefaultAttributesAndNormalize_60; }
	inline void set_addDefaultAttributesAndNormalize_60(bool value)
	{
		___addDefaultAttributesAndNormalize_60 = value;
	}

	inline static int32_t get_offset_of_stringBuilder_61() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___stringBuilder_61)); }
	inline StringBuilder_t1712802186 * get_stringBuilder_61() const { return ___stringBuilder_61; }
	inline StringBuilder_t1712802186 ** get_address_of_stringBuilder_61() { return &___stringBuilder_61; }
	inline void set_stringBuilder_61(StringBuilder_t1712802186 * value)
	{
		___stringBuilder_61 = value;
		Il2CppCodeGenWriteBarrier((&___stringBuilder_61), value);
	}

	inline static int32_t get_offset_of_rootElementParsed_62() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___rootElementParsed_62)); }
	inline bool get_rootElementParsed_62() const { return ___rootElementParsed_62; }
	inline bool* get_address_of_rootElementParsed_62() { return &___rootElementParsed_62; }
	inline void set_rootElementParsed_62(bool value)
	{
		___rootElementParsed_62 = value;
	}

	inline static int32_t get_offset_of_standalone_63() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___standalone_63)); }
	inline bool get_standalone_63() const { return ___standalone_63; }
	inline bool* get_address_of_standalone_63() { return &___standalone_63; }
	inline void set_standalone_63(bool value)
	{
		___standalone_63 = value;
	}

	inline static int32_t get_offset_of_nextEntityId_64() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___nextEntityId_64)); }
	inline int32_t get_nextEntityId_64() const { return ___nextEntityId_64; }
	inline int32_t* get_address_of_nextEntityId_64() { return &___nextEntityId_64; }
	inline void set_nextEntityId_64(int32_t value)
	{
		___nextEntityId_64 = value;
	}

	inline static int32_t get_offset_of_parsingMode_65() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___parsingMode_65)); }
	inline int32_t get_parsingMode_65() const { return ___parsingMode_65; }
	inline int32_t* get_address_of_parsingMode_65() { return &___parsingMode_65; }
	inline void set_parsingMode_65(int32_t value)
	{
		___parsingMode_65 = value;
	}

	inline static int32_t get_offset_of_readState_66() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___readState_66)); }
	inline int32_t get_readState_66() const { return ___readState_66; }
	inline int32_t* get_address_of_readState_66() { return &___readState_66; }
	inline void set_readState_66(int32_t value)
	{
		___readState_66 = value;
	}

	inline static int32_t get_offset_of_lastEntity_67() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___lastEntity_67)); }
	inline RuntimeObject* get_lastEntity_67() const { return ___lastEntity_67; }
	inline RuntimeObject** get_address_of_lastEntity_67() { return &___lastEntity_67; }
	inline void set_lastEntity_67(RuntimeObject* value)
	{
		___lastEntity_67 = value;
		Il2CppCodeGenWriteBarrier((&___lastEntity_67), value);
	}

	inline static int32_t get_offset_of_afterResetState_68() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___afterResetState_68)); }
	inline bool get_afterResetState_68() const { return ___afterResetState_68; }
	inline bool* get_address_of_afterResetState_68() { return &___afterResetState_68; }
	inline void set_afterResetState_68(bool value)
	{
		___afterResetState_68 = value;
	}

	inline static int32_t get_offset_of_documentStartBytePos_69() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___documentStartBytePos_69)); }
	inline int32_t get_documentStartBytePos_69() const { return ___documentStartBytePos_69; }
	inline int32_t* get_address_of_documentStartBytePos_69() { return &___documentStartBytePos_69; }
	inline void set_documentStartBytePos_69(int32_t value)
	{
		___documentStartBytePos_69 = value;
	}

	inline static int32_t get_offset_of_readValueOffset_70() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___readValueOffset_70)); }
	inline int32_t get_readValueOffset_70() const { return ___readValueOffset_70; }
	inline int32_t* get_address_of_readValueOffset_70() { return &___readValueOffset_70; }
	inline void set_readValueOffset_70(int32_t value)
	{
		___readValueOffset_70 = value;
	}

	inline static int32_t get_offset_of_charactersInDocument_71() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___charactersInDocument_71)); }
	inline int64_t get_charactersInDocument_71() const { return ___charactersInDocument_71; }
	inline int64_t* get_address_of_charactersInDocument_71() { return &___charactersInDocument_71; }
	inline void set_charactersInDocument_71(int64_t value)
	{
		___charactersInDocument_71 = value;
	}

	inline static int32_t get_offset_of_charactersFromEntities_72() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___charactersFromEntities_72)); }
	inline int64_t get_charactersFromEntities_72() const { return ___charactersFromEntities_72; }
	inline int64_t* get_address_of_charactersFromEntities_72() { return &___charactersFromEntities_72; }
	inline void set_charactersFromEntities_72(int64_t value)
	{
		___charactersFromEntities_72 = value;
	}

	inline static int32_t get_offset_of_currentEntities_73() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___currentEntities_73)); }
	inline Dictionary_2_t1740447880 * get_currentEntities_73() const { return ___currentEntities_73; }
	inline Dictionary_2_t1740447880 ** get_address_of_currentEntities_73() { return &___currentEntities_73; }
	inline void set_currentEntities_73(Dictionary_2_t1740447880 * value)
	{
		___currentEntities_73 = value;
		Il2CppCodeGenWriteBarrier((&___currentEntities_73), value);
	}

	inline static int32_t get_offset_of_disableUndeclaredEntityCheck_74() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___disableUndeclaredEntityCheck_74)); }
	inline bool get_disableUndeclaredEntityCheck_74() const { return ___disableUndeclaredEntityCheck_74; }
	inline bool* get_address_of_disableUndeclaredEntityCheck_74() { return &___disableUndeclaredEntityCheck_74; }
	inline void set_disableUndeclaredEntityCheck_74(bool value)
	{
		___disableUndeclaredEntityCheck_74 = value;
	}

	inline static int32_t get_offset_of_outerReader_75() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___outerReader_75)); }
	inline XmlReader_t3121518892 * get_outerReader_75() const { return ___outerReader_75; }
	inline XmlReader_t3121518892 ** get_address_of_outerReader_75() { return &___outerReader_75; }
	inline void set_outerReader_75(XmlReader_t3121518892 * value)
	{
		___outerReader_75 = value;
		Il2CppCodeGenWriteBarrier((&___outerReader_75), value);
	}

	inline static int32_t get_offset_of_xmlResolverIsSet_76() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___xmlResolverIsSet_76)); }
	inline bool get_xmlResolverIsSet_76() const { return ___xmlResolverIsSet_76; }
	inline bool* get_address_of_xmlResolverIsSet_76() { return &___xmlResolverIsSet_76; }
	inline void set_xmlResolverIsSet_76(bool value)
	{
		___xmlResolverIsSet_76 = value;
	}

	inline static int32_t get_offset_of_Xml_77() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___Xml_77)); }
	inline String_t* get_Xml_77() const { return ___Xml_77; }
	inline String_t** get_address_of_Xml_77() { return &___Xml_77; }
	inline void set_Xml_77(String_t* value)
	{
		___Xml_77 = value;
		Il2CppCodeGenWriteBarrier((&___Xml_77), value);
	}

	inline static int32_t get_offset_of_XmlNs_78() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t178060594, ___XmlNs_78)); }
	inline String_t* get_XmlNs_78() const { return ___XmlNs_78; }
	inline String_t** get_address_of_XmlNs_78() { return &___XmlNs_78; }
	inline void set_XmlNs_78(String_t* value)
	{
		___XmlNs_78 = value;
		Il2CppCodeGenWriteBarrier((&___XmlNs_78), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTREADERIMPL_T178060594_H
#ifndef XMLENCODEDRAWTEXTWRITER_T977350554_H
#define XMLENCODEDRAWTEXTWRITER_T977350554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEncodedRawTextWriter
struct  XmlEncodedRawTextWriter_t977350554  : public XmlRawWriter_t722320575
{
public:
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::useAsync
	bool ___useAsync_3;
	// System.Byte[] System.Xml.XmlEncodedRawTextWriter::bufBytes
	ByteU5BU5D_t4116647657* ___bufBytes_4;
	// System.IO.Stream System.Xml.XmlEncodedRawTextWriter::stream
	Stream_t1273022909 * ___stream_5;
	// System.Text.Encoding System.Xml.XmlEncodedRawTextWriter::encoding
	Encoding_t1523322056 * ___encoding_6;
	// System.Xml.XmlCharType System.Xml.XmlEncodedRawTextWriter::xmlCharType
	XmlCharType_t2277243275  ___xmlCharType_7;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::bufPos
	int32_t ___bufPos_8;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::textPos
	int32_t ___textPos_9;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::contentPos
	int32_t ___contentPos_10;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::cdataPos
	int32_t ___cdataPos_11;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::attrEndPos
	int32_t ___attrEndPos_12;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::bufLen
	int32_t ___bufLen_13;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::writeToNull
	bool ___writeToNull_14;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::hadDoubleBracket
	bool ___hadDoubleBracket_15;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::inAttributeValue
	bool ___inAttributeValue_16;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::bufBytesUsed
	int32_t ___bufBytesUsed_17;
	// System.Char[] System.Xml.XmlEncodedRawTextWriter::bufChars
	CharU5BU5D_t3528271667* ___bufChars_18;
	// System.Text.Encoder System.Xml.XmlEncodedRawTextWriter::encoder
	Encoder_t2198218980 * ___encoder_19;
	// System.IO.TextWriter System.Xml.XmlEncodedRawTextWriter::writer
	TextWriter_t3478189236 * ___writer_20;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::trackTextContent
	bool ___trackTextContent_21;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::inTextContent
	bool ___inTextContent_22;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::lastMarkPos
	int32_t ___lastMarkPos_23;
	// System.Int32[] System.Xml.XmlEncodedRawTextWriter::textContentMarks
	Int32U5BU5D_t385246372* ___textContentMarks_24;
	// System.Xml.CharEntityEncoderFallback System.Xml.XmlEncodedRawTextWriter::charEntityFallback
	CharEntityEncoderFallback_t110445598 * ___charEntityFallback_25;
	// System.Xml.NewLineHandling System.Xml.XmlEncodedRawTextWriter::newLineHandling
	int32_t ___newLineHandling_26;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::closeOutput
	bool ___closeOutput_27;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::omitXmlDeclaration
	bool ___omitXmlDeclaration_28;
	// System.String System.Xml.XmlEncodedRawTextWriter::newLineChars
	String_t* ___newLineChars_29;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::checkCharacters
	bool ___checkCharacters_30;
	// System.Xml.XmlStandalone System.Xml.XmlEncodedRawTextWriter::standalone
	int32_t ___standalone_31;
	// System.Xml.XmlOutputMethod System.Xml.XmlEncodedRawTextWriter::outputMethod
	int32_t ___outputMethod_32;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::autoXmlDeclaration
	bool ___autoXmlDeclaration_33;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::mergeCDataSections
	bool ___mergeCDataSections_34;

public:
	inline static int32_t get_offset_of_useAsync_3() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___useAsync_3)); }
	inline bool get_useAsync_3() const { return ___useAsync_3; }
	inline bool* get_address_of_useAsync_3() { return &___useAsync_3; }
	inline void set_useAsync_3(bool value)
	{
		___useAsync_3 = value;
	}

	inline static int32_t get_offset_of_bufBytes_4() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___bufBytes_4)); }
	inline ByteU5BU5D_t4116647657* get_bufBytes_4() const { return ___bufBytes_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_bufBytes_4() { return &___bufBytes_4; }
	inline void set_bufBytes_4(ByteU5BU5D_t4116647657* value)
	{
		___bufBytes_4 = value;
		Il2CppCodeGenWriteBarrier((&___bufBytes_4), value);
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___stream_5)); }
	inline Stream_t1273022909 * get_stream_5() const { return ___stream_5; }
	inline Stream_t1273022909 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_t1273022909 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___encoding_6)); }
	inline Encoding_t1523322056 * get_encoding_6() const { return ___encoding_6; }
	inline Encoding_t1523322056 ** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(Encoding_t1523322056 * value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_6), value);
	}

	inline static int32_t get_offset_of_xmlCharType_7() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___xmlCharType_7)); }
	inline XmlCharType_t2277243275  get_xmlCharType_7() const { return ___xmlCharType_7; }
	inline XmlCharType_t2277243275 * get_address_of_xmlCharType_7() { return &___xmlCharType_7; }
	inline void set_xmlCharType_7(XmlCharType_t2277243275  value)
	{
		___xmlCharType_7 = value;
	}

	inline static int32_t get_offset_of_bufPos_8() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___bufPos_8)); }
	inline int32_t get_bufPos_8() const { return ___bufPos_8; }
	inline int32_t* get_address_of_bufPos_8() { return &___bufPos_8; }
	inline void set_bufPos_8(int32_t value)
	{
		___bufPos_8 = value;
	}

	inline static int32_t get_offset_of_textPos_9() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___textPos_9)); }
	inline int32_t get_textPos_9() const { return ___textPos_9; }
	inline int32_t* get_address_of_textPos_9() { return &___textPos_9; }
	inline void set_textPos_9(int32_t value)
	{
		___textPos_9 = value;
	}

	inline static int32_t get_offset_of_contentPos_10() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___contentPos_10)); }
	inline int32_t get_contentPos_10() const { return ___contentPos_10; }
	inline int32_t* get_address_of_contentPos_10() { return &___contentPos_10; }
	inline void set_contentPos_10(int32_t value)
	{
		___contentPos_10 = value;
	}

	inline static int32_t get_offset_of_cdataPos_11() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___cdataPos_11)); }
	inline int32_t get_cdataPos_11() const { return ___cdataPos_11; }
	inline int32_t* get_address_of_cdataPos_11() { return &___cdataPos_11; }
	inline void set_cdataPos_11(int32_t value)
	{
		___cdataPos_11 = value;
	}

	inline static int32_t get_offset_of_attrEndPos_12() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___attrEndPos_12)); }
	inline int32_t get_attrEndPos_12() const { return ___attrEndPos_12; }
	inline int32_t* get_address_of_attrEndPos_12() { return &___attrEndPos_12; }
	inline void set_attrEndPos_12(int32_t value)
	{
		___attrEndPos_12 = value;
	}

	inline static int32_t get_offset_of_bufLen_13() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___bufLen_13)); }
	inline int32_t get_bufLen_13() const { return ___bufLen_13; }
	inline int32_t* get_address_of_bufLen_13() { return &___bufLen_13; }
	inline void set_bufLen_13(int32_t value)
	{
		___bufLen_13 = value;
	}

	inline static int32_t get_offset_of_writeToNull_14() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___writeToNull_14)); }
	inline bool get_writeToNull_14() const { return ___writeToNull_14; }
	inline bool* get_address_of_writeToNull_14() { return &___writeToNull_14; }
	inline void set_writeToNull_14(bool value)
	{
		___writeToNull_14 = value;
	}

	inline static int32_t get_offset_of_hadDoubleBracket_15() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___hadDoubleBracket_15)); }
	inline bool get_hadDoubleBracket_15() const { return ___hadDoubleBracket_15; }
	inline bool* get_address_of_hadDoubleBracket_15() { return &___hadDoubleBracket_15; }
	inline void set_hadDoubleBracket_15(bool value)
	{
		___hadDoubleBracket_15 = value;
	}

	inline static int32_t get_offset_of_inAttributeValue_16() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___inAttributeValue_16)); }
	inline bool get_inAttributeValue_16() const { return ___inAttributeValue_16; }
	inline bool* get_address_of_inAttributeValue_16() { return &___inAttributeValue_16; }
	inline void set_inAttributeValue_16(bool value)
	{
		___inAttributeValue_16 = value;
	}

	inline static int32_t get_offset_of_bufBytesUsed_17() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___bufBytesUsed_17)); }
	inline int32_t get_bufBytesUsed_17() const { return ___bufBytesUsed_17; }
	inline int32_t* get_address_of_bufBytesUsed_17() { return &___bufBytesUsed_17; }
	inline void set_bufBytesUsed_17(int32_t value)
	{
		___bufBytesUsed_17 = value;
	}

	inline static int32_t get_offset_of_bufChars_18() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___bufChars_18)); }
	inline CharU5BU5D_t3528271667* get_bufChars_18() const { return ___bufChars_18; }
	inline CharU5BU5D_t3528271667** get_address_of_bufChars_18() { return &___bufChars_18; }
	inline void set_bufChars_18(CharU5BU5D_t3528271667* value)
	{
		___bufChars_18 = value;
		Il2CppCodeGenWriteBarrier((&___bufChars_18), value);
	}

	inline static int32_t get_offset_of_encoder_19() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___encoder_19)); }
	inline Encoder_t2198218980 * get_encoder_19() const { return ___encoder_19; }
	inline Encoder_t2198218980 ** get_address_of_encoder_19() { return &___encoder_19; }
	inline void set_encoder_19(Encoder_t2198218980 * value)
	{
		___encoder_19 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_19), value);
	}

	inline static int32_t get_offset_of_writer_20() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___writer_20)); }
	inline TextWriter_t3478189236 * get_writer_20() const { return ___writer_20; }
	inline TextWriter_t3478189236 ** get_address_of_writer_20() { return &___writer_20; }
	inline void set_writer_20(TextWriter_t3478189236 * value)
	{
		___writer_20 = value;
		Il2CppCodeGenWriteBarrier((&___writer_20), value);
	}

	inline static int32_t get_offset_of_trackTextContent_21() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___trackTextContent_21)); }
	inline bool get_trackTextContent_21() const { return ___trackTextContent_21; }
	inline bool* get_address_of_trackTextContent_21() { return &___trackTextContent_21; }
	inline void set_trackTextContent_21(bool value)
	{
		___trackTextContent_21 = value;
	}

	inline static int32_t get_offset_of_inTextContent_22() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___inTextContent_22)); }
	inline bool get_inTextContent_22() const { return ___inTextContent_22; }
	inline bool* get_address_of_inTextContent_22() { return &___inTextContent_22; }
	inline void set_inTextContent_22(bool value)
	{
		___inTextContent_22 = value;
	}

	inline static int32_t get_offset_of_lastMarkPos_23() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___lastMarkPos_23)); }
	inline int32_t get_lastMarkPos_23() const { return ___lastMarkPos_23; }
	inline int32_t* get_address_of_lastMarkPos_23() { return &___lastMarkPos_23; }
	inline void set_lastMarkPos_23(int32_t value)
	{
		___lastMarkPos_23 = value;
	}

	inline static int32_t get_offset_of_textContentMarks_24() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___textContentMarks_24)); }
	inline Int32U5BU5D_t385246372* get_textContentMarks_24() const { return ___textContentMarks_24; }
	inline Int32U5BU5D_t385246372** get_address_of_textContentMarks_24() { return &___textContentMarks_24; }
	inline void set_textContentMarks_24(Int32U5BU5D_t385246372* value)
	{
		___textContentMarks_24 = value;
		Il2CppCodeGenWriteBarrier((&___textContentMarks_24), value);
	}

	inline static int32_t get_offset_of_charEntityFallback_25() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___charEntityFallback_25)); }
	inline CharEntityEncoderFallback_t110445598 * get_charEntityFallback_25() const { return ___charEntityFallback_25; }
	inline CharEntityEncoderFallback_t110445598 ** get_address_of_charEntityFallback_25() { return &___charEntityFallback_25; }
	inline void set_charEntityFallback_25(CharEntityEncoderFallback_t110445598 * value)
	{
		___charEntityFallback_25 = value;
		Il2CppCodeGenWriteBarrier((&___charEntityFallback_25), value);
	}

	inline static int32_t get_offset_of_newLineHandling_26() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___newLineHandling_26)); }
	inline int32_t get_newLineHandling_26() const { return ___newLineHandling_26; }
	inline int32_t* get_address_of_newLineHandling_26() { return &___newLineHandling_26; }
	inline void set_newLineHandling_26(int32_t value)
	{
		___newLineHandling_26 = value;
	}

	inline static int32_t get_offset_of_closeOutput_27() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___closeOutput_27)); }
	inline bool get_closeOutput_27() const { return ___closeOutput_27; }
	inline bool* get_address_of_closeOutput_27() { return &___closeOutput_27; }
	inline void set_closeOutput_27(bool value)
	{
		___closeOutput_27 = value;
	}

	inline static int32_t get_offset_of_omitXmlDeclaration_28() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___omitXmlDeclaration_28)); }
	inline bool get_omitXmlDeclaration_28() const { return ___omitXmlDeclaration_28; }
	inline bool* get_address_of_omitXmlDeclaration_28() { return &___omitXmlDeclaration_28; }
	inline void set_omitXmlDeclaration_28(bool value)
	{
		___omitXmlDeclaration_28 = value;
	}

	inline static int32_t get_offset_of_newLineChars_29() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___newLineChars_29)); }
	inline String_t* get_newLineChars_29() const { return ___newLineChars_29; }
	inline String_t** get_address_of_newLineChars_29() { return &___newLineChars_29; }
	inline void set_newLineChars_29(String_t* value)
	{
		___newLineChars_29 = value;
		Il2CppCodeGenWriteBarrier((&___newLineChars_29), value);
	}

	inline static int32_t get_offset_of_checkCharacters_30() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___checkCharacters_30)); }
	inline bool get_checkCharacters_30() const { return ___checkCharacters_30; }
	inline bool* get_address_of_checkCharacters_30() { return &___checkCharacters_30; }
	inline void set_checkCharacters_30(bool value)
	{
		___checkCharacters_30 = value;
	}

	inline static int32_t get_offset_of_standalone_31() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___standalone_31)); }
	inline int32_t get_standalone_31() const { return ___standalone_31; }
	inline int32_t* get_address_of_standalone_31() { return &___standalone_31; }
	inline void set_standalone_31(int32_t value)
	{
		___standalone_31 = value;
	}

	inline static int32_t get_offset_of_outputMethod_32() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___outputMethod_32)); }
	inline int32_t get_outputMethod_32() const { return ___outputMethod_32; }
	inline int32_t* get_address_of_outputMethod_32() { return &___outputMethod_32; }
	inline void set_outputMethod_32(int32_t value)
	{
		___outputMethod_32 = value;
	}

	inline static int32_t get_offset_of_autoXmlDeclaration_33() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___autoXmlDeclaration_33)); }
	inline bool get_autoXmlDeclaration_33() const { return ___autoXmlDeclaration_33; }
	inline bool* get_address_of_autoXmlDeclaration_33() { return &___autoXmlDeclaration_33; }
	inline void set_autoXmlDeclaration_33(bool value)
	{
		___autoXmlDeclaration_33 = value;
	}

	inline static int32_t get_offset_of_mergeCDataSections_34() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_t977350554, ___mergeCDataSections_34)); }
	inline bool get_mergeCDataSections_34() const { return ___mergeCDataSections_34; }
	inline bool* get_address_of_mergeCDataSections_34() { return &___mergeCDataSections_34; }
	inline void set_mergeCDataSections_34(bool value)
	{
		___mergeCDataSections_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENCODEDRAWTEXTWRITER_T977350554_H
#ifndef XMLPARSERCONTEXT_T2544895291_H
#define XMLPARSERCONTEXT_T2544895291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserContext
struct  XmlParserContext_t2544895291  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.XmlParserContext::_nt
	XmlNameTable_t71772148 * ____nt_0;
	// System.Xml.XmlNamespaceManager System.Xml.XmlParserContext::_nsMgr
	XmlNamespaceManager_t418790500 * ____nsMgr_1;
	// System.String System.Xml.XmlParserContext::_docTypeName
	String_t* ____docTypeName_2;
	// System.String System.Xml.XmlParserContext::_pubId
	String_t* ____pubId_3;
	// System.String System.Xml.XmlParserContext::_sysId
	String_t* ____sysId_4;
	// System.String System.Xml.XmlParserContext::_internalSubset
	String_t* ____internalSubset_5;
	// System.String System.Xml.XmlParserContext::_xmlLang
	String_t* ____xmlLang_6;
	// System.Xml.XmlSpace System.Xml.XmlParserContext::_xmlSpace
	int32_t ____xmlSpace_7;
	// System.String System.Xml.XmlParserContext::_baseURI
	String_t* ____baseURI_8;
	// System.Text.Encoding System.Xml.XmlParserContext::_encoding
	Encoding_t1523322056 * ____encoding_9;

public:
	inline static int32_t get_offset_of__nt_0() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ____nt_0)); }
	inline XmlNameTable_t71772148 * get__nt_0() const { return ____nt_0; }
	inline XmlNameTable_t71772148 ** get_address_of__nt_0() { return &____nt_0; }
	inline void set__nt_0(XmlNameTable_t71772148 * value)
	{
		____nt_0 = value;
		Il2CppCodeGenWriteBarrier((&____nt_0), value);
	}

	inline static int32_t get_offset_of__nsMgr_1() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ____nsMgr_1)); }
	inline XmlNamespaceManager_t418790500 * get__nsMgr_1() const { return ____nsMgr_1; }
	inline XmlNamespaceManager_t418790500 ** get_address_of__nsMgr_1() { return &____nsMgr_1; }
	inline void set__nsMgr_1(XmlNamespaceManager_t418790500 * value)
	{
		____nsMgr_1 = value;
		Il2CppCodeGenWriteBarrier((&____nsMgr_1), value);
	}

	inline static int32_t get_offset_of__docTypeName_2() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ____docTypeName_2)); }
	inline String_t* get__docTypeName_2() const { return ____docTypeName_2; }
	inline String_t** get_address_of__docTypeName_2() { return &____docTypeName_2; }
	inline void set__docTypeName_2(String_t* value)
	{
		____docTypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&____docTypeName_2), value);
	}

	inline static int32_t get_offset_of__pubId_3() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ____pubId_3)); }
	inline String_t* get__pubId_3() const { return ____pubId_3; }
	inline String_t** get_address_of__pubId_3() { return &____pubId_3; }
	inline void set__pubId_3(String_t* value)
	{
		____pubId_3 = value;
		Il2CppCodeGenWriteBarrier((&____pubId_3), value);
	}

	inline static int32_t get_offset_of__sysId_4() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ____sysId_4)); }
	inline String_t* get__sysId_4() const { return ____sysId_4; }
	inline String_t** get_address_of__sysId_4() { return &____sysId_4; }
	inline void set__sysId_4(String_t* value)
	{
		____sysId_4 = value;
		Il2CppCodeGenWriteBarrier((&____sysId_4), value);
	}

	inline static int32_t get_offset_of__internalSubset_5() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ____internalSubset_5)); }
	inline String_t* get__internalSubset_5() const { return ____internalSubset_5; }
	inline String_t** get_address_of__internalSubset_5() { return &____internalSubset_5; }
	inline void set__internalSubset_5(String_t* value)
	{
		____internalSubset_5 = value;
		Il2CppCodeGenWriteBarrier((&____internalSubset_5), value);
	}

	inline static int32_t get_offset_of__xmlLang_6() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ____xmlLang_6)); }
	inline String_t* get__xmlLang_6() const { return ____xmlLang_6; }
	inline String_t** get_address_of__xmlLang_6() { return &____xmlLang_6; }
	inline void set__xmlLang_6(String_t* value)
	{
		____xmlLang_6 = value;
		Il2CppCodeGenWriteBarrier((&____xmlLang_6), value);
	}

	inline static int32_t get_offset_of__xmlSpace_7() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ____xmlSpace_7)); }
	inline int32_t get__xmlSpace_7() const { return ____xmlSpace_7; }
	inline int32_t* get_address_of__xmlSpace_7() { return &____xmlSpace_7; }
	inline void set__xmlSpace_7(int32_t value)
	{
		____xmlSpace_7 = value;
	}

	inline static int32_t get_offset_of__baseURI_8() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ____baseURI_8)); }
	inline String_t* get__baseURI_8() const { return ____baseURI_8; }
	inline String_t** get_address_of__baseURI_8() { return &____baseURI_8; }
	inline void set__baseURI_8(String_t* value)
	{
		____baseURI_8 = value;
		Il2CppCodeGenWriteBarrier((&____baseURI_8), value);
	}

	inline static int32_t get_offset_of__encoding_9() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ____encoding_9)); }
	inline Encoding_t1523322056 * get__encoding_9() const { return ____encoding_9; }
	inline Encoding_t1523322056 ** get_address_of__encoding_9() { return &____encoding_9; }
	inline void set__encoding_9(Encoding_t1523322056 * value)
	{
		____encoding_9 = value;
		Il2CppCodeGenWriteBarrier((&____encoding_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPARSERCONTEXT_T2544895291_H
#ifndef XMLEVENT_T2951711862_H
#define XMLEVENT_T2951711862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEventCache/XmlEvent
struct  XmlEvent_t2951711862 
{
public:
	// System.Xml.XmlEventCache/XmlEventType System.Xml.XmlEventCache/XmlEvent::eventType
	int32_t ___eventType_0;
	// System.String System.Xml.XmlEventCache/XmlEvent::s1
	String_t* ___s1_1;
	// System.String System.Xml.XmlEventCache/XmlEvent::s2
	String_t* ___s2_2;
	// System.String System.Xml.XmlEventCache/XmlEvent::s3
	String_t* ___s3_3;
	// System.Object System.Xml.XmlEventCache/XmlEvent::o
	RuntimeObject * ___o_4;

public:
	inline static int32_t get_offset_of_eventType_0() { return static_cast<int32_t>(offsetof(XmlEvent_t2951711862, ___eventType_0)); }
	inline int32_t get_eventType_0() const { return ___eventType_0; }
	inline int32_t* get_address_of_eventType_0() { return &___eventType_0; }
	inline void set_eventType_0(int32_t value)
	{
		___eventType_0 = value;
	}

	inline static int32_t get_offset_of_s1_1() { return static_cast<int32_t>(offsetof(XmlEvent_t2951711862, ___s1_1)); }
	inline String_t* get_s1_1() const { return ___s1_1; }
	inline String_t** get_address_of_s1_1() { return &___s1_1; }
	inline void set_s1_1(String_t* value)
	{
		___s1_1 = value;
		Il2CppCodeGenWriteBarrier((&___s1_1), value);
	}

	inline static int32_t get_offset_of_s2_2() { return static_cast<int32_t>(offsetof(XmlEvent_t2951711862, ___s2_2)); }
	inline String_t* get_s2_2() const { return ___s2_2; }
	inline String_t** get_address_of_s2_2() { return &___s2_2; }
	inline void set_s2_2(String_t* value)
	{
		___s2_2 = value;
		Il2CppCodeGenWriteBarrier((&___s2_2), value);
	}

	inline static int32_t get_offset_of_s3_3() { return static_cast<int32_t>(offsetof(XmlEvent_t2951711862, ___s3_3)); }
	inline String_t* get_s3_3() const { return ___s3_3; }
	inline String_t** get_address_of_s3_3() { return &___s3_3; }
	inline void set_s3_3(String_t* value)
	{
		___s3_3 = value;
		Il2CppCodeGenWriteBarrier((&___s3_3), value);
	}

	inline static int32_t get_offset_of_o_4() { return static_cast<int32_t>(offsetof(XmlEvent_t2951711862, ___o_4)); }
	inline RuntimeObject * get_o_4() const { return ___o_4; }
	inline RuntimeObject ** get_address_of_o_4() { return &___o_4; }
	inline void set_o_4(RuntimeObject * value)
	{
		___o_4 = value;
		Il2CppCodeGenWriteBarrier((&___o_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlEventCache/XmlEvent
struct XmlEvent_t2951711862_marshaled_pinvoke
{
	int32_t ___eventType_0;
	char* ___s1_1;
	char* ___s2_2;
	char* ___s3_3;
	Il2CppIUnknown* ___o_4;
};
// Native definition for COM marshalling of System.Xml.XmlEventCache/XmlEvent
struct XmlEvent_t2951711862_marshaled_com
{
	int32_t ___eventType_0;
	Il2CppChar* ___s1_1;
	Il2CppChar* ___s2_2;
	Il2CppChar* ___s3_3;
	Il2CppIUnknown* ___o_4;
};
#endif // XMLEVENT_T2951711862_H
#ifndef NAMESPACE_T2679594434_H
#define NAMESPACE_T2679594434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/Namespace
struct  Namespace_t2679594434 
{
public:
	// System.String System.Xml.XmlWellFormedWriter/Namespace::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlWellFormedWriter/Namespace::namespaceUri
	String_t* ___namespaceUri_1;
	// System.Xml.XmlWellFormedWriter/NamespaceKind System.Xml.XmlWellFormedWriter/Namespace::kind
	int32_t ___kind_2;
	// System.Int32 System.Xml.XmlWellFormedWriter/Namespace::prevNsIndex
	int32_t ___prevNsIndex_3;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(Namespace_t2679594434, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_namespaceUri_1() { return static_cast<int32_t>(offsetof(Namespace_t2679594434, ___namespaceUri_1)); }
	inline String_t* get_namespaceUri_1() const { return ___namespaceUri_1; }
	inline String_t** get_address_of_namespaceUri_1() { return &___namespaceUri_1; }
	inline void set_namespaceUri_1(String_t* value)
	{
		___namespaceUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_1), value);
	}

	inline static int32_t get_offset_of_kind_2() { return static_cast<int32_t>(offsetof(Namespace_t2679594434, ___kind_2)); }
	inline int32_t get_kind_2() const { return ___kind_2; }
	inline int32_t* get_address_of_kind_2() { return &___kind_2; }
	inline void set_kind_2(int32_t value)
	{
		___kind_2 = value;
	}

	inline static int32_t get_offset_of_prevNsIndex_3() { return static_cast<int32_t>(offsetof(Namespace_t2679594434, ___prevNsIndex_3)); }
	inline int32_t get_prevNsIndex_3() const { return ___prevNsIndex_3; }
	inline int32_t* get_address_of_prevNsIndex_3() { return &___prevNsIndex_3; }
	inline void set_prevNsIndex_3(int32_t value)
	{
		___prevNsIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlWellFormedWriter/Namespace
struct Namespace_t2679594434_marshaled_pinvoke
{
	char* ___prefix_0;
	char* ___namespaceUri_1;
	int32_t ___kind_2;
	int32_t ___prevNsIndex_3;
};
// Native definition for COM marshalling of System.Xml.XmlWellFormedWriter/Namespace
struct Namespace_t2679594434_marshaled_com
{
	Il2CppChar* ___prefix_0;
	Il2CppChar* ___namespaceUri_1;
	int32_t ___kind_2;
	int32_t ___prevNsIndex_3;
};
#endif // NAMESPACE_T2679594434_H
#ifndef ELEMENTSCOPE_T3058411185_H
#define ELEMENTSCOPE_T3058411185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/ElementScope
struct  ElementScope_t3058411185 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter/ElementScope::prevNSTop
	int32_t ___prevNSTop_0;
	// System.String System.Xml.XmlWellFormedWriter/ElementScope::prefix
	String_t* ___prefix_1;
	// System.String System.Xml.XmlWellFormedWriter/ElementScope::localName
	String_t* ___localName_2;
	// System.String System.Xml.XmlWellFormedWriter/ElementScope::namespaceUri
	String_t* ___namespaceUri_3;
	// System.Xml.XmlSpace System.Xml.XmlWellFormedWriter/ElementScope::xmlSpace
	int32_t ___xmlSpace_4;
	// System.String System.Xml.XmlWellFormedWriter/ElementScope::xmlLang
	String_t* ___xmlLang_5;

public:
	inline static int32_t get_offset_of_prevNSTop_0() { return static_cast<int32_t>(offsetof(ElementScope_t3058411185, ___prevNSTop_0)); }
	inline int32_t get_prevNSTop_0() const { return ___prevNSTop_0; }
	inline int32_t* get_address_of_prevNSTop_0() { return &___prevNSTop_0; }
	inline void set_prevNSTop_0(int32_t value)
	{
		___prevNSTop_0 = value;
	}

	inline static int32_t get_offset_of_prefix_1() { return static_cast<int32_t>(offsetof(ElementScope_t3058411185, ___prefix_1)); }
	inline String_t* get_prefix_1() const { return ___prefix_1; }
	inline String_t** get_address_of_prefix_1() { return &___prefix_1; }
	inline void set_prefix_1(String_t* value)
	{
		___prefix_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_1), value);
	}

	inline static int32_t get_offset_of_localName_2() { return static_cast<int32_t>(offsetof(ElementScope_t3058411185, ___localName_2)); }
	inline String_t* get_localName_2() const { return ___localName_2; }
	inline String_t** get_address_of_localName_2() { return &___localName_2; }
	inline void set_localName_2(String_t* value)
	{
		___localName_2 = value;
		Il2CppCodeGenWriteBarrier((&___localName_2), value);
	}

	inline static int32_t get_offset_of_namespaceUri_3() { return static_cast<int32_t>(offsetof(ElementScope_t3058411185, ___namespaceUri_3)); }
	inline String_t* get_namespaceUri_3() const { return ___namespaceUri_3; }
	inline String_t** get_address_of_namespaceUri_3() { return &___namespaceUri_3; }
	inline void set_namespaceUri_3(String_t* value)
	{
		___namespaceUri_3 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_3), value);
	}

	inline static int32_t get_offset_of_xmlSpace_4() { return static_cast<int32_t>(offsetof(ElementScope_t3058411185, ___xmlSpace_4)); }
	inline int32_t get_xmlSpace_4() const { return ___xmlSpace_4; }
	inline int32_t* get_address_of_xmlSpace_4() { return &___xmlSpace_4; }
	inline void set_xmlSpace_4(int32_t value)
	{
		___xmlSpace_4 = value;
	}

	inline static int32_t get_offset_of_xmlLang_5() { return static_cast<int32_t>(offsetof(ElementScope_t3058411185, ___xmlLang_5)); }
	inline String_t* get_xmlLang_5() const { return ___xmlLang_5; }
	inline String_t** get_address_of_xmlLang_5() { return &___xmlLang_5; }
	inline void set_xmlLang_5(String_t* value)
	{
		___xmlLang_5 = value;
		Il2CppCodeGenWriteBarrier((&___xmlLang_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlWellFormedWriter/ElementScope
struct ElementScope_t3058411185_marshaled_pinvoke
{
	int32_t ___prevNSTop_0;
	char* ___prefix_1;
	char* ___localName_2;
	char* ___namespaceUri_3;
	int32_t ___xmlSpace_4;
	char* ___xmlLang_5;
};
// Native definition for COM marshalling of System.Xml.XmlWellFormedWriter/ElementScope
struct ElementScope_t3058411185_marshaled_com
{
	int32_t ___prevNSTop_0;
	Il2CppChar* ___prefix_1;
	Il2CppChar* ___localName_2;
	Il2CppChar* ___namespaceUri_3;
	int32_t ___xmlSpace_4;
	Il2CppChar* ___xmlLang_5;
};
#endif // ELEMENTSCOPE_T3058411185_H
#ifndef READCONTENTASBINARYHELPER_T3017207972_H
#define READCONTENTASBINARYHELPER_T3017207972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReadContentAsBinaryHelper
struct  ReadContentAsBinaryHelper_t3017207972  : public RuntimeObject
{
public:
	// System.Xml.XmlReader System.Xml.ReadContentAsBinaryHelper::reader
	XmlReader_t3121518892 * ___reader_0;
	// System.Xml.ReadContentAsBinaryHelper/State System.Xml.ReadContentAsBinaryHelper::state
	int32_t ___state_1;
	// System.Int32 System.Xml.ReadContentAsBinaryHelper::valueOffset
	int32_t ___valueOffset_2;
	// System.Boolean System.Xml.ReadContentAsBinaryHelper::isEnd
	bool ___isEnd_3;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(ReadContentAsBinaryHelper_t3017207972, ___reader_0)); }
	inline XmlReader_t3121518892 * get_reader_0() const { return ___reader_0; }
	inline XmlReader_t3121518892 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(XmlReader_t3121518892 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier((&___reader_0), value);
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(ReadContentAsBinaryHelper_t3017207972, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_valueOffset_2() { return static_cast<int32_t>(offsetof(ReadContentAsBinaryHelper_t3017207972, ___valueOffset_2)); }
	inline int32_t get_valueOffset_2() const { return ___valueOffset_2; }
	inline int32_t* get_address_of_valueOffset_2() { return &___valueOffset_2; }
	inline void set_valueOffset_2(int32_t value)
	{
		___valueOffset_2 = value;
	}

	inline static int32_t get_offset_of_isEnd_3() { return static_cast<int32_t>(offsetof(ReadContentAsBinaryHelper_t3017207972, ___isEnd_3)); }
	inline bool get_isEnd_3() const { return ___isEnd_3; }
	inline bool* get_address_of_isEnd_3() { return &___isEnd_3; }
	inline void set_isEnd_3(bool value)
	{
		___isEnd_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READCONTENTASBINARYHELPER_T3017207972_H
#ifndef XMLCDATASECTION_T3267478366_H
#define XMLCDATASECTION_T3267478366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCDataSection
struct  XmlCDataSection_t3267478366  : public XmlCharacterData_t1167807131
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCDATASECTION_T3267478366_H
#ifndef XMLWRITERSETTINGS_T3314986516_H
#define XMLWRITERSETTINGS_T3314986516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWriterSettings
struct  XmlWriterSettings_t3314986516  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.XmlWriterSettings::useAsync
	bool ___useAsync_0;
	// System.Text.Encoding System.Xml.XmlWriterSettings::encoding
	Encoding_t1523322056 * ___encoding_1;
	// System.Boolean System.Xml.XmlWriterSettings::omitXmlDecl
	bool ___omitXmlDecl_2;
	// System.Xml.NewLineHandling System.Xml.XmlWriterSettings::newLineHandling
	int32_t ___newLineHandling_3;
	// System.String System.Xml.XmlWriterSettings::newLineChars
	String_t* ___newLineChars_4;
	// System.Xml.TriState System.Xml.XmlWriterSettings::indent
	int32_t ___indent_5;
	// System.String System.Xml.XmlWriterSettings::indentChars
	String_t* ___indentChars_6;
	// System.Boolean System.Xml.XmlWriterSettings::newLineOnAttributes
	bool ___newLineOnAttributes_7;
	// System.Boolean System.Xml.XmlWriterSettings::closeOutput
	bool ___closeOutput_8;
	// System.Xml.NamespaceHandling System.Xml.XmlWriterSettings::namespaceHandling
	int32_t ___namespaceHandling_9;
	// System.Xml.ConformanceLevel System.Xml.XmlWriterSettings::conformanceLevel
	int32_t ___conformanceLevel_10;
	// System.Boolean System.Xml.XmlWriterSettings::checkCharacters
	bool ___checkCharacters_11;
	// System.Boolean System.Xml.XmlWriterSettings::writeEndDocumentOnClose
	bool ___writeEndDocumentOnClose_12;
	// System.Xml.XmlOutputMethod System.Xml.XmlWriterSettings::outputMethod
	int32_t ___outputMethod_13;
	// System.Collections.Generic.List`1<System.Xml.XmlQualifiedName> System.Xml.XmlWriterSettings::cdataSections
	List_1_t4232729054 * ___cdataSections_14;
	// System.Boolean System.Xml.XmlWriterSettings::doNotEscapeUriAttributes
	bool ___doNotEscapeUriAttributes_15;
	// System.Boolean System.Xml.XmlWriterSettings::mergeCDataSections
	bool ___mergeCDataSections_16;
	// System.String System.Xml.XmlWriterSettings::mediaType
	String_t* ___mediaType_17;
	// System.String System.Xml.XmlWriterSettings::docTypeSystem
	String_t* ___docTypeSystem_18;
	// System.String System.Xml.XmlWriterSettings::docTypePublic
	String_t* ___docTypePublic_19;
	// System.Xml.XmlStandalone System.Xml.XmlWriterSettings::standalone
	int32_t ___standalone_20;
	// System.Boolean System.Xml.XmlWriterSettings::autoXmlDecl
	bool ___autoXmlDecl_21;
	// System.Boolean System.Xml.XmlWriterSettings::isReadOnly
	bool ___isReadOnly_22;

public:
	inline static int32_t get_offset_of_useAsync_0() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___useAsync_0)); }
	inline bool get_useAsync_0() const { return ___useAsync_0; }
	inline bool* get_address_of_useAsync_0() { return &___useAsync_0; }
	inline void set_useAsync_0(bool value)
	{
		___useAsync_0 = value;
	}

	inline static int32_t get_offset_of_encoding_1() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___encoding_1)); }
	inline Encoding_t1523322056 * get_encoding_1() const { return ___encoding_1; }
	inline Encoding_t1523322056 ** get_address_of_encoding_1() { return &___encoding_1; }
	inline void set_encoding_1(Encoding_t1523322056 * value)
	{
		___encoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_1), value);
	}

	inline static int32_t get_offset_of_omitXmlDecl_2() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___omitXmlDecl_2)); }
	inline bool get_omitXmlDecl_2() const { return ___omitXmlDecl_2; }
	inline bool* get_address_of_omitXmlDecl_2() { return &___omitXmlDecl_2; }
	inline void set_omitXmlDecl_2(bool value)
	{
		___omitXmlDecl_2 = value;
	}

	inline static int32_t get_offset_of_newLineHandling_3() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___newLineHandling_3)); }
	inline int32_t get_newLineHandling_3() const { return ___newLineHandling_3; }
	inline int32_t* get_address_of_newLineHandling_3() { return &___newLineHandling_3; }
	inline void set_newLineHandling_3(int32_t value)
	{
		___newLineHandling_3 = value;
	}

	inline static int32_t get_offset_of_newLineChars_4() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___newLineChars_4)); }
	inline String_t* get_newLineChars_4() const { return ___newLineChars_4; }
	inline String_t** get_address_of_newLineChars_4() { return &___newLineChars_4; }
	inline void set_newLineChars_4(String_t* value)
	{
		___newLineChars_4 = value;
		Il2CppCodeGenWriteBarrier((&___newLineChars_4), value);
	}

	inline static int32_t get_offset_of_indent_5() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___indent_5)); }
	inline int32_t get_indent_5() const { return ___indent_5; }
	inline int32_t* get_address_of_indent_5() { return &___indent_5; }
	inline void set_indent_5(int32_t value)
	{
		___indent_5 = value;
	}

	inline static int32_t get_offset_of_indentChars_6() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___indentChars_6)); }
	inline String_t* get_indentChars_6() const { return ___indentChars_6; }
	inline String_t** get_address_of_indentChars_6() { return &___indentChars_6; }
	inline void set_indentChars_6(String_t* value)
	{
		___indentChars_6 = value;
		Il2CppCodeGenWriteBarrier((&___indentChars_6), value);
	}

	inline static int32_t get_offset_of_newLineOnAttributes_7() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___newLineOnAttributes_7)); }
	inline bool get_newLineOnAttributes_7() const { return ___newLineOnAttributes_7; }
	inline bool* get_address_of_newLineOnAttributes_7() { return &___newLineOnAttributes_7; }
	inline void set_newLineOnAttributes_7(bool value)
	{
		___newLineOnAttributes_7 = value;
	}

	inline static int32_t get_offset_of_closeOutput_8() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___closeOutput_8)); }
	inline bool get_closeOutput_8() const { return ___closeOutput_8; }
	inline bool* get_address_of_closeOutput_8() { return &___closeOutput_8; }
	inline void set_closeOutput_8(bool value)
	{
		___closeOutput_8 = value;
	}

	inline static int32_t get_offset_of_namespaceHandling_9() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___namespaceHandling_9)); }
	inline int32_t get_namespaceHandling_9() const { return ___namespaceHandling_9; }
	inline int32_t* get_address_of_namespaceHandling_9() { return &___namespaceHandling_9; }
	inline void set_namespaceHandling_9(int32_t value)
	{
		___namespaceHandling_9 = value;
	}

	inline static int32_t get_offset_of_conformanceLevel_10() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___conformanceLevel_10)); }
	inline int32_t get_conformanceLevel_10() const { return ___conformanceLevel_10; }
	inline int32_t* get_address_of_conformanceLevel_10() { return &___conformanceLevel_10; }
	inline void set_conformanceLevel_10(int32_t value)
	{
		___conformanceLevel_10 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_11() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___checkCharacters_11)); }
	inline bool get_checkCharacters_11() const { return ___checkCharacters_11; }
	inline bool* get_address_of_checkCharacters_11() { return &___checkCharacters_11; }
	inline void set_checkCharacters_11(bool value)
	{
		___checkCharacters_11 = value;
	}

	inline static int32_t get_offset_of_writeEndDocumentOnClose_12() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___writeEndDocumentOnClose_12)); }
	inline bool get_writeEndDocumentOnClose_12() const { return ___writeEndDocumentOnClose_12; }
	inline bool* get_address_of_writeEndDocumentOnClose_12() { return &___writeEndDocumentOnClose_12; }
	inline void set_writeEndDocumentOnClose_12(bool value)
	{
		___writeEndDocumentOnClose_12 = value;
	}

	inline static int32_t get_offset_of_outputMethod_13() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___outputMethod_13)); }
	inline int32_t get_outputMethod_13() const { return ___outputMethod_13; }
	inline int32_t* get_address_of_outputMethod_13() { return &___outputMethod_13; }
	inline void set_outputMethod_13(int32_t value)
	{
		___outputMethod_13 = value;
	}

	inline static int32_t get_offset_of_cdataSections_14() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___cdataSections_14)); }
	inline List_1_t4232729054 * get_cdataSections_14() const { return ___cdataSections_14; }
	inline List_1_t4232729054 ** get_address_of_cdataSections_14() { return &___cdataSections_14; }
	inline void set_cdataSections_14(List_1_t4232729054 * value)
	{
		___cdataSections_14 = value;
		Il2CppCodeGenWriteBarrier((&___cdataSections_14), value);
	}

	inline static int32_t get_offset_of_doNotEscapeUriAttributes_15() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___doNotEscapeUriAttributes_15)); }
	inline bool get_doNotEscapeUriAttributes_15() const { return ___doNotEscapeUriAttributes_15; }
	inline bool* get_address_of_doNotEscapeUriAttributes_15() { return &___doNotEscapeUriAttributes_15; }
	inline void set_doNotEscapeUriAttributes_15(bool value)
	{
		___doNotEscapeUriAttributes_15 = value;
	}

	inline static int32_t get_offset_of_mergeCDataSections_16() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___mergeCDataSections_16)); }
	inline bool get_mergeCDataSections_16() const { return ___mergeCDataSections_16; }
	inline bool* get_address_of_mergeCDataSections_16() { return &___mergeCDataSections_16; }
	inline void set_mergeCDataSections_16(bool value)
	{
		___mergeCDataSections_16 = value;
	}

	inline static int32_t get_offset_of_mediaType_17() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___mediaType_17)); }
	inline String_t* get_mediaType_17() const { return ___mediaType_17; }
	inline String_t** get_address_of_mediaType_17() { return &___mediaType_17; }
	inline void set_mediaType_17(String_t* value)
	{
		___mediaType_17 = value;
		Il2CppCodeGenWriteBarrier((&___mediaType_17), value);
	}

	inline static int32_t get_offset_of_docTypeSystem_18() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___docTypeSystem_18)); }
	inline String_t* get_docTypeSystem_18() const { return ___docTypeSystem_18; }
	inline String_t** get_address_of_docTypeSystem_18() { return &___docTypeSystem_18; }
	inline void set_docTypeSystem_18(String_t* value)
	{
		___docTypeSystem_18 = value;
		Il2CppCodeGenWriteBarrier((&___docTypeSystem_18), value);
	}

	inline static int32_t get_offset_of_docTypePublic_19() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___docTypePublic_19)); }
	inline String_t* get_docTypePublic_19() const { return ___docTypePublic_19; }
	inline String_t** get_address_of_docTypePublic_19() { return &___docTypePublic_19; }
	inline void set_docTypePublic_19(String_t* value)
	{
		___docTypePublic_19 = value;
		Il2CppCodeGenWriteBarrier((&___docTypePublic_19), value);
	}

	inline static int32_t get_offset_of_standalone_20() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___standalone_20)); }
	inline int32_t get_standalone_20() const { return ___standalone_20; }
	inline int32_t* get_address_of_standalone_20() { return &___standalone_20; }
	inline void set_standalone_20(int32_t value)
	{
		___standalone_20 = value;
	}

	inline static int32_t get_offset_of_autoXmlDecl_21() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___autoXmlDecl_21)); }
	inline bool get_autoXmlDecl_21() const { return ___autoXmlDecl_21; }
	inline bool* get_address_of_autoXmlDecl_21() { return &___autoXmlDecl_21; }
	inline void set_autoXmlDecl_21(bool value)
	{
		___autoXmlDecl_21 = value;
	}

	inline static int32_t get_offset_of_isReadOnly_22() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___isReadOnly_22)); }
	inline bool get_isReadOnly_22() const { return ___isReadOnly_22; }
	inline bool* get_address_of_isReadOnly_22() { return &___isReadOnly_22; }
	inline void set_isReadOnly_22(bool value)
	{
		___isReadOnly_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWRITERSETTINGS_T3314986516_H
#ifndef XMLVALIDATINGREADERIMPL_T1817762384_H
#define XMLVALIDATINGREADERIMPL_T1817762384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlValidatingReaderImpl
struct  XmlValidatingReaderImpl_t1817762384  : public XmlReader_t3121518892
{
public:
	// System.Xml.XmlReader System.Xml.XmlValidatingReaderImpl::coreReader
	XmlReader_t3121518892 * ___coreReader_3;
	// System.Xml.XmlTextReaderImpl System.Xml.XmlValidatingReaderImpl::coreReaderImpl
	XmlTextReaderImpl_t178060594 * ___coreReaderImpl_4;
	// System.Xml.IXmlNamespaceResolver System.Xml.XmlValidatingReaderImpl::coreReaderNSResolver
	RuntimeObject* ___coreReaderNSResolver_5;
	// System.Xml.ValidationType System.Xml.XmlValidatingReaderImpl::validationType
	int32_t ___validationType_6;
	// System.Xml.Schema.BaseValidator System.Xml.XmlValidatingReaderImpl::validator
	BaseValidator_t868759770 * ___validator_7;
	// System.Xml.Schema.XmlSchemaCollection System.Xml.XmlValidatingReaderImpl::schemaCollection
	XmlSchemaCollection_t3610399789 * ___schemaCollection_8;
	// System.Boolean System.Xml.XmlValidatingReaderImpl::processIdentityConstraints
	bool ___processIdentityConstraints_9;
	// System.Xml.XmlValidatingReaderImpl/ParsingFunction System.Xml.XmlValidatingReaderImpl::parsingFunction
	int32_t ___parsingFunction_10;
	// System.Xml.XmlValidatingReaderImpl/ValidationEventHandling System.Xml.XmlValidatingReaderImpl::eventHandling
	ValidationEventHandling_t3812646699 * ___eventHandling_11;
	// System.Xml.XmlParserContext System.Xml.XmlValidatingReaderImpl::parserContext
	XmlParserContext_t2544895291 * ___parserContext_12;
	// System.Xml.ReadContentAsBinaryHelper System.Xml.XmlValidatingReaderImpl::readBinaryHelper
	ReadContentAsBinaryHelper_t3017207972 * ___readBinaryHelper_13;
	// System.Xml.XmlReader System.Xml.XmlValidatingReaderImpl::outerReader
	XmlReader_t3121518892 * ___outerReader_14;

public:
	inline static int32_t get_offset_of_coreReader_3() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1817762384, ___coreReader_3)); }
	inline XmlReader_t3121518892 * get_coreReader_3() const { return ___coreReader_3; }
	inline XmlReader_t3121518892 ** get_address_of_coreReader_3() { return &___coreReader_3; }
	inline void set_coreReader_3(XmlReader_t3121518892 * value)
	{
		___coreReader_3 = value;
		Il2CppCodeGenWriteBarrier((&___coreReader_3), value);
	}

	inline static int32_t get_offset_of_coreReaderImpl_4() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1817762384, ___coreReaderImpl_4)); }
	inline XmlTextReaderImpl_t178060594 * get_coreReaderImpl_4() const { return ___coreReaderImpl_4; }
	inline XmlTextReaderImpl_t178060594 ** get_address_of_coreReaderImpl_4() { return &___coreReaderImpl_4; }
	inline void set_coreReaderImpl_4(XmlTextReaderImpl_t178060594 * value)
	{
		___coreReaderImpl_4 = value;
		Il2CppCodeGenWriteBarrier((&___coreReaderImpl_4), value);
	}

	inline static int32_t get_offset_of_coreReaderNSResolver_5() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1817762384, ___coreReaderNSResolver_5)); }
	inline RuntimeObject* get_coreReaderNSResolver_5() const { return ___coreReaderNSResolver_5; }
	inline RuntimeObject** get_address_of_coreReaderNSResolver_5() { return &___coreReaderNSResolver_5; }
	inline void set_coreReaderNSResolver_5(RuntimeObject* value)
	{
		___coreReaderNSResolver_5 = value;
		Il2CppCodeGenWriteBarrier((&___coreReaderNSResolver_5), value);
	}

	inline static int32_t get_offset_of_validationType_6() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1817762384, ___validationType_6)); }
	inline int32_t get_validationType_6() const { return ___validationType_6; }
	inline int32_t* get_address_of_validationType_6() { return &___validationType_6; }
	inline void set_validationType_6(int32_t value)
	{
		___validationType_6 = value;
	}

	inline static int32_t get_offset_of_validator_7() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1817762384, ___validator_7)); }
	inline BaseValidator_t868759770 * get_validator_7() const { return ___validator_7; }
	inline BaseValidator_t868759770 ** get_address_of_validator_7() { return &___validator_7; }
	inline void set_validator_7(BaseValidator_t868759770 * value)
	{
		___validator_7 = value;
		Il2CppCodeGenWriteBarrier((&___validator_7), value);
	}

	inline static int32_t get_offset_of_schemaCollection_8() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1817762384, ___schemaCollection_8)); }
	inline XmlSchemaCollection_t3610399789 * get_schemaCollection_8() const { return ___schemaCollection_8; }
	inline XmlSchemaCollection_t3610399789 ** get_address_of_schemaCollection_8() { return &___schemaCollection_8; }
	inline void set_schemaCollection_8(XmlSchemaCollection_t3610399789 * value)
	{
		___schemaCollection_8 = value;
		Il2CppCodeGenWriteBarrier((&___schemaCollection_8), value);
	}

	inline static int32_t get_offset_of_processIdentityConstraints_9() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1817762384, ___processIdentityConstraints_9)); }
	inline bool get_processIdentityConstraints_9() const { return ___processIdentityConstraints_9; }
	inline bool* get_address_of_processIdentityConstraints_9() { return &___processIdentityConstraints_9; }
	inline void set_processIdentityConstraints_9(bool value)
	{
		___processIdentityConstraints_9 = value;
	}

	inline static int32_t get_offset_of_parsingFunction_10() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1817762384, ___parsingFunction_10)); }
	inline int32_t get_parsingFunction_10() const { return ___parsingFunction_10; }
	inline int32_t* get_address_of_parsingFunction_10() { return &___parsingFunction_10; }
	inline void set_parsingFunction_10(int32_t value)
	{
		___parsingFunction_10 = value;
	}

	inline static int32_t get_offset_of_eventHandling_11() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1817762384, ___eventHandling_11)); }
	inline ValidationEventHandling_t3812646699 * get_eventHandling_11() const { return ___eventHandling_11; }
	inline ValidationEventHandling_t3812646699 ** get_address_of_eventHandling_11() { return &___eventHandling_11; }
	inline void set_eventHandling_11(ValidationEventHandling_t3812646699 * value)
	{
		___eventHandling_11 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandling_11), value);
	}

	inline static int32_t get_offset_of_parserContext_12() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1817762384, ___parserContext_12)); }
	inline XmlParserContext_t2544895291 * get_parserContext_12() const { return ___parserContext_12; }
	inline XmlParserContext_t2544895291 ** get_address_of_parserContext_12() { return &___parserContext_12; }
	inline void set_parserContext_12(XmlParserContext_t2544895291 * value)
	{
		___parserContext_12 = value;
		Il2CppCodeGenWriteBarrier((&___parserContext_12), value);
	}

	inline static int32_t get_offset_of_readBinaryHelper_13() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1817762384, ___readBinaryHelper_13)); }
	inline ReadContentAsBinaryHelper_t3017207972 * get_readBinaryHelper_13() const { return ___readBinaryHelper_13; }
	inline ReadContentAsBinaryHelper_t3017207972 ** get_address_of_readBinaryHelper_13() { return &___readBinaryHelper_13; }
	inline void set_readBinaryHelper_13(ReadContentAsBinaryHelper_t3017207972 * value)
	{
		___readBinaryHelper_13 = value;
		Il2CppCodeGenWriteBarrier((&___readBinaryHelper_13), value);
	}

	inline static int32_t get_offset_of_outerReader_14() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1817762384, ___outerReader_14)); }
	inline XmlReader_t3121518892 * get_outerReader_14() const { return ___outerReader_14; }
	inline XmlReader_t3121518892 ** get_address_of_outerReader_14() { return &___outerReader_14; }
	inline void set_outerReader_14(XmlReader_t3121518892 * value)
	{
		___outerReader_14 = value;
		Il2CppCodeGenWriteBarrier((&___outerReader_14), value);
	}
};

struct XmlValidatingReaderImpl_t1817762384_StaticFields
{
public:
	// System.Xml.XmlResolver System.Xml.XmlValidatingReaderImpl::s_tempResolver
	XmlResolver_t626023767 * ___s_tempResolver_15;

public:
	inline static int32_t get_offset_of_s_tempResolver_15() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1817762384_StaticFields, ___s_tempResolver_15)); }
	inline XmlResolver_t626023767 * get_s_tempResolver_15() const { return ___s_tempResolver_15; }
	inline XmlResolver_t626023767 ** get_address_of_s_tempResolver_15() { return &___s_tempResolver_15; }
	inline void set_s_tempResolver_15(XmlResolver_t626023767 * value)
	{
		___s_tempResolver_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_tempResolver_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLVALIDATINGREADERIMPL_T1817762384_H
#ifndef XMLATTRIBUTECOLLECTION_T2316283784_H
#define XMLATTRIBUTECOLLECTION_T2316283784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAttributeCollection
struct  XmlAttributeCollection_t2316283784  : public XmlNamedNodeMap_t2821286253
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTECOLLECTION_T2316283784_H
#ifndef XMLWELLFORMEDWRITER_T2234459422_H
#define XMLWELLFORMEDWRITER_T2234459422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter
struct  XmlWellFormedWriter_t2234459422  : public XmlWriter_t127905479
{
public:
	// System.Xml.XmlWriter System.Xml.XmlWellFormedWriter::writer
	XmlWriter_t127905479 * ___writer_1;
	// System.Xml.XmlRawWriter System.Xml.XmlWellFormedWriter::rawWriter
	XmlRawWriter_t722320575 * ___rawWriter_2;
	// System.Xml.IXmlNamespaceResolver System.Xml.XmlWellFormedWriter::predefinedNamespaces
	RuntimeObject* ___predefinedNamespaces_3;
	// System.Xml.XmlWellFormedWriter/Namespace[] System.Xml.XmlWellFormedWriter::nsStack
	NamespaceU5BU5D_t1923479799* ___nsStack_4;
	// System.Int32 System.Xml.XmlWellFormedWriter::nsTop
	int32_t ___nsTop_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlWellFormedWriter::nsHashtable
	Dictionary_2_t2736202052 * ___nsHashtable_6;
	// System.Boolean System.Xml.XmlWellFormedWriter::useNsHashtable
	bool ___useNsHashtable_7;
	// System.Xml.XmlWellFormedWriter/ElementScope[] System.Xml.XmlWellFormedWriter::elemScopeStack
	ElementScopeU5BU5D_t4144864108* ___elemScopeStack_8;
	// System.Int32 System.Xml.XmlWellFormedWriter::elemTop
	int32_t ___elemTop_9;
	// System.Xml.XmlWellFormedWriter/AttrName[] System.Xml.XmlWellFormedWriter::attrStack
	AttrNameU5BU5D_t1251672482* ___attrStack_10;
	// System.Int32 System.Xml.XmlWellFormedWriter::attrCount
	int32_t ___attrCount_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlWellFormedWriter::attrHashTable
	Dictionary_2_t2736202052 * ___attrHashTable_12;
	// System.Xml.XmlWellFormedWriter/SpecialAttribute System.Xml.XmlWellFormedWriter::specAttr
	int32_t ___specAttr_13;
	// System.Xml.XmlWellFormedWriter/AttributeValueCache System.Xml.XmlWellFormedWriter::attrValueCache
	AttributeValueCache_t2432505004 * ___attrValueCache_14;
	// System.String System.Xml.XmlWellFormedWriter::curDeclPrefix
	String_t* ___curDeclPrefix_15;
	// System.Xml.XmlWellFormedWriter/State[] System.Xml.XmlWellFormedWriter::stateTable
	StateU5BU5D_t2644572590* ___stateTable_16;
	// System.Xml.XmlWellFormedWriter/State System.Xml.XmlWellFormedWriter::currentState
	int32_t ___currentState_17;
	// System.Boolean System.Xml.XmlWellFormedWriter::checkCharacters
	bool ___checkCharacters_18;
	// System.Boolean System.Xml.XmlWellFormedWriter::omitDuplNamespaces
	bool ___omitDuplNamespaces_19;
	// System.Boolean System.Xml.XmlWellFormedWriter::writeEndDocumentOnClose
	bool ___writeEndDocumentOnClose_20;
	// System.Xml.ConformanceLevel System.Xml.XmlWellFormedWriter::conformanceLevel
	int32_t ___conformanceLevel_21;
	// System.Boolean System.Xml.XmlWellFormedWriter::dtdWritten
	bool ___dtdWritten_22;
	// System.Boolean System.Xml.XmlWellFormedWriter::xmlDeclFollows
	bool ___xmlDeclFollows_23;
	// System.Xml.XmlCharType System.Xml.XmlWellFormedWriter::xmlCharType
	XmlCharType_t2277243275  ___xmlCharType_24;
	// System.Xml.SecureStringHasher System.Xml.XmlWellFormedWriter::hasher
	SecureStringHasher_t95812985 * ___hasher_25;

public:
	inline static int32_t get_offset_of_writer_1() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___writer_1)); }
	inline XmlWriter_t127905479 * get_writer_1() const { return ___writer_1; }
	inline XmlWriter_t127905479 ** get_address_of_writer_1() { return &___writer_1; }
	inline void set_writer_1(XmlWriter_t127905479 * value)
	{
		___writer_1 = value;
		Il2CppCodeGenWriteBarrier((&___writer_1), value);
	}

	inline static int32_t get_offset_of_rawWriter_2() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___rawWriter_2)); }
	inline XmlRawWriter_t722320575 * get_rawWriter_2() const { return ___rawWriter_2; }
	inline XmlRawWriter_t722320575 ** get_address_of_rawWriter_2() { return &___rawWriter_2; }
	inline void set_rawWriter_2(XmlRawWriter_t722320575 * value)
	{
		___rawWriter_2 = value;
		Il2CppCodeGenWriteBarrier((&___rawWriter_2), value);
	}

	inline static int32_t get_offset_of_predefinedNamespaces_3() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___predefinedNamespaces_3)); }
	inline RuntimeObject* get_predefinedNamespaces_3() const { return ___predefinedNamespaces_3; }
	inline RuntimeObject** get_address_of_predefinedNamespaces_3() { return &___predefinedNamespaces_3; }
	inline void set_predefinedNamespaces_3(RuntimeObject* value)
	{
		___predefinedNamespaces_3 = value;
		Il2CppCodeGenWriteBarrier((&___predefinedNamespaces_3), value);
	}

	inline static int32_t get_offset_of_nsStack_4() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___nsStack_4)); }
	inline NamespaceU5BU5D_t1923479799* get_nsStack_4() const { return ___nsStack_4; }
	inline NamespaceU5BU5D_t1923479799** get_address_of_nsStack_4() { return &___nsStack_4; }
	inline void set_nsStack_4(NamespaceU5BU5D_t1923479799* value)
	{
		___nsStack_4 = value;
		Il2CppCodeGenWriteBarrier((&___nsStack_4), value);
	}

	inline static int32_t get_offset_of_nsTop_5() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___nsTop_5)); }
	inline int32_t get_nsTop_5() const { return ___nsTop_5; }
	inline int32_t* get_address_of_nsTop_5() { return &___nsTop_5; }
	inline void set_nsTop_5(int32_t value)
	{
		___nsTop_5 = value;
	}

	inline static int32_t get_offset_of_nsHashtable_6() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___nsHashtable_6)); }
	inline Dictionary_2_t2736202052 * get_nsHashtable_6() const { return ___nsHashtable_6; }
	inline Dictionary_2_t2736202052 ** get_address_of_nsHashtable_6() { return &___nsHashtable_6; }
	inline void set_nsHashtable_6(Dictionary_2_t2736202052 * value)
	{
		___nsHashtable_6 = value;
		Il2CppCodeGenWriteBarrier((&___nsHashtable_6), value);
	}

	inline static int32_t get_offset_of_useNsHashtable_7() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___useNsHashtable_7)); }
	inline bool get_useNsHashtable_7() const { return ___useNsHashtable_7; }
	inline bool* get_address_of_useNsHashtable_7() { return &___useNsHashtable_7; }
	inline void set_useNsHashtable_7(bool value)
	{
		___useNsHashtable_7 = value;
	}

	inline static int32_t get_offset_of_elemScopeStack_8() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___elemScopeStack_8)); }
	inline ElementScopeU5BU5D_t4144864108* get_elemScopeStack_8() const { return ___elemScopeStack_8; }
	inline ElementScopeU5BU5D_t4144864108** get_address_of_elemScopeStack_8() { return &___elemScopeStack_8; }
	inline void set_elemScopeStack_8(ElementScopeU5BU5D_t4144864108* value)
	{
		___elemScopeStack_8 = value;
		Il2CppCodeGenWriteBarrier((&___elemScopeStack_8), value);
	}

	inline static int32_t get_offset_of_elemTop_9() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___elemTop_9)); }
	inline int32_t get_elemTop_9() const { return ___elemTop_9; }
	inline int32_t* get_address_of_elemTop_9() { return &___elemTop_9; }
	inline void set_elemTop_9(int32_t value)
	{
		___elemTop_9 = value;
	}

	inline static int32_t get_offset_of_attrStack_10() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___attrStack_10)); }
	inline AttrNameU5BU5D_t1251672482* get_attrStack_10() const { return ___attrStack_10; }
	inline AttrNameU5BU5D_t1251672482** get_address_of_attrStack_10() { return &___attrStack_10; }
	inline void set_attrStack_10(AttrNameU5BU5D_t1251672482* value)
	{
		___attrStack_10 = value;
		Il2CppCodeGenWriteBarrier((&___attrStack_10), value);
	}

	inline static int32_t get_offset_of_attrCount_11() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___attrCount_11)); }
	inline int32_t get_attrCount_11() const { return ___attrCount_11; }
	inline int32_t* get_address_of_attrCount_11() { return &___attrCount_11; }
	inline void set_attrCount_11(int32_t value)
	{
		___attrCount_11 = value;
	}

	inline static int32_t get_offset_of_attrHashTable_12() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___attrHashTable_12)); }
	inline Dictionary_2_t2736202052 * get_attrHashTable_12() const { return ___attrHashTable_12; }
	inline Dictionary_2_t2736202052 ** get_address_of_attrHashTable_12() { return &___attrHashTable_12; }
	inline void set_attrHashTable_12(Dictionary_2_t2736202052 * value)
	{
		___attrHashTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___attrHashTable_12), value);
	}

	inline static int32_t get_offset_of_specAttr_13() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___specAttr_13)); }
	inline int32_t get_specAttr_13() const { return ___specAttr_13; }
	inline int32_t* get_address_of_specAttr_13() { return &___specAttr_13; }
	inline void set_specAttr_13(int32_t value)
	{
		___specAttr_13 = value;
	}

	inline static int32_t get_offset_of_attrValueCache_14() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___attrValueCache_14)); }
	inline AttributeValueCache_t2432505004 * get_attrValueCache_14() const { return ___attrValueCache_14; }
	inline AttributeValueCache_t2432505004 ** get_address_of_attrValueCache_14() { return &___attrValueCache_14; }
	inline void set_attrValueCache_14(AttributeValueCache_t2432505004 * value)
	{
		___attrValueCache_14 = value;
		Il2CppCodeGenWriteBarrier((&___attrValueCache_14), value);
	}

	inline static int32_t get_offset_of_curDeclPrefix_15() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___curDeclPrefix_15)); }
	inline String_t* get_curDeclPrefix_15() const { return ___curDeclPrefix_15; }
	inline String_t** get_address_of_curDeclPrefix_15() { return &___curDeclPrefix_15; }
	inline void set_curDeclPrefix_15(String_t* value)
	{
		___curDeclPrefix_15 = value;
		Il2CppCodeGenWriteBarrier((&___curDeclPrefix_15), value);
	}

	inline static int32_t get_offset_of_stateTable_16() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___stateTable_16)); }
	inline StateU5BU5D_t2644572590* get_stateTable_16() const { return ___stateTable_16; }
	inline StateU5BU5D_t2644572590** get_address_of_stateTable_16() { return &___stateTable_16; }
	inline void set_stateTable_16(StateU5BU5D_t2644572590* value)
	{
		___stateTable_16 = value;
		Il2CppCodeGenWriteBarrier((&___stateTable_16), value);
	}

	inline static int32_t get_offset_of_currentState_17() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___currentState_17)); }
	inline int32_t get_currentState_17() const { return ___currentState_17; }
	inline int32_t* get_address_of_currentState_17() { return &___currentState_17; }
	inline void set_currentState_17(int32_t value)
	{
		___currentState_17 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_18() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___checkCharacters_18)); }
	inline bool get_checkCharacters_18() const { return ___checkCharacters_18; }
	inline bool* get_address_of_checkCharacters_18() { return &___checkCharacters_18; }
	inline void set_checkCharacters_18(bool value)
	{
		___checkCharacters_18 = value;
	}

	inline static int32_t get_offset_of_omitDuplNamespaces_19() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___omitDuplNamespaces_19)); }
	inline bool get_omitDuplNamespaces_19() const { return ___omitDuplNamespaces_19; }
	inline bool* get_address_of_omitDuplNamespaces_19() { return &___omitDuplNamespaces_19; }
	inline void set_omitDuplNamespaces_19(bool value)
	{
		___omitDuplNamespaces_19 = value;
	}

	inline static int32_t get_offset_of_writeEndDocumentOnClose_20() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___writeEndDocumentOnClose_20)); }
	inline bool get_writeEndDocumentOnClose_20() const { return ___writeEndDocumentOnClose_20; }
	inline bool* get_address_of_writeEndDocumentOnClose_20() { return &___writeEndDocumentOnClose_20; }
	inline void set_writeEndDocumentOnClose_20(bool value)
	{
		___writeEndDocumentOnClose_20 = value;
	}

	inline static int32_t get_offset_of_conformanceLevel_21() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___conformanceLevel_21)); }
	inline int32_t get_conformanceLevel_21() const { return ___conformanceLevel_21; }
	inline int32_t* get_address_of_conformanceLevel_21() { return &___conformanceLevel_21; }
	inline void set_conformanceLevel_21(int32_t value)
	{
		___conformanceLevel_21 = value;
	}

	inline static int32_t get_offset_of_dtdWritten_22() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___dtdWritten_22)); }
	inline bool get_dtdWritten_22() const { return ___dtdWritten_22; }
	inline bool* get_address_of_dtdWritten_22() { return &___dtdWritten_22; }
	inline void set_dtdWritten_22(bool value)
	{
		___dtdWritten_22 = value;
	}

	inline static int32_t get_offset_of_xmlDeclFollows_23() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___xmlDeclFollows_23)); }
	inline bool get_xmlDeclFollows_23() const { return ___xmlDeclFollows_23; }
	inline bool* get_address_of_xmlDeclFollows_23() { return &___xmlDeclFollows_23; }
	inline void set_xmlDeclFollows_23(bool value)
	{
		___xmlDeclFollows_23 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_24() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___xmlCharType_24)); }
	inline XmlCharType_t2277243275  get_xmlCharType_24() const { return ___xmlCharType_24; }
	inline XmlCharType_t2277243275 * get_address_of_xmlCharType_24() { return &___xmlCharType_24; }
	inline void set_xmlCharType_24(XmlCharType_t2277243275  value)
	{
		___xmlCharType_24 = value;
	}

	inline static int32_t get_offset_of_hasher_25() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422, ___hasher_25)); }
	inline SecureStringHasher_t95812985 * get_hasher_25() const { return ___hasher_25; }
	inline SecureStringHasher_t95812985 ** get_address_of_hasher_25() { return &___hasher_25; }
	inline void set_hasher_25(SecureStringHasher_t95812985 * value)
	{
		___hasher_25 = value;
		Il2CppCodeGenWriteBarrier((&___hasher_25), value);
	}
};

struct XmlWellFormedWriter_t2234459422_StaticFields
{
public:
	// System.String[] System.Xml.XmlWellFormedWriter::stateName
	StringU5BU5D_t1281789340* ___stateName_26;
	// System.String[] System.Xml.XmlWellFormedWriter::tokenName
	StringU5BU5D_t1281789340* ___tokenName_27;
	// System.Xml.WriteState[] System.Xml.XmlWellFormedWriter::state2WriteState
	WriteStateU5BU5D_t2967056742* ___state2WriteState_28;
	// System.Xml.XmlWellFormedWriter/State[] System.Xml.XmlWellFormedWriter::StateTableDocument
	StateU5BU5D_t2644572590* ___StateTableDocument_29;
	// System.Xml.XmlWellFormedWriter/State[] System.Xml.XmlWellFormedWriter::StateTableAuto
	StateU5BU5D_t2644572590* ___StateTableAuto_30;

public:
	inline static int32_t get_offset_of_stateName_26() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422_StaticFields, ___stateName_26)); }
	inline StringU5BU5D_t1281789340* get_stateName_26() const { return ___stateName_26; }
	inline StringU5BU5D_t1281789340** get_address_of_stateName_26() { return &___stateName_26; }
	inline void set_stateName_26(StringU5BU5D_t1281789340* value)
	{
		___stateName_26 = value;
		Il2CppCodeGenWriteBarrier((&___stateName_26), value);
	}

	inline static int32_t get_offset_of_tokenName_27() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422_StaticFields, ___tokenName_27)); }
	inline StringU5BU5D_t1281789340* get_tokenName_27() const { return ___tokenName_27; }
	inline StringU5BU5D_t1281789340** get_address_of_tokenName_27() { return &___tokenName_27; }
	inline void set_tokenName_27(StringU5BU5D_t1281789340* value)
	{
		___tokenName_27 = value;
		Il2CppCodeGenWriteBarrier((&___tokenName_27), value);
	}

	inline static int32_t get_offset_of_state2WriteState_28() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422_StaticFields, ___state2WriteState_28)); }
	inline WriteStateU5BU5D_t2967056742* get_state2WriteState_28() const { return ___state2WriteState_28; }
	inline WriteStateU5BU5D_t2967056742** get_address_of_state2WriteState_28() { return &___state2WriteState_28; }
	inline void set_state2WriteState_28(WriteStateU5BU5D_t2967056742* value)
	{
		___state2WriteState_28 = value;
		Il2CppCodeGenWriteBarrier((&___state2WriteState_28), value);
	}

	inline static int32_t get_offset_of_StateTableDocument_29() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422_StaticFields, ___StateTableDocument_29)); }
	inline StateU5BU5D_t2644572590* get_StateTableDocument_29() const { return ___StateTableDocument_29; }
	inline StateU5BU5D_t2644572590** get_address_of_StateTableDocument_29() { return &___StateTableDocument_29; }
	inline void set_StateTableDocument_29(StateU5BU5D_t2644572590* value)
	{
		___StateTableDocument_29 = value;
		Il2CppCodeGenWriteBarrier((&___StateTableDocument_29), value);
	}

	inline static int32_t get_offset_of_StateTableAuto_30() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t2234459422_StaticFields, ___StateTableAuto_30)); }
	inline StateU5BU5D_t2644572590* get_StateTableAuto_30() const { return ___StateTableAuto_30; }
	inline StateU5BU5D_t2644572590** get_address_of_StateTableAuto_30() { return &___StateTableAuto_30; }
	inline void set_StateTableAuto_30(StateU5BU5D_t2644572590* value)
	{
		___StateTableAuto_30 = value;
		Il2CppCodeGenWriteBarrier((&___StateTableAuto_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWELLFORMEDWRITER_T2234459422_H
#ifndef NODEDATA_T1817330133_H
#define NODEDATA_T1817330133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/NodeData
struct  NodeData_t1817330133  : public RuntimeObject
{
public:
	// System.Xml.XmlNodeType System.Xml.XmlTextReaderImpl/NodeData::type
	int32_t ___type_1;
	// System.String System.Xml.XmlTextReaderImpl/NodeData::localName
	String_t* ___localName_2;
	// System.String System.Xml.XmlTextReaderImpl/NodeData::prefix
	String_t* ___prefix_3;
	// System.String System.Xml.XmlTextReaderImpl/NodeData::ns
	String_t* ___ns_4;
	// System.String System.Xml.XmlTextReaderImpl/NodeData::nameWPrefix
	String_t* ___nameWPrefix_5;
	// System.String System.Xml.XmlTextReaderImpl/NodeData::value
	String_t* ___value_6;
	// System.Char[] System.Xml.XmlTextReaderImpl/NodeData::chars
	CharU5BU5D_t3528271667* ___chars_7;
	// System.Int32 System.Xml.XmlTextReaderImpl/NodeData::valueStartPos
	int32_t ___valueStartPos_8;
	// System.Int32 System.Xml.XmlTextReaderImpl/NodeData::valueLength
	int32_t ___valueLength_9;
	// System.Xml.LineInfo System.Xml.XmlTextReaderImpl/NodeData::lineInfo
	LineInfo_t3266778363  ___lineInfo_10;
	// System.Xml.LineInfo System.Xml.XmlTextReaderImpl/NodeData::lineInfo2
	LineInfo_t3266778363  ___lineInfo2_11;
	// System.Char System.Xml.XmlTextReaderImpl/NodeData::quoteChar
	Il2CppChar ___quoteChar_12;
	// System.Int32 System.Xml.XmlTextReaderImpl/NodeData::depth
	int32_t ___depth_13;
	// System.Boolean System.Xml.XmlTextReaderImpl/NodeData::isEmptyOrDefault
	bool ___isEmptyOrDefault_14;
	// System.Int32 System.Xml.XmlTextReaderImpl/NodeData::entityId
	int32_t ___entityId_15;
	// System.Boolean System.Xml.XmlTextReaderImpl/NodeData::xmlContextPushed
	bool ___xmlContextPushed_16;
	// System.Xml.XmlTextReaderImpl/NodeData System.Xml.XmlTextReaderImpl/NodeData::nextAttrValueChunk
	NodeData_t1817330133 * ___nextAttrValueChunk_17;
	// System.Object System.Xml.XmlTextReaderImpl/NodeData::schemaType
	RuntimeObject * ___schemaType_18;
	// System.Object System.Xml.XmlTextReaderImpl/NodeData::typedValue
	RuntimeObject * ___typedValue_19;

public:
	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_localName_2() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___localName_2)); }
	inline String_t* get_localName_2() const { return ___localName_2; }
	inline String_t** get_address_of_localName_2() { return &___localName_2; }
	inline void set_localName_2(String_t* value)
	{
		___localName_2 = value;
		Il2CppCodeGenWriteBarrier((&___localName_2), value);
	}

	inline static int32_t get_offset_of_prefix_3() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___prefix_3)); }
	inline String_t* get_prefix_3() const { return ___prefix_3; }
	inline String_t** get_address_of_prefix_3() { return &___prefix_3; }
	inline void set_prefix_3(String_t* value)
	{
		___prefix_3 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_3), value);
	}

	inline static int32_t get_offset_of_ns_4() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___ns_4)); }
	inline String_t* get_ns_4() const { return ___ns_4; }
	inline String_t** get_address_of_ns_4() { return &___ns_4; }
	inline void set_ns_4(String_t* value)
	{
		___ns_4 = value;
		Il2CppCodeGenWriteBarrier((&___ns_4), value);
	}

	inline static int32_t get_offset_of_nameWPrefix_5() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___nameWPrefix_5)); }
	inline String_t* get_nameWPrefix_5() const { return ___nameWPrefix_5; }
	inline String_t** get_address_of_nameWPrefix_5() { return &___nameWPrefix_5; }
	inline void set_nameWPrefix_5(String_t* value)
	{
		___nameWPrefix_5 = value;
		Il2CppCodeGenWriteBarrier((&___nameWPrefix_5), value);
	}

	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___value_6)); }
	inline String_t* get_value_6() const { return ___value_6; }
	inline String_t** get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(String_t* value)
	{
		___value_6 = value;
		Il2CppCodeGenWriteBarrier((&___value_6), value);
	}

	inline static int32_t get_offset_of_chars_7() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___chars_7)); }
	inline CharU5BU5D_t3528271667* get_chars_7() const { return ___chars_7; }
	inline CharU5BU5D_t3528271667** get_address_of_chars_7() { return &___chars_7; }
	inline void set_chars_7(CharU5BU5D_t3528271667* value)
	{
		___chars_7 = value;
		Il2CppCodeGenWriteBarrier((&___chars_7), value);
	}

	inline static int32_t get_offset_of_valueStartPos_8() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___valueStartPos_8)); }
	inline int32_t get_valueStartPos_8() const { return ___valueStartPos_8; }
	inline int32_t* get_address_of_valueStartPos_8() { return &___valueStartPos_8; }
	inline void set_valueStartPos_8(int32_t value)
	{
		___valueStartPos_8 = value;
	}

	inline static int32_t get_offset_of_valueLength_9() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___valueLength_9)); }
	inline int32_t get_valueLength_9() const { return ___valueLength_9; }
	inline int32_t* get_address_of_valueLength_9() { return &___valueLength_9; }
	inline void set_valueLength_9(int32_t value)
	{
		___valueLength_9 = value;
	}

	inline static int32_t get_offset_of_lineInfo_10() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___lineInfo_10)); }
	inline LineInfo_t3266778363  get_lineInfo_10() const { return ___lineInfo_10; }
	inline LineInfo_t3266778363 * get_address_of_lineInfo_10() { return &___lineInfo_10; }
	inline void set_lineInfo_10(LineInfo_t3266778363  value)
	{
		___lineInfo_10 = value;
	}

	inline static int32_t get_offset_of_lineInfo2_11() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___lineInfo2_11)); }
	inline LineInfo_t3266778363  get_lineInfo2_11() const { return ___lineInfo2_11; }
	inline LineInfo_t3266778363 * get_address_of_lineInfo2_11() { return &___lineInfo2_11; }
	inline void set_lineInfo2_11(LineInfo_t3266778363  value)
	{
		___lineInfo2_11 = value;
	}

	inline static int32_t get_offset_of_quoteChar_12() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___quoteChar_12)); }
	inline Il2CppChar get_quoteChar_12() const { return ___quoteChar_12; }
	inline Il2CppChar* get_address_of_quoteChar_12() { return &___quoteChar_12; }
	inline void set_quoteChar_12(Il2CppChar value)
	{
		___quoteChar_12 = value;
	}

	inline static int32_t get_offset_of_depth_13() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___depth_13)); }
	inline int32_t get_depth_13() const { return ___depth_13; }
	inline int32_t* get_address_of_depth_13() { return &___depth_13; }
	inline void set_depth_13(int32_t value)
	{
		___depth_13 = value;
	}

	inline static int32_t get_offset_of_isEmptyOrDefault_14() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___isEmptyOrDefault_14)); }
	inline bool get_isEmptyOrDefault_14() const { return ___isEmptyOrDefault_14; }
	inline bool* get_address_of_isEmptyOrDefault_14() { return &___isEmptyOrDefault_14; }
	inline void set_isEmptyOrDefault_14(bool value)
	{
		___isEmptyOrDefault_14 = value;
	}

	inline static int32_t get_offset_of_entityId_15() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___entityId_15)); }
	inline int32_t get_entityId_15() const { return ___entityId_15; }
	inline int32_t* get_address_of_entityId_15() { return &___entityId_15; }
	inline void set_entityId_15(int32_t value)
	{
		___entityId_15 = value;
	}

	inline static int32_t get_offset_of_xmlContextPushed_16() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___xmlContextPushed_16)); }
	inline bool get_xmlContextPushed_16() const { return ___xmlContextPushed_16; }
	inline bool* get_address_of_xmlContextPushed_16() { return &___xmlContextPushed_16; }
	inline void set_xmlContextPushed_16(bool value)
	{
		___xmlContextPushed_16 = value;
	}

	inline static int32_t get_offset_of_nextAttrValueChunk_17() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___nextAttrValueChunk_17)); }
	inline NodeData_t1817330133 * get_nextAttrValueChunk_17() const { return ___nextAttrValueChunk_17; }
	inline NodeData_t1817330133 ** get_address_of_nextAttrValueChunk_17() { return &___nextAttrValueChunk_17; }
	inline void set_nextAttrValueChunk_17(NodeData_t1817330133 * value)
	{
		___nextAttrValueChunk_17 = value;
		Il2CppCodeGenWriteBarrier((&___nextAttrValueChunk_17), value);
	}

	inline static int32_t get_offset_of_schemaType_18() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___schemaType_18)); }
	inline RuntimeObject * get_schemaType_18() const { return ___schemaType_18; }
	inline RuntimeObject ** get_address_of_schemaType_18() { return &___schemaType_18; }
	inline void set_schemaType_18(RuntimeObject * value)
	{
		___schemaType_18 = value;
		Il2CppCodeGenWriteBarrier((&___schemaType_18), value);
	}

	inline static int32_t get_offset_of_typedValue_19() { return static_cast<int32_t>(offsetof(NodeData_t1817330133, ___typedValue_19)); }
	inline RuntimeObject * get_typedValue_19() const { return ___typedValue_19; }
	inline RuntimeObject ** get_address_of_typedValue_19() { return &___typedValue_19; }
	inline void set_typedValue_19(RuntimeObject * value)
	{
		___typedValue_19 = value;
		Il2CppCodeGenWriteBarrier((&___typedValue_19), value);
	}
};

struct NodeData_t1817330133_StaticFields
{
public:
	// System.Xml.XmlTextReaderImpl/NodeData modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlTextReaderImpl/NodeData::s_None
	NodeData_t1817330133 * ___s_None_0;

public:
	inline static int32_t get_offset_of_s_None_0() { return static_cast<int32_t>(offsetof(NodeData_t1817330133_StaticFields, ___s_None_0)); }
	inline NodeData_t1817330133 * get_s_None_0() const { return ___s_None_0; }
	inline NodeData_t1817330133 ** get_address_of_s_None_0() { return &___s_None_0; }
	inline void set_s_None_0(NodeData_t1817330133 * value)
	{
		___s_None_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_None_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODEDATA_T1817330133_H
#ifndef ITEM_T4182252821_H
#define ITEM_T4182252821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/AttributeValueCache/Item
struct  Item_t4182252821  : public RuntimeObject
{
public:
	// System.Xml.XmlWellFormedWriter/AttributeValueCache/ItemType System.Xml.XmlWellFormedWriter/AttributeValueCache/Item::type
	int32_t ___type_0;
	// System.Object System.Xml.XmlWellFormedWriter/AttributeValueCache/Item::data
	RuntimeObject * ___data_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(Item_t4182252821, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(Item_t4182252821, ___data_1)); }
	inline RuntimeObject * get_data_1() const { return ___data_1; }
	inline RuntimeObject ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(RuntimeObject * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEM_T4182252821_H
#ifndef XMLUTF8RAWTEXTWRITER_T2752398654_H
#define XMLUTF8RAWTEXTWRITER_T2752398654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUtf8RawTextWriter
struct  XmlUtf8RawTextWriter_t2752398654  : public XmlRawWriter_t722320575
{
public:
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::useAsync
	bool ___useAsync_3;
	// System.Byte[] System.Xml.XmlUtf8RawTextWriter::bufBytes
	ByteU5BU5D_t4116647657* ___bufBytes_4;
	// System.IO.Stream System.Xml.XmlUtf8RawTextWriter::stream
	Stream_t1273022909 * ___stream_5;
	// System.Text.Encoding System.Xml.XmlUtf8RawTextWriter::encoding
	Encoding_t1523322056 * ___encoding_6;
	// System.Xml.XmlCharType System.Xml.XmlUtf8RawTextWriter::xmlCharType
	XmlCharType_t2277243275  ___xmlCharType_7;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::bufPos
	int32_t ___bufPos_8;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::textPos
	int32_t ___textPos_9;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::contentPos
	int32_t ___contentPos_10;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::cdataPos
	int32_t ___cdataPos_11;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::attrEndPos
	int32_t ___attrEndPos_12;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::bufLen
	int32_t ___bufLen_13;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::writeToNull
	bool ___writeToNull_14;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::hadDoubleBracket
	bool ___hadDoubleBracket_15;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::inAttributeValue
	bool ___inAttributeValue_16;
	// System.Xml.NewLineHandling System.Xml.XmlUtf8RawTextWriter::newLineHandling
	int32_t ___newLineHandling_17;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::closeOutput
	bool ___closeOutput_18;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::omitXmlDeclaration
	bool ___omitXmlDeclaration_19;
	// System.String System.Xml.XmlUtf8RawTextWriter::newLineChars
	String_t* ___newLineChars_20;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::checkCharacters
	bool ___checkCharacters_21;
	// System.Xml.XmlStandalone System.Xml.XmlUtf8RawTextWriter::standalone
	int32_t ___standalone_22;
	// System.Xml.XmlOutputMethod System.Xml.XmlUtf8RawTextWriter::outputMethod
	int32_t ___outputMethod_23;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::autoXmlDeclaration
	bool ___autoXmlDeclaration_24;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::mergeCDataSections
	bool ___mergeCDataSections_25;

public:
	inline static int32_t get_offset_of_useAsync_3() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___useAsync_3)); }
	inline bool get_useAsync_3() const { return ___useAsync_3; }
	inline bool* get_address_of_useAsync_3() { return &___useAsync_3; }
	inline void set_useAsync_3(bool value)
	{
		___useAsync_3 = value;
	}

	inline static int32_t get_offset_of_bufBytes_4() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___bufBytes_4)); }
	inline ByteU5BU5D_t4116647657* get_bufBytes_4() const { return ___bufBytes_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_bufBytes_4() { return &___bufBytes_4; }
	inline void set_bufBytes_4(ByteU5BU5D_t4116647657* value)
	{
		___bufBytes_4 = value;
		Il2CppCodeGenWriteBarrier((&___bufBytes_4), value);
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___stream_5)); }
	inline Stream_t1273022909 * get_stream_5() const { return ___stream_5; }
	inline Stream_t1273022909 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_t1273022909 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___encoding_6)); }
	inline Encoding_t1523322056 * get_encoding_6() const { return ___encoding_6; }
	inline Encoding_t1523322056 ** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(Encoding_t1523322056 * value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_6), value);
	}

	inline static int32_t get_offset_of_xmlCharType_7() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___xmlCharType_7)); }
	inline XmlCharType_t2277243275  get_xmlCharType_7() const { return ___xmlCharType_7; }
	inline XmlCharType_t2277243275 * get_address_of_xmlCharType_7() { return &___xmlCharType_7; }
	inline void set_xmlCharType_7(XmlCharType_t2277243275  value)
	{
		___xmlCharType_7 = value;
	}

	inline static int32_t get_offset_of_bufPos_8() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___bufPos_8)); }
	inline int32_t get_bufPos_8() const { return ___bufPos_8; }
	inline int32_t* get_address_of_bufPos_8() { return &___bufPos_8; }
	inline void set_bufPos_8(int32_t value)
	{
		___bufPos_8 = value;
	}

	inline static int32_t get_offset_of_textPos_9() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___textPos_9)); }
	inline int32_t get_textPos_9() const { return ___textPos_9; }
	inline int32_t* get_address_of_textPos_9() { return &___textPos_9; }
	inline void set_textPos_9(int32_t value)
	{
		___textPos_9 = value;
	}

	inline static int32_t get_offset_of_contentPos_10() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___contentPos_10)); }
	inline int32_t get_contentPos_10() const { return ___contentPos_10; }
	inline int32_t* get_address_of_contentPos_10() { return &___contentPos_10; }
	inline void set_contentPos_10(int32_t value)
	{
		___contentPos_10 = value;
	}

	inline static int32_t get_offset_of_cdataPos_11() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___cdataPos_11)); }
	inline int32_t get_cdataPos_11() const { return ___cdataPos_11; }
	inline int32_t* get_address_of_cdataPos_11() { return &___cdataPos_11; }
	inline void set_cdataPos_11(int32_t value)
	{
		___cdataPos_11 = value;
	}

	inline static int32_t get_offset_of_attrEndPos_12() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___attrEndPos_12)); }
	inline int32_t get_attrEndPos_12() const { return ___attrEndPos_12; }
	inline int32_t* get_address_of_attrEndPos_12() { return &___attrEndPos_12; }
	inline void set_attrEndPos_12(int32_t value)
	{
		___attrEndPos_12 = value;
	}

	inline static int32_t get_offset_of_bufLen_13() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___bufLen_13)); }
	inline int32_t get_bufLen_13() const { return ___bufLen_13; }
	inline int32_t* get_address_of_bufLen_13() { return &___bufLen_13; }
	inline void set_bufLen_13(int32_t value)
	{
		___bufLen_13 = value;
	}

	inline static int32_t get_offset_of_writeToNull_14() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___writeToNull_14)); }
	inline bool get_writeToNull_14() const { return ___writeToNull_14; }
	inline bool* get_address_of_writeToNull_14() { return &___writeToNull_14; }
	inline void set_writeToNull_14(bool value)
	{
		___writeToNull_14 = value;
	}

	inline static int32_t get_offset_of_hadDoubleBracket_15() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___hadDoubleBracket_15)); }
	inline bool get_hadDoubleBracket_15() const { return ___hadDoubleBracket_15; }
	inline bool* get_address_of_hadDoubleBracket_15() { return &___hadDoubleBracket_15; }
	inline void set_hadDoubleBracket_15(bool value)
	{
		___hadDoubleBracket_15 = value;
	}

	inline static int32_t get_offset_of_inAttributeValue_16() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___inAttributeValue_16)); }
	inline bool get_inAttributeValue_16() const { return ___inAttributeValue_16; }
	inline bool* get_address_of_inAttributeValue_16() { return &___inAttributeValue_16; }
	inline void set_inAttributeValue_16(bool value)
	{
		___inAttributeValue_16 = value;
	}

	inline static int32_t get_offset_of_newLineHandling_17() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___newLineHandling_17)); }
	inline int32_t get_newLineHandling_17() const { return ___newLineHandling_17; }
	inline int32_t* get_address_of_newLineHandling_17() { return &___newLineHandling_17; }
	inline void set_newLineHandling_17(int32_t value)
	{
		___newLineHandling_17 = value;
	}

	inline static int32_t get_offset_of_closeOutput_18() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___closeOutput_18)); }
	inline bool get_closeOutput_18() const { return ___closeOutput_18; }
	inline bool* get_address_of_closeOutput_18() { return &___closeOutput_18; }
	inline void set_closeOutput_18(bool value)
	{
		___closeOutput_18 = value;
	}

	inline static int32_t get_offset_of_omitXmlDeclaration_19() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___omitXmlDeclaration_19)); }
	inline bool get_omitXmlDeclaration_19() const { return ___omitXmlDeclaration_19; }
	inline bool* get_address_of_omitXmlDeclaration_19() { return &___omitXmlDeclaration_19; }
	inline void set_omitXmlDeclaration_19(bool value)
	{
		___omitXmlDeclaration_19 = value;
	}

	inline static int32_t get_offset_of_newLineChars_20() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___newLineChars_20)); }
	inline String_t* get_newLineChars_20() const { return ___newLineChars_20; }
	inline String_t** get_address_of_newLineChars_20() { return &___newLineChars_20; }
	inline void set_newLineChars_20(String_t* value)
	{
		___newLineChars_20 = value;
		Il2CppCodeGenWriteBarrier((&___newLineChars_20), value);
	}

	inline static int32_t get_offset_of_checkCharacters_21() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___checkCharacters_21)); }
	inline bool get_checkCharacters_21() const { return ___checkCharacters_21; }
	inline bool* get_address_of_checkCharacters_21() { return &___checkCharacters_21; }
	inline void set_checkCharacters_21(bool value)
	{
		___checkCharacters_21 = value;
	}

	inline static int32_t get_offset_of_standalone_22() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___standalone_22)); }
	inline int32_t get_standalone_22() const { return ___standalone_22; }
	inline int32_t* get_address_of_standalone_22() { return &___standalone_22; }
	inline void set_standalone_22(int32_t value)
	{
		___standalone_22 = value;
	}

	inline static int32_t get_offset_of_outputMethod_23() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___outputMethod_23)); }
	inline int32_t get_outputMethod_23() const { return ___outputMethod_23; }
	inline int32_t* get_address_of_outputMethod_23() { return &___outputMethod_23; }
	inline void set_outputMethod_23(int32_t value)
	{
		___outputMethod_23 = value;
	}

	inline static int32_t get_offset_of_autoXmlDeclaration_24() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___autoXmlDeclaration_24)); }
	inline bool get_autoXmlDeclaration_24() const { return ___autoXmlDeclaration_24; }
	inline bool* get_address_of_autoXmlDeclaration_24() { return &___autoXmlDeclaration_24; }
	inline void set_autoXmlDeclaration_24(bool value)
	{
		___autoXmlDeclaration_24 = value;
	}

	inline static int32_t get_offset_of_mergeCDataSections_25() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_t2752398654, ___mergeCDataSections_25)); }
	inline bool get_mergeCDataSections_25() const { return ___mergeCDataSections_25; }
	inline bool* get_address_of_mergeCDataSections_25() { return &___mergeCDataSections_25; }
	inline void set_mergeCDataSections_25(bool value)
	{
		___mergeCDataSections_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLUTF8RAWTEXTWRITER_T2752398654_H
#ifndef XMLCONTEXT_T1618903103_H
#define XMLCONTEXT_T1618903103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/XmlContext
struct  XmlContext_t1618903103  : public RuntimeObject
{
public:
	// System.Xml.XmlSpace System.Xml.XmlTextReaderImpl/XmlContext::xmlSpace
	int32_t ___xmlSpace_0;
	// System.String System.Xml.XmlTextReaderImpl/XmlContext::xmlLang
	String_t* ___xmlLang_1;
	// System.String System.Xml.XmlTextReaderImpl/XmlContext::defaultNamespace
	String_t* ___defaultNamespace_2;
	// System.Xml.XmlTextReaderImpl/XmlContext System.Xml.XmlTextReaderImpl/XmlContext::previousContext
	XmlContext_t1618903103 * ___previousContext_3;

public:
	inline static int32_t get_offset_of_xmlSpace_0() { return static_cast<int32_t>(offsetof(XmlContext_t1618903103, ___xmlSpace_0)); }
	inline int32_t get_xmlSpace_0() const { return ___xmlSpace_0; }
	inline int32_t* get_address_of_xmlSpace_0() { return &___xmlSpace_0; }
	inline void set_xmlSpace_0(int32_t value)
	{
		___xmlSpace_0 = value;
	}

	inline static int32_t get_offset_of_xmlLang_1() { return static_cast<int32_t>(offsetof(XmlContext_t1618903103, ___xmlLang_1)); }
	inline String_t* get_xmlLang_1() const { return ___xmlLang_1; }
	inline String_t** get_address_of_xmlLang_1() { return &___xmlLang_1; }
	inline void set_xmlLang_1(String_t* value)
	{
		___xmlLang_1 = value;
		Il2CppCodeGenWriteBarrier((&___xmlLang_1), value);
	}

	inline static int32_t get_offset_of_defaultNamespace_2() { return static_cast<int32_t>(offsetof(XmlContext_t1618903103, ___defaultNamespace_2)); }
	inline String_t* get_defaultNamespace_2() const { return ___defaultNamespace_2; }
	inline String_t** get_address_of_defaultNamespace_2() { return &___defaultNamespace_2; }
	inline void set_defaultNamespace_2(String_t* value)
	{
		___defaultNamespace_2 = value;
		Il2CppCodeGenWriteBarrier((&___defaultNamespace_2), value);
	}

	inline static int32_t get_offset_of_previousContext_3() { return static_cast<int32_t>(offsetof(XmlContext_t1618903103, ___previousContext_3)); }
	inline XmlContext_t1618903103 * get_previousContext_3() const { return ___previousContext_3; }
	inline XmlContext_t1618903103 ** get_address_of_previousContext_3() { return &___previousContext_3; }
	inline void set_previousContext_3(XmlContext_t1618903103 * value)
	{
		___previousContext_3 = value;
		Il2CppCodeGenWriteBarrier((&___previousContext_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCONTEXT_T1618903103_H
#ifndef XMLCOMMENT_T2476947920_H
#define XMLCOMMENT_T2476947920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlComment
struct  XmlComment_t2476947920  : public XmlCharacterData_t1167807131
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCOMMENT_T2476947920_H
#ifndef XMLTEXTWRITER_T2114213153_H
#define XMLTEXTWRITER_T2114213153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter
struct  XmlTextWriter_t2114213153  : public XmlWriter_t127905479
{
public:
	// System.IO.TextWriter System.Xml.XmlTextWriter::textWriter
	TextWriter_t3478189236 * ___textWriter_1;
	// System.Xml.XmlTextEncoder System.Xml.XmlTextWriter::xmlEncoder
	XmlTextEncoder_t1632274355 * ___xmlEncoder_2;
	// System.Text.Encoding System.Xml.XmlTextWriter::encoding
	Encoding_t1523322056 * ___encoding_3;
	// System.Xml.Formatting System.Xml.XmlTextWriter::formatting
	int32_t ___formatting_4;
	// System.Boolean System.Xml.XmlTextWriter::indented
	bool ___indented_5;
	// System.Int32 System.Xml.XmlTextWriter::indentation
	int32_t ___indentation_6;
	// System.Char System.Xml.XmlTextWriter::indentChar
	Il2CppChar ___indentChar_7;
	// System.Xml.XmlTextWriter/TagInfo[] System.Xml.XmlTextWriter::stack
	TagInfoU5BU5D_t2840723532* ___stack_8;
	// System.Int32 System.Xml.XmlTextWriter::top
	int32_t ___top_9;
	// System.Xml.XmlTextWriter/State[] System.Xml.XmlTextWriter::stateTable
	StateU5BU5D_t428546178* ___stateTable_10;
	// System.Xml.XmlTextWriter/State System.Xml.XmlTextWriter::currentState
	int32_t ___currentState_11;
	// System.Xml.XmlTextWriter/Token System.Xml.XmlTextWriter::lastToken
	int32_t ___lastToken_12;
	// System.Xml.XmlTextWriterBase64Encoder System.Xml.XmlTextWriter::base64Encoder
	XmlTextWriterBase64Encoder_t4259465041 * ___base64Encoder_13;
	// System.Char System.Xml.XmlTextWriter::quoteChar
	Il2CppChar ___quoteChar_14;
	// System.Char System.Xml.XmlTextWriter::curQuoteChar
	Il2CppChar ___curQuoteChar_15;
	// System.Boolean System.Xml.XmlTextWriter::namespaces
	bool ___namespaces_16;
	// System.Xml.XmlTextWriter/SpecialAttr System.Xml.XmlTextWriter::specialAttr
	int32_t ___specialAttr_17;
	// System.String System.Xml.XmlTextWriter::prefixForXmlNs
	String_t* ___prefixForXmlNs_18;
	// System.Boolean System.Xml.XmlTextWriter::flush
	bool ___flush_19;
	// System.Xml.XmlTextWriter/Namespace[] System.Xml.XmlTextWriter::nsStack
	NamespaceU5BU5D_t4259279085* ___nsStack_20;
	// System.Int32 System.Xml.XmlTextWriter::nsTop
	int32_t ___nsTop_21;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlTextWriter::nsHashtable
	Dictionary_2_t2736202052 * ___nsHashtable_22;
	// System.Boolean System.Xml.XmlTextWriter::useNsHashtable
	bool ___useNsHashtable_23;
	// System.Xml.XmlCharType System.Xml.XmlTextWriter::xmlCharType
	XmlCharType_t2277243275  ___xmlCharType_24;

public:
	inline static int32_t get_offset_of_textWriter_1() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___textWriter_1)); }
	inline TextWriter_t3478189236 * get_textWriter_1() const { return ___textWriter_1; }
	inline TextWriter_t3478189236 ** get_address_of_textWriter_1() { return &___textWriter_1; }
	inline void set_textWriter_1(TextWriter_t3478189236 * value)
	{
		___textWriter_1 = value;
		Il2CppCodeGenWriteBarrier((&___textWriter_1), value);
	}

	inline static int32_t get_offset_of_xmlEncoder_2() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___xmlEncoder_2)); }
	inline XmlTextEncoder_t1632274355 * get_xmlEncoder_2() const { return ___xmlEncoder_2; }
	inline XmlTextEncoder_t1632274355 ** get_address_of_xmlEncoder_2() { return &___xmlEncoder_2; }
	inline void set_xmlEncoder_2(XmlTextEncoder_t1632274355 * value)
	{
		___xmlEncoder_2 = value;
		Il2CppCodeGenWriteBarrier((&___xmlEncoder_2), value);
	}

	inline static int32_t get_offset_of_encoding_3() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___encoding_3)); }
	inline Encoding_t1523322056 * get_encoding_3() const { return ___encoding_3; }
	inline Encoding_t1523322056 ** get_address_of_encoding_3() { return &___encoding_3; }
	inline void set_encoding_3(Encoding_t1523322056 * value)
	{
		___encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_3), value);
	}

	inline static int32_t get_offset_of_formatting_4() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___formatting_4)); }
	inline int32_t get_formatting_4() const { return ___formatting_4; }
	inline int32_t* get_address_of_formatting_4() { return &___formatting_4; }
	inline void set_formatting_4(int32_t value)
	{
		___formatting_4 = value;
	}

	inline static int32_t get_offset_of_indented_5() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___indented_5)); }
	inline bool get_indented_5() const { return ___indented_5; }
	inline bool* get_address_of_indented_5() { return &___indented_5; }
	inline void set_indented_5(bool value)
	{
		___indented_5 = value;
	}

	inline static int32_t get_offset_of_indentation_6() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___indentation_6)); }
	inline int32_t get_indentation_6() const { return ___indentation_6; }
	inline int32_t* get_address_of_indentation_6() { return &___indentation_6; }
	inline void set_indentation_6(int32_t value)
	{
		___indentation_6 = value;
	}

	inline static int32_t get_offset_of_indentChar_7() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___indentChar_7)); }
	inline Il2CppChar get_indentChar_7() const { return ___indentChar_7; }
	inline Il2CppChar* get_address_of_indentChar_7() { return &___indentChar_7; }
	inline void set_indentChar_7(Il2CppChar value)
	{
		___indentChar_7 = value;
	}

	inline static int32_t get_offset_of_stack_8() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___stack_8)); }
	inline TagInfoU5BU5D_t2840723532* get_stack_8() const { return ___stack_8; }
	inline TagInfoU5BU5D_t2840723532** get_address_of_stack_8() { return &___stack_8; }
	inline void set_stack_8(TagInfoU5BU5D_t2840723532* value)
	{
		___stack_8 = value;
		Il2CppCodeGenWriteBarrier((&___stack_8), value);
	}

	inline static int32_t get_offset_of_top_9() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___top_9)); }
	inline int32_t get_top_9() const { return ___top_9; }
	inline int32_t* get_address_of_top_9() { return &___top_9; }
	inline void set_top_9(int32_t value)
	{
		___top_9 = value;
	}

	inline static int32_t get_offset_of_stateTable_10() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___stateTable_10)); }
	inline StateU5BU5D_t428546178* get_stateTable_10() const { return ___stateTable_10; }
	inline StateU5BU5D_t428546178** get_address_of_stateTable_10() { return &___stateTable_10; }
	inline void set_stateTable_10(StateU5BU5D_t428546178* value)
	{
		___stateTable_10 = value;
		Il2CppCodeGenWriteBarrier((&___stateTable_10), value);
	}

	inline static int32_t get_offset_of_currentState_11() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___currentState_11)); }
	inline int32_t get_currentState_11() const { return ___currentState_11; }
	inline int32_t* get_address_of_currentState_11() { return &___currentState_11; }
	inline void set_currentState_11(int32_t value)
	{
		___currentState_11 = value;
	}

	inline static int32_t get_offset_of_lastToken_12() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___lastToken_12)); }
	inline int32_t get_lastToken_12() const { return ___lastToken_12; }
	inline int32_t* get_address_of_lastToken_12() { return &___lastToken_12; }
	inline void set_lastToken_12(int32_t value)
	{
		___lastToken_12 = value;
	}

	inline static int32_t get_offset_of_base64Encoder_13() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___base64Encoder_13)); }
	inline XmlTextWriterBase64Encoder_t4259465041 * get_base64Encoder_13() const { return ___base64Encoder_13; }
	inline XmlTextWriterBase64Encoder_t4259465041 ** get_address_of_base64Encoder_13() { return &___base64Encoder_13; }
	inline void set_base64Encoder_13(XmlTextWriterBase64Encoder_t4259465041 * value)
	{
		___base64Encoder_13 = value;
		Il2CppCodeGenWriteBarrier((&___base64Encoder_13), value);
	}

	inline static int32_t get_offset_of_quoteChar_14() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___quoteChar_14)); }
	inline Il2CppChar get_quoteChar_14() const { return ___quoteChar_14; }
	inline Il2CppChar* get_address_of_quoteChar_14() { return &___quoteChar_14; }
	inline void set_quoteChar_14(Il2CppChar value)
	{
		___quoteChar_14 = value;
	}

	inline static int32_t get_offset_of_curQuoteChar_15() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___curQuoteChar_15)); }
	inline Il2CppChar get_curQuoteChar_15() const { return ___curQuoteChar_15; }
	inline Il2CppChar* get_address_of_curQuoteChar_15() { return &___curQuoteChar_15; }
	inline void set_curQuoteChar_15(Il2CppChar value)
	{
		___curQuoteChar_15 = value;
	}

	inline static int32_t get_offset_of_namespaces_16() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___namespaces_16)); }
	inline bool get_namespaces_16() const { return ___namespaces_16; }
	inline bool* get_address_of_namespaces_16() { return &___namespaces_16; }
	inline void set_namespaces_16(bool value)
	{
		___namespaces_16 = value;
	}

	inline static int32_t get_offset_of_specialAttr_17() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___specialAttr_17)); }
	inline int32_t get_specialAttr_17() const { return ___specialAttr_17; }
	inline int32_t* get_address_of_specialAttr_17() { return &___specialAttr_17; }
	inline void set_specialAttr_17(int32_t value)
	{
		___specialAttr_17 = value;
	}

	inline static int32_t get_offset_of_prefixForXmlNs_18() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___prefixForXmlNs_18)); }
	inline String_t* get_prefixForXmlNs_18() const { return ___prefixForXmlNs_18; }
	inline String_t** get_address_of_prefixForXmlNs_18() { return &___prefixForXmlNs_18; }
	inline void set_prefixForXmlNs_18(String_t* value)
	{
		___prefixForXmlNs_18 = value;
		Il2CppCodeGenWriteBarrier((&___prefixForXmlNs_18), value);
	}

	inline static int32_t get_offset_of_flush_19() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___flush_19)); }
	inline bool get_flush_19() const { return ___flush_19; }
	inline bool* get_address_of_flush_19() { return &___flush_19; }
	inline void set_flush_19(bool value)
	{
		___flush_19 = value;
	}

	inline static int32_t get_offset_of_nsStack_20() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___nsStack_20)); }
	inline NamespaceU5BU5D_t4259279085* get_nsStack_20() const { return ___nsStack_20; }
	inline NamespaceU5BU5D_t4259279085** get_address_of_nsStack_20() { return &___nsStack_20; }
	inline void set_nsStack_20(NamespaceU5BU5D_t4259279085* value)
	{
		___nsStack_20 = value;
		Il2CppCodeGenWriteBarrier((&___nsStack_20), value);
	}

	inline static int32_t get_offset_of_nsTop_21() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___nsTop_21)); }
	inline int32_t get_nsTop_21() const { return ___nsTop_21; }
	inline int32_t* get_address_of_nsTop_21() { return &___nsTop_21; }
	inline void set_nsTop_21(int32_t value)
	{
		___nsTop_21 = value;
	}

	inline static int32_t get_offset_of_nsHashtable_22() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___nsHashtable_22)); }
	inline Dictionary_2_t2736202052 * get_nsHashtable_22() const { return ___nsHashtable_22; }
	inline Dictionary_2_t2736202052 ** get_address_of_nsHashtable_22() { return &___nsHashtable_22; }
	inline void set_nsHashtable_22(Dictionary_2_t2736202052 * value)
	{
		___nsHashtable_22 = value;
		Il2CppCodeGenWriteBarrier((&___nsHashtable_22), value);
	}

	inline static int32_t get_offset_of_useNsHashtable_23() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___useNsHashtable_23)); }
	inline bool get_useNsHashtable_23() const { return ___useNsHashtable_23; }
	inline bool* get_address_of_useNsHashtable_23() { return &___useNsHashtable_23; }
	inline void set_useNsHashtable_23(bool value)
	{
		___useNsHashtable_23 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_24() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___xmlCharType_24)); }
	inline XmlCharType_t2277243275  get_xmlCharType_24() const { return ___xmlCharType_24; }
	inline XmlCharType_t2277243275 * get_address_of_xmlCharType_24() { return &___xmlCharType_24; }
	inline void set_xmlCharType_24(XmlCharType_t2277243275  value)
	{
		___xmlCharType_24 = value;
	}
};

struct XmlTextWriter_t2114213153_StaticFields
{
public:
	// System.String[] System.Xml.XmlTextWriter::stateName
	StringU5BU5D_t1281789340* ___stateName_25;
	// System.String[] System.Xml.XmlTextWriter::tokenName
	StringU5BU5D_t1281789340* ___tokenName_26;
	// System.Xml.XmlTextWriter/State[] System.Xml.XmlTextWriter::stateTableDefault
	StateU5BU5D_t428546178* ___stateTableDefault_27;
	// System.Xml.XmlTextWriter/State[] System.Xml.XmlTextWriter::stateTableDocument
	StateU5BU5D_t428546178* ___stateTableDocument_28;

public:
	inline static int32_t get_offset_of_stateName_25() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153_StaticFields, ___stateName_25)); }
	inline StringU5BU5D_t1281789340* get_stateName_25() const { return ___stateName_25; }
	inline StringU5BU5D_t1281789340** get_address_of_stateName_25() { return &___stateName_25; }
	inline void set_stateName_25(StringU5BU5D_t1281789340* value)
	{
		___stateName_25 = value;
		Il2CppCodeGenWriteBarrier((&___stateName_25), value);
	}

	inline static int32_t get_offset_of_tokenName_26() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153_StaticFields, ___tokenName_26)); }
	inline StringU5BU5D_t1281789340* get_tokenName_26() const { return ___tokenName_26; }
	inline StringU5BU5D_t1281789340** get_address_of_tokenName_26() { return &___tokenName_26; }
	inline void set_tokenName_26(StringU5BU5D_t1281789340* value)
	{
		___tokenName_26 = value;
		Il2CppCodeGenWriteBarrier((&___tokenName_26), value);
	}

	inline static int32_t get_offset_of_stateTableDefault_27() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153_StaticFields, ___stateTableDefault_27)); }
	inline StateU5BU5D_t428546178* get_stateTableDefault_27() const { return ___stateTableDefault_27; }
	inline StateU5BU5D_t428546178** get_address_of_stateTableDefault_27() { return &___stateTableDefault_27; }
	inline void set_stateTableDefault_27(StateU5BU5D_t428546178* value)
	{
		___stateTableDefault_27 = value;
		Il2CppCodeGenWriteBarrier((&___stateTableDefault_27), value);
	}

	inline static int32_t get_offset_of_stateTableDocument_28() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153_StaticFields, ___stateTableDocument_28)); }
	inline StateU5BU5D_t428546178* get_stateTableDocument_28() const { return ___stateTableDocument_28; }
	inline StateU5BU5D_t428546178** get_address_of_stateTableDocument_28() { return &___stateTableDocument_28; }
	inline void set_stateTableDocument_28(StateU5BU5D_t428546178* value)
	{
		___stateTableDocument_28 = value;
		Il2CppCodeGenWriteBarrier((&___stateTableDocument_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTWRITER_T2114213153_H
#ifndef TAGINFO_T3526638417_H
#define TAGINFO_T3526638417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/TagInfo
struct  TagInfo_t3526638417 
{
public:
	// System.String System.Xml.XmlTextWriter/TagInfo::name
	String_t* ___name_0;
	// System.String System.Xml.XmlTextWriter/TagInfo::prefix
	String_t* ___prefix_1;
	// System.String System.Xml.XmlTextWriter/TagInfo::defaultNs
	String_t* ___defaultNs_2;
	// System.Xml.XmlTextWriter/NamespaceState System.Xml.XmlTextWriter/TagInfo::defaultNsState
	int32_t ___defaultNsState_3;
	// System.Xml.XmlSpace System.Xml.XmlTextWriter/TagInfo::xmlSpace
	int32_t ___xmlSpace_4;
	// System.String System.Xml.XmlTextWriter/TagInfo::xmlLang
	String_t* ___xmlLang_5;
	// System.Int32 System.Xml.XmlTextWriter/TagInfo::prevNsTop
	int32_t ___prevNsTop_6;
	// System.Int32 System.Xml.XmlTextWriter/TagInfo::prefixCount
	int32_t ___prefixCount_7;
	// System.Boolean System.Xml.XmlTextWriter/TagInfo::mixed
	bool ___mixed_8;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(TagInfo_t3526638417, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_prefix_1() { return static_cast<int32_t>(offsetof(TagInfo_t3526638417, ___prefix_1)); }
	inline String_t* get_prefix_1() const { return ___prefix_1; }
	inline String_t** get_address_of_prefix_1() { return &___prefix_1; }
	inline void set_prefix_1(String_t* value)
	{
		___prefix_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_1), value);
	}

	inline static int32_t get_offset_of_defaultNs_2() { return static_cast<int32_t>(offsetof(TagInfo_t3526638417, ___defaultNs_2)); }
	inline String_t* get_defaultNs_2() const { return ___defaultNs_2; }
	inline String_t** get_address_of_defaultNs_2() { return &___defaultNs_2; }
	inline void set_defaultNs_2(String_t* value)
	{
		___defaultNs_2 = value;
		Il2CppCodeGenWriteBarrier((&___defaultNs_2), value);
	}

	inline static int32_t get_offset_of_defaultNsState_3() { return static_cast<int32_t>(offsetof(TagInfo_t3526638417, ___defaultNsState_3)); }
	inline int32_t get_defaultNsState_3() const { return ___defaultNsState_3; }
	inline int32_t* get_address_of_defaultNsState_3() { return &___defaultNsState_3; }
	inline void set_defaultNsState_3(int32_t value)
	{
		___defaultNsState_3 = value;
	}

	inline static int32_t get_offset_of_xmlSpace_4() { return static_cast<int32_t>(offsetof(TagInfo_t3526638417, ___xmlSpace_4)); }
	inline int32_t get_xmlSpace_4() const { return ___xmlSpace_4; }
	inline int32_t* get_address_of_xmlSpace_4() { return &___xmlSpace_4; }
	inline void set_xmlSpace_4(int32_t value)
	{
		___xmlSpace_4 = value;
	}

	inline static int32_t get_offset_of_xmlLang_5() { return static_cast<int32_t>(offsetof(TagInfo_t3526638417, ___xmlLang_5)); }
	inline String_t* get_xmlLang_5() const { return ___xmlLang_5; }
	inline String_t** get_address_of_xmlLang_5() { return &___xmlLang_5; }
	inline void set_xmlLang_5(String_t* value)
	{
		___xmlLang_5 = value;
		Il2CppCodeGenWriteBarrier((&___xmlLang_5), value);
	}

	inline static int32_t get_offset_of_prevNsTop_6() { return static_cast<int32_t>(offsetof(TagInfo_t3526638417, ___prevNsTop_6)); }
	inline int32_t get_prevNsTop_6() const { return ___prevNsTop_6; }
	inline int32_t* get_address_of_prevNsTop_6() { return &___prevNsTop_6; }
	inline void set_prevNsTop_6(int32_t value)
	{
		___prevNsTop_6 = value;
	}

	inline static int32_t get_offset_of_prefixCount_7() { return static_cast<int32_t>(offsetof(TagInfo_t3526638417, ___prefixCount_7)); }
	inline int32_t get_prefixCount_7() const { return ___prefixCount_7; }
	inline int32_t* get_address_of_prefixCount_7() { return &___prefixCount_7; }
	inline void set_prefixCount_7(int32_t value)
	{
		___prefixCount_7 = value;
	}

	inline static int32_t get_offset_of_mixed_8() { return static_cast<int32_t>(offsetof(TagInfo_t3526638417, ___mixed_8)); }
	inline bool get_mixed_8() const { return ___mixed_8; }
	inline bool* get_address_of_mixed_8() { return &___mixed_8; }
	inline void set_mixed_8(bool value)
	{
		___mixed_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlTextWriter/TagInfo
struct TagInfo_t3526638417_marshaled_pinvoke
{
	char* ___name_0;
	char* ___prefix_1;
	char* ___defaultNs_2;
	int32_t ___defaultNsState_3;
	int32_t ___xmlSpace_4;
	char* ___xmlLang_5;
	int32_t ___prevNsTop_6;
	int32_t ___prefixCount_7;
	int32_t ___mixed_8;
};
// Native definition for COM marshalling of System.Xml.XmlTextWriter/TagInfo
struct TagInfo_t3526638417_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___prefix_1;
	Il2CppChar* ___defaultNs_2;
	int32_t ___defaultNsState_3;
	int32_t ___xmlSpace_4;
	Il2CppChar* ___xmlLang_5;
	int32_t ___prevNsTop_6;
	int32_t ___prefixCount_7;
	int32_t ___mixed_8;
};
#endif // TAGINFO_T3526638417_H
#ifndef HASHCODEOFSTRINGDELEGATE_T1614268366_H
#define HASHCODEOFSTRINGDELEGATE_T1614268366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.SecureStringHasher/HashCodeOfStringDelegate
struct  HashCodeOfStringDelegate_t1614268366  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHCODEOFSTRINGDELEGATE_T1614268366_H
#ifndef XMLENCODEDRAWTEXTWRITERINDENT_T2293832529_H
#define XMLENCODEDRAWTEXTWRITERINDENT_T2293832529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEncodedRawTextWriterIndent
struct  XmlEncodedRawTextWriterIndent_t2293832529  : public XmlEncodedRawTextWriter_t977350554
{
public:
	// System.Int32 System.Xml.XmlEncodedRawTextWriterIndent::indentLevel
	int32_t ___indentLevel_35;
	// System.Boolean System.Xml.XmlEncodedRawTextWriterIndent::newLineOnAttributes
	bool ___newLineOnAttributes_36;
	// System.String System.Xml.XmlEncodedRawTextWriterIndent::indentChars
	String_t* ___indentChars_37;
	// System.Boolean System.Xml.XmlEncodedRawTextWriterIndent::mixedContent
	bool ___mixedContent_38;
	// System.Xml.BitStack System.Xml.XmlEncodedRawTextWriterIndent::mixedContentStack
	BitStack_t371189938 * ___mixedContentStack_39;
	// System.Xml.ConformanceLevel System.Xml.XmlEncodedRawTextWriterIndent::conformanceLevel
	int32_t ___conformanceLevel_40;

public:
	inline static int32_t get_offset_of_indentLevel_35() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_t2293832529, ___indentLevel_35)); }
	inline int32_t get_indentLevel_35() const { return ___indentLevel_35; }
	inline int32_t* get_address_of_indentLevel_35() { return &___indentLevel_35; }
	inline void set_indentLevel_35(int32_t value)
	{
		___indentLevel_35 = value;
	}

	inline static int32_t get_offset_of_newLineOnAttributes_36() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_t2293832529, ___newLineOnAttributes_36)); }
	inline bool get_newLineOnAttributes_36() const { return ___newLineOnAttributes_36; }
	inline bool* get_address_of_newLineOnAttributes_36() { return &___newLineOnAttributes_36; }
	inline void set_newLineOnAttributes_36(bool value)
	{
		___newLineOnAttributes_36 = value;
	}

	inline static int32_t get_offset_of_indentChars_37() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_t2293832529, ___indentChars_37)); }
	inline String_t* get_indentChars_37() const { return ___indentChars_37; }
	inline String_t** get_address_of_indentChars_37() { return &___indentChars_37; }
	inline void set_indentChars_37(String_t* value)
	{
		___indentChars_37 = value;
		Il2CppCodeGenWriteBarrier((&___indentChars_37), value);
	}

	inline static int32_t get_offset_of_mixedContent_38() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_t2293832529, ___mixedContent_38)); }
	inline bool get_mixedContent_38() const { return ___mixedContent_38; }
	inline bool* get_address_of_mixedContent_38() { return &___mixedContent_38; }
	inline void set_mixedContent_38(bool value)
	{
		___mixedContent_38 = value;
	}

	inline static int32_t get_offset_of_mixedContentStack_39() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_t2293832529, ___mixedContentStack_39)); }
	inline BitStack_t371189938 * get_mixedContentStack_39() const { return ___mixedContentStack_39; }
	inline BitStack_t371189938 ** get_address_of_mixedContentStack_39() { return &___mixedContentStack_39; }
	inline void set_mixedContentStack_39(BitStack_t371189938 * value)
	{
		___mixedContentStack_39 = value;
		Il2CppCodeGenWriteBarrier((&___mixedContentStack_39), value);
	}

	inline static int32_t get_offset_of_conformanceLevel_40() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_t2293832529, ___conformanceLevel_40)); }
	inline int32_t get_conformanceLevel_40() const { return ___conformanceLevel_40; }
	inline int32_t* get_address_of_conformanceLevel_40() { return &___conformanceLevel_40; }
	inline void set_conformanceLevel_40(int32_t value)
	{
		___conformanceLevel_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENCODEDRAWTEXTWRITERINDENT_T2293832529_H
#ifndef TEXTENCODEDRAWTEXTWRITER_T290234645_H
#define TEXTENCODEDRAWTEXTWRITER_T290234645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.TextEncodedRawTextWriter
struct  TextEncodedRawTextWriter_t290234645  : public XmlEncodedRawTextWriter_t977350554
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTENCODEDRAWTEXTWRITER_T290234645_H
#ifndef ONDEFAULTATTRIBUTEUSEDELEGATE_T2911570364_H
#define ONDEFAULTATTRIBUTEUSEDELEGATE_T2911570364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/OnDefaultAttributeUseDelegate
struct  OnDefaultAttributeUseDelegate_t2911570364  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDEFAULTATTRIBUTEUSEDELEGATE_T2911570364_H
#ifndef TEXTUTF8RAWTEXTWRITER_T2166652245_H
#define TEXTUTF8RAWTEXTWRITER_T2166652245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.TextUtf8RawTextWriter
struct  TextUtf8RawTextWriter_t2166652245  : public XmlUtf8RawTextWriter_t2752398654
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUTF8RAWTEXTWRITER_T2166652245_H
#ifndef ONREMOVEWRITER_T4019152709_H
#define ONREMOVEWRITER_T4019152709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.OnRemoveWriter
struct  OnRemoveWriter_t4019152709  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONREMOVEWRITER_T4019152709_H
#ifndef XMLUTF8RAWTEXTWRITERINDENT_T1896581129_H
#define XMLUTF8RAWTEXTWRITERINDENT_T1896581129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUtf8RawTextWriterIndent
struct  XmlUtf8RawTextWriterIndent_t1896581129  : public XmlUtf8RawTextWriter_t2752398654
{
public:
	// System.Int32 System.Xml.XmlUtf8RawTextWriterIndent::indentLevel
	int32_t ___indentLevel_26;
	// System.Boolean System.Xml.XmlUtf8RawTextWriterIndent::newLineOnAttributes
	bool ___newLineOnAttributes_27;
	// System.String System.Xml.XmlUtf8RawTextWriterIndent::indentChars
	String_t* ___indentChars_28;
	// System.Boolean System.Xml.XmlUtf8RawTextWriterIndent::mixedContent
	bool ___mixedContent_29;
	// System.Xml.BitStack System.Xml.XmlUtf8RawTextWriterIndent::mixedContentStack
	BitStack_t371189938 * ___mixedContentStack_30;
	// System.Xml.ConformanceLevel System.Xml.XmlUtf8RawTextWriterIndent::conformanceLevel
	int32_t ___conformanceLevel_31;

public:
	inline static int32_t get_offset_of_indentLevel_26() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t1896581129, ___indentLevel_26)); }
	inline int32_t get_indentLevel_26() const { return ___indentLevel_26; }
	inline int32_t* get_address_of_indentLevel_26() { return &___indentLevel_26; }
	inline void set_indentLevel_26(int32_t value)
	{
		___indentLevel_26 = value;
	}

	inline static int32_t get_offset_of_newLineOnAttributes_27() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t1896581129, ___newLineOnAttributes_27)); }
	inline bool get_newLineOnAttributes_27() const { return ___newLineOnAttributes_27; }
	inline bool* get_address_of_newLineOnAttributes_27() { return &___newLineOnAttributes_27; }
	inline void set_newLineOnAttributes_27(bool value)
	{
		___newLineOnAttributes_27 = value;
	}

	inline static int32_t get_offset_of_indentChars_28() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t1896581129, ___indentChars_28)); }
	inline String_t* get_indentChars_28() const { return ___indentChars_28; }
	inline String_t** get_address_of_indentChars_28() { return &___indentChars_28; }
	inline void set_indentChars_28(String_t* value)
	{
		___indentChars_28 = value;
		Il2CppCodeGenWriteBarrier((&___indentChars_28), value);
	}

	inline static int32_t get_offset_of_mixedContent_29() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t1896581129, ___mixedContent_29)); }
	inline bool get_mixedContent_29() const { return ___mixedContent_29; }
	inline bool* get_address_of_mixedContent_29() { return &___mixedContent_29; }
	inline void set_mixedContent_29(bool value)
	{
		___mixedContent_29 = value;
	}

	inline static int32_t get_offset_of_mixedContentStack_30() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t1896581129, ___mixedContentStack_30)); }
	inline BitStack_t371189938 * get_mixedContentStack_30() const { return ___mixedContentStack_30; }
	inline BitStack_t371189938 ** get_address_of_mixedContentStack_30() { return &___mixedContentStack_30; }
	inline void set_mixedContentStack_30(BitStack_t371189938 * value)
	{
		___mixedContentStack_30 = value;
		Il2CppCodeGenWriteBarrier((&___mixedContentStack_30), value);
	}

	inline static int32_t get_offset_of_conformanceLevel_31() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t1896581129, ___conformanceLevel_31)); }
	inline int32_t get_conformanceLevel_31() const { return ___conformanceLevel_31; }
	inline int32_t* get_address_of_conformanceLevel_31() { return &___conformanceLevel_31; }
	inline void set_conformanceLevel_31(int32_t value)
	{
		___conformanceLevel_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLUTF8RAWTEXTWRITERINDENT_T1896581129_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (IncrementalReadDecoder_t3011954239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (IncrementalReadDummyDecoder_t2572367147), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (OnRemoveWriter_t4019152709), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (NamespaceHandling_t4087553436)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1905[3] = 
{
	NamespaceHandling_t4087553436::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (NewLineHandling_t850339274)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1906[4] = 
{
	NewLineHandling_t850339274::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (QueryOutputWriter_t1945554537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[12] = 
{
	QueryOutputWriter_t1945554537::get_offset_of_wrapped_3(),
	QueryOutputWriter_t1945554537::get_offset_of_inCDataSection_4(),
	QueryOutputWriter_t1945554537::get_offset_of_lookupCDataElems_5(),
	QueryOutputWriter_t1945554537::get_offset_of_bitsCData_6(),
	QueryOutputWriter_t1945554537::get_offset_of_qnameCData_7(),
	QueryOutputWriter_t1945554537::get_offset_of_outputDocType_8(),
	QueryOutputWriter_t1945554537::get_offset_of_checkWellFormedDoc_9(),
	QueryOutputWriter_t1945554537::get_offset_of_hasDocElem_10(),
	QueryOutputWriter_t1945554537::get_offset_of_inAttr_11(),
	QueryOutputWriter_t1945554537::get_offset_of_systemId_12(),
	QueryOutputWriter_t1945554537::get_offset_of_publicId_13(),
	QueryOutputWriter_t1945554537::get_offset_of_depth_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (ReadContentAsBinaryHelper_t3017207972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[4] = 
{
	ReadContentAsBinaryHelper_t3017207972::get_offset_of_reader_0(),
	ReadContentAsBinaryHelper_t3017207972::get_offset_of_state_1(),
	ReadContentAsBinaryHelper_t3017207972::get_offset_of_valueOffset_2(),
	ReadContentAsBinaryHelper_t3017207972::get_offset_of_isEnd_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (State_t1355373645)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1909[4] = 
{
	State_t1355373645::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (ElementProperties_t469398153)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1910[10] = 
{
	ElementProperties_t469398153::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (AttributeProperties_t3715092149)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1911[5] = 
{
	AttributeProperties_t3715092149::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (TernaryTreeReadOnly_t172569514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[1] = 
{
	TernaryTreeReadOnly_t172569514::get_offset_of_nodeBuffer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (ReadState_t944984020)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1913[6] = 
{
	ReadState_t944984020::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (SecureStringHasher_t95812985), -1, sizeof(SecureStringHasher_t95812985_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1914[2] = 
{
	SecureStringHasher_t95812985_StaticFields::get_offset_of_hashCodeDelegate_0(),
	SecureStringHasher_t95812985::get_offset_of_hashCodeRandomizer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (HashCodeOfStringDelegate_t1614268366), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (TextEncodedRawTextWriter_t290234645), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (TextUtf8RawTextWriter_t2166652245), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (ValidationType_t4049928607)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1918[6] = 
{
	ValidationType_t4049928607::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (WhitespaceHandling_t784045650)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1919[4] = 
{
	WhitespaceHandling_t784045650::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (XmlAsyncCheckWriter_t4057126699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1920[2] = 
{
	XmlAsyncCheckWriter_t4057126699::get_offset_of_coreWriter_1(),
	XmlAsyncCheckWriter_t4057126699::get_offset_of_lastTask_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (XmlAutoDetectWriter_t3131353423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[6] = 
{
	XmlAutoDetectWriter_t3131353423::get_offset_of_wrapped_3(),
	XmlAutoDetectWriter_t3131353423::get_offset_of_onRemove_4(),
	XmlAutoDetectWriter_t3131353423::get_offset_of_writerSettings_5(),
	XmlAutoDetectWriter_t3131353423::get_offset_of_eventCache_6(),
	XmlAutoDetectWriter_t3131353423::get_offset_of_textWriter_7(),
	XmlAutoDetectWriter_t3131353423::get_offset_of_strm_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (XmlEncodedRawTextWriter_t977350554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1922[32] = 
{
	XmlEncodedRawTextWriter_t977350554::get_offset_of_useAsync_3(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_bufBytes_4(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_stream_5(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_encoding_6(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_xmlCharType_7(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_bufPos_8(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_textPos_9(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_contentPos_10(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_cdataPos_11(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_attrEndPos_12(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_bufLen_13(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_writeToNull_14(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_hadDoubleBracket_15(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_inAttributeValue_16(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_bufBytesUsed_17(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_bufChars_18(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_encoder_19(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_writer_20(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_trackTextContent_21(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_inTextContent_22(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_lastMarkPos_23(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_textContentMarks_24(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_charEntityFallback_25(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_newLineHandling_26(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_closeOutput_27(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_omitXmlDeclaration_28(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_newLineChars_29(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_checkCharacters_30(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_standalone_31(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_outputMethod_32(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_autoXmlDeclaration_33(),
	XmlEncodedRawTextWriter_t977350554::get_offset_of_mergeCDataSections_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (XmlEncodedRawTextWriterIndent_t2293832529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[6] = 
{
	XmlEncodedRawTextWriterIndent_t2293832529::get_offset_of_indentLevel_35(),
	XmlEncodedRawTextWriterIndent_t2293832529::get_offset_of_newLineOnAttributes_36(),
	XmlEncodedRawTextWriterIndent_t2293832529::get_offset_of_indentChars_37(),
	XmlEncodedRawTextWriterIndent_t2293832529::get_offset_of_mixedContent_38(),
	XmlEncodedRawTextWriterIndent_t2293832529::get_offset_of_mixedContentStack_39(),
	XmlEncodedRawTextWriterIndent_t2293832529::get_offset_of_conformanceLevel_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (XmlEventCache_t1546883120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[6] = 
{
	XmlEventCache_t1546883120::get_offset_of_pages_3(),
	XmlEventCache_t1546883120::get_offset_of_pageCurr_4(),
	XmlEventCache_t1546883120::get_offset_of_pageSize_5(),
	XmlEventCache_t1546883120::get_offset_of_hasRootNode_6(),
	XmlEventCache_t1546883120::get_offset_of_singleText_7(),
	XmlEventCache_t1546883120::get_offset_of_baseUri_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (XmlEventType_t2232656300)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1925[27] = 
{
	XmlEventType_t2232656300::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (XmlEvent_t2951711862)+ sizeof (RuntimeObject), sizeof(XmlEvent_t2951711862_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1926[5] = 
{
	XmlEvent_t2951711862::get_offset_of_eventType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XmlEvent_t2951711862::get_offset_of_s1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XmlEvent_t2951711862::get_offset_of_s2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XmlEvent_t2951711862::get_offset_of_s3_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XmlEvent_t2951711862::get_offset_of_o_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (XmlParserContext_t2544895291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1927[10] = 
{
	XmlParserContext_t2544895291::get_offset_of__nt_0(),
	XmlParserContext_t2544895291::get_offset_of__nsMgr_1(),
	XmlParserContext_t2544895291::get_offset_of__docTypeName_2(),
	XmlParserContext_t2544895291::get_offset_of__pubId_3(),
	XmlParserContext_t2544895291::get_offset_of__sysId_4(),
	XmlParserContext_t2544895291::get_offset_of__internalSubset_5(),
	XmlParserContext_t2544895291::get_offset_of__xmlLang_6(),
	XmlParserContext_t2544895291::get_offset_of__xmlSpace_7(),
	XmlParserContext_t2544895291::get_offset_of__baseURI_8(),
	XmlParserContext_t2544895291::get_offset_of__encoding_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (XmlRawWriter_t722320575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[2] = 
{
	XmlRawWriter_t722320575::get_offset_of_base64Encoder_1(),
	XmlRawWriter_t722320575::get_offset_of_resolver_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (XmlReader_t3121518892), -1, sizeof(XmlReader_t3121518892_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1929[3] = 
{
	XmlReader_t3121518892_StaticFields::get_offset_of_IsTextualNodeBitmap_0(),
	XmlReader_t3121518892_StaticFields::get_offset_of_CanReadContentAsBitmap_1(),
	XmlReader_t3121518892_StaticFields::get_offset_of_HasValueBitmap_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (XmlReaderSettings_t2186285234), -1, sizeof(XmlReaderSettings_t2186285234_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1930[20] = 
{
	XmlReaderSettings_t2186285234::get_offset_of_useAsync_0(),
	XmlReaderSettings_t2186285234::get_offset_of_nameTable_1(),
	XmlReaderSettings_t2186285234::get_offset_of_xmlResolver_2(),
	XmlReaderSettings_t2186285234::get_offset_of_lineNumberOffset_3(),
	XmlReaderSettings_t2186285234::get_offset_of_linePositionOffset_4(),
	XmlReaderSettings_t2186285234::get_offset_of_conformanceLevel_5(),
	XmlReaderSettings_t2186285234::get_offset_of_checkCharacters_6(),
	XmlReaderSettings_t2186285234::get_offset_of_maxCharactersInDocument_7(),
	XmlReaderSettings_t2186285234::get_offset_of_maxCharactersFromEntities_8(),
	XmlReaderSettings_t2186285234::get_offset_of_ignoreWhitespace_9(),
	XmlReaderSettings_t2186285234::get_offset_of_ignorePIs_10(),
	XmlReaderSettings_t2186285234::get_offset_of_ignoreComments_11(),
	XmlReaderSettings_t2186285234::get_offset_of_dtdProcessing_12(),
	XmlReaderSettings_t2186285234::get_offset_of_validationType_13(),
	XmlReaderSettings_t2186285234::get_offset_of_validationFlags_14(),
	XmlReaderSettings_t2186285234::get_offset_of_schemas_15(),
	XmlReaderSettings_t2186285234::get_offset_of_closeInput_16(),
	XmlReaderSettings_t2186285234::get_offset_of_isReadOnly_17(),
	XmlReaderSettings_t2186285234::get_offset_of_U3CIsXmlResolverSetU3Ek__BackingField_18(),
	XmlReaderSettings_t2186285234_StaticFields::get_offset_of_s_enableLegacyXmlSettings_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (XmlSpace_t3324193251)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1931[4] = 
{
	XmlSpace_t3324193251::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (XmlTextEncoder_t1632274355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[6] = 
{
	XmlTextEncoder_t1632274355::get_offset_of_textWriter_0(),
	XmlTextEncoder_t1632274355::get_offset_of_inAttribute_1(),
	XmlTextEncoder_t1632274355::get_offset_of_quoteChar_2(),
	XmlTextEncoder_t1632274355::get_offset_of_attrValue_3(),
	XmlTextEncoder_t1632274355::get_offset_of_cacheAttrValue_4(),
	XmlTextEncoder_t1632274355::get_offset_of_xmlCharType_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (XmlTextReader_t4233384356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1933[1] = 
{
	XmlTextReader_t4233384356::get_offset_of_impl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (XmlTextReaderImpl_t178060594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[76] = 
{
	XmlTextReaderImpl_t178060594::get_offset_of_parseText_dummyTask_3(),
	XmlTextReaderImpl_t178060594::get_offset_of_laterInitParam_4(),
	XmlTextReaderImpl_t178060594::get_offset_of_xmlCharType_5(),
	XmlTextReaderImpl_t178060594::get_offset_of_ps_6(),
	XmlTextReaderImpl_t178060594::get_offset_of_parsingFunction_7(),
	XmlTextReaderImpl_t178060594::get_offset_of_nextParsingFunction_8(),
	XmlTextReaderImpl_t178060594::get_offset_of_nextNextParsingFunction_9(),
	XmlTextReaderImpl_t178060594::get_offset_of_nodes_10(),
	XmlTextReaderImpl_t178060594::get_offset_of_curNode_11(),
	XmlTextReaderImpl_t178060594::get_offset_of_index_12(),
	XmlTextReaderImpl_t178060594::get_offset_of_curAttrIndex_13(),
	XmlTextReaderImpl_t178060594::get_offset_of_attrCount_14(),
	XmlTextReaderImpl_t178060594::get_offset_of_attrHashtable_15(),
	XmlTextReaderImpl_t178060594::get_offset_of_attrDuplWalkCount_16(),
	XmlTextReaderImpl_t178060594::get_offset_of_attrNeedNamespaceLookup_17(),
	XmlTextReaderImpl_t178060594::get_offset_of_fullAttrCleanup_18(),
	XmlTextReaderImpl_t178060594::get_offset_of_attrDuplSortingArray_19(),
	XmlTextReaderImpl_t178060594::get_offset_of_nameTable_20(),
	XmlTextReaderImpl_t178060594::get_offset_of_nameTableFromSettings_21(),
	XmlTextReaderImpl_t178060594::get_offset_of_xmlResolver_22(),
	XmlTextReaderImpl_t178060594::get_offset_of_url_23(),
	XmlTextReaderImpl_t178060594::get_offset_of_normalize_24(),
	XmlTextReaderImpl_t178060594::get_offset_of_supportNamespaces_25(),
	XmlTextReaderImpl_t178060594::get_offset_of_whitespaceHandling_26(),
	XmlTextReaderImpl_t178060594::get_offset_of_dtdProcessing_27(),
	XmlTextReaderImpl_t178060594::get_offset_of_entityHandling_28(),
	XmlTextReaderImpl_t178060594::get_offset_of_ignorePIs_29(),
	XmlTextReaderImpl_t178060594::get_offset_of_ignoreComments_30(),
	XmlTextReaderImpl_t178060594::get_offset_of_checkCharacters_31(),
	XmlTextReaderImpl_t178060594::get_offset_of_lineNumberOffset_32(),
	XmlTextReaderImpl_t178060594::get_offset_of_linePositionOffset_33(),
	XmlTextReaderImpl_t178060594::get_offset_of_closeInput_34(),
	XmlTextReaderImpl_t178060594::get_offset_of_maxCharactersInDocument_35(),
	XmlTextReaderImpl_t178060594::get_offset_of_maxCharactersFromEntities_36(),
	XmlTextReaderImpl_t178060594::get_offset_of_v1Compat_37(),
	XmlTextReaderImpl_t178060594::get_offset_of_namespaceManager_38(),
	XmlTextReaderImpl_t178060594::get_offset_of_lastPrefix_39(),
	XmlTextReaderImpl_t178060594::get_offset_of_xmlContext_40(),
	XmlTextReaderImpl_t178060594::get_offset_of_parsingStatesStack_41(),
	XmlTextReaderImpl_t178060594::get_offset_of_parsingStatesStackTop_42(),
	XmlTextReaderImpl_t178060594::get_offset_of_reportedBaseUri_43(),
	XmlTextReaderImpl_t178060594::get_offset_of_reportedEncoding_44(),
	XmlTextReaderImpl_t178060594::get_offset_of_dtdInfo_45(),
	XmlTextReaderImpl_t178060594::get_offset_of_fragmentType_46(),
	XmlTextReaderImpl_t178060594::get_offset_of_fragmentParserContext_47(),
	XmlTextReaderImpl_t178060594::get_offset_of_fragment_48(),
	XmlTextReaderImpl_t178060594::get_offset_of_incReadDecoder_49(),
	XmlTextReaderImpl_t178060594::get_offset_of_incReadState_50(),
	XmlTextReaderImpl_t178060594::get_offset_of_incReadLineInfo_51(),
	XmlTextReaderImpl_t178060594::get_offset_of_incReadDepth_52(),
	XmlTextReaderImpl_t178060594::get_offset_of_incReadLeftStartPos_53(),
	XmlTextReaderImpl_t178060594::get_offset_of_incReadLeftEndPos_54(),
	XmlTextReaderImpl_t178060594::get_offset_of_attributeValueBaseEntityId_55(),
	XmlTextReaderImpl_t178060594::get_offset_of_emptyEntityInAttributeResolved_56(),
	XmlTextReaderImpl_t178060594::get_offset_of_validationEventHandling_57(),
	XmlTextReaderImpl_t178060594::get_offset_of_onDefaultAttributeUse_58(),
	XmlTextReaderImpl_t178060594::get_offset_of_validatingReaderCompatFlag_59(),
	XmlTextReaderImpl_t178060594::get_offset_of_addDefaultAttributesAndNormalize_60(),
	XmlTextReaderImpl_t178060594::get_offset_of_stringBuilder_61(),
	XmlTextReaderImpl_t178060594::get_offset_of_rootElementParsed_62(),
	XmlTextReaderImpl_t178060594::get_offset_of_standalone_63(),
	XmlTextReaderImpl_t178060594::get_offset_of_nextEntityId_64(),
	XmlTextReaderImpl_t178060594::get_offset_of_parsingMode_65(),
	XmlTextReaderImpl_t178060594::get_offset_of_readState_66(),
	XmlTextReaderImpl_t178060594::get_offset_of_lastEntity_67(),
	XmlTextReaderImpl_t178060594::get_offset_of_afterResetState_68(),
	XmlTextReaderImpl_t178060594::get_offset_of_documentStartBytePos_69(),
	XmlTextReaderImpl_t178060594::get_offset_of_readValueOffset_70(),
	XmlTextReaderImpl_t178060594::get_offset_of_charactersInDocument_71(),
	XmlTextReaderImpl_t178060594::get_offset_of_charactersFromEntities_72(),
	XmlTextReaderImpl_t178060594::get_offset_of_currentEntities_73(),
	XmlTextReaderImpl_t178060594::get_offset_of_disableUndeclaredEntityCheck_74(),
	XmlTextReaderImpl_t178060594::get_offset_of_outerReader_75(),
	XmlTextReaderImpl_t178060594::get_offset_of_xmlResolverIsSet_76(),
	XmlTextReaderImpl_t178060594::get_offset_of_Xml_77(),
	XmlTextReaderImpl_t178060594::get_offset_of_XmlNs_78(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (ParsingFunction_t2722146511)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1935[27] = 
{
	ParsingFunction_t2722146511::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (ParsingMode_t1812726891)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1936[4] = 
{
	ParsingMode_t1812726891::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (EntityType_t2463313529)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1937[9] = 
{
	EntityType_t2463313529::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (EntityExpandType_t2794135677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1938[4] = 
{
	EntityExpandType_t2794135677::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (IncrementalReadState_t591482297)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1939[16] = 
{
	IncrementalReadState_t591482297::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (LaterInitParam_t1449395818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1940[10] = 
{
	LaterInitParam_t1449395818::get_offset_of_useAsync_0(),
	LaterInitParam_t1449395818::get_offset_of_inputStream_1(),
	LaterInitParam_t1449395818::get_offset_of_inputBytes_2(),
	LaterInitParam_t1449395818::get_offset_of_inputByteCount_3(),
	LaterInitParam_t1449395818::get_offset_of_inputbaseUri_4(),
	LaterInitParam_t1449395818::get_offset_of_inputUriStr_5(),
	LaterInitParam_t1449395818::get_offset_of_inputUriResolver_6(),
	LaterInitParam_t1449395818::get_offset_of_inputContext_7(),
	LaterInitParam_t1449395818::get_offset_of_inputTextReader_8(),
	LaterInitParam_t1449395818::get_offset_of_initType_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (InitInputType_t933223089)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1941[5] = 
{
	InitInputType_t933223089::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (DtdParserProxy_t3101460057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1942[1] = 
{
	DtdParserProxy_t3101460057::get_offset_of_reader_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (ParsingState_t1780334922)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[21] = 
{
	ParsingState_t1780334922::get_offset_of_chars_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_charPos_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_charsUsed_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_encoding_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_appendMode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_stream_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_decoder_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_bytes_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_bytePos_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_bytesUsed_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_textReader_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_lineNo_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_lineStartPos_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_baseUriStr_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_baseUri_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_isEof_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_isStreamEof_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_entity_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_entityId_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_eolNormalized_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_t1780334922::get_offset_of_entityResolvedManually_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (XmlContext_t1618903103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1944[4] = 
{
	XmlContext_t1618903103::get_offset_of_xmlSpace_0(),
	XmlContext_t1618903103::get_offset_of_xmlLang_1(),
	XmlContext_t1618903103::get_offset_of_defaultNamespace_2(),
	XmlContext_t1618903103::get_offset_of_previousContext_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (NoNamespaceManager_t2350683444), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (NodeData_t1817330133), -1, sizeof(NodeData_t1817330133_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1946[20] = 
{
	NodeData_t1817330133_StaticFields::get_offset_of_s_None_0(),
	NodeData_t1817330133::get_offset_of_type_1(),
	NodeData_t1817330133::get_offset_of_localName_2(),
	NodeData_t1817330133::get_offset_of_prefix_3(),
	NodeData_t1817330133::get_offset_of_ns_4(),
	NodeData_t1817330133::get_offset_of_nameWPrefix_5(),
	NodeData_t1817330133::get_offset_of_value_6(),
	NodeData_t1817330133::get_offset_of_chars_7(),
	NodeData_t1817330133::get_offset_of_valueStartPos_8(),
	NodeData_t1817330133::get_offset_of_valueLength_9(),
	NodeData_t1817330133::get_offset_of_lineInfo_10(),
	NodeData_t1817330133::get_offset_of_lineInfo2_11(),
	NodeData_t1817330133::get_offset_of_quoteChar_12(),
	NodeData_t1817330133::get_offset_of_depth_13(),
	NodeData_t1817330133::get_offset_of_isEmptyOrDefault_14(),
	NodeData_t1817330133::get_offset_of_entityId_15(),
	NodeData_t1817330133::get_offset_of_xmlContextPushed_16(),
	NodeData_t1817330133::get_offset_of_nextAttrValueChunk_17(),
	NodeData_t1817330133::get_offset_of_schemaType_18(),
	NodeData_t1817330133::get_offset_of_typedValue_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (DtdDefaultAttributeInfoToNodeDataComparer_t1152788373), -1, sizeof(DtdDefaultAttributeInfoToNodeDataComparer_t1152788373_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1947[1] = 
{
	DtdDefaultAttributeInfoToNodeDataComparer_t1152788373_StaticFields::get_offset_of_s_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (OnDefaultAttributeUseDelegate_t2911570364), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (Formatting_t1232942836)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1949[3] = 
{
	Formatting_t1232942836::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (XmlTextWriter_t2114213153), -1, sizeof(XmlTextWriter_t2114213153_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1950[28] = 
{
	XmlTextWriter_t2114213153::get_offset_of_textWriter_1(),
	XmlTextWriter_t2114213153::get_offset_of_xmlEncoder_2(),
	XmlTextWriter_t2114213153::get_offset_of_encoding_3(),
	XmlTextWriter_t2114213153::get_offset_of_formatting_4(),
	XmlTextWriter_t2114213153::get_offset_of_indented_5(),
	XmlTextWriter_t2114213153::get_offset_of_indentation_6(),
	XmlTextWriter_t2114213153::get_offset_of_indentChar_7(),
	XmlTextWriter_t2114213153::get_offset_of_stack_8(),
	XmlTextWriter_t2114213153::get_offset_of_top_9(),
	XmlTextWriter_t2114213153::get_offset_of_stateTable_10(),
	XmlTextWriter_t2114213153::get_offset_of_currentState_11(),
	XmlTextWriter_t2114213153::get_offset_of_lastToken_12(),
	XmlTextWriter_t2114213153::get_offset_of_base64Encoder_13(),
	XmlTextWriter_t2114213153::get_offset_of_quoteChar_14(),
	XmlTextWriter_t2114213153::get_offset_of_curQuoteChar_15(),
	XmlTextWriter_t2114213153::get_offset_of_namespaces_16(),
	XmlTextWriter_t2114213153::get_offset_of_specialAttr_17(),
	XmlTextWriter_t2114213153::get_offset_of_prefixForXmlNs_18(),
	XmlTextWriter_t2114213153::get_offset_of_flush_19(),
	XmlTextWriter_t2114213153::get_offset_of_nsStack_20(),
	XmlTextWriter_t2114213153::get_offset_of_nsTop_21(),
	XmlTextWriter_t2114213153::get_offset_of_nsHashtable_22(),
	XmlTextWriter_t2114213153::get_offset_of_useNsHashtable_23(),
	XmlTextWriter_t2114213153::get_offset_of_xmlCharType_24(),
	XmlTextWriter_t2114213153_StaticFields::get_offset_of_stateName_25(),
	XmlTextWriter_t2114213153_StaticFields::get_offset_of_tokenName_26(),
	XmlTextWriter_t2114213153_StaticFields::get_offset_of_stateTableDefault_27(),
	XmlTextWriter_t2114213153_StaticFields::get_offset_of_stateTableDocument_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (NamespaceState_t853084585)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1951[5] = 
{
	NamespaceState_t853084585::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (TagInfo_t3526638417)+ sizeof (RuntimeObject), sizeof(TagInfo_t3526638417_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1952[9] = 
{
	TagInfo_t3526638417::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t3526638417::get_offset_of_prefix_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t3526638417::get_offset_of_defaultNs_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t3526638417::get_offset_of_defaultNsState_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t3526638417::get_offset_of_xmlSpace_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t3526638417::get_offset_of_xmlLang_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t3526638417::get_offset_of_prevNsTop_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t3526638417::get_offset_of_prefixCount_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t3526638417::get_offset_of_mixed_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (Namespace_t2218256516)+ sizeof (RuntimeObject), sizeof(Namespace_t2218256516_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1953[4] = 
{
	Namespace_t2218256516::get_offset_of_prefix_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Namespace_t2218256516::get_offset_of_ns_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Namespace_t2218256516::get_offset_of_declared_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Namespace_t2218256516::get_offset_of_prevNsIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (SpecialAttr_t319442854)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1954[5] = 
{
	SpecialAttr_t319442854::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (State_t1792539347)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1955[11] = 
{
	State_t1792539347::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (Token_t2430492165)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1956[15] = 
{
	Token_t2430492165::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (XmlUtf8RawTextWriter_t2752398654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[23] = 
{
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_useAsync_3(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_bufBytes_4(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_stream_5(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_encoding_6(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_xmlCharType_7(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_bufPos_8(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_textPos_9(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_contentPos_10(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_cdataPos_11(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_attrEndPos_12(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_bufLen_13(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_writeToNull_14(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_hadDoubleBracket_15(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_inAttributeValue_16(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_newLineHandling_17(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_closeOutput_18(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_omitXmlDeclaration_19(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_newLineChars_20(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_checkCharacters_21(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_standalone_22(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_outputMethod_23(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_autoXmlDeclaration_24(),
	XmlUtf8RawTextWriter_t2752398654::get_offset_of_mergeCDataSections_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (XmlUtf8RawTextWriterIndent_t1896581129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1958[6] = 
{
	XmlUtf8RawTextWriterIndent_t1896581129::get_offset_of_indentLevel_26(),
	XmlUtf8RawTextWriterIndent_t1896581129::get_offset_of_newLineOnAttributes_27(),
	XmlUtf8RawTextWriterIndent_t1896581129::get_offset_of_indentChars_28(),
	XmlUtf8RawTextWriterIndent_t1896581129::get_offset_of_mixedContent_29(),
	XmlUtf8RawTextWriterIndent_t1896581129::get_offset_of_mixedContentStack_30(),
	XmlUtf8RawTextWriterIndent_t1896581129::get_offset_of_conformanceLevel_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (XmlValidatingReader_t1719295192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[1] = 
{
	XmlValidatingReader_t1719295192::get_offset_of_impl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (XmlValidatingReaderImpl_t1817762384), -1, sizeof(XmlValidatingReaderImpl_t1817762384_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1960[13] = 
{
	XmlValidatingReaderImpl_t1817762384::get_offset_of_coreReader_3(),
	XmlValidatingReaderImpl_t1817762384::get_offset_of_coreReaderImpl_4(),
	XmlValidatingReaderImpl_t1817762384::get_offset_of_coreReaderNSResolver_5(),
	XmlValidatingReaderImpl_t1817762384::get_offset_of_validationType_6(),
	XmlValidatingReaderImpl_t1817762384::get_offset_of_validator_7(),
	XmlValidatingReaderImpl_t1817762384::get_offset_of_schemaCollection_8(),
	XmlValidatingReaderImpl_t1817762384::get_offset_of_processIdentityConstraints_9(),
	XmlValidatingReaderImpl_t1817762384::get_offset_of_parsingFunction_10(),
	XmlValidatingReaderImpl_t1817762384::get_offset_of_eventHandling_11(),
	XmlValidatingReaderImpl_t1817762384::get_offset_of_parserContext_12(),
	XmlValidatingReaderImpl_t1817762384::get_offset_of_readBinaryHelper_13(),
	XmlValidatingReaderImpl_t1817762384::get_offset_of_outerReader_14(),
	XmlValidatingReaderImpl_t1817762384_StaticFields::get_offset_of_s_tempResolver_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (ParsingFunction_t767331079)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1961[9] = 
{
	ParsingFunction_t767331079::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (ValidationEventHandling_t3812646699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1962[2] = 
{
	ValidationEventHandling_t3812646699::get_offset_of_reader_0(),
	ValidationEventHandling_t3812646699::get_offset_of_eventHandler_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (XmlWellFormedWriter_t2234459422), -1, sizeof(XmlWellFormedWriter_t2234459422_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1963[30] = 
{
	XmlWellFormedWriter_t2234459422::get_offset_of_writer_1(),
	XmlWellFormedWriter_t2234459422::get_offset_of_rawWriter_2(),
	XmlWellFormedWriter_t2234459422::get_offset_of_predefinedNamespaces_3(),
	XmlWellFormedWriter_t2234459422::get_offset_of_nsStack_4(),
	XmlWellFormedWriter_t2234459422::get_offset_of_nsTop_5(),
	XmlWellFormedWriter_t2234459422::get_offset_of_nsHashtable_6(),
	XmlWellFormedWriter_t2234459422::get_offset_of_useNsHashtable_7(),
	XmlWellFormedWriter_t2234459422::get_offset_of_elemScopeStack_8(),
	XmlWellFormedWriter_t2234459422::get_offset_of_elemTop_9(),
	XmlWellFormedWriter_t2234459422::get_offset_of_attrStack_10(),
	XmlWellFormedWriter_t2234459422::get_offset_of_attrCount_11(),
	XmlWellFormedWriter_t2234459422::get_offset_of_attrHashTable_12(),
	XmlWellFormedWriter_t2234459422::get_offset_of_specAttr_13(),
	XmlWellFormedWriter_t2234459422::get_offset_of_attrValueCache_14(),
	XmlWellFormedWriter_t2234459422::get_offset_of_curDeclPrefix_15(),
	XmlWellFormedWriter_t2234459422::get_offset_of_stateTable_16(),
	XmlWellFormedWriter_t2234459422::get_offset_of_currentState_17(),
	XmlWellFormedWriter_t2234459422::get_offset_of_checkCharacters_18(),
	XmlWellFormedWriter_t2234459422::get_offset_of_omitDuplNamespaces_19(),
	XmlWellFormedWriter_t2234459422::get_offset_of_writeEndDocumentOnClose_20(),
	XmlWellFormedWriter_t2234459422::get_offset_of_conformanceLevel_21(),
	XmlWellFormedWriter_t2234459422::get_offset_of_dtdWritten_22(),
	XmlWellFormedWriter_t2234459422::get_offset_of_xmlDeclFollows_23(),
	XmlWellFormedWriter_t2234459422::get_offset_of_xmlCharType_24(),
	XmlWellFormedWriter_t2234459422::get_offset_of_hasher_25(),
	XmlWellFormedWriter_t2234459422_StaticFields::get_offset_of_stateName_26(),
	XmlWellFormedWriter_t2234459422_StaticFields::get_offset_of_tokenName_27(),
	XmlWellFormedWriter_t2234459422_StaticFields::get_offset_of_state2WriteState_28(),
	XmlWellFormedWriter_t2234459422_StaticFields::get_offset_of_StateTableDocument_29(),
	XmlWellFormedWriter_t2234459422_StaticFields::get_offset_of_StateTableAuto_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (State_t3712518167)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1964[34] = 
{
	State_t3712518167::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (Token_t678414789)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1965[16] = 
{
	Token_t678414789::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (ElementScope_t3058411185)+ sizeof (RuntimeObject), sizeof(ElementScope_t3058411185_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1966[6] = 
{
	ElementScope_t3058411185::get_offset_of_prevNSTop_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementScope_t3058411185::get_offset_of_prefix_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementScope_t3058411185::get_offset_of_localName_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementScope_t3058411185::get_offset_of_namespaceUri_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementScope_t3058411185::get_offset_of_xmlSpace_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementScope_t3058411185::get_offset_of_xmlLang_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (Namespace_t2679594434)+ sizeof (RuntimeObject), sizeof(Namespace_t2679594434_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1967[4] = 
{
	Namespace_t2679594434::get_offset_of_prefix_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Namespace_t2679594434::get_offset_of_namespaceUri_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Namespace_t2679594434::get_offset_of_kind_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Namespace_t2679594434::get_offset_of_prevNsIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (AttributeValueCache_t2432505004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1968[5] = 
{
	AttributeValueCache_t2432505004::get_offset_of_stringValue_0(),
	AttributeValueCache_t2432505004::get_offset_of_singleStringValue_1(),
	AttributeValueCache_t2432505004::get_offset_of_items_2(),
	AttributeValueCache_t2432505004::get_offset_of_firstItem_3(),
	AttributeValueCache_t2432505004::get_offset_of_lastItem_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (ItemType_t208921270)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1969[10] = 
{
	ItemType_t208921270::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (Item_t4182252821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1970[2] = 
{
	Item_t4182252821::get_offset_of_type_0(),
	Item_t4182252821::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (BufferChunk_t1400678142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1971[3] = 
{
	BufferChunk_t1400678142::get_offset_of_buffer_0(),
	BufferChunk_t1400678142::get_offset_of_index_1(),
	BufferChunk_t1400678142::get_offset_of_count_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (NamespaceResolverProxy_t4291765537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1972[1] = 
{
	NamespaceResolverProxy_t4291765537::get_offset_of_wfWriter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (NamespaceKind_t1063930693)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1973[5] = 
{
	NamespaceKind_t1063930693::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (AttrName_t2097328179)+ sizeof (RuntimeObject), sizeof(AttrName_t2097328179_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1974[4] = 
{
	AttrName_t2097328179::get_offset_of_prefix_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttrName_t2097328179::get_offset_of_namespaceUri_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttrName_t2097328179::get_offset_of_localName_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttrName_t2097328179::get_offset_of_prev_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (SpecialAttribute_t2983285139)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1975[6] = 
{
	SpecialAttribute_t2983285139::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (XmlWriter_t127905479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1976[1] = 
{
	XmlWriter_t127905479::get_offset_of_writeNodeBuffer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (WriteState_t3983380671)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1977[8] = 
{
	WriteState_t3983380671::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (XmlOutputMethod_t2185361861)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1978[5] = 
{
	XmlOutputMethod_t2185361861::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (TriState_t50539540)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1979[4] = 
{
	TriState_t50539540::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (XmlStandalone_t2027388713)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1980[4] = 
{
	XmlStandalone_t2027388713::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (XmlWriterSettings_t3314986516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1981[23] = 
{
	XmlWriterSettings_t3314986516::get_offset_of_useAsync_0(),
	XmlWriterSettings_t3314986516::get_offset_of_encoding_1(),
	XmlWriterSettings_t3314986516::get_offset_of_omitXmlDecl_2(),
	XmlWriterSettings_t3314986516::get_offset_of_newLineHandling_3(),
	XmlWriterSettings_t3314986516::get_offset_of_newLineChars_4(),
	XmlWriterSettings_t3314986516::get_offset_of_indent_5(),
	XmlWriterSettings_t3314986516::get_offset_of_indentChars_6(),
	XmlWriterSettings_t3314986516::get_offset_of_newLineOnAttributes_7(),
	XmlWriterSettings_t3314986516::get_offset_of_closeOutput_8(),
	XmlWriterSettings_t3314986516::get_offset_of_namespaceHandling_9(),
	XmlWriterSettings_t3314986516::get_offset_of_conformanceLevel_10(),
	XmlWriterSettings_t3314986516::get_offset_of_checkCharacters_11(),
	XmlWriterSettings_t3314986516::get_offset_of_writeEndDocumentOnClose_12(),
	XmlWriterSettings_t3314986516::get_offset_of_outputMethod_13(),
	XmlWriterSettings_t3314986516::get_offset_of_cdataSections_14(),
	XmlWriterSettings_t3314986516::get_offset_of_doNotEscapeUriAttributes_15(),
	XmlWriterSettings_t3314986516::get_offset_of_mergeCDataSections_16(),
	XmlWriterSettings_t3314986516::get_offset_of_mediaType_17(),
	XmlWriterSettings_t3314986516::get_offset_of_docTypeSystem_18(),
	XmlWriterSettings_t3314986516::get_offset_of_docTypePublic_19(),
	XmlWriterSettings_t3314986516::get_offset_of_standalone_20(),
	XmlWriterSettings_t3314986516::get_offset_of_autoXmlDecl_21(),
	XmlWriterSettings_t3314986516::get_offset_of_isReadOnly_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (DomNameTable_t751058560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1982[5] = 
{
	DomNameTable_t751058560::get_offset_of_entries_0(),
	DomNameTable_t751058560::get_offset_of_count_1(),
	DomNameTable_t751058560::get_offset_of_mask_2(),
	DomNameTable_t751058560::get_offset_of_ownerDocument_3(),
	DomNameTable_t751058560::get_offset_of_nameTable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (XmlAttributeCollection_t2316283784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (XmlAttribute_t1173852259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1984[2] = 
{
	XmlAttribute_t1173852259::get_offset_of_name_1(),
	XmlAttribute_t1173852259::get_offset_of_lastChild_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (XmlCDataSection_t3267478366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (XmlCharacterData_t1167807131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1986[1] = 
{
	XmlCharacterData_t1167807131::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (XmlChildEnumerator_t4124507839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1987[3] = 
{
	XmlChildEnumerator_t4124507839::get_offset_of_container_0(),
	XmlChildEnumerator_t4124507839::get_offset_of_child_1(),
	XmlChildEnumerator_t4124507839::get_offset_of_isFirst_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (XmlChildNodes_t2906253626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1988[1] = 
{
	XmlChildNodes_t2906253626::get_offset_of_container_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (XmlComment_t2476947920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (XmlDeclaration_t679870411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1990[3] = 
{
	XmlDeclaration_t679870411::get_offset_of_version_2(),
	XmlDeclaration_t679870411::get_offset_of_encoding_3(),
	XmlDeclaration_t679870411::get_offset_of_standalone_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (XmlDocument_t2837193595), -1, sizeof(XmlDocument_t2837193595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1991[44] = 
{
	XmlDocument_t2837193595::get_offset_of_implementation_1(),
	XmlDocument_t2837193595::get_offset_of_domNameTable_2(),
	XmlDocument_t2837193595::get_offset_of_lastChild_3(),
	XmlDocument_t2837193595::get_offset_of_entities_4(),
	XmlDocument_t2837193595::get_offset_of_htElementIdMap_5(),
	XmlDocument_t2837193595::get_offset_of_htElementIDAttrDecl_6(),
	XmlDocument_t2837193595::get_offset_of_schemaInfo_7(),
	XmlDocument_t2837193595::get_offset_of_schemas_8(),
	XmlDocument_t2837193595::get_offset_of_reportValidity_9(),
	XmlDocument_t2837193595::get_offset_of_actualLoadingStatus_10(),
	XmlDocument_t2837193595::get_offset_of_onNodeInsertingDelegate_11(),
	XmlDocument_t2837193595::get_offset_of_onNodeInsertedDelegate_12(),
	XmlDocument_t2837193595::get_offset_of_onNodeRemovingDelegate_13(),
	XmlDocument_t2837193595::get_offset_of_onNodeRemovedDelegate_14(),
	XmlDocument_t2837193595::get_offset_of_onNodeChangingDelegate_15(),
	XmlDocument_t2837193595::get_offset_of_onNodeChangedDelegate_16(),
	XmlDocument_t2837193595::get_offset_of_fEntRefNodesPresent_17(),
	XmlDocument_t2837193595::get_offset_of_fCDataNodesPresent_18(),
	XmlDocument_t2837193595::get_offset_of_preserveWhitespace_19(),
	XmlDocument_t2837193595::get_offset_of_isLoading_20(),
	XmlDocument_t2837193595::get_offset_of_strDocumentName_21(),
	XmlDocument_t2837193595::get_offset_of_strDocumentFragmentName_22(),
	XmlDocument_t2837193595::get_offset_of_strCommentName_23(),
	XmlDocument_t2837193595::get_offset_of_strTextName_24(),
	XmlDocument_t2837193595::get_offset_of_strCDataSectionName_25(),
	XmlDocument_t2837193595::get_offset_of_strEntityName_26(),
	XmlDocument_t2837193595::get_offset_of_strID_27(),
	XmlDocument_t2837193595::get_offset_of_strXmlns_28(),
	XmlDocument_t2837193595::get_offset_of_strXml_29(),
	XmlDocument_t2837193595::get_offset_of_strSpace_30(),
	XmlDocument_t2837193595::get_offset_of_strLang_31(),
	XmlDocument_t2837193595::get_offset_of_strEmpty_32(),
	XmlDocument_t2837193595::get_offset_of_strNonSignificantWhitespaceName_33(),
	XmlDocument_t2837193595::get_offset_of_strSignificantWhitespaceName_34(),
	XmlDocument_t2837193595::get_offset_of_strReservedXmlns_35(),
	XmlDocument_t2837193595::get_offset_of_strReservedXml_36(),
	XmlDocument_t2837193595::get_offset_of_baseURI_37(),
	XmlDocument_t2837193595::get_offset_of_resolver_38(),
	XmlDocument_t2837193595::get_offset_of_bSetResolver_39(),
	XmlDocument_t2837193595::get_offset_of_objLock_40(),
	XmlDocument_t2837193595_StaticFields::get_offset_of_EmptyEnumerator_41(),
	XmlDocument_t2837193595_StaticFields::get_offset_of_NotKnownSchemaInfo_42(),
	XmlDocument_t2837193595_StaticFields::get_offset_of_ValidSchemaInfo_43(),
	XmlDocument_t2837193595_StaticFields::get_offset_of_InvalidSchemaInfo_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (XmlDocumentFragment_t1323348855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1992[1] = 
{
	XmlDocumentFragment_t1323348855::get_offset_of_lastChild_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (XmlDocumentType_t4112370061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1993[8] = 
{
	XmlDocumentType_t4112370061::get_offset_of_name_2(),
	XmlDocumentType_t4112370061::get_offset_of_publicId_3(),
	XmlDocumentType_t4112370061::get_offset_of_systemId_4(),
	XmlDocumentType_t4112370061::get_offset_of_internalSubset_5(),
	XmlDocumentType_t4112370061::get_offset_of_namespaces_6(),
	XmlDocumentType_t4112370061::get_offset_of_entities_7(),
	XmlDocumentType_t4112370061::get_offset_of_notations_8(),
	XmlDocumentType_t4112370061::get_offset_of_schemaInfo_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (XmlElement_t561603118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1994[3] = 
{
	XmlElement_t561603118::get_offset_of_name_2(),
	XmlElement_t561603118::get_offset_of_attributes_3(),
	XmlElement_t561603118::get_offset_of_lastChild_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (XmlEntity_t3308518401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1995[8] = 
{
	XmlEntity_t3308518401::get_offset_of_publicId_1(),
	XmlEntity_t3308518401::get_offset_of_systemId_2(),
	XmlEntity_t3308518401::get_offset_of_notationName_3(),
	XmlEntity_t3308518401::get_offset_of_name_4(),
	XmlEntity_t3308518401::get_offset_of_unparsedReplacementStr_5(),
	XmlEntity_t3308518401::get_offset_of_baseURI_6(),
	XmlEntity_t3308518401::get_offset_of_lastChild_7(),
	XmlEntity_t3308518401::get_offset_of_childrenFoliating_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (XmlEntityReference_t1966808559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1996[2] = 
{
	XmlEntityReference_t1966808559::get_offset_of_name_2(),
	XmlEntityReference_t1966808559::get_offset_of_lastChild_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (XmlNodeChangedAction_t3227731597)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1997[4] = 
{
	XmlNodeChangedAction_t3227731597::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (XmlImplementation_t254178875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1998[1] = 
{
	XmlImplementation_t254178875::get_offset_of_nameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (XmlLinkedNode_t1437094927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1999[1] = 
{
	XmlLinkedNode_t1437094927::get_offset_of_next_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
