﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MeshCollider
struct MeshCollider_t903564387;
// HoloToolkit.Unity.AudioEvent
struct AudioEvent_t3236711075;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.String
struct String_t;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// System.Func`2<UnityEngine.Vector2,System.Single>
struct Func_2_t3672504941;
// System.Func`2<UnityEngine.Vector3,System.Single>
struct Func_2_t3934069716;
// System.Collections.Generic.List`1<System.Func`3<HoloToolkit.Unity.ComparableRaycastResult,HoloToolkit.Unity.ComparableRaycastResult,System.Int32>>
struct List_1_t2955808224;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// System.Action`1<UnityEngine.AudioSource>
struct Action_1_t4107773183;
// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<System.Single>>
struct Action_1_t3487857569;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// HoloToolkit.Unity.RaycastResultComparer
struct RaycastResultComparer_t838146858;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.ICollection`1<UnityEngine.Transform>
struct ICollection_1_t2133550859;
// System.Collections.Generic.Queue`1<UnityEngine.Transform>
struct Queue_1_t3446625415;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Threading.Tasks.Task
struct Task_t3187275312;
// System.Void
struct Void_t1185182177;
// System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData>
struct Dictionary_2_t2868937860;
// HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData
struct MeshData_t2646720140;
// System.Threading.Tasks.Task`1<HoloToolkit.Unity.MicrophoneStatus>
struct Task_1_t1265886475;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_t923100567;
// System.Action
struct Action_t1264377477;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData[]
struct MeshDataU5BU5D_t513299113;
// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle>
struct List_1_t528545633;
// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/RaycastResult
struct RaycastResult_t1603201782;
// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats
struct PlayspaceStats_t3538817570;
// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceAlignment
struct PlayspaceAlignment_t492554602;
// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementResult
struct ObjectPlacementResult_t4094328837;
// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Vector2>>
struct Action_1_t4246820318;
// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Quaternion>>
struct Action_1_t97551830;
// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Vector3>>
struct Action_1_t1517936963;
// System.Threading.Tasks.Task`1<System.Boolean>
struct Task_1_t1502828140;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t4150874583;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Color>>
struct Action_1_t351309823;
// HoloToolkit.Unity.SpatialUnderstandingCustomMesh
struct SpatialUnderstandingCustomMesh_t3317789065;
// System.Diagnostics.Stopwatch
struct Stopwatch_t305734070;
// HoloToolkit.Unity.SpatialUnderstandingDll
struct SpatialUnderstandingDll_t3348055109;
// Windows.Media.Capture.MediaCaptureInitializationSettings
struct MediaCaptureInitializationSettings_t546830356;
// Windows.Media.Capture.MediaCapture
struct MediaCapture_t1516581975;
// System.Exception
struct Exception_t1436737249;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// HoloToolkit.Unity.UAudioClip[]
struct UAudioClipU5BU5D_t3119163315;
// UnityEngine.Audio.AudioMixerGroup
struct AudioMixerGroup_t2743564464;
// HoloToolkit.Unity.AudioContainer
struct AudioContainer_t974065762;
// HoloToolkit.Unity.AudioEvent[]
struct AudioEventU5BU5D_t1217199986;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Windows.Foundation.IAsyncOperation`1<Windows.Storage.Streams.IRandomAccessStreamWithContentType>
struct IAsyncOperation_1_t1918115372;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// System.Collections.Generic.List`1<HoloToolkit.Unity.IAudioInfluencer>
struct List_1_t3900577486;
// HoloToolkit.Unity.WorldAnchorManager
struct WorldAnchorManager_t2731889775;
// System.Collections.Generic.List`1<UnityEngine.AudioSource>
struct List_1_t1112413034;
// HoloToolkit.Unity.FadeScript
struct FadeScript_t3201568845;
// System.EventHandler`1<HoloToolkit.Unity.SpatialMapping.DataEventArgs`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject>>
struct EventHandler_1_t826557235;
// System.EventHandler`1<HoloToolkit.Unity.SpatialMapping.DataEventArgs`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceUpdate>>
struct EventHandler_1_t2444444190;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1515976428;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Collections.Generic.List`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject>
struct List_1_t4085315060;
// System.Collections.ObjectModel.ReadOnlyCollection`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject>
struct ReadOnlyCollection_1_t3825816605;
// HoloToolkit.Unity.AudioEventBank[]
struct AudioEventBankU5BU5D_t218382902;
// System.Collections.Generic.List`1<HoloToolkit.Unity.ActiveEvent>
struct List_1_t3163578750;
// System.Collections.Generic.List`1<HoloToolkit.Unity.AudioEventBank>
struct List_1_t1645733925;
// HoloToolkit.Unity.SpatialUnderstanding
struct SpatialUnderstanding_t1544682859;
// System.Func`2<UnityEngine.GameObject,UnityEngine.GameObject>
struct Func_2_t965320650;
// System.Collections.Generic.Dictionary`2<System.String,HoloToolkit.Unity.AudioEvent>
struct Dictionary_2_t3021967374;
// UnityEngine.Material
struct Material_t340375123;
// HoloToolkit.Unity.SpatialUnderstandingSourceMesh
struct SpatialUnderstandingSourceMesh_t2539908667;
// HoloToolkit.Unity.SpatialUnderstanding/OnScanDoneDelegate
struct OnScanDoneDelegate_t908476570;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// System.Collections.Generic.Queue`1<HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo>
struct Queue_1_t2234784041;
// UnityEngine.XR.WSA.Persistence.WorldAnchorStore
struct WorldAnchorStore_t633400888;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t898892918;

struct Vector3_t3722313464 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef MICROPHONEHELPER_T2952751248_H
#define MICROPHONEHELPER_T2952751248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MicrophoneHelper
struct  MicrophoneHelper_t2952751248  : public RuntimeObject
{
public:

public:
};

struct MicrophoneHelper_t2952751248_StaticFields
{
public:
	// System.Int32 HoloToolkit.Unity.MicrophoneHelper::NoCaptureDevicesHResult
	int32_t ___NoCaptureDevicesHResult_0;

public:
	inline static int32_t get_offset_of_NoCaptureDevicesHResult_0() { return static_cast<int32_t>(offsetof(MicrophoneHelper_t2952751248_StaticFields, ___NoCaptureDevicesHResult_0)); }
	inline int32_t get_NoCaptureDevicesHResult_0() const { return ___NoCaptureDevicesHResult_0; }
	inline int32_t* get_address_of_NoCaptureDevicesHResult_0() { return &___NoCaptureDevicesHResult_0; }
	inline void set_NoCaptureDevicesHResult_0(int32_t value)
	{
		___NoCaptureDevicesHResult_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROPHONEHELPER_T2952751248_H
#ifndef MESHDATA_T2646720140_H
#define MESHDATA_T2646720140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData
struct  MeshData_t2646720140  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData::verts
	List_1_t899420910 * ___verts_0;
	// System.Collections.Generic.List`1<System.Int32> HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData::tris
	List_1_t128053199 * ___tris_1;
	// UnityEngine.Mesh HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData::MeshObject
	Mesh_t3648964284 * ___MeshObject_2;
	// UnityEngine.MeshCollider HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData::SpatialCollider
	MeshCollider_t903564387 * ___SpatialCollider_3;
	// System.Boolean HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData::CreateMeshCollider
	bool ___CreateMeshCollider_4;

public:
	inline static int32_t get_offset_of_verts_0() { return static_cast<int32_t>(offsetof(MeshData_t2646720140, ___verts_0)); }
	inline List_1_t899420910 * get_verts_0() const { return ___verts_0; }
	inline List_1_t899420910 ** get_address_of_verts_0() { return &___verts_0; }
	inline void set_verts_0(List_1_t899420910 * value)
	{
		___verts_0 = value;
		Il2CppCodeGenWriteBarrier((&___verts_0), value);
	}

	inline static int32_t get_offset_of_tris_1() { return static_cast<int32_t>(offsetof(MeshData_t2646720140, ___tris_1)); }
	inline List_1_t128053199 * get_tris_1() const { return ___tris_1; }
	inline List_1_t128053199 ** get_address_of_tris_1() { return &___tris_1; }
	inline void set_tris_1(List_1_t128053199 * value)
	{
		___tris_1 = value;
		Il2CppCodeGenWriteBarrier((&___tris_1), value);
	}

	inline static int32_t get_offset_of_MeshObject_2() { return static_cast<int32_t>(offsetof(MeshData_t2646720140, ___MeshObject_2)); }
	inline Mesh_t3648964284 * get_MeshObject_2() const { return ___MeshObject_2; }
	inline Mesh_t3648964284 ** get_address_of_MeshObject_2() { return &___MeshObject_2; }
	inline void set_MeshObject_2(Mesh_t3648964284 * value)
	{
		___MeshObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___MeshObject_2), value);
	}

	inline static int32_t get_offset_of_SpatialCollider_3() { return static_cast<int32_t>(offsetof(MeshData_t2646720140, ___SpatialCollider_3)); }
	inline MeshCollider_t903564387 * get_SpatialCollider_3() const { return ___SpatialCollider_3; }
	inline MeshCollider_t903564387 ** get_address_of_SpatialCollider_3() { return &___SpatialCollider_3; }
	inline void set_SpatialCollider_3(MeshCollider_t903564387 * value)
	{
		___SpatialCollider_3 = value;
		Il2CppCodeGenWriteBarrier((&___SpatialCollider_3), value);
	}

	inline static int32_t get_offset_of_CreateMeshCollider_4() { return static_cast<int32_t>(offsetof(MeshData_t2646720140, ___CreateMeshCollider_4)); }
	inline bool get_CreateMeshCollider_4() const { return ___CreateMeshCollider_4; }
	inline bool* get_address_of_CreateMeshCollider_4() { return &___CreateMeshCollider_4; }
	inline void set_CreateMeshCollider_4(bool value)
	{
		___CreateMeshCollider_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHDATA_T2646720140_H
#ifndef U3CU3EC__DISPLAYCLASS29_0_T1666631841_H
#define U3CU3EC__DISPLAYCLASS29_0_T1666631841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ActiveEvent/<>c__DisplayClass29_0
struct  U3CU3Ec__DisplayClass29_0_t1666631841  : public RuntimeObject
{
public:
	// HoloToolkit.Unity.AudioEvent HoloToolkit.Unity.ActiveEvent/<>c__DisplayClass29_0::audioEvent
	AudioEvent_t3236711075 * ___audioEvent_0;
	// System.Single HoloToolkit.Unity.ActiveEvent/<>c__DisplayClass29_0::pitch
	float ___pitch_1;
	// System.Single HoloToolkit.Unity.ActiveEvent/<>c__DisplayClass29_0::vol
	float ___vol_2;
	// System.Single HoloToolkit.Unity.ActiveEvent/<>c__DisplayClass29_0::pan
	float ___pan_3;

public:
	inline static int32_t get_offset_of_audioEvent_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t1666631841, ___audioEvent_0)); }
	inline AudioEvent_t3236711075 * get_audioEvent_0() const { return ___audioEvent_0; }
	inline AudioEvent_t3236711075 ** get_address_of_audioEvent_0() { return &___audioEvent_0; }
	inline void set_audioEvent_0(AudioEvent_t3236711075 * value)
	{
		___audioEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___audioEvent_0), value);
	}

	inline static int32_t get_offset_of_pitch_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t1666631841, ___pitch_1)); }
	inline float get_pitch_1() const { return ___pitch_1; }
	inline float* get_address_of_pitch_1() { return &___pitch_1; }
	inline void set_pitch_1(float value)
	{
		___pitch_1 = value;
	}

	inline static int32_t get_offset_of_vol_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t1666631841, ___vol_2)); }
	inline float get_vol_2() const { return ___vol_2; }
	inline float* get_address_of_vol_2() { return &___vol_2; }
	inline void set_vol_2(float value)
	{
		___vol_2 = value;
	}

	inline static int32_t get_offset_of_pan_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t1666631841, ___pan_3)); }
	inline float get_pan_3() const { return ___pan_3; }
	inline float* get_address_of_pan_3() { return &___pan_3; }
	inline void set_pan_3(float value)
	{
		___pan_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS29_0_T1666631841_H
#ifndef ACTIVEEVENT_T1691504008_H
#define ACTIVEEVENT_T1691504008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ActiveEvent
struct  ActiveEvent_t1691504008  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource HoloToolkit.Unity.ActiveEvent::primarySource
	AudioSource_t3935305588 * ___primarySource_0;
	// UnityEngine.AudioSource HoloToolkit.Unity.ActiveEvent::secondarySource
	AudioSource_t3935305588 * ___secondarySource_1;
	// UnityEngine.GameObject HoloToolkit.Unity.ActiveEvent::<AudioEmitter>k__BackingField
	GameObject_t1113636619 * ___U3CAudioEmitterU3Ek__BackingField_2;
	// System.String HoloToolkit.Unity.ActiveEvent::<MessageOnAudioEnd>k__BackingField
	String_t* ___U3CMessageOnAudioEndU3Ek__BackingField_3;
	// HoloToolkit.Unity.AudioEvent HoloToolkit.Unity.ActiveEvent::AudioEvent
	AudioEvent_t3236711075 * ___AudioEvent_4;
	// System.Boolean HoloToolkit.Unity.ActiveEvent::IsStoppable
	bool ___IsStoppable_5;
	// System.Single HoloToolkit.Unity.ActiveEvent::VolDest
	float ___VolDest_6;
	// System.Single HoloToolkit.Unity.ActiveEvent::AltVolDest
	float ___AltVolDest_7;
	// System.Single HoloToolkit.Unity.ActiveEvent::CurrentFade
	float ___CurrentFade_8;
	// System.Boolean HoloToolkit.Unity.ActiveEvent::PlayingAlt
	bool ___PlayingAlt_9;
	// System.Boolean HoloToolkit.Unity.ActiveEvent::IsActiveTimeComplete
	bool ___IsActiveTimeComplete_10;
	// System.Single HoloToolkit.Unity.ActiveEvent::ActiveTime
	float ___ActiveTime_11;
	// System.Boolean HoloToolkit.Unity.ActiveEvent::CancelEvent
	bool ___CancelEvent_12;

public:
	inline static int32_t get_offset_of_primarySource_0() { return static_cast<int32_t>(offsetof(ActiveEvent_t1691504008, ___primarySource_0)); }
	inline AudioSource_t3935305588 * get_primarySource_0() const { return ___primarySource_0; }
	inline AudioSource_t3935305588 ** get_address_of_primarySource_0() { return &___primarySource_0; }
	inline void set_primarySource_0(AudioSource_t3935305588 * value)
	{
		___primarySource_0 = value;
		Il2CppCodeGenWriteBarrier((&___primarySource_0), value);
	}

	inline static int32_t get_offset_of_secondarySource_1() { return static_cast<int32_t>(offsetof(ActiveEvent_t1691504008, ___secondarySource_1)); }
	inline AudioSource_t3935305588 * get_secondarySource_1() const { return ___secondarySource_1; }
	inline AudioSource_t3935305588 ** get_address_of_secondarySource_1() { return &___secondarySource_1; }
	inline void set_secondarySource_1(AudioSource_t3935305588 * value)
	{
		___secondarySource_1 = value;
		Il2CppCodeGenWriteBarrier((&___secondarySource_1), value);
	}

	inline static int32_t get_offset_of_U3CAudioEmitterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ActiveEvent_t1691504008, ___U3CAudioEmitterU3Ek__BackingField_2)); }
	inline GameObject_t1113636619 * get_U3CAudioEmitterU3Ek__BackingField_2() const { return ___U3CAudioEmitterU3Ek__BackingField_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CAudioEmitterU3Ek__BackingField_2() { return &___U3CAudioEmitterU3Ek__BackingField_2; }
	inline void set_U3CAudioEmitterU3Ek__BackingField_2(GameObject_t1113636619 * value)
	{
		___U3CAudioEmitterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAudioEmitterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CMessageOnAudioEndU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ActiveEvent_t1691504008, ___U3CMessageOnAudioEndU3Ek__BackingField_3)); }
	inline String_t* get_U3CMessageOnAudioEndU3Ek__BackingField_3() const { return ___U3CMessageOnAudioEndU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CMessageOnAudioEndU3Ek__BackingField_3() { return &___U3CMessageOnAudioEndU3Ek__BackingField_3; }
	inline void set_U3CMessageOnAudioEndU3Ek__BackingField_3(String_t* value)
	{
		___U3CMessageOnAudioEndU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageOnAudioEndU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_AudioEvent_4() { return static_cast<int32_t>(offsetof(ActiveEvent_t1691504008, ___AudioEvent_4)); }
	inline AudioEvent_t3236711075 * get_AudioEvent_4() const { return ___AudioEvent_4; }
	inline AudioEvent_t3236711075 ** get_address_of_AudioEvent_4() { return &___AudioEvent_4; }
	inline void set_AudioEvent_4(AudioEvent_t3236711075 * value)
	{
		___AudioEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___AudioEvent_4), value);
	}

	inline static int32_t get_offset_of_IsStoppable_5() { return static_cast<int32_t>(offsetof(ActiveEvent_t1691504008, ___IsStoppable_5)); }
	inline bool get_IsStoppable_5() const { return ___IsStoppable_5; }
	inline bool* get_address_of_IsStoppable_5() { return &___IsStoppable_5; }
	inline void set_IsStoppable_5(bool value)
	{
		___IsStoppable_5 = value;
	}

	inline static int32_t get_offset_of_VolDest_6() { return static_cast<int32_t>(offsetof(ActiveEvent_t1691504008, ___VolDest_6)); }
	inline float get_VolDest_6() const { return ___VolDest_6; }
	inline float* get_address_of_VolDest_6() { return &___VolDest_6; }
	inline void set_VolDest_6(float value)
	{
		___VolDest_6 = value;
	}

	inline static int32_t get_offset_of_AltVolDest_7() { return static_cast<int32_t>(offsetof(ActiveEvent_t1691504008, ___AltVolDest_7)); }
	inline float get_AltVolDest_7() const { return ___AltVolDest_7; }
	inline float* get_address_of_AltVolDest_7() { return &___AltVolDest_7; }
	inline void set_AltVolDest_7(float value)
	{
		___AltVolDest_7 = value;
	}

	inline static int32_t get_offset_of_CurrentFade_8() { return static_cast<int32_t>(offsetof(ActiveEvent_t1691504008, ___CurrentFade_8)); }
	inline float get_CurrentFade_8() const { return ___CurrentFade_8; }
	inline float* get_address_of_CurrentFade_8() { return &___CurrentFade_8; }
	inline void set_CurrentFade_8(float value)
	{
		___CurrentFade_8 = value;
	}

	inline static int32_t get_offset_of_PlayingAlt_9() { return static_cast<int32_t>(offsetof(ActiveEvent_t1691504008, ___PlayingAlt_9)); }
	inline bool get_PlayingAlt_9() const { return ___PlayingAlt_9; }
	inline bool* get_address_of_PlayingAlt_9() { return &___PlayingAlt_9; }
	inline void set_PlayingAlt_9(bool value)
	{
		___PlayingAlt_9 = value;
	}

	inline static int32_t get_offset_of_IsActiveTimeComplete_10() { return static_cast<int32_t>(offsetof(ActiveEvent_t1691504008, ___IsActiveTimeComplete_10)); }
	inline bool get_IsActiveTimeComplete_10() const { return ___IsActiveTimeComplete_10; }
	inline bool* get_address_of_IsActiveTimeComplete_10() { return &___IsActiveTimeComplete_10; }
	inline void set_IsActiveTimeComplete_10(bool value)
	{
		___IsActiveTimeComplete_10 = value;
	}

	inline static int32_t get_offset_of_ActiveTime_11() { return static_cast<int32_t>(offsetof(ActiveEvent_t1691504008, ___ActiveTime_11)); }
	inline float get_ActiveTime_11() const { return ___ActiveTime_11; }
	inline float* get_address_of_ActiveTime_11() { return &___ActiveTime_11; }
	inline void set_ActiveTime_11(float value)
	{
		___ActiveTime_11 = value;
	}

	inline static int32_t get_offset_of_CancelEvent_12() { return static_cast<int32_t>(offsetof(ActiveEvent_t1691504008, ___CancelEvent_12)); }
	inline bool get_CancelEvent_12() const { return ___CancelEvent_12; }
	inline bool* get_address_of_CancelEvent_12() { return &___CancelEvent_12; }
	inline void set_CancelEvent_12(bool value)
	{
		___CancelEvent_12 = value;
	}
};

struct ActiveEvent_t1691504008_StaticFields
{
public:
	// UnityEngine.AnimationCurve HoloToolkit.Unity.ActiveEvent::SpatialRolloff
	AnimationCurve_t3046754366 * ___SpatialRolloff_13;

public:
	inline static int32_t get_offset_of_SpatialRolloff_13() { return static_cast<int32_t>(offsetof(ActiveEvent_t1691504008_StaticFields, ___SpatialRolloff_13)); }
	inline AnimationCurve_t3046754366 * get_SpatialRolloff_13() const { return ___SpatialRolloff_13; }
	inline AnimationCurve_t3046754366 ** get_address_of_SpatialRolloff_13() { return &___SpatialRolloff_13; }
	inline void set_SpatialRolloff_13(AnimationCurve_t3046754366 * value)
	{
		___SpatialRolloff_13 = value;
		Il2CppCodeGenWriteBarrier((&___SpatialRolloff_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEEVENT_T1691504008_H
#ifndef MATHUTILS_T1302358766_H
#define MATHUTILS_T1302358766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MathUtils
struct  MathUtils_t1302358766  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTILS_T1302358766_H
#ifndef INTERPOLATIONUTILITIES_T2843552804_H
#define INTERPOLATIONUTILITIES_T2843552804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolationUtilities
struct  InterpolationUtilities_t2843552804  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATIONUTILITIES_T2843552804_H
#ifndef U3CU3EC_T2675217287_H
#define U3CU3EC_T2675217287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.VectorExtensions/<>c
struct  U3CU3Ec_t2675217287  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2675217287_StaticFields
{
public:
	// HoloToolkit.Unity.VectorExtensions/<>c HoloToolkit.Unity.VectorExtensions/<>c::<>9
	U3CU3Ec_t2675217287 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Vector2,System.Single> HoloToolkit.Unity.VectorExtensions/<>c::<>9__12_0
	Func_2_t3672504941 * ___U3CU3E9__12_0_1;
	// System.Func`2<UnityEngine.Vector3,System.Single> HoloToolkit.Unity.VectorExtensions/<>c::<>9__13_0
	Func_2_t3934069716 * ___U3CU3E9__13_0_2;
	// System.Func`2<UnityEngine.Vector2,System.Single> HoloToolkit.Unity.VectorExtensions/<>c::<>9__14_0
	Func_2_t3672504941 * ___U3CU3E9__14_0_3;
	// System.Func`2<UnityEngine.Vector3,System.Single> HoloToolkit.Unity.VectorExtensions/<>c::<>9__15_0
	Func_2_t3934069716 * ___U3CU3E9__15_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2675217287_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2675217287 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2675217287 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2675217287 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2675217287_StaticFields, ___U3CU3E9__12_0_1)); }
	inline Func_2_t3672504941 * get_U3CU3E9__12_0_1() const { return ___U3CU3E9__12_0_1; }
	inline Func_2_t3672504941 ** get_address_of_U3CU3E9__12_0_1() { return &___U3CU3E9__12_0_1; }
	inline void set_U3CU3E9__12_0_1(Func_2_t3672504941 * value)
	{
		___U3CU3E9__12_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__13_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2675217287_StaticFields, ___U3CU3E9__13_0_2)); }
	inline Func_2_t3934069716 * get_U3CU3E9__13_0_2() const { return ___U3CU3E9__13_0_2; }
	inline Func_2_t3934069716 ** get_address_of_U3CU3E9__13_0_2() { return &___U3CU3E9__13_0_2; }
	inline void set_U3CU3E9__13_0_2(Func_2_t3934069716 * value)
	{
		___U3CU3E9__13_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__13_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2675217287_StaticFields, ___U3CU3E9__14_0_3)); }
	inline Func_2_t3672504941 * get_U3CU3E9__14_0_3() const { return ___U3CU3E9__14_0_3; }
	inline Func_2_t3672504941 ** get_address_of_U3CU3E9__14_0_3() { return &___U3CU3E9__14_0_3; }
	inline void set_U3CU3E9__14_0_3(Func_2_t3672504941 * value)
	{
		___U3CU3E9__14_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2675217287_StaticFields, ___U3CU3E9__15_0_4)); }
	inline Func_2_t3934069716 * get_U3CU3E9__15_0_4() const { return ___U3CU3E9__15_0_4; }
	inline Func_2_t3934069716 ** get_address_of_U3CU3E9__15_0_4() { return &___U3CU3E9__15_0_4; }
	inline void set_U3CU3E9__15_0_4(Func_2_t3934069716 * value)
	{
		___U3CU3E9__15_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__15_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2675217287_H
#ifndef RAYCASTRESULTCOMPARER_T838146858_H
#define RAYCASTRESULTCOMPARER_T838146858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RaycastResultComparer
struct  RaycastResultComparer_t838146858  : public RuntimeObject
{
public:

public:
};

struct RaycastResultComparer_t838146858_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Func`3<HoloToolkit.Unity.ComparableRaycastResult,HoloToolkit.Unity.ComparableRaycastResult,System.Int32>> HoloToolkit.Unity.RaycastResultComparer::Comparers
	List_1_t2955808224 * ___Comparers_0;

public:
	inline static int32_t get_offset_of_Comparers_0() { return static_cast<int32_t>(offsetof(RaycastResultComparer_t838146858_StaticFields, ___Comparers_0)); }
	inline List_1_t2955808224 * get_Comparers_0() const { return ___Comparers_0; }
	inline List_1_t2955808224 ** get_address_of_Comparers_0() { return &___Comparers_0; }
	inline void set_Comparers_0(List_1_t2955808224 * value)
	{
		___Comparers_0 = value;
		Il2CppCodeGenWriteBarrier((&___Comparers_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTRESULTCOMPARER_T838146858_H
#ifndef SPATIALUNDERSTANDINGDLLOBJECTPLACEMENT_T605993937_H
#define SPATIALUNDERSTANDINGDLLOBJECTPLACEMENT_T605993937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement
struct  SpatialUnderstandingDllObjectPlacement_t605993937  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALUNDERSTANDINGDLLOBJECTPLACEMENT_T605993937_H
#ifndef AUDIOSOURCEPLAYCLIPEXTENSION_T4158734528_H
#define AUDIOSOURCEPLAYCLIPEXTENSION_T4158734528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioSourcePlayClipExtension
struct  AudioSourcePlayClipExtension_t4158734528  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCEPLAYCLIPEXTENSION_T4158734528_H
#ifndef IMPORTS_T718195452_H
#define IMPORTS_T718195452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll/Imports
struct  Imports_t718195452  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPORTS_T718195452_H
#ifndef PLAYSPACESTATS_T3538817570_H
#define PLAYSPACESTATS_T3538817570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats
#pragma pack(push, tp, 1)
struct  PlayspaceStats_t3538817570  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::IsWorkingOnStats
	int32_t ___IsWorkingOnStats_0;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::HorizSurfaceArea
	float ___HorizSurfaceArea_1;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::TotalSurfaceArea
	float ___TotalSurfaceArea_2;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::UpSurfaceArea
	float ___UpSurfaceArea_3;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::DownSurfaceArea
	float ___DownSurfaceArea_4;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::WallSurfaceArea
	float ___WallSurfaceArea_5;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::VirtualCeilingSurfaceArea
	float ___VirtualCeilingSurfaceArea_6;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::VirtualWallSurfaceArea
	float ___VirtualWallSurfaceArea_7;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::NumFloor
	int32_t ___NumFloor_8;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::NumCeiling
	int32_t ___NumCeiling_9;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::NumWall_XNeg
	int32_t ___NumWall_XNeg_10;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::NumWall_XPos
	int32_t ___NumWall_XPos_11;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::NumWall_ZNeg
	int32_t ___NumWall_ZNeg_12;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::NumWall_ZPos
	int32_t ___NumWall_ZPos_13;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::NumPlatform
	int32_t ___NumPlatform_14;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::CellCount_IsPaintMode
	int32_t ___CellCount_IsPaintMode_15;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::CellCount_IsSeenQualtiy_None
	int32_t ___CellCount_IsSeenQualtiy_None_16;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::CellCount_IsSeenQualtiy_Seen
	int32_t ___CellCount_IsSeenQualtiy_Seen_17;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats::CellCount_IsSeenQualtiy_Good
	int32_t ___CellCount_IsSeenQualtiy_Good_18;

public:
	inline static int32_t get_offset_of_IsWorkingOnStats_0() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___IsWorkingOnStats_0)); }
	inline int32_t get_IsWorkingOnStats_0() const { return ___IsWorkingOnStats_0; }
	inline int32_t* get_address_of_IsWorkingOnStats_0() { return &___IsWorkingOnStats_0; }
	inline void set_IsWorkingOnStats_0(int32_t value)
	{
		___IsWorkingOnStats_0 = value;
	}

	inline static int32_t get_offset_of_HorizSurfaceArea_1() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___HorizSurfaceArea_1)); }
	inline float get_HorizSurfaceArea_1() const { return ___HorizSurfaceArea_1; }
	inline float* get_address_of_HorizSurfaceArea_1() { return &___HorizSurfaceArea_1; }
	inline void set_HorizSurfaceArea_1(float value)
	{
		___HorizSurfaceArea_1 = value;
	}

	inline static int32_t get_offset_of_TotalSurfaceArea_2() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___TotalSurfaceArea_2)); }
	inline float get_TotalSurfaceArea_2() const { return ___TotalSurfaceArea_2; }
	inline float* get_address_of_TotalSurfaceArea_2() { return &___TotalSurfaceArea_2; }
	inline void set_TotalSurfaceArea_2(float value)
	{
		___TotalSurfaceArea_2 = value;
	}

	inline static int32_t get_offset_of_UpSurfaceArea_3() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___UpSurfaceArea_3)); }
	inline float get_UpSurfaceArea_3() const { return ___UpSurfaceArea_3; }
	inline float* get_address_of_UpSurfaceArea_3() { return &___UpSurfaceArea_3; }
	inline void set_UpSurfaceArea_3(float value)
	{
		___UpSurfaceArea_3 = value;
	}

	inline static int32_t get_offset_of_DownSurfaceArea_4() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___DownSurfaceArea_4)); }
	inline float get_DownSurfaceArea_4() const { return ___DownSurfaceArea_4; }
	inline float* get_address_of_DownSurfaceArea_4() { return &___DownSurfaceArea_4; }
	inline void set_DownSurfaceArea_4(float value)
	{
		___DownSurfaceArea_4 = value;
	}

	inline static int32_t get_offset_of_WallSurfaceArea_5() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___WallSurfaceArea_5)); }
	inline float get_WallSurfaceArea_5() const { return ___WallSurfaceArea_5; }
	inline float* get_address_of_WallSurfaceArea_5() { return &___WallSurfaceArea_5; }
	inline void set_WallSurfaceArea_5(float value)
	{
		___WallSurfaceArea_5 = value;
	}

	inline static int32_t get_offset_of_VirtualCeilingSurfaceArea_6() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___VirtualCeilingSurfaceArea_6)); }
	inline float get_VirtualCeilingSurfaceArea_6() const { return ___VirtualCeilingSurfaceArea_6; }
	inline float* get_address_of_VirtualCeilingSurfaceArea_6() { return &___VirtualCeilingSurfaceArea_6; }
	inline void set_VirtualCeilingSurfaceArea_6(float value)
	{
		___VirtualCeilingSurfaceArea_6 = value;
	}

	inline static int32_t get_offset_of_VirtualWallSurfaceArea_7() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___VirtualWallSurfaceArea_7)); }
	inline float get_VirtualWallSurfaceArea_7() const { return ___VirtualWallSurfaceArea_7; }
	inline float* get_address_of_VirtualWallSurfaceArea_7() { return &___VirtualWallSurfaceArea_7; }
	inline void set_VirtualWallSurfaceArea_7(float value)
	{
		___VirtualWallSurfaceArea_7 = value;
	}

	inline static int32_t get_offset_of_NumFloor_8() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___NumFloor_8)); }
	inline int32_t get_NumFloor_8() const { return ___NumFloor_8; }
	inline int32_t* get_address_of_NumFloor_8() { return &___NumFloor_8; }
	inline void set_NumFloor_8(int32_t value)
	{
		___NumFloor_8 = value;
	}

	inline static int32_t get_offset_of_NumCeiling_9() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___NumCeiling_9)); }
	inline int32_t get_NumCeiling_9() const { return ___NumCeiling_9; }
	inline int32_t* get_address_of_NumCeiling_9() { return &___NumCeiling_9; }
	inline void set_NumCeiling_9(int32_t value)
	{
		___NumCeiling_9 = value;
	}

	inline static int32_t get_offset_of_NumWall_XNeg_10() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___NumWall_XNeg_10)); }
	inline int32_t get_NumWall_XNeg_10() const { return ___NumWall_XNeg_10; }
	inline int32_t* get_address_of_NumWall_XNeg_10() { return &___NumWall_XNeg_10; }
	inline void set_NumWall_XNeg_10(int32_t value)
	{
		___NumWall_XNeg_10 = value;
	}

	inline static int32_t get_offset_of_NumWall_XPos_11() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___NumWall_XPos_11)); }
	inline int32_t get_NumWall_XPos_11() const { return ___NumWall_XPos_11; }
	inline int32_t* get_address_of_NumWall_XPos_11() { return &___NumWall_XPos_11; }
	inline void set_NumWall_XPos_11(int32_t value)
	{
		___NumWall_XPos_11 = value;
	}

	inline static int32_t get_offset_of_NumWall_ZNeg_12() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___NumWall_ZNeg_12)); }
	inline int32_t get_NumWall_ZNeg_12() const { return ___NumWall_ZNeg_12; }
	inline int32_t* get_address_of_NumWall_ZNeg_12() { return &___NumWall_ZNeg_12; }
	inline void set_NumWall_ZNeg_12(int32_t value)
	{
		___NumWall_ZNeg_12 = value;
	}

	inline static int32_t get_offset_of_NumWall_ZPos_13() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___NumWall_ZPos_13)); }
	inline int32_t get_NumWall_ZPos_13() const { return ___NumWall_ZPos_13; }
	inline int32_t* get_address_of_NumWall_ZPos_13() { return &___NumWall_ZPos_13; }
	inline void set_NumWall_ZPos_13(int32_t value)
	{
		___NumWall_ZPos_13 = value;
	}

	inline static int32_t get_offset_of_NumPlatform_14() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___NumPlatform_14)); }
	inline int32_t get_NumPlatform_14() const { return ___NumPlatform_14; }
	inline int32_t* get_address_of_NumPlatform_14() { return &___NumPlatform_14; }
	inline void set_NumPlatform_14(int32_t value)
	{
		___NumPlatform_14 = value;
	}

	inline static int32_t get_offset_of_CellCount_IsPaintMode_15() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___CellCount_IsPaintMode_15)); }
	inline int32_t get_CellCount_IsPaintMode_15() const { return ___CellCount_IsPaintMode_15; }
	inline int32_t* get_address_of_CellCount_IsPaintMode_15() { return &___CellCount_IsPaintMode_15; }
	inline void set_CellCount_IsPaintMode_15(int32_t value)
	{
		___CellCount_IsPaintMode_15 = value;
	}

	inline static int32_t get_offset_of_CellCount_IsSeenQualtiy_None_16() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___CellCount_IsSeenQualtiy_None_16)); }
	inline int32_t get_CellCount_IsSeenQualtiy_None_16() const { return ___CellCount_IsSeenQualtiy_None_16; }
	inline int32_t* get_address_of_CellCount_IsSeenQualtiy_None_16() { return &___CellCount_IsSeenQualtiy_None_16; }
	inline void set_CellCount_IsSeenQualtiy_None_16(int32_t value)
	{
		___CellCount_IsSeenQualtiy_None_16 = value;
	}

	inline static int32_t get_offset_of_CellCount_IsSeenQualtiy_Seen_17() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___CellCount_IsSeenQualtiy_Seen_17)); }
	inline int32_t get_CellCount_IsSeenQualtiy_Seen_17() const { return ___CellCount_IsSeenQualtiy_Seen_17; }
	inline int32_t* get_address_of_CellCount_IsSeenQualtiy_Seen_17() { return &___CellCount_IsSeenQualtiy_Seen_17; }
	inline void set_CellCount_IsSeenQualtiy_Seen_17(int32_t value)
	{
		___CellCount_IsSeenQualtiy_Seen_17 = value;
	}

	inline static int32_t get_offset_of_CellCount_IsSeenQualtiy_Good_18() { return static_cast<int32_t>(offsetof(PlayspaceStats_t3538817570, ___CellCount_IsSeenQualtiy_Good_18)); }
	inline int32_t get_CellCount_IsSeenQualtiy_Good_18() const { return ___CellCount_IsSeenQualtiy_Good_18; }
	inline int32_t* get_address_of_CellCount_IsSeenQualtiy_Good_18() { return &___CellCount_IsSeenQualtiy_Good_18; }
	inline void set_CellCount_IsSeenQualtiy_Good_18(int32_t value)
	{
		___CellCount_IsSeenQualtiy_Good_18 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats
#pragma pack(push, tp, 1)
struct PlayspaceStats_t3538817570_marshaled_pinvoke
{
	int32_t ___IsWorkingOnStats_0;
	float ___HorizSurfaceArea_1;
	float ___TotalSurfaceArea_2;
	float ___UpSurfaceArea_3;
	float ___DownSurfaceArea_4;
	float ___WallSurfaceArea_5;
	float ___VirtualCeilingSurfaceArea_6;
	float ___VirtualWallSurfaceArea_7;
	int32_t ___NumFloor_8;
	int32_t ___NumCeiling_9;
	int32_t ___NumWall_XNeg_10;
	int32_t ___NumWall_XPos_11;
	int32_t ___NumWall_ZNeg_12;
	int32_t ___NumWall_ZPos_13;
	int32_t ___NumPlatform_14;
	int32_t ___CellCount_IsPaintMode_15;
	int32_t ___CellCount_IsSeenQualtiy_None_16;
	int32_t ___CellCount_IsSeenQualtiy_Seen_17;
	int32_t ___CellCount_IsSeenQualtiy_Good_18;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats
#pragma pack(push, tp, 1)
struct PlayspaceStats_t3538817570_marshaled_com
{
	int32_t ___IsWorkingOnStats_0;
	float ___HorizSurfaceArea_1;
	float ___TotalSurfaceArea_2;
	float ___UpSurfaceArea_3;
	float ___DownSurfaceArea_4;
	float ___WallSurfaceArea_5;
	float ___VirtualCeilingSurfaceArea_6;
	float ___VirtualWallSurfaceArea_7;
	int32_t ___NumFloor_8;
	int32_t ___NumCeiling_9;
	int32_t ___NumWall_XNeg_10;
	int32_t ___NumWall_XPos_11;
	int32_t ___NumWall_ZNeg_12;
	int32_t ___NumWall_ZPos_13;
	int32_t ___NumPlatform_14;
	int32_t ___CellCount_IsPaintMode_15;
	int32_t ___CellCount_IsSeenQualtiy_None_16;
	int32_t ___CellCount_IsSeenQualtiy_Seen_17;
	int32_t ___CellCount_IsSeenQualtiy_Good_18;
};
#pragma pack(pop, tp)
#endif // PLAYSPACESTATS_T3538817570_H
#ifndef UAUDIOCLIP_T192323766_H
#define UAUDIOCLIP_T192323766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UAudioClip
struct  UAudioClip_t192323766  : public RuntimeObject
{
public:
	// UnityEngine.AudioClip HoloToolkit.Unity.UAudioClip::Sound
	AudioClip_t3680889665 * ___Sound_0;
	// System.Boolean HoloToolkit.Unity.UAudioClip::Looping
	bool ___Looping_1;
	// System.Single HoloToolkit.Unity.UAudioClip::DelayCenter
	float ___DelayCenter_2;
	// System.Single HoloToolkit.Unity.UAudioClip::DelayRandomization
	float ___DelayRandomization_3;

public:
	inline static int32_t get_offset_of_Sound_0() { return static_cast<int32_t>(offsetof(UAudioClip_t192323766, ___Sound_0)); }
	inline AudioClip_t3680889665 * get_Sound_0() const { return ___Sound_0; }
	inline AudioClip_t3680889665 ** get_address_of_Sound_0() { return &___Sound_0; }
	inline void set_Sound_0(AudioClip_t3680889665 * value)
	{
		___Sound_0 = value;
		Il2CppCodeGenWriteBarrier((&___Sound_0), value);
	}

	inline static int32_t get_offset_of_Looping_1() { return static_cast<int32_t>(offsetof(UAudioClip_t192323766, ___Looping_1)); }
	inline bool get_Looping_1() const { return ___Looping_1; }
	inline bool* get_address_of_Looping_1() { return &___Looping_1; }
	inline void set_Looping_1(bool value)
	{
		___Looping_1 = value;
	}

	inline static int32_t get_offset_of_DelayCenter_2() { return static_cast<int32_t>(offsetof(UAudioClip_t192323766, ___DelayCenter_2)); }
	inline float get_DelayCenter_2() const { return ___DelayCenter_2; }
	inline float* get_address_of_DelayCenter_2() { return &___DelayCenter_2; }
	inline void set_DelayCenter_2(float value)
	{
		___DelayCenter_2 = value;
	}

	inline static int32_t get_offset_of_DelayRandomization_3() { return static_cast<int32_t>(offsetof(UAudioClip_t192323766, ___DelayRandomization_3)); }
	inline float get_DelayRandomization_3() const { return ___DelayRandomization_3; }
	inline float* get_address_of_DelayRandomization_3() { return &___DelayRandomization_3; }
	inline void set_DelayRandomization_3(float value)
	{
		___DelayRandomization_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UAUDIOCLIP_T192323766_H
#ifndef U3CU3EC_T4034150452_H
#define U3CU3EC_T4034150452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ActiveEvent/<>c
struct  U3CU3Ec_t4034150452  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4034150452_StaticFields
{
public:
	// HoloToolkit.Unity.ActiveEvent/<>c HoloToolkit.Unity.ActiveEvent/<>c::<>9
	U3CU3Ec_t4034150452 * ___U3CU3E9_0;
	// System.Action`1<UnityEngine.AudioSource> HoloToolkit.Unity.ActiveEvent/<>c::<>9__29_1
	Action_1_t4107773183 * ___U3CU3E9__29_1_1;
	// System.Action`1<UnityEngine.AudioSource> HoloToolkit.Unity.ActiveEvent/<>c::<>9__29_2
	Action_1_t4107773183 * ___U3CU3E9__29_2_2;
	// System.Action`1<UnityEngine.AudioSource> HoloToolkit.Unity.ActiveEvent/<>c::<>9__29_3
	Action_1_t4107773183 * ___U3CU3E9__29_3_3;
	// System.Action`1<UnityEngine.AudioSource> HoloToolkit.Unity.ActiveEvent/<>c::<>9__29_8
	Action_1_t4107773183 * ___U3CU3E9__29_8_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4034150452_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4034150452 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4034150452 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4034150452 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4034150452_StaticFields, ___U3CU3E9__29_1_1)); }
	inline Action_1_t4107773183 * get_U3CU3E9__29_1_1() const { return ___U3CU3E9__29_1_1; }
	inline Action_1_t4107773183 ** get_address_of_U3CU3E9__29_1_1() { return &___U3CU3E9__29_1_1; }
	inline void set_U3CU3E9__29_1_1(Action_1_t4107773183 * value)
	{
		___U3CU3E9__29_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4034150452_StaticFields, ___U3CU3E9__29_2_2)); }
	inline Action_1_t4107773183 * get_U3CU3E9__29_2_2() const { return ___U3CU3E9__29_2_2; }
	inline Action_1_t4107773183 ** get_address_of_U3CU3E9__29_2_2() { return &___U3CU3E9__29_2_2; }
	inline void set_U3CU3E9__29_2_2(Action_1_t4107773183 * value)
	{
		___U3CU3E9__29_2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_3_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4034150452_StaticFields, ___U3CU3E9__29_3_3)); }
	inline Action_1_t4107773183 * get_U3CU3E9__29_3_3() const { return ___U3CU3E9__29_3_3; }
	inline Action_1_t4107773183 ** get_address_of_U3CU3E9__29_3_3() { return &___U3CU3E9__29_3_3; }
	inline void set_U3CU3E9__29_3_3(Action_1_t4107773183 * value)
	{
		___U3CU3E9__29_3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_8_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4034150452_StaticFields, ___U3CU3E9__29_8_4)); }
	inline Action_1_t4107773183 * get_U3CU3E9__29_8_4() const { return ___U3CU3E9__29_8_4; }
	inline Action_1_t4107773183 ** get_address_of_U3CU3E9__29_8_4() { return &___U3CU3E9__29_8_4; }
	inline void set_U3CU3E9__29_8_4(Action_1_t4107773183 * value)
	{
		___U3CU3E9__29_8_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_8_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4034150452_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef INTERPOLATEDVALUE_1_T3315389974_H
#define INTERPOLATEDVALUE_1_T3315389974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedValue`1<System.Single>
struct  InterpolatedValue_1_t3315389974  : public RuntimeObject
{
public:
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Started
	Action_1_t3487857569 * ___Started_2;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Completed
	Action_1_t3487857569 * ___Completed_3;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::ValueChanged
	Action_1_t3487857569 * ___ValueChanged_4;
	// T HoloToolkit.Unity.InterpolatedValue`1::targetValue
	float ___targetValue_5;
	// T HoloToolkit.Unity.InterpolatedValue`1::startValue
	float ___startValue_6;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::timeInterpolationStartedAt
	float ___timeInterpolationStartedAt_7;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::firstUpdateFrameSkipped
	bool ___firstUpdateFrameSkipped_8;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::skipFirstUpdateFrame
	bool ___skipFirstUpdateFrame_9;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::performingInterpolativeSnap
	bool ___performingInterpolativeSnap_10;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::duration
	float ___duration_11;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.InterpolatedValue`1::curve
	AnimationCurve_t3046754366 * ___curve_12;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::<IsRunning>k__BackingField
	bool ___U3CIsRunningU3Ek__BackingField_13;
	// T HoloToolkit.Unity.InterpolatedValue`1::value
	float ___value_14;

public:
	inline static int32_t get_offset_of_Started_2() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t3315389974, ___Started_2)); }
	inline Action_1_t3487857569 * get_Started_2() const { return ___Started_2; }
	inline Action_1_t3487857569 ** get_address_of_Started_2() { return &___Started_2; }
	inline void set_Started_2(Action_1_t3487857569 * value)
	{
		___Started_2 = value;
		Il2CppCodeGenWriteBarrier((&___Started_2), value);
	}

	inline static int32_t get_offset_of_Completed_3() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t3315389974, ___Completed_3)); }
	inline Action_1_t3487857569 * get_Completed_3() const { return ___Completed_3; }
	inline Action_1_t3487857569 ** get_address_of_Completed_3() { return &___Completed_3; }
	inline void set_Completed_3(Action_1_t3487857569 * value)
	{
		___Completed_3 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_3), value);
	}

	inline static int32_t get_offset_of_ValueChanged_4() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t3315389974, ___ValueChanged_4)); }
	inline Action_1_t3487857569 * get_ValueChanged_4() const { return ___ValueChanged_4; }
	inline Action_1_t3487857569 ** get_address_of_ValueChanged_4() { return &___ValueChanged_4; }
	inline void set_ValueChanged_4(Action_1_t3487857569 * value)
	{
		___ValueChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___ValueChanged_4), value);
	}

	inline static int32_t get_offset_of_targetValue_5() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t3315389974, ___targetValue_5)); }
	inline float get_targetValue_5() const { return ___targetValue_5; }
	inline float* get_address_of_targetValue_5() { return &___targetValue_5; }
	inline void set_targetValue_5(float value)
	{
		___targetValue_5 = value;
	}

	inline static int32_t get_offset_of_startValue_6() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t3315389974, ___startValue_6)); }
	inline float get_startValue_6() const { return ___startValue_6; }
	inline float* get_address_of_startValue_6() { return &___startValue_6; }
	inline void set_startValue_6(float value)
	{
		___startValue_6 = value;
	}

	inline static int32_t get_offset_of_timeInterpolationStartedAt_7() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t3315389974, ___timeInterpolationStartedAt_7)); }
	inline float get_timeInterpolationStartedAt_7() const { return ___timeInterpolationStartedAt_7; }
	inline float* get_address_of_timeInterpolationStartedAt_7() { return &___timeInterpolationStartedAt_7; }
	inline void set_timeInterpolationStartedAt_7(float value)
	{
		___timeInterpolationStartedAt_7 = value;
	}

	inline static int32_t get_offset_of_firstUpdateFrameSkipped_8() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t3315389974, ___firstUpdateFrameSkipped_8)); }
	inline bool get_firstUpdateFrameSkipped_8() const { return ___firstUpdateFrameSkipped_8; }
	inline bool* get_address_of_firstUpdateFrameSkipped_8() { return &___firstUpdateFrameSkipped_8; }
	inline void set_firstUpdateFrameSkipped_8(bool value)
	{
		___firstUpdateFrameSkipped_8 = value;
	}

	inline static int32_t get_offset_of_skipFirstUpdateFrame_9() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t3315389974, ___skipFirstUpdateFrame_9)); }
	inline bool get_skipFirstUpdateFrame_9() const { return ___skipFirstUpdateFrame_9; }
	inline bool* get_address_of_skipFirstUpdateFrame_9() { return &___skipFirstUpdateFrame_9; }
	inline void set_skipFirstUpdateFrame_9(bool value)
	{
		___skipFirstUpdateFrame_9 = value;
	}

	inline static int32_t get_offset_of_performingInterpolativeSnap_10() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t3315389974, ___performingInterpolativeSnap_10)); }
	inline bool get_performingInterpolativeSnap_10() const { return ___performingInterpolativeSnap_10; }
	inline bool* get_address_of_performingInterpolativeSnap_10() { return &___performingInterpolativeSnap_10; }
	inline void set_performingInterpolativeSnap_10(bool value)
	{
		___performingInterpolativeSnap_10 = value;
	}

	inline static int32_t get_offset_of_duration_11() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t3315389974, ___duration_11)); }
	inline float get_duration_11() const { return ___duration_11; }
	inline float* get_address_of_duration_11() { return &___duration_11; }
	inline void set_duration_11(float value)
	{
		___duration_11 = value;
	}

	inline static int32_t get_offset_of_curve_12() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t3315389974, ___curve_12)); }
	inline AnimationCurve_t3046754366 * get_curve_12() const { return ___curve_12; }
	inline AnimationCurve_t3046754366 ** get_address_of_curve_12() { return &___curve_12; }
	inline void set_curve_12(AnimationCurve_t3046754366 * value)
	{
		___curve_12 = value;
		Il2CppCodeGenWriteBarrier((&___curve_12), value);
	}

	inline static int32_t get_offset_of_U3CIsRunningU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t3315389974, ___U3CIsRunningU3Ek__BackingField_13)); }
	inline bool get_U3CIsRunningU3Ek__BackingField_13() const { return ___U3CIsRunningU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsRunningU3Ek__BackingField_13() { return &___U3CIsRunningU3Ek__BackingField_13; }
	inline void set_U3CIsRunningU3Ek__BackingField_13(bool value)
	{
		___U3CIsRunningU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_value_14() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t3315389974, ___value_14)); }
	inline float get_value_14() const { return ___value_14; }
	inline float* get_address_of_value_14() { return &___value_14; }
	inline void set_value_14(float value)
	{
		___value_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDVALUE_1_T3315389974_H
#ifndef VECTOREXTENSIONS_T3799715090_H
#define VECTOREXTENSIONS_T3799715090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.VectorExtensions
struct  VectorExtensions_t3799715090  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOREXTENSIONS_T3799715090_H
#ifndef EXTENSIONS_T1944519571_H
#define EXTENSIONS_T1944519571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Extensions
struct  Extensions_t1944519571  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONS_T1944519571_H
#ifndef MATHEXTENSIONS_T3477755654_H
#define MATHEXTENSIONS_T3477755654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MathExtensions
struct  MathExtensions_t3477755654  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHEXTENSIONS_T3477755654_H
#ifndef EVENTSYSTEMEXTENSIONS_T171429629_H
#define EVENTSYSTEMEXTENSIONS_T171429629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.EventSystemExtensions
struct  EventSystemExtensions_t171429629  : public RuntimeObject
{
public:

public:
};

struct EventSystemExtensions_t171429629_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> HoloToolkit.Unity.EventSystemExtensions::RaycastResults
	List_1_t537414295 * ___RaycastResults_0;
	// HoloToolkit.Unity.RaycastResultComparer HoloToolkit.Unity.EventSystemExtensions::RaycastResultComparer
	RaycastResultComparer_t838146858 * ___RaycastResultComparer_1;

public:
	inline static int32_t get_offset_of_RaycastResults_0() { return static_cast<int32_t>(offsetof(EventSystemExtensions_t171429629_StaticFields, ___RaycastResults_0)); }
	inline List_1_t537414295 * get_RaycastResults_0() const { return ___RaycastResults_0; }
	inline List_1_t537414295 ** get_address_of_RaycastResults_0() { return &___RaycastResults_0; }
	inline void set_RaycastResults_0(List_1_t537414295 * value)
	{
		___RaycastResults_0 = value;
		Il2CppCodeGenWriteBarrier((&___RaycastResults_0), value);
	}

	inline static int32_t get_offset_of_RaycastResultComparer_1() { return static_cast<int32_t>(offsetof(EventSystemExtensions_t171429629_StaticFields, ___RaycastResultComparer_1)); }
	inline RaycastResultComparer_t838146858 * get_RaycastResultComparer_1() const { return ___RaycastResultComparer_1; }
	inline RaycastResultComparer_t838146858 ** get_address_of_RaycastResultComparer_1() { return &___RaycastResultComparer_1; }
	inline void set_RaycastResultComparer_1(RaycastResultComparer_t838146858 * value)
	{
		___RaycastResultComparer_1 = value;
		Il2CppCodeGenWriteBarrier((&___RaycastResultComparer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEMEXTENSIONS_T171429629_H
#ifndef TRANSFORMEXTENSIONS_T3217287941_H
#define TRANSFORMEXTENSIONS_T3217287941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TransformExtensions
struct  TransformExtensions_t3217287941  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMEXTENSIONS_T3217287941_H
#ifndef LAYEREXTENSIONS_T2226445283_H
#define LAYEREXTENSIONS_T2226445283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.LayerExtensions
struct  LayerExtensions_t2226445283  : public RuntimeObject
{
public:

public:
};

struct LayerExtensions_t2226445283_StaticFields
{
public:
	// System.Int32 HoloToolkit.Unity.LayerExtensions::defaultLayer
	int32_t ___defaultLayer_1;
	// System.Int32 HoloToolkit.Unity.LayerExtensions::surfaceLayer
	int32_t ___surfaceLayer_2;
	// System.Int32 HoloToolkit.Unity.LayerExtensions::interactionLayer
	int32_t ___interactionLayer_3;
	// System.Int32 HoloToolkit.Unity.LayerExtensions::activationLayer
	int32_t ___activationLayer_4;

public:
	inline static int32_t get_offset_of_defaultLayer_1() { return static_cast<int32_t>(offsetof(LayerExtensions_t2226445283_StaticFields, ___defaultLayer_1)); }
	inline int32_t get_defaultLayer_1() const { return ___defaultLayer_1; }
	inline int32_t* get_address_of_defaultLayer_1() { return &___defaultLayer_1; }
	inline void set_defaultLayer_1(int32_t value)
	{
		___defaultLayer_1 = value;
	}

	inline static int32_t get_offset_of_surfaceLayer_2() { return static_cast<int32_t>(offsetof(LayerExtensions_t2226445283_StaticFields, ___surfaceLayer_2)); }
	inline int32_t get_surfaceLayer_2() const { return ___surfaceLayer_2; }
	inline int32_t* get_address_of_surfaceLayer_2() { return &___surfaceLayer_2; }
	inline void set_surfaceLayer_2(int32_t value)
	{
		___surfaceLayer_2 = value;
	}

	inline static int32_t get_offset_of_interactionLayer_3() { return static_cast<int32_t>(offsetof(LayerExtensions_t2226445283_StaticFields, ___interactionLayer_3)); }
	inline int32_t get_interactionLayer_3() const { return ___interactionLayer_3; }
	inline int32_t* get_address_of_interactionLayer_3() { return &___interactionLayer_3; }
	inline void set_interactionLayer_3(int32_t value)
	{
		___interactionLayer_3 = value;
	}

	inline static int32_t get_offset_of_activationLayer_4() { return static_cast<int32_t>(offsetof(LayerExtensions_t2226445283_StaticFields, ___activationLayer_4)); }
	inline int32_t get_activationLayer_4() const { return ___activationLayer_4; }
	inline int32_t* get_address_of_activationLayer_4() { return &___activationLayer_4; }
	inline void set_activationLayer_4(int32_t value)
	{
		___activationLayer_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYEREXTENSIONS_T2226445283_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef GAMEOBJECTEXTENSIONS_T3801208853_H
#define GAMEOBJECTEXTENSIONS_T3801208853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.GameObjectExtensions
struct  GameObjectExtensions_t3801208853  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTEXTENSIONS_T3801208853_H
#ifndef INTERACTIONSOURCEEXTENSIONS_T135024349_H
#define INTERACTIONSOURCEEXTENSIONS_T135024349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InteractionSourceExtensions
struct  InteractionSourceExtensions_t135024349  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSOURCEEXTENSIONS_T135024349_H
#ifndef CAMERAEXTENSIONS_T967686036_H
#define CAMERAEXTENSIONS_T967686036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.CameraExtensions
struct  CameraExtensions_t967686036  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEXTENSIONS_T967686036_H
#ifndef ACTIONEXTENSIONS_T643829156_H
#define ACTIONEXTENSIONS_T643829156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ActionExtensions
struct  ActionExtensions_t643829156  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONEXTENSIONS_T643829156_H
#ifndef U3CENUMERATEHIERARCHYCOREU3ED__4_T2426800674_H
#define U3CENUMERATEHIERARCHYCOREU3ED__4_T2426800674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TransformExtensions/<EnumerateHierarchyCore>d__4
struct  U3CEnumerateHierarchyCoreU3Ed__4_t2426800674  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.TransformExtensions/<EnumerateHierarchyCore>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Transform HoloToolkit.Unity.TransformExtensions/<EnumerateHierarchyCore>d__4::<>2__current
	Transform_t3600365921 * ___U3CU3E2__current_1;
	// System.Int32 HoloToolkit.Unity.TransformExtensions/<EnumerateHierarchyCore>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Transform HoloToolkit.Unity.TransformExtensions/<EnumerateHierarchyCore>d__4::root
	Transform_t3600365921 * ___root_3;
	// UnityEngine.Transform HoloToolkit.Unity.TransformExtensions/<EnumerateHierarchyCore>d__4::<>3__root
	Transform_t3600365921 * ___U3CU3E3__root_4;
	// System.Collections.Generic.ICollection`1<UnityEngine.Transform> HoloToolkit.Unity.TransformExtensions/<EnumerateHierarchyCore>d__4::ignore
	RuntimeObject* ___ignore_5;
	// System.Collections.Generic.ICollection`1<UnityEngine.Transform> HoloToolkit.Unity.TransformExtensions/<EnumerateHierarchyCore>d__4::<>3__ignore
	RuntimeObject* ___U3CU3E3__ignore_6;
	// System.Collections.Generic.Queue`1<UnityEngine.Transform> HoloToolkit.Unity.TransformExtensions/<EnumerateHierarchyCore>d__4::<transformQueue>5__1
	Queue_1_t3446625415 * ___U3CtransformQueueU3E5__1_7;
	// UnityEngine.Transform HoloToolkit.Unity.TransformExtensions/<EnumerateHierarchyCore>d__4::<parentTransform>5__2
	Transform_t3600365921 * ___U3CparentTransformU3E5__2_8;
	// System.Int32 HoloToolkit.Unity.TransformExtensions/<EnumerateHierarchyCore>d__4::<i>5__3
	int32_t ___U3CiU3E5__3_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t2426800674, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t2426800674, ___U3CU3E2__current_1)); }
	inline Transform_t3600365921 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Transform_t3600365921 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Transform_t3600365921 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t2426800674, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_root_3() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t2426800674, ___root_3)); }
	inline Transform_t3600365921 * get_root_3() const { return ___root_3; }
	inline Transform_t3600365921 ** get_address_of_root_3() { return &___root_3; }
	inline void set_root_3(Transform_t3600365921 * value)
	{
		___root_3 = value;
		Il2CppCodeGenWriteBarrier((&___root_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__root_4() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t2426800674, ___U3CU3E3__root_4)); }
	inline Transform_t3600365921 * get_U3CU3E3__root_4() const { return ___U3CU3E3__root_4; }
	inline Transform_t3600365921 ** get_address_of_U3CU3E3__root_4() { return &___U3CU3E3__root_4; }
	inline void set_U3CU3E3__root_4(Transform_t3600365921 * value)
	{
		___U3CU3E3__root_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__root_4), value);
	}

	inline static int32_t get_offset_of_ignore_5() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t2426800674, ___ignore_5)); }
	inline RuntimeObject* get_ignore_5() const { return ___ignore_5; }
	inline RuntimeObject** get_address_of_ignore_5() { return &___ignore_5; }
	inline void set_ignore_5(RuntimeObject* value)
	{
		___ignore_5 = value;
		Il2CppCodeGenWriteBarrier((&___ignore_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__ignore_6() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t2426800674, ___U3CU3E3__ignore_6)); }
	inline RuntimeObject* get_U3CU3E3__ignore_6() const { return ___U3CU3E3__ignore_6; }
	inline RuntimeObject** get_address_of_U3CU3E3__ignore_6() { return &___U3CU3E3__ignore_6; }
	inline void set_U3CU3E3__ignore_6(RuntimeObject* value)
	{
		___U3CU3E3__ignore_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__ignore_6), value);
	}

	inline static int32_t get_offset_of_U3CtransformQueueU3E5__1_7() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t2426800674, ___U3CtransformQueueU3E5__1_7)); }
	inline Queue_1_t3446625415 * get_U3CtransformQueueU3E5__1_7() const { return ___U3CtransformQueueU3E5__1_7; }
	inline Queue_1_t3446625415 ** get_address_of_U3CtransformQueueU3E5__1_7() { return &___U3CtransformQueueU3E5__1_7; }
	inline void set_U3CtransformQueueU3E5__1_7(Queue_1_t3446625415 * value)
	{
		___U3CtransformQueueU3E5__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransformQueueU3E5__1_7), value);
	}

	inline static int32_t get_offset_of_U3CparentTransformU3E5__2_8() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t2426800674, ___U3CparentTransformU3E5__2_8)); }
	inline Transform_t3600365921 * get_U3CparentTransformU3E5__2_8() const { return ___U3CparentTransformU3E5__2_8; }
	inline Transform_t3600365921 ** get_address_of_U3CparentTransformU3E5__2_8() { return &___U3CparentTransformU3E5__2_8; }
	inline void set_U3CparentTransformU3E5__2_8(Transform_t3600365921 * value)
	{
		___U3CparentTransformU3E5__2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparentTransformU3E5__2_8), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_9() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t2426800674, ___U3CiU3E5__3_9)); }
	inline int32_t get_U3CiU3E5__3_9() const { return ___U3CiU3E5__3_9; }
	inline int32_t* get_address_of_U3CiU3E5__3_9() { return &___U3CiU3E5__3_9; }
	inline void set_U3CiU3E5__3_9(int32_t value)
	{
		___U3CiU3E5__3_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENUMERATEHIERARCHYCOREU3ED__4_T2426800674_H
#ifndef COLOR32EXTENSIONS_T1528800064_H
#define COLOR32EXTENSIONS_T1528800064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Color32Extensions
struct  Color32Extensions_t1528800064  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32EXTENSIONS_T1528800064_H
#ifndef ENUMERABLEEXTENSIONS_T3128455254_H
#define ENUMERABLEEXTENSIONS_T3128455254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.EnumerableExtensions
struct  EnumerableExtensions_t3128455254  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERABLEEXTENSIONS_T3128455254_H
#ifndef U3CENUMERATEANCESTORSU3ED__6_T622793014_H
#define U3CENUMERATEANCESTORSU3ED__6_T622793014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ComponentExtensions/<EnumerateAncestors>d__6
struct  U3CEnumerateAncestorsU3Ed__6_t622793014  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.ComponentExtensions/<EnumerateAncestors>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Transform HoloToolkit.Unity.ComponentExtensions/<EnumerateAncestors>d__6::<>2__current
	Transform_t3600365921 * ___U3CU3E2__current_1;
	// System.Int32 HoloToolkit.Unity.ComponentExtensions/<EnumerateAncestors>d__6::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Transform HoloToolkit.Unity.ComponentExtensions/<EnumerateAncestors>d__6::startTransform
	Transform_t3600365921 * ___startTransform_3;
	// UnityEngine.Transform HoloToolkit.Unity.ComponentExtensions/<EnumerateAncestors>d__6::<>3__startTransform
	Transform_t3600365921 * ___U3CU3E3__startTransform_4;
	// System.Boolean HoloToolkit.Unity.ComponentExtensions/<EnumerateAncestors>d__6::includeSelf
	bool ___includeSelf_5;
	// System.Boolean HoloToolkit.Unity.ComponentExtensions/<EnumerateAncestors>d__6::<>3__includeSelf
	bool ___U3CU3E3__includeSelf_6;
	// UnityEngine.Transform HoloToolkit.Unity.ComponentExtensions/<EnumerateAncestors>d__6::<transform>5__1
	Transform_t3600365921 * ___U3CtransformU3E5__1_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_t622793014, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_t622793014, ___U3CU3E2__current_1)); }
	inline Transform_t3600365921 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Transform_t3600365921 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Transform_t3600365921 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_t622793014, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_startTransform_3() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_t622793014, ___startTransform_3)); }
	inline Transform_t3600365921 * get_startTransform_3() const { return ___startTransform_3; }
	inline Transform_t3600365921 ** get_address_of_startTransform_3() { return &___startTransform_3; }
	inline void set_startTransform_3(Transform_t3600365921 * value)
	{
		___startTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___startTransform_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__startTransform_4() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_t622793014, ___U3CU3E3__startTransform_4)); }
	inline Transform_t3600365921 * get_U3CU3E3__startTransform_4() const { return ___U3CU3E3__startTransform_4; }
	inline Transform_t3600365921 ** get_address_of_U3CU3E3__startTransform_4() { return &___U3CU3E3__startTransform_4; }
	inline void set_U3CU3E3__startTransform_4(Transform_t3600365921 * value)
	{
		___U3CU3E3__startTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__startTransform_4), value);
	}

	inline static int32_t get_offset_of_includeSelf_5() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_t622793014, ___includeSelf_5)); }
	inline bool get_includeSelf_5() const { return ___includeSelf_5; }
	inline bool* get_address_of_includeSelf_5() { return &___includeSelf_5; }
	inline void set_includeSelf_5(bool value)
	{
		___includeSelf_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__includeSelf_6() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_t622793014, ___U3CU3E3__includeSelf_6)); }
	inline bool get_U3CU3E3__includeSelf_6() const { return ___U3CU3E3__includeSelf_6; }
	inline bool* get_address_of_U3CU3E3__includeSelf_6() { return &___U3CU3E3__includeSelf_6; }
	inline void set_U3CU3E3__includeSelf_6(bool value)
	{
		___U3CU3E3__includeSelf_6 = value;
	}

	inline static int32_t get_offset_of_U3CtransformU3E5__1_7() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_t622793014, ___U3CtransformU3E5__1_7)); }
	inline Transform_t3600365921 * get_U3CtransformU3E5__1_7() const { return ___U3CtransformU3E5__1_7; }
	inline Transform_t3600365921 ** get_address_of_U3CtransformU3E5__1_7() { return &___U3CtransformU3E5__1_7; }
	inline void set_U3CtransformU3E5__1_7(Transform_t3600365921 * value)
	{
		___U3CtransformU3E5__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransformU3E5__1_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENUMERATEANCESTORSU3ED__6_T622793014_H
#ifndef COMPONENTEXTENSIONS_T1008225482_H
#define COMPONENTEXTENSIONS_T1008225482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ComponentExtensions
struct  ComponentExtensions_t1008225482  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTEXTENSIONS_T1008225482_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t385246372* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t385246372* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_31)); }
	inline DateTime_t3738529785  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t3738529785 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t3738529785  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_32)); }
	inline DateTime_t3738529785  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t3738529785  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef TASKAWAITER_T919683548_H
#define TASKAWAITER_T919683548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter
struct  TaskAwaiter_t919683548 
{
public:
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.TaskAwaiter::m_task
	Task_t3187275312 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_t919683548, ___m_task_0)); }
	inline Task_t3187275312 * get_m_task_0() const { return ___m_task_0; }
	inline Task_t3187275312 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_t3187275312 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t919683548_marshaled_pinvoke
{
	Task_t3187275312 * ___m_task_0;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t919683548_marshaled_com
{
	Task_t3187275312 * ___m_task_0;
};
#endif // TASKAWAITER_T919683548_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INTERPOLATEDFLOAT_T3082949779_H
#define INTERPOLATEDFLOAT_T3082949779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedFloat
struct  InterpolatedFloat_t3082949779  : public InterpolatedValue_1_t3315389974
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDFLOAT_T3082949779_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef ENUMERATOR_T3437831517_H
#define ENUMERATOR_T3437831517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector3,HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData>
struct  Enumerator_t3437831517 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::dictionary
	Dictionary_2_t2868937860 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::version
	int32_t ___version_2;
	// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::currentValue
	MeshData_t2646720140 * ___currentValue_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3437831517, ___dictionary_0)); }
	inline Dictionary_2_t2868937860 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2868937860 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2868937860 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t3437831517, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t3437831517, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_currentValue_3() { return static_cast<int32_t>(offsetof(Enumerator_t3437831517, ___currentValue_3)); }
	inline MeshData_t2646720140 * get_currentValue_3() const { return ___currentValue_3; }
	inline MeshData_t2646720140 ** get_address_of_currentValue_3() { return &___currentValue_3; }
	inline void set_currentValue_3(MeshData_t2646720140 * value)
	{
		___currentValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentValue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3437831517_H
#ifndef INT3_T3771461231_H
#define INT3_T3771461231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Int3
#pragma pack(push, tp, 4)
struct  Int3_t3771461231 
{
public:
	// System.Int32 HoloToolkit.Unity.Int3::x
	int32_t ___x_8;
	// System.Int32 HoloToolkit.Unity.Int3::y
	int32_t ___y_9;
	// System.Int32 HoloToolkit.Unity.Int3::z
	int32_t ___z_10;

public:
	inline static int32_t get_offset_of_x_8() { return static_cast<int32_t>(offsetof(Int3_t3771461231, ___x_8)); }
	inline int32_t get_x_8() const { return ___x_8; }
	inline int32_t* get_address_of_x_8() { return &___x_8; }
	inline void set_x_8(int32_t value)
	{
		___x_8 = value;
	}

	inline static int32_t get_offset_of_y_9() { return static_cast<int32_t>(offsetof(Int3_t3771461231, ___y_9)); }
	inline int32_t get_y_9() const { return ___y_9; }
	inline int32_t* get_address_of_y_9() { return &___y_9; }
	inline void set_y_9(int32_t value)
	{
		___y_9 = value;
	}

	inline static int32_t get_offset_of_z_10() { return static_cast<int32_t>(offsetof(Int3_t3771461231, ___z_10)); }
	inline int32_t get_z_10() const { return ___z_10; }
	inline int32_t* get_address_of_z_10() { return &___z_10; }
	inline void set_z_10(int32_t value)
	{
		___z_10 = value;
	}
};
#pragma pack(pop, tp)

struct Int3_t3771461231_StaticFields
{
public:
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::zero
	Int3_t3771461231  ___zero_0;
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::one
	Int3_t3771461231  ___one_1;
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::forward
	Int3_t3771461231  ___forward_2;
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::back
	Int3_t3771461231  ___back_3;
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::up
	Int3_t3771461231  ___up_4;
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::down
	Int3_t3771461231  ___down_5;
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::left
	Int3_t3771461231  ___left_6;
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::right
	Int3_t3771461231  ___right_7;

public:
	inline static int32_t get_offset_of_zero_0() { return static_cast<int32_t>(offsetof(Int3_t3771461231_StaticFields, ___zero_0)); }
	inline Int3_t3771461231  get_zero_0() const { return ___zero_0; }
	inline Int3_t3771461231 * get_address_of_zero_0() { return &___zero_0; }
	inline void set_zero_0(Int3_t3771461231  value)
	{
		___zero_0 = value;
	}

	inline static int32_t get_offset_of_one_1() { return static_cast<int32_t>(offsetof(Int3_t3771461231_StaticFields, ___one_1)); }
	inline Int3_t3771461231  get_one_1() const { return ___one_1; }
	inline Int3_t3771461231 * get_address_of_one_1() { return &___one_1; }
	inline void set_one_1(Int3_t3771461231  value)
	{
		___one_1 = value;
	}

	inline static int32_t get_offset_of_forward_2() { return static_cast<int32_t>(offsetof(Int3_t3771461231_StaticFields, ___forward_2)); }
	inline Int3_t3771461231  get_forward_2() const { return ___forward_2; }
	inline Int3_t3771461231 * get_address_of_forward_2() { return &___forward_2; }
	inline void set_forward_2(Int3_t3771461231  value)
	{
		___forward_2 = value;
	}

	inline static int32_t get_offset_of_back_3() { return static_cast<int32_t>(offsetof(Int3_t3771461231_StaticFields, ___back_3)); }
	inline Int3_t3771461231  get_back_3() const { return ___back_3; }
	inline Int3_t3771461231 * get_address_of_back_3() { return &___back_3; }
	inline void set_back_3(Int3_t3771461231  value)
	{
		___back_3 = value;
	}

	inline static int32_t get_offset_of_up_4() { return static_cast<int32_t>(offsetof(Int3_t3771461231_StaticFields, ___up_4)); }
	inline Int3_t3771461231  get_up_4() const { return ___up_4; }
	inline Int3_t3771461231 * get_address_of_up_4() { return &___up_4; }
	inline void set_up_4(Int3_t3771461231  value)
	{
		___up_4 = value;
	}

	inline static int32_t get_offset_of_down_5() { return static_cast<int32_t>(offsetof(Int3_t3771461231_StaticFields, ___down_5)); }
	inline Int3_t3771461231  get_down_5() const { return ___down_5; }
	inline Int3_t3771461231 * get_address_of_down_5() { return &___down_5; }
	inline void set_down_5(Int3_t3771461231  value)
	{
		___down_5 = value;
	}

	inline static int32_t get_offset_of_left_6() { return static_cast<int32_t>(offsetof(Int3_t3771461231_StaticFields, ___left_6)); }
	inline Int3_t3771461231  get_left_6() const { return ___left_6; }
	inline Int3_t3771461231 * get_address_of_left_6() { return &___left_6; }
	inline void set_left_6(Int3_t3771461231  value)
	{
		___left_6 = value;
	}

	inline static int32_t get_offset_of_right_7() { return static_cast<int32_t>(offsetof(Int3_t3771461231_StaticFields, ___right_7)); }
	inline Int3_t3771461231  get_right_7() const { return ___right_7; }
	inline Int3_t3771461231 * get_address_of_right_7() { return &___right_7; }
	inline void set_right_7(Int3_t3771461231  value)
	{
		___right_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT3_T3771461231_H
#ifndef TASKAWAITER_1_T2654828731_H
#define TASKAWAITER_1_T2654828731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter`1<HoloToolkit.Unity.MicrophoneStatus>
struct  TaskAwaiter_1_t2654828731 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.TaskAwaiter`1::m_task
	Task_1_t1265886475 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_1_t2654828731, ___m_task_0)); }
	inline Task_1_t1265886475 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t1265886475 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t1265886475 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKAWAITER_1_T2654828731_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef ASYNCMETHODBUILDERCORE_T2955600131_H
#define ASYNCMETHODBUILDERCORE_T2955600131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t2955600131 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t1264377477 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2955600131, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_stateMachine_0), value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2955600131, ___m_defaultContextAction_1)); }
	inline Action_t1264377477 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t1264377477 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t1264377477 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultContextAction_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2955600131_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2955600131_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
#endif // ASYNCMETHODBUILDERCORE_T2955600131_H
#ifndef SURFACETYPES_T6986610_H
#define SURFACETYPES_T6986610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/RaycastResult/SurfaceTypes
struct  SurfaceTypes_t6986610 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/RaycastResult/SurfaceTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SurfaceTypes_t6986610, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACETYPES_T6986610_H
#ifndef PLACEMENTTYPE_T1028595510_H
#define PLACEMENTTYPE_T1028595510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementDefinition/PlacementType
struct  PlacementType_t1028595510 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementDefinition/PlacementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlacementType_t1028595510, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEMENTTYPE_T1028595510_H
#ifndef INTERACTIONSOURCEFLAGS_T1798650303_H
#define INTERACTIONSOURCEFLAGS_T1798650303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.WSA.Input.InteractionSourceFlags
struct  InteractionSourceFlags_t1798650303 
{
public:
	// System.Int32 UnityEngine.XR.WSA.Input.InteractionSourceFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InteractionSourceFlags_t1798650303, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSOURCEFLAGS_T1798650303_H
#ifndef PLAYSPACEALIGNMENT_T492554602_H
#define PLAYSPACEALIGNMENT_T492554602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceAlignment
#pragma pack(push, tp, 1)
struct  PlayspaceAlignment_t492554602  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceAlignment::Center
	Vector3_t3722313464  ___Center_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceAlignment::HalfDims
	Vector3_t3722313464  ___HalfDims_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceAlignment::BasisX
	Vector3_t3722313464  ___BasisX_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceAlignment::BasisY
	Vector3_t3722313464  ___BasisY_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceAlignment::BasisZ
	Vector3_t3722313464  ___BasisZ_4;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceAlignment::FloorYValue
	float ___FloorYValue_5;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceAlignment::CeilingYValue
	float ___CeilingYValue_6;

public:
	inline static int32_t get_offset_of_Center_0() { return static_cast<int32_t>(offsetof(PlayspaceAlignment_t492554602, ___Center_0)); }
	inline Vector3_t3722313464  get_Center_0() const { return ___Center_0; }
	inline Vector3_t3722313464 * get_address_of_Center_0() { return &___Center_0; }
	inline void set_Center_0(Vector3_t3722313464  value)
	{
		___Center_0 = value;
	}

	inline static int32_t get_offset_of_HalfDims_1() { return static_cast<int32_t>(offsetof(PlayspaceAlignment_t492554602, ___HalfDims_1)); }
	inline Vector3_t3722313464  get_HalfDims_1() const { return ___HalfDims_1; }
	inline Vector3_t3722313464 * get_address_of_HalfDims_1() { return &___HalfDims_1; }
	inline void set_HalfDims_1(Vector3_t3722313464  value)
	{
		___HalfDims_1 = value;
	}

	inline static int32_t get_offset_of_BasisX_2() { return static_cast<int32_t>(offsetof(PlayspaceAlignment_t492554602, ___BasisX_2)); }
	inline Vector3_t3722313464  get_BasisX_2() const { return ___BasisX_2; }
	inline Vector3_t3722313464 * get_address_of_BasisX_2() { return &___BasisX_2; }
	inline void set_BasisX_2(Vector3_t3722313464  value)
	{
		___BasisX_2 = value;
	}

	inline static int32_t get_offset_of_BasisY_3() { return static_cast<int32_t>(offsetof(PlayspaceAlignment_t492554602, ___BasisY_3)); }
	inline Vector3_t3722313464  get_BasisY_3() const { return ___BasisY_3; }
	inline Vector3_t3722313464 * get_address_of_BasisY_3() { return &___BasisY_3; }
	inline void set_BasisY_3(Vector3_t3722313464  value)
	{
		___BasisY_3 = value;
	}

	inline static int32_t get_offset_of_BasisZ_4() { return static_cast<int32_t>(offsetof(PlayspaceAlignment_t492554602, ___BasisZ_4)); }
	inline Vector3_t3722313464  get_BasisZ_4() const { return ___BasisZ_4; }
	inline Vector3_t3722313464 * get_address_of_BasisZ_4() { return &___BasisZ_4; }
	inline void set_BasisZ_4(Vector3_t3722313464  value)
	{
		___BasisZ_4 = value;
	}

	inline static int32_t get_offset_of_FloorYValue_5() { return static_cast<int32_t>(offsetof(PlayspaceAlignment_t492554602, ___FloorYValue_5)); }
	inline float get_FloorYValue_5() const { return ___FloorYValue_5; }
	inline float* get_address_of_FloorYValue_5() { return &___FloorYValue_5; }
	inline void set_FloorYValue_5(float value)
	{
		___FloorYValue_5 = value;
	}

	inline static int32_t get_offset_of_CeilingYValue_6() { return static_cast<int32_t>(offsetof(PlayspaceAlignment_t492554602, ___CeilingYValue_6)); }
	inline float get_CeilingYValue_6() const { return ___CeilingYValue_6; }
	inline float* get_address_of_CeilingYValue_6() { return &___CeilingYValue_6; }
	inline void set_CeilingYValue_6(float value)
	{
		___CeilingYValue_6 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceAlignment
#pragma pack(push, tp, 1)
struct PlayspaceAlignment_t492554602_marshaled_pinvoke
{
	Vector3_t3722313464  ___Center_0;
	Vector3_t3722313464  ___HalfDims_1;
	Vector3_t3722313464  ___BasisX_2;
	Vector3_t3722313464  ___BasisY_3;
	Vector3_t3722313464  ___BasisZ_4;
	float ___FloorYValue_5;
	float ___CeilingYValue_6;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceAlignment
#pragma pack(push, tp, 1)
struct PlayspaceAlignment_t492554602_marshaled_com
{
	Vector3_t3722313464  ___Center_0;
	Vector3_t3722313464  ___HalfDims_1;
	Vector3_t3722313464  ___BasisX_2;
	Vector3_t3722313464  ___BasisY_3;
	Vector3_t3722313464  ___BasisZ_4;
	float ___FloorYValue_5;
	float ___CeilingYValue_6;
};
#pragma pack(pop, tp)
#endif // PLAYSPACEALIGNMENT_T492554602_H
#ifndef MESHDATA_T3857626232_H
#define MESHDATA_T3857626232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData
#pragma pack(push, tp, 1)
struct  MeshData_t3857626232 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::meshID
	int32_t ___meshID_0;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::lastUpdateID
	int32_t ___lastUpdateID_1;
	// UnityEngine.Matrix4x4 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::transform
	Matrix4x4_t1817901843  ___transform_2;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::vertCount
	int32_t ___vertCount_3;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::indexCount
	int32_t ___indexCount_4;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::verts
	intptr_t ___verts_5;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::normals
	intptr_t ___normals_6;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData::indices
	intptr_t ___indices_7;

public:
	inline static int32_t get_offset_of_meshID_0() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___meshID_0)); }
	inline int32_t get_meshID_0() const { return ___meshID_0; }
	inline int32_t* get_address_of_meshID_0() { return &___meshID_0; }
	inline void set_meshID_0(int32_t value)
	{
		___meshID_0 = value;
	}

	inline static int32_t get_offset_of_lastUpdateID_1() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___lastUpdateID_1)); }
	inline int32_t get_lastUpdateID_1() const { return ___lastUpdateID_1; }
	inline int32_t* get_address_of_lastUpdateID_1() { return &___lastUpdateID_1; }
	inline void set_lastUpdateID_1(int32_t value)
	{
		___lastUpdateID_1 = value;
	}

	inline static int32_t get_offset_of_transform_2() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___transform_2)); }
	inline Matrix4x4_t1817901843  get_transform_2() const { return ___transform_2; }
	inline Matrix4x4_t1817901843 * get_address_of_transform_2() { return &___transform_2; }
	inline void set_transform_2(Matrix4x4_t1817901843  value)
	{
		___transform_2 = value;
	}

	inline static int32_t get_offset_of_vertCount_3() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___vertCount_3)); }
	inline int32_t get_vertCount_3() const { return ___vertCount_3; }
	inline int32_t* get_address_of_vertCount_3() { return &___vertCount_3; }
	inline void set_vertCount_3(int32_t value)
	{
		___vertCount_3 = value;
	}

	inline static int32_t get_offset_of_indexCount_4() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___indexCount_4)); }
	inline int32_t get_indexCount_4() const { return ___indexCount_4; }
	inline int32_t* get_address_of_indexCount_4() { return &___indexCount_4; }
	inline void set_indexCount_4(int32_t value)
	{
		___indexCount_4 = value;
	}

	inline static int32_t get_offset_of_verts_5() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___verts_5)); }
	inline intptr_t get_verts_5() const { return ___verts_5; }
	inline intptr_t* get_address_of_verts_5() { return &___verts_5; }
	inline void set_verts_5(intptr_t value)
	{
		___verts_5 = value;
	}

	inline static int32_t get_offset_of_normals_6() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___normals_6)); }
	inline intptr_t get_normals_6() const { return ___normals_6; }
	inline intptr_t* get_address_of_normals_6() { return &___normals_6; }
	inline void set_normals_6(intptr_t value)
	{
		___normals_6 = value;
	}

	inline static int32_t get_offset_of_indices_7() { return static_cast<int32_t>(offsetof(MeshData_t3857626232, ___indices_7)); }
	inline intptr_t get_indices_7() const { return ___indices_7; }
	inline intptr_t* get_address_of_indices_7() { return &___indices_7; }
	inline void set_indices_7(intptr_t value)
	{
		___indices_7 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHDATA_T3857626232_H
#ifndef MESHDATA_T3361835602_H
#define MESHDATA_T3361835602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll/MeshData
struct  MeshData_t3361835602 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/MeshData::MeshID
	int32_t ___MeshID_0;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll/MeshData::LastUpdateID
	int32_t ___LastUpdateID_1;
	// UnityEngine.Matrix4x4 HoloToolkit.Unity.SpatialUnderstandingDll/MeshData::Transform
	Matrix4x4_t1817901843  ___Transform_2;
	// UnityEngine.Vector3[] HoloToolkit.Unity.SpatialUnderstandingDll/MeshData::Verts
	Vector3U5BU5D_t1718750761* ___Verts_3;
	// UnityEngine.Vector3[] HoloToolkit.Unity.SpatialUnderstandingDll/MeshData::Normals
	Vector3U5BU5D_t1718750761* ___Normals_4;
	// System.Int32[] HoloToolkit.Unity.SpatialUnderstandingDll/MeshData::Indices
	Int32U5BU5D_t385246372* ___Indices_5;

public:
	inline static int32_t get_offset_of_MeshID_0() { return static_cast<int32_t>(offsetof(MeshData_t3361835602, ___MeshID_0)); }
	inline int32_t get_MeshID_0() const { return ___MeshID_0; }
	inline int32_t* get_address_of_MeshID_0() { return &___MeshID_0; }
	inline void set_MeshID_0(int32_t value)
	{
		___MeshID_0 = value;
	}

	inline static int32_t get_offset_of_LastUpdateID_1() { return static_cast<int32_t>(offsetof(MeshData_t3361835602, ___LastUpdateID_1)); }
	inline int32_t get_LastUpdateID_1() const { return ___LastUpdateID_1; }
	inline int32_t* get_address_of_LastUpdateID_1() { return &___LastUpdateID_1; }
	inline void set_LastUpdateID_1(int32_t value)
	{
		___LastUpdateID_1 = value;
	}

	inline static int32_t get_offset_of_Transform_2() { return static_cast<int32_t>(offsetof(MeshData_t3361835602, ___Transform_2)); }
	inline Matrix4x4_t1817901843  get_Transform_2() const { return ___Transform_2; }
	inline Matrix4x4_t1817901843 * get_address_of_Transform_2() { return &___Transform_2; }
	inline void set_Transform_2(Matrix4x4_t1817901843  value)
	{
		___Transform_2 = value;
	}

	inline static int32_t get_offset_of_Verts_3() { return static_cast<int32_t>(offsetof(MeshData_t3361835602, ___Verts_3)); }
	inline Vector3U5BU5D_t1718750761* get_Verts_3() const { return ___Verts_3; }
	inline Vector3U5BU5D_t1718750761** get_address_of_Verts_3() { return &___Verts_3; }
	inline void set_Verts_3(Vector3U5BU5D_t1718750761* value)
	{
		___Verts_3 = value;
		Il2CppCodeGenWriteBarrier((&___Verts_3), value);
	}

	inline static int32_t get_offset_of_Normals_4() { return static_cast<int32_t>(offsetof(MeshData_t3361835602, ___Normals_4)); }
	inline Vector3U5BU5D_t1718750761* get_Normals_4() const { return ___Normals_4; }
	inline Vector3U5BU5D_t1718750761** get_address_of_Normals_4() { return &___Normals_4; }
	inline void set_Normals_4(Vector3U5BU5D_t1718750761* value)
	{
		___Normals_4 = value;
		Il2CppCodeGenWriteBarrier((&___Normals_4), value);
	}

	inline static int32_t get_offset_of_Indices_5() { return static_cast<int32_t>(offsetof(MeshData_t3361835602, ___Indices_5)); }
	inline Int32U5BU5D_t385246372* get_Indices_5() const { return ___Indices_5; }
	inline Int32U5BU5D_t385246372** get_address_of_Indices_5() { return &___Indices_5; }
	inline void set_Indices_5(Int32U5BU5D_t385246372* value)
	{
		___Indices_5 = value;
		Il2CppCodeGenWriteBarrier((&___Indices_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/MeshData
struct MeshData_t3361835602_marshaled_pinvoke
{
	int32_t ___MeshID_0;
	int32_t ___LastUpdateID_1;
	Matrix4x4_t1817901843  ___Transform_2;
	Vector3_t3722313464 * ___Verts_3;
	Vector3_t3722313464 * ___Normals_4;
	int32_t* ___Indices_5;
};
// Native definition for COM marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/MeshData
struct MeshData_t3361835602_marshaled_com
{
	int32_t ___MeshID_0;
	int32_t ___LastUpdateID_1;
	Matrix4x4_t1817901843  ___Transform_2;
	Vector3_t3722313464 * ___Verts_3;
	Vector3_t3722313464 * ___Normals_4;
	int32_t* ___Indices_5;
};
#endif // MESHDATA_T3361835602_H
#ifndef ASYNCTASKMETHODBUILDER_1_T2181320810_H
#define ASYNCTASKMETHODBUILDER_1_T2181320810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<HoloToolkit.Unity.MicrophoneStatus>
struct  AsyncTaskMethodBuilder_1_t2181320810 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2955600131  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t1265886475 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2181320810, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2955600131  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2955600131 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2955600131  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2181320810, ___m_task_2)); }
	inline Task_1_t1265886475 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t1265886475 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t1265886475 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t2181320810_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t1265886475 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2181320810_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t1265886475 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t1265886475 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t1265886475 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T2181320810_H
#ifndef SPATIALUNDERSTANDINGDLL_T3348055109_H
#define SPATIALUNDERSTANDINGDLL_T3348055109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll
struct  SpatialUnderstandingDll_t3348055109  : public RuntimeObject
{
public:
	// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData[] HoloToolkit.Unity.SpatialUnderstandingDll::reusedMeshesForMarshalling
	MeshDataU5BU5D_t513299113* ___reusedMeshesForMarshalling_0;
	// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle> HoloToolkit.Unity.SpatialUnderstandingDll::reusedPinnedMemoryHandles
	List_1_t528545633 * ___reusedPinnedMemoryHandles_1;
	// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/RaycastResult HoloToolkit.Unity.SpatialUnderstandingDll::reusedRaycastResult
	RaycastResult_t1603201782 * ___reusedRaycastResult_2;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll::reusedRaycastResultPtr
	intptr_t ___reusedRaycastResultPtr_3;
	// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats HoloToolkit.Unity.SpatialUnderstandingDll::reusedPlayspaceStats
	PlayspaceStats_t3538817570 * ___reusedPlayspaceStats_4;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll::reusedPlayspaceStatsPtr
	intptr_t ___reusedPlayspaceStatsPtr_5;
	// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceAlignment HoloToolkit.Unity.SpatialUnderstandingDll::reusedPlayspaceAlignment
	PlayspaceAlignment_t492554602 * ___reusedPlayspaceAlignment_6;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll::reusedPlayspaceAlignmentPtr
	intptr_t ___reusedPlayspaceAlignmentPtr_7;
	// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementResult HoloToolkit.Unity.SpatialUnderstandingDll::reusedObjectPlacementResult
	ObjectPlacementResult_t4094328837 * ___reusedObjectPlacementResult_8;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll::reusedObjectPlacementResultPtr
	intptr_t ___reusedObjectPlacementResultPtr_9;

public:
	inline static int32_t get_offset_of_reusedMeshesForMarshalling_0() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t3348055109, ___reusedMeshesForMarshalling_0)); }
	inline MeshDataU5BU5D_t513299113* get_reusedMeshesForMarshalling_0() const { return ___reusedMeshesForMarshalling_0; }
	inline MeshDataU5BU5D_t513299113** get_address_of_reusedMeshesForMarshalling_0() { return &___reusedMeshesForMarshalling_0; }
	inline void set_reusedMeshesForMarshalling_0(MeshDataU5BU5D_t513299113* value)
	{
		___reusedMeshesForMarshalling_0 = value;
		Il2CppCodeGenWriteBarrier((&___reusedMeshesForMarshalling_0), value);
	}

	inline static int32_t get_offset_of_reusedPinnedMemoryHandles_1() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t3348055109, ___reusedPinnedMemoryHandles_1)); }
	inline List_1_t528545633 * get_reusedPinnedMemoryHandles_1() const { return ___reusedPinnedMemoryHandles_1; }
	inline List_1_t528545633 ** get_address_of_reusedPinnedMemoryHandles_1() { return &___reusedPinnedMemoryHandles_1; }
	inline void set_reusedPinnedMemoryHandles_1(List_1_t528545633 * value)
	{
		___reusedPinnedMemoryHandles_1 = value;
		Il2CppCodeGenWriteBarrier((&___reusedPinnedMemoryHandles_1), value);
	}

	inline static int32_t get_offset_of_reusedRaycastResult_2() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t3348055109, ___reusedRaycastResult_2)); }
	inline RaycastResult_t1603201782 * get_reusedRaycastResult_2() const { return ___reusedRaycastResult_2; }
	inline RaycastResult_t1603201782 ** get_address_of_reusedRaycastResult_2() { return &___reusedRaycastResult_2; }
	inline void set_reusedRaycastResult_2(RaycastResult_t1603201782 * value)
	{
		___reusedRaycastResult_2 = value;
		Il2CppCodeGenWriteBarrier((&___reusedRaycastResult_2), value);
	}

	inline static int32_t get_offset_of_reusedRaycastResultPtr_3() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t3348055109, ___reusedRaycastResultPtr_3)); }
	inline intptr_t get_reusedRaycastResultPtr_3() const { return ___reusedRaycastResultPtr_3; }
	inline intptr_t* get_address_of_reusedRaycastResultPtr_3() { return &___reusedRaycastResultPtr_3; }
	inline void set_reusedRaycastResultPtr_3(intptr_t value)
	{
		___reusedRaycastResultPtr_3 = value;
	}

	inline static int32_t get_offset_of_reusedPlayspaceStats_4() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t3348055109, ___reusedPlayspaceStats_4)); }
	inline PlayspaceStats_t3538817570 * get_reusedPlayspaceStats_4() const { return ___reusedPlayspaceStats_4; }
	inline PlayspaceStats_t3538817570 ** get_address_of_reusedPlayspaceStats_4() { return &___reusedPlayspaceStats_4; }
	inline void set_reusedPlayspaceStats_4(PlayspaceStats_t3538817570 * value)
	{
		___reusedPlayspaceStats_4 = value;
		Il2CppCodeGenWriteBarrier((&___reusedPlayspaceStats_4), value);
	}

	inline static int32_t get_offset_of_reusedPlayspaceStatsPtr_5() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t3348055109, ___reusedPlayspaceStatsPtr_5)); }
	inline intptr_t get_reusedPlayspaceStatsPtr_5() const { return ___reusedPlayspaceStatsPtr_5; }
	inline intptr_t* get_address_of_reusedPlayspaceStatsPtr_5() { return &___reusedPlayspaceStatsPtr_5; }
	inline void set_reusedPlayspaceStatsPtr_5(intptr_t value)
	{
		___reusedPlayspaceStatsPtr_5 = value;
	}

	inline static int32_t get_offset_of_reusedPlayspaceAlignment_6() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t3348055109, ___reusedPlayspaceAlignment_6)); }
	inline PlayspaceAlignment_t492554602 * get_reusedPlayspaceAlignment_6() const { return ___reusedPlayspaceAlignment_6; }
	inline PlayspaceAlignment_t492554602 ** get_address_of_reusedPlayspaceAlignment_6() { return &___reusedPlayspaceAlignment_6; }
	inline void set_reusedPlayspaceAlignment_6(PlayspaceAlignment_t492554602 * value)
	{
		___reusedPlayspaceAlignment_6 = value;
		Il2CppCodeGenWriteBarrier((&___reusedPlayspaceAlignment_6), value);
	}

	inline static int32_t get_offset_of_reusedPlayspaceAlignmentPtr_7() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t3348055109, ___reusedPlayspaceAlignmentPtr_7)); }
	inline intptr_t get_reusedPlayspaceAlignmentPtr_7() const { return ___reusedPlayspaceAlignmentPtr_7; }
	inline intptr_t* get_address_of_reusedPlayspaceAlignmentPtr_7() { return &___reusedPlayspaceAlignmentPtr_7; }
	inline void set_reusedPlayspaceAlignmentPtr_7(intptr_t value)
	{
		___reusedPlayspaceAlignmentPtr_7 = value;
	}

	inline static int32_t get_offset_of_reusedObjectPlacementResult_8() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t3348055109, ___reusedObjectPlacementResult_8)); }
	inline ObjectPlacementResult_t4094328837 * get_reusedObjectPlacementResult_8() const { return ___reusedObjectPlacementResult_8; }
	inline ObjectPlacementResult_t4094328837 ** get_address_of_reusedObjectPlacementResult_8() { return &___reusedObjectPlacementResult_8; }
	inline void set_reusedObjectPlacementResult_8(ObjectPlacementResult_t4094328837 * value)
	{
		___reusedObjectPlacementResult_8 = value;
		Il2CppCodeGenWriteBarrier((&___reusedObjectPlacementResult_8), value);
	}

	inline static int32_t get_offset_of_reusedObjectPlacementResultPtr_9() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t3348055109, ___reusedObjectPlacementResultPtr_9)); }
	inline intptr_t get_reusedObjectPlacementResultPtr_9() const { return ___reusedObjectPlacementResultPtr_9; }
	inline intptr_t* get_address_of_reusedObjectPlacementResultPtr_9() { return &___reusedObjectPlacementResultPtr_9; }
	inline void set_reusedObjectPlacementResultPtr_9(intptr_t value)
	{
		___reusedObjectPlacementResultPtr_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALUNDERSTANDINGDLL_T3348055109_H
#ifndef WALLTYPEFLAGS_T3149592466_H
#define WALLTYPEFLAGS_T3149592466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementDefinition/WallTypeFlags
struct  WallTypeFlags_t3149592466 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementDefinition/WallTypeFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WallTypeFlags_t3149592466, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WALLTYPEFLAGS_T3149592466_H
#ifndef INTERPOLATEDVALUE_1_T4074352723_H
#define INTERPOLATEDVALUE_1_T4074352723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Vector2>
struct  InterpolatedValue_1_t4074352723  : public RuntimeObject
{
public:
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Started
	Action_1_t4246820318 * ___Started_2;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Completed
	Action_1_t4246820318 * ___Completed_3;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::ValueChanged
	Action_1_t4246820318 * ___ValueChanged_4;
	// T HoloToolkit.Unity.InterpolatedValue`1::targetValue
	Vector2_t2156229523  ___targetValue_5;
	// T HoloToolkit.Unity.InterpolatedValue`1::startValue
	Vector2_t2156229523  ___startValue_6;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::timeInterpolationStartedAt
	float ___timeInterpolationStartedAt_7;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::firstUpdateFrameSkipped
	bool ___firstUpdateFrameSkipped_8;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::skipFirstUpdateFrame
	bool ___skipFirstUpdateFrame_9;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::performingInterpolativeSnap
	bool ___performingInterpolativeSnap_10;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::duration
	float ___duration_11;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.InterpolatedValue`1::curve
	AnimationCurve_t3046754366 * ___curve_12;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::<IsRunning>k__BackingField
	bool ___U3CIsRunningU3Ek__BackingField_13;
	// T HoloToolkit.Unity.InterpolatedValue`1::value
	Vector2_t2156229523  ___value_14;

public:
	inline static int32_t get_offset_of_Started_2() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4074352723, ___Started_2)); }
	inline Action_1_t4246820318 * get_Started_2() const { return ___Started_2; }
	inline Action_1_t4246820318 ** get_address_of_Started_2() { return &___Started_2; }
	inline void set_Started_2(Action_1_t4246820318 * value)
	{
		___Started_2 = value;
		Il2CppCodeGenWriteBarrier((&___Started_2), value);
	}

	inline static int32_t get_offset_of_Completed_3() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4074352723, ___Completed_3)); }
	inline Action_1_t4246820318 * get_Completed_3() const { return ___Completed_3; }
	inline Action_1_t4246820318 ** get_address_of_Completed_3() { return &___Completed_3; }
	inline void set_Completed_3(Action_1_t4246820318 * value)
	{
		___Completed_3 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_3), value);
	}

	inline static int32_t get_offset_of_ValueChanged_4() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4074352723, ___ValueChanged_4)); }
	inline Action_1_t4246820318 * get_ValueChanged_4() const { return ___ValueChanged_4; }
	inline Action_1_t4246820318 ** get_address_of_ValueChanged_4() { return &___ValueChanged_4; }
	inline void set_ValueChanged_4(Action_1_t4246820318 * value)
	{
		___ValueChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___ValueChanged_4), value);
	}

	inline static int32_t get_offset_of_targetValue_5() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4074352723, ___targetValue_5)); }
	inline Vector2_t2156229523  get_targetValue_5() const { return ___targetValue_5; }
	inline Vector2_t2156229523 * get_address_of_targetValue_5() { return &___targetValue_5; }
	inline void set_targetValue_5(Vector2_t2156229523  value)
	{
		___targetValue_5 = value;
	}

	inline static int32_t get_offset_of_startValue_6() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4074352723, ___startValue_6)); }
	inline Vector2_t2156229523  get_startValue_6() const { return ___startValue_6; }
	inline Vector2_t2156229523 * get_address_of_startValue_6() { return &___startValue_6; }
	inline void set_startValue_6(Vector2_t2156229523  value)
	{
		___startValue_6 = value;
	}

	inline static int32_t get_offset_of_timeInterpolationStartedAt_7() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4074352723, ___timeInterpolationStartedAt_7)); }
	inline float get_timeInterpolationStartedAt_7() const { return ___timeInterpolationStartedAt_7; }
	inline float* get_address_of_timeInterpolationStartedAt_7() { return &___timeInterpolationStartedAt_7; }
	inline void set_timeInterpolationStartedAt_7(float value)
	{
		___timeInterpolationStartedAt_7 = value;
	}

	inline static int32_t get_offset_of_firstUpdateFrameSkipped_8() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4074352723, ___firstUpdateFrameSkipped_8)); }
	inline bool get_firstUpdateFrameSkipped_8() const { return ___firstUpdateFrameSkipped_8; }
	inline bool* get_address_of_firstUpdateFrameSkipped_8() { return &___firstUpdateFrameSkipped_8; }
	inline void set_firstUpdateFrameSkipped_8(bool value)
	{
		___firstUpdateFrameSkipped_8 = value;
	}

	inline static int32_t get_offset_of_skipFirstUpdateFrame_9() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4074352723, ___skipFirstUpdateFrame_9)); }
	inline bool get_skipFirstUpdateFrame_9() const { return ___skipFirstUpdateFrame_9; }
	inline bool* get_address_of_skipFirstUpdateFrame_9() { return &___skipFirstUpdateFrame_9; }
	inline void set_skipFirstUpdateFrame_9(bool value)
	{
		___skipFirstUpdateFrame_9 = value;
	}

	inline static int32_t get_offset_of_performingInterpolativeSnap_10() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4074352723, ___performingInterpolativeSnap_10)); }
	inline bool get_performingInterpolativeSnap_10() const { return ___performingInterpolativeSnap_10; }
	inline bool* get_address_of_performingInterpolativeSnap_10() { return &___performingInterpolativeSnap_10; }
	inline void set_performingInterpolativeSnap_10(bool value)
	{
		___performingInterpolativeSnap_10 = value;
	}

	inline static int32_t get_offset_of_duration_11() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4074352723, ___duration_11)); }
	inline float get_duration_11() const { return ___duration_11; }
	inline float* get_address_of_duration_11() { return &___duration_11; }
	inline void set_duration_11(float value)
	{
		___duration_11 = value;
	}

	inline static int32_t get_offset_of_curve_12() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4074352723, ___curve_12)); }
	inline AnimationCurve_t3046754366 * get_curve_12() const { return ___curve_12; }
	inline AnimationCurve_t3046754366 ** get_address_of_curve_12() { return &___curve_12; }
	inline void set_curve_12(AnimationCurve_t3046754366 * value)
	{
		___curve_12 = value;
		Il2CppCodeGenWriteBarrier((&___curve_12), value);
	}

	inline static int32_t get_offset_of_U3CIsRunningU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4074352723, ___U3CIsRunningU3Ek__BackingField_13)); }
	inline bool get_U3CIsRunningU3Ek__BackingField_13() const { return ___U3CIsRunningU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsRunningU3Ek__BackingField_13() { return &___U3CIsRunningU3Ek__BackingField_13; }
	inline void set_U3CIsRunningU3Ek__BackingField_13(bool value)
	{
		___U3CIsRunningU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_value_14() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4074352723, ___value_14)); }
	inline Vector2_t2156229523  get_value_14() const { return ___value_14; }
	inline Vector2_t2156229523 * get_address_of_value_14() { return &___value_14; }
	inline void set_value_14(Vector2_t2156229523  value)
	{
		___value_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDVALUE_1_T4074352723_H
#ifndef INTERPOLATEDVALUE_1_T4220051531_H
#define INTERPOLATEDVALUE_1_T4220051531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Quaternion>
struct  InterpolatedValue_1_t4220051531  : public RuntimeObject
{
public:
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Started
	Action_1_t97551830 * ___Started_2;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Completed
	Action_1_t97551830 * ___Completed_3;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::ValueChanged
	Action_1_t97551830 * ___ValueChanged_4;
	// T HoloToolkit.Unity.InterpolatedValue`1::targetValue
	Quaternion_t2301928331  ___targetValue_5;
	// T HoloToolkit.Unity.InterpolatedValue`1::startValue
	Quaternion_t2301928331  ___startValue_6;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::timeInterpolationStartedAt
	float ___timeInterpolationStartedAt_7;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::firstUpdateFrameSkipped
	bool ___firstUpdateFrameSkipped_8;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::skipFirstUpdateFrame
	bool ___skipFirstUpdateFrame_9;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::performingInterpolativeSnap
	bool ___performingInterpolativeSnap_10;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::duration
	float ___duration_11;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.InterpolatedValue`1::curve
	AnimationCurve_t3046754366 * ___curve_12;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::<IsRunning>k__BackingField
	bool ___U3CIsRunningU3Ek__BackingField_13;
	// T HoloToolkit.Unity.InterpolatedValue`1::value
	Quaternion_t2301928331  ___value_14;

public:
	inline static int32_t get_offset_of_Started_2() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4220051531, ___Started_2)); }
	inline Action_1_t97551830 * get_Started_2() const { return ___Started_2; }
	inline Action_1_t97551830 ** get_address_of_Started_2() { return &___Started_2; }
	inline void set_Started_2(Action_1_t97551830 * value)
	{
		___Started_2 = value;
		Il2CppCodeGenWriteBarrier((&___Started_2), value);
	}

	inline static int32_t get_offset_of_Completed_3() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4220051531, ___Completed_3)); }
	inline Action_1_t97551830 * get_Completed_3() const { return ___Completed_3; }
	inline Action_1_t97551830 ** get_address_of_Completed_3() { return &___Completed_3; }
	inline void set_Completed_3(Action_1_t97551830 * value)
	{
		___Completed_3 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_3), value);
	}

	inline static int32_t get_offset_of_ValueChanged_4() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4220051531, ___ValueChanged_4)); }
	inline Action_1_t97551830 * get_ValueChanged_4() const { return ___ValueChanged_4; }
	inline Action_1_t97551830 ** get_address_of_ValueChanged_4() { return &___ValueChanged_4; }
	inline void set_ValueChanged_4(Action_1_t97551830 * value)
	{
		___ValueChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___ValueChanged_4), value);
	}

	inline static int32_t get_offset_of_targetValue_5() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4220051531, ___targetValue_5)); }
	inline Quaternion_t2301928331  get_targetValue_5() const { return ___targetValue_5; }
	inline Quaternion_t2301928331 * get_address_of_targetValue_5() { return &___targetValue_5; }
	inline void set_targetValue_5(Quaternion_t2301928331  value)
	{
		___targetValue_5 = value;
	}

	inline static int32_t get_offset_of_startValue_6() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4220051531, ___startValue_6)); }
	inline Quaternion_t2301928331  get_startValue_6() const { return ___startValue_6; }
	inline Quaternion_t2301928331 * get_address_of_startValue_6() { return &___startValue_6; }
	inline void set_startValue_6(Quaternion_t2301928331  value)
	{
		___startValue_6 = value;
	}

	inline static int32_t get_offset_of_timeInterpolationStartedAt_7() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4220051531, ___timeInterpolationStartedAt_7)); }
	inline float get_timeInterpolationStartedAt_7() const { return ___timeInterpolationStartedAt_7; }
	inline float* get_address_of_timeInterpolationStartedAt_7() { return &___timeInterpolationStartedAt_7; }
	inline void set_timeInterpolationStartedAt_7(float value)
	{
		___timeInterpolationStartedAt_7 = value;
	}

	inline static int32_t get_offset_of_firstUpdateFrameSkipped_8() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4220051531, ___firstUpdateFrameSkipped_8)); }
	inline bool get_firstUpdateFrameSkipped_8() const { return ___firstUpdateFrameSkipped_8; }
	inline bool* get_address_of_firstUpdateFrameSkipped_8() { return &___firstUpdateFrameSkipped_8; }
	inline void set_firstUpdateFrameSkipped_8(bool value)
	{
		___firstUpdateFrameSkipped_8 = value;
	}

	inline static int32_t get_offset_of_skipFirstUpdateFrame_9() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4220051531, ___skipFirstUpdateFrame_9)); }
	inline bool get_skipFirstUpdateFrame_9() const { return ___skipFirstUpdateFrame_9; }
	inline bool* get_address_of_skipFirstUpdateFrame_9() { return &___skipFirstUpdateFrame_9; }
	inline void set_skipFirstUpdateFrame_9(bool value)
	{
		___skipFirstUpdateFrame_9 = value;
	}

	inline static int32_t get_offset_of_performingInterpolativeSnap_10() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4220051531, ___performingInterpolativeSnap_10)); }
	inline bool get_performingInterpolativeSnap_10() const { return ___performingInterpolativeSnap_10; }
	inline bool* get_address_of_performingInterpolativeSnap_10() { return &___performingInterpolativeSnap_10; }
	inline void set_performingInterpolativeSnap_10(bool value)
	{
		___performingInterpolativeSnap_10 = value;
	}

	inline static int32_t get_offset_of_duration_11() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4220051531, ___duration_11)); }
	inline float get_duration_11() const { return ___duration_11; }
	inline float* get_address_of_duration_11() { return &___duration_11; }
	inline void set_duration_11(float value)
	{
		___duration_11 = value;
	}

	inline static int32_t get_offset_of_curve_12() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4220051531, ___curve_12)); }
	inline AnimationCurve_t3046754366 * get_curve_12() const { return ___curve_12; }
	inline AnimationCurve_t3046754366 ** get_address_of_curve_12() { return &___curve_12; }
	inline void set_curve_12(AnimationCurve_t3046754366 * value)
	{
		___curve_12 = value;
		Il2CppCodeGenWriteBarrier((&___curve_12), value);
	}

	inline static int32_t get_offset_of_U3CIsRunningU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4220051531, ___U3CIsRunningU3Ek__BackingField_13)); }
	inline bool get_U3CIsRunningU3Ek__BackingField_13() const { return ___U3CIsRunningU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsRunningU3Ek__BackingField_13() { return &___U3CIsRunningU3Ek__BackingField_13; }
	inline void set_U3CIsRunningU3Ek__BackingField_13(bool value)
	{
		___U3CIsRunningU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_value_14() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t4220051531, ___value_14)); }
	inline Quaternion_t2301928331  get_value_14() const { return ___value_14; }
	inline Quaternion_t2301928331 * get_address_of_value_14() { return &___value_14; }
	inline void set_value_14(Quaternion_t2301928331  value)
	{
		___value_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDVALUE_1_T4220051531_H
#ifndef INTERPOLATEDVALUE_1_T1345469368_H
#define INTERPOLATEDVALUE_1_T1345469368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Vector3>
struct  InterpolatedValue_1_t1345469368  : public RuntimeObject
{
public:
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Started
	Action_1_t1517936963 * ___Started_2;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Completed
	Action_1_t1517936963 * ___Completed_3;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::ValueChanged
	Action_1_t1517936963 * ___ValueChanged_4;
	// T HoloToolkit.Unity.InterpolatedValue`1::targetValue
	Vector3_t3722313464  ___targetValue_5;
	// T HoloToolkit.Unity.InterpolatedValue`1::startValue
	Vector3_t3722313464  ___startValue_6;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::timeInterpolationStartedAt
	float ___timeInterpolationStartedAt_7;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::firstUpdateFrameSkipped
	bool ___firstUpdateFrameSkipped_8;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::skipFirstUpdateFrame
	bool ___skipFirstUpdateFrame_9;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::performingInterpolativeSnap
	bool ___performingInterpolativeSnap_10;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::duration
	float ___duration_11;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.InterpolatedValue`1::curve
	AnimationCurve_t3046754366 * ___curve_12;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::<IsRunning>k__BackingField
	bool ___U3CIsRunningU3Ek__BackingField_13;
	// T HoloToolkit.Unity.InterpolatedValue`1::value
	Vector3_t3722313464  ___value_14;

public:
	inline static int32_t get_offset_of_Started_2() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t1345469368, ___Started_2)); }
	inline Action_1_t1517936963 * get_Started_2() const { return ___Started_2; }
	inline Action_1_t1517936963 ** get_address_of_Started_2() { return &___Started_2; }
	inline void set_Started_2(Action_1_t1517936963 * value)
	{
		___Started_2 = value;
		Il2CppCodeGenWriteBarrier((&___Started_2), value);
	}

	inline static int32_t get_offset_of_Completed_3() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t1345469368, ___Completed_3)); }
	inline Action_1_t1517936963 * get_Completed_3() const { return ___Completed_3; }
	inline Action_1_t1517936963 ** get_address_of_Completed_3() { return &___Completed_3; }
	inline void set_Completed_3(Action_1_t1517936963 * value)
	{
		___Completed_3 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_3), value);
	}

	inline static int32_t get_offset_of_ValueChanged_4() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t1345469368, ___ValueChanged_4)); }
	inline Action_1_t1517936963 * get_ValueChanged_4() const { return ___ValueChanged_4; }
	inline Action_1_t1517936963 ** get_address_of_ValueChanged_4() { return &___ValueChanged_4; }
	inline void set_ValueChanged_4(Action_1_t1517936963 * value)
	{
		___ValueChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___ValueChanged_4), value);
	}

	inline static int32_t get_offset_of_targetValue_5() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t1345469368, ___targetValue_5)); }
	inline Vector3_t3722313464  get_targetValue_5() const { return ___targetValue_5; }
	inline Vector3_t3722313464 * get_address_of_targetValue_5() { return &___targetValue_5; }
	inline void set_targetValue_5(Vector3_t3722313464  value)
	{
		___targetValue_5 = value;
	}

	inline static int32_t get_offset_of_startValue_6() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t1345469368, ___startValue_6)); }
	inline Vector3_t3722313464  get_startValue_6() const { return ___startValue_6; }
	inline Vector3_t3722313464 * get_address_of_startValue_6() { return &___startValue_6; }
	inline void set_startValue_6(Vector3_t3722313464  value)
	{
		___startValue_6 = value;
	}

	inline static int32_t get_offset_of_timeInterpolationStartedAt_7() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t1345469368, ___timeInterpolationStartedAt_7)); }
	inline float get_timeInterpolationStartedAt_7() const { return ___timeInterpolationStartedAt_7; }
	inline float* get_address_of_timeInterpolationStartedAt_7() { return &___timeInterpolationStartedAt_7; }
	inline void set_timeInterpolationStartedAt_7(float value)
	{
		___timeInterpolationStartedAt_7 = value;
	}

	inline static int32_t get_offset_of_firstUpdateFrameSkipped_8() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t1345469368, ___firstUpdateFrameSkipped_8)); }
	inline bool get_firstUpdateFrameSkipped_8() const { return ___firstUpdateFrameSkipped_8; }
	inline bool* get_address_of_firstUpdateFrameSkipped_8() { return &___firstUpdateFrameSkipped_8; }
	inline void set_firstUpdateFrameSkipped_8(bool value)
	{
		___firstUpdateFrameSkipped_8 = value;
	}

	inline static int32_t get_offset_of_skipFirstUpdateFrame_9() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t1345469368, ___skipFirstUpdateFrame_9)); }
	inline bool get_skipFirstUpdateFrame_9() const { return ___skipFirstUpdateFrame_9; }
	inline bool* get_address_of_skipFirstUpdateFrame_9() { return &___skipFirstUpdateFrame_9; }
	inline void set_skipFirstUpdateFrame_9(bool value)
	{
		___skipFirstUpdateFrame_9 = value;
	}

	inline static int32_t get_offset_of_performingInterpolativeSnap_10() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t1345469368, ___performingInterpolativeSnap_10)); }
	inline bool get_performingInterpolativeSnap_10() const { return ___performingInterpolativeSnap_10; }
	inline bool* get_address_of_performingInterpolativeSnap_10() { return &___performingInterpolativeSnap_10; }
	inline void set_performingInterpolativeSnap_10(bool value)
	{
		___performingInterpolativeSnap_10 = value;
	}

	inline static int32_t get_offset_of_duration_11() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t1345469368, ___duration_11)); }
	inline float get_duration_11() const { return ___duration_11; }
	inline float* get_address_of_duration_11() { return &___duration_11; }
	inline void set_duration_11(float value)
	{
		___duration_11 = value;
	}

	inline static int32_t get_offset_of_curve_12() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t1345469368, ___curve_12)); }
	inline AnimationCurve_t3046754366 * get_curve_12() const { return ___curve_12; }
	inline AnimationCurve_t3046754366 ** get_address_of_curve_12() { return &___curve_12; }
	inline void set_curve_12(AnimationCurve_t3046754366 * value)
	{
		___curve_12 = value;
		Il2CppCodeGenWriteBarrier((&___curve_12), value);
	}

	inline static int32_t get_offset_of_U3CIsRunningU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t1345469368, ___U3CIsRunningU3Ek__BackingField_13)); }
	inline bool get_U3CIsRunningU3Ek__BackingField_13() const { return ___U3CIsRunningU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsRunningU3Ek__BackingField_13() { return &___U3CIsRunningU3Ek__BackingField_13; }
	inline void set_U3CIsRunningU3Ek__BackingField_13(bool value)
	{
		___U3CIsRunningU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_value_14() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t1345469368, ___value_14)); }
	inline Vector3_t3722313464  get_value_14() const { return ___value_14; }
	inline Vector3_t3722313464 * get_address_of_value_14() { return &___value_14; }
	inline void set_value_14(Vector3_t3722313464  value)
	{
		___value_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDVALUE_1_T1345469368_H
#ifndef ASYNCTASKMETHODBUILDER_1_T2418262475_H
#define ASYNCTASKMETHODBUILDER_1_T2418262475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>
struct  AsyncTaskMethodBuilder_1_t2418262475 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2955600131  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t1502828140 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2418262475, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2955600131  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2955600131 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2955600131  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2418262475, ___m_task_2)); }
	inline Task_1_t1502828140 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t1502828140 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t1502828140 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t2418262475_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t1502828140 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2418262475_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t1502828140 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t1502828140 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t1502828140 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T2418262475_H
#ifndef RAYCASTRESULT_T3360306849_H
#define RAYCASTRESULT_T3360306849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t3360306849 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1113636619 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t4150874583 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t3722313464  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t3722313464  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2156229523  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___m_GameObject_0)); }
	inline GameObject_t1113636619 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1113636619 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___module_1)); }
	inline BaseRaycaster_t4150874583 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t4150874583 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t4150874583 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldPosition_7)); }
	inline Vector3_t3722313464  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t3722313464 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t3722313464  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldNormal_8)); }
	inline Vector3_t3722313464  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t3722313464 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t3722313464  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___screenPosition_9)); }
	inline Vector2_t2156229523  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2156229523 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2156229523  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_pinvoke
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_com
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T3360306849_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef OBJECTPLACEMENTCONSTRAINTTYPE_T378844790_H
#define OBJECTPLACEMENTCONSTRAINTTYPE_T378844790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementConstraint/ObjectPlacementConstraintType
struct  ObjectPlacementConstraintType_t378844790 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementConstraint/ObjectPlacementConstraintType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectPlacementConstraintType_t378844790, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPLACEMENTCONSTRAINTTYPE_T378844790_H
#ifndef OBJECTPLACEMENTRULETYPE_T3448184229_H
#define OBJECTPLACEMENTRULETYPE_T3448184229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementRule/ObjectPlacementRuleType
struct  ObjectPlacementRuleType_t3448184229 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementRule/ObjectPlacementRuleType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectPlacementRuleType_t3448184229, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPLACEMENTRULETYPE_T3448184229_H
#ifndef INTERACTIONSOURCEHANDEDNESS_T3096408347_H
#define INTERACTIONSOURCEHANDEDNESS_T3096408347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.WSA.Input.InteractionSourceHandedness
struct  InteractionSourceHandedness_t3096408347 
{
public:
	// System.Int32 UnityEngine.XR.WSA.Input.InteractionSourceHandedness::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InteractionSourceHandedness_t3096408347, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSOURCEHANDEDNESS_T3096408347_H
#ifndef INTERPOLATEDVALUE_1_T178842228_H
#define INTERPOLATEDVALUE_1_T178842228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Color>
struct  InterpolatedValue_1_t178842228  : public RuntimeObject
{
public:
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Started
	Action_1_t351309823 * ___Started_2;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Completed
	Action_1_t351309823 * ___Completed_3;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::ValueChanged
	Action_1_t351309823 * ___ValueChanged_4;
	// T HoloToolkit.Unity.InterpolatedValue`1::targetValue
	Color_t2555686324  ___targetValue_5;
	// T HoloToolkit.Unity.InterpolatedValue`1::startValue
	Color_t2555686324  ___startValue_6;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::timeInterpolationStartedAt
	float ___timeInterpolationStartedAt_7;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::firstUpdateFrameSkipped
	bool ___firstUpdateFrameSkipped_8;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::skipFirstUpdateFrame
	bool ___skipFirstUpdateFrame_9;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::performingInterpolativeSnap
	bool ___performingInterpolativeSnap_10;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::duration
	float ___duration_11;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.InterpolatedValue`1::curve
	AnimationCurve_t3046754366 * ___curve_12;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::<IsRunning>k__BackingField
	bool ___U3CIsRunningU3Ek__BackingField_13;
	// T HoloToolkit.Unity.InterpolatedValue`1::value
	Color_t2555686324  ___value_14;

public:
	inline static int32_t get_offset_of_Started_2() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t178842228, ___Started_2)); }
	inline Action_1_t351309823 * get_Started_2() const { return ___Started_2; }
	inline Action_1_t351309823 ** get_address_of_Started_2() { return &___Started_2; }
	inline void set_Started_2(Action_1_t351309823 * value)
	{
		___Started_2 = value;
		Il2CppCodeGenWriteBarrier((&___Started_2), value);
	}

	inline static int32_t get_offset_of_Completed_3() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t178842228, ___Completed_3)); }
	inline Action_1_t351309823 * get_Completed_3() const { return ___Completed_3; }
	inline Action_1_t351309823 ** get_address_of_Completed_3() { return &___Completed_3; }
	inline void set_Completed_3(Action_1_t351309823 * value)
	{
		___Completed_3 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_3), value);
	}

	inline static int32_t get_offset_of_ValueChanged_4() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t178842228, ___ValueChanged_4)); }
	inline Action_1_t351309823 * get_ValueChanged_4() const { return ___ValueChanged_4; }
	inline Action_1_t351309823 ** get_address_of_ValueChanged_4() { return &___ValueChanged_4; }
	inline void set_ValueChanged_4(Action_1_t351309823 * value)
	{
		___ValueChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___ValueChanged_4), value);
	}

	inline static int32_t get_offset_of_targetValue_5() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t178842228, ___targetValue_5)); }
	inline Color_t2555686324  get_targetValue_5() const { return ___targetValue_5; }
	inline Color_t2555686324 * get_address_of_targetValue_5() { return &___targetValue_5; }
	inline void set_targetValue_5(Color_t2555686324  value)
	{
		___targetValue_5 = value;
	}

	inline static int32_t get_offset_of_startValue_6() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t178842228, ___startValue_6)); }
	inline Color_t2555686324  get_startValue_6() const { return ___startValue_6; }
	inline Color_t2555686324 * get_address_of_startValue_6() { return &___startValue_6; }
	inline void set_startValue_6(Color_t2555686324  value)
	{
		___startValue_6 = value;
	}

	inline static int32_t get_offset_of_timeInterpolationStartedAt_7() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t178842228, ___timeInterpolationStartedAt_7)); }
	inline float get_timeInterpolationStartedAt_7() const { return ___timeInterpolationStartedAt_7; }
	inline float* get_address_of_timeInterpolationStartedAt_7() { return &___timeInterpolationStartedAt_7; }
	inline void set_timeInterpolationStartedAt_7(float value)
	{
		___timeInterpolationStartedAt_7 = value;
	}

	inline static int32_t get_offset_of_firstUpdateFrameSkipped_8() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t178842228, ___firstUpdateFrameSkipped_8)); }
	inline bool get_firstUpdateFrameSkipped_8() const { return ___firstUpdateFrameSkipped_8; }
	inline bool* get_address_of_firstUpdateFrameSkipped_8() { return &___firstUpdateFrameSkipped_8; }
	inline void set_firstUpdateFrameSkipped_8(bool value)
	{
		___firstUpdateFrameSkipped_8 = value;
	}

	inline static int32_t get_offset_of_skipFirstUpdateFrame_9() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t178842228, ___skipFirstUpdateFrame_9)); }
	inline bool get_skipFirstUpdateFrame_9() const { return ___skipFirstUpdateFrame_9; }
	inline bool* get_address_of_skipFirstUpdateFrame_9() { return &___skipFirstUpdateFrame_9; }
	inline void set_skipFirstUpdateFrame_9(bool value)
	{
		___skipFirstUpdateFrame_9 = value;
	}

	inline static int32_t get_offset_of_performingInterpolativeSnap_10() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t178842228, ___performingInterpolativeSnap_10)); }
	inline bool get_performingInterpolativeSnap_10() const { return ___performingInterpolativeSnap_10; }
	inline bool* get_address_of_performingInterpolativeSnap_10() { return &___performingInterpolativeSnap_10; }
	inline void set_performingInterpolativeSnap_10(bool value)
	{
		___performingInterpolativeSnap_10 = value;
	}

	inline static int32_t get_offset_of_duration_11() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t178842228, ___duration_11)); }
	inline float get_duration_11() const { return ___duration_11; }
	inline float* get_address_of_duration_11() { return &___duration_11; }
	inline void set_duration_11(float value)
	{
		___duration_11 = value;
	}

	inline static int32_t get_offset_of_curve_12() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t178842228, ___curve_12)); }
	inline AnimationCurve_t3046754366 * get_curve_12() const { return ___curve_12; }
	inline AnimationCurve_t3046754366 ** get_address_of_curve_12() { return &___curve_12; }
	inline void set_curve_12(AnimationCurve_t3046754366 * value)
	{
		___curve_12 = value;
		Il2CppCodeGenWriteBarrier((&___curve_12), value);
	}

	inline static int32_t get_offset_of_U3CIsRunningU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t178842228, ___U3CIsRunningU3Ek__BackingField_13)); }
	inline bool get_U3CIsRunningU3Ek__BackingField_13() const { return ___U3CIsRunningU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsRunningU3Ek__BackingField_13() { return &___U3CIsRunningU3Ek__BackingField_13; }
	inline void set_U3CIsRunningU3Ek__BackingField_13(bool value)
	{
		___U3CIsRunningU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_value_14() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t178842228, ___value_14)); }
	inline Color_t2555686324  get_value_14() const { return ___value_14; }
	inline Color_t2555686324 * get_address_of_value_14() { return &___value_14; }
	inline void set_value_14(Color_t2555686324  value)
	{
		___value_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDVALUE_1_T178842228_H
#ifndef INTERACTIONSOURCEKIND_T3005082353_H
#define INTERACTIONSOURCEKIND_T3005082353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.WSA.Input.InteractionSourceKind
struct  InteractionSourceKind_t3005082353 
{
public:
	// System.Int32 UnityEngine.XR.WSA.Input.InteractionSourceKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InteractionSourceKind_t3005082353, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSOURCEKIND_T3005082353_H
#ifndef U3CIMPORT_UNDERSTANDINGMESHU3ED__28_T801016225_H
#define U3CIMPORT_UNDERSTANDINGMESHU3ED__28_T801016225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28
struct  U3CImport_UnderstandingMeshU3Ed__28_t801016225  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.SpatialUnderstandingCustomMesh HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<>4__this
	SpatialUnderstandingCustomMesh_t3317789065 * ___U3CU3E4__this_2;
	// System.Diagnostics.Stopwatch HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<stopwatch>5__1
	Stopwatch_t305734070 * ___U3CstopwatchU3E5__1_3;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<startFrameCount>5__2
	int32_t ___U3CstartFrameCountU3E5__2_4;
	// HoloToolkit.Unity.SpatialUnderstandingDll HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<dll>5__3
	SpatialUnderstandingDll_t3348055109 * ___U3CdllU3E5__3_5;
	// UnityEngine.Vector3[] HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<meshVertices>5__4
	Vector3U5BU5D_t1718750761* ___U3CmeshVerticesU3E5__4_6;
	// UnityEngine.Vector3[] HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<meshNormals>5__5
	Vector3U5BU5D_t1718750761* ___U3CmeshNormalsU3E5__5_7;
	// System.Int32[] HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<meshIndices>5__6
	Int32U5BU5D_t385246372* ___U3CmeshIndicesU3E5__6_8;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<vertCount>5__7
	int32_t ___U3CvertCountU3E5__7_9;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<idxCount>5__8
	int32_t ___U3CidxCountU3E5__8_10;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<deltaFrameCount>5__9
	int32_t ___U3CdeltaFrameCountU3E5__9_11;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<vertPos>5__10
	intptr_t ___U3CvertPosU3E5__10_12;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<vertNorm>5__11
	intptr_t ___U3CvertNormU3E5__11_13;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<indices>5__12
	intptr_t ___U3CindicesU3E5__12_14;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<startTime>5__13
	float ___U3CstartTimeU3E5__13_15;
	// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector3,HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData> HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<>s__14
	Enumerator_t3437831517  ___U3CU3Es__14_16;
	// HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<meshdata>5__15
	MeshData_t2646720140 * ___U3CmeshdataU3E5__15_17;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<index>5__16
	int32_t ___U3CindexU3E5__16_18;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<firstVertex>5__17
	Vector3_t3722313464  ___U3CfirstVertexU3E5__17_19;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<secondVertex>5__18
	Vector3_t3722313464  ___U3CsecondVertexU3E5__18_20;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<thirdVertex>5__19
	Vector3_t3722313464  ___U3CthirdVertexU3E5__19_21;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<firstSector>5__20
	Vector3_t3722313464  ___U3CfirstSectorU3E5__20_22;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<secondSector>5__21
	Vector3_t3722313464  ___U3CsecondSectorU3E5__21_23;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<thirdSector>5__22
	Vector3_t3722313464  ___U3CthirdSectorU3E5__22_24;
	// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Vector3,HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData> HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<>s__23
	Enumerator_t3437831517  ___U3CU3Es__23_25;
	// HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData HoloToolkit.Unity.SpatialUnderstandingCustomMesh/<Import_UnderstandingMesh>d__28::<meshData>5__24
	MeshData_t2646720140 * ___U3CmeshDataU3E5__24_26;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CU3E4__this_2)); }
	inline SpatialUnderstandingCustomMesh_t3317789065 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SpatialUnderstandingCustomMesh_t3317789065 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SpatialUnderstandingCustomMesh_t3317789065 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CstopwatchU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CstopwatchU3E5__1_3)); }
	inline Stopwatch_t305734070 * get_U3CstopwatchU3E5__1_3() const { return ___U3CstopwatchU3E5__1_3; }
	inline Stopwatch_t305734070 ** get_address_of_U3CstopwatchU3E5__1_3() { return &___U3CstopwatchU3E5__1_3; }
	inline void set_U3CstopwatchU3E5__1_3(Stopwatch_t305734070 * value)
	{
		___U3CstopwatchU3E5__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstopwatchU3E5__1_3), value);
	}

	inline static int32_t get_offset_of_U3CstartFrameCountU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CstartFrameCountU3E5__2_4)); }
	inline int32_t get_U3CstartFrameCountU3E5__2_4() const { return ___U3CstartFrameCountU3E5__2_4; }
	inline int32_t* get_address_of_U3CstartFrameCountU3E5__2_4() { return &___U3CstartFrameCountU3E5__2_4; }
	inline void set_U3CstartFrameCountU3E5__2_4(int32_t value)
	{
		___U3CstartFrameCountU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CdllU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CdllU3E5__3_5)); }
	inline SpatialUnderstandingDll_t3348055109 * get_U3CdllU3E5__3_5() const { return ___U3CdllU3E5__3_5; }
	inline SpatialUnderstandingDll_t3348055109 ** get_address_of_U3CdllU3E5__3_5() { return &___U3CdllU3E5__3_5; }
	inline void set_U3CdllU3E5__3_5(SpatialUnderstandingDll_t3348055109 * value)
	{
		___U3CdllU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdllU3E5__3_5), value);
	}

	inline static int32_t get_offset_of_U3CmeshVerticesU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CmeshVerticesU3E5__4_6)); }
	inline Vector3U5BU5D_t1718750761* get_U3CmeshVerticesU3E5__4_6() const { return ___U3CmeshVerticesU3E5__4_6; }
	inline Vector3U5BU5D_t1718750761** get_address_of_U3CmeshVerticesU3E5__4_6() { return &___U3CmeshVerticesU3E5__4_6; }
	inline void set_U3CmeshVerticesU3E5__4_6(Vector3U5BU5D_t1718750761* value)
	{
		___U3CmeshVerticesU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmeshVerticesU3E5__4_6), value);
	}

	inline static int32_t get_offset_of_U3CmeshNormalsU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CmeshNormalsU3E5__5_7)); }
	inline Vector3U5BU5D_t1718750761* get_U3CmeshNormalsU3E5__5_7() const { return ___U3CmeshNormalsU3E5__5_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of_U3CmeshNormalsU3E5__5_7() { return &___U3CmeshNormalsU3E5__5_7; }
	inline void set_U3CmeshNormalsU3E5__5_7(Vector3U5BU5D_t1718750761* value)
	{
		___U3CmeshNormalsU3E5__5_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmeshNormalsU3E5__5_7), value);
	}

	inline static int32_t get_offset_of_U3CmeshIndicesU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CmeshIndicesU3E5__6_8)); }
	inline Int32U5BU5D_t385246372* get_U3CmeshIndicesU3E5__6_8() const { return ___U3CmeshIndicesU3E5__6_8; }
	inline Int32U5BU5D_t385246372** get_address_of_U3CmeshIndicesU3E5__6_8() { return &___U3CmeshIndicesU3E5__6_8; }
	inline void set_U3CmeshIndicesU3E5__6_8(Int32U5BU5D_t385246372* value)
	{
		___U3CmeshIndicesU3E5__6_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmeshIndicesU3E5__6_8), value);
	}

	inline static int32_t get_offset_of_U3CvertCountU3E5__7_9() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CvertCountU3E5__7_9)); }
	inline int32_t get_U3CvertCountU3E5__7_9() const { return ___U3CvertCountU3E5__7_9; }
	inline int32_t* get_address_of_U3CvertCountU3E5__7_9() { return &___U3CvertCountU3E5__7_9; }
	inline void set_U3CvertCountU3E5__7_9(int32_t value)
	{
		___U3CvertCountU3E5__7_9 = value;
	}

	inline static int32_t get_offset_of_U3CidxCountU3E5__8_10() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CidxCountU3E5__8_10)); }
	inline int32_t get_U3CidxCountU3E5__8_10() const { return ___U3CidxCountU3E5__8_10; }
	inline int32_t* get_address_of_U3CidxCountU3E5__8_10() { return &___U3CidxCountU3E5__8_10; }
	inline void set_U3CidxCountU3E5__8_10(int32_t value)
	{
		___U3CidxCountU3E5__8_10 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaFrameCountU3E5__9_11() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CdeltaFrameCountU3E5__9_11)); }
	inline int32_t get_U3CdeltaFrameCountU3E5__9_11() const { return ___U3CdeltaFrameCountU3E5__9_11; }
	inline int32_t* get_address_of_U3CdeltaFrameCountU3E5__9_11() { return &___U3CdeltaFrameCountU3E5__9_11; }
	inline void set_U3CdeltaFrameCountU3E5__9_11(int32_t value)
	{
		___U3CdeltaFrameCountU3E5__9_11 = value;
	}

	inline static int32_t get_offset_of_U3CvertPosU3E5__10_12() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CvertPosU3E5__10_12)); }
	inline intptr_t get_U3CvertPosU3E5__10_12() const { return ___U3CvertPosU3E5__10_12; }
	inline intptr_t* get_address_of_U3CvertPosU3E5__10_12() { return &___U3CvertPosU3E5__10_12; }
	inline void set_U3CvertPosU3E5__10_12(intptr_t value)
	{
		___U3CvertPosU3E5__10_12 = value;
	}

	inline static int32_t get_offset_of_U3CvertNormU3E5__11_13() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CvertNormU3E5__11_13)); }
	inline intptr_t get_U3CvertNormU3E5__11_13() const { return ___U3CvertNormU3E5__11_13; }
	inline intptr_t* get_address_of_U3CvertNormU3E5__11_13() { return &___U3CvertNormU3E5__11_13; }
	inline void set_U3CvertNormU3E5__11_13(intptr_t value)
	{
		___U3CvertNormU3E5__11_13 = value;
	}

	inline static int32_t get_offset_of_U3CindicesU3E5__12_14() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CindicesU3E5__12_14)); }
	inline intptr_t get_U3CindicesU3E5__12_14() const { return ___U3CindicesU3E5__12_14; }
	inline intptr_t* get_address_of_U3CindicesU3E5__12_14() { return &___U3CindicesU3E5__12_14; }
	inline void set_U3CindicesU3E5__12_14(intptr_t value)
	{
		___U3CindicesU3E5__12_14 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__13_15() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CstartTimeU3E5__13_15)); }
	inline float get_U3CstartTimeU3E5__13_15() const { return ___U3CstartTimeU3E5__13_15; }
	inline float* get_address_of_U3CstartTimeU3E5__13_15() { return &___U3CstartTimeU3E5__13_15; }
	inline void set_U3CstartTimeU3E5__13_15(float value)
	{
		___U3CstartTimeU3E5__13_15 = value;
	}

	inline static int32_t get_offset_of_U3CU3Es__14_16() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CU3Es__14_16)); }
	inline Enumerator_t3437831517  get_U3CU3Es__14_16() const { return ___U3CU3Es__14_16; }
	inline Enumerator_t3437831517 * get_address_of_U3CU3Es__14_16() { return &___U3CU3Es__14_16; }
	inline void set_U3CU3Es__14_16(Enumerator_t3437831517  value)
	{
		___U3CU3Es__14_16 = value;
	}

	inline static int32_t get_offset_of_U3CmeshdataU3E5__15_17() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CmeshdataU3E5__15_17)); }
	inline MeshData_t2646720140 * get_U3CmeshdataU3E5__15_17() const { return ___U3CmeshdataU3E5__15_17; }
	inline MeshData_t2646720140 ** get_address_of_U3CmeshdataU3E5__15_17() { return &___U3CmeshdataU3E5__15_17; }
	inline void set_U3CmeshdataU3E5__15_17(MeshData_t2646720140 * value)
	{
		___U3CmeshdataU3E5__15_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmeshdataU3E5__15_17), value);
	}

	inline static int32_t get_offset_of_U3CindexU3E5__16_18() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CindexU3E5__16_18)); }
	inline int32_t get_U3CindexU3E5__16_18() const { return ___U3CindexU3E5__16_18; }
	inline int32_t* get_address_of_U3CindexU3E5__16_18() { return &___U3CindexU3E5__16_18; }
	inline void set_U3CindexU3E5__16_18(int32_t value)
	{
		___U3CindexU3E5__16_18 = value;
	}

	inline static int32_t get_offset_of_U3CfirstVertexU3E5__17_19() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CfirstVertexU3E5__17_19)); }
	inline Vector3_t3722313464  get_U3CfirstVertexU3E5__17_19() const { return ___U3CfirstVertexU3E5__17_19; }
	inline Vector3_t3722313464 * get_address_of_U3CfirstVertexU3E5__17_19() { return &___U3CfirstVertexU3E5__17_19; }
	inline void set_U3CfirstVertexU3E5__17_19(Vector3_t3722313464  value)
	{
		___U3CfirstVertexU3E5__17_19 = value;
	}

	inline static int32_t get_offset_of_U3CsecondVertexU3E5__18_20() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CsecondVertexU3E5__18_20)); }
	inline Vector3_t3722313464  get_U3CsecondVertexU3E5__18_20() const { return ___U3CsecondVertexU3E5__18_20; }
	inline Vector3_t3722313464 * get_address_of_U3CsecondVertexU3E5__18_20() { return &___U3CsecondVertexU3E5__18_20; }
	inline void set_U3CsecondVertexU3E5__18_20(Vector3_t3722313464  value)
	{
		___U3CsecondVertexU3E5__18_20 = value;
	}

	inline static int32_t get_offset_of_U3CthirdVertexU3E5__19_21() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CthirdVertexU3E5__19_21)); }
	inline Vector3_t3722313464  get_U3CthirdVertexU3E5__19_21() const { return ___U3CthirdVertexU3E5__19_21; }
	inline Vector3_t3722313464 * get_address_of_U3CthirdVertexU3E5__19_21() { return &___U3CthirdVertexU3E5__19_21; }
	inline void set_U3CthirdVertexU3E5__19_21(Vector3_t3722313464  value)
	{
		___U3CthirdVertexU3E5__19_21 = value;
	}

	inline static int32_t get_offset_of_U3CfirstSectorU3E5__20_22() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CfirstSectorU3E5__20_22)); }
	inline Vector3_t3722313464  get_U3CfirstSectorU3E5__20_22() const { return ___U3CfirstSectorU3E5__20_22; }
	inline Vector3_t3722313464 * get_address_of_U3CfirstSectorU3E5__20_22() { return &___U3CfirstSectorU3E5__20_22; }
	inline void set_U3CfirstSectorU3E5__20_22(Vector3_t3722313464  value)
	{
		___U3CfirstSectorU3E5__20_22 = value;
	}

	inline static int32_t get_offset_of_U3CsecondSectorU3E5__21_23() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CsecondSectorU3E5__21_23)); }
	inline Vector3_t3722313464  get_U3CsecondSectorU3E5__21_23() const { return ___U3CsecondSectorU3E5__21_23; }
	inline Vector3_t3722313464 * get_address_of_U3CsecondSectorU3E5__21_23() { return &___U3CsecondSectorU3E5__21_23; }
	inline void set_U3CsecondSectorU3E5__21_23(Vector3_t3722313464  value)
	{
		___U3CsecondSectorU3E5__21_23 = value;
	}

	inline static int32_t get_offset_of_U3CthirdSectorU3E5__22_24() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CthirdSectorU3E5__22_24)); }
	inline Vector3_t3722313464  get_U3CthirdSectorU3E5__22_24() const { return ___U3CthirdSectorU3E5__22_24; }
	inline Vector3_t3722313464 * get_address_of_U3CthirdSectorU3E5__22_24() { return &___U3CthirdSectorU3E5__22_24; }
	inline void set_U3CthirdSectorU3E5__22_24(Vector3_t3722313464  value)
	{
		___U3CthirdSectorU3E5__22_24 = value;
	}

	inline static int32_t get_offset_of_U3CU3Es__23_25() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CU3Es__23_25)); }
	inline Enumerator_t3437831517  get_U3CU3Es__23_25() const { return ___U3CU3Es__23_25; }
	inline Enumerator_t3437831517 * get_address_of_U3CU3Es__23_25() { return &___U3CU3Es__23_25; }
	inline void set_U3CU3Es__23_25(Enumerator_t3437831517  value)
	{
		___U3CU3Es__23_25 = value;
	}

	inline static int32_t get_offset_of_U3CmeshDataU3E5__24_26() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_t801016225, ___U3CmeshDataU3E5__24_26)); }
	inline MeshData_t2646720140 * get_U3CmeshDataU3E5__24_26() const { return ___U3CmeshDataU3E5__24_26; }
	inline MeshData_t2646720140 ** get_address_of_U3CmeshDataU3E5__24_26() { return &___U3CmeshDataU3E5__24_26; }
	inline void set_U3CmeshDataU3E5__24_26(MeshData_t2646720140 * value)
	{
		___U3CmeshDataU3E5__24_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmeshDataU3E5__24_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CIMPORT_UNDERSTANDINGMESHU3ED__28_T801016225_H
#ifndef RAYSTEP_T2147652507_H
#define RAYSTEP_T2147652507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RayStep
struct  RayStep_t2147652507 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.RayStep::<Origin>k__BackingField
	Vector3_t3722313464  ___U3COriginU3Ek__BackingField_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.RayStep::<Terminus>k__BackingField
	Vector3_t3722313464  ___U3CTerminusU3Ek__BackingField_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.RayStep::<Direction>k__BackingField
	Vector3_t3722313464  ___U3CDirectionU3Ek__BackingField_2;
	// System.Single HoloToolkit.Unity.RayStep::<Length>k__BackingField
	float ___U3CLengthU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3COriginU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RayStep_t2147652507, ___U3COriginU3Ek__BackingField_0)); }
	inline Vector3_t3722313464  get_U3COriginU3Ek__BackingField_0() const { return ___U3COriginU3Ek__BackingField_0; }
	inline Vector3_t3722313464 * get_address_of_U3COriginU3Ek__BackingField_0() { return &___U3COriginU3Ek__BackingField_0; }
	inline void set_U3COriginU3Ek__BackingField_0(Vector3_t3722313464  value)
	{
		___U3COriginU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTerminusU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RayStep_t2147652507, ___U3CTerminusU3Ek__BackingField_1)); }
	inline Vector3_t3722313464  get_U3CTerminusU3Ek__BackingField_1() const { return ___U3CTerminusU3Ek__BackingField_1; }
	inline Vector3_t3722313464 * get_address_of_U3CTerminusU3Ek__BackingField_1() { return &___U3CTerminusU3Ek__BackingField_1; }
	inline void set_U3CTerminusU3Ek__BackingField_1(Vector3_t3722313464  value)
	{
		___U3CTerminusU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDirectionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RayStep_t2147652507, ___U3CDirectionU3Ek__BackingField_2)); }
	inline Vector3_t3722313464  get_U3CDirectionU3Ek__BackingField_2() const { return ___U3CDirectionU3Ek__BackingField_2; }
	inline Vector3_t3722313464 * get_address_of_U3CDirectionU3Ek__BackingField_2() { return &___U3CDirectionU3Ek__BackingField_2; }
	inline void set_U3CDirectionU3Ek__BackingField_2(Vector3_t3722313464  value)
	{
		___U3CDirectionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RayStep_t2147652507, ___U3CLengthU3Ek__BackingField_3)); }
	inline float get_U3CLengthU3Ek__BackingField_3() const { return ___U3CLengthU3Ek__BackingField_3; }
	inline float* get_address_of_U3CLengthU3Ek__BackingField_3() { return &___U3CLengthU3Ek__BackingField_3; }
	inline void set_U3CLengthU3Ek__BackingField_3(float value)
	{
		___U3CLengthU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYSTEP_T2147652507_H
#ifndef QUATERNIONINTERPOLATED_T376031970_H
#define QUATERNIONINTERPOLATED_T376031970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.QuaternionInterpolated
struct  QuaternionInterpolated_t376031970  : public RuntimeObject
{
public:
	// System.Single HoloToolkit.Unity.QuaternionInterpolated::DeltaSpeed
	float ___DeltaSpeed_0;
	// UnityEngine.Quaternion HoloToolkit.Unity.QuaternionInterpolated::<Value>k__BackingField
	Quaternion_t2301928331  ___U3CValueU3Ek__BackingField_1;
	// UnityEngine.Quaternion HoloToolkit.Unity.QuaternionInterpolated::<TargetValue>k__BackingField
	Quaternion_t2301928331  ___U3CTargetValueU3Ek__BackingField_2;
	// UnityEngine.Quaternion HoloToolkit.Unity.QuaternionInterpolated::<StartValue>k__BackingField
	Quaternion_t2301928331  ___U3CStartValueU3Ek__BackingField_3;
	// System.Single HoloToolkit.Unity.QuaternionInterpolated::<Duration>k__BackingField
	float ___U3CDurationU3Ek__BackingField_4;
	// System.Single HoloToolkit.Unity.QuaternionInterpolated::<Counter>k__BackingField
	float ___U3CCounterU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_DeltaSpeed_0() { return static_cast<int32_t>(offsetof(QuaternionInterpolated_t376031970, ___DeltaSpeed_0)); }
	inline float get_DeltaSpeed_0() const { return ___DeltaSpeed_0; }
	inline float* get_address_of_DeltaSpeed_0() { return &___DeltaSpeed_0; }
	inline void set_DeltaSpeed_0(float value)
	{
		___DeltaSpeed_0 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(QuaternionInterpolated_t376031970, ___U3CValueU3Ek__BackingField_1)); }
	inline Quaternion_t2301928331  get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline Quaternion_t2301928331 * get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(Quaternion_t2301928331  value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTargetValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(QuaternionInterpolated_t376031970, ___U3CTargetValueU3Ek__BackingField_2)); }
	inline Quaternion_t2301928331  get_U3CTargetValueU3Ek__BackingField_2() const { return ___U3CTargetValueU3Ek__BackingField_2; }
	inline Quaternion_t2301928331 * get_address_of_U3CTargetValueU3Ek__BackingField_2() { return &___U3CTargetValueU3Ek__BackingField_2; }
	inline void set_U3CTargetValueU3Ek__BackingField_2(Quaternion_t2301928331  value)
	{
		___U3CTargetValueU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CStartValueU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(QuaternionInterpolated_t376031970, ___U3CStartValueU3Ek__BackingField_3)); }
	inline Quaternion_t2301928331  get_U3CStartValueU3Ek__BackingField_3() const { return ___U3CStartValueU3Ek__BackingField_3; }
	inline Quaternion_t2301928331 * get_address_of_U3CStartValueU3Ek__BackingField_3() { return &___U3CStartValueU3Ek__BackingField_3; }
	inline void set_U3CStartValueU3Ek__BackingField_3(Quaternion_t2301928331  value)
	{
		___U3CStartValueU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CDurationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(QuaternionInterpolated_t376031970, ___U3CDurationU3Ek__BackingField_4)); }
	inline float get_U3CDurationU3Ek__BackingField_4() const { return ___U3CDurationU3Ek__BackingField_4; }
	inline float* get_address_of_U3CDurationU3Ek__BackingField_4() { return &___U3CDurationU3Ek__BackingField_4; }
	inline void set_U3CDurationU3Ek__BackingField_4(float value)
	{
		___U3CDurationU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CCounterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(QuaternionInterpolated_t376031970, ___U3CCounterU3Ek__BackingField_5)); }
	inline float get_U3CCounterU3Ek__BackingField_5() const { return ___U3CCounterU3Ek__BackingField_5; }
	inline float* get_address_of_U3CCounterU3Ek__BackingField_5() { return &___U3CCounterU3Ek__BackingField_5; }
	inline void set_U3CCounterU3Ek__BackingField_5(float value)
	{
		___U3CCounterU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONINTERPOLATED_T376031970_H
#ifndef ANCHOROPERATION_T2663234089_H
#define ANCHOROPERATION_T2663234089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.WorldAnchorManager/AnchorOperation
struct  AnchorOperation_t2663234089 
{
public:
	// System.Int32 HoloToolkit.Unity.WorldAnchorManager/AnchorOperation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnchorOperation_t2663234089, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHOROPERATION_T2663234089_H
#ifndef VECTOR3INTERPOLATED_T2867391164_H
#define VECTOR3INTERPOLATED_T2867391164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Vector3Interpolated
struct  Vector3Interpolated_t2867391164  : public RuntimeObject
{
public:
	// System.Single HoloToolkit.Unity.Vector3Interpolated::HalfLife
	float ___HalfLife_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.Vector3Interpolated::<Value>k__BackingField
	Vector3_t3722313464  ___U3CValueU3Ek__BackingField_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.Vector3Interpolated::<TargetValue>k__BackingField
	Vector3_t3722313464  ___U3CTargetValueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_HalfLife_0() { return static_cast<int32_t>(offsetof(Vector3Interpolated_t2867391164, ___HalfLife_0)); }
	inline float get_HalfLife_0() const { return ___HalfLife_0; }
	inline float* get_address_of_HalfLife_0() { return &___HalfLife_0; }
	inline void set_HalfLife_0(float value)
	{
		___HalfLife_0 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Vector3Interpolated_t2867391164, ___U3CValueU3Ek__BackingField_1)); }
	inline Vector3_t3722313464  get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline Vector3_t3722313464 * get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(Vector3_t3722313464  value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTargetValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vector3Interpolated_t2867391164, ___U3CTargetValueU3Ek__BackingField_2)); }
	inline Vector3_t3722313464  get_U3CTargetValueU3Ek__BackingField_2() const { return ___U3CTargetValueU3Ek__BackingField_2; }
	inline Vector3_t3722313464 * get_address_of_U3CTargetValueU3Ek__BackingField_2() { return &___U3CTargetValueU3Ek__BackingField_2; }
	inline void set_U3CTargetValueU3Ek__BackingField_2(Vector3_t3722313464  value)
	{
		___U3CTargetValueU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3INTERPOLATED_T2867391164_H
#ifndef AUDIOEVENTATTRIBUTE_T4038497407_H
#define AUDIOEVENTATTRIBUTE_T4038497407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioEventAttribute
struct  AudioEventAttribute_t4038497407  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOEVENTATTRIBUTE_T4038497407_H
#ifndef AUDIOEVENTINSTANCEBEHAVIOR_T1649659247_H
#define AUDIOEVENTINSTANCEBEHAVIOR_T1649659247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioEventInstanceBehavior
struct  AudioEventInstanceBehavior_t1649659247 
{
public:
	// System.Int32 HoloToolkit.Unity.AudioEventInstanceBehavior::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AudioEventInstanceBehavior_t1649659247, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOEVENTINSTANCEBEHAVIOR_T1649659247_H
#ifndef VECTORROLLINGSTATISTICS_T1281701527_H
#define VECTORROLLINGSTATISTICS_T1281701527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.VectorRollingStatistics
struct  VectorRollingStatistics_t1281701527  : public RuntimeObject
{
public:
	// System.Single HoloToolkit.Unity.VectorRollingStatistics::CurrentStandardDeviation
	float ___CurrentStandardDeviation_0;
	// System.Single HoloToolkit.Unity.VectorRollingStatistics::StandardDeviationDeltaAfterLatestSample
	float ___StandardDeviationDeltaAfterLatestSample_1;
	// System.Single HoloToolkit.Unity.VectorRollingStatistics::StandardDeviationsAwayOfLatestSample
	float ___StandardDeviationsAwayOfLatestSample_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.VectorRollingStatistics::Average
	Vector3_t3722313464  ___Average_3;
	// System.Single HoloToolkit.Unity.VectorRollingStatistics::ActualSampleCount
	float ___ActualSampleCount_4;
	// System.Int32 HoloToolkit.Unity.VectorRollingStatistics::currentSampleIndex
	int32_t ___currentSampleIndex_5;
	// UnityEngine.Vector3[] HoloToolkit.Unity.VectorRollingStatistics::samples
	Vector3U5BU5D_t1718750761* ___samples_6;
	// UnityEngine.Vector3 HoloToolkit.Unity.VectorRollingStatistics::cumulativeFrame
	Vector3_t3722313464  ___cumulativeFrame_7;
	// UnityEngine.Vector3 HoloToolkit.Unity.VectorRollingStatistics::cumulativeFrameSquared
	Vector3_t3722313464  ___cumulativeFrameSquared_8;
	// System.Int32 HoloToolkit.Unity.VectorRollingStatistics::cumulativeFrameSamples
	int32_t ___cumulativeFrameSamples_9;
	// System.Int32 HoloToolkit.Unity.VectorRollingStatistics::maxSamples
	int32_t ___maxSamples_10;

public:
	inline static int32_t get_offset_of_CurrentStandardDeviation_0() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_t1281701527, ___CurrentStandardDeviation_0)); }
	inline float get_CurrentStandardDeviation_0() const { return ___CurrentStandardDeviation_0; }
	inline float* get_address_of_CurrentStandardDeviation_0() { return &___CurrentStandardDeviation_0; }
	inline void set_CurrentStandardDeviation_0(float value)
	{
		___CurrentStandardDeviation_0 = value;
	}

	inline static int32_t get_offset_of_StandardDeviationDeltaAfterLatestSample_1() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_t1281701527, ___StandardDeviationDeltaAfterLatestSample_1)); }
	inline float get_StandardDeviationDeltaAfterLatestSample_1() const { return ___StandardDeviationDeltaAfterLatestSample_1; }
	inline float* get_address_of_StandardDeviationDeltaAfterLatestSample_1() { return &___StandardDeviationDeltaAfterLatestSample_1; }
	inline void set_StandardDeviationDeltaAfterLatestSample_1(float value)
	{
		___StandardDeviationDeltaAfterLatestSample_1 = value;
	}

	inline static int32_t get_offset_of_StandardDeviationsAwayOfLatestSample_2() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_t1281701527, ___StandardDeviationsAwayOfLatestSample_2)); }
	inline float get_StandardDeviationsAwayOfLatestSample_2() const { return ___StandardDeviationsAwayOfLatestSample_2; }
	inline float* get_address_of_StandardDeviationsAwayOfLatestSample_2() { return &___StandardDeviationsAwayOfLatestSample_2; }
	inline void set_StandardDeviationsAwayOfLatestSample_2(float value)
	{
		___StandardDeviationsAwayOfLatestSample_2 = value;
	}

	inline static int32_t get_offset_of_Average_3() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_t1281701527, ___Average_3)); }
	inline Vector3_t3722313464  get_Average_3() const { return ___Average_3; }
	inline Vector3_t3722313464 * get_address_of_Average_3() { return &___Average_3; }
	inline void set_Average_3(Vector3_t3722313464  value)
	{
		___Average_3 = value;
	}

	inline static int32_t get_offset_of_ActualSampleCount_4() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_t1281701527, ___ActualSampleCount_4)); }
	inline float get_ActualSampleCount_4() const { return ___ActualSampleCount_4; }
	inline float* get_address_of_ActualSampleCount_4() { return &___ActualSampleCount_4; }
	inline void set_ActualSampleCount_4(float value)
	{
		___ActualSampleCount_4 = value;
	}

	inline static int32_t get_offset_of_currentSampleIndex_5() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_t1281701527, ___currentSampleIndex_5)); }
	inline int32_t get_currentSampleIndex_5() const { return ___currentSampleIndex_5; }
	inline int32_t* get_address_of_currentSampleIndex_5() { return &___currentSampleIndex_5; }
	inline void set_currentSampleIndex_5(int32_t value)
	{
		___currentSampleIndex_5 = value;
	}

	inline static int32_t get_offset_of_samples_6() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_t1281701527, ___samples_6)); }
	inline Vector3U5BU5D_t1718750761* get_samples_6() const { return ___samples_6; }
	inline Vector3U5BU5D_t1718750761** get_address_of_samples_6() { return &___samples_6; }
	inline void set_samples_6(Vector3U5BU5D_t1718750761* value)
	{
		___samples_6 = value;
		Il2CppCodeGenWriteBarrier((&___samples_6), value);
	}

	inline static int32_t get_offset_of_cumulativeFrame_7() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_t1281701527, ___cumulativeFrame_7)); }
	inline Vector3_t3722313464  get_cumulativeFrame_7() const { return ___cumulativeFrame_7; }
	inline Vector3_t3722313464 * get_address_of_cumulativeFrame_7() { return &___cumulativeFrame_7; }
	inline void set_cumulativeFrame_7(Vector3_t3722313464  value)
	{
		___cumulativeFrame_7 = value;
	}

	inline static int32_t get_offset_of_cumulativeFrameSquared_8() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_t1281701527, ___cumulativeFrameSquared_8)); }
	inline Vector3_t3722313464  get_cumulativeFrameSquared_8() const { return ___cumulativeFrameSquared_8; }
	inline Vector3_t3722313464 * get_address_of_cumulativeFrameSquared_8() { return &___cumulativeFrameSquared_8; }
	inline void set_cumulativeFrameSquared_8(Vector3_t3722313464  value)
	{
		___cumulativeFrameSquared_8 = value;
	}

	inline static int32_t get_offset_of_cumulativeFrameSamples_9() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_t1281701527, ___cumulativeFrameSamples_9)); }
	inline int32_t get_cumulativeFrameSamples_9() const { return ___cumulativeFrameSamples_9; }
	inline int32_t* get_address_of_cumulativeFrameSamples_9() { return &___cumulativeFrameSamples_9; }
	inline void set_cumulativeFrameSamples_9(int32_t value)
	{
		___cumulativeFrameSamples_9 = value;
	}

	inline static int32_t get_offset_of_maxSamples_10() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_t1281701527, ___maxSamples_10)); }
	inline int32_t get_maxSamples_10() const { return ___maxSamples_10; }
	inline int32_t* get_address_of_maxSamples_10() { return &___maxSamples_10; }
	inline void set_maxSamples_10(int32_t value)
	{
		___maxSamples_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORROLLINGSTATISTICS_T1281701527_H
#ifndef MICROPHONESTATUS_T4155313596_H
#define MICROPHONESTATUS_T4155313596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MicrophoneStatus
struct  MicrophoneStatus_t4155313596 
{
public:
	// System.Int32 HoloToolkit.Unity.MicrophoneStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MicrophoneStatus_t4155313596, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROPHONESTATUS_T4155313596_H
#ifndef SPATIALPOSITIONINGTYPE_T2231436405_H
#define SPATIALPOSITIONINGTYPE_T2231436405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialPositioningType
struct  SpatialPositioningType_t2231436405 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialPositioningType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpatialPositioningType_t2231436405, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALPOSITIONINGTYPE_T2231436405_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T3234723286_H
#define U3CU3EC__DISPLAYCLASS22_0_T3234723286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MathUtils/<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t3234723286  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.MathUtils/<>c__DisplayClass22_0::nearestPoint
	Vector3_t3722313464  ___nearestPoint_0;
	// System.Single HoloToolkit.Unity.MathUtils/<>c__DisplayClass22_0::ransac_threshold
	float ___ransac_threshold_1;

public:
	inline static int32_t get_offset_of_nearestPoint_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t3234723286, ___nearestPoint_0)); }
	inline Vector3_t3722313464  get_nearestPoint_0() const { return ___nearestPoint_0; }
	inline Vector3_t3722313464 * get_address_of_nearestPoint_0() { return &___nearestPoint_0; }
	inline void set_nearestPoint_0(Vector3_t3722313464  value)
	{
		___nearestPoint_0 = value;
	}

	inline static int32_t get_offset_of_ransac_threshold_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t3234723286, ___ransac_threshold_1)); }
	inline float get_ransac_threshold_1() const { return ___ransac_threshold_1; }
	inline float* get_address_of_ransac_threshold_1() { return &___ransac_threshold_1; }
	inline void set_ransac_threshold_1(float value)
	{
		___ransac_threshold_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T3234723286_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef AUDIOCONTAINERTYPE_T399292031_H
#define AUDIOCONTAINERTYPE_T399292031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioContainerType
struct  AudioContainerType_t399292031 
{
public:
	// System.Int32 HoloToolkit.Unity.AudioContainerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AudioContainerType_t399292031, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCONTAINERTYPE_T399292031_H
#ifndef SCANSTATES_T3301420450_H
#define SCANSTATES_T3301420450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstanding/ScanStates
struct  ScanStates_t3301420450 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstanding/ScanStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScanStates_t3301420450, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANSTATES_T3301420450_H
#ifndef SPATIALSOUNDROOMSIZES_T1648336194_H
#define SPATIALSOUNDROOMSIZES_T1648336194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialSoundRoomSizes
struct  SpatialSoundRoomSizes_t1648336194 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialSoundRoomSizes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpatialSoundRoomSizes_t1648336194, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALSOUNDROOMSIZES_T1648336194_H
#ifndef SPATIALSOUNDPARAMETERS_T3931834461_H
#define SPATIALSOUNDPARAMETERS_T3931834461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialSoundSettings/SpatialSoundParameters
struct  SpatialSoundParameters_t3931834461 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialSoundSettings/SpatialSoundParameters::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpatialSoundParameters_t3931834461, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALSOUNDPARAMETERS_T3931834461_H
#ifndef U3CGETMICROPHONESTATUSU3ED__1_T3955005665_H
#define U3CGETMICROPHONESTATUSU3ED__1_T3955005665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MicrophoneHelper/<GetMicrophoneStatus>d__1
struct  U3CGetMicrophoneStatusU3Ed__1_t3955005665  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.MicrophoneHelper/<GetMicrophoneStatus>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<HoloToolkit.Unity.MicrophoneStatus> HoloToolkit.Unity.MicrophoneHelper/<GetMicrophoneStatus>d__1::<>t__builder
	AsyncTaskMethodBuilder_1_t2181320810  ___U3CU3Et__builder_1;
	// Windows.Media.Capture.MediaCaptureInitializationSettings HoloToolkit.Unity.MicrophoneHelper/<GetMicrophoneStatus>d__1::<settings>5__1
	MediaCaptureInitializationSettings_t546830356 * ___U3CsettingsU3E5__1_2;
	// Windows.Media.Capture.MediaCapture HoloToolkit.Unity.MicrophoneHelper/<GetMicrophoneStatus>d__1::<capture>5__2
	MediaCapture_t1516581975 * ___U3CcaptureU3E5__2_3;
	// System.Exception HoloToolkit.Unity.MicrophoneHelper/<GetMicrophoneStatus>d__1::<exception>5__3
	Exception_t1436737249 * ___U3CexceptionU3E5__3_4;
	// System.Runtime.CompilerServices.TaskAwaiter HoloToolkit.Unity.MicrophoneHelper/<GetMicrophoneStatus>d__1::<>u__1
	TaskAwaiter_t919683548  ___U3CU3Eu__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetMicrophoneStatusU3Ed__1_t3955005665, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CGetMicrophoneStatusU3Ed__1_t3955005665, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t2181320810  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t2181320810 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t2181320810  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CsettingsU3E5__1_2() { return static_cast<int32_t>(offsetof(U3CGetMicrophoneStatusU3Ed__1_t3955005665, ___U3CsettingsU3E5__1_2)); }
	inline MediaCaptureInitializationSettings_t546830356 * get_U3CsettingsU3E5__1_2() const { return ___U3CsettingsU3E5__1_2; }
	inline MediaCaptureInitializationSettings_t546830356 ** get_address_of_U3CsettingsU3E5__1_2() { return &___U3CsettingsU3E5__1_2; }
	inline void set_U3CsettingsU3E5__1_2(MediaCaptureInitializationSettings_t546830356 * value)
	{
		___U3CsettingsU3E5__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3E5__1_2), value);
	}

	inline static int32_t get_offset_of_U3CcaptureU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CGetMicrophoneStatusU3Ed__1_t3955005665, ___U3CcaptureU3E5__2_3)); }
	inline MediaCapture_t1516581975 * get_U3CcaptureU3E5__2_3() const { return ___U3CcaptureU3E5__2_3; }
	inline MediaCapture_t1516581975 ** get_address_of_U3CcaptureU3E5__2_3() { return &___U3CcaptureU3E5__2_3; }
	inline void set_U3CcaptureU3E5__2_3(MediaCapture_t1516581975 * value)
	{
		___U3CcaptureU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcaptureU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CexceptionU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CGetMicrophoneStatusU3Ed__1_t3955005665, ___U3CexceptionU3E5__3_4)); }
	inline Exception_t1436737249 * get_U3CexceptionU3E5__3_4() const { return ___U3CexceptionU3E5__3_4; }
	inline Exception_t1436737249 ** get_address_of_U3CexceptionU3E5__3_4() { return &___U3CexceptionU3E5__3_4; }
	inline void set_U3CexceptionU3E5__3_4(Exception_t1436737249 * value)
	{
		___U3CexceptionU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CexceptionU3E5__3_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CGetMicrophoneStatusU3Ed__1_t3955005665, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_t919683548  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_t919683548 * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_t919683548  value)
	{
		___U3CU3Eu__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETMICROPHONESTATUSU3ED__1_T3955005665_H
#ifndef U3CISMICROPHONEREADYU3ED__2_T1727120308_H
#define U3CISMICROPHONEREADYU3ED__2_T1727120308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MicrophoneHelper/<IsMicrophoneReady>d__2
struct  U3CIsMicrophoneReadyU3Ed__2_t1727120308  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.MicrophoneHelper/<IsMicrophoneReady>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean> HoloToolkit.Unity.MicrophoneHelper/<IsMicrophoneReady>d__2::<>t__builder
	AsyncTaskMethodBuilder_1_t2418262475  ___U3CU3Et__builder_1;
	// HoloToolkit.Unity.MicrophoneStatus HoloToolkit.Unity.MicrophoneHelper/<IsMicrophoneReady>d__2::<status>5__1
	int32_t ___U3CstatusU3E5__1_2;
	// HoloToolkit.Unity.MicrophoneStatus HoloToolkit.Unity.MicrophoneHelper/<IsMicrophoneReady>d__2::<>s__2
	int32_t ___U3CU3Es__2_3;
	// System.Runtime.CompilerServices.TaskAwaiter`1<HoloToolkit.Unity.MicrophoneStatus> HoloToolkit.Unity.MicrophoneHelper/<IsMicrophoneReady>d__2::<>u__1
	TaskAwaiter_1_t2654828731  ___U3CU3Eu__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CIsMicrophoneReadyU3Ed__2_t1727120308, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CIsMicrophoneReadyU3Ed__2_t1727120308, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t2418262475  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t2418262475 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t2418262475  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CstatusU3E5__1_2() { return static_cast<int32_t>(offsetof(U3CIsMicrophoneReadyU3Ed__2_t1727120308, ___U3CstatusU3E5__1_2)); }
	inline int32_t get_U3CstatusU3E5__1_2() const { return ___U3CstatusU3E5__1_2; }
	inline int32_t* get_address_of_U3CstatusU3E5__1_2() { return &___U3CstatusU3E5__1_2; }
	inline void set_U3CstatusU3E5__1_2(int32_t value)
	{
		___U3CstatusU3E5__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3Es__2_3() { return static_cast<int32_t>(offsetof(U3CIsMicrophoneReadyU3Ed__2_t1727120308, ___U3CU3Es__2_3)); }
	inline int32_t get_U3CU3Es__2_3() const { return ___U3CU3Es__2_3; }
	inline int32_t* get_address_of_U3CU3Es__2_3() { return &___U3CU3Es__2_3; }
	inline void set_U3CU3Es__2_3(int32_t value)
	{
		___U3CU3Es__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_4() { return static_cast<int32_t>(offsetof(U3CIsMicrophoneReadyU3Ed__2_t1727120308, ___U3CU3Eu__1_4)); }
	inline TaskAwaiter_1_t2654828731  get_U3CU3Eu__1_4() const { return ___U3CU3Eu__1_4; }
	inline TaskAwaiter_1_t2654828731 * get_address_of_U3CU3Eu__1_4() { return &___U3CU3Eu__1_4; }
	inline void set_U3CU3Eu__1_4(TaskAwaiter_1_t2654828731  value)
	{
		___U3CU3Eu__1_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CISMICROPHONEREADYU3ED__2_T1727120308_H
#ifndef INTERPOLATEDVECTOR3_T3860084216_H
#define INTERPOLATEDVECTOR3_T3860084216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedVector3
struct  InterpolatedVector3_t3860084216  : public InterpolatedValue_1_t1345469368
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDVECTOR3_T3860084216_H
#ifndef COMPARABLERAYCASTRESULT_T244165836_H
#define COMPARABLERAYCASTRESULT_T244165836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ComparableRaycastResult
struct  ComparableRaycastResult_t244165836 
{
public:
	// System.Int32 HoloToolkit.Unity.ComparableRaycastResult::LayerMaskIndex
	int32_t ___LayerMaskIndex_0;
	// UnityEngine.EventSystems.RaycastResult HoloToolkit.Unity.ComparableRaycastResult::RaycastResult
	RaycastResult_t3360306849  ___RaycastResult_1;

public:
	inline static int32_t get_offset_of_LayerMaskIndex_0() { return static_cast<int32_t>(offsetof(ComparableRaycastResult_t244165836, ___LayerMaskIndex_0)); }
	inline int32_t get_LayerMaskIndex_0() const { return ___LayerMaskIndex_0; }
	inline int32_t* get_address_of_LayerMaskIndex_0() { return &___LayerMaskIndex_0; }
	inline void set_LayerMaskIndex_0(int32_t value)
	{
		___LayerMaskIndex_0 = value;
	}

	inline static int32_t get_offset_of_RaycastResult_1() { return static_cast<int32_t>(offsetof(ComparableRaycastResult_t244165836, ___RaycastResult_1)); }
	inline RaycastResult_t3360306849  get_RaycastResult_1() const { return ___RaycastResult_1; }
	inline RaycastResult_t3360306849 * get_address_of_RaycastResult_1() { return &___RaycastResult_1; }
	inline void set_RaycastResult_1(RaycastResult_t3360306849  value)
	{
		___RaycastResult_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.ComparableRaycastResult
struct ComparableRaycastResult_t244165836_marshaled_pinvoke
{
	int32_t ___LayerMaskIndex_0;
	RaycastResult_t3360306849_marshaled_pinvoke ___RaycastResult_1;
};
// Native definition for COM marshalling of HoloToolkit.Unity.ComparableRaycastResult
struct ComparableRaycastResult_t244165836_marshaled_com
{
	int32_t ___LayerMaskIndex_0;
	RaycastResult_t3360306849_marshaled_com ___RaycastResult_1;
};
#endif // COMPARABLERAYCASTRESULT_T244165836_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef ANCHORATTACHMENTINFO_T2388524547_H
#define ANCHORATTACHMENTINFO_T2388524547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo
struct  AnchorAttachmentInfo_t2388524547 
{
public:
	// UnityEngine.GameObject HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo::<AnchoredGameObject>k__BackingField
	GameObject_t1113636619 * ___U3CAnchoredGameObjectU3Ek__BackingField_0;
	// System.String HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo::<AnchorName>k__BackingField
	String_t* ___U3CAnchorNameU3Ek__BackingField_1;
	// HoloToolkit.Unity.WorldAnchorManager/AnchorOperation HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo::<Operation>k__BackingField
	int32_t ___U3COperationU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CAnchoredGameObjectU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AnchorAttachmentInfo_t2388524547, ___U3CAnchoredGameObjectU3Ek__BackingField_0)); }
	inline GameObject_t1113636619 * get_U3CAnchoredGameObjectU3Ek__BackingField_0() const { return ___U3CAnchoredGameObjectU3Ek__BackingField_0; }
	inline GameObject_t1113636619 ** get_address_of_U3CAnchoredGameObjectU3Ek__BackingField_0() { return &___U3CAnchoredGameObjectU3Ek__BackingField_0; }
	inline void set_U3CAnchoredGameObjectU3Ek__BackingField_0(GameObject_t1113636619 * value)
	{
		___U3CAnchoredGameObjectU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnchoredGameObjectU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAnchorNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AnchorAttachmentInfo_t2388524547, ___U3CAnchorNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CAnchorNameU3Ek__BackingField_1() const { return ___U3CAnchorNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CAnchorNameU3Ek__BackingField_1() { return &___U3CAnchorNameU3Ek__BackingField_1; }
	inline void set_U3CAnchorNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CAnchorNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnchorNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3COperationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AnchorAttachmentInfo_t2388524547, ___U3COperationU3Ek__BackingField_2)); }
	inline int32_t get_U3COperationU3Ek__BackingField_2() const { return ___U3COperationU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3COperationU3Ek__BackingField_2() { return &___U3COperationU3Ek__BackingField_2; }
	inline void set_U3COperationU3Ek__BackingField_2(int32_t value)
	{
		___U3COperationU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo
struct AnchorAttachmentInfo_t2388524547_marshaled_pinvoke
{
	GameObject_t1113636619 * ___U3CAnchoredGameObjectU3Ek__BackingField_0;
	char* ___U3CAnchorNameU3Ek__BackingField_1;
	int32_t ___U3COperationU3Ek__BackingField_2;
};
// Native definition for COM marshalling of HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo
struct AnchorAttachmentInfo_t2388524547_marshaled_com
{
	GameObject_t1113636619 * ___U3CAnchoredGameObjectU3Ek__BackingField_0;
	Il2CppChar* ___U3CAnchorNameU3Ek__BackingField_1;
	int32_t ___U3COperationU3Ek__BackingField_2;
};
#endif // ANCHORATTACHMENTINFO_T2388524547_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef INTERPOLATEDVECTOR2_T3860084215_H
#define INTERPOLATEDVECTOR2_T3860084215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedVector2
struct  InterpolatedVector2_t3860084215  : public InterpolatedValue_1_t4074352723
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDVECTOR2_T3860084215_H
#ifndef INTERPOLATEDCOLOR_T3157406735_H
#define INTERPOLATEDCOLOR_T3157406735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedColor
struct  InterpolatedColor_t3157406735  : public InterpolatedValue_1_t178842228
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDCOLOR_T3157406735_H
#ifndef OBJECTPLACEMENTRULE_T559886570_H
#define OBJECTPLACEMENTRULE_T559886570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementRule
#pragma pack(push, tp, 1)
struct  ObjectPlacementRule_t559886570 
{
public:
	// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementRule/ObjectPlacementRuleType HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementRule::Type
	int32_t ___Type_0;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementRule::RuleParam_Int_0
	int32_t ___RuleParam_Int_0_1;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementRule::RuleParam_Float_0
	float ___RuleParam_Float_0_2;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementRule::RuleParam_Float_1
	float ___RuleParam_Float_1_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementRule::RuleParam_Vec3_0
	Vector3_t3722313464  ___RuleParam_Vec3_0_4;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(ObjectPlacementRule_t559886570, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Int_0_1() { return static_cast<int32_t>(offsetof(ObjectPlacementRule_t559886570, ___RuleParam_Int_0_1)); }
	inline int32_t get_RuleParam_Int_0_1() const { return ___RuleParam_Int_0_1; }
	inline int32_t* get_address_of_RuleParam_Int_0_1() { return &___RuleParam_Int_0_1; }
	inline void set_RuleParam_Int_0_1(int32_t value)
	{
		___RuleParam_Int_0_1 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Float_0_2() { return static_cast<int32_t>(offsetof(ObjectPlacementRule_t559886570, ___RuleParam_Float_0_2)); }
	inline float get_RuleParam_Float_0_2() const { return ___RuleParam_Float_0_2; }
	inline float* get_address_of_RuleParam_Float_0_2() { return &___RuleParam_Float_0_2; }
	inline void set_RuleParam_Float_0_2(float value)
	{
		___RuleParam_Float_0_2 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Float_1_3() { return static_cast<int32_t>(offsetof(ObjectPlacementRule_t559886570, ___RuleParam_Float_1_3)); }
	inline float get_RuleParam_Float_1_3() const { return ___RuleParam_Float_1_3; }
	inline float* get_address_of_RuleParam_Float_1_3() { return &___RuleParam_Float_1_3; }
	inline void set_RuleParam_Float_1_3(float value)
	{
		___RuleParam_Float_1_3 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Vec3_0_4() { return static_cast<int32_t>(offsetof(ObjectPlacementRule_t559886570, ___RuleParam_Vec3_0_4)); }
	inline Vector3_t3722313464  get_RuleParam_Vec3_0_4() const { return ___RuleParam_Vec3_0_4; }
	inline Vector3_t3722313464 * get_address_of_RuleParam_Vec3_0_4() { return &___RuleParam_Vec3_0_4; }
	inline void set_RuleParam_Vec3_0_4(Vector3_t3722313464  value)
	{
		___RuleParam_Vec3_0_4 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPLACEMENTRULE_T559886570_H
#ifndef OBJECTPLACEMENTDEFINITION_T2072559250_H
#define OBJECTPLACEMENTDEFINITION_T2072559250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementDefinition
#pragma pack(push, tp, 1)
struct  ObjectPlacementDefinition_t2072559250 
{
public:
	// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementDefinition/PlacementType HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementDefinition::Type
	int32_t ___Type_0;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementDefinition::PlacementParam_Int_0
	int32_t ___PlacementParam_Int_0_1;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementDefinition::PlacementParam_Float_0
	float ___PlacementParam_Float_0_2;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementDefinition::PlacementParam_Float_1
	float ___PlacementParam_Float_1_3;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementDefinition::PlacementParam_Float_2
	float ___PlacementParam_Float_2_4;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementDefinition::PlacementParam_Float_3
	float ___PlacementParam_Float_3_5;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementDefinition::PlacementParam_Str_0
	intptr_t ___PlacementParam_Str_0_6;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementDefinition::WallFlags
	int32_t ___WallFlags_7;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementDefinition::HalfDims
	Vector3_t3722313464  ___HalfDims_8;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t2072559250, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_PlacementParam_Int_0_1() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t2072559250, ___PlacementParam_Int_0_1)); }
	inline int32_t get_PlacementParam_Int_0_1() const { return ___PlacementParam_Int_0_1; }
	inline int32_t* get_address_of_PlacementParam_Int_0_1() { return &___PlacementParam_Int_0_1; }
	inline void set_PlacementParam_Int_0_1(int32_t value)
	{
		___PlacementParam_Int_0_1 = value;
	}

	inline static int32_t get_offset_of_PlacementParam_Float_0_2() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t2072559250, ___PlacementParam_Float_0_2)); }
	inline float get_PlacementParam_Float_0_2() const { return ___PlacementParam_Float_0_2; }
	inline float* get_address_of_PlacementParam_Float_0_2() { return &___PlacementParam_Float_0_2; }
	inline void set_PlacementParam_Float_0_2(float value)
	{
		___PlacementParam_Float_0_2 = value;
	}

	inline static int32_t get_offset_of_PlacementParam_Float_1_3() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t2072559250, ___PlacementParam_Float_1_3)); }
	inline float get_PlacementParam_Float_1_3() const { return ___PlacementParam_Float_1_3; }
	inline float* get_address_of_PlacementParam_Float_1_3() { return &___PlacementParam_Float_1_3; }
	inline void set_PlacementParam_Float_1_3(float value)
	{
		___PlacementParam_Float_1_3 = value;
	}

	inline static int32_t get_offset_of_PlacementParam_Float_2_4() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t2072559250, ___PlacementParam_Float_2_4)); }
	inline float get_PlacementParam_Float_2_4() const { return ___PlacementParam_Float_2_4; }
	inline float* get_address_of_PlacementParam_Float_2_4() { return &___PlacementParam_Float_2_4; }
	inline void set_PlacementParam_Float_2_4(float value)
	{
		___PlacementParam_Float_2_4 = value;
	}

	inline static int32_t get_offset_of_PlacementParam_Float_3_5() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t2072559250, ___PlacementParam_Float_3_5)); }
	inline float get_PlacementParam_Float_3_5() const { return ___PlacementParam_Float_3_5; }
	inline float* get_address_of_PlacementParam_Float_3_5() { return &___PlacementParam_Float_3_5; }
	inline void set_PlacementParam_Float_3_5(float value)
	{
		___PlacementParam_Float_3_5 = value;
	}

	inline static int32_t get_offset_of_PlacementParam_Str_0_6() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t2072559250, ___PlacementParam_Str_0_6)); }
	inline intptr_t get_PlacementParam_Str_0_6() const { return ___PlacementParam_Str_0_6; }
	inline intptr_t* get_address_of_PlacementParam_Str_0_6() { return &___PlacementParam_Str_0_6; }
	inline void set_PlacementParam_Str_0_6(intptr_t value)
	{
		___PlacementParam_Str_0_6 = value;
	}

	inline static int32_t get_offset_of_WallFlags_7() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t2072559250, ___WallFlags_7)); }
	inline int32_t get_WallFlags_7() const { return ___WallFlags_7; }
	inline int32_t* get_address_of_WallFlags_7() { return &___WallFlags_7; }
	inline void set_WallFlags_7(int32_t value)
	{
		___WallFlags_7 = value;
	}

	inline static int32_t get_offset_of_HalfDims_8() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t2072559250, ___HalfDims_8)); }
	inline Vector3_t3722313464  get_HalfDims_8() const { return ___HalfDims_8; }
	inline Vector3_t3722313464 * get_address_of_HalfDims_8() { return &___HalfDims_8; }
	inline void set_HalfDims_8(Vector3_t3722313464  value)
	{
		___HalfDims_8 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPLACEMENTDEFINITION_T2072559250_H
#ifndef RAYCASTRESULT_T1603201782_H
#define RAYCASTRESULT_T1603201782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/RaycastResult
#pragma pack(push, tp, 1)
struct  RaycastResult_t1603201782  : public RuntimeObject
{
public:
	// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/RaycastResult/SurfaceTypes HoloToolkit.Unity.SpatialUnderstandingDll/Imports/RaycastResult::SurfaceType
	int32_t ___SurfaceType_0;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll/Imports/RaycastResult::SurfaceArea
	float ___SurfaceArea_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/RaycastResult::IntersectPoint
	Vector3_t3722313464  ___IntersectPoint_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDll/Imports/RaycastResult::IntersectNormal
	Vector3_t3722313464  ___IntersectNormal_3;

public:
	inline static int32_t get_offset_of_SurfaceType_0() { return static_cast<int32_t>(offsetof(RaycastResult_t1603201782, ___SurfaceType_0)); }
	inline int32_t get_SurfaceType_0() const { return ___SurfaceType_0; }
	inline int32_t* get_address_of_SurfaceType_0() { return &___SurfaceType_0; }
	inline void set_SurfaceType_0(int32_t value)
	{
		___SurfaceType_0 = value;
	}

	inline static int32_t get_offset_of_SurfaceArea_1() { return static_cast<int32_t>(offsetof(RaycastResult_t1603201782, ___SurfaceArea_1)); }
	inline float get_SurfaceArea_1() const { return ___SurfaceArea_1; }
	inline float* get_address_of_SurfaceArea_1() { return &___SurfaceArea_1; }
	inline void set_SurfaceArea_1(float value)
	{
		___SurfaceArea_1 = value;
	}

	inline static int32_t get_offset_of_IntersectPoint_2() { return static_cast<int32_t>(offsetof(RaycastResult_t1603201782, ___IntersectPoint_2)); }
	inline Vector3_t3722313464  get_IntersectPoint_2() const { return ___IntersectPoint_2; }
	inline Vector3_t3722313464 * get_address_of_IntersectPoint_2() { return &___IntersectPoint_2; }
	inline void set_IntersectPoint_2(Vector3_t3722313464  value)
	{
		___IntersectPoint_2 = value;
	}

	inline static int32_t get_offset_of_IntersectNormal_3() { return static_cast<int32_t>(offsetof(RaycastResult_t1603201782, ___IntersectNormal_3)); }
	inline Vector3_t3722313464  get_IntersectNormal_3() const { return ___IntersectNormal_3; }
	inline Vector3_t3722313464 * get_address_of_IntersectNormal_3() { return &___IntersectNormal_3; }
	inline void set_IntersectNormal_3(Vector3_t3722313464  value)
	{
		___IntersectNormal_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/Imports/RaycastResult
#pragma pack(push, tp, 1)
struct RaycastResult_t1603201782_marshaled_pinvoke
{
	int32_t ___SurfaceType_0;
	float ___SurfaceArea_1;
	Vector3_t3722313464  ___IntersectPoint_2;
	Vector3_t3722313464  ___IntersectNormal_3;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/Imports/RaycastResult
#pragma pack(push, tp, 1)
struct RaycastResult_t1603201782_marshaled_com
{
	int32_t ___SurfaceType_0;
	float ___SurfaceArea_1;
	Vector3_t3722313464  ___IntersectPoint_2;
	Vector3_t3722313464  ___IntersectNormal_3;
};
#pragma pack(pop, tp)
#endif // RAYCASTRESULT_T1603201782_H
#ifndef SPATIALSOUNDSETTINGS_T982532882_H
#define SPATIALSOUNDSETTINGS_T982532882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialSoundSettings
struct  SpatialSoundSettings_t982532882  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALSOUNDSETTINGS_T982532882_H
#ifndef OBJECTPLACEMENTCONSTRAINT_T4230355661_H
#define OBJECTPLACEMENTCONSTRAINT_T4230355661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementConstraint
#pragma pack(push, tp, 1)
struct  ObjectPlacementConstraint_t4230355661 
{
public:
	// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementConstraint/ObjectPlacementConstraintType HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementConstraint::Type
	int32_t ___Type_0;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementConstraint::RuleParam_Int_0
	int32_t ___RuleParam_Int_0_1;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementConstraint::RuleParam_Float_0
	float ___RuleParam_Float_0_2;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementConstraint::RuleParam_Float_1
	float ___RuleParam_Float_1_3;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementConstraint::RuleParam_Float_2
	float ___RuleParam_Float_2_4;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementConstraint::RuleParam_Vec3_0
	Vector3_t3722313464  ___RuleParam_Vec3_0_5;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(ObjectPlacementConstraint_t4230355661, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Int_0_1() { return static_cast<int32_t>(offsetof(ObjectPlacementConstraint_t4230355661, ___RuleParam_Int_0_1)); }
	inline int32_t get_RuleParam_Int_0_1() const { return ___RuleParam_Int_0_1; }
	inline int32_t* get_address_of_RuleParam_Int_0_1() { return &___RuleParam_Int_0_1; }
	inline void set_RuleParam_Int_0_1(int32_t value)
	{
		___RuleParam_Int_0_1 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Float_0_2() { return static_cast<int32_t>(offsetof(ObjectPlacementConstraint_t4230355661, ___RuleParam_Float_0_2)); }
	inline float get_RuleParam_Float_0_2() const { return ___RuleParam_Float_0_2; }
	inline float* get_address_of_RuleParam_Float_0_2() { return &___RuleParam_Float_0_2; }
	inline void set_RuleParam_Float_0_2(float value)
	{
		___RuleParam_Float_0_2 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Float_1_3() { return static_cast<int32_t>(offsetof(ObjectPlacementConstraint_t4230355661, ___RuleParam_Float_1_3)); }
	inline float get_RuleParam_Float_1_3() const { return ___RuleParam_Float_1_3; }
	inline float* get_address_of_RuleParam_Float_1_3() { return &___RuleParam_Float_1_3; }
	inline void set_RuleParam_Float_1_3(float value)
	{
		___RuleParam_Float_1_3 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Float_2_4() { return static_cast<int32_t>(offsetof(ObjectPlacementConstraint_t4230355661, ___RuleParam_Float_2_4)); }
	inline float get_RuleParam_Float_2_4() const { return ___RuleParam_Float_2_4; }
	inline float* get_address_of_RuleParam_Float_2_4() { return &___RuleParam_Float_2_4; }
	inline void set_RuleParam_Float_2_4(float value)
	{
		___RuleParam_Float_2_4 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Vec3_0_5() { return static_cast<int32_t>(offsetof(ObjectPlacementConstraint_t4230355661, ___RuleParam_Vec3_0_5)); }
	inline Vector3_t3722313464  get_RuleParam_Vec3_0_5() const { return ___RuleParam_Vec3_0_5; }
	inline Vector3_t3722313464 * get_address_of_RuleParam_Vec3_0_5() { return &___RuleParam_Vec3_0_5; }
	inline void set_RuleParam_Vec3_0_5(Vector3_t3722313464  value)
	{
		___RuleParam_Vec3_0_5 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPLACEMENTCONSTRAINT_T4230355661_H
#ifndef MULTICASTDELEGATE_T157516450_H
#define MULTICASTDELEGATE_T157516450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t157516450  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t157516450, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T157516450_H
#ifndef INTERPOLATEDQUATERNION_T1023115731_H
#define INTERPOLATEDQUATERNION_T1023115731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedQuaternion
struct  InterpolatedQuaternion_t1023115731  : public InterpolatedValue_1_t4220051531
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDQUATERNION_T1023115731_H
#ifndef INTERACTIONSOURCE_T872619030_H
#define INTERACTIONSOURCE_T872619030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.WSA.Input.InteractionSource
struct  InteractionSource_t872619030 
{
public:
	// System.UInt32 UnityEngine.XR.WSA.Input.InteractionSource::m_Id
	uint32_t ___m_Id_0;
	// UnityEngine.XR.WSA.Input.InteractionSourceKind UnityEngine.XR.WSA.Input.InteractionSource::m_SourceKind
	int32_t ___m_SourceKind_1;
	// UnityEngine.XR.WSA.Input.InteractionSourceHandedness UnityEngine.XR.WSA.Input.InteractionSource::m_Handedness
	int32_t ___m_Handedness_2;
	// UnityEngine.XR.WSA.Input.InteractionSourceFlags UnityEngine.XR.WSA.Input.InteractionSource::m_Flags
	int32_t ___m_Flags_3;
	// System.UInt16 UnityEngine.XR.WSA.Input.InteractionSource::m_VendorId
	uint16_t ___m_VendorId_4;
	// System.UInt16 UnityEngine.XR.WSA.Input.InteractionSource::m_ProductId
	uint16_t ___m_ProductId_5;
	// System.UInt16 UnityEngine.XR.WSA.Input.InteractionSource::m_ProductVersion
	uint16_t ___m_ProductVersion_6;

public:
	inline static int32_t get_offset_of_m_Id_0() { return static_cast<int32_t>(offsetof(InteractionSource_t872619030, ___m_Id_0)); }
	inline uint32_t get_m_Id_0() const { return ___m_Id_0; }
	inline uint32_t* get_address_of_m_Id_0() { return &___m_Id_0; }
	inline void set_m_Id_0(uint32_t value)
	{
		___m_Id_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceKind_1() { return static_cast<int32_t>(offsetof(InteractionSource_t872619030, ___m_SourceKind_1)); }
	inline int32_t get_m_SourceKind_1() const { return ___m_SourceKind_1; }
	inline int32_t* get_address_of_m_SourceKind_1() { return &___m_SourceKind_1; }
	inline void set_m_SourceKind_1(int32_t value)
	{
		___m_SourceKind_1 = value;
	}

	inline static int32_t get_offset_of_m_Handedness_2() { return static_cast<int32_t>(offsetof(InteractionSource_t872619030, ___m_Handedness_2)); }
	inline int32_t get_m_Handedness_2() const { return ___m_Handedness_2; }
	inline int32_t* get_address_of_m_Handedness_2() { return &___m_Handedness_2; }
	inline void set_m_Handedness_2(int32_t value)
	{
		___m_Handedness_2 = value;
	}

	inline static int32_t get_offset_of_m_Flags_3() { return static_cast<int32_t>(offsetof(InteractionSource_t872619030, ___m_Flags_3)); }
	inline int32_t get_m_Flags_3() const { return ___m_Flags_3; }
	inline int32_t* get_address_of_m_Flags_3() { return &___m_Flags_3; }
	inline void set_m_Flags_3(int32_t value)
	{
		___m_Flags_3 = value;
	}

	inline static int32_t get_offset_of_m_VendorId_4() { return static_cast<int32_t>(offsetof(InteractionSource_t872619030, ___m_VendorId_4)); }
	inline uint16_t get_m_VendorId_4() const { return ___m_VendorId_4; }
	inline uint16_t* get_address_of_m_VendorId_4() { return &___m_VendorId_4; }
	inline void set_m_VendorId_4(uint16_t value)
	{
		___m_VendorId_4 = value;
	}

	inline static int32_t get_offset_of_m_ProductId_5() { return static_cast<int32_t>(offsetof(InteractionSource_t872619030, ___m_ProductId_5)); }
	inline uint16_t get_m_ProductId_5() const { return ___m_ProductId_5; }
	inline uint16_t* get_address_of_m_ProductId_5() { return &___m_ProductId_5; }
	inline void set_m_ProductId_5(uint16_t value)
	{
		___m_ProductId_5 = value;
	}

	inline static int32_t get_offset_of_m_ProductVersion_6() { return static_cast<int32_t>(offsetof(InteractionSource_t872619030, ___m_ProductVersion_6)); }
	inline uint16_t get_m_ProductVersion_6() const { return ___m_ProductVersion_6; }
	inline uint16_t* get_address_of_m_ProductVersion_6() { return &___m_ProductVersion_6; }
	inline void set_m_ProductVersion_6(uint16_t value)
	{
		___m_ProductVersion_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSOURCE_T872619030_H
#ifndef AUDIOCONTAINER_T974065762_H
#define AUDIOCONTAINER_T974065762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioContainer
struct  AudioContainer_t974065762  : public RuntimeObject
{
public:
	// HoloToolkit.Unity.AudioContainerType HoloToolkit.Unity.AudioContainer::ContainerType
	int32_t ___ContainerType_0;
	// System.Boolean HoloToolkit.Unity.AudioContainer::Looping
	bool ___Looping_1;
	// System.Single HoloToolkit.Unity.AudioContainer::LoopTime
	float ___LoopTime_2;
	// HoloToolkit.Unity.UAudioClip[] HoloToolkit.Unity.AudioContainer::Sounds
	UAudioClipU5BU5D_t3119163315* ___Sounds_3;
	// System.Single HoloToolkit.Unity.AudioContainer::CrossfadeTime
	float ___CrossfadeTime_4;
	// System.Int32 HoloToolkit.Unity.AudioContainer::CurrentClip
	int32_t ___CurrentClip_5;

public:
	inline static int32_t get_offset_of_ContainerType_0() { return static_cast<int32_t>(offsetof(AudioContainer_t974065762, ___ContainerType_0)); }
	inline int32_t get_ContainerType_0() const { return ___ContainerType_0; }
	inline int32_t* get_address_of_ContainerType_0() { return &___ContainerType_0; }
	inline void set_ContainerType_0(int32_t value)
	{
		___ContainerType_0 = value;
	}

	inline static int32_t get_offset_of_Looping_1() { return static_cast<int32_t>(offsetof(AudioContainer_t974065762, ___Looping_1)); }
	inline bool get_Looping_1() const { return ___Looping_1; }
	inline bool* get_address_of_Looping_1() { return &___Looping_1; }
	inline void set_Looping_1(bool value)
	{
		___Looping_1 = value;
	}

	inline static int32_t get_offset_of_LoopTime_2() { return static_cast<int32_t>(offsetof(AudioContainer_t974065762, ___LoopTime_2)); }
	inline float get_LoopTime_2() const { return ___LoopTime_2; }
	inline float* get_address_of_LoopTime_2() { return &___LoopTime_2; }
	inline void set_LoopTime_2(float value)
	{
		___LoopTime_2 = value;
	}

	inline static int32_t get_offset_of_Sounds_3() { return static_cast<int32_t>(offsetof(AudioContainer_t974065762, ___Sounds_3)); }
	inline UAudioClipU5BU5D_t3119163315* get_Sounds_3() const { return ___Sounds_3; }
	inline UAudioClipU5BU5D_t3119163315** get_address_of_Sounds_3() { return &___Sounds_3; }
	inline void set_Sounds_3(UAudioClipU5BU5D_t3119163315* value)
	{
		___Sounds_3 = value;
		Il2CppCodeGenWriteBarrier((&___Sounds_3), value);
	}

	inline static int32_t get_offset_of_CrossfadeTime_4() { return static_cast<int32_t>(offsetof(AudioContainer_t974065762, ___CrossfadeTime_4)); }
	inline float get_CrossfadeTime_4() const { return ___CrossfadeTime_4; }
	inline float* get_address_of_CrossfadeTime_4() { return &___CrossfadeTime_4; }
	inline void set_CrossfadeTime_4(float value)
	{
		___CrossfadeTime_4 = value;
	}

	inline static int32_t get_offset_of_CurrentClip_5() { return static_cast<int32_t>(offsetof(AudioContainer_t974065762, ___CurrentClip_5)); }
	inline int32_t get_CurrentClip_5() const { return ___CurrentClip_5; }
	inline int32_t* get_address_of_CurrentClip_5() { return &___CurrentClip_5; }
	inline void set_CurrentClip_5(int32_t value)
	{
		___CurrentClip_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCONTAINER_T974065762_H
#ifndef AUDIOEVENT_T3236711075_H
#define AUDIOEVENT_T3236711075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioEvent
struct  AudioEvent_t3236711075  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.AudioEvent::Name
	String_t* ___Name_0;
	// HoloToolkit.Unity.SpatialPositioningType HoloToolkit.Unity.AudioEvent::Spatialization
	int32_t ___Spatialization_1;
	// HoloToolkit.Unity.SpatialSoundRoomSizes HoloToolkit.Unity.AudioEvent::RoomSize
	int32_t ___RoomSize_2;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.AudioEvent::AttenuationCurve
	AnimationCurve_t3046754366 * ___AttenuationCurve_3;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.AudioEvent::SpatialCurve
	AnimationCurve_t3046754366 * ___SpatialCurve_4;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.AudioEvent::SpreadCurve
	AnimationCurve_t3046754366 * ___SpreadCurve_5;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.AudioEvent::ReverbCurve
	AnimationCurve_t3046754366 * ___ReverbCurve_6;
	// System.Single HoloToolkit.Unity.AudioEvent::MaxDistanceAttenuation3D
	float ___MaxDistanceAttenuation3D_7;
	// UnityEngine.Audio.AudioMixerGroup HoloToolkit.Unity.AudioEvent::AudioBus
	AudioMixerGroup_t2743564464 * ___AudioBus_8;
	// System.Single HoloToolkit.Unity.AudioEvent::PitchCenter
	float ___PitchCenter_9;
	// System.Single HoloToolkit.Unity.AudioEvent::PitchRandomization
	float ___PitchRandomization_10;
	// System.Single HoloToolkit.Unity.AudioEvent::VolumeCenter
	float ___VolumeCenter_11;
	// System.Single HoloToolkit.Unity.AudioEvent::VolumeRandomization
	float ___VolumeRandomization_12;
	// System.Single HoloToolkit.Unity.AudioEvent::PanCenter
	float ___PanCenter_13;
	// System.Single HoloToolkit.Unity.AudioEvent::PanRandomization
	float ___PanRandomization_14;
	// System.Single HoloToolkit.Unity.AudioEvent::FadeInTime
	float ___FadeInTime_15;
	// System.Single HoloToolkit.Unity.AudioEvent::FadeOutTime
	float ___FadeOutTime_16;
	// System.Int32 HoloToolkit.Unity.AudioEvent::InstanceLimit
	int32_t ___InstanceLimit_17;
	// System.Single HoloToolkit.Unity.AudioEvent::InstanceTimeBuffer
	float ___InstanceTimeBuffer_18;
	// HoloToolkit.Unity.AudioEventInstanceBehavior HoloToolkit.Unity.AudioEvent::AudioEventInstanceBehavior
	int32_t ___AudioEventInstanceBehavior_19;
	// HoloToolkit.Unity.AudioContainer HoloToolkit.Unity.AudioEvent::Container
	AudioContainer_t974065762 * ___Container_20;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Spatialization_1() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___Spatialization_1)); }
	inline int32_t get_Spatialization_1() const { return ___Spatialization_1; }
	inline int32_t* get_address_of_Spatialization_1() { return &___Spatialization_1; }
	inline void set_Spatialization_1(int32_t value)
	{
		___Spatialization_1 = value;
	}

	inline static int32_t get_offset_of_RoomSize_2() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___RoomSize_2)); }
	inline int32_t get_RoomSize_2() const { return ___RoomSize_2; }
	inline int32_t* get_address_of_RoomSize_2() { return &___RoomSize_2; }
	inline void set_RoomSize_2(int32_t value)
	{
		___RoomSize_2 = value;
	}

	inline static int32_t get_offset_of_AttenuationCurve_3() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___AttenuationCurve_3)); }
	inline AnimationCurve_t3046754366 * get_AttenuationCurve_3() const { return ___AttenuationCurve_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_AttenuationCurve_3() { return &___AttenuationCurve_3; }
	inline void set_AttenuationCurve_3(AnimationCurve_t3046754366 * value)
	{
		___AttenuationCurve_3 = value;
		Il2CppCodeGenWriteBarrier((&___AttenuationCurve_3), value);
	}

	inline static int32_t get_offset_of_SpatialCurve_4() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___SpatialCurve_4)); }
	inline AnimationCurve_t3046754366 * get_SpatialCurve_4() const { return ___SpatialCurve_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_SpatialCurve_4() { return &___SpatialCurve_4; }
	inline void set_SpatialCurve_4(AnimationCurve_t3046754366 * value)
	{
		___SpatialCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___SpatialCurve_4), value);
	}

	inline static int32_t get_offset_of_SpreadCurve_5() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___SpreadCurve_5)); }
	inline AnimationCurve_t3046754366 * get_SpreadCurve_5() const { return ___SpreadCurve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_SpreadCurve_5() { return &___SpreadCurve_5; }
	inline void set_SpreadCurve_5(AnimationCurve_t3046754366 * value)
	{
		___SpreadCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___SpreadCurve_5), value);
	}

	inline static int32_t get_offset_of_ReverbCurve_6() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___ReverbCurve_6)); }
	inline AnimationCurve_t3046754366 * get_ReverbCurve_6() const { return ___ReverbCurve_6; }
	inline AnimationCurve_t3046754366 ** get_address_of_ReverbCurve_6() { return &___ReverbCurve_6; }
	inline void set_ReverbCurve_6(AnimationCurve_t3046754366 * value)
	{
		___ReverbCurve_6 = value;
		Il2CppCodeGenWriteBarrier((&___ReverbCurve_6), value);
	}

	inline static int32_t get_offset_of_MaxDistanceAttenuation3D_7() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___MaxDistanceAttenuation3D_7)); }
	inline float get_MaxDistanceAttenuation3D_7() const { return ___MaxDistanceAttenuation3D_7; }
	inline float* get_address_of_MaxDistanceAttenuation3D_7() { return &___MaxDistanceAttenuation3D_7; }
	inline void set_MaxDistanceAttenuation3D_7(float value)
	{
		___MaxDistanceAttenuation3D_7 = value;
	}

	inline static int32_t get_offset_of_AudioBus_8() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___AudioBus_8)); }
	inline AudioMixerGroup_t2743564464 * get_AudioBus_8() const { return ___AudioBus_8; }
	inline AudioMixerGroup_t2743564464 ** get_address_of_AudioBus_8() { return &___AudioBus_8; }
	inline void set_AudioBus_8(AudioMixerGroup_t2743564464 * value)
	{
		___AudioBus_8 = value;
		Il2CppCodeGenWriteBarrier((&___AudioBus_8), value);
	}

	inline static int32_t get_offset_of_PitchCenter_9() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___PitchCenter_9)); }
	inline float get_PitchCenter_9() const { return ___PitchCenter_9; }
	inline float* get_address_of_PitchCenter_9() { return &___PitchCenter_9; }
	inline void set_PitchCenter_9(float value)
	{
		___PitchCenter_9 = value;
	}

	inline static int32_t get_offset_of_PitchRandomization_10() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___PitchRandomization_10)); }
	inline float get_PitchRandomization_10() const { return ___PitchRandomization_10; }
	inline float* get_address_of_PitchRandomization_10() { return &___PitchRandomization_10; }
	inline void set_PitchRandomization_10(float value)
	{
		___PitchRandomization_10 = value;
	}

	inline static int32_t get_offset_of_VolumeCenter_11() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___VolumeCenter_11)); }
	inline float get_VolumeCenter_11() const { return ___VolumeCenter_11; }
	inline float* get_address_of_VolumeCenter_11() { return &___VolumeCenter_11; }
	inline void set_VolumeCenter_11(float value)
	{
		___VolumeCenter_11 = value;
	}

	inline static int32_t get_offset_of_VolumeRandomization_12() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___VolumeRandomization_12)); }
	inline float get_VolumeRandomization_12() const { return ___VolumeRandomization_12; }
	inline float* get_address_of_VolumeRandomization_12() { return &___VolumeRandomization_12; }
	inline void set_VolumeRandomization_12(float value)
	{
		___VolumeRandomization_12 = value;
	}

	inline static int32_t get_offset_of_PanCenter_13() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___PanCenter_13)); }
	inline float get_PanCenter_13() const { return ___PanCenter_13; }
	inline float* get_address_of_PanCenter_13() { return &___PanCenter_13; }
	inline void set_PanCenter_13(float value)
	{
		___PanCenter_13 = value;
	}

	inline static int32_t get_offset_of_PanRandomization_14() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___PanRandomization_14)); }
	inline float get_PanRandomization_14() const { return ___PanRandomization_14; }
	inline float* get_address_of_PanRandomization_14() { return &___PanRandomization_14; }
	inline void set_PanRandomization_14(float value)
	{
		___PanRandomization_14 = value;
	}

	inline static int32_t get_offset_of_FadeInTime_15() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___FadeInTime_15)); }
	inline float get_FadeInTime_15() const { return ___FadeInTime_15; }
	inline float* get_address_of_FadeInTime_15() { return &___FadeInTime_15; }
	inline void set_FadeInTime_15(float value)
	{
		___FadeInTime_15 = value;
	}

	inline static int32_t get_offset_of_FadeOutTime_16() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___FadeOutTime_16)); }
	inline float get_FadeOutTime_16() const { return ___FadeOutTime_16; }
	inline float* get_address_of_FadeOutTime_16() { return &___FadeOutTime_16; }
	inline void set_FadeOutTime_16(float value)
	{
		___FadeOutTime_16 = value;
	}

	inline static int32_t get_offset_of_InstanceLimit_17() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___InstanceLimit_17)); }
	inline int32_t get_InstanceLimit_17() const { return ___InstanceLimit_17; }
	inline int32_t* get_address_of_InstanceLimit_17() { return &___InstanceLimit_17; }
	inline void set_InstanceLimit_17(int32_t value)
	{
		___InstanceLimit_17 = value;
	}

	inline static int32_t get_offset_of_InstanceTimeBuffer_18() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___InstanceTimeBuffer_18)); }
	inline float get_InstanceTimeBuffer_18() const { return ___InstanceTimeBuffer_18; }
	inline float* get_address_of_InstanceTimeBuffer_18() { return &___InstanceTimeBuffer_18; }
	inline void set_InstanceTimeBuffer_18(float value)
	{
		___InstanceTimeBuffer_18 = value;
	}

	inline static int32_t get_offset_of_AudioEventInstanceBehavior_19() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___AudioEventInstanceBehavior_19)); }
	inline int32_t get_AudioEventInstanceBehavior_19() const { return ___AudioEventInstanceBehavior_19; }
	inline int32_t* get_address_of_AudioEventInstanceBehavior_19() { return &___AudioEventInstanceBehavior_19; }
	inline void set_AudioEventInstanceBehavior_19(int32_t value)
	{
		___AudioEventInstanceBehavior_19 = value;
	}

	inline static int32_t get_offset_of_Container_20() { return static_cast<int32_t>(offsetof(AudioEvent_t3236711075, ___Container_20)); }
	inline AudioContainer_t974065762 * get_Container_20() const { return ___Container_20; }
	inline AudioContainer_t974065762 ** get_address_of_Container_20() { return &___Container_20; }
	inline void set_Container_20(AudioContainer_t974065762 * value)
	{
		___Container_20 = value;
		Il2CppCodeGenWriteBarrier((&___Container_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOEVENT_T3236711075_H
#ifndef AUDIOBANK_1_T3411070910_H
#define AUDIOBANK_1_T3411070910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioBank`1<HoloToolkit.Unity.AudioEvent>
struct  AudioBank_1_t3411070910  : public ScriptableObject_t2528358522
{
public:
	// T[] HoloToolkit.Unity.AudioBank`1::Events
	AudioEventU5BU5D_t1217199986* ___Events_2;

public:
	inline static int32_t get_offset_of_Events_2() { return static_cast<int32_t>(offsetof(AudioBank_1_t3411070910, ___Events_2)); }
	inline AudioEventU5BU5D_t1217199986* get_Events_2() const { return ___Events_2; }
	inline AudioEventU5BU5D_t1217199986** get_address_of_Events_2() { return &___Events_2; }
	inline void set_Events_2(AudioEventU5BU5D_t1217199986* value)
	{
		___Events_2 = value;
		Il2CppCodeGenWriteBarrier((&___Events_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOBANK_1_T3411070910_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T1269231438_H
#define U3CU3EC__DISPLAYCLASS3_0_T1269231438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InteractionSourceExtensions/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t1269231438  : public RuntimeObject
{
public:
	// UnityEngine.XR.WSA.Input.InteractionSource HoloToolkit.Unity.InteractionSourceExtensions/<>c__DisplayClass3_0::interactionSource
	InteractionSource_t872619030  ___interactionSource_0;

public:
	inline static int32_t get_offset_of_interactionSource_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t1269231438, ___interactionSource_0)); }
	inline InteractionSource_t872619030  get_interactionSource_0() const { return ___interactionSource_0; }
	inline InteractionSource_t872619030 * get_address_of_interactionSource_0() { return &___interactionSource_0; }
	inline void set_interactionSource_0(InteractionSource_t872619030  value)
	{
		___interactionSource_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T1269231438_H
#ifndef ONSCANDONEDELEGATE_T908476570_H
#define ONSCANDONEDELEGATE_T908476570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstanding/OnScanDoneDelegate
struct  OnScanDoneDelegate_t908476570  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSCANDONEDELEGATE_T908476570_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T3607883598_H
#define U3CU3EC__DISPLAYCLASS2_0_T3607883598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InteractionSourceExtensions/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t3607883598  : public RuntimeObject
{
public:
	// UnityEngine.XR.WSA.Input.InteractionSource HoloToolkit.Unity.InteractionSourceExtensions/<>c__DisplayClass2_0::interactionSource
	InteractionSource_t872619030  ___interactionSource_0;
	// System.Single HoloToolkit.Unity.InteractionSourceExtensions/<>c__DisplayClass2_0::durationInSeconds
	float ___durationInSeconds_1;
	// System.Single HoloToolkit.Unity.InteractionSourceExtensions/<>c__DisplayClass2_0::intensity
	float ___intensity_2;

public:
	inline static int32_t get_offset_of_interactionSource_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t3607883598, ___interactionSource_0)); }
	inline InteractionSource_t872619030  get_interactionSource_0() const { return ___interactionSource_0; }
	inline InteractionSource_t872619030 * get_address_of_interactionSource_0() { return &___interactionSource_0; }
	inline void set_interactionSource_0(InteractionSource_t872619030  value)
	{
		___interactionSource_0 = value;
	}

	inline static int32_t get_offset_of_durationInSeconds_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t3607883598, ___durationInSeconds_1)); }
	inline float get_durationInSeconds_1() const { return ___durationInSeconds_1; }
	inline float* get_address_of_durationInSeconds_1() { return &___durationInSeconds_1; }
	inline void set_durationInSeconds_1(float value)
	{
		___durationInSeconds_1 = value;
	}

	inline static int32_t get_offset_of_intensity_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t3607883598, ___intensity_2)); }
	inline float get_intensity_2() const { return ___intensity_2; }
	inline float* get_address_of_intensity_2() { return &___intensity_2; }
	inline void set_intensity_2(float value)
	{
		___intensity_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T3607883598_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T3990220622_H
#define U3CU3EC__DISPLAYCLASS4_0_T3990220622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InteractionSourceExtensions/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t3990220622  : public RuntimeObject
{
public:
	// UnityEngine.XR.WSA.Input.InteractionSource HoloToolkit.Unity.InteractionSourceExtensions/<>c__DisplayClass4_0::interactionSource
	InteractionSource_t872619030  ___interactionSource_0;
	// Windows.Foundation.IAsyncOperation`1<Windows.Storage.Streams.IRandomAccessStreamWithContentType> HoloToolkit.Unity.InteractionSourceExtensions/<>c__DisplayClass4_0::returnValue
	RuntimeObject* ___returnValue_1;

public:
	inline static int32_t get_offset_of_interactionSource_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t3990220622, ___interactionSource_0)); }
	inline InteractionSource_t872619030  get_interactionSource_0() const { return ___interactionSource_0; }
	inline InteractionSource_t872619030 * get_address_of_interactionSource_0() { return &___interactionSource_0; }
	inline void set_interactionSource_0(InteractionSource_t872619030  value)
	{
		___interactionSource_0 = value;
	}

	inline static int32_t get_offset_of_returnValue_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t3990220622, ___returnValue_1)); }
	inline RuntimeObject* get_returnValue_1() const { return ___returnValue_1; }
	inline RuntimeObject** get_address_of_returnValue_1() { return &___returnValue_1; }
	inline void set_returnValue_1(RuntimeObject* value)
	{
		___returnValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___returnValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T3990220622_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef AUDIOEVENTBANK_T173659183_H
#define AUDIOEVENTBANK_T173659183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioEventBank
struct  AudioEventBank_t173659183  : public AudioBank_1_t3411070910
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOEVENTBANK_T173659183_H
#ifndef AUDIOEMITTER_T393868155_H
#define AUDIOEMITTER_T393868155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioEmitter
struct  AudioEmitter_t393868155  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HoloToolkit.Unity.AudioEmitter::UpdateInterval
	float ___UpdateInterval_2;
	// System.Single HoloToolkit.Unity.AudioEmitter::MaxDistance
	float ___MaxDistance_3;
	// System.Int32 HoloToolkit.Unity.AudioEmitter::MaxObjects
	int32_t ___MaxObjects_4;
	// System.DateTime HoloToolkit.Unity.AudioEmitter::lastUpdate
	DateTime_t3738529785  ___lastUpdate_5;
	// UnityEngine.AudioSource HoloToolkit.Unity.AudioEmitter::audioSource
	AudioSource_t3935305588 * ___audioSource_6;
	// System.Single HoloToolkit.Unity.AudioEmitter::initialAudioSourceVolume
	float ___initialAudioSourceVolume_7;
	// UnityEngine.RaycastHit[] HoloToolkit.Unity.AudioEmitter::hits
	RaycastHitU5BU5D_t1690781147* ___hits_8;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.IAudioInfluencer> HoloToolkit.Unity.AudioEmitter::previousInfluencers
	List_1_t3900577486 * ___previousInfluencers_9;

public:
	inline static int32_t get_offset_of_UpdateInterval_2() { return static_cast<int32_t>(offsetof(AudioEmitter_t393868155, ___UpdateInterval_2)); }
	inline float get_UpdateInterval_2() const { return ___UpdateInterval_2; }
	inline float* get_address_of_UpdateInterval_2() { return &___UpdateInterval_2; }
	inline void set_UpdateInterval_2(float value)
	{
		___UpdateInterval_2 = value;
	}

	inline static int32_t get_offset_of_MaxDistance_3() { return static_cast<int32_t>(offsetof(AudioEmitter_t393868155, ___MaxDistance_3)); }
	inline float get_MaxDistance_3() const { return ___MaxDistance_3; }
	inline float* get_address_of_MaxDistance_3() { return &___MaxDistance_3; }
	inline void set_MaxDistance_3(float value)
	{
		___MaxDistance_3 = value;
	}

	inline static int32_t get_offset_of_MaxObjects_4() { return static_cast<int32_t>(offsetof(AudioEmitter_t393868155, ___MaxObjects_4)); }
	inline int32_t get_MaxObjects_4() const { return ___MaxObjects_4; }
	inline int32_t* get_address_of_MaxObjects_4() { return &___MaxObjects_4; }
	inline void set_MaxObjects_4(int32_t value)
	{
		___MaxObjects_4 = value;
	}

	inline static int32_t get_offset_of_lastUpdate_5() { return static_cast<int32_t>(offsetof(AudioEmitter_t393868155, ___lastUpdate_5)); }
	inline DateTime_t3738529785  get_lastUpdate_5() const { return ___lastUpdate_5; }
	inline DateTime_t3738529785 * get_address_of_lastUpdate_5() { return &___lastUpdate_5; }
	inline void set_lastUpdate_5(DateTime_t3738529785  value)
	{
		___lastUpdate_5 = value;
	}

	inline static int32_t get_offset_of_audioSource_6() { return static_cast<int32_t>(offsetof(AudioEmitter_t393868155, ___audioSource_6)); }
	inline AudioSource_t3935305588 * get_audioSource_6() const { return ___audioSource_6; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_6() { return &___audioSource_6; }
	inline void set_audioSource_6(AudioSource_t3935305588 * value)
	{
		___audioSource_6 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_6), value);
	}

	inline static int32_t get_offset_of_initialAudioSourceVolume_7() { return static_cast<int32_t>(offsetof(AudioEmitter_t393868155, ___initialAudioSourceVolume_7)); }
	inline float get_initialAudioSourceVolume_7() const { return ___initialAudioSourceVolume_7; }
	inline float* get_address_of_initialAudioSourceVolume_7() { return &___initialAudioSourceVolume_7; }
	inline void set_initialAudioSourceVolume_7(float value)
	{
		___initialAudioSourceVolume_7 = value;
	}

	inline static int32_t get_offset_of_hits_8() { return static_cast<int32_t>(offsetof(AudioEmitter_t393868155, ___hits_8)); }
	inline RaycastHitU5BU5D_t1690781147* get_hits_8() const { return ___hits_8; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_hits_8() { return &___hits_8; }
	inline void set_hits_8(RaycastHitU5BU5D_t1690781147* value)
	{
		___hits_8 = value;
		Il2CppCodeGenWriteBarrier((&___hits_8), value);
	}

	inline static int32_t get_offset_of_previousInfluencers_9() { return static_cast<int32_t>(offsetof(AudioEmitter_t393868155, ___previousInfluencers_9)); }
	inline List_1_t3900577486 * get_previousInfluencers_9() const { return ___previousInfluencers_9; }
	inline List_1_t3900577486 ** get_address_of_previousInfluencers_9() { return &___previousInfluencers_9; }
	inline void set_previousInfluencers_9(List_1_t3900577486 * value)
	{
		___previousInfluencers_9 = value;
		Il2CppCodeGenWriteBarrier((&___previousInfluencers_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOEMITTER_T393868155_H
#ifndef SINGLETON_1_T3132114229_H
#define SINGLETON_1_T3132114229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.WorldAnchorManager>
struct  Singleton_1_t3132114229  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t3132114229_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	WorldAnchorManager_t2731889775 * ___instance_2;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t3132114229_StaticFields, ___instance_2)); }
	inline WorldAnchorManager_t2731889775 * get_instance_2() const { return ___instance_2; }
	inline WorldAnchorManager_t2731889775 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(WorldAnchorManager_t2731889775 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_searchForInstance_3() { return static_cast<int32_t>(offsetof(Singleton_1_t3132114229_StaticFields, ___searchForInstance_3)); }
	inline bool get_searchForInstance_3() const { return ___searchForInstance_3; }
	inline bool* get_address_of_searchForInstance_3() { return &___searchForInstance_3; }
	inline void set_searchForInstance_3(bool value)
	{
		___searchForInstance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T3132114229_H
#ifndef AUDIOSOURCESREFERENCE_T2128394248_H
#define AUDIOSOURCESREFERENCE_T2128394248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioSourcesReference
struct  AudioSourcesReference_t2128394248  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AudioSource> HoloToolkit.Unity.AudioSourcesReference::audioSources
	List_1_t1112413034 * ___audioSources_2;

public:
	inline static int32_t get_offset_of_audioSources_2() { return static_cast<int32_t>(offsetof(AudioSourcesReference_t2128394248, ___audioSources_2)); }
	inline List_1_t1112413034 * get_audioSources_2() const { return ___audioSources_2; }
	inline List_1_t1112413034 ** get_address_of_audioSources_2() { return &___audioSources_2; }
	inline void set_audioSources_2(List_1_t1112413034 * value)
	{
		___audioSources_2 = value;
		Il2CppCodeGenWriteBarrier((&___audioSources_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCESREFERENCE_T2128394248_H
#ifndef AUDIOOCCLUDER_T3526511466_H
#define AUDIOOCCLUDER_T3526511466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioOccluder
struct  AudioOccluder_t3526511466  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HoloToolkit.Unity.AudioOccluder::NeutralFrequency
	float ___NeutralFrequency_2;
	// System.Single HoloToolkit.Unity.AudioOccluder::CutoffFrequency
	float ___CutoffFrequency_3;
	// System.Single HoloToolkit.Unity.AudioOccluder::VolumePassThrough
	float ___VolumePassThrough_4;

public:
	inline static int32_t get_offset_of_NeutralFrequency_2() { return static_cast<int32_t>(offsetof(AudioOccluder_t3526511466, ___NeutralFrequency_2)); }
	inline float get_NeutralFrequency_2() const { return ___NeutralFrequency_2; }
	inline float* get_address_of_NeutralFrequency_2() { return &___NeutralFrequency_2; }
	inline void set_NeutralFrequency_2(float value)
	{
		___NeutralFrequency_2 = value;
	}

	inline static int32_t get_offset_of_CutoffFrequency_3() { return static_cast<int32_t>(offsetof(AudioOccluder_t3526511466, ___CutoffFrequency_3)); }
	inline float get_CutoffFrequency_3() const { return ___CutoffFrequency_3; }
	inline float* get_address_of_CutoffFrequency_3() { return &___CutoffFrequency_3; }
	inline void set_CutoffFrequency_3(float value)
	{
		___CutoffFrequency_3 = value;
	}

	inline static int32_t get_offset_of_VolumePassThrough_4() { return static_cast<int32_t>(offsetof(AudioOccluder_t3526511466, ___VolumePassThrough_4)); }
	inline float get_VolumePassThrough_4() const { return ___VolumePassThrough_4; }
	inline float* get_address_of_VolumePassThrough_4() { return &___VolumePassThrough_4; }
	inline void set_VolumePassThrough_4(float value)
	{
		___VolumePassThrough_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOOCCLUDER_T3526511466_H
#ifndef SINGLETON_1_T3601793299_H
#define SINGLETON_1_T3601793299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.FadeScript>
struct  Singleton_1_t3601793299  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t3601793299_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	FadeScript_t3201568845 * ___instance_2;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t3601793299_StaticFields, ___instance_2)); }
	inline FadeScript_t3201568845 * get_instance_2() const { return ___instance_2; }
	inline FadeScript_t3201568845 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(FadeScript_t3201568845 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_searchForInstance_3() { return static_cast<int32_t>(offsetof(Singleton_1_t3601793299_StaticFields, ___searchForInstance_3)); }
	inline bool get_searchForInstance_3() const { return ___searchForInstance_3; }
	inline bool* get_address_of_searchForInstance_3() { return &___searchForInstance_3; }
	inline void set_searchForInstance_3(bool value)
	{
		___searchForInstance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T3601793299_H
#ifndef SPATIALMAPPINGSOURCE_T778736449_H
#define SPATIALMAPPINGSOURCE_T778736449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialMapping.SpatialMappingSource
struct  SpatialMappingSource_t778736449  : public MonoBehaviour_t3962482529
{
public:
	// System.EventHandler`1<HoloToolkit.Unity.SpatialMapping.DataEventArgs`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject>> HoloToolkit.Unity.SpatialMapping.SpatialMappingSource::SurfaceAdded
	EventHandler_1_t826557235 * ___SurfaceAdded_2;
	// System.EventHandler`1<HoloToolkit.Unity.SpatialMapping.DataEventArgs`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceUpdate>> HoloToolkit.Unity.SpatialMapping.SpatialMappingSource::SurfaceUpdated
	EventHandler_1_t2444444190 * ___SurfaceUpdated_3;
	// System.EventHandler`1<HoloToolkit.Unity.SpatialMapping.DataEventArgs`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject>> HoloToolkit.Unity.SpatialMapping.SpatialMappingSource::SurfaceRemoved
	EventHandler_1_t826557235 * ___SurfaceRemoved_4;
	// System.EventHandler`1<System.EventArgs> HoloToolkit.Unity.SpatialMapping.SpatialMappingSource::RemovingAllSurfaces
	EventHandler_1_t1515976428 * ___RemovingAllSurfaces_5;
	// System.Type[] HoloToolkit.Unity.SpatialMapping.SpatialMappingSource::componentsRequiredForSurfaceMesh
	TypeU5BU5D_t3940880105* ___componentsRequiredForSurfaceMesh_6;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject> HoloToolkit.Unity.SpatialMapping.SpatialMappingSource::surfaceObjectsWriteable
	List_1_t4085315060 * ___surfaceObjectsWriteable_7;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject> HoloToolkit.Unity.SpatialMapping.SpatialMappingSource::surfaceObjects
	ReadOnlyCollection_1_t3825816605 * ___surfaceObjects_8;

public:
	inline static int32_t get_offset_of_SurfaceAdded_2() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t778736449, ___SurfaceAdded_2)); }
	inline EventHandler_1_t826557235 * get_SurfaceAdded_2() const { return ___SurfaceAdded_2; }
	inline EventHandler_1_t826557235 ** get_address_of_SurfaceAdded_2() { return &___SurfaceAdded_2; }
	inline void set_SurfaceAdded_2(EventHandler_1_t826557235 * value)
	{
		___SurfaceAdded_2 = value;
		Il2CppCodeGenWriteBarrier((&___SurfaceAdded_2), value);
	}

	inline static int32_t get_offset_of_SurfaceUpdated_3() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t778736449, ___SurfaceUpdated_3)); }
	inline EventHandler_1_t2444444190 * get_SurfaceUpdated_3() const { return ___SurfaceUpdated_3; }
	inline EventHandler_1_t2444444190 ** get_address_of_SurfaceUpdated_3() { return &___SurfaceUpdated_3; }
	inline void set_SurfaceUpdated_3(EventHandler_1_t2444444190 * value)
	{
		___SurfaceUpdated_3 = value;
		Il2CppCodeGenWriteBarrier((&___SurfaceUpdated_3), value);
	}

	inline static int32_t get_offset_of_SurfaceRemoved_4() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t778736449, ___SurfaceRemoved_4)); }
	inline EventHandler_1_t826557235 * get_SurfaceRemoved_4() const { return ___SurfaceRemoved_4; }
	inline EventHandler_1_t826557235 ** get_address_of_SurfaceRemoved_4() { return &___SurfaceRemoved_4; }
	inline void set_SurfaceRemoved_4(EventHandler_1_t826557235 * value)
	{
		___SurfaceRemoved_4 = value;
		Il2CppCodeGenWriteBarrier((&___SurfaceRemoved_4), value);
	}

	inline static int32_t get_offset_of_RemovingAllSurfaces_5() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t778736449, ___RemovingAllSurfaces_5)); }
	inline EventHandler_1_t1515976428 * get_RemovingAllSurfaces_5() const { return ___RemovingAllSurfaces_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_RemovingAllSurfaces_5() { return &___RemovingAllSurfaces_5; }
	inline void set_RemovingAllSurfaces_5(EventHandler_1_t1515976428 * value)
	{
		___RemovingAllSurfaces_5 = value;
		Il2CppCodeGenWriteBarrier((&___RemovingAllSurfaces_5), value);
	}

	inline static int32_t get_offset_of_componentsRequiredForSurfaceMesh_6() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t778736449, ___componentsRequiredForSurfaceMesh_6)); }
	inline TypeU5BU5D_t3940880105* get_componentsRequiredForSurfaceMesh_6() const { return ___componentsRequiredForSurfaceMesh_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_componentsRequiredForSurfaceMesh_6() { return &___componentsRequiredForSurfaceMesh_6; }
	inline void set_componentsRequiredForSurfaceMesh_6(TypeU5BU5D_t3940880105* value)
	{
		___componentsRequiredForSurfaceMesh_6 = value;
		Il2CppCodeGenWriteBarrier((&___componentsRequiredForSurfaceMesh_6), value);
	}

	inline static int32_t get_offset_of_surfaceObjectsWriteable_7() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t778736449, ___surfaceObjectsWriteable_7)); }
	inline List_1_t4085315060 * get_surfaceObjectsWriteable_7() const { return ___surfaceObjectsWriteable_7; }
	inline List_1_t4085315060 ** get_address_of_surfaceObjectsWriteable_7() { return &___surfaceObjectsWriteable_7; }
	inline void set_surfaceObjectsWriteable_7(List_1_t4085315060 * value)
	{
		___surfaceObjectsWriteable_7 = value;
		Il2CppCodeGenWriteBarrier((&___surfaceObjectsWriteable_7), value);
	}

	inline static int32_t get_offset_of_surfaceObjects_8() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t778736449, ___surfaceObjects_8)); }
	inline ReadOnlyCollection_1_t3825816605 * get_surfaceObjects_8() const { return ___surfaceObjects_8; }
	inline ReadOnlyCollection_1_t3825816605 ** get_address_of_surfaceObjects_8() { return &___surfaceObjects_8; }
	inline void set_surfaceObjects_8(ReadOnlyCollection_1_t3825816605 * value)
	{
		___surfaceObjects_8 = value;
		Il2CppCodeGenWriteBarrier((&___surfaceObjects_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALMAPPINGSOURCE_T778736449_H
#ifndef UAUDIOMANAGERBASE_2_T1167026727_H
#define UAUDIOMANAGERBASE_2_T1167026727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UAudioManagerBase`2<HoloToolkit.Unity.AudioEvent,HoloToolkit.Unity.AudioEventBank>
struct  UAudioManagerBase_2_t1167026727  : public MonoBehaviour_t3962482529
{
public:
	// TBank[] HoloToolkit.Unity.UAudioManagerBase`2::DefaultBanks
	AudioEventBankU5BU5D_t218382902* ___DefaultBanks_2;
	// TEvent[] HoloToolkit.Unity.UAudioManagerBase`2::Events
	AudioEventU5BU5D_t1217199986* ___Events_3;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.ActiveEvent> HoloToolkit.Unity.UAudioManagerBase`2::ActiveEvents
	List_1_t3163578750 * ___ActiveEvents_5;
	// System.Collections.Generic.List`1<TBank> HoloToolkit.Unity.UAudioManagerBase`2::LoadedBanks
	List_1_t1645733925 * ___LoadedBanks_6;

public:
	inline static int32_t get_offset_of_DefaultBanks_2() { return static_cast<int32_t>(offsetof(UAudioManagerBase_2_t1167026727, ___DefaultBanks_2)); }
	inline AudioEventBankU5BU5D_t218382902* get_DefaultBanks_2() const { return ___DefaultBanks_2; }
	inline AudioEventBankU5BU5D_t218382902** get_address_of_DefaultBanks_2() { return &___DefaultBanks_2; }
	inline void set_DefaultBanks_2(AudioEventBankU5BU5D_t218382902* value)
	{
		___DefaultBanks_2 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultBanks_2), value);
	}

	inline static int32_t get_offset_of_Events_3() { return static_cast<int32_t>(offsetof(UAudioManagerBase_2_t1167026727, ___Events_3)); }
	inline AudioEventU5BU5D_t1217199986* get_Events_3() const { return ___Events_3; }
	inline AudioEventU5BU5D_t1217199986** get_address_of_Events_3() { return &___Events_3; }
	inline void set_Events_3(AudioEventU5BU5D_t1217199986* value)
	{
		___Events_3 = value;
		Il2CppCodeGenWriteBarrier((&___Events_3), value);
	}

	inline static int32_t get_offset_of_ActiveEvents_5() { return static_cast<int32_t>(offsetof(UAudioManagerBase_2_t1167026727, ___ActiveEvents_5)); }
	inline List_1_t3163578750 * get_ActiveEvents_5() const { return ___ActiveEvents_5; }
	inline List_1_t3163578750 ** get_address_of_ActiveEvents_5() { return &___ActiveEvents_5; }
	inline void set_ActiveEvents_5(List_1_t3163578750 * value)
	{
		___ActiveEvents_5 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveEvents_5), value);
	}

	inline static int32_t get_offset_of_LoadedBanks_6() { return static_cast<int32_t>(offsetof(UAudioManagerBase_2_t1167026727, ___LoadedBanks_6)); }
	inline List_1_t1645733925 * get_LoadedBanks_6() const { return ___LoadedBanks_6; }
	inline List_1_t1645733925 ** get_address_of_LoadedBanks_6() { return &___LoadedBanks_6; }
	inline void set_LoadedBanks_6(List_1_t1645733925 * value)
	{
		___LoadedBanks_6 = value;
		Il2CppCodeGenWriteBarrier((&___LoadedBanks_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UAUDIOMANAGERBASE_2_T1167026727_H
#ifndef SINGLETON_1_T1944907313_H
#define SINGLETON_1_T1944907313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.SpatialUnderstanding>
struct  Singleton_1_t1944907313  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t1944907313_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	SpatialUnderstanding_t1544682859 * ___instance_2;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1944907313_StaticFields, ___instance_2)); }
	inline SpatialUnderstanding_t1544682859 * get_instance_2() const { return ___instance_2; }
	inline SpatialUnderstanding_t1544682859 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(SpatialUnderstanding_t1544682859 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_searchForInstance_3() { return static_cast<int32_t>(offsetof(Singleton_1_t1944907313_StaticFields, ___searchForInstance_3)); }
	inline bool get_searchForInstance_3() const { return ___searchForInstance_3; }
	inline bool* get_address_of_searchForInstance_3() { return &___searchForInstance_3; }
	inline void set_searchForInstance_3(bool value)
	{
		___searchForInstance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1944907313_H
#ifndef CALIBRATIONSPACE_T425571081_H
#define CALIBRATIONSPACE_T425571081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.CalibrationSpace
struct  CalibrationSpace_t425571081  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALIBRATIONSPACE_T425571081_H
#ifndef INTERPOLATOR_T3604653897_H
#define INTERPOLATOR_T3604653897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Interpolator
struct  Interpolator_t3604653897  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean HoloToolkit.Unity.Interpolator::UseUnscaledTime
	bool ___UseUnscaledTime_2;
	// System.Single HoloToolkit.Unity.Interpolator::PositionPerSecond
	float ___PositionPerSecond_4;
	// System.Single HoloToolkit.Unity.Interpolator::RotationDegreesPerSecond
	float ___RotationDegreesPerSecond_5;
	// System.Single HoloToolkit.Unity.Interpolator::RotationSpeedScaler
	float ___RotationSpeedScaler_6;
	// System.Single HoloToolkit.Unity.Interpolator::ScalePerSecond
	float ___ScalePerSecond_7;
	// System.Boolean HoloToolkit.Unity.Interpolator::SmoothLerpToTarget
	bool ___SmoothLerpToTarget_8;
	// System.Single HoloToolkit.Unity.Interpolator::SmoothPositionLerpRatio
	float ___SmoothPositionLerpRatio_9;
	// System.Single HoloToolkit.Unity.Interpolator::SmoothRotationLerpRatio
	float ___SmoothRotationLerpRatio_10;
	// System.Single HoloToolkit.Unity.Interpolator::SmoothScaleLerpRatio
	float ___SmoothScaleLerpRatio_11;
	// UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::targetPosition
	Vector3_t3722313464  ___targetPosition_12;
	// System.Boolean HoloToolkit.Unity.Interpolator::<AnimatingPosition>k__BackingField
	bool ___U3CAnimatingPositionU3Ek__BackingField_13;
	// UnityEngine.Quaternion HoloToolkit.Unity.Interpolator::targetRotation
	Quaternion_t2301928331  ___targetRotation_14;
	// System.Boolean HoloToolkit.Unity.Interpolator::<AnimatingRotation>k__BackingField
	bool ___U3CAnimatingRotationU3Ek__BackingField_15;
	// UnityEngine.Quaternion HoloToolkit.Unity.Interpolator::targetLocalRotation
	Quaternion_t2301928331  ___targetLocalRotation_16;
	// System.Boolean HoloToolkit.Unity.Interpolator::<AnimatingLocalRotation>k__BackingField
	bool ___U3CAnimatingLocalRotationU3Ek__BackingField_17;
	// UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::targetLocalScale
	Vector3_t3722313464  ___targetLocalScale_18;
	// System.Boolean HoloToolkit.Unity.Interpolator::<AnimatingLocalScale>k__BackingField
	bool ___U3CAnimatingLocalScaleU3Ek__BackingField_19;
	// System.Action HoloToolkit.Unity.Interpolator::InterpolationStarted
	Action_t1264377477 * ___InterpolationStarted_20;
	// System.Action HoloToolkit.Unity.Interpolator::InterpolationDone
	Action_t1264377477 * ___InterpolationDone_21;
	// UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::<PositionVelocity>k__BackingField
	Vector3_t3722313464  ___U3CPositionVelocityU3Ek__BackingField_22;
	// UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::oldPosition
	Vector3_t3722313464  ___oldPosition_23;

public:
	inline static int32_t get_offset_of_UseUnscaledTime_2() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___UseUnscaledTime_2)); }
	inline bool get_UseUnscaledTime_2() const { return ___UseUnscaledTime_2; }
	inline bool* get_address_of_UseUnscaledTime_2() { return &___UseUnscaledTime_2; }
	inline void set_UseUnscaledTime_2(bool value)
	{
		___UseUnscaledTime_2 = value;
	}

	inline static int32_t get_offset_of_PositionPerSecond_4() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___PositionPerSecond_4)); }
	inline float get_PositionPerSecond_4() const { return ___PositionPerSecond_4; }
	inline float* get_address_of_PositionPerSecond_4() { return &___PositionPerSecond_4; }
	inline void set_PositionPerSecond_4(float value)
	{
		___PositionPerSecond_4 = value;
	}

	inline static int32_t get_offset_of_RotationDegreesPerSecond_5() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___RotationDegreesPerSecond_5)); }
	inline float get_RotationDegreesPerSecond_5() const { return ___RotationDegreesPerSecond_5; }
	inline float* get_address_of_RotationDegreesPerSecond_5() { return &___RotationDegreesPerSecond_5; }
	inline void set_RotationDegreesPerSecond_5(float value)
	{
		___RotationDegreesPerSecond_5 = value;
	}

	inline static int32_t get_offset_of_RotationSpeedScaler_6() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___RotationSpeedScaler_6)); }
	inline float get_RotationSpeedScaler_6() const { return ___RotationSpeedScaler_6; }
	inline float* get_address_of_RotationSpeedScaler_6() { return &___RotationSpeedScaler_6; }
	inline void set_RotationSpeedScaler_6(float value)
	{
		___RotationSpeedScaler_6 = value;
	}

	inline static int32_t get_offset_of_ScalePerSecond_7() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___ScalePerSecond_7)); }
	inline float get_ScalePerSecond_7() const { return ___ScalePerSecond_7; }
	inline float* get_address_of_ScalePerSecond_7() { return &___ScalePerSecond_7; }
	inline void set_ScalePerSecond_7(float value)
	{
		___ScalePerSecond_7 = value;
	}

	inline static int32_t get_offset_of_SmoothLerpToTarget_8() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___SmoothLerpToTarget_8)); }
	inline bool get_SmoothLerpToTarget_8() const { return ___SmoothLerpToTarget_8; }
	inline bool* get_address_of_SmoothLerpToTarget_8() { return &___SmoothLerpToTarget_8; }
	inline void set_SmoothLerpToTarget_8(bool value)
	{
		___SmoothLerpToTarget_8 = value;
	}

	inline static int32_t get_offset_of_SmoothPositionLerpRatio_9() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___SmoothPositionLerpRatio_9)); }
	inline float get_SmoothPositionLerpRatio_9() const { return ___SmoothPositionLerpRatio_9; }
	inline float* get_address_of_SmoothPositionLerpRatio_9() { return &___SmoothPositionLerpRatio_9; }
	inline void set_SmoothPositionLerpRatio_9(float value)
	{
		___SmoothPositionLerpRatio_9 = value;
	}

	inline static int32_t get_offset_of_SmoothRotationLerpRatio_10() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___SmoothRotationLerpRatio_10)); }
	inline float get_SmoothRotationLerpRatio_10() const { return ___SmoothRotationLerpRatio_10; }
	inline float* get_address_of_SmoothRotationLerpRatio_10() { return &___SmoothRotationLerpRatio_10; }
	inline void set_SmoothRotationLerpRatio_10(float value)
	{
		___SmoothRotationLerpRatio_10 = value;
	}

	inline static int32_t get_offset_of_SmoothScaleLerpRatio_11() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___SmoothScaleLerpRatio_11)); }
	inline float get_SmoothScaleLerpRatio_11() const { return ___SmoothScaleLerpRatio_11; }
	inline float* get_address_of_SmoothScaleLerpRatio_11() { return &___SmoothScaleLerpRatio_11; }
	inline void set_SmoothScaleLerpRatio_11(float value)
	{
		___SmoothScaleLerpRatio_11 = value;
	}

	inline static int32_t get_offset_of_targetPosition_12() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___targetPosition_12)); }
	inline Vector3_t3722313464  get_targetPosition_12() const { return ___targetPosition_12; }
	inline Vector3_t3722313464 * get_address_of_targetPosition_12() { return &___targetPosition_12; }
	inline void set_targetPosition_12(Vector3_t3722313464  value)
	{
		___targetPosition_12 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingPositionU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___U3CAnimatingPositionU3Ek__BackingField_13)); }
	inline bool get_U3CAnimatingPositionU3Ek__BackingField_13() const { return ___U3CAnimatingPositionU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CAnimatingPositionU3Ek__BackingField_13() { return &___U3CAnimatingPositionU3Ek__BackingField_13; }
	inline void set_U3CAnimatingPositionU3Ek__BackingField_13(bool value)
	{
		___U3CAnimatingPositionU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_targetRotation_14() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___targetRotation_14)); }
	inline Quaternion_t2301928331  get_targetRotation_14() const { return ___targetRotation_14; }
	inline Quaternion_t2301928331 * get_address_of_targetRotation_14() { return &___targetRotation_14; }
	inline void set_targetRotation_14(Quaternion_t2301928331  value)
	{
		___targetRotation_14 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingRotationU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___U3CAnimatingRotationU3Ek__BackingField_15)); }
	inline bool get_U3CAnimatingRotationU3Ek__BackingField_15() const { return ___U3CAnimatingRotationU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CAnimatingRotationU3Ek__BackingField_15() { return &___U3CAnimatingRotationU3Ek__BackingField_15; }
	inline void set_U3CAnimatingRotationU3Ek__BackingField_15(bool value)
	{
		___U3CAnimatingRotationU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_targetLocalRotation_16() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___targetLocalRotation_16)); }
	inline Quaternion_t2301928331  get_targetLocalRotation_16() const { return ___targetLocalRotation_16; }
	inline Quaternion_t2301928331 * get_address_of_targetLocalRotation_16() { return &___targetLocalRotation_16; }
	inline void set_targetLocalRotation_16(Quaternion_t2301928331  value)
	{
		___targetLocalRotation_16 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingLocalRotationU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___U3CAnimatingLocalRotationU3Ek__BackingField_17)); }
	inline bool get_U3CAnimatingLocalRotationU3Ek__BackingField_17() const { return ___U3CAnimatingLocalRotationU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CAnimatingLocalRotationU3Ek__BackingField_17() { return &___U3CAnimatingLocalRotationU3Ek__BackingField_17; }
	inline void set_U3CAnimatingLocalRotationU3Ek__BackingField_17(bool value)
	{
		___U3CAnimatingLocalRotationU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_targetLocalScale_18() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___targetLocalScale_18)); }
	inline Vector3_t3722313464  get_targetLocalScale_18() const { return ___targetLocalScale_18; }
	inline Vector3_t3722313464 * get_address_of_targetLocalScale_18() { return &___targetLocalScale_18; }
	inline void set_targetLocalScale_18(Vector3_t3722313464  value)
	{
		___targetLocalScale_18 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingLocalScaleU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___U3CAnimatingLocalScaleU3Ek__BackingField_19)); }
	inline bool get_U3CAnimatingLocalScaleU3Ek__BackingField_19() const { return ___U3CAnimatingLocalScaleU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CAnimatingLocalScaleU3Ek__BackingField_19() { return &___U3CAnimatingLocalScaleU3Ek__BackingField_19; }
	inline void set_U3CAnimatingLocalScaleU3Ek__BackingField_19(bool value)
	{
		___U3CAnimatingLocalScaleU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_InterpolationStarted_20() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___InterpolationStarted_20)); }
	inline Action_t1264377477 * get_InterpolationStarted_20() const { return ___InterpolationStarted_20; }
	inline Action_t1264377477 ** get_address_of_InterpolationStarted_20() { return &___InterpolationStarted_20; }
	inline void set_InterpolationStarted_20(Action_t1264377477 * value)
	{
		___InterpolationStarted_20 = value;
		Il2CppCodeGenWriteBarrier((&___InterpolationStarted_20), value);
	}

	inline static int32_t get_offset_of_InterpolationDone_21() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___InterpolationDone_21)); }
	inline Action_t1264377477 * get_InterpolationDone_21() const { return ___InterpolationDone_21; }
	inline Action_t1264377477 ** get_address_of_InterpolationDone_21() { return &___InterpolationDone_21; }
	inline void set_InterpolationDone_21(Action_t1264377477 * value)
	{
		___InterpolationDone_21 = value;
		Il2CppCodeGenWriteBarrier((&___InterpolationDone_21), value);
	}

	inline static int32_t get_offset_of_U3CPositionVelocityU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___U3CPositionVelocityU3Ek__BackingField_22)); }
	inline Vector3_t3722313464  get_U3CPositionVelocityU3Ek__BackingField_22() const { return ___U3CPositionVelocityU3Ek__BackingField_22; }
	inline Vector3_t3722313464 * get_address_of_U3CPositionVelocityU3Ek__BackingField_22() { return &___U3CPositionVelocityU3Ek__BackingField_22; }
	inline void set_U3CPositionVelocityU3Ek__BackingField_22(Vector3_t3722313464  value)
	{
		___U3CPositionVelocityU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_oldPosition_23() { return static_cast<int32_t>(offsetof(Interpolator_t3604653897, ___oldPosition_23)); }
	inline Vector3_t3722313464  get_oldPosition_23() const { return ___oldPosition_23; }
	inline Vector3_t3722313464 * get_address_of_oldPosition_23() { return &___oldPosition_23; }
	inline void set_oldPosition_23(Vector3_t3722313464  value)
	{
		___oldPosition_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATOR_T3604653897_H
#ifndef STARTAWAREBEHAVIOUR_T3471312134_H
#define STARTAWAREBEHAVIOUR_T3471312134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.StartAwareBehaviour
struct  StartAwareBehaviour_t3471312134  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean HoloToolkit.Unity.StartAwareBehaviour::<IsStarted>k__BackingField
	bool ___U3CIsStartedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CIsStartedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(StartAwareBehaviour_t3471312134, ___U3CIsStartedU3Ek__BackingField_2)); }
	inline bool get_U3CIsStartedU3Ek__BackingField_2() const { return ___U3CIsStartedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsStartedU3Ek__BackingField_2() { return &___U3CIsStartedU3Ek__BackingField_2; }
	inline void set_U3CIsStartedU3Ek__BackingField_2(bool value)
	{
		___U3CIsStartedU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTAWAREBEHAVIOUR_T3471312134_H
#ifndef UAUDIOMANAGER_T15065897_H
#define UAUDIOMANAGER_T15065897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UAudioManager
struct  UAudioManager_t15065897  : public UAudioManagerBase_2_t1167026727
{
public:
	// System.Int32 HoloToolkit.Unity.UAudioManager::globalEventInstanceLimit
	int32_t ___globalEventInstanceLimit_7;
	// HoloToolkit.Unity.AudioEventInstanceBehavior HoloToolkit.Unity.UAudioManager::globalInstanceBehavior
	int32_t ___globalInstanceBehavior_8;
	// System.Func`2<UnityEngine.GameObject,UnityEngine.GameObject> HoloToolkit.Unity.UAudioManager::<AudioEmitterTransform>k__BackingField
	Func_2_t965320650 * ___U3CAudioEmitterTransformU3Ek__BackingField_9;
	// System.Collections.Generic.Dictionary`2<System.String,HoloToolkit.Unity.AudioEvent> HoloToolkit.Unity.UAudioManager::eventsDictionary
	Dictionary_2_t3021967374 * ___eventsDictionary_10;

public:
	inline static int32_t get_offset_of_globalEventInstanceLimit_7() { return static_cast<int32_t>(offsetof(UAudioManager_t15065897, ___globalEventInstanceLimit_7)); }
	inline int32_t get_globalEventInstanceLimit_7() const { return ___globalEventInstanceLimit_7; }
	inline int32_t* get_address_of_globalEventInstanceLimit_7() { return &___globalEventInstanceLimit_7; }
	inline void set_globalEventInstanceLimit_7(int32_t value)
	{
		___globalEventInstanceLimit_7 = value;
	}

	inline static int32_t get_offset_of_globalInstanceBehavior_8() { return static_cast<int32_t>(offsetof(UAudioManager_t15065897, ___globalInstanceBehavior_8)); }
	inline int32_t get_globalInstanceBehavior_8() const { return ___globalInstanceBehavior_8; }
	inline int32_t* get_address_of_globalInstanceBehavior_8() { return &___globalInstanceBehavior_8; }
	inline void set_globalInstanceBehavior_8(int32_t value)
	{
		___globalInstanceBehavior_8 = value;
	}

	inline static int32_t get_offset_of_U3CAudioEmitterTransformU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UAudioManager_t15065897, ___U3CAudioEmitterTransformU3Ek__BackingField_9)); }
	inline Func_2_t965320650 * get_U3CAudioEmitterTransformU3Ek__BackingField_9() const { return ___U3CAudioEmitterTransformU3Ek__BackingField_9; }
	inline Func_2_t965320650 ** get_address_of_U3CAudioEmitterTransformU3Ek__BackingField_9() { return &___U3CAudioEmitterTransformU3Ek__BackingField_9; }
	inline void set_U3CAudioEmitterTransformU3Ek__BackingField_9(Func_2_t965320650 * value)
	{
		___U3CAudioEmitterTransformU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAudioEmitterTransformU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_eventsDictionary_10() { return static_cast<int32_t>(offsetof(UAudioManager_t15065897, ___eventsDictionary_10)); }
	inline Dictionary_2_t3021967374 * get_eventsDictionary_10() const { return ___eventsDictionary_10; }
	inline Dictionary_2_t3021967374 ** get_address_of_eventsDictionary_10() { return &___eventsDictionary_10; }
	inline void set_eventsDictionary_10(Dictionary_2_t3021967374 * value)
	{
		___eventsDictionary_10 = value;
		Il2CppCodeGenWriteBarrier((&___eventsDictionary_10), value);
	}
};

struct UAudioManager_t15065897_StaticFields
{
public:
	// HoloToolkit.Unity.UAudioManager HoloToolkit.Unity.UAudioManager::instance
	UAudioManager_t15065897 * ___instance_11;

public:
	inline static int32_t get_offset_of_instance_11() { return static_cast<int32_t>(offsetof(UAudioManager_t15065897_StaticFields, ___instance_11)); }
	inline UAudioManager_t15065897 * get_instance_11() const { return ___instance_11; }
	inline UAudioManager_t15065897 ** get_address_of_instance_11() { return &___instance_11; }
	inline void set_instance_11(UAudioManager_t15065897 * value)
	{
		___instance_11 = value;
		Il2CppCodeGenWriteBarrier((&___instance_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UAUDIOMANAGER_T15065897_H
#ifndef SPATIALUNDERSTANDINGCUSTOMMESH_T3317789065_H
#define SPATIALUNDERSTANDINGCUSTOMMESH_T3317789065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingCustomMesh
struct  SpatialUnderstandingCustomMesh_t3317789065  : public SpatialMappingSource_t778736449
{
public:
	// System.Single HoloToolkit.Unity.SpatialUnderstandingCustomMesh::ImportMeshPeriod
	float ___ImportMeshPeriod_9;
	// UnityEngine.Material HoloToolkit.Unity.SpatialUnderstandingCustomMesh::meshMaterial
	Material_t340375123 * ___meshMaterial_10;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingCustomMesh::MaxFrameTime
	float ___MaxFrameTime_11;
	// System.Boolean HoloToolkit.Unity.SpatialUnderstandingCustomMesh::CreateMeshColliders
	bool ___CreateMeshColliders_12;
	// System.Boolean HoloToolkit.Unity.SpatialUnderstandingCustomMesh::drawProcessedMesh
	bool ___drawProcessedMesh_13;
	// System.Boolean HoloToolkit.Unity.SpatialUnderstandingCustomMesh::<IsImportActive>k__BackingField
	bool ___U3CIsImportActiveU3Ek__BackingField_14;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingCustomMesh::timeLastImportedMesh
	float ___timeLastImportedMesh_15;
	// HoloToolkit.Unity.SpatialUnderstanding HoloToolkit.Unity.SpatialUnderstandingCustomMesh::spatialUnderstanding
	SpatialUnderstanding_t1544682859 * ___spatialUnderstanding_16;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData> HoloToolkit.Unity.SpatialUnderstandingCustomMesh::meshSectors
	Dictionary_2_t2868937860 * ___meshSectors_17;

public:
	inline static int32_t get_offset_of_ImportMeshPeriod_9() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_t3317789065, ___ImportMeshPeriod_9)); }
	inline float get_ImportMeshPeriod_9() const { return ___ImportMeshPeriod_9; }
	inline float* get_address_of_ImportMeshPeriod_9() { return &___ImportMeshPeriod_9; }
	inline void set_ImportMeshPeriod_9(float value)
	{
		___ImportMeshPeriod_9 = value;
	}

	inline static int32_t get_offset_of_meshMaterial_10() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_t3317789065, ___meshMaterial_10)); }
	inline Material_t340375123 * get_meshMaterial_10() const { return ___meshMaterial_10; }
	inline Material_t340375123 ** get_address_of_meshMaterial_10() { return &___meshMaterial_10; }
	inline void set_meshMaterial_10(Material_t340375123 * value)
	{
		___meshMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&___meshMaterial_10), value);
	}

	inline static int32_t get_offset_of_MaxFrameTime_11() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_t3317789065, ___MaxFrameTime_11)); }
	inline float get_MaxFrameTime_11() const { return ___MaxFrameTime_11; }
	inline float* get_address_of_MaxFrameTime_11() { return &___MaxFrameTime_11; }
	inline void set_MaxFrameTime_11(float value)
	{
		___MaxFrameTime_11 = value;
	}

	inline static int32_t get_offset_of_CreateMeshColliders_12() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_t3317789065, ___CreateMeshColliders_12)); }
	inline bool get_CreateMeshColliders_12() const { return ___CreateMeshColliders_12; }
	inline bool* get_address_of_CreateMeshColliders_12() { return &___CreateMeshColliders_12; }
	inline void set_CreateMeshColliders_12(bool value)
	{
		___CreateMeshColliders_12 = value;
	}

	inline static int32_t get_offset_of_drawProcessedMesh_13() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_t3317789065, ___drawProcessedMesh_13)); }
	inline bool get_drawProcessedMesh_13() const { return ___drawProcessedMesh_13; }
	inline bool* get_address_of_drawProcessedMesh_13() { return &___drawProcessedMesh_13; }
	inline void set_drawProcessedMesh_13(bool value)
	{
		___drawProcessedMesh_13 = value;
	}

	inline static int32_t get_offset_of_U3CIsImportActiveU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_t3317789065, ___U3CIsImportActiveU3Ek__BackingField_14)); }
	inline bool get_U3CIsImportActiveU3Ek__BackingField_14() const { return ___U3CIsImportActiveU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CIsImportActiveU3Ek__BackingField_14() { return &___U3CIsImportActiveU3Ek__BackingField_14; }
	inline void set_U3CIsImportActiveU3Ek__BackingField_14(bool value)
	{
		___U3CIsImportActiveU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_timeLastImportedMesh_15() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_t3317789065, ___timeLastImportedMesh_15)); }
	inline float get_timeLastImportedMesh_15() const { return ___timeLastImportedMesh_15; }
	inline float* get_address_of_timeLastImportedMesh_15() { return &___timeLastImportedMesh_15; }
	inline void set_timeLastImportedMesh_15(float value)
	{
		___timeLastImportedMesh_15 = value;
	}

	inline static int32_t get_offset_of_spatialUnderstanding_16() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_t3317789065, ___spatialUnderstanding_16)); }
	inline SpatialUnderstanding_t1544682859 * get_spatialUnderstanding_16() const { return ___spatialUnderstanding_16; }
	inline SpatialUnderstanding_t1544682859 ** get_address_of_spatialUnderstanding_16() { return &___spatialUnderstanding_16; }
	inline void set_spatialUnderstanding_16(SpatialUnderstanding_t1544682859 * value)
	{
		___spatialUnderstanding_16 = value;
		Il2CppCodeGenWriteBarrier((&___spatialUnderstanding_16), value);
	}

	inline static int32_t get_offset_of_meshSectors_17() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_t3317789065, ___meshSectors_17)); }
	inline Dictionary_2_t2868937860 * get_meshSectors_17() const { return ___meshSectors_17; }
	inline Dictionary_2_t2868937860 ** get_address_of_meshSectors_17() { return &___meshSectors_17; }
	inline void set_meshSectors_17(Dictionary_2_t2868937860 * value)
	{
		___meshSectors_17 = value;
		Il2CppCodeGenWriteBarrier((&___meshSectors_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALUNDERSTANDINGCUSTOMMESH_T3317789065_H
#ifndef SPATIALUNDERSTANDING_T1544682859_H
#define SPATIALUNDERSTANDING_T1544682859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstanding
struct  SpatialUnderstanding_t1544682859  : public Singleton_1_t1944907313
{
public:
	// System.Boolean HoloToolkit.Unity.SpatialUnderstanding::AutoBeginScanning
	bool ___AutoBeginScanning_5;
	// System.Single HoloToolkit.Unity.SpatialUnderstanding::UpdatePeriod_DuringScanning
	float ___UpdatePeriod_DuringScanning_6;
	// System.Single HoloToolkit.Unity.SpatialUnderstanding::UpdatePeriod_AfterScanning
	float ___UpdatePeriod_AfterScanning_7;
	// HoloToolkit.Unity.SpatialUnderstandingDll HoloToolkit.Unity.SpatialUnderstanding::<UnderstandingDLL>k__BackingField
	SpatialUnderstandingDll_t3348055109 * ___U3CUnderstandingDLLU3Ek__BackingField_8;
	// HoloToolkit.Unity.SpatialUnderstandingSourceMesh HoloToolkit.Unity.SpatialUnderstanding::<UnderstandingSourceMesh>k__BackingField
	SpatialUnderstandingSourceMesh_t2539908667 * ___U3CUnderstandingSourceMeshU3Ek__BackingField_9;
	// HoloToolkit.Unity.SpatialUnderstandingCustomMesh HoloToolkit.Unity.SpatialUnderstanding::<UnderstandingCustomMesh>k__BackingField
	SpatialUnderstandingCustomMesh_t3317789065 * ___U3CUnderstandingCustomMeshU3Ek__BackingField_10;
	// HoloToolkit.Unity.SpatialUnderstanding/OnScanDoneDelegate HoloToolkit.Unity.SpatialUnderstanding::OnScanDone
	OnScanDoneDelegate_t908476570 * ___OnScanDone_11;
	// System.Action HoloToolkit.Unity.SpatialUnderstanding::ScanStateChanged
	Action_t1264377477 * ___ScanStateChanged_12;
	// HoloToolkit.Unity.SpatialUnderstanding/ScanStates HoloToolkit.Unity.SpatialUnderstanding::scanState
	int32_t ___scanState_13;
	// System.Single HoloToolkit.Unity.SpatialUnderstanding::timeSinceLastUpdate
	float ___timeSinceLastUpdate_14;

public:
	inline static int32_t get_offset_of_AutoBeginScanning_5() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t1544682859, ___AutoBeginScanning_5)); }
	inline bool get_AutoBeginScanning_5() const { return ___AutoBeginScanning_5; }
	inline bool* get_address_of_AutoBeginScanning_5() { return &___AutoBeginScanning_5; }
	inline void set_AutoBeginScanning_5(bool value)
	{
		___AutoBeginScanning_5 = value;
	}

	inline static int32_t get_offset_of_UpdatePeriod_DuringScanning_6() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t1544682859, ___UpdatePeriod_DuringScanning_6)); }
	inline float get_UpdatePeriod_DuringScanning_6() const { return ___UpdatePeriod_DuringScanning_6; }
	inline float* get_address_of_UpdatePeriod_DuringScanning_6() { return &___UpdatePeriod_DuringScanning_6; }
	inline void set_UpdatePeriod_DuringScanning_6(float value)
	{
		___UpdatePeriod_DuringScanning_6 = value;
	}

	inline static int32_t get_offset_of_UpdatePeriod_AfterScanning_7() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t1544682859, ___UpdatePeriod_AfterScanning_7)); }
	inline float get_UpdatePeriod_AfterScanning_7() const { return ___UpdatePeriod_AfterScanning_7; }
	inline float* get_address_of_UpdatePeriod_AfterScanning_7() { return &___UpdatePeriod_AfterScanning_7; }
	inline void set_UpdatePeriod_AfterScanning_7(float value)
	{
		___UpdatePeriod_AfterScanning_7 = value;
	}

	inline static int32_t get_offset_of_U3CUnderstandingDLLU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t1544682859, ___U3CUnderstandingDLLU3Ek__BackingField_8)); }
	inline SpatialUnderstandingDll_t3348055109 * get_U3CUnderstandingDLLU3Ek__BackingField_8() const { return ___U3CUnderstandingDLLU3Ek__BackingField_8; }
	inline SpatialUnderstandingDll_t3348055109 ** get_address_of_U3CUnderstandingDLLU3Ek__BackingField_8() { return &___U3CUnderstandingDLLU3Ek__BackingField_8; }
	inline void set_U3CUnderstandingDLLU3Ek__BackingField_8(SpatialUnderstandingDll_t3348055109 * value)
	{
		___U3CUnderstandingDLLU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderstandingDLLU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CUnderstandingSourceMeshU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t1544682859, ___U3CUnderstandingSourceMeshU3Ek__BackingField_9)); }
	inline SpatialUnderstandingSourceMesh_t2539908667 * get_U3CUnderstandingSourceMeshU3Ek__BackingField_9() const { return ___U3CUnderstandingSourceMeshU3Ek__BackingField_9; }
	inline SpatialUnderstandingSourceMesh_t2539908667 ** get_address_of_U3CUnderstandingSourceMeshU3Ek__BackingField_9() { return &___U3CUnderstandingSourceMeshU3Ek__BackingField_9; }
	inline void set_U3CUnderstandingSourceMeshU3Ek__BackingField_9(SpatialUnderstandingSourceMesh_t2539908667 * value)
	{
		___U3CUnderstandingSourceMeshU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderstandingSourceMeshU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CUnderstandingCustomMeshU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t1544682859, ___U3CUnderstandingCustomMeshU3Ek__BackingField_10)); }
	inline SpatialUnderstandingCustomMesh_t3317789065 * get_U3CUnderstandingCustomMeshU3Ek__BackingField_10() const { return ___U3CUnderstandingCustomMeshU3Ek__BackingField_10; }
	inline SpatialUnderstandingCustomMesh_t3317789065 ** get_address_of_U3CUnderstandingCustomMeshU3Ek__BackingField_10() { return &___U3CUnderstandingCustomMeshU3Ek__BackingField_10; }
	inline void set_U3CUnderstandingCustomMeshU3Ek__BackingField_10(SpatialUnderstandingCustomMesh_t3317789065 * value)
	{
		___U3CUnderstandingCustomMeshU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderstandingCustomMeshU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_OnScanDone_11() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t1544682859, ___OnScanDone_11)); }
	inline OnScanDoneDelegate_t908476570 * get_OnScanDone_11() const { return ___OnScanDone_11; }
	inline OnScanDoneDelegate_t908476570 ** get_address_of_OnScanDone_11() { return &___OnScanDone_11; }
	inline void set_OnScanDone_11(OnScanDoneDelegate_t908476570 * value)
	{
		___OnScanDone_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnScanDone_11), value);
	}

	inline static int32_t get_offset_of_ScanStateChanged_12() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t1544682859, ___ScanStateChanged_12)); }
	inline Action_t1264377477 * get_ScanStateChanged_12() const { return ___ScanStateChanged_12; }
	inline Action_t1264377477 ** get_address_of_ScanStateChanged_12() { return &___ScanStateChanged_12; }
	inline void set_ScanStateChanged_12(Action_t1264377477 * value)
	{
		___ScanStateChanged_12 = value;
		Il2CppCodeGenWriteBarrier((&___ScanStateChanged_12), value);
	}

	inline static int32_t get_offset_of_scanState_13() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t1544682859, ___scanState_13)); }
	inline int32_t get_scanState_13() const { return ___scanState_13; }
	inline int32_t* get_address_of_scanState_13() { return &___scanState_13; }
	inline void set_scanState_13(int32_t value)
	{
		___scanState_13 = value;
	}

	inline static int32_t get_offset_of_timeSinceLastUpdate_14() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t1544682859, ___timeSinceLastUpdate_14)); }
	inline float get_timeSinceLastUpdate_14() const { return ___timeSinceLastUpdate_14; }
	inline float* get_address_of_timeSinceLastUpdate_14() { return &___timeSinceLastUpdate_14; }
	inline void set_timeSinceLastUpdate_14(float value)
	{
		___timeSinceLastUpdate_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALUNDERSTANDING_T1544682859_H
#ifndef WORLDANCHORMANAGER_T2731889775_H
#define WORLDANCHORMANAGER_T2731889775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.WorldAnchorManager
struct  WorldAnchorManager_t2731889775  : public Singleton_1_t3132114229
{
public:
	// UnityEngine.TextMesh HoloToolkit.Unity.WorldAnchorManager::AnchorDebugText
	TextMesh_t1536577757 * ___AnchorDebugText_4;
	// System.Boolean HoloToolkit.Unity.WorldAnchorManager::ShowDetailedLogs
	bool ___ShowDetailedLogs_5;
	// System.Boolean HoloToolkit.Unity.WorldAnchorManager::PersistentAnchors
	bool ___PersistentAnchors_6;
	// System.Collections.Generic.Queue`1<HoloToolkit.Unity.WorldAnchorManager/AnchorAttachmentInfo> HoloToolkit.Unity.WorldAnchorManager::LocalAnchorOperations
	Queue_1_t2234784041 * ___LocalAnchorOperations_7;
	// UnityEngine.XR.WSA.Persistence.WorldAnchorStore HoloToolkit.Unity.WorldAnchorManager::<AnchorStore>k__BackingField
	WorldAnchorStore_t633400888 * ___U3CAnchorStoreU3Ek__BackingField_8;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> HoloToolkit.Unity.WorldAnchorManager::AnchorGameObjectReferenceList
	Dictionary_2_t898892918 * ___AnchorGameObjectReferenceList_9;

public:
	inline static int32_t get_offset_of_AnchorDebugText_4() { return static_cast<int32_t>(offsetof(WorldAnchorManager_t2731889775, ___AnchorDebugText_4)); }
	inline TextMesh_t1536577757 * get_AnchorDebugText_4() const { return ___AnchorDebugText_4; }
	inline TextMesh_t1536577757 ** get_address_of_AnchorDebugText_4() { return &___AnchorDebugText_4; }
	inline void set_AnchorDebugText_4(TextMesh_t1536577757 * value)
	{
		___AnchorDebugText_4 = value;
		Il2CppCodeGenWriteBarrier((&___AnchorDebugText_4), value);
	}

	inline static int32_t get_offset_of_ShowDetailedLogs_5() { return static_cast<int32_t>(offsetof(WorldAnchorManager_t2731889775, ___ShowDetailedLogs_5)); }
	inline bool get_ShowDetailedLogs_5() const { return ___ShowDetailedLogs_5; }
	inline bool* get_address_of_ShowDetailedLogs_5() { return &___ShowDetailedLogs_5; }
	inline void set_ShowDetailedLogs_5(bool value)
	{
		___ShowDetailedLogs_5 = value;
	}

	inline static int32_t get_offset_of_PersistentAnchors_6() { return static_cast<int32_t>(offsetof(WorldAnchorManager_t2731889775, ___PersistentAnchors_6)); }
	inline bool get_PersistentAnchors_6() const { return ___PersistentAnchors_6; }
	inline bool* get_address_of_PersistentAnchors_6() { return &___PersistentAnchors_6; }
	inline void set_PersistentAnchors_6(bool value)
	{
		___PersistentAnchors_6 = value;
	}

	inline static int32_t get_offset_of_LocalAnchorOperations_7() { return static_cast<int32_t>(offsetof(WorldAnchorManager_t2731889775, ___LocalAnchorOperations_7)); }
	inline Queue_1_t2234784041 * get_LocalAnchorOperations_7() const { return ___LocalAnchorOperations_7; }
	inline Queue_1_t2234784041 ** get_address_of_LocalAnchorOperations_7() { return &___LocalAnchorOperations_7; }
	inline void set_LocalAnchorOperations_7(Queue_1_t2234784041 * value)
	{
		___LocalAnchorOperations_7 = value;
		Il2CppCodeGenWriteBarrier((&___LocalAnchorOperations_7), value);
	}

	inline static int32_t get_offset_of_U3CAnchorStoreU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(WorldAnchorManager_t2731889775, ___U3CAnchorStoreU3Ek__BackingField_8)); }
	inline WorldAnchorStore_t633400888 * get_U3CAnchorStoreU3Ek__BackingField_8() const { return ___U3CAnchorStoreU3Ek__BackingField_8; }
	inline WorldAnchorStore_t633400888 ** get_address_of_U3CAnchorStoreU3Ek__BackingField_8() { return &___U3CAnchorStoreU3Ek__BackingField_8; }
	inline void set_U3CAnchorStoreU3Ek__BackingField_8(WorldAnchorStore_t633400888 * value)
	{
		___U3CAnchorStoreU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnchorStoreU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_AnchorGameObjectReferenceList_9() { return static_cast<int32_t>(offsetof(WorldAnchorManager_t2731889775, ___AnchorGameObjectReferenceList_9)); }
	inline Dictionary_2_t898892918 * get_AnchorGameObjectReferenceList_9() const { return ___AnchorGameObjectReferenceList_9; }
	inline Dictionary_2_t898892918 ** get_address_of_AnchorGameObjectReferenceList_9() { return &___AnchorGameObjectReferenceList_9; }
	inline void set_AnchorGameObjectReferenceList_9(Dictionary_2_t898892918 * value)
	{
		___AnchorGameObjectReferenceList_9 = value;
		Il2CppCodeGenWriteBarrier((&___AnchorGameObjectReferenceList_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDANCHORMANAGER_T2731889775_H
#ifndef FADESCRIPT_T3201568845_H
#define FADESCRIPT_T3201568845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.FadeScript
struct  FadeScript_t3201568845  : public Singleton_1_t3601793299
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADESCRIPT_T3201568845_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5500 = { sizeof (ActionExtensions_t643829156), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5501 = { sizeof (CameraExtensions_t967686036), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5502 = { sizeof (Color32Extensions_t1528800064), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5503 = { sizeof (ComponentExtensions_t1008225482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5504 = { sizeof (U3CEnumerateAncestorsU3Ed__6_t622793014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5504[8] = 
{
	U3CEnumerateAncestorsU3Ed__6_t622793014::get_offset_of_U3CU3E1__state_0(),
	U3CEnumerateAncestorsU3Ed__6_t622793014::get_offset_of_U3CU3E2__current_1(),
	U3CEnumerateAncestorsU3Ed__6_t622793014::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CEnumerateAncestorsU3Ed__6_t622793014::get_offset_of_startTransform_3(),
	U3CEnumerateAncestorsU3Ed__6_t622793014::get_offset_of_U3CU3E3__startTransform_4(),
	U3CEnumerateAncestorsU3Ed__6_t622793014::get_offset_of_includeSelf_5(),
	U3CEnumerateAncestorsU3Ed__6_t622793014::get_offset_of_U3CU3E3__includeSelf_6(),
	U3CEnumerateAncestorsU3Ed__6_t622793014::get_offset_of_U3CtransformU3E5__1_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5505 = { sizeof (EnumerableExtensions_t3128455254), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5506 = { sizeof (EventSystemExtensions_t171429629), -1, sizeof(EventSystemExtensions_t171429629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5506[2] = 
{
	EventSystemExtensions_t171429629_StaticFields::get_offset_of_RaycastResults_0(),
	EventSystemExtensions_t171429629_StaticFields::get_offset_of_RaycastResultComparer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5507 = { sizeof (Extensions_t1944519571), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5508 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5508[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5509 = { sizeof (GameObjectExtensions_t3801208853), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5510 = { sizeof (InteractionSourceExtensions_t135024349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5510[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5511 = { sizeof (U3CU3Ec__DisplayClass2_0_t3607883598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5511[3] = 
{
	U3CU3Ec__DisplayClass2_0_t3607883598::get_offset_of_interactionSource_0(),
	U3CU3Ec__DisplayClass2_0_t3607883598::get_offset_of_durationInSeconds_1(),
	U3CU3Ec__DisplayClass2_0_t3607883598::get_offset_of_intensity_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5512 = { sizeof (U3CU3Ec__DisplayClass3_0_t1269231438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5512[1] = 
{
	U3CU3Ec__DisplayClass3_0_t1269231438::get_offset_of_interactionSource_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5513 = { sizeof (U3CU3Ec__DisplayClass4_0_t3990220622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5513[2] = 
{
	U3CU3Ec__DisplayClass4_0_t3990220622::get_offset_of_interactionSource_0(),
	U3CU3Ec__DisplayClass4_0_t3990220622::get_offset_of_returnValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5514 = { sizeof (LayerExtensions_t2226445283), -1, sizeof(LayerExtensions_t2226445283_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5514[5] = 
{
	0,
	LayerExtensions_t2226445283_StaticFields::get_offset_of_defaultLayer_1(),
	LayerExtensions_t2226445283_StaticFields::get_offset_of_surfaceLayer_2(),
	LayerExtensions_t2226445283_StaticFields::get_offset_of_interactionLayer_3(),
	LayerExtensions_t2226445283_StaticFields::get_offset_of_activationLayer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5515 = { sizeof (MathExtensions_t3477755654), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5516 = { sizeof (TransformExtensions_t3217287941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5517 = { sizeof (U3CEnumerateHierarchyCoreU3Ed__4_t2426800674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5517[10] = 
{
	U3CEnumerateHierarchyCoreU3Ed__4_t2426800674::get_offset_of_U3CU3E1__state_0(),
	U3CEnumerateHierarchyCoreU3Ed__4_t2426800674::get_offset_of_U3CU3E2__current_1(),
	U3CEnumerateHierarchyCoreU3Ed__4_t2426800674::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CEnumerateHierarchyCoreU3Ed__4_t2426800674::get_offset_of_root_3(),
	U3CEnumerateHierarchyCoreU3Ed__4_t2426800674::get_offset_of_U3CU3E3__root_4(),
	U3CEnumerateHierarchyCoreU3Ed__4_t2426800674::get_offset_of_ignore_5(),
	U3CEnumerateHierarchyCoreU3Ed__4_t2426800674::get_offset_of_U3CU3E3__ignore_6(),
	U3CEnumerateHierarchyCoreU3Ed__4_t2426800674::get_offset_of_U3CtransformQueueU3E5__1_7(),
	U3CEnumerateHierarchyCoreU3Ed__4_t2426800674::get_offset_of_U3CparentTransformU3E5__2_8(),
	U3CEnumerateHierarchyCoreU3Ed__4_t2426800674::get_offset_of_U3CiU3E5__3_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5518 = { sizeof (VectorExtensions_t3799715090), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5519 = { sizeof (U3CU3Ec_t2675217287), -1, sizeof(U3CU3Ec_t2675217287_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5519[5] = 
{
	U3CU3Ec_t2675217287_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2675217287_StaticFields::get_offset_of_U3CU3E9__12_0_1(),
	U3CU3Ec_t2675217287_StaticFields::get_offset_of_U3CU3E9__13_0_2(),
	U3CU3Ec_t2675217287_StaticFields::get_offset_of_U3CU3E9__14_0_3(),
	U3CU3Ec_t2675217287_StaticFields::get_offset_of_U3CU3E9__15_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5520 = { sizeof (Int3_t3771461231)+ sizeof (RuntimeObject), sizeof(Int3_t3771461231 ), sizeof(Int3_t3771461231_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5520[11] = 
{
	Int3_t3771461231_StaticFields::get_offset_of_zero_0(),
	Int3_t3771461231_StaticFields::get_offset_of_one_1(),
	Int3_t3771461231_StaticFields::get_offset_of_forward_2(),
	Int3_t3771461231_StaticFields::get_offset_of_back_3(),
	Int3_t3771461231_StaticFields::get_offset_of_up_4(),
	Int3_t3771461231_StaticFields::get_offset_of_down_5(),
	Int3_t3771461231_StaticFields::get_offset_of_left_6(),
	Int3_t3771461231_StaticFields::get_offset_of_right_7(),
	Int3_t3771461231::get_offset_of_x_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Int3_t3771461231::get_offset_of_y_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Int3_t3771461231::get_offset_of_z_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5521 = { sizeof (InterpolatedColor_t3157406735), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5522 = { sizeof (InterpolatedFloat_t3082949779), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5523 = { sizeof (InterpolatedQuaternion_t1023115731), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5524 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5524[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5525 = { sizeof (InterpolatedVector2_t3860084215), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5526 = { sizeof (InterpolatedVector3_t3860084216), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5527 = { sizeof (QuaternionInterpolated_t376031970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5527[6] = 
{
	QuaternionInterpolated_t376031970::get_offset_of_DeltaSpeed_0(),
	QuaternionInterpolated_t376031970::get_offset_of_U3CValueU3Ek__BackingField_1(),
	QuaternionInterpolated_t376031970::get_offset_of_U3CTargetValueU3Ek__BackingField_2(),
	QuaternionInterpolated_t376031970::get_offset_of_U3CStartValueU3Ek__BackingField_3(),
	QuaternionInterpolated_t376031970::get_offset_of_U3CDurationU3Ek__BackingField_4(),
	QuaternionInterpolated_t376031970::get_offset_of_U3CCounterU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5528 = { sizeof (Vector3Interpolated_t2867391164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5528[3] = 
{
	Vector3Interpolated_t2867391164::get_offset_of_HalfLife_0(),
	Vector3Interpolated_t2867391164::get_offset_of_U3CValueU3Ek__BackingField_1(),
	Vector3Interpolated_t2867391164::get_offset_of_U3CTargetValueU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5529 = { sizeof (InterpolationUtilities_t2843552804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5530 = { sizeof (Interpolator_t3604653897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5530[22] = 
{
	Interpolator_t3604653897::get_offset_of_UseUnscaledTime_2(),
	0,
	Interpolator_t3604653897::get_offset_of_PositionPerSecond_4(),
	Interpolator_t3604653897::get_offset_of_RotationDegreesPerSecond_5(),
	Interpolator_t3604653897::get_offset_of_RotationSpeedScaler_6(),
	Interpolator_t3604653897::get_offset_of_ScalePerSecond_7(),
	Interpolator_t3604653897::get_offset_of_SmoothLerpToTarget_8(),
	Interpolator_t3604653897::get_offset_of_SmoothPositionLerpRatio_9(),
	Interpolator_t3604653897::get_offset_of_SmoothRotationLerpRatio_10(),
	Interpolator_t3604653897::get_offset_of_SmoothScaleLerpRatio_11(),
	Interpolator_t3604653897::get_offset_of_targetPosition_12(),
	Interpolator_t3604653897::get_offset_of_U3CAnimatingPositionU3Ek__BackingField_13(),
	Interpolator_t3604653897::get_offset_of_targetRotation_14(),
	Interpolator_t3604653897::get_offset_of_U3CAnimatingRotationU3Ek__BackingField_15(),
	Interpolator_t3604653897::get_offset_of_targetLocalRotation_16(),
	Interpolator_t3604653897::get_offset_of_U3CAnimatingLocalRotationU3Ek__BackingField_17(),
	Interpolator_t3604653897::get_offset_of_targetLocalScale_18(),
	Interpolator_t3604653897::get_offset_of_U3CAnimatingLocalScaleU3Ek__BackingField_19(),
	Interpolator_t3604653897::get_offset_of_InterpolationStarted_20(),
	Interpolator_t3604653897::get_offset_of_InterpolationDone_21(),
	Interpolator_t3604653897::get_offset_of_U3CPositionVelocityU3Ek__BackingField_22(),
	Interpolator_t3604653897::get_offset_of_oldPosition_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5531 = { sizeof (MathUtils_t1302358766), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5532 = { sizeof (U3CU3Ec__DisplayClass22_0_t3234723286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5532[2] = 
{
	U3CU3Ec__DisplayClass22_0_t3234723286::get_offset_of_nearestPoint_0(),
	U3CU3Ec__DisplayClass22_0_t3234723286::get_offset_of_ransac_threshold_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5533 = { sizeof (ComparableRaycastResult_t244165836)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5533[2] = 
{
	ComparableRaycastResult_t244165836::get_offset_of_LayerMaskIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ComparableRaycastResult_t244165836::get_offset_of_RaycastResult_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5534 = { sizeof (RaycastResultComparer_t838146858), -1, sizeof(RaycastResultComparer_t838146858_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5534[1] = 
{
	RaycastResultComparer_t838146858_StaticFields::get_offset_of_Comparers_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5535 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5535[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5536 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5536[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5537 = { sizeof (StartAwareBehaviour_t3471312134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5537[1] = 
{
	StartAwareBehaviour_t3471312134::get_offset_of_U3CIsStartedU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5538 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5538[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5539 = { sizeof (WorldAnchorManager_t2731889775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5539[6] = 
{
	WorldAnchorManager_t2731889775::get_offset_of_AnchorDebugText_4(),
	WorldAnchorManager_t2731889775::get_offset_of_ShowDetailedLogs_5(),
	WorldAnchorManager_t2731889775::get_offset_of_PersistentAnchors_6(),
	WorldAnchorManager_t2731889775::get_offset_of_LocalAnchorOperations_7(),
	WorldAnchorManager_t2731889775::get_offset_of_U3CAnchorStoreU3Ek__BackingField_8(),
	WorldAnchorManager_t2731889775::get_offset_of_AnchorGameObjectReferenceList_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5540 = { sizeof (AnchorAttachmentInfo_t2388524547)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5540[3] = 
{
	AnchorAttachmentInfo_t2388524547::get_offset_of_U3CAnchoredGameObjectU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnchorAttachmentInfo_t2388524547::get_offset_of_U3CAnchorNameU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnchorAttachmentInfo_t2388524547::get_offset_of_U3COperationU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5541 = { sizeof (AnchorOperation_t2663234089)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5541[3] = 
{
	AnchorOperation_t2663234089::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5542 = { sizeof (CalibrationSpace_t425571081), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5543 = { sizeof (RayStep_t2147652507)+ sizeof (RuntimeObject), sizeof(RayStep_t2147652507 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5543[4] = 
{
	RayStep_t2147652507::get_offset_of_U3COriginU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RayStep_t2147652507::get_offset_of_U3CTerminusU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RayStep_t2147652507::get_offset_of_U3CDirectionU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RayStep_t2147652507::get_offset_of_U3CLengthU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5544 = { sizeof (FadeScript_t3201568845), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5545 = { sizeof (MicrophoneStatus_t4155313596)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5545[7] = 
{
	MicrophoneStatus_t4155313596::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5546 = { sizeof (MicrophoneHelper_t2952751248), -1, sizeof(MicrophoneHelper_t2952751248_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5546[1] = 
{
	MicrophoneHelper_t2952751248_StaticFields::get_offset_of_NoCaptureDevicesHResult_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5547 = { sizeof (U3CGetMicrophoneStatusU3Ed__1_t3955005665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5547[6] = 
{
	U3CGetMicrophoneStatusU3Ed__1_t3955005665::get_offset_of_U3CU3E1__state_0(),
	U3CGetMicrophoneStatusU3Ed__1_t3955005665::get_offset_of_U3CU3Et__builder_1(),
	U3CGetMicrophoneStatusU3Ed__1_t3955005665::get_offset_of_U3CsettingsU3E5__1_2(),
	U3CGetMicrophoneStatusU3Ed__1_t3955005665::get_offset_of_U3CcaptureU3E5__2_3(),
	U3CGetMicrophoneStatusU3Ed__1_t3955005665::get_offset_of_U3CexceptionU3E5__3_4(),
	U3CGetMicrophoneStatusU3Ed__1_t3955005665::get_offset_of_U3CU3Eu__1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5548 = { sizeof (U3CIsMicrophoneReadyU3Ed__2_t1727120308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5548[5] = 
{
	U3CIsMicrophoneReadyU3Ed__2_t1727120308::get_offset_of_U3CU3E1__state_0(),
	U3CIsMicrophoneReadyU3Ed__2_t1727120308::get_offset_of_U3CU3Et__builder_1(),
	U3CIsMicrophoneReadyU3Ed__2_t1727120308::get_offset_of_U3CstatusU3E5__1_2(),
	U3CIsMicrophoneReadyU3Ed__2_t1727120308::get_offset_of_U3CU3Es__2_3(),
	U3CIsMicrophoneReadyU3Ed__2_t1727120308::get_offset_of_U3CU3Eu__1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5549 = { sizeof (VectorRollingStatistics_t1281701527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5549[11] = 
{
	VectorRollingStatistics_t1281701527::get_offset_of_CurrentStandardDeviation_0(),
	VectorRollingStatistics_t1281701527::get_offset_of_StandardDeviationDeltaAfterLatestSample_1(),
	VectorRollingStatistics_t1281701527::get_offset_of_StandardDeviationsAwayOfLatestSample_2(),
	VectorRollingStatistics_t1281701527::get_offset_of_Average_3(),
	VectorRollingStatistics_t1281701527::get_offset_of_ActualSampleCount_4(),
	VectorRollingStatistics_t1281701527::get_offset_of_currentSampleIndex_5(),
	VectorRollingStatistics_t1281701527::get_offset_of_samples_6(),
	VectorRollingStatistics_t1281701527::get_offset_of_cumulativeFrame_7(),
	VectorRollingStatistics_t1281701527::get_offset_of_cumulativeFrameSquared_8(),
	VectorRollingStatistics_t1281701527::get_offset_of_cumulativeFrameSamples_9(),
	VectorRollingStatistics_t1281701527::get_offset_of_maxSamples_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5550 = { sizeof (AudioEmitter_t393868155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5550[8] = 
{
	AudioEmitter_t393868155::get_offset_of_UpdateInterval_2(),
	AudioEmitter_t393868155::get_offset_of_MaxDistance_3(),
	AudioEmitter_t393868155::get_offset_of_MaxObjects_4(),
	AudioEmitter_t393868155::get_offset_of_lastUpdate_5(),
	AudioEmitter_t393868155::get_offset_of_audioSource_6(),
	AudioEmitter_t393868155::get_offset_of_initialAudioSourceVolume_7(),
	AudioEmitter_t393868155::get_offset_of_hits_8(),
	AudioEmitter_t393868155::get_offset_of_previousInfluencers_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5551 = { sizeof (AudioOccluder_t3526511466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5551[3] = 
{
	AudioOccluder_t3526511466::get_offset_of_NeutralFrequency_2(),
	AudioOccluder_t3526511466::get_offset_of_CutoffFrequency_3(),
	AudioOccluder_t3526511466::get_offset_of_VolumePassThrough_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5552 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5553 = { sizeof (ActiveEvent_t1691504008), -1, sizeof(ActiveEvent_t1691504008_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5553[14] = 
{
	ActiveEvent_t1691504008::get_offset_of_primarySource_0(),
	ActiveEvent_t1691504008::get_offset_of_secondarySource_1(),
	ActiveEvent_t1691504008::get_offset_of_U3CAudioEmitterU3Ek__BackingField_2(),
	ActiveEvent_t1691504008::get_offset_of_U3CMessageOnAudioEndU3Ek__BackingField_3(),
	ActiveEvent_t1691504008::get_offset_of_AudioEvent_4(),
	ActiveEvent_t1691504008::get_offset_of_IsStoppable_5(),
	ActiveEvent_t1691504008::get_offset_of_VolDest_6(),
	ActiveEvent_t1691504008::get_offset_of_AltVolDest_7(),
	ActiveEvent_t1691504008::get_offset_of_CurrentFade_8(),
	ActiveEvent_t1691504008::get_offset_of_PlayingAlt_9(),
	ActiveEvent_t1691504008::get_offset_of_IsActiveTimeComplete_10(),
	ActiveEvent_t1691504008::get_offset_of_ActiveTime_11(),
	ActiveEvent_t1691504008::get_offset_of_CancelEvent_12(),
	ActiveEvent_t1691504008_StaticFields::get_offset_of_SpatialRolloff_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5554 = { sizeof (U3CU3Ec__DisplayClass29_0_t1666631841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5554[4] = 
{
	U3CU3Ec__DisplayClass29_0_t1666631841::get_offset_of_audioEvent_0(),
	U3CU3Ec__DisplayClass29_0_t1666631841::get_offset_of_pitch_1(),
	U3CU3Ec__DisplayClass29_0_t1666631841::get_offset_of_vol_2(),
	U3CU3Ec__DisplayClass29_0_t1666631841::get_offset_of_pan_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5555 = { sizeof (U3CU3Ec_t4034150452), -1, sizeof(U3CU3Ec_t4034150452_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5555[5] = 
{
	U3CU3Ec_t4034150452_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4034150452_StaticFields::get_offset_of_U3CU3E9__29_1_1(),
	U3CU3Ec_t4034150452_StaticFields::get_offset_of_U3CU3E9__29_2_2(),
	U3CU3Ec_t4034150452_StaticFields::get_offset_of_U3CU3E9__29_3_3(),
	U3CU3Ec_t4034150452_StaticFields::get_offset_of_U3CU3E9__29_8_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5556 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5556[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5557 = { sizeof (UAudioClip_t192323766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5557[4] = 
{
	UAudioClip_t192323766::get_offset_of_Sound_0(),
	UAudioClip_t192323766::get_offset_of_Looping_1(),
	UAudioClip_t192323766::get_offset_of_DelayCenter_2(),
	UAudioClip_t192323766::get_offset_of_DelayRandomization_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5558 = { sizeof (AudioContainer_t974065762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5558[6] = 
{
	AudioContainer_t974065762::get_offset_of_ContainerType_0(),
	AudioContainer_t974065762::get_offset_of_Looping_1(),
	AudioContainer_t974065762::get_offset_of_LoopTime_2(),
	AudioContainer_t974065762::get_offset_of_Sounds_3(),
	AudioContainer_t974065762::get_offset_of_CrossfadeTime_4(),
	AudioContainer_t974065762::get_offset_of_CurrentClip_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5559 = { sizeof (AudioContainerType_t399292031)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5559[6] = 
{
	AudioContainerType_t399292031::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5560 = { sizeof (AudioEventInstanceBehavior_t1649659247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5560[3] = 
{
	AudioEventInstanceBehavior_t1649659247::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5561 = { sizeof (SpatialPositioningType_t2231436405)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5561[4] = 
{
	SpatialPositioningType_t2231436405::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5562 = { sizeof (AudioEvent_t3236711075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5562[21] = 
{
	AudioEvent_t3236711075::get_offset_of_Name_0(),
	AudioEvent_t3236711075::get_offset_of_Spatialization_1(),
	AudioEvent_t3236711075::get_offset_of_RoomSize_2(),
	AudioEvent_t3236711075::get_offset_of_AttenuationCurve_3(),
	AudioEvent_t3236711075::get_offset_of_SpatialCurve_4(),
	AudioEvent_t3236711075::get_offset_of_SpreadCurve_5(),
	AudioEvent_t3236711075::get_offset_of_ReverbCurve_6(),
	AudioEvent_t3236711075::get_offset_of_MaxDistanceAttenuation3D_7(),
	AudioEvent_t3236711075::get_offset_of_AudioBus_8(),
	AudioEvent_t3236711075::get_offset_of_PitchCenter_9(),
	AudioEvent_t3236711075::get_offset_of_PitchRandomization_10(),
	AudioEvent_t3236711075::get_offset_of_VolumeCenter_11(),
	AudioEvent_t3236711075::get_offset_of_VolumeRandomization_12(),
	AudioEvent_t3236711075::get_offset_of_PanCenter_13(),
	AudioEvent_t3236711075::get_offset_of_PanRandomization_14(),
	AudioEvent_t3236711075::get_offset_of_FadeInTime_15(),
	AudioEvent_t3236711075::get_offset_of_FadeOutTime_16(),
	AudioEvent_t3236711075::get_offset_of_InstanceLimit_17(),
	AudioEvent_t3236711075::get_offset_of_InstanceTimeBuffer_18(),
	AudioEvent_t3236711075::get_offset_of_AudioEventInstanceBehavior_19(),
	AudioEvent_t3236711075::get_offset_of_Container_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5563 = { sizeof (AudioEventAttribute_t4038497407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5564 = { sizeof (AudioEventBank_t173659183), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5565 = { sizeof (AudioSourcePlayClipExtension_t4158734528), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5566 = { sizeof (AudioSourcesReference_t2128394248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5566[1] = 
{
	AudioSourcesReference_t2128394248::get_offset_of_audioSources_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5567 = { sizeof (SpatialSoundRoomSizes_t1648336194)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5567[5] = 
{
	SpatialSoundRoomSizes_t1648336194::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5568 = { sizeof (SpatialSoundSettings_t982532882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5568[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5569 = { sizeof (SpatialSoundParameters_t3931834461)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5569[2] = 
{
	SpatialSoundParameters_t3931834461::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5570 = { sizeof (UAudioManager_t15065897), -1, sizeof(UAudioManager_t15065897_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5570[5] = 
{
	UAudioManager_t15065897::get_offset_of_globalEventInstanceLimit_7(),
	UAudioManager_t15065897::get_offset_of_globalInstanceBehavior_8(),
	UAudioManager_t15065897::get_offset_of_U3CAudioEmitterTransformU3Ek__BackingField_9(),
	UAudioManager_t15065897::get_offset_of_eventsDictionary_10(),
	UAudioManager_t15065897_StaticFields::get_offset_of_instance_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5571 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5571[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5572 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5572[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5573 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5573[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5574 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5574[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5575 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5575[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5576 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5576[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5577 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5577[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5578 = { sizeof (SpatialUnderstanding_t1544682859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5578[11] = 
{
	0,
	SpatialUnderstanding_t1544682859::get_offset_of_AutoBeginScanning_5(),
	SpatialUnderstanding_t1544682859::get_offset_of_UpdatePeriod_DuringScanning_6(),
	SpatialUnderstanding_t1544682859::get_offset_of_UpdatePeriod_AfterScanning_7(),
	SpatialUnderstanding_t1544682859::get_offset_of_U3CUnderstandingDLLU3Ek__BackingField_8(),
	SpatialUnderstanding_t1544682859::get_offset_of_U3CUnderstandingSourceMeshU3Ek__BackingField_9(),
	SpatialUnderstanding_t1544682859::get_offset_of_U3CUnderstandingCustomMeshU3Ek__BackingField_10(),
	SpatialUnderstanding_t1544682859::get_offset_of_OnScanDone_11(),
	SpatialUnderstanding_t1544682859::get_offset_of_ScanStateChanged_12(),
	SpatialUnderstanding_t1544682859::get_offset_of_scanState_13(),
	SpatialUnderstanding_t1544682859::get_offset_of_timeSinceLastUpdate_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5579 = { sizeof (ScanStates_t3301420450)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5579[6] = 
{
	ScanStates_t3301420450::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5580 = { sizeof (OnScanDoneDelegate_t908476570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5581 = { sizeof (SpatialUnderstandingCustomMesh_t3317789065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5581[9] = 
{
	SpatialUnderstandingCustomMesh_t3317789065::get_offset_of_ImportMeshPeriod_9(),
	SpatialUnderstandingCustomMesh_t3317789065::get_offset_of_meshMaterial_10(),
	SpatialUnderstandingCustomMesh_t3317789065::get_offset_of_MaxFrameTime_11(),
	SpatialUnderstandingCustomMesh_t3317789065::get_offset_of_CreateMeshColliders_12(),
	SpatialUnderstandingCustomMesh_t3317789065::get_offset_of_drawProcessedMesh_13(),
	SpatialUnderstandingCustomMesh_t3317789065::get_offset_of_U3CIsImportActiveU3Ek__BackingField_14(),
	SpatialUnderstandingCustomMesh_t3317789065::get_offset_of_timeLastImportedMesh_15(),
	SpatialUnderstandingCustomMesh_t3317789065::get_offset_of_spatialUnderstanding_16(),
	SpatialUnderstandingCustomMesh_t3317789065::get_offset_of_meshSectors_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5582 = { sizeof (MeshData_t2646720140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5582[5] = 
{
	MeshData_t2646720140::get_offset_of_verts_0(),
	MeshData_t2646720140::get_offset_of_tris_1(),
	MeshData_t2646720140::get_offset_of_MeshObject_2(),
	MeshData_t2646720140::get_offset_of_SpatialCollider_3(),
	MeshData_t2646720140::get_offset_of_CreateMeshCollider_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5583 = { sizeof (U3CImport_UnderstandingMeshU3Ed__28_t801016225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5583[27] = 
{
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CU3E1__state_0(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CU3E2__current_1(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CU3E4__this_2(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CstopwatchU3E5__1_3(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CstartFrameCountU3E5__2_4(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CdllU3E5__3_5(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CmeshVerticesU3E5__4_6(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CmeshNormalsU3E5__5_7(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CmeshIndicesU3E5__6_8(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CvertCountU3E5__7_9(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CidxCountU3E5__8_10(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CdeltaFrameCountU3E5__9_11(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CvertPosU3E5__10_12(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CvertNormU3E5__11_13(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CindicesU3E5__12_14(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CstartTimeU3E5__13_15(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CU3Es__14_16(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CmeshdataU3E5__15_17(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CindexU3E5__16_18(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CfirstVertexU3E5__17_19(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CsecondVertexU3E5__18_20(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CthirdVertexU3E5__19_21(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CfirstSectorU3E5__20_22(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CsecondSectorU3E5__21_23(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CthirdSectorU3E5__22_24(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CU3Es__23_25(),
	U3CImport_UnderstandingMeshU3Ed__28_t801016225::get_offset_of_U3CmeshDataU3E5__24_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5584 = { sizeof (SpatialUnderstandingDll_t3348055109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5584[10] = 
{
	SpatialUnderstandingDll_t3348055109::get_offset_of_reusedMeshesForMarshalling_0(),
	SpatialUnderstandingDll_t3348055109::get_offset_of_reusedPinnedMemoryHandles_1(),
	SpatialUnderstandingDll_t3348055109::get_offset_of_reusedRaycastResult_2(),
	SpatialUnderstandingDll_t3348055109::get_offset_of_reusedRaycastResultPtr_3(),
	SpatialUnderstandingDll_t3348055109::get_offset_of_reusedPlayspaceStats_4(),
	SpatialUnderstandingDll_t3348055109::get_offset_of_reusedPlayspaceStatsPtr_5(),
	SpatialUnderstandingDll_t3348055109::get_offset_of_reusedPlayspaceAlignment_6(),
	SpatialUnderstandingDll_t3348055109::get_offset_of_reusedPlayspaceAlignmentPtr_7(),
	SpatialUnderstandingDll_t3348055109::get_offset_of_reusedObjectPlacementResult_8(),
	SpatialUnderstandingDll_t3348055109::get_offset_of_reusedObjectPlacementResultPtr_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5585 = { sizeof (MeshData_t3361835602)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5585[6] = 
{
	MeshData_t3361835602::get_offset_of_MeshID_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t3361835602::get_offset_of_LastUpdateID_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t3361835602::get_offset_of_Transform_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t3361835602::get_offset_of_Verts_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t3361835602::get_offset_of_Normals_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t3361835602::get_offset_of_Indices_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5586 = { sizeof (Imports_t718195452), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5587 = { sizeof (MeshData_t3857626232)+ sizeof (RuntimeObject), sizeof(MeshData_t3857626232 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5587[8] = 
{
	MeshData_t3857626232::get_offset_of_meshID_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t3857626232::get_offset_of_lastUpdateID_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t3857626232::get_offset_of_transform_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t3857626232::get_offset_of_vertCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t3857626232::get_offset_of_indexCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t3857626232::get_offset_of_verts_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t3857626232::get_offset_of_normals_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t3857626232::get_offset_of_indices_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5588 = { sizeof (PlayspaceStats_t3538817570), sizeof(PlayspaceStats_t3538817570_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5588[19] = 
{
	PlayspaceStats_t3538817570::get_offset_of_IsWorkingOnStats_0(),
	PlayspaceStats_t3538817570::get_offset_of_HorizSurfaceArea_1(),
	PlayspaceStats_t3538817570::get_offset_of_TotalSurfaceArea_2(),
	PlayspaceStats_t3538817570::get_offset_of_UpSurfaceArea_3(),
	PlayspaceStats_t3538817570::get_offset_of_DownSurfaceArea_4(),
	PlayspaceStats_t3538817570::get_offset_of_WallSurfaceArea_5(),
	PlayspaceStats_t3538817570::get_offset_of_VirtualCeilingSurfaceArea_6(),
	PlayspaceStats_t3538817570::get_offset_of_VirtualWallSurfaceArea_7(),
	PlayspaceStats_t3538817570::get_offset_of_NumFloor_8(),
	PlayspaceStats_t3538817570::get_offset_of_NumCeiling_9(),
	PlayspaceStats_t3538817570::get_offset_of_NumWall_XNeg_10(),
	PlayspaceStats_t3538817570::get_offset_of_NumWall_XPos_11(),
	PlayspaceStats_t3538817570::get_offset_of_NumWall_ZNeg_12(),
	PlayspaceStats_t3538817570::get_offset_of_NumWall_ZPos_13(),
	PlayspaceStats_t3538817570::get_offset_of_NumPlatform_14(),
	PlayspaceStats_t3538817570::get_offset_of_CellCount_IsPaintMode_15(),
	PlayspaceStats_t3538817570::get_offset_of_CellCount_IsSeenQualtiy_None_16(),
	PlayspaceStats_t3538817570::get_offset_of_CellCount_IsSeenQualtiy_Seen_17(),
	PlayspaceStats_t3538817570::get_offset_of_CellCount_IsSeenQualtiy_Good_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5589 = { sizeof (PlayspaceAlignment_t492554602), sizeof(PlayspaceAlignment_t492554602_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5589[7] = 
{
	PlayspaceAlignment_t492554602::get_offset_of_Center_0(),
	PlayspaceAlignment_t492554602::get_offset_of_HalfDims_1(),
	PlayspaceAlignment_t492554602::get_offset_of_BasisX_2(),
	PlayspaceAlignment_t492554602::get_offset_of_BasisY_3(),
	PlayspaceAlignment_t492554602::get_offset_of_BasisZ_4(),
	PlayspaceAlignment_t492554602::get_offset_of_FloorYValue_5(),
	PlayspaceAlignment_t492554602::get_offset_of_CeilingYValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5590 = { sizeof (RaycastResult_t1603201782), sizeof(RaycastResult_t1603201782_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5590[4] = 
{
	RaycastResult_t1603201782::get_offset_of_SurfaceType_0(),
	RaycastResult_t1603201782::get_offset_of_SurfaceArea_1(),
	RaycastResult_t1603201782::get_offset_of_IntersectPoint_2(),
	RaycastResult_t1603201782::get_offset_of_IntersectNormal_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5591 = { sizeof (SurfaceTypes_t6986610)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5591[9] = 
{
	SurfaceTypes_t6986610::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5592 = { sizeof (SpatialUnderstandingDllObjectPlacement_t605993937), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5593 = { sizeof (ObjectPlacementDefinition_t2072559250)+ sizeof (RuntimeObject), sizeof(ObjectPlacementDefinition_t2072559250 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5593[9] = 
{
	ObjectPlacementDefinition_t2072559250::get_offset_of_Type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t2072559250::get_offset_of_PlacementParam_Int_0_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t2072559250::get_offset_of_PlacementParam_Float_0_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t2072559250::get_offset_of_PlacementParam_Float_1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t2072559250::get_offset_of_PlacementParam_Float_2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t2072559250::get_offset_of_PlacementParam_Float_3_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t2072559250::get_offset_of_PlacementParam_Str_0_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t2072559250::get_offset_of_WallFlags_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t2072559250::get_offset_of_HalfDims_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5594 = { sizeof (PlacementType_t1028595510)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5594[10] = 
{
	PlacementType_t1028595510::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5595 = { sizeof (WallTypeFlags_t3149592466)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5595[6] = 
{
	WallTypeFlags_t3149592466::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5596 = { sizeof (ObjectPlacementRule_t559886570)+ sizeof (RuntimeObject), sizeof(ObjectPlacementRule_t559886570 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5596[5] = 
{
	ObjectPlacementRule_t559886570::get_offset_of_Type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementRule_t559886570::get_offset_of_RuleParam_Int_0_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementRule_t559886570::get_offset_of_RuleParam_Float_0_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementRule_t559886570::get_offset_of_RuleParam_Float_1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementRule_t559886570::get_offset_of_RuleParam_Vec3_0_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5597 = { sizeof (ObjectPlacementRuleType_t3448184229)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5597[4] = 
{
	ObjectPlacementRuleType_t3448184229::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5598 = { sizeof (ObjectPlacementConstraint_t4230355661)+ sizeof (RuntimeObject), sizeof(ObjectPlacementConstraint_t4230355661 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5598[6] = 
{
	ObjectPlacementConstraint_t4230355661::get_offset_of_Type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementConstraint_t4230355661::get_offset_of_RuleParam_Int_0_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementConstraint_t4230355661::get_offset_of_RuleParam_Float_0_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementConstraint_t4230355661::get_offset_of_RuleParam_Float_1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementConstraint_t4230355661::get_offset_of_RuleParam_Float_2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementConstraint_t4230355661::get_offset_of_RuleParam_Vec3_0_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5599 = { sizeof (ObjectPlacementConstraintType_t378844790)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5599[7] = 
{
	ObjectPlacementConstraintType_t378844790::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
