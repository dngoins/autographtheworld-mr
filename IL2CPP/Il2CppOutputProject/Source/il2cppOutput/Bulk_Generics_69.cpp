﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Generic.IEnumerable`1<System.Byte>
struct IEnumerable_1_t114149265;
// System.Collections.Generic.IEnumerable`1<System.Byte>[]
struct IEnumerable_1U5BU5D_t3387783948;
// System.Collections.Generic.IEnumerable`1<System.Char>
struct IEnumerable_1_t2614313359;
// System.Collections.Generic.IEnumerable`1<System.Char>[]
struct IEnumerable_1U5BU5D_t2799407958;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t1510070208;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct IEnumerable_1U5BU5D_t3684372801;
// System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>
struct IEnumerable_1_t921020900;
// System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>[]
struct IEnumerable_1U5BU5D_t614637069;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1930798642;
// System.Collections.Generic.IEnumerable`1<System.Int32>[]
struct IEnumerable_1U5BU5D_t3951349959;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// System.Collections.Generic.IEnumerable`1<System.Object>[]
struct IEnumerable_1U5BU5D_t2115075616;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t1463797649;
// System.Collections.Generic.IEnumerable`1<System.Type>[]
struct IEnumerable_1U5BU5D_t3212016396;
// System.Collections.Generic.IList`1<System.Byte>
struct IList_1_t2949616159;
// System.Collections.Generic.IList`1<System.Byte>[]
struct IList_1U5BU5D_t3612319366;
// System.Collections.Generic.IList`1<System.Char>
struct IList_1_t1154812957;
// System.Collections.Generic.IList`1<System.Char>[]
struct IList_1U5BU5D_t3023943376;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IList_1_t50569806;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct IList_1U5BU5D_t3908908219;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t471298240;
// System.Collections.Generic.IList`1<System.Int32>[]
struct IList_1U5BU5D_t4175885377;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t600458651;
// System.Collections.Generic.IList`1<System.Object>[]
struct IList_1U5BU5D_t2339611034;
// System.Collections.Generic.IList`1<System.Type>
struct IList_1_t4297247;
// System.Collections.Generic.IList`1<System.Type>[]
struct IList_1U5BU5D_t3436551814;
// System.Collections.Generic.IReadOnlyList`1<System.Byte>
struct IReadOnlyList_1_t1698719282;
// System.Collections.Generic.IReadOnlyList`1<System.Byte>[]
struct IReadOnlyList_1U5BU5D_t2744586439;
// System.Collections.Generic.IReadOnlyList`1<System.Char>
struct IReadOnlyList_1_t4198883376;
// System.Collections.Generic.IReadOnlyList`1<System.Char>[]
struct IReadOnlyList_1U5BU5D_t2156210449;
// System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IReadOnlyList_1_t3094640225;
// System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct IReadOnlyList_1U5BU5D_t3041175292;
// System.Collections.Generic.IReadOnlyList`1<System.Int32>
struct IReadOnlyList_1_t3515368659;
// System.Collections.Generic.IReadOnlyList`1<System.Int32>[]
struct IReadOnlyList_1U5BU5D_t3308152450;
// System.Collections.Generic.IReadOnlyList`1<System.Object>
struct IReadOnlyList_1_t3644529070;
// System.Collections.Generic.IReadOnlyList`1<System.Object>[]
struct IReadOnlyList_1U5BU5D_t1471878107;
// System.Collections.Generic.IReadOnlyList`1<System.Type>
struct IReadOnlyList_1_t3048367666;
// System.Collections.Generic.IReadOnlyList`1<System.Type>[]
struct IReadOnlyList_1U5BU5D_t2568818887;
// System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>[]
struct KeyValuePair_2U5BU5D_t3332857673;
// System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>[]
struct KeyValuePair_2U5BU5D_t134831157;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t2652375035;
// System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>[]
struct KeyValuePair_2U5BU5D_t3818836818;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t171748081;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1954543557;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t118269214;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.String>[]
struct KeyValuePair_2U5BU5D_t2851086525;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t1951926774;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3734722250;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct KeyValuePair_2U5BU5D_t1898447907;
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct KeyValuePair_2U5BU5D_t336297922;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Type>[]
struct KeyValuePair_2U5BU5D_t2995388687;
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>[]
struct KeyValuePair_2U5BU5D_t130504686;
// System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>[]
struct KeyValuePair_2U5BU5D_t2544954260;
// System.Collections.IEnumerable
struct IEnumerable_t1941168011;
// System.Collections.IEnumerable[]
struct IEnumerableU5BU5D_t1343500778;
// System.Collections.IList
struct IList_t2094931216;
// System.Collections.IList[]
struct IListU5BU5D_t2181495985;
// System.Collections.Specialized.INotifyCollectionChanged
struct INotifyCollectionChanged_t3298403757;
// System.Collections.Specialized.INotifyCollectionChanged[]
struct INotifyCollectionChangedU5BU5D_t557462784;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t777629997;
// System.String
struct String_t;
// System.DateTimeOffset[]
struct DateTimeOffsetU5BU5D_t3473357058;
// System.Double[]
struct DoubleU5BU5D_t3413330114;
// System.Exception
struct Exception_t1436737249;
// System.Exception[]
struct ExceptionU5BU5D_t2535001212;
// System.Guid[]
struct GuidU5BU5D_t545550574;
// System.IDisposable
struct IDisposable_t3640265483;
// System.IDisposable[]
struct IDisposableU5BU5D_t3584190570;
// System.Int16[]
struct Int16U5BU5D_t3686840178;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Int64[]
struct Int64U5BU5D_t2559172825;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Type
struct Type_t;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Reflection.Binder
struct Binder_t2999457153;

extern RuntimeClass* Il2CppComObject_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_GetAt_m490189953_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m3046606360_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m217716711_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m2250330655_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m3769613402_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m1634167149_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m3042122219_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m4072637945_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m458852302_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m1427262250_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m2417665634_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m405762550_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m2080563997_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m4222947830_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m3514813479_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m3919556009_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m929209378_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m71529672_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m3312192623_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m2292353266_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m2339960516_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m3038433658_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m2862071070_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m3396401288_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m2447159008_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m2260873046_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m915303395_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m8013008_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m3633388355_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m1599459309_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m1804213203_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m3746205704_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m3621300116_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m3521302946_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m1250314613_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m2072164899_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m236800478_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m2274155770_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t231828568_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_GetAt_m581037041_MetadataUsageId;
extern const uint32_t IVectorView_1_IndexOf_m3984942326_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m910516119_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t3930634460_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_IndexOf_m3026496961_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t71524366_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_GetAt_m1033266262_MetadataUsageId;
extern const uint32_t IVectorView_1_IndexOf_m2919924410_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m2431495291_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t2245450819_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_GetAt_m3698596162_MetadataUsageId;
extern const uint32_t IVectorView_1_IndexOf_m195073214_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m1367398776_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t3842366416_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_GetAt_m2114263251_MetadataUsageId;
extern const uint32_t IVectorView_1_IndexOf_m1800219957_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m792906882_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t2401056908_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_GetAt_m2907280382_MetadataUsageId;
extern const uint32_t IVectorView_1_IndexOf_m1224710320_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m1250367745_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_GetAt_m4135153857_MetadataUsageId;
extern const uint32_t IVectorView_1_IndexOf_m2426904384_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m718367012_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t1297561844_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_GetAt_m3427646343_MetadataUsageId;
extern const uint32_t IVectorView_1_IndexOf_m3115195681_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m3208394414_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t2280216431_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_GetAt_m3200486354_MetadataUsageId;
extern const uint32_t IVectorView_1_IndexOf_m1159407127_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m2903092860_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t838906923_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_GetAt_m1543733215_MetadataUsageId;
extern const uint32_t IVectorView_1_IndexOf_m766512542_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m2121778822_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t968067334_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_GetAt_m4155768441_MetadataUsageId;
extern const uint32_t IVectorView_1_IndexOf_m61054490_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m3141254740_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t4030379155_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_GetAt_m3371945293_MetadataUsageId;
extern const uint32_t IVectorView_1_IndexOf_m653354477_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m1052653815_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t371905930_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_IndexOf_m3999170120_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t3030996695_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_IndexOf_m2218762648_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t2457078697_il2cpp_TypeInfo_var;
extern const uint32_t IVectorView_1_GetAt_m3476973415_MetadataUsageId;
extern const uint32_t IVectorView_1_IndexOf_m836020600_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m2658557676_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m3012281261_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m1961556821_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m380625345_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m2636815833_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m1114485513_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m1942078576_MetadataUsageId;
extern RuntimeClass* ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4286223022;
extern Il2CppCodeGenString* _stringLiteral311452336;
extern const uint32_t IVectorView_1_GetAt_m1224151036_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m1032517645_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m3461880810_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m1458693733_MetadataUsageId;
extern const uint32_t IVectorView_1_GetAt_m726655415_MetadataUsageId;
extern const uint32_t IVectorView_1_GetMany_m4226896717_MetadataUsageId;
struct IKeyValuePair_2_t572118738;
struct IKeyValuePair_2_t3270170437;
struct IBindableVectorView_t4191940899;
struct INotifyCollectionChanged_t3244377239;
struct IKeyValuePair_2_t1256548186;
struct IKeyValuePair_2_t1096243984;
struct IClosable_t326290202;
struct IKeyValuePair_2_t660386782;
struct IKeyValuePair_2_t1992786952;
struct IKeyValuePair_2_t3481798315;
struct IKeyValuePair_2_t1396625548;
struct IKeyValuePair_2_t760131477;
struct IKeyValuePair_2_t4055716313;
struct IBindableIterator_t667386168;
struct IKeyValuePair_2_t2322281462;
struct IBindableVector_t1801110279;
struct IKeyValuePair_2_t1863626541;
struct IBindableIterable_t2147255965;
struct IKeyValuePair_2_t3304936049;
struct IIterator_1_t954249637;
struct IIterable_1_t105342220;
struct IIterator_1_t825089226;
struct IIterable_1_t701503624;
struct IIterator_1_t358088233;
struct IVector_1_t80114895;
struct IVectorView_1_t2973325732;
struct IVector_1_t1874918097;
struct IVectorView_1_t473161638;
struct IIterable_1_t572343213;
struct IIterator_1_t3303407145;
struct IIterable_1_t1255857930;
struct IKeyValuePair_2_t3425776526;
struct IIterable_1_t3050661132;
struct IIterator_1_t1508603943;
struct IIterable_1_t3857532767;
struct IIterator_1_t4110278780;
struct IIterable_1_t151614779;
struct IIterator_1_t404360792;
struct IVector_1_t3270839040;
struct IVectorView_1_t1869082581;
struct IVectorView_1_t1822810022;
struct IKeyValuePair_2_t3554936937;
struct IVectorView_1_t2418971426;
struct IVectorView_1_t2289811015;
struct IVector_1_t3820727885;
struct IVector_1_t3691567474;
struct IVector_1_t3224566481;
struct Exception_t1436737249_marshaled_pinvoke;
struct Exception_t1436737249_marshaled_com;
struct DateTime_t1679451545 ;
struct Guid_t ;

struct IEnumerable_1U5BU5D_t3387783948;
struct IEnumerable_1U5BU5D_t2799407958;
struct IEnumerable_1U5BU5D_t3684372801;
struct IEnumerable_1U5BU5D_t614637069;
struct IEnumerable_1U5BU5D_t3951349959;
struct IEnumerable_1U5BU5D_t2115075616;
struct IEnumerable_1U5BU5D_t3212016396;
struct IList_1U5BU5D_t3612319366;
struct IList_1U5BU5D_t3023943376;
struct IList_1U5BU5D_t3908908219;
struct IList_1U5BU5D_t4175885377;
struct IList_1U5BU5D_t2339611034;
struct IList_1U5BU5D_t3436551814;
struct IReadOnlyList_1U5BU5D_t2744586439;
struct IReadOnlyList_1U5BU5D_t2156210449;
struct IReadOnlyList_1U5BU5D_t3041175292;
struct IReadOnlyList_1U5BU5D_t3308152450;
struct IReadOnlyList_1U5BU5D_t1471878107;
struct IReadOnlyList_1U5BU5D_t2568818887;
struct KeyValuePair_2U5BU5D_t3332857673;
struct KeyValuePair_2U5BU5D_t134831157;
struct KeyValuePair_2U5BU5D_t2652375035;
struct KeyValuePair_2U5BU5D_t3818836818;
struct KeyValuePair_2U5BU5D_t171748081;
struct KeyValuePair_2U5BU5D_t1954543557;
struct KeyValuePair_2U5BU5D_t118269214;
struct KeyValuePair_2U5BU5D_t2851086525;
struct KeyValuePair_2U5BU5D_t1951926774;
struct KeyValuePair_2U5BU5D_t3734722250;
struct KeyValuePair_2U5BU5D_t1898447907;
struct KeyValuePair_2U5BU5D_t336297922;
struct KeyValuePair_2U5BU5D_t2995388687;
struct KeyValuePair_2U5BU5D_t130504686;
struct KeyValuePair_2U5BU5D_t2544954260;
struct IEnumerableU5BU5D_t1343500778;
struct IListU5BU5D_t2181495985;
struct INotifyCollectionChangedU5BU5D_t557462784;
struct DateTimeOffsetU5BU5D_t3473357058;
struct DoubleU5BU5D_t3413330114;
struct ExceptionU5BU5D_t2535001212;
struct GuidU5BU5D_t545550574;
struct IDisposableU5BU5D_t3584190570;
struct Int16U5BU5D_t3686840178;
struct Int32U5BU5D_t385246372;
struct Int64U5BU5D_t2559172825;
struct ObjectU5BU5D_t2843939325;


// Windows.Foundation.Collections.IKeyValuePair`2<System.Int64,System.Object>
struct NOVTABLE IKeyValuePair_2_t3270170437 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m1937891137(int64_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m1356929877(Il2CppIInspectable** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct NOVTABLE IVectorView_1_t3181231678 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m2114263251(uint32_t ___index0, IKeyValuePair_2_t572118738** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m2486136128(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1800219957(IKeyValuePair_2_t572118738* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m792906882(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t572118738** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Double>
struct NOVTABLE IVectorView_1_t4228497921 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3029520189(uint32_t ___index0, double* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1840672366(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m519924844(double ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m4072621343(uint32_t ___startIndex0, uint32_t ___items1ArraySize, double* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.Int32,System.Object>
struct NOVTABLE IKeyValuePair_2_t1096243984 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2186241555(int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m210688108(Il2CppIInspectable** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct NOVTABLE IVectorView_1_t1584316081 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3698596162(uint32_t ___index0, IKeyValuePair_2_t3270170437** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3315242568(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m195073214(IKeyValuePair_2_t3270170437* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m1367398776(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t3270170437** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.UI.Xaml.Interop.IBindableVector
struct NOVTABLE IBindableVector_t1801110279 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IBindableVector_GetAt_m2354796153(uint32_t ___index0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IBindableVector_get_Size_m151438974(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IBindableVector_GetView_m1491544680(IBindableVectorView_t4191940899** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IBindableVector_IndexOf_m627110519(Il2CppIInspectable* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IBindableVector_SetAt_m1920883433(uint32_t ___index0, Il2CppIInspectable* ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IBindableVector_InsertAt_m450046008(uint32_t ___index0, Il2CppIInspectable* ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IBindableVector_RemoveAt_m3479961725(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IBindableVector_Append_m2201043447(Il2CppIInspectable* ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IBindableVector_RemoveAtEnd_m2914381745() = 0;
	virtual il2cpp_hresult_t STDCALL IBindableVector_Clear_m717217363() = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.Int32>
struct NOVTABLE IKeyValuePair_2_t3425776526 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m1066766749(Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m269040038(int32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Specialized.INotifyCollectionChanged>
struct NOVTABLE IVectorView_1_t2637269019 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1114485513(uint32_t ___index0, INotifyCollectionChanged_t3244377239** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1381695206(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2659620536(INotifyCollectionChanged_t3244377239* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m1942078576(uint32_t ___startIndex0, uint32_t ___items1ArraySize, INotifyCollectionChanged_t3244377239** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.UI.Xaml.Interop.INotifyCollectionChanged
struct NOVTABLE INotifyCollectionChanged_t3244377239 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL INotifyCollectionChanged_U24__Stripped0_add_CollectionChanged_m3492513552() = 0;
	virtual il2cpp_hresult_t STDCALL INotifyCollectionChanged_U24__Stripped1_remove_CollectionChanged_m1626086428() = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.Boolean>
struct NOVTABLE IKeyValuePair_2_t572118738 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m1916408783(Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m3611222262(bool* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Int64>
struct NOVTABLE IVectorView_1_t3075432566 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m4097062415(uint32_t ___index0, int64_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3689183858(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m928127612(int64_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2435124798(uint32_t ___startIndex0, uint32_t ___items1ArraySize, int64_t* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Int16>
struct NOVTABLE IVectorView_1_t1891685649 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1319642345(uint32_t ___index0, int16_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m2071989762(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1081404675(int16_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3136641509(uint32_t ___startIndex0, uint32_t ___items1ArraySize, int16_t* ___items1, uint32_t* comReturnValue) = 0;
};
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
// Windows.Foundation.Collections.IVectorView`1<System.Type>
struct NOVTABLE IVectorView_1_t1822810022 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3708182184(uint32_t ___index0, Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1097778865(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1129849567(Type_t * ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3274208783(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Type_t ** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>
struct NOVTABLE IVectorView_1_t3865661126 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m581037041(uint32_t ___index0, IKeyValuePair_2_t1256548186** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1855235872(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m3984942326(IKeyValuePair_2_t1256548186* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m910516119(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t1256548186** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct NOVTABLE IVectorView_1_t3705356924 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1033266262(uint32_t ___index0, IKeyValuePair_2_t1096243984** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m777270669(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2919924410(IKeyValuePair_2_t1096243984* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2431495291(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t1096243984** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Exception>
struct NOVTABLE IVectorView_1_t775602511 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m116971458(uint32_t ___index0, int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m917737768(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2525069292(int32_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m979855732(uint32_t ___startIndex0, uint32_t ___items1ArraySize, int32_t* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.IDisposable>
struct NOVTABLE IVectorView_1_t2979130745 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3461880810(uint32_t ___index0, IClosable_t326290202** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m671734930(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m456269461(IClosable_t326290202* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m1458693733(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IClosable_t326290202** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>>
struct NOVTABLE IVectorView_1_t3269499722 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m310514428(uint32_t ___index0, IKeyValuePair_2_t660386782** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3486021694(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m3026496961(IKeyValuePair_2_t660386782* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3259869068(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t660386782** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.IClosable
struct NOVTABLE IClosable_t326290202 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IClosable_Close_m649191502() = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.Type,System.Type>
struct NOVTABLE IKeyValuePair_2_t4055716313 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2294505739(Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m1073902776(Type_t ** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Object>
struct NOVTABLE IKeyValuePair_2_t1992786952 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m3523589551(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m3279123715(Il2CppIInspectable** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>
struct NOVTABLE IVectorView_1_t306932596 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m4155768441(uint32_t ___index0, IKeyValuePair_2_t1992786952** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m578036601(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m61054490(IKeyValuePair_2_t1992786952* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3141254740(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t1992786952** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>
struct NOVTABLE IVectorView_1_t1795943959 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3476973415(uint32_t ___index0, IKeyValuePair_2_t3481798315** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1565795300(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m836020600(IKeyValuePair_2_t3481798315* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2658557676(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t3481798315** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Int32>
struct NOVTABLE IKeyValuePair_2_t1863626541 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m599934735(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m2337749369(int32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Type>>
struct NOVTABLE IVectorView_1_t4005738488 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m4266810032(uint32_t ___index0, IKeyValuePair_2_t1396625548** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1164900279(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m3999170120(IKeyValuePair_2_t1396625548* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m1120548418(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t1396625548** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Type>
struct NOVTABLE IKeyValuePair_2_t1396625548 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2199926285(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m1427991677(Type_t ** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.String>
struct NOVTABLE IKeyValuePair_2_t760131477 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m3013852749(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m2003745203(Il2CppHString* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct NOVTABLE IVectorView_1_t3369244417 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3371945293(uint32_t ___index0, IKeyValuePair_2_t760131477** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m280905071(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m653354477(IKeyValuePair_2_t760131477* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m1052653815(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t760131477** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>>
struct NOVTABLE IVectorView_1_t2369861957 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m690924722(uint32_t ___index0, IKeyValuePair_2_t4055716313** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m483378542(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2218762648(IKeyValuePair_2_t4055716313* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3562856357(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t4055716313** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.UI.Xaml.Interop.IBindableIterable
struct NOVTABLE IBindableIterable_t2147255965 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IBindableIterable_First_m3188154520(IBindableIterator_t667386168** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.String>
struct NOVTABLE IKeyValuePair_2_t2322281462 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2722966289(Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m3432963267(Il2CppHString* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.String>>
struct NOVTABLE IVectorView_1_t636427106 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3427646343(uint32_t ___index0, IKeyValuePair_2_t2322281462** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m2827533443(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m3115195681(IKeyValuePair_2_t2322281462* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3208394414(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t2322281462** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.IList>
struct NOVTABLE IVectorView_1_t1433796478 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m380625345(uint32_t ___index0, IBindableVector_t1801110279** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3533088173(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m180434441(IBindableVector_t1801110279* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2636815833(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IBindableVector_t1801110279** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.Object>
struct NOVTABLE IKeyValuePair_2_t3554936937 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2750152149(Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m2672088708(Il2CppIInspectable** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Boolean>
struct NOVTABLE IKeyValuePair_2_t3304936049 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2534079307(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m2353902038(bool* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>
struct NOVTABLE IVectorView_1_t177772185 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1543733215(uint32_t ___index0, IKeyValuePair_2_t1863626541** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m2909509684(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m766512542(IKeyValuePair_2_t1863626541* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2121778822(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t1863626541** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.UInt32,System.Object>
struct NOVTABLE IKeyValuePair_2_t3481798315 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2175672871(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m3078130594(Il2CppIInspectable** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.IEnumerable>
struct NOVTABLE IVectorView_1_t1280033273 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3012281261(uint32_t ___index0, IBindableIterable_t2147255965** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m514591163(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m189209080(IBindableIterable_t2147255965* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m1961556821(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IBindableIterable_t2147255965** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>>
struct NOVTABLE IVectorView_1_t1619081693 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3200486354(uint32_t ___index0, IKeyValuePair_2_t3304936049** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1168021312(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1159407127(IKeyValuePair_2_t3304936049* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2903092860(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t3304936049** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Object>
struct NOVTABLE IIterable_1_t701503624 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3341821894(IIterator_1_t954249637** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Type>>
struct NOVTABLE IVectorView_1_t802662911 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m2080563997(uint32_t ___index0, IIterable_1_t105342220** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m2628474012(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2055924243(IIterable_1_t105342220* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m4222947830(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IIterable_1_t105342220** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Int32>
struct NOVTABLE IIterable_1_t572343213 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1280577591(IIterator_1_t825089226** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Object>>
struct NOVTABLE IVectorView_1_t1398824315 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m2417665634(uint32_t ___index0, IIterable_1_t701503624** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1445025516(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1815040482(IIterable_1_t701503624* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m405762550(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IIterable_1_t701503624** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Type>
struct NOVTABLE IIterable_1_t105342220 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m449735320(IIterator_1_t358088233** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Char>>
struct NOVTABLE IVectorView_1_t493678219 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m929209378(uint32_t ___index0, IVector_1_t80114895** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1197104070(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1593887152(IVector_1_t80114895* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m71529672(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IVector_1_t80114895** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Char>
struct NOVTABLE IVector_1_t80114895 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m3761631649(uint32_t ___index0, Il2CppChar* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m729810301(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m3718260831(IVectorView_1_t2973325732** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m1347992874(Il2CppChar ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m38195806(uint32_t ___index0, Il2CppChar ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m843679895(uint32_t ___index0, Il2CppChar ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m1420036444(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m3292248582(Il2CppChar ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m3410828052() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m4210028140() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m1663454247(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Il2CppChar* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m968700710(uint32_t ___items0ArraySize, Il2CppChar* ___items0) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Byte>>
struct NOVTABLE IVectorView_1_t2288481421 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3514813479(uint32_t ___index0, IVector_1_t1874918097** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m432441603(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2177043747(IVector_1_t1874918097* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3919556009(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IVector_1_t1874918097** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Byte>
struct NOVTABLE IVector_1_t1874918097 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m156571538(uint32_t ___index0, uint8_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m216665452(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m2759398957(IVectorView_1_t473161638** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m2293818318(uint8_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m3049348516(uint32_t ___index0, uint8_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m1632374023(uint32_t ___index0, uint8_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m313767260(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m1600833624(uint8_t ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m2280259536() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m1881849540() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m3108887717(uint32_t ___startIndex0, uint32_t ___items1ArraySize, uint8_t* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m3769885758(uint32_t ___items0ArraySize, uint8_t* ___items0) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Int32>>
struct NOVTABLE IVectorView_1_t1269663904 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m458852302(uint32_t ___index0, IIterable_1_t572343213** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3390099310(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m179787137(IIterable_1_t572343213* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m1427262250(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IIterable_1_t572343213** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Byte>
struct NOVTABLE IIterable_1_t3050661132 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m770671629(IIterator_1_t3303407145** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Char>>
struct NOVTABLE IVectorView_1_t1953178621 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m217716711(uint32_t ___index0, IIterable_1_t1255857930** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m859457156(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m4230661809(IIterable_1_t1255857930* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2250330655(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IIterable_1_t1255857930** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct NOVTABLE IVectorView_1_t1739922170 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m2907280382(uint32_t ___index0, IKeyValuePair_2_t3425776526** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3227332446(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1224710320(IKeyValuePair_2_t3425776526* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m1250367745(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t3425776526** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Byte>>
struct NOVTABLE IVectorView_1_t3747981823 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m490189953(uint32_t ___index0, IIterable_1_t3050661132** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1475217278(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m413031432(IIterable_1_t3050661132* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3046606360(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IIterable_1_t3050661132** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Char>
struct NOVTABLE IIterable_1_t1255857930 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m2678403700(IIterator_1_t1508603943** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>>
struct NOVTABLE IVectorView_1_t259886162 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3042122219(uint32_t ___index0, IIterable_1_t3857532767** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m4174749221(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m4185849224(IIterable_1_t3857532767* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m4072637945(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IIterable_1_t3857532767** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.IEnumerable>
struct NOVTABLE IIterable_1_t3857532767 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m595838657(IIterator_1_t4110278780** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>
struct NOVTABLE IVectorView_1_t848935470 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3769613402(uint32_t ___index0, IIterable_1_t151614779** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m4098135628(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1726440118(IIterable_1_t151614779* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m1634167149(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IIterable_1_t151614779** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct NOVTABLE IIterable_1_t151614779 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m4041697847(IIterator_1_t404360792** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>
struct NOVTABLE IVectorView_1_t3684402364 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3312192623(uint32_t ___index0, IVector_1_t3270839040** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1958026243(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2900489442(IVector_1_t3270839040* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2292353266(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IVector_1_t3270839040** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Char>
struct NOVTABLE IVectorView_1_t2973325732 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1110858976(uint32_t ___index0, Il2CppChar* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1535793255(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m613537994(Il2CppChar ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3420541689(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Il2CppChar* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>
struct NOVTABLE IVectorView_1_t2433505487 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1804213203(uint32_t ___index0, IVectorView_1_t1869082581** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m2958914420(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m794865249(IVectorView_1_t1869082581* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3746205704(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IVectorView_1_t1869082581** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Char>>
struct NOVTABLE IVectorView_1_t3537748638 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3633388355(uint32_t ___index0, IVectorView_1_t2973325732** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1501815834(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1146956588(IVectorView_1_t2973325732* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m1599459309(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IVectorView_1_t2973325732** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Type>>
struct NOVTABLE IVectorView_1_t2387232928 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m236800478(uint32_t ___index0, IVectorView_1_t1822810022** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1539862075(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m921936937(IVectorView_1_t1822810022* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2274155770(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IVectorView_1_t1822810022** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct NOVTABLE IVectorView_1_t1869082581 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m4135153857(uint32_t ___index0, IKeyValuePair_2_t3554936937** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3464555386(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2426904384(IKeyValuePair_2_t3554936937* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m718367012(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t3554936937** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Object>>
struct NOVTABLE IVectorView_1_t2983394332 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1250314613(uint32_t ___index0, IVectorView_1_t2418971426** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m943330745(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1217520174(IVectorView_1_t2418971426* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2072164899(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IVectorView_1_t2418971426** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Object>
struct NOVTABLE IVectorView_1_t2418971426 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m726655415(uint32_t ___index0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1102039859(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2349108669(Il2CppIInspectable* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m4226896717(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Il2CppIInspectable** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Int32>>
struct NOVTABLE IVectorView_1_t2854233921 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3621300116(uint32_t ___index0, IVectorView_1_t2289811015** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m481043476(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2242687983(IVectorView_1_t2289811015* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3521302946(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IVectorView_1_t2289811015** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Int32>
struct NOVTABLE IVectorView_1_t2289811015 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1038499426(uint32_t ___index0, int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3269133634(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m711808760(int32_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m632553004(uint32_t ___startIndex0, uint32_t ___items1ArraySize, int32_t* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Int32>
struct NOVTABLE IVector_1_t3691567474 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m669642145(uint32_t ___index0, int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m1083909874(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m1457083017(IVectorView_1_t2289811015** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m1513096330(int32_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m2697343022(uint32_t ___index0, int32_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m2016133864(uint32_t ___index0, int32_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m2081155343(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m71244649(int32_t ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m2906723444() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m2316468687() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m3133207996(uint32_t ___startIndex0, uint32_t ___items1ArraySize, int32_t* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m4247928211(uint32_t ___items0ArraySize, int32_t* ___items0) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Object>>
struct NOVTABLE IVectorView_1_t4234291209 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m2862071070(uint32_t ___index0, IVector_1_t3820727885** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3624020244(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2814520576(IVector_1_t3820727885* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3396401288(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IVector_1_t3820727885** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct NOVTABLE IVector_1_t3270839040 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m4281933817(uint32_t ___index0, IKeyValuePair_2_t3554936937** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m3264460740(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m2985413962(IVectorView_1_t1869082581** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m1739374806(IKeyValuePair_2_t3554936937* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m4098079864(uint32_t ___index0, IKeyValuePair_2_t3554936937* ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m1532618444(uint32_t ___index0, IKeyValuePair_2_t3554936937* ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m2367384165(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m1636376961(IKeyValuePair_2_t3554936937* ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m1114958347() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m555469687() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m275626091(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t3554936937** ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m2100117582(uint32_t ___items0ArraySize, IKeyValuePair_2_t3554936937** ___items0) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Int32>>
struct NOVTABLE IVectorView_1_t4105130798 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m2339960516(uint32_t ___index0, IVector_1_t3691567474** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m804582976(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1759454951(IVector_1_t3691567474* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3038433658(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IVector_1_t3691567474** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Object>
struct NOVTABLE IVector_1_t3820727885 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m219694725(uint32_t ___index0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m2561126536(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m3086397386(IVectorView_1_t2418971426** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m1930043913(Il2CppIInspectable* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m1678827239(uint32_t ___index0, Il2CppIInspectable* ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m1249203443(uint32_t ___index0, Il2CppIInspectable* ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m1710167275(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m1926269351(Il2CppIInspectable* ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m3025735428() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m305000098() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m3541038849(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Il2CppIInspectable** ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m2515130844(uint32_t ___items0ArraySize, Il2CppIInspectable** ___items0) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Byte>>
struct NOVTABLE IVectorView_1_t1037584544 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m915303395(uint32_t ___index0, IVectorView_1_t473161638** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3254210033(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1711338065(IVectorView_1_t473161638* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m8013008(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IVectorView_1_t473161638** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Byte>
struct NOVTABLE IVectorView_1_t473161638 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m54195267(uint32_t ___index0, uint8_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m473387791(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m3353801494(uint8_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3912635590(uint32_t ___startIndex0, uint32_t ___items1ArraySize, uint8_t* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Type>>
struct NOVTABLE IVectorView_1_t3638129805 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m2447159008(uint32_t ___index0, IVector_1_t3224566481** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3336166233(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m3213369082(IVector_1_t3224566481* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2260873046(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IVector_1_t3224566481** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Type>
struct NOVTABLE IVector_1_t3224566481 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m3023396459(uint32_t ___index0, Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m3611201515(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m2404813821(IVectorView_1_t1822810022** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m2314055355(Type_t * ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m3724063766(uint32_t ___index0, Type_t * ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m2145145338(uint32_t ___index0, Type_t * ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m154224944(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m386838768(Type_t * ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m110681880() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m1062744925() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m1218665569(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Type_t ** ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m4247109972(uint32_t ___items0ArraySize, Type_t ** ___items0) = 0;
};
#ifndef EXCEPTION_T1436737249_H
#define EXCEPTION_T1436737249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1436737249  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t1436737249 * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____innerException_4)); }
	inline Exception_t1436737249 * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t1436737249 ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t1436737249 * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t1436737249, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t1436737249_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t1436737249_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t1436737249_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1436737249_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t1436737249_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1436737249_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T1436737249_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef KEYVALUEPAIR_2_T71524366_H
#define KEYVALUEPAIR_2_T71524366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
struct  KeyValuePair_2_t71524366 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t71524366, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t71524366, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T71524366_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t594665363_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t594665363_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_4;
	// System.Int16 System.Guid::_b
	int16_t ____b_5;
	// System.Int16 System.Guid::_c
	int16_t ____c_6;
	// System.Byte System.Guid::_d
	uint8_t ____d_7;
	// System.Byte System.Guid::_e
	uint8_t ____e_8;
	// System.Byte System.Guid::_f
	uint8_t ____f_9;
	// System.Byte System.Guid::_g
	uint8_t ____g_10;
	// System.Byte System.Guid::_h
	uint8_t ____h_11;
	// System.Byte System.Guid::_i
	uint8_t ____i_12;
	// System.Byte System.Guid::_j
	uint8_t ____j_13;
	// System.Byte System.Guid::_k
	uint8_t ____k_14;

public:
	inline static int32_t get_offset_of__a_4() { return static_cast<int32_t>(offsetof(Guid_t, ____a_4)); }
	inline int32_t get__a_4() const { return ____a_4; }
	inline int32_t* get_address_of__a_4() { return &____a_4; }
	inline void set__a_4(int32_t value)
	{
		____a_4 = value;
	}

	inline static int32_t get_offset_of__b_5() { return static_cast<int32_t>(offsetof(Guid_t, ____b_5)); }
	inline int16_t get__b_5() const { return ____b_5; }
	inline int16_t* get_address_of__b_5() { return &____b_5; }
	inline void set__b_5(int16_t value)
	{
		____b_5 = value;
	}

	inline static int32_t get_offset_of__c_6() { return static_cast<int32_t>(offsetof(Guid_t, ____c_6)); }
	inline int16_t get__c_6() const { return ____c_6; }
	inline int16_t* get_address_of__c_6() { return &____c_6; }
	inline void set__c_6(int16_t value)
	{
		____c_6 = value;
	}

	inline static int32_t get_offset_of__d_7() { return static_cast<int32_t>(offsetof(Guid_t, ____d_7)); }
	inline uint8_t get__d_7() const { return ____d_7; }
	inline uint8_t* get_address_of__d_7() { return &____d_7; }
	inline void set__d_7(uint8_t value)
	{
		____d_7 = value;
	}

	inline static int32_t get_offset_of__e_8() { return static_cast<int32_t>(offsetof(Guid_t, ____e_8)); }
	inline uint8_t get__e_8() const { return ____e_8; }
	inline uint8_t* get_address_of__e_8() { return &____e_8; }
	inline void set__e_8(uint8_t value)
	{
		____e_8 = value;
	}

	inline static int32_t get_offset_of__f_9() { return static_cast<int32_t>(offsetof(Guid_t, ____f_9)); }
	inline uint8_t get__f_9() const { return ____f_9; }
	inline uint8_t* get_address_of__f_9() { return &____f_9; }
	inline void set__f_9(uint8_t value)
	{
		____f_9 = value;
	}

	inline static int32_t get_offset_of__g_10() { return static_cast<int32_t>(offsetof(Guid_t, ____g_10)); }
	inline uint8_t get__g_10() const { return ____g_10; }
	inline uint8_t* get_address_of__g_10() { return &____g_10; }
	inline void set__g_10(uint8_t value)
	{
		____g_10 = value;
	}

	inline static int32_t get_offset_of__h_11() { return static_cast<int32_t>(offsetof(Guid_t, ____h_11)); }
	inline uint8_t get__h_11() const { return ____h_11; }
	inline uint8_t* get_address_of__h_11() { return &____h_11; }
	inline void set__h_11(uint8_t value)
	{
		____h_11 = value;
	}

	inline static int32_t get_offset_of__i_12() { return static_cast<int32_t>(offsetof(Guid_t, ____i_12)); }
	inline uint8_t get__i_12() const { return ____i_12; }
	inline uint8_t* get_address_of__i_12() { return &____i_12; }
	inline void set__i_12(uint8_t value)
	{
		____i_12 = value;
	}

	inline static int32_t get_offset_of__j_13() { return static_cast<int32_t>(offsetof(Guid_t, ____j_13)); }
	inline uint8_t get__j_13() const { return ____j_13; }
	inline uint8_t* get_address_of__j_13() { return &____j_13; }
	inline void set__j_13(uint8_t value)
	{
		____j_13 = value;
	}

	inline static int32_t get_offset_of__k_14() { return static_cast<int32_t>(offsetof(Guid_t, ____k_14)); }
	inline uint8_t get__k_14() const { return ____k_14; }
	inline uint8_t* get_address_of__k_14() { return &____k_14; }
	inline void set__k_14(uint8_t value)
	{
		____k_14 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_0;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_1;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_2;
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_3;

public:
	inline static int32_t get_offset_of__rngAccess_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_0)); }
	inline RuntimeObject * get__rngAccess_0() const { return ____rngAccess_0; }
	inline RuntimeObject ** get_address_of__rngAccess_0() { return &____rngAccess_0; }
	inline void set__rngAccess_0(RuntimeObject * value)
	{
		____rngAccess_0 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_0), value);
	}

	inline static int32_t get_offset_of__rng_1() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_1)); }
	inline RandomNumberGenerator_t386037858 * get__rng_1() const { return ____rng_1; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_1() { return &____rng_1; }
	inline void set__rng_1(RandomNumberGenerator_t386037858 * value)
	{
		____rng_1 = value;
		Il2CppCodeGenWriteBarrier((&____rng_1), value);
	}

	inline static int32_t get_offset_of__fastRng_2() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_2)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_2() const { return ____fastRng_2; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_2() { return &____fastRng_2; }
	inline void set__fastRng_2(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_2 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_2), value);
	}

	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_3)); }
	inline Guid_t  get_Empty_3() const { return ___Empty_3; }
	inline Guid_t * get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(Guid_t  value)
	{
		___Empty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef DATETIME_T1679451545_H
#define DATETIME_T1679451545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.DateTime
struct  DateTime_t1679451545 
{
public:
	// System.Int64 Windows.Foundation.DateTime::UniversalTime
	int64_t ___UniversalTime_0;

public:
	inline static int32_t get_offset_of_UniversalTime_0() { return static_cast<int32_t>(offsetof(DateTime_t1679451545, ___UniversalTime_0)); }
	inline int64_t get_UniversalTime_0() const { return ___UniversalTime_0; }
	inline int64_t* get_address_of_UniversalTime_0() { return &___UniversalTime_0; }
	inline void set_UniversalTime_0(int64_t value)
	{
		___UniversalTime_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T1679451545_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t385246372* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t385246372* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_31)); }
	inline DateTime_t3738529785  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t3738529785 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t3738529785  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_32)); }
	inline DateTime_t3738529785  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t3738529785  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef KEYVALUEPAIR_2_T2245450819_H
#define KEYVALUEPAIR_2_T2245450819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>
struct  KeyValuePair_2_t2245450819 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int64_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2245450819, ___key_0)); }
	inline int64_t get_key_0() const { return ___key_0; }
	inline int64_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int64_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2245450819, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2245450819_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT16_T2552820387_H
#define INT16_T2552820387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int16
struct  Int16_t2552820387 
{
public:
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int16_t2552820387, ___m_value_0)); }
	inline int16_t get_m_value_0() const { return ___m_value_0; }
	inline int16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int16_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT16_T2552820387_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef KEYVALUEPAIR_2_T2457078697_H
#define KEYVALUEPAIR_2_T2457078697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>
struct  KeyValuePair_2_t2457078697 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	uint32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2457078697, ___key_0)); }
	inline uint32_t get_key_0() const { return ___key_0; }
	inline uint32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(uint32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2457078697, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2457078697_H
#ifndef KEYVALUEPAIR_2_T371905930_H
#define KEYVALUEPAIR_2_T371905930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.Type>
struct  KeyValuePair_2_t371905930 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	Type_t * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t371905930, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t371905930, ___value_1)); }
	inline Type_t * get_value_1() const { return ___value_1; }
	inline Type_t ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Type_t * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T371905930_H
#ifndef KEYVALUEPAIR_2_T1297561844_H
#define KEYVALUEPAIR_2_T1297561844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.String>
struct  KeyValuePair_2_t1297561844 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1297561844, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1297561844, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1297561844_H
#ifndef KEYVALUEPAIR_2_T838906923_H
#define KEYVALUEPAIR_2_T838906923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>
struct  KeyValuePair_2_t838906923 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t838906923, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t838906923, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T838906923_H
#ifndef KEYVALUEPAIR_2_T4030379155_H
#define KEYVALUEPAIR_2_T4030379155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
struct  KeyValuePair_2_t4030379155 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4030379155, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4030379155, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T4030379155_H
#ifndef KEYVALUEPAIR_2_T3030996695_H
#define KEYVALUEPAIR_2_T3030996695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>
struct  KeyValuePair_2_t3030996695 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Type_t * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	Type_t * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3030996695, ___key_0)); }
	inline Type_t * get_key_0() const { return ___key_0; }
	inline Type_t ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Type_t * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3030996695, ___value_1)); }
	inline Type_t * get_value_1() const { return ___value_1; }
	inline Type_t ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Type_t * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3030996695_H
#ifndef KEYVALUEPAIR_2_T968067334_H
#define KEYVALUEPAIR_2_T968067334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>
struct  KeyValuePair_2_t968067334 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t968067334, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t968067334, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T968067334_H
#ifndef KEYVALUEPAIR_2_T3842366416_H
#define KEYVALUEPAIR_2_T3842366416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
struct  KeyValuePair_2_t3842366416 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	bool ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3842366416, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3842366416, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3842366416_H
#ifndef KEYVALUEPAIR_2_T2530217319_H
#define KEYVALUEPAIR_2_T2530217319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t2530217319 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2530217319_H
#ifndef KEYVALUEPAIR_2_T2280216431_H
#define KEYVALUEPAIR_2_T2280216431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
struct  KeyValuePair_2_t2280216431 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	bool ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2280216431, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2280216431, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2280216431_H
#ifndef KEYVALUEPAIR_2_T2401056908_H
#define KEYVALUEPAIR_2_T2401056908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
struct  KeyValuePair_2_t2401056908 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2401056908, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2401056908, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2401056908_H
// Windows.Foundation.Collections.IVectorView`1<System.DateTimeOffset>
struct NOVTABLE IVectorView_1_t2568152769 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1224151036(uint32_t ___index0, DateTime_t1679451545 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1566737220(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1513712387(DateTime_t1679451545  ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m1032517645(uint32_t ___startIndex0, uint32_t ___items1ArraySize, DateTime_t1679451545 * ___items1, uint32_t* comReturnValue) = 0;
};
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef DATETIMEOFFSET_T3229287507_H
#define DATETIMEOFFSET_T3229287507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeOffset
struct  DateTimeOffset_t3229287507 
{
public:
	// System.DateTime System.DateTimeOffset::m_dateTime
	DateTime_t3738529785  ___m_dateTime_2;
	// System.Int16 System.DateTimeOffset::m_offsetMinutes
	int16_t ___m_offsetMinutes_3;

public:
	inline static int32_t get_offset_of_m_dateTime_2() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507, ___m_dateTime_2)); }
	inline DateTime_t3738529785  get_m_dateTime_2() const { return ___m_dateTime_2; }
	inline DateTime_t3738529785 * get_address_of_m_dateTime_2() { return &___m_dateTime_2; }
	inline void set_m_dateTime_2(DateTime_t3738529785  value)
	{
		___m_dateTime_2 = value;
	}

	inline static int32_t get_offset_of_m_offsetMinutes_3() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507, ___m_offsetMinutes_3)); }
	inline int16_t get_m_offsetMinutes_3() const { return ___m_offsetMinutes_3; }
	inline int16_t* get_address_of_m_offsetMinutes_3() { return &___m_offsetMinutes_3; }
	inline void set_m_offsetMinutes_3(int16_t value)
	{
		___m_offsetMinutes_3 = value;
	}
};

struct DateTimeOffset_t3229287507_StaticFields
{
public:
	// System.DateTimeOffset System.DateTimeOffset::MinValue
	DateTimeOffset_t3229287507  ___MinValue_0;
	// System.DateTimeOffset System.DateTimeOffset::MaxValue
	DateTimeOffset_t3229287507  ___MaxValue_1;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507_StaticFields, ___MinValue_0)); }
	inline DateTimeOffset_t3229287507  get_MinValue_0() const { return ___MinValue_0; }
	inline DateTimeOffset_t3229287507 * get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(DateTimeOffset_t3229287507  value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507_StaticFields, ___MaxValue_1)); }
	inline DateTimeOffset_t3229287507  get_MaxValue_1() const { return ___MaxValue_1; }
	inline DateTimeOffset_t3229287507 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(DateTimeOffset_t3229287507  value)
	{
		___MaxValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEOFFSET_T3229287507_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
// Windows.Foundation.Collections.IVectorView`1<System.Guid>
struct NOVTABLE IVectorView_1_t2532398149 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3400009220(uint32_t ___index0, Guid_t * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m50190753(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m4154363339(Guid_t  ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2882594533(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Guid_t * ___items1, uint32_t* comReturnValue) = 0;
};
#ifndef KEYVALUEPAIR_2_T3930634460_H
#define KEYVALUEPAIR_2_T3930634460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>
struct  KeyValuePair_2_t3930634460 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Guid_t  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	Type_t * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3930634460, ___key_0)); }
	inline Guid_t  get_key_0() const { return ___key_0; }
	inline Guid_t * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Guid_t  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3930634460, ___value_1)); }
	inline Type_t * get_value_1() const { return ___value_1; }
	inline Type_t ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Type_t * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3930634460_H
// Windows.Foundation.Collections.IKeyValuePair`2<System.Guid,System.Object>
struct NOVTABLE IKeyValuePair_2_t1256548186 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m3324945768(Guid_t * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m52124236(Il2CppIInspectable** comReturnValue) = 0;
};
#ifndef KEYVALUEPAIR_2_T231828568_H
#define KEYVALUEPAIR_2_T231828568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>
struct  KeyValuePair_2_t231828568 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Guid_t  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t231828568, ___key_0)); }
	inline Guid_t  get_key_0() const { return ___key_0; }
	inline Guid_t * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Guid_t  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t231828568, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T231828568_H
// Windows.Foundation.Collections.IKeyValuePair`2<System.Guid,System.Type>
struct NOVTABLE IKeyValuePair_2_t660386782 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m1247672299(Guid_t * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m3450640679(Type_t ** comReturnValue) = 0;
};
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_0;

public:
	inline static int32_t get_offset_of__impl_0() { return static_cast<int32_t>(offsetof(Type_t, ____impl_0)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_0() const { return ____impl_0; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_0() { return &____impl_0; }
	inline void set__impl_0(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_0 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_1;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_2;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_3;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_4;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_5;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_6;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2999457153 * ___defaultBinder_7;

public:
	inline static int32_t get_offset_of_FilterAttribute_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_1)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_1() const { return ___FilterAttribute_1; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_1() { return &___FilterAttribute_1; }
	inline void set_FilterAttribute_1(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_1), value);
	}

	inline static int32_t get_offset_of_FilterName_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_2)); }
	inline MemberFilter_t426314064 * get_FilterName_2() const { return ___FilterName_2; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_2() { return &___FilterName_2; }
	inline void set_FilterName_2(MemberFilter_t426314064 * value)
	{
		___FilterName_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_2), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_3)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_3() const { return ___FilterNameIgnoreCase_3; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_3() { return &___FilterNameIgnoreCase_3; }
	inline void set_FilterNameIgnoreCase_3(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_3 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_3), value);
	}

	inline static int32_t get_offset_of_Missing_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_4)); }
	inline RuntimeObject * get_Missing_4() const { return ___Missing_4; }
	inline RuntimeObject ** get_address_of_Missing_4() { return &___Missing_4; }
	inline void set_Missing_4(RuntimeObject * value)
	{
		___Missing_4 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_4), value);
	}

	inline static int32_t get_offset_of_Delimiter_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_5)); }
	inline Il2CppChar get_Delimiter_5() const { return ___Delimiter_5; }
	inline Il2CppChar* get_address_of_Delimiter_5() { return &___Delimiter_5; }
	inline void set_Delimiter_5(Il2CppChar value)
	{
		___Delimiter_5 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_6)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_6() const { return ___EmptyTypes_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_6() { return &___EmptyTypes_6; }
	inline void set_EmptyTypes_6(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_6 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_6), value);
	}

	inline static int32_t get_offset_of_defaultBinder_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_7)); }
	inline Binder_t2999457153 * get_defaultBinder_7() const { return ___defaultBinder_7; }
	inline Binder_t2999457153 ** get_address_of_defaultBinder_7() { return &___defaultBinder_7; }
	inline void set_defaultBinder_7(Binder_t2999457153 * value)
	{
		___defaultBinder_7 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
// System.Collections.Generic.IEnumerable`1<System.Byte>[]
struct IEnumerable_1U5BU5D_t3387783948  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IEnumerable`1<System.Char>[]
struct IEnumerable_1U5BU5D_t2799407958  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct IEnumerable_1U5BU5D_t3684372801  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>[]
struct IEnumerable_1U5BU5D_t614637069  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IEnumerable`1<System.Int32>[]
struct IEnumerable_1U5BU5D_t3951349959  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IEnumerable`1<System.Object>[]
struct IEnumerable_1U5BU5D_t2115075616  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IEnumerable`1<System.Type>[]
struct IEnumerable_1U5BU5D_t3212016396  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IList`1<System.Byte>[]
struct IList_1U5BU5D_t3612319366  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IList`1<System.Char>[]
struct IList_1U5BU5D_t3023943376  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct IList_1U5BU5D_t3908908219  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IList`1<System.Int32>[]
struct IList_1U5BU5D_t4175885377  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IList`1<System.Object>[]
struct IList_1U5BU5D_t2339611034  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IList`1<System.Type>[]
struct IList_1U5BU5D_t3436551814  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IReadOnlyList`1<System.Byte>[]
struct IReadOnlyList_1U5BU5D_t2744586439  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IReadOnlyList`1<System.Char>[]
struct IReadOnlyList_1U5BU5D_t2156210449  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct IReadOnlyList_1U5BU5D_t3041175292  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IReadOnlyList`1<System.Int32>[]
struct IReadOnlyList_1U5BU5D_t3308152450  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IReadOnlyList`1<System.Object>[]
struct IReadOnlyList_1U5BU5D_t1471878107  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IReadOnlyList`1<System.Type>[]
struct IReadOnlyList_1U5BU5D_t2568818887  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>[]
struct KeyValuePair_2U5BU5D_t3332857673  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t231828568  m_Items[1];

public:
	inline KeyValuePair_2_t231828568  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t231828568 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t231828568  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t231828568  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t231828568 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t231828568  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>[]
struct KeyValuePair_2U5BU5D_t134831157  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3930634460  m_Items[1];

public:
	inline KeyValuePair_2_t3930634460  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t3930634460 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3930634460  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t3930634460  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t3930634460 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t3930634460  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t2652375035  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t71524366  m_Items[1];

public:
	inline KeyValuePair_2_t71524366  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t71524366 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t71524366  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t71524366  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t71524366 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t71524366  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>[]
struct KeyValuePair_2U5BU5D_t3818836818  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2245450819  m_Items[1];

public:
	inline KeyValuePair_2_t2245450819  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t2245450819 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2245450819  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t2245450819  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t2245450819 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t2245450819  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t171748081  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3842366416  m_Items[1];

public:
	inline KeyValuePair_2_t3842366416  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t3842366416 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3842366416  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t3842366416  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t3842366416 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t3842366416  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1954543557  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2401056908  m_Items[1];

public:
	inline KeyValuePair_2_t2401056908  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t2401056908 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2401056908  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t2401056908  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t2401056908 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t2401056908  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t118269214  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2530217319  m_Items[1];

public:
	inline KeyValuePair_2_t2530217319  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t2530217319 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2530217319  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t2530217319  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t2530217319 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t2530217319  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.String>[]
struct KeyValuePair_2U5BU5D_t2851086525  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1297561844  m_Items[1];

public:
	inline KeyValuePair_2_t1297561844  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t1297561844 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1297561844  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t1297561844  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t1297561844 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t1297561844  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t1951926774  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2280216431  m_Items[1];

public:
	inline KeyValuePair_2_t2280216431  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t2280216431 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2280216431  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t2280216431  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t2280216431 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t2280216431  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3734722250  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t838906923  m_Items[1];

public:
	inline KeyValuePair_2_t838906923  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t838906923 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t838906923  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t838906923  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t838906923 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t838906923  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct KeyValuePair_2U5BU5D_t1898447907  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t968067334  m_Items[1];

public:
	inline KeyValuePair_2_t968067334  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t968067334 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t968067334  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t968067334  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t968067334 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t968067334  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct KeyValuePair_2U5BU5D_t336297922  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4030379155  m_Items[1];

public:
	inline KeyValuePair_2_t4030379155  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t4030379155 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4030379155  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t4030379155  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t4030379155 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t4030379155  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Type>[]
struct KeyValuePair_2U5BU5D_t2995388687  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t371905930  m_Items[1];

public:
	inline KeyValuePair_2_t371905930  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t371905930 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t371905930  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t371905930  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t371905930 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t371905930  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>[]
struct KeyValuePair_2U5BU5D_t130504686  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3030996695  m_Items[1];

public:
	inline KeyValuePair_2_t3030996695  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t3030996695 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3030996695  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t3030996695  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t3030996695 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t3030996695  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>[]
struct KeyValuePair_2U5BU5D_t2544954260  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2457078697  m_Items[1];

public:
	inline KeyValuePair_2_t2457078697  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t2457078697 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2457078697  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t2457078697  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t2457078697 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t2457078697  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.IEnumerable[]
struct IEnumerableU5BU5D_t1343500778  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.IList[]
struct IListU5BU5D_t2181495985  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Specialized.INotifyCollectionChanged[]
struct INotifyCollectionChangedU5BU5D_t557462784  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.DateTimeOffset[]
struct DateTimeOffsetU5BU5D_t3473357058  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) DateTimeOffset_t3229287507  m_Items[1];

public:
	inline DateTimeOffset_t3229287507  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DateTimeOffset_t3229287507 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DateTimeOffset_t3229287507  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline DateTimeOffset_t3229287507  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DateTimeOffset_t3229287507 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DateTimeOffset_t3229287507  value)
	{
		m_Items[index] = value;
	}
};
// System.Double[]
struct DoubleU5BU5D_t3413330114  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) double m_Items[1];

public:
	inline double GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline double* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, double value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline double GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline double* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, double value)
	{
		m_Items[index] = value;
	}
};
// System.Exception[]
struct ExceptionU5BU5D_t2535001212  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Exception_t1436737249 * m_Items[1];

public:
	inline Exception_t1436737249 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Exception_t1436737249 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Exception_t1436737249 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Exception_t1436737249 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Exception_t1436737249 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Exception_t1436737249 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Guid[]
struct GuidU5BU5D_t545550574  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Guid_t  m_Items[1];

public:
	inline Guid_t  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Guid_t * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Guid_t  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Guid_t  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Guid_t * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Guid_t  value)
	{
		m_Items[index] = value;
	}
};
// System.IDisposable[]
struct IDisposableU5BU5D_t3584190570  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int16[]
struct Int16U5BU5D_t3686840178  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int16_t m_Items[1];

public:
	inline int16_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int16_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int16_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int16_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int16_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int16_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t385246372  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int64[]
struct Int64U5BU5D_t2559172825  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int64_t m_Items[1];

public:
	inline int64_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int64_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int64_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int64_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int64_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int64_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};



// System.Void System.ArgumentOutOfRangeException::.ctor(System.String,System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m282481429 (ArgumentOutOfRangeException_t777629997 * __this, String_t* ___paramName0, String_t* ___message1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::ToLocalTime(System.Boolean)
extern "C"  DateTimeOffset_t3229287507  DateTimeOffset_ToLocalTime_m1141180670 (DateTimeOffset_t3229287507 * __this, bool ___throwOnOverflow0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Byte>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m490189953 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m490189953_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3747981823* ____ivectorView_1_t3747981823 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3747981823::IID, reinterpret_cast<void**>(&____ivectorView_1_t3747981823));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterable_1_t3050661132* returnValue = NULL;
	hr = ____ivectorView_1_t3747981823->IVectorView_1_GetAt_m490189953(___index0, &returnValue);
	____ivectorView_1_t3747981823->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Byte>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m1475217278 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t3747981823* ____ivectorView_1_t3747981823 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3747981823::IID, reinterpret_cast<void**>(&____ivectorView_1_t3747981823));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3747981823->IVectorView_1_get_Size_m1475217278(&returnValue);
	____ivectorView_1_t3747981823->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Byte>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m413031432 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t3747981823* ____ivectorView_1_t3747981823 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3747981823::IID, reinterpret_cast<void**>(&____ivectorView_1_t3747981823));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IIterable_1_t3050661132* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IIterable_1_t3050661132::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IIterable_1_t3050661132>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t3747981823->IVectorView_1_IndexOf_m413031432(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t3747981823->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Byte>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m3046606360 (RuntimeObject* __this, uint32_t ___startIndex0, IEnumerable_1U5BU5D_t3387783948* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m3046606360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3747981823* ____ivectorView_1_t3747981823 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3747981823::IID, reinterpret_cast<void**>(&____ivectorView_1_t3747981823));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IIterable_1_t3050661132** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IIterable_1_t3050661132*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IIterable_1_t3050661132*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3747981823->IVectorView_1_GetMany_m3046606360(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t3747981823->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Char>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m217716711 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m217716711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1953178621* ____ivectorView_1_t1953178621 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1953178621::IID, reinterpret_cast<void**>(&____ivectorView_1_t1953178621));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterable_1_t1255857930* returnValue = NULL;
	hr = ____ivectorView_1_t1953178621->IVectorView_1_GetAt_m217716711(___index0, &returnValue);
	____ivectorView_1_t1953178621->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Char>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m859457156 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t1953178621* ____ivectorView_1_t1953178621 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1953178621::IID, reinterpret_cast<void**>(&____ivectorView_1_t1953178621));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1953178621->IVectorView_1_get_Size_m859457156(&returnValue);
	____ivectorView_1_t1953178621->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Char>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m4230661809 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t1953178621* ____ivectorView_1_t1953178621 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1953178621::IID, reinterpret_cast<void**>(&____ivectorView_1_t1953178621));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IIterable_1_t1255857930* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IIterable_1_t1255857930::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IIterable_1_t1255857930>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t1953178621->IVectorView_1_IndexOf_m4230661809(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t1953178621->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Char>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m2250330655 (RuntimeObject* __this, uint32_t ___startIndex0, IEnumerable_1U5BU5D_t2799407958* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m2250330655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1953178621* ____ivectorView_1_t1953178621 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1953178621::IID, reinterpret_cast<void**>(&____ivectorView_1_t1953178621));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IIterable_1_t1255857930** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IIterable_1_t1255857930*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IIterable_1_t1255857930*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1953178621->IVectorView_1_GetMany_m2250330655(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t1953178621->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m3769613402 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m3769613402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t848935470* ____ivectorView_1_t848935470 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t848935470::IID, reinterpret_cast<void**>(&____ivectorView_1_t848935470));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterable_1_t151614779* returnValue = NULL;
	hr = ____ivectorView_1_t848935470->IVectorView_1_GetAt_m3769613402(___index0, &returnValue);
	____ivectorView_1_t848935470->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m4098135628 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t848935470* ____ivectorView_1_t848935470 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t848935470::IID, reinterpret_cast<void**>(&____ivectorView_1_t848935470));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t848935470->IVectorView_1_get_Size_m4098135628(&returnValue);
	____ivectorView_1_t848935470->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m1726440118 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t848935470* ____ivectorView_1_t848935470 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t848935470::IID, reinterpret_cast<void**>(&____ivectorView_1_t848935470));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IIterable_1_t151614779* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IIterable_1_t151614779::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IIterable_1_t151614779>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t848935470->IVectorView_1_IndexOf_m1726440118(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t848935470->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m1634167149 (RuntimeObject* __this, uint32_t ___startIndex0, IEnumerable_1U5BU5D_t3684372801* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m1634167149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t848935470* ____ivectorView_1_t848935470 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t848935470::IID, reinterpret_cast<void**>(&____ivectorView_1_t848935470));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IIterable_1_t151614779** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IIterable_1_t151614779*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IIterable_1_t151614779*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t848935470->IVectorView_1_GetMany_m1634167149(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t848935470->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m3042122219 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m3042122219_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t259886162* ____ivectorView_1_t259886162 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t259886162::IID, reinterpret_cast<void**>(&____ivectorView_1_t259886162));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterable_1_t3857532767* returnValue = NULL;
	hr = ____ivectorView_1_t259886162->IVectorView_1_GetAt_m3042122219(___index0, &returnValue);
	____ivectorView_1_t259886162->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m4174749221 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t259886162* ____ivectorView_1_t259886162 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t259886162::IID, reinterpret_cast<void**>(&____ivectorView_1_t259886162));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t259886162->IVectorView_1_get_Size_m4174749221(&returnValue);
	____ivectorView_1_t259886162->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m4185849224 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t259886162* ____ivectorView_1_t259886162 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t259886162::IID, reinterpret_cast<void**>(&____ivectorView_1_t259886162));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IIterable_1_t3857532767* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IIterable_1_t3857532767::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IIterable_1_t3857532767>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t259886162->IVectorView_1_IndexOf_m4185849224(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t259886162->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m4072637945 (RuntimeObject* __this, uint32_t ___startIndex0, IEnumerable_1U5BU5D_t614637069* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m4072637945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t259886162* ____ivectorView_1_t259886162 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t259886162::IID, reinterpret_cast<void**>(&____ivectorView_1_t259886162));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IIterable_1_t3857532767** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IIterable_1_t3857532767*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IIterable_1_t3857532767*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t259886162->IVectorView_1_GetMany_m4072637945(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t259886162->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Int32>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m458852302 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m458852302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1269663904* ____ivectorView_1_t1269663904 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1269663904::IID, reinterpret_cast<void**>(&____ivectorView_1_t1269663904));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterable_1_t572343213* returnValue = NULL;
	hr = ____ivectorView_1_t1269663904->IVectorView_1_GetAt_m458852302(___index0, &returnValue);
	____ivectorView_1_t1269663904->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Int32>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m3390099310 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t1269663904* ____ivectorView_1_t1269663904 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1269663904::IID, reinterpret_cast<void**>(&____ivectorView_1_t1269663904));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1269663904->IVectorView_1_get_Size_m3390099310(&returnValue);
	____ivectorView_1_t1269663904->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Int32>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m179787137 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t1269663904* ____ivectorView_1_t1269663904 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1269663904::IID, reinterpret_cast<void**>(&____ivectorView_1_t1269663904));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IIterable_1_t572343213* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IIterable_1_t572343213::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IIterable_1_t572343213>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t1269663904->IVectorView_1_IndexOf_m179787137(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t1269663904->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Int32>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m1427262250 (RuntimeObject* __this, uint32_t ___startIndex0, IEnumerable_1U5BU5D_t3951349959* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m1427262250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1269663904* ____ivectorView_1_t1269663904 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1269663904::IID, reinterpret_cast<void**>(&____ivectorView_1_t1269663904));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IIterable_1_t572343213** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IIterable_1_t572343213*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IIterable_1_t572343213*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1269663904->IVectorView_1_GetMany_m1427262250(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t1269663904->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Object>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m2417665634 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m2417665634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1398824315* ____ivectorView_1_t1398824315 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1398824315::IID, reinterpret_cast<void**>(&____ivectorView_1_t1398824315));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterable_1_t701503624* returnValue = NULL;
	hr = ____ivectorView_1_t1398824315->IVectorView_1_GetAt_m2417665634(___index0, &returnValue);
	____ivectorView_1_t1398824315->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Object>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m1445025516 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t1398824315* ____ivectorView_1_t1398824315 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1398824315::IID, reinterpret_cast<void**>(&____ivectorView_1_t1398824315));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1398824315->IVectorView_1_get_Size_m1445025516(&returnValue);
	____ivectorView_1_t1398824315->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Object>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m1815040482 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t1398824315* ____ivectorView_1_t1398824315 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1398824315::IID, reinterpret_cast<void**>(&____ivectorView_1_t1398824315));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IIterable_1_t701503624* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IIterable_1_t701503624::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IIterable_1_t701503624>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t1398824315->IVectorView_1_IndexOf_m1815040482(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t1398824315->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Object>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m405762550 (RuntimeObject* __this, uint32_t ___startIndex0, IEnumerable_1U5BU5D_t2115075616* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m405762550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1398824315* ____ivectorView_1_t1398824315 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1398824315::IID, reinterpret_cast<void**>(&____ivectorView_1_t1398824315));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IIterable_1_t701503624** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IIterable_1_t701503624*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IIterable_1_t701503624*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1398824315->IVectorView_1_GetMany_m405762550(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t1398824315->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Type>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m2080563997 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m2080563997_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t802662911* ____ivectorView_1_t802662911 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t802662911::IID, reinterpret_cast<void**>(&____ivectorView_1_t802662911));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterable_1_t105342220* returnValue = NULL;
	hr = ____ivectorView_1_t802662911->IVectorView_1_GetAt_m2080563997(___index0, &returnValue);
	____ivectorView_1_t802662911->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Type>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m2628474012 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t802662911* ____ivectorView_1_t802662911 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t802662911::IID, reinterpret_cast<void**>(&____ivectorView_1_t802662911));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t802662911->IVectorView_1_get_Size_m2628474012(&returnValue);
	____ivectorView_1_t802662911->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Type>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m2055924243 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t802662911* ____ivectorView_1_t802662911 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t802662911::IID, reinterpret_cast<void**>(&____ivectorView_1_t802662911));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IIterable_1_t105342220* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IIterable_1_t105342220::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IIterable_1_t105342220>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t802662911->IVectorView_1_IndexOf_m2055924243(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t802662911->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IEnumerable`1<System.Type>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m4222947830 (RuntimeObject* __this, uint32_t ___startIndex0, IEnumerable_1U5BU5D_t3212016396* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m4222947830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t802662911* ____ivectorView_1_t802662911 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t802662911::IID, reinterpret_cast<void**>(&____ivectorView_1_t802662911));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IIterable_1_t105342220** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IIterable_1_t105342220*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IIterable_1_t105342220*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t802662911->IVectorView_1_GetMany_m4222947830(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t802662911->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Byte>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m3514813479 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m3514813479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2288481421* ____ivectorView_1_t2288481421 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2288481421::IID, reinterpret_cast<void**>(&____ivectorView_1_t2288481421));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVector_1_t1874918097* returnValue = NULL;
	hr = ____ivectorView_1_t2288481421->IVectorView_1_GetAt_m3514813479(___index0, &returnValue);
	____ivectorView_1_t2288481421->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Byte>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m432441603 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t2288481421* ____ivectorView_1_t2288481421 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2288481421::IID, reinterpret_cast<void**>(&____ivectorView_1_t2288481421));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2288481421->IVectorView_1_get_Size_m432441603(&returnValue);
	____ivectorView_1_t2288481421->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Byte>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m2177043747 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t2288481421* ____ivectorView_1_t2288481421 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2288481421::IID, reinterpret_cast<void**>(&____ivectorView_1_t2288481421));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IVector_1_t1874918097* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IVector_1_t1874918097::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IVector_1_t1874918097>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t2288481421->IVectorView_1_IndexOf_m2177043747(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t2288481421->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Byte>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m3919556009 (RuntimeObject* __this, uint32_t ___startIndex0, IList_1U5BU5D_t3612319366* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m3919556009_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2288481421* ____ivectorView_1_t2288481421 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2288481421::IID, reinterpret_cast<void**>(&____ivectorView_1_t2288481421));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IVector_1_t1874918097** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IVector_1_t1874918097*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IVector_1_t1874918097*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2288481421->IVectorView_1_GetMany_m3919556009(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t2288481421->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Char>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m929209378 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m929209378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t493678219* ____ivectorView_1_t493678219 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t493678219::IID, reinterpret_cast<void**>(&____ivectorView_1_t493678219));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVector_1_t80114895* returnValue = NULL;
	hr = ____ivectorView_1_t493678219->IVectorView_1_GetAt_m929209378(___index0, &returnValue);
	____ivectorView_1_t493678219->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Char>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m1197104070 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t493678219* ____ivectorView_1_t493678219 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t493678219::IID, reinterpret_cast<void**>(&____ivectorView_1_t493678219));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t493678219->IVectorView_1_get_Size_m1197104070(&returnValue);
	____ivectorView_1_t493678219->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Char>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m1593887152 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t493678219* ____ivectorView_1_t493678219 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t493678219::IID, reinterpret_cast<void**>(&____ivectorView_1_t493678219));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IVector_1_t80114895* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IVector_1_t80114895::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IVector_1_t80114895>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t493678219->IVectorView_1_IndexOf_m1593887152(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t493678219->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Char>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m71529672 (RuntimeObject* __this, uint32_t ___startIndex0, IList_1U5BU5D_t3023943376* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m71529672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t493678219* ____ivectorView_1_t493678219 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t493678219::IID, reinterpret_cast<void**>(&____ivectorView_1_t493678219));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IVector_1_t80114895** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IVector_1_t80114895*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IVector_1_t80114895*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t493678219->IVectorView_1_GetMany_m71529672(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t493678219->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m3312192623 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m3312192623_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3684402364* ____ivectorView_1_t3684402364 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3684402364::IID, reinterpret_cast<void**>(&____ivectorView_1_t3684402364));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVector_1_t3270839040* returnValue = NULL;
	hr = ____ivectorView_1_t3684402364->IVectorView_1_GetAt_m3312192623(___index0, &returnValue);
	____ivectorView_1_t3684402364->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m1958026243 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t3684402364* ____ivectorView_1_t3684402364 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3684402364::IID, reinterpret_cast<void**>(&____ivectorView_1_t3684402364));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3684402364->IVectorView_1_get_Size_m1958026243(&returnValue);
	____ivectorView_1_t3684402364->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m2900489442 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t3684402364* ____ivectorView_1_t3684402364 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3684402364::IID, reinterpret_cast<void**>(&____ivectorView_1_t3684402364));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IVector_1_t3270839040* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IVector_1_t3270839040::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IVector_1_t3270839040>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t3684402364->IVectorView_1_IndexOf_m2900489442(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t3684402364->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m2292353266 (RuntimeObject* __this, uint32_t ___startIndex0, IList_1U5BU5D_t3908908219* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m2292353266_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3684402364* ____ivectorView_1_t3684402364 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3684402364::IID, reinterpret_cast<void**>(&____ivectorView_1_t3684402364));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IVector_1_t3270839040** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IVector_1_t3270839040*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IVector_1_t3270839040*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3684402364->IVectorView_1_GetMany_m2292353266(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t3684402364->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Int32>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m2339960516 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m2339960516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t4105130798* ____ivectorView_1_t4105130798 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t4105130798::IID, reinterpret_cast<void**>(&____ivectorView_1_t4105130798));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVector_1_t3691567474* returnValue = NULL;
	hr = ____ivectorView_1_t4105130798->IVectorView_1_GetAt_m2339960516(___index0, &returnValue);
	____ivectorView_1_t4105130798->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Int32>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m804582976 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t4105130798* ____ivectorView_1_t4105130798 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t4105130798::IID, reinterpret_cast<void**>(&____ivectorView_1_t4105130798));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t4105130798->IVectorView_1_get_Size_m804582976(&returnValue);
	____ivectorView_1_t4105130798->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Int32>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m1759454951 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t4105130798* ____ivectorView_1_t4105130798 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t4105130798::IID, reinterpret_cast<void**>(&____ivectorView_1_t4105130798));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IVector_1_t3691567474* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IVector_1_t3691567474::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IVector_1_t3691567474>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t4105130798->IVectorView_1_IndexOf_m1759454951(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t4105130798->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Int32>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m3038433658 (RuntimeObject* __this, uint32_t ___startIndex0, IList_1U5BU5D_t4175885377* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m3038433658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t4105130798* ____ivectorView_1_t4105130798 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t4105130798::IID, reinterpret_cast<void**>(&____ivectorView_1_t4105130798));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IVector_1_t3691567474** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IVector_1_t3691567474*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IVector_1_t3691567474*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t4105130798->IVectorView_1_GetMany_m3038433658(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t4105130798->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Object>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m2862071070 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m2862071070_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t4234291209* ____ivectorView_1_t4234291209 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t4234291209::IID, reinterpret_cast<void**>(&____ivectorView_1_t4234291209));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVector_1_t3820727885* returnValue = NULL;
	hr = ____ivectorView_1_t4234291209->IVectorView_1_GetAt_m2862071070(___index0, &returnValue);
	____ivectorView_1_t4234291209->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Object>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m3624020244 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t4234291209* ____ivectorView_1_t4234291209 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t4234291209::IID, reinterpret_cast<void**>(&____ivectorView_1_t4234291209));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t4234291209->IVectorView_1_get_Size_m3624020244(&returnValue);
	____ivectorView_1_t4234291209->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Object>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m2814520576 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t4234291209* ____ivectorView_1_t4234291209 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t4234291209::IID, reinterpret_cast<void**>(&____ivectorView_1_t4234291209));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IVector_1_t3820727885* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IVector_1_t3820727885::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IVector_1_t3820727885>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t4234291209->IVectorView_1_IndexOf_m2814520576(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t4234291209->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Object>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m3396401288 (RuntimeObject* __this, uint32_t ___startIndex0, IList_1U5BU5D_t2339611034* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m3396401288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t4234291209* ____ivectorView_1_t4234291209 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t4234291209::IID, reinterpret_cast<void**>(&____ivectorView_1_t4234291209));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IVector_1_t3820727885** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IVector_1_t3820727885*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IVector_1_t3820727885*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t4234291209->IVectorView_1_GetMany_m3396401288(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t4234291209->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Type>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m2447159008 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m2447159008_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3638129805* ____ivectorView_1_t3638129805 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3638129805::IID, reinterpret_cast<void**>(&____ivectorView_1_t3638129805));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVector_1_t3224566481* returnValue = NULL;
	hr = ____ivectorView_1_t3638129805->IVectorView_1_GetAt_m2447159008(___index0, &returnValue);
	____ivectorView_1_t3638129805->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Type>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m3336166233 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t3638129805* ____ivectorView_1_t3638129805 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3638129805::IID, reinterpret_cast<void**>(&____ivectorView_1_t3638129805));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3638129805->IVectorView_1_get_Size_m3336166233(&returnValue);
	____ivectorView_1_t3638129805->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Type>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m3213369082 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t3638129805* ____ivectorView_1_t3638129805 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3638129805::IID, reinterpret_cast<void**>(&____ivectorView_1_t3638129805));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IVector_1_t3224566481* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IVector_1_t3224566481::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IVector_1_t3224566481>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t3638129805->IVectorView_1_IndexOf_m3213369082(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t3638129805->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IList`1<System.Type>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m2260873046 (RuntimeObject* __this, uint32_t ___startIndex0, IList_1U5BU5D_t3436551814* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m2260873046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3638129805* ____ivectorView_1_t3638129805 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3638129805::IID, reinterpret_cast<void**>(&____ivectorView_1_t3638129805));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IVector_1_t3224566481** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IVector_1_t3224566481*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IVector_1_t3224566481*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3638129805->IVectorView_1_GetMany_m2260873046(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t3638129805->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Byte>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m915303395 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m915303395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1037584544* ____ivectorView_1_t1037584544 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1037584544::IID, reinterpret_cast<void**>(&____ivectorView_1_t1037584544));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t473161638* returnValue = NULL;
	hr = ____ivectorView_1_t1037584544->IVectorView_1_GetAt_m915303395(___index0, &returnValue);
	____ivectorView_1_t1037584544->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Byte>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m3254210033 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t1037584544* ____ivectorView_1_t1037584544 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1037584544::IID, reinterpret_cast<void**>(&____ivectorView_1_t1037584544));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1037584544->IVectorView_1_get_Size_m3254210033(&returnValue);
	____ivectorView_1_t1037584544->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Byte>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m1711338065 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t1037584544* ____ivectorView_1_t1037584544 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1037584544::IID, reinterpret_cast<void**>(&____ivectorView_1_t1037584544));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IVectorView_1_t473161638* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IVectorView_1_t473161638::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IVectorView_1_t473161638>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t1037584544->IVectorView_1_IndexOf_m1711338065(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t1037584544->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Byte>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m8013008 (RuntimeObject* __this, uint32_t ___startIndex0, IReadOnlyList_1U5BU5D_t2744586439* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m8013008_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1037584544* ____ivectorView_1_t1037584544 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1037584544::IID, reinterpret_cast<void**>(&____ivectorView_1_t1037584544));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IVectorView_1_t473161638** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IVectorView_1_t473161638*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IVectorView_1_t473161638*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1037584544->IVectorView_1_GetMany_m8013008(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t1037584544->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Char>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m3633388355 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m3633388355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3537748638* ____ivectorView_1_t3537748638 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3537748638::IID, reinterpret_cast<void**>(&____ivectorView_1_t3537748638));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t2973325732* returnValue = NULL;
	hr = ____ivectorView_1_t3537748638->IVectorView_1_GetAt_m3633388355(___index0, &returnValue);
	____ivectorView_1_t3537748638->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Char>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m1501815834 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t3537748638* ____ivectorView_1_t3537748638 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3537748638::IID, reinterpret_cast<void**>(&____ivectorView_1_t3537748638));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3537748638->IVectorView_1_get_Size_m1501815834(&returnValue);
	____ivectorView_1_t3537748638->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Char>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m1146956588 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t3537748638* ____ivectorView_1_t3537748638 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3537748638::IID, reinterpret_cast<void**>(&____ivectorView_1_t3537748638));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IVectorView_1_t2973325732* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IVectorView_1_t2973325732::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IVectorView_1_t2973325732>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t3537748638->IVectorView_1_IndexOf_m1146956588(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t3537748638->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Char>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m1599459309 (RuntimeObject* __this, uint32_t ___startIndex0, IReadOnlyList_1U5BU5D_t2156210449* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m1599459309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3537748638* ____ivectorView_1_t3537748638 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3537748638::IID, reinterpret_cast<void**>(&____ivectorView_1_t3537748638));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IVectorView_1_t2973325732** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IVectorView_1_t2973325732*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IVectorView_1_t2973325732*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3537748638->IVectorView_1_GetMany_m1599459309(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t3537748638->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m1804213203 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m1804213203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2433505487* ____ivectorView_1_t2433505487 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2433505487::IID, reinterpret_cast<void**>(&____ivectorView_1_t2433505487));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t1869082581* returnValue = NULL;
	hr = ____ivectorView_1_t2433505487->IVectorView_1_GetAt_m1804213203(___index0, &returnValue);
	____ivectorView_1_t2433505487->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m2958914420 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t2433505487* ____ivectorView_1_t2433505487 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2433505487::IID, reinterpret_cast<void**>(&____ivectorView_1_t2433505487));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2433505487->IVectorView_1_get_Size_m2958914420(&returnValue);
	____ivectorView_1_t2433505487->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m794865249 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t2433505487* ____ivectorView_1_t2433505487 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2433505487::IID, reinterpret_cast<void**>(&____ivectorView_1_t2433505487));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IVectorView_1_t1869082581* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IVectorView_1_t1869082581::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IVectorView_1_t1869082581>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t2433505487->IVectorView_1_IndexOf_m794865249(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t2433505487->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m3746205704 (RuntimeObject* __this, uint32_t ___startIndex0, IReadOnlyList_1U5BU5D_t3041175292* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m3746205704_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2433505487* ____ivectorView_1_t2433505487 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2433505487::IID, reinterpret_cast<void**>(&____ivectorView_1_t2433505487));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IVectorView_1_t1869082581** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IVectorView_1_t1869082581*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IVectorView_1_t1869082581*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2433505487->IVectorView_1_GetMany_m3746205704(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t2433505487->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Int32>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m3621300116 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m3621300116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2854233921* ____ivectorView_1_t2854233921 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2854233921::IID, reinterpret_cast<void**>(&____ivectorView_1_t2854233921));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t2289811015* returnValue = NULL;
	hr = ____ivectorView_1_t2854233921->IVectorView_1_GetAt_m3621300116(___index0, &returnValue);
	____ivectorView_1_t2854233921->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Int32>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m481043476 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t2854233921* ____ivectorView_1_t2854233921 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2854233921::IID, reinterpret_cast<void**>(&____ivectorView_1_t2854233921));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2854233921->IVectorView_1_get_Size_m481043476(&returnValue);
	____ivectorView_1_t2854233921->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Int32>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m2242687983 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t2854233921* ____ivectorView_1_t2854233921 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2854233921::IID, reinterpret_cast<void**>(&____ivectorView_1_t2854233921));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IVectorView_1_t2289811015* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IVectorView_1_t2289811015::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IVectorView_1_t2289811015>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t2854233921->IVectorView_1_IndexOf_m2242687983(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t2854233921->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Int32>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m3521302946 (RuntimeObject* __this, uint32_t ___startIndex0, IReadOnlyList_1U5BU5D_t3308152450* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m3521302946_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2854233921* ____ivectorView_1_t2854233921 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2854233921::IID, reinterpret_cast<void**>(&____ivectorView_1_t2854233921));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IVectorView_1_t2289811015** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IVectorView_1_t2289811015*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IVectorView_1_t2289811015*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2854233921->IVectorView_1_GetMany_m3521302946(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t2854233921->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Object>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m1250314613 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m1250314613_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2983394332* ____ivectorView_1_t2983394332 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2983394332::IID, reinterpret_cast<void**>(&____ivectorView_1_t2983394332));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t2418971426* returnValue = NULL;
	hr = ____ivectorView_1_t2983394332->IVectorView_1_GetAt_m1250314613(___index0, &returnValue);
	____ivectorView_1_t2983394332->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Object>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m943330745 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t2983394332* ____ivectorView_1_t2983394332 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2983394332::IID, reinterpret_cast<void**>(&____ivectorView_1_t2983394332));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2983394332->IVectorView_1_get_Size_m943330745(&returnValue);
	____ivectorView_1_t2983394332->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Object>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m1217520174 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t2983394332* ____ivectorView_1_t2983394332 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2983394332::IID, reinterpret_cast<void**>(&____ivectorView_1_t2983394332));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IVectorView_1_t2418971426* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IVectorView_1_t2418971426::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IVectorView_1_t2418971426>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t2983394332->IVectorView_1_IndexOf_m1217520174(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t2983394332->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Object>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m2072164899 (RuntimeObject* __this, uint32_t ___startIndex0, IReadOnlyList_1U5BU5D_t1471878107* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m2072164899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2983394332* ____ivectorView_1_t2983394332 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2983394332::IID, reinterpret_cast<void**>(&____ivectorView_1_t2983394332));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IVectorView_1_t2418971426** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IVectorView_1_t2418971426*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IVectorView_1_t2418971426*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2983394332->IVectorView_1_GetMany_m2072164899(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t2983394332->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Type>>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m236800478 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m236800478_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2387232928* ____ivectorView_1_t2387232928 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2387232928::IID, reinterpret_cast<void**>(&____ivectorView_1_t2387232928));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t1822810022* returnValue = NULL;
	hr = ____ivectorView_1_t2387232928->IVectorView_1_GetAt_m236800478(___index0, &returnValue);
	____ivectorView_1_t2387232928->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Type>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m1539862075 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t2387232928* ____ivectorView_1_t2387232928 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2387232928::IID, reinterpret_cast<void**>(&____ivectorView_1_t2387232928));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2387232928->IVectorView_1_get_Size_m1539862075(&returnValue);
	____ivectorView_1_t2387232928->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Type>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m921936937 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t2387232928* ____ivectorView_1_t2387232928 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2387232928::IID, reinterpret_cast<void**>(&____ivectorView_1_t2387232928));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IVectorView_1_t1822810022* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IVectorView_1_t1822810022::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IVectorView_1_t1822810022>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t2387232928->IVectorView_1_IndexOf_m921936937(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t2387232928->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.IReadOnlyList`1<System.Type>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m2274155770 (RuntimeObject* __this, uint32_t ___startIndex0, IReadOnlyList_1U5BU5D_t2568818887* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m2274155770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2387232928* ____ivectorView_1_t2387232928 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2387232928::IID, reinterpret_cast<void**>(&____ivectorView_1_t2387232928));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IVectorView_1_t1822810022** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IVectorView_1_t1822810022*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IVectorView_1_t1822810022*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2387232928->IVectorView_1_GetMany_m2274155770(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t2387232928->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::GetAt(System.UInt32)
extern "C"  KeyValuePair_2_t231828568  IVectorView_1_GetAt_m581037041 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m581037041_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3865661126* ____ivectorView_1_t3865661126 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3865661126::IID, reinterpret_cast<void**>(&____ivectorView_1_t3865661126));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IKeyValuePair_2_t1256548186* returnValue = NULL;
	hr = ____ivectorView_1_t3865661126->IVectorView_1_GetAt_m581037041(___index0, &returnValue);
	____ivectorView_1_t3865661126->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	KeyValuePair_2_t231828568  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	NullCheck(returnValue);

	{
		Il2CppIManagedObjectHolder* returnValue_imanagedObject = NULL;
		il2cpp_hresult_t hr = (returnValue)->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&returnValue_imanagedObject));
		if (IL2CPP_HR_SUCCEEDED(hr))
		{
			_returnValue_unmarshaled = *static_cast<KeyValuePair_2_t231828568 *>(UnBox(returnValue_imanagedObject->GetManagedObject(), KeyValuePair_2_t231828568_il2cpp_TypeInfo_var));
			returnValue_imanagedObject->Release();
		}
		else
		{
			KeyValuePair_2_t231828568  returnValueStaging;
			Guid_t  returnValueKeyNative = {};
			hr = (returnValue)->IKeyValuePair_2_get_Key_m3324945768(&returnValueKeyNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_key_0(returnValueKeyNative);

			Il2CppIInspectable* returnValueValueNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Value_m52124236(&returnValueValueNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (returnValueValueNative != NULL)
			{
				returnValueStaging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValueValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
			}
			else
			{
				returnValueStaging.set_value_1(NULL);
			}

			if (returnValueValueNative != NULL)
			{
				(returnValueValueNative)->Release();
				returnValueValueNative = NULL;
			}

			_returnValue_unmarshaled = returnValueStaging;
		}
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m1855235872 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t3865661126* ____ivectorView_1_t3865661126 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3865661126::IID, reinterpret_cast<void**>(&____ivectorView_1_t3865661126));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3865661126->IVectorView_1_get_Size_m1855235872(&returnValue);
	____ivectorView_1_t3865661126->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m3984942326 (RuntimeObject* __this, KeyValuePair_2_t231828568  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_IndexOf_m3984942326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3865661126* ____ivectorView_1_t3865661126 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3865661126::IID, reinterpret_cast<void**>(&____ivectorView_1_t3865661126));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IKeyValuePair_2_t1256548186* ____value0_marshaled = NULL;
	____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IKeyValuePair_2_t1256548186>(Box(KeyValuePair_2_t231828568_il2cpp_TypeInfo_var, &___value0));

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t3865661126->IVectorView_1_IndexOf_m3984942326(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t3865661126->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m910516119 (RuntimeObject* __this, uint32_t ___startIndex0, KeyValuePair_2U5BU5D_t3332857673* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m910516119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3865661126* ____ivectorView_1_t3865661126 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3865661126::IID, reinterpret_cast<void**>(&____ivectorView_1_t3865661126));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IKeyValuePair_2_t1256548186** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IKeyValuePair_2_t1256548186*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IKeyValuePair_2_t1256548186*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3865661126->IVectorView_1_GetMany_m910516119(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t3865661126->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			KeyValuePair_2_t231828568  _____items1_marshaled_i__unmarshaled;
			memset(&_____items1_marshaled_i__unmarshaled, 0, sizeof(_____items1_marshaled_i__unmarshaled));
			NullCheck((____items1_marshaled)[i]);

			{
				Il2CppIManagedObjectHolder* ____items1_marshaled_i__imanagedObject = NULL;
				il2cpp_hresult_t hr = ((____items1_marshaled)[i])->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&____items1_marshaled_i__imanagedObject));
				if (IL2CPP_HR_SUCCEEDED(hr))
				{
					_____items1_marshaled_i__unmarshaled = *static_cast<KeyValuePair_2_t231828568 *>(UnBox(____items1_marshaled_i__imanagedObject->GetManagedObject(), KeyValuePair_2_t231828568_il2cpp_TypeInfo_var));
					____items1_marshaled_i__imanagedObject->Release();
				}
				else
				{
					KeyValuePair_2_t231828568  ____items1_marshaled_i_Staging;
					Guid_t  ____items1_marshaled_i_KeyNative = {};
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Key_m3324945768(&____items1_marshaled_i_KeyNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items1_marshaled_i_Staging.set_key_0(____items1_marshaled_i_KeyNative);

					Il2CppIInspectable* ____items1_marshaled_i_ValueNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Value_m52124236(&____items1_marshaled_i_ValueNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					if (____items1_marshaled_i_ValueNative != NULL)
					{
						____items1_marshaled_i_Staging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(____items1_marshaled_i_ValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
					}
					else
					{
						____items1_marshaled_i_Staging.set_value_1(NULL);
					}

					if (____items1_marshaled_i_ValueNative != NULL)
					{
						(____items1_marshaled_i_ValueNative)->Release();
						____items1_marshaled_i_ValueNative = NULL;
					}

					_____items1_marshaled_i__unmarshaled = ____items1_marshaled_i_Staging;
				}
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>>::GetAt(System.UInt32)
extern "C"  KeyValuePair_2_t3930634460  IVectorView_1_GetAt_m310514428 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m3486021694 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t3269499722* ____ivectorView_1_t3269499722 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3269499722::IID, reinterpret_cast<void**>(&____ivectorView_1_t3269499722));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3269499722->IVectorView_1_get_Size_m3486021694(&returnValue);
	____ivectorView_1_t3269499722->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m3026496961 (RuntimeObject* __this, KeyValuePair_2_t3930634460  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_IndexOf_m3026496961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3269499722* ____ivectorView_1_t3269499722 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3269499722::IID, reinterpret_cast<void**>(&____ivectorView_1_t3269499722));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IKeyValuePair_2_t660386782* ____value0_marshaled = NULL;
	____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IKeyValuePair_2_t660386782>(Box(KeyValuePair_2_t3930634460_il2cpp_TypeInfo_var, &___value0));

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t3269499722->IVectorView_1_IndexOf_m3026496961(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t3269499722->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m3259869068 (RuntimeObject* __this, uint32_t ___startIndex0, KeyValuePair_2U5BU5D_t134831157* ___items1, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::GetAt(System.UInt32)
extern "C"  KeyValuePair_2_t71524366  IVectorView_1_GetAt_m1033266262 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m1033266262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3705356924* ____ivectorView_1_t3705356924 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3705356924::IID, reinterpret_cast<void**>(&____ivectorView_1_t3705356924));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IKeyValuePair_2_t1096243984* returnValue = NULL;
	hr = ____ivectorView_1_t3705356924->IVectorView_1_GetAt_m1033266262(___index0, &returnValue);
	____ivectorView_1_t3705356924->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	KeyValuePair_2_t71524366  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	NullCheck(returnValue);

	{
		Il2CppIManagedObjectHolder* returnValue_imanagedObject = NULL;
		il2cpp_hresult_t hr = (returnValue)->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&returnValue_imanagedObject));
		if (IL2CPP_HR_SUCCEEDED(hr))
		{
			_returnValue_unmarshaled = *static_cast<KeyValuePair_2_t71524366 *>(UnBox(returnValue_imanagedObject->GetManagedObject(), KeyValuePair_2_t71524366_il2cpp_TypeInfo_var));
			returnValue_imanagedObject->Release();
		}
		else
		{
			KeyValuePair_2_t71524366  returnValueStaging;
			int32_t returnValueKeyNative = 0;
			hr = (returnValue)->IKeyValuePair_2_get_Key_m2186241555(&returnValueKeyNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_key_0(returnValueKeyNative);

			Il2CppIInspectable* returnValueValueNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Value_m210688108(&returnValueValueNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (returnValueValueNative != NULL)
			{
				returnValueStaging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValueValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
			}
			else
			{
				returnValueStaging.set_value_1(NULL);
			}

			if (returnValueValueNative != NULL)
			{
				(returnValueValueNative)->Release();
				returnValueValueNative = NULL;
			}

			_returnValue_unmarshaled = returnValueStaging;
		}
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m777270669 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t3705356924* ____ivectorView_1_t3705356924 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3705356924::IID, reinterpret_cast<void**>(&____ivectorView_1_t3705356924));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3705356924->IVectorView_1_get_Size_m777270669(&returnValue);
	____ivectorView_1_t3705356924->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m2919924410 (RuntimeObject* __this, KeyValuePair_2_t71524366  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_IndexOf_m2919924410_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3705356924* ____ivectorView_1_t3705356924 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3705356924::IID, reinterpret_cast<void**>(&____ivectorView_1_t3705356924));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IKeyValuePair_2_t1096243984* ____value0_marshaled = NULL;
	____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IKeyValuePair_2_t1096243984>(Box(KeyValuePair_2_t71524366_il2cpp_TypeInfo_var, &___value0));

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t3705356924->IVectorView_1_IndexOf_m2919924410(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t3705356924->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m2431495291 (RuntimeObject* __this, uint32_t ___startIndex0, KeyValuePair_2U5BU5D_t2652375035* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m2431495291_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3705356924* ____ivectorView_1_t3705356924 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3705356924::IID, reinterpret_cast<void**>(&____ivectorView_1_t3705356924));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IKeyValuePair_2_t1096243984** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IKeyValuePair_2_t1096243984*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IKeyValuePair_2_t1096243984*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3705356924->IVectorView_1_GetMany_m2431495291(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t3705356924->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			KeyValuePair_2_t71524366  _____items1_marshaled_i__unmarshaled;
			memset(&_____items1_marshaled_i__unmarshaled, 0, sizeof(_____items1_marshaled_i__unmarshaled));
			NullCheck((____items1_marshaled)[i]);

			{
				Il2CppIManagedObjectHolder* ____items1_marshaled_i__imanagedObject = NULL;
				il2cpp_hresult_t hr = ((____items1_marshaled)[i])->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&____items1_marshaled_i__imanagedObject));
				if (IL2CPP_HR_SUCCEEDED(hr))
				{
					_____items1_marshaled_i__unmarshaled = *static_cast<KeyValuePair_2_t71524366 *>(UnBox(____items1_marshaled_i__imanagedObject->GetManagedObject(), KeyValuePair_2_t71524366_il2cpp_TypeInfo_var));
					____items1_marshaled_i__imanagedObject->Release();
				}
				else
				{
					KeyValuePair_2_t71524366  ____items1_marshaled_i_Staging;
					int32_t ____items1_marshaled_i_KeyNative = 0;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Key_m2186241555(&____items1_marshaled_i_KeyNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items1_marshaled_i_Staging.set_key_0(____items1_marshaled_i_KeyNative);

					Il2CppIInspectable* ____items1_marshaled_i_ValueNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Value_m210688108(&____items1_marshaled_i_ValueNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					if (____items1_marshaled_i_ValueNative != NULL)
					{
						____items1_marshaled_i_Staging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(____items1_marshaled_i_ValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
					}
					else
					{
						____items1_marshaled_i_Staging.set_value_1(NULL);
					}

					if (____items1_marshaled_i_ValueNative != NULL)
					{
						(____items1_marshaled_i_ValueNative)->Release();
						____items1_marshaled_i_ValueNative = NULL;
					}

					_____items1_marshaled_i__unmarshaled = ____items1_marshaled_i_Staging;
				}
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::GetAt(System.UInt32)
extern "C"  KeyValuePair_2_t2245450819  IVectorView_1_GetAt_m3698596162 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m3698596162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1584316081* ____ivectorView_1_t1584316081 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1584316081::IID, reinterpret_cast<void**>(&____ivectorView_1_t1584316081));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IKeyValuePair_2_t3270170437* returnValue = NULL;
	hr = ____ivectorView_1_t1584316081->IVectorView_1_GetAt_m3698596162(___index0, &returnValue);
	____ivectorView_1_t1584316081->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	KeyValuePair_2_t2245450819  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	NullCheck(returnValue);

	{
		Il2CppIManagedObjectHolder* returnValue_imanagedObject = NULL;
		il2cpp_hresult_t hr = (returnValue)->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&returnValue_imanagedObject));
		if (IL2CPP_HR_SUCCEEDED(hr))
		{
			_returnValue_unmarshaled = *static_cast<KeyValuePair_2_t2245450819 *>(UnBox(returnValue_imanagedObject->GetManagedObject(), KeyValuePair_2_t2245450819_il2cpp_TypeInfo_var));
			returnValue_imanagedObject->Release();
		}
		else
		{
			KeyValuePair_2_t2245450819  returnValueStaging;
			int64_t returnValueKeyNative = 0;
			hr = (returnValue)->IKeyValuePair_2_get_Key_m1937891137(&returnValueKeyNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_key_0(returnValueKeyNative);

			Il2CppIInspectable* returnValueValueNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Value_m1356929877(&returnValueValueNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (returnValueValueNative != NULL)
			{
				returnValueStaging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValueValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
			}
			else
			{
				returnValueStaging.set_value_1(NULL);
			}

			if (returnValueValueNative != NULL)
			{
				(returnValueValueNative)->Release();
				returnValueValueNative = NULL;
			}

			_returnValue_unmarshaled = returnValueStaging;
		}
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m3315242568 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t1584316081* ____ivectorView_1_t1584316081 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1584316081::IID, reinterpret_cast<void**>(&____ivectorView_1_t1584316081));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1584316081->IVectorView_1_get_Size_m3315242568(&returnValue);
	____ivectorView_1_t1584316081->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m195073214 (RuntimeObject* __this, KeyValuePair_2_t2245450819  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_IndexOf_m195073214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1584316081* ____ivectorView_1_t1584316081 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1584316081::IID, reinterpret_cast<void**>(&____ivectorView_1_t1584316081));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IKeyValuePair_2_t3270170437* ____value0_marshaled = NULL;
	____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IKeyValuePair_2_t3270170437>(Box(KeyValuePair_2_t2245450819_il2cpp_TypeInfo_var, &___value0));

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t1584316081->IVectorView_1_IndexOf_m195073214(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t1584316081->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m1367398776 (RuntimeObject* __this, uint32_t ___startIndex0, KeyValuePair_2U5BU5D_t3818836818* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m1367398776_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1584316081* ____ivectorView_1_t1584316081 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1584316081::IID, reinterpret_cast<void**>(&____ivectorView_1_t1584316081));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IKeyValuePair_2_t3270170437** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IKeyValuePair_2_t3270170437*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IKeyValuePair_2_t3270170437*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1584316081->IVectorView_1_GetMany_m1367398776(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t1584316081->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			KeyValuePair_2_t2245450819  _____items1_marshaled_i__unmarshaled;
			memset(&_____items1_marshaled_i__unmarshaled, 0, sizeof(_____items1_marshaled_i__unmarshaled));
			NullCheck((____items1_marshaled)[i]);

			{
				Il2CppIManagedObjectHolder* ____items1_marshaled_i__imanagedObject = NULL;
				il2cpp_hresult_t hr = ((____items1_marshaled)[i])->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&____items1_marshaled_i__imanagedObject));
				if (IL2CPP_HR_SUCCEEDED(hr))
				{
					_____items1_marshaled_i__unmarshaled = *static_cast<KeyValuePair_2_t2245450819 *>(UnBox(____items1_marshaled_i__imanagedObject->GetManagedObject(), KeyValuePair_2_t2245450819_il2cpp_TypeInfo_var));
					____items1_marshaled_i__imanagedObject->Release();
				}
				else
				{
					KeyValuePair_2_t2245450819  ____items1_marshaled_i_Staging;
					int64_t ____items1_marshaled_i_KeyNative = 0;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Key_m1937891137(&____items1_marshaled_i_KeyNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items1_marshaled_i_Staging.set_key_0(____items1_marshaled_i_KeyNative);

					Il2CppIInspectable* ____items1_marshaled_i_ValueNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Value_m1356929877(&____items1_marshaled_i_ValueNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					if (____items1_marshaled_i_ValueNative != NULL)
					{
						____items1_marshaled_i_Staging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(____items1_marshaled_i_ValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
					}
					else
					{
						____items1_marshaled_i_Staging.set_value_1(NULL);
					}

					if (____items1_marshaled_i_ValueNative != NULL)
					{
						(____items1_marshaled_i_ValueNative)->Release();
						____items1_marshaled_i_ValueNative = NULL;
					}

					_____items1_marshaled_i__unmarshaled = ____items1_marshaled_i_Staging;
				}
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::GetAt(System.UInt32)
extern "C"  KeyValuePair_2_t3842366416  IVectorView_1_GetAt_m2114263251 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m2114263251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3181231678* ____ivectorView_1_t3181231678 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3181231678::IID, reinterpret_cast<void**>(&____ivectorView_1_t3181231678));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IKeyValuePair_2_t572118738* returnValue = NULL;
	hr = ____ivectorView_1_t3181231678->IVectorView_1_GetAt_m2114263251(___index0, &returnValue);
	____ivectorView_1_t3181231678->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	KeyValuePair_2_t3842366416  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	NullCheck(returnValue);

	{
		Il2CppIManagedObjectHolder* returnValue_imanagedObject = NULL;
		il2cpp_hresult_t hr = (returnValue)->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&returnValue_imanagedObject));
		if (IL2CPP_HR_SUCCEEDED(hr))
		{
			_returnValue_unmarshaled = *static_cast<KeyValuePair_2_t3842366416 *>(UnBox(returnValue_imanagedObject->GetManagedObject(), KeyValuePair_2_t3842366416_il2cpp_TypeInfo_var));
			returnValue_imanagedObject->Release();
		}
		else
		{
			KeyValuePair_2_t3842366416  returnValueStaging;
			Il2CppIInspectable* returnValueKeyNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Key_m1916408783(&returnValueKeyNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (returnValueKeyNative != NULL)
			{
				returnValueStaging.set_key_0(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValueKeyNative, Il2CppComObject_il2cpp_TypeInfo_var));
			}
			else
			{
				returnValueStaging.set_key_0(NULL);
			}

			if (returnValueKeyNative != NULL)
			{
				(returnValueKeyNative)->Release();
				returnValueKeyNative = NULL;
			}

			bool returnValueValueNative = 0;
			hr = (returnValue)->IKeyValuePair_2_get_Value_m3611222262(&returnValueValueNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_value_1(returnValueValueNative);

			_returnValue_unmarshaled = returnValueStaging;
		}
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m2486136128 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t3181231678* ____ivectorView_1_t3181231678 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3181231678::IID, reinterpret_cast<void**>(&____ivectorView_1_t3181231678));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3181231678->IVectorView_1_get_Size_m2486136128(&returnValue);
	____ivectorView_1_t3181231678->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m1800219957 (RuntimeObject* __this, KeyValuePair_2_t3842366416  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_IndexOf_m1800219957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3181231678* ____ivectorView_1_t3181231678 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3181231678::IID, reinterpret_cast<void**>(&____ivectorView_1_t3181231678));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IKeyValuePair_2_t572118738* ____value0_marshaled = NULL;
	____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IKeyValuePair_2_t572118738>(Box(KeyValuePair_2_t3842366416_il2cpp_TypeInfo_var, &___value0));

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t3181231678->IVectorView_1_IndexOf_m1800219957(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t3181231678->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m792906882 (RuntimeObject* __this, uint32_t ___startIndex0, KeyValuePair_2U5BU5D_t171748081* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m792906882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3181231678* ____ivectorView_1_t3181231678 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3181231678::IID, reinterpret_cast<void**>(&____ivectorView_1_t3181231678));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IKeyValuePair_2_t572118738** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IKeyValuePair_2_t572118738*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IKeyValuePair_2_t572118738*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3181231678->IVectorView_1_GetMany_m792906882(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t3181231678->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			KeyValuePair_2_t3842366416  _____items1_marshaled_i__unmarshaled;
			memset(&_____items1_marshaled_i__unmarshaled, 0, sizeof(_____items1_marshaled_i__unmarshaled));
			NullCheck((____items1_marshaled)[i]);

			{
				Il2CppIManagedObjectHolder* ____items1_marshaled_i__imanagedObject = NULL;
				il2cpp_hresult_t hr = ((____items1_marshaled)[i])->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&____items1_marshaled_i__imanagedObject));
				if (IL2CPP_HR_SUCCEEDED(hr))
				{
					_____items1_marshaled_i__unmarshaled = *static_cast<KeyValuePair_2_t3842366416 *>(UnBox(____items1_marshaled_i__imanagedObject->GetManagedObject(), KeyValuePair_2_t3842366416_il2cpp_TypeInfo_var));
					____items1_marshaled_i__imanagedObject->Release();
				}
				else
				{
					KeyValuePair_2_t3842366416  ____items1_marshaled_i_Staging;
					Il2CppIInspectable* ____items1_marshaled_i_KeyNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Key_m1916408783(&____items1_marshaled_i_KeyNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					if (____items1_marshaled_i_KeyNative != NULL)
					{
						____items1_marshaled_i_Staging.set_key_0(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(____items1_marshaled_i_KeyNative, Il2CppComObject_il2cpp_TypeInfo_var));
					}
					else
					{
						____items1_marshaled_i_Staging.set_key_0(NULL);
					}

					if (____items1_marshaled_i_KeyNative != NULL)
					{
						(____items1_marshaled_i_KeyNative)->Release();
						____items1_marshaled_i_KeyNative = NULL;
					}

					bool ____items1_marshaled_i_ValueNative = 0;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Value_m3611222262(&____items1_marshaled_i_ValueNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items1_marshaled_i_Staging.set_value_1(____items1_marshaled_i_ValueNative);

					_____items1_marshaled_i__unmarshaled = ____items1_marshaled_i_Staging;
				}
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::GetAt(System.UInt32)
extern "C"  KeyValuePair_2_t2401056908  IVectorView_1_GetAt_m2907280382 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m2907280382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1739922170* ____ivectorView_1_t1739922170 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1739922170::IID, reinterpret_cast<void**>(&____ivectorView_1_t1739922170));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IKeyValuePair_2_t3425776526* returnValue = NULL;
	hr = ____ivectorView_1_t1739922170->IVectorView_1_GetAt_m2907280382(___index0, &returnValue);
	____ivectorView_1_t1739922170->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	KeyValuePair_2_t2401056908  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	NullCheck(returnValue);

	{
		Il2CppIManagedObjectHolder* returnValue_imanagedObject = NULL;
		il2cpp_hresult_t hr = (returnValue)->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&returnValue_imanagedObject));
		if (IL2CPP_HR_SUCCEEDED(hr))
		{
			_returnValue_unmarshaled = *static_cast<KeyValuePair_2_t2401056908 *>(UnBox(returnValue_imanagedObject->GetManagedObject(), KeyValuePair_2_t2401056908_il2cpp_TypeInfo_var));
			returnValue_imanagedObject->Release();
		}
		else
		{
			KeyValuePair_2_t2401056908  returnValueStaging;
			Il2CppIInspectable* returnValueKeyNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Key_m1066766749(&returnValueKeyNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (returnValueKeyNative != NULL)
			{
				returnValueStaging.set_key_0(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValueKeyNative, Il2CppComObject_il2cpp_TypeInfo_var));
			}
			else
			{
				returnValueStaging.set_key_0(NULL);
			}

			if (returnValueKeyNative != NULL)
			{
				(returnValueKeyNative)->Release();
				returnValueKeyNative = NULL;
			}

			int32_t returnValueValueNative = 0;
			hr = (returnValue)->IKeyValuePair_2_get_Value_m269040038(&returnValueValueNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_value_1(returnValueValueNative);

			_returnValue_unmarshaled = returnValueStaging;
		}
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m3227332446 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t1739922170* ____ivectorView_1_t1739922170 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1739922170::IID, reinterpret_cast<void**>(&____ivectorView_1_t1739922170));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1739922170->IVectorView_1_get_Size_m3227332446(&returnValue);
	____ivectorView_1_t1739922170->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m1224710320 (RuntimeObject* __this, KeyValuePair_2_t2401056908  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_IndexOf_m1224710320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1739922170* ____ivectorView_1_t1739922170 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1739922170::IID, reinterpret_cast<void**>(&____ivectorView_1_t1739922170));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IKeyValuePair_2_t3425776526* ____value0_marshaled = NULL;
	____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IKeyValuePair_2_t3425776526>(Box(KeyValuePair_2_t2401056908_il2cpp_TypeInfo_var, &___value0));

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t1739922170->IVectorView_1_IndexOf_m1224710320(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t1739922170->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m1250367745 (RuntimeObject* __this, uint32_t ___startIndex0, KeyValuePair_2U5BU5D_t1954543557* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m1250367745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1739922170* ____ivectorView_1_t1739922170 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1739922170::IID, reinterpret_cast<void**>(&____ivectorView_1_t1739922170));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IKeyValuePair_2_t3425776526** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IKeyValuePair_2_t3425776526*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IKeyValuePair_2_t3425776526*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1739922170->IVectorView_1_GetMany_m1250367745(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t1739922170->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			KeyValuePair_2_t2401056908  _____items1_marshaled_i__unmarshaled;
			memset(&_____items1_marshaled_i__unmarshaled, 0, sizeof(_____items1_marshaled_i__unmarshaled));
			NullCheck((____items1_marshaled)[i]);

			{
				Il2CppIManagedObjectHolder* ____items1_marshaled_i__imanagedObject = NULL;
				il2cpp_hresult_t hr = ((____items1_marshaled)[i])->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&____items1_marshaled_i__imanagedObject));
				if (IL2CPP_HR_SUCCEEDED(hr))
				{
					_____items1_marshaled_i__unmarshaled = *static_cast<KeyValuePair_2_t2401056908 *>(UnBox(____items1_marshaled_i__imanagedObject->GetManagedObject(), KeyValuePair_2_t2401056908_il2cpp_TypeInfo_var));
					____items1_marshaled_i__imanagedObject->Release();
				}
				else
				{
					KeyValuePair_2_t2401056908  ____items1_marshaled_i_Staging;
					Il2CppIInspectable* ____items1_marshaled_i_KeyNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Key_m1066766749(&____items1_marshaled_i_KeyNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					if (____items1_marshaled_i_KeyNative != NULL)
					{
						____items1_marshaled_i_Staging.set_key_0(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(____items1_marshaled_i_KeyNative, Il2CppComObject_il2cpp_TypeInfo_var));
					}
					else
					{
						____items1_marshaled_i_Staging.set_key_0(NULL);
					}

					if (____items1_marshaled_i_KeyNative != NULL)
					{
						(____items1_marshaled_i_KeyNative)->Release();
						____items1_marshaled_i_KeyNative = NULL;
					}

					int32_t ____items1_marshaled_i_ValueNative = 0;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Value_m269040038(&____items1_marshaled_i_ValueNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items1_marshaled_i_Staging.set_value_1(____items1_marshaled_i_ValueNative);

					_____items1_marshaled_i__unmarshaled = ____items1_marshaled_i_Staging;
				}
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetAt(System.UInt32)
extern "C"  KeyValuePair_2_t2530217319  IVectorView_1_GetAt_m4135153857 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m4135153857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1869082581* ____ivectorView_1_t1869082581 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1869082581::IID, reinterpret_cast<void**>(&____ivectorView_1_t1869082581));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IKeyValuePair_2_t3554936937* returnValue = NULL;
	hr = ____ivectorView_1_t1869082581->IVectorView_1_GetAt_m4135153857(___index0, &returnValue);
	____ivectorView_1_t1869082581->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	KeyValuePair_2_t2530217319  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	NullCheck(returnValue);

	{
		Il2CppIManagedObjectHolder* returnValue_imanagedObject = NULL;
		il2cpp_hresult_t hr = (returnValue)->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&returnValue_imanagedObject));
		if (IL2CPP_HR_SUCCEEDED(hr))
		{
			_returnValue_unmarshaled = *static_cast<KeyValuePair_2_t2530217319 *>(UnBox(returnValue_imanagedObject->GetManagedObject(), KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var));
			returnValue_imanagedObject->Release();
		}
		else
		{
			KeyValuePair_2_t2530217319  returnValueStaging;
			Il2CppIInspectable* returnValueKeyNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Key_m2750152149(&returnValueKeyNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (returnValueKeyNative != NULL)
			{
				returnValueStaging.set_key_0(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValueKeyNative, Il2CppComObject_il2cpp_TypeInfo_var));
			}
			else
			{
				returnValueStaging.set_key_0(NULL);
			}

			if (returnValueKeyNative != NULL)
			{
				(returnValueKeyNative)->Release();
				returnValueKeyNative = NULL;
			}

			Il2CppIInspectable* returnValueValueNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Value_m2672088708(&returnValueValueNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (returnValueValueNative != NULL)
			{
				returnValueStaging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValueValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
			}
			else
			{
				returnValueStaging.set_value_1(NULL);
			}

			if (returnValueValueNative != NULL)
			{
				(returnValueValueNative)->Release();
				returnValueValueNative = NULL;
			}

			_returnValue_unmarshaled = returnValueStaging;
		}
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m3464555386 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t1869082581* ____ivectorView_1_t1869082581 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1869082581::IID, reinterpret_cast<void**>(&____ivectorView_1_t1869082581));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1869082581->IVectorView_1_get_Size_m3464555386(&returnValue);
	____ivectorView_1_t1869082581->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m2426904384 (RuntimeObject* __this, KeyValuePair_2_t2530217319  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_IndexOf_m2426904384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1869082581* ____ivectorView_1_t1869082581 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1869082581::IID, reinterpret_cast<void**>(&____ivectorView_1_t1869082581));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IKeyValuePair_2_t3554936937* ____value0_marshaled = NULL;
	____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IKeyValuePair_2_t3554936937>(Box(KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, &___value0));

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t1869082581->IVectorView_1_IndexOf_m2426904384(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t1869082581->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m718367012 (RuntimeObject* __this, uint32_t ___startIndex0, KeyValuePair_2U5BU5D_t118269214* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m718367012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1869082581* ____ivectorView_1_t1869082581 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1869082581::IID, reinterpret_cast<void**>(&____ivectorView_1_t1869082581));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IKeyValuePair_2_t3554936937** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IKeyValuePair_2_t3554936937*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IKeyValuePair_2_t3554936937*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1869082581->IVectorView_1_GetMany_m718367012(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t1869082581->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			KeyValuePair_2_t2530217319  _____items1_marshaled_i__unmarshaled;
			memset(&_____items1_marshaled_i__unmarshaled, 0, sizeof(_____items1_marshaled_i__unmarshaled));
			NullCheck((____items1_marshaled)[i]);

			{
				Il2CppIManagedObjectHolder* ____items1_marshaled_i__imanagedObject = NULL;
				il2cpp_hresult_t hr = ((____items1_marshaled)[i])->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&____items1_marshaled_i__imanagedObject));
				if (IL2CPP_HR_SUCCEEDED(hr))
				{
					_____items1_marshaled_i__unmarshaled = *static_cast<KeyValuePair_2_t2530217319 *>(UnBox(____items1_marshaled_i__imanagedObject->GetManagedObject(), KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var));
					____items1_marshaled_i__imanagedObject->Release();
				}
				else
				{
					KeyValuePair_2_t2530217319  ____items1_marshaled_i_Staging;
					Il2CppIInspectable* ____items1_marshaled_i_KeyNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Key_m2750152149(&____items1_marshaled_i_KeyNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					if (____items1_marshaled_i_KeyNative != NULL)
					{
						____items1_marshaled_i_Staging.set_key_0(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(____items1_marshaled_i_KeyNative, Il2CppComObject_il2cpp_TypeInfo_var));
					}
					else
					{
						____items1_marshaled_i_Staging.set_key_0(NULL);
					}

					if (____items1_marshaled_i_KeyNative != NULL)
					{
						(____items1_marshaled_i_KeyNative)->Release();
						____items1_marshaled_i_KeyNative = NULL;
					}

					Il2CppIInspectable* ____items1_marshaled_i_ValueNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Value_m2672088708(&____items1_marshaled_i_ValueNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					if (____items1_marshaled_i_ValueNative != NULL)
					{
						____items1_marshaled_i_Staging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(____items1_marshaled_i_ValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
					}
					else
					{
						____items1_marshaled_i_Staging.set_value_1(NULL);
					}

					if (____items1_marshaled_i_ValueNative != NULL)
					{
						(____items1_marshaled_i_ValueNative)->Release();
						____items1_marshaled_i_ValueNative = NULL;
					}

					_____items1_marshaled_i__unmarshaled = ____items1_marshaled_i_Staging;
				}
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.String>>::GetAt(System.UInt32)
extern "C"  KeyValuePair_2_t1297561844  IVectorView_1_GetAt_m3427646343 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m3427646343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t636427106* ____ivectorView_1_t636427106 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t636427106::IID, reinterpret_cast<void**>(&____ivectorView_1_t636427106));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IKeyValuePair_2_t2322281462* returnValue = NULL;
	hr = ____ivectorView_1_t636427106->IVectorView_1_GetAt_m3427646343(___index0, &returnValue);
	____ivectorView_1_t636427106->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	KeyValuePair_2_t1297561844  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	NullCheck(returnValue);

	{
		Il2CppIManagedObjectHolder* returnValue_imanagedObject = NULL;
		il2cpp_hresult_t hr = (returnValue)->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&returnValue_imanagedObject));
		if (IL2CPP_HR_SUCCEEDED(hr))
		{
			_returnValue_unmarshaled = *static_cast<KeyValuePair_2_t1297561844 *>(UnBox(returnValue_imanagedObject->GetManagedObject(), KeyValuePair_2_t1297561844_il2cpp_TypeInfo_var));
			returnValue_imanagedObject->Release();
		}
		else
		{
			KeyValuePair_2_t1297561844  returnValueStaging;
			Il2CppIInspectable* returnValueKeyNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Key_m2722966289(&returnValueKeyNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (returnValueKeyNative != NULL)
			{
				returnValueStaging.set_key_0(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValueKeyNative, Il2CppComObject_il2cpp_TypeInfo_var));
			}
			else
			{
				returnValueStaging.set_key_0(NULL);
			}

			if (returnValueKeyNative != NULL)
			{
				(returnValueKeyNative)->Release();
				returnValueKeyNative = NULL;
			}

			Il2CppHString returnValueValueNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Value_m3432963267(&returnValueValueNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_value_1(il2cpp_codegen_marshal_hstring_result(returnValueValueNative));

			il2cpp_codegen_marshal_free_hstring(returnValueValueNative);
			returnValueValueNative = NULL;

			_returnValue_unmarshaled = returnValueStaging;
		}
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.String>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m2827533443 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t636427106* ____ivectorView_1_t636427106 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t636427106::IID, reinterpret_cast<void**>(&____ivectorView_1_t636427106));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t636427106->IVectorView_1_get_Size_m2827533443(&returnValue);
	____ivectorView_1_t636427106->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.String>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m3115195681 (RuntimeObject* __this, KeyValuePair_2_t1297561844  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_IndexOf_m3115195681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t636427106* ____ivectorView_1_t636427106 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t636427106::IID, reinterpret_cast<void**>(&____ivectorView_1_t636427106));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IKeyValuePair_2_t2322281462* ____value0_marshaled = NULL;
	____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IKeyValuePair_2_t2322281462>(Box(KeyValuePair_2_t1297561844_il2cpp_TypeInfo_var, &___value0));

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t636427106->IVectorView_1_IndexOf_m3115195681(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t636427106->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.String>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m3208394414 (RuntimeObject* __this, uint32_t ___startIndex0, KeyValuePair_2U5BU5D_t2851086525* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m3208394414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t636427106* ____ivectorView_1_t636427106 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t636427106::IID, reinterpret_cast<void**>(&____ivectorView_1_t636427106));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IKeyValuePair_2_t2322281462** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IKeyValuePair_2_t2322281462*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IKeyValuePair_2_t2322281462*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t636427106->IVectorView_1_GetMany_m3208394414(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t636427106->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			KeyValuePair_2_t1297561844  _____items1_marshaled_i__unmarshaled;
			memset(&_____items1_marshaled_i__unmarshaled, 0, sizeof(_____items1_marshaled_i__unmarshaled));
			NullCheck((____items1_marshaled)[i]);

			{
				Il2CppIManagedObjectHolder* ____items1_marshaled_i__imanagedObject = NULL;
				il2cpp_hresult_t hr = ((____items1_marshaled)[i])->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&____items1_marshaled_i__imanagedObject));
				if (IL2CPP_HR_SUCCEEDED(hr))
				{
					_____items1_marshaled_i__unmarshaled = *static_cast<KeyValuePair_2_t1297561844 *>(UnBox(____items1_marshaled_i__imanagedObject->GetManagedObject(), KeyValuePair_2_t1297561844_il2cpp_TypeInfo_var));
					____items1_marshaled_i__imanagedObject->Release();
				}
				else
				{
					KeyValuePair_2_t1297561844  ____items1_marshaled_i_Staging;
					Il2CppIInspectable* ____items1_marshaled_i_KeyNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Key_m2722966289(&____items1_marshaled_i_KeyNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					if (____items1_marshaled_i_KeyNative != NULL)
					{
						____items1_marshaled_i_Staging.set_key_0(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(____items1_marshaled_i_KeyNative, Il2CppComObject_il2cpp_TypeInfo_var));
					}
					else
					{
						____items1_marshaled_i_Staging.set_key_0(NULL);
					}

					if (____items1_marshaled_i_KeyNative != NULL)
					{
						(____items1_marshaled_i_KeyNative)->Release();
						____items1_marshaled_i_KeyNative = NULL;
					}

					Il2CppHString ____items1_marshaled_i_ValueNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Value_m3432963267(&____items1_marshaled_i_ValueNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items1_marshaled_i_Staging.set_value_1(il2cpp_codegen_marshal_hstring_result(____items1_marshaled_i_ValueNative));

					il2cpp_codegen_marshal_free_hstring(____items1_marshaled_i_ValueNative);
					____items1_marshaled_i_ValueNative = NULL;

					_____items1_marshaled_i__unmarshaled = ____items1_marshaled_i_Staging;
				}
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>>::GetAt(System.UInt32)
extern "C"  KeyValuePair_2_t2280216431  IVectorView_1_GetAt_m3200486354 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m3200486354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1619081693* ____ivectorView_1_t1619081693 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1619081693::IID, reinterpret_cast<void**>(&____ivectorView_1_t1619081693));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IKeyValuePair_2_t3304936049* returnValue = NULL;
	hr = ____ivectorView_1_t1619081693->IVectorView_1_GetAt_m3200486354(___index0, &returnValue);
	____ivectorView_1_t1619081693->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	KeyValuePair_2_t2280216431  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	NullCheck(returnValue);

	{
		Il2CppIManagedObjectHolder* returnValue_imanagedObject = NULL;
		il2cpp_hresult_t hr = (returnValue)->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&returnValue_imanagedObject));
		if (IL2CPP_HR_SUCCEEDED(hr))
		{
			_returnValue_unmarshaled = *static_cast<KeyValuePair_2_t2280216431 *>(UnBox(returnValue_imanagedObject->GetManagedObject(), KeyValuePair_2_t2280216431_il2cpp_TypeInfo_var));
			returnValue_imanagedObject->Release();
		}
		else
		{
			KeyValuePair_2_t2280216431  returnValueStaging;
			Il2CppHString returnValueKeyNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Key_m2534079307(&returnValueKeyNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_key_0(il2cpp_codegen_marshal_hstring_result(returnValueKeyNative));

			il2cpp_codegen_marshal_free_hstring(returnValueKeyNative);
			returnValueKeyNative = NULL;

			bool returnValueValueNative = 0;
			hr = (returnValue)->IKeyValuePair_2_get_Value_m2353902038(&returnValueValueNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_value_1(returnValueValueNative);

			_returnValue_unmarshaled = returnValueStaging;
		}
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m1168021312 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t1619081693* ____ivectorView_1_t1619081693 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1619081693::IID, reinterpret_cast<void**>(&____ivectorView_1_t1619081693));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1619081693->IVectorView_1_get_Size_m1168021312(&returnValue);
	____ivectorView_1_t1619081693->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m1159407127 (RuntimeObject* __this, KeyValuePair_2_t2280216431  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_IndexOf_m1159407127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1619081693* ____ivectorView_1_t1619081693 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1619081693::IID, reinterpret_cast<void**>(&____ivectorView_1_t1619081693));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IKeyValuePair_2_t3304936049* ____value0_marshaled = NULL;
	____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IKeyValuePair_2_t3304936049>(Box(KeyValuePair_2_t2280216431_il2cpp_TypeInfo_var, &___value0));

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t1619081693->IVectorView_1_IndexOf_m1159407127(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t1619081693->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m2903092860 (RuntimeObject* __this, uint32_t ___startIndex0, KeyValuePair_2U5BU5D_t1951926774* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m2903092860_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1619081693* ____ivectorView_1_t1619081693 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1619081693::IID, reinterpret_cast<void**>(&____ivectorView_1_t1619081693));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IKeyValuePair_2_t3304936049** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IKeyValuePair_2_t3304936049*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IKeyValuePair_2_t3304936049*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1619081693->IVectorView_1_GetMany_m2903092860(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t1619081693->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			KeyValuePair_2_t2280216431  _____items1_marshaled_i__unmarshaled;
			memset(&_____items1_marshaled_i__unmarshaled, 0, sizeof(_____items1_marshaled_i__unmarshaled));
			NullCheck((____items1_marshaled)[i]);

			{
				Il2CppIManagedObjectHolder* ____items1_marshaled_i__imanagedObject = NULL;
				il2cpp_hresult_t hr = ((____items1_marshaled)[i])->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&____items1_marshaled_i__imanagedObject));
				if (IL2CPP_HR_SUCCEEDED(hr))
				{
					_____items1_marshaled_i__unmarshaled = *static_cast<KeyValuePair_2_t2280216431 *>(UnBox(____items1_marshaled_i__imanagedObject->GetManagedObject(), KeyValuePair_2_t2280216431_il2cpp_TypeInfo_var));
					____items1_marshaled_i__imanagedObject->Release();
				}
				else
				{
					KeyValuePair_2_t2280216431  ____items1_marshaled_i_Staging;
					Il2CppHString ____items1_marshaled_i_KeyNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Key_m2534079307(&____items1_marshaled_i_KeyNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items1_marshaled_i_Staging.set_key_0(il2cpp_codegen_marshal_hstring_result(____items1_marshaled_i_KeyNative));

					il2cpp_codegen_marshal_free_hstring(____items1_marshaled_i_KeyNative);
					____items1_marshaled_i_KeyNative = NULL;

					bool ____items1_marshaled_i_ValueNative = 0;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Value_m2353902038(&____items1_marshaled_i_ValueNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items1_marshaled_i_Staging.set_value_1(____items1_marshaled_i_ValueNative);

					_____items1_marshaled_i__unmarshaled = ____items1_marshaled_i_Staging;
				}
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::GetAt(System.UInt32)
extern "C"  KeyValuePair_2_t838906923  IVectorView_1_GetAt_m1543733215 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m1543733215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t177772185* ____ivectorView_1_t177772185 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t177772185::IID, reinterpret_cast<void**>(&____ivectorView_1_t177772185));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IKeyValuePair_2_t1863626541* returnValue = NULL;
	hr = ____ivectorView_1_t177772185->IVectorView_1_GetAt_m1543733215(___index0, &returnValue);
	____ivectorView_1_t177772185->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	KeyValuePair_2_t838906923  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	NullCheck(returnValue);

	{
		Il2CppIManagedObjectHolder* returnValue_imanagedObject = NULL;
		il2cpp_hresult_t hr = (returnValue)->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&returnValue_imanagedObject));
		if (IL2CPP_HR_SUCCEEDED(hr))
		{
			_returnValue_unmarshaled = *static_cast<KeyValuePair_2_t838906923 *>(UnBox(returnValue_imanagedObject->GetManagedObject(), KeyValuePair_2_t838906923_il2cpp_TypeInfo_var));
			returnValue_imanagedObject->Release();
		}
		else
		{
			KeyValuePair_2_t838906923  returnValueStaging;
			Il2CppHString returnValueKeyNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Key_m599934735(&returnValueKeyNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_key_0(il2cpp_codegen_marshal_hstring_result(returnValueKeyNative));

			il2cpp_codegen_marshal_free_hstring(returnValueKeyNative);
			returnValueKeyNative = NULL;

			int32_t returnValueValueNative = 0;
			hr = (returnValue)->IKeyValuePair_2_get_Value_m2337749369(&returnValueValueNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_value_1(returnValueValueNative);

			_returnValue_unmarshaled = returnValueStaging;
		}
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m2909509684 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t177772185* ____ivectorView_1_t177772185 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t177772185::IID, reinterpret_cast<void**>(&____ivectorView_1_t177772185));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t177772185->IVectorView_1_get_Size_m2909509684(&returnValue);
	____ivectorView_1_t177772185->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m766512542 (RuntimeObject* __this, KeyValuePair_2_t838906923  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_IndexOf_m766512542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t177772185* ____ivectorView_1_t177772185 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t177772185::IID, reinterpret_cast<void**>(&____ivectorView_1_t177772185));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IKeyValuePair_2_t1863626541* ____value0_marshaled = NULL;
	____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IKeyValuePair_2_t1863626541>(Box(KeyValuePair_2_t838906923_il2cpp_TypeInfo_var, &___value0));

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t177772185->IVectorView_1_IndexOf_m766512542(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t177772185->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m2121778822 (RuntimeObject* __this, uint32_t ___startIndex0, KeyValuePair_2U5BU5D_t3734722250* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m2121778822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t177772185* ____ivectorView_1_t177772185 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t177772185::IID, reinterpret_cast<void**>(&____ivectorView_1_t177772185));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IKeyValuePair_2_t1863626541** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IKeyValuePair_2_t1863626541*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IKeyValuePair_2_t1863626541*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t177772185->IVectorView_1_GetMany_m2121778822(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t177772185->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			KeyValuePair_2_t838906923  _____items1_marshaled_i__unmarshaled;
			memset(&_____items1_marshaled_i__unmarshaled, 0, sizeof(_____items1_marshaled_i__unmarshaled));
			NullCheck((____items1_marshaled)[i]);

			{
				Il2CppIManagedObjectHolder* ____items1_marshaled_i__imanagedObject = NULL;
				il2cpp_hresult_t hr = ((____items1_marshaled)[i])->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&____items1_marshaled_i__imanagedObject));
				if (IL2CPP_HR_SUCCEEDED(hr))
				{
					_____items1_marshaled_i__unmarshaled = *static_cast<KeyValuePair_2_t838906923 *>(UnBox(____items1_marshaled_i__imanagedObject->GetManagedObject(), KeyValuePair_2_t838906923_il2cpp_TypeInfo_var));
					____items1_marshaled_i__imanagedObject->Release();
				}
				else
				{
					KeyValuePair_2_t838906923  ____items1_marshaled_i_Staging;
					Il2CppHString ____items1_marshaled_i_KeyNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Key_m599934735(&____items1_marshaled_i_KeyNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items1_marshaled_i_Staging.set_key_0(il2cpp_codegen_marshal_hstring_result(____items1_marshaled_i_KeyNative));

					il2cpp_codegen_marshal_free_hstring(____items1_marshaled_i_KeyNative);
					____items1_marshaled_i_KeyNative = NULL;

					int32_t ____items1_marshaled_i_ValueNative = 0;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Value_m2337749369(&____items1_marshaled_i_ValueNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items1_marshaled_i_Staging.set_value_1(____items1_marshaled_i_ValueNative);

					_____items1_marshaled_i__unmarshaled = ____items1_marshaled_i_Staging;
				}
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::GetAt(System.UInt32)
extern "C"  KeyValuePair_2_t968067334  IVectorView_1_GetAt_m4155768441 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m4155768441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t306932596* ____ivectorView_1_t306932596 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t306932596::IID, reinterpret_cast<void**>(&____ivectorView_1_t306932596));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IKeyValuePair_2_t1992786952* returnValue = NULL;
	hr = ____ivectorView_1_t306932596->IVectorView_1_GetAt_m4155768441(___index0, &returnValue);
	____ivectorView_1_t306932596->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	KeyValuePair_2_t968067334  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	NullCheck(returnValue);

	{
		Il2CppIManagedObjectHolder* returnValue_imanagedObject = NULL;
		il2cpp_hresult_t hr = (returnValue)->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&returnValue_imanagedObject));
		if (IL2CPP_HR_SUCCEEDED(hr))
		{
			_returnValue_unmarshaled = *static_cast<KeyValuePair_2_t968067334 *>(UnBox(returnValue_imanagedObject->GetManagedObject(), KeyValuePair_2_t968067334_il2cpp_TypeInfo_var));
			returnValue_imanagedObject->Release();
		}
		else
		{
			KeyValuePair_2_t968067334  returnValueStaging;
			Il2CppHString returnValueKeyNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Key_m3523589551(&returnValueKeyNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_key_0(il2cpp_codegen_marshal_hstring_result(returnValueKeyNative));

			il2cpp_codegen_marshal_free_hstring(returnValueKeyNative);
			returnValueKeyNative = NULL;

			Il2CppIInspectable* returnValueValueNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Value_m3279123715(&returnValueValueNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (returnValueValueNative != NULL)
			{
				returnValueStaging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValueValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
			}
			else
			{
				returnValueStaging.set_value_1(NULL);
			}

			if (returnValueValueNative != NULL)
			{
				(returnValueValueNative)->Release();
				returnValueValueNative = NULL;
			}

			_returnValue_unmarshaled = returnValueStaging;
		}
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m578036601 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t306932596* ____ivectorView_1_t306932596 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t306932596::IID, reinterpret_cast<void**>(&____ivectorView_1_t306932596));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t306932596->IVectorView_1_get_Size_m578036601(&returnValue);
	____ivectorView_1_t306932596->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m61054490 (RuntimeObject* __this, KeyValuePair_2_t968067334  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_IndexOf_m61054490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t306932596* ____ivectorView_1_t306932596 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t306932596::IID, reinterpret_cast<void**>(&____ivectorView_1_t306932596));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IKeyValuePair_2_t1992786952* ____value0_marshaled = NULL;
	____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IKeyValuePair_2_t1992786952>(Box(KeyValuePair_2_t968067334_il2cpp_TypeInfo_var, &___value0));

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t306932596->IVectorView_1_IndexOf_m61054490(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t306932596->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m3141254740 (RuntimeObject* __this, uint32_t ___startIndex0, KeyValuePair_2U5BU5D_t1898447907* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m3141254740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t306932596* ____ivectorView_1_t306932596 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t306932596::IID, reinterpret_cast<void**>(&____ivectorView_1_t306932596));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IKeyValuePair_2_t1992786952** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IKeyValuePair_2_t1992786952*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IKeyValuePair_2_t1992786952*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t306932596->IVectorView_1_GetMany_m3141254740(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t306932596->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			KeyValuePair_2_t968067334  _____items1_marshaled_i__unmarshaled;
			memset(&_____items1_marshaled_i__unmarshaled, 0, sizeof(_____items1_marshaled_i__unmarshaled));
			NullCheck((____items1_marshaled)[i]);

			{
				Il2CppIManagedObjectHolder* ____items1_marshaled_i__imanagedObject = NULL;
				il2cpp_hresult_t hr = ((____items1_marshaled)[i])->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&____items1_marshaled_i__imanagedObject));
				if (IL2CPP_HR_SUCCEEDED(hr))
				{
					_____items1_marshaled_i__unmarshaled = *static_cast<KeyValuePair_2_t968067334 *>(UnBox(____items1_marshaled_i__imanagedObject->GetManagedObject(), KeyValuePair_2_t968067334_il2cpp_TypeInfo_var));
					____items1_marshaled_i__imanagedObject->Release();
				}
				else
				{
					KeyValuePair_2_t968067334  ____items1_marshaled_i_Staging;
					Il2CppHString ____items1_marshaled_i_KeyNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Key_m3523589551(&____items1_marshaled_i_KeyNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items1_marshaled_i_Staging.set_key_0(il2cpp_codegen_marshal_hstring_result(____items1_marshaled_i_KeyNative));

					il2cpp_codegen_marshal_free_hstring(____items1_marshaled_i_KeyNative);
					____items1_marshaled_i_KeyNative = NULL;

					Il2CppIInspectable* ____items1_marshaled_i_ValueNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Value_m3279123715(&____items1_marshaled_i_ValueNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					if (____items1_marshaled_i_ValueNative != NULL)
					{
						____items1_marshaled_i_Staging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(____items1_marshaled_i_ValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
					}
					else
					{
						____items1_marshaled_i_Staging.set_value_1(NULL);
					}

					if (____items1_marshaled_i_ValueNative != NULL)
					{
						(____items1_marshaled_i_ValueNative)->Release();
						____items1_marshaled_i_ValueNative = NULL;
					}

					_____items1_marshaled_i__unmarshaled = ____items1_marshaled_i_Staging;
				}
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::GetAt(System.UInt32)
extern "C"  KeyValuePair_2_t4030379155  IVectorView_1_GetAt_m3371945293 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m3371945293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3369244417* ____ivectorView_1_t3369244417 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3369244417::IID, reinterpret_cast<void**>(&____ivectorView_1_t3369244417));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IKeyValuePair_2_t760131477* returnValue = NULL;
	hr = ____ivectorView_1_t3369244417->IVectorView_1_GetAt_m3371945293(___index0, &returnValue);
	____ivectorView_1_t3369244417->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	KeyValuePair_2_t4030379155  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	NullCheck(returnValue);

	{
		Il2CppIManagedObjectHolder* returnValue_imanagedObject = NULL;
		il2cpp_hresult_t hr = (returnValue)->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&returnValue_imanagedObject));
		if (IL2CPP_HR_SUCCEEDED(hr))
		{
			_returnValue_unmarshaled = *static_cast<KeyValuePair_2_t4030379155 *>(UnBox(returnValue_imanagedObject->GetManagedObject(), KeyValuePair_2_t4030379155_il2cpp_TypeInfo_var));
			returnValue_imanagedObject->Release();
		}
		else
		{
			KeyValuePair_2_t4030379155  returnValueStaging;
			Il2CppHString returnValueKeyNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Key_m3013852749(&returnValueKeyNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_key_0(il2cpp_codegen_marshal_hstring_result(returnValueKeyNative));

			il2cpp_codegen_marshal_free_hstring(returnValueKeyNative);
			returnValueKeyNative = NULL;

			Il2CppHString returnValueValueNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Value_m2003745203(&returnValueValueNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_value_1(il2cpp_codegen_marshal_hstring_result(returnValueValueNative));

			il2cpp_codegen_marshal_free_hstring(returnValueValueNative);
			returnValueValueNative = NULL;

			_returnValue_unmarshaled = returnValueStaging;
		}
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m280905071 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t3369244417* ____ivectorView_1_t3369244417 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3369244417::IID, reinterpret_cast<void**>(&____ivectorView_1_t3369244417));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3369244417->IVectorView_1_get_Size_m280905071(&returnValue);
	____ivectorView_1_t3369244417->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m653354477 (RuntimeObject* __this, KeyValuePair_2_t4030379155  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_IndexOf_m653354477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3369244417* ____ivectorView_1_t3369244417 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3369244417::IID, reinterpret_cast<void**>(&____ivectorView_1_t3369244417));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IKeyValuePair_2_t760131477* ____value0_marshaled = NULL;
	____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IKeyValuePair_2_t760131477>(Box(KeyValuePair_2_t4030379155_il2cpp_TypeInfo_var, &___value0));

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t3369244417->IVectorView_1_IndexOf_m653354477(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t3369244417->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m1052653815 (RuntimeObject* __this, uint32_t ___startIndex0, KeyValuePair_2U5BU5D_t336297922* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m1052653815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t3369244417* ____ivectorView_1_t3369244417 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3369244417::IID, reinterpret_cast<void**>(&____ivectorView_1_t3369244417));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IKeyValuePair_2_t760131477** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IKeyValuePair_2_t760131477*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IKeyValuePair_2_t760131477*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3369244417->IVectorView_1_GetMany_m1052653815(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t3369244417->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			KeyValuePair_2_t4030379155  _____items1_marshaled_i__unmarshaled;
			memset(&_____items1_marshaled_i__unmarshaled, 0, sizeof(_____items1_marshaled_i__unmarshaled));
			NullCheck((____items1_marshaled)[i]);

			{
				Il2CppIManagedObjectHolder* ____items1_marshaled_i__imanagedObject = NULL;
				il2cpp_hresult_t hr = ((____items1_marshaled)[i])->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&____items1_marshaled_i__imanagedObject));
				if (IL2CPP_HR_SUCCEEDED(hr))
				{
					_____items1_marshaled_i__unmarshaled = *static_cast<KeyValuePair_2_t4030379155 *>(UnBox(____items1_marshaled_i__imanagedObject->GetManagedObject(), KeyValuePair_2_t4030379155_il2cpp_TypeInfo_var));
					____items1_marshaled_i__imanagedObject->Release();
				}
				else
				{
					KeyValuePair_2_t4030379155  ____items1_marshaled_i_Staging;
					Il2CppHString ____items1_marshaled_i_KeyNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Key_m3013852749(&____items1_marshaled_i_KeyNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items1_marshaled_i_Staging.set_key_0(il2cpp_codegen_marshal_hstring_result(____items1_marshaled_i_KeyNative));

					il2cpp_codegen_marshal_free_hstring(____items1_marshaled_i_KeyNative);
					____items1_marshaled_i_KeyNative = NULL;

					Il2CppHString ____items1_marshaled_i_ValueNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Value_m2003745203(&____items1_marshaled_i_ValueNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items1_marshaled_i_Staging.set_value_1(il2cpp_codegen_marshal_hstring_result(____items1_marshaled_i_ValueNative));

					il2cpp_codegen_marshal_free_hstring(____items1_marshaled_i_ValueNative);
					____items1_marshaled_i_ValueNative = NULL;

					_____items1_marshaled_i__unmarshaled = ____items1_marshaled_i_Staging;
				}
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Type>>::GetAt(System.UInt32)
extern "C"  KeyValuePair_2_t371905930  IVectorView_1_GetAt_m4266810032 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Type>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m1164900279 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t4005738488* ____ivectorView_1_t4005738488 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t4005738488::IID, reinterpret_cast<void**>(&____ivectorView_1_t4005738488));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t4005738488->IVectorView_1_get_Size_m1164900279(&returnValue);
	____ivectorView_1_t4005738488->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Type>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m3999170120 (RuntimeObject* __this, KeyValuePair_2_t371905930  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_IndexOf_m3999170120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t4005738488* ____ivectorView_1_t4005738488 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t4005738488::IID, reinterpret_cast<void**>(&____ivectorView_1_t4005738488));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IKeyValuePair_2_t1396625548* ____value0_marshaled = NULL;
	____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IKeyValuePair_2_t1396625548>(Box(KeyValuePair_2_t371905930_il2cpp_TypeInfo_var, &___value0));

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t4005738488->IVectorView_1_IndexOf_m3999170120(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t4005738488->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Type>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m1120548418 (RuntimeObject* __this, uint32_t ___startIndex0, KeyValuePair_2U5BU5D_t2995388687* ___items1, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>>::GetAt(System.UInt32)
extern "C"  KeyValuePair_2_t3030996695  IVectorView_1_GetAt_m690924722 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m483378542 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t2369861957* ____ivectorView_1_t2369861957 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2369861957::IID, reinterpret_cast<void**>(&____ivectorView_1_t2369861957));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2369861957->IVectorView_1_get_Size_m483378542(&returnValue);
	____ivectorView_1_t2369861957->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m2218762648 (RuntimeObject* __this, KeyValuePair_2_t3030996695  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_IndexOf_m2218762648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2369861957* ____ivectorView_1_t2369861957 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2369861957::IID, reinterpret_cast<void**>(&____ivectorView_1_t2369861957));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IKeyValuePair_2_t4055716313* ____value0_marshaled = NULL;
	____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IKeyValuePair_2_t4055716313>(Box(KeyValuePair_2_t3030996695_il2cpp_TypeInfo_var, &___value0));

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t2369861957->IVectorView_1_IndexOf_m2218762648(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t2369861957->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m3562856357 (RuntimeObject* __this, uint32_t ___startIndex0, KeyValuePair_2U5BU5D_t130504686* ___items1, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::GetAt(System.UInt32)
extern "C"  KeyValuePair_2_t2457078697  IVectorView_1_GetAt_m3476973415 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m3476973415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1795943959* ____ivectorView_1_t1795943959 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1795943959::IID, reinterpret_cast<void**>(&____ivectorView_1_t1795943959));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IKeyValuePair_2_t3481798315* returnValue = NULL;
	hr = ____ivectorView_1_t1795943959->IVectorView_1_GetAt_m3476973415(___index0, &returnValue);
	____ivectorView_1_t1795943959->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	KeyValuePair_2_t2457078697  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	NullCheck(returnValue);

	{
		Il2CppIManagedObjectHolder* returnValue_imanagedObject = NULL;
		il2cpp_hresult_t hr = (returnValue)->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&returnValue_imanagedObject));
		if (IL2CPP_HR_SUCCEEDED(hr))
		{
			_returnValue_unmarshaled = *static_cast<KeyValuePair_2_t2457078697 *>(UnBox(returnValue_imanagedObject->GetManagedObject(), KeyValuePair_2_t2457078697_il2cpp_TypeInfo_var));
			returnValue_imanagedObject->Release();
		}
		else
		{
			KeyValuePair_2_t2457078697  returnValueStaging;
			uint32_t returnValueKeyNative = 0;
			hr = (returnValue)->IKeyValuePair_2_get_Key_m2175672871(&returnValueKeyNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_key_0(returnValueKeyNative);

			Il2CppIInspectable* returnValueValueNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Value_m3078130594(&returnValueValueNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (returnValueValueNative != NULL)
			{
				returnValueStaging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValueValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
			}
			else
			{
				returnValueStaging.set_value_1(NULL);
			}

			if (returnValueValueNative != NULL)
			{
				(returnValueValueNative)->Release();
				returnValueValueNative = NULL;
			}

			_returnValue_unmarshaled = returnValueStaging;
		}
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m1565795300 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t1795943959* ____ivectorView_1_t1795943959 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1795943959::IID, reinterpret_cast<void**>(&____ivectorView_1_t1795943959));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1795943959->IVectorView_1_get_Size_m1565795300(&returnValue);
	____ivectorView_1_t1795943959->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m836020600 (RuntimeObject* __this, KeyValuePair_2_t2457078697  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_IndexOf_m836020600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1795943959* ____ivectorView_1_t1795943959 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1795943959::IID, reinterpret_cast<void**>(&____ivectorView_1_t1795943959));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IKeyValuePair_2_t3481798315* ____value0_marshaled = NULL;
	____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IKeyValuePair_2_t3481798315>(Box(KeyValuePair_2_t2457078697_il2cpp_TypeInfo_var, &___value0));

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t1795943959->IVectorView_1_IndexOf_m836020600(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t1795943959->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m2658557676 (RuntimeObject* __this, uint32_t ___startIndex0, KeyValuePair_2U5BU5D_t2544954260* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m2658557676_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1795943959* ____ivectorView_1_t1795943959 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1795943959::IID, reinterpret_cast<void**>(&____ivectorView_1_t1795943959));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IKeyValuePair_2_t3481798315** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IKeyValuePair_2_t3481798315*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IKeyValuePair_2_t3481798315*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1795943959->IVectorView_1_GetMany_m2658557676(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t1795943959->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			KeyValuePair_2_t2457078697  _____items1_marshaled_i__unmarshaled;
			memset(&_____items1_marshaled_i__unmarshaled, 0, sizeof(_____items1_marshaled_i__unmarshaled));
			NullCheck((____items1_marshaled)[i]);

			{
				Il2CppIManagedObjectHolder* ____items1_marshaled_i__imanagedObject = NULL;
				il2cpp_hresult_t hr = ((____items1_marshaled)[i])->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&____items1_marshaled_i__imanagedObject));
				if (IL2CPP_HR_SUCCEEDED(hr))
				{
					_____items1_marshaled_i__unmarshaled = *static_cast<KeyValuePair_2_t2457078697 *>(UnBox(____items1_marshaled_i__imanagedObject->GetManagedObject(), KeyValuePair_2_t2457078697_il2cpp_TypeInfo_var));
					____items1_marshaled_i__imanagedObject->Release();
				}
				else
				{
					KeyValuePair_2_t2457078697  ____items1_marshaled_i_Staging;
					uint32_t ____items1_marshaled_i_KeyNative = 0;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Key_m2175672871(&____items1_marshaled_i_KeyNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items1_marshaled_i_Staging.set_key_0(____items1_marshaled_i_KeyNative);

					Il2CppIInspectable* ____items1_marshaled_i_ValueNative = NULL;
					hr = ((____items1_marshaled)[i])->IKeyValuePair_2_get_Value_m3078130594(&____items1_marshaled_i_ValueNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					if (____items1_marshaled_i_ValueNative != NULL)
					{
						____items1_marshaled_i_Staging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(____items1_marshaled_i_ValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
					}
					else
					{
						____items1_marshaled_i_Staging.set_value_1(NULL);
					}

					if (____items1_marshaled_i_ValueNative != NULL)
					{
						(____items1_marshaled_i_ValueNative)->Release();
						____items1_marshaled_i_ValueNative = NULL;
					}

					_____items1_marshaled_i__unmarshaled = ____items1_marshaled_i_Staging;
				}
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.IEnumerable>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m3012281261 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m3012281261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1280033273* ____ivectorView_1_t1280033273 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1280033273::IID, reinterpret_cast<void**>(&____ivectorView_1_t1280033273));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IBindableIterable_t2147255965* returnValue = NULL;
	hr = ____ivectorView_1_t1280033273->IVectorView_1_GetAt_m3012281261(___index0, &returnValue);
	____ivectorView_1_t1280033273->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.IEnumerable>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m514591163 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t1280033273* ____ivectorView_1_t1280033273 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1280033273::IID, reinterpret_cast<void**>(&____ivectorView_1_t1280033273));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1280033273->IVectorView_1_get_Size_m514591163(&returnValue);
	____ivectorView_1_t1280033273->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.IEnumerable>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m189209080 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t1280033273* ____ivectorView_1_t1280033273 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1280033273::IID, reinterpret_cast<void**>(&____ivectorView_1_t1280033273));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IBindableIterable_t2147255965* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IBindableIterable_t2147255965::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IBindableIterable_t2147255965>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t1280033273->IVectorView_1_IndexOf_m189209080(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t1280033273->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.IEnumerable>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m1961556821 (RuntimeObject* __this, uint32_t ___startIndex0, IEnumerableU5BU5D_t1343500778* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m1961556821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1280033273* ____ivectorView_1_t1280033273 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1280033273::IID, reinterpret_cast<void**>(&____ivectorView_1_t1280033273));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IBindableIterable_t2147255965** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IBindableIterable_t2147255965*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IBindableIterable_t2147255965*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1280033273->IVectorView_1_GetMany_m1961556821(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t1280033273->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.IList>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m380625345 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m380625345_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1433796478* ____ivectorView_1_t1433796478 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1433796478::IID, reinterpret_cast<void**>(&____ivectorView_1_t1433796478));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IBindableVector_t1801110279* returnValue = NULL;
	hr = ____ivectorView_1_t1433796478->IVectorView_1_GetAt_m380625345(___index0, &returnValue);
	____ivectorView_1_t1433796478->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.IList>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m3533088173 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t1433796478* ____ivectorView_1_t1433796478 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1433796478::IID, reinterpret_cast<void**>(&____ivectorView_1_t1433796478));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1433796478->IVectorView_1_get_Size_m3533088173(&returnValue);
	____ivectorView_1_t1433796478->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.IList>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m180434441 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t1433796478* ____ivectorView_1_t1433796478 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1433796478::IID, reinterpret_cast<void**>(&____ivectorView_1_t1433796478));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IBindableVector_t1801110279* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IBindableVector_t1801110279::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IBindableVector_t1801110279>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t1433796478->IVectorView_1_IndexOf_m180434441(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t1433796478->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.IList>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m2636815833 (RuntimeObject* __this, uint32_t ___startIndex0, IListU5BU5D_t2181495985* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m2636815833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t1433796478* ____ivectorView_1_t1433796478 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1433796478::IID, reinterpret_cast<void**>(&____ivectorView_1_t1433796478));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IBindableVector_t1801110279** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IBindableVector_t1801110279*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IBindableVector_t1801110279*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1433796478->IVectorView_1_GetMany_m2636815833(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t1433796478->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Collections.Specialized.INotifyCollectionChanged>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m1114485513 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m1114485513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2637269019* ____ivectorView_1_t2637269019 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2637269019::IID, reinterpret_cast<void**>(&____ivectorView_1_t2637269019));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	INotifyCollectionChanged_t3244377239* returnValue = NULL;
	hr = ____ivectorView_1_t2637269019->IVectorView_1_GetAt_m1114485513(___index0, &returnValue);
	____ivectorView_1_t2637269019->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Specialized.INotifyCollectionChanged>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m1381695206 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t2637269019* ____ivectorView_1_t2637269019 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2637269019::IID, reinterpret_cast<void**>(&____ivectorView_1_t2637269019));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2637269019->IVectorView_1_get_Size_m1381695206(&returnValue);
	____ivectorView_1_t2637269019->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Collections.Specialized.INotifyCollectionChanged>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m2659620536 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t2637269019* ____ivectorView_1_t2637269019 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2637269019::IID, reinterpret_cast<void**>(&____ivectorView_1_t2637269019));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	INotifyCollectionChanged_t3244377239* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(INotifyCollectionChanged_t3244377239::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<INotifyCollectionChanged_t3244377239>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t2637269019->IVectorView_1_IndexOf_m2659620536(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t2637269019->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Collections.Specialized.INotifyCollectionChanged>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m1942078576 (RuntimeObject* __this, uint32_t ___startIndex0, INotifyCollectionChangedU5BU5D_t557462784* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m1942078576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2637269019* ____ivectorView_1_t2637269019 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2637269019::IID, reinterpret_cast<void**>(&____ivectorView_1_t2637269019));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	INotifyCollectionChanged_t3244377239** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<INotifyCollectionChanged_t3244377239*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(INotifyCollectionChanged_t3244377239*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2637269019->IVectorView_1_GetMany_m1942078576(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t2637269019->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.DateTimeOffset>::GetAt(System.UInt32)
extern "C"  DateTimeOffset_t3229287507  IVectorView_1_GetAt_m1224151036 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m1224151036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2568152769* ____ivectorView_1_t2568152769 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2568152769::IID, reinterpret_cast<void**>(&____ivectorView_1_t2568152769));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	DateTime_t1679451545  returnValue = {};
	hr = ____ivectorView_1_t2568152769->IVectorView_1_GetAt_m1224151036(___index0, &returnValue);
	____ivectorView_1_t2568152769->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	DateTimeOffset_t3229287507  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	if ((returnValue).get_UniversalTime_0() < -504911232000000000 || (returnValue).get_UniversalTime_0() > 2650467743999999999)
	{
		ArgumentOutOfRangeException_t777629997 * exception = (ArgumentOutOfRangeException_t777629997*)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m282481429(exception, _stringLiteral4286223022, _stringLiteral311452336, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(exception);
	}

	DateTimeOffset_t3229287507  _returnValue_unmarshaledStaging;
	DateTime_t3738529785  _returnValue_unmarshaledDateTime;
	_returnValue_unmarshaledDateTime.set_dateData_44((returnValue).get_UniversalTime_0() + 504911232000000000);
	_returnValue_unmarshaledStaging.set_m_dateTime_2(_returnValue_unmarshaledDateTime);
	_returnValue_unmarshaledStaging.set_m_offsetMinutes_3(0);
	_returnValue_unmarshaled = DateTimeOffset_ToLocalTime_m1141180670((&_returnValue_unmarshaledStaging), true, NULL);;

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.DateTimeOffset>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m1566737220 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t2568152769* ____ivectorView_1_t2568152769 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2568152769::IID, reinterpret_cast<void**>(&____ivectorView_1_t2568152769));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2568152769->IVectorView_1_get_Size_m1566737220(&returnValue);
	____ivectorView_1_t2568152769->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.DateTimeOffset>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m1513712387 (RuntimeObject* __this, DateTimeOffset_t3229287507  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t2568152769* ____ivectorView_1_t2568152769 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2568152769::IID, reinterpret_cast<void**>(&____ivectorView_1_t2568152769));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	DateTime_t1679451545  ____value0_marshaled = {};
	(____value0_marshaled).set_UniversalTime_0((___value0.get_m_dateTime_2().get_dateData_44() & 0x3FFFFFFFFFFFFFFF) - 504911232000000000);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t2568152769->IVectorView_1_IndexOf_m1513712387(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t2568152769->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.DateTimeOffset>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m1032517645 (RuntimeObject* __this, uint32_t ___startIndex0, DateTimeOffsetU5BU5D_t3473357058* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m1032517645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2568152769* ____ivectorView_1_t2568152769 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2568152769::IID, reinterpret_cast<void**>(&____ivectorView_1_t2568152769));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	DateTime_t1679451545 * ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<DateTime_t1679451545 >(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(DateTime_t1679451545 ));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2568152769->IVectorView_1_GetMany_m1032517645(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t2568152769->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			DateTimeOffset_t3229287507  _____items1_marshaled_i__unmarshaled;
			memset(&_____items1_marshaled_i__unmarshaled, 0, sizeof(_____items1_marshaled_i__unmarshaled));
			if (((____items1_marshaled)[i]).get_UniversalTime_0() < -504911232000000000 || ((____items1_marshaled)[i]).get_UniversalTime_0() > 2650467743999999999)
			{
				ArgumentOutOfRangeException_t777629997 * exception = (ArgumentOutOfRangeException_t777629997*)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
				ArgumentOutOfRangeException__ctor_m282481429(exception, _stringLiteral4286223022, _stringLiteral311452336, NULL);
				IL2CPP_RAISE_MANAGED_EXCEPTION(exception);
			}

			DateTimeOffset_t3229287507  _____items1_marshaled_i__unmarshaledStaging;
			DateTime_t3738529785  _____items1_marshaled_i__unmarshaledDateTime;
			_____items1_marshaled_i__unmarshaledDateTime.set_dateData_44(((____items1_marshaled)[i]).get_UniversalTime_0() + 504911232000000000);
			_____items1_marshaled_i__unmarshaledStaging.set_m_dateTime_2(_____items1_marshaled_i__unmarshaledDateTime);
			_____items1_marshaled_i__unmarshaledStaging.set_m_offsetMinutes_3(0);
			_____items1_marshaled_i__unmarshaled = DateTimeOffset_ToLocalTime_m1141180670((&_____items1_marshaled_i__unmarshaledStaging), true, NULL);;
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Double>::GetAt(System.UInt32)
extern "C"  double IVectorView_1_GetAt_m3029520189 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVectorView_1_t4228497921* ____ivectorView_1_t4228497921 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t4228497921::IID, reinterpret_cast<void**>(&____ivectorView_1_t4228497921));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	double returnValue = 0.0;
	hr = ____ivectorView_1_t4228497921->IVectorView_1_GetAt_m3029520189(___index0, &returnValue);
	____ivectorView_1_t4228497921->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Double>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m1840672366 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t4228497921* ____ivectorView_1_t4228497921 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t4228497921::IID, reinterpret_cast<void**>(&____ivectorView_1_t4228497921));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t4228497921->IVectorView_1_get_Size_m1840672366(&returnValue);
	____ivectorView_1_t4228497921->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Double>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m519924844 (RuntimeObject* __this, double ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t4228497921* ____ivectorView_1_t4228497921 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t4228497921::IID, reinterpret_cast<void**>(&____ivectorView_1_t4228497921));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t4228497921->IVectorView_1_IndexOf_m519924844(___value0, (&____index1_empty), &returnValue);
	____ivectorView_1_t4228497921->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Double>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m4072621343 (RuntimeObject* __this, uint32_t ___startIndex0, DoubleU5BU5D_t3413330114* ___items1, const RuntimeMethod* method)
{
	IVectorView_1_t4228497921* ____ivectorView_1_t4228497921 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t4228497921::IID, reinterpret_cast<void**>(&____ivectorView_1_t4228497921));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	double* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<double>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(double));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t4228497921->IVectorView_1_GetMany_m4072621343(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t4228497921->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Exception>::GetAt(System.UInt32)
extern "C"  Exception_t1436737249 * IVectorView_1_GetAt_m116971458 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVectorView_1_t775602511* ____ivectorView_1_t775602511 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t775602511::IID, reinterpret_cast<void**>(&____ivectorView_1_t775602511));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	int32_t returnValue = 0;
	hr = ____ivectorView_1_t775602511->IVectorView_1_GetAt_m116971458(___index0, &returnValue);
	____ivectorView_1_t775602511->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return (returnValue != IL2CPP_S_OK ? reinterpret_cast<Exception_t1436737249 *>(il2cpp_codegen_com_get_exception(returnValue, false)) : NULL);
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Exception>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m917737768 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t775602511* ____ivectorView_1_t775602511 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t775602511::IID, reinterpret_cast<void**>(&____ivectorView_1_t775602511));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t775602511->IVectorView_1_get_Size_m917737768(&returnValue);
	____ivectorView_1_t775602511->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Exception>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m2525069292 (RuntimeObject* __this, Exception_t1436737249 * ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t775602511* ____ivectorView_1_t775602511 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t775602511::IID, reinterpret_cast<void**>(&____ivectorView_1_t775602511));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t775602511->IVectorView_1_IndexOf_m2525069292((___value0 != NULL ? reinterpret_cast<RuntimeException*>(___value0)->hresult : IL2CPP_S_OK), (&____index1_empty), &returnValue);
	____ivectorView_1_t775602511->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Exception>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m979855732 (RuntimeObject* __this, uint32_t ___startIndex0, ExceptionU5BU5D_t2535001212* ___items1, const RuntimeMethod* method)
{
	IVectorView_1_t775602511* ____ivectorView_1_t775602511 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t775602511::IID, reinterpret_cast<void**>(&____ivectorView_1_t775602511));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	int32_t* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<int32_t>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(int32_t));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t775602511->IVectorView_1_GetMany_m979855732(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t775602511->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), ((____items1_marshaled)[i] != IL2CPP_S_OK ? reinterpret_cast<Exception_t1436737249 *>(il2cpp_codegen_com_get_exception((____items1_marshaled)[i], false)) : NULL));
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Guid>::GetAt(System.UInt32)
extern "C"  Guid_t  IVectorView_1_GetAt_m3400009220 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVectorView_1_t2532398149* ____ivectorView_1_t2532398149 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2532398149::IID, reinterpret_cast<void**>(&____ivectorView_1_t2532398149));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Guid_t  returnValue = {};
	hr = ____ivectorView_1_t2532398149->IVectorView_1_GetAt_m3400009220(___index0, &returnValue);
	____ivectorView_1_t2532398149->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Guid>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m50190753 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t2532398149* ____ivectorView_1_t2532398149 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2532398149::IID, reinterpret_cast<void**>(&____ivectorView_1_t2532398149));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2532398149->IVectorView_1_get_Size_m50190753(&returnValue);
	____ivectorView_1_t2532398149->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Guid>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m4154363339 (RuntimeObject* __this, Guid_t  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t2532398149* ____ivectorView_1_t2532398149 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2532398149::IID, reinterpret_cast<void**>(&____ivectorView_1_t2532398149));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t2532398149->IVectorView_1_IndexOf_m4154363339(___value0, (&____index1_empty), &returnValue);
	____ivectorView_1_t2532398149->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Guid>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m2882594533 (RuntimeObject* __this, uint32_t ___startIndex0, GuidU5BU5D_t545550574* ___items1, const RuntimeMethod* method)
{
	IVectorView_1_t2532398149* ____ivectorView_1_t2532398149 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2532398149::IID, reinterpret_cast<void**>(&____ivectorView_1_t2532398149));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	Guid_t * ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<Guid_t >(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(Guid_t ));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2532398149->IVectorView_1_GetMany_m2882594533(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t2532398149->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.IDisposable>::GetAt(System.UInt32)
extern "C"  RuntimeObject* IVectorView_1_GetAt_m3461880810 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m3461880810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2979130745* ____ivectorView_1_t2979130745 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2979130745::IID, reinterpret_cast<void**>(&____ivectorView_1_t2979130745));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IClosable_t326290202* returnValue = NULL;
	hr = ____ivectorView_1_t2979130745->IVectorView_1_GetAt_m3461880810(___index0, &returnValue);
	____ivectorView_1_t2979130745->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.IDisposable>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m671734930 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t2979130745* ____ivectorView_1_t2979130745 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2979130745::IID, reinterpret_cast<void**>(&____ivectorView_1_t2979130745));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2979130745->IVectorView_1_get_Size_m671734930(&returnValue);
	____ivectorView_1_t2979130745->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.IDisposable>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m456269461 (RuntimeObject* __this, RuntimeObject* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t2979130745* ____ivectorView_1_t2979130745 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2979130745::IID, reinterpret_cast<void**>(&____ivectorView_1_t2979130745));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IClosable_t326290202* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(IClosable_t326290202::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<IClosable_t326290202>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t2979130745->IVectorView_1_IndexOf_m456269461(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t2979130745->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.IDisposable>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m1458693733 (RuntimeObject* __this, uint32_t ___startIndex0, IDisposableU5BU5D_t3584190570* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m1458693733_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2979130745* ____ivectorView_1_t2979130745 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2979130745::IID, reinterpret_cast<void**>(&____ivectorView_1_t2979130745));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IClosable_t326290202** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IClosable_t326290202*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IClosable_t326290202*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2979130745->IVectorView_1_GetMany_m1458693733(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t2979130745->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Int16>::GetAt(System.UInt32)
extern "C"  int16_t IVectorView_1_GetAt_m1319642345 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVectorView_1_t1891685649* ____ivectorView_1_t1891685649 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1891685649::IID, reinterpret_cast<void**>(&____ivectorView_1_t1891685649));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	int16_t returnValue = 0;
	hr = ____ivectorView_1_t1891685649->IVectorView_1_GetAt_m1319642345(___index0, &returnValue);
	____ivectorView_1_t1891685649->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Int16>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m2071989762 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t1891685649* ____ivectorView_1_t1891685649 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1891685649::IID, reinterpret_cast<void**>(&____ivectorView_1_t1891685649));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1891685649->IVectorView_1_get_Size_m2071989762(&returnValue);
	____ivectorView_1_t1891685649->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Int16>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m1081404675 (RuntimeObject* __this, int16_t ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t1891685649* ____ivectorView_1_t1891685649 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1891685649::IID, reinterpret_cast<void**>(&____ivectorView_1_t1891685649));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t1891685649->IVectorView_1_IndexOf_m1081404675(___value0, (&____index1_empty), &returnValue);
	____ivectorView_1_t1891685649->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Int16>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m3136641509 (RuntimeObject* __this, uint32_t ___startIndex0, Int16U5BU5D_t3686840178* ___items1, const RuntimeMethod* method)
{
	IVectorView_1_t1891685649* ____ivectorView_1_t1891685649 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t1891685649::IID, reinterpret_cast<void**>(&____ivectorView_1_t1891685649));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	int16_t* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<int16_t>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(int16_t));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t1891685649->IVectorView_1_GetMany_m3136641509(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t1891685649->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Int32>::GetAt(System.UInt32)
extern "C"  int32_t IVectorView_1_GetAt_m1038499426 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVectorView_1_t2289811015* ____ivectorView_1_t2289811015 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2289811015::IID, reinterpret_cast<void**>(&____ivectorView_1_t2289811015));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	int32_t returnValue = 0;
	hr = ____ivectorView_1_t2289811015->IVectorView_1_GetAt_m1038499426(___index0, &returnValue);
	____ivectorView_1_t2289811015->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Int32>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m3269133634 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t2289811015* ____ivectorView_1_t2289811015 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2289811015::IID, reinterpret_cast<void**>(&____ivectorView_1_t2289811015));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2289811015->IVectorView_1_get_Size_m3269133634(&returnValue);
	____ivectorView_1_t2289811015->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Int32>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m711808760 (RuntimeObject* __this, int32_t ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t2289811015* ____ivectorView_1_t2289811015 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2289811015::IID, reinterpret_cast<void**>(&____ivectorView_1_t2289811015));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t2289811015->IVectorView_1_IndexOf_m711808760(___value0, (&____index1_empty), &returnValue);
	____ivectorView_1_t2289811015->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Int32>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m632553004 (RuntimeObject* __this, uint32_t ___startIndex0, Int32U5BU5D_t385246372* ___items1, const RuntimeMethod* method)
{
	IVectorView_1_t2289811015* ____ivectorView_1_t2289811015 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2289811015::IID, reinterpret_cast<void**>(&____ivectorView_1_t2289811015));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	int32_t* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<int32_t>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(int32_t));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2289811015->IVectorView_1_GetMany_m632553004(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t2289811015->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Int64>::GetAt(System.UInt32)
extern "C"  int64_t IVectorView_1_GetAt_m4097062415 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVectorView_1_t3075432566* ____ivectorView_1_t3075432566 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3075432566::IID, reinterpret_cast<void**>(&____ivectorView_1_t3075432566));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	int64_t returnValue = 0;
	hr = ____ivectorView_1_t3075432566->IVectorView_1_GetAt_m4097062415(___index0, &returnValue);
	____ivectorView_1_t3075432566->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Int64>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m3689183858 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t3075432566* ____ivectorView_1_t3075432566 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3075432566::IID, reinterpret_cast<void**>(&____ivectorView_1_t3075432566));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3075432566->IVectorView_1_get_Size_m3689183858(&returnValue);
	____ivectorView_1_t3075432566->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Int64>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m928127612 (RuntimeObject* __this, int64_t ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t3075432566* ____ivectorView_1_t3075432566 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3075432566::IID, reinterpret_cast<void**>(&____ivectorView_1_t3075432566));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t3075432566->IVectorView_1_IndexOf_m928127612(___value0, (&____index1_empty), &returnValue);
	____ivectorView_1_t3075432566->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Int64>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m2435124798 (RuntimeObject* __this, uint32_t ___startIndex0, Int64U5BU5D_t2559172825* ___items1, const RuntimeMethod* method)
{
	IVectorView_1_t3075432566* ____ivectorView_1_t3075432566 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t3075432566::IID, reinterpret_cast<void**>(&____ivectorView_1_t3075432566));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	int64_t* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<int64_t>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(int64_t));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t3075432566->IVectorView_1_GetMany_m2435124798(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t3075432566->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// T Windows.Foundation.Collections.IVectorView`1<System.Object>::GetAt(System.UInt32)
extern "C"  RuntimeObject * IVectorView_1_GetAt_m726655415 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetAt_m726655415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2418971426* ____ivectorView_1_t2418971426 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2418971426::IID, reinterpret_cast<void**>(&____ivectorView_1_t2418971426));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____ivectorView_1_t2418971426->IVectorView_1_GetAt_m726655415(___index0, &returnValue);
	____ivectorView_1_t2418971426->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Object>::get_Size()
extern "C"  uint32_t IVectorView_1_get_Size_m1102039859 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVectorView_1_t2418971426* ____ivectorView_1_t2418971426 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2418971426::IID, reinterpret_cast<void**>(&____ivectorView_1_t2418971426));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2418971426->IVectorView_1_get_Size_m1102039859(&returnValue);
	____ivectorView_1_t2418971426->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IVectorView`1<System.Object>::IndexOf(T,System.UInt32&)
extern "C"  bool IVectorView_1_IndexOf_m2349108669 (RuntimeObject* __this, RuntimeObject * ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVectorView_1_t2418971426* ____ivectorView_1_t2418971426 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2418971426::IID, reinterpret_cast<void**>(&____ivectorView_1_t2418971426));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	Il2CppIInspectable* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivectorView_1_t2418971426->IVectorView_1_IndexOf_m2349108669(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivectorView_1_t2418971426->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVectorView`1<System.Object>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVectorView_1_GetMany_m4226896717 (RuntimeObject* __this, uint32_t ___startIndex0, ObjectU5BU5D_t2843939325* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVectorView_1_GetMany_m4226896717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVectorView_1_t2418971426* ____ivectorView_1_t2418971426 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVectorView_1_t2418971426::IID, reinterpret_cast<void**>(&____ivectorView_1_t2418971426));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	Il2CppIInspectable** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<Il2CppIInspectable*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(Il2CppIInspectable*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivectorView_1_t2418971426->IVectorView_1_GetMany_m4226896717(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivectorView_1_t2418971426->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject * _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
