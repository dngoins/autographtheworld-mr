﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Object>>
struct IIterator_1_t1518672543;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Type>>
struct IIterator_1_t922511139;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>
struct IIterator_1_t2400939337;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>>
struct IIterator_1_t1804777933;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IIterator_1_t2240635135;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct IIterator_1_t119594292;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct IIterator_1_t1716509889;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IIterator_1_t275200381;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IIterator_1_t404360792;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.String>>
struct IIterator_1_t3466672613;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>>
struct IIterator_1_t154359904;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>
struct IIterator_1_t3008017692;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>
struct IIterator_1_t3137178103;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct IIterator_1_t1904522628;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Type>>
struct IIterator_1_t2541016699;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>>
struct IIterator_1_t905140168;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>
struct IIterator_1_t331222170;
// Windows.Foundation.Collections.IIterator`1<System.Collections.IEnumerable>
struct IIterator_1_t4110278780;
// Windows.Foundation.Collections.IIterator`1<System.Collections.IList>
struct IIterator_1_t4264041985;
// Windows.Foundation.Collections.IIterator`1<System.Collections.Specialized.INotifyCollectionChanged>
struct IIterator_1_t1172547230;
// Windows.Foundation.Collections.IIterator`1<System.DateTimeOffset>
struct IIterator_1_t1103430980;
// Windows.Foundation.Collections.IIterator`1<System.Double>
struct IIterator_1_t2763776132;
// Windows.Foundation.Collections.IIterator`1<System.Exception>
struct IIterator_1_t3605848018;
// Windows.Foundation.Collections.IIterator`1<System.Guid>
struct IIterator_1_t1067676360;
// Windows.Foundation.Collections.IIterator`1<System.IDisposable>
struct IIterator_1_t1514408956;
// Windows.Foundation.Collections.IIterator`1<System.Int16>
struct IIterator_1_t426963860;
// Windows.Foundation.Collections.IIterator`1<System.Int32>
struct IIterator_1_t825089226;
// Windows.Foundation.Collections.IIterator`1<System.Int64>
struct IIterator_1_t1610710777;
// Windows.Foundation.Collections.IIterator`1<System.Object>
struct IIterator_1_t954249637;
// Windows.Foundation.Collections.IIterator`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct IIterator_1_t2488001557;
// Windows.Foundation.Collections.IIterator`1<System.Single>
struct IIterator_1_t3566377543;
// Windows.Foundation.Collections.IIterator`1<System.String>
struct IIterator_1_t4016561458;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;

extern RuntimeClass* Il2CppComObject_il2cpp_TypeInfo_var;
extern const uint32_t IIterable_1_First_m3945548141_MetadataUsageId;
extern const uint32_t IIterable_1_First_m3575125075_MetadataUsageId;
extern const uint32_t IIterable_1_First_m597753835_MetadataUsageId;
extern const uint32_t IIterable_1_First_m3518999738_MetadataUsageId;
extern const uint32_t IIterable_1_First_m513098253_MetadataUsageId;
extern const uint32_t IIterable_1_First_m1699614814_MetadataUsageId;
extern const uint32_t IIterable_1_First_m202819341_MetadataUsageId;
extern const uint32_t IIterable_1_First_m2644326382_MetadataUsageId;
extern const uint32_t IIterable_1_First_m4041697847_MetadataUsageId;
extern const uint32_t IIterable_1_First_m3483466982_MetadataUsageId;
extern const uint32_t IIterable_1_First_m3386644619_MetadataUsageId;
extern const uint32_t IIterable_1_First_m3521229620_MetadataUsageId;
extern const uint32_t IIterable_1_First_m1145832441_MetadataUsageId;
extern const uint32_t IIterable_1_First_m2142866660_MetadataUsageId;
extern const uint32_t IIterable_1_First_m2694388202_MetadataUsageId;
extern const uint32_t IIterable_1_First_m99514461_MetadataUsageId;
extern const uint32_t IIterable_1_First_m817385671_MetadataUsageId;
extern const uint32_t IIterable_1_First_m595838657_MetadataUsageId;
extern const uint32_t IIterable_1_First_m3066985290_MetadataUsageId;
extern const uint32_t IIterable_1_First_m1959018634_MetadataUsageId;
extern const uint32_t IIterable_1_First_m2198371330_MetadataUsageId;
extern const uint32_t IIterable_1_First_m2037907598_MetadataUsageId;
extern const uint32_t IIterable_1_First_m2732774064_MetadataUsageId;
extern const uint32_t IIterable_1_First_m401588167_MetadataUsageId;
extern const uint32_t IIterable_1_First_m3456704609_MetadataUsageId;
extern const uint32_t IIterable_1_First_m502408437_MetadataUsageId;
extern const uint32_t IIterable_1_First_m1280577591_MetadataUsageId;
extern const uint32_t IIterable_1_First_m3916705692_MetadataUsageId;
extern const uint32_t IIterable_1_First_m3341821894_MetadataUsageId;
extern const uint32_t IIterable_1_First_m4102057096_MetadataUsageId;
extern const uint32_t IIterable_1_First_m4078210676_MetadataUsageId;
extern const uint32_t IIterable_1_First_m3975378966_MetadataUsageId;
struct IIterator_1_t1103430980;
struct INotifyCollectionChanged_t3244377239;
struct IIterator_1_t2763776132;
struct IIterator_1_t3605848018;
struct IIterator_1_t1172547230;
struct IIterator_1_t331222170;
struct IKeyValuePair_2_t4055716313;
struct IIterator_1_t905140168;
struct IIterator_1_t4110278780;
struct IBindableVector_t1801110279;
struct IIterator_1_t4264041985;
struct IBindableIterable_t2147255965;
struct IIterator_1_t1067676360;
struct IIterator_1_t954249637;
struct IIterator_1_t2488001557;
struct IIterator_1_t3566377543;
struct IIterator_1_t4016561458;
struct IIterator_1_t1610710777;
struct IClosable_t326290202;
struct IIterator_1_t1514408956;
struct IIterator_1_t426963860;
struct IIterator_1_t825089226;
struct IKeyValuePair_2_t1096243984;
struct IIterator_1_t119594292;
struct IKeyValuePair_2_t660386782;
struct IIterator_1_t2240635135;
struct IKeyValuePair_2_t572118738;
struct IIterator_1_t275200381;
struct IKeyValuePair_2_t3270170437;
struct IIterator_1_t1716509889;
struct IVectorView_1_t2418971426;
struct IIterator_1_t922511139;
struct IKeyValuePair_2_t3481798315;
struct IIterator_1_t1518672543;
struct IKeyValuePair_2_t1256548186;
struct IIterator_1_t1804777933;
struct IVectorView_1_t1822810022;
struct IIterator_1_t2400939337;
struct IKeyValuePair_2_t1992786952;
struct IIterator_1_t3137178103;
struct IKeyValuePair_2_t1863626541;
struct IIterator_1_t1904522628;
struct IKeyValuePair_2_t1396625548;
struct IIterator_1_t2541016699;
struct IKeyValuePair_2_t760131477;
struct IIterator_1_t3008017692;
struct IKeyValuePair_2_t3554936937;
struct IIterator_1_t404360792;
struct IKeyValuePair_2_t3425776526;
struct IIterator_1_t3466672613;
struct IIterator_1_t154359904;
struct IKeyValuePair_2_t3304936049;
struct IKeyValuePair_2_t2322281462;
struct EventRegistrationToken_t318890788 ;
struct Guid_t ;
struct DateTime_t1679451545 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
// Windows.Foundation.Collections.IIterable`1<System.DateTimeOffset>
struct NOVTABLE IIterable_1_t850684967 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m2198371330(IIterator_1_t1103430980** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Specialized.INotifyCollectionChanged>
struct NOVTABLE IIterator_1_t1172547230 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3492343637(INotifyCollectionChanged_t3244377239** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2572569362(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m4245137134(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2177145106(uint32_t ___items0ArraySize, INotifyCollectionChanged_t3244377239** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Double>
struct NOVTABLE IIterable_1_t2511030119 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m2037907598(IIterator_1_t2763776132** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Exception>
struct NOVTABLE IIterator_1_t3605848018 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1320729276(int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m310314726(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1496487038(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2336542395(uint32_t ___items0ArraySize, int32_t* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Exception>
struct NOVTABLE IIterable_1_t3353102005 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m2732774064(IIterator_1_t3605848018** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Double>
struct NOVTABLE IIterator_1_t2763776132 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m354509700(double* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3297171499(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3160156876(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2319078079(uint32_t ___items0ArraySize, double* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Specialized.INotifyCollectionChanged>
struct NOVTABLE IIterable_1_t919801217 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1959018634(IIterator_1_t1172547230** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>
struct NOVTABLE IIterable_1_t78476157 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m817385671(IIterator_1_t331222170** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>>
struct NOVTABLE IIterator_1_t905140168 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3137541978(IKeyValuePair_2_t4055716313** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m94151755(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2289443705(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3549273332(uint32_t ___items0ArraySize, IKeyValuePair_2_t4055716313** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>>
struct NOVTABLE IIterable_1_t652394155 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m99514461(IIterator_1_t905140168** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.IEnumerable>
struct NOVTABLE IIterable_1_t3857532767 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m595838657(IIterator_1_t4110278780** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.IList>
struct NOVTABLE IIterator_1_t4264041985 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m868157985(IBindableVector_t1801110279** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1647396666(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m95947916(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1762761344(uint32_t ___items0ArraySize, IBindableVector_t1801110279** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.IList>
struct NOVTABLE IIterable_1_t4011295972 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3066985290(IIterator_1_t4264041985** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.IEnumerable>
struct NOVTABLE IIterator_1_t4110278780 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3525622341(IBindableIterable_t2147255965** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3052657133(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2281684807(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m86224293(uint32_t ___items0ArraySize, IBindableIterable_t2147255965** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Guid>
struct NOVTABLE IIterable_1_t814930347 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m401588167(IIterator_1_t1067676360** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Object>
struct NOVTABLE IIterator_1_t954249637 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m4033148858(Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2403098075(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3126031461(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1443615899(uint32_t ___items0ArraySize, Il2CppIInspectable** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Object>
struct NOVTABLE IIterable_1_t701503624 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3341821894(IIterator_1_t954249637** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Int64>
struct NOVTABLE IIterator_1_t1610710777 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m384601225(int64_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3484924956(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m997059212(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m779360946(uint32_t ___items0ArraySize, int64_t* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct NOVTABLE IIterable_1_t2235255544 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m4102057096(IIterator_1_t2488001557** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Single>
struct NOVTABLE IIterator_1_t3566377543 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1146799328(float* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2822908534(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2523572727(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3822961495(uint32_t ___items0ArraySize, float* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Single>
struct NOVTABLE IIterable_1_t3313631530 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m4078210676(IIterator_1_t3566377543** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.String>
struct NOVTABLE IIterable_1_t3763815445 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3975378966(IIterator_1_t4016561458** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Int64>
struct NOVTABLE IIterable_1_t1357964764 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3916705692(IIterator_1_t1610710777** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.IDisposable>
struct NOVTABLE IIterator_1_t1514408956 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1657856171(IClosable_t326290202** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m888591543(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m495239480(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m10755140(uint32_t ___items0ArraySize, IClosable_t326290202** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.IDisposable>
struct NOVTABLE IIterable_1_t1261662943 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3456704609(IIterator_1_t1514408956** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.String>
struct NOVTABLE IIterator_1_t4016561458 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2970055406(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3146542745(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1173289273(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1644995850(uint32_t ___items0ArraySize, Il2CppHString* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Int16>
struct NOVTABLE IIterable_1_t174217847 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m502408437(IIterator_1_t426963860** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Int32>
struct NOVTABLE IIterator_1_t825089226 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3230580114(int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3822991096(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1179569845(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2626728204(uint32_t ___items0ArraySize, int32_t* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Int32>
struct NOVTABLE IIterable_1_t572343213 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1280577591(IIterator_1_t825089226** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Int16>
struct NOVTABLE IIterator_1_t426963860 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3967028616(int16_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3077644356(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3913675470(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2584956007(uint32_t ___items0ArraySize, int16_t* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct NOVTABLE IIterator_1_t2240635135 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2947568524(IKeyValuePair_2_t1096243984** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3859218363(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2670346608(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3900158597(uint32_t ___items0ArraySize, IKeyValuePair_2_t1096243984** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct NOVTABLE IIterable_1_t4161815575 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1699614814(IIterator_1_t119594292** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>>
struct NOVTABLE IIterator_1_t1804777933 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3513860320(IKeyValuePair_2_t660386782** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m65888504(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m859540255(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3415100491(uint32_t ___items0ArraySize, IKeyValuePair_2_t660386782** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct NOVTABLE IIterable_1_t1987889122 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m513098253(IIterator_1_t2240635135** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct NOVTABLE IIterator_1_t1716509889 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3308861090(IKeyValuePair_2_t572118738** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m583209192(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3227612391(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3064915941(uint32_t ___items0ArraySize, IKeyValuePair_2_t572118738** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct NOVTABLE IIterable_1_t22454368 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m2644326382(IIterator_1_t275200381** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct NOVTABLE IIterator_1_t119594292 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3547052056(IKeyValuePair_2_t3270170437** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m344842528(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1308250629(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3058286030(uint32_t ___items0ArraySize, IKeyValuePair_2_t3270170437** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct NOVTABLE IIterable_1_t1463763876 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m202819341(IIterator_1_t1716509889** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Object>>
struct NOVTABLE IIterator_1_t1518672543 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3967696190(IVectorView_1_t2418971426** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2125417207(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3930599802(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1750935882(uint32_t ___items0ArraySize, IVectorView_1_t2418971426** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.IReadOnlyList`1<System.Type>>
struct NOVTABLE IIterable_1_t669765126 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3575125075(IIterator_1_t922511139** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>
struct NOVTABLE IIterator_1_t331222170 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3355334578(IKeyValuePair_2_t3481798315** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m940628351(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1278101282(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m253528417(uint32_t ___items0ArraySize, IKeyValuePair_2_t3481798315** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.IReadOnlyList`1<System.Object>>
struct NOVTABLE IIterable_1_t1265926530 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3945548141(IIterator_1_t1518672543** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>
struct NOVTABLE IIterator_1_t2400939337 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m74209353(IKeyValuePair_2_t1256548186** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2249475151(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2303149837(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3197996383(uint32_t ___items0ArraySize, IKeyValuePair_2_t1256548186** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>>
struct NOVTABLE IIterable_1_t1552031920 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3518999738(IIterator_1_t1804777933** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Type>>
struct NOVTABLE IIterator_1_t922511139 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2737245895(IVectorView_1_t1822810022** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3441350389(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1152394543(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m289763957(uint32_t ___items0ArraySize, IVectorView_1_t1822810022** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>
struct NOVTABLE IIterable_1_t2148193324 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m597753835(IIterator_1_t2400939337** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>
struct NOVTABLE IIterator_1_t3137178103 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m666052747(IKeyValuePair_2_t1992786952** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1155820548(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m662502371(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2177387005(uint32_t ___items0ArraySize, IKeyValuePair_2_t1992786952** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>
struct NOVTABLE IIterable_1_t2884432090 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1145832441(IIterator_1_t3137178103** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>
struct NOVTABLE IIterator_1_t3008017692 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2662827370(IKeyValuePair_2_t1863626541** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1246919178(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3014791206(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3416053910(uint32_t ___items0ArraySize, IKeyValuePair_2_t1863626541** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct NOVTABLE IIterable_1_t1651776615 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m2142866660(IIterator_1_t1904522628** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Type>>
struct NOVTABLE IIterator_1_t2541016699 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1678804315(IKeyValuePair_2_t1396625548** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m299964395(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3438651992(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1622155850(uint32_t ___items0ArraySize, IKeyValuePair_2_t1396625548** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Type>>
struct NOVTABLE IIterable_1_t2288270686 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m2694388202(IIterator_1_t2541016699** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct NOVTABLE IIterator_1_t1904522628 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m726370678(IKeyValuePair_2_t760131477** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2172811539(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1580168139(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3453179664(uint32_t ___items0ArraySize, IKeyValuePair_2_t760131477** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>
struct NOVTABLE IIterable_1_t2755271679 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3521229620(IIterator_1_t3008017692** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct NOVTABLE IIterator_1_t404360792 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3426234107(IKeyValuePair_2_t3554936937** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2174897718(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m106249972(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3542193621(uint32_t ___items0ArraySize, IKeyValuePair_2_t3554936937** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct NOVTABLE IIterable_1_t151614779 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m4041697847(IIterator_1_t404360792** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct NOVTABLE IIterator_1_t275200381 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3813727452(IKeyValuePair_2_t3425776526** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m538388502(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2855647886(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3959935491(uint32_t ___items0ArraySize, IKeyValuePair_2_t3425776526** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.String>>
struct NOVTABLE IIterable_1_t3213926600 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3483466982(IIterator_1_t3466672613** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>>
struct NOVTABLE IIterable_1_t4196581187 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3386644619(IIterator_1_t154359904** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>>
struct NOVTABLE IIterator_1_t154359904 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2930912929(IKeyValuePair_2_t3304936049** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2156060275(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3137142701(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1195944220(uint32_t ___items0ArraySize, IKeyValuePair_2_t3304936049** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.String>>
struct NOVTABLE IIterator_1_t3466672613 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2651871047(IKeyValuePair_2_t2322281462** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3598836853(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1594093953(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m4151208252(uint32_t ___items0ArraySize, IKeyValuePair_2_t2322281462** ___items0, uint32_t* comReturnValue) = 0;
};
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_4;
	// System.Int16 System.Guid::_b
	int16_t ____b_5;
	// System.Int16 System.Guid::_c
	int16_t ____c_6;
	// System.Byte System.Guid::_d
	uint8_t ____d_7;
	// System.Byte System.Guid::_e
	uint8_t ____e_8;
	// System.Byte System.Guid::_f
	uint8_t ____f_9;
	// System.Byte System.Guid::_g
	uint8_t ____g_10;
	// System.Byte System.Guid::_h
	uint8_t ____h_11;
	// System.Byte System.Guid::_i
	uint8_t ____i_12;
	// System.Byte System.Guid::_j
	uint8_t ____j_13;
	// System.Byte System.Guid::_k
	uint8_t ____k_14;

public:
	inline static int32_t get_offset_of__a_4() { return static_cast<int32_t>(offsetof(Guid_t, ____a_4)); }
	inline int32_t get__a_4() const { return ____a_4; }
	inline int32_t* get_address_of__a_4() { return &____a_4; }
	inline void set__a_4(int32_t value)
	{
		____a_4 = value;
	}

	inline static int32_t get_offset_of__b_5() { return static_cast<int32_t>(offsetof(Guid_t, ____b_5)); }
	inline int16_t get__b_5() const { return ____b_5; }
	inline int16_t* get_address_of__b_5() { return &____b_5; }
	inline void set__b_5(int16_t value)
	{
		____b_5 = value;
	}

	inline static int32_t get_offset_of__c_6() { return static_cast<int32_t>(offsetof(Guid_t, ____c_6)); }
	inline int16_t get__c_6() const { return ____c_6; }
	inline int16_t* get_address_of__c_6() { return &____c_6; }
	inline void set__c_6(int16_t value)
	{
		____c_6 = value;
	}

	inline static int32_t get_offset_of__d_7() { return static_cast<int32_t>(offsetof(Guid_t, ____d_7)); }
	inline uint8_t get__d_7() const { return ____d_7; }
	inline uint8_t* get_address_of__d_7() { return &____d_7; }
	inline void set__d_7(uint8_t value)
	{
		____d_7 = value;
	}

	inline static int32_t get_offset_of__e_8() { return static_cast<int32_t>(offsetof(Guid_t, ____e_8)); }
	inline uint8_t get__e_8() const { return ____e_8; }
	inline uint8_t* get_address_of__e_8() { return &____e_8; }
	inline void set__e_8(uint8_t value)
	{
		____e_8 = value;
	}

	inline static int32_t get_offset_of__f_9() { return static_cast<int32_t>(offsetof(Guid_t, ____f_9)); }
	inline uint8_t get__f_9() const { return ____f_9; }
	inline uint8_t* get_address_of__f_9() { return &____f_9; }
	inline void set__f_9(uint8_t value)
	{
		____f_9 = value;
	}

	inline static int32_t get_offset_of__g_10() { return static_cast<int32_t>(offsetof(Guid_t, ____g_10)); }
	inline uint8_t get__g_10() const { return ____g_10; }
	inline uint8_t* get_address_of__g_10() { return &____g_10; }
	inline void set__g_10(uint8_t value)
	{
		____g_10 = value;
	}

	inline static int32_t get_offset_of__h_11() { return static_cast<int32_t>(offsetof(Guid_t, ____h_11)); }
	inline uint8_t get__h_11() const { return ____h_11; }
	inline uint8_t* get_address_of__h_11() { return &____h_11; }
	inline void set__h_11(uint8_t value)
	{
		____h_11 = value;
	}

	inline static int32_t get_offset_of__i_12() { return static_cast<int32_t>(offsetof(Guid_t, ____i_12)); }
	inline uint8_t get__i_12() const { return ____i_12; }
	inline uint8_t* get_address_of__i_12() { return &____i_12; }
	inline void set__i_12(uint8_t value)
	{
		____i_12 = value;
	}

	inline static int32_t get_offset_of__j_13() { return static_cast<int32_t>(offsetof(Guid_t, ____j_13)); }
	inline uint8_t get__j_13() const { return ____j_13; }
	inline uint8_t* get_address_of__j_13() { return &____j_13; }
	inline void set__j_13(uint8_t value)
	{
		____j_13 = value;
	}

	inline static int32_t get_offset_of__k_14() { return static_cast<int32_t>(offsetof(Guid_t, ____k_14)); }
	inline uint8_t get__k_14() const { return ____k_14; }
	inline uint8_t* get_address_of__k_14() { return &____k_14; }
	inline void set__k_14(uint8_t value)
	{
		____k_14 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_0;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_1;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_2;
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_3;

public:
	inline static int32_t get_offset_of__rngAccess_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_0)); }
	inline RuntimeObject * get__rngAccess_0() const { return ____rngAccess_0; }
	inline RuntimeObject ** get_address_of__rngAccess_0() { return &____rngAccess_0; }
	inline void set__rngAccess_0(RuntimeObject * value)
	{
		____rngAccess_0 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_0), value);
	}

	inline static int32_t get_offset_of__rng_1() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_1)); }
	inline RandomNumberGenerator_t386037858 * get__rng_1() const { return ____rng_1; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_1() { return &____rng_1; }
	inline void set__rng_1(RandomNumberGenerator_t386037858 * value)
	{
		____rng_1 = value;
		Il2CppCodeGenWriteBarrier((&____rng_1), value);
	}

	inline static int32_t get_offset_of__fastRng_2() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_2)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_2() const { return ____fastRng_2; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_2() { return &____fastRng_2; }
	inline void set__fastRng_2(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_2 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_2), value);
	}

	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_3)); }
	inline Guid_t  get_Empty_3() const { return ___Empty_3; }
	inline Guid_t * get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(Guid_t  value)
	{
		___Empty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef EVENTREGISTRATIONTOKEN_T318890788_H
#define EVENTREGISTRATIONTOKEN_T318890788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken
struct  EventRegistrationToken_t318890788 
{
public:
	// System.UInt64 System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(EventRegistrationToken_t318890788, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTREGISTRATIONTOKEN_T318890788_H
#ifndef DATETIME_T1679451545_H
#define DATETIME_T1679451545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.DateTime
struct  DateTime_t1679451545 
{
public:
	// System.Int64 Windows.Foundation.DateTime::UniversalTime
	int64_t ___UniversalTime_0;

public:
	inline static int32_t get_offset_of_UniversalTime_0() { return static_cast<int32_t>(offsetof(DateTime_t1679451545, ___UniversalTime_0)); }
	inline int64_t get_UniversalTime_0() const { return ___UniversalTime_0; }
	inline int64_t* get_address_of_UniversalTime_0() { return &___UniversalTime_0; }
	inline void set_UniversalTime_0(int64_t value)
	{
		___UniversalTime_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T1679451545_H
// Windows.Foundation.Collections.IIterator`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct NOVTABLE IIterator_1_t2488001557 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2952276126(EventRegistrationToken_t318890788 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1513168069(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m467192383(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m620125635(uint32_t ___items0ArraySize, EventRegistrationToken_t318890788 * ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Guid>
struct NOVTABLE IIterator_1_t1067676360 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m5372693(Guid_t * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3692598929(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2924594529(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3031797434(uint32_t ___items0ArraySize, Guid_t * ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.DateTimeOffset>
struct NOVTABLE IIterator_1_t1103430980 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3227006710(DateTime_t1679451545 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m57224880(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1172097404(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m875085322(uint32_t ___items0ArraySize, DateTime_t1679451545 * ___items0, uint32_t* comReturnValue) = 0;
};



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.IReadOnlyList`1<System.Object>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m3945548141 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m3945548141_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t1265926530* ____iiterable_1_t1265926530 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t1265926530::IID, reinterpret_cast<void**>(&____iiterable_1_t1265926530));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t1518672543* returnValue = NULL;
	hr = ____iiterable_1_t1265926530->IIterable_1_First_m3945548141(&returnValue);
	____iiterable_1_t1265926530->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.IReadOnlyList`1<System.Type>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m3575125075 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m3575125075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t669765126* ____iiterable_1_t669765126 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t669765126::IID, reinterpret_cast<void**>(&____iiterable_1_t669765126));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t922511139* returnValue = NULL;
	hr = ____iiterable_1_t669765126->IIterable_1_First_m3575125075(&returnValue);
	____iiterable_1_t669765126->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m597753835 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m597753835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t2148193324* ____iiterable_1_t2148193324 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t2148193324::IID, reinterpret_cast<void**>(&____iiterable_1_t2148193324));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t2400939337* returnValue = NULL;
	hr = ____iiterable_1_t2148193324->IIterable_1_First_m597753835(&returnValue);
	____iiterable_1_t2148193324->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m3518999738 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m3518999738_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t1552031920* ____iiterable_1_t1552031920 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t1552031920::IID, reinterpret_cast<void**>(&____iiterable_1_t1552031920));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t1804777933* returnValue = NULL;
	hr = ____iiterable_1_t1552031920->IIterable_1_First_m3518999738(&returnValue);
	____iiterable_1_t1552031920->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m513098253 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m513098253_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t1987889122* ____iiterable_1_t1987889122 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t1987889122::IID, reinterpret_cast<void**>(&____iiterable_1_t1987889122));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t2240635135* returnValue = NULL;
	hr = ____iiterable_1_t1987889122->IIterable_1_First_m513098253(&returnValue);
	____iiterable_1_t1987889122->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m1699614814 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m1699614814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t4161815575* ____iiterable_1_t4161815575 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t4161815575::IID, reinterpret_cast<void**>(&____iiterable_1_t4161815575));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t119594292* returnValue = NULL;
	hr = ____iiterable_1_t4161815575->IIterable_1_First_m1699614814(&returnValue);
	____iiterable_1_t4161815575->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m202819341 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m202819341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t1463763876* ____iiterable_1_t1463763876 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t1463763876::IID, reinterpret_cast<void**>(&____iiterable_1_t1463763876));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t1716509889* returnValue = NULL;
	hr = ____iiterable_1_t1463763876->IIterable_1_First_m202819341(&returnValue);
	____iiterable_1_t1463763876->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m2644326382 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m2644326382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t22454368* ____iiterable_1_t22454368 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t22454368::IID, reinterpret_cast<void**>(&____iiterable_1_t22454368));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t275200381* returnValue = NULL;
	hr = ____iiterable_1_t22454368->IIterable_1_First_m2644326382(&returnValue);
	____iiterable_1_t22454368->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m4041697847 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m4041697847_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t151614779* ____iiterable_1_t151614779 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t151614779::IID, reinterpret_cast<void**>(&____iiterable_1_t151614779));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t404360792* returnValue = NULL;
	hr = ____iiterable_1_t151614779->IIterable_1_First_m4041697847(&returnValue);
	____iiterable_1_t151614779->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.String>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m3483466982 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m3483466982_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t3213926600* ____iiterable_1_t3213926600 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t3213926600::IID, reinterpret_cast<void**>(&____iiterable_1_t3213926600));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t3466672613* returnValue = NULL;
	hr = ____iiterable_1_t3213926600->IIterable_1_First_m3483466982(&returnValue);
	____iiterable_1_t3213926600->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m3386644619 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m3386644619_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t4196581187* ____iiterable_1_t4196581187 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t4196581187::IID, reinterpret_cast<void**>(&____iiterable_1_t4196581187));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t154359904* returnValue = NULL;
	hr = ____iiterable_1_t4196581187->IIterable_1_First_m3386644619(&returnValue);
	____iiterable_1_t4196581187->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m3521229620 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m3521229620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t2755271679* ____iiterable_1_t2755271679 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t2755271679::IID, reinterpret_cast<void**>(&____iiterable_1_t2755271679));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t3008017692* returnValue = NULL;
	hr = ____iiterable_1_t2755271679->IIterable_1_First_m3521229620(&returnValue);
	____iiterable_1_t2755271679->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m1145832441 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m1145832441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t2884432090* ____iiterable_1_t2884432090 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t2884432090::IID, reinterpret_cast<void**>(&____iiterable_1_t2884432090));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t3137178103* returnValue = NULL;
	hr = ____iiterable_1_t2884432090->IIterable_1_First_m1145832441(&returnValue);
	____iiterable_1_t2884432090->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m2142866660 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m2142866660_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t1651776615* ____iiterable_1_t1651776615 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t1651776615::IID, reinterpret_cast<void**>(&____iiterable_1_t1651776615));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t1904522628* returnValue = NULL;
	hr = ____iiterable_1_t1651776615->IIterable_1_First_m2142866660(&returnValue);
	____iiterable_1_t1651776615->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Type>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m2694388202 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m2694388202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t2288270686* ____iiterable_1_t2288270686 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t2288270686::IID, reinterpret_cast<void**>(&____iiterable_1_t2288270686));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t2541016699* returnValue = NULL;
	hr = ____iiterable_1_t2288270686->IIterable_1_First_m2694388202(&returnValue);
	____iiterable_1_t2288270686->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Type,System.Type>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m99514461 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m99514461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t652394155* ____iiterable_1_t652394155 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t652394155::IID, reinterpret_cast<void**>(&____iiterable_1_t652394155));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t905140168* returnValue = NULL;
	hr = ____iiterable_1_t652394155->IIterable_1_First_m99514461(&returnValue);
	____iiterable_1_t652394155->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::First()
extern "C"  RuntimeObject* IIterable_1_First_m817385671 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m817385671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t78476157* ____iiterable_1_t78476157 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t78476157::IID, reinterpret_cast<void**>(&____iiterable_1_t78476157));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t331222170* returnValue = NULL;
	hr = ____iiterable_1_t78476157->IIterable_1_First_m817385671(&returnValue);
	____iiterable_1_t78476157->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.IEnumerable>::First()
extern "C"  RuntimeObject* IIterable_1_First_m595838657 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m595838657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t3857532767* ____iiterable_1_t3857532767 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t3857532767::IID, reinterpret_cast<void**>(&____iiterable_1_t3857532767));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t4110278780* returnValue = NULL;
	hr = ____iiterable_1_t3857532767->IIterable_1_First_m595838657(&returnValue);
	____iiterable_1_t3857532767->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.IList>::First()
extern "C"  RuntimeObject* IIterable_1_First_m3066985290 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m3066985290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t4011295972* ____iiterable_1_t4011295972 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t4011295972::IID, reinterpret_cast<void**>(&____iiterable_1_t4011295972));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t4264041985* returnValue = NULL;
	hr = ____iiterable_1_t4011295972->IIterable_1_First_m3066985290(&returnValue);
	____iiterable_1_t4011295972->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Collections.Specialized.INotifyCollectionChanged>::First()
extern "C"  RuntimeObject* IIterable_1_First_m1959018634 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m1959018634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t919801217* ____iiterable_1_t919801217 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t919801217::IID, reinterpret_cast<void**>(&____iiterable_1_t919801217));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t1172547230* returnValue = NULL;
	hr = ____iiterable_1_t919801217->IIterable_1_First_m1959018634(&returnValue);
	____iiterable_1_t919801217->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.DateTimeOffset>::First()
extern "C"  RuntimeObject* IIterable_1_First_m2198371330 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m2198371330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t850684967* ____iiterable_1_t850684967 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t850684967::IID, reinterpret_cast<void**>(&____iiterable_1_t850684967));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t1103430980* returnValue = NULL;
	hr = ____iiterable_1_t850684967->IIterable_1_First_m2198371330(&returnValue);
	____iiterable_1_t850684967->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Double>::First()
extern "C"  RuntimeObject* IIterable_1_First_m2037907598 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m2037907598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t2511030119* ____iiterable_1_t2511030119 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t2511030119::IID, reinterpret_cast<void**>(&____iiterable_1_t2511030119));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t2763776132* returnValue = NULL;
	hr = ____iiterable_1_t2511030119->IIterable_1_First_m2037907598(&returnValue);
	____iiterable_1_t2511030119->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Exception>::First()
extern "C"  RuntimeObject* IIterable_1_First_m2732774064 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m2732774064_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t3353102005* ____iiterable_1_t3353102005 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t3353102005::IID, reinterpret_cast<void**>(&____iiterable_1_t3353102005));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t3605848018* returnValue = NULL;
	hr = ____iiterable_1_t3353102005->IIterable_1_First_m2732774064(&returnValue);
	____iiterable_1_t3353102005->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Guid>::First()
extern "C"  RuntimeObject* IIterable_1_First_m401588167 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m401588167_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t814930347* ____iiterable_1_t814930347 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t814930347::IID, reinterpret_cast<void**>(&____iiterable_1_t814930347));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t1067676360* returnValue = NULL;
	hr = ____iiterable_1_t814930347->IIterable_1_First_m401588167(&returnValue);
	____iiterable_1_t814930347->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.IDisposable>::First()
extern "C"  RuntimeObject* IIterable_1_First_m3456704609 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m3456704609_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t1261662943* ____iiterable_1_t1261662943 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t1261662943::IID, reinterpret_cast<void**>(&____iiterable_1_t1261662943));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t1514408956* returnValue = NULL;
	hr = ____iiterable_1_t1261662943->IIterable_1_First_m3456704609(&returnValue);
	____iiterable_1_t1261662943->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Int16>::First()
extern "C"  RuntimeObject* IIterable_1_First_m502408437 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m502408437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t174217847* ____iiterable_1_t174217847 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t174217847::IID, reinterpret_cast<void**>(&____iiterable_1_t174217847));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t426963860* returnValue = NULL;
	hr = ____iiterable_1_t174217847->IIterable_1_First_m502408437(&returnValue);
	____iiterable_1_t174217847->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Int32>::First()
extern "C"  RuntimeObject* IIterable_1_First_m1280577591 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m1280577591_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t572343213* ____iiterable_1_t572343213 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t572343213::IID, reinterpret_cast<void**>(&____iiterable_1_t572343213));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t825089226* returnValue = NULL;
	hr = ____iiterable_1_t572343213->IIterable_1_First_m1280577591(&returnValue);
	____iiterable_1_t572343213->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Int64>::First()
extern "C"  RuntimeObject* IIterable_1_First_m3916705692 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m3916705692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t1357964764* ____iiterable_1_t1357964764 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t1357964764::IID, reinterpret_cast<void**>(&____iiterable_1_t1357964764));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t1610710777* returnValue = NULL;
	hr = ____iiterable_1_t1357964764->IIterable_1_First_m3916705692(&returnValue);
	____iiterable_1_t1357964764->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Object>::First()
extern "C"  RuntimeObject* IIterable_1_First_m3341821894 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m3341821894_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t701503624* ____iiterable_1_t701503624 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t701503624::IID, reinterpret_cast<void**>(&____iiterable_1_t701503624));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t954249637* returnValue = NULL;
	hr = ____iiterable_1_t701503624->IIterable_1_First_m3341821894(&returnValue);
	____iiterable_1_t701503624->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>::First()
extern "C"  RuntimeObject* IIterable_1_First_m4102057096 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m4102057096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t2235255544* ____iiterable_1_t2235255544 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t2235255544::IID, reinterpret_cast<void**>(&____iiterable_1_t2235255544));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t2488001557* returnValue = NULL;
	hr = ____iiterable_1_t2235255544->IIterable_1_First_m4102057096(&returnValue);
	____iiterable_1_t2235255544->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.Single>::First()
extern "C"  RuntimeObject* IIterable_1_First_m4078210676 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m4078210676_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t3313631530* ____iiterable_1_t3313631530 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t3313631530::IID, reinterpret_cast<void**>(&____iiterable_1_t3313631530));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t3566377543* returnValue = NULL;
	hr = ____iiterable_1_t3313631530->IIterable_1_First_m4078210676(&returnValue);
	____iiterable_1_t3313631530->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1<System.String>::First()
extern "C"  RuntimeObject* IIterable_1_First_m3975378966 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterable_1_First_m3975378966_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterable_1_t3763815445* ____iiterable_1_t3763815445 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterable_1_t3763815445::IID, reinterpret_cast<void**>(&____iiterable_1_t3763815445));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterator_1_t4016561458* returnValue = NULL;
	hr = ____iiterable_1_t3763815445->IIterable_1_First_m3975378966(&returnValue);
	____iiterable_1_t3763815445->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
