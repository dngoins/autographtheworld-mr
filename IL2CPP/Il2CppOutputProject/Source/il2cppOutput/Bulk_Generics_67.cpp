﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Generic.IReadOnlyList`1<System.Int16>
struct IReadOnlyList_1_t3117243293;
// System.Int16[]
struct Int16U5BU5D_t3686840178;
// System.Collections.Generic.IReadOnlyList`1<System.Int32>
struct IReadOnlyList_1_t3515368659;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.IReadOnlyList`1<System.Int64>
struct IReadOnlyList_1_t6022914;
// System.Int64[]
struct Int64U5BU5D_t2559172825;
// System.Collections.Generic.IReadOnlyList`1<System.Object>
struct IReadOnlyList_1_t3644529070;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Collections.Generic.IReadOnlyList`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct IReadOnlyList_1_t883313694;
// System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken[]
struct EventRegistrationTokenU5BU5D_t897718221;
// System.Collections.Generic.IReadOnlyList`1<System.Single>
struct IReadOnlyList_1_t1961689680;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.Collections.Generic.IReadOnlyList`1<System.String>
struct IReadOnlyList_1_t2411873595;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.IReadOnlyList`1<System.TimeSpan>
struct IReadOnlyList_1_t1445582155;
// System.TimeSpan[]
struct TimeSpanU5BU5D_t4291357516;
// System.Type
struct Type_t;
// System.Collections.Generic.IReadOnlyList`1<System.Type>
struct IReadOnlyList_1_t3048367666;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Collections.Generic.IReadOnlyList`1<System.UInt16>
struct IReadOnlyList_1_t2742147864;
// System.UInt16[]
struct UInt16U5BU5D_t3326319531;
// System.Collections.Generic.IReadOnlyList`1<System.UInt32>
struct IReadOnlyList_1_t3124484884;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Collections.Generic.IReadOnlyList`1<System.UInt64>
struct IReadOnlyList_1_t403495702;
// System.UInt64[]
struct UInt64U5BU5D_t1659327989;
// System.Uri
struct Uri_t100236324;
// System.Collections.Generic.IReadOnlyList`1<System.Uri>
struct IReadOnlyList_1_t664659230;
// System.Uri[]
struct UriU5BU5D_t673446605;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// Windows.Foundation.IUriRuntimeClassFactory
struct IUriRuntimeClassFactory_t1786313621;
// Windows.Foundation.IUriEscapeStatics
struct IUriEscapeStatics_t1418340874;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.Reflection.Binder
struct Binder_t2999457153;
// System.UriParser
struct UriParser_t3890150400;
// System.Uri/UriInfo
struct UriInfo_t1092684687;

extern RuntimeClass* Il2CppComObject_il2cpp_TypeInfo_var;
extern const uint32_t IVector_1_GetView_m3272889438_MetadataUsageId;
extern const uint32_t IVector_1_GetView_m1457083017_MetadataUsageId;
extern const uint32_t IVector_1_GetView_m2857412624_MetadataUsageId;
extern const uint32_t IVector_1_GetAt_m219694725_MetadataUsageId;
extern const uint32_t IVector_1_GetView_m3086397386_MetadataUsageId;
extern const uint32_t IVector_1_GetMany_m3541038849_MetadataUsageId;
extern const uint32_t IVector_1_GetView_m3332817310_MetadataUsageId;
extern const uint32_t IVector_1_GetView_m1670428144_MetadataUsageId;
extern const uint32_t IVector_1_GetView_m1219033264_MetadataUsageId;
extern const uint32_t IVector_1_GetView_m2578442391_MetadataUsageId;
extern const uint32_t IVector_1_GetView_m2404813821_MetadataUsageId;
extern const uint32_t IVector_1_GetView_m3025995885_MetadataUsageId;
extern const uint32_t IVector_1_GetView_m776116354_MetadataUsageId;
extern const uint32_t IVector_1_GetView_m435781098_MetadataUsageId;
extern RuntimeClass* Uri_t100236324_il2cpp_TypeInfo_var;
extern const uint32_t IVector_1_GetAt_m3184039671_MetadataUsageId;
extern const uint32_t IVector_1_GetView_m748183030_MetadataUsageId;
extern RuntimeClass* Uri_t952691556_il2cpp_TypeInfo_var;
extern const uint32_t IVector_1_IndexOf_m3867025885_MetadataUsageId;
extern const uint32_t IVector_1_SetAt_m2446451012_MetadataUsageId;
extern const uint32_t IVector_1_InsertAt_m697980859_MetadataUsageId;
extern const uint32_t IVector_1_Append_m2334270570_MetadataUsageId;
extern const uint32_t IVector_1_GetMany_m3924300725_MetadataUsageId;
extern const uint32_t IVector_1_ReplaceAll_m3525453833_MetadataUsageId;
struct IVectorView_1_t1186315951;
struct IUriRuntimeClass_t921050115;;
struct IVectorView_1_t3734068882;
struct IVectorView_1_t736132036;
struct IVectorView_1_t1516590220;
struct IVectorView_1_t1898927240;
struct IVectorView_1_t3472905354;
struct IVectorView_1_t2289811015;
struct IVectorView_1_t1891685649;
struct IVectorView_1_t1822810022;
struct IVectorView_1_t2418971426;
struct IVectorView_1_t3075432566;
struct IWwwFormUrlDecoderRuntimeClass_t497020701;
struct EventRegistrationToken_t318890788 ;
struct IVectorView_1_t3952723346;
struct IVectorView_1_t220024511;
struct TimeSpan_t881159249 ;

struct Int16U5BU5D_t3686840178;
struct Int32U5BU5D_t385246372;
struct Int64U5BU5D_t2559172825;
struct ObjectU5BU5D_t2843939325;
struct EventRegistrationTokenU5BU5D_t897718221;
struct SingleU5BU5D_t1444911251;
struct StringU5BU5D_t1281789340;
struct TimeSpanU5BU5D_t4291357516;
struct TypeU5BU5D_t3940880105;
struct UInt16U5BU5D_t3326319531;
struct UInt32U5BU5D_t2770800703;
struct UInt64U5BU5D_t1659327989;
struct UriU5BU5D_t673446605;


// Windows.Foundation.Collections.IVectorView`1<System.Single>
struct NOVTABLE IVectorView_1_t736132036 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3845931799(uint32_t ___index0, float* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m2530367390(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m481527169(float ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m996189245(uint32_t ___startIndex0, uint32_t ___items1ArraySize, float* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.String>
struct NOVTABLE IVector_1_t2588072410 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m2881185809(uint32_t ___index0, Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m3596466296(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m1219033264(IVectorView_1_t1186315951** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m2107560054(Il2CppHString ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m166369194(uint32_t ___index0, Il2CppHString ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m2973823425(uint32_t ___index0, Il2CppHString ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m269128213(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m3989127587(Il2CppHString ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m3708964671() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m867824() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m3511123964(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Il2CppHString* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m4096830123(uint32_t ___items0ArraySize, Il2CppHString* ___items0) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.UInt64>
struct NOVTABLE IVectorView_1_t3472905354 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m4005243902(uint32_t ___index0, uint64_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3900133190(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m3903182137(uint64_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3669431734(uint32_t ___startIndex0, uint32_t ___items1ArraySize, uint64_t* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Uri>
struct NOVTABLE IVector_1_t840858045 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m3184039671(uint32_t ___index0, IUriRuntimeClass_t921050115** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m1541693482(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m748183030(IVectorView_1_t3734068882** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m3867025885(IUriRuntimeClass_t921050115* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m2446451012(uint32_t ___index0, IUriRuntimeClass_t921050115* ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m697980859(uint32_t ___index0, IUriRuntimeClass_t921050115* ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m4032724530(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m2334270570(IUriRuntimeClass_t921050115* ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m2604786809() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m902403216() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m3924300725(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IUriRuntimeClass_t921050115** ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m3525453833(uint32_t ___items0ArraySize, IUriRuntimeClass_t921050115** ___items0) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Object>
struct NOVTABLE IVectorView_1_t2418971426 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m726655415(uint32_t ___index0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1102039859(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2349108669(Il2CppIInspectable* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m4226896717(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Il2CppIInspectable** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Uri>
struct NOVTABLE IVectorView_1_t3734068882 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m454402293(uint32_t ___index0, IUriRuntimeClass_t921050115** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3232372556(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m56161609(IUriRuntimeClass_t921050115* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3086367392(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IUriRuntimeClass_t921050115** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Single>
struct NOVTABLE IVector_1_t2137888495 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m3700246983(uint32_t ___index0, float* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m3531442778(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m1670428144(IVectorView_1_t736132036** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m739803125(float ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m1156666243(uint32_t ___index0, float ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m1898551949(uint32_t ___index0, float ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m3164249930(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m2356107643(float ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m2431982566() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m3622925119() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m2648716604(uint32_t ___startIndex0, uint32_t ___items1ArraySize, float* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m1968832847(uint32_t ___items0ArraySize, float* ___items0) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.UInt16>
struct NOVTABLE IVectorView_1_t1516590220 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3314156493(uint32_t ___index0, uint16_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m2270537288(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m450613661(uint16_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m619054749(uint32_t ___startIndex0, uint32_t ___items1ArraySize, uint16_t* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.UInt16>
struct NOVTABLE IVector_1_t2918346679 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m238587295(uint32_t ___index0, uint16_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m3469677531(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m3025995885(IVectorView_1_t1516590220** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m1343053975(uint16_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m246985172(uint32_t ___index0, uint16_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m2991702087(uint32_t ___index0, uint16_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m2673603791(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m1576189264(uint16_t ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m1857915307() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m3093710788() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m91550909(uint32_t ___startIndex0, uint32_t ___items1ArraySize, uint16_t* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m2701549418(uint32_t ___items0ArraySize, uint16_t* ___items0) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Type>
struct NOVTABLE IVectorView_1_t1822810022 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3708182184(uint32_t ___index0, Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1097778865(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1129849567(Type_t * ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3274208783(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Type_t ** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.UInt32>
struct NOVTABLE IVector_1_t3300683699 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m1377932999(uint32_t ___index0, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m1923345822(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m776116354(IVectorView_1_t1898927240** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m3489169491(uint32_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m3079373580(uint32_t ___index0, uint32_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m2718637520(uint32_t ___index0, uint32_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m328020143(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m2065973735(uint32_t ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m1175668767() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m3720926093() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m2743709791(uint32_t ___startIndex0, uint32_t ___items1ArraySize, uint32_t* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m3864847556(uint32_t ___items0ArraySize, uint32_t* ___items0) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.String>
struct NOVTABLE IVectorView_1_t1186315951 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3375407458(uint32_t ___index0, Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m2689776779(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2724138940(Il2CppHString ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m912540698(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Il2CppHString* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.UInt64>
struct NOVTABLE IVector_1_t579694517 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m951635711(uint32_t ___index0, uint64_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m1121146914(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m435781098(IVectorView_1_t3472905354** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m2037213150(uint64_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m2428609496(uint32_t ___index0, uint64_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m3377046151(uint32_t ___index0, uint64_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m3822942214(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m3066519795(uint64_t ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m688723122() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m3650286301() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m1446676414(uint32_t ___startIndex0, uint32_t ___items1ArraySize, uint64_t* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m544562035(uint32_t ___items0ArraySize, uint64_t* ___items0) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.UInt32>
struct NOVTABLE IVectorView_1_t1898927240 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1103191833(uint32_t ___index0, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3012050323(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2476062681(uint32_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2297801015(uint32_t ___startIndex0, uint32_t ___items1ArraySize, uint32_t* ___items1, uint32_t* comReturnValue) = 0;
};
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
// Windows.Foundation.IStringable
struct NOVTABLE IStringable_t1634798504 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IStringable_ToString_m698137009(Il2CppHString* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Int16>
struct NOVTABLE IVectorView_1_t1891685649 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1319642345(uint32_t ___index0, int16_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m2071989762(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1081404675(int16_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3136641509(uint32_t ___startIndex0, uint32_t ___items1ArraySize, int16_t* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Int32>
struct NOVTABLE IVector_1_t3691567474 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m669642145(uint32_t ___index0, int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m1083909874(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m1457083017(IVectorView_1_t2289811015** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m1513096330(int32_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m2697343022(uint32_t ___index0, int32_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m2016133864(uint32_t ___index0, int32_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m2081155343(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m71244649(int32_t ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m2906723444() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m2316468687() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m3133207996(uint32_t ___startIndex0, uint32_t ___items1ArraySize, int32_t* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m4247928211(uint32_t ___items0ArraySize, int32_t* ___items0) = 0;
};
// Windows.Foundation.IUriRuntimeClassWithAbsoluteCanonicalUri
struct NOVTABLE IUriRuntimeClassWithAbsoluteCanonicalUri_t3505007788 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClassWithAbsoluteCanonicalUri_get_AbsoluteCanonicalUri_m4057547880(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClassWithAbsoluteCanonicalUri_get_DisplayIri_m4067013853(Il2CppHString* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Int16>
struct NOVTABLE IVector_1_t3293442108 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m2480307800(uint32_t ___index0, int16_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m2555907765(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m3272889438(IVectorView_1_t1891685649** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m4004437884(int16_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m2546160938(uint32_t ___index0, int16_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m1503796612(uint32_t ___index0, int16_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m3237793706(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m73662841(int16_t ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m2323914946() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m1518645361() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m2254068249(uint32_t ___startIndex0, uint32_t ___items1ArraySize, int16_t* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m4039134861(uint32_t ___items0ArraySize, int16_t* ___items0) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Type>
struct NOVTABLE IVector_1_t3224566481 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m3023396459(uint32_t ___index0, Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m3611201515(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m2404813821(IVectorView_1_t1822810022** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m2314055355(Type_t * ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m3724063766(uint32_t ___index0, Type_t * ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m2145145338(uint32_t ___index0, Type_t * ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m154224944(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m386838768(Type_t * ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m110681880() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m1062744925() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m1218665569(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Type_t ** ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m4247109972(uint32_t ___items0ArraySize, Type_t ** ___items0) = 0;
};
// Windows.Foundation.IUriRuntimeClassFactory
struct NOVTABLE IUriRuntimeClassFactory_t1786313621 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClassFactory_CreateUri_m1347622952(Il2CppHString ___uri0, IUriRuntimeClass_t921050115** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClassFactory_CreateWithRelativeUri_m922460588(Il2CppHString ___baseUri0, Il2CppHString ___relativeUri1, IUriRuntimeClass_t921050115** comReturnValue) = 0;
};
// Windows.Foundation.IUriEscapeStatics
struct NOVTABLE IUriEscapeStatics_t1418340874 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IUriEscapeStatics_UnescapeComponent_m4034034788(Il2CppHString ___toUnescape0, Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriEscapeStatics_EscapeComponent_m2989309814(Il2CppHString ___toEscape0, Il2CppHString* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Object>
struct NOVTABLE IVector_1_t3820727885 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m219694725(uint32_t ___index0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m2561126536(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m3086397386(IVectorView_1_t2418971426** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m1930043913(Il2CppIInspectable* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m1678827239(uint32_t ___index0, Il2CppIInspectable* ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m1249203443(uint32_t ___index0, Il2CppIInspectable* ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m1710167275(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m1926269351(Il2CppIInspectable* ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m3025735428() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m305000098() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m3541038849(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Il2CppIInspectable** ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m2515130844(uint32_t ___items0ArraySize, Il2CppIInspectable** ___items0) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Int64>
struct NOVTABLE IVectorView_1_t3075432566 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m4097062415(uint32_t ___index0, int64_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3689183858(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m928127612(int64_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2435124798(uint32_t ___startIndex0, uint32_t ___items1ArraySize, int64_t* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Int64>
struct NOVTABLE IVector_1_t182221729 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m425655216(uint32_t ___index0, int64_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m3151845843(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m2857412624(IVectorView_1_t3075432566** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m211653580(int64_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m3641861081(uint32_t ___index0, int64_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m3699328408(uint32_t ___index0, int64_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m730199872(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m1860785285(int64_t ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m801618119() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m1532284886() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m3643023987(uint32_t ___startIndex0, uint32_t ___items1ArraySize, int64_t* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m3104744120(uint32_t ___items0ArraySize, int64_t* ___items0) = 0;
};
// Windows.Foundation.IUriRuntimeClass
struct NOVTABLE IUriRuntimeClass_t921050115 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_get_AbsoluteUri_m24555840(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_get_DisplayUri_m1911302195(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_get_Domain_m3775838853(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_get_Extension_m2380123(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_get_Fragment_m2866416593(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_get_Host_m781094962(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_get_Password_m478846179(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_get_Path_m3724347053(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_get_Query_m621658451(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_get_QueryParsed_m1747618489(IWwwFormUrlDecoderRuntimeClass_t497020701** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_get_RawUri_m1303737506(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_get_SchemeName_m368768956(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_get_UserName_m4126713170(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_get_Port_m3527560009(int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_get_Suspicious_m850333333(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_Equals_m846289381(IUriRuntimeClass_t921050115* ___pUri0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IUriRuntimeClass_CombineUri_m3748903678(Il2CppHString ___relativeUri0, IUriRuntimeClass_t921050115** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Int32>
struct NOVTABLE IVectorView_1_t2289811015 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1038499426(uint32_t ___index0, int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3269133634(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m711808760(int32_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m632553004(uint32_t ___startIndex0, uint32_t ___items1ArraySize, int32_t* ___items1, uint32_t* comReturnValue) = 0;
};
#ifndef IL2CPPCOMOBJECT_H
#define IL2CPPCOMOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.__Il2CppComObject

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IL2CPPCOMOBJECT_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INT16_T2552820387_H
#define INT16_T2552820387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int16
struct  Int16_t2552820387 
{
public:
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int16_t2552820387, ___m_value_0)); }
	inline int16_t get_m_value_0() const { return ___m_value_0; }
	inline int16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int16_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT16_T2552820387_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef URI_T952691556_H
#define URI_T952691556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Uri
struct  Uri_t952691556  : public Il2CppComObject
{
public:
	// Cached pointer to Windows.Foundation.IUriRuntimeClass
	IUriRuntimeClass_t921050115* ____iuriRuntimeClass_t921050115;
	// Cached pointer to Windows.Foundation.IUriRuntimeClassWithAbsoluteCanonicalUri
	IUriRuntimeClassWithAbsoluteCanonicalUri_t3505007788* ____iuriRuntimeClassWithAbsoluteCanonicalUri_t3505007788;
	// Cached pointer to Windows.Foundation.IStringable
	IStringable_t1634798504* ____istringable_t1634798504;

public:
	inline IUriRuntimeClass_t921050115* get_____iuriRuntimeClass_t921050115()
	{
		IUriRuntimeClass_t921050115* returnValue = ____iuriRuntimeClass_t921050115;
		if (returnValue == NULL)
		{
			const il2cpp_hresult_t hr = identity->QueryInterface(IUriRuntimeClass_t921050115::IID, reinterpret_cast<void**>(&returnValue));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (il2cpp_codegen_atomic_compare_exchange_pointer<IUriRuntimeClass_t921050115>((&____iuriRuntimeClass_t921050115), returnValue, NULL) != NULL)
			{
				returnValue->Release();
				returnValue = ____iuriRuntimeClass_t921050115;
			}
		}
		return returnValue;
	}

	inline IUriRuntimeClassWithAbsoluteCanonicalUri_t3505007788* get_____iuriRuntimeClassWithAbsoluteCanonicalUri_t3505007788()
	{
		IUriRuntimeClassWithAbsoluteCanonicalUri_t3505007788* returnValue = ____iuriRuntimeClassWithAbsoluteCanonicalUri_t3505007788;
		if (returnValue == NULL)
		{
			const il2cpp_hresult_t hr = identity->QueryInterface(IUriRuntimeClassWithAbsoluteCanonicalUri_t3505007788::IID, reinterpret_cast<void**>(&returnValue));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (il2cpp_codegen_atomic_compare_exchange_pointer<IUriRuntimeClassWithAbsoluteCanonicalUri_t3505007788>((&____iuriRuntimeClassWithAbsoluteCanonicalUri_t3505007788), returnValue, NULL) != NULL)
			{
				returnValue->Release();
				returnValue = ____iuriRuntimeClassWithAbsoluteCanonicalUri_t3505007788;
			}
		}
		return returnValue;
	}

	inline IStringable_t1634798504* get_____istringable_t1634798504()
	{
		IStringable_t1634798504* returnValue = ____istringable_t1634798504;
		if (returnValue == NULL)
		{
			const il2cpp_hresult_t hr = identity->QueryInterface(IStringable_t1634798504::IID, reinterpret_cast<void**>(&returnValue));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (il2cpp_codegen_atomic_compare_exchange_pointer<IStringable_t1634798504>((&____istringable_t1634798504), returnValue, NULL) != NULL)
			{
				returnValue->Release();
				returnValue = ____istringable_t1634798504;
			}
		}
		return returnValue;
	}
};

struct Uri_t952691556_StaticFields
{
public:
	// Cached pointer to IActivationFactory
	Il2CppIActivationFactory* activationFactory;
	// Cached pointer to Windows.Foundation.IUriRuntimeClassFactory
	IUriRuntimeClassFactory_t1786313621* ____iuriRuntimeClassFactory_t1786313621;
	// Cached pointer to Windows.Foundation.IUriEscapeStatics
	IUriEscapeStatics_t1418340874* ____iuriEscapeStatics_t1418340874;

public:
	inline Il2CppIActivationFactory* get_activationFactory()
	{
		Il2CppIActivationFactory* returnValue = activationFactory;
		if (returnValue == NULL)
		{
			il2cpp::utils::StringView<Il2CppNativeChar> className(IL2CPP_NATIVE_STRING("Windows.Foundation.Uri"));
			returnValue = il2cpp_codegen_windows_runtime_get_activation_factory(className);

			if (il2cpp_codegen_atomic_compare_exchange_pointer<Il2CppIActivationFactory>((&activationFactory), returnValue, NULL) != NULL)
			{
				returnValue->Release();
				returnValue = activationFactory;
			}
		}
		return returnValue;
	}

	inline IUriRuntimeClassFactory_t1786313621* get_____iuriRuntimeClassFactory_t1786313621()
	{
		IUriRuntimeClassFactory_t1786313621* returnValue = ____iuriRuntimeClassFactory_t1786313621;
		if (returnValue == NULL)
		{
			const il2cpp_hresult_t hr = get_activationFactory()->QueryInterface(IUriRuntimeClassFactory_t1786313621::IID, reinterpret_cast<void**>(&returnValue));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (il2cpp_codegen_atomic_compare_exchange_pointer<IUriRuntimeClassFactory_t1786313621>((&____iuriRuntimeClassFactory_t1786313621), returnValue, NULL) != NULL)
			{
				returnValue->Release();
				returnValue = ____iuriRuntimeClassFactory_t1786313621;
			}
		}
		return returnValue;
	}

	inline IUriEscapeStatics_t1418340874* get_____iuriEscapeStatics_t1418340874()
	{
		IUriEscapeStatics_t1418340874* returnValue = ____iuriEscapeStatics_t1418340874;
		if (returnValue == NULL)
		{
			const il2cpp_hresult_t hr = get_activationFactory()->QueryInterface(IUriEscapeStatics_t1418340874::IID, reinterpret_cast<void**>(&returnValue));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (il2cpp_codegen_atomic_compare_exchange_pointer<IUriEscapeStatics_t1418340874>((&____iuriEscapeStatics_t1418340874), returnValue, NULL) != NULL)
			{
				returnValue->Release();
				returnValue = ____iuriEscapeStatics_t1418340874;
			}
		}
		return returnValue;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_T952691556_H
#ifndef UINT16_T2177724958_H
#define UINT16_T2177724958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt16
struct  UInt16_t2177724958 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt16_t2177724958, ___m_value_0)); }
	inline uint16_t get_m_value_0() const { return ___m_value_0; }
	inline uint16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint16_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT16_T2177724958_H
#ifndef UINT64_T4134040092_H
#define UINT64_T4134040092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt64
struct  UInt64_t4134040092 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_t4134040092, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT64_T4134040092_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef EVENTREGISTRATIONTOKEN_T318890788_H
#define EVENTREGISTRATIONTOKEN_T318890788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken
struct  EventRegistrationToken_t318890788 
{
public:
	// System.UInt64 System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(EventRegistrationToken_t318890788, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTREGISTRATIONTOKEN_T318890788_H
// Windows.Foundation.Collections.IVectorView`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct NOVTABLE IVectorView_1_t3952723346 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1574166267(uint32_t ___index0, EventRegistrationToken_t318890788 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m828705928(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m523834847(EventRegistrationToken_t318890788  ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m2455943328(uint32_t ___startIndex0, uint32_t ___items1ArraySize, EventRegistrationToken_t318890788 * ___items1, uint32_t* comReturnValue) = 0;
};
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_19)); }
	inline TimeSpan_t881159249  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_t881159249 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_t881159249  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_t881159249  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_t881159249  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_21)); }
	inline TimeSpan_t881159249  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_t881159249  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef URIIDNSCOPE_T1847433844_H
#define URIIDNSCOPE_T1847433844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriIdnScope
struct  UriIdnScope_t1847433844 
{
public:
	// System.Int32 System.UriIdnScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriIdnScope_t1847433844, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIIDNSCOPE_T1847433844_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
// Windows.Foundation.Collections.IVector`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>
struct NOVTABLE IVector_1_t1059512509 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m100662259(uint32_t ___index0, EventRegistrationToken_t318890788 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m621277326(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m3332817310(IVectorView_1_t3952723346** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m1226224308(EventRegistrationToken_t318890788  ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m676705480(uint32_t ___index0, EventRegistrationToken_t318890788  ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m2214717186(uint32_t ___index0, EventRegistrationToken_t318890788  ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m1750123518(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m3164318064(EventRegistrationToken_t318890788  ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m2693486160() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m3309901166() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m3542174024(uint32_t ___startIndex0, uint32_t ___items1ArraySize, EventRegistrationToken_t318890788 * ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m239273076(uint32_t ___items0ArraySize, EventRegistrationToken_t318890788 * ___items0) = 0;
};
#ifndef FLAGS_T2372798318_H
#define FLAGS_T2372798318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri/Flags
struct  Flags_t2372798318 
{
public:
	// System.UInt64 System.Uri/Flags::value__
	uint64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_t2372798318, ___value___2)); }
	inline uint64_t get_value___2() const { return ___value___2; }
	inline uint64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint64_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLAGS_T2372798318_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_0;

public:
	inline static int32_t get_offset_of__impl_0() { return static_cast<int32_t>(offsetof(Type_t, ____impl_0)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_0() const { return ____impl_0; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_0() { return &____impl_0; }
	inline void set__impl_0(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_0 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_1;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_2;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_3;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_4;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_5;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_6;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2999457153 * ___defaultBinder_7;

public:
	inline static int32_t get_offset_of_FilterAttribute_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_1)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_1() const { return ___FilterAttribute_1; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_1() { return &___FilterAttribute_1; }
	inline void set_FilterAttribute_1(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_1), value);
	}

	inline static int32_t get_offset_of_FilterName_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_2)); }
	inline MemberFilter_t426314064 * get_FilterName_2() const { return ___FilterName_2; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_2() { return &___FilterName_2; }
	inline void set_FilterName_2(MemberFilter_t426314064 * value)
	{
		___FilterName_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_2), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_3)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_3() const { return ___FilterNameIgnoreCase_3; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_3() { return &___FilterNameIgnoreCase_3; }
	inline void set_FilterNameIgnoreCase_3(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_3 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_3), value);
	}

	inline static int32_t get_offset_of_Missing_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_4)); }
	inline RuntimeObject * get_Missing_4() const { return ___Missing_4; }
	inline RuntimeObject ** get_address_of_Missing_4() { return &___Missing_4; }
	inline void set_Missing_4(RuntimeObject * value)
	{
		___Missing_4 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_4), value);
	}

	inline static int32_t get_offset_of_Delimiter_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_5)); }
	inline Il2CppChar get_Delimiter_5() const { return ___Delimiter_5; }
	inline Il2CppChar* get_address_of_Delimiter_5() { return &___Delimiter_5; }
	inline void set_Delimiter_5(Il2CppChar value)
	{
		___Delimiter_5 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_6)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_6() const { return ___EmptyTypes_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_6() { return &___EmptyTypes_6; }
	inline void set_EmptyTypes_6(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_6 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_6), value);
	}

	inline static int32_t get_offset_of_defaultBinder_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_7)); }
	inline Binder_t2999457153 * get_defaultBinder_7() const { return ___defaultBinder_7; }
	inline Binder_t2999457153 ** get_address_of_defaultBinder_7() { return &___defaultBinder_7; }
	inline void set_defaultBinder_7(Binder_t2999457153 * value)
	{
		___defaultBinder_7 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef URI_T100236324_H
#define URI_T100236324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri
struct  Uri_t100236324  : public RuntimeObject
{
public:
	// System.String System.Uri::m_String
	String_t* ___m_String_13;
	// System.String System.Uri::m_originalUnicodeString
	String_t* ___m_originalUnicodeString_14;
	// System.UriParser System.Uri::m_Syntax
	UriParser_t3890150400 * ___m_Syntax_15;
	// System.String System.Uri::m_DnsSafeHost
	String_t* ___m_DnsSafeHost_16;
	// System.Uri/Flags System.Uri::m_Flags
	uint64_t ___m_Flags_17;
	// System.Uri/UriInfo System.Uri::m_Info
	UriInfo_t1092684687 * ___m_Info_18;
	// System.Boolean System.Uri::m_iriParsing
	bool ___m_iriParsing_19;

public:
	inline static int32_t get_offset_of_m_String_13() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_String_13)); }
	inline String_t* get_m_String_13() const { return ___m_String_13; }
	inline String_t** get_address_of_m_String_13() { return &___m_String_13; }
	inline void set_m_String_13(String_t* value)
	{
		___m_String_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_String_13), value);
	}

	inline static int32_t get_offset_of_m_originalUnicodeString_14() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_originalUnicodeString_14)); }
	inline String_t* get_m_originalUnicodeString_14() const { return ___m_originalUnicodeString_14; }
	inline String_t** get_address_of_m_originalUnicodeString_14() { return &___m_originalUnicodeString_14; }
	inline void set_m_originalUnicodeString_14(String_t* value)
	{
		___m_originalUnicodeString_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_originalUnicodeString_14), value);
	}

	inline static int32_t get_offset_of_m_Syntax_15() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_Syntax_15)); }
	inline UriParser_t3890150400 * get_m_Syntax_15() const { return ___m_Syntax_15; }
	inline UriParser_t3890150400 ** get_address_of_m_Syntax_15() { return &___m_Syntax_15; }
	inline void set_m_Syntax_15(UriParser_t3890150400 * value)
	{
		___m_Syntax_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Syntax_15), value);
	}

	inline static int32_t get_offset_of_m_DnsSafeHost_16() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_DnsSafeHost_16)); }
	inline String_t* get_m_DnsSafeHost_16() const { return ___m_DnsSafeHost_16; }
	inline String_t** get_address_of_m_DnsSafeHost_16() { return &___m_DnsSafeHost_16; }
	inline void set_m_DnsSafeHost_16(String_t* value)
	{
		___m_DnsSafeHost_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_DnsSafeHost_16), value);
	}

	inline static int32_t get_offset_of_m_Flags_17() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_Flags_17)); }
	inline uint64_t get_m_Flags_17() const { return ___m_Flags_17; }
	inline uint64_t* get_address_of_m_Flags_17() { return &___m_Flags_17; }
	inline void set_m_Flags_17(uint64_t value)
	{
		___m_Flags_17 = value;
	}

	inline static int32_t get_offset_of_m_Info_18() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_Info_18)); }
	inline UriInfo_t1092684687 * get_m_Info_18() const { return ___m_Info_18; }
	inline UriInfo_t1092684687 ** get_address_of_m_Info_18() { return &___m_Info_18; }
	inline void set_m_Info_18(UriInfo_t1092684687 * value)
	{
		___m_Info_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Info_18), value);
	}

	inline static int32_t get_offset_of_m_iriParsing_19() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_iriParsing_19)); }
	inline bool get_m_iriParsing_19() const { return ___m_iriParsing_19; }
	inline bool* get_address_of_m_iriParsing_19() { return &___m_iriParsing_19; }
	inline void set_m_iriParsing_19(bool value)
	{
		___m_iriParsing_19 = value;
	}
};

struct Uri_t100236324_StaticFields
{
public:
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_0;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_1;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_2;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_3;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_4;
	// System.String System.Uri::UriSchemeWs
	String_t* ___UriSchemeWs_5;
	// System.String System.Uri::UriSchemeWss
	String_t* ___UriSchemeWss_6;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_7;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_8;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_9;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_10;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_11;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_12;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitialized
	bool ___s_ConfigInitialized_20;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitializing
	bool ___s_ConfigInitializing_21;
	// System.UriIdnScope modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IdnScope
	int32_t ___s_IdnScope_22;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IriParsing
	bool ___s_IriParsing_23;
	// System.Boolean System.Uri::useDotNetRelativeOrAbsolute
	bool ___useDotNetRelativeOrAbsolute_24;
	// System.Boolean System.Uri::IsWindowsFileSystem
	bool ___IsWindowsFileSystem_25;
	// System.Object System.Uri::s_initLock
	RuntimeObject * ___s_initLock_26;
	// System.Char[] System.Uri::HexLowerChars
	CharU5BU5D_t3528271667* ___HexLowerChars_27;
	// System.Char[] System.Uri::_WSchars
	CharU5BU5D_t3528271667* ____WSchars_28;

public:
	inline static int32_t get_offset_of_UriSchemeFile_0() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeFile_0)); }
	inline String_t* get_UriSchemeFile_0() const { return ___UriSchemeFile_0; }
	inline String_t** get_address_of_UriSchemeFile_0() { return &___UriSchemeFile_0; }
	inline void set_UriSchemeFile_0(String_t* value)
	{
		___UriSchemeFile_0 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFile_0), value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_1() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeFtp_1)); }
	inline String_t* get_UriSchemeFtp_1() const { return ___UriSchemeFtp_1; }
	inline String_t** get_address_of_UriSchemeFtp_1() { return &___UriSchemeFtp_1; }
	inline void set_UriSchemeFtp_1(String_t* value)
	{
		___UriSchemeFtp_1 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFtp_1), value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_2() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeGopher_2)); }
	inline String_t* get_UriSchemeGopher_2() const { return ___UriSchemeGopher_2; }
	inline String_t** get_address_of_UriSchemeGopher_2() { return &___UriSchemeGopher_2; }
	inline void set_UriSchemeGopher_2(String_t* value)
	{
		___UriSchemeGopher_2 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeGopher_2), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_3() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeHttp_3)); }
	inline String_t* get_UriSchemeHttp_3() const { return ___UriSchemeHttp_3; }
	inline String_t** get_address_of_UriSchemeHttp_3() { return &___UriSchemeHttp_3; }
	inline void set_UriSchemeHttp_3(String_t* value)
	{
		___UriSchemeHttp_3 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttp_3), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_4() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeHttps_4)); }
	inline String_t* get_UriSchemeHttps_4() const { return ___UriSchemeHttps_4; }
	inline String_t** get_address_of_UriSchemeHttps_4() { return &___UriSchemeHttps_4; }
	inline void set_UriSchemeHttps_4(String_t* value)
	{
		___UriSchemeHttps_4 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttps_4), value);
	}

	inline static int32_t get_offset_of_UriSchemeWs_5() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeWs_5)); }
	inline String_t* get_UriSchemeWs_5() const { return ___UriSchemeWs_5; }
	inline String_t** get_address_of_UriSchemeWs_5() { return &___UriSchemeWs_5; }
	inline void set_UriSchemeWs_5(String_t* value)
	{
		___UriSchemeWs_5 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeWs_5), value);
	}

	inline static int32_t get_offset_of_UriSchemeWss_6() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeWss_6)); }
	inline String_t* get_UriSchemeWss_6() const { return ___UriSchemeWss_6; }
	inline String_t** get_address_of_UriSchemeWss_6() { return &___UriSchemeWss_6; }
	inline void set_UriSchemeWss_6(String_t* value)
	{
		___UriSchemeWss_6 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeWss_6), value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_7() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeMailto_7)); }
	inline String_t* get_UriSchemeMailto_7() const { return ___UriSchemeMailto_7; }
	inline String_t** get_address_of_UriSchemeMailto_7() { return &___UriSchemeMailto_7; }
	inline void set_UriSchemeMailto_7(String_t* value)
	{
		___UriSchemeMailto_7 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeMailto_7), value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_8() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNews_8)); }
	inline String_t* get_UriSchemeNews_8() const { return ___UriSchemeNews_8; }
	inline String_t** get_address_of_UriSchemeNews_8() { return &___UriSchemeNews_8; }
	inline void set_UriSchemeNews_8(String_t* value)
	{
		___UriSchemeNews_8 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNews_8), value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_9() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNntp_9)); }
	inline String_t* get_UriSchemeNntp_9() const { return ___UriSchemeNntp_9; }
	inline String_t** get_address_of_UriSchemeNntp_9() { return &___UriSchemeNntp_9; }
	inline void set_UriSchemeNntp_9(String_t* value)
	{
		___UriSchemeNntp_9 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNntp_9), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_10() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNetTcp_10)); }
	inline String_t* get_UriSchemeNetTcp_10() const { return ___UriSchemeNetTcp_10; }
	inline String_t** get_address_of_UriSchemeNetTcp_10() { return &___UriSchemeNetTcp_10; }
	inline void set_UriSchemeNetTcp_10(String_t* value)
	{
		___UriSchemeNetTcp_10 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetTcp_10), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_11() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNetPipe_11)); }
	inline String_t* get_UriSchemeNetPipe_11() const { return ___UriSchemeNetPipe_11; }
	inline String_t** get_address_of_UriSchemeNetPipe_11() { return &___UriSchemeNetPipe_11; }
	inline void set_UriSchemeNetPipe_11(String_t* value)
	{
		___UriSchemeNetPipe_11 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetPipe_11), value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_12() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___SchemeDelimiter_12)); }
	inline String_t* get_SchemeDelimiter_12() const { return ___SchemeDelimiter_12; }
	inline String_t** get_address_of_SchemeDelimiter_12() { return &___SchemeDelimiter_12; }
	inline void set_SchemeDelimiter_12(String_t* value)
	{
		___SchemeDelimiter_12 = value;
		Il2CppCodeGenWriteBarrier((&___SchemeDelimiter_12), value);
	}

	inline static int32_t get_offset_of_s_ConfigInitialized_20() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_ConfigInitialized_20)); }
	inline bool get_s_ConfigInitialized_20() const { return ___s_ConfigInitialized_20; }
	inline bool* get_address_of_s_ConfigInitialized_20() { return &___s_ConfigInitialized_20; }
	inline void set_s_ConfigInitialized_20(bool value)
	{
		___s_ConfigInitialized_20 = value;
	}

	inline static int32_t get_offset_of_s_ConfigInitializing_21() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_ConfigInitializing_21)); }
	inline bool get_s_ConfigInitializing_21() const { return ___s_ConfigInitializing_21; }
	inline bool* get_address_of_s_ConfigInitializing_21() { return &___s_ConfigInitializing_21; }
	inline void set_s_ConfigInitializing_21(bool value)
	{
		___s_ConfigInitializing_21 = value;
	}

	inline static int32_t get_offset_of_s_IdnScope_22() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_IdnScope_22)); }
	inline int32_t get_s_IdnScope_22() const { return ___s_IdnScope_22; }
	inline int32_t* get_address_of_s_IdnScope_22() { return &___s_IdnScope_22; }
	inline void set_s_IdnScope_22(int32_t value)
	{
		___s_IdnScope_22 = value;
	}

	inline static int32_t get_offset_of_s_IriParsing_23() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_IriParsing_23)); }
	inline bool get_s_IriParsing_23() const { return ___s_IriParsing_23; }
	inline bool* get_address_of_s_IriParsing_23() { return &___s_IriParsing_23; }
	inline void set_s_IriParsing_23(bool value)
	{
		___s_IriParsing_23 = value;
	}

	inline static int32_t get_offset_of_useDotNetRelativeOrAbsolute_24() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___useDotNetRelativeOrAbsolute_24)); }
	inline bool get_useDotNetRelativeOrAbsolute_24() const { return ___useDotNetRelativeOrAbsolute_24; }
	inline bool* get_address_of_useDotNetRelativeOrAbsolute_24() { return &___useDotNetRelativeOrAbsolute_24; }
	inline void set_useDotNetRelativeOrAbsolute_24(bool value)
	{
		___useDotNetRelativeOrAbsolute_24 = value;
	}

	inline static int32_t get_offset_of_IsWindowsFileSystem_25() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___IsWindowsFileSystem_25)); }
	inline bool get_IsWindowsFileSystem_25() const { return ___IsWindowsFileSystem_25; }
	inline bool* get_address_of_IsWindowsFileSystem_25() { return &___IsWindowsFileSystem_25; }
	inline void set_IsWindowsFileSystem_25(bool value)
	{
		___IsWindowsFileSystem_25 = value;
	}

	inline static int32_t get_offset_of_s_initLock_26() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_initLock_26)); }
	inline RuntimeObject * get_s_initLock_26() const { return ___s_initLock_26; }
	inline RuntimeObject ** get_address_of_s_initLock_26() { return &___s_initLock_26; }
	inline void set_s_initLock_26(RuntimeObject * value)
	{
		___s_initLock_26 = value;
		Il2CppCodeGenWriteBarrier((&___s_initLock_26), value);
	}

	inline static int32_t get_offset_of_HexLowerChars_27() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___HexLowerChars_27)); }
	inline CharU5BU5D_t3528271667* get_HexLowerChars_27() const { return ___HexLowerChars_27; }
	inline CharU5BU5D_t3528271667** get_address_of_HexLowerChars_27() { return &___HexLowerChars_27; }
	inline void set_HexLowerChars_27(CharU5BU5D_t3528271667* value)
	{
		___HexLowerChars_27 = value;
		Il2CppCodeGenWriteBarrier((&___HexLowerChars_27), value);
	}

	inline static int32_t get_offset_of__WSchars_28() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ____WSchars_28)); }
	inline CharU5BU5D_t3528271667* get__WSchars_28() const { return ____WSchars_28; }
	inline CharU5BU5D_t3528271667** get_address_of__WSchars_28() { return &____WSchars_28; }
	inline void set__WSchars_28(CharU5BU5D_t3528271667* value)
	{
		____WSchars_28 = value;
		Il2CppCodeGenWriteBarrier((&____WSchars_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_T100236324_H
// Windows.Foundation.Collections.IVector`1<System.TimeSpan>
struct NOVTABLE IVector_1_t1621780970 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m4001576701(uint32_t ___index0, TimeSpan_t881159249 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m1979953161(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m2578442391(IVectorView_1_t220024511** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m3610534390(TimeSpan_t881159249  ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m3169382320(uint32_t ___index0, TimeSpan_t881159249  ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m2337166544(uint32_t ___index0, TimeSpan_t881159249  ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m3743278496(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m250096651(TimeSpan_t881159249  ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m4140907611() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m2231523500() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m3395665562(uint32_t ___startIndex0, uint32_t ___items1ArraySize, TimeSpan_t881159249 * ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m2447609914(uint32_t ___items0ArraySize, TimeSpan_t881159249 * ___items0) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.TimeSpan>
struct NOVTABLE IVectorView_1_t220024511 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1798264202(uint32_t ___index0, TimeSpan_t881159249 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m846008238(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2157973748(TimeSpan_t881159249  ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3909526617(uint32_t ___startIndex0, uint32_t ___items1ArraySize, TimeSpan_t881159249 * ___items1, uint32_t* comReturnValue) = 0;
};
// System.Int16[]
struct Int16U5BU5D_t3686840178  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int16_t m_Items[1];

public:
	inline int16_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int16_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int16_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int16_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int16_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int16_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t385246372  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int64[]
struct Int64U5BU5D_t2559172825  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int64_t m_Items[1];

public:
	inline int64_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int64_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int64_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int64_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int64_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int64_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken[]
struct EventRegistrationTokenU5BU5D_t897718221  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) EventRegistrationToken_t318890788  m_Items[1];

public:
	inline EventRegistrationToken_t318890788  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline EventRegistrationToken_t318890788 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, EventRegistrationToken_t318890788  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline EventRegistrationToken_t318890788  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline EventRegistrationToken_t318890788 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, EventRegistrationToken_t318890788  value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t1444911251  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.TimeSpan[]
struct TimeSpanU5BU5D_t4291357516  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) TimeSpan_t881159249  m_Items[1];

public:
	inline TimeSpan_t881159249  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TimeSpan_t881159249 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TimeSpan_t881159249  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline TimeSpan_t881159249  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TimeSpan_t881159249 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TimeSpan_t881159249  value)
	{
		m_Items[index] = value;
	}
};
// System.Type[]
struct TypeU5BU5D_t3940880105  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.UInt16[]
struct UInt16U5BU5D_t3326319531  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint16_t m_Items[1];

public:
	inline uint16_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint16_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint16_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint16_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint16_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint16_t value)
	{
		m_Items[index] = value;
	}
};
// System.UInt32[]
struct UInt32U5BU5D_t2770800703  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint32_t m_Items[1];

public:
	inline uint32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint32_t value)
	{
		m_Items[index] = value;
	}
};
// System.UInt64[]
struct UInt64U5BU5D_t1659327989  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint64_t m_Items[1];

public:
	inline uint64_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint64_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint64_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint64_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint64_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint64_t value)
	{
		m_Items[index] = value;
	}
};
// System.Uri[]
struct UriU5BU5D_t673446605  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Uri_t100236324 * m_Items[1];

public:
	inline Uri_t100236324 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Uri_t100236324 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Uri_t100236324 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Uri_t100236324 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Uri_t100236324 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Uri_t100236324 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};



// System.Void System.Uri::.ctor(System.String)
extern "C"  void Uri__ctor_m1474792120 (Uri_t100236324 * __this, String_t* ___uriString0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Uri::get_OriginalString()
extern "C"  String_t* Uri_get_OriginalString_m2551181575 (Uri_t100236324 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// T Windows.Foundation.Collections.IVector`1<System.Int16>::GetAt(System.UInt32)
extern "C"  int16_t IVector_1_GetAt_m2480307800 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t3293442108* ____ivector_1_t3293442108 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3293442108::IID, reinterpret_cast<void**>(&____ivector_1_t3293442108));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	int16_t returnValue = 0;
	hr = ____ivector_1_t3293442108->IVector_1_GetAt_m2480307800(___index0, &returnValue);
	____ivector_1_t3293442108->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Int16>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m2555907765 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t3293442108* ____ivector_1_t3293442108 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3293442108::IID, reinterpret_cast<void**>(&____ivector_1_t3293442108));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t3293442108->IVector_1_get_Size_m2555907765(&returnValue);
	____ivector_1_t3293442108->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.Int16>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m3272889438 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m3272889438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t3293442108* ____ivector_1_t3293442108 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3293442108::IID, reinterpret_cast<void**>(&____ivector_1_t3293442108));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t1891685649* returnValue = NULL;
	hr = ____ivector_1_t3293442108->IVector_1_GetView_m3272889438(&returnValue);
	____ivector_1_t3293442108->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.Int16>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m4004437884 (RuntimeObject* __this, int16_t ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVector_1_t3293442108* ____ivector_1_t3293442108 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3293442108::IID, reinterpret_cast<void**>(&____ivector_1_t3293442108));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t3293442108->IVector_1_IndexOf_m4004437884(___value0, (&____index1_empty), &returnValue);
	____ivector_1_t3293442108->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int16>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m2546160938 (RuntimeObject* __this, uint32_t ___index0, int16_t ___value1, const RuntimeMethod* method)
{
	IVector_1_t3293442108* ____ivector_1_t3293442108 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3293442108::IID, reinterpret_cast<void**>(&____ivector_1_t3293442108));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3293442108->IVector_1_SetAt_m2546160938(___index0, ___value1);
	____ivector_1_t3293442108->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int16>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m1503796612 (RuntimeObject* __this, uint32_t ___index0, int16_t ___value1, const RuntimeMethod* method)
{
	IVector_1_t3293442108* ____ivector_1_t3293442108 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3293442108::IID, reinterpret_cast<void**>(&____ivector_1_t3293442108));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3293442108->IVector_1_InsertAt_m1503796612(___index0, ___value1);
	____ivector_1_t3293442108->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int16>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m3237793706 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t3293442108* ____ivector_1_t3293442108 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3293442108::IID, reinterpret_cast<void**>(&____ivector_1_t3293442108));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3293442108->IVector_1_RemoveAt_m3237793706(___index0);
	____ivector_1_t3293442108->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int16>::Append(T)
extern "C"  void IVector_1_Append_m73662841 (RuntimeObject* __this, int16_t ___value0, const RuntimeMethod* method)
{
	IVector_1_t3293442108* ____ivector_1_t3293442108 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3293442108::IID, reinterpret_cast<void**>(&____ivector_1_t3293442108));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3293442108->IVector_1_Append_m73662841(___value0);
	____ivector_1_t3293442108->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int16>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m2323914946 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t3293442108* ____ivector_1_t3293442108 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3293442108::IID, reinterpret_cast<void**>(&____ivector_1_t3293442108));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3293442108->IVector_1_RemoveAtEnd_m2323914946();
	____ivector_1_t3293442108->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int16>::Clear()
extern "C"  void IVector_1_Clear_m1518645361 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t3293442108* ____ivector_1_t3293442108 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3293442108::IID, reinterpret_cast<void**>(&____ivector_1_t3293442108));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3293442108->IVector_1_Clear_m1518645361();
	____ivector_1_t3293442108->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Int16>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m2254068249 (RuntimeObject* __this, uint32_t ___startIndex0, Int16U5BU5D_t3686840178* ___items1, const RuntimeMethod* method)
{
	IVector_1_t3293442108* ____ivector_1_t3293442108 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3293442108::IID, reinterpret_cast<void**>(&____ivector_1_t3293442108));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	int16_t* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<int16_t>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(int16_t));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t3293442108->IVector_1_GetMany_m2254068249(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivector_1_t3293442108->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int16>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m4039134861 (RuntimeObject* __this, Int16U5BU5D_t3686840178* ___items0, const RuntimeMethod* method)
{
	IVector_1_t3293442108* ____ivector_1_t3293442108 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3293442108::IID, reinterpret_cast<void**>(&____ivector_1_t3293442108));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	int16_t* ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = reinterpret_cast<int16_t*>((___items0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	hr = ____ivector_1_t3293442108->IVector_1_ReplaceAll_m4039134861(____items0_marshaledArraySize, ____items0_marshaled);
	____ivector_1_t3293442108->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// T Windows.Foundation.Collections.IVector`1<System.Int32>::GetAt(System.UInt32)
extern "C"  int32_t IVector_1_GetAt_m669642145 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t3691567474* ____ivector_1_t3691567474 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3691567474::IID, reinterpret_cast<void**>(&____ivector_1_t3691567474));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	int32_t returnValue = 0;
	hr = ____ivector_1_t3691567474->IVector_1_GetAt_m669642145(___index0, &returnValue);
	____ivector_1_t3691567474->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Int32>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m1083909874 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t3691567474* ____ivector_1_t3691567474 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3691567474::IID, reinterpret_cast<void**>(&____ivector_1_t3691567474));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t3691567474->IVector_1_get_Size_m1083909874(&returnValue);
	____ivector_1_t3691567474->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.Int32>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m1457083017 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m1457083017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t3691567474* ____ivector_1_t3691567474 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3691567474::IID, reinterpret_cast<void**>(&____ivector_1_t3691567474));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t2289811015* returnValue = NULL;
	hr = ____ivector_1_t3691567474->IVector_1_GetView_m1457083017(&returnValue);
	____ivector_1_t3691567474->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.Int32>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m1513096330 (RuntimeObject* __this, int32_t ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVector_1_t3691567474* ____ivector_1_t3691567474 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3691567474::IID, reinterpret_cast<void**>(&____ivector_1_t3691567474));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t3691567474->IVector_1_IndexOf_m1513096330(___value0, (&____index1_empty), &returnValue);
	____ivector_1_t3691567474->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int32>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m2697343022 (RuntimeObject* __this, uint32_t ___index0, int32_t ___value1, const RuntimeMethod* method)
{
	IVector_1_t3691567474* ____ivector_1_t3691567474 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3691567474::IID, reinterpret_cast<void**>(&____ivector_1_t3691567474));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3691567474->IVector_1_SetAt_m2697343022(___index0, ___value1);
	____ivector_1_t3691567474->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int32>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m2016133864 (RuntimeObject* __this, uint32_t ___index0, int32_t ___value1, const RuntimeMethod* method)
{
	IVector_1_t3691567474* ____ivector_1_t3691567474 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3691567474::IID, reinterpret_cast<void**>(&____ivector_1_t3691567474));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3691567474->IVector_1_InsertAt_m2016133864(___index0, ___value1);
	____ivector_1_t3691567474->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int32>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m2081155343 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t3691567474* ____ivector_1_t3691567474 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3691567474::IID, reinterpret_cast<void**>(&____ivector_1_t3691567474));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3691567474->IVector_1_RemoveAt_m2081155343(___index0);
	____ivector_1_t3691567474->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int32>::Append(T)
extern "C"  void IVector_1_Append_m71244649 (RuntimeObject* __this, int32_t ___value0, const RuntimeMethod* method)
{
	IVector_1_t3691567474* ____ivector_1_t3691567474 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3691567474::IID, reinterpret_cast<void**>(&____ivector_1_t3691567474));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3691567474->IVector_1_Append_m71244649(___value0);
	____ivector_1_t3691567474->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int32>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m2906723444 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t3691567474* ____ivector_1_t3691567474 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3691567474::IID, reinterpret_cast<void**>(&____ivector_1_t3691567474));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3691567474->IVector_1_RemoveAtEnd_m2906723444();
	____ivector_1_t3691567474->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int32>::Clear()
extern "C"  void IVector_1_Clear_m2316468687 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t3691567474* ____ivector_1_t3691567474 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3691567474::IID, reinterpret_cast<void**>(&____ivector_1_t3691567474));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3691567474->IVector_1_Clear_m2316468687();
	____ivector_1_t3691567474->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Int32>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m3133207996 (RuntimeObject* __this, uint32_t ___startIndex0, Int32U5BU5D_t385246372* ___items1, const RuntimeMethod* method)
{
	IVector_1_t3691567474* ____ivector_1_t3691567474 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3691567474::IID, reinterpret_cast<void**>(&____ivector_1_t3691567474));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	int32_t* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<int32_t>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(int32_t));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t3691567474->IVector_1_GetMany_m3133207996(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivector_1_t3691567474->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int32>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m4247928211 (RuntimeObject* __this, Int32U5BU5D_t385246372* ___items0, const RuntimeMethod* method)
{
	IVector_1_t3691567474* ____ivector_1_t3691567474 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3691567474::IID, reinterpret_cast<void**>(&____ivector_1_t3691567474));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	int32_t* ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = reinterpret_cast<int32_t*>((___items0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	hr = ____ivector_1_t3691567474->IVector_1_ReplaceAll_m4247928211(____items0_marshaledArraySize, ____items0_marshaled);
	____ivector_1_t3691567474->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// T Windows.Foundation.Collections.IVector`1<System.Int64>::GetAt(System.UInt32)
extern "C"  int64_t IVector_1_GetAt_m425655216 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t182221729* ____ivector_1_t182221729 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t182221729::IID, reinterpret_cast<void**>(&____ivector_1_t182221729));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	int64_t returnValue = 0;
	hr = ____ivector_1_t182221729->IVector_1_GetAt_m425655216(___index0, &returnValue);
	____ivector_1_t182221729->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Int64>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m3151845843 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t182221729* ____ivector_1_t182221729 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t182221729::IID, reinterpret_cast<void**>(&____ivector_1_t182221729));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t182221729->IVector_1_get_Size_m3151845843(&returnValue);
	____ivector_1_t182221729->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.Int64>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m2857412624 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m2857412624_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t182221729* ____ivector_1_t182221729 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t182221729::IID, reinterpret_cast<void**>(&____ivector_1_t182221729));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t3075432566* returnValue = NULL;
	hr = ____ivector_1_t182221729->IVector_1_GetView_m2857412624(&returnValue);
	____ivector_1_t182221729->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.Int64>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m211653580 (RuntimeObject* __this, int64_t ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVector_1_t182221729* ____ivector_1_t182221729 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t182221729::IID, reinterpret_cast<void**>(&____ivector_1_t182221729));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t182221729->IVector_1_IndexOf_m211653580(___value0, (&____index1_empty), &returnValue);
	____ivector_1_t182221729->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int64>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m3641861081 (RuntimeObject* __this, uint32_t ___index0, int64_t ___value1, const RuntimeMethod* method)
{
	IVector_1_t182221729* ____ivector_1_t182221729 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t182221729::IID, reinterpret_cast<void**>(&____ivector_1_t182221729));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t182221729->IVector_1_SetAt_m3641861081(___index0, ___value1);
	____ivector_1_t182221729->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int64>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m3699328408 (RuntimeObject* __this, uint32_t ___index0, int64_t ___value1, const RuntimeMethod* method)
{
	IVector_1_t182221729* ____ivector_1_t182221729 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t182221729::IID, reinterpret_cast<void**>(&____ivector_1_t182221729));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t182221729->IVector_1_InsertAt_m3699328408(___index0, ___value1);
	____ivector_1_t182221729->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int64>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m730199872 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t182221729* ____ivector_1_t182221729 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t182221729::IID, reinterpret_cast<void**>(&____ivector_1_t182221729));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t182221729->IVector_1_RemoveAt_m730199872(___index0);
	____ivector_1_t182221729->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int64>::Append(T)
extern "C"  void IVector_1_Append_m1860785285 (RuntimeObject* __this, int64_t ___value0, const RuntimeMethod* method)
{
	IVector_1_t182221729* ____ivector_1_t182221729 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t182221729::IID, reinterpret_cast<void**>(&____ivector_1_t182221729));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t182221729->IVector_1_Append_m1860785285(___value0);
	____ivector_1_t182221729->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int64>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m801618119 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t182221729* ____ivector_1_t182221729 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t182221729::IID, reinterpret_cast<void**>(&____ivector_1_t182221729));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t182221729->IVector_1_RemoveAtEnd_m801618119();
	____ivector_1_t182221729->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int64>::Clear()
extern "C"  void IVector_1_Clear_m1532284886 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t182221729* ____ivector_1_t182221729 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t182221729::IID, reinterpret_cast<void**>(&____ivector_1_t182221729));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t182221729->IVector_1_Clear_m1532284886();
	____ivector_1_t182221729->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Int64>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m3643023987 (RuntimeObject* __this, uint32_t ___startIndex0, Int64U5BU5D_t2559172825* ___items1, const RuntimeMethod* method)
{
	IVector_1_t182221729* ____ivector_1_t182221729 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t182221729::IID, reinterpret_cast<void**>(&____ivector_1_t182221729));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	int64_t* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<int64_t>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(int64_t));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t182221729->IVector_1_GetMany_m3643023987(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivector_1_t182221729->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Int64>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m3104744120 (RuntimeObject* __this, Int64U5BU5D_t2559172825* ___items0, const RuntimeMethod* method)
{
	IVector_1_t182221729* ____ivector_1_t182221729 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t182221729::IID, reinterpret_cast<void**>(&____ivector_1_t182221729));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	int64_t* ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = reinterpret_cast<int64_t*>((___items0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	hr = ____ivector_1_t182221729->IVector_1_ReplaceAll_m3104744120(____items0_marshaledArraySize, ____items0_marshaled);
	____ivector_1_t182221729->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// T Windows.Foundation.Collections.IVector`1<System.Object>::GetAt(System.UInt32)
extern "C"  RuntimeObject * IVector_1_GetAt_m219694725 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetAt_m219694725_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t3820727885* ____ivector_1_t3820727885 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3820727885::IID, reinterpret_cast<void**>(&____ivector_1_t3820727885));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____ivector_1_t3820727885->IVector_1_GetAt_m219694725(___index0, &returnValue);
	____ivector_1_t3820727885->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Object>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m2561126536 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t3820727885* ____ivector_1_t3820727885 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3820727885::IID, reinterpret_cast<void**>(&____ivector_1_t3820727885));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t3820727885->IVector_1_get_Size_m2561126536(&returnValue);
	____ivector_1_t3820727885->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.Object>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m3086397386 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m3086397386_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t3820727885* ____ivector_1_t3820727885 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3820727885::IID, reinterpret_cast<void**>(&____ivector_1_t3820727885));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t2418971426* returnValue = NULL;
	hr = ____ivector_1_t3820727885->IVector_1_GetView_m3086397386(&returnValue);
	____ivector_1_t3820727885->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.Object>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m1930043913 (RuntimeObject* __this, RuntimeObject * ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVector_1_t3820727885* ____ivector_1_t3820727885 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3820727885::IID, reinterpret_cast<void**>(&____ivector_1_t3820727885));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	Il2CppIInspectable* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t3820727885->IVector_1_IndexOf_m1930043913(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivector_1_t3820727885->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Object>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m1678827239 (RuntimeObject* __this, uint32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	IVector_1_t3820727885* ____ivector_1_t3820727885 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3820727885::IID, reinterpret_cast<void**>(&____ivector_1_t3820727885));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value1' to native representation
	Il2CppIInspectable* ____value1_marshaled = NULL;
	if (___value1 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value1))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value1)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____value1_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value1_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___value1);
		}
	}
	else
	{
		____value1_marshaled = NULL;
	}

	// Native function invocation
	hr = ____ivector_1_t3820727885->IVector_1_SetAt_m1678827239(___index0, ____value1_marshaled);
	____ivector_1_t3820727885->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value1' native representation
	if (____value1_marshaled != NULL)
	{
		(____value1_marshaled)->Release();
		____value1_marshaled = NULL;
	}

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Object>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m1249203443 (RuntimeObject* __this, uint32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	IVector_1_t3820727885* ____ivector_1_t3820727885 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3820727885::IID, reinterpret_cast<void**>(&____ivector_1_t3820727885));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value1' to native representation
	Il2CppIInspectable* ____value1_marshaled = NULL;
	if (___value1 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value1))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value1)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____value1_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value1_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___value1);
		}
	}
	else
	{
		____value1_marshaled = NULL;
	}

	// Native function invocation
	hr = ____ivector_1_t3820727885->IVector_1_InsertAt_m1249203443(___index0, ____value1_marshaled);
	____ivector_1_t3820727885->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value1' native representation
	if (____value1_marshaled != NULL)
	{
		(____value1_marshaled)->Release();
		____value1_marshaled = NULL;
	}

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Object>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m1710167275 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t3820727885* ____ivector_1_t3820727885 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3820727885::IID, reinterpret_cast<void**>(&____ivector_1_t3820727885));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3820727885->IVector_1_RemoveAt_m1710167275(___index0);
	____ivector_1_t3820727885->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Object>::Append(T)
extern "C"  void IVector_1_Append_m1926269351 (RuntimeObject* __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	IVector_1_t3820727885* ____ivector_1_t3820727885 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3820727885::IID, reinterpret_cast<void**>(&____ivector_1_t3820727885));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	Il2CppIInspectable* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____value0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___value0);
		}
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Native function invocation
	hr = ____ivector_1_t3820727885->IVector_1_Append_m1926269351(____value0_marshaled);
	____ivector_1_t3820727885->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Object>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m3025735428 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t3820727885* ____ivector_1_t3820727885 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3820727885::IID, reinterpret_cast<void**>(&____ivector_1_t3820727885));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3820727885->IVector_1_RemoveAtEnd_m3025735428();
	____ivector_1_t3820727885->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Object>::Clear()
extern "C"  void IVector_1_Clear_m305000098 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t3820727885* ____ivector_1_t3820727885 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3820727885::IID, reinterpret_cast<void**>(&____ivector_1_t3820727885));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3820727885->IVector_1_Clear_m305000098();
	____ivector_1_t3820727885->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Object>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m3541038849 (RuntimeObject* __this, uint32_t ___startIndex0, ObjectU5BU5D_t2843939325* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetMany_m3541038849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t3820727885* ____ivector_1_t3820727885 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3820727885::IID, reinterpret_cast<void**>(&____ivector_1_t3820727885));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	Il2CppIInspectable** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<Il2CppIInspectable*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(Il2CppIInspectable*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t3820727885->IVector_1_GetMany_m3541038849(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivector_1_t3820727885->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			RuntimeObject * _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				_____items1_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items1_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Object>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m2515130844 (RuntimeObject* __this, ObjectU5BU5D_t2843939325* ___items0, const RuntimeMethod* method)
{
	IVector_1_t3820727885* ____ivector_1_t3820727885 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3820727885::IID, reinterpret_cast<void**>(&____ivector_1_t3820727885));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	Il2CppIInspectable** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<Il2CppIInspectable*>(static_cast<int32_t>(____items0_marshaledArraySize));
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			if ((___items0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)) != NULL)
			{
				if (il2cpp_codegen_is_import_or_windows_runtime((___items0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i))))
				{
					il2cpp_hresult_t hr = ((Il2CppComObject *)(___items0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)))->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&(____items0_marshaled)[i]));
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);
				}
				else
				{
					(____items0_marshaled)[i] = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>((___items0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
				}
			}
			else
			{
				(____items0_marshaled)[i] = NULL;
			}
		}
	}
	else
	{
		____items0_marshaledArraySize = NULL;
		____items0_marshaled = NULL;
	}

	// Native function invocation
	hr = ____ivector_1_t3820727885->IVector_1_ReplaceAll_m2515130844(____items0_marshaledArraySize, ____items0_marshaled);
	____ivector_1_t3820727885->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

}
// T Windows.Foundation.Collections.IVector`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>::GetAt(System.UInt32)
extern "C"  EventRegistrationToken_t318890788  IVector_1_GetAt_m100662259 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t1059512509* ____ivector_1_t1059512509 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1059512509::IID, reinterpret_cast<void**>(&____ivector_1_t1059512509));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	EventRegistrationToken_t318890788  returnValue = {};
	hr = ____ivector_1_t1059512509->IVector_1_GetAt_m100662259(___index0, &returnValue);
	____ivector_1_t1059512509->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m621277326 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t1059512509* ____ivector_1_t1059512509 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1059512509::IID, reinterpret_cast<void**>(&____ivector_1_t1059512509));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t1059512509->IVector_1_get_Size_m621277326(&returnValue);
	____ivector_1_t1059512509->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m3332817310 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m3332817310_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t1059512509* ____ivector_1_t1059512509 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1059512509::IID, reinterpret_cast<void**>(&____ivector_1_t1059512509));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t3952723346* returnValue = NULL;
	hr = ____ivector_1_t1059512509->IVector_1_GetView_m3332817310(&returnValue);
	____ivector_1_t1059512509->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m1226224308 (RuntimeObject* __this, EventRegistrationToken_t318890788  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVector_1_t1059512509* ____ivector_1_t1059512509 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1059512509::IID, reinterpret_cast<void**>(&____ivector_1_t1059512509));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t1059512509->IVector_1_IndexOf_m1226224308(___value0, (&____index1_empty), &returnValue);
	____ivector_1_t1059512509->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m676705480 (RuntimeObject* __this, uint32_t ___index0, EventRegistrationToken_t318890788  ___value1, const RuntimeMethod* method)
{
	IVector_1_t1059512509* ____ivector_1_t1059512509 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1059512509::IID, reinterpret_cast<void**>(&____ivector_1_t1059512509));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1059512509->IVector_1_SetAt_m676705480(___index0, ___value1);
	____ivector_1_t1059512509->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m2214717186 (RuntimeObject* __this, uint32_t ___index0, EventRegistrationToken_t318890788  ___value1, const RuntimeMethod* method)
{
	IVector_1_t1059512509* ____ivector_1_t1059512509 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1059512509::IID, reinterpret_cast<void**>(&____ivector_1_t1059512509));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1059512509->IVector_1_InsertAt_m2214717186(___index0, ___value1);
	____ivector_1_t1059512509->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m1750123518 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t1059512509* ____ivector_1_t1059512509 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1059512509::IID, reinterpret_cast<void**>(&____ivector_1_t1059512509));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1059512509->IVector_1_RemoveAt_m1750123518(___index0);
	____ivector_1_t1059512509->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>::Append(T)
extern "C"  void IVector_1_Append_m3164318064 (RuntimeObject* __this, EventRegistrationToken_t318890788  ___value0, const RuntimeMethod* method)
{
	IVector_1_t1059512509* ____ivector_1_t1059512509 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1059512509::IID, reinterpret_cast<void**>(&____ivector_1_t1059512509));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1059512509->IVector_1_Append_m3164318064(___value0);
	____ivector_1_t1059512509->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m2693486160 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t1059512509* ____ivector_1_t1059512509 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1059512509::IID, reinterpret_cast<void**>(&____ivector_1_t1059512509));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1059512509->IVector_1_RemoveAtEnd_m2693486160();
	____ivector_1_t1059512509->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>::Clear()
extern "C"  void IVector_1_Clear_m3309901166 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t1059512509* ____ivector_1_t1059512509 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1059512509::IID, reinterpret_cast<void**>(&____ivector_1_t1059512509));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1059512509->IVector_1_Clear_m3309901166();
	____ivector_1_t1059512509->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m3542174024 (RuntimeObject* __this, uint32_t ___startIndex0, EventRegistrationTokenU5BU5D_t897718221* ___items1, const RuntimeMethod* method)
{
	IVector_1_t1059512509* ____ivector_1_t1059512509 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1059512509::IID, reinterpret_cast<void**>(&____ivector_1_t1059512509));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	EventRegistrationToken_t318890788 * ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<EventRegistrationToken_t318890788 >(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(EventRegistrationToken_t318890788 ));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t1059512509->IVector_1_GetMany_m3542174024(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivector_1_t1059512509->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m239273076 (RuntimeObject* __this, EventRegistrationTokenU5BU5D_t897718221* ___items0, const RuntimeMethod* method)
{
	IVector_1_t1059512509* ____ivector_1_t1059512509 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1059512509::IID, reinterpret_cast<void**>(&____ivector_1_t1059512509));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	EventRegistrationToken_t318890788 * ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = reinterpret_cast<EventRegistrationToken_t318890788 *>((___items0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	hr = ____ivector_1_t1059512509->IVector_1_ReplaceAll_m239273076(____items0_marshaledArraySize, ____items0_marshaled);
	____ivector_1_t1059512509->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// T Windows.Foundation.Collections.IVector`1<System.Single>::GetAt(System.UInt32)
extern "C"  float IVector_1_GetAt_m3700246983 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t2137888495* ____ivector_1_t2137888495 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2137888495::IID, reinterpret_cast<void**>(&____ivector_1_t2137888495));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	float returnValue = 0.0f;
	hr = ____ivector_1_t2137888495->IVector_1_GetAt_m3700246983(___index0, &returnValue);
	____ivector_1_t2137888495->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Single>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m3531442778 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t2137888495* ____ivector_1_t2137888495 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2137888495::IID, reinterpret_cast<void**>(&____ivector_1_t2137888495));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t2137888495->IVector_1_get_Size_m3531442778(&returnValue);
	____ivector_1_t2137888495->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.Single>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m1670428144 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m1670428144_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t2137888495* ____ivector_1_t2137888495 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2137888495::IID, reinterpret_cast<void**>(&____ivector_1_t2137888495));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t736132036* returnValue = NULL;
	hr = ____ivector_1_t2137888495->IVector_1_GetView_m1670428144(&returnValue);
	____ivector_1_t2137888495->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.Single>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m739803125 (RuntimeObject* __this, float ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVector_1_t2137888495* ____ivector_1_t2137888495 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2137888495::IID, reinterpret_cast<void**>(&____ivector_1_t2137888495));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t2137888495->IVector_1_IndexOf_m739803125(___value0, (&____index1_empty), &returnValue);
	____ivector_1_t2137888495->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Single>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m1156666243 (RuntimeObject* __this, uint32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	IVector_1_t2137888495* ____ivector_1_t2137888495 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2137888495::IID, reinterpret_cast<void**>(&____ivector_1_t2137888495));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t2137888495->IVector_1_SetAt_m1156666243(___index0, ___value1);
	____ivector_1_t2137888495->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Single>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m1898551949 (RuntimeObject* __this, uint32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	IVector_1_t2137888495* ____ivector_1_t2137888495 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2137888495::IID, reinterpret_cast<void**>(&____ivector_1_t2137888495));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t2137888495->IVector_1_InsertAt_m1898551949(___index0, ___value1);
	____ivector_1_t2137888495->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Single>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m3164249930 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t2137888495* ____ivector_1_t2137888495 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2137888495::IID, reinterpret_cast<void**>(&____ivector_1_t2137888495));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t2137888495->IVector_1_RemoveAt_m3164249930(___index0);
	____ivector_1_t2137888495->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Single>::Append(T)
extern "C"  void IVector_1_Append_m2356107643 (RuntimeObject* __this, float ___value0, const RuntimeMethod* method)
{
	IVector_1_t2137888495* ____ivector_1_t2137888495 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2137888495::IID, reinterpret_cast<void**>(&____ivector_1_t2137888495));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t2137888495->IVector_1_Append_m2356107643(___value0);
	____ivector_1_t2137888495->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Single>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m2431982566 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t2137888495* ____ivector_1_t2137888495 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2137888495::IID, reinterpret_cast<void**>(&____ivector_1_t2137888495));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t2137888495->IVector_1_RemoveAtEnd_m2431982566();
	____ivector_1_t2137888495->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Single>::Clear()
extern "C"  void IVector_1_Clear_m3622925119 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t2137888495* ____ivector_1_t2137888495 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2137888495::IID, reinterpret_cast<void**>(&____ivector_1_t2137888495));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t2137888495->IVector_1_Clear_m3622925119();
	____ivector_1_t2137888495->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Single>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m2648716604 (RuntimeObject* __this, uint32_t ___startIndex0, SingleU5BU5D_t1444911251* ___items1, const RuntimeMethod* method)
{
	IVector_1_t2137888495* ____ivector_1_t2137888495 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2137888495::IID, reinterpret_cast<void**>(&____ivector_1_t2137888495));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	float* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<float>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(float));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t2137888495->IVector_1_GetMany_m2648716604(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivector_1_t2137888495->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Single>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m1968832847 (RuntimeObject* __this, SingleU5BU5D_t1444911251* ___items0, const RuntimeMethod* method)
{
	IVector_1_t2137888495* ____ivector_1_t2137888495 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2137888495::IID, reinterpret_cast<void**>(&____ivector_1_t2137888495));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	float* ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = reinterpret_cast<float*>((___items0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	hr = ____ivector_1_t2137888495->IVector_1_ReplaceAll_m1968832847(____items0_marshaledArraySize, ____items0_marshaled);
	____ivector_1_t2137888495->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// T Windows.Foundation.Collections.IVector`1<System.String>::GetAt(System.UInt32)
extern "C"  String_t* IVector_1_GetAt_m2881185809 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t2588072410* ____ivector_1_t2588072410 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2588072410::IID, reinterpret_cast<void**>(&____ivector_1_t2588072410));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppHString returnValue = NULL;
	hr = ____ivector_1_t2588072410->IVector_1_GetAt_m2881185809(___index0, &returnValue);
	____ivector_1_t2588072410->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_hstring_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free_hstring(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.String>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m3596466296 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t2588072410* ____ivector_1_t2588072410 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2588072410::IID, reinterpret_cast<void**>(&____ivector_1_t2588072410));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t2588072410->IVector_1_get_Size_m3596466296(&returnValue);
	____ivector_1_t2588072410->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.String>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m1219033264 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m1219033264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t2588072410* ____ivector_1_t2588072410 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2588072410::IID, reinterpret_cast<void**>(&____ivector_1_t2588072410));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t1186315951* returnValue = NULL;
	hr = ____ivector_1_t2588072410->IVector_1_GetView_m1219033264(&returnValue);
	____ivector_1_t2588072410->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.String>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m2107560054 (RuntimeObject* __this, String_t* ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVector_1_t2588072410* ____ivector_1_t2588072410 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2588072410::IID, reinterpret_cast<void**>(&____ivector_1_t2588072410));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	Il2CppHString ____value0_marshaled = NULL;
	if (___value0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("value"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___value0NativeView, ___value0);
	il2cpp::utils::Il2CppHStringReference ___value0HStringReference(___value0NativeView);
	____value0_marshaled = ___value0HStringReference;

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t2588072410->IVector_1_IndexOf_m2107560054(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivector_1_t2588072410->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.String>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m166369194 (RuntimeObject* __this, uint32_t ___index0, String_t* ___value1, const RuntimeMethod* method)
{
	IVector_1_t2588072410* ____ivector_1_t2588072410 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2588072410::IID, reinterpret_cast<void**>(&____ivector_1_t2588072410));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value1' to native representation
	Il2CppHString ____value1_marshaled = NULL;
	if (___value1 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("value"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___value1NativeView, ___value1);
	il2cpp::utils::Il2CppHStringReference ___value1HStringReference(___value1NativeView);
	____value1_marshaled = ___value1HStringReference;

	// Native function invocation
	hr = ____ivector_1_t2588072410->IVector_1_SetAt_m166369194(___index0, ____value1_marshaled);
	____ivector_1_t2588072410->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.String>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m2973823425 (RuntimeObject* __this, uint32_t ___index0, String_t* ___value1, const RuntimeMethod* method)
{
	IVector_1_t2588072410* ____ivector_1_t2588072410 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2588072410::IID, reinterpret_cast<void**>(&____ivector_1_t2588072410));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value1' to native representation
	Il2CppHString ____value1_marshaled = NULL;
	if (___value1 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("value"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___value1NativeView, ___value1);
	il2cpp::utils::Il2CppHStringReference ___value1HStringReference(___value1NativeView);
	____value1_marshaled = ___value1HStringReference;

	// Native function invocation
	hr = ____ivector_1_t2588072410->IVector_1_InsertAt_m2973823425(___index0, ____value1_marshaled);
	____ivector_1_t2588072410->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.String>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m269128213 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t2588072410* ____ivector_1_t2588072410 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2588072410::IID, reinterpret_cast<void**>(&____ivector_1_t2588072410));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t2588072410->IVector_1_RemoveAt_m269128213(___index0);
	____ivector_1_t2588072410->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.String>::Append(T)
extern "C"  void IVector_1_Append_m3989127587 (RuntimeObject* __this, String_t* ___value0, const RuntimeMethod* method)
{
	IVector_1_t2588072410* ____ivector_1_t2588072410 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2588072410::IID, reinterpret_cast<void**>(&____ivector_1_t2588072410));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	Il2CppHString ____value0_marshaled = NULL;
	if (___value0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("value"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___value0NativeView, ___value0);
	il2cpp::utils::Il2CppHStringReference ___value0HStringReference(___value0NativeView);
	____value0_marshaled = ___value0HStringReference;

	// Native function invocation
	hr = ____ivector_1_t2588072410->IVector_1_Append_m3989127587(____value0_marshaled);
	____ivector_1_t2588072410->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.String>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m3708964671 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t2588072410* ____ivector_1_t2588072410 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2588072410::IID, reinterpret_cast<void**>(&____ivector_1_t2588072410));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t2588072410->IVector_1_RemoveAtEnd_m3708964671();
	____ivector_1_t2588072410->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.String>::Clear()
extern "C"  void IVector_1_Clear_m867824 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t2588072410* ____ivector_1_t2588072410 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2588072410::IID, reinterpret_cast<void**>(&____ivector_1_t2588072410));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t2588072410->IVector_1_Clear_m867824();
	____ivector_1_t2588072410->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.String>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m3511123964 (RuntimeObject* __this, uint32_t ___startIndex0, StringU5BU5D_t1281789340* ___items1, const RuntimeMethod* method)
{
	IVector_1_t2588072410* ____ivector_1_t2588072410 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2588072410::IID, reinterpret_cast<void**>(&____ivector_1_t2588072410));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	Il2CppHString* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<Il2CppHString>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(Il2CppHString));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t2588072410->IVector_1_GetMany_m3511123964(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivector_1_t2588072410->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			String_t* _____items1_marshaled_i__unmarshaled = NULL;
			_____items1_marshaled_i__unmarshaled = il2cpp_codegen_marshal_hstring_result((____items1_marshaled)[i]);
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free_hstring((____items1_marshaled)[i]);
			(____items1_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.String>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m4096830123 (RuntimeObject* __this, StringU5BU5D_t1281789340* ___items0, const RuntimeMethod* method)
{
	IVector_1_t2588072410* ____ivector_1_t2588072410 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2588072410::IID, reinterpret_cast<void**>(&____ivector_1_t2588072410));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	Il2CppHString* ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<Il2CppHString>(static_cast<int32_t>(____items0_marshaledArraySize));
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			if ((___items0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)) == NULL)
			{
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("items"));
			}
			(____items0_marshaled)[i] = il2cpp_codegen_create_hstring((___items0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____items0_marshaledArraySize = NULL;
		____items0_marshaled = NULL;
	}

	// Native function invocation
	hr = ____ivector_1_t2588072410->IVector_1_ReplaceAll_m4096830123(____items0_marshaledArraySize, ____items0_marshaled);
	____ivector_1_t2588072410->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free_hstring((____items0_marshaled)[i]);
			(____items0_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

}
// T Windows.Foundation.Collections.IVector`1<System.TimeSpan>::GetAt(System.UInt32)
extern "C"  TimeSpan_t881159249  IVector_1_GetAt_m4001576701 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t1621780970* ____ivector_1_t1621780970 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1621780970::IID, reinterpret_cast<void**>(&____ivector_1_t1621780970));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	TimeSpan_t881159249  returnValue = {};
	hr = ____ivector_1_t1621780970->IVector_1_GetAt_m4001576701(___index0, &returnValue);
	____ivector_1_t1621780970->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.TimeSpan>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m1979953161 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t1621780970* ____ivector_1_t1621780970 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1621780970::IID, reinterpret_cast<void**>(&____ivector_1_t1621780970));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t1621780970->IVector_1_get_Size_m1979953161(&returnValue);
	____ivector_1_t1621780970->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.TimeSpan>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m2578442391 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m2578442391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t1621780970* ____ivector_1_t1621780970 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1621780970::IID, reinterpret_cast<void**>(&____ivector_1_t1621780970));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t220024511* returnValue = NULL;
	hr = ____ivector_1_t1621780970->IVector_1_GetView_m2578442391(&returnValue);
	____ivector_1_t1621780970->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.TimeSpan>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m3610534390 (RuntimeObject* __this, TimeSpan_t881159249  ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVector_1_t1621780970* ____ivector_1_t1621780970 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1621780970::IID, reinterpret_cast<void**>(&____ivector_1_t1621780970));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t1621780970->IVector_1_IndexOf_m3610534390(___value0, (&____index1_empty), &returnValue);
	____ivector_1_t1621780970->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.TimeSpan>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m3169382320 (RuntimeObject* __this, uint32_t ___index0, TimeSpan_t881159249  ___value1, const RuntimeMethod* method)
{
	IVector_1_t1621780970* ____ivector_1_t1621780970 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1621780970::IID, reinterpret_cast<void**>(&____ivector_1_t1621780970));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1621780970->IVector_1_SetAt_m3169382320(___index0, ___value1);
	____ivector_1_t1621780970->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.TimeSpan>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m2337166544 (RuntimeObject* __this, uint32_t ___index0, TimeSpan_t881159249  ___value1, const RuntimeMethod* method)
{
	IVector_1_t1621780970* ____ivector_1_t1621780970 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1621780970::IID, reinterpret_cast<void**>(&____ivector_1_t1621780970));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1621780970->IVector_1_InsertAt_m2337166544(___index0, ___value1);
	____ivector_1_t1621780970->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.TimeSpan>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m3743278496 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t1621780970* ____ivector_1_t1621780970 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1621780970::IID, reinterpret_cast<void**>(&____ivector_1_t1621780970));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1621780970->IVector_1_RemoveAt_m3743278496(___index0);
	____ivector_1_t1621780970->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.TimeSpan>::Append(T)
extern "C"  void IVector_1_Append_m250096651 (RuntimeObject* __this, TimeSpan_t881159249  ___value0, const RuntimeMethod* method)
{
	IVector_1_t1621780970* ____ivector_1_t1621780970 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1621780970::IID, reinterpret_cast<void**>(&____ivector_1_t1621780970));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1621780970->IVector_1_Append_m250096651(___value0);
	____ivector_1_t1621780970->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.TimeSpan>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m4140907611 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t1621780970* ____ivector_1_t1621780970 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1621780970::IID, reinterpret_cast<void**>(&____ivector_1_t1621780970));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1621780970->IVector_1_RemoveAtEnd_m4140907611();
	____ivector_1_t1621780970->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.TimeSpan>::Clear()
extern "C"  void IVector_1_Clear_m2231523500 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t1621780970* ____ivector_1_t1621780970 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1621780970::IID, reinterpret_cast<void**>(&____ivector_1_t1621780970));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1621780970->IVector_1_Clear_m2231523500();
	____ivector_1_t1621780970->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.TimeSpan>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m3395665562 (RuntimeObject* __this, uint32_t ___startIndex0, TimeSpanU5BU5D_t4291357516* ___items1, const RuntimeMethod* method)
{
	IVector_1_t1621780970* ____ivector_1_t1621780970 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1621780970::IID, reinterpret_cast<void**>(&____ivector_1_t1621780970));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	TimeSpan_t881159249 * ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<TimeSpan_t881159249 >(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(TimeSpan_t881159249 ));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t1621780970->IVector_1_GetMany_m3395665562(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivector_1_t1621780970->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.TimeSpan>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m2447609914 (RuntimeObject* __this, TimeSpanU5BU5D_t4291357516* ___items0, const RuntimeMethod* method)
{
	IVector_1_t1621780970* ____ivector_1_t1621780970 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1621780970::IID, reinterpret_cast<void**>(&____ivector_1_t1621780970));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	TimeSpan_t881159249 * ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = reinterpret_cast<TimeSpan_t881159249 *>((___items0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	hr = ____ivector_1_t1621780970->IVector_1_ReplaceAll_m2447609914(____items0_marshaledArraySize, ____items0_marshaled);
	____ivector_1_t1621780970->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// T Windows.Foundation.Collections.IVector`1<System.Type>::GetAt(System.UInt32)
extern "C"  Type_t * IVector_1_GetAt_m3023396459 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Type>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m3611201515 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t3224566481* ____ivector_1_t3224566481 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3224566481::IID, reinterpret_cast<void**>(&____ivector_1_t3224566481));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t3224566481->IVector_1_get_Size_m3611201515(&returnValue);
	____ivector_1_t3224566481->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.Type>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m2404813821 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m2404813821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t3224566481* ____ivector_1_t3224566481 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3224566481::IID, reinterpret_cast<void**>(&____ivector_1_t3224566481));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t1822810022* returnValue = NULL;
	hr = ____ivector_1_t3224566481->IVector_1_GetView_m2404813821(&returnValue);
	____ivector_1_t3224566481->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.Type>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m2314055355 (RuntimeObject* __this, Type_t * ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Type>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m3724063766 (RuntimeObject* __this, uint32_t ___index0, Type_t * ___value1, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Type>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m2145145338 (RuntimeObject* __this, uint32_t ___index0, Type_t * ___value1, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Type>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m154224944 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t3224566481* ____ivector_1_t3224566481 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3224566481::IID, reinterpret_cast<void**>(&____ivector_1_t3224566481));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3224566481->IVector_1_RemoveAt_m154224944(___index0);
	____ivector_1_t3224566481->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Type>::Append(T)
extern "C"  void IVector_1_Append_m386838768 (RuntimeObject* __this, Type_t * ___value0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Type>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m110681880 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t3224566481* ____ivector_1_t3224566481 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3224566481::IID, reinterpret_cast<void**>(&____ivector_1_t3224566481));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3224566481->IVector_1_RemoveAtEnd_m110681880();
	____ivector_1_t3224566481->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Type>::Clear()
extern "C"  void IVector_1_Clear_m1062744925 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t3224566481* ____ivector_1_t3224566481 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3224566481::IID, reinterpret_cast<void**>(&____ivector_1_t3224566481));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3224566481->IVector_1_Clear_m1062744925();
	____ivector_1_t3224566481->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Type>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m1218665569 (RuntimeObject* __this, uint32_t ___startIndex0, TypeU5BU5D_t3940880105* ___items1, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Type>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m4247109972 (RuntimeObject* __this, TypeU5BU5D_t3940880105* ___items0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// T Windows.Foundation.Collections.IVector`1<System.UInt16>::GetAt(System.UInt32)
extern "C"  uint16_t IVector_1_GetAt_m238587295 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t2918346679* ____ivector_1_t2918346679 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2918346679::IID, reinterpret_cast<void**>(&____ivector_1_t2918346679));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint16_t returnValue = 0;
	hr = ____ivector_1_t2918346679->IVector_1_GetAt_m238587295(___index0, &returnValue);
	____ivector_1_t2918346679->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.UInt16>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m3469677531 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t2918346679* ____ivector_1_t2918346679 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2918346679::IID, reinterpret_cast<void**>(&____ivector_1_t2918346679));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t2918346679->IVector_1_get_Size_m3469677531(&returnValue);
	____ivector_1_t2918346679->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.UInt16>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m3025995885 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m3025995885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t2918346679* ____ivector_1_t2918346679 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2918346679::IID, reinterpret_cast<void**>(&____ivector_1_t2918346679));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t1516590220* returnValue = NULL;
	hr = ____ivector_1_t2918346679->IVector_1_GetView_m3025995885(&returnValue);
	____ivector_1_t2918346679->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.UInt16>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m1343053975 (RuntimeObject* __this, uint16_t ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVector_1_t2918346679* ____ivector_1_t2918346679 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2918346679::IID, reinterpret_cast<void**>(&____ivector_1_t2918346679));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t2918346679->IVector_1_IndexOf_m1343053975(___value0, (&____index1_empty), &returnValue);
	____ivector_1_t2918346679->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt16>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m246985172 (RuntimeObject* __this, uint32_t ___index0, uint16_t ___value1, const RuntimeMethod* method)
{
	IVector_1_t2918346679* ____ivector_1_t2918346679 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2918346679::IID, reinterpret_cast<void**>(&____ivector_1_t2918346679));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t2918346679->IVector_1_SetAt_m246985172(___index0, ___value1);
	____ivector_1_t2918346679->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt16>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m2991702087 (RuntimeObject* __this, uint32_t ___index0, uint16_t ___value1, const RuntimeMethod* method)
{
	IVector_1_t2918346679* ____ivector_1_t2918346679 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2918346679::IID, reinterpret_cast<void**>(&____ivector_1_t2918346679));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t2918346679->IVector_1_InsertAt_m2991702087(___index0, ___value1);
	____ivector_1_t2918346679->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt16>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m2673603791 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t2918346679* ____ivector_1_t2918346679 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2918346679::IID, reinterpret_cast<void**>(&____ivector_1_t2918346679));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t2918346679->IVector_1_RemoveAt_m2673603791(___index0);
	____ivector_1_t2918346679->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt16>::Append(T)
extern "C"  void IVector_1_Append_m1576189264 (RuntimeObject* __this, uint16_t ___value0, const RuntimeMethod* method)
{
	IVector_1_t2918346679* ____ivector_1_t2918346679 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2918346679::IID, reinterpret_cast<void**>(&____ivector_1_t2918346679));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t2918346679->IVector_1_Append_m1576189264(___value0);
	____ivector_1_t2918346679->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt16>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m1857915307 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t2918346679* ____ivector_1_t2918346679 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2918346679::IID, reinterpret_cast<void**>(&____ivector_1_t2918346679));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t2918346679->IVector_1_RemoveAtEnd_m1857915307();
	____ivector_1_t2918346679->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt16>::Clear()
extern "C"  void IVector_1_Clear_m3093710788 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t2918346679* ____ivector_1_t2918346679 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2918346679::IID, reinterpret_cast<void**>(&____ivector_1_t2918346679));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t2918346679->IVector_1_Clear_m3093710788();
	____ivector_1_t2918346679->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.UInt16>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m91550909 (RuntimeObject* __this, uint32_t ___startIndex0, UInt16U5BU5D_t3326319531* ___items1, const RuntimeMethod* method)
{
	IVector_1_t2918346679* ____ivector_1_t2918346679 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2918346679::IID, reinterpret_cast<void**>(&____ivector_1_t2918346679));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	uint16_t* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<uint16_t>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(uint16_t));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t2918346679->IVector_1_GetMany_m91550909(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivector_1_t2918346679->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt16>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m2701549418 (RuntimeObject* __this, UInt16U5BU5D_t3326319531* ___items0, const RuntimeMethod* method)
{
	IVector_1_t2918346679* ____ivector_1_t2918346679 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t2918346679::IID, reinterpret_cast<void**>(&____ivector_1_t2918346679));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	uint16_t* ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = reinterpret_cast<uint16_t*>((___items0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	hr = ____ivector_1_t2918346679->IVector_1_ReplaceAll_m2701549418(____items0_marshaledArraySize, ____items0_marshaled);
	____ivector_1_t2918346679->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// T Windows.Foundation.Collections.IVector`1<System.UInt32>::GetAt(System.UInt32)
extern "C"  uint32_t IVector_1_GetAt_m1377932999 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t3300683699* ____ivector_1_t3300683699 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3300683699::IID, reinterpret_cast<void**>(&____ivector_1_t3300683699));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t3300683699->IVector_1_GetAt_m1377932999(___index0, &returnValue);
	____ivector_1_t3300683699->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.UInt32>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m1923345822 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t3300683699* ____ivector_1_t3300683699 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3300683699::IID, reinterpret_cast<void**>(&____ivector_1_t3300683699));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t3300683699->IVector_1_get_Size_m1923345822(&returnValue);
	____ivector_1_t3300683699->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.UInt32>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m776116354 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m776116354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t3300683699* ____ivector_1_t3300683699 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3300683699::IID, reinterpret_cast<void**>(&____ivector_1_t3300683699));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t1898927240* returnValue = NULL;
	hr = ____ivector_1_t3300683699->IVector_1_GetView_m776116354(&returnValue);
	____ivector_1_t3300683699->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.UInt32>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m3489169491 (RuntimeObject* __this, uint32_t ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVector_1_t3300683699* ____ivector_1_t3300683699 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3300683699::IID, reinterpret_cast<void**>(&____ivector_1_t3300683699));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t3300683699->IVector_1_IndexOf_m3489169491(___value0, (&____index1_empty), &returnValue);
	____ivector_1_t3300683699->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt32>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m3079373580 (RuntimeObject* __this, uint32_t ___index0, uint32_t ___value1, const RuntimeMethod* method)
{
	IVector_1_t3300683699* ____ivector_1_t3300683699 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3300683699::IID, reinterpret_cast<void**>(&____ivector_1_t3300683699));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3300683699->IVector_1_SetAt_m3079373580(___index0, ___value1);
	____ivector_1_t3300683699->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt32>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m2718637520 (RuntimeObject* __this, uint32_t ___index0, uint32_t ___value1, const RuntimeMethod* method)
{
	IVector_1_t3300683699* ____ivector_1_t3300683699 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3300683699::IID, reinterpret_cast<void**>(&____ivector_1_t3300683699));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3300683699->IVector_1_InsertAt_m2718637520(___index0, ___value1);
	____ivector_1_t3300683699->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt32>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m328020143 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t3300683699* ____ivector_1_t3300683699 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3300683699::IID, reinterpret_cast<void**>(&____ivector_1_t3300683699));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3300683699->IVector_1_RemoveAt_m328020143(___index0);
	____ivector_1_t3300683699->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt32>::Append(T)
extern "C"  void IVector_1_Append_m2065973735 (RuntimeObject* __this, uint32_t ___value0, const RuntimeMethod* method)
{
	IVector_1_t3300683699* ____ivector_1_t3300683699 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3300683699::IID, reinterpret_cast<void**>(&____ivector_1_t3300683699));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3300683699->IVector_1_Append_m2065973735(___value0);
	____ivector_1_t3300683699->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt32>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m1175668767 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t3300683699* ____ivector_1_t3300683699 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3300683699::IID, reinterpret_cast<void**>(&____ivector_1_t3300683699));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3300683699->IVector_1_RemoveAtEnd_m1175668767();
	____ivector_1_t3300683699->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt32>::Clear()
extern "C"  void IVector_1_Clear_m3720926093 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t3300683699* ____ivector_1_t3300683699 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3300683699::IID, reinterpret_cast<void**>(&____ivector_1_t3300683699));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t3300683699->IVector_1_Clear_m3720926093();
	____ivector_1_t3300683699->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.UInt32>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m2743709791 (RuntimeObject* __this, uint32_t ___startIndex0, UInt32U5BU5D_t2770800703* ___items1, const RuntimeMethod* method)
{
	IVector_1_t3300683699* ____ivector_1_t3300683699 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3300683699::IID, reinterpret_cast<void**>(&____ivector_1_t3300683699));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	uint32_t* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<uint32_t>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(uint32_t));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t3300683699->IVector_1_GetMany_m2743709791(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivector_1_t3300683699->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt32>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m3864847556 (RuntimeObject* __this, UInt32U5BU5D_t2770800703* ___items0, const RuntimeMethod* method)
{
	IVector_1_t3300683699* ____ivector_1_t3300683699 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t3300683699::IID, reinterpret_cast<void**>(&____ivector_1_t3300683699));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	uint32_t* ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = reinterpret_cast<uint32_t*>((___items0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	hr = ____ivector_1_t3300683699->IVector_1_ReplaceAll_m3864847556(____items0_marshaledArraySize, ____items0_marshaled);
	____ivector_1_t3300683699->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// T Windows.Foundation.Collections.IVector`1<System.UInt64>::GetAt(System.UInt32)
extern "C"  uint64_t IVector_1_GetAt_m951635711 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t579694517* ____ivector_1_t579694517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t579694517::IID, reinterpret_cast<void**>(&____ivector_1_t579694517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint64_t returnValue = 0;
	hr = ____ivector_1_t579694517->IVector_1_GetAt_m951635711(___index0, &returnValue);
	____ivector_1_t579694517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.UInt64>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m1121146914 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t579694517* ____ivector_1_t579694517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t579694517::IID, reinterpret_cast<void**>(&____ivector_1_t579694517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t579694517->IVector_1_get_Size_m1121146914(&returnValue);
	____ivector_1_t579694517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.UInt64>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m435781098 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m435781098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t579694517* ____ivector_1_t579694517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t579694517::IID, reinterpret_cast<void**>(&____ivector_1_t579694517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t3472905354* returnValue = NULL;
	hr = ____ivector_1_t579694517->IVector_1_GetView_m435781098(&returnValue);
	____ivector_1_t579694517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.UInt64>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m2037213150 (RuntimeObject* __this, uint64_t ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVector_1_t579694517* ____ivector_1_t579694517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t579694517::IID, reinterpret_cast<void**>(&____ivector_1_t579694517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t579694517->IVector_1_IndexOf_m2037213150(___value0, (&____index1_empty), &returnValue);
	____ivector_1_t579694517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt64>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m2428609496 (RuntimeObject* __this, uint32_t ___index0, uint64_t ___value1, const RuntimeMethod* method)
{
	IVector_1_t579694517* ____ivector_1_t579694517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t579694517::IID, reinterpret_cast<void**>(&____ivector_1_t579694517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t579694517->IVector_1_SetAt_m2428609496(___index0, ___value1);
	____ivector_1_t579694517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt64>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m3377046151 (RuntimeObject* __this, uint32_t ___index0, uint64_t ___value1, const RuntimeMethod* method)
{
	IVector_1_t579694517* ____ivector_1_t579694517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t579694517::IID, reinterpret_cast<void**>(&____ivector_1_t579694517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t579694517->IVector_1_InsertAt_m3377046151(___index0, ___value1);
	____ivector_1_t579694517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt64>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m3822942214 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t579694517* ____ivector_1_t579694517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t579694517::IID, reinterpret_cast<void**>(&____ivector_1_t579694517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t579694517->IVector_1_RemoveAt_m3822942214(___index0);
	____ivector_1_t579694517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt64>::Append(T)
extern "C"  void IVector_1_Append_m3066519795 (RuntimeObject* __this, uint64_t ___value0, const RuntimeMethod* method)
{
	IVector_1_t579694517* ____ivector_1_t579694517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t579694517::IID, reinterpret_cast<void**>(&____ivector_1_t579694517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t579694517->IVector_1_Append_m3066519795(___value0);
	____ivector_1_t579694517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt64>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m688723122 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t579694517* ____ivector_1_t579694517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t579694517::IID, reinterpret_cast<void**>(&____ivector_1_t579694517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t579694517->IVector_1_RemoveAtEnd_m688723122();
	____ivector_1_t579694517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt64>::Clear()
extern "C"  void IVector_1_Clear_m3650286301 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t579694517* ____ivector_1_t579694517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t579694517::IID, reinterpret_cast<void**>(&____ivector_1_t579694517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t579694517->IVector_1_Clear_m3650286301();
	____ivector_1_t579694517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.UInt64>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m1446676414 (RuntimeObject* __this, uint32_t ___startIndex0, UInt64U5BU5D_t1659327989* ___items1, const RuntimeMethod* method)
{
	IVector_1_t579694517* ____ivector_1_t579694517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t579694517::IID, reinterpret_cast<void**>(&____ivector_1_t579694517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	uint64_t* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<uint64_t>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(uint64_t));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t579694517->IVector_1_GetMany_m1446676414(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivector_1_t579694517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.UInt64>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m544562035 (RuntimeObject* __this, UInt64U5BU5D_t1659327989* ___items0, const RuntimeMethod* method)
{
	IVector_1_t579694517* ____ivector_1_t579694517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t579694517::IID, reinterpret_cast<void**>(&____ivector_1_t579694517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	uint64_t* ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = reinterpret_cast<uint64_t*>((___items0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	hr = ____ivector_1_t579694517->IVector_1_ReplaceAll_m544562035(____items0_marshaledArraySize, ____items0_marshaled);
	____ivector_1_t579694517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// T Windows.Foundation.Collections.IVector`1<System.Uri>::GetAt(System.UInt32)
extern "C"  Uri_t100236324 * IVector_1_GetAt_m3184039671 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetAt_m3184039671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t840858045* ____ivector_1_t840858045 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t840858045::IID, reinterpret_cast<void**>(&____ivector_1_t840858045));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IUriRuntimeClass_t921050115* returnValue = NULL;
	hr = ____ivector_1_t840858045->IVector_1_GetAt_m3184039671(___index0, &returnValue);
	____ivector_1_t840858045->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	Uri_t100236324 * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		Il2CppHString __returnValue_unmarshaledAsString_marshaled = NULL;
		il2cpp_hresult_t hr = (returnValue)->IUriRuntimeClass_get_RawUri_m1303737506((&__returnValue_unmarshaledAsString_marshaled));
		il2cpp_codegen_com_raise_exception_if_failed(hr, false);

		String_t* ___returnValue_unmarshaledAsString_marshaled_unmarshaled = NULL;
		___returnValue_unmarshaledAsString_marshaled_unmarshaled = il2cpp_codegen_marshal_hstring_result(__returnValue_unmarshaledAsString_marshaled);
		il2cpp_codegen_marshal_free_hstring(__returnValue_unmarshaledAsString_marshaled);
		__returnValue_unmarshaledAsString_marshaled = NULL;

		Uri_t100236324 * _returnValue_unmarshaledTemp = (Uri_t100236324*)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m1474792120(_returnValue_unmarshaledTemp, ___returnValue_unmarshaledAsString_marshaled_unmarshaled, NULL);
		_returnValue_unmarshaled = _returnValue_unmarshaledTemp;
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Uri>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m1541693482 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t840858045* ____ivector_1_t840858045 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t840858045::IID, reinterpret_cast<void**>(&____ivector_1_t840858045));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t840858045->IVector_1_get_Size_m1541693482(&returnValue);
	____ivector_1_t840858045->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.Uri>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m748183030 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m748183030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t840858045* ____ivector_1_t840858045 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t840858045::IID, reinterpret_cast<void**>(&____ivector_1_t840858045));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t3734068882* returnValue = NULL;
	hr = ____ivector_1_t840858045->IVector_1_GetView_m748183030(&returnValue);
	____ivector_1_t840858045->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.Uri>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m3867025885 (RuntimeObject* __this, Uri_t100236324 * ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_IndexOf_m3867025885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t840858045* ____ivector_1_t840858045 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t840858045::IID, reinterpret_cast<void**>(&____ivector_1_t840858045));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IUriRuntimeClass_t921050115* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		String_t* ___value0AsString = Uri_get_OriginalString_m2551181575(___value0, NULL);
		Il2CppHString ____value0AsString_marshaled = NULL;
		if (___value0AsString == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("___value0AsString"));
		}

		DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___value0AsStringNativeView, ___value0AsString);
		il2cpp::utils::Il2CppHStringReference ___value0AsStringHStringReference(___value0AsStringNativeView);
		____value0AsString_marshaled = ___value0AsStringHStringReference;
		il2cpp_hresult_t hr = ((Uri_t952691556_StaticFields*)Uri_t952691556_il2cpp_TypeInfo_var->static_fields)->get_____iuriRuntimeClassFactory_t1786313621()->IUriRuntimeClassFactory_CreateUri_m1347622952(____value0AsString_marshaled, (&____value0_marshaled));
		il2cpp_codegen_com_raise_exception_if_failed(hr, false);
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t840858045->IVector_1_IndexOf_m3867025885(____value0_marshaled, (&____index1_empty), &returnValue);
	____ivector_1_t840858045->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Uri>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m2446451012 (RuntimeObject* __this, uint32_t ___index0, Uri_t100236324 * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_SetAt_m2446451012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t840858045* ____ivector_1_t840858045 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t840858045::IID, reinterpret_cast<void**>(&____ivector_1_t840858045));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value1' to native representation
	IUriRuntimeClass_t921050115* ____value1_marshaled = NULL;
	if (___value1 != NULL)
	{
		String_t* ___value1AsString = Uri_get_OriginalString_m2551181575(___value1, NULL);
		Il2CppHString ____value1AsString_marshaled = NULL;
		if (___value1AsString == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("___value1AsString"));
		}

		DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___value1AsStringNativeView, ___value1AsString);
		il2cpp::utils::Il2CppHStringReference ___value1AsStringHStringReference(___value1AsStringNativeView);
		____value1AsString_marshaled = ___value1AsStringHStringReference;
		il2cpp_hresult_t hr = ((Uri_t952691556_StaticFields*)Uri_t952691556_il2cpp_TypeInfo_var->static_fields)->get_____iuriRuntimeClassFactory_t1786313621()->IUriRuntimeClassFactory_CreateUri_m1347622952(____value1AsString_marshaled, (&____value1_marshaled));
		il2cpp_codegen_com_raise_exception_if_failed(hr, false);
	}
	else
	{
		____value1_marshaled = NULL;
	}

	// Native function invocation
	hr = ____ivector_1_t840858045->IVector_1_SetAt_m2446451012(___index0, ____value1_marshaled);
	____ivector_1_t840858045->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value1' native representation
	if (____value1_marshaled != NULL)
	{
		(____value1_marshaled)->Release();
		____value1_marshaled = NULL;
	}

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Uri>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m697980859 (RuntimeObject* __this, uint32_t ___index0, Uri_t100236324 * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_InsertAt_m697980859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t840858045* ____ivector_1_t840858045 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t840858045::IID, reinterpret_cast<void**>(&____ivector_1_t840858045));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value1' to native representation
	IUriRuntimeClass_t921050115* ____value1_marshaled = NULL;
	if (___value1 != NULL)
	{
		String_t* ___value1AsString = Uri_get_OriginalString_m2551181575(___value1, NULL);
		Il2CppHString ____value1AsString_marshaled = NULL;
		if (___value1AsString == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("___value1AsString"));
		}

		DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___value1AsStringNativeView, ___value1AsString);
		il2cpp::utils::Il2CppHStringReference ___value1AsStringHStringReference(___value1AsStringNativeView);
		____value1AsString_marshaled = ___value1AsStringHStringReference;
		il2cpp_hresult_t hr = ((Uri_t952691556_StaticFields*)Uri_t952691556_il2cpp_TypeInfo_var->static_fields)->get_____iuriRuntimeClassFactory_t1786313621()->IUriRuntimeClassFactory_CreateUri_m1347622952(____value1AsString_marshaled, (&____value1_marshaled));
		il2cpp_codegen_com_raise_exception_if_failed(hr, false);
	}
	else
	{
		____value1_marshaled = NULL;
	}

	// Native function invocation
	hr = ____ivector_1_t840858045->IVector_1_InsertAt_m697980859(___index0, ____value1_marshaled);
	____ivector_1_t840858045->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value1' native representation
	if (____value1_marshaled != NULL)
	{
		(____value1_marshaled)->Release();
		____value1_marshaled = NULL;
	}

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Uri>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m4032724530 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t840858045* ____ivector_1_t840858045 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t840858045::IID, reinterpret_cast<void**>(&____ivector_1_t840858045));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t840858045->IVector_1_RemoveAt_m4032724530(___index0);
	____ivector_1_t840858045->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Uri>::Append(T)
extern "C"  void IVector_1_Append_m2334270570 (RuntimeObject* __this, Uri_t100236324 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_Append_m2334270570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t840858045* ____ivector_1_t840858045 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t840858045::IID, reinterpret_cast<void**>(&____ivector_1_t840858045));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value0' to native representation
	IUriRuntimeClass_t921050115* ____value0_marshaled = NULL;
	if (___value0 != NULL)
	{
		String_t* ___value0AsString = Uri_get_OriginalString_m2551181575(___value0, NULL);
		Il2CppHString ____value0AsString_marshaled = NULL;
		if (___value0AsString == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("___value0AsString"));
		}

		DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___value0AsStringNativeView, ___value0AsString);
		il2cpp::utils::Il2CppHStringReference ___value0AsStringHStringReference(___value0AsStringNativeView);
		____value0AsString_marshaled = ___value0AsStringHStringReference;
		il2cpp_hresult_t hr = ((Uri_t952691556_StaticFields*)Uri_t952691556_il2cpp_TypeInfo_var->static_fields)->get_____iuriRuntimeClassFactory_t1786313621()->IUriRuntimeClassFactory_CreateUri_m1347622952(____value0AsString_marshaled, (&____value0_marshaled));
		il2cpp_codegen_com_raise_exception_if_failed(hr, false);
	}
	else
	{
		____value0_marshaled = NULL;
	}

	// Native function invocation
	hr = ____ivector_1_t840858045->IVector_1_Append_m2334270570(____value0_marshaled);
	____ivector_1_t840858045->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value0' native representation
	if (____value0_marshaled != NULL)
	{
		(____value0_marshaled)->Release();
		____value0_marshaled = NULL;
	}

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Uri>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m2604786809 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t840858045* ____ivector_1_t840858045 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t840858045::IID, reinterpret_cast<void**>(&____ivector_1_t840858045));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t840858045->IVector_1_RemoveAtEnd_m2604786809();
	____ivector_1_t840858045->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Uri>::Clear()
extern "C"  void IVector_1_Clear_m902403216 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t840858045* ____ivector_1_t840858045 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t840858045::IID, reinterpret_cast<void**>(&____ivector_1_t840858045));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t840858045->IVector_1_Clear_m902403216();
	____ivector_1_t840858045->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Uri>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m3924300725 (RuntimeObject* __this, uint32_t ___startIndex0, UriU5BU5D_t673446605* ___items1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetMany_m3924300725_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t840858045* ____ivector_1_t840858045 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t840858045::IID, reinterpret_cast<void**>(&____ivector_1_t840858045));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	IUriRuntimeClass_t921050115** ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<IUriRuntimeClass_t921050115*>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(IUriRuntimeClass_t921050115*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t840858045->IVector_1_GetMany_m3924300725(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivector_1_t840858045->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			Uri_t100236324 * _____items1_marshaled_i__unmarshaled = NULL;
			if ((____items1_marshaled)[i] != NULL)
			{
				Il2CppHString ______items1_marshaled_i__unmarshaledAsString_marshaled = NULL;
				il2cpp_hresult_t hr = ((____items1_marshaled)[i])->IUriRuntimeClass_get_RawUri_m1303737506((&______items1_marshaled_i__unmarshaledAsString_marshaled));
				il2cpp_codegen_com_raise_exception_if_failed(hr, false);

				String_t* _______items1_marshaled_i__unmarshaledAsString_marshaled_unmarshaled = NULL;
				_______items1_marshaled_i__unmarshaledAsString_marshaled_unmarshaled = il2cpp_codegen_marshal_hstring_result(______items1_marshaled_i__unmarshaledAsString_marshaled);
				il2cpp_codegen_marshal_free_hstring(______items1_marshaled_i__unmarshaledAsString_marshaled);
				______items1_marshaled_i__unmarshaledAsString_marshaled = NULL;

				Uri_t100236324 * _____items1_marshaled_i__unmarshaledTemp = (Uri_t100236324*)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
				Uri__ctor_m1474792120(_____items1_marshaled_i__unmarshaledTemp, _______items1_marshaled_i__unmarshaledAsString_marshaled_unmarshaled, NULL);
				_____items1_marshaled_i__unmarshaled = _____items1_marshaled_i__unmarshaledTemp;
			}
			else
			{
				_____items1_marshaled_i__unmarshaled = NULL;
			}
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items1_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	if (____items1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items1_marshaled_CleanupLoopCount = (___items1 != NULL) ? (___items1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items1_marshaled_CleanupLoopCount); i++)
		{
			if ((____items1_marshaled)[i] != NULL)
			{
				((____items1_marshaled)[i])->Release();
				(____items1_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items1_marshaled);
		____items1_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Uri>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m3525453833 (RuntimeObject* __this, UriU5BU5D_t673446605* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_ReplaceAll_m3525453833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t840858045* ____ivector_1_t840858045 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t840858045::IID, reinterpret_cast<void**>(&____ivector_1_t840858045));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IUriRuntimeClass_t921050115** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IUriRuntimeClass_t921050115*>(static_cast<int32_t>(____items0_marshaledArraySize));
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			if ((___items0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)) != NULL)
			{
				String_t* ___items0_itemAsString = Uri_get_OriginalString_m2551181575((___items0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)), NULL);
				Il2CppHString ____items0_itemAsString_marshaled = NULL;
				if (___items0_itemAsString == NULL)
				{
					IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("___items0_itemAsString"));
				}

				DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___items0_itemAsStringNativeView, ___items0_itemAsString);
				il2cpp::utils::Il2CppHStringReference ___items0_itemAsStringHStringReference(___items0_itemAsStringNativeView);
				____items0_itemAsString_marshaled = ___items0_itemAsStringHStringReference;
				il2cpp_hresult_t hr = ((Uri_t952691556_StaticFields*)Uri_t952691556_il2cpp_TypeInfo_var->static_fields)->get_____iuriRuntimeClassFactory_t1786313621()->IUriRuntimeClassFactory_CreateUri_m1347622952(____items0_itemAsString_marshaled, (&(____items0_marshaled)[i]));
				il2cpp_codegen_com_raise_exception_if_failed(hr, false);
			}
			else
			{
				(____items0_marshaled)[i] = NULL;
			}
		}
	}
	else
	{
		____items0_marshaledArraySize = NULL;
		____items0_marshaled = NULL;
	}

	// Native function invocation
	hr = ____ivector_1_t840858045->IVector_1_ReplaceAll_m3525453833(____items0_marshaledArraySize, ____items0_marshaled);
	____ivector_1_t840858045->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
