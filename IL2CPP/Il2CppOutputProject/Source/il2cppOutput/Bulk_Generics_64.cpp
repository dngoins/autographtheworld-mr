﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Windows.Devices.Haptics.ISimpleHapticsControllerFeedback
struct ISimpleHapticsControllerFeedback_t2199416581;
// Windows.Devices.Haptics.ISimpleHapticsControllerFeedback[]
struct ISimpleHapticsControllerFeedbackU5BU5D_t2466915144;
// Windows.Devices.Haptics.SimpleHapticsControllerFeedback
struct SimpleHapticsControllerFeedback_t913461083;
// Windows.Devices.Haptics.SimpleHapticsControllerFeedback[]
struct SimpleHapticsControllerFeedbackU5BU5D_t1558149338;
// Windows.Foundation.IWwwFormUrlDecoderEntry
struct IWwwFormUrlDecoderEntry_t2943299970;
// Windows.Foundation.IWwwFormUrlDecoderEntry[]
struct IWwwFormUrlDecoderEntryU5BU5D_t40345143;
// Windows.Foundation.Point[]
struct PointU5BU5D_t724808658;
// Windows.Foundation.Rect[]
struct RectU5BU5D_t5657558;
// Windows.Foundation.Size[]
struct SizeU5BU5D_t1671914275;
// Windows.Media.SpeechSynthesis.IVoiceInformation
struct IVoiceInformation_t3111056019;
// Windows.Media.SpeechSynthesis.IVoiceInformation[]
struct IVoiceInformationU5BU5D_t3472394690;
// Windows.Media.SpeechSynthesis.VoiceInformation
struct VoiceInformation_t4266404632;
// Windows.Media.SpeechSynthesis.VoiceInformation[]
struct VoiceInformationU5BU5D_t1943932297;
// Windows.UI.Input.Spatial.ISpatialInteractionSourceState
struct ISpatialInteractionSourceState_t1358829803;
// Windows.UI.Input.Spatial.ISpatialInteractionSourceState[]
struct ISpatialInteractionSourceStateU5BU5D_t513935114;
// Windows.UI.Input.Spatial.ISpatialInteractionSourceState2
struct ISpatialInteractionSourceState2_t2977437931;
// Windows.UI.Input.Spatial.ISpatialInteractionSourceState2[]
struct ISpatialInteractionSourceState2U5BU5D_t3674212106;
// Windows.UI.Input.Spatial.SpatialInteractionSourceState
struct SpatialInteractionSourceState_t1215688063;
// Windows.UI.Input.Spatial.SpatialInteractionSourceState[]
struct SpatialInteractionSourceStateU5BU5D_t1865583014;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Collections.Generic.IReadOnlyDictionary`2<System.Guid,System.Object>
struct IReadOnlyDictionary_2_t2545093093;
// System.Collections.Generic.IReadOnlyDictionary`2<System.Guid,System.Type>
struct IReadOnlyDictionary_2_t1948931689;
// System.Collections.Generic.IReadOnlyDictionary`2<System.Int32,System.Object>
struct IReadOnlyDictionary_2_t2384788891;
// System.Collections.Generic.IReadOnlyDictionary`2<System.Int64,System.Object>
struct IReadOnlyDictionary_2_t263748048;
// System.Collections.Generic.IReadOnlyDictionary`2<System.Object,System.Boolean>
struct IReadOnlyDictionary_2_t1860663645;
// System.Collections.Generic.IReadOnlyDictionary`2<System.Object,System.Int32>
struct IReadOnlyDictionary_2_t419354137;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Reflection.Binder
struct Binder_t2999457153;

extern RuntimeClass* Il2CppComObject_il2cpp_TypeInfo_var;
extern const uint32_t IIterator_1_get_Current_m1134133026_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m253268830_MetadataUsageId;
extern RuntimeClass* SimpleHapticsControllerFeedback_t913461083_il2cpp_TypeInfo_var;
extern const uint32_t IIterator_1_get_Current_m3794219604_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m3370528625_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m1946311790_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m402209304_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m4261329911_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m1097615757_MetadataUsageId;
extern RuntimeClass* VoiceInformation_t4266404632_il2cpp_TypeInfo_var;
extern const uint32_t IIterator_1_get_Current_m4213660447_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m1697528449_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m1347393236_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m3554813579_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m1548962596_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m2124254469_MetadataUsageId;
extern RuntimeClass* SpatialInteractionSourceState_t1215688063_il2cpp_TypeInfo_var;
extern const uint32_t IIterator_1_get_Current_m1757905955_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m852781585_MetadataUsageId;
extern const uint32_t IKeyValuePair_2_get_Value_m52124236_MetadataUsageId;
extern const uint32_t IKeyValuePair_2_get_Value_m210688108_MetadataUsageId;
extern const uint32_t IKeyValuePair_2_get_Value_m1356929877_MetadataUsageId;
extern const uint32_t IKeyValuePair_2_get_Key_m1916408783_MetadataUsageId;
extern const uint32_t IKeyValuePair_2_get_Key_m1066766749_MetadataUsageId;
extern const uint32_t IKeyValuePair_2_get_Key_m2750152149_MetadataUsageId;
extern const uint32_t IKeyValuePair_2_get_Value_m2672088708_MetadataUsageId;
extern const uint32_t IKeyValuePair_2_get_Key_m2722966289_MetadataUsageId;
extern const uint32_t IKeyValuePair_2_get_Value_m3279123715_MetadataUsageId;
extern const uint32_t IKeyValuePair_2_get_Value_m3078130594_MetadataUsageId;
extern const uint32_t IMap_2_Lookup_m3068028334_MetadataUsageId;
extern const uint32_t IMap_2_GetView_m3237694789_MetadataUsageId;
extern const uint32_t IMap_2_GetView_m3956818822_MetadataUsageId;
extern const uint32_t IMap_2_Lookup_m2451726409_MetadataUsageId;
extern const uint32_t IMap_2_GetView_m3361060608_MetadataUsageId;
extern const uint32_t IMap_2_Lookup_m367753107_MetadataUsageId;
extern const uint32_t IMap_2_GetView_m1077656533_MetadataUsageId;
extern const uint32_t IMap_2_GetView_m1166070801_MetadataUsageId;
extern const uint32_t IMap_2_GetView_m2992416671_MetadataUsageId;
struct IMapView_2_t3717796159;
struct ISpatialInteractionSourceState2_t2977437931;
struct ISpatialInteractionSource_t2185314266;
struct ISpatialInteractionSourceState_t1358829803;
struct IWwwFormUrlDecoderEntry_t2943299970;
struct IMapView_2_t1752361405;
struct ISimpleHapticsControllerFeedback_t2199416581;
struct IVoiceInformation_t3111056019;
struct IMapView_2_t1596755316;
struct IMapView_2_t3193670913;
struct IMapView_2_t3281938957;
struct Point_t4164953539 ;
struct Size_t550917638 ;
struct IMapView_2_t3878100361;
struct Rect_t2695113487 ;

struct ISimpleHapticsControllerFeedbackU5BU5D_t2466915144;
struct SimpleHapticsControllerFeedbackU5BU5D_t1558149338;
struct IWwwFormUrlDecoderEntryU5BU5D_t40345143;
struct PointU5BU5D_t724808658;
struct RectU5BU5D_t5657558;
struct SizeU5BU5D_t1671914275;
struct IVoiceInformationU5BU5D_t3472394690;
struct VoiceInformationU5BU5D_t1943932297;
struct ISpatialInteractionSourceStateU5BU5D_t513935114;
struct ISpatialInteractionSourceState2U5BU5D_t3674212106;
struct SpatialInteractionSourceStateU5BU5D_t1865583014;


// Windows.Foundation.Collections.IKeyValuePair`2<System.Type,System.Type>
struct NOVTABLE IKeyValuePair_2_t4055716313 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2294505739(Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m1073902776(Type_t ** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.UInt32,System.Object>
struct NOVTABLE IKeyValuePair_2_t3481798315 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2175672871(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m3078130594(Il2CppIInspectable** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Type>
struct NOVTABLE IKeyValuePair_2_t1396625548 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2199926285(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m1427991677(Type_t ** comReturnValue) = 0;
};
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
// Windows.Foundation.Collections.IMap`2<System.Int32,System.Object>
struct NOVTABLE IMap_2_t618456315 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMap_2_Lookup_m2451726409(int32_t ___key0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_get_Size_m2259249674(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_HasKey_m4208301656(int32_t ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_GetView_m3361060608(IMapView_2_t3717796159** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Insert_m490092833(int32_t ___key0, Il2CppIInspectable* ___value1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Remove_m1067146362(int32_t ___key0) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Clear_m3558300509() = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState2>
struct NOVTABLE IIterator_1_t851581404 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1548962596(ISpatialInteractionSourceState2_t2977437931** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m20766391(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2099795593(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2124254469(uint32_t ___items0ArraySize, ISpatialInteractionSourceState2_t2977437931** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.UI.Input.Spatial.ISpatialInteractionSourceState
struct NOVTABLE ISpatialInteractionSourceState_t1358829803 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL ISpatialInteractionSourceState_get_Source_m497120721(ISpatialInteractionSource_t2185314266** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL ISpatialInteractionSourceState_U24__Stripped0_get_Properties_m1189053370() = 0;
	virtual il2cpp_hresult_t STDCALL ISpatialInteractionSourceState_U24__Stripped1_get_IsPressed_m546887925() = 0;
	virtual il2cpp_hresult_t STDCALL ISpatialInteractionSourceState_U24__Stripped2_get_Timestamp_m1483989895() = 0;
	virtual il2cpp_hresult_t STDCALL ISpatialInteractionSourceState_U24__Stripped3_TryGetPointerPose_m837462904() = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.SpatialInteractionSourceState>
struct NOVTABLE IIterator_1_t3384798832 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1757905955(ISpatialInteractionSourceState_t1358829803** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1287739422(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3375070988(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m852781585(uint32_t ___items0ArraySize, ISpatialInteractionSourceState_t1358829803** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.UI.Input.Spatial.ISpatialInteractionSourceState2
struct NOVTABLE ISpatialInteractionSourceState2_t2977437931 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL ISpatialInteractionSourceState2_U24__Stripped0_get_IsSelectPressed_m1462024929() = 0;
	virtual il2cpp_hresult_t STDCALL ISpatialInteractionSourceState2_U24__Stripped1_get_IsMenuPressed_m2884506468() = 0;
	virtual il2cpp_hresult_t STDCALL ISpatialInteractionSourceState2_U24__Stripped2_get_IsGrasped_m2033866613() = 0;
	virtual il2cpp_hresult_t STDCALL ISpatialInteractionSourceState2_U24__Stripped3_get_SelectPressedValue_m3876541456() = 0;
	virtual il2cpp_hresult_t STDCALL ISpatialInteractionSourceState2_U24__Stripped4_get_ControllerProperties_m1166340460() = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.String>
struct NOVTABLE IKeyValuePair_2_t760131477 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m3013852749(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m2003745203(Il2CppHString* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.Int32>
struct NOVTABLE IKeyValuePair_2_t3425776526 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m1066766749(Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m269040038(int32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.Boolean>
struct NOVTABLE IKeyValuePair_2_t572118738 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m1916408783(Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m3611222262(bool* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.String>
struct NOVTABLE IKeyValuePair_2_t2322281462 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2722966289(Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m3432963267(Il2CppHString* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.Object>
struct NOVTABLE IKeyValuePair_2_t3554936937 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2750152149(Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m2672088708(Il2CppIInspectable** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Boolean>
struct NOVTABLE IKeyValuePair_2_t3304936049 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2534079307(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m2353902038(bool* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Object>
struct NOVTABLE IKeyValuePair_2_t1992786952 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m3523589551(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m3279123715(Il2CppIInspectable** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.Int32,System.Int32>
struct NOVTABLE IKeyValuePair_2_t967083573 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2344176952(int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m1590219272(int32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Int32>
struct NOVTABLE IKeyValuePair_2_t1863626541 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m599934735(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m2337749369(int32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.Int32,System.Object>
struct NOVTABLE IKeyValuePair_2_t1096243984 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2186241555(int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m210688108(Il2CppIInspectable** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState>
struct NOVTABLE IIterator_1_t3527940572 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1347393236(ISpatialInteractionSourceState_t1358829803** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1096701536(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1173429162(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3554813579(uint32_t ___items0ArraySize, ISpatialInteractionSourceState_t1358829803** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.IWwwFormUrlDecoderEntry
struct NOVTABLE IWwwFormUrlDecoderEntry_t2943299970 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IWwwFormUrlDecoderEntry_U24__Stripped0_get_Name_m1841419405() = 0;
	virtual il2cpp_hresult_t STDCALL IWwwFormUrlDecoderEntry_U24__Stripped1_get_Value_m2688400907() = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.Foundation.IWwwFormUrlDecoderEntry>
struct NOVTABLE IIterator_1_t817443443 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1946311790(IWwwFormUrlDecoderEntry_t2943299970** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m4030884216(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m564191597(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m402209304(uint32_t ___items0ArraySize, IWwwFormUrlDecoderEntry_t2943299970** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IMap`2<System.Object,System.Int32>
struct NOVTABLE IMap_2_t2947988857 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMap_2_Lookup_m4271171535(Il2CppIInspectable* ___key0, int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_get_Size_m672085459(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_HasKey_m377757278(Il2CppIInspectable* ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_GetView_m2992416671(IMapView_2_t1752361405** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Insert_m3186075525(Il2CppIInspectable* ___key0, int32_t ___value1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Remove_m1560976792(Il2CppIInspectable* ___key0) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Clear_m1661948797() = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.Object,System.Int32>
struct NOVTABLE IMapView_2_t1752361405 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m877167602(Il2CppIInspectable* ___key0, int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m3871500701(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m1872447125(Il2CppIInspectable* ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m4061037733(IMapView_2_t1752361405** ___first0, IMapView_2_t1752361405** ___second1) = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.Devices.Haptics.ISimpleHapticsControllerFeedback>
struct NOVTABLE IIterator_1_t73560054 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1134133026(ISimpleHapticsControllerFeedback_t2199416581** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m497198888(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m683426578(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m253268830(uint32_t ___items0ArraySize, ISimpleHapticsControllerFeedback_t2199416581** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.Int64,System.Object>
struct NOVTABLE IKeyValuePair_2_t3270170437 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m1937891137(int64_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m1356929877(Il2CppIInspectable** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.Devices.Haptics.SimpleHapticsControllerFeedback>
struct NOVTABLE IIterator_1_t3082571852 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3794219604(ISimpleHapticsControllerFeedback_t2199416581** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3051985185(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m4247690978(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3370528625(uint32_t ___items0ArraySize, ISimpleHapticsControllerFeedback_t2199416581** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Devices.Haptics.ISimpleHapticsControllerFeedback
struct NOVTABLE ISimpleHapticsControllerFeedback_t2199416581 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL ISimpleHapticsControllerFeedback_get_Waveform_m2893722823(uint16_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL ISimpleHapticsControllerFeedback_U24__Stripped0_get_Duration_m1134348836() = 0;
};
// Windows.Media.SpeechSynthesis.IVoiceInformation
struct NOVTABLE IVoiceInformation_t3111056019 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVoiceInformation_get_DisplayName_m1429510609(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVoiceInformation_U24__Stripped0_get_Id_m1776229927() = 0;
	virtual il2cpp_hresult_t STDCALL IVoiceInformation_U24__Stripped1_get_Language_m1418474925() = 0;
	virtual il2cpp_hresult_t STDCALL IVoiceInformation_U24__Stripped2_get_Description_m153214867() = 0;
	virtual il2cpp_hresult_t STDCALL IVoiceInformation_U24__Stripped3_get_Gender_m2588825291() = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.Media.SpeechSynthesis.IVoiceInformation>
struct NOVTABLE IIterator_1_t985199492 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m4261329911(IVoiceInformation_t3111056019** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2587194256(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1083425478(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1097615757(uint32_t ___items0ArraySize, IVoiceInformation_t3111056019** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.Int32,System.Object>
struct NOVTABLE IMapView_2_t3717796159 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m3742065343(int32_t ___key0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m1359240774(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m973232472(int32_t ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m842968636(IMapView_2_t3717796159** ___first0, IMapView_2_t3717796159** ___second1) = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.Media.SpeechSynthesis.VoiceInformation>
struct NOVTABLE IIterator_1_t2140548105 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m4213660447(IVoiceInformation_t3111056019** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1766660868(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m112683399(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1697528449(uint32_t ___items0ArraySize, IVoiceInformation_t3111056019** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.Int64,System.Object>
struct NOVTABLE IMapView_2_t1596755316 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m2833276555(int64_t ___key0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m2657859418(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m1129223030(int64_t ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m2875932391(IMapView_2_t1596755316** ___first0, IMapView_2_t1596755316** ___second1) = 0;
};
// Windows.Foundation.Collections.IMap`2<System.Object,System.Boolean>
struct NOVTABLE IMap_2_t94331069 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMap_2_Lookup_m2791672174(Il2CppIInspectable* ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_get_Size_m521129328(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_HasKey_m3863327211(Il2CppIInspectable* ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_GetView_m1166070801(IMapView_2_t3193670913** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Insert_m3431746527(Il2CppIInspectable* ___key0, bool ___value1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Remove_m1939883922(Il2CppIInspectable* ___key0) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Clear_m403209908() = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.Object,System.Boolean>
struct NOVTABLE IMapView_2_t3193670913 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m4048973909(Il2CppIInspectable* ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m516432122(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m1413942596(Il2CppIInspectable* ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m2647546983(IMapView_2_t3193670913** ___first0, IMapView_2_t3193670913** ___second1) = 0;
};
// Windows.Foundation.Collections.IMap`2<System.Int64,System.Object>
struct NOVTABLE IMap_2_t2792382768 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMap_2_Lookup_m367753107(int64_t ___key0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_get_Size_m137360363(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_HasKey_m663731653(int64_t ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_GetView_m1077656533(IMapView_2_t1596755316** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Insert_m2086881606(int64_t ___key0, Il2CppIInspectable* ___value1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Remove_m2064551222(int64_t ___key0) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Clear_m156915013() = 0;
};
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef IL2CPPCOMOBJECT_H
#define IL2CPPCOMOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.__Il2CppComObject

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IL2CPPCOMOBJECT_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef RECT_T2695113487_H
#define RECT_T2695113487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Rect
struct  Rect_t2695113487 
{
public:
	// System.Single Windows.Foundation.Rect::_x
	float ____x_0;
	// System.Single Windows.Foundation.Rect::_y
	float ____y_1;
	// System.Single Windows.Foundation.Rect::_width
	float ____width_2;
	// System.Single Windows.Foundation.Rect::_height
	float ____height_3;

public:
	inline static int32_t get_offset_of__x_0() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____x_0)); }
	inline float get__x_0() const { return ____x_0; }
	inline float* get_address_of__x_0() { return &____x_0; }
	inline void set__x_0(float value)
	{
		____x_0 = value;
	}

	inline static int32_t get_offset_of__y_1() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____y_1)); }
	inline float get__y_1() const { return ____y_1; }
	inline float* get_address_of__y_1() { return &____y_1; }
	inline void set__y_1(float value)
	{
		____y_1 = value;
	}

	inline static int32_t get_offset_of__width_2() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____width_2)); }
	inline float get__width_2() const { return ____width_2; }
	inline float* get_address_of__width_2() { return &____width_2; }
	inline void set__width_2(float value)
	{
		____width_2 = value;
	}

	inline static int32_t get_offset_of__height_3() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____height_3)); }
	inline float get__height_3() const { return ____height_3; }
	inline float* get_address_of__height_3() { return &____height_3; }
	inline void set__height_3(float value)
	{
		____height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2695113487_H
#ifndef POINT_T4164953539_H
#define POINT_T4164953539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Point
struct  Point_t4164953539 
{
public:
	// System.Single Windows.Foundation.Point::_x
	float ____x_0;
	// System.Single Windows.Foundation.Point::_y
	float ____y_1;

public:
	inline static int32_t get_offset_of__x_0() { return static_cast<int32_t>(offsetof(Point_t4164953539, ____x_0)); }
	inline float get__x_0() const { return ____x_0; }
	inline float* get_address_of__x_0() { return &____x_0; }
	inline void set__x_0(float value)
	{
		____x_0 = value;
	}

	inline static int32_t get_offset_of__y_1() { return static_cast<int32_t>(offsetof(Point_t4164953539, ____y_1)); }
	inline float get__y_1() const { return ____y_1; }
	inline float* get_address_of__y_1() { return &____y_1; }
	inline void set__y_1(float value)
	{
		____y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT_T4164953539_H
#ifndef SIZE_T550917638_H
#define SIZE_T550917638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Size
struct  Size_t550917638 
{
public:
	// System.Single Windows.Foundation.Size::_width
	float ____width_0;
	// System.Single Windows.Foundation.Size::_height
	float ____height_1;

public:
	inline static int32_t get_offset_of__width_0() { return static_cast<int32_t>(offsetof(Size_t550917638, ____width_0)); }
	inline float get__width_0() const { return ____width_0; }
	inline float* get_address_of__width_0() { return &____width_0; }
	inline void set__width_0(float value)
	{
		____width_0 = value;
	}

	inline static int32_t get_offset_of__height_1() { return static_cast<int32_t>(offsetof(Size_t550917638, ____height_1)); }
	inline float get__height_1() const { return ____height_1; }
	inline float* get_address_of__height_1() { return &____height_1; }
	inline void set__height_1(float value)
	{
		____height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIZE_T550917638_H
#ifndef SPATIALINTERACTIONSOURCESTATE_T1215688063_H
#define SPATIALINTERACTIONSOURCESTATE_T1215688063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.UI.Input.Spatial.SpatialInteractionSourceState
struct  SpatialInteractionSourceState_t1215688063  : public Il2CppComObject
{
public:
	// Cached pointer to Windows.UI.Input.Spatial.ISpatialInteractionSourceState
	ISpatialInteractionSourceState_t1358829803* ____ispatialInteractionSourceState_t1358829803;
	// Cached pointer to Windows.UI.Input.Spatial.ISpatialInteractionSourceState2
	ISpatialInteractionSourceState2_t2977437931* ____ispatialInteractionSourceState2_t2977437931;

public:
	inline ISpatialInteractionSourceState_t1358829803* get_____ispatialInteractionSourceState_t1358829803()
	{
		ISpatialInteractionSourceState_t1358829803* returnValue = ____ispatialInteractionSourceState_t1358829803;
		if (returnValue == NULL)
		{
			const il2cpp_hresult_t hr = identity->QueryInterface(ISpatialInteractionSourceState_t1358829803::IID, reinterpret_cast<void**>(&returnValue));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (il2cpp_codegen_atomic_compare_exchange_pointer<ISpatialInteractionSourceState_t1358829803>((&____ispatialInteractionSourceState_t1358829803), returnValue, NULL) != NULL)
			{
				returnValue->Release();
				returnValue = ____ispatialInteractionSourceState_t1358829803;
			}
		}
		return returnValue;
	}

	inline ISpatialInteractionSourceState2_t2977437931* get_____ispatialInteractionSourceState2_t2977437931()
	{
		ISpatialInteractionSourceState2_t2977437931* returnValue = ____ispatialInteractionSourceState2_t2977437931;
		if (returnValue == NULL)
		{
			const il2cpp_hresult_t hr = identity->QueryInterface(ISpatialInteractionSourceState2_t2977437931::IID, reinterpret_cast<void**>(&returnValue));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (il2cpp_codegen_atomic_compare_exchange_pointer<ISpatialInteractionSourceState2_t2977437931>((&____ispatialInteractionSourceState2_t2977437931), returnValue, NULL) != NULL)
			{
				returnValue->Release();
				returnValue = ____ispatialInteractionSourceState2_t2977437931;
			}
		}
		return returnValue;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALINTERACTIONSOURCESTATE_T1215688063_H
#ifndef VOICEINFORMATION_T4266404632_H
#define VOICEINFORMATION_T4266404632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Media.SpeechSynthesis.VoiceInformation
struct  VoiceInformation_t4266404632  : public Il2CppComObject
{
public:
	// Cached pointer to Windows.Media.SpeechSynthesis.IVoiceInformation
	IVoiceInformation_t3111056019* ____ivoiceInformation_t3111056019;

public:
	inline IVoiceInformation_t3111056019* get_____ivoiceInformation_t3111056019()
	{
		IVoiceInformation_t3111056019* returnValue = ____ivoiceInformation_t3111056019;
		if (returnValue == NULL)
		{
			const il2cpp_hresult_t hr = identity->QueryInterface(IVoiceInformation_t3111056019::IID, reinterpret_cast<void**>(&returnValue));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (il2cpp_codegen_atomic_compare_exchange_pointer<IVoiceInformation_t3111056019>((&____ivoiceInformation_t3111056019), returnValue, NULL) != NULL)
			{
				returnValue->Release();
				returnValue = ____ivoiceInformation_t3111056019;
			}
		}
		return returnValue;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOICEINFORMATION_T4266404632_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef SIMPLEHAPTICSCONTROLLERFEEDBACK_T913461083_H
#define SIMPLEHAPTICSCONTROLLERFEEDBACK_T913461083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Devices.Haptics.SimpleHapticsControllerFeedback
struct  SimpleHapticsControllerFeedback_t913461083  : public Il2CppComObject
{
public:
	// Cached pointer to Windows.Devices.Haptics.ISimpleHapticsControllerFeedback
	ISimpleHapticsControllerFeedback_t2199416581* ____isimpleHapticsControllerFeedback_t2199416581;

public:
	inline ISimpleHapticsControllerFeedback_t2199416581* get_____isimpleHapticsControllerFeedback_t2199416581()
	{
		ISimpleHapticsControllerFeedback_t2199416581* returnValue = ____isimpleHapticsControllerFeedback_t2199416581;
		if (returnValue == NULL)
		{
			const il2cpp_hresult_t hr = identity->QueryInterface(ISimpleHapticsControllerFeedback_t2199416581::IID, reinterpret_cast<void**>(&returnValue));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (il2cpp_codegen_atomic_compare_exchange_pointer<ISimpleHapticsControllerFeedback_t2199416581>((&____isimpleHapticsControllerFeedback_t2199416581), returnValue, NULL) != NULL)
			{
				returnValue->Release();
				returnValue = ____isimpleHapticsControllerFeedback_t2199416581;
			}
		}
		return returnValue;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEHAPTICSCONTROLLERFEEDBACK_T913461083_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_4;
	// System.Int16 System.Guid::_b
	int16_t ____b_5;
	// System.Int16 System.Guid::_c
	int16_t ____c_6;
	// System.Byte System.Guid::_d
	uint8_t ____d_7;
	// System.Byte System.Guid::_e
	uint8_t ____e_8;
	// System.Byte System.Guid::_f
	uint8_t ____f_9;
	// System.Byte System.Guid::_g
	uint8_t ____g_10;
	// System.Byte System.Guid::_h
	uint8_t ____h_11;
	// System.Byte System.Guid::_i
	uint8_t ____i_12;
	// System.Byte System.Guid::_j
	uint8_t ____j_13;
	// System.Byte System.Guid::_k
	uint8_t ____k_14;

public:
	inline static int32_t get_offset_of__a_4() { return static_cast<int32_t>(offsetof(Guid_t, ____a_4)); }
	inline int32_t get__a_4() const { return ____a_4; }
	inline int32_t* get_address_of__a_4() { return &____a_4; }
	inline void set__a_4(int32_t value)
	{
		____a_4 = value;
	}

	inline static int32_t get_offset_of__b_5() { return static_cast<int32_t>(offsetof(Guid_t, ____b_5)); }
	inline int16_t get__b_5() const { return ____b_5; }
	inline int16_t* get_address_of__b_5() { return &____b_5; }
	inline void set__b_5(int16_t value)
	{
		____b_5 = value;
	}

	inline static int32_t get_offset_of__c_6() { return static_cast<int32_t>(offsetof(Guid_t, ____c_6)); }
	inline int16_t get__c_6() const { return ____c_6; }
	inline int16_t* get_address_of__c_6() { return &____c_6; }
	inline void set__c_6(int16_t value)
	{
		____c_6 = value;
	}

	inline static int32_t get_offset_of__d_7() { return static_cast<int32_t>(offsetof(Guid_t, ____d_7)); }
	inline uint8_t get__d_7() const { return ____d_7; }
	inline uint8_t* get_address_of__d_7() { return &____d_7; }
	inline void set__d_7(uint8_t value)
	{
		____d_7 = value;
	}

	inline static int32_t get_offset_of__e_8() { return static_cast<int32_t>(offsetof(Guid_t, ____e_8)); }
	inline uint8_t get__e_8() const { return ____e_8; }
	inline uint8_t* get_address_of__e_8() { return &____e_8; }
	inline void set__e_8(uint8_t value)
	{
		____e_8 = value;
	}

	inline static int32_t get_offset_of__f_9() { return static_cast<int32_t>(offsetof(Guid_t, ____f_9)); }
	inline uint8_t get__f_9() const { return ____f_9; }
	inline uint8_t* get_address_of__f_9() { return &____f_9; }
	inline void set__f_9(uint8_t value)
	{
		____f_9 = value;
	}

	inline static int32_t get_offset_of__g_10() { return static_cast<int32_t>(offsetof(Guid_t, ____g_10)); }
	inline uint8_t get__g_10() const { return ____g_10; }
	inline uint8_t* get_address_of__g_10() { return &____g_10; }
	inline void set__g_10(uint8_t value)
	{
		____g_10 = value;
	}

	inline static int32_t get_offset_of__h_11() { return static_cast<int32_t>(offsetof(Guid_t, ____h_11)); }
	inline uint8_t get__h_11() const { return ____h_11; }
	inline uint8_t* get_address_of__h_11() { return &____h_11; }
	inline void set__h_11(uint8_t value)
	{
		____h_11 = value;
	}

	inline static int32_t get_offset_of__i_12() { return static_cast<int32_t>(offsetof(Guid_t, ____i_12)); }
	inline uint8_t get__i_12() const { return ____i_12; }
	inline uint8_t* get_address_of__i_12() { return &____i_12; }
	inline void set__i_12(uint8_t value)
	{
		____i_12 = value;
	}

	inline static int32_t get_offset_of__j_13() { return static_cast<int32_t>(offsetof(Guid_t, ____j_13)); }
	inline uint8_t get__j_13() const { return ____j_13; }
	inline uint8_t* get_address_of__j_13() { return &____j_13; }
	inline void set__j_13(uint8_t value)
	{
		____j_13 = value;
	}

	inline static int32_t get_offset_of__k_14() { return static_cast<int32_t>(offsetof(Guid_t, ____k_14)); }
	inline uint8_t get__k_14() const { return ____k_14; }
	inline uint8_t* get_address_of__k_14() { return &____k_14; }
	inline void set__k_14(uint8_t value)
	{
		____k_14 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_0;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_1;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_2;
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_3;

public:
	inline static int32_t get_offset_of__rngAccess_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_0)); }
	inline RuntimeObject * get__rngAccess_0() const { return ____rngAccess_0; }
	inline RuntimeObject ** get_address_of__rngAccess_0() { return &____rngAccess_0; }
	inline void set__rngAccess_0(RuntimeObject * value)
	{
		____rngAccess_0 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_0), value);
	}

	inline static int32_t get_offset_of__rng_1() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_1)); }
	inline RandomNumberGenerator_t386037858 * get__rng_1() const { return ____rng_1; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_1() { return &____rng_1; }
	inline void set__rng_1(RandomNumberGenerator_t386037858 * value)
	{
		____rng_1 = value;
		Il2CppCodeGenWriteBarrier((&____rng_1), value);
	}

	inline static int32_t get_offset_of__fastRng_2() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_2)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_2() const { return ____fastRng_2; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_2() { return &____fastRng_2; }
	inline void set__fastRng_2(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_2 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_2), value);
	}

	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_3)); }
	inline Guid_t  get_Empty_3() const { return ___Empty_3; }
	inline Guid_t * get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(Guid_t  value)
	{
		___Empty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
// Windows.Foundation.Collections.IKeyValuePair`2<System.Guid,System.Type>
struct NOVTABLE IKeyValuePair_2_t660386782 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m1247672299(Guid_t * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m3450640679(Type_t ** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.Guid,System.Type>
struct NOVTABLE IMapView_2_t3281938957 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m3896662699(Guid_t  ___key0, Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m2349127300(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m1382000797(Guid_t  ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m2592840147(IMapView_2_t3281938957** ___first0, IMapView_2_t3281938957** ___second1) = 0;
};
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
// Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Point>
struct NOVTABLE IIterator_1_t2039097012 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3650755423(Point_t4164953539 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m941782850(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1050410819(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1958449181(uint32_t ___items0ArraySize, Point_t4164953539 * ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Size>
struct NOVTABLE IIterator_1_t2720028407 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m573419872(Size_t550917638 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m4223115842(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2301142642(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2384573303(uint32_t ___items0ArraySize, Size_t550917638 * ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.Guid,System.Object>
struct NOVTABLE IMapView_2_t3878100361 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m3331997343(Guid_t  ___key0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m3858358881(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m3111850129(Guid_t  ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m3394162196(IMapView_2_t3878100361** ___first0, IMapView_2_t3878100361** ___second1) = 0;
};
// Windows.Foundation.Collections.IMap`2<System.Guid,System.Type>
struct NOVTABLE IMap_2_t182599113 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMap_2_Lookup_m372021310(Guid_t  ___key0, Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_get_Size_m4209346384(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_HasKey_m2767397819(Guid_t  ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_GetView_m3956818822(IMapView_2_t3281938957** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Insert_m52862695(Guid_t  ___key0, Type_t * ___value1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Remove_m441912298(Guid_t  ___key0) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Clear_m1518512299() = 0;
};
// Windows.Foundation.Collections.IMap`2<System.Guid,System.Object>
struct NOVTABLE IMap_2_t778760517 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMap_2_Lookup_m3068028334(Guid_t  ___key0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_get_Size_m1704379977(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_HasKey_m2236943020(Guid_t  ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_GetView_m3237694789(IMapView_2_t3878100361** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Insert_m1575381716(Guid_t  ___key0, Il2CppIInspectable* ___value1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Remove_m3289693839(Guid_t  ___key0) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Clear_m2519350850() = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.Guid,System.Object>
struct NOVTABLE IKeyValuePair_2_t1256548186 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m3324945768(Guid_t * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m52124236(Il2CppIInspectable** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Rect>
struct NOVTABLE IIterator_1_t569256960 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1183023450(Rect_t2695113487 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1554425837(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m328760485(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m269641506(uint32_t ___items0ArraySize, Rect_t2695113487 * ___items0, uint32_t* comReturnValue) = 0;
};
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_0;

public:
	inline static int32_t get_offset_of__impl_0() { return static_cast<int32_t>(offsetof(Type_t, ____impl_0)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_0() const { return ____impl_0; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_0() { return &____impl_0; }
	inline void set__impl_0(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_0 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_1;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_2;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_3;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_4;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_5;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_6;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2999457153 * ___defaultBinder_7;

public:
	inline static int32_t get_offset_of_FilterAttribute_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_1)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_1() const { return ___FilterAttribute_1; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_1() { return &___FilterAttribute_1; }
	inline void set_FilterAttribute_1(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_1), value);
	}

	inline static int32_t get_offset_of_FilterName_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_2)); }
	inline MemberFilter_t426314064 * get_FilterName_2() const { return ___FilterName_2; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_2() { return &___FilterName_2; }
	inline void set_FilterName_2(MemberFilter_t426314064 * value)
	{
		___FilterName_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_2), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_3)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_3() const { return ___FilterNameIgnoreCase_3; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_3() { return &___FilterNameIgnoreCase_3; }
	inline void set_FilterNameIgnoreCase_3(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_3 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_3), value);
	}

	inline static int32_t get_offset_of_Missing_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_4)); }
	inline RuntimeObject * get_Missing_4() const { return ___Missing_4; }
	inline RuntimeObject ** get_address_of_Missing_4() { return &___Missing_4; }
	inline void set_Missing_4(RuntimeObject * value)
	{
		___Missing_4 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_4), value);
	}

	inline static int32_t get_offset_of_Delimiter_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_5)); }
	inline Il2CppChar get_Delimiter_5() const { return ___Delimiter_5; }
	inline Il2CppChar* get_address_of_Delimiter_5() { return &___Delimiter_5; }
	inline void set_Delimiter_5(Il2CppChar value)
	{
		___Delimiter_5 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_6)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_6() const { return ___EmptyTypes_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_6() { return &___EmptyTypes_6; }
	inline void set_EmptyTypes_6(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_6 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_6), value);
	}

	inline static int32_t get_offset_of_defaultBinder_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_7)); }
	inline Binder_t2999457153 * get_defaultBinder_7() const { return ___defaultBinder_7; }
	inline Binder_t2999457153 ** get_address_of_defaultBinder_7() { return &___defaultBinder_7; }
	inline void set_defaultBinder_7(Binder_t2999457153 * value)
	{
		___defaultBinder_7 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
// Windows.Devices.Haptics.ISimpleHapticsControllerFeedback[]
struct ISimpleHapticsControllerFeedbackU5BU5D_t2466915144  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Windows.Devices.Haptics.SimpleHapticsControllerFeedback[]
struct SimpleHapticsControllerFeedbackU5BU5D_t1558149338  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) SimpleHapticsControllerFeedback_t913461083 * m_Items[1];

public:
	inline SimpleHapticsControllerFeedback_t913461083 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SimpleHapticsControllerFeedback_t913461083 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SimpleHapticsControllerFeedback_t913461083 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SimpleHapticsControllerFeedback_t913461083 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SimpleHapticsControllerFeedback_t913461083 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SimpleHapticsControllerFeedback_t913461083 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Windows.Foundation.IWwwFormUrlDecoderEntry[]
struct IWwwFormUrlDecoderEntryU5BU5D_t40345143  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Windows.Foundation.Point[]
struct PointU5BU5D_t724808658  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Point_t4164953539  m_Items[1];

public:
	inline Point_t4164953539  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Point_t4164953539 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Point_t4164953539  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Point_t4164953539  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Point_t4164953539 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Point_t4164953539  value)
	{
		m_Items[index] = value;
	}
};
// Windows.Foundation.Rect[]
struct RectU5BU5D_t5657558  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Rect_t2695113487  m_Items[1];

public:
	inline Rect_t2695113487  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Rect_t2695113487 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Rect_t2695113487  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Rect_t2695113487  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Rect_t2695113487 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Rect_t2695113487  value)
	{
		m_Items[index] = value;
	}
};
// Windows.Foundation.Size[]
struct SizeU5BU5D_t1671914275  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Size_t550917638  m_Items[1];

public:
	inline Size_t550917638  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Size_t550917638 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Size_t550917638  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Size_t550917638  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Size_t550917638 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Size_t550917638  value)
	{
		m_Items[index] = value;
	}
};
// Windows.Media.SpeechSynthesis.IVoiceInformation[]
struct IVoiceInformationU5BU5D_t3472394690  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Windows.Media.SpeechSynthesis.VoiceInformation[]
struct VoiceInformationU5BU5D_t1943932297  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) VoiceInformation_t4266404632 * m_Items[1];

public:
	inline VoiceInformation_t4266404632 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline VoiceInformation_t4266404632 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, VoiceInformation_t4266404632 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline VoiceInformation_t4266404632 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline VoiceInformation_t4266404632 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, VoiceInformation_t4266404632 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Windows.UI.Input.Spatial.ISpatialInteractionSourceState[]
struct ISpatialInteractionSourceStateU5BU5D_t513935114  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Windows.UI.Input.Spatial.ISpatialInteractionSourceState2[]
struct ISpatialInteractionSourceState2U5BU5D_t3674212106  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Windows.UI.Input.Spatial.SpatialInteractionSourceState[]
struct SpatialInteractionSourceStateU5BU5D_t1865583014  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) SpatialInteractionSourceState_t1215688063 * m_Items[1];

public:
	inline SpatialInteractionSourceState_t1215688063 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SpatialInteractionSourceState_t1215688063 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SpatialInteractionSourceState_t1215688063 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SpatialInteractionSourceState_t1215688063 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SpatialInteractionSourceState_t1215688063 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SpatialInteractionSourceState_t1215688063 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// T Windows.Foundation.Collections.IIterator`1<Windows.Devices.Haptics.ISimpleHapticsControllerFeedback>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m1134133026 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m1134133026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t73560054* ____iiterator_1_t73560054 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t73560054::IID, reinterpret_cast<void**>(&____iiterator_1_t73560054));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	ISimpleHapticsControllerFeedback_t2199416581* returnValue = NULL;
	hr = ____iiterator_1_t73560054->IIterator_1_get_Current_m1134133026(&returnValue);
	____iiterator_1_t73560054->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Devices.Haptics.ISimpleHapticsControllerFeedback>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m497198888 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t73560054* ____iiterator_1_t73560054 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t73560054::IID, reinterpret_cast<void**>(&____iiterator_1_t73560054));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t73560054->IIterator_1_get_HasCurrent_m497198888(&returnValue);
	____iiterator_1_t73560054->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Devices.Haptics.ISimpleHapticsControllerFeedback>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m683426578 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t73560054* ____iiterator_1_t73560054 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t73560054::IID, reinterpret_cast<void**>(&____iiterator_1_t73560054));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t73560054->IIterator_1_MoveNext_m683426578(&returnValue);
	____iiterator_1_t73560054->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<Windows.Devices.Haptics.ISimpleHapticsControllerFeedback>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m253268830 (RuntimeObject* __this, ISimpleHapticsControllerFeedbackU5BU5D_t2466915144* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m253268830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t73560054* ____iiterator_1_t73560054 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t73560054::IID, reinterpret_cast<void**>(&____iiterator_1_t73560054));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	ISimpleHapticsControllerFeedback_t2199416581** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<ISimpleHapticsControllerFeedback_t2199416581*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(ISimpleHapticsControllerFeedback_t2199416581*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t73560054->IIterator_1_GetMany_m253268830(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t73560054->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<Windows.Devices.Haptics.SimpleHapticsControllerFeedback>::get_Current()
extern "C"  SimpleHapticsControllerFeedback_t913461083 * IIterator_1_get_Current_m3794219604 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m3794219604_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3082571852* ____iiterator_1_t3082571852 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3082571852::IID, reinterpret_cast<void**>(&____iiterator_1_t3082571852));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	ISimpleHapticsControllerFeedback_t2199416581* returnValue = NULL;
	hr = ____iiterator_1_t3082571852->IIterator_1_get_Current_m3794219604(&returnValue);
	____iiterator_1_t3082571852->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	SimpleHapticsControllerFeedback_t913461083 * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_for_sealed_class<SimpleHapticsControllerFeedback_t913461083>(returnValue, SimpleHapticsControllerFeedback_t913461083_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Devices.Haptics.SimpleHapticsControllerFeedback>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m3051985185 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3082571852* ____iiterator_1_t3082571852 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3082571852::IID, reinterpret_cast<void**>(&____iiterator_1_t3082571852));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3082571852->IIterator_1_get_HasCurrent_m3051985185(&returnValue);
	____iiterator_1_t3082571852->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Devices.Haptics.SimpleHapticsControllerFeedback>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m4247690978 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3082571852* ____iiterator_1_t3082571852 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3082571852::IID, reinterpret_cast<void**>(&____iiterator_1_t3082571852));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3082571852->IIterator_1_MoveNext_m4247690978(&returnValue);
	____iiterator_1_t3082571852->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<Windows.Devices.Haptics.SimpleHapticsControllerFeedback>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m3370528625 (RuntimeObject* __this, SimpleHapticsControllerFeedbackU5BU5D_t1558149338* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m3370528625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3082571852* ____iiterator_1_t3082571852 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3082571852::IID, reinterpret_cast<void**>(&____iiterator_1_t3082571852));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	ISimpleHapticsControllerFeedback_t2199416581** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<ISimpleHapticsControllerFeedback_t2199416581*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(ISimpleHapticsControllerFeedback_t2199416581*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t3082571852->IIterator_1_GetMany_m3370528625(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t3082571852->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			SimpleHapticsControllerFeedback_t913461083 * _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_for_sealed_class<SimpleHapticsControllerFeedback_t913461083>((____items0_marshaled)[i], SimpleHapticsControllerFeedback_t913461083_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<Windows.Foundation.IWwwFormUrlDecoderEntry>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m1946311790 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m1946311790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t817443443* ____iiterator_1_t817443443 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t817443443::IID, reinterpret_cast<void**>(&____iiterator_1_t817443443));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IWwwFormUrlDecoderEntry_t2943299970* returnValue = NULL;
	hr = ____iiterator_1_t817443443->IIterator_1_get_Current_m1946311790(&returnValue);
	____iiterator_1_t817443443->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Foundation.IWwwFormUrlDecoderEntry>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m4030884216 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t817443443* ____iiterator_1_t817443443 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t817443443::IID, reinterpret_cast<void**>(&____iiterator_1_t817443443));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t817443443->IIterator_1_get_HasCurrent_m4030884216(&returnValue);
	____iiterator_1_t817443443->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Foundation.IWwwFormUrlDecoderEntry>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m564191597 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t817443443* ____iiterator_1_t817443443 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t817443443::IID, reinterpret_cast<void**>(&____iiterator_1_t817443443));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t817443443->IIterator_1_MoveNext_m564191597(&returnValue);
	____iiterator_1_t817443443->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<Windows.Foundation.IWwwFormUrlDecoderEntry>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m402209304 (RuntimeObject* __this, IWwwFormUrlDecoderEntryU5BU5D_t40345143* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m402209304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t817443443* ____iiterator_1_t817443443 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t817443443::IID, reinterpret_cast<void**>(&____iiterator_1_t817443443));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IWwwFormUrlDecoderEntry_t2943299970** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IWwwFormUrlDecoderEntry_t2943299970*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IWwwFormUrlDecoderEntry_t2943299970*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t817443443->IIterator_1_GetMany_m402209304(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t817443443->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Point>::get_Current()
extern "C"  Point_t4164953539  IIterator_1_get_Current_m3650755423 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2039097012* ____iiterator_1_t2039097012 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2039097012::IID, reinterpret_cast<void**>(&____iiterator_1_t2039097012));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Point_t4164953539  returnValue = {};
	hr = ____iiterator_1_t2039097012->IIterator_1_get_Current_m3650755423(&returnValue);
	____iiterator_1_t2039097012->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Point>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m941782850 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2039097012* ____iiterator_1_t2039097012 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2039097012::IID, reinterpret_cast<void**>(&____iiterator_1_t2039097012));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2039097012->IIterator_1_get_HasCurrent_m941782850(&returnValue);
	____iiterator_1_t2039097012->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Point>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m1050410819 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2039097012* ____iiterator_1_t2039097012 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2039097012::IID, reinterpret_cast<void**>(&____iiterator_1_t2039097012));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2039097012->IIterator_1_MoveNext_m1050410819(&returnValue);
	____iiterator_1_t2039097012->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Point>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m1958449181 (RuntimeObject* __this, PointU5BU5D_t724808658* ___items0, const RuntimeMethod* method)
{
	IIterator_1_t2039097012* ____iiterator_1_t2039097012 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2039097012::IID, reinterpret_cast<void**>(&____iiterator_1_t2039097012));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	Point_t4164953539 * ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<Point_t4164953539 >(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(Point_t4164953539 ));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t2039097012->IIterator_1_GetMany_m1958449181(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t2039097012->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items0_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	il2cpp_codegen_marshal_free(____items0_marshaled);
	____items0_marshaled = NULL;

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Rect>::get_Current()
extern "C"  Rect_t2695113487  IIterator_1_get_Current_m1183023450 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t569256960* ____iiterator_1_t569256960 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t569256960::IID, reinterpret_cast<void**>(&____iiterator_1_t569256960));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Rect_t2695113487  returnValue = {};
	hr = ____iiterator_1_t569256960->IIterator_1_get_Current_m1183023450(&returnValue);
	____iiterator_1_t569256960->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Rect>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m1554425837 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t569256960* ____iiterator_1_t569256960 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t569256960::IID, reinterpret_cast<void**>(&____iiterator_1_t569256960));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t569256960->IIterator_1_get_HasCurrent_m1554425837(&returnValue);
	____iiterator_1_t569256960->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Rect>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m328760485 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t569256960* ____iiterator_1_t569256960 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t569256960::IID, reinterpret_cast<void**>(&____iiterator_1_t569256960));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t569256960->IIterator_1_MoveNext_m328760485(&returnValue);
	____iiterator_1_t569256960->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Rect>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m269641506 (RuntimeObject* __this, RectU5BU5D_t5657558* ___items0, const RuntimeMethod* method)
{
	IIterator_1_t569256960* ____iiterator_1_t569256960 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t569256960::IID, reinterpret_cast<void**>(&____iiterator_1_t569256960));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	Rect_t2695113487 * ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<Rect_t2695113487 >(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(Rect_t2695113487 ));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t569256960->IIterator_1_GetMany_m269641506(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t569256960->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items0_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	il2cpp_codegen_marshal_free(____items0_marshaled);
	____items0_marshaled = NULL;

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Size>::get_Current()
extern "C"  Size_t550917638  IIterator_1_get_Current_m573419872 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2720028407* ____iiterator_1_t2720028407 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2720028407::IID, reinterpret_cast<void**>(&____iiterator_1_t2720028407));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Size_t550917638  returnValue = {};
	hr = ____iiterator_1_t2720028407->IIterator_1_get_Current_m573419872(&returnValue);
	____iiterator_1_t2720028407->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Size>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m4223115842 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2720028407* ____iiterator_1_t2720028407 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2720028407::IID, reinterpret_cast<void**>(&____iiterator_1_t2720028407));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2720028407->IIterator_1_get_HasCurrent_m4223115842(&returnValue);
	____iiterator_1_t2720028407->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Size>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m2301142642 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2720028407* ____iiterator_1_t2720028407 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2720028407::IID, reinterpret_cast<void**>(&____iiterator_1_t2720028407));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2720028407->IIterator_1_MoveNext_m2301142642(&returnValue);
	____iiterator_1_t2720028407->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<Windows.Foundation.Size>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m2384573303 (RuntimeObject* __this, SizeU5BU5D_t1671914275* ___items0, const RuntimeMethod* method)
{
	IIterator_1_t2720028407* ____iiterator_1_t2720028407 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2720028407::IID, reinterpret_cast<void**>(&____iiterator_1_t2720028407));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	Size_t550917638 * ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<Size_t550917638 >(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(Size_t550917638 ));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t2720028407->IIterator_1_GetMany_m2384573303(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t2720028407->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items0_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	il2cpp_codegen_marshal_free(____items0_marshaled);
	____items0_marshaled = NULL;

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<Windows.Media.SpeechSynthesis.IVoiceInformation>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m4261329911 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m4261329911_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t985199492* ____iiterator_1_t985199492 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t985199492::IID, reinterpret_cast<void**>(&____iiterator_1_t985199492));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVoiceInformation_t3111056019* returnValue = NULL;
	hr = ____iiterator_1_t985199492->IIterator_1_get_Current_m4261329911(&returnValue);
	____iiterator_1_t985199492->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Media.SpeechSynthesis.IVoiceInformation>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m2587194256 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t985199492* ____iiterator_1_t985199492 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t985199492::IID, reinterpret_cast<void**>(&____iiterator_1_t985199492));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t985199492->IIterator_1_get_HasCurrent_m2587194256(&returnValue);
	____iiterator_1_t985199492->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Media.SpeechSynthesis.IVoiceInformation>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m1083425478 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t985199492* ____iiterator_1_t985199492 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t985199492::IID, reinterpret_cast<void**>(&____iiterator_1_t985199492));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t985199492->IIterator_1_MoveNext_m1083425478(&returnValue);
	____iiterator_1_t985199492->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<Windows.Media.SpeechSynthesis.IVoiceInformation>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m1097615757 (RuntimeObject* __this, IVoiceInformationU5BU5D_t3472394690* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m1097615757_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t985199492* ____iiterator_1_t985199492 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t985199492::IID, reinterpret_cast<void**>(&____iiterator_1_t985199492));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IVoiceInformation_t3111056019** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IVoiceInformation_t3111056019*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IVoiceInformation_t3111056019*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t985199492->IIterator_1_GetMany_m1097615757(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t985199492->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<Windows.Media.SpeechSynthesis.VoiceInformation>::get_Current()
extern "C"  VoiceInformation_t4266404632 * IIterator_1_get_Current_m4213660447 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m4213660447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2140548105* ____iiterator_1_t2140548105 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2140548105::IID, reinterpret_cast<void**>(&____iiterator_1_t2140548105));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVoiceInformation_t3111056019* returnValue = NULL;
	hr = ____iiterator_1_t2140548105->IIterator_1_get_Current_m4213660447(&returnValue);
	____iiterator_1_t2140548105->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	VoiceInformation_t4266404632 * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_for_sealed_class<VoiceInformation_t4266404632>(returnValue, VoiceInformation_t4266404632_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Media.SpeechSynthesis.VoiceInformation>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m1766660868 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2140548105* ____iiterator_1_t2140548105 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2140548105::IID, reinterpret_cast<void**>(&____iiterator_1_t2140548105));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2140548105->IIterator_1_get_HasCurrent_m1766660868(&returnValue);
	____iiterator_1_t2140548105->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.Media.SpeechSynthesis.VoiceInformation>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m112683399 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2140548105* ____iiterator_1_t2140548105 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2140548105::IID, reinterpret_cast<void**>(&____iiterator_1_t2140548105));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2140548105->IIterator_1_MoveNext_m112683399(&returnValue);
	____iiterator_1_t2140548105->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<Windows.Media.SpeechSynthesis.VoiceInformation>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m1697528449 (RuntimeObject* __this, VoiceInformationU5BU5D_t1943932297* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m1697528449_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2140548105* ____iiterator_1_t2140548105 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2140548105::IID, reinterpret_cast<void**>(&____iiterator_1_t2140548105));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IVoiceInformation_t3111056019** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IVoiceInformation_t3111056019*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IVoiceInformation_t3111056019*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t2140548105->IIterator_1_GetMany_m1697528449(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t2140548105->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			VoiceInformation_t4266404632 * _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_for_sealed_class<VoiceInformation_t4266404632>((____items0_marshaled)[i], VoiceInformation_t4266404632_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m1347393236 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m1347393236_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3527940572* ____iiterator_1_t3527940572 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3527940572::IID, reinterpret_cast<void**>(&____iiterator_1_t3527940572));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	ISpatialInteractionSourceState_t1358829803* returnValue = NULL;
	hr = ____iiterator_1_t3527940572->IIterator_1_get_Current_m1347393236(&returnValue);
	____iiterator_1_t3527940572->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m1096701536 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3527940572* ____iiterator_1_t3527940572 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3527940572::IID, reinterpret_cast<void**>(&____iiterator_1_t3527940572));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3527940572->IIterator_1_get_HasCurrent_m1096701536(&returnValue);
	____iiterator_1_t3527940572->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m1173429162 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3527940572* ____iiterator_1_t3527940572 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3527940572::IID, reinterpret_cast<void**>(&____iiterator_1_t3527940572));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3527940572->IIterator_1_MoveNext_m1173429162(&returnValue);
	____iiterator_1_t3527940572->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m3554813579 (RuntimeObject* __this, ISpatialInteractionSourceStateU5BU5D_t513935114* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m3554813579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3527940572* ____iiterator_1_t3527940572 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3527940572::IID, reinterpret_cast<void**>(&____iiterator_1_t3527940572));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	ISpatialInteractionSourceState_t1358829803** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<ISpatialInteractionSourceState_t1358829803*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(ISpatialInteractionSourceState_t1358829803*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t3527940572->IIterator_1_GetMany_m3554813579(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t3527940572->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState2>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m1548962596 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m1548962596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t851581404* ____iiterator_1_t851581404 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t851581404::IID, reinterpret_cast<void**>(&____iiterator_1_t851581404));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	ISpatialInteractionSourceState2_t2977437931* returnValue = NULL;
	hr = ____iiterator_1_t851581404->IIterator_1_get_Current_m1548962596(&returnValue);
	____iiterator_1_t851581404->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState2>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m20766391 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t851581404* ____iiterator_1_t851581404 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t851581404::IID, reinterpret_cast<void**>(&____iiterator_1_t851581404));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t851581404->IIterator_1_get_HasCurrent_m20766391(&returnValue);
	____iiterator_1_t851581404->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState2>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m2099795593 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t851581404* ____iiterator_1_t851581404 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t851581404::IID, reinterpret_cast<void**>(&____iiterator_1_t851581404));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t851581404->IIterator_1_MoveNext_m2099795593(&returnValue);
	____iiterator_1_t851581404->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.ISpatialInteractionSourceState2>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m2124254469 (RuntimeObject* __this, ISpatialInteractionSourceState2U5BU5D_t3674212106* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m2124254469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t851581404* ____iiterator_1_t851581404 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t851581404::IID, reinterpret_cast<void**>(&____iiterator_1_t851581404));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	ISpatialInteractionSourceState2_t2977437931** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<ISpatialInteractionSourceState2_t2977437931*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(ISpatialInteractionSourceState2_t2977437931*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t851581404->IIterator_1_GetMany_m2124254469(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t851581404->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.SpatialInteractionSourceState>::get_Current()
extern "C"  SpatialInteractionSourceState_t1215688063 * IIterator_1_get_Current_m1757905955 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m1757905955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3384798832* ____iiterator_1_t3384798832 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3384798832::IID, reinterpret_cast<void**>(&____iiterator_1_t3384798832));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	ISpatialInteractionSourceState_t1358829803* returnValue = NULL;
	hr = ____iiterator_1_t3384798832->IIterator_1_get_Current_m1757905955(&returnValue);
	____iiterator_1_t3384798832->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	SpatialInteractionSourceState_t1215688063 * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_for_sealed_class<SpatialInteractionSourceState_t1215688063>(returnValue, SpatialInteractionSourceState_t1215688063_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.SpatialInteractionSourceState>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m1287739422 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3384798832* ____iiterator_1_t3384798832 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3384798832::IID, reinterpret_cast<void**>(&____iiterator_1_t3384798832));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3384798832->IIterator_1_get_HasCurrent_m1287739422(&returnValue);
	____iiterator_1_t3384798832->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.SpatialInteractionSourceState>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m3375070988 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3384798832* ____iiterator_1_t3384798832 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3384798832::IID, reinterpret_cast<void**>(&____iiterator_1_t3384798832));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3384798832->IIterator_1_MoveNext_m3375070988(&returnValue);
	____iiterator_1_t3384798832->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<Windows.UI.Input.Spatial.SpatialInteractionSourceState>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m852781585 (RuntimeObject* __this, SpatialInteractionSourceStateU5BU5D_t1865583014* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m852781585_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3384798832* ____iiterator_1_t3384798832 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3384798832::IID, reinterpret_cast<void**>(&____iiterator_1_t3384798832));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	ISpatialInteractionSourceState_t1358829803** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<ISpatialInteractionSourceState_t1358829803*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(ISpatialInteractionSourceState_t1358829803*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t3384798832->IIterator_1_GetMany_m852781585(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t3384798832->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			SpatialInteractionSourceState_t1215688063 * _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_for_sealed_class<SpatialInteractionSourceState_t1215688063>((____items0_marshaled)[i], SpatialInteractionSourceState_t1215688063_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.Guid,System.Object>::get_Key()
extern "C"  Guid_t  IKeyValuePair_2_get_Key_m3324945768 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t1256548186* ____ikeyValuePair_2_t1256548186 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t1256548186::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t1256548186));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Guid_t  returnValue = {};
	hr = ____ikeyValuePair_2_t1256548186->IKeyValuePair_2_get_Key_m3324945768(&returnValue);
	____ikeyValuePair_2_t1256548186->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.Guid,System.Object>::get_Value()
extern "C"  RuntimeObject * IKeyValuePair_2_get_Value_m52124236 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IKeyValuePair_2_get_Value_m52124236_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IKeyValuePair_2_t1256548186* ____ikeyValuePair_2_t1256548186 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t1256548186::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t1256548186));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____ikeyValuePair_2_t1256548186->IKeyValuePair_2_get_Value_m52124236(&returnValue);
	____ikeyValuePair_2_t1256548186->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.Guid,System.Type>::get_Key()
extern "C"  Guid_t  IKeyValuePair_2_get_Key_m1247672299 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t660386782* ____ikeyValuePair_2_t660386782 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t660386782::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t660386782));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Guid_t  returnValue = {};
	hr = ____ikeyValuePair_2_t660386782->IKeyValuePair_2_get_Key_m1247672299(&returnValue);
	____ikeyValuePair_2_t660386782->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.Guid,System.Type>::get_Value()
extern "C"  Type_t * IKeyValuePair_2_get_Value_m3450640679 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.Int32,System.Int32>::get_Key()
extern "C"  int32_t IKeyValuePair_2_get_Key_m2344176952 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t967083573* ____ikeyValuePair_2_t967083573 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t967083573::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t967083573));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	int32_t returnValue = 0;
	hr = ____ikeyValuePair_2_t967083573->IKeyValuePair_2_get_Key_m2344176952(&returnValue);
	____ikeyValuePair_2_t967083573->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.Int32,System.Int32>::get_Value()
extern "C"  int32_t IKeyValuePair_2_get_Value_m1590219272 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t967083573* ____ikeyValuePair_2_t967083573 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t967083573::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t967083573));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	int32_t returnValue = 0;
	hr = ____ikeyValuePair_2_t967083573->IKeyValuePair_2_get_Value_m1590219272(&returnValue);
	____ikeyValuePair_2_t967083573->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t IKeyValuePair_2_get_Key_m2186241555 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t1096243984* ____ikeyValuePair_2_t1096243984 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t1096243984::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t1096243984));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	int32_t returnValue = 0;
	hr = ____ikeyValuePair_2_t1096243984->IKeyValuePair_2_get_Key_m2186241555(&returnValue);
	____ikeyValuePair_2_t1096243984->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  RuntimeObject * IKeyValuePair_2_get_Value_m210688108 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IKeyValuePair_2_get_Value_m210688108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IKeyValuePair_2_t1096243984* ____ikeyValuePair_2_t1096243984 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t1096243984::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t1096243984));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____ikeyValuePair_2_t1096243984->IKeyValuePair_2_get_Value_m210688108(&returnValue);
	____ikeyValuePair_2_t1096243984->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.Int64,System.Object>::get_Key()
extern "C"  int64_t IKeyValuePair_2_get_Key_m1937891137 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t3270170437* ____ikeyValuePair_2_t3270170437 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t3270170437::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t3270170437));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	int64_t returnValue = 0;
	hr = ____ikeyValuePair_2_t3270170437->IKeyValuePair_2_get_Key_m1937891137(&returnValue);
	____ikeyValuePair_2_t3270170437->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.Int64,System.Object>::get_Value()
extern "C"  RuntimeObject * IKeyValuePair_2_get_Value_m1356929877 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IKeyValuePair_2_get_Value_m1356929877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IKeyValuePair_2_t3270170437* ____ikeyValuePair_2_t3270170437 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t3270170437::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t3270170437));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____ikeyValuePair_2_t3270170437->IKeyValuePair_2_get_Value_m1356929877(&returnValue);
	____ikeyValuePair_2_t3270170437->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  RuntimeObject * IKeyValuePair_2_get_Key_m1916408783 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IKeyValuePair_2_get_Key_m1916408783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IKeyValuePair_2_t572118738* ____ikeyValuePair_2_t572118738 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t572118738::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t572118738));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____ikeyValuePair_2_t572118738->IKeyValuePair_2_get_Key_m1916408783(&returnValue);
	____ikeyValuePair_2_t572118738->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool IKeyValuePair_2_get_Value_m3611222262 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t572118738* ____ikeyValuePair_2_t572118738 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t572118738::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t572118738));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____ikeyValuePair_2_t572118738->IKeyValuePair_2_get_Value_m3611222262(&returnValue);
	____ikeyValuePair_2_t572118738->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  RuntimeObject * IKeyValuePair_2_get_Key_m1066766749 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IKeyValuePair_2_get_Key_m1066766749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IKeyValuePair_2_t3425776526* ____ikeyValuePair_2_t3425776526 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t3425776526::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t3425776526));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____ikeyValuePair_2_t3425776526->IKeyValuePair_2_get_Key_m1066766749(&returnValue);
	____ikeyValuePair_2_t3425776526->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t IKeyValuePair_2_get_Value_m269040038 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t3425776526* ____ikeyValuePair_2_t3425776526 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t3425776526::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t3425776526));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	int32_t returnValue = 0;
	hr = ____ikeyValuePair_2_t3425776526->IKeyValuePair_2_get_Value_m269040038(&returnValue);
	____ikeyValuePair_2_t3425776526->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  RuntimeObject * IKeyValuePair_2_get_Key_m2750152149 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IKeyValuePair_2_get_Key_m2750152149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IKeyValuePair_2_t3554936937* ____ikeyValuePair_2_t3554936937 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t3554936937::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t3554936937));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____ikeyValuePair_2_t3554936937->IKeyValuePair_2_get_Key_m2750152149(&returnValue);
	____ikeyValuePair_2_t3554936937->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  RuntimeObject * IKeyValuePair_2_get_Value_m2672088708 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IKeyValuePair_2_get_Value_m2672088708_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IKeyValuePair_2_t3554936937* ____ikeyValuePair_2_t3554936937 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t3554936937::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t3554936937));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____ikeyValuePair_2_t3554936937->IKeyValuePair_2_get_Value_m2672088708(&returnValue);
	____ikeyValuePair_2_t3554936937->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.String>::get_Key()
extern "C"  RuntimeObject * IKeyValuePair_2_get_Key_m2722966289 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IKeyValuePair_2_get_Key_m2722966289_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IKeyValuePair_2_t2322281462* ____ikeyValuePair_2_t2322281462 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t2322281462::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t2322281462));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____ikeyValuePair_2_t2322281462->IKeyValuePair_2_get_Key_m2722966289(&returnValue);
	____ikeyValuePair_2_t2322281462->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.Object,System.String>::get_Value()
extern "C"  String_t* IKeyValuePair_2_get_Value_m3432963267 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t2322281462* ____ikeyValuePair_2_t2322281462 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t2322281462::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t2322281462));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppHString returnValue = NULL;
	hr = ____ikeyValuePair_2_t2322281462->IKeyValuePair_2_get_Value_m3432963267(&returnValue);
	____ikeyValuePair_2_t2322281462->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_hstring_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free_hstring(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Boolean>::get_Key()
extern "C"  String_t* IKeyValuePair_2_get_Key_m2534079307 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t3304936049* ____ikeyValuePair_2_t3304936049 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t3304936049::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t3304936049));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppHString returnValue = NULL;
	hr = ____ikeyValuePair_2_t3304936049->IKeyValuePair_2_get_Key_m2534079307(&returnValue);
	____ikeyValuePair_2_t3304936049->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_hstring_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free_hstring(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Boolean>::get_Value()
extern "C"  bool IKeyValuePair_2_get_Value_m2353902038 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t3304936049* ____ikeyValuePair_2_t3304936049 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t3304936049::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t3304936049));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____ikeyValuePair_2_t3304936049->IKeyValuePair_2_get_Value_m2353902038(&returnValue);
	____ikeyValuePair_2_t3304936049->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Int32>::get_Key()
extern "C"  String_t* IKeyValuePair_2_get_Key_m599934735 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t1863626541* ____ikeyValuePair_2_t1863626541 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t1863626541::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t1863626541));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppHString returnValue = NULL;
	hr = ____ikeyValuePair_2_t1863626541->IKeyValuePair_2_get_Key_m599934735(&returnValue);
	____ikeyValuePair_2_t1863626541->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_hstring_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free_hstring(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Int32>::get_Value()
extern "C"  int32_t IKeyValuePair_2_get_Value_m2337749369 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t1863626541* ____ikeyValuePair_2_t1863626541 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t1863626541::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t1863626541));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	int32_t returnValue = 0;
	hr = ____ikeyValuePair_2_t1863626541->IKeyValuePair_2_get_Value_m2337749369(&returnValue);
	____ikeyValuePair_2_t1863626541->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Object>::get_Key()
extern "C"  String_t* IKeyValuePair_2_get_Key_m3523589551 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t1992786952* ____ikeyValuePair_2_t1992786952 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t1992786952::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t1992786952));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppHString returnValue = NULL;
	hr = ____ikeyValuePair_2_t1992786952->IKeyValuePair_2_get_Key_m3523589551(&returnValue);
	____ikeyValuePair_2_t1992786952->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_hstring_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free_hstring(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Object>::get_Value()
extern "C"  RuntimeObject * IKeyValuePair_2_get_Value_m3279123715 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IKeyValuePair_2_get_Value_m3279123715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IKeyValuePair_2_t1992786952* ____ikeyValuePair_2_t1992786952 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t1992786952::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t1992786952));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____ikeyValuePair_2_t1992786952->IKeyValuePair_2_get_Value_m3279123715(&returnValue);
	____ikeyValuePair_2_t1992786952->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.String>::get_Key()
extern "C"  String_t* IKeyValuePair_2_get_Key_m3013852749 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t760131477* ____ikeyValuePair_2_t760131477 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t760131477::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t760131477));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppHString returnValue = NULL;
	hr = ____ikeyValuePair_2_t760131477->IKeyValuePair_2_get_Key_m3013852749(&returnValue);
	____ikeyValuePair_2_t760131477->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_hstring_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free_hstring(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.String>::get_Value()
extern "C"  String_t* IKeyValuePair_2_get_Value_m2003745203 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t760131477* ____ikeyValuePair_2_t760131477 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t760131477::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t760131477));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppHString returnValue = NULL;
	hr = ____ikeyValuePair_2_t760131477->IKeyValuePair_2_get_Value_m2003745203(&returnValue);
	____ikeyValuePair_2_t760131477->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_hstring_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free_hstring(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Type>::get_Key()
extern "C"  String_t* IKeyValuePair_2_get_Key_m2199926285 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t1396625548* ____ikeyValuePair_2_t1396625548 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t1396625548::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t1396625548));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppHString returnValue = NULL;
	hr = ____ikeyValuePair_2_t1396625548->IKeyValuePair_2_get_Key_m2199926285(&returnValue);
	____ikeyValuePair_2_t1396625548->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_hstring_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free_hstring(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.String,System.Type>::get_Value()
extern "C"  Type_t * IKeyValuePair_2_get_Value_m1427991677 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.Type,System.Type>::get_Key()
extern "C"  Type_t * IKeyValuePair_2_get_Key_m2294505739 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.Type,System.Type>::get_Value()
extern "C"  Type_t * IKeyValuePair_2_get_Value_m1073902776 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// K Windows.Foundation.Collections.IKeyValuePair`2<System.UInt32,System.Object>::get_Key()
extern "C"  uint32_t IKeyValuePair_2_get_Key_m2175672871 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IKeyValuePair_2_t3481798315* ____ikeyValuePair_2_t3481798315 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t3481798315::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t3481798315));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ikeyValuePair_2_t3481798315->IKeyValuePair_2_get_Key_m2175672871(&returnValue);
	____ikeyValuePair_2_t3481798315->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// V Windows.Foundation.Collections.IKeyValuePair`2<System.UInt32,System.Object>::get_Value()
extern "C"  RuntimeObject * IKeyValuePair_2_get_Value_m3078130594 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IKeyValuePair_2_get_Value_m3078130594_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IKeyValuePair_2_t3481798315* ____ikeyValuePair_2_t3481798315 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IKeyValuePair_2_t3481798315::IID, reinterpret_cast<void**>(&____ikeyValuePair_2_t3481798315));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____ikeyValuePair_2_t3481798315->IKeyValuePair_2_get_Value_m3078130594(&returnValue);
	____ikeyValuePair_2_t3481798315->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// V Windows.Foundation.Collections.IMap`2<System.Guid,System.Object>::Lookup(K)
extern "C"  RuntimeObject * IMap_2_Lookup_m3068028334 (RuntimeObject* __this, Guid_t  ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_Lookup_m3068028334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t778760517* ____imap_2_t778760517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t778760517::IID, reinterpret_cast<void**>(&____imap_2_t778760517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____imap_2_t778760517->IMap_2_Lookup_m3068028334(___key0, &returnValue);
	____imap_2_t778760517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMap`2<System.Guid,System.Object>::get_Size()
extern "C"  uint32_t IMap_2_get_Size_m1704379977 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t778760517* ____imap_2_t778760517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t778760517::IID, reinterpret_cast<void**>(&____imap_2_t778760517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imap_2_t778760517->IMap_2_get_Size_m1704379977(&returnValue);
	____imap_2_t778760517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Guid,System.Object>::HasKey(K)
extern "C"  bool IMap_2_HasKey_m2236943020 (RuntimeObject* __this, Guid_t  ___key0, const RuntimeMethod* method)
{
	IMap_2_t778760517* ____imap_2_t778760517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t778760517::IID, reinterpret_cast<void**>(&____imap_2_t778760517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t778760517->IMap_2_HasKey_m2236943020(___key0, &returnValue);
	____imap_2_t778760517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2<System.Guid,System.Object>::GetView()
extern "C"  RuntimeObject* IMap_2_GetView_m3237694789 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_GetView_m3237694789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t778760517* ____imap_2_t778760517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t778760517::IID, reinterpret_cast<void**>(&____imap_2_t778760517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IMapView_2_t3878100361* returnValue = NULL;
	hr = ____imap_2_t778760517->IMap_2_GetView_m3237694789(&returnValue);
	____imap_2_t778760517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Guid,System.Object>::Insert(K,V)
extern "C"  bool IMap_2_Insert_m1575381716 (RuntimeObject* __this, Guid_t  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	IMap_2_t778760517* ____imap_2_t778760517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t778760517::IID, reinterpret_cast<void**>(&____imap_2_t778760517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value1' to native representation
	Il2CppIInspectable* ____value1_marshaled = NULL;
	if (___value1 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value1))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value1)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____value1_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value1_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___value1);
		}
	}
	else
	{
		____value1_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t778760517->IMap_2_Insert_m1575381716(___key0, ____value1_marshaled, &returnValue);
	____imap_2_t778760517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value1' native representation
	if (____value1_marshaled != NULL)
	{
		(____value1_marshaled)->Release();
		____value1_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMap`2<System.Guid,System.Object>::Remove(K)
extern "C"  void IMap_2_Remove_m3289693839 (RuntimeObject* __this, Guid_t  ___key0, const RuntimeMethod* method)
{
	IMap_2_t778760517* ____imap_2_t778760517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t778760517::IID, reinterpret_cast<void**>(&____imap_2_t778760517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t778760517->IMap_2_Remove_m3289693839(___key0);
	____imap_2_t778760517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IMap`2<System.Guid,System.Object>::Clear()
extern "C"  void IMap_2_Clear_m2519350850 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t778760517* ____imap_2_t778760517 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t778760517::IID, reinterpret_cast<void**>(&____imap_2_t778760517));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t778760517->IMap_2_Clear_m2519350850();
	____imap_2_t778760517->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// V Windows.Foundation.Collections.IMap`2<System.Guid,System.Type>::Lookup(K)
extern "C"  Type_t * IMap_2_Lookup_m372021310 (RuntimeObject* __this, Guid_t  ___key0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.UInt32 Windows.Foundation.Collections.IMap`2<System.Guid,System.Type>::get_Size()
extern "C"  uint32_t IMap_2_get_Size_m4209346384 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t182599113* ____imap_2_t182599113 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t182599113::IID, reinterpret_cast<void**>(&____imap_2_t182599113));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imap_2_t182599113->IMap_2_get_Size_m4209346384(&returnValue);
	____imap_2_t182599113->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Guid,System.Type>::HasKey(K)
extern "C"  bool IMap_2_HasKey_m2767397819 (RuntimeObject* __this, Guid_t  ___key0, const RuntimeMethod* method)
{
	IMap_2_t182599113* ____imap_2_t182599113 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t182599113::IID, reinterpret_cast<void**>(&____imap_2_t182599113));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t182599113->IMap_2_HasKey_m2767397819(___key0, &returnValue);
	____imap_2_t182599113->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2<System.Guid,System.Type>::GetView()
extern "C"  RuntimeObject* IMap_2_GetView_m3956818822 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_GetView_m3956818822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t182599113* ____imap_2_t182599113 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t182599113::IID, reinterpret_cast<void**>(&____imap_2_t182599113));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IMapView_2_t3281938957* returnValue = NULL;
	hr = ____imap_2_t182599113->IMap_2_GetView_m3956818822(&returnValue);
	____imap_2_t182599113->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Guid,System.Type>::Insert(K,V)
extern "C"  bool IMap_2_Insert_m52862695 (RuntimeObject* __this, Guid_t  ___key0, Type_t * ___value1, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.Void Windows.Foundation.Collections.IMap`2<System.Guid,System.Type>::Remove(K)
extern "C"  void IMap_2_Remove_m441912298 (RuntimeObject* __this, Guid_t  ___key0, const RuntimeMethod* method)
{
	IMap_2_t182599113* ____imap_2_t182599113 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t182599113::IID, reinterpret_cast<void**>(&____imap_2_t182599113));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t182599113->IMap_2_Remove_m441912298(___key0);
	____imap_2_t182599113->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IMap`2<System.Guid,System.Type>::Clear()
extern "C"  void IMap_2_Clear_m1518512299 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t182599113* ____imap_2_t182599113 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t182599113::IID, reinterpret_cast<void**>(&____imap_2_t182599113));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t182599113->IMap_2_Clear_m1518512299();
	____imap_2_t182599113->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// V Windows.Foundation.Collections.IMap`2<System.Int32,System.Object>::Lookup(K)
extern "C"  RuntimeObject * IMap_2_Lookup_m2451726409 (RuntimeObject* __this, int32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_Lookup_m2451726409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t618456315* ____imap_2_t618456315 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t618456315::IID, reinterpret_cast<void**>(&____imap_2_t618456315));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____imap_2_t618456315->IMap_2_Lookup_m2451726409(___key0, &returnValue);
	____imap_2_t618456315->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMap`2<System.Int32,System.Object>::get_Size()
extern "C"  uint32_t IMap_2_get_Size_m2259249674 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t618456315* ____imap_2_t618456315 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t618456315::IID, reinterpret_cast<void**>(&____imap_2_t618456315));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imap_2_t618456315->IMap_2_get_Size_m2259249674(&returnValue);
	____imap_2_t618456315->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Int32,System.Object>::HasKey(K)
extern "C"  bool IMap_2_HasKey_m4208301656 (RuntimeObject* __this, int32_t ___key0, const RuntimeMethod* method)
{
	IMap_2_t618456315* ____imap_2_t618456315 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t618456315::IID, reinterpret_cast<void**>(&____imap_2_t618456315));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t618456315->IMap_2_HasKey_m4208301656(___key0, &returnValue);
	____imap_2_t618456315->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2<System.Int32,System.Object>::GetView()
extern "C"  RuntimeObject* IMap_2_GetView_m3361060608 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_GetView_m3361060608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t618456315* ____imap_2_t618456315 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t618456315::IID, reinterpret_cast<void**>(&____imap_2_t618456315));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IMapView_2_t3717796159* returnValue = NULL;
	hr = ____imap_2_t618456315->IMap_2_GetView_m3361060608(&returnValue);
	____imap_2_t618456315->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Int32,System.Object>::Insert(K,V)
extern "C"  bool IMap_2_Insert_m490092833 (RuntimeObject* __this, int32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	IMap_2_t618456315* ____imap_2_t618456315 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t618456315::IID, reinterpret_cast<void**>(&____imap_2_t618456315));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value1' to native representation
	Il2CppIInspectable* ____value1_marshaled = NULL;
	if (___value1 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value1))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value1)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____value1_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value1_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___value1);
		}
	}
	else
	{
		____value1_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t618456315->IMap_2_Insert_m490092833(___key0, ____value1_marshaled, &returnValue);
	____imap_2_t618456315->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value1' native representation
	if (____value1_marshaled != NULL)
	{
		(____value1_marshaled)->Release();
		____value1_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMap`2<System.Int32,System.Object>::Remove(K)
extern "C"  void IMap_2_Remove_m1067146362 (RuntimeObject* __this, int32_t ___key0, const RuntimeMethod* method)
{
	IMap_2_t618456315* ____imap_2_t618456315 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t618456315::IID, reinterpret_cast<void**>(&____imap_2_t618456315));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t618456315->IMap_2_Remove_m1067146362(___key0);
	____imap_2_t618456315->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IMap`2<System.Int32,System.Object>::Clear()
extern "C"  void IMap_2_Clear_m3558300509 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t618456315* ____imap_2_t618456315 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t618456315::IID, reinterpret_cast<void**>(&____imap_2_t618456315));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t618456315->IMap_2_Clear_m3558300509();
	____imap_2_t618456315->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// V Windows.Foundation.Collections.IMap`2<System.Int64,System.Object>::Lookup(K)
extern "C"  RuntimeObject * IMap_2_Lookup_m367753107 (RuntimeObject* __this, int64_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_Lookup_m367753107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t2792382768* ____imap_2_t2792382768 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2792382768::IID, reinterpret_cast<void**>(&____imap_2_t2792382768));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____imap_2_t2792382768->IMap_2_Lookup_m367753107(___key0, &returnValue);
	____imap_2_t2792382768->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMap`2<System.Int64,System.Object>::get_Size()
extern "C"  uint32_t IMap_2_get_Size_m137360363 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t2792382768* ____imap_2_t2792382768 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2792382768::IID, reinterpret_cast<void**>(&____imap_2_t2792382768));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imap_2_t2792382768->IMap_2_get_Size_m137360363(&returnValue);
	____imap_2_t2792382768->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Int64,System.Object>::HasKey(K)
extern "C"  bool IMap_2_HasKey_m663731653 (RuntimeObject* __this, int64_t ___key0, const RuntimeMethod* method)
{
	IMap_2_t2792382768* ____imap_2_t2792382768 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2792382768::IID, reinterpret_cast<void**>(&____imap_2_t2792382768));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t2792382768->IMap_2_HasKey_m663731653(___key0, &returnValue);
	____imap_2_t2792382768->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2<System.Int64,System.Object>::GetView()
extern "C"  RuntimeObject* IMap_2_GetView_m1077656533 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_GetView_m1077656533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t2792382768* ____imap_2_t2792382768 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2792382768::IID, reinterpret_cast<void**>(&____imap_2_t2792382768));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IMapView_2_t1596755316* returnValue = NULL;
	hr = ____imap_2_t2792382768->IMap_2_GetView_m1077656533(&returnValue);
	____imap_2_t2792382768->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Int64,System.Object>::Insert(K,V)
extern "C"  bool IMap_2_Insert_m2086881606 (RuntimeObject* __this, int64_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	IMap_2_t2792382768* ____imap_2_t2792382768 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2792382768::IID, reinterpret_cast<void**>(&____imap_2_t2792382768));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value1' to native representation
	Il2CppIInspectable* ____value1_marshaled = NULL;
	if (___value1 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value1))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value1)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____value1_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value1_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___value1);
		}
	}
	else
	{
		____value1_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t2792382768->IMap_2_Insert_m2086881606(___key0, ____value1_marshaled, &returnValue);
	____imap_2_t2792382768->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value1' native representation
	if (____value1_marshaled != NULL)
	{
		(____value1_marshaled)->Release();
		____value1_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMap`2<System.Int64,System.Object>::Remove(K)
extern "C"  void IMap_2_Remove_m2064551222 (RuntimeObject* __this, int64_t ___key0, const RuntimeMethod* method)
{
	IMap_2_t2792382768* ____imap_2_t2792382768 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2792382768::IID, reinterpret_cast<void**>(&____imap_2_t2792382768));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t2792382768->IMap_2_Remove_m2064551222(___key0);
	____imap_2_t2792382768->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IMap`2<System.Int64,System.Object>::Clear()
extern "C"  void IMap_2_Clear_m156915013 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t2792382768* ____imap_2_t2792382768 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2792382768::IID, reinterpret_cast<void**>(&____imap_2_t2792382768));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t2792382768->IMap_2_Clear_m156915013();
	____imap_2_t2792382768->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// V Windows.Foundation.Collections.IMap`2<System.Object,System.Boolean>::Lookup(K)
extern "C"  bool IMap_2_Lookup_m2791672174 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMap_2_t94331069* ____imap_2_t94331069 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t94331069::IID, reinterpret_cast<void**>(&____imap_2_t94331069));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t94331069->IMap_2_Lookup_m2791672174(____key0_marshaled, &returnValue);
	____imap_2_t94331069->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IMap`2<System.Object,System.Boolean>::get_Size()
extern "C"  uint32_t IMap_2_get_Size_m521129328 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t94331069* ____imap_2_t94331069 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t94331069::IID, reinterpret_cast<void**>(&____imap_2_t94331069));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imap_2_t94331069->IMap_2_get_Size_m521129328(&returnValue);
	____imap_2_t94331069->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Object,System.Boolean>::HasKey(K)
extern "C"  bool IMap_2_HasKey_m3863327211 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMap_2_t94331069* ____imap_2_t94331069 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t94331069::IID, reinterpret_cast<void**>(&____imap_2_t94331069));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t94331069->IMap_2_HasKey_m3863327211(____key0_marshaled, &returnValue);
	____imap_2_t94331069->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return returnValue;
}
// System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2<System.Object,System.Boolean>::GetView()
extern "C"  RuntimeObject* IMap_2_GetView_m1166070801 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_GetView_m1166070801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t94331069* ____imap_2_t94331069 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t94331069::IID, reinterpret_cast<void**>(&____imap_2_t94331069));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IMapView_2_t3193670913* returnValue = NULL;
	hr = ____imap_2_t94331069->IMap_2_GetView_m1166070801(&returnValue);
	____imap_2_t94331069->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Object,System.Boolean>::Insert(K,V)
extern "C"  bool IMap_2_Insert_m3431746527 (RuntimeObject* __this, RuntimeObject * ___key0, bool ___value1, const RuntimeMethod* method)
{
	IMap_2_t94331069* ____imap_2_t94331069 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t94331069::IID, reinterpret_cast<void**>(&____imap_2_t94331069));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t94331069->IMap_2_Insert_m3431746527(____key0_marshaled, ___value1, &returnValue);
	____imap_2_t94331069->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMap`2<System.Object,System.Boolean>::Remove(K)
extern "C"  void IMap_2_Remove_m1939883922 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMap_2_t94331069* ____imap_2_t94331069 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t94331069::IID, reinterpret_cast<void**>(&____imap_2_t94331069));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	hr = ____imap_2_t94331069->IMap_2_Remove_m1939883922(____key0_marshaled);
	____imap_2_t94331069->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

}
// System.Void Windows.Foundation.Collections.IMap`2<System.Object,System.Boolean>::Clear()
extern "C"  void IMap_2_Clear_m403209908 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t94331069* ____imap_2_t94331069 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t94331069::IID, reinterpret_cast<void**>(&____imap_2_t94331069));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t94331069->IMap_2_Clear_m403209908();
	____imap_2_t94331069->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// V Windows.Foundation.Collections.IMap`2<System.Object,System.Int32>::Lookup(K)
extern "C"  int32_t IMap_2_Lookup_m4271171535 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMap_2_t2947988857* ____imap_2_t2947988857 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2947988857::IID, reinterpret_cast<void**>(&____imap_2_t2947988857));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	int32_t returnValue = 0;
	hr = ____imap_2_t2947988857->IMap_2_Lookup_m4271171535(____key0_marshaled, &returnValue);
	____imap_2_t2947988857->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IMap`2<System.Object,System.Int32>::get_Size()
extern "C"  uint32_t IMap_2_get_Size_m672085459 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t2947988857* ____imap_2_t2947988857 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2947988857::IID, reinterpret_cast<void**>(&____imap_2_t2947988857));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imap_2_t2947988857->IMap_2_get_Size_m672085459(&returnValue);
	____imap_2_t2947988857->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Object,System.Int32>::HasKey(K)
extern "C"  bool IMap_2_HasKey_m377757278 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMap_2_t2947988857* ____imap_2_t2947988857 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2947988857::IID, reinterpret_cast<void**>(&____imap_2_t2947988857));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t2947988857->IMap_2_HasKey_m377757278(____key0_marshaled, &returnValue);
	____imap_2_t2947988857->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return returnValue;
}
// System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2<System.Object,System.Int32>::GetView()
extern "C"  RuntimeObject* IMap_2_GetView_m2992416671 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_GetView_m2992416671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t2947988857* ____imap_2_t2947988857 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2947988857::IID, reinterpret_cast<void**>(&____imap_2_t2947988857));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IMapView_2_t1752361405* returnValue = NULL;
	hr = ____imap_2_t2947988857->IMap_2_GetView_m2992416671(&returnValue);
	____imap_2_t2947988857->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Object,System.Int32>::Insert(K,V)
extern "C"  bool IMap_2_Insert_m3186075525 (RuntimeObject* __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	IMap_2_t2947988857* ____imap_2_t2947988857 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2947988857::IID, reinterpret_cast<void**>(&____imap_2_t2947988857));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t2947988857->IMap_2_Insert_m3186075525(____key0_marshaled, ___value1, &returnValue);
	____imap_2_t2947988857->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMap`2<System.Object,System.Int32>::Remove(K)
extern "C"  void IMap_2_Remove_m1560976792 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMap_2_t2947988857* ____imap_2_t2947988857 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2947988857::IID, reinterpret_cast<void**>(&____imap_2_t2947988857));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	hr = ____imap_2_t2947988857->IMap_2_Remove_m1560976792(____key0_marshaled);
	____imap_2_t2947988857->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

}
// System.Void Windows.Foundation.Collections.IMap`2<System.Object,System.Int32>::Clear()
extern "C"  void IMap_2_Clear_m1661948797 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t2947988857* ____imap_2_t2947988857 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2947988857::IID, reinterpret_cast<void**>(&____imap_2_t2947988857));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t2947988857->IMap_2_Clear_m1661948797();
	____imap_2_t2947988857->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
