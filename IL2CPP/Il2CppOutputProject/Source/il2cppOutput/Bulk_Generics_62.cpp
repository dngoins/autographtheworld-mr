﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.IEnumerable`1<System.Byte>
struct IEnumerable_1_t114149265;
// System.Collections.Generic.IEnumerable`1<System.Byte>[]
struct IEnumerable_1U5BU5D_t3387783948;
// System.Collections.Generic.IEnumerable`1<System.Char>
struct IEnumerable_1_t2614313359;
// System.Collections.Generic.IEnumerable`1<System.Char>[]
struct IEnumerable_1U5BU5D_t2799407958;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t1510070208;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct IEnumerable_1U5BU5D_t3684372801;
// System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>
struct IEnumerable_1_t921020900;
// System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>[]
struct IEnumerable_1U5BU5D_t614637069;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1930798642;
// System.Collections.Generic.IEnumerable`1<System.Int32>[]
struct IEnumerable_1U5BU5D_t3951349959;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// System.Collections.Generic.IEnumerable`1<System.Object>[]
struct IEnumerable_1U5BU5D_t2115075616;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t1463797649;
// System.Collections.Generic.IEnumerable`1<System.Type>[]
struct IEnumerable_1U5BU5D_t3212016396;
// System.Collections.Generic.IList`1<System.Byte>
struct IList_1_t2949616159;
// System.Collections.Generic.IList`1<System.Byte>[]
struct IList_1U5BU5D_t3612319366;
// System.Collections.Generic.IList`1<System.Char>
struct IList_1_t1154812957;
// System.Collections.Generic.IList`1<System.Char>[]
struct IList_1U5BU5D_t3023943376;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IList_1_t50569806;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct IList_1U5BU5D_t3908908219;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t471298240;
// System.Collections.Generic.IList`1<System.Int32>[]
struct IList_1U5BU5D_t4175885377;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t600458651;
// System.Collections.Generic.IList`1<System.Object>[]
struct IList_1U5BU5D_t2339611034;
// System.Collections.Generic.IList`1<System.Type>
struct IList_1_t4297247;
// System.Collections.Generic.IList`1<System.Type>[]
struct IList_1U5BU5D_t3436551814;
// System.Collections.Generic.IReadOnlyList`1<System.Byte>
struct IReadOnlyList_1_t1698719282;
// System.Collections.Generic.IReadOnlyList`1<System.Byte>[]
struct IReadOnlyList_1U5BU5D_t2744586439;
// System.Collections.Generic.IReadOnlyList`1<System.Char>
struct IReadOnlyList_1_t4198883376;
// System.Collections.Generic.IReadOnlyList`1<System.Char>[]
struct IReadOnlyList_1U5BU5D_t2156210449;
// System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IReadOnlyList_1_t3094640225;
// System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct IReadOnlyList_1U5BU5D_t3041175292;
// System.Collections.Generic.IReadOnlyList`1<System.Int32>
struct IReadOnlyList_1_t3515368659;
// System.Collections.Generic.IReadOnlyList`1<System.Int32>[]
struct IReadOnlyList_1U5BU5D_t3308152450;
// System.Collections.Generic.IReadOnlyList`1<System.Object>
struct IReadOnlyList_1_t3644529070;
// System.Collections.Generic.IReadOnlyList`1<System.Object>[]
struct IReadOnlyList_1U5BU5D_t1471878107;
// System.Collections.Generic.IReadOnlyList`1<System.Type>
struct IReadOnlyList_1_t3048367666;
// System.Collections.Generic.IReadOnlyList`1<System.Type>[]
struct IReadOnlyList_1U5BU5D_t2568818887;
// System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>[]
struct KeyValuePair_2U5BU5D_t3332857673;
// System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>[]
struct KeyValuePair_2U5BU5D_t134831157;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t2652375035;
// System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>[]
struct KeyValuePair_2U5BU5D_t3818836818;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;

extern RuntimeClass* Il2CppComObject_il2cpp_TypeInfo_var;
extern const uint32_t IIterator_1_get_Current_m1519559364_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m2546598823_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m1349286191_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m584948694_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m2603537795_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m2880769161_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m187820912_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m3357328415_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m1422813924_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m3762768911_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m3392241508_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m1876875758_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m2769114354_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m570960681_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m515121726_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m2394539671_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m3243601558_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m2939364071_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m3712385382_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m3382075537_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m923614831_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m4123894872_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m3862601297_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m3397438022_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m332460872_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m2493206689_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m2135388021_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m2625825879_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m3966292519_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m3700328516_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m572385241_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m3167107739_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m4063167894_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m141829215_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m3967696190_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m1750935882_MetadataUsageId;
extern const uint32_t IIterator_1_get_Current_m2737245895_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m289763957_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t231828568_il2cpp_TypeInfo_var;
extern const uint32_t IIterator_1_get_Current_m74209353_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m3197996383_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t71524366_il2cpp_TypeInfo_var;
extern const uint32_t IIterator_1_get_Current_m2947568524_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m3900158597_MetadataUsageId;
extern RuntimeClass* KeyValuePair_2_t2245450819_il2cpp_TypeInfo_var;
extern const uint32_t IIterator_1_get_Current_m3547052056_MetadataUsageId;
extern const uint32_t IIterator_1_GetMany_m3058286030_MetadataUsageId;
struct IVectorView_1_t1822810022;
struct IVector_1_t3224566481;
struct IVectorView_1_t2418971426;
struct IVectorView_1_t2973325732;
struct IVectorView_1_t473161638;
struct IKeyValuePair_2_t3554936937;
struct IVectorView_1_t1869082581;
struct IVector_1_t3270839040;
struct IVector_1_t3820727885;
struct IVector_1_t3691567474;
struct IKeyValuePair_2_t660386782;
struct IKeyValuePair_2_t1096243984;
struct IKeyValuePair_2_t1256548186;
struct IVectorView_1_t2289811015;
struct IVector_1_t80114895;
struct IIterable_1_t3050661132;
struct IKeyValuePair_2_t3270170437;
struct IIterator_1_t3303407145;
struct IIterator_1_t1508603943;
struct IIterable_1_t1255857930;
struct IIterable_1_t151614779;
struct IIterable_1_t105342220;
struct IIterator_1_t954249637;
struct IIterator_1_t358088233;
struct IVector_1_t1874918097;
struct IIterable_1_t701503624;
struct IIterable_1_t3857532767;
struct IIterator_1_t404360792;
struct IIterator_1_t4110278780;
struct IIterator_1_t825089226;
struct IIterable_1_t572343213;

struct BooleanU5BU5D_t2897418192;
struct ByteU5BU5D_t4116647657;
struct CharU5BU5D_t3528271667;
struct IEnumerable_1U5BU5D_t3387783948;
struct IEnumerable_1U5BU5D_t2799407958;
struct IEnumerable_1U5BU5D_t3684372801;
struct IEnumerable_1U5BU5D_t614637069;
struct IEnumerable_1U5BU5D_t3951349959;
struct IEnumerable_1U5BU5D_t2115075616;
struct IEnumerable_1U5BU5D_t3212016396;
struct IList_1U5BU5D_t3612319366;
struct IList_1U5BU5D_t3023943376;
struct IList_1U5BU5D_t3908908219;
struct IList_1U5BU5D_t4175885377;
struct IList_1U5BU5D_t2339611034;
struct IList_1U5BU5D_t3436551814;
struct IReadOnlyList_1U5BU5D_t2744586439;
struct IReadOnlyList_1U5BU5D_t2156210449;
struct IReadOnlyList_1U5BU5D_t3041175292;
struct IReadOnlyList_1U5BU5D_t3308152450;
struct IReadOnlyList_1U5BU5D_t1471878107;
struct IReadOnlyList_1U5BU5D_t2568818887;
struct KeyValuePair_2U5BU5D_t3332857673;
struct KeyValuePair_2U5BU5D_t134831157;
struct KeyValuePair_2U5BU5D_t2652375035;
struct KeyValuePair_2U5BU5D_t3818836818;


// Windows.Foundation.Collections.IVector`1<System.Type>
struct NOVTABLE IVector_1_t3224566481 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m3023396459(uint32_t ___index0, Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m3611201515(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m2404813821(IVectorView_1_t1822810022** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m2314055355(Type_t * ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m3724063766(uint32_t ___index0, Type_t * ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m2145145338(uint32_t ___index0, Type_t * ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m154224944(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m386838768(Type_t * ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m110681880() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m1062744925() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m1218665569(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Type_t ** ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m4247109972(uint32_t ___items0ArraySize, Type_t ** ___items0) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Type>>
struct NOVTABLE IIterator_1_t2173408016 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m332460872(IVector_1_t3224566481** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1193456253(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1942606005(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2493206689(uint32_t ___items0ArraySize, IVector_1_t3224566481** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Object>
struct NOVTABLE IVector_1_t3820727885 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m219694725(uint32_t ___index0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m2561126536(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m3086397386(IVectorView_1_t2418971426** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m1930043913(Il2CppIInspectable* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m1678827239(uint32_t ___index0, Il2CppIInspectable* ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m1249203443(uint32_t ___index0, Il2CppIInspectable* ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m1710167275(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m1926269351(Il2CppIInspectable* ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m3025735428() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m305000098() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m3541038849(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Il2CppIInspectable** ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m2515130844(uint32_t ___items0ArraySize, Il2CppIInspectable** ___items0) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Char>>
struct NOVTABLE IIterator_1_t2073026849 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3966292519(IVectorView_1_t2973325732** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3490881394(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3512902479(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3700328516(uint32_t ___items0ArraySize, IVectorView_1_t2973325732** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Byte>
struct NOVTABLE IVectorView_1_t473161638 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m54195267(uint32_t ___index0, uint8_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m473387791(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m3353801494(uint8_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3912635590(uint32_t ___startIndex0, uint32_t ___items1ArraySize, uint8_t* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Byte>>
struct NOVTABLE IIterator_1_t3867830051 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2135388021(IVectorView_1_t473161638** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2920819059(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2787953408(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2625825879(uint32_t ___items0ArraySize, IVectorView_1_t473161638** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct NOVTABLE IVector_1_t3270839040 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m4281933817(uint32_t ___index0, IKeyValuePair_2_t3554936937** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m3264460740(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m2985413962(IVectorView_1_t1869082581** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m1739374806(IKeyValuePair_2_t3554936937* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m4098079864(uint32_t ___index0, IKeyValuePair_2_t3554936937* ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m1532618444(uint32_t ___index0, IKeyValuePair_2_t3554936937* ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m2367384165(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m1636376961(IKeyValuePair_2_t3554936937* ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m1114958347() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m555469687() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m275626091(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t3554936937** ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m2100117582(uint32_t ___items0ArraySize, IKeyValuePair_2_t3554936937** ___items0) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>
struct NOVTABLE IIterator_1_t2219680575 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3712385382(IVector_1_t3270839040** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m553689303(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1785064651(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3382075537(uint32_t ___items0ArraySize, IVector_1_t3270839040** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Char>
struct NOVTABLE IVector_1_t80114895 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m3761631649(uint32_t ___index0, Il2CppChar* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m729810301(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m3718260831(IVectorView_1_t2973325732** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m1347992874(Il2CppChar ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m38195806(uint32_t ___index0, Il2CppChar ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m843679895(uint32_t ___index0, Il2CppChar ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m1420036444(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m3292248582(Il2CppChar ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m3410828052() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m4210028140() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m1663454247(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Il2CppChar* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m968700710(uint32_t ___items0ArraySize, Il2CppChar* ___items0) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Object>>
struct NOVTABLE IIterator_1_t2769569420 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3862601297(IVector_1_t3820727885** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m335949631(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1293331021(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3397438022(uint32_t ___items0ArraySize, IVector_1_t3820727885** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IKeyValuePair`2<System.Int32,System.Object>
struct NOVTABLE IKeyValuePair_2_t1096243984 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m2186241555(int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m210688108(Il2CppIInspectable** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Int32>>
struct NOVTABLE IIterator_1_t2640409009 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m923614831(IVector_1_t3691567474** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2921379558(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1403774112(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m4123894872(uint32_t ___items0ArraySize, IVector_1_t3691567474** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Type>
struct NOVTABLE IVectorView_1_t1822810022 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m3708182184(uint32_t ___index0, Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1097778865(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m1129849567(Type_t * ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3274208783(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Type_t ** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Type>>
struct NOVTABLE IIterator_1_t922511139 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2737245895(IVectorView_1_t1822810022** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3441350389(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1152394543(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m289763957(uint32_t ___items0ArraySize, IVectorView_1_t1822810022** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Object>
struct NOVTABLE IVectorView_1_t2418971426 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m726655415(uint32_t ___index0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1102039859(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2349108669(Il2CppIInspectable* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m4226896717(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Il2CppIInspectable** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>>
struct NOVTABLE IIterator_1_t1804777933 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3513860320(IKeyValuePair_2_t660386782** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m65888504(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m859540255(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3415100491(uint32_t ___items0ArraySize, IKeyValuePair_2_t660386782** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct NOVTABLE IIterator_1_t2240635135 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2947568524(IKeyValuePair_2_t1096243984** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3859218363(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2670346608(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3900158597(uint32_t ___items0ArraySize, IKeyValuePair_2_t1096243984** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>
struct NOVTABLE IIterator_1_t2400939337 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m74209353(IKeyValuePair_2_t1256548186** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2249475151(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2303149837(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3197996383(uint32_t ___items0ArraySize, IKeyValuePair_2_t1256548186** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct NOVTABLE IVectorView_1_t1869082581 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m4135153857(uint32_t ___index0, IKeyValuePair_2_t3554936937** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3464555386(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2426904384(IKeyValuePair_2_t3554936937* ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m718367012(uint32_t ___startIndex0, uint32_t ___items1ArraySize, IKeyValuePair_2_t3554936937** ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>
struct NOVTABLE IIterator_1_t968783698 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m572385241(IVectorView_1_t1869082581** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2610655164(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1915646989(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3167107739(uint32_t ___items0ArraySize, IVectorView_1_t1869082581** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Char>
struct NOVTABLE IVectorView_1_t2973325732 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1110858976(uint32_t ___index0, Il2CppChar* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1535793255(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m613537994(Il2CppChar ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3420541689(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Il2CppChar* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Object>>
struct NOVTABLE IIterator_1_t1518672543 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3967696190(IVectorView_1_t2418971426** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2125417207(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3930599802(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1750935882(uint32_t ___items0ArraySize, IVectorView_1_t2418971426** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Int32>
struct NOVTABLE IVectorView_1_t2289811015 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1038499426(uint32_t ___index0, int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m3269133634(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m711808760(int32_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m632553004(uint32_t ___startIndex0, uint32_t ___items1ArraySize, int32_t* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Int32>>
struct NOVTABLE IIterator_1_t1389512132 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m4063167894(IVectorView_1_t2289811015** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m410236777(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m6758746(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m141829215(uint32_t ___items0ArraySize, IVectorView_1_t2289811015** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Char>>
struct NOVTABLE IIterator_1_t3323923726 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3243601558(IVector_1_t80114895** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3775173996(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2311052861(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2939364071(uint32_t ___items0ArraySize, IVector_1_t80114895** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Byte>>
struct NOVTABLE IIterator_1_t2283260034 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1519559364(IIterable_1_t3050661132** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1556416047(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m4202981245(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2546598823(uint32_t ___items0ArraySize, IIterable_1_t3050661132** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct NOVTABLE IIterator_1_t119594292 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3547052056(IKeyValuePair_2_t3270170437** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m344842528(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1308250629(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3058286030(uint32_t ___items0ArraySize, IKeyValuePair_2_t3270170437** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Byte>
struct NOVTABLE IIterable_1_t3050661132 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m770671629(IIterator_1_t3303407145** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Char>
struct NOVTABLE IIterable_1_t1255857930 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m2678403700(IIterator_1_t1508603943** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Char>>
struct NOVTABLE IIterator_1_t488456832 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1349286191(IIterable_1_t1255857930** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2777725380(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1861778747(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m584948694(uint32_t ___items0ArraySize, IIterable_1_t1255857930** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Char>
struct NOVTABLE IIterator_1_t1508603943 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m917445785(Il2CppChar* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2716317294(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3026282028(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2511776893(uint32_t ___items0ArraySize, Il2CppChar* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Boolean>
struct NOVTABLE IIterator_1_t2266398734 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2444129316(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2357690268(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1331932701(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m280069361(uint32_t ___items0ArraySize, bool* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Int32>
struct NOVTABLE IVector_1_t3691567474 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m669642145(uint32_t ___index0, int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m1083909874(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m1457083017(IVectorView_1_t2289811015** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m1513096330(int32_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m2697343022(uint32_t ___index0, int32_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m2016133864(uint32_t ___index0, int32_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m2081155343(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m71244649(int32_t ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m2906723444() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m2316468687() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m3133207996(uint32_t ___startIndex0, uint32_t ___items1ArraySize, int32_t* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m4247928211(uint32_t ___items0ArraySize, int32_t* ___items0) = 0;
};
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
// Windows.Foundation.Collections.IKeyValuePair`2<System.Int64,System.Object>
struct NOVTABLE IKeyValuePair_2_t3270170437 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m1937891137(int64_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m1356929877(Il2CppIInspectable** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Byte>
struct NOVTABLE IIterator_1_t3303407145 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2682899182(uint8_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m2127558108(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3980616567(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3559710954(uint32_t ___items0ArraySize, uint8_t* ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>
struct NOVTABLE IIterator_1_t3679180977 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2603537795(IIterable_1_t151614779** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3338406598(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m1516355594(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2880769161(uint32_t ___items0ArraySize, IIterable_1_t151614779** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Type>>
struct NOVTABLE IIterator_1_t3632908418 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m2769114354(IIterable_1_t105342220** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1172506826(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2441608574(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m570960681(uint32_t ___items0ArraySize, IIterable_1_t105342220** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Object>
struct NOVTABLE IIterable_1_t701503624 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m3341821894(IIterator_1_t954249637** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Type>
struct NOVTABLE IIterable_1_t105342220 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m449735320(IIterator_1_t358088233** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Byte>
struct NOVTABLE IVector_1_t1874918097 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m156571538(uint32_t ___index0, uint8_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m216665452(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m2759398957(IVectorView_1_t473161638** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m2293818318(uint8_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m3049348516(uint32_t ___index0, uint8_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m1632374023(uint32_t ___index0, uint8_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m313767260(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m1600833624(uint8_t ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m2280259536() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m1881849540() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m3108887717(uint32_t ___startIndex0, uint32_t ___items1ArraySize, uint8_t* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m3769885758(uint32_t ___items0ArraySize, uint8_t* ___items0) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Byte>>
struct NOVTABLE IIterator_1_t823759632 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m515121726(IVector_1_t1874918097** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1175499161(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2055979826(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m2394539671(uint32_t ___items0ArraySize, IVector_1_t1874918097** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Object>>
struct NOVTABLE IIterator_1_t4229069822 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m3392241508(IIterable_1_t701503624** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3736649651(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m4151249391(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m1876875758(uint32_t ___items0ArraySize, IIterable_1_t701503624** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>>
struct NOVTABLE IIterator_1_t3090131669 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m187820912(IIterable_1_t3857532767** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m3044678832(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m2342562340(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3357328415(uint32_t ___items0ArraySize, IIterable_1_t3857532767** ___items0, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct NOVTABLE IIterable_1_t151614779 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m4041697847(IIterator_1_t404360792** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Collections.IEnumerable>
struct NOVTABLE IIterable_1_t3857532767 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m595838657(IIterator_1_t4110278780** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterable`1<System.Int32>
struct NOVTABLE IIterable_1_t572343213 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterable_1_First_m1280577591(IIterator_1_t825089226** comReturnValue) = 0;
};
// Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Int32>>
struct NOVTABLE IIterator_1_t4099909411 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_Current_m1422813924(IIterable_1_t572343213** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_get_HasCurrent_m1945045992(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_MoveNext_m3946051403(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IIterator_1_GetMany_m3762768911(uint32_t ___items0ArraySize, IIterable_1_t572343213** ___items0, uint32_t* comReturnValue) = 0;
};
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef KEYVALUEPAIR_2_T2245450819_H
#define KEYVALUEPAIR_2_T2245450819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>
struct  KeyValuePair_2_t2245450819 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int64_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2245450819, ___key_0)); }
	inline int64_t get_key_0() const { return ___key_0; }
	inline int64_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int64_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2245450819, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2245450819_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_t4116647657* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_t4116647657* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_t4116647657* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((&___categoryForLatin1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef KEYVALUEPAIR_2_T71524366_H
#define KEYVALUEPAIR_2_T71524366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
struct  KeyValuePair_2_t71524366 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t71524366, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t71524366, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T71524366_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_4;
	// System.Int16 System.Guid::_b
	int16_t ____b_5;
	// System.Int16 System.Guid::_c
	int16_t ____c_6;
	// System.Byte System.Guid::_d
	uint8_t ____d_7;
	// System.Byte System.Guid::_e
	uint8_t ____e_8;
	// System.Byte System.Guid::_f
	uint8_t ____f_9;
	// System.Byte System.Guid::_g
	uint8_t ____g_10;
	// System.Byte System.Guid::_h
	uint8_t ____h_11;
	// System.Byte System.Guid::_i
	uint8_t ____i_12;
	// System.Byte System.Guid::_j
	uint8_t ____j_13;
	// System.Byte System.Guid::_k
	uint8_t ____k_14;

public:
	inline static int32_t get_offset_of__a_4() { return static_cast<int32_t>(offsetof(Guid_t, ____a_4)); }
	inline int32_t get__a_4() const { return ____a_4; }
	inline int32_t* get_address_of__a_4() { return &____a_4; }
	inline void set__a_4(int32_t value)
	{
		____a_4 = value;
	}

	inline static int32_t get_offset_of__b_5() { return static_cast<int32_t>(offsetof(Guid_t, ____b_5)); }
	inline int16_t get__b_5() const { return ____b_5; }
	inline int16_t* get_address_of__b_5() { return &____b_5; }
	inline void set__b_5(int16_t value)
	{
		____b_5 = value;
	}

	inline static int32_t get_offset_of__c_6() { return static_cast<int32_t>(offsetof(Guid_t, ____c_6)); }
	inline int16_t get__c_6() const { return ____c_6; }
	inline int16_t* get_address_of__c_6() { return &____c_6; }
	inline void set__c_6(int16_t value)
	{
		____c_6 = value;
	}

	inline static int32_t get_offset_of__d_7() { return static_cast<int32_t>(offsetof(Guid_t, ____d_7)); }
	inline uint8_t get__d_7() const { return ____d_7; }
	inline uint8_t* get_address_of__d_7() { return &____d_7; }
	inline void set__d_7(uint8_t value)
	{
		____d_7 = value;
	}

	inline static int32_t get_offset_of__e_8() { return static_cast<int32_t>(offsetof(Guid_t, ____e_8)); }
	inline uint8_t get__e_8() const { return ____e_8; }
	inline uint8_t* get_address_of__e_8() { return &____e_8; }
	inline void set__e_8(uint8_t value)
	{
		____e_8 = value;
	}

	inline static int32_t get_offset_of__f_9() { return static_cast<int32_t>(offsetof(Guid_t, ____f_9)); }
	inline uint8_t get__f_9() const { return ____f_9; }
	inline uint8_t* get_address_of__f_9() { return &____f_9; }
	inline void set__f_9(uint8_t value)
	{
		____f_9 = value;
	}

	inline static int32_t get_offset_of__g_10() { return static_cast<int32_t>(offsetof(Guid_t, ____g_10)); }
	inline uint8_t get__g_10() const { return ____g_10; }
	inline uint8_t* get_address_of__g_10() { return &____g_10; }
	inline void set__g_10(uint8_t value)
	{
		____g_10 = value;
	}

	inline static int32_t get_offset_of__h_11() { return static_cast<int32_t>(offsetof(Guid_t, ____h_11)); }
	inline uint8_t get__h_11() const { return ____h_11; }
	inline uint8_t* get_address_of__h_11() { return &____h_11; }
	inline void set__h_11(uint8_t value)
	{
		____h_11 = value;
	}

	inline static int32_t get_offset_of__i_12() { return static_cast<int32_t>(offsetof(Guid_t, ____i_12)); }
	inline uint8_t get__i_12() const { return ____i_12; }
	inline uint8_t* get_address_of__i_12() { return &____i_12; }
	inline void set__i_12(uint8_t value)
	{
		____i_12 = value;
	}

	inline static int32_t get_offset_of__j_13() { return static_cast<int32_t>(offsetof(Guid_t, ____j_13)); }
	inline uint8_t get__j_13() const { return ____j_13; }
	inline uint8_t* get_address_of__j_13() { return &____j_13; }
	inline void set__j_13(uint8_t value)
	{
		____j_13 = value;
	}

	inline static int32_t get_offset_of__k_14() { return static_cast<int32_t>(offsetof(Guid_t, ____k_14)); }
	inline uint8_t get__k_14() const { return ____k_14; }
	inline uint8_t* get_address_of__k_14() { return &____k_14; }
	inline void set__k_14(uint8_t value)
	{
		____k_14 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_0;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_1;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_2;
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_3;

public:
	inline static int32_t get_offset_of__rngAccess_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_0)); }
	inline RuntimeObject * get__rngAccess_0() const { return ____rngAccess_0; }
	inline RuntimeObject ** get_address_of__rngAccess_0() { return &____rngAccess_0; }
	inline void set__rngAccess_0(RuntimeObject * value)
	{
		____rngAccess_0 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_0), value);
	}

	inline static int32_t get_offset_of__rng_1() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_1)); }
	inline RandomNumberGenerator_t386037858 * get__rng_1() const { return ____rng_1; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_1() { return &____rng_1; }
	inline void set__rng_1(RandomNumberGenerator_t386037858 * value)
	{
		____rng_1 = value;
		Il2CppCodeGenWriteBarrier((&____rng_1), value);
	}

	inline static int32_t get_offset_of__fastRng_2() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_2)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_2() const { return ____fastRng_2; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_2() { return &____fastRng_2; }
	inline void set__fastRng_2(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_2 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_2), value);
	}

	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_3)); }
	inline Guid_t  get_Empty_3() const { return ___Empty_3; }
	inline Guid_t * get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(Guid_t  value)
	{
		___Empty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
// Windows.Foundation.Collections.IKeyValuePair`2<System.Guid,System.Object>
struct NOVTABLE IKeyValuePair_2_t1256548186 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Key_m3324945768(Guid_t * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IKeyValuePair_2_get_Value_m52124236(Il2CppIInspectable** comReturnValue) = 0;
};
#ifndef KEYVALUEPAIR_2_T3930634460_H
#define KEYVALUEPAIR_2_T3930634460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>
struct  KeyValuePair_2_t3930634460 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Guid_t  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	Type_t * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3930634460, ___key_0)); }
	inline Guid_t  get_key_0() const { return ___key_0; }
	inline Guid_t * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Guid_t  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3930634460, ___value_1)); }
	inline Type_t * get_value_1() const { return ___value_1; }
	inline Type_t ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Type_t * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3930634460_H
#ifndef KEYVALUEPAIR_2_T231828568_H
#define KEYVALUEPAIR_2_T231828568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>
struct  KeyValuePair_2_t231828568 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Guid_t  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t231828568, ___key_0)); }
	inline Guid_t  get_key_0() const { return ___key_0; }
	inline Guid_t * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Guid_t  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t231828568, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T231828568_H
// System.Boolean[]
struct BooleanU5BU5D_t2897418192  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) bool m_Items[1];

public:
	inline bool GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline bool* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline bool GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline bool* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Char[]
struct CharU5BU5D_t3528271667  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.IEnumerable`1<System.Byte>[]
struct IEnumerable_1U5BU5D_t3387783948  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IEnumerable`1<System.Char>[]
struct IEnumerable_1U5BU5D_t2799407958  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct IEnumerable_1U5BU5D_t3684372801  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>[]
struct IEnumerable_1U5BU5D_t614637069  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IEnumerable`1<System.Int32>[]
struct IEnumerable_1U5BU5D_t3951349959  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IEnumerable`1<System.Object>[]
struct IEnumerable_1U5BU5D_t2115075616  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IEnumerable`1<System.Type>[]
struct IEnumerable_1U5BU5D_t3212016396  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IList`1<System.Byte>[]
struct IList_1U5BU5D_t3612319366  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IList`1<System.Char>[]
struct IList_1U5BU5D_t3023943376  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct IList_1U5BU5D_t3908908219  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IList`1<System.Int32>[]
struct IList_1U5BU5D_t4175885377  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IList`1<System.Object>[]
struct IList_1U5BU5D_t2339611034  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IList`1<System.Type>[]
struct IList_1U5BU5D_t3436551814  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IReadOnlyList`1<System.Byte>[]
struct IReadOnlyList_1U5BU5D_t2744586439  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IReadOnlyList`1<System.Char>[]
struct IReadOnlyList_1U5BU5D_t2156210449  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct IReadOnlyList_1U5BU5D_t3041175292  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IReadOnlyList`1<System.Int32>[]
struct IReadOnlyList_1U5BU5D_t3308152450  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IReadOnlyList`1<System.Object>[]
struct IReadOnlyList_1U5BU5D_t1471878107  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.IReadOnlyList`1<System.Type>[]
struct IReadOnlyList_1U5BU5D_t2568818887  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>[]
struct KeyValuePair_2U5BU5D_t3332857673  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t231828568  m_Items[1];

public:
	inline KeyValuePair_2_t231828568  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t231828568 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t231828568  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t231828568  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t231828568 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t231828568  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>[]
struct KeyValuePair_2U5BU5D_t134831157  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3930634460  m_Items[1];

public:
	inline KeyValuePair_2_t3930634460  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t3930634460 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3930634460  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t3930634460  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t3930634460 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t3930634460  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t2652375035  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t71524366  m_Items[1];

public:
	inline KeyValuePair_2_t71524366  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t71524366 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t71524366  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t71524366  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t71524366 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t71524366  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>[]
struct KeyValuePair_2U5BU5D_t3818836818  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2245450819  m_Items[1];

public:
	inline KeyValuePair_2_t2245450819  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t2245450819 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2245450819  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t2245450819  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t2245450819 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t2245450819  value)
	{
		m_Items[index] = value;
	}
};



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// T Windows.Foundation.Collections.IIterator`1<System.Boolean>::get_Current()
extern "C"  bool IIterator_1_get_Current_m2444129316 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2266398734* ____iiterator_1_t2266398734 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2266398734::IID, reinterpret_cast<void**>(&____iiterator_1_t2266398734));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2266398734->IIterator_1_get_Current_m2444129316(&returnValue);
	____iiterator_1_t2266398734->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Boolean>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m2357690268 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2266398734* ____iiterator_1_t2266398734 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2266398734::IID, reinterpret_cast<void**>(&____iiterator_1_t2266398734));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2266398734->IIterator_1_get_HasCurrent_m2357690268(&returnValue);
	____iiterator_1_t2266398734->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Boolean>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m1331932701 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2266398734* ____iiterator_1_t2266398734 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2266398734::IID, reinterpret_cast<void**>(&____iiterator_1_t2266398734));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2266398734->IIterator_1_MoveNext_m1331932701(&returnValue);
	____iiterator_1_t2266398734->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Boolean>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m280069361 (RuntimeObject* __this, BooleanU5BU5D_t2897418192* ___items0, const RuntimeMethod* method)
{
	IIterator_1_t2266398734* ____iiterator_1_t2266398734 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2266398734::IID, reinterpret_cast<void**>(&____iiterator_1_t2266398734));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	bool* ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<bool>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(bool));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t2266398734->IIterator_1_GetMany_m280069361(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t2266398734->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items0_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	il2cpp_codegen_marshal_free(____items0_marshaled);
	____items0_marshaled = NULL;

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Byte>::get_Current()
extern "C"  uint8_t IIterator_1_get_Current_m2682899182 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3303407145* ____iiterator_1_t3303407145 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3303407145::IID, reinterpret_cast<void**>(&____iiterator_1_t3303407145));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint8_t returnValue = 0;
	hr = ____iiterator_1_t3303407145->IIterator_1_get_Current_m2682899182(&returnValue);
	____iiterator_1_t3303407145->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Byte>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m2127558108 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3303407145* ____iiterator_1_t3303407145 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3303407145::IID, reinterpret_cast<void**>(&____iiterator_1_t3303407145));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3303407145->IIterator_1_get_HasCurrent_m2127558108(&returnValue);
	____iiterator_1_t3303407145->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Byte>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m3980616567 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3303407145* ____iiterator_1_t3303407145 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3303407145::IID, reinterpret_cast<void**>(&____iiterator_1_t3303407145));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3303407145->IIterator_1_MoveNext_m3980616567(&returnValue);
	____iiterator_1_t3303407145->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Byte>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m3559710954 (RuntimeObject* __this, ByteU5BU5D_t4116647657* ___items0, const RuntimeMethod* method)
{
	IIterator_1_t3303407145* ____iiterator_1_t3303407145 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3303407145::IID, reinterpret_cast<void**>(&____iiterator_1_t3303407145));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	uint8_t* ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<uint8_t>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(uint8_t));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t3303407145->IIterator_1_GetMany_m3559710954(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t3303407145->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items0_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	il2cpp_codegen_marshal_free(____items0_marshaled);
	____items0_marshaled = NULL;

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Char>::get_Current()
extern "C"  Il2CppChar IIterator_1_get_Current_m917445785 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t1508603943* ____iiterator_1_t1508603943 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t1508603943::IID, reinterpret_cast<void**>(&____iiterator_1_t1508603943));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppChar returnValue = 0;
	hr = ____iiterator_1_t1508603943->IIterator_1_get_Current_m917445785(&returnValue);
	____iiterator_1_t1508603943->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Char>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m2716317294 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t1508603943* ____iiterator_1_t1508603943 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t1508603943::IID, reinterpret_cast<void**>(&____iiterator_1_t1508603943));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t1508603943->IIterator_1_get_HasCurrent_m2716317294(&returnValue);
	____iiterator_1_t1508603943->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Char>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m3026282028 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t1508603943* ____iiterator_1_t1508603943 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t1508603943::IID, reinterpret_cast<void**>(&____iiterator_1_t1508603943));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t1508603943->IIterator_1_MoveNext_m3026282028(&returnValue);
	____iiterator_1_t1508603943->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Char>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m2511776893 (RuntimeObject* __this, CharU5BU5D_t3528271667* ___items0, const RuntimeMethod* method)
{
	IIterator_1_t1508603943* ____iiterator_1_t1508603943 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t1508603943::IID, reinterpret_cast<void**>(&____iiterator_1_t1508603943));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	Il2CppChar* ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<Il2CppChar>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(Il2CppChar));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t1508603943->IIterator_1_GetMany_m2511776893(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t1508603943->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items0_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	il2cpp_codegen_marshal_free(____items0_marshaled);
	____items0_marshaled = NULL;

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Byte>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m1519559364 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m1519559364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2283260034* ____iiterator_1_t2283260034 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2283260034::IID, reinterpret_cast<void**>(&____iiterator_1_t2283260034));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterable_1_t3050661132* returnValue = NULL;
	hr = ____iiterator_1_t2283260034->IIterator_1_get_Current_m1519559364(&returnValue);
	____iiterator_1_t2283260034->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Byte>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m1556416047 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2283260034* ____iiterator_1_t2283260034 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2283260034::IID, reinterpret_cast<void**>(&____iiterator_1_t2283260034));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2283260034->IIterator_1_get_HasCurrent_m1556416047(&returnValue);
	____iiterator_1_t2283260034->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Byte>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m4202981245 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2283260034* ____iiterator_1_t2283260034 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2283260034::IID, reinterpret_cast<void**>(&____iiterator_1_t2283260034));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2283260034->IIterator_1_MoveNext_m4202981245(&returnValue);
	____iiterator_1_t2283260034->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Byte>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m2546598823 (RuntimeObject* __this, IEnumerable_1U5BU5D_t3387783948* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m2546598823_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2283260034* ____iiterator_1_t2283260034 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2283260034::IID, reinterpret_cast<void**>(&____iiterator_1_t2283260034));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IIterable_1_t3050661132** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IIterable_1_t3050661132*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IIterable_1_t3050661132*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t2283260034->IIterator_1_GetMany_m2546598823(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t2283260034->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Char>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m1349286191 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m1349286191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t488456832* ____iiterator_1_t488456832 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t488456832::IID, reinterpret_cast<void**>(&____iiterator_1_t488456832));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterable_1_t1255857930* returnValue = NULL;
	hr = ____iiterator_1_t488456832->IIterator_1_get_Current_m1349286191(&returnValue);
	____iiterator_1_t488456832->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Char>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m2777725380 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t488456832* ____iiterator_1_t488456832 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t488456832::IID, reinterpret_cast<void**>(&____iiterator_1_t488456832));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t488456832->IIterator_1_get_HasCurrent_m2777725380(&returnValue);
	____iiterator_1_t488456832->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Char>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m1861778747 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t488456832* ____iiterator_1_t488456832 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t488456832::IID, reinterpret_cast<void**>(&____iiterator_1_t488456832));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t488456832->IIterator_1_MoveNext_m1861778747(&returnValue);
	____iiterator_1_t488456832->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Char>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m584948694 (RuntimeObject* __this, IEnumerable_1U5BU5D_t2799407958* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m584948694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t488456832* ____iiterator_1_t488456832 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t488456832::IID, reinterpret_cast<void**>(&____iiterator_1_t488456832));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IIterable_1_t1255857930** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IIterable_1_t1255857930*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IIterable_1_t1255857930*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t488456832->IIterator_1_GetMany_m584948694(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t488456832->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m2603537795 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m2603537795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3679180977* ____iiterator_1_t3679180977 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3679180977::IID, reinterpret_cast<void**>(&____iiterator_1_t3679180977));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterable_1_t151614779* returnValue = NULL;
	hr = ____iiterator_1_t3679180977->IIterator_1_get_Current_m2603537795(&returnValue);
	____iiterator_1_t3679180977->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m3338406598 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3679180977* ____iiterator_1_t3679180977 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3679180977::IID, reinterpret_cast<void**>(&____iiterator_1_t3679180977));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3679180977->IIterator_1_get_HasCurrent_m3338406598(&returnValue);
	____iiterator_1_t3679180977->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m1516355594 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3679180977* ____iiterator_1_t3679180977 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3679180977::IID, reinterpret_cast<void**>(&____iiterator_1_t3679180977));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3679180977->IIterator_1_MoveNext_m1516355594(&returnValue);
	____iiterator_1_t3679180977->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m2880769161 (RuntimeObject* __this, IEnumerable_1U5BU5D_t3684372801* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m2880769161_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3679180977* ____iiterator_1_t3679180977 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3679180977::IID, reinterpret_cast<void**>(&____iiterator_1_t3679180977));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IIterable_1_t151614779** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IIterable_1_t151614779*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IIterable_1_t151614779*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t3679180977->IIterator_1_GetMany_m2880769161(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t3679180977->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m187820912 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m187820912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3090131669* ____iiterator_1_t3090131669 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3090131669::IID, reinterpret_cast<void**>(&____iiterator_1_t3090131669));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterable_1_t3857532767* returnValue = NULL;
	hr = ____iiterator_1_t3090131669->IIterator_1_get_Current_m187820912(&returnValue);
	____iiterator_1_t3090131669->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m3044678832 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3090131669* ____iiterator_1_t3090131669 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3090131669::IID, reinterpret_cast<void**>(&____iiterator_1_t3090131669));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3090131669->IIterator_1_get_HasCurrent_m3044678832(&returnValue);
	____iiterator_1_t3090131669->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m2342562340 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3090131669* ____iiterator_1_t3090131669 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3090131669::IID, reinterpret_cast<void**>(&____iiterator_1_t3090131669));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3090131669->IIterator_1_MoveNext_m2342562340(&returnValue);
	____iiterator_1_t3090131669->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m3357328415 (RuntimeObject* __this, IEnumerable_1U5BU5D_t614637069* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m3357328415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3090131669* ____iiterator_1_t3090131669 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3090131669::IID, reinterpret_cast<void**>(&____iiterator_1_t3090131669));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IIterable_1_t3857532767** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IIterable_1_t3857532767*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IIterable_1_t3857532767*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t3090131669->IIterator_1_GetMany_m3357328415(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t3090131669->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Int32>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m1422813924 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m1422813924_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t4099909411* ____iiterator_1_t4099909411 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t4099909411::IID, reinterpret_cast<void**>(&____iiterator_1_t4099909411));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterable_1_t572343213* returnValue = NULL;
	hr = ____iiterator_1_t4099909411->IIterator_1_get_Current_m1422813924(&returnValue);
	____iiterator_1_t4099909411->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Int32>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m1945045992 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t4099909411* ____iiterator_1_t4099909411 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t4099909411::IID, reinterpret_cast<void**>(&____iiterator_1_t4099909411));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t4099909411->IIterator_1_get_HasCurrent_m1945045992(&returnValue);
	____iiterator_1_t4099909411->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Int32>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m3946051403 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t4099909411* ____iiterator_1_t4099909411 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t4099909411::IID, reinterpret_cast<void**>(&____iiterator_1_t4099909411));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t4099909411->IIterator_1_MoveNext_m3946051403(&returnValue);
	____iiterator_1_t4099909411->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Int32>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m3762768911 (RuntimeObject* __this, IEnumerable_1U5BU5D_t3951349959* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m3762768911_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t4099909411* ____iiterator_1_t4099909411 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t4099909411::IID, reinterpret_cast<void**>(&____iiterator_1_t4099909411));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IIterable_1_t572343213** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IIterable_1_t572343213*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IIterable_1_t572343213*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t4099909411->IIterator_1_GetMany_m3762768911(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t4099909411->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Object>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m3392241508 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m3392241508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t4229069822* ____iiterator_1_t4229069822 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t4229069822::IID, reinterpret_cast<void**>(&____iiterator_1_t4229069822));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterable_1_t701503624* returnValue = NULL;
	hr = ____iiterator_1_t4229069822->IIterator_1_get_Current_m3392241508(&returnValue);
	____iiterator_1_t4229069822->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Object>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m3736649651 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t4229069822* ____iiterator_1_t4229069822 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t4229069822::IID, reinterpret_cast<void**>(&____iiterator_1_t4229069822));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t4229069822->IIterator_1_get_HasCurrent_m3736649651(&returnValue);
	____iiterator_1_t4229069822->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Object>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m4151249391 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t4229069822* ____iiterator_1_t4229069822 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t4229069822::IID, reinterpret_cast<void**>(&____iiterator_1_t4229069822));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t4229069822->IIterator_1_MoveNext_m4151249391(&returnValue);
	____iiterator_1_t4229069822->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Object>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m1876875758 (RuntimeObject* __this, IEnumerable_1U5BU5D_t2115075616* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m1876875758_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t4229069822* ____iiterator_1_t4229069822 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t4229069822::IID, reinterpret_cast<void**>(&____iiterator_1_t4229069822));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IIterable_1_t701503624** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IIterable_1_t701503624*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IIterable_1_t701503624*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t4229069822->IIterator_1_GetMany_m1876875758(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t4229069822->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Type>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m2769114354 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m2769114354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3632908418* ____iiterator_1_t3632908418 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3632908418::IID, reinterpret_cast<void**>(&____iiterator_1_t3632908418));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IIterable_1_t105342220* returnValue = NULL;
	hr = ____iiterator_1_t3632908418->IIterator_1_get_Current_m2769114354(&returnValue);
	____iiterator_1_t3632908418->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Type>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m1172506826 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3632908418* ____iiterator_1_t3632908418 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3632908418::IID, reinterpret_cast<void**>(&____iiterator_1_t3632908418));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3632908418->IIterator_1_get_HasCurrent_m1172506826(&returnValue);
	____iiterator_1_t3632908418->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Type>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m2441608574 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3632908418* ____iiterator_1_t3632908418 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3632908418::IID, reinterpret_cast<void**>(&____iiterator_1_t3632908418));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3632908418->IIterator_1_MoveNext_m2441608574(&returnValue);
	____iiterator_1_t3632908418->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IEnumerable`1<System.Type>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m570960681 (RuntimeObject* __this, IEnumerable_1U5BU5D_t3212016396* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m570960681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3632908418* ____iiterator_1_t3632908418 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3632908418::IID, reinterpret_cast<void**>(&____iiterator_1_t3632908418));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IIterable_1_t105342220** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IIterable_1_t105342220*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IIterable_1_t105342220*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t3632908418->IIterator_1_GetMany_m570960681(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t3632908418->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Byte>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m515121726 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m515121726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t823759632* ____iiterator_1_t823759632 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t823759632::IID, reinterpret_cast<void**>(&____iiterator_1_t823759632));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVector_1_t1874918097* returnValue = NULL;
	hr = ____iiterator_1_t823759632->IIterator_1_get_Current_m515121726(&returnValue);
	____iiterator_1_t823759632->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Byte>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m1175499161 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t823759632* ____iiterator_1_t823759632 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t823759632::IID, reinterpret_cast<void**>(&____iiterator_1_t823759632));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t823759632->IIterator_1_get_HasCurrent_m1175499161(&returnValue);
	____iiterator_1_t823759632->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Byte>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m2055979826 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t823759632* ____iiterator_1_t823759632 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t823759632::IID, reinterpret_cast<void**>(&____iiterator_1_t823759632));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t823759632->IIterator_1_MoveNext_m2055979826(&returnValue);
	____iiterator_1_t823759632->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Byte>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m2394539671 (RuntimeObject* __this, IList_1U5BU5D_t3612319366* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m2394539671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t823759632* ____iiterator_1_t823759632 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t823759632::IID, reinterpret_cast<void**>(&____iiterator_1_t823759632));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IVector_1_t1874918097** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IVector_1_t1874918097*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IVector_1_t1874918097*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t823759632->IIterator_1_GetMany_m2394539671(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t823759632->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Char>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m3243601558 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m3243601558_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3323923726* ____iiterator_1_t3323923726 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3323923726::IID, reinterpret_cast<void**>(&____iiterator_1_t3323923726));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVector_1_t80114895* returnValue = NULL;
	hr = ____iiterator_1_t3323923726->IIterator_1_get_Current_m3243601558(&returnValue);
	____iiterator_1_t3323923726->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Char>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m3775173996 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3323923726* ____iiterator_1_t3323923726 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3323923726::IID, reinterpret_cast<void**>(&____iiterator_1_t3323923726));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3323923726->IIterator_1_get_HasCurrent_m3775173996(&returnValue);
	____iiterator_1_t3323923726->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Char>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m2311052861 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3323923726* ____iiterator_1_t3323923726 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3323923726::IID, reinterpret_cast<void**>(&____iiterator_1_t3323923726));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3323923726->IIterator_1_MoveNext_m2311052861(&returnValue);
	____iiterator_1_t3323923726->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Char>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m2939364071 (RuntimeObject* __this, IList_1U5BU5D_t3023943376* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m2939364071_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3323923726* ____iiterator_1_t3323923726 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3323923726::IID, reinterpret_cast<void**>(&____iiterator_1_t3323923726));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IVector_1_t80114895** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IVector_1_t80114895*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IVector_1_t80114895*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t3323923726->IIterator_1_GetMany_m2939364071(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t3323923726->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m3712385382 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m3712385382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2219680575* ____iiterator_1_t2219680575 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2219680575::IID, reinterpret_cast<void**>(&____iiterator_1_t2219680575));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVector_1_t3270839040* returnValue = NULL;
	hr = ____iiterator_1_t2219680575->IIterator_1_get_Current_m3712385382(&returnValue);
	____iiterator_1_t2219680575->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m553689303 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2219680575* ____iiterator_1_t2219680575 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2219680575::IID, reinterpret_cast<void**>(&____iiterator_1_t2219680575));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2219680575->IIterator_1_get_HasCurrent_m553689303(&returnValue);
	____iiterator_1_t2219680575->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m1785064651 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2219680575* ____iiterator_1_t2219680575 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2219680575::IID, reinterpret_cast<void**>(&____iiterator_1_t2219680575));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2219680575->IIterator_1_MoveNext_m1785064651(&returnValue);
	____iiterator_1_t2219680575->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m3382075537 (RuntimeObject* __this, IList_1U5BU5D_t3908908219* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m3382075537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2219680575* ____iiterator_1_t2219680575 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2219680575::IID, reinterpret_cast<void**>(&____iiterator_1_t2219680575));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IVector_1_t3270839040** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IVector_1_t3270839040*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IVector_1_t3270839040*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t2219680575->IIterator_1_GetMany_m3382075537(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t2219680575->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Int32>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m923614831 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m923614831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2640409009* ____iiterator_1_t2640409009 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2640409009::IID, reinterpret_cast<void**>(&____iiterator_1_t2640409009));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVector_1_t3691567474* returnValue = NULL;
	hr = ____iiterator_1_t2640409009->IIterator_1_get_Current_m923614831(&returnValue);
	____iiterator_1_t2640409009->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Int32>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m2921379558 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2640409009* ____iiterator_1_t2640409009 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2640409009::IID, reinterpret_cast<void**>(&____iiterator_1_t2640409009));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2640409009->IIterator_1_get_HasCurrent_m2921379558(&returnValue);
	____iiterator_1_t2640409009->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Int32>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m1403774112 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2640409009* ____iiterator_1_t2640409009 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2640409009::IID, reinterpret_cast<void**>(&____iiterator_1_t2640409009));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2640409009->IIterator_1_MoveNext_m1403774112(&returnValue);
	____iiterator_1_t2640409009->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Int32>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m4123894872 (RuntimeObject* __this, IList_1U5BU5D_t4175885377* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m4123894872_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2640409009* ____iiterator_1_t2640409009 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2640409009::IID, reinterpret_cast<void**>(&____iiterator_1_t2640409009));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IVector_1_t3691567474** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IVector_1_t3691567474*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IVector_1_t3691567474*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t2640409009->IIterator_1_GetMany_m4123894872(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t2640409009->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Object>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m3862601297 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m3862601297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2769569420* ____iiterator_1_t2769569420 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2769569420::IID, reinterpret_cast<void**>(&____iiterator_1_t2769569420));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVector_1_t3820727885* returnValue = NULL;
	hr = ____iiterator_1_t2769569420->IIterator_1_get_Current_m3862601297(&returnValue);
	____iiterator_1_t2769569420->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Object>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m335949631 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2769569420* ____iiterator_1_t2769569420 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2769569420::IID, reinterpret_cast<void**>(&____iiterator_1_t2769569420));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2769569420->IIterator_1_get_HasCurrent_m335949631(&returnValue);
	____iiterator_1_t2769569420->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Object>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m1293331021 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2769569420* ____iiterator_1_t2769569420 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2769569420::IID, reinterpret_cast<void**>(&____iiterator_1_t2769569420));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2769569420->IIterator_1_MoveNext_m1293331021(&returnValue);
	____iiterator_1_t2769569420->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Object>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m3397438022 (RuntimeObject* __this, IList_1U5BU5D_t2339611034* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m3397438022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2769569420* ____iiterator_1_t2769569420 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2769569420::IID, reinterpret_cast<void**>(&____iiterator_1_t2769569420));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IVector_1_t3820727885** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IVector_1_t3820727885*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IVector_1_t3820727885*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t2769569420->IIterator_1_GetMany_m3397438022(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t2769569420->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Type>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m332460872 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m332460872_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2173408016* ____iiterator_1_t2173408016 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2173408016::IID, reinterpret_cast<void**>(&____iiterator_1_t2173408016));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVector_1_t3224566481* returnValue = NULL;
	hr = ____iiterator_1_t2173408016->IIterator_1_get_Current_m332460872(&returnValue);
	____iiterator_1_t2173408016->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Type>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m1193456253 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2173408016* ____iiterator_1_t2173408016 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2173408016::IID, reinterpret_cast<void**>(&____iiterator_1_t2173408016));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2173408016->IIterator_1_get_HasCurrent_m1193456253(&returnValue);
	____iiterator_1_t2173408016->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Type>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m1942606005 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2173408016* ____iiterator_1_t2173408016 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2173408016::IID, reinterpret_cast<void**>(&____iiterator_1_t2173408016));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2173408016->IIterator_1_MoveNext_m1942606005(&returnValue);
	____iiterator_1_t2173408016->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IList`1<System.Type>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m2493206689 (RuntimeObject* __this, IList_1U5BU5D_t3436551814* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m2493206689_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2173408016* ____iiterator_1_t2173408016 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2173408016::IID, reinterpret_cast<void**>(&____iiterator_1_t2173408016));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IVector_1_t3224566481** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IVector_1_t3224566481*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IVector_1_t3224566481*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t2173408016->IIterator_1_GetMany_m2493206689(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t2173408016->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Byte>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m2135388021 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m2135388021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3867830051* ____iiterator_1_t3867830051 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3867830051::IID, reinterpret_cast<void**>(&____iiterator_1_t3867830051));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t473161638* returnValue = NULL;
	hr = ____iiterator_1_t3867830051->IIterator_1_get_Current_m2135388021(&returnValue);
	____iiterator_1_t3867830051->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Byte>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m2920819059 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3867830051* ____iiterator_1_t3867830051 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3867830051::IID, reinterpret_cast<void**>(&____iiterator_1_t3867830051));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3867830051->IIterator_1_get_HasCurrent_m2920819059(&returnValue);
	____iiterator_1_t3867830051->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Byte>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m2787953408 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t3867830051* ____iiterator_1_t3867830051 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3867830051::IID, reinterpret_cast<void**>(&____iiterator_1_t3867830051));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t3867830051->IIterator_1_MoveNext_m2787953408(&returnValue);
	____iiterator_1_t3867830051->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Byte>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m2625825879 (RuntimeObject* __this, IReadOnlyList_1U5BU5D_t2744586439* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m2625825879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t3867830051* ____iiterator_1_t3867830051 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t3867830051::IID, reinterpret_cast<void**>(&____iiterator_1_t3867830051));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IVectorView_1_t473161638** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IVectorView_1_t473161638*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IVectorView_1_t473161638*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t3867830051->IIterator_1_GetMany_m2625825879(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t3867830051->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Char>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m3966292519 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m3966292519_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2073026849* ____iiterator_1_t2073026849 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2073026849::IID, reinterpret_cast<void**>(&____iiterator_1_t2073026849));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t2973325732* returnValue = NULL;
	hr = ____iiterator_1_t2073026849->IIterator_1_get_Current_m3966292519(&returnValue);
	____iiterator_1_t2073026849->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Char>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m3490881394 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2073026849* ____iiterator_1_t2073026849 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2073026849::IID, reinterpret_cast<void**>(&____iiterator_1_t2073026849));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2073026849->IIterator_1_get_HasCurrent_m3490881394(&returnValue);
	____iiterator_1_t2073026849->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Char>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m3512902479 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2073026849* ____iiterator_1_t2073026849 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2073026849::IID, reinterpret_cast<void**>(&____iiterator_1_t2073026849));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2073026849->IIterator_1_MoveNext_m3512902479(&returnValue);
	____iiterator_1_t2073026849->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Char>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m3700328516 (RuntimeObject* __this, IReadOnlyList_1U5BU5D_t2156210449* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m3700328516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2073026849* ____iiterator_1_t2073026849 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2073026849::IID, reinterpret_cast<void**>(&____iiterator_1_t2073026849));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IVectorView_1_t2973325732** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IVectorView_1_t2973325732*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IVectorView_1_t2973325732*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t2073026849->IIterator_1_GetMany_m3700328516(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t2073026849->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m572385241 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m572385241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t968783698* ____iiterator_1_t968783698 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t968783698::IID, reinterpret_cast<void**>(&____iiterator_1_t968783698));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t1869082581* returnValue = NULL;
	hr = ____iiterator_1_t968783698->IIterator_1_get_Current_m572385241(&returnValue);
	____iiterator_1_t968783698->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m2610655164 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t968783698* ____iiterator_1_t968783698 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t968783698::IID, reinterpret_cast<void**>(&____iiterator_1_t968783698));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t968783698->IIterator_1_get_HasCurrent_m2610655164(&returnValue);
	____iiterator_1_t968783698->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m1915646989 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t968783698* ____iiterator_1_t968783698 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t968783698::IID, reinterpret_cast<void**>(&____iiterator_1_t968783698));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t968783698->IIterator_1_MoveNext_m1915646989(&returnValue);
	____iiterator_1_t968783698->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m3167107739 (RuntimeObject* __this, IReadOnlyList_1U5BU5D_t3041175292* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m3167107739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t968783698* ____iiterator_1_t968783698 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t968783698::IID, reinterpret_cast<void**>(&____iiterator_1_t968783698));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IVectorView_1_t1869082581** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IVectorView_1_t1869082581*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IVectorView_1_t1869082581*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t968783698->IIterator_1_GetMany_m3167107739(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t968783698->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Int32>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m4063167894 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m4063167894_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t1389512132* ____iiterator_1_t1389512132 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t1389512132::IID, reinterpret_cast<void**>(&____iiterator_1_t1389512132));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t2289811015* returnValue = NULL;
	hr = ____iiterator_1_t1389512132->IIterator_1_get_Current_m4063167894(&returnValue);
	____iiterator_1_t1389512132->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Int32>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m410236777 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t1389512132* ____iiterator_1_t1389512132 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t1389512132::IID, reinterpret_cast<void**>(&____iiterator_1_t1389512132));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t1389512132->IIterator_1_get_HasCurrent_m410236777(&returnValue);
	____iiterator_1_t1389512132->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Int32>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m6758746 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t1389512132* ____iiterator_1_t1389512132 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t1389512132::IID, reinterpret_cast<void**>(&____iiterator_1_t1389512132));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t1389512132->IIterator_1_MoveNext_m6758746(&returnValue);
	____iiterator_1_t1389512132->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Int32>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m141829215 (RuntimeObject* __this, IReadOnlyList_1U5BU5D_t3308152450* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m141829215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t1389512132* ____iiterator_1_t1389512132 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t1389512132::IID, reinterpret_cast<void**>(&____iiterator_1_t1389512132));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IVectorView_1_t2289811015** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IVectorView_1_t2289811015*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IVectorView_1_t2289811015*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t1389512132->IIterator_1_GetMany_m141829215(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t1389512132->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Object>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m3967696190 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m3967696190_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t1518672543* ____iiterator_1_t1518672543 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t1518672543::IID, reinterpret_cast<void**>(&____iiterator_1_t1518672543));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t2418971426* returnValue = NULL;
	hr = ____iiterator_1_t1518672543->IIterator_1_get_Current_m3967696190(&returnValue);
	____iiterator_1_t1518672543->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Object>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m2125417207 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t1518672543* ____iiterator_1_t1518672543 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t1518672543::IID, reinterpret_cast<void**>(&____iiterator_1_t1518672543));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t1518672543->IIterator_1_get_HasCurrent_m2125417207(&returnValue);
	____iiterator_1_t1518672543->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Object>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m3930599802 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t1518672543* ____iiterator_1_t1518672543 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t1518672543::IID, reinterpret_cast<void**>(&____iiterator_1_t1518672543));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t1518672543->IIterator_1_MoveNext_m3930599802(&returnValue);
	____iiterator_1_t1518672543->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Object>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m1750935882 (RuntimeObject* __this, IReadOnlyList_1U5BU5D_t1471878107* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m1750935882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t1518672543* ____iiterator_1_t1518672543 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t1518672543::IID, reinterpret_cast<void**>(&____iiterator_1_t1518672543));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IVectorView_1_t2418971426** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IVectorView_1_t2418971426*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IVectorView_1_t2418971426*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t1518672543->IIterator_1_GetMany_m1750935882(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t1518672543->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Type>>::get_Current()
extern "C"  RuntimeObject* IIterator_1_get_Current_m2737245895 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m2737245895_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t922511139* ____iiterator_1_t922511139 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t922511139::IID, reinterpret_cast<void**>(&____iiterator_1_t922511139));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t1822810022* returnValue = NULL;
	hr = ____iiterator_1_t922511139->IIterator_1_get_Current_m2737245895(&returnValue);
	____iiterator_1_t922511139->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Type>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m3441350389 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t922511139* ____iiterator_1_t922511139 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t922511139::IID, reinterpret_cast<void**>(&____iiterator_1_t922511139));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t922511139->IIterator_1_get_HasCurrent_m3441350389(&returnValue);
	____iiterator_1_t922511139->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Type>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m1152394543 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t922511139* ____iiterator_1_t922511139 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t922511139::IID, reinterpret_cast<void**>(&____iiterator_1_t922511139));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t922511139->IIterator_1_MoveNext_m1152394543(&returnValue);
	____iiterator_1_t922511139->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.IReadOnlyList`1<System.Type>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m289763957 (RuntimeObject* __this, IReadOnlyList_1U5BU5D_t2568818887* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m289763957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t922511139* ____iiterator_1_t922511139 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t922511139::IID, reinterpret_cast<void**>(&____iiterator_1_t922511139));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IVectorView_1_t1822810022** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IVectorView_1_t1822810022*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IVectorView_1_t1822810022*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t922511139->IIterator_1_GetMany_m289763957(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t922511139->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			RuntimeObject* _____items0_marshaled_i__unmarshaled = NULL;
			if ((____items0_marshaled)[i] != NULL)
			{
				_____items0_marshaled_i__unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>((____items0_marshaled)[i], Il2CppComObject_il2cpp_TypeInfo_var);
			}
			else
			{
				_____items0_marshaled_i__unmarshaled = NULL;
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t231828568  IIterator_1_get_Current_m74209353 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m74209353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2400939337* ____iiterator_1_t2400939337 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2400939337::IID, reinterpret_cast<void**>(&____iiterator_1_t2400939337));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IKeyValuePair_2_t1256548186* returnValue = NULL;
	hr = ____iiterator_1_t2400939337->IIterator_1_get_Current_m74209353(&returnValue);
	____iiterator_1_t2400939337->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	KeyValuePair_2_t231828568  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	NullCheck(returnValue);

	{
		Il2CppIManagedObjectHolder* returnValue_imanagedObject = NULL;
		il2cpp_hresult_t hr = (returnValue)->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&returnValue_imanagedObject));
		if (IL2CPP_HR_SUCCEEDED(hr))
		{
			_returnValue_unmarshaled = *static_cast<KeyValuePair_2_t231828568 *>(UnBox(returnValue_imanagedObject->GetManagedObject(), KeyValuePair_2_t231828568_il2cpp_TypeInfo_var));
			returnValue_imanagedObject->Release();
		}
		else
		{
			KeyValuePair_2_t231828568  returnValueStaging;
			Guid_t  returnValueKeyNative = {};
			hr = (returnValue)->IKeyValuePair_2_get_Key_m3324945768(&returnValueKeyNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_key_0(returnValueKeyNative);

			Il2CppIInspectable* returnValueValueNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Value_m52124236(&returnValueValueNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (returnValueValueNative != NULL)
			{
				returnValueStaging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValueValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
			}
			else
			{
				returnValueStaging.set_value_1(NULL);
			}

			if (returnValueValueNative != NULL)
			{
				(returnValueValueNative)->Release();
				returnValueValueNative = NULL;
			}

			_returnValue_unmarshaled = returnValueStaging;
		}
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m2249475151 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2400939337* ____iiterator_1_t2400939337 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2400939337::IID, reinterpret_cast<void**>(&____iiterator_1_t2400939337));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2400939337->IIterator_1_get_HasCurrent_m2249475151(&returnValue);
	____iiterator_1_t2400939337->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m2303149837 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2400939337* ____iiterator_1_t2400939337 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2400939337::IID, reinterpret_cast<void**>(&____iiterator_1_t2400939337));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2400939337->IIterator_1_MoveNext_m2303149837(&returnValue);
	____iiterator_1_t2400939337->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m3197996383 (RuntimeObject* __this, KeyValuePair_2U5BU5D_t3332857673* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m3197996383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2400939337* ____iiterator_1_t2400939337 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2400939337::IID, reinterpret_cast<void**>(&____iiterator_1_t2400939337));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IKeyValuePair_2_t1256548186** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IKeyValuePair_2_t1256548186*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IKeyValuePair_2_t1256548186*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t2400939337->IIterator_1_GetMany_m3197996383(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t2400939337->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			KeyValuePair_2_t231828568  _____items0_marshaled_i__unmarshaled;
			memset(&_____items0_marshaled_i__unmarshaled, 0, sizeof(_____items0_marshaled_i__unmarshaled));
			NullCheck((____items0_marshaled)[i]);

			{
				Il2CppIManagedObjectHolder* ____items0_marshaled_i__imanagedObject = NULL;
				il2cpp_hresult_t hr = ((____items0_marshaled)[i])->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&____items0_marshaled_i__imanagedObject));
				if (IL2CPP_HR_SUCCEEDED(hr))
				{
					_____items0_marshaled_i__unmarshaled = *static_cast<KeyValuePair_2_t231828568 *>(UnBox(____items0_marshaled_i__imanagedObject->GetManagedObject(), KeyValuePair_2_t231828568_il2cpp_TypeInfo_var));
					____items0_marshaled_i__imanagedObject->Release();
				}
				else
				{
					KeyValuePair_2_t231828568  ____items0_marshaled_i_Staging;
					Guid_t  ____items0_marshaled_i_KeyNative = {};
					hr = ((____items0_marshaled)[i])->IKeyValuePair_2_get_Key_m3324945768(&____items0_marshaled_i_KeyNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items0_marshaled_i_Staging.set_key_0(____items0_marshaled_i_KeyNative);

					Il2CppIInspectable* ____items0_marshaled_i_ValueNative = NULL;
					hr = ((____items0_marshaled)[i])->IKeyValuePair_2_get_Value_m52124236(&____items0_marshaled_i_ValueNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					if (____items0_marshaled_i_ValueNative != NULL)
					{
						____items0_marshaled_i_Staging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(____items0_marshaled_i_ValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
					}
					else
					{
						____items0_marshaled_i_Staging.set_value_1(NULL);
					}

					if (____items0_marshaled_i_ValueNative != NULL)
					{
						(____items0_marshaled_i_ValueNative)->Release();
						____items0_marshaled_i_ValueNative = NULL;
					}

					_____items0_marshaled_i__unmarshaled = ____items0_marshaled_i_Staging;
				}
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>>::get_Current()
extern "C"  KeyValuePair_2_t3930634460  IIterator_1_get_Current_m3513860320 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m65888504 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t1804777933* ____iiterator_1_t1804777933 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t1804777933::IID, reinterpret_cast<void**>(&____iiterator_1_t1804777933));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t1804777933->IIterator_1_get_HasCurrent_m65888504(&returnValue);
	____iiterator_1_t1804777933->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m859540255 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t1804777933* ____iiterator_1_t1804777933 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t1804777933::IID, reinterpret_cast<void**>(&____iiterator_1_t1804777933));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t1804777933->IIterator_1_MoveNext_m859540255(&returnValue);
	____iiterator_1_t1804777933->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Type>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m3415100491 (RuntimeObject* __this, KeyValuePair_2U5BU5D_t134831157* ___items0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t71524366  IIterator_1_get_Current_m2947568524 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m2947568524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2240635135* ____iiterator_1_t2240635135 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2240635135::IID, reinterpret_cast<void**>(&____iiterator_1_t2240635135));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IKeyValuePair_2_t1096243984* returnValue = NULL;
	hr = ____iiterator_1_t2240635135->IIterator_1_get_Current_m2947568524(&returnValue);
	____iiterator_1_t2240635135->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	KeyValuePair_2_t71524366  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	NullCheck(returnValue);

	{
		Il2CppIManagedObjectHolder* returnValue_imanagedObject = NULL;
		il2cpp_hresult_t hr = (returnValue)->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&returnValue_imanagedObject));
		if (IL2CPP_HR_SUCCEEDED(hr))
		{
			_returnValue_unmarshaled = *static_cast<KeyValuePair_2_t71524366 *>(UnBox(returnValue_imanagedObject->GetManagedObject(), KeyValuePair_2_t71524366_il2cpp_TypeInfo_var));
			returnValue_imanagedObject->Release();
		}
		else
		{
			KeyValuePair_2_t71524366  returnValueStaging;
			int32_t returnValueKeyNative = 0;
			hr = (returnValue)->IKeyValuePair_2_get_Key_m2186241555(&returnValueKeyNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_key_0(returnValueKeyNative);

			Il2CppIInspectable* returnValueValueNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Value_m210688108(&returnValueValueNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (returnValueValueNative != NULL)
			{
				returnValueStaging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValueValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
			}
			else
			{
				returnValueStaging.set_value_1(NULL);
			}

			if (returnValueValueNative != NULL)
			{
				(returnValueValueNative)->Release();
				returnValueValueNative = NULL;
			}

			_returnValue_unmarshaled = returnValueStaging;
		}
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m3859218363 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2240635135* ____iiterator_1_t2240635135 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2240635135::IID, reinterpret_cast<void**>(&____iiterator_1_t2240635135));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2240635135->IIterator_1_get_HasCurrent_m3859218363(&returnValue);
	____iiterator_1_t2240635135->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m2670346608 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t2240635135* ____iiterator_1_t2240635135 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2240635135::IID, reinterpret_cast<void**>(&____iiterator_1_t2240635135));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t2240635135->IIterator_1_MoveNext_m2670346608(&returnValue);
	____iiterator_1_t2240635135->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m3900158597 (RuntimeObject* __this, KeyValuePair_2U5BU5D_t2652375035* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m3900158597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t2240635135* ____iiterator_1_t2240635135 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t2240635135::IID, reinterpret_cast<void**>(&____iiterator_1_t2240635135));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IKeyValuePair_2_t1096243984** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IKeyValuePair_2_t1096243984*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IKeyValuePair_2_t1096243984*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t2240635135->IIterator_1_GetMany_m3900158597(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t2240635135->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			KeyValuePair_2_t71524366  _____items0_marshaled_i__unmarshaled;
			memset(&_____items0_marshaled_i__unmarshaled, 0, sizeof(_____items0_marshaled_i__unmarshaled));
			NullCheck((____items0_marshaled)[i]);

			{
				Il2CppIManagedObjectHolder* ____items0_marshaled_i__imanagedObject = NULL;
				il2cpp_hresult_t hr = ((____items0_marshaled)[i])->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&____items0_marshaled_i__imanagedObject));
				if (IL2CPP_HR_SUCCEEDED(hr))
				{
					_____items0_marshaled_i__unmarshaled = *static_cast<KeyValuePair_2_t71524366 *>(UnBox(____items0_marshaled_i__imanagedObject->GetManagedObject(), KeyValuePair_2_t71524366_il2cpp_TypeInfo_var));
					____items0_marshaled_i__imanagedObject->Release();
				}
				else
				{
					KeyValuePair_2_t71524366  ____items0_marshaled_i_Staging;
					int32_t ____items0_marshaled_i_KeyNative = 0;
					hr = ((____items0_marshaled)[i])->IKeyValuePair_2_get_Key_m2186241555(&____items0_marshaled_i_KeyNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items0_marshaled_i_Staging.set_key_0(____items0_marshaled_i_KeyNative);

					Il2CppIInspectable* ____items0_marshaled_i_ValueNative = NULL;
					hr = ((____items0_marshaled)[i])->IKeyValuePair_2_get_Value_m210688108(&____items0_marshaled_i_ValueNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					if (____items0_marshaled_i_ValueNative != NULL)
					{
						____items0_marshaled_i_Staging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(____items0_marshaled_i_ValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
					}
					else
					{
						____items0_marshaled_i_Staging.set_value_1(NULL);
					}

					if (____items0_marshaled_i_ValueNative != NULL)
					{
						(____items0_marshaled_i_ValueNative)->Release();
						____items0_marshaled_i_ValueNative = NULL;
					}

					_____items0_marshaled_i__unmarshaled = ____items0_marshaled_i_Staging;
				}
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
// T Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t2245450819  IIterator_1_get_Current_m3547052056 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_get_Current_m3547052056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t119594292* ____iiterator_1_t119594292 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t119594292::IID, reinterpret_cast<void**>(&____iiterator_1_t119594292));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IKeyValuePair_2_t3270170437* returnValue = NULL;
	hr = ____iiterator_1_t119594292->IIterator_1_get_Current_m3547052056(&returnValue);
	____iiterator_1_t119594292->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	KeyValuePair_2_t2245450819  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	NullCheck(returnValue);

	{
		Il2CppIManagedObjectHolder* returnValue_imanagedObject = NULL;
		il2cpp_hresult_t hr = (returnValue)->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&returnValue_imanagedObject));
		if (IL2CPP_HR_SUCCEEDED(hr))
		{
			_returnValue_unmarshaled = *static_cast<KeyValuePair_2_t2245450819 *>(UnBox(returnValue_imanagedObject->GetManagedObject(), KeyValuePair_2_t2245450819_il2cpp_TypeInfo_var));
			returnValue_imanagedObject->Release();
		}
		else
		{
			KeyValuePair_2_t2245450819  returnValueStaging;
			int64_t returnValueKeyNative = 0;
			hr = (returnValue)->IKeyValuePair_2_get_Key_m1937891137(&returnValueKeyNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			returnValueStaging.set_key_0(returnValueKeyNative);

			Il2CppIInspectable* returnValueValueNative = NULL;
			hr = (returnValue)->IKeyValuePair_2_get_Value_m1356929877(&returnValueValueNative);
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (returnValueValueNative != NULL)
			{
				returnValueStaging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValueValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
			}
			else
			{
				returnValueStaging.set_value_1(NULL);
			}

			if (returnValueValueNative != NULL)
			{
				(returnValueValueNative)->Release();
				returnValueValueNative = NULL;
			}

			_returnValue_unmarshaled = returnValueStaging;
		}
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::get_HasCurrent()
extern "C"  bool IIterator_1_get_HasCurrent_m344842528 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t119594292* ____iiterator_1_t119594292 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t119594292::IID, reinterpret_cast<void**>(&____iiterator_1_t119594292));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t119594292->IIterator_1_get_HasCurrent_m344842528(&returnValue);
	____iiterator_1_t119594292->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::MoveNext()
extern "C"  bool IIterator_1_MoveNext_m1308250629 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IIterator_1_t119594292* ____iiterator_1_t119594292 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t119594292::IID, reinterpret_cast<void**>(&____iiterator_1_t119594292));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____iiterator_1_t119594292->IIterator_1_MoveNext_m1308250629(&returnValue);
	____iiterator_1_t119594292->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IIterator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::GetMany(T[])
extern "C"  uint32_t IIterator_1_GetMany_m3058286030 (RuntimeObject* __this, KeyValuePair_2U5BU5D_t3818836818* ___items0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IIterator_1_GetMany_m3058286030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IIterator_1_t119594292* ____iiterator_1_t119594292 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IIterator_1_t119594292::IID, reinterpret_cast<void**>(&____iiterator_1_t119594292));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	IKeyValuePair_2_t3270170437** ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = il2cpp_codegen_marshal_allocate_array<IKeyValuePair_2_t3270170437*>(static_cast<int32_t>(____items0_marshaledArraySize));
		memset(____items0_marshaled, 0, static_cast<int32_t>(____items0_marshaledArraySize) * sizeof(IKeyValuePair_2_t3270170437*));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____iiterator_1_t119594292->IIterator_1_GetMany_m3058286030(____items0_marshaledArraySize, ____items0_marshaled, &returnValue);
	____iiterator_1_t119594292->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' back from native representation
	if (____items0_marshaled != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items0_marshaledArraySize)); i++)
		{
			KeyValuePair_2_t2245450819  _____items0_marshaled_i__unmarshaled;
			memset(&_____items0_marshaled_i__unmarshaled, 0, sizeof(_____items0_marshaled_i__unmarshaled));
			NullCheck((____items0_marshaled)[i]);

			{
				Il2CppIManagedObjectHolder* ____items0_marshaled_i__imanagedObject = NULL;
				il2cpp_hresult_t hr = ((____items0_marshaled)[i])->QueryInterface(Il2CppIManagedObjectHolder::IID, reinterpret_cast<void**>(&____items0_marshaled_i__imanagedObject));
				if (IL2CPP_HR_SUCCEEDED(hr))
				{
					_____items0_marshaled_i__unmarshaled = *static_cast<KeyValuePair_2_t2245450819 *>(UnBox(____items0_marshaled_i__imanagedObject->GetManagedObject(), KeyValuePair_2_t2245450819_il2cpp_TypeInfo_var));
					____items0_marshaled_i__imanagedObject->Release();
				}
				else
				{
					KeyValuePair_2_t2245450819  ____items0_marshaled_i_Staging;
					int64_t ____items0_marshaled_i_KeyNative = 0;
					hr = ((____items0_marshaled)[i])->IKeyValuePair_2_get_Key_m1937891137(&____items0_marshaled_i_KeyNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					____items0_marshaled_i_Staging.set_key_0(____items0_marshaled_i_KeyNative);

					Il2CppIInspectable* ____items0_marshaled_i_ValueNative = NULL;
					hr = ((____items0_marshaled)[i])->IKeyValuePair_2_get_Value_m1356929877(&____items0_marshaled_i_ValueNative);
					il2cpp_codegen_com_raise_exception_if_failed(hr, false);

					if (____items0_marshaled_i_ValueNative != NULL)
					{
						____items0_marshaled_i_Staging.set_value_1(il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(____items0_marshaled_i_ValueNative, Il2CppComObject_il2cpp_TypeInfo_var));
					}
					else
					{
						____items0_marshaled_i_Staging.set_value_1(NULL);
					}

					if (____items0_marshaled_i_ValueNative != NULL)
					{
						(____items0_marshaled_i_ValueNative)->Release();
						____items0_marshaled_i_ValueNative = NULL;
					}

					_____items0_marshaled_i__unmarshaled = ____items0_marshaled_i_Staging;
				}
			}
			(___items0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _____items0_marshaled_i__unmarshaled);
		}
	}

	// Marshaling cleanup of parameter '___items0' native representation
	if (____items0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____items0_marshaled_CleanupLoopCount = (___items0 != NULL) ? (___items0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____items0_marshaled_CleanupLoopCount); i++)
		{
			if ((____items0_marshaled)[i] != NULL)
			{
				((____items0_marshaled)[i])->Release();
				(____items0_marshaled)[i] = NULL;
			}
		}
		il2cpp_codegen_marshal_free(____items0_marshaled);
		____items0_marshaled = NULL;
	}

	return returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
