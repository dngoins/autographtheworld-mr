﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Single[]
struct SingleU5BU5D_t1444911251;
// HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0
struct U3CU3Ec__DisplayClass15_0_t1766518704;
// System.String
struct String_t;
// System.Func`1<Windows.Foundation.IAsyncOperation`1<Windows.Media.SpeechSynthesis.SpeechSynthesisStream>>
struct Func_1_t373274125;
// HoloToolkit.Unity.TextToSpeech
struct TextToSpeech_t421734152;
// HoloToolkit.Unity.Buttons.Button
struct Button_t2162584723;
// HoloToolkit.Unity.UX.Duplicator
struct Duplicator_t4067483021;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Comparison`1<HoloToolkit.Unity.Collections.CollectionNode>
struct Comparison_1_t2108040925;
// HoloToolkit.Unity.TimerScheduler/Callback
struct Callback_t2663646540;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Threading.Tasks.Task`1<System.UInt32>
struct Task_1_t3965602153;
// System.Threading.Tasks.Task`1<Windows.Media.SpeechSynthesis.SpeechSynthesisStream>
struct Task_1_t3727879933;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_t923100567;
// System.Action
struct Action_t1264377477;
// System.Void
struct Void_t1185182177;
// HoloToolkit.Unity.Buttons.CompoundButtonIcon
struct CompoundButtonIcon_t3845426619;
// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>
struct Task_1_t4022128754;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_2
struct U3CU3Ec__DisplayClass15_2_t1384181680;
// Windows.Media.SpeechSynthesis.SpeechSynthesisStream
struct SpeechSynthesisStream_t2322339758;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_1
struct U3CU3Ec__DisplayClass15_1_t4105170864;
// Windows.Storage.Streams.IInputStream
struct IInputStream_t2821136229;
// Windows.Storage.Streams.DataReader
struct DataReader_t2381813649;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// HoloToolkit.Unity.SolverHandler
struct SolverHandler_t1963548039;
// UnityEngine.Font
struct Font_t1956802104;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// HoloToolkit.Unity.InputModule.IPointingSource
struct IPointingSource_t2683498357;
// HoloToolkit.Unity.StabilizationPlaneModifier
struct StabilizationPlaneModifier_t1238920175;
// HoloToolkit.Unity.TimerScheduler
struct TimerScheduler_t541408523;
// UnityEngine.BoxCollider
struct BoxCollider_t1640800422;
// HoloToolkit.Unity.Interpolator
struct Interpolator_t3604653897;
// UnityEngine.Plane[]
struct PlaneU5BU5D_t3656189108;
// HoloToolkit.Unity.UX.LineBase
struct LineBase_t3631573702;
// UnityEngine.Gradient
struct Gradient_t3067099924;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// HoloToolkit.Unity.Buttons.CompoundButtonMesh/MeshButtonDatum[]
struct MeshButtonDatumU5BU5D_t2480441126;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t3210418286;
// System.Action`1<HoloToolkit.Unity.Buttons.ButtonStateEnum>
struct Action_1_t3460105790;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_t1286104214;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// HoloToolkit.Unity.Buttons.ButtonMeshProfile
struct ButtonMeshProfile_t417185472;
// HoloToolkit.Unity.Buttons.ButtonIconProfile
struct ButtonIconProfile_t2026164329;
// Windows.Media.SpeechSynthesis.SpeechSynthesizer
struct SpeechSynthesizer_t2130945875;
// Windows.Media.SpeechSynthesis.VoiceInformation
struct VoiceInformation_t4266404632;
// HoloToolkit.Unity.Buttons.ButtonTextProfile
struct ButtonTextProfile_t3615608975;
// HoloToolkit.Unity.Buttons.ButtonSoundProfile
struct ButtonSoundProfile_t3426452996;
// System.Action`1<HoloToolkit.Unity.Collections.ObjectCollection>
struct Action_1_t2391422249;
// System.Collections.Generic.List`1<HoloToolkit.Unity.Collections.CollectionNode>
struct List_1_t3805184488;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.Collections.Generic.List`1<HoloToolkit.Unity.UX.Distorter>
struct List_1_t961122480;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// HoloToolkit.Unity.Buttons.CompoundButton
struct CompoundButton_t147810562;
// HoloToolkit.Unity.Buttons.CompoundButtonText
struct CompoundButtonText_t2169052765;
// HoloToolkit.Unity.UX.BoundingBox
struct BoundingBox_t212658891;
// HoloToolkit.Unity.Buttons.ButtonProfile
struct ButtonProfile_t1729449105;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Animator
struct Animator_t434523843;
// HoloToolkit.Unity.Buttons.AnimatorControllerAction[]
struct AnimatorControllerActionU5BU5D_t4171275305;
// System.Collections.Generic.List`1<HoloToolkit.Unity.Collections.ObjectCollectionDynamic/CollectionNodeDynamic>
struct List_1_t1466063516;
// HoloToolkit.Unity.Collections.ObjectCollection
struct ObjectCollection_t2218954654;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// HoloToolkit.Unity.Buttons.ObjectButton/ObjectButtonDatum[]
struct ObjectButtonDatumU5BU5D_t1611756286;
// HoloToolkit.Unity.Buttons.MeshButtonDatum[]
struct MeshButtonDatumU5BU5D_t1991104643;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t149664596;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>
struct Dictionary_2_t3625702484;
// HoloToolkit.Unity.Buttons.SpriteButtonDatum[]
struct SpriteButtonDatumU5BU5D_t3985620692;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// HoloToolkit.Unity.Buttons.CompoundButtonMesh/MeshButtonDatum
struct MeshButtonDatum_t3924860415;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3213117958;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t2302988098;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Rendering.CommandBuffer>
struct Dictionary_2_t2419791724;
// UnityEngine.Collider
struct Collider_t1773347010;
// HoloToolkit.Unity.PriorityQueue`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>
struct PriorityQueue_2_t107015337;
// System.Collections.Generic.List`1<HoloToolkit.Unity.TimerScheduler/TimerData>
struct List_1_t1233822936;
// System.Collections.Generic.List`1<HoloToolkit.Unity.TimerScheduler/TimerIdPair>
struct List_1_t2393398647;
// UnityEngine.Light
struct Light_t3756812086;
// FastSimplexNoise
struct FastSimplexNoise_t743524795;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef USABILITYUTILITIES_T4181744403_H
#define USABILITYUTILITIES_T4181744403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UsabilityUtilities
struct  UsabilityUtilities_t4181744403  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USABILITYUTILITIES_T4181744403_H
#ifndef U3CU3EC__DISPLAYCLASS15_2_T1384181680_H
#define U3CU3EC__DISPLAYCLASS15_2_T1384181680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_2
struct  U3CU3Ec__DisplayClass15_2_t1384181680  : public RuntimeObject
{
public:
	// System.Single[] HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_2::unityData
	SingleU5BU5D_t1444911251* ___unityData_0;
	// System.Int32 HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_2::sampleCount
	int32_t ___sampleCount_1;
	// System.Int32 HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_2::frequency
	int32_t ___frequency_2;
	// HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0 HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_2::CS$<>8__locals1
	U3CU3Ec__DisplayClass15_0_t1766518704 * ___CSU24U3CU3E8__locals1_3;

public:
	inline static int32_t get_offset_of_unityData_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_2_t1384181680, ___unityData_0)); }
	inline SingleU5BU5D_t1444911251* get_unityData_0() const { return ___unityData_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_unityData_0() { return &___unityData_0; }
	inline void set_unityData_0(SingleU5BU5D_t1444911251* value)
	{
		___unityData_0 = value;
		Il2CppCodeGenWriteBarrier((&___unityData_0), value);
	}

	inline static int32_t get_offset_of_sampleCount_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_2_t1384181680, ___sampleCount_1)); }
	inline int32_t get_sampleCount_1() const { return ___sampleCount_1; }
	inline int32_t* get_address_of_sampleCount_1() { return &___sampleCount_1; }
	inline void set_sampleCount_1(int32_t value)
	{
		___sampleCount_1 = value;
	}

	inline static int32_t get_offset_of_frequency_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_2_t1384181680, ___frequency_2)); }
	inline int32_t get_frequency_2() const { return ___frequency_2; }
	inline int32_t* get_address_of_frequency_2() { return &___frequency_2; }
	inline void set_frequency_2(int32_t value)
	{
		___frequency_2 = value;
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_2_t1384181680, ___CSU24U3CU3E8__locals1_3)); }
	inline U3CU3Ec__DisplayClass15_0_t1766518704 * get_CSU24U3CU3E8__locals1_3() const { return ___CSU24U3CU3E8__locals1_3; }
	inline U3CU3Ec__DisplayClass15_0_t1766518704 ** get_address_of_CSU24U3CU3E8__locals1_3() { return &___CSU24U3CU3E8__locals1_3; }
	inline void set_CSU24U3CU3E8__locals1_3(U3CU3Ec__DisplayClass15_0_t1766518704 * value)
	{
		___CSU24U3CU3E8__locals1_3 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_2_T1384181680_H
#ifndef U3CU3EC__DISPLAYCLASS15_1_T4105170864_H
#define U3CU3EC__DISPLAYCLASS15_1_T4105170864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_1
struct  U3CU3Ec__DisplayClass15_1_t4105170864  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_1::voiceName
	String_t* ___voiceName_0;

public:
	inline static int32_t get_offset_of_voiceName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_1_t4105170864, ___voiceName_0)); }
	inline String_t* get_voiceName_0() const { return ___voiceName_0; }
	inline String_t** get_address_of_voiceName_0() { return &___voiceName_0; }
	inline void set_voiceName_0(String_t* value)
	{
		___voiceName_0 = value;
		Il2CppCodeGenWriteBarrier((&___voiceName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_1_T4105170864_H
#ifndef U3CU3EC__DISPLAYCLASS17_0_T898832891_H
#define U3CU3EC__DISPLAYCLASS17_0_T898832891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TimerScheduler/<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_t898832891  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.TimerScheduler/<>c__DisplayClass17_0::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t898832891, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_0_T898832891_H
#ifndef U3CU3EC__DISPLAYCLASS15_0_T1766518704_H
#define U3CU3EC__DISPLAYCLASS15_0_T1766518704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_t1766518704  : public RuntimeObject
{
public:
	// System.Func`1<Windows.Foundation.IAsyncOperation`1<Windows.Media.SpeechSynthesis.SpeechSynthesisStream>> HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0::speakFunc
	Func_1_t373274125 * ___speakFunc_0;
	// HoloToolkit.Unity.TextToSpeech HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0::<>4__this
	TextToSpeech_t421734152 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_speakFunc_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_t1766518704, ___speakFunc_0)); }
	inline Func_1_t373274125 * get_speakFunc_0() const { return ___speakFunc_0; }
	inline Func_1_t373274125 ** get_address_of_speakFunc_0() { return &___speakFunc_0; }
	inline void set_speakFunc_0(Func_1_t373274125 * value)
	{
		___speakFunc_0 = value;
		Il2CppCodeGenWriteBarrier((&___speakFunc_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_t1766518704, ___U3CU3E4__this_1)); }
	inline TextToSpeech_t421734152 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline TextToSpeech_t421734152 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(TextToSpeech_t421734152 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_0_T1766518704_H
#ifndef UTILS_T85740694_H
#define UTILS_T85740694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Utils
struct  Utils_t85740694  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T85740694_H
#ifndef U3CDELAYEDRELEASEU3ED__41_T1082381697_H
#define U3CDELAYEDRELEASEU3ED__41_T1082381697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.Button/<DelayedRelease>d__41
struct  U3CDelayedReleaseU3Ed__41_t1082381697  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.Buttons.Button/<DelayedRelease>d__41::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.Buttons.Button/<DelayedRelease>d__41::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single HoloToolkit.Unity.Buttons.Button/<DelayedRelease>d__41::delay
	float ___delay_2;
	// HoloToolkit.Unity.Buttons.Button HoloToolkit.Unity.Buttons.Button/<DelayedRelease>d__41::<>4__this
	Button_t2162584723 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayedReleaseU3Ed__41_t1082381697, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayedReleaseU3Ed__41_t1082381697, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CDelayedReleaseU3Ed__41_t1082381697, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDelayedReleaseU3Ed__41_t1082381697, ___U3CU3E4__this_3)); }
	inline Button_t2162584723 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline Button_t2162584723 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(Button_t2162584723 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDRELEASEU3ED__41_T1082381697_H
#ifndef U3CUPDATETARGETU3ED__30_T3559022823_H
#define U3CUPDATETARGETU3ED__30_T3559022823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Duplicator/<UpdateTarget>d__30
struct  U3CUpdateTargetU3Ed__30_t3559022823  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.UX.Duplicator/<UpdateTarget>d__30::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.UX.Duplicator/<UpdateTarget>d__30::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.UX.Duplicator HoloToolkit.Unity.UX.Duplicator/<UpdateTarget>d__30::<>4__this
	Duplicator_t4067483021 * ___U3CU3E4__this_2;
	// System.Single HoloToolkit.Unity.UX.Duplicator/<UpdateTarget>d__30::<normalizedTime>5__1
	float ___U3CnormalizedTimeU3E5__1_3;
	// System.Single HoloToolkit.Unity.UX.Duplicator/<UpdateTarget>d__30::<idleStartTime>5__2
	float ___U3CidleStartTimeU3E5__2_4;
	// System.Single HoloToolkit.Unity.UX.Duplicator/<UpdateTarget>d__30::<lastTimeChanged>5__3
	float ___U3ClastTimeChangedU3E5__3_5;
	// System.Single HoloToolkit.Unity.UX.Duplicator/<UpdateTarget>d__30::<timedOutTime>5__4
	float ___U3CtimedOutTimeU3E5__4_6;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.Duplicator/<UpdateTarget>d__30::<originalTarget>5__5
	GameObject_t1113636619 * ___U3CoriginalTargetU3E5__5_7;
	// System.Single HoloToolkit.Unity.UX.Duplicator/<UpdateTarget>d__30::<duplicateStartTime>5__6
	float ___U3CduplicateStartTimeU3E5__6_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateTargetU3Ed__30_t3559022823, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateTargetU3Ed__30_t3559022823, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateTargetU3Ed__30_t3559022823, ___U3CU3E4__this_2)); }
	inline Duplicator_t4067483021 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Duplicator_t4067483021 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Duplicator_t4067483021 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CnormalizedTimeU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CUpdateTargetU3Ed__30_t3559022823, ___U3CnormalizedTimeU3E5__1_3)); }
	inline float get_U3CnormalizedTimeU3E5__1_3() const { return ___U3CnormalizedTimeU3E5__1_3; }
	inline float* get_address_of_U3CnormalizedTimeU3E5__1_3() { return &___U3CnormalizedTimeU3E5__1_3; }
	inline void set_U3CnormalizedTimeU3E5__1_3(float value)
	{
		___U3CnormalizedTimeU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CidleStartTimeU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CUpdateTargetU3Ed__30_t3559022823, ___U3CidleStartTimeU3E5__2_4)); }
	inline float get_U3CidleStartTimeU3E5__2_4() const { return ___U3CidleStartTimeU3E5__2_4; }
	inline float* get_address_of_U3CidleStartTimeU3E5__2_4() { return &___U3CidleStartTimeU3E5__2_4; }
	inline void set_U3CidleStartTimeU3E5__2_4(float value)
	{
		___U3CidleStartTimeU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3ClastTimeChangedU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CUpdateTargetU3Ed__30_t3559022823, ___U3ClastTimeChangedU3E5__3_5)); }
	inline float get_U3ClastTimeChangedU3E5__3_5() const { return ___U3ClastTimeChangedU3E5__3_5; }
	inline float* get_address_of_U3ClastTimeChangedU3E5__3_5() { return &___U3ClastTimeChangedU3E5__3_5; }
	inline void set_U3ClastTimeChangedU3E5__3_5(float value)
	{
		___U3ClastTimeChangedU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CtimedOutTimeU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CUpdateTargetU3Ed__30_t3559022823, ___U3CtimedOutTimeU3E5__4_6)); }
	inline float get_U3CtimedOutTimeU3E5__4_6() const { return ___U3CtimedOutTimeU3E5__4_6; }
	inline float* get_address_of_U3CtimedOutTimeU3E5__4_6() { return &___U3CtimedOutTimeU3E5__4_6; }
	inline void set_U3CtimedOutTimeU3E5__4_6(float value)
	{
		___U3CtimedOutTimeU3E5__4_6 = value;
	}

	inline static int32_t get_offset_of_U3CoriginalTargetU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CUpdateTargetU3Ed__30_t3559022823, ___U3CoriginalTargetU3E5__5_7)); }
	inline GameObject_t1113636619 * get_U3CoriginalTargetU3E5__5_7() const { return ___U3CoriginalTargetU3E5__5_7; }
	inline GameObject_t1113636619 ** get_address_of_U3CoriginalTargetU3E5__5_7() { return &___U3CoriginalTargetU3E5__5_7; }
	inline void set_U3CoriginalTargetU3E5__5_7(GameObject_t1113636619 * value)
	{
		___U3CoriginalTargetU3E5__5_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoriginalTargetU3E5__5_7), value);
	}

	inline static int32_t get_offset_of_U3CduplicateStartTimeU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CUpdateTargetU3Ed__30_t3559022823, ___U3CduplicateStartTimeU3E5__6_8)); }
	inline float get_U3CduplicateStartTimeU3E5__6_8() const { return ___U3CduplicateStartTimeU3E5__6_8; }
	inline float* get_address_of_U3CduplicateStartTimeU3E5__6_8() { return &___U3CduplicateStartTimeU3E5__6_8; }
	inline void set_U3CduplicateStartTimeU3E5__6_8(float value)
	{
		___U3CduplicateStartTimeU3E5__6_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATETARGETU3ED__30_T3559022823_H
#ifndef U3CU3EC_T33098766_H
#define U3CU3EC_T33098766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.ObjectCollection/<>c
struct  U3CU3Ec_t33098766  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t33098766_StaticFields
{
public:
	// HoloToolkit.Unity.Collections.ObjectCollection/<>c HoloToolkit.Unity.Collections.ObjectCollection/<>c::<>9
	U3CU3Ec_t33098766 * ___U3CU3E9_0;
	// System.Comparison`1<HoloToolkit.Unity.Collections.CollectionNode> HoloToolkit.Unity.Collections.ObjectCollection/<>c::<>9__22_0
	Comparison_1_t2108040925 * ___U3CU3E9__22_0_1;
	// System.Comparison`1<HoloToolkit.Unity.Collections.CollectionNode> HoloToolkit.Unity.Collections.ObjectCollection/<>c::<>9__22_1
	Comparison_1_t2108040925 * ___U3CU3E9__22_1_2;
	// System.Comparison`1<HoloToolkit.Unity.Collections.CollectionNode> HoloToolkit.Unity.Collections.ObjectCollection/<>c::<>9__22_2
	Comparison_1_t2108040925 * ___U3CU3E9__22_2_3;
	// System.Comparison`1<HoloToolkit.Unity.Collections.CollectionNode> HoloToolkit.Unity.Collections.ObjectCollection/<>c::<>9__22_3
	Comparison_1_t2108040925 * ___U3CU3E9__22_3_4;
	// System.Comparison`1<HoloToolkit.Unity.Collections.CollectionNode> HoloToolkit.Unity.Collections.ObjectCollection/<>c::<>9__28_0
	Comparison_1_t2108040925 * ___U3CU3E9__28_0_5;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t33098766_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t33098766 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t33098766 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t33098766 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t33098766_StaticFields, ___U3CU3E9__22_0_1)); }
	inline Comparison_1_t2108040925 * get_U3CU3E9__22_0_1() const { return ___U3CU3E9__22_0_1; }
	inline Comparison_1_t2108040925 ** get_address_of_U3CU3E9__22_0_1() { return &___U3CU3E9__22_0_1; }
	inline void set_U3CU3E9__22_0_1(Comparison_1_t2108040925 * value)
	{
		___U3CU3E9__22_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__22_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t33098766_StaticFields, ___U3CU3E9__22_1_2)); }
	inline Comparison_1_t2108040925 * get_U3CU3E9__22_1_2() const { return ___U3CU3E9__22_1_2; }
	inline Comparison_1_t2108040925 ** get_address_of_U3CU3E9__22_1_2() { return &___U3CU3E9__22_1_2; }
	inline void set_U3CU3E9__22_1_2(Comparison_1_t2108040925 * value)
	{
		___U3CU3E9__22_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__22_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t33098766_StaticFields, ___U3CU3E9__22_2_3)); }
	inline Comparison_1_t2108040925 * get_U3CU3E9__22_2_3() const { return ___U3CU3E9__22_2_3; }
	inline Comparison_1_t2108040925 ** get_address_of_U3CU3E9__22_2_3() { return &___U3CU3E9__22_2_3; }
	inline void set_U3CU3E9__22_2_3(Comparison_1_t2108040925 * value)
	{
		___U3CU3E9__22_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__22_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t33098766_StaticFields, ___U3CU3E9__22_3_4)); }
	inline Comparison_1_t2108040925 * get_U3CU3E9__22_3_4() const { return ___U3CU3E9__22_3_4; }
	inline Comparison_1_t2108040925 ** get_address_of_U3CU3E9__22_3_4() { return &___U3CU3E9__22_3_4; }
	inline void set_U3CU3E9__22_3_4(Comparison_1_t2108040925 * value)
	{
		___U3CU3E9__22_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__22_3_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__28_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t33098766_StaticFields, ___U3CU3E9__28_0_5)); }
	inline Comparison_1_t2108040925 * get_U3CU3E9__28_0_5() const { return ___U3CU3E9__28_0_5; }
	inline Comparison_1_t2108040925 ** get_address_of_U3CU3E9__28_0_5() { return &___U3CU3E9__28_0_5; }
	inline void set_U3CU3E9__28_0_5(Comparison_1_t2108040925 * value)
	{
		___U3CU3E9__28_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__28_0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T33098766_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_T1765666736_H
#define U3CU3EC__DISPLAYCLASS18_0_T1765666736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_t1765666736  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass18_0::text
	String_t* ___text_0;
	// HoloToolkit.Unity.TextToSpeech HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass18_0::<>4__this
	TextToSpeech_t421734152 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t1765666736, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t1765666736, ___U3CU3E4__this_1)); }
	inline TextToSpeech_t421734152 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline TextToSpeech_t421734152 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(TextToSpeech_t421734152 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_T1765666736_H
#ifndef U3CU3EC__DISPLAYCLASS17_0_T1766387632_H
#define U3CU3EC__DISPLAYCLASS17_0_T1766387632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_t1766387632  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass17_0::ssml
	String_t* ___ssml_0;
	// HoloToolkit.Unity.TextToSpeech HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass17_0::<>4__this
	TextToSpeech_t421734152 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_ssml_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t1766387632, ___ssml_0)); }
	inline String_t* get_ssml_0() const { return ___ssml_0; }
	inline String_t** get_address_of_ssml_0() { return &___ssml_0; }
	inline void set_ssml_0(String_t* value)
	{
		___ssml_0 = value;
		Il2CppCodeGenWriteBarrier((&___ssml_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t1766387632, ___U3CU3E4__this_1)); }
	inline TextToSpeech_t421734152 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline TextToSpeech_t421734152 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(TextToSpeech_t421734152 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_0_T1766387632_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TIMERDATA_T4056715490_H
#define TIMERDATA_T4056715490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TimerScheduler/TimerData
struct  TimerData_t4056715490 
{
public:
	// HoloToolkit.Unity.TimerScheduler/Callback HoloToolkit.Unity.TimerScheduler/TimerData::Callback
	Callback_t2663646540 * ___Callback_0;
	// System.Single HoloToolkit.Unity.TimerScheduler/TimerData::Duration
	float ___Duration_1;
	// System.Boolean HoloToolkit.Unity.TimerScheduler/TimerData::Loop
	bool ___Loop_2;
	// System.Int32 HoloToolkit.Unity.TimerScheduler/TimerData::Id
	int32_t ___Id_3;

public:
	inline static int32_t get_offset_of_Callback_0() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Callback_0)); }
	inline Callback_t2663646540 * get_Callback_0() const { return ___Callback_0; }
	inline Callback_t2663646540 ** get_address_of_Callback_0() { return &___Callback_0; }
	inline void set_Callback_0(Callback_t2663646540 * value)
	{
		___Callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___Callback_0), value);
	}

	inline static int32_t get_offset_of_Duration_1() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Duration_1)); }
	inline float get_Duration_1() const { return ___Duration_1; }
	inline float* get_address_of_Duration_1() { return &___Duration_1; }
	inline void set_Duration_1(float value)
	{
		___Duration_1 = value;
	}

	inline static int32_t get_offset_of_Loop_2() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Loop_2)); }
	inline bool get_Loop_2() const { return ___Loop_2; }
	inline bool* get_address_of_Loop_2() { return &___Loop_2; }
	inline void set_Loop_2(bool value)
	{
		___Loop_2 = value;
	}

	inline static int32_t get_offset_of_Id_3() { return static_cast<int32_t>(offsetof(TimerData_t4056715490, ___Id_3)); }
	inline int32_t get_Id_3() const { return ___Id_3; }
	inline int32_t* get_address_of_Id_3() { return &___Id_3; }
	inline void set_Id_3(int32_t value)
	{
		___Id_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.TimerScheduler/TimerData
struct TimerData_t4056715490_marshaled_pinvoke
{
	Il2CppMethodPointer ___Callback_0;
	float ___Duration_1;
	int32_t ___Loop_2;
	int32_t ___Id_3;
};
// Native definition for COM marshalling of HoloToolkit.Unity.TimerScheduler/TimerData
struct TimerData_t4056715490_marshaled_com
{
	Il2CppMethodPointer ___Callback_0;
	float ___Duration_1;
	int32_t ___Loop_2;
	int32_t ___Id_3;
};
#endif // TIMERDATA_T4056715490_H
#ifndef TIMERIDPAIR_T921323905_H
#define TIMERIDPAIR_T921323905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TimerScheduler/TimerIdPair
struct  TimerIdPair_t921323905 
{
public:
	// System.Int32 HoloToolkit.Unity.TimerScheduler/TimerIdPair::Id
	int32_t ___Id_0;
	// System.Int32 HoloToolkit.Unity.TimerScheduler/TimerIdPair::KeyTime
	int32_t ___KeyTime_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(TimerIdPair_t921323905, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_KeyTime_1() { return static_cast<int32_t>(offsetof(TimerIdPair_t921323905, ___KeyTime_1)); }
	inline int32_t get_KeyTime_1() const { return ___KeyTime_1; }
	inline int32_t* get_address_of_KeyTime_1() { return &___KeyTime_1; }
	inline void set_KeyTime_1(int32_t value)
	{
		___KeyTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERIDPAIR_T921323905_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef TASKAWAITER_1_T1059577113_H
#define TASKAWAITER_1_T1059577113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter`1<System.UInt32>
struct  TaskAwaiter_1_t1059577113 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.TaskAwaiter`1::m_task
	Task_1_t3965602153 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_1_t1059577113, ___m_task_0)); }
	inline Task_1_t3965602153 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t3965602153 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t3965602153 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKAWAITER_1_T1059577113_H
#ifndef TASKAWAITER_1_T821854893_H
#define TASKAWAITER_1_T821854893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter`1<Windows.Media.SpeechSynthesis.SpeechSynthesisStream>
struct  TaskAwaiter_1_t821854893 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.TaskAwaiter`1::m_task
	Task_1_t3727879933 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_1_t821854893, ___m_task_0)); }
	inline Task_1_t3727879933 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t3727879933 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t3727879933 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKAWAITER_1_T821854893_H
#ifndef ASYNCMETHODBUILDERCORE_T2955600131_H
#define ASYNCMETHODBUILDERCORE_T2955600131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t2955600131 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t1264377477 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2955600131, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_stateMachine_0), value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2955600131, ___m_defaultContextAction_1)); }
	inline Action_t1264377477 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t1264377477 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t1264377477 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultContextAction_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2955600131_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2955600131_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
#endif // ASYNCMETHODBUILDERCORE_T2955600131_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TIMER_T3129043708_H
#define TIMER_T3129043708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Timer
struct  Timer_t3129043708 
{
public:
	// System.Int32 HoloToolkit.Unity.Timer::Id
	int32_t ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(Timer_t3129043708, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}
};

struct Timer_t3129043708_StaticFields
{
public:
	// HoloToolkit.Unity.Timer HoloToolkit.Unity.Timer::Invalid
	Timer_t3129043708  ___Invalid_1;

public:
	inline static int32_t get_offset_of_Invalid_1() { return static_cast<int32_t>(offsetof(Timer_t3129043708_StaticFields, ___Invalid_1)); }
	inline Timer_t3129043708  get_Invalid_1() const { return ___Invalid_1; }
	inline Timer_t3129043708 * get_address_of_Invalid_1() { return &___Invalid_1; }
	inline void set_Invalid_1(Timer_t3129043708  value)
	{
		___Invalid_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMER_T3129043708_H
#ifndef STATEENUM_T2014529335_H
#define STATEENUM_T2014529335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Duplicator/StateEnum
struct  StateEnum_t2014529335 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.Duplicator/StateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StateEnum_t2014529335, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEENUM_T2014529335_H
#ifndef ACTIVATEMODEENUM_T1579891057_H
#define ACTIVATEMODEENUM_T1579891057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Duplicator/ActivateModeEnum
struct  ActivateModeEnum_t1579891057 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.Duplicator/ActivateModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActivateModeEnum_t1579891057, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATEMODEENUM_T1579891057_H
#ifndef TRACKEDOBJECTTOREFERENCEENUM_T2659894601_H
#define TRACKEDOBJECTTOREFERENCEENUM_T2659894601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverHandler/TrackedObjectToReferenceEnum
struct  TrackedObjectToReferenceEnum_t2659894601 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverHandler/TrackedObjectToReferenceEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackedObjectToReferenceEnum_t2659894601, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKEDOBJECTTOREFERENCEENUM_T2659894601_H
#ifndef INTERPOLATIONMODEENUM_T1306451942_H
#define INTERPOLATIONMODEENUM_T1306451942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.InterpolationModeEnum
struct  InterpolationModeEnum_t1306451942 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.InterpolationModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InterpolationModeEnum_t1306451942, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATIONMODEENUM_T1306451942_H
#ifndef POINTSET_T452872835_H
#define POINTSET_T452872835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Bezier/PointSet
struct  PointSet_t452872835 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Bezier/PointSet::Point1
	Vector3_t3722313464  ___Point1_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Bezier/PointSet::Point2
	Vector3_t3722313464  ___Point2_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Bezier/PointSet::Point3
	Vector3_t3722313464  ___Point3_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Bezier/PointSet::Point4
	Vector3_t3722313464  ___Point4_3;

public:
	inline static int32_t get_offset_of_Point1_0() { return static_cast<int32_t>(offsetof(PointSet_t452872835, ___Point1_0)); }
	inline Vector3_t3722313464  get_Point1_0() const { return ___Point1_0; }
	inline Vector3_t3722313464 * get_address_of_Point1_0() { return &___Point1_0; }
	inline void set_Point1_0(Vector3_t3722313464  value)
	{
		___Point1_0 = value;
	}

	inline static int32_t get_offset_of_Point2_1() { return static_cast<int32_t>(offsetof(PointSet_t452872835, ___Point2_1)); }
	inline Vector3_t3722313464  get_Point2_1() const { return ___Point2_1; }
	inline Vector3_t3722313464 * get_address_of_Point2_1() { return &___Point2_1; }
	inline void set_Point2_1(Vector3_t3722313464  value)
	{
		___Point2_1 = value;
	}

	inline static int32_t get_offset_of_Point3_2() { return static_cast<int32_t>(offsetof(PointSet_t452872835, ___Point3_2)); }
	inline Vector3_t3722313464  get_Point3_2() const { return ___Point3_2; }
	inline Vector3_t3722313464 * get_address_of_Point3_2() { return &___Point3_2; }
	inline void set_Point3_2(Vector3_t3722313464  value)
	{
		___Point3_2 = value;
	}

	inline static int32_t get_offset_of_Point4_3() { return static_cast<int32_t>(offsetof(PointSet_t452872835, ___Point4_3)); }
	inline Vector3_t3722313464  get_Point4_3() const { return ___Point4_3; }
	inline Vector3_t3722313464 * get_address_of_Point4_3() { return &___Point4_3; }
	inline void set_Point4_3(Vector3_t3722313464  value)
	{
		___Point4_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTSET_T452872835_H
#ifndef U3CUPDATEALPHAU3ED__25_T3458770013_H
#define U3CUPDATEALPHAU3ED__25_T3458770013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonIcon/<UpdateAlpha>d__25
struct  U3CUpdateAlphaU3Ed__25_t3458770013  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.Buttons.CompoundButtonIcon/<UpdateAlpha>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.Buttons.CompoundButtonIcon/<UpdateAlpha>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.Buttons.CompoundButtonIcon HoloToolkit.Unity.Buttons.CompoundButtonIcon/<UpdateAlpha>d__25::<>4__this
	CompoundButtonIcon_t3845426619 * ___U3CU3E4__this_2;
	// System.Single HoloToolkit.Unity.Buttons.CompoundButtonIcon/<UpdateAlpha>d__25::<startTime>5__1
	float ___U3CstartTimeU3E5__1_3;
	// UnityEngine.Color HoloToolkit.Unity.Buttons.CompoundButtonIcon/<UpdateAlpha>d__25::<color>5__2
	Color_t2555686324  ___U3CcolorU3E5__2_4;
	// System.String HoloToolkit.Unity.Buttons.CompoundButtonIcon/<UpdateAlpha>d__25::<alphaColorProperty>5__3
	String_t* ___U3CalphaColorPropertyU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateAlphaU3Ed__25_t3458770013, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateAlphaU3Ed__25_t3458770013, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateAlphaU3Ed__25_t3458770013, ___U3CU3E4__this_2)); }
	inline CompoundButtonIcon_t3845426619 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CompoundButtonIcon_t3845426619 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CompoundButtonIcon_t3845426619 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CUpdateAlphaU3Ed__25_t3458770013, ___U3CstartTimeU3E5__1_3)); }
	inline float get_U3CstartTimeU3E5__1_3() const { return ___U3CstartTimeU3E5__1_3; }
	inline float* get_address_of_U3CstartTimeU3E5__1_3() { return &___U3CstartTimeU3E5__1_3; }
	inline void set_U3CstartTimeU3E5__1_3(float value)
	{
		___U3CstartTimeU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CcolorU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CUpdateAlphaU3Ed__25_t3458770013, ___U3CcolorU3E5__2_4)); }
	inline Color_t2555686324  get_U3CcolorU3E5__2_4() const { return ___U3CcolorU3E5__2_4; }
	inline Color_t2555686324 * get_address_of_U3CcolorU3E5__2_4() { return &___U3CcolorU3E5__2_4; }
	inline void set_U3CcolorU3E5__2_4(Color_t2555686324  value)
	{
		___U3CcolorU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CalphaColorPropertyU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CUpdateAlphaU3Ed__25_t3458770013, ___U3CalphaColorPropertyU3E5__3_5)); }
	inline String_t* get_U3CalphaColorPropertyU3E5__3_5() const { return ___U3CalphaColorPropertyU3E5__3_5; }
	inline String_t** get_address_of_U3CalphaColorPropertyU3E5__3_5() { return &___U3CalphaColorPropertyU3E5__3_5; }
	inline void set_U3CalphaColorPropertyU3E5__3_5(String_t* value)
	{
		___U3CalphaColorPropertyU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CalphaColorPropertyU3E5__3_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEALPHAU3ED__25_T3458770013_H
#ifndef TOGGLEBEHAVIORENUM_T2809601438_H
#define TOGGLEBEHAVIORENUM_T2809601438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ToggleBehaviorEnum
struct  ToggleBehaviorEnum_t2809601438 
{
public:
	// System.Int32 HoloToolkit.Unity.Buttons.ToggleBehaviorEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ToggleBehaviorEnum_t2809601438, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEBEHAVIORENUM_T2809601438_H
#ifndef BUTTONSTATEENUM_T3287638195_H
#define BUTTONSTATEENUM_T3287638195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonStateEnum
struct  ButtonStateEnum_t3287638195 
{
public:
	// System.Int32 HoloToolkit.Unity.Buttons.ButtonStateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonStateEnum_t3287638195, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATEENUM_T3287638195_H
#ifndef FLATTENMODEENUM_T2920636453_H
#define FLATTENMODEENUM_T2920636453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBox/FlattenModeEnum
struct  FlattenModeEnum_t2920636453 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.BoundingBox/FlattenModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FlattenModeEnum_t2920636453, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLATTENMODEENUM_T2920636453_H
#ifndef BOUNDSCALCULATIONMETHODENUM_T2485099182_H
#define BOUNDSCALCULATIONMETHODENUM_T2485099182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBox/BoundsCalculationMethodEnum
struct  BoundsCalculationMethodEnum_t2485099182 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.BoundingBox/BoundsCalculationMethodEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BoundsCalculationMethodEnum_t2485099182, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDSCALCULATIONMETHODENUM_T2485099182_H
#ifndef KEYWORDSOURCEENUM_T377777876_H
#define KEYWORDSOURCEENUM_T377777876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonSpeech/KeywordSourceEnum
struct  KeywordSourceEnum_t377777876 
{
public:
	// System.Int32 HoloToolkit.Unity.Buttons.CompoundButtonSpeech/KeywordSourceEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeywordSourceEnum_t377777876, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYWORDSOURCEENUM_T377777876_H
#ifndef TEXTANCHOR_T2035777396_H
#define TEXTANCHOR_T2035777396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t2035777396 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAnchor_t2035777396, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T2035777396_H
#ifndef FONTSTYLE_T82229486_H
#define FONTSTYLE_T82229486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FontStyle
struct  FontStyle_t82229486 
{
public:
	// System.Int32 UnityEngine.FontStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontStyle_t82229486, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLE_T82229486_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef ANIMATORCONTROLLERPARAMETERTYPE_T3317225440_H
#define ANIMATORCONTROLLERPARAMETERTYPE_T3317225440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorControllerParameterType
struct  AnimatorControllerParameterType_t3317225440 
{
public:
	// System.Int32 UnityEngine.AnimatorControllerParameterType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnimatorControllerParameterType_t3317225440, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORCONTROLLERPARAMETERTYPE_T3317225440_H
#ifndef INTERACTIONSOURCEPRESSINFO_T1268418933_H
#define INTERACTIONSOURCEPRESSINFO_T1268418933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.InteractionSourcePressInfo
struct  InteractionSourcePressInfo_t1268418933 
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.InteractionSourcePressInfo::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InteractionSourcePressInfo_t1268418933, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSOURCEPRESSINFO_T1268418933_H
#ifndef TEXTALIGNMENT_T822270402_H
#define TEXTALIGNMENT_T822270402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAlignment
struct  TextAlignment_t822270402 
{
public:
	// System.Int32 UnityEngine.TextAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignment_t822270402, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENT_T822270402_H
#ifndef ROTATIONTYPEENUM_T2706851285_H
#define ROTATIONTYPEENUM_T2706851285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.RotationTypeEnum
struct  RotationTypeEnum_t2706851285 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.RotationTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotationTypeEnum_t2706851285, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONTYPEENUM_T2706851285_H
#ifndef ASYNCTASKMETHODBUILDER_1_T642595793_H
#define ASYNCTASKMETHODBUILDER_1_T642595793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult>
struct  AsyncTaskMethodBuilder_1_t642595793 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2955600131  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t4022128754 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t642595793, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2955600131  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2955600131 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2955600131  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t642595793, ___m_task_2)); }
	inline Task_1_t4022128754 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t4022128754 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t4022128754 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t642595793_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t4022128754 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t642595793_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t4022128754 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t4022128754 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t4022128754 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T642595793_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DISTORTIONTYPEENUM_T1115486950_H
#define DISTORTIONTYPEENUM_T1115486950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.DistortionTypeEnum
struct  DistortionTypeEnum_t1115486950 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.DistortionTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DistortionTypeEnum_t1115486950, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTIONTYPEENUM_T1115486950_H
#ifndef POINTDISTRIBUTIONTYPEENUM_T3937265912_H
#define POINTDISTRIBUTIONTYPEENUM_T3937265912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.PointDistributionTypeEnum
struct  PointDistributionTypeEnum_t3937265912 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.PointDistributionTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PointDistributionTypeEnum_t3937265912, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTDISTRIBUTIONTYPEENUM_T3937265912_H
#ifndef STEPMODEENUM_T4074296035_H
#define STEPMODEENUM_T4074296035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.StepModeEnum
struct  StepModeEnum_t4074296035 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.StepModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StepModeEnum_t4074296035, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEPMODEENUM_T4074296035_H
#ifndef LAYOUTTYPEENUM_T86934664_H
#define LAYOUTTYPEENUM_T86934664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.LayoutTypeEnum
struct  LayoutTypeEnum_t86934664 
{
public:
	// System.Int32 HoloToolkit.Unity.Collections.LayoutTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LayoutTypeEnum_t86934664, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTTYPEENUM_T86934664_H
#ifndef BEHAVIORENUM_T1724453858_H
#define BEHAVIORENUM_T1724453858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.ObjectCollectionDynamic/BehaviorEnum
struct  BehaviorEnum_t1724453858 
{
public:
	// System.Int32 HoloToolkit.Unity.Collections.ObjectCollectionDynamic/BehaviorEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BehaviorEnum_t1724453858, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIORENUM_T1724453858_H
#ifndef TEXTTOSPEECHVOICE_T443523131_H
#define TEXTTOSPEECHVOICE_T443523131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TextToSpeechVoice
struct  TextToSpeechVoice_t443523131 
{
public:
	// System.Int32 HoloToolkit.Unity.TextToSpeechVoice::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextToSpeechVoice_t443523131, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTTOSPEECHVOICE_T443523131_H
#ifndef ORIENTTYPEENUM_T3115570328_H
#define ORIENTTYPEENUM_T3115570328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.OrientTypeEnum
struct  OrientTypeEnum_t3115570328 
{
public:
	// System.Int32 HoloToolkit.Unity.Collections.OrientTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OrientTypeEnum_t3115570328, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTTYPEENUM_T3115570328_H
#ifndef COLLECTIONNODE_T2333109746_H
#define COLLECTIONNODE_T2333109746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.CollectionNode
struct  CollectionNode_t2333109746  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.Collections.CollectionNode::Name
	String_t* ___Name_0;
	// UnityEngine.Vector2 HoloToolkit.Unity.Collections.CollectionNode::Offset
	Vector2_t2156229523  ___Offset_1;
	// System.Single HoloToolkit.Unity.Collections.CollectionNode::Radius
	float ___Radius_2;
	// UnityEngine.Transform HoloToolkit.Unity.Collections.CollectionNode::transform
	Transform_t3600365921 * ___transform_3;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(CollectionNode_t2333109746, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Offset_1() { return static_cast<int32_t>(offsetof(CollectionNode_t2333109746, ___Offset_1)); }
	inline Vector2_t2156229523  get_Offset_1() const { return ___Offset_1; }
	inline Vector2_t2156229523 * get_address_of_Offset_1() { return &___Offset_1; }
	inline void set_Offset_1(Vector2_t2156229523  value)
	{
		___Offset_1 = value;
	}

	inline static int32_t get_offset_of_Radius_2() { return static_cast<int32_t>(offsetof(CollectionNode_t2333109746, ___Radius_2)); }
	inline float get_Radius_2() const { return ___Radius_2; }
	inline float* get_address_of_Radius_2() { return &___Radius_2; }
	inline void set_Radius_2(float value)
	{
		___Radius_2 = value;
	}

	inline static int32_t get_offset_of_transform_3() { return static_cast<int32_t>(offsetof(CollectionNode_t2333109746, ___transform_3)); }
	inline Transform_t3600365921 * get_transform_3() const { return ___transform_3; }
	inline Transform_t3600365921 ** get_address_of_transform_3() { return &___transform_3; }
	inline void set_transform_3(Transform_t3600365921 * value)
	{
		___transform_3 = value;
		Il2CppCodeGenWriteBarrier((&___transform_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONNODE_T2333109746_H
#ifndef SURFACETYPEENUM_T1788140069_H
#define SURFACETYPEENUM_T1788140069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.SurfaceTypeEnum
struct  SurfaceTypeEnum_t1788140069 
{
public:
	// System.Int32 HoloToolkit.Unity.Collections.SurfaceTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SurfaceTypeEnum_t1788140069, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACETYPEENUM_T1788140069_H
#ifndef SORTTYPEENUM_T439642796_H
#define SORTTYPEENUM_T439642796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.SortTypeEnum
struct  SortTypeEnum_t439642796 
{
public:
	// System.Int32 HoloToolkit.Unity.Collections.SortTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SortTypeEnum_t439642796, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTTYPEENUM_T439642796_H
#ifndef REFERENCEDIRECTIONENUM_T1005305064_H
#define REFERENCEDIRECTIONENUM_T1005305064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverRadialView/ReferenceDirectionEnum
struct  ReferenceDirectionEnum_t1005305064 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverRadialView/ReferenceDirectionEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReferenceDirectionEnum_t1005305064, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCEDIRECTIONENUM_T1005305064_H
#ifndef RAYCASTDIRECTIONENUM_T1407303881_H
#define RAYCASTDIRECTIONENUM_T1407303881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverSurfaceMagnetism/RaycastDirectionEnum
struct  RaycastDirectionEnum_t1407303881 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverSurfaceMagnetism/RaycastDirectionEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RaycastDirectionEnum_t1407303881, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTDIRECTIONENUM_T1407303881_H
#ifndef VECTOR3SMOOTHED_T107253641_H
#define VECTOR3SMOOTHED_T107253641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverHandler/Vector3Smoothed
struct  Vector3Smoothed_t107253641 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverHandler/Vector3Smoothed::<Current>k__BackingField
	Vector3_t3722313464  ___U3CCurrentU3Ek__BackingField_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverHandler/Vector3Smoothed::<Goal>k__BackingField
	Vector3_t3722313464  ___U3CGoalU3Ek__BackingField_1;
	// System.Single HoloToolkit.Unity.SolverHandler/Vector3Smoothed::<SmoothTime>k__BackingField
	float ___U3CSmoothTimeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCurrentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Vector3Smoothed_t107253641, ___U3CCurrentU3Ek__BackingField_0)); }
	inline Vector3_t3722313464  get_U3CCurrentU3Ek__BackingField_0() const { return ___U3CCurrentU3Ek__BackingField_0; }
	inline Vector3_t3722313464 * get_address_of_U3CCurrentU3Ek__BackingField_0() { return &___U3CCurrentU3Ek__BackingField_0; }
	inline void set_U3CCurrentU3Ek__BackingField_0(Vector3_t3722313464  value)
	{
		___U3CCurrentU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CGoalU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Vector3Smoothed_t107253641, ___U3CGoalU3Ek__BackingField_1)); }
	inline Vector3_t3722313464  get_U3CGoalU3Ek__BackingField_1() const { return ___U3CGoalU3Ek__BackingField_1; }
	inline Vector3_t3722313464 * get_address_of_U3CGoalU3Ek__BackingField_1() { return &___U3CGoalU3Ek__BackingField_1; }
	inline void set_U3CGoalU3Ek__BackingField_1(Vector3_t3722313464  value)
	{
		___U3CGoalU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CSmoothTimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vector3Smoothed_t107253641, ___U3CSmoothTimeU3Ek__BackingField_2)); }
	inline float get_U3CSmoothTimeU3Ek__BackingField_2() const { return ___U3CSmoothTimeU3Ek__BackingField_2; }
	inline float* get_address_of_U3CSmoothTimeU3Ek__BackingField_2() { return &___U3CSmoothTimeU3Ek__BackingField_2; }
	inline void set_U3CSmoothTimeU3Ek__BackingField_2(float value)
	{
		___U3CSmoothTimeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3SMOOTHED_T107253641_H
#ifndef QUATERNIONSMOOTHED_T1703835205_H
#define QUATERNIONSMOOTHED_T1703835205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverHandler/QuaternionSmoothed
struct  QuaternionSmoothed_t1703835205 
{
public:
	// UnityEngine.Quaternion HoloToolkit.Unity.SolverHandler/QuaternionSmoothed::<Current>k__BackingField
	Quaternion_t2301928331  ___U3CCurrentU3Ek__BackingField_0;
	// UnityEngine.Quaternion HoloToolkit.Unity.SolverHandler/QuaternionSmoothed::<Goal>k__BackingField
	Quaternion_t2301928331  ___U3CGoalU3Ek__BackingField_1;
	// System.Single HoloToolkit.Unity.SolverHandler/QuaternionSmoothed::<SmoothTime>k__BackingField
	float ___U3CSmoothTimeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCurrentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(QuaternionSmoothed_t1703835205, ___U3CCurrentU3Ek__BackingField_0)); }
	inline Quaternion_t2301928331  get_U3CCurrentU3Ek__BackingField_0() const { return ___U3CCurrentU3Ek__BackingField_0; }
	inline Quaternion_t2301928331 * get_address_of_U3CCurrentU3Ek__BackingField_0() { return &___U3CCurrentU3Ek__BackingField_0; }
	inline void set_U3CCurrentU3Ek__BackingField_0(Quaternion_t2301928331  value)
	{
		___U3CCurrentU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CGoalU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(QuaternionSmoothed_t1703835205, ___U3CGoalU3Ek__BackingField_1)); }
	inline Quaternion_t2301928331  get_U3CGoalU3Ek__BackingField_1() const { return ___U3CGoalU3Ek__BackingField_1; }
	inline Quaternion_t2301928331 * get_address_of_U3CGoalU3Ek__BackingField_1() { return &___U3CGoalU3Ek__BackingField_1; }
	inline void set_U3CGoalU3Ek__BackingField_1(Quaternion_t2301928331  value)
	{
		___U3CGoalU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CSmoothTimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(QuaternionSmoothed_t1703835205, ___U3CSmoothTimeU3Ek__BackingField_2)); }
	inline float get_U3CSmoothTimeU3Ek__BackingField_2() const { return ___U3CSmoothTimeU3Ek__BackingField_2; }
	inline float* get_address_of_U3CSmoothTimeU3Ek__BackingField_2() { return &___U3CSmoothTimeU3Ek__BackingField_2; }
	inline void set_U3CSmoothTimeU3Ek__BackingField_2(float value)
	{
		___U3CSmoothTimeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONSMOOTHED_T1703835205_H
#ifndef ORIENTMODEENUM_T4291832292_H
#define ORIENTMODEENUM_T4291832292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverSurfaceMagnetism/OrientModeEnum
struct  OrientModeEnum_t4291832292 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverSurfaceMagnetism/OrientModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OrientModeEnum_t4291832292, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTMODEENUM_T4291832292_H
#ifndef RAYCASTMODEENUM_T2024436208_H
#define RAYCASTMODEENUM_T2024436208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverSurfaceMagnetism/RaycastModeEnum
struct  RaycastModeEnum_t2024436208 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverSurfaceMagnetism/RaycastModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RaycastModeEnum_t2024436208, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTMODEENUM_T2024436208_H
#ifndef OBJECTBUTTONDATUM_T759934215_H
#define OBJECTBUTTONDATUM_T759934215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ObjectButton/ObjectButtonDatum
struct  ObjectButtonDatum_t759934215  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.Buttons.ObjectButton/ObjectButtonDatum::Name
	String_t* ___Name_0;
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.ObjectButton/ObjectButtonDatum::ActiveState
	int32_t ___ActiveState_1;
	// UnityEngine.GameObject HoloToolkit.Unity.Buttons.ObjectButton/ObjectButtonDatum::Prefab
	GameObject_t1113636619 * ___Prefab_2;
	// UnityEngine.GameObject HoloToolkit.Unity.Buttons.ObjectButton/ObjectButtonDatum::Instance
	GameObject_t1113636619 * ___Instance_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ObjectButton/ObjectButtonDatum::Offset
	Vector3_t3722313464  ___Offset_4;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ObjectButton/ObjectButtonDatum::Scale
	Vector3_t3722313464  ___Scale_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ObjectButtonDatum_t759934215, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_ActiveState_1() { return static_cast<int32_t>(offsetof(ObjectButtonDatum_t759934215, ___ActiveState_1)); }
	inline int32_t get_ActiveState_1() const { return ___ActiveState_1; }
	inline int32_t* get_address_of_ActiveState_1() { return &___ActiveState_1; }
	inline void set_ActiveState_1(int32_t value)
	{
		___ActiveState_1 = value;
	}

	inline static int32_t get_offset_of_Prefab_2() { return static_cast<int32_t>(offsetof(ObjectButtonDatum_t759934215, ___Prefab_2)); }
	inline GameObject_t1113636619 * get_Prefab_2() const { return ___Prefab_2; }
	inline GameObject_t1113636619 ** get_address_of_Prefab_2() { return &___Prefab_2; }
	inline void set_Prefab_2(GameObject_t1113636619 * value)
	{
		___Prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_2), value);
	}

	inline static int32_t get_offset_of_Instance_3() { return static_cast<int32_t>(offsetof(ObjectButtonDatum_t759934215, ___Instance_3)); }
	inline GameObject_t1113636619 * get_Instance_3() const { return ___Instance_3; }
	inline GameObject_t1113636619 ** get_address_of_Instance_3() { return &___Instance_3; }
	inline void set_Instance_3(GameObject_t1113636619 * value)
	{
		___Instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_3), value);
	}

	inline static int32_t get_offset_of_Offset_4() { return static_cast<int32_t>(offsetof(ObjectButtonDatum_t759934215, ___Offset_4)); }
	inline Vector3_t3722313464  get_Offset_4() const { return ___Offset_4; }
	inline Vector3_t3722313464 * get_address_of_Offset_4() { return &___Offset_4; }
	inline void set_Offset_4(Vector3_t3722313464  value)
	{
		___Offset_4 = value;
	}

	inline static int32_t get_offset_of_Scale_5() { return static_cast<int32_t>(offsetof(ObjectButtonDatum_t759934215, ___Scale_5)); }
	inline Vector3_t3722313464  get_Scale_5() const { return ___Scale_5; }
	inline Vector3_t3722313464 * get_address_of_Scale_5() { return &___Scale_5; }
	inline void set_Scale_5(Vector3_t3722313464  value)
	{
		___Scale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTBUTTONDATUM_T759934215_H
#ifndef ANIMATORCONTROLLERACTION_T150210808_H
#define ANIMATORCONTROLLERACTION_T150210808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.AnimatorControllerAction
struct  AnimatorControllerAction_t150210808 
{
public:
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.AnimatorControllerAction::ButtonState
	int32_t ___ButtonState_0;
	// System.String HoloToolkit.Unity.Buttons.AnimatorControllerAction::ParamName
	String_t* ___ParamName_1;
	// UnityEngine.AnimatorControllerParameterType HoloToolkit.Unity.Buttons.AnimatorControllerAction::ParamType
	int32_t ___ParamType_2;
	// System.Boolean HoloToolkit.Unity.Buttons.AnimatorControllerAction::BoolValue
	bool ___BoolValue_3;
	// System.Int32 HoloToolkit.Unity.Buttons.AnimatorControllerAction::IntValue
	int32_t ___IntValue_4;
	// System.Single HoloToolkit.Unity.Buttons.AnimatorControllerAction::FloatValue
	float ___FloatValue_5;
	// System.Boolean HoloToolkit.Unity.Buttons.AnimatorControllerAction::InvalidParam
	bool ___InvalidParam_6;

public:
	inline static int32_t get_offset_of_ButtonState_0() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t150210808, ___ButtonState_0)); }
	inline int32_t get_ButtonState_0() const { return ___ButtonState_0; }
	inline int32_t* get_address_of_ButtonState_0() { return &___ButtonState_0; }
	inline void set_ButtonState_0(int32_t value)
	{
		___ButtonState_0 = value;
	}

	inline static int32_t get_offset_of_ParamName_1() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t150210808, ___ParamName_1)); }
	inline String_t* get_ParamName_1() const { return ___ParamName_1; }
	inline String_t** get_address_of_ParamName_1() { return &___ParamName_1; }
	inline void set_ParamName_1(String_t* value)
	{
		___ParamName_1 = value;
		Il2CppCodeGenWriteBarrier((&___ParamName_1), value);
	}

	inline static int32_t get_offset_of_ParamType_2() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t150210808, ___ParamType_2)); }
	inline int32_t get_ParamType_2() const { return ___ParamType_2; }
	inline int32_t* get_address_of_ParamType_2() { return &___ParamType_2; }
	inline void set_ParamType_2(int32_t value)
	{
		___ParamType_2 = value;
	}

	inline static int32_t get_offset_of_BoolValue_3() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t150210808, ___BoolValue_3)); }
	inline bool get_BoolValue_3() const { return ___BoolValue_3; }
	inline bool* get_address_of_BoolValue_3() { return &___BoolValue_3; }
	inline void set_BoolValue_3(bool value)
	{
		___BoolValue_3 = value;
	}

	inline static int32_t get_offset_of_IntValue_4() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t150210808, ___IntValue_4)); }
	inline int32_t get_IntValue_4() const { return ___IntValue_4; }
	inline int32_t* get_address_of_IntValue_4() { return &___IntValue_4; }
	inline void set_IntValue_4(int32_t value)
	{
		___IntValue_4 = value;
	}

	inline static int32_t get_offset_of_FloatValue_5() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t150210808, ___FloatValue_5)); }
	inline float get_FloatValue_5() const { return ___FloatValue_5; }
	inline float* get_address_of_FloatValue_5() { return &___FloatValue_5; }
	inline void set_FloatValue_5(float value)
	{
		___FloatValue_5 = value;
	}

	inline static int32_t get_offset_of_InvalidParam_6() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t150210808, ___InvalidParam_6)); }
	inline bool get_InvalidParam_6() const { return ___InvalidParam_6; }
	inline bool* get_address_of_InvalidParam_6() { return &___InvalidParam_6; }
	inline void set_InvalidParam_6(bool value)
	{
		___InvalidParam_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.Buttons.AnimatorControllerAction
struct AnimatorControllerAction_t150210808_marshaled_pinvoke
{
	int32_t ___ButtonState_0;
	char* ___ParamName_1;
	int32_t ___ParamType_2;
	int32_t ___BoolValue_3;
	int32_t ___IntValue_4;
	float ___FloatValue_5;
	int32_t ___InvalidParam_6;
};
// Native definition for COM marshalling of HoloToolkit.Unity.Buttons.AnimatorControllerAction
struct AnimatorControllerAction_t150210808_marshaled_com
{
	int32_t ___ButtonState_0;
	Il2CppChar* ___ParamName_1;
	int32_t ___ParamType_2;
	int32_t ___BoolValue_3;
	int32_t ___IntValue_4;
	float ___FloatValue_5;
	int32_t ___InvalidParam_6;
};
#endif // ANIMATORCONTROLLERACTION_T150210808_H
#ifndef COLLECTIONNODEDYNAMIC_T4288956070_H
#define COLLECTIONNODEDYNAMIC_T4288956070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.ObjectCollectionDynamic/CollectionNodeDynamic
struct  CollectionNodeDynamic_t4288956070  : public CollectionNode_t2333109746
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.Collections.ObjectCollectionDynamic/CollectionNodeDynamic::localPositionOnStartup
	Vector3_t3722313464  ___localPositionOnStartup_4;
	// UnityEngine.Vector3 HoloToolkit.Unity.Collections.ObjectCollectionDynamic/CollectionNodeDynamic::localEulerAnglesOnStartup
	Vector3_t3722313464  ___localEulerAnglesOnStartup_5;

public:
	inline static int32_t get_offset_of_localPositionOnStartup_4() { return static_cast<int32_t>(offsetof(CollectionNodeDynamic_t4288956070, ___localPositionOnStartup_4)); }
	inline Vector3_t3722313464  get_localPositionOnStartup_4() const { return ___localPositionOnStartup_4; }
	inline Vector3_t3722313464 * get_address_of_localPositionOnStartup_4() { return &___localPositionOnStartup_4; }
	inline void set_localPositionOnStartup_4(Vector3_t3722313464  value)
	{
		___localPositionOnStartup_4 = value;
	}

	inline static int32_t get_offset_of_localEulerAnglesOnStartup_5() { return static_cast<int32_t>(offsetof(CollectionNodeDynamic_t4288956070, ___localEulerAnglesOnStartup_5)); }
	inline Vector3_t3722313464  get_localEulerAnglesOnStartup_5() const { return ___localEulerAnglesOnStartup_5; }
	inline Vector3_t3722313464 * get_address_of_localEulerAnglesOnStartup_5() { return &___localEulerAnglesOnStartup_5; }
	inline void set_localEulerAnglesOnStartup_5(Vector3_t3722313464  value)
	{
		___localEulerAnglesOnStartup_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONNODEDYNAMIC_T4288956070_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MULTICASTDELEGATE_T157516450_H
#define MULTICASTDELEGATE_T157516450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t157516450  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t157516450, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T157516450_H
#ifndef SPRITEBUTTONDATUM_T2537539945_H
#define SPRITEBUTTONDATUM_T2537539945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.SpriteButtonDatum
struct  SpriteButtonDatum_t2537539945  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.Buttons.SpriteButtonDatum::Name
	String_t* ___Name_0;
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.SpriteButtonDatum::ActiveState
	int32_t ___ActiveState_1;
	// UnityEngine.Sprite HoloToolkit.Unity.Buttons.SpriteButtonDatum::ButtonSprite
	Sprite_t280657092 * ___ButtonSprite_2;
	// UnityEngine.Color HoloToolkit.Unity.Buttons.SpriteButtonDatum::SpriteColor
	Color_t2555686324  ___SpriteColor_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.SpriteButtonDatum::Scale
	Vector3_t3722313464  ___Scale_4;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(SpriteButtonDatum_t2537539945, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_ActiveState_1() { return static_cast<int32_t>(offsetof(SpriteButtonDatum_t2537539945, ___ActiveState_1)); }
	inline int32_t get_ActiveState_1() const { return ___ActiveState_1; }
	inline int32_t* get_address_of_ActiveState_1() { return &___ActiveState_1; }
	inline void set_ActiveState_1(int32_t value)
	{
		___ActiveState_1 = value;
	}

	inline static int32_t get_offset_of_ButtonSprite_2() { return static_cast<int32_t>(offsetof(SpriteButtonDatum_t2537539945, ___ButtonSprite_2)); }
	inline Sprite_t280657092 * get_ButtonSprite_2() const { return ___ButtonSprite_2; }
	inline Sprite_t280657092 ** get_address_of_ButtonSprite_2() { return &___ButtonSprite_2; }
	inline void set_ButtonSprite_2(Sprite_t280657092 * value)
	{
		___ButtonSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonSprite_2), value);
	}

	inline static int32_t get_offset_of_SpriteColor_3() { return static_cast<int32_t>(offsetof(SpriteButtonDatum_t2537539945, ___SpriteColor_3)); }
	inline Color_t2555686324  get_SpriteColor_3() const { return ___SpriteColor_3; }
	inline Color_t2555686324 * get_address_of_SpriteColor_3() { return &___SpriteColor_3; }
	inline void set_SpriteColor_3(Color_t2555686324  value)
	{
		___SpriteColor_3 = value;
	}

	inline static int32_t get_offset_of_Scale_4() { return static_cast<int32_t>(offsetof(SpriteButtonDatum_t2537539945, ___Scale_4)); }
	inline Vector3_t3722313464  get_Scale_4() const { return ___Scale_4; }
	inline Vector3_t3722313464 * get_address_of_Scale_4() { return &___Scale_4; }
	inline void set_Scale_4(Vector3_t3722313464  value)
	{
		___Scale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEBUTTONDATUM_T2537539945_H
#ifndef ASYNCTASKMETHODBUILDER_T3536885450_H
#define ASYNCTASKMETHODBUILDER_T3536885450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct  AsyncTaskMethodBuilder_t3536885450 
{
public:
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::m_builder
	AsyncTaskMethodBuilder_1_t642595793  ___m_builder_1;

public:
	inline static int32_t get_offset_of_m_builder_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t3536885450, ___m_builder_1)); }
	inline AsyncTaskMethodBuilder_1_t642595793  get_m_builder_1() const { return ___m_builder_1; }
	inline AsyncTaskMethodBuilder_1_t642595793 * get_address_of_m_builder_1() { return &___m_builder_1; }
	inline void set_m_builder_1(AsyncTaskMethodBuilder_1_t642595793  value)
	{
		___m_builder_1 = value;
	}
};

struct AsyncTaskMethodBuilder_t3536885450_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::s_cachedCompleted
	Task_1_t4022128754 * ___s_cachedCompleted_0;

public:
	inline static int32_t get_offset_of_s_cachedCompleted_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t3536885450_StaticFields, ___s_cachedCompleted_0)); }
	inline Task_1_t4022128754 * get_s_cachedCompleted_0() const { return ___s_cachedCompleted_0; }
	inline Task_1_t4022128754 ** get_address_of_s_cachedCompleted_0() { return &___s_cachedCompleted_0; }
	inline void set_s_cachedCompleted_0(Task_1_t4022128754 * value)
	{
		___s_cachedCompleted_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_cachedCompleted_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t3536885450_marshaled_pinvoke
{
	AsyncTaskMethodBuilder_1_t642595793  ___m_builder_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t3536885450_marshaled_com
{
	AsyncTaskMethodBuilder_1_t642595793  ___m_builder_1;
};
#endif // ASYNCTASKMETHODBUILDER_T3536885450_H
#ifndef MESHBUTTONDATUM_T3924860415_H
#define MESHBUTTONDATUM_T3924860415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonMesh/MeshButtonDatum
struct  MeshButtonDatum_t3924860415  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.Buttons.CompoundButtonMesh/MeshButtonDatum::Name
	String_t* ___Name_0;
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.CompoundButtonMesh/MeshButtonDatum::ActiveState
	int32_t ___ActiveState_1;
	// UnityEngine.Color HoloToolkit.Unity.Buttons.CompoundButtonMesh/MeshButtonDatum::StateColor
	Color_t2555686324  ___StateColor_2;
	// System.Single HoloToolkit.Unity.Buttons.CompoundButtonMesh/MeshButtonDatum::StateValue
	float ___StateValue_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.CompoundButtonMesh/MeshButtonDatum::Offset
	Vector3_t3722313464  ___Offset_4;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.CompoundButtonMesh/MeshButtonDatum::Scale
	Vector3_t3722313464  ___Scale_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t3924860415, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_ActiveState_1() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t3924860415, ___ActiveState_1)); }
	inline int32_t get_ActiveState_1() const { return ___ActiveState_1; }
	inline int32_t* get_address_of_ActiveState_1() { return &___ActiveState_1; }
	inline void set_ActiveState_1(int32_t value)
	{
		___ActiveState_1 = value;
	}

	inline static int32_t get_offset_of_StateColor_2() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t3924860415, ___StateColor_2)); }
	inline Color_t2555686324  get_StateColor_2() const { return ___StateColor_2; }
	inline Color_t2555686324 * get_address_of_StateColor_2() { return &___StateColor_2; }
	inline void set_StateColor_2(Color_t2555686324  value)
	{
		___StateColor_2 = value;
	}

	inline static int32_t get_offset_of_StateValue_3() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t3924860415, ___StateValue_3)); }
	inline float get_StateValue_3() const { return ___StateValue_3; }
	inline float* get_address_of_StateValue_3() { return &___StateValue_3; }
	inline void set_StateValue_3(float value)
	{
		___StateValue_3 = value;
	}

	inline static int32_t get_offset_of_Offset_4() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t3924860415, ___Offset_4)); }
	inline Vector3_t3722313464  get_Offset_4() const { return ___Offset_4; }
	inline Vector3_t3722313464 * get_address_of_Offset_4() { return &___Offset_4; }
	inline void set_Offset_4(Vector3_t3722313464  value)
	{
		___Offset_4 = value;
	}

	inline static int32_t get_offset_of_Scale_5() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t3924860415, ___Scale_5)); }
	inline Vector3_t3722313464  get_Scale_5() const { return ___Scale_5; }
	inline Vector3_t3722313464 * get_address_of_Scale_5() { return &___Scale_5; }
	inline void set_Scale_5(Vector3_t3722313464  value)
	{
		___Scale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHBUTTONDATUM_T3924860415_H
#ifndef MESHBUTTONDATUM_T1736360998_H
#define MESHBUTTONDATUM_T1736360998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.MeshButtonDatum
struct  MeshButtonDatum_t1736360998  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.Buttons.MeshButtonDatum::Name
	String_t* ___Name_0;
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.MeshButtonDatum::ActiveState
	int32_t ___ActiveState_1;
	// UnityEngine.Color HoloToolkit.Unity.Buttons.MeshButtonDatum::StateColor
	Color_t2555686324  ___StateColor_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.MeshButtonDatum::Offset
	Vector3_t3722313464  ___Offset_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.MeshButtonDatum::Scale
	Vector3_t3722313464  ___Scale_4;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t1736360998, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_ActiveState_1() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t1736360998, ___ActiveState_1)); }
	inline int32_t get_ActiveState_1() const { return ___ActiveState_1; }
	inline int32_t* get_address_of_ActiveState_1() { return &___ActiveState_1; }
	inline void set_ActiveState_1(int32_t value)
	{
		___ActiveState_1 = value;
	}

	inline static int32_t get_offset_of_StateColor_2() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t1736360998, ___StateColor_2)); }
	inline Color_t2555686324  get_StateColor_2() const { return ___StateColor_2; }
	inline Color_t2555686324 * get_address_of_StateColor_2() { return &___StateColor_2; }
	inline void set_StateColor_2(Color_t2555686324  value)
	{
		___StateColor_2 = value;
	}

	inline static int32_t get_offset_of_Offset_3() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t1736360998, ___Offset_3)); }
	inline Vector3_t3722313464  get_Offset_3() const { return ___Offset_3; }
	inline Vector3_t3722313464 * get_address_of_Offset_3() { return &___Offset_3; }
	inline void set_Offset_3(Vector3_t3722313464  value)
	{
		___Offset_3 = value;
	}

	inline static int32_t get_offset_of_Scale_4() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t1736360998, ___Scale_4)); }
	inline Vector3_t3722313464  get_Scale_4() const { return ___Scale_4; }
	inline Vector3_t3722313464 * get_address_of_Scale_4() { return &___Scale_4; }
	inline void set_Scale_4(Vector3_t3722313464  value)
	{
		___Scale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHBUTTONDATUM_T1736360998_H
#ifndef CALLBACK_T2663646540_H
#define CALLBACK_T2663646540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TimerScheduler/Callback
struct  Callback_t2663646540  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACK_T2663646540_H
#ifndef PROFILEBASE_T3960377668_H
#define PROFILEBASE_T3960377668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ProfileBase
struct  ProfileBase_t3960377668  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEBASE_T3960377668_H
#ifndef U3CU3CPLAYSPEECHU3EB__0U3ED_T1000717489_H
#define U3CU3CPLAYSPEECHU3EB__0U3ED_T1000717489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0/<<PlaySpeech>b__0>d
struct  U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0/<<PlaySpeech>b__0>d::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0/<<PlaySpeech>b__0>d::<>t__builder
	AsyncTaskMethodBuilder_t3536885450  ___U3CU3Et__builder_1;
	// HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0 HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0/<<PlaySpeech>b__0>d::<>4__this
	U3CU3Ec__DisplayClass15_0_t1766518704 * ___U3CU3E4__this_2;
	// HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_2 HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0/<<PlaySpeech>b__0>d::<>8__1
	U3CU3Ec__DisplayClass15_2_t1384181680 * ___U3CU3E8__1_3;
	// Windows.Media.SpeechSynthesis.SpeechSynthesisStream HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0/<<PlaySpeech>b__0>d::<speechStream>5__2
	SpeechSynthesisStream_t2322339758 * ___U3CspeechStreamU3E5__2_4;
	// System.UInt64 HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0/<<PlaySpeech>b__0>d::<size>5__3
	uint64_t ___U3CsizeU3E5__3_5;
	// System.Byte[] HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0/<<PlaySpeech>b__0>d::<buffer>5__4
	ByteU5BU5D_t4116647657* ___U3CbufferU3E5__4_6;
	// HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_1 HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0/<<PlaySpeech>b__0>d::<>8__5
	U3CU3Ec__DisplayClass15_1_t4105170864 * ___U3CU3E8__5_7;
	// Windows.Media.SpeechSynthesis.SpeechSynthesisStream HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0/<<PlaySpeech>b__0>d::<>s__6
	SpeechSynthesisStream_t2322339758 * ___U3CU3Es__6_8;
	// Windows.Storage.Streams.IInputStream HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0/<<PlaySpeech>b__0>d::<inputStream>5__7
	RuntimeObject* ___U3CinputStreamU3E5__7_9;
	// Windows.Storage.Streams.DataReader HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0/<<PlaySpeech>b__0>d::<dataReader>5__8
	DataReader_t2381813649 * ___U3CdataReaderU3E5__8_10;
	// System.Runtime.CompilerServices.TaskAwaiter`1<Windows.Media.SpeechSynthesis.SpeechSynthesisStream> HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0/<<PlaySpeech>b__0>d::<>u__1
	TaskAwaiter_1_t821854893  ___U3CU3Eu__1_11;
	// System.Runtime.CompilerServices.TaskAwaiter`1<System.UInt32> HoloToolkit.Unity.TextToSpeech/<>c__DisplayClass15_0/<<PlaySpeech>b__0>d::<>u__2
	TaskAwaiter_1_t1059577113  ___U3CU3Eu__2_12;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t3536885450  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t3536885450 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t3536885450  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489, ___U3CU3E4__this_2)); }
	inline U3CU3Ec__DisplayClass15_0_t1766518704 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline U3CU3Ec__DisplayClass15_0_t1766518704 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(U3CU3Ec__DisplayClass15_0_t1766518704 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_3() { return static_cast<int32_t>(offsetof(U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489, ___U3CU3E8__1_3)); }
	inline U3CU3Ec__DisplayClass15_2_t1384181680 * get_U3CU3E8__1_3() const { return ___U3CU3E8__1_3; }
	inline U3CU3Ec__DisplayClass15_2_t1384181680 ** get_address_of_U3CU3E8__1_3() { return &___U3CU3E8__1_3; }
	inline void set_U3CU3E8__1_3(U3CU3Ec__DisplayClass15_2_t1384181680 * value)
	{
		___U3CU3E8__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_3), value);
	}

	inline static int32_t get_offset_of_U3CspeechStreamU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489, ___U3CspeechStreamU3E5__2_4)); }
	inline SpeechSynthesisStream_t2322339758 * get_U3CspeechStreamU3E5__2_4() const { return ___U3CspeechStreamU3E5__2_4; }
	inline SpeechSynthesisStream_t2322339758 ** get_address_of_U3CspeechStreamU3E5__2_4() { return &___U3CspeechStreamU3E5__2_4; }
	inline void set_U3CspeechStreamU3E5__2_4(SpeechSynthesisStream_t2322339758 * value)
	{
		___U3CspeechStreamU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CspeechStreamU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CsizeU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489, ___U3CsizeU3E5__3_5)); }
	inline uint64_t get_U3CsizeU3E5__3_5() const { return ___U3CsizeU3E5__3_5; }
	inline uint64_t* get_address_of_U3CsizeU3E5__3_5() { return &___U3CsizeU3E5__3_5; }
	inline void set_U3CsizeU3E5__3_5(uint64_t value)
	{
		___U3CsizeU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CbufferU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489, ___U3CbufferU3E5__4_6)); }
	inline ByteU5BU5D_t4116647657* get_U3CbufferU3E5__4_6() const { return ___U3CbufferU3E5__4_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CbufferU3E5__4_6() { return &___U3CbufferU3E5__4_6; }
	inline void set_U3CbufferU3E5__4_6(ByteU5BU5D_t4116647657* value)
	{
		___U3CbufferU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbufferU3E5__4_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__5_7() { return static_cast<int32_t>(offsetof(U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489, ___U3CU3E8__5_7)); }
	inline U3CU3Ec__DisplayClass15_1_t4105170864 * get_U3CU3E8__5_7() const { return ___U3CU3E8__5_7; }
	inline U3CU3Ec__DisplayClass15_1_t4105170864 ** get_address_of_U3CU3E8__5_7() { return &___U3CU3E8__5_7; }
	inline void set_U3CU3E8__5_7(U3CU3Ec__DisplayClass15_1_t4105170864 * value)
	{
		___U3CU3E8__5_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__5_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__6_8() { return static_cast<int32_t>(offsetof(U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489, ___U3CU3Es__6_8)); }
	inline SpeechSynthesisStream_t2322339758 * get_U3CU3Es__6_8() const { return ___U3CU3Es__6_8; }
	inline SpeechSynthesisStream_t2322339758 ** get_address_of_U3CU3Es__6_8() { return &___U3CU3Es__6_8; }
	inline void set_U3CU3Es__6_8(SpeechSynthesisStream_t2322339758 * value)
	{
		___U3CU3Es__6_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__6_8), value);
	}

	inline static int32_t get_offset_of_U3CinputStreamU3E5__7_9() { return static_cast<int32_t>(offsetof(U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489, ___U3CinputStreamU3E5__7_9)); }
	inline RuntimeObject* get_U3CinputStreamU3E5__7_9() const { return ___U3CinputStreamU3E5__7_9; }
	inline RuntimeObject** get_address_of_U3CinputStreamU3E5__7_9() { return &___U3CinputStreamU3E5__7_9; }
	inline void set_U3CinputStreamU3E5__7_9(RuntimeObject* value)
	{
		___U3CinputStreamU3E5__7_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinputStreamU3E5__7_9), value);
	}

	inline static int32_t get_offset_of_U3CdataReaderU3E5__8_10() { return static_cast<int32_t>(offsetof(U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489, ___U3CdataReaderU3E5__8_10)); }
	inline DataReader_t2381813649 * get_U3CdataReaderU3E5__8_10() const { return ___U3CdataReaderU3E5__8_10; }
	inline DataReader_t2381813649 ** get_address_of_U3CdataReaderU3E5__8_10() { return &___U3CdataReaderU3E5__8_10; }
	inline void set_U3CdataReaderU3E5__8_10(DataReader_t2381813649 * value)
	{
		___U3CdataReaderU3E5__8_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataReaderU3E5__8_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_11() { return static_cast<int32_t>(offsetof(U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489, ___U3CU3Eu__1_11)); }
	inline TaskAwaiter_1_t821854893  get_U3CU3Eu__1_11() const { return ___U3CU3Eu__1_11; }
	inline TaskAwaiter_1_t821854893 * get_address_of_U3CU3Eu__1_11() { return &___U3CU3Eu__1_11; }
	inline void set_U3CU3Eu__1_11(TaskAwaiter_1_t821854893  value)
	{
		___U3CU3Eu__1_11 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__2_12() { return static_cast<int32_t>(offsetof(U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489, ___U3CU3Eu__2_12)); }
	inline TaskAwaiter_1_t1059577113  get_U3CU3Eu__2_12() const { return ___U3CU3Eu__2_12; }
	inline TaskAwaiter_1_t1059577113 * get_address_of_U3CU3Eu__2_12() { return &___U3CU3Eu__2_12; }
	inline void set_U3CU3Eu__2_12(TaskAwaiter_1_t1059577113  value)
	{
		___U3CU3Eu__2_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3CPLAYSPEECHU3EB__0U3ED_T1000717489_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef BUTTONPROFILE_T1729449105_H
#define BUTTONPROFILE_T1729449105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonProfile
struct  ButtonProfile_t1729449105  : public ProfileBase_t3960377668
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONPROFILE_T1729449105_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef BUTTONSOUNDS_T3557837633_H
#define BUTTONSOUNDS_T3557837633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonSounds
struct  ButtonSounds_t3557837633  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSounds::ButtonCanceled
	AudioClip_t3680889665 * ___ButtonCanceled_3;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSounds::ButtonHeld
	AudioClip_t3680889665 * ___ButtonHeld_4;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSounds::ButtonPressed
	AudioClip_t3680889665 * ___ButtonPressed_5;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSounds::ButtonReleased
	AudioClip_t3680889665 * ___ButtonReleased_6;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSounds::ButtonObservation
	AudioClip_t3680889665 * ___ButtonObservation_7;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSounds::ButtonObservationTargeted
	AudioClip_t3680889665 * ___ButtonObservationTargeted_8;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSounds::ButtonTargeted
	AudioClip_t3680889665 * ___ButtonTargeted_9;
	// UnityEngine.AudioSource HoloToolkit.Unity.Buttons.ButtonSounds::audioSource
	AudioSource_t3935305588 * ___audioSource_10;

public:
	inline static int32_t get_offset_of_ButtonCanceled_3() { return static_cast<int32_t>(offsetof(ButtonSounds_t3557837633, ___ButtonCanceled_3)); }
	inline AudioClip_t3680889665 * get_ButtonCanceled_3() const { return ___ButtonCanceled_3; }
	inline AudioClip_t3680889665 ** get_address_of_ButtonCanceled_3() { return &___ButtonCanceled_3; }
	inline void set_ButtonCanceled_3(AudioClip_t3680889665 * value)
	{
		___ButtonCanceled_3 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonCanceled_3), value);
	}

	inline static int32_t get_offset_of_ButtonHeld_4() { return static_cast<int32_t>(offsetof(ButtonSounds_t3557837633, ___ButtonHeld_4)); }
	inline AudioClip_t3680889665 * get_ButtonHeld_4() const { return ___ButtonHeld_4; }
	inline AudioClip_t3680889665 ** get_address_of_ButtonHeld_4() { return &___ButtonHeld_4; }
	inline void set_ButtonHeld_4(AudioClip_t3680889665 * value)
	{
		___ButtonHeld_4 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonHeld_4), value);
	}

	inline static int32_t get_offset_of_ButtonPressed_5() { return static_cast<int32_t>(offsetof(ButtonSounds_t3557837633, ___ButtonPressed_5)); }
	inline AudioClip_t3680889665 * get_ButtonPressed_5() const { return ___ButtonPressed_5; }
	inline AudioClip_t3680889665 ** get_address_of_ButtonPressed_5() { return &___ButtonPressed_5; }
	inline void set_ButtonPressed_5(AudioClip_t3680889665 * value)
	{
		___ButtonPressed_5 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonPressed_5), value);
	}

	inline static int32_t get_offset_of_ButtonReleased_6() { return static_cast<int32_t>(offsetof(ButtonSounds_t3557837633, ___ButtonReleased_6)); }
	inline AudioClip_t3680889665 * get_ButtonReleased_6() const { return ___ButtonReleased_6; }
	inline AudioClip_t3680889665 ** get_address_of_ButtonReleased_6() { return &___ButtonReleased_6; }
	inline void set_ButtonReleased_6(AudioClip_t3680889665 * value)
	{
		___ButtonReleased_6 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonReleased_6), value);
	}

	inline static int32_t get_offset_of_ButtonObservation_7() { return static_cast<int32_t>(offsetof(ButtonSounds_t3557837633, ___ButtonObservation_7)); }
	inline AudioClip_t3680889665 * get_ButtonObservation_7() const { return ___ButtonObservation_7; }
	inline AudioClip_t3680889665 ** get_address_of_ButtonObservation_7() { return &___ButtonObservation_7; }
	inline void set_ButtonObservation_7(AudioClip_t3680889665 * value)
	{
		___ButtonObservation_7 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonObservation_7), value);
	}

	inline static int32_t get_offset_of_ButtonObservationTargeted_8() { return static_cast<int32_t>(offsetof(ButtonSounds_t3557837633, ___ButtonObservationTargeted_8)); }
	inline AudioClip_t3680889665 * get_ButtonObservationTargeted_8() const { return ___ButtonObservationTargeted_8; }
	inline AudioClip_t3680889665 ** get_address_of_ButtonObservationTargeted_8() { return &___ButtonObservationTargeted_8; }
	inline void set_ButtonObservationTargeted_8(AudioClip_t3680889665 * value)
	{
		___ButtonObservationTargeted_8 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonObservationTargeted_8), value);
	}

	inline static int32_t get_offset_of_ButtonTargeted_9() { return static_cast<int32_t>(offsetof(ButtonSounds_t3557837633, ___ButtonTargeted_9)); }
	inline AudioClip_t3680889665 * get_ButtonTargeted_9() const { return ___ButtonTargeted_9; }
	inline AudioClip_t3680889665 ** get_address_of_ButtonTargeted_9() { return &___ButtonTargeted_9; }
	inline void set_ButtonTargeted_9(AudioClip_t3680889665 * value)
	{
		___ButtonTargeted_9 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonTargeted_9), value);
	}

	inline static int32_t get_offset_of_audioSource_10() { return static_cast<int32_t>(offsetof(ButtonSounds_t3557837633, ___audioSource_10)); }
	inline AudioSource_t3935305588 * get_audioSource_10() const { return ___audioSource_10; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_10() { return &___audioSource_10; }
	inline void set_audioSource_10(AudioSource_t3935305588 * value)
	{
		___audioSource_10 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_10), value);
	}
};

struct ButtonSounds_t3557837633_StaticFields
{
public:
	// System.String HoloToolkit.Unity.Buttons.ButtonSounds::lastClipName
	String_t* ___lastClipName_11;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSounds::lastClipTime
	float ___lastClipTime_12;

public:
	inline static int32_t get_offset_of_lastClipName_11() { return static_cast<int32_t>(offsetof(ButtonSounds_t3557837633_StaticFields, ___lastClipName_11)); }
	inline String_t* get_lastClipName_11() const { return ___lastClipName_11; }
	inline String_t** get_address_of_lastClipName_11() { return &___lastClipName_11; }
	inline void set_lastClipName_11(String_t* value)
	{
		___lastClipName_11 = value;
		Il2CppCodeGenWriteBarrier((&___lastClipName_11), value);
	}

	inline static int32_t get_offset_of_lastClipTime_12() { return static_cast<int32_t>(offsetof(ButtonSounds_t3557837633_StaticFields, ___lastClipTime_12)); }
	inline float get_lastClipTime_12() const { return ___lastClipTime_12; }
	inline float* get_address_of_lastClipTime_12() { return &___lastClipTime_12; }
	inline void set_lastClipTime_12(float value)
	{
		___lastClipTime_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSOUNDS_T3557837633_H
#ifndef USABILITYSCALER_T2506837711_H
#define USABILITYSCALER_T2506837711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UsabilityScaler
struct  UsabilityScaler_t2506837711  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UsabilityScaler::baseScale
	Vector3_t3722313464  ___baseScale_2;

public:
	inline static int32_t get_offset_of_baseScale_2() { return static_cast<int32_t>(offsetof(UsabilityScaler_t2506837711, ___baseScale_2)); }
	inline Vector3_t3722313464  get_baseScale_2() const { return ___baseScale_2; }
	inline Vector3_t3722313464 * get_address_of_baseScale_2() { return &___baseScale_2; }
	inline void set_baseScale_2(Vector3_t3722313464  value)
	{
		___baseScale_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USABILITYSCALER_T2506837711_H
#ifndef SOLVER_T4167981057_H
#define SOLVER_T4167981057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Solver
struct  Solver_t4167981057  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean HoloToolkit.Unity.Solver::UpdateLinkedTransform
	bool ___UpdateLinkedTransform_2;
	// System.Single HoloToolkit.Unity.Solver::MoveLerpTime
	float ___MoveLerpTime_3;
	// System.Single HoloToolkit.Unity.Solver::RotateLerpTime
	float ___RotateLerpTime_4;
	// System.Single HoloToolkit.Unity.Solver::ScaleLerpTime
	float ___ScaleLerpTime_5;
	// System.Boolean HoloToolkit.Unity.Solver::MaintainScale
	bool ___MaintainScale_6;
	// UnityEngine.Vector3 HoloToolkit.Unity.Solver::GoalPosition
	Vector3_t3722313464  ___GoalPosition_7;
	// UnityEngine.Quaternion HoloToolkit.Unity.Solver::GoalRotation
	Quaternion_t2301928331  ___GoalRotation_8;
	// UnityEngine.Vector3 HoloToolkit.Unity.Solver::GoalScale
	Vector3_t3722313464  ___GoalScale_9;
	// System.Boolean HoloToolkit.Unity.Solver::Smoothing
	bool ___Smoothing_10;
	// System.Single HoloToolkit.Unity.Solver::Lifetime
	float ___Lifetime_11;
	// HoloToolkit.Unity.SolverHandler HoloToolkit.Unity.Solver::solverHandler
	SolverHandler_t1963548039 * ___solverHandler_12;
	// System.Single HoloToolkit.Unity.Solver::lifetime
	float ___lifetime_13;

public:
	inline static int32_t get_offset_of_UpdateLinkedTransform_2() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___UpdateLinkedTransform_2)); }
	inline bool get_UpdateLinkedTransform_2() const { return ___UpdateLinkedTransform_2; }
	inline bool* get_address_of_UpdateLinkedTransform_2() { return &___UpdateLinkedTransform_2; }
	inline void set_UpdateLinkedTransform_2(bool value)
	{
		___UpdateLinkedTransform_2 = value;
	}

	inline static int32_t get_offset_of_MoveLerpTime_3() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___MoveLerpTime_3)); }
	inline float get_MoveLerpTime_3() const { return ___MoveLerpTime_3; }
	inline float* get_address_of_MoveLerpTime_3() { return &___MoveLerpTime_3; }
	inline void set_MoveLerpTime_3(float value)
	{
		___MoveLerpTime_3 = value;
	}

	inline static int32_t get_offset_of_RotateLerpTime_4() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___RotateLerpTime_4)); }
	inline float get_RotateLerpTime_4() const { return ___RotateLerpTime_4; }
	inline float* get_address_of_RotateLerpTime_4() { return &___RotateLerpTime_4; }
	inline void set_RotateLerpTime_4(float value)
	{
		___RotateLerpTime_4 = value;
	}

	inline static int32_t get_offset_of_ScaleLerpTime_5() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___ScaleLerpTime_5)); }
	inline float get_ScaleLerpTime_5() const { return ___ScaleLerpTime_5; }
	inline float* get_address_of_ScaleLerpTime_5() { return &___ScaleLerpTime_5; }
	inline void set_ScaleLerpTime_5(float value)
	{
		___ScaleLerpTime_5 = value;
	}

	inline static int32_t get_offset_of_MaintainScale_6() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___MaintainScale_6)); }
	inline bool get_MaintainScale_6() const { return ___MaintainScale_6; }
	inline bool* get_address_of_MaintainScale_6() { return &___MaintainScale_6; }
	inline void set_MaintainScale_6(bool value)
	{
		___MaintainScale_6 = value;
	}

	inline static int32_t get_offset_of_GoalPosition_7() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___GoalPosition_7)); }
	inline Vector3_t3722313464  get_GoalPosition_7() const { return ___GoalPosition_7; }
	inline Vector3_t3722313464 * get_address_of_GoalPosition_7() { return &___GoalPosition_7; }
	inline void set_GoalPosition_7(Vector3_t3722313464  value)
	{
		___GoalPosition_7 = value;
	}

	inline static int32_t get_offset_of_GoalRotation_8() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___GoalRotation_8)); }
	inline Quaternion_t2301928331  get_GoalRotation_8() const { return ___GoalRotation_8; }
	inline Quaternion_t2301928331 * get_address_of_GoalRotation_8() { return &___GoalRotation_8; }
	inline void set_GoalRotation_8(Quaternion_t2301928331  value)
	{
		___GoalRotation_8 = value;
	}

	inline static int32_t get_offset_of_GoalScale_9() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___GoalScale_9)); }
	inline Vector3_t3722313464  get_GoalScale_9() const { return ___GoalScale_9; }
	inline Vector3_t3722313464 * get_address_of_GoalScale_9() { return &___GoalScale_9; }
	inline void set_GoalScale_9(Vector3_t3722313464  value)
	{
		___GoalScale_9 = value;
	}

	inline static int32_t get_offset_of_Smoothing_10() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___Smoothing_10)); }
	inline bool get_Smoothing_10() const { return ___Smoothing_10; }
	inline bool* get_address_of_Smoothing_10() { return &___Smoothing_10; }
	inline void set_Smoothing_10(bool value)
	{
		___Smoothing_10 = value;
	}

	inline static int32_t get_offset_of_Lifetime_11() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___Lifetime_11)); }
	inline float get_Lifetime_11() const { return ___Lifetime_11; }
	inline float* get_address_of_Lifetime_11() { return &___Lifetime_11; }
	inline void set_Lifetime_11(float value)
	{
		___Lifetime_11 = value;
	}

	inline static int32_t get_offset_of_solverHandler_12() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___solverHandler_12)); }
	inline SolverHandler_t1963548039 * get_solverHandler_12() const { return ___solverHandler_12; }
	inline SolverHandler_t1963548039 ** get_address_of_solverHandler_12() { return &___solverHandler_12; }
	inline void set_solverHandler_12(SolverHandler_t1963548039 * value)
	{
		___solverHandler_12 = value;
		Il2CppCodeGenWriteBarrier((&___solverHandler_12), value);
	}

	inline static int32_t get_offset_of_lifetime_13() { return static_cast<int32_t>(offsetof(Solver_t4167981057, ___lifetime_13)); }
	inline float get_lifetime_13() const { return ___lifetime_13; }
	inline float* get_address_of_lifetime_13() { return &___lifetime_13; }
	inline void set_lifetime_13(float value)
	{
		___lifetime_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVER_T4167981057_H
#ifndef TOGGLEDEBUGWINDOW_T2216991934_H
#define TOGGLEDEBUGWINDOW_T2216991934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ToggleDebugWindow
struct  ToggleDebugWindow_t2216991934  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean HoloToolkit.Unity.ToggleDebugWindow::debugEnabled
	bool ___debugEnabled_2;
	// UnityEngine.GameObject HoloToolkit.Unity.ToggleDebugWindow::DebugWindow
	GameObject_t1113636619 * ___DebugWindow_3;

public:
	inline static int32_t get_offset_of_debugEnabled_2() { return static_cast<int32_t>(offsetof(ToggleDebugWindow_t2216991934, ___debugEnabled_2)); }
	inline bool get_debugEnabled_2() const { return ___debugEnabled_2; }
	inline bool* get_address_of_debugEnabled_2() { return &___debugEnabled_2; }
	inline void set_debugEnabled_2(bool value)
	{
		___debugEnabled_2 = value;
	}

	inline static int32_t get_offset_of_DebugWindow_3() { return static_cast<int32_t>(offsetof(ToggleDebugWindow_t2216991934, ___DebugWindow_3)); }
	inline GameObject_t1113636619 * get_DebugWindow_3() const { return ___DebugWindow_3; }
	inline GameObject_t1113636619 ** get_address_of_DebugWindow_3() { return &___DebugWindow_3; }
	inline void set_DebugWindow_3(GameObject_t1113636619 * value)
	{
		___DebugWindow_3 = value;
		Il2CppCodeGenWriteBarrier((&___DebugWindow_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEDEBUGWINDOW_T2216991934_H
#ifndef BUTTONTEXTPROFILE_T3615608975_H
#define BUTTONTEXTPROFILE_T3615608975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonTextProfile
struct  ButtonTextProfile_t3615608975  : public ButtonProfile_t1729449105
{
public:
	// UnityEngine.TextAlignment HoloToolkit.Unity.Buttons.ButtonTextProfile::Alignment
	int32_t ___Alignment_2;
	// UnityEngine.TextAnchor HoloToolkit.Unity.Buttons.ButtonTextProfile::Anchor
	int32_t ___Anchor_3;
	// UnityEngine.FontStyle HoloToolkit.Unity.Buttons.ButtonTextProfile::Style
	int32_t ___Style_4;
	// System.Int32 HoloToolkit.Unity.Buttons.ButtonTextProfile::Size
	int32_t ___Size_5;
	// UnityEngine.Color HoloToolkit.Unity.Buttons.ButtonTextProfile::Color
	Color_t2555686324  ___Color_6;
	// UnityEngine.Font HoloToolkit.Unity.Buttons.ButtonTextProfile::Font
	Font_t1956802104 * ___Font_7;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorLowerCenterOffset
	Vector3_t3722313464  ___AnchorLowerCenterOffset_8;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorLowerLeftOffset
	Vector3_t3722313464  ___AnchorLowerLeftOffset_9;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorLowerRightOffset
	Vector3_t3722313464  ___AnchorLowerRightOffset_10;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorMiddleCenterOffset
	Vector3_t3722313464  ___AnchorMiddleCenterOffset_11;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorMiddleLeftOffset
	Vector3_t3722313464  ___AnchorMiddleLeftOffset_12;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorMiddleRightOffset
	Vector3_t3722313464  ___AnchorMiddleRightOffset_13;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorUpperCenterOffset
	Vector3_t3722313464  ___AnchorUpperCenterOffset_14;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorUpperLeftOffset
	Vector3_t3722313464  ___AnchorUpperLeftOffset_15;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorUpperRightOffset
	Vector3_t3722313464  ___AnchorUpperRightOffset_16;

public:
	inline static int32_t get_offset_of_Alignment_2() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t3615608975, ___Alignment_2)); }
	inline int32_t get_Alignment_2() const { return ___Alignment_2; }
	inline int32_t* get_address_of_Alignment_2() { return &___Alignment_2; }
	inline void set_Alignment_2(int32_t value)
	{
		___Alignment_2 = value;
	}

	inline static int32_t get_offset_of_Anchor_3() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t3615608975, ___Anchor_3)); }
	inline int32_t get_Anchor_3() const { return ___Anchor_3; }
	inline int32_t* get_address_of_Anchor_3() { return &___Anchor_3; }
	inline void set_Anchor_3(int32_t value)
	{
		___Anchor_3 = value;
	}

	inline static int32_t get_offset_of_Style_4() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t3615608975, ___Style_4)); }
	inline int32_t get_Style_4() const { return ___Style_4; }
	inline int32_t* get_address_of_Style_4() { return &___Style_4; }
	inline void set_Style_4(int32_t value)
	{
		___Style_4 = value;
	}

	inline static int32_t get_offset_of_Size_5() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t3615608975, ___Size_5)); }
	inline int32_t get_Size_5() const { return ___Size_5; }
	inline int32_t* get_address_of_Size_5() { return &___Size_5; }
	inline void set_Size_5(int32_t value)
	{
		___Size_5 = value;
	}

	inline static int32_t get_offset_of_Color_6() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t3615608975, ___Color_6)); }
	inline Color_t2555686324  get_Color_6() const { return ___Color_6; }
	inline Color_t2555686324 * get_address_of_Color_6() { return &___Color_6; }
	inline void set_Color_6(Color_t2555686324  value)
	{
		___Color_6 = value;
	}

	inline static int32_t get_offset_of_Font_7() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t3615608975, ___Font_7)); }
	inline Font_t1956802104 * get_Font_7() const { return ___Font_7; }
	inline Font_t1956802104 ** get_address_of_Font_7() { return &___Font_7; }
	inline void set_Font_7(Font_t1956802104 * value)
	{
		___Font_7 = value;
		Il2CppCodeGenWriteBarrier((&___Font_7), value);
	}

	inline static int32_t get_offset_of_AnchorLowerCenterOffset_8() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t3615608975, ___AnchorLowerCenterOffset_8)); }
	inline Vector3_t3722313464  get_AnchorLowerCenterOffset_8() const { return ___AnchorLowerCenterOffset_8; }
	inline Vector3_t3722313464 * get_address_of_AnchorLowerCenterOffset_8() { return &___AnchorLowerCenterOffset_8; }
	inline void set_AnchorLowerCenterOffset_8(Vector3_t3722313464  value)
	{
		___AnchorLowerCenterOffset_8 = value;
	}

	inline static int32_t get_offset_of_AnchorLowerLeftOffset_9() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t3615608975, ___AnchorLowerLeftOffset_9)); }
	inline Vector3_t3722313464  get_AnchorLowerLeftOffset_9() const { return ___AnchorLowerLeftOffset_9; }
	inline Vector3_t3722313464 * get_address_of_AnchorLowerLeftOffset_9() { return &___AnchorLowerLeftOffset_9; }
	inline void set_AnchorLowerLeftOffset_9(Vector3_t3722313464  value)
	{
		___AnchorLowerLeftOffset_9 = value;
	}

	inline static int32_t get_offset_of_AnchorLowerRightOffset_10() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t3615608975, ___AnchorLowerRightOffset_10)); }
	inline Vector3_t3722313464  get_AnchorLowerRightOffset_10() const { return ___AnchorLowerRightOffset_10; }
	inline Vector3_t3722313464 * get_address_of_AnchorLowerRightOffset_10() { return &___AnchorLowerRightOffset_10; }
	inline void set_AnchorLowerRightOffset_10(Vector3_t3722313464  value)
	{
		___AnchorLowerRightOffset_10 = value;
	}

	inline static int32_t get_offset_of_AnchorMiddleCenterOffset_11() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t3615608975, ___AnchorMiddleCenterOffset_11)); }
	inline Vector3_t3722313464  get_AnchorMiddleCenterOffset_11() const { return ___AnchorMiddleCenterOffset_11; }
	inline Vector3_t3722313464 * get_address_of_AnchorMiddleCenterOffset_11() { return &___AnchorMiddleCenterOffset_11; }
	inline void set_AnchorMiddleCenterOffset_11(Vector3_t3722313464  value)
	{
		___AnchorMiddleCenterOffset_11 = value;
	}

	inline static int32_t get_offset_of_AnchorMiddleLeftOffset_12() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t3615608975, ___AnchorMiddleLeftOffset_12)); }
	inline Vector3_t3722313464  get_AnchorMiddleLeftOffset_12() const { return ___AnchorMiddleLeftOffset_12; }
	inline Vector3_t3722313464 * get_address_of_AnchorMiddleLeftOffset_12() { return &___AnchorMiddleLeftOffset_12; }
	inline void set_AnchorMiddleLeftOffset_12(Vector3_t3722313464  value)
	{
		___AnchorMiddleLeftOffset_12 = value;
	}

	inline static int32_t get_offset_of_AnchorMiddleRightOffset_13() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t3615608975, ___AnchorMiddleRightOffset_13)); }
	inline Vector3_t3722313464  get_AnchorMiddleRightOffset_13() const { return ___AnchorMiddleRightOffset_13; }
	inline Vector3_t3722313464 * get_address_of_AnchorMiddleRightOffset_13() { return &___AnchorMiddleRightOffset_13; }
	inline void set_AnchorMiddleRightOffset_13(Vector3_t3722313464  value)
	{
		___AnchorMiddleRightOffset_13 = value;
	}

	inline static int32_t get_offset_of_AnchorUpperCenterOffset_14() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t3615608975, ___AnchorUpperCenterOffset_14)); }
	inline Vector3_t3722313464  get_AnchorUpperCenterOffset_14() const { return ___AnchorUpperCenterOffset_14; }
	inline Vector3_t3722313464 * get_address_of_AnchorUpperCenterOffset_14() { return &___AnchorUpperCenterOffset_14; }
	inline void set_AnchorUpperCenterOffset_14(Vector3_t3722313464  value)
	{
		___AnchorUpperCenterOffset_14 = value;
	}

	inline static int32_t get_offset_of_AnchorUpperLeftOffset_15() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t3615608975, ___AnchorUpperLeftOffset_15)); }
	inline Vector3_t3722313464  get_AnchorUpperLeftOffset_15() const { return ___AnchorUpperLeftOffset_15; }
	inline Vector3_t3722313464 * get_address_of_AnchorUpperLeftOffset_15() { return &___AnchorUpperLeftOffset_15; }
	inline void set_AnchorUpperLeftOffset_15(Vector3_t3722313464  value)
	{
		___AnchorUpperLeftOffset_15 = value;
	}

	inline static int32_t get_offset_of_AnchorUpperRightOffset_16() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t3615608975, ___AnchorUpperRightOffset_16)); }
	inline Vector3_t3722313464  get_AnchorUpperRightOffset_16() const { return ___AnchorUpperRightOffset_16; }
	inline Vector3_t3722313464 * get_address_of_AnchorUpperRightOffset_16() { return &___AnchorUpperRightOffset_16; }
	inline void set_AnchorUpperRightOffset_16(Vector3_t3722313464  value)
	{
		___AnchorUpperRightOffset_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONTEXTPROFILE_T3615608975_H
#ifndef INTERACTIONRECEIVER_T3572119366_H
#define INTERACTIONRECEIVER_T3572119366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Receivers.InteractionReceiver
struct  InteractionReceiver_t3572119366  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HoloToolkit.Unity.Receivers.InteractionReceiver::interactables
	List_1_t2585711361 * ___interactables_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HoloToolkit.Unity.Receivers.InteractionReceiver::Targets
	List_1_t2585711361 * ___Targets_3;
	// System.Boolean HoloToolkit.Unity.Receivers.InteractionReceiver::lockFocus
	bool ___lockFocus_4;
	// HoloToolkit.Unity.InputModule.IPointingSource HoloToolkit.Unity.Receivers.InteractionReceiver::_selectingFocuser
	RuntimeObject* ____selectingFocuser_5;

public:
	inline static int32_t get_offset_of_interactables_2() { return static_cast<int32_t>(offsetof(InteractionReceiver_t3572119366, ___interactables_2)); }
	inline List_1_t2585711361 * get_interactables_2() const { return ___interactables_2; }
	inline List_1_t2585711361 ** get_address_of_interactables_2() { return &___interactables_2; }
	inline void set_interactables_2(List_1_t2585711361 * value)
	{
		___interactables_2 = value;
		Il2CppCodeGenWriteBarrier((&___interactables_2), value);
	}

	inline static int32_t get_offset_of_Targets_3() { return static_cast<int32_t>(offsetof(InteractionReceiver_t3572119366, ___Targets_3)); }
	inline List_1_t2585711361 * get_Targets_3() const { return ___Targets_3; }
	inline List_1_t2585711361 ** get_address_of_Targets_3() { return &___Targets_3; }
	inline void set_Targets_3(List_1_t2585711361 * value)
	{
		___Targets_3 = value;
		Il2CppCodeGenWriteBarrier((&___Targets_3), value);
	}

	inline static int32_t get_offset_of_lockFocus_4() { return static_cast<int32_t>(offsetof(InteractionReceiver_t3572119366, ___lockFocus_4)); }
	inline bool get_lockFocus_4() const { return ___lockFocus_4; }
	inline bool* get_address_of_lockFocus_4() { return &___lockFocus_4; }
	inline void set_lockFocus_4(bool value)
	{
		___lockFocus_4 = value;
	}

	inline static int32_t get_offset_of__selectingFocuser_5() { return static_cast<int32_t>(offsetof(InteractionReceiver_t3572119366, ____selectingFocuser_5)); }
	inline RuntimeObject* get__selectingFocuser_5() const { return ____selectingFocuser_5; }
	inline RuntimeObject** get_address_of__selectingFocuser_5() { return &____selectingFocuser_5; }
	inline void set__selectingFocuser_5(RuntimeObject* value)
	{
		____selectingFocuser_5 = value;
		Il2CppCodeGenWriteBarrier((&____selectingFocuser_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONRECEIVER_T3572119366_H
#ifndef SINGLETON_1_T1639144629_H
#define SINGLETON_1_T1639144629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.StabilizationPlaneModifier>
struct  Singleton_1_t1639144629  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t1639144629_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	StabilizationPlaneModifier_t1238920175 * ___instance_2;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1639144629_StaticFields, ___instance_2)); }
	inline StabilizationPlaneModifier_t1238920175 * get_instance_2() const { return ___instance_2; }
	inline StabilizationPlaneModifier_t1238920175 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(StabilizationPlaneModifier_t1238920175 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_searchForInstance_3() { return static_cast<int32_t>(offsetof(Singleton_1_t1639144629_StaticFields, ___searchForInstance_3)); }
	inline bool get_searchForInstance_3() const { return ___searchForInstance_3; }
	inline bool* get_address_of_searchForInstance_3() { return &___searchForInstance_3; }
	inline void set_searchForInstance_3(bool value)
	{
		___searchForInstance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1639144629_H
#ifndef SINGLETON_1_T941632977_H
#define SINGLETON_1_T941632977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.TimerScheduler>
struct  Singleton_1_t941632977  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t941632977_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	TimerScheduler_t541408523 * ___instance_2;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t941632977_StaticFields, ___instance_2)); }
	inline TimerScheduler_t541408523 * get_instance_2() const { return ___instance_2; }
	inline TimerScheduler_t541408523 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(TimerScheduler_t541408523 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_searchForInstance_3() { return static_cast<int32_t>(offsetof(Singleton_1_t941632977_StaticFields, ___searchForInstance_3)); }
	inline bool get_searchForInstance_3() const { return ___searchForInstance_3; }
	inline bool* get_address_of_searchForInstance_3() { return &___searchForInstance_3; }
	inline void set_searchForInstance_3(bool value)
	{
		___searchForInstance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T941632977_H
#ifndef SIMPLETAGALONG_T3886944103_H
#define SIMPLETAGALONG_T3886944103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SimpleTagalong
struct  SimpleTagalong_t3886944103  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HoloToolkit.Unity.SimpleTagalong::TagalongDistance
	float ___TagalongDistance_2;
	// System.Boolean HoloToolkit.Unity.SimpleTagalong::EnforceDistance
	bool ___EnforceDistance_3;
	// System.Single HoloToolkit.Unity.SimpleTagalong::PositionUpdateSpeed
	float ___PositionUpdateSpeed_4;
	// System.Boolean HoloToolkit.Unity.SimpleTagalong::SmoothMotion
	bool ___SmoothMotion_5;
	// System.Single HoloToolkit.Unity.SimpleTagalong::SmoothingFactor
	float ___SmoothingFactor_6;
	// UnityEngine.BoxCollider HoloToolkit.Unity.SimpleTagalong::tagalongCollider
	BoxCollider_t1640800422 * ___tagalongCollider_7;
	// HoloToolkit.Unity.Interpolator HoloToolkit.Unity.SimpleTagalong::interpolator
	Interpolator_t3604653897 * ___interpolator_8;
	// UnityEngine.Plane[] HoloToolkit.Unity.SimpleTagalong::frustumPlanes
	PlaneU5BU5D_t3656189108* ___frustumPlanes_9;

public:
	inline static int32_t get_offset_of_TagalongDistance_2() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___TagalongDistance_2)); }
	inline float get_TagalongDistance_2() const { return ___TagalongDistance_2; }
	inline float* get_address_of_TagalongDistance_2() { return &___TagalongDistance_2; }
	inline void set_TagalongDistance_2(float value)
	{
		___TagalongDistance_2 = value;
	}

	inline static int32_t get_offset_of_EnforceDistance_3() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___EnforceDistance_3)); }
	inline bool get_EnforceDistance_3() const { return ___EnforceDistance_3; }
	inline bool* get_address_of_EnforceDistance_3() { return &___EnforceDistance_3; }
	inline void set_EnforceDistance_3(bool value)
	{
		___EnforceDistance_3 = value;
	}

	inline static int32_t get_offset_of_PositionUpdateSpeed_4() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___PositionUpdateSpeed_4)); }
	inline float get_PositionUpdateSpeed_4() const { return ___PositionUpdateSpeed_4; }
	inline float* get_address_of_PositionUpdateSpeed_4() { return &___PositionUpdateSpeed_4; }
	inline void set_PositionUpdateSpeed_4(float value)
	{
		___PositionUpdateSpeed_4 = value;
	}

	inline static int32_t get_offset_of_SmoothMotion_5() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___SmoothMotion_5)); }
	inline bool get_SmoothMotion_5() const { return ___SmoothMotion_5; }
	inline bool* get_address_of_SmoothMotion_5() { return &___SmoothMotion_5; }
	inline void set_SmoothMotion_5(bool value)
	{
		___SmoothMotion_5 = value;
	}

	inline static int32_t get_offset_of_SmoothingFactor_6() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___SmoothingFactor_6)); }
	inline float get_SmoothingFactor_6() const { return ___SmoothingFactor_6; }
	inline float* get_address_of_SmoothingFactor_6() { return &___SmoothingFactor_6; }
	inline void set_SmoothingFactor_6(float value)
	{
		___SmoothingFactor_6 = value;
	}

	inline static int32_t get_offset_of_tagalongCollider_7() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___tagalongCollider_7)); }
	inline BoxCollider_t1640800422 * get_tagalongCollider_7() const { return ___tagalongCollider_7; }
	inline BoxCollider_t1640800422 ** get_address_of_tagalongCollider_7() { return &___tagalongCollider_7; }
	inline void set_tagalongCollider_7(BoxCollider_t1640800422 * value)
	{
		___tagalongCollider_7 = value;
		Il2CppCodeGenWriteBarrier((&___tagalongCollider_7), value);
	}

	inline static int32_t get_offset_of_interpolator_8() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___interpolator_8)); }
	inline Interpolator_t3604653897 * get_interpolator_8() const { return ___interpolator_8; }
	inline Interpolator_t3604653897 ** get_address_of_interpolator_8() { return &___interpolator_8; }
	inline void set_interpolator_8(Interpolator_t3604653897 * value)
	{
		___interpolator_8 = value;
		Il2CppCodeGenWriteBarrier((&___interpolator_8), value);
	}

	inline static int32_t get_offset_of_frustumPlanes_9() { return static_cast<int32_t>(offsetof(SimpleTagalong_t3886944103, ___frustumPlanes_9)); }
	inline PlaneU5BU5D_t3656189108* get_frustumPlanes_9() const { return ___frustumPlanes_9; }
	inline PlaneU5BU5D_t3656189108** get_address_of_frustumPlanes_9() { return &___frustumPlanes_9; }
	inline void set_frustumPlanes_9(PlaneU5BU5D_t3656189108* value)
	{
		___frustumPlanes_9 = value;
		Il2CppCodeGenWriteBarrier((&___frustumPlanes_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLETAGALONG_T3886944103_H
#ifndef BUTTONSOUNDPROFILE_T3426452996_H
#define BUTTONSOUNDPROFILE_T3426452996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonSoundProfile
struct  ButtonSoundProfile_t3426452996  : public ButtonProfile_t1729449105
{
public:
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonCanceled
	AudioClip_t3680889665 * ___ButtonCanceled_2;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonHeld
	AudioClip_t3680889665 * ___ButtonHeld_3;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonPressed
	AudioClip_t3680889665 * ___ButtonPressed_4;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonReleased
	AudioClip_t3680889665 * ___ButtonReleased_5;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonObservation
	AudioClip_t3680889665 * ___ButtonObservation_6;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonObservationTargeted
	AudioClip_t3680889665 * ___ButtonObservationTargeted_7;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonTargeted
	AudioClip_t3680889665 * ___ButtonTargeted_8;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonCanceledVolume
	float ___ButtonCanceledVolume_9;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonHeldVolume
	float ___ButtonHeldVolume_10;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonPressedVolume
	float ___ButtonPressedVolume_11;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonReleasedVolume
	float ___ButtonReleasedVolume_12;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonObservationVolume
	float ___ButtonObservationVolume_13;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonObservationTargetedVolume
	float ___ButtonObservationTargetedVolume_14;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonTargetedVolume
	float ___ButtonTargetedVolume_15;

public:
	inline static int32_t get_offset_of_ButtonCanceled_2() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t3426452996, ___ButtonCanceled_2)); }
	inline AudioClip_t3680889665 * get_ButtonCanceled_2() const { return ___ButtonCanceled_2; }
	inline AudioClip_t3680889665 ** get_address_of_ButtonCanceled_2() { return &___ButtonCanceled_2; }
	inline void set_ButtonCanceled_2(AudioClip_t3680889665 * value)
	{
		___ButtonCanceled_2 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonCanceled_2), value);
	}

	inline static int32_t get_offset_of_ButtonHeld_3() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t3426452996, ___ButtonHeld_3)); }
	inline AudioClip_t3680889665 * get_ButtonHeld_3() const { return ___ButtonHeld_3; }
	inline AudioClip_t3680889665 ** get_address_of_ButtonHeld_3() { return &___ButtonHeld_3; }
	inline void set_ButtonHeld_3(AudioClip_t3680889665 * value)
	{
		___ButtonHeld_3 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonHeld_3), value);
	}

	inline static int32_t get_offset_of_ButtonPressed_4() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t3426452996, ___ButtonPressed_4)); }
	inline AudioClip_t3680889665 * get_ButtonPressed_4() const { return ___ButtonPressed_4; }
	inline AudioClip_t3680889665 ** get_address_of_ButtonPressed_4() { return &___ButtonPressed_4; }
	inline void set_ButtonPressed_4(AudioClip_t3680889665 * value)
	{
		___ButtonPressed_4 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonPressed_4), value);
	}

	inline static int32_t get_offset_of_ButtonReleased_5() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t3426452996, ___ButtonReleased_5)); }
	inline AudioClip_t3680889665 * get_ButtonReleased_5() const { return ___ButtonReleased_5; }
	inline AudioClip_t3680889665 ** get_address_of_ButtonReleased_5() { return &___ButtonReleased_5; }
	inline void set_ButtonReleased_5(AudioClip_t3680889665 * value)
	{
		___ButtonReleased_5 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonReleased_5), value);
	}

	inline static int32_t get_offset_of_ButtonObservation_6() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t3426452996, ___ButtonObservation_6)); }
	inline AudioClip_t3680889665 * get_ButtonObservation_6() const { return ___ButtonObservation_6; }
	inline AudioClip_t3680889665 ** get_address_of_ButtonObservation_6() { return &___ButtonObservation_6; }
	inline void set_ButtonObservation_6(AudioClip_t3680889665 * value)
	{
		___ButtonObservation_6 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonObservation_6), value);
	}

	inline static int32_t get_offset_of_ButtonObservationTargeted_7() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t3426452996, ___ButtonObservationTargeted_7)); }
	inline AudioClip_t3680889665 * get_ButtonObservationTargeted_7() const { return ___ButtonObservationTargeted_7; }
	inline AudioClip_t3680889665 ** get_address_of_ButtonObservationTargeted_7() { return &___ButtonObservationTargeted_7; }
	inline void set_ButtonObservationTargeted_7(AudioClip_t3680889665 * value)
	{
		___ButtonObservationTargeted_7 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonObservationTargeted_7), value);
	}

	inline static int32_t get_offset_of_ButtonTargeted_8() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t3426452996, ___ButtonTargeted_8)); }
	inline AudioClip_t3680889665 * get_ButtonTargeted_8() const { return ___ButtonTargeted_8; }
	inline AudioClip_t3680889665 ** get_address_of_ButtonTargeted_8() { return &___ButtonTargeted_8; }
	inline void set_ButtonTargeted_8(AudioClip_t3680889665 * value)
	{
		___ButtonTargeted_8 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonTargeted_8), value);
	}

	inline static int32_t get_offset_of_ButtonCanceledVolume_9() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t3426452996, ___ButtonCanceledVolume_9)); }
	inline float get_ButtonCanceledVolume_9() const { return ___ButtonCanceledVolume_9; }
	inline float* get_address_of_ButtonCanceledVolume_9() { return &___ButtonCanceledVolume_9; }
	inline void set_ButtonCanceledVolume_9(float value)
	{
		___ButtonCanceledVolume_9 = value;
	}

	inline static int32_t get_offset_of_ButtonHeldVolume_10() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t3426452996, ___ButtonHeldVolume_10)); }
	inline float get_ButtonHeldVolume_10() const { return ___ButtonHeldVolume_10; }
	inline float* get_address_of_ButtonHeldVolume_10() { return &___ButtonHeldVolume_10; }
	inline void set_ButtonHeldVolume_10(float value)
	{
		___ButtonHeldVolume_10 = value;
	}

	inline static int32_t get_offset_of_ButtonPressedVolume_11() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t3426452996, ___ButtonPressedVolume_11)); }
	inline float get_ButtonPressedVolume_11() const { return ___ButtonPressedVolume_11; }
	inline float* get_address_of_ButtonPressedVolume_11() { return &___ButtonPressedVolume_11; }
	inline void set_ButtonPressedVolume_11(float value)
	{
		___ButtonPressedVolume_11 = value;
	}

	inline static int32_t get_offset_of_ButtonReleasedVolume_12() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t3426452996, ___ButtonReleasedVolume_12)); }
	inline float get_ButtonReleasedVolume_12() const { return ___ButtonReleasedVolume_12; }
	inline float* get_address_of_ButtonReleasedVolume_12() { return &___ButtonReleasedVolume_12; }
	inline void set_ButtonReleasedVolume_12(float value)
	{
		___ButtonReleasedVolume_12 = value;
	}

	inline static int32_t get_offset_of_ButtonObservationVolume_13() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t3426452996, ___ButtonObservationVolume_13)); }
	inline float get_ButtonObservationVolume_13() const { return ___ButtonObservationVolume_13; }
	inline float* get_address_of_ButtonObservationVolume_13() { return &___ButtonObservationVolume_13; }
	inline void set_ButtonObservationVolume_13(float value)
	{
		___ButtonObservationVolume_13 = value;
	}

	inline static int32_t get_offset_of_ButtonObservationTargetedVolume_14() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t3426452996, ___ButtonObservationTargetedVolume_14)); }
	inline float get_ButtonObservationTargetedVolume_14() const { return ___ButtonObservationTargetedVolume_14; }
	inline float* get_address_of_ButtonObservationTargetedVolume_14() { return &___ButtonObservationTargetedVolume_14; }
	inline void set_ButtonObservationTargetedVolume_14(float value)
	{
		___ButtonObservationTargetedVolume_14 = value;
	}

	inline static int32_t get_offset_of_ButtonTargetedVolume_15() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t3426452996, ___ButtonTargetedVolume_15)); }
	inline float get_ButtonTargetedVolume_15() const { return ___ButtonTargetedVolume_15; }
	inline float* get_address_of_ButtonTargetedVolume_15() { return &___ButtonTargetedVolume_15; }
	inline void set_ButtonTargetedVolume_15(float value)
	{
		___ButtonTargetedVolume_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSOUNDPROFILE_T3426452996_H
#ifndef LINERENDERERBASE_T210867989_H
#define LINERENDERERBASE_T210867989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.LineRendererBase
struct  LineRendererBase_t210867989  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Unity.UX.LineBase HoloToolkit.Unity.UX.LineRendererBase::source
	LineBase_t3631573702 * ___source_2;
	// UnityEngine.Gradient HoloToolkit.Unity.UX.LineRendererBase::LineColor
	Gradient_t3067099924 * ___LineColor_3;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.LineRendererBase::LineWidth
	AnimationCurve_t3046754366 * ___LineWidth_4;
	// System.Single HoloToolkit.Unity.UX.LineRendererBase::WidthMultiplier
	float ___WidthMultiplier_5;
	// System.Single HoloToolkit.Unity.UX.LineRendererBase::ColorOffset
	float ___ColorOffset_6;
	// System.Single HoloToolkit.Unity.UX.LineRendererBase::WidthOffset
	float ___WidthOffset_7;
	// System.Single HoloToolkit.Unity.UX.LineRendererBase::RotationOffset
	float ___RotationOffset_8;
	// HoloToolkit.Unity.UX.StepModeEnum HoloToolkit.Unity.UX.LineRendererBase::StepMode
	int32_t ___StepMode_9;
	// System.Int32 HoloToolkit.Unity.UX.LineRendererBase::NumLineSteps
	int32_t ___NumLineSteps_10;
	// HoloToolkit.Unity.UX.InterpolationModeEnum HoloToolkit.Unity.UX.LineRendererBase::InterpolationMode
	int32_t ___InterpolationMode_11;
	// System.Single HoloToolkit.Unity.UX.LineRendererBase::StepLength
	float ___StepLength_12;
	// System.Int32 HoloToolkit.Unity.UX.LineRendererBase::MaxLineSteps
	int32_t ___MaxLineSteps_13;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.LineRendererBase::StepLengthCurve
	AnimationCurve_t3046754366 * ___StepLengthCurve_14;
	// System.Single[] HoloToolkit.Unity.UX.LineRendererBase::normalizedLengths
	SingleU5BU5D_t1444911251* ___normalizedLengths_15;

public:
	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(LineRendererBase_t210867989, ___source_2)); }
	inline LineBase_t3631573702 * get_source_2() const { return ___source_2; }
	inline LineBase_t3631573702 ** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(LineBase_t3631573702 * value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier((&___source_2), value);
	}

	inline static int32_t get_offset_of_LineColor_3() { return static_cast<int32_t>(offsetof(LineRendererBase_t210867989, ___LineColor_3)); }
	inline Gradient_t3067099924 * get_LineColor_3() const { return ___LineColor_3; }
	inline Gradient_t3067099924 ** get_address_of_LineColor_3() { return &___LineColor_3; }
	inline void set_LineColor_3(Gradient_t3067099924 * value)
	{
		___LineColor_3 = value;
		Il2CppCodeGenWriteBarrier((&___LineColor_3), value);
	}

	inline static int32_t get_offset_of_LineWidth_4() { return static_cast<int32_t>(offsetof(LineRendererBase_t210867989, ___LineWidth_4)); }
	inline AnimationCurve_t3046754366 * get_LineWidth_4() const { return ___LineWidth_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_LineWidth_4() { return &___LineWidth_4; }
	inline void set_LineWidth_4(AnimationCurve_t3046754366 * value)
	{
		___LineWidth_4 = value;
		Il2CppCodeGenWriteBarrier((&___LineWidth_4), value);
	}

	inline static int32_t get_offset_of_WidthMultiplier_5() { return static_cast<int32_t>(offsetof(LineRendererBase_t210867989, ___WidthMultiplier_5)); }
	inline float get_WidthMultiplier_5() const { return ___WidthMultiplier_5; }
	inline float* get_address_of_WidthMultiplier_5() { return &___WidthMultiplier_5; }
	inline void set_WidthMultiplier_5(float value)
	{
		___WidthMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_ColorOffset_6() { return static_cast<int32_t>(offsetof(LineRendererBase_t210867989, ___ColorOffset_6)); }
	inline float get_ColorOffset_6() const { return ___ColorOffset_6; }
	inline float* get_address_of_ColorOffset_6() { return &___ColorOffset_6; }
	inline void set_ColorOffset_6(float value)
	{
		___ColorOffset_6 = value;
	}

	inline static int32_t get_offset_of_WidthOffset_7() { return static_cast<int32_t>(offsetof(LineRendererBase_t210867989, ___WidthOffset_7)); }
	inline float get_WidthOffset_7() const { return ___WidthOffset_7; }
	inline float* get_address_of_WidthOffset_7() { return &___WidthOffset_7; }
	inline void set_WidthOffset_7(float value)
	{
		___WidthOffset_7 = value;
	}

	inline static int32_t get_offset_of_RotationOffset_8() { return static_cast<int32_t>(offsetof(LineRendererBase_t210867989, ___RotationOffset_8)); }
	inline float get_RotationOffset_8() const { return ___RotationOffset_8; }
	inline float* get_address_of_RotationOffset_8() { return &___RotationOffset_8; }
	inline void set_RotationOffset_8(float value)
	{
		___RotationOffset_8 = value;
	}

	inline static int32_t get_offset_of_StepMode_9() { return static_cast<int32_t>(offsetof(LineRendererBase_t210867989, ___StepMode_9)); }
	inline int32_t get_StepMode_9() const { return ___StepMode_9; }
	inline int32_t* get_address_of_StepMode_9() { return &___StepMode_9; }
	inline void set_StepMode_9(int32_t value)
	{
		___StepMode_9 = value;
	}

	inline static int32_t get_offset_of_NumLineSteps_10() { return static_cast<int32_t>(offsetof(LineRendererBase_t210867989, ___NumLineSteps_10)); }
	inline int32_t get_NumLineSteps_10() const { return ___NumLineSteps_10; }
	inline int32_t* get_address_of_NumLineSteps_10() { return &___NumLineSteps_10; }
	inline void set_NumLineSteps_10(int32_t value)
	{
		___NumLineSteps_10 = value;
	}

	inline static int32_t get_offset_of_InterpolationMode_11() { return static_cast<int32_t>(offsetof(LineRendererBase_t210867989, ___InterpolationMode_11)); }
	inline int32_t get_InterpolationMode_11() const { return ___InterpolationMode_11; }
	inline int32_t* get_address_of_InterpolationMode_11() { return &___InterpolationMode_11; }
	inline void set_InterpolationMode_11(int32_t value)
	{
		___InterpolationMode_11 = value;
	}

	inline static int32_t get_offset_of_StepLength_12() { return static_cast<int32_t>(offsetof(LineRendererBase_t210867989, ___StepLength_12)); }
	inline float get_StepLength_12() const { return ___StepLength_12; }
	inline float* get_address_of_StepLength_12() { return &___StepLength_12; }
	inline void set_StepLength_12(float value)
	{
		___StepLength_12 = value;
	}

	inline static int32_t get_offset_of_MaxLineSteps_13() { return static_cast<int32_t>(offsetof(LineRendererBase_t210867989, ___MaxLineSteps_13)); }
	inline int32_t get_MaxLineSteps_13() const { return ___MaxLineSteps_13; }
	inline int32_t* get_address_of_MaxLineSteps_13() { return &___MaxLineSteps_13; }
	inline void set_MaxLineSteps_13(int32_t value)
	{
		___MaxLineSteps_13 = value;
	}

	inline static int32_t get_offset_of_StepLengthCurve_14() { return static_cast<int32_t>(offsetof(LineRendererBase_t210867989, ___StepLengthCurve_14)); }
	inline AnimationCurve_t3046754366 * get_StepLengthCurve_14() const { return ___StepLengthCurve_14; }
	inline AnimationCurve_t3046754366 ** get_address_of_StepLengthCurve_14() { return &___StepLengthCurve_14; }
	inline void set_StepLengthCurve_14(AnimationCurve_t3046754366 * value)
	{
		___StepLengthCurve_14 = value;
		Il2CppCodeGenWriteBarrier((&___StepLengthCurve_14), value);
	}

	inline static int32_t get_offset_of_normalizedLengths_15() { return static_cast<int32_t>(offsetof(LineRendererBase_t210867989, ___normalizedLengths_15)); }
	inline SingleU5BU5D_t1444911251* get_normalizedLengths_15() const { return ___normalizedLengths_15; }
	inline SingleU5BU5D_t1444911251** get_address_of_normalizedLengths_15() { return &___normalizedLengths_15; }
	inline void set_normalizedLengths_15(SingleU5BU5D_t1444911251* value)
	{
		___normalizedLengths_15 = value;
		Il2CppCodeGenWriteBarrier((&___normalizedLengths_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINERENDERERBASE_T210867989_H
#ifndef BUTTONMESHPROFILE_T417185472_H
#define BUTTONMESHPROFILE_T417185472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonMeshProfile
struct  ButtonMeshProfile_t417185472  : public ButtonProfile_t1729449105
{
public:
	// System.String HoloToolkit.Unity.Buttons.ButtonMeshProfile::ColorPropertyName
	String_t* ___ColorPropertyName_2;
	// System.String HoloToolkit.Unity.Buttons.ButtonMeshProfile::ValuePropertyName
	String_t* ___ValuePropertyName_3;
	// System.Boolean HoloToolkit.Unity.Buttons.ButtonMeshProfile::SmoothStateChanges
	bool ___SmoothStateChanges_4;
	// System.Boolean HoloToolkit.Unity.Buttons.ButtonMeshProfile::StickyPressedEvents
	bool ___StickyPressedEvents_5;
	// System.Single HoloToolkit.Unity.Buttons.ButtonMeshProfile::StickyPressedTime
	float ___StickyPressedTime_6;
	// System.Single HoloToolkit.Unity.Buttons.ButtonMeshProfile::AnimationSpeed
	float ___AnimationSpeed_7;
	// HoloToolkit.Unity.Buttons.CompoundButtonMesh/MeshButtonDatum[] HoloToolkit.Unity.Buttons.ButtonMeshProfile::ButtonStates
	MeshButtonDatumU5BU5D_t2480441126* ___ButtonStates_8;

public:
	inline static int32_t get_offset_of_ColorPropertyName_2() { return static_cast<int32_t>(offsetof(ButtonMeshProfile_t417185472, ___ColorPropertyName_2)); }
	inline String_t* get_ColorPropertyName_2() const { return ___ColorPropertyName_2; }
	inline String_t** get_address_of_ColorPropertyName_2() { return &___ColorPropertyName_2; }
	inline void set_ColorPropertyName_2(String_t* value)
	{
		___ColorPropertyName_2 = value;
		Il2CppCodeGenWriteBarrier((&___ColorPropertyName_2), value);
	}

	inline static int32_t get_offset_of_ValuePropertyName_3() { return static_cast<int32_t>(offsetof(ButtonMeshProfile_t417185472, ___ValuePropertyName_3)); }
	inline String_t* get_ValuePropertyName_3() const { return ___ValuePropertyName_3; }
	inline String_t** get_address_of_ValuePropertyName_3() { return &___ValuePropertyName_3; }
	inline void set_ValuePropertyName_3(String_t* value)
	{
		___ValuePropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___ValuePropertyName_3), value);
	}

	inline static int32_t get_offset_of_SmoothStateChanges_4() { return static_cast<int32_t>(offsetof(ButtonMeshProfile_t417185472, ___SmoothStateChanges_4)); }
	inline bool get_SmoothStateChanges_4() const { return ___SmoothStateChanges_4; }
	inline bool* get_address_of_SmoothStateChanges_4() { return &___SmoothStateChanges_4; }
	inline void set_SmoothStateChanges_4(bool value)
	{
		___SmoothStateChanges_4 = value;
	}

	inline static int32_t get_offset_of_StickyPressedEvents_5() { return static_cast<int32_t>(offsetof(ButtonMeshProfile_t417185472, ___StickyPressedEvents_5)); }
	inline bool get_StickyPressedEvents_5() const { return ___StickyPressedEvents_5; }
	inline bool* get_address_of_StickyPressedEvents_5() { return &___StickyPressedEvents_5; }
	inline void set_StickyPressedEvents_5(bool value)
	{
		___StickyPressedEvents_5 = value;
	}

	inline static int32_t get_offset_of_StickyPressedTime_6() { return static_cast<int32_t>(offsetof(ButtonMeshProfile_t417185472, ___StickyPressedTime_6)); }
	inline float get_StickyPressedTime_6() const { return ___StickyPressedTime_6; }
	inline float* get_address_of_StickyPressedTime_6() { return &___StickyPressedTime_6; }
	inline void set_StickyPressedTime_6(float value)
	{
		___StickyPressedTime_6 = value;
	}

	inline static int32_t get_offset_of_AnimationSpeed_7() { return static_cast<int32_t>(offsetof(ButtonMeshProfile_t417185472, ___AnimationSpeed_7)); }
	inline float get_AnimationSpeed_7() const { return ___AnimationSpeed_7; }
	inline float* get_address_of_AnimationSpeed_7() { return &___AnimationSpeed_7; }
	inline void set_AnimationSpeed_7(float value)
	{
		___AnimationSpeed_7 = value;
	}

	inline static int32_t get_offset_of_ButtonStates_8() { return static_cast<int32_t>(offsetof(ButtonMeshProfile_t417185472, ___ButtonStates_8)); }
	inline MeshButtonDatumU5BU5D_t2480441126* get_ButtonStates_8() const { return ___ButtonStates_8; }
	inline MeshButtonDatumU5BU5D_t2480441126** get_address_of_ButtonStates_8() { return &___ButtonStates_8; }
	inline void set_ButtonStates_8(MeshButtonDatumU5BU5D_t2480441126* value)
	{
		___ButtonStates_8 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonStates_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONMESHPROFILE_T417185472_H
#ifndef SORTINGLAYEROVERRIDE_T4116415771_H
#define SORTINGLAYEROVERRIDE_T4116415771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SortingLayerOverride
struct  SortingLayerOverride_t4116415771  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean HoloToolkit.Unity.SortingLayerOverride::UseLastLayer
	bool ___UseLastLayer_2;
	// System.String HoloToolkit.Unity.SortingLayerOverride::TargetSortingLayerName
	String_t* ___TargetSortingLayerName_3;
	// UnityEngine.Renderer[] HoloToolkit.Unity.SortingLayerOverride::renderers
	RendererU5BU5D_t3210418286* ___renderers_4;

public:
	inline static int32_t get_offset_of_UseLastLayer_2() { return static_cast<int32_t>(offsetof(SortingLayerOverride_t4116415771, ___UseLastLayer_2)); }
	inline bool get_UseLastLayer_2() const { return ___UseLastLayer_2; }
	inline bool* get_address_of_UseLastLayer_2() { return &___UseLastLayer_2; }
	inline void set_UseLastLayer_2(bool value)
	{
		___UseLastLayer_2 = value;
	}

	inline static int32_t get_offset_of_TargetSortingLayerName_3() { return static_cast<int32_t>(offsetof(SortingLayerOverride_t4116415771, ___TargetSortingLayerName_3)); }
	inline String_t* get_TargetSortingLayerName_3() const { return ___TargetSortingLayerName_3; }
	inline String_t** get_address_of_TargetSortingLayerName_3() { return &___TargetSortingLayerName_3; }
	inline void set_TargetSortingLayerName_3(String_t* value)
	{
		___TargetSortingLayerName_3 = value;
		Il2CppCodeGenWriteBarrier((&___TargetSortingLayerName_3), value);
	}

	inline static int32_t get_offset_of_renderers_4() { return static_cast<int32_t>(offsetof(SortingLayerOverride_t4116415771, ___renderers_4)); }
	inline RendererU5BU5D_t3210418286* get_renderers_4() const { return ___renderers_4; }
	inline RendererU5BU5D_t3210418286** get_address_of_renderers_4() { return &___renderers_4; }
	inline void set_renderers_4(RendererU5BU5D_t3210418286* value)
	{
		___renderers_4 = value;
		Il2CppCodeGenWriteBarrier((&___renderers_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTINGLAYEROVERRIDE_T4116415771_H
#ifndef BUTTON_T2162584723_H
#define BUTTON_T2162584723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.Button
struct  Button_t2162584723  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.Button::ButtonState
	int32_t ___ButtonState_2;
	// HoloToolkit.Unity.InputModule.InteractionSourcePressInfo HoloToolkit.Unity.Buttons.Button::ButtonPressFilter
	int32_t ___ButtonPressFilter_3;
	// System.Boolean HoloToolkit.Unity.Buttons.Button::RequireGaze
	bool ___RequireGaze_4;
	// System.Action`1<HoloToolkit.Unity.Buttons.ButtonStateEnum> HoloToolkit.Unity.Buttons.Button::StateChange
	Action_1_t3460105790 * ___StateChange_5;
	// System.Action`1<UnityEngine.GameObject> HoloToolkit.Unity.Buttons.Button::OnButtonPressed
	Action_1_t1286104214 * ___OnButtonPressed_6;
	// System.Action`1<UnityEngine.GameObject> HoloToolkit.Unity.Buttons.Button::OnButtonReleased
	Action_1_t1286104214 * ___OnButtonReleased_7;
	// System.Action`1<UnityEngine.GameObject> HoloToolkit.Unity.Buttons.Button::OnButtonClicked
	Action_1_t1286104214 * ___OnButtonClicked_8;
	// System.Action`1<UnityEngine.GameObject> HoloToolkit.Unity.Buttons.Button::OnButtonHeld
	Action_1_t1286104214 * ___OnButtonHeld_9;
	// System.Action`1<UnityEngine.GameObject> HoloToolkit.Unity.Buttons.Button::OnButtonCanceled
	Action_1_t1286104214 * ___OnButtonCanceled_10;
	// System.String HoloToolkit.Unity.Buttons.Button::_GizmoIcon
	String_t* ____GizmoIcon_11;
	// System.Boolean HoloToolkit.Unity.Buttons.Button::_bLastHandVisible
	bool ____bLastHandVisible_12;
	// System.Boolean HoloToolkit.Unity.Buttons.Button::_bHandVisible
	bool ____bHandVisible_13;
	// System.Boolean HoloToolkit.Unity.Buttons.Button::_bFocused
	bool ____bFocused_14;
	// System.Int32 HoloToolkit.Unity.Buttons.Button::_handCount
	int32_t ____handCount_15;

public:
	inline static int32_t get_offset_of_ButtonState_2() { return static_cast<int32_t>(offsetof(Button_t2162584723, ___ButtonState_2)); }
	inline int32_t get_ButtonState_2() const { return ___ButtonState_2; }
	inline int32_t* get_address_of_ButtonState_2() { return &___ButtonState_2; }
	inline void set_ButtonState_2(int32_t value)
	{
		___ButtonState_2 = value;
	}

	inline static int32_t get_offset_of_ButtonPressFilter_3() { return static_cast<int32_t>(offsetof(Button_t2162584723, ___ButtonPressFilter_3)); }
	inline int32_t get_ButtonPressFilter_3() const { return ___ButtonPressFilter_3; }
	inline int32_t* get_address_of_ButtonPressFilter_3() { return &___ButtonPressFilter_3; }
	inline void set_ButtonPressFilter_3(int32_t value)
	{
		___ButtonPressFilter_3 = value;
	}

	inline static int32_t get_offset_of_RequireGaze_4() { return static_cast<int32_t>(offsetof(Button_t2162584723, ___RequireGaze_4)); }
	inline bool get_RequireGaze_4() const { return ___RequireGaze_4; }
	inline bool* get_address_of_RequireGaze_4() { return &___RequireGaze_4; }
	inline void set_RequireGaze_4(bool value)
	{
		___RequireGaze_4 = value;
	}

	inline static int32_t get_offset_of_StateChange_5() { return static_cast<int32_t>(offsetof(Button_t2162584723, ___StateChange_5)); }
	inline Action_1_t3460105790 * get_StateChange_5() const { return ___StateChange_5; }
	inline Action_1_t3460105790 ** get_address_of_StateChange_5() { return &___StateChange_5; }
	inline void set_StateChange_5(Action_1_t3460105790 * value)
	{
		___StateChange_5 = value;
		Il2CppCodeGenWriteBarrier((&___StateChange_5), value);
	}

	inline static int32_t get_offset_of_OnButtonPressed_6() { return static_cast<int32_t>(offsetof(Button_t2162584723, ___OnButtonPressed_6)); }
	inline Action_1_t1286104214 * get_OnButtonPressed_6() const { return ___OnButtonPressed_6; }
	inline Action_1_t1286104214 ** get_address_of_OnButtonPressed_6() { return &___OnButtonPressed_6; }
	inline void set_OnButtonPressed_6(Action_1_t1286104214 * value)
	{
		___OnButtonPressed_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonPressed_6), value);
	}

	inline static int32_t get_offset_of_OnButtonReleased_7() { return static_cast<int32_t>(offsetof(Button_t2162584723, ___OnButtonReleased_7)); }
	inline Action_1_t1286104214 * get_OnButtonReleased_7() const { return ___OnButtonReleased_7; }
	inline Action_1_t1286104214 ** get_address_of_OnButtonReleased_7() { return &___OnButtonReleased_7; }
	inline void set_OnButtonReleased_7(Action_1_t1286104214 * value)
	{
		___OnButtonReleased_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonReleased_7), value);
	}

	inline static int32_t get_offset_of_OnButtonClicked_8() { return static_cast<int32_t>(offsetof(Button_t2162584723, ___OnButtonClicked_8)); }
	inline Action_1_t1286104214 * get_OnButtonClicked_8() const { return ___OnButtonClicked_8; }
	inline Action_1_t1286104214 ** get_address_of_OnButtonClicked_8() { return &___OnButtonClicked_8; }
	inline void set_OnButtonClicked_8(Action_1_t1286104214 * value)
	{
		___OnButtonClicked_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonClicked_8), value);
	}

	inline static int32_t get_offset_of_OnButtonHeld_9() { return static_cast<int32_t>(offsetof(Button_t2162584723, ___OnButtonHeld_9)); }
	inline Action_1_t1286104214 * get_OnButtonHeld_9() const { return ___OnButtonHeld_9; }
	inline Action_1_t1286104214 ** get_address_of_OnButtonHeld_9() { return &___OnButtonHeld_9; }
	inline void set_OnButtonHeld_9(Action_1_t1286104214 * value)
	{
		___OnButtonHeld_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonHeld_9), value);
	}

	inline static int32_t get_offset_of_OnButtonCanceled_10() { return static_cast<int32_t>(offsetof(Button_t2162584723, ___OnButtonCanceled_10)); }
	inline Action_1_t1286104214 * get_OnButtonCanceled_10() const { return ___OnButtonCanceled_10; }
	inline Action_1_t1286104214 ** get_address_of_OnButtonCanceled_10() { return &___OnButtonCanceled_10; }
	inline void set_OnButtonCanceled_10(Action_1_t1286104214 * value)
	{
		___OnButtonCanceled_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonCanceled_10), value);
	}

	inline static int32_t get_offset_of__GizmoIcon_11() { return static_cast<int32_t>(offsetof(Button_t2162584723, ____GizmoIcon_11)); }
	inline String_t* get__GizmoIcon_11() const { return ____GizmoIcon_11; }
	inline String_t** get_address_of__GizmoIcon_11() { return &____GizmoIcon_11; }
	inline void set__GizmoIcon_11(String_t* value)
	{
		____GizmoIcon_11 = value;
		Il2CppCodeGenWriteBarrier((&____GizmoIcon_11), value);
	}

	inline static int32_t get_offset_of__bLastHandVisible_12() { return static_cast<int32_t>(offsetof(Button_t2162584723, ____bLastHandVisible_12)); }
	inline bool get__bLastHandVisible_12() const { return ____bLastHandVisible_12; }
	inline bool* get_address_of__bLastHandVisible_12() { return &____bLastHandVisible_12; }
	inline void set__bLastHandVisible_12(bool value)
	{
		____bLastHandVisible_12 = value;
	}

	inline static int32_t get_offset_of__bHandVisible_13() { return static_cast<int32_t>(offsetof(Button_t2162584723, ____bHandVisible_13)); }
	inline bool get__bHandVisible_13() const { return ____bHandVisible_13; }
	inline bool* get_address_of__bHandVisible_13() { return &____bHandVisible_13; }
	inline void set__bHandVisible_13(bool value)
	{
		____bHandVisible_13 = value;
	}

	inline static int32_t get_offset_of__bFocused_14() { return static_cast<int32_t>(offsetof(Button_t2162584723, ____bFocused_14)); }
	inline bool get__bFocused_14() const { return ____bFocused_14; }
	inline bool* get_address_of__bFocused_14() { return &____bFocused_14; }
	inline void set__bFocused_14(bool value)
	{
		____bFocused_14 = value;
	}

	inline static int32_t get_offset_of__handCount_15() { return static_cast<int32_t>(offsetof(Button_t2162584723, ____handCount_15)); }
	inline int32_t get__handCount_15() const { return ____handCount_15; }
	inline int32_t* get_address_of__handCount_15() { return &____handCount_15; }
	inline void set__handCount_15(int32_t value)
	{
		____handCount_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T2162584723_H
#ifndef BUTTONICONPROFILE_T2026164329_H
#define BUTTONICONPROFILE_T2026164329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonIconProfile
struct  ButtonIconProfile_t2026164329  : public ButtonProfile_t1729449105
{
public:
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfile::_IconNotFound
	Texture2D_t3840446185 * ____IconNotFound_2;
	// System.Single HoloToolkit.Unity.Buttons.ButtonIconProfile::AlphaTransitionSpeed
	float ___AlphaTransitionSpeed_3;
	// UnityEngine.Material HoloToolkit.Unity.Buttons.ButtonIconProfile::IconMaterial
	Material_t340375123 * ___IconMaterial_4;
	// UnityEngine.Mesh HoloToolkit.Unity.Buttons.ButtonIconProfile::IconMesh
	Mesh_t3648964284 * ___IconMesh_5;
	// System.String HoloToolkit.Unity.Buttons.ButtonIconProfile::AlphaColorProperty
	String_t* ___AlphaColorProperty_6;

public:
	inline static int32_t get_offset_of__IconNotFound_2() { return static_cast<int32_t>(offsetof(ButtonIconProfile_t2026164329, ____IconNotFound_2)); }
	inline Texture2D_t3840446185 * get__IconNotFound_2() const { return ____IconNotFound_2; }
	inline Texture2D_t3840446185 ** get_address_of__IconNotFound_2() { return &____IconNotFound_2; }
	inline void set__IconNotFound_2(Texture2D_t3840446185 * value)
	{
		____IconNotFound_2 = value;
		Il2CppCodeGenWriteBarrier((&____IconNotFound_2), value);
	}

	inline static int32_t get_offset_of_AlphaTransitionSpeed_3() { return static_cast<int32_t>(offsetof(ButtonIconProfile_t2026164329, ___AlphaTransitionSpeed_3)); }
	inline float get_AlphaTransitionSpeed_3() const { return ___AlphaTransitionSpeed_3; }
	inline float* get_address_of_AlphaTransitionSpeed_3() { return &___AlphaTransitionSpeed_3; }
	inline void set_AlphaTransitionSpeed_3(float value)
	{
		___AlphaTransitionSpeed_3 = value;
	}

	inline static int32_t get_offset_of_IconMaterial_4() { return static_cast<int32_t>(offsetof(ButtonIconProfile_t2026164329, ___IconMaterial_4)); }
	inline Material_t340375123 * get_IconMaterial_4() const { return ___IconMaterial_4; }
	inline Material_t340375123 ** get_address_of_IconMaterial_4() { return &___IconMaterial_4; }
	inline void set_IconMaterial_4(Material_t340375123 * value)
	{
		___IconMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___IconMaterial_4), value);
	}

	inline static int32_t get_offset_of_IconMesh_5() { return static_cast<int32_t>(offsetof(ButtonIconProfile_t2026164329, ___IconMesh_5)); }
	inline Mesh_t3648964284 * get_IconMesh_5() const { return ___IconMesh_5; }
	inline Mesh_t3648964284 ** get_address_of_IconMesh_5() { return &___IconMesh_5; }
	inline void set_IconMesh_5(Mesh_t3648964284 * value)
	{
		___IconMesh_5 = value;
		Il2CppCodeGenWriteBarrier((&___IconMesh_5), value);
	}

	inline static int32_t get_offset_of_AlphaColorProperty_6() { return static_cast<int32_t>(offsetof(ButtonIconProfile_t2026164329, ___AlphaColorProperty_6)); }
	inline String_t* get_AlphaColorProperty_6() const { return ___AlphaColorProperty_6; }
	inline String_t** get_address_of_AlphaColorProperty_6() { return &___AlphaColorProperty_6; }
	inline void set_AlphaColorProperty_6(String_t* value)
	{
		___AlphaColorProperty_6 = value;
		Il2CppCodeGenWriteBarrier((&___AlphaColorProperty_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONICONPROFILE_T2026164329_H
#ifndef BUTTONLOCALIZEDTEXT_T3494258600_H
#define BUTTONLOCALIZEDTEXT_T3494258600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonLocalizedText
struct  ButtonLocalizedText_t3494258600  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.TextMesh HoloToolkit.Unity.Buttons.ButtonLocalizedText::TextMesh
	TextMesh_t1536577757 * ___TextMesh_2;

public:
	inline static int32_t get_offset_of_TextMesh_2() { return static_cast<int32_t>(offsetof(ButtonLocalizedText_t3494258600, ___TextMesh_2)); }
	inline TextMesh_t1536577757 * get_TextMesh_2() const { return ___TextMesh_2; }
	inline TextMesh_t1536577757 ** get_address_of_TextMesh_2() { return &___TextMesh_2; }
	inline void set_TextMesh_2(TextMesh_t1536577757 * value)
	{
		___TextMesh_2 = value;
		Il2CppCodeGenWriteBarrier((&___TextMesh_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONLOCALIZEDTEXT_T3494258600_H
#ifndef PROFILEBUTTONBASE_1_T413189496_H
#define PROFILEBUTTONBASE_1_T413189496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ProfileButtonBase`1<HoloToolkit.Unity.Buttons.ButtonMeshProfile>
struct  ProfileButtonBase_1_t413189496  : public MonoBehaviour_t3962482529
{
public:
	// T HoloToolkit.Unity.Buttons.ProfileButtonBase`1::Profile
	ButtonMeshProfile_t417185472 * ___Profile_2;

public:
	inline static int32_t get_offset_of_Profile_2() { return static_cast<int32_t>(offsetof(ProfileButtonBase_1_t413189496, ___Profile_2)); }
	inline ButtonMeshProfile_t417185472 * get_Profile_2() const { return ___Profile_2; }
	inline ButtonMeshProfile_t417185472 ** get_address_of_Profile_2() { return &___Profile_2; }
	inline void set_Profile_2(ButtonMeshProfile_t417185472 * value)
	{
		___Profile_2 = value;
		Il2CppCodeGenWriteBarrier((&___Profile_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEBUTTONBASE_1_T413189496_H
#ifndef PROFILEBUTTONBASE_1_T2022168353_H
#define PROFILEBUTTONBASE_1_T2022168353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ProfileButtonBase`1<HoloToolkit.Unity.Buttons.ButtonIconProfile>
struct  ProfileButtonBase_1_t2022168353  : public MonoBehaviour_t3962482529
{
public:
	// T HoloToolkit.Unity.Buttons.ProfileButtonBase`1::Profile
	ButtonIconProfile_t2026164329 * ___Profile_2;

public:
	inline static int32_t get_offset_of_Profile_2() { return static_cast<int32_t>(offsetof(ProfileButtonBase_1_t2022168353, ___Profile_2)); }
	inline ButtonIconProfile_t2026164329 * get_Profile_2() const { return ___Profile_2; }
	inline ButtonIconProfile_t2026164329 ** get_address_of_Profile_2() { return &___Profile_2; }
	inline void set_Profile_2(ButtonIconProfile_t2026164329 * value)
	{
		___Profile_2 = value;
		Il2CppCodeGenWriteBarrier((&___Profile_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEBUTTONBASE_1_T2022168353_H
#ifndef TEXTTOSPEECH_T421734152_H
#define TEXTTOSPEECH_T421734152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TextToSpeech
struct  TextToSpeech_t421734152  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource HoloToolkit.Unity.TextToSpeech::audioSource
	AudioSource_t3935305588 * ___audioSource_2;
	// HoloToolkit.Unity.TextToSpeechVoice HoloToolkit.Unity.TextToSpeech::voice
	int32_t ___voice_3;
	// Windows.Media.SpeechSynthesis.SpeechSynthesizer HoloToolkit.Unity.TextToSpeech::synthesizer
	SpeechSynthesizer_t2130945875 * ___synthesizer_4;
	// Windows.Media.SpeechSynthesis.VoiceInformation HoloToolkit.Unity.TextToSpeech::voiceInfo
	VoiceInformation_t4266404632 * ___voiceInfo_5;
	// System.Boolean HoloToolkit.Unity.TextToSpeech::speechTextInQueue
	bool ___speechTextInQueue_6;

public:
	inline static int32_t get_offset_of_audioSource_2() { return static_cast<int32_t>(offsetof(TextToSpeech_t421734152, ___audioSource_2)); }
	inline AudioSource_t3935305588 * get_audioSource_2() const { return ___audioSource_2; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_2() { return &___audioSource_2; }
	inline void set_audioSource_2(AudioSource_t3935305588 * value)
	{
		___audioSource_2 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_2), value);
	}

	inline static int32_t get_offset_of_voice_3() { return static_cast<int32_t>(offsetof(TextToSpeech_t421734152, ___voice_3)); }
	inline int32_t get_voice_3() const { return ___voice_3; }
	inline int32_t* get_address_of_voice_3() { return &___voice_3; }
	inline void set_voice_3(int32_t value)
	{
		___voice_3 = value;
	}

	inline static int32_t get_offset_of_synthesizer_4() { return static_cast<int32_t>(offsetof(TextToSpeech_t421734152, ___synthesizer_4)); }
	inline SpeechSynthesizer_t2130945875 * get_synthesizer_4() const { return ___synthesizer_4; }
	inline SpeechSynthesizer_t2130945875 ** get_address_of_synthesizer_4() { return &___synthesizer_4; }
	inline void set_synthesizer_4(SpeechSynthesizer_t2130945875 * value)
	{
		___synthesizer_4 = value;
		Il2CppCodeGenWriteBarrier((&___synthesizer_4), value);
	}

	inline static int32_t get_offset_of_voiceInfo_5() { return static_cast<int32_t>(offsetof(TextToSpeech_t421734152, ___voiceInfo_5)); }
	inline VoiceInformation_t4266404632 * get_voiceInfo_5() const { return ___voiceInfo_5; }
	inline VoiceInformation_t4266404632 ** get_address_of_voiceInfo_5() { return &___voiceInfo_5; }
	inline void set_voiceInfo_5(VoiceInformation_t4266404632 * value)
	{
		___voiceInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___voiceInfo_5), value);
	}

	inline static int32_t get_offset_of_speechTextInQueue_6() { return static_cast<int32_t>(offsetof(TextToSpeech_t421734152, ___speechTextInQueue_6)); }
	inline bool get_speechTextInQueue_6() const { return ___speechTextInQueue_6; }
	inline bool* get_address_of_speechTextInQueue_6() { return &___speechTextInQueue_6; }
	inline void set_speechTextInQueue_6(bool value)
	{
		___speechTextInQueue_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTTOSPEECH_T421734152_H
#ifndef SPHEREBASEDTAGALONG_T2880395387_H
#define SPHEREBASEDTAGALONG_T2880395387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SphereBasedTagalong
struct  SphereBasedTagalong_t2880395387  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HoloToolkit.Unity.SphereBasedTagalong::SphereRadius
	float ___SphereRadius_2;
	// System.Single HoloToolkit.Unity.SphereBasedTagalong::MoveSpeed
	float ___MoveSpeed_3;
	// System.Boolean HoloToolkit.Unity.SphereBasedTagalong::useUnscaledTime
	bool ___useUnscaledTime_4;
	// System.Boolean HoloToolkit.Unity.SphereBasedTagalong::hideOnStart
	bool ___hideOnStart_5;
	// System.Boolean HoloToolkit.Unity.SphereBasedTagalong::debugDisplaySphere
	bool ___debugDisplaySphere_6;
	// System.Boolean HoloToolkit.Unity.SphereBasedTagalong::debugDisplayTargetPosition
	bool ___debugDisplayTargetPosition_7;
	// UnityEngine.Vector3 HoloToolkit.Unity.SphereBasedTagalong::targetPosition
	Vector3_t3722313464  ___targetPosition_8;
	// UnityEngine.Vector3 HoloToolkit.Unity.SphereBasedTagalong::optimalPosition
	Vector3_t3722313464  ___optimalPosition_9;
	// System.Single HoloToolkit.Unity.SphereBasedTagalong::initialDistanceToCamera
	float ___initialDistanceToCamera_10;

public:
	inline static int32_t get_offset_of_SphereRadius_2() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t2880395387, ___SphereRadius_2)); }
	inline float get_SphereRadius_2() const { return ___SphereRadius_2; }
	inline float* get_address_of_SphereRadius_2() { return &___SphereRadius_2; }
	inline void set_SphereRadius_2(float value)
	{
		___SphereRadius_2 = value;
	}

	inline static int32_t get_offset_of_MoveSpeed_3() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t2880395387, ___MoveSpeed_3)); }
	inline float get_MoveSpeed_3() const { return ___MoveSpeed_3; }
	inline float* get_address_of_MoveSpeed_3() { return &___MoveSpeed_3; }
	inline void set_MoveSpeed_3(float value)
	{
		___MoveSpeed_3 = value;
	}

	inline static int32_t get_offset_of_useUnscaledTime_4() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t2880395387, ___useUnscaledTime_4)); }
	inline bool get_useUnscaledTime_4() const { return ___useUnscaledTime_4; }
	inline bool* get_address_of_useUnscaledTime_4() { return &___useUnscaledTime_4; }
	inline void set_useUnscaledTime_4(bool value)
	{
		___useUnscaledTime_4 = value;
	}

	inline static int32_t get_offset_of_hideOnStart_5() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t2880395387, ___hideOnStart_5)); }
	inline bool get_hideOnStart_5() const { return ___hideOnStart_5; }
	inline bool* get_address_of_hideOnStart_5() { return &___hideOnStart_5; }
	inline void set_hideOnStart_5(bool value)
	{
		___hideOnStart_5 = value;
	}

	inline static int32_t get_offset_of_debugDisplaySphere_6() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t2880395387, ___debugDisplaySphere_6)); }
	inline bool get_debugDisplaySphere_6() const { return ___debugDisplaySphere_6; }
	inline bool* get_address_of_debugDisplaySphere_6() { return &___debugDisplaySphere_6; }
	inline void set_debugDisplaySphere_6(bool value)
	{
		___debugDisplaySphere_6 = value;
	}

	inline static int32_t get_offset_of_debugDisplayTargetPosition_7() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t2880395387, ___debugDisplayTargetPosition_7)); }
	inline bool get_debugDisplayTargetPosition_7() const { return ___debugDisplayTargetPosition_7; }
	inline bool* get_address_of_debugDisplayTargetPosition_7() { return &___debugDisplayTargetPosition_7; }
	inline void set_debugDisplayTargetPosition_7(bool value)
	{
		___debugDisplayTargetPosition_7 = value;
	}

	inline static int32_t get_offset_of_targetPosition_8() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t2880395387, ___targetPosition_8)); }
	inline Vector3_t3722313464  get_targetPosition_8() const { return ___targetPosition_8; }
	inline Vector3_t3722313464 * get_address_of_targetPosition_8() { return &___targetPosition_8; }
	inline void set_targetPosition_8(Vector3_t3722313464  value)
	{
		___targetPosition_8 = value;
	}

	inline static int32_t get_offset_of_optimalPosition_9() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t2880395387, ___optimalPosition_9)); }
	inline Vector3_t3722313464  get_optimalPosition_9() const { return ___optimalPosition_9; }
	inline Vector3_t3722313464 * get_address_of_optimalPosition_9() { return &___optimalPosition_9; }
	inline void set_optimalPosition_9(Vector3_t3722313464  value)
	{
		___optimalPosition_9 = value;
	}

	inline static int32_t get_offset_of_initialDistanceToCamera_10() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t2880395387, ___initialDistanceToCamera_10)); }
	inline float get_initialDistanceToCamera_10() const { return ___initialDistanceToCamera_10; }
	inline float* get_address_of_initialDistanceToCamera_10() { return &___initialDistanceToCamera_10; }
	inline void set_initialDistanceToCamera_10(float value)
	{
		___initialDistanceToCamera_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPHEREBASEDTAGALONG_T2880395387_H
#ifndef PROFILEBUTTONBASE_1_T3611612999_H
#define PROFILEBUTTONBASE_1_T3611612999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ProfileButtonBase`1<HoloToolkit.Unity.Buttons.ButtonTextProfile>
struct  ProfileButtonBase_1_t3611612999  : public MonoBehaviour_t3962482529
{
public:
	// T HoloToolkit.Unity.Buttons.ProfileButtonBase`1::Profile
	ButtonTextProfile_t3615608975 * ___Profile_2;

public:
	inline static int32_t get_offset_of_Profile_2() { return static_cast<int32_t>(offsetof(ProfileButtonBase_1_t3611612999, ___Profile_2)); }
	inline ButtonTextProfile_t3615608975 * get_Profile_2() const { return ___Profile_2; }
	inline ButtonTextProfile_t3615608975 ** get_address_of_Profile_2() { return &___Profile_2; }
	inline void set_Profile_2(ButtonTextProfile_t3615608975 * value)
	{
		___Profile_2 = value;
		Il2CppCodeGenWriteBarrier((&___Profile_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEBUTTONBASE_1_T3611612999_H
#ifndef PROFILEBUTTONBASE_1_T3422457020_H
#define PROFILEBUTTONBASE_1_T3422457020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ProfileButtonBase`1<HoloToolkit.Unity.Buttons.ButtonSoundProfile>
struct  ProfileButtonBase_1_t3422457020  : public MonoBehaviour_t3962482529
{
public:
	// T HoloToolkit.Unity.Buttons.ProfileButtonBase`1::Profile
	ButtonSoundProfile_t3426452996 * ___Profile_2;

public:
	inline static int32_t get_offset_of_Profile_2() { return static_cast<int32_t>(offsetof(ProfileButtonBase_1_t3422457020, ___Profile_2)); }
	inline ButtonSoundProfile_t3426452996 * get_Profile_2() const { return ___Profile_2; }
	inline ButtonSoundProfile_t3426452996 ** get_address_of_Profile_2() { return &___Profile_2; }
	inline void set_Profile_2(ButtonSoundProfile_t3426452996 * value)
	{
		___Profile_2 = value;
		Il2CppCodeGenWriteBarrier((&___Profile_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEBUTTONBASE_1_T3422457020_H
#ifndef OBJECTCOLLECTION_T2218954654_H
#define OBJECTCOLLECTION_T2218954654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.ObjectCollection
struct  ObjectCollection_t2218954654  : public MonoBehaviour_t3962482529
{
public:
	// System.Action`1<HoloToolkit.Unity.Collections.ObjectCollection> HoloToolkit.Unity.Collections.ObjectCollection::OnCollectionUpdated
	Action_1_t2391422249 * ___OnCollectionUpdated_2;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.Collections.CollectionNode> HoloToolkit.Unity.Collections.ObjectCollection::NodeList
	List_1_t3805184488 * ___NodeList_3;
	// HoloToolkit.Unity.Collections.SurfaceTypeEnum HoloToolkit.Unity.Collections.ObjectCollection::SurfaceType
	int32_t ___SurfaceType_4;
	// HoloToolkit.Unity.Collections.SortTypeEnum HoloToolkit.Unity.Collections.ObjectCollection::SortType
	int32_t ___SortType_5;
	// HoloToolkit.Unity.Collections.OrientTypeEnum HoloToolkit.Unity.Collections.ObjectCollection::OrientType
	int32_t ___OrientType_6;
	// HoloToolkit.Unity.Collections.LayoutTypeEnum HoloToolkit.Unity.Collections.ObjectCollection::LayoutType
	int32_t ___LayoutType_7;
	// System.Boolean HoloToolkit.Unity.Collections.ObjectCollection::IgnoreInactiveTransforms
	bool ___IgnoreInactiveTransforms_8;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::Radius
	float ___Radius_9;
	// System.Int32 HoloToolkit.Unity.Collections.ObjectCollection::Rows
	int32_t ___Rows_10;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::CellWidth
	float ___CellWidth_11;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::CellHeight
	float ___CellHeight_12;
	// UnityEngine.Mesh HoloToolkit.Unity.Collections.ObjectCollection::SphereMesh
	Mesh_t3648964284 * ___SphereMesh_13;
	// UnityEngine.Mesh HoloToolkit.Unity.Collections.ObjectCollection::CylinderMesh
	Mesh_t3648964284 * ___CylinderMesh_14;
	// System.Int32 HoloToolkit.Unity.Collections.ObjectCollection::_columns
	int32_t ____columns_15;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::_width
	float ____width_16;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::_height
	float ____height_17;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::_circumference
	float ____circumference_18;
	// UnityEngine.Vector2 HoloToolkit.Unity.Collections.ObjectCollection::_halfCell
	Vector2_t2156229523  ____halfCell_19;

public:
	inline static int32_t get_offset_of_OnCollectionUpdated_2() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ___OnCollectionUpdated_2)); }
	inline Action_1_t2391422249 * get_OnCollectionUpdated_2() const { return ___OnCollectionUpdated_2; }
	inline Action_1_t2391422249 ** get_address_of_OnCollectionUpdated_2() { return &___OnCollectionUpdated_2; }
	inline void set_OnCollectionUpdated_2(Action_1_t2391422249 * value)
	{
		___OnCollectionUpdated_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnCollectionUpdated_2), value);
	}

	inline static int32_t get_offset_of_NodeList_3() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ___NodeList_3)); }
	inline List_1_t3805184488 * get_NodeList_3() const { return ___NodeList_3; }
	inline List_1_t3805184488 ** get_address_of_NodeList_3() { return &___NodeList_3; }
	inline void set_NodeList_3(List_1_t3805184488 * value)
	{
		___NodeList_3 = value;
		Il2CppCodeGenWriteBarrier((&___NodeList_3), value);
	}

	inline static int32_t get_offset_of_SurfaceType_4() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ___SurfaceType_4)); }
	inline int32_t get_SurfaceType_4() const { return ___SurfaceType_4; }
	inline int32_t* get_address_of_SurfaceType_4() { return &___SurfaceType_4; }
	inline void set_SurfaceType_4(int32_t value)
	{
		___SurfaceType_4 = value;
	}

	inline static int32_t get_offset_of_SortType_5() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ___SortType_5)); }
	inline int32_t get_SortType_5() const { return ___SortType_5; }
	inline int32_t* get_address_of_SortType_5() { return &___SortType_5; }
	inline void set_SortType_5(int32_t value)
	{
		___SortType_5 = value;
	}

	inline static int32_t get_offset_of_OrientType_6() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ___OrientType_6)); }
	inline int32_t get_OrientType_6() const { return ___OrientType_6; }
	inline int32_t* get_address_of_OrientType_6() { return &___OrientType_6; }
	inline void set_OrientType_6(int32_t value)
	{
		___OrientType_6 = value;
	}

	inline static int32_t get_offset_of_LayoutType_7() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ___LayoutType_7)); }
	inline int32_t get_LayoutType_7() const { return ___LayoutType_7; }
	inline int32_t* get_address_of_LayoutType_7() { return &___LayoutType_7; }
	inline void set_LayoutType_7(int32_t value)
	{
		___LayoutType_7 = value;
	}

	inline static int32_t get_offset_of_IgnoreInactiveTransforms_8() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ___IgnoreInactiveTransforms_8)); }
	inline bool get_IgnoreInactiveTransforms_8() const { return ___IgnoreInactiveTransforms_8; }
	inline bool* get_address_of_IgnoreInactiveTransforms_8() { return &___IgnoreInactiveTransforms_8; }
	inline void set_IgnoreInactiveTransforms_8(bool value)
	{
		___IgnoreInactiveTransforms_8 = value;
	}

	inline static int32_t get_offset_of_Radius_9() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ___Radius_9)); }
	inline float get_Radius_9() const { return ___Radius_9; }
	inline float* get_address_of_Radius_9() { return &___Radius_9; }
	inline void set_Radius_9(float value)
	{
		___Radius_9 = value;
	}

	inline static int32_t get_offset_of_Rows_10() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ___Rows_10)); }
	inline int32_t get_Rows_10() const { return ___Rows_10; }
	inline int32_t* get_address_of_Rows_10() { return &___Rows_10; }
	inline void set_Rows_10(int32_t value)
	{
		___Rows_10 = value;
	}

	inline static int32_t get_offset_of_CellWidth_11() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ___CellWidth_11)); }
	inline float get_CellWidth_11() const { return ___CellWidth_11; }
	inline float* get_address_of_CellWidth_11() { return &___CellWidth_11; }
	inline void set_CellWidth_11(float value)
	{
		___CellWidth_11 = value;
	}

	inline static int32_t get_offset_of_CellHeight_12() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ___CellHeight_12)); }
	inline float get_CellHeight_12() const { return ___CellHeight_12; }
	inline float* get_address_of_CellHeight_12() { return &___CellHeight_12; }
	inline void set_CellHeight_12(float value)
	{
		___CellHeight_12 = value;
	}

	inline static int32_t get_offset_of_SphereMesh_13() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ___SphereMesh_13)); }
	inline Mesh_t3648964284 * get_SphereMesh_13() const { return ___SphereMesh_13; }
	inline Mesh_t3648964284 ** get_address_of_SphereMesh_13() { return &___SphereMesh_13; }
	inline void set_SphereMesh_13(Mesh_t3648964284 * value)
	{
		___SphereMesh_13 = value;
		Il2CppCodeGenWriteBarrier((&___SphereMesh_13), value);
	}

	inline static int32_t get_offset_of_CylinderMesh_14() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ___CylinderMesh_14)); }
	inline Mesh_t3648964284 * get_CylinderMesh_14() const { return ___CylinderMesh_14; }
	inline Mesh_t3648964284 ** get_address_of_CylinderMesh_14() { return &___CylinderMesh_14; }
	inline void set_CylinderMesh_14(Mesh_t3648964284 * value)
	{
		___CylinderMesh_14 = value;
		Il2CppCodeGenWriteBarrier((&___CylinderMesh_14), value);
	}

	inline static int32_t get_offset_of__columns_15() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ____columns_15)); }
	inline int32_t get__columns_15() const { return ____columns_15; }
	inline int32_t* get_address_of__columns_15() { return &____columns_15; }
	inline void set__columns_15(int32_t value)
	{
		____columns_15 = value;
	}

	inline static int32_t get_offset_of__width_16() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ____width_16)); }
	inline float get__width_16() const { return ____width_16; }
	inline float* get_address_of__width_16() { return &____width_16; }
	inline void set__width_16(float value)
	{
		____width_16 = value;
	}

	inline static int32_t get_offset_of__height_17() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ____height_17)); }
	inline float get__height_17() const { return ____height_17; }
	inline float* get_address_of__height_17() { return &____height_17; }
	inline void set__height_17(float value)
	{
		____height_17 = value;
	}

	inline static int32_t get_offset_of__circumference_18() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ____circumference_18)); }
	inline float get__circumference_18() const { return ____circumference_18; }
	inline float* get_address_of__circumference_18() { return &____circumference_18; }
	inline void set__circumference_18(float value)
	{
		____circumference_18 = value;
	}

	inline static int32_t get_offset_of__halfCell_19() { return static_cast<int32_t>(offsetof(ObjectCollection_t2218954654, ____halfCell_19)); }
	inline Vector2_t2156229523  get__halfCell_19() const { return ____halfCell_19; }
	inline Vector2_t2156229523 * get_address_of__halfCell_19() { return &____halfCell_19; }
	inline void set__halfCell_19(Vector2_t2156229523  value)
	{
		____halfCell_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCOLLECTION_T2218954654_H
#ifndef LINEBASE_T3631573702_H
#define LINEBASE_T3631573702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.LineBase
struct  LineBase_t3631573702  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HoloToolkit.Unity.UX.LineBase::LineStartClamp
	float ___LineStartClamp_3;
	// System.Single HoloToolkit.Unity.UX.LineBase::LineEndClamp
	float ___LineEndClamp_4;
	// HoloToolkit.Unity.UX.RotationTypeEnum HoloToolkit.Unity.UX.LineBase::RotationType
	int32_t ___RotationType_5;
	// System.Boolean HoloToolkit.Unity.UX.LineBase::FlipUpVector
	bool ___FlipUpVector_6;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.LineBase::OriginOffset
	Vector3_t3722313464  ___OriginOffset_7;
	// System.Single HoloToolkit.Unity.UX.LineBase::ManualUpVectorBlend
	float ___ManualUpVectorBlend_8;
	// UnityEngine.Vector3[] HoloToolkit.Unity.UX.LineBase::ManualUpVectors
	Vector3U5BU5D_t1718750761* ___ManualUpVectors_9;
	// System.Single HoloToolkit.Unity.UX.LineBase::VelocitySearchRange
	float ___VelocitySearchRange_10;
	// System.Single HoloToolkit.Unity.UX.LineBase::VelocityBlend
	float ___VelocityBlend_11;
	// HoloToolkit.Unity.UX.DistortionTypeEnum HoloToolkit.Unity.UX.LineBase::DistortionType
	int32_t ___DistortionType_12;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.LineBase::DistortionStrength
	AnimationCurve_t3046754366 * ___DistortionStrength_13;
	// System.Single HoloToolkit.Unity.UX.LineBase::UniformDistortionStrength
	float ___UniformDistortionStrength_14;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.UX.Distorter> HoloToolkit.Unity.UX.LineBase::distorters
	List_1_t961122480 * ___distorters_15;
	// System.Boolean HoloToolkit.Unity.UX.LineBase::loops
	bool ___loops_16;

public:
	inline static int32_t get_offset_of_LineStartClamp_3() { return static_cast<int32_t>(offsetof(LineBase_t3631573702, ___LineStartClamp_3)); }
	inline float get_LineStartClamp_3() const { return ___LineStartClamp_3; }
	inline float* get_address_of_LineStartClamp_3() { return &___LineStartClamp_3; }
	inline void set_LineStartClamp_3(float value)
	{
		___LineStartClamp_3 = value;
	}

	inline static int32_t get_offset_of_LineEndClamp_4() { return static_cast<int32_t>(offsetof(LineBase_t3631573702, ___LineEndClamp_4)); }
	inline float get_LineEndClamp_4() const { return ___LineEndClamp_4; }
	inline float* get_address_of_LineEndClamp_4() { return &___LineEndClamp_4; }
	inline void set_LineEndClamp_4(float value)
	{
		___LineEndClamp_4 = value;
	}

	inline static int32_t get_offset_of_RotationType_5() { return static_cast<int32_t>(offsetof(LineBase_t3631573702, ___RotationType_5)); }
	inline int32_t get_RotationType_5() const { return ___RotationType_5; }
	inline int32_t* get_address_of_RotationType_5() { return &___RotationType_5; }
	inline void set_RotationType_5(int32_t value)
	{
		___RotationType_5 = value;
	}

	inline static int32_t get_offset_of_FlipUpVector_6() { return static_cast<int32_t>(offsetof(LineBase_t3631573702, ___FlipUpVector_6)); }
	inline bool get_FlipUpVector_6() const { return ___FlipUpVector_6; }
	inline bool* get_address_of_FlipUpVector_6() { return &___FlipUpVector_6; }
	inline void set_FlipUpVector_6(bool value)
	{
		___FlipUpVector_6 = value;
	}

	inline static int32_t get_offset_of_OriginOffset_7() { return static_cast<int32_t>(offsetof(LineBase_t3631573702, ___OriginOffset_7)); }
	inline Vector3_t3722313464  get_OriginOffset_7() const { return ___OriginOffset_7; }
	inline Vector3_t3722313464 * get_address_of_OriginOffset_7() { return &___OriginOffset_7; }
	inline void set_OriginOffset_7(Vector3_t3722313464  value)
	{
		___OriginOffset_7 = value;
	}

	inline static int32_t get_offset_of_ManualUpVectorBlend_8() { return static_cast<int32_t>(offsetof(LineBase_t3631573702, ___ManualUpVectorBlend_8)); }
	inline float get_ManualUpVectorBlend_8() const { return ___ManualUpVectorBlend_8; }
	inline float* get_address_of_ManualUpVectorBlend_8() { return &___ManualUpVectorBlend_8; }
	inline void set_ManualUpVectorBlend_8(float value)
	{
		___ManualUpVectorBlend_8 = value;
	}

	inline static int32_t get_offset_of_ManualUpVectors_9() { return static_cast<int32_t>(offsetof(LineBase_t3631573702, ___ManualUpVectors_9)); }
	inline Vector3U5BU5D_t1718750761* get_ManualUpVectors_9() const { return ___ManualUpVectors_9; }
	inline Vector3U5BU5D_t1718750761** get_address_of_ManualUpVectors_9() { return &___ManualUpVectors_9; }
	inline void set_ManualUpVectors_9(Vector3U5BU5D_t1718750761* value)
	{
		___ManualUpVectors_9 = value;
		Il2CppCodeGenWriteBarrier((&___ManualUpVectors_9), value);
	}

	inline static int32_t get_offset_of_VelocitySearchRange_10() { return static_cast<int32_t>(offsetof(LineBase_t3631573702, ___VelocitySearchRange_10)); }
	inline float get_VelocitySearchRange_10() const { return ___VelocitySearchRange_10; }
	inline float* get_address_of_VelocitySearchRange_10() { return &___VelocitySearchRange_10; }
	inline void set_VelocitySearchRange_10(float value)
	{
		___VelocitySearchRange_10 = value;
	}

	inline static int32_t get_offset_of_VelocityBlend_11() { return static_cast<int32_t>(offsetof(LineBase_t3631573702, ___VelocityBlend_11)); }
	inline float get_VelocityBlend_11() const { return ___VelocityBlend_11; }
	inline float* get_address_of_VelocityBlend_11() { return &___VelocityBlend_11; }
	inline void set_VelocityBlend_11(float value)
	{
		___VelocityBlend_11 = value;
	}

	inline static int32_t get_offset_of_DistortionType_12() { return static_cast<int32_t>(offsetof(LineBase_t3631573702, ___DistortionType_12)); }
	inline int32_t get_DistortionType_12() const { return ___DistortionType_12; }
	inline int32_t* get_address_of_DistortionType_12() { return &___DistortionType_12; }
	inline void set_DistortionType_12(int32_t value)
	{
		___DistortionType_12 = value;
	}

	inline static int32_t get_offset_of_DistortionStrength_13() { return static_cast<int32_t>(offsetof(LineBase_t3631573702, ___DistortionStrength_13)); }
	inline AnimationCurve_t3046754366 * get_DistortionStrength_13() const { return ___DistortionStrength_13; }
	inline AnimationCurve_t3046754366 ** get_address_of_DistortionStrength_13() { return &___DistortionStrength_13; }
	inline void set_DistortionStrength_13(AnimationCurve_t3046754366 * value)
	{
		___DistortionStrength_13 = value;
		Il2CppCodeGenWriteBarrier((&___DistortionStrength_13), value);
	}

	inline static int32_t get_offset_of_UniformDistortionStrength_14() { return static_cast<int32_t>(offsetof(LineBase_t3631573702, ___UniformDistortionStrength_14)); }
	inline float get_UniformDistortionStrength_14() const { return ___UniformDistortionStrength_14; }
	inline float* get_address_of_UniformDistortionStrength_14() { return &___UniformDistortionStrength_14; }
	inline void set_UniformDistortionStrength_14(float value)
	{
		___UniformDistortionStrength_14 = value;
	}

	inline static int32_t get_offset_of_distorters_15() { return static_cast<int32_t>(offsetof(LineBase_t3631573702, ___distorters_15)); }
	inline List_1_t961122480 * get_distorters_15() const { return ___distorters_15; }
	inline List_1_t961122480 ** get_address_of_distorters_15() { return &___distorters_15; }
	inline void set_distorters_15(List_1_t961122480 * value)
	{
		___distorters_15 = value;
		Il2CppCodeGenWriteBarrier((&___distorters_15), value);
	}

	inline static int32_t get_offset_of_loops_16() { return static_cast<int32_t>(offsetof(LineBase_t3631573702, ___loops_16)); }
	inline bool get_loops_16() const { return ___loops_16; }
	inline bool* get_address_of_loops_16() { return &___loops_16; }
	inline void set_loops_16(bool value)
	{
		___loops_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEBASE_T3631573702_H
#ifndef BOUNDINGBOX_T212658891_H
#define BOUNDINGBOX_T212658891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBox
struct  BoundingBox_t212658891  : public MonoBehaviour_t3962482529
{
public:
	// System.Action HoloToolkit.Unity.UX.BoundingBox::OnFlattenedAxisChange
	Action_t1264377477 * ___OnFlattenedAxisChange_2;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.BoundingBox::target
	GameObject_t1113636619 * ___target_3;
	// UnityEngine.Transform HoloToolkit.Unity.UX.BoundingBox::scaleTransform
	Transform_t3600365921 * ___scaleTransform_4;
	// HoloToolkit.Unity.UX.BoundingBox/FlattenModeEnum HoloToolkit.Unity.UX.BoundingBox::flattenPreference
	int32_t ___flattenPreference_5;
	// System.Single HoloToolkit.Unity.UX.BoundingBox::flattenAxisThreshold
	float ___flattenAxisThreshold_6;
	// System.Single HoloToolkit.Unity.UX.BoundingBox::flattenedAxisThickness
	float ___flattenedAxisThickness_7;
	// System.Single HoloToolkit.Unity.UX.BoundingBox::scalePadding
	float ___scalePadding_8;
	// System.Single HoloToolkit.Unity.UX.BoundingBox::flattenedScalePadding
	float ___flattenedScalePadding_9;
	// HoloToolkit.Unity.UX.BoundingBox/BoundsCalculationMethodEnum HoloToolkit.Unity.UX.BoundingBox::boundsCalculationMethod
	int32_t ___boundsCalculationMethod_10;
	// UnityEngine.LayerMask HoloToolkit.Unity.UX.BoundingBox::ignoreLayers
	LayerMask_t3493934918  ___ignoreLayers_11;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.BoundingBox::targetBoundsWorldCenter
	Vector3_t3722313464  ___targetBoundsWorldCenter_12;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.BoundingBox::targetBoundsLocalScale
	Vector3_t3722313464  ___targetBoundsLocalScale_13;
	// UnityEngine.Bounds HoloToolkit.Unity.UX.BoundingBox::localTargetBounds
	Bounds_t2266837910  ___localTargetBounds_14;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> HoloToolkit.Unity.UX.BoundingBox::boundsPoints
	List_1_t899420910 * ___boundsPoints_15;
	// HoloToolkit.Unity.UX.BoundingBox/FlattenModeEnum HoloToolkit.Unity.UX.BoundingBox::flattenedAxis
	int32_t ___flattenedAxis_16;

public:
	inline static int32_t get_offset_of_OnFlattenedAxisChange_2() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891, ___OnFlattenedAxisChange_2)); }
	inline Action_t1264377477 * get_OnFlattenedAxisChange_2() const { return ___OnFlattenedAxisChange_2; }
	inline Action_t1264377477 ** get_address_of_OnFlattenedAxisChange_2() { return &___OnFlattenedAxisChange_2; }
	inline void set_OnFlattenedAxisChange_2(Action_t1264377477 * value)
	{
		___OnFlattenedAxisChange_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnFlattenedAxisChange_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891, ___target_3)); }
	inline GameObject_t1113636619 * get_target_3() const { return ___target_3; }
	inline GameObject_t1113636619 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(GameObject_t1113636619 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_scaleTransform_4() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891, ___scaleTransform_4)); }
	inline Transform_t3600365921 * get_scaleTransform_4() const { return ___scaleTransform_4; }
	inline Transform_t3600365921 ** get_address_of_scaleTransform_4() { return &___scaleTransform_4; }
	inline void set_scaleTransform_4(Transform_t3600365921 * value)
	{
		___scaleTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___scaleTransform_4), value);
	}

	inline static int32_t get_offset_of_flattenPreference_5() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891, ___flattenPreference_5)); }
	inline int32_t get_flattenPreference_5() const { return ___flattenPreference_5; }
	inline int32_t* get_address_of_flattenPreference_5() { return &___flattenPreference_5; }
	inline void set_flattenPreference_5(int32_t value)
	{
		___flattenPreference_5 = value;
	}

	inline static int32_t get_offset_of_flattenAxisThreshold_6() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891, ___flattenAxisThreshold_6)); }
	inline float get_flattenAxisThreshold_6() const { return ___flattenAxisThreshold_6; }
	inline float* get_address_of_flattenAxisThreshold_6() { return &___flattenAxisThreshold_6; }
	inline void set_flattenAxisThreshold_6(float value)
	{
		___flattenAxisThreshold_6 = value;
	}

	inline static int32_t get_offset_of_flattenedAxisThickness_7() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891, ___flattenedAxisThickness_7)); }
	inline float get_flattenedAxisThickness_7() const { return ___flattenedAxisThickness_7; }
	inline float* get_address_of_flattenedAxisThickness_7() { return &___flattenedAxisThickness_7; }
	inline void set_flattenedAxisThickness_7(float value)
	{
		___flattenedAxisThickness_7 = value;
	}

	inline static int32_t get_offset_of_scalePadding_8() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891, ___scalePadding_8)); }
	inline float get_scalePadding_8() const { return ___scalePadding_8; }
	inline float* get_address_of_scalePadding_8() { return &___scalePadding_8; }
	inline void set_scalePadding_8(float value)
	{
		___scalePadding_8 = value;
	}

	inline static int32_t get_offset_of_flattenedScalePadding_9() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891, ___flattenedScalePadding_9)); }
	inline float get_flattenedScalePadding_9() const { return ___flattenedScalePadding_9; }
	inline float* get_address_of_flattenedScalePadding_9() { return &___flattenedScalePadding_9; }
	inline void set_flattenedScalePadding_9(float value)
	{
		___flattenedScalePadding_9 = value;
	}

	inline static int32_t get_offset_of_boundsCalculationMethod_10() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891, ___boundsCalculationMethod_10)); }
	inline int32_t get_boundsCalculationMethod_10() const { return ___boundsCalculationMethod_10; }
	inline int32_t* get_address_of_boundsCalculationMethod_10() { return &___boundsCalculationMethod_10; }
	inline void set_boundsCalculationMethod_10(int32_t value)
	{
		___boundsCalculationMethod_10 = value;
	}

	inline static int32_t get_offset_of_ignoreLayers_11() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891, ___ignoreLayers_11)); }
	inline LayerMask_t3493934918  get_ignoreLayers_11() const { return ___ignoreLayers_11; }
	inline LayerMask_t3493934918 * get_address_of_ignoreLayers_11() { return &___ignoreLayers_11; }
	inline void set_ignoreLayers_11(LayerMask_t3493934918  value)
	{
		___ignoreLayers_11 = value;
	}

	inline static int32_t get_offset_of_targetBoundsWorldCenter_12() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891, ___targetBoundsWorldCenter_12)); }
	inline Vector3_t3722313464  get_targetBoundsWorldCenter_12() const { return ___targetBoundsWorldCenter_12; }
	inline Vector3_t3722313464 * get_address_of_targetBoundsWorldCenter_12() { return &___targetBoundsWorldCenter_12; }
	inline void set_targetBoundsWorldCenter_12(Vector3_t3722313464  value)
	{
		___targetBoundsWorldCenter_12 = value;
	}

	inline static int32_t get_offset_of_targetBoundsLocalScale_13() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891, ___targetBoundsLocalScale_13)); }
	inline Vector3_t3722313464  get_targetBoundsLocalScale_13() const { return ___targetBoundsLocalScale_13; }
	inline Vector3_t3722313464 * get_address_of_targetBoundsLocalScale_13() { return &___targetBoundsLocalScale_13; }
	inline void set_targetBoundsLocalScale_13(Vector3_t3722313464  value)
	{
		___targetBoundsLocalScale_13 = value;
	}

	inline static int32_t get_offset_of_localTargetBounds_14() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891, ___localTargetBounds_14)); }
	inline Bounds_t2266837910  get_localTargetBounds_14() const { return ___localTargetBounds_14; }
	inline Bounds_t2266837910 * get_address_of_localTargetBounds_14() { return &___localTargetBounds_14; }
	inline void set_localTargetBounds_14(Bounds_t2266837910  value)
	{
		___localTargetBounds_14 = value;
	}

	inline static int32_t get_offset_of_boundsPoints_15() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891, ___boundsPoints_15)); }
	inline List_1_t899420910 * get_boundsPoints_15() const { return ___boundsPoints_15; }
	inline List_1_t899420910 ** get_address_of_boundsPoints_15() { return &___boundsPoints_15; }
	inline void set_boundsPoints_15(List_1_t899420910 * value)
	{
		___boundsPoints_15 = value;
		Il2CppCodeGenWriteBarrier((&___boundsPoints_15), value);
	}

	inline static int32_t get_offset_of_flattenedAxis_16() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891, ___flattenedAxis_16)); }
	inline int32_t get_flattenedAxis_16() const { return ___flattenedAxis_16; }
	inline int32_t* get_address_of_flattenedAxis_16() { return &___flattenedAxis_16; }
	inline void set_flattenedAxis_16(int32_t value)
	{
		___flattenedAxis_16 = value;
	}
};

struct BoundingBox_t212658891_StaticFields
{
public:
	// UnityEngine.Vector3[] HoloToolkit.Unity.UX.BoundingBox::corners
	Vector3U5BU5D_t1718750761* ___corners_17;
	// UnityEngine.Vector3[] HoloToolkit.Unity.UX.BoundingBox::rectTransformCorners
	Vector3U5BU5D_t1718750761* ___rectTransformCorners_18;

public:
	inline static int32_t get_offset_of_corners_17() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891_StaticFields, ___corners_17)); }
	inline Vector3U5BU5D_t1718750761* get_corners_17() const { return ___corners_17; }
	inline Vector3U5BU5D_t1718750761** get_address_of_corners_17() { return &___corners_17; }
	inline void set_corners_17(Vector3U5BU5D_t1718750761* value)
	{
		___corners_17 = value;
		Il2CppCodeGenWriteBarrier((&___corners_17), value);
	}

	inline static int32_t get_offset_of_rectTransformCorners_18() { return static_cast<int32_t>(offsetof(BoundingBox_t212658891_StaticFields, ___rectTransformCorners_18)); }
	inline Vector3U5BU5D_t1718750761* get_rectTransformCorners_18() const { return ___rectTransformCorners_18; }
	inline Vector3U5BU5D_t1718750761** get_address_of_rectTransformCorners_18() { return &___rectTransformCorners_18; }
	inline void set_rectTransformCorners_18(Vector3U5BU5D_t1718750761* value)
	{
		___rectTransformCorners_18 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransformCorners_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOX_T212658891_H
#ifndef COMPOUNDBUTTONSPEECH_T1131825139_H
#define COMPOUNDBUTTONSPEECH_T1131825139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonSpeech
struct  CompoundButtonSpeech_t1131825139  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Unity.Buttons.CompoundButtonSpeech/KeywordSourceEnum HoloToolkit.Unity.Buttons.CompoundButtonSpeech::KeywordSource
	int32_t ___KeywordSource_2;
	// System.String HoloToolkit.Unity.Buttons.CompoundButtonSpeech::Keyword
	String_t* ___Keyword_3;
	// System.String HoloToolkit.Unity.Buttons.CompoundButtonSpeech::prevButtonText
	String_t* ___prevButtonText_4;
	// System.String HoloToolkit.Unity.Buttons.CompoundButtonSpeech::keyWord
	String_t* ___keyWord_5;
	// HoloToolkit.Unity.Buttons.CompoundButton HoloToolkit.Unity.Buttons.CompoundButtonSpeech::m_button
	CompoundButton_t147810562 * ___m_button_6;
	// HoloToolkit.Unity.Buttons.CompoundButtonText HoloToolkit.Unity.Buttons.CompoundButtonSpeech::m_button_text
	CompoundButtonText_t2169052765 * ___m_button_text_7;

public:
	inline static int32_t get_offset_of_KeywordSource_2() { return static_cast<int32_t>(offsetof(CompoundButtonSpeech_t1131825139, ___KeywordSource_2)); }
	inline int32_t get_KeywordSource_2() const { return ___KeywordSource_2; }
	inline int32_t* get_address_of_KeywordSource_2() { return &___KeywordSource_2; }
	inline void set_KeywordSource_2(int32_t value)
	{
		___KeywordSource_2 = value;
	}

	inline static int32_t get_offset_of_Keyword_3() { return static_cast<int32_t>(offsetof(CompoundButtonSpeech_t1131825139, ___Keyword_3)); }
	inline String_t* get_Keyword_3() const { return ___Keyword_3; }
	inline String_t** get_address_of_Keyword_3() { return &___Keyword_3; }
	inline void set_Keyword_3(String_t* value)
	{
		___Keyword_3 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_3), value);
	}

	inline static int32_t get_offset_of_prevButtonText_4() { return static_cast<int32_t>(offsetof(CompoundButtonSpeech_t1131825139, ___prevButtonText_4)); }
	inline String_t* get_prevButtonText_4() const { return ___prevButtonText_4; }
	inline String_t** get_address_of_prevButtonText_4() { return &___prevButtonText_4; }
	inline void set_prevButtonText_4(String_t* value)
	{
		___prevButtonText_4 = value;
		Il2CppCodeGenWriteBarrier((&___prevButtonText_4), value);
	}

	inline static int32_t get_offset_of_keyWord_5() { return static_cast<int32_t>(offsetof(CompoundButtonSpeech_t1131825139, ___keyWord_5)); }
	inline String_t* get_keyWord_5() const { return ___keyWord_5; }
	inline String_t** get_address_of_keyWord_5() { return &___keyWord_5; }
	inline void set_keyWord_5(String_t* value)
	{
		___keyWord_5 = value;
		Il2CppCodeGenWriteBarrier((&___keyWord_5), value);
	}

	inline static int32_t get_offset_of_m_button_6() { return static_cast<int32_t>(offsetof(CompoundButtonSpeech_t1131825139, ___m_button_6)); }
	inline CompoundButton_t147810562 * get_m_button_6() const { return ___m_button_6; }
	inline CompoundButton_t147810562 ** get_address_of_m_button_6() { return &___m_button_6; }
	inline void set_m_button_6(CompoundButton_t147810562 * value)
	{
		___m_button_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_button_6), value);
	}

	inline static int32_t get_offset_of_m_button_text_7() { return static_cast<int32_t>(offsetof(CompoundButtonSpeech_t1131825139, ___m_button_text_7)); }
	inline CompoundButtonText_t2169052765 * get_m_button_text_7() const { return ___m_button_text_7; }
	inline CompoundButtonText_t2169052765 ** get_address_of_m_button_text_7() { return &___m_button_text_7; }
	inline void set_m_button_text_7(CompoundButtonText_t2169052765 * value)
	{
		___m_button_text_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_button_text_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTONSPEECH_T1131825139_H
#ifndef BOUNDINGBOXGIZMO_T584836069_H
#define BOUNDINGBOXGIZMO_T584836069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBoxGizmo
struct  BoundingBoxGizmo_t584836069  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Unity.UX.BoundingBox HoloToolkit.Unity.UX.BoundingBoxGizmo::boundingBox
	BoundingBox_t212658891 * ___boundingBox_2;

public:
	inline static int32_t get_offset_of_boundingBox_2() { return static_cast<int32_t>(offsetof(BoundingBoxGizmo_t584836069, ___boundingBox_2)); }
	inline BoundingBox_t212658891 * get_boundingBox_2() const { return ___boundingBox_2; }
	inline BoundingBox_t212658891 ** get_address_of_boundingBox_2() { return &___boundingBox_2; }
	inline void set_boundingBox_2(BoundingBox_t212658891 * value)
	{
		___boundingBox_2 = value;
		Il2CppCodeGenWriteBarrier((&___boundingBox_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXGIZMO_T584836069_H
#ifndef DISTORTER_T3784015034_H
#define DISTORTER_T3784015034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Distorter
struct  Distorter_t3784015034  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 HoloToolkit.Unity.UX.Distorter::distortOrder
	int32_t ___distortOrder_2;
	// System.Single HoloToolkit.Unity.UX.Distorter::distortStrength
	float ___distortStrength_3;

public:
	inline static int32_t get_offset_of_distortOrder_2() { return static_cast<int32_t>(offsetof(Distorter_t3784015034, ___distortOrder_2)); }
	inline int32_t get_distortOrder_2() const { return ___distortOrder_2; }
	inline int32_t* get_address_of_distortOrder_2() { return &___distortOrder_2; }
	inline void set_distortOrder_2(int32_t value)
	{
		___distortOrder_2 = value;
	}

	inline static int32_t get_offset_of_distortStrength_3() { return static_cast<int32_t>(offsetof(Distorter_t3784015034, ___distortStrength_3)); }
	inline float get_distortStrength_3() const { return ___distortStrength_3; }
	inline float* get_address_of_distortStrength_3() { return &___distortStrength_3; }
	inline void set_distortStrength_3(float value)
	{
		___distortStrength_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTER_T3784015034_H
#ifndef COMPOUNDBUTTONTOGGLE_T340090535_H
#define COMPOUNDBUTTONTOGGLE_T340090535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonToggle
struct  CompoundButtonToggle_t340090535  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Unity.Buttons.ToggleBehaviorEnum HoloToolkit.Unity.Buttons.CompoundButtonToggle::Behavior
	int32_t ___Behavior_2;
	// HoloToolkit.Unity.Buttons.ButtonProfile HoloToolkit.Unity.Buttons.CompoundButtonToggle::OnProfile
	ButtonProfile_t1729449105 * ___OnProfile_3;
	// HoloToolkit.Unity.Buttons.ButtonProfile HoloToolkit.Unity.Buttons.CompoundButtonToggle::OffProfile
	ButtonProfile_t1729449105 * ___OffProfile_4;
	// UnityEngine.Component HoloToolkit.Unity.Buttons.CompoundButtonToggle::Target
	Component_t1923634451 * ___Target_5;
	// HoloToolkit.Unity.Buttons.CompoundButton HoloToolkit.Unity.Buttons.CompoundButtonToggle::m_compButton
	CompoundButton_t147810562 * ___m_compButton_6;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonToggle::state
	bool ___state_7;

public:
	inline static int32_t get_offset_of_Behavior_2() { return static_cast<int32_t>(offsetof(CompoundButtonToggle_t340090535, ___Behavior_2)); }
	inline int32_t get_Behavior_2() const { return ___Behavior_2; }
	inline int32_t* get_address_of_Behavior_2() { return &___Behavior_2; }
	inline void set_Behavior_2(int32_t value)
	{
		___Behavior_2 = value;
	}

	inline static int32_t get_offset_of_OnProfile_3() { return static_cast<int32_t>(offsetof(CompoundButtonToggle_t340090535, ___OnProfile_3)); }
	inline ButtonProfile_t1729449105 * get_OnProfile_3() const { return ___OnProfile_3; }
	inline ButtonProfile_t1729449105 ** get_address_of_OnProfile_3() { return &___OnProfile_3; }
	inline void set_OnProfile_3(ButtonProfile_t1729449105 * value)
	{
		___OnProfile_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnProfile_3), value);
	}

	inline static int32_t get_offset_of_OffProfile_4() { return static_cast<int32_t>(offsetof(CompoundButtonToggle_t340090535, ___OffProfile_4)); }
	inline ButtonProfile_t1729449105 * get_OffProfile_4() const { return ___OffProfile_4; }
	inline ButtonProfile_t1729449105 ** get_address_of_OffProfile_4() { return &___OffProfile_4; }
	inline void set_OffProfile_4(ButtonProfile_t1729449105 * value)
	{
		___OffProfile_4 = value;
		Il2CppCodeGenWriteBarrier((&___OffProfile_4), value);
	}

	inline static int32_t get_offset_of_Target_5() { return static_cast<int32_t>(offsetof(CompoundButtonToggle_t340090535, ___Target_5)); }
	inline Component_t1923634451 * get_Target_5() const { return ___Target_5; }
	inline Component_t1923634451 ** get_address_of_Target_5() { return &___Target_5; }
	inline void set_Target_5(Component_t1923634451 * value)
	{
		___Target_5 = value;
		Il2CppCodeGenWriteBarrier((&___Target_5), value);
	}

	inline static int32_t get_offset_of_m_compButton_6() { return static_cast<int32_t>(offsetof(CompoundButtonToggle_t340090535, ___m_compButton_6)); }
	inline CompoundButton_t147810562 * get_m_compButton_6() const { return ___m_compButton_6; }
	inline CompoundButton_t147810562 ** get_address_of_m_compButton_6() { return &___m_compButton_6; }
	inline void set_m_compButton_6(CompoundButton_t147810562 * value)
	{
		___m_compButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_compButton_6), value);
	}

	inline static int32_t get_offset_of_state_7() { return static_cast<int32_t>(offsetof(CompoundButtonToggle_t340090535, ___state_7)); }
	inline bool get_state_7() const { return ___state_7; }
	inline bool* get_address_of_state_7() { return &___state_7; }
	inline void set_state_7(bool value)
	{
		___state_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTONTOGGLE_T340090535_H
#ifndef COMPOUNDBUTTONANIM_T700402005_H
#define COMPOUNDBUTTONANIM_T700402005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonAnim
struct  CompoundButtonAnim_t700402005  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator HoloToolkit.Unity.Buttons.CompoundButtonAnim::TargetAnimator
	Animator_t434523843 * ___TargetAnimator_2;
	// HoloToolkit.Unity.Buttons.AnimatorControllerAction[] HoloToolkit.Unity.Buttons.CompoundButtonAnim::AnimActions
	AnimatorControllerActionU5BU5D_t4171275305* ___AnimActions_3;

public:
	inline static int32_t get_offset_of_TargetAnimator_2() { return static_cast<int32_t>(offsetof(CompoundButtonAnim_t700402005, ___TargetAnimator_2)); }
	inline Animator_t434523843 * get_TargetAnimator_2() const { return ___TargetAnimator_2; }
	inline Animator_t434523843 ** get_address_of_TargetAnimator_2() { return &___TargetAnimator_2; }
	inline void set_TargetAnimator_2(Animator_t434523843 * value)
	{
		___TargetAnimator_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetAnimator_2), value);
	}

	inline static int32_t get_offset_of_AnimActions_3() { return static_cast<int32_t>(offsetof(CompoundButtonAnim_t700402005, ___AnimActions_3)); }
	inline AnimatorControllerActionU5BU5D_t4171275305* get_AnimActions_3() const { return ___AnimActions_3; }
	inline AnimatorControllerActionU5BU5D_t4171275305** get_address_of_AnimActions_3() { return &___AnimActions_3; }
	inline void set_AnimActions_3(AnimatorControllerActionU5BU5D_t4171275305* value)
	{
		___AnimActions_3 = value;
		Il2CppCodeGenWriteBarrier((&___AnimActions_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTONANIM_T700402005_H
#ifndef OBJECTCOLLECTIONDYNAMIC_T3678455523_H
#define OBJECTCOLLECTIONDYNAMIC_T3678455523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.ObjectCollectionDynamic
struct  ObjectCollectionDynamic_t3678455523  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Unity.Collections.ObjectCollectionDynamic/BehaviorEnum HoloToolkit.Unity.Collections.ObjectCollectionDynamic::Behavior
	int32_t ___Behavior_2;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.Collections.ObjectCollectionDynamic/CollectionNodeDynamic> HoloToolkit.Unity.Collections.ObjectCollectionDynamic::DynamicNodeList
	List_1_t1466063516 * ___DynamicNodeList_3;
	// HoloToolkit.Unity.Collections.ObjectCollection HoloToolkit.Unity.Collections.ObjectCollectionDynamic::collection
	ObjectCollection_t2218954654 * ___collection_4;

public:
	inline static int32_t get_offset_of_Behavior_2() { return static_cast<int32_t>(offsetof(ObjectCollectionDynamic_t3678455523, ___Behavior_2)); }
	inline int32_t get_Behavior_2() const { return ___Behavior_2; }
	inline int32_t* get_address_of_Behavior_2() { return &___Behavior_2; }
	inline void set_Behavior_2(int32_t value)
	{
		___Behavior_2 = value;
	}

	inline static int32_t get_offset_of_DynamicNodeList_3() { return static_cast<int32_t>(offsetof(ObjectCollectionDynamic_t3678455523, ___DynamicNodeList_3)); }
	inline List_1_t1466063516 * get_DynamicNodeList_3() const { return ___DynamicNodeList_3; }
	inline List_1_t1466063516 ** get_address_of_DynamicNodeList_3() { return &___DynamicNodeList_3; }
	inline void set_DynamicNodeList_3(List_1_t1466063516 * value)
	{
		___DynamicNodeList_3 = value;
		Il2CppCodeGenWriteBarrier((&___DynamicNodeList_3), value);
	}

	inline static int32_t get_offset_of_collection_4() { return static_cast<int32_t>(offsetof(ObjectCollectionDynamic_t3678455523, ___collection_4)); }
	inline ObjectCollection_t2218954654 * get_collection_4() const { return ___collection_4; }
	inline ObjectCollection_t2218954654 ** get_address_of_collection_4() { return &___collection_4; }
	inline void set_collection_4(ObjectCollection_t2218954654 * value)
	{
		___collection_4 = value;
		Il2CppCodeGenWriteBarrier((&___collection_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCOLLECTIONDYNAMIC_T3678455523_H
#ifndef DUPLICATOR_T4067483021_H
#define DUPLICATOR_T4067483021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Duplicator
struct  Duplicator_t4067483021  : public MonoBehaviour_t3962482529
{
public:
	// System.Action HoloToolkit.Unity.UX.Duplicator::OnTargetIdle
	Action_t1264377477 * ___OnTargetIdle_2;
	// System.Action HoloToolkit.Unity.UX.Duplicator::OnTargetActive
	Action_t1264377477 * ___OnTargetActive_3;
	// System.Action HoloToolkit.Unity.UX.Duplicator::OnTargetEmpty
	Action_t1264377477 * ___OnTargetEmpty_4;
	// System.Action HoloToolkit.Unity.UX.Duplicator::OnTargetDuplicateStart
	Action_t1264377477 * ___OnTargetDuplicateStart_5;
	// System.Action HoloToolkit.Unity.UX.Duplicator::OnTargetDuplicateEnd
	Action_t1264377477 * ___OnTargetDuplicateEnd_6;
	// HoloToolkit.Unity.UX.Duplicator/ActivateModeEnum HoloToolkit.Unity.UX.Duplicator::activateMode
	int32_t ___activateMode_7;
	// System.Single HoloToolkit.Unity.UX.Duplicator::autoActivateRadius
	float ___autoActivateRadius_8;
	// System.Single HoloToolkit.Unity.UX.Duplicator::removeRadius
	float ___removeRadius_9;
	// System.Single HoloToolkit.Unity.UX.Duplicator::restorePosSpeed
	float ___restorePosSpeed_10;
	// System.Single HoloToolkit.Unity.UX.Duplicator::activeTimeout
	float ___activeTimeout_11;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.Duplicator::restoreCurve
	AnimationCurve_t3046754366 * ___restoreCurve_12;
	// System.Boolean HoloToolkit.Unity.UX.Duplicator::limitDuplicates
	bool ___limitDuplicates_13;
	// System.Int32 HoloToolkit.Unity.UX.Duplicator::maxDuplicates
	int32_t ___maxDuplicates_14;
	// System.Single HoloToolkit.Unity.UX.Duplicator::duplicateSpeed
	float ___duplicateSpeed_15;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.Duplicator::duplicateGrowCurve
	AnimationCurve_t3046754366 * ___duplicateGrowCurve_16;
	// HoloToolkit.Unity.UX.Duplicator/StateEnum HoloToolkit.Unity.UX.Duplicator::state
	int32_t ___state_17;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Duplicator::targetPos
	Vector3_t3722313464  ___targetPos_18;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Duplicator::targetScale
	Vector3_t3722313464  ___targetScale_19;
	// UnityEngine.Quaternion HoloToolkit.Unity.UX.Duplicator::targetRot
	Quaternion_t2301928331  ___targetRot_20;
	// System.Int32 HoloToolkit.Unity.UX.Duplicator::numDuplicates
	int32_t ___numDuplicates_21;

public:
	inline static int32_t get_offset_of_OnTargetIdle_2() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___OnTargetIdle_2)); }
	inline Action_t1264377477 * get_OnTargetIdle_2() const { return ___OnTargetIdle_2; }
	inline Action_t1264377477 ** get_address_of_OnTargetIdle_2() { return &___OnTargetIdle_2; }
	inline void set_OnTargetIdle_2(Action_t1264377477 * value)
	{
		___OnTargetIdle_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnTargetIdle_2), value);
	}

	inline static int32_t get_offset_of_OnTargetActive_3() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___OnTargetActive_3)); }
	inline Action_t1264377477 * get_OnTargetActive_3() const { return ___OnTargetActive_3; }
	inline Action_t1264377477 ** get_address_of_OnTargetActive_3() { return &___OnTargetActive_3; }
	inline void set_OnTargetActive_3(Action_t1264377477 * value)
	{
		___OnTargetActive_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnTargetActive_3), value);
	}

	inline static int32_t get_offset_of_OnTargetEmpty_4() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___OnTargetEmpty_4)); }
	inline Action_t1264377477 * get_OnTargetEmpty_4() const { return ___OnTargetEmpty_4; }
	inline Action_t1264377477 ** get_address_of_OnTargetEmpty_4() { return &___OnTargetEmpty_4; }
	inline void set_OnTargetEmpty_4(Action_t1264377477 * value)
	{
		___OnTargetEmpty_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnTargetEmpty_4), value);
	}

	inline static int32_t get_offset_of_OnTargetDuplicateStart_5() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___OnTargetDuplicateStart_5)); }
	inline Action_t1264377477 * get_OnTargetDuplicateStart_5() const { return ___OnTargetDuplicateStart_5; }
	inline Action_t1264377477 ** get_address_of_OnTargetDuplicateStart_5() { return &___OnTargetDuplicateStart_5; }
	inline void set_OnTargetDuplicateStart_5(Action_t1264377477 * value)
	{
		___OnTargetDuplicateStart_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnTargetDuplicateStart_5), value);
	}

	inline static int32_t get_offset_of_OnTargetDuplicateEnd_6() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___OnTargetDuplicateEnd_6)); }
	inline Action_t1264377477 * get_OnTargetDuplicateEnd_6() const { return ___OnTargetDuplicateEnd_6; }
	inline Action_t1264377477 ** get_address_of_OnTargetDuplicateEnd_6() { return &___OnTargetDuplicateEnd_6; }
	inline void set_OnTargetDuplicateEnd_6(Action_t1264377477 * value)
	{
		___OnTargetDuplicateEnd_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnTargetDuplicateEnd_6), value);
	}

	inline static int32_t get_offset_of_activateMode_7() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___activateMode_7)); }
	inline int32_t get_activateMode_7() const { return ___activateMode_7; }
	inline int32_t* get_address_of_activateMode_7() { return &___activateMode_7; }
	inline void set_activateMode_7(int32_t value)
	{
		___activateMode_7 = value;
	}

	inline static int32_t get_offset_of_autoActivateRadius_8() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___autoActivateRadius_8)); }
	inline float get_autoActivateRadius_8() const { return ___autoActivateRadius_8; }
	inline float* get_address_of_autoActivateRadius_8() { return &___autoActivateRadius_8; }
	inline void set_autoActivateRadius_8(float value)
	{
		___autoActivateRadius_8 = value;
	}

	inline static int32_t get_offset_of_removeRadius_9() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___removeRadius_9)); }
	inline float get_removeRadius_9() const { return ___removeRadius_9; }
	inline float* get_address_of_removeRadius_9() { return &___removeRadius_9; }
	inline void set_removeRadius_9(float value)
	{
		___removeRadius_9 = value;
	}

	inline static int32_t get_offset_of_restorePosSpeed_10() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___restorePosSpeed_10)); }
	inline float get_restorePosSpeed_10() const { return ___restorePosSpeed_10; }
	inline float* get_address_of_restorePosSpeed_10() { return &___restorePosSpeed_10; }
	inline void set_restorePosSpeed_10(float value)
	{
		___restorePosSpeed_10 = value;
	}

	inline static int32_t get_offset_of_activeTimeout_11() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___activeTimeout_11)); }
	inline float get_activeTimeout_11() const { return ___activeTimeout_11; }
	inline float* get_address_of_activeTimeout_11() { return &___activeTimeout_11; }
	inline void set_activeTimeout_11(float value)
	{
		___activeTimeout_11 = value;
	}

	inline static int32_t get_offset_of_restoreCurve_12() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___restoreCurve_12)); }
	inline AnimationCurve_t3046754366 * get_restoreCurve_12() const { return ___restoreCurve_12; }
	inline AnimationCurve_t3046754366 ** get_address_of_restoreCurve_12() { return &___restoreCurve_12; }
	inline void set_restoreCurve_12(AnimationCurve_t3046754366 * value)
	{
		___restoreCurve_12 = value;
		Il2CppCodeGenWriteBarrier((&___restoreCurve_12), value);
	}

	inline static int32_t get_offset_of_limitDuplicates_13() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___limitDuplicates_13)); }
	inline bool get_limitDuplicates_13() const { return ___limitDuplicates_13; }
	inline bool* get_address_of_limitDuplicates_13() { return &___limitDuplicates_13; }
	inline void set_limitDuplicates_13(bool value)
	{
		___limitDuplicates_13 = value;
	}

	inline static int32_t get_offset_of_maxDuplicates_14() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___maxDuplicates_14)); }
	inline int32_t get_maxDuplicates_14() const { return ___maxDuplicates_14; }
	inline int32_t* get_address_of_maxDuplicates_14() { return &___maxDuplicates_14; }
	inline void set_maxDuplicates_14(int32_t value)
	{
		___maxDuplicates_14 = value;
	}

	inline static int32_t get_offset_of_duplicateSpeed_15() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___duplicateSpeed_15)); }
	inline float get_duplicateSpeed_15() const { return ___duplicateSpeed_15; }
	inline float* get_address_of_duplicateSpeed_15() { return &___duplicateSpeed_15; }
	inline void set_duplicateSpeed_15(float value)
	{
		___duplicateSpeed_15 = value;
	}

	inline static int32_t get_offset_of_duplicateGrowCurve_16() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___duplicateGrowCurve_16)); }
	inline AnimationCurve_t3046754366 * get_duplicateGrowCurve_16() const { return ___duplicateGrowCurve_16; }
	inline AnimationCurve_t3046754366 ** get_address_of_duplicateGrowCurve_16() { return &___duplicateGrowCurve_16; }
	inline void set_duplicateGrowCurve_16(AnimationCurve_t3046754366 * value)
	{
		___duplicateGrowCurve_16 = value;
		Il2CppCodeGenWriteBarrier((&___duplicateGrowCurve_16), value);
	}

	inline static int32_t get_offset_of_state_17() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___state_17)); }
	inline int32_t get_state_17() const { return ___state_17; }
	inline int32_t* get_address_of_state_17() { return &___state_17; }
	inline void set_state_17(int32_t value)
	{
		___state_17 = value;
	}

	inline static int32_t get_offset_of_targetPos_18() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___targetPos_18)); }
	inline Vector3_t3722313464  get_targetPos_18() const { return ___targetPos_18; }
	inline Vector3_t3722313464 * get_address_of_targetPos_18() { return &___targetPos_18; }
	inline void set_targetPos_18(Vector3_t3722313464  value)
	{
		___targetPos_18 = value;
	}

	inline static int32_t get_offset_of_targetScale_19() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___targetScale_19)); }
	inline Vector3_t3722313464  get_targetScale_19() const { return ___targetScale_19; }
	inline Vector3_t3722313464 * get_address_of_targetScale_19() { return &___targetScale_19; }
	inline void set_targetScale_19(Vector3_t3722313464  value)
	{
		___targetScale_19 = value;
	}

	inline static int32_t get_offset_of_targetRot_20() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___targetRot_20)); }
	inline Quaternion_t2301928331  get_targetRot_20() const { return ___targetRot_20; }
	inline Quaternion_t2301928331 * get_address_of_targetRot_20() { return &___targetRot_20; }
	inline void set_targetRot_20(Quaternion_t2301928331  value)
	{
		___targetRot_20 = value;
	}

	inline static int32_t get_offset_of_numDuplicates_21() { return static_cast<int32_t>(offsetof(Duplicator_t4067483021, ___numDuplicates_21)); }
	inline int32_t get_numDuplicates_21() const { return ___numDuplicates_21; }
	inline int32_t* get_address_of_numDuplicates_21() { return &___numDuplicates_21; }
	inline void set_numDuplicates_21(int32_t value)
	{
		___numDuplicates_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUPLICATOR_T4067483021_H
#ifndef LINEOBJECTCOLLECTION_T2519879353_H
#define LINEOBJECTCOLLECTION_T2519879353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.LineObjectCollection
struct  LineObjectCollection_t2519879353  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Transform> HoloToolkit.Unity.UX.LineObjectCollection::Objects
	List_1_t777473367 * ___Objects_2;
	// System.Single HoloToolkit.Unity.UX.LineObjectCollection::DistributionOffset
	float ___DistributionOffset_3;
	// System.Single HoloToolkit.Unity.UX.LineObjectCollection::LengthOffset
	float ___LengthOffset_4;
	// System.Single HoloToolkit.Unity.UX.LineObjectCollection::ScaleOffset
	float ___ScaleOffset_5;
	// System.Single HoloToolkit.Unity.UX.LineObjectCollection::ScaleMultiplier
	float ___ScaleMultiplier_6;
	// System.Single HoloToolkit.Unity.UX.LineObjectCollection::PositionMultiplier
	float ___PositionMultiplier_7;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.LineObjectCollection::ObjectScale
	AnimationCurve_t3046754366 * ___ObjectScale_8;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.LineObjectCollection::ObjectPosition
	AnimationCurve_t3046754366 * ___ObjectPosition_9;
	// System.Boolean HoloToolkit.Unity.UX.LineObjectCollection::FlipRotation
	bool ___FlipRotation_10;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.LineObjectCollection::RotationOffset
	Vector3_t3722313464  ___RotationOffset_11;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.LineObjectCollection::PositionOffset
	Vector3_t3722313464  ___PositionOffset_12;
	// HoloToolkit.Unity.UX.RotationTypeEnum HoloToolkit.Unity.UX.LineObjectCollection::RotationTypeOverride
	int32_t ___RotationTypeOverride_13;
	// HoloToolkit.Unity.UX.PointDistributionTypeEnum HoloToolkit.Unity.UX.LineObjectCollection::DistributionType
	int32_t ___DistributionType_14;
	// HoloToolkit.Unity.UX.StepModeEnum HoloToolkit.Unity.UX.LineObjectCollection::StepMode
	int32_t ___StepMode_15;
	// HoloToolkit.Unity.UX.LineBase HoloToolkit.Unity.UX.LineObjectCollection::source
	LineBase_t3631573702 * ___source_16;
	// UnityEngine.Transform HoloToolkit.Unity.UX.LineObjectCollection::transformHelper
	Transform_t3600365921 * ___transformHelper_17;

public:
	inline static int32_t get_offset_of_Objects_2() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___Objects_2)); }
	inline List_1_t777473367 * get_Objects_2() const { return ___Objects_2; }
	inline List_1_t777473367 ** get_address_of_Objects_2() { return &___Objects_2; }
	inline void set_Objects_2(List_1_t777473367 * value)
	{
		___Objects_2 = value;
		Il2CppCodeGenWriteBarrier((&___Objects_2), value);
	}

	inline static int32_t get_offset_of_DistributionOffset_3() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___DistributionOffset_3)); }
	inline float get_DistributionOffset_3() const { return ___DistributionOffset_3; }
	inline float* get_address_of_DistributionOffset_3() { return &___DistributionOffset_3; }
	inline void set_DistributionOffset_3(float value)
	{
		___DistributionOffset_3 = value;
	}

	inline static int32_t get_offset_of_LengthOffset_4() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___LengthOffset_4)); }
	inline float get_LengthOffset_4() const { return ___LengthOffset_4; }
	inline float* get_address_of_LengthOffset_4() { return &___LengthOffset_4; }
	inline void set_LengthOffset_4(float value)
	{
		___LengthOffset_4 = value;
	}

	inline static int32_t get_offset_of_ScaleOffset_5() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___ScaleOffset_5)); }
	inline float get_ScaleOffset_5() const { return ___ScaleOffset_5; }
	inline float* get_address_of_ScaleOffset_5() { return &___ScaleOffset_5; }
	inline void set_ScaleOffset_5(float value)
	{
		___ScaleOffset_5 = value;
	}

	inline static int32_t get_offset_of_ScaleMultiplier_6() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___ScaleMultiplier_6)); }
	inline float get_ScaleMultiplier_6() const { return ___ScaleMultiplier_6; }
	inline float* get_address_of_ScaleMultiplier_6() { return &___ScaleMultiplier_6; }
	inline void set_ScaleMultiplier_6(float value)
	{
		___ScaleMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_PositionMultiplier_7() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___PositionMultiplier_7)); }
	inline float get_PositionMultiplier_7() const { return ___PositionMultiplier_7; }
	inline float* get_address_of_PositionMultiplier_7() { return &___PositionMultiplier_7; }
	inline void set_PositionMultiplier_7(float value)
	{
		___PositionMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_ObjectScale_8() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___ObjectScale_8)); }
	inline AnimationCurve_t3046754366 * get_ObjectScale_8() const { return ___ObjectScale_8; }
	inline AnimationCurve_t3046754366 ** get_address_of_ObjectScale_8() { return &___ObjectScale_8; }
	inline void set_ObjectScale_8(AnimationCurve_t3046754366 * value)
	{
		___ObjectScale_8 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectScale_8), value);
	}

	inline static int32_t get_offset_of_ObjectPosition_9() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___ObjectPosition_9)); }
	inline AnimationCurve_t3046754366 * get_ObjectPosition_9() const { return ___ObjectPosition_9; }
	inline AnimationCurve_t3046754366 ** get_address_of_ObjectPosition_9() { return &___ObjectPosition_9; }
	inline void set_ObjectPosition_9(AnimationCurve_t3046754366 * value)
	{
		___ObjectPosition_9 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectPosition_9), value);
	}

	inline static int32_t get_offset_of_FlipRotation_10() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___FlipRotation_10)); }
	inline bool get_FlipRotation_10() const { return ___FlipRotation_10; }
	inline bool* get_address_of_FlipRotation_10() { return &___FlipRotation_10; }
	inline void set_FlipRotation_10(bool value)
	{
		___FlipRotation_10 = value;
	}

	inline static int32_t get_offset_of_RotationOffset_11() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___RotationOffset_11)); }
	inline Vector3_t3722313464  get_RotationOffset_11() const { return ___RotationOffset_11; }
	inline Vector3_t3722313464 * get_address_of_RotationOffset_11() { return &___RotationOffset_11; }
	inline void set_RotationOffset_11(Vector3_t3722313464  value)
	{
		___RotationOffset_11 = value;
	}

	inline static int32_t get_offset_of_PositionOffset_12() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___PositionOffset_12)); }
	inline Vector3_t3722313464  get_PositionOffset_12() const { return ___PositionOffset_12; }
	inline Vector3_t3722313464 * get_address_of_PositionOffset_12() { return &___PositionOffset_12; }
	inline void set_PositionOffset_12(Vector3_t3722313464  value)
	{
		___PositionOffset_12 = value;
	}

	inline static int32_t get_offset_of_RotationTypeOverride_13() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___RotationTypeOverride_13)); }
	inline int32_t get_RotationTypeOverride_13() const { return ___RotationTypeOverride_13; }
	inline int32_t* get_address_of_RotationTypeOverride_13() { return &___RotationTypeOverride_13; }
	inline void set_RotationTypeOverride_13(int32_t value)
	{
		___RotationTypeOverride_13 = value;
	}

	inline static int32_t get_offset_of_DistributionType_14() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___DistributionType_14)); }
	inline int32_t get_DistributionType_14() const { return ___DistributionType_14; }
	inline int32_t* get_address_of_DistributionType_14() { return &___DistributionType_14; }
	inline void set_DistributionType_14(int32_t value)
	{
		___DistributionType_14 = value;
	}

	inline static int32_t get_offset_of_StepMode_15() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___StepMode_15)); }
	inline int32_t get_StepMode_15() const { return ___StepMode_15; }
	inline int32_t* get_address_of_StepMode_15() { return &___StepMode_15; }
	inline void set_StepMode_15(int32_t value)
	{
		___StepMode_15 = value;
	}

	inline static int32_t get_offset_of_source_16() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___source_16)); }
	inline LineBase_t3631573702 * get_source_16() const { return ___source_16; }
	inline LineBase_t3631573702 ** get_address_of_source_16() { return &___source_16; }
	inline void set_source_16(LineBase_t3631573702 * value)
	{
		___source_16 = value;
		Il2CppCodeGenWriteBarrier((&___source_16), value);
	}

	inline static int32_t get_offset_of_transformHelper_17() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2519879353, ___transformHelper_17)); }
	inline Transform_t3600365921 * get_transformHelper_17() const { return ___transformHelper_17; }
	inline Transform_t3600365921 ** get_address_of_transformHelper_17() { return &___transformHelper_17; }
	inline void set_transformHelper_17(Transform_t3600365921 * value)
	{
		___transformHelper_17 = value;
		Il2CppCodeGenWriteBarrier((&___transformHelper_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEOBJECTCOLLECTION_T2519879353_H
#ifndef BOUNDINGBOXGIZMOSHELL_T63524259_H
#define BOUNDINGBOXGIZMOSHELL_T63524259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBoxGizmoShell
struct  BoundingBoxGizmoShell_t63524259  : public BoundingBoxGizmo_t584836069
{
public:
	// System.Boolean HoloToolkit.Unity.UX.BoundingBoxGizmoShell::DisplayEdgesWhenSelected
	bool ___DisplayEdgesWhenSelected_3;
	// System.Boolean HoloToolkit.Unity.UX.BoundingBoxGizmoShell::ClampHandleScale
	bool ___ClampHandleScale_4;
	// System.Single HoloToolkit.Unity.UX.BoundingBoxGizmoShell::HandleScaleMin
	float ___HandleScaleMin_5;
	// System.Single HoloToolkit.Unity.UX.BoundingBoxGizmoShell::HandleScaleMax
	float ___HandleScaleMax_6;
	// UnityEngine.Renderer HoloToolkit.Unity.UX.BoundingBoxGizmoShell::edgeRenderer
	Renderer_t2627027031 * ___edgeRenderer_7;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.BoundingBoxGizmoShell::xyzHandlesObject
	GameObject_t1113636619 * ___xyzHandlesObject_8;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.BoundingBoxGizmoShell::xyHandlesObject
	GameObject_t1113636619 * ___xyHandlesObject_9;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.BoundingBoxGizmoShell::xzHandlesObject
	GameObject_t1113636619 * ___xzHandlesObject_10;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.BoundingBoxGizmoShell::zyHandlesObject
	GameObject_t1113636619 * ___zyHandlesObject_11;

public:
	inline static int32_t get_offset_of_DisplayEdgesWhenSelected_3() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t63524259, ___DisplayEdgesWhenSelected_3)); }
	inline bool get_DisplayEdgesWhenSelected_3() const { return ___DisplayEdgesWhenSelected_3; }
	inline bool* get_address_of_DisplayEdgesWhenSelected_3() { return &___DisplayEdgesWhenSelected_3; }
	inline void set_DisplayEdgesWhenSelected_3(bool value)
	{
		___DisplayEdgesWhenSelected_3 = value;
	}

	inline static int32_t get_offset_of_ClampHandleScale_4() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t63524259, ___ClampHandleScale_4)); }
	inline bool get_ClampHandleScale_4() const { return ___ClampHandleScale_4; }
	inline bool* get_address_of_ClampHandleScale_4() { return &___ClampHandleScale_4; }
	inline void set_ClampHandleScale_4(bool value)
	{
		___ClampHandleScale_4 = value;
	}

	inline static int32_t get_offset_of_HandleScaleMin_5() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t63524259, ___HandleScaleMin_5)); }
	inline float get_HandleScaleMin_5() const { return ___HandleScaleMin_5; }
	inline float* get_address_of_HandleScaleMin_5() { return &___HandleScaleMin_5; }
	inline void set_HandleScaleMin_5(float value)
	{
		___HandleScaleMin_5 = value;
	}

	inline static int32_t get_offset_of_HandleScaleMax_6() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t63524259, ___HandleScaleMax_6)); }
	inline float get_HandleScaleMax_6() const { return ___HandleScaleMax_6; }
	inline float* get_address_of_HandleScaleMax_6() { return &___HandleScaleMax_6; }
	inline void set_HandleScaleMax_6(float value)
	{
		___HandleScaleMax_6 = value;
	}

	inline static int32_t get_offset_of_edgeRenderer_7() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t63524259, ___edgeRenderer_7)); }
	inline Renderer_t2627027031 * get_edgeRenderer_7() const { return ___edgeRenderer_7; }
	inline Renderer_t2627027031 ** get_address_of_edgeRenderer_7() { return &___edgeRenderer_7; }
	inline void set_edgeRenderer_7(Renderer_t2627027031 * value)
	{
		___edgeRenderer_7 = value;
		Il2CppCodeGenWriteBarrier((&___edgeRenderer_7), value);
	}

	inline static int32_t get_offset_of_xyzHandlesObject_8() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t63524259, ___xyzHandlesObject_8)); }
	inline GameObject_t1113636619 * get_xyzHandlesObject_8() const { return ___xyzHandlesObject_8; }
	inline GameObject_t1113636619 ** get_address_of_xyzHandlesObject_8() { return &___xyzHandlesObject_8; }
	inline void set_xyzHandlesObject_8(GameObject_t1113636619 * value)
	{
		___xyzHandlesObject_8 = value;
		Il2CppCodeGenWriteBarrier((&___xyzHandlesObject_8), value);
	}

	inline static int32_t get_offset_of_xyHandlesObject_9() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t63524259, ___xyHandlesObject_9)); }
	inline GameObject_t1113636619 * get_xyHandlesObject_9() const { return ___xyHandlesObject_9; }
	inline GameObject_t1113636619 ** get_address_of_xyHandlesObject_9() { return &___xyHandlesObject_9; }
	inline void set_xyHandlesObject_9(GameObject_t1113636619 * value)
	{
		___xyHandlesObject_9 = value;
		Il2CppCodeGenWriteBarrier((&___xyHandlesObject_9), value);
	}

	inline static int32_t get_offset_of_xzHandlesObject_10() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t63524259, ___xzHandlesObject_10)); }
	inline GameObject_t1113636619 * get_xzHandlesObject_10() const { return ___xzHandlesObject_10; }
	inline GameObject_t1113636619 ** get_address_of_xzHandlesObject_10() { return &___xzHandlesObject_10; }
	inline void set_xzHandlesObject_10(GameObject_t1113636619 * value)
	{
		___xzHandlesObject_10 = value;
		Il2CppCodeGenWriteBarrier((&___xzHandlesObject_10), value);
	}

	inline static int32_t get_offset_of_zyHandlesObject_11() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t63524259, ___zyHandlesObject_11)); }
	inline GameObject_t1113636619 * get_zyHandlesObject_11() const { return ___zyHandlesObject_11; }
	inline GameObject_t1113636619 ** get_address_of_zyHandlesObject_11() { return &___zyHandlesObject_11; }
	inline void set_zyHandlesObject_11(GameObject_t1113636619 * value)
	{
		___zyHandlesObject_11 = value;
		Il2CppCodeGenWriteBarrier((&___zyHandlesObject_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXGIZMOSHELL_T63524259_H
#ifndef ANIMBUTTON_T3218175740_H
#define ANIMBUTTON_T3218175740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.AnimButton
struct  AnimButton_t3218175740  : public Button_t2162584723
{
public:
	// UnityEngine.Animator HoloToolkit.Unity.Buttons.AnimButton::_animator
	Animator_t434523843 * ____animator_16;

public:
	inline static int32_t get_offset_of__animator_16() { return static_cast<int32_t>(offsetof(AnimButton_t3218175740, ____animator_16)); }
	inline Animator_t434523843 * get__animator_16() const { return ____animator_16; }
	inline Animator_t434523843 ** get_address_of__animator_16() { return &____animator_16; }
	inline void set__animator_16(Animator_t434523843 * value)
	{
		____animator_16 = value;
		Il2CppCodeGenWriteBarrier((&____animator_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMBUTTON_T3218175740_H
#ifndef LINE_T2631947846_H
#define LINE_T2631947846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Line
struct  Line_t2631947846  : public LineBase_t3631573702
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Line::Start
	Vector3_t3722313464  ___Start_17;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Line::End
	Vector3_t3722313464  ___End_18;

public:
	inline static int32_t get_offset_of_Start_17() { return static_cast<int32_t>(offsetof(Line_t2631947846, ___Start_17)); }
	inline Vector3_t3722313464  get_Start_17() const { return ___Start_17; }
	inline Vector3_t3722313464 * get_address_of_Start_17() { return &___Start_17; }
	inline void set_Start_17(Vector3_t3722313464  value)
	{
		___Start_17 = value;
	}

	inline static int32_t get_offset_of_End_18() { return static_cast<int32_t>(offsetof(Line_t2631947846, ___End_18)); }
	inline Vector3_t3722313464  get_End_18() const { return ___End_18; }
	inline Vector3_t3722313464 * get_address_of_End_18() { return &___End_18; }
	inline void set_End_18(Vector3_t3722313464  value)
	{
		___End_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINE_T2631947846_H
#ifndef SOLVERRADIALVIEW_T454380380_H
#define SOLVERRADIALVIEW_T454380380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverRadialView
struct  SolverRadialView_t454380380  : public Solver_t4167981057
{
public:
	// HoloToolkit.Unity.SolverRadialView/ReferenceDirectionEnum HoloToolkit.Unity.SolverRadialView::ReferenceDirection
	int32_t ___ReferenceDirection_14;
	// System.Single HoloToolkit.Unity.SolverRadialView::MinDistance
	float ___MinDistance_15;
	// System.Single HoloToolkit.Unity.SolverRadialView::MaxDistance
	float ___MaxDistance_16;
	// System.Single HoloToolkit.Unity.SolverRadialView::MinViewDegrees
	float ___MinViewDegrees_17;
	// System.Single HoloToolkit.Unity.SolverRadialView::MaxViewDegrees
	float ___MaxViewDegrees_18;
	// System.Single HoloToolkit.Unity.SolverRadialView::AspectV
	float ___AspectV_19;
	// System.Boolean HoloToolkit.Unity.SolverRadialView::IgnoreAngleClamp
	bool ___IgnoreAngleClamp_20;
	// System.Boolean HoloToolkit.Unity.SolverRadialView::IgnoreDistanceClamp
	bool ___IgnoreDistanceClamp_21;
	// System.Boolean HoloToolkit.Unity.SolverRadialView::OrientToRefDir
	bool ___OrientToRefDir_22;

public:
	inline static int32_t get_offset_of_ReferenceDirection_14() { return static_cast<int32_t>(offsetof(SolverRadialView_t454380380, ___ReferenceDirection_14)); }
	inline int32_t get_ReferenceDirection_14() const { return ___ReferenceDirection_14; }
	inline int32_t* get_address_of_ReferenceDirection_14() { return &___ReferenceDirection_14; }
	inline void set_ReferenceDirection_14(int32_t value)
	{
		___ReferenceDirection_14 = value;
	}

	inline static int32_t get_offset_of_MinDistance_15() { return static_cast<int32_t>(offsetof(SolverRadialView_t454380380, ___MinDistance_15)); }
	inline float get_MinDistance_15() const { return ___MinDistance_15; }
	inline float* get_address_of_MinDistance_15() { return &___MinDistance_15; }
	inline void set_MinDistance_15(float value)
	{
		___MinDistance_15 = value;
	}

	inline static int32_t get_offset_of_MaxDistance_16() { return static_cast<int32_t>(offsetof(SolverRadialView_t454380380, ___MaxDistance_16)); }
	inline float get_MaxDistance_16() const { return ___MaxDistance_16; }
	inline float* get_address_of_MaxDistance_16() { return &___MaxDistance_16; }
	inline void set_MaxDistance_16(float value)
	{
		___MaxDistance_16 = value;
	}

	inline static int32_t get_offset_of_MinViewDegrees_17() { return static_cast<int32_t>(offsetof(SolverRadialView_t454380380, ___MinViewDegrees_17)); }
	inline float get_MinViewDegrees_17() const { return ___MinViewDegrees_17; }
	inline float* get_address_of_MinViewDegrees_17() { return &___MinViewDegrees_17; }
	inline void set_MinViewDegrees_17(float value)
	{
		___MinViewDegrees_17 = value;
	}

	inline static int32_t get_offset_of_MaxViewDegrees_18() { return static_cast<int32_t>(offsetof(SolverRadialView_t454380380, ___MaxViewDegrees_18)); }
	inline float get_MaxViewDegrees_18() const { return ___MaxViewDegrees_18; }
	inline float* get_address_of_MaxViewDegrees_18() { return &___MaxViewDegrees_18; }
	inline void set_MaxViewDegrees_18(float value)
	{
		___MaxViewDegrees_18 = value;
	}

	inline static int32_t get_offset_of_AspectV_19() { return static_cast<int32_t>(offsetof(SolverRadialView_t454380380, ___AspectV_19)); }
	inline float get_AspectV_19() const { return ___AspectV_19; }
	inline float* get_address_of_AspectV_19() { return &___AspectV_19; }
	inline void set_AspectV_19(float value)
	{
		___AspectV_19 = value;
	}

	inline static int32_t get_offset_of_IgnoreAngleClamp_20() { return static_cast<int32_t>(offsetof(SolverRadialView_t454380380, ___IgnoreAngleClamp_20)); }
	inline bool get_IgnoreAngleClamp_20() const { return ___IgnoreAngleClamp_20; }
	inline bool* get_address_of_IgnoreAngleClamp_20() { return &___IgnoreAngleClamp_20; }
	inline void set_IgnoreAngleClamp_20(bool value)
	{
		___IgnoreAngleClamp_20 = value;
	}

	inline static int32_t get_offset_of_IgnoreDistanceClamp_21() { return static_cast<int32_t>(offsetof(SolverRadialView_t454380380, ___IgnoreDistanceClamp_21)); }
	inline bool get_IgnoreDistanceClamp_21() const { return ___IgnoreDistanceClamp_21; }
	inline bool* get_address_of_IgnoreDistanceClamp_21() { return &___IgnoreDistanceClamp_21; }
	inline void set_IgnoreDistanceClamp_21(bool value)
	{
		___IgnoreDistanceClamp_21 = value;
	}

	inline static int32_t get_offset_of_OrientToRefDir_22() { return static_cast<int32_t>(offsetof(SolverRadialView_t454380380, ___OrientToRefDir_22)); }
	inline bool get_OrientToRefDir_22() const { return ___OrientToRefDir_22; }
	inline bool* get_address_of_OrientToRefDir_22() { return &___OrientToRefDir_22; }
	inline void set_OrientToRefDir_22(bool value)
	{
		___OrientToRefDir_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVERRADIALVIEW_T454380380_H
#ifndef OBJECTBUTTON_T1512479393_H
#define OBJECTBUTTON_T1512479393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ObjectButton
struct  ObjectButton_t1512479393  : public Button_t2162584723
{
public:
	// HoloToolkit.Unity.Buttons.ObjectButton/ObjectButtonDatum[] HoloToolkit.Unity.Buttons.ObjectButton::ButtonStates
	ObjectButtonDatumU5BU5D_t1611756286* ___ButtonStates_16;

public:
	inline static int32_t get_offset_of_ButtonStates_16() { return static_cast<int32_t>(offsetof(ObjectButton_t1512479393, ___ButtonStates_16)); }
	inline ObjectButtonDatumU5BU5D_t1611756286* get_ButtonStates_16() const { return ___ButtonStates_16; }
	inline ObjectButtonDatumU5BU5D_t1611756286** get_address_of_ButtonStates_16() { return &___ButtonStates_16; }
	inline void set_ButtonStates_16(ObjectButtonDatumU5BU5D_t1611756286* value)
	{
		___ButtonStates_16 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonStates_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTBUTTON_T1512479393_H
#ifndef MESHBUTTON_T4168117946_H
#define MESHBUTTON_T4168117946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.MeshButton
struct  MeshButton_t4168117946  : public Button_t2162584723
{
public:
	// System.Boolean HoloToolkit.Unity.Buttons.MeshButton::UseAnimator
	bool ___UseAnimator_16;
	// HoloToolkit.Unity.Buttons.MeshButtonDatum[] HoloToolkit.Unity.Buttons.MeshButton::ButtonStates
	MeshButtonDatumU5BU5D_t1991104643* ___ButtonStates_17;
	// UnityEngine.MeshRenderer HoloToolkit.Unity.Buttons.MeshButton::_renderer
	MeshRenderer_t587009260 * ____renderer_18;
	// UnityEngine.MeshFilter HoloToolkit.Unity.Buttons.MeshButton::_meshFilter
	MeshFilter_t3523625662 * ____meshFilter_19;
	// UnityEngine.Animator HoloToolkit.Unity.Buttons.MeshButton::_animator
	Animator_t434523843 * ____animator_20;

public:
	inline static int32_t get_offset_of_UseAnimator_16() { return static_cast<int32_t>(offsetof(MeshButton_t4168117946, ___UseAnimator_16)); }
	inline bool get_UseAnimator_16() const { return ___UseAnimator_16; }
	inline bool* get_address_of_UseAnimator_16() { return &___UseAnimator_16; }
	inline void set_UseAnimator_16(bool value)
	{
		___UseAnimator_16 = value;
	}

	inline static int32_t get_offset_of_ButtonStates_17() { return static_cast<int32_t>(offsetof(MeshButton_t4168117946, ___ButtonStates_17)); }
	inline MeshButtonDatumU5BU5D_t1991104643* get_ButtonStates_17() const { return ___ButtonStates_17; }
	inline MeshButtonDatumU5BU5D_t1991104643** get_address_of_ButtonStates_17() { return &___ButtonStates_17; }
	inline void set_ButtonStates_17(MeshButtonDatumU5BU5D_t1991104643* value)
	{
		___ButtonStates_17 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonStates_17), value);
	}

	inline static int32_t get_offset_of__renderer_18() { return static_cast<int32_t>(offsetof(MeshButton_t4168117946, ____renderer_18)); }
	inline MeshRenderer_t587009260 * get__renderer_18() const { return ____renderer_18; }
	inline MeshRenderer_t587009260 ** get_address_of__renderer_18() { return &____renderer_18; }
	inline void set__renderer_18(MeshRenderer_t587009260 * value)
	{
		____renderer_18 = value;
		Il2CppCodeGenWriteBarrier((&____renderer_18), value);
	}

	inline static int32_t get_offset_of__meshFilter_19() { return static_cast<int32_t>(offsetof(MeshButton_t4168117946, ____meshFilter_19)); }
	inline MeshFilter_t3523625662 * get__meshFilter_19() const { return ____meshFilter_19; }
	inline MeshFilter_t3523625662 ** get_address_of__meshFilter_19() { return &____meshFilter_19; }
	inline void set__meshFilter_19(MeshFilter_t3523625662 * value)
	{
		____meshFilter_19 = value;
		Il2CppCodeGenWriteBarrier((&____meshFilter_19), value);
	}

	inline static int32_t get_offset_of__animator_20() { return static_cast<int32_t>(offsetof(MeshButton_t4168117946, ____animator_20)); }
	inline Animator_t434523843 * get__animator_20() const { return ____animator_20; }
	inline Animator_t434523843 ** get_address_of__animator_20() { return &____animator_20; }
	inline void set__animator_20(Animator_t434523843 * value)
	{
		____animator_20 = value;
		Il2CppCodeGenWriteBarrier((&____animator_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHBUTTON_T4168117946_H
#ifndef SOLVERMOMENTUMIZER_T3553753120_H
#define SOLVERMOMENTUMIZER_T3553753120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverMomentumizer
struct  SolverMomentumizer_t3553753120  : public Solver_t4167981057
{
public:
	// System.Single HoloToolkit.Unity.SolverMomentumizer::resistance
	float ___resistance_14;
	// System.Single HoloToolkit.Unity.SolverMomentumizer::resistanceVelPower
	float ___resistanceVelPower_15;
	// System.Single HoloToolkit.Unity.SolverMomentumizer::accelRate
	float ___accelRate_16;
	// System.Single HoloToolkit.Unity.SolverMomentumizer::springiness
	float ___springiness_17;
	// System.Boolean HoloToolkit.Unity.SolverMomentumizer::SnapZ
	bool ___SnapZ_18;
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverMomentumizer::velocity
	Vector3_t3722313464  ___velocity_19;

public:
	inline static int32_t get_offset_of_resistance_14() { return static_cast<int32_t>(offsetof(SolverMomentumizer_t3553753120, ___resistance_14)); }
	inline float get_resistance_14() const { return ___resistance_14; }
	inline float* get_address_of_resistance_14() { return &___resistance_14; }
	inline void set_resistance_14(float value)
	{
		___resistance_14 = value;
	}

	inline static int32_t get_offset_of_resistanceVelPower_15() { return static_cast<int32_t>(offsetof(SolverMomentumizer_t3553753120, ___resistanceVelPower_15)); }
	inline float get_resistanceVelPower_15() const { return ___resistanceVelPower_15; }
	inline float* get_address_of_resistanceVelPower_15() { return &___resistanceVelPower_15; }
	inline void set_resistanceVelPower_15(float value)
	{
		___resistanceVelPower_15 = value;
	}

	inline static int32_t get_offset_of_accelRate_16() { return static_cast<int32_t>(offsetof(SolverMomentumizer_t3553753120, ___accelRate_16)); }
	inline float get_accelRate_16() const { return ___accelRate_16; }
	inline float* get_address_of_accelRate_16() { return &___accelRate_16; }
	inline void set_accelRate_16(float value)
	{
		___accelRate_16 = value;
	}

	inline static int32_t get_offset_of_springiness_17() { return static_cast<int32_t>(offsetof(SolverMomentumizer_t3553753120, ___springiness_17)); }
	inline float get_springiness_17() const { return ___springiness_17; }
	inline float* get_address_of_springiness_17() { return &___springiness_17; }
	inline void set_springiness_17(float value)
	{
		___springiness_17 = value;
	}

	inline static int32_t get_offset_of_SnapZ_18() { return static_cast<int32_t>(offsetof(SolverMomentumizer_t3553753120, ___SnapZ_18)); }
	inline bool get_SnapZ_18() const { return ___SnapZ_18; }
	inline bool* get_address_of_SnapZ_18() { return &___SnapZ_18; }
	inline void set_SnapZ_18(bool value)
	{
		___SnapZ_18 = value;
	}

	inline static int32_t get_offset_of_velocity_19() { return static_cast<int32_t>(offsetof(SolverMomentumizer_t3553753120, ___velocity_19)); }
	inline Vector3_t3722313464  get_velocity_19() const { return ___velocity_19; }
	inline Vector3_t3722313464 * get_address_of_velocity_19() { return &___velocity_19; }
	inline void set_velocity_19(Vector3_t3722313464  value)
	{
		___velocity_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVERMOMENTUMIZER_T3553753120_H
#ifndef BUTTONICONPROFILETEXTURE_T3085289291_H
#define BUTTONICONPROFILETEXTURE_T3085289291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonIconProfileTexture
struct  ButtonIconProfileTexture_t3085289291  : public ButtonIconProfile_t2026164329
{
public:
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::GlobalNavButton
	Texture2D_t3840446185 * ___GlobalNavButton_7;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::ChevronUp
	Texture2D_t3840446185 * ___ChevronUp_8;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::ChevronDown
	Texture2D_t3840446185 * ___ChevronDown_9;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::ChevronLeft
	Texture2D_t3840446185 * ___ChevronLeft_10;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::ChevronRight
	Texture2D_t3840446185 * ___ChevronRight_11;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Forward
	Texture2D_t3840446185 * ___Forward_12;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Back
	Texture2D_t3840446185 * ___Back_13;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::PageLeft
	Texture2D_t3840446185 * ___PageLeft_14;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::PageRight
	Texture2D_t3840446185 * ___PageRight_15;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Add
	Texture2D_t3840446185 * ___Add_16;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Remove
	Texture2D_t3840446185 * ___Remove_17;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Clear
	Texture2D_t3840446185 * ___Clear_18;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Cancel
	Texture2D_t3840446185 * ___Cancel_19;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Zoom
	Texture2D_t3840446185 * ___Zoom_20;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Refresh
	Texture2D_t3840446185 * ___Refresh_21;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Lock
	Texture2D_t3840446185 * ___Lock_22;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Accept
	Texture2D_t3840446185 * ___Accept_23;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::OpenInNewWindow
	Texture2D_t3840446185 * ___OpenInNewWindow_24;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Completed
	Texture2D_t3840446185 * ___Completed_25;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Error
	Texture2D_t3840446185 * ___Error_26;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Contact
	Texture2D_t3840446185 * ___Contact_27;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Volume
	Texture2D_t3840446185 * ___Volume_28;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::KeyboardClassic
	Texture2D_t3840446185 * ___KeyboardClassic_29;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Camera
	Texture2D_t3840446185 * ___Camera_30;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Video
	Texture2D_t3840446185 * ___Video_31;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Microphone
	Texture2D_t3840446185 * ___Microphone_32;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Ready
	Texture2D_t3840446185 * ___Ready_33;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::AirTap
	Texture2D_t3840446185 * ___AirTap_34;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::PressHold
	Texture2D_t3840446185 * ___PressHold_35;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Drag
	Texture2D_t3840446185 * ___Drag_36;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::TapToPlaceArt
	Texture2D_t3840446185 * ___TapToPlaceArt_37;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::AdjustWithHand
	Texture2D_t3840446185 * ___AdjustWithHand_38;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::AdjustHologram
	Texture2D_t3840446185 * ___AdjustHologram_39;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::RemoveHologram
	Texture2D_t3840446185 * ___RemoveHologram_40;
	// UnityEngine.Texture2D[] HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::CustomIcons
	Texture2DU5BU5D_t149664596* ___CustomIcons_41;
	// System.Boolean HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::initialized
	bool ___initialized_42;
	// System.Collections.Generic.List`1<System.String> HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::iconKeys
	List_1_t3319525431 * ___iconKeys_43;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D> HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::iconLookup
	Dictionary_2_t3625702484 * ___iconLookup_44;

public:
	inline static int32_t get_offset_of_GlobalNavButton_7() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___GlobalNavButton_7)); }
	inline Texture2D_t3840446185 * get_GlobalNavButton_7() const { return ___GlobalNavButton_7; }
	inline Texture2D_t3840446185 ** get_address_of_GlobalNavButton_7() { return &___GlobalNavButton_7; }
	inline void set_GlobalNavButton_7(Texture2D_t3840446185 * value)
	{
		___GlobalNavButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___GlobalNavButton_7), value);
	}

	inline static int32_t get_offset_of_ChevronUp_8() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___ChevronUp_8)); }
	inline Texture2D_t3840446185 * get_ChevronUp_8() const { return ___ChevronUp_8; }
	inline Texture2D_t3840446185 ** get_address_of_ChevronUp_8() { return &___ChevronUp_8; }
	inline void set_ChevronUp_8(Texture2D_t3840446185 * value)
	{
		___ChevronUp_8 = value;
		Il2CppCodeGenWriteBarrier((&___ChevronUp_8), value);
	}

	inline static int32_t get_offset_of_ChevronDown_9() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___ChevronDown_9)); }
	inline Texture2D_t3840446185 * get_ChevronDown_9() const { return ___ChevronDown_9; }
	inline Texture2D_t3840446185 ** get_address_of_ChevronDown_9() { return &___ChevronDown_9; }
	inline void set_ChevronDown_9(Texture2D_t3840446185 * value)
	{
		___ChevronDown_9 = value;
		Il2CppCodeGenWriteBarrier((&___ChevronDown_9), value);
	}

	inline static int32_t get_offset_of_ChevronLeft_10() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___ChevronLeft_10)); }
	inline Texture2D_t3840446185 * get_ChevronLeft_10() const { return ___ChevronLeft_10; }
	inline Texture2D_t3840446185 ** get_address_of_ChevronLeft_10() { return &___ChevronLeft_10; }
	inline void set_ChevronLeft_10(Texture2D_t3840446185 * value)
	{
		___ChevronLeft_10 = value;
		Il2CppCodeGenWriteBarrier((&___ChevronLeft_10), value);
	}

	inline static int32_t get_offset_of_ChevronRight_11() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___ChevronRight_11)); }
	inline Texture2D_t3840446185 * get_ChevronRight_11() const { return ___ChevronRight_11; }
	inline Texture2D_t3840446185 ** get_address_of_ChevronRight_11() { return &___ChevronRight_11; }
	inline void set_ChevronRight_11(Texture2D_t3840446185 * value)
	{
		___ChevronRight_11 = value;
		Il2CppCodeGenWriteBarrier((&___ChevronRight_11), value);
	}

	inline static int32_t get_offset_of_Forward_12() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Forward_12)); }
	inline Texture2D_t3840446185 * get_Forward_12() const { return ___Forward_12; }
	inline Texture2D_t3840446185 ** get_address_of_Forward_12() { return &___Forward_12; }
	inline void set_Forward_12(Texture2D_t3840446185 * value)
	{
		___Forward_12 = value;
		Il2CppCodeGenWriteBarrier((&___Forward_12), value);
	}

	inline static int32_t get_offset_of_Back_13() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Back_13)); }
	inline Texture2D_t3840446185 * get_Back_13() const { return ___Back_13; }
	inline Texture2D_t3840446185 ** get_address_of_Back_13() { return &___Back_13; }
	inline void set_Back_13(Texture2D_t3840446185 * value)
	{
		___Back_13 = value;
		Il2CppCodeGenWriteBarrier((&___Back_13), value);
	}

	inline static int32_t get_offset_of_PageLeft_14() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___PageLeft_14)); }
	inline Texture2D_t3840446185 * get_PageLeft_14() const { return ___PageLeft_14; }
	inline Texture2D_t3840446185 ** get_address_of_PageLeft_14() { return &___PageLeft_14; }
	inline void set_PageLeft_14(Texture2D_t3840446185 * value)
	{
		___PageLeft_14 = value;
		Il2CppCodeGenWriteBarrier((&___PageLeft_14), value);
	}

	inline static int32_t get_offset_of_PageRight_15() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___PageRight_15)); }
	inline Texture2D_t3840446185 * get_PageRight_15() const { return ___PageRight_15; }
	inline Texture2D_t3840446185 ** get_address_of_PageRight_15() { return &___PageRight_15; }
	inline void set_PageRight_15(Texture2D_t3840446185 * value)
	{
		___PageRight_15 = value;
		Il2CppCodeGenWriteBarrier((&___PageRight_15), value);
	}

	inline static int32_t get_offset_of_Add_16() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Add_16)); }
	inline Texture2D_t3840446185 * get_Add_16() const { return ___Add_16; }
	inline Texture2D_t3840446185 ** get_address_of_Add_16() { return &___Add_16; }
	inline void set_Add_16(Texture2D_t3840446185 * value)
	{
		___Add_16 = value;
		Il2CppCodeGenWriteBarrier((&___Add_16), value);
	}

	inline static int32_t get_offset_of_Remove_17() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Remove_17)); }
	inline Texture2D_t3840446185 * get_Remove_17() const { return ___Remove_17; }
	inline Texture2D_t3840446185 ** get_address_of_Remove_17() { return &___Remove_17; }
	inline void set_Remove_17(Texture2D_t3840446185 * value)
	{
		___Remove_17 = value;
		Il2CppCodeGenWriteBarrier((&___Remove_17), value);
	}

	inline static int32_t get_offset_of_Clear_18() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Clear_18)); }
	inline Texture2D_t3840446185 * get_Clear_18() const { return ___Clear_18; }
	inline Texture2D_t3840446185 ** get_address_of_Clear_18() { return &___Clear_18; }
	inline void set_Clear_18(Texture2D_t3840446185 * value)
	{
		___Clear_18 = value;
		Il2CppCodeGenWriteBarrier((&___Clear_18), value);
	}

	inline static int32_t get_offset_of_Cancel_19() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Cancel_19)); }
	inline Texture2D_t3840446185 * get_Cancel_19() const { return ___Cancel_19; }
	inline Texture2D_t3840446185 ** get_address_of_Cancel_19() { return &___Cancel_19; }
	inline void set_Cancel_19(Texture2D_t3840446185 * value)
	{
		___Cancel_19 = value;
		Il2CppCodeGenWriteBarrier((&___Cancel_19), value);
	}

	inline static int32_t get_offset_of_Zoom_20() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Zoom_20)); }
	inline Texture2D_t3840446185 * get_Zoom_20() const { return ___Zoom_20; }
	inline Texture2D_t3840446185 ** get_address_of_Zoom_20() { return &___Zoom_20; }
	inline void set_Zoom_20(Texture2D_t3840446185 * value)
	{
		___Zoom_20 = value;
		Il2CppCodeGenWriteBarrier((&___Zoom_20), value);
	}

	inline static int32_t get_offset_of_Refresh_21() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Refresh_21)); }
	inline Texture2D_t3840446185 * get_Refresh_21() const { return ___Refresh_21; }
	inline Texture2D_t3840446185 ** get_address_of_Refresh_21() { return &___Refresh_21; }
	inline void set_Refresh_21(Texture2D_t3840446185 * value)
	{
		___Refresh_21 = value;
		Il2CppCodeGenWriteBarrier((&___Refresh_21), value);
	}

	inline static int32_t get_offset_of_Lock_22() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Lock_22)); }
	inline Texture2D_t3840446185 * get_Lock_22() const { return ___Lock_22; }
	inline Texture2D_t3840446185 ** get_address_of_Lock_22() { return &___Lock_22; }
	inline void set_Lock_22(Texture2D_t3840446185 * value)
	{
		___Lock_22 = value;
		Il2CppCodeGenWriteBarrier((&___Lock_22), value);
	}

	inline static int32_t get_offset_of_Accept_23() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Accept_23)); }
	inline Texture2D_t3840446185 * get_Accept_23() const { return ___Accept_23; }
	inline Texture2D_t3840446185 ** get_address_of_Accept_23() { return &___Accept_23; }
	inline void set_Accept_23(Texture2D_t3840446185 * value)
	{
		___Accept_23 = value;
		Il2CppCodeGenWriteBarrier((&___Accept_23), value);
	}

	inline static int32_t get_offset_of_OpenInNewWindow_24() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___OpenInNewWindow_24)); }
	inline Texture2D_t3840446185 * get_OpenInNewWindow_24() const { return ___OpenInNewWindow_24; }
	inline Texture2D_t3840446185 ** get_address_of_OpenInNewWindow_24() { return &___OpenInNewWindow_24; }
	inline void set_OpenInNewWindow_24(Texture2D_t3840446185 * value)
	{
		___OpenInNewWindow_24 = value;
		Il2CppCodeGenWriteBarrier((&___OpenInNewWindow_24), value);
	}

	inline static int32_t get_offset_of_Completed_25() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Completed_25)); }
	inline Texture2D_t3840446185 * get_Completed_25() const { return ___Completed_25; }
	inline Texture2D_t3840446185 ** get_address_of_Completed_25() { return &___Completed_25; }
	inline void set_Completed_25(Texture2D_t3840446185 * value)
	{
		___Completed_25 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_25), value);
	}

	inline static int32_t get_offset_of_Error_26() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Error_26)); }
	inline Texture2D_t3840446185 * get_Error_26() const { return ___Error_26; }
	inline Texture2D_t3840446185 ** get_address_of_Error_26() { return &___Error_26; }
	inline void set_Error_26(Texture2D_t3840446185 * value)
	{
		___Error_26 = value;
		Il2CppCodeGenWriteBarrier((&___Error_26), value);
	}

	inline static int32_t get_offset_of_Contact_27() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Contact_27)); }
	inline Texture2D_t3840446185 * get_Contact_27() const { return ___Contact_27; }
	inline Texture2D_t3840446185 ** get_address_of_Contact_27() { return &___Contact_27; }
	inline void set_Contact_27(Texture2D_t3840446185 * value)
	{
		___Contact_27 = value;
		Il2CppCodeGenWriteBarrier((&___Contact_27), value);
	}

	inline static int32_t get_offset_of_Volume_28() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Volume_28)); }
	inline Texture2D_t3840446185 * get_Volume_28() const { return ___Volume_28; }
	inline Texture2D_t3840446185 ** get_address_of_Volume_28() { return &___Volume_28; }
	inline void set_Volume_28(Texture2D_t3840446185 * value)
	{
		___Volume_28 = value;
		Il2CppCodeGenWriteBarrier((&___Volume_28), value);
	}

	inline static int32_t get_offset_of_KeyboardClassic_29() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___KeyboardClassic_29)); }
	inline Texture2D_t3840446185 * get_KeyboardClassic_29() const { return ___KeyboardClassic_29; }
	inline Texture2D_t3840446185 ** get_address_of_KeyboardClassic_29() { return &___KeyboardClassic_29; }
	inline void set_KeyboardClassic_29(Texture2D_t3840446185 * value)
	{
		___KeyboardClassic_29 = value;
		Il2CppCodeGenWriteBarrier((&___KeyboardClassic_29), value);
	}

	inline static int32_t get_offset_of_Camera_30() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Camera_30)); }
	inline Texture2D_t3840446185 * get_Camera_30() const { return ___Camera_30; }
	inline Texture2D_t3840446185 ** get_address_of_Camera_30() { return &___Camera_30; }
	inline void set_Camera_30(Texture2D_t3840446185 * value)
	{
		___Camera_30 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_30), value);
	}

	inline static int32_t get_offset_of_Video_31() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Video_31)); }
	inline Texture2D_t3840446185 * get_Video_31() const { return ___Video_31; }
	inline Texture2D_t3840446185 ** get_address_of_Video_31() { return &___Video_31; }
	inline void set_Video_31(Texture2D_t3840446185 * value)
	{
		___Video_31 = value;
		Il2CppCodeGenWriteBarrier((&___Video_31), value);
	}

	inline static int32_t get_offset_of_Microphone_32() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Microphone_32)); }
	inline Texture2D_t3840446185 * get_Microphone_32() const { return ___Microphone_32; }
	inline Texture2D_t3840446185 ** get_address_of_Microphone_32() { return &___Microphone_32; }
	inline void set_Microphone_32(Texture2D_t3840446185 * value)
	{
		___Microphone_32 = value;
		Il2CppCodeGenWriteBarrier((&___Microphone_32), value);
	}

	inline static int32_t get_offset_of_Ready_33() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Ready_33)); }
	inline Texture2D_t3840446185 * get_Ready_33() const { return ___Ready_33; }
	inline Texture2D_t3840446185 ** get_address_of_Ready_33() { return &___Ready_33; }
	inline void set_Ready_33(Texture2D_t3840446185 * value)
	{
		___Ready_33 = value;
		Il2CppCodeGenWriteBarrier((&___Ready_33), value);
	}

	inline static int32_t get_offset_of_AirTap_34() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___AirTap_34)); }
	inline Texture2D_t3840446185 * get_AirTap_34() const { return ___AirTap_34; }
	inline Texture2D_t3840446185 ** get_address_of_AirTap_34() { return &___AirTap_34; }
	inline void set_AirTap_34(Texture2D_t3840446185 * value)
	{
		___AirTap_34 = value;
		Il2CppCodeGenWriteBarrier((&___AirTap_34), value);
	}

	inline static int32_t get_offset_of_PressHold_35() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___PressHold_35)); }
	inline Texture2D_t3840446185 * get_PressHold_35() const { return ___PressHold_35; }
	inline Texture2D_t3840446185 ** get_address_of_PressHold_35() { return &___PressHold_35; }
	inline void set_PressHold_35(Texture2D_t3840446185 * value)
	{
		___PressHold_35 = value;
		Il2CppCodeGenWriteBarrier((&___PressHold_35), value);
	}

	inline static int32_t get_offset_of_Drag_36() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___Drag_36)); }
	inline Texture2D_t3840446185 * get_Drag_36() const { return ___Drag_36; }
	inline Texture2D_t3840446185 ** get_address_of_Drag_36() { return &___Drag_36; }
	inline void set_Drag_36(Texture2D_t3840446185 * value)
	{
		___Drag_36 = value;
		Il2CppCodeGenWriteBarrier((&___Drag_36), value);
	}

	inline static int32_t get_offset_of_TapToPlaceArt_37() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___TapToPlaceArt_37)); }
	inline Texture2D_t3840446185 * get_TapToPlaceArt_37() const { return ___TapToPlaceArt_37; }
	inline Texture2D_t3840446185 ** get_address_of_TapToPlaceArt_37() { return &___TapToPlaceArt_37; }
	inline void set_TapToPlaceArt_37(Texture2D_t3840446185 * value)
	{
		___TapToPlaceArt_37 = value;
		Il2CppCodeGenWriteBarrier((&___TapToPlaceArt_37), value);
	}

	inline static int32_t get_offset_of_AdjustWithHand_38() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___AdjustWithHand_38)); }
	inline Texture2D_t3840446185 * get_AdjustWithHand_38() const { return ___AdjustWithHand_38; }
	inline Texture2D_t3840446185 ** get_address_of_AdjustWithHand_38() { return &___AdjustWithHand_38; }
	inline void set_AdjustWithHand_38(Texture2D_t3840446185 * value)
	{
		___AdjustWithHand_38 = value;
		Il2CppCodeGenWriteBarrier((&___AdjustWithHand_38), value);
	}

	inline static int32_t get_offset_of_AdjustHologram_39() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___AdjustHologram_39)); }
	inline Texture2D_t3840446185 * get_AdjustHologram_39() const { return ___AdjustHologram_39; }
	inline Texture2D_t3840446185 ** get_address_of_AdjustHologram_39() { return &___AdjustHologram_39; }
	inline void set_AdjustHologram_39(Texture2D_t3840446185 * value)
	{
		___AdjustHologram_39 = value;
		Il2CppCodeGenWriteBarrier((&___AdjustHologram_39), value);
	}

	inline static int32_t get_offset_of_RemoveHologram_40() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___RemoveHologram_40)); }
	inline Texture2D_t3840446185 * get_RemoveHologram_40() const { return ___RemoveHologram_40; }
	inline Texture2D_t3840446185 ** get_address_of_RemoveHologram_40() { return &___RemoveHologram_40; }
	inline void set_RemoveHologram_40(Texture2D_t3840446185 * value)
	{
		___RemoveHologram_40 = value;
		Il2CppCodeGenWriteBarrier((&___RemoveHologram_40), value);
	}

	inline static int32_t get_offset_of_CustomIcons_41() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___CustomIcons_41)); }
	inline Texture2DU5BU5D_t149664596* get_CustomIcons_41() const { return ___CustomIcons_41; }
	inline Texture2DU5BU5D_t149664596** get_address_of_CustomIcons_41() { return &___CustomIcons_41; }
	inline void set_CustomIcons_41(Texture2DU5BU5D_t149664596* value)
	{
		___CustomIcons_41 = value;
		Il2CppCodeGenWriteBarrier((&___CustomIcons_41), value);
	}

	inline static int32_t get_offset_of_initialized_42() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___initialized_42)); }
	inline bool get_initialized_42() const { return ___initialized_42; }
	inline bool* get_address_of_initialized_42() { return &___initialized_42; }
	inline void set_initialized_42(bool value)
	{
		___initialized_42 = value;
	}

	inline static int32_t get_offset_of_iconKeys_43() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___iconKeys_43)); }
	inline List_1_t3319525431 * get_iconKeys_43() const { return ___iconKeys_43; }
	inline List_1_t3319525431 ** get_address_of_iconKeys_43() { return &___iconKeys_43; }
	inline void set_iconKeys_43(List_1_t3319525431 * value)
	{
		___iconKeys_43 = value;
		Il2CppCodeGenWriteBarrier((&___iconKeys_43), value);
	}

	inline static int32_t get_offset_of_iconLookup_44() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_t3085289291, ___iconLookup_44)); }
	inline Dictionary_2_t3625702484 * get_iconLookup_44() const { return ___iconLookup_44; }
	inline Dictionary_2_t3625702484 ** get_address_of_iconLookup_44() { return &___iconLookup_44; }
	inline void set_iconLookup_44(Dictionary_2_t3625702484 * value)
	{
		___iconLookup_44 = value;
		Il2CppCodeGenWriteBarrier((&___iconLookup_44), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONICONPROFILETEXTURE_T3085289291_H
#ifndef SPRITEBUTTON_T167139624_H
#define SPRITEBUTTON_T167139624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.SpriteButton
struct  SpriteButton_t167139624  : public Button_t2162584723
{
public:
	// HoloToolkit.Unity.Buttons.SpriteButtonDatum[] HoloToolkit.Unity.Buttons.SpriteButton::ButtonStates
	SpriteButtonDatumU5BU5D_t3985620692* ___ButtonStates_16;
	// UnityEngine.SpriteRenderer HoloToolkit.Unity.Buttons.SpriteButton::_renderer
	SpriteRenderer_t3235626157 * ____renderer_17;

public:
	inline static int32_t get_offset_of_ButtonStates_16() { return static_cast<int32_t>(offsetof(SpriteButton_t167139624, ___ButtonStates_16)); }
	inline SpriteButtonDatumU5BU5D_t3985620692* get_ButtonStates_16() const { return ___ButtonStates_16; }
	inline SpriteButtonDatumU5BU5D_t3985620692** get_address_of_ButtonStates_16() { return &___ButtonStates_16; }
	inline void set_ButtonStates_16(SpriteButtonDatumU5BU5D_t3985620692* value)
	{
		___ButtonStates_16 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonStates_16), value);
	}

	inline static int32_t get_offset_of__renderer_17() { return static_cast<int32_t>(offsetof(SpriteButton_t167139624, ____renderer_17)); }
	inline SpriteRenderer_t3235626157 * get__renderer_17() const { return ____renderer_17; }
	inline SpriteRenderer_t3235626157 ** get_address_of__renderer_17() { return &____renderer_17; }
	inline void set__renderer_17(SpriteRenderer_t3235626157 * value)
	{
		____renderer_17 = value;
		Il2CppCodeGenWriteBarrier((&____renderer_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEBUTTON_T167139624_H
#ifndef SOLVERSURFACEMAGNETISM_T615223073_H
#define SOLVERSURFACEMAGNETISM_T615223073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverSurfaceMagnetism
struct  SolverSurfaceMagnetism_t615223073  : public Solver_t4167981057
{
public:
	// UnityEngine.LayerMask HoloToolkit.Unity.SolverSurfaceMagnetism::MagneticSurface
	LayerMask_t3493934918  ___MagneticSurface_14;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::MaxDistance
	float ___MaxDistance_15;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::CloseDistance
	float ___CloseDistance_16;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::SurfaceNormalOffset
	float ___SurfaceNormalOffset_17;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::SurfaceRayOffset
	float ___SurfaceRayOffset_18;
	// HoloToolkit.Unity.SolverSurfaceMagnetism/RaycastModeEnum HoloToolkit.Unity.SolverSurfaceMagnetism::raycastMode
	int32_t ___raycastMode_19;
	// System.Int32 HoloToolkit.Unity.SolverSurfaceMagnetism::BoxRaysPerEdge
	int32_t ___BoxRaysPerEdge_20;
	// System.Boolean HoloToolkit.Unity.SolverSurfaceMagnetism::OrthoBoxCast
	bool ___OrthoBoxCast_21;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::MaximumNormalVariance
	float ___MaximumNormalVariance_22;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::SphereSize
	float ___SphereSize_23;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::VolumeCastSizeOverride
	float ___VolumeCastSizeOverride_24;
	// System.Boolean HoloToolkit.Unity.SolverSurfaceMagnetism::UseLinkedAltScaleOverride
	bool ___UseLinkedAltScaleOverride_25;
	// System.Boolean HoloToolkit.Unity.SolverSurfaceMagnetism::UseTexCoordNormals
	bool ___UseTexCoordNormals_26;
	// HoloToolkit.Unity.SolverSurfaceMagnetism/RaycastDirectionEnum HoloToolkit.Unity.SolverSurfaceMagnetism::raycastDirection
	int32_t ___raycastDirection_27;
	// HoloToolkit.Unity.SolverSurfaceMagnetism/OrientModeEnum HoloToolkit.Unity.SolverSurfaceMagnetism::orientationMode
	int32_t ___orientationMode_28;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::OrientBlend
	float ___OrientBlend_29;
	// System.Boolean HoloToolkit.Unity.SolverSurfaceMagnetism::OnSurface
	bool ___OnSurface_30;
	// UnityEngine.BoxCollider HoloToolkit.Unity.SolverSurfaceMagnetism::m_BoxCollider
	BoxCollider_t1640800422 * ___m_BoxCollider_31;

public:
	inline static int32_t get_offset_of_MagneticSurface_14() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___MagneticSurface_14)); }
	inline LayerMask_t3493934918  get_MagneticSurface_14() const { return ___MagneticSurface_14; }
	inline LayerMask_t3493934918 * get_address_of_MagneticSurface_14() { return &___MagneticSurface_14; }
	inline void set_MagneticSurface_14(LayerMask_t3493934918  value)
	{
		___MagneticSurface_14 = value;
	}

	inline static int32_t get_offset_of_MaxDistance_15() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___MaxDistance_15)); }
	inline float get_MaxDistance_15() const { return ___MaxDistance_15; }
	inline float* get_address_of_MaxDistance_15() { return &___MaxDistance_15; }
	inline void set_MaxDistance_15(float value)
	{
		___MaxDistance_15 = value;
	}

	inline static int32_t get_offset_of_CloseDistance_16() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___CloseDistance_16)); }
	inline float get_CloseDistance_16() const { return ___CloseDistance_16; }
	inline float* get_address_of_CloseDistance_16() { return &___CloseDistance_16; }
	inline void set_CloseDistance_16(float value)
	{
		___CloseDistance_16 = value;
	}

	inline static int32_t get_offset_of_SurfaceNormalOffset_17() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___SurfaceNormalOffset_17)); }
	inline float get_SurfaceNormalOffset_17() const { return ___SurfaceNormalOffset_17; }
	inline float* get_address_of_SurfaceNormalOffset_17() { return &___SurfaceNormalOffset_17; }
	inline void set_SurfaceNormalOffset_17(float value)
	{
		___SurfaceNormalOffset_17 = value;
	}

	inline static int32_t get_offset_of_SurfaceRayOffset_18() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___SurfaceRayOffset_18)); }
	inline float get_SurfaceRayOffset_18() const { return ___SurfaceRayOffset_18; }
	inline float* get_address_of_SurfaceRayOffset_18() { return &___SurfaceRayOffset_18; }
	inline void set_SurfaceRayOffset_18(float value)
	{
		___SurfaceRayOffset_18 = value;
	}

	inline static int32_t get_offset_of_raycastMode_19() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___raycastMode_19)); }
	inline int32_t get_raycastMode_19() const { return ___raycastMode_19; }
	inline int32_t* get_address_of_raycastMode_19() { return &___raycastMode_19; }
	inline void set_raycastMode_19(int32_t value)
	{
		___raycastMode_19 = value;
	}

	inline static int32_t get_offset_of_BoxRaysPerEdge_20() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___BoxRaysPerEdge_20)); }
	inline int32_t get_BoxRaysPerEdge_20() const { return ___BoxRaysPerEdge_20; }
	inline int32_t* get_address_of_BoxRaysPerEdge_20() { return &___BoxRaysPerEdge_20; }
	inline void set_BoxRaysPerEdge_20(int32_t value)
	{
		___BoxRaysPerEdge_20 = value;
	}

	inline static int32_t get_offset_of_OrthoBoxCast_21() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___OrthoBoxCast_21)); }
	inline bool get_OrthoBoxCast_21() const { return ___OrthoBoxCast_21; }
	inline bool* get_address_of_OrthoBoxCast_21() { return &___OrthoBoxCast_21; }
	inline void set_OrthoBoxCast_21(bool value)
	{
		___OrthoBoxCast_21 = value;
	}

	inline static int32_t get_offset_of_MaximumNormalVariance_22() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___MaximumNormalVariance_22)); }
	inline float get_MaximumNormalVariance_22() const { return ___MaximumNormalVariance_22; }
	inline float* get_address_of_MaximumNormalVariance_22() { return &___MaximumNormalVariance_22; }
	inline void set_MaximumNormalVariance_22(float value)
	{
		___MaximumNormalVariance_22 = value;
	}

	inline static int32_t get_offset_of_SphereSize_23() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___SphereSize_23)); }
	inline float get_SphereSize_23() const { return ___SphereSize_23; }
	inline float* get_address_of_SphereSize_23() { return &___SphereSize_23; }
	inline void set_SphereSize_23(float value)
	{
		___SphereSize_23 = value;
	}

	inline static int32_t get_offset_of_VolumeCastSizeOverride_24() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___VolumeCastSizeOverride_24)); }
	inline float get_VolumeCastSizeOverride_24() const { return ___VolumeCastSizeOverride_24; }
	inline float* get_address_of_VolumeCastSizeOverride_24() { return &___VolumeCastSizeOverride_24; }
	inline void set_VolumeCastSizeOverride_24(float value)
	{
		___VolumeCastSizeOverride_24 = value;
	}

	inline static int32_t get_offset_of_UseLinkedAltScaleOverride_25() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___UseLinkedAltScaleOverride_25)); }
	inline bool get_UseLinkedAltScaleOverride_25() const { return ___UseLinkedAltScaleOverride_25; }
	inline bool* get_address_of_UseLinkedAltScaleOverride_25() { return &___UseLinkedAltScaleOverride_25; }
	inline void set_UseLinkedAltScaleOverride_25(bool value)
	{
		___UseLinkedAltScaleOverride_25 = value;
	}

	inline static int32_t get_offset_of_UseTexCoordNormals_26() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___UseTexCoordNormals_26)); }
	inline bool get_UseTexCoordNormals_26() const { return ___UseTexCoordNormals_26; }
	inline bool* get_address_of_UseTexCoordNormals_26() { return &___UseTexCoordNormals_26; }
	inline void set_UseTexCoordNormals_26(bool value)
	{
		___UseTexCoordNormals_26 = value;
	}

	inline static int32_t get_offset_of_raycastDirection_27() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___raycastDirection_27)); }
	inline int32_t get_raycastDirection_27() const { return ___raycastDirection_27; }
	inline int32_t* get_address_of_raycastDirection_27() { return &___raycastDirection_27; }
	inline void set_raycastDirection_27(int32_t value)
	{
		___raycastDirection_27 = value;
	}

	inline static int32_t get_offset_of_orientationMode_28() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___orientationMode_28)); }
	inline int32_t get_orientationMode_28() const { return ___orientationMode_28; }
	inline int32_t* get_address_of_orientationMode_28() { return &___orientationMode_28; }
	inline void set_orientationMode_28(int32_t value)
	{
		___orientationMode_28 = value;
	}

	inline static int32_t get_offset_of_OrientBlend_29() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___OrientBlend_29)); }
	inline float get_OrientBlend_29() const { return ___OrientBlend_29; }
	inline float* get_address_of_OrientBlend_29() { return &___OrientBlend_29; }
	inline void set_OrientBlend_29(float value)
	{
		___OrientBlend_29 = value;
	}

	inline static int32_t get_offset_of_OnSurface_30() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___OnSurface_30)); }
	inline bool get_OnSurface_30() const { return ___OnSurface_30; }
	inline bool* get_address_of_OnSurface_30() { return &___OnSurface_30; }
	inline void set_OnSurface_30(bool value)
	{
		___OnSurface_30 = value;
	}

	inline static int32_t get_offset_of_m_BoxCollider_31() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_t615223073, ___m_BoxCollider_31)); }
	inline BoxCollider_t1640800422 * get_m_BoxCollider_31() const { return ___m_BoxCollider_31; }
	inline BoxCollider_t1640800422 ** get_address_of_m_BoxCollider_31() { return &___m_BoxCollider_31; }
	inline void set_m_BoxCollider_31(BoxCollider_t1640800422 * value)
	{
		___m_BoxCollider_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoxCollider_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVERSURFACEMAGNETISM_T615223073_H
#ifndef COMPOUNDBUTTONTEXT_T2169052765_H
#define COMPOUNDBUTTONTEXT_T2169052765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonText
struct  CompoundButtonText_t2169052765  : public ProfileButtonBase_1_t3611612999
{
public:
	// UnityEngine.TextMesh HoloToolkit.Unity.Buttons.CompoundButtonText::TextMesh
	TextMesh_t1536577757 * ___TextMesh_3;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonText::OverrideFontStyle
	bool ___OverrideFontStyle_4;
	// UnityEngine.FontStyle HoloToolkit.Unity.Buttons.CompoundButtonText::Style
	int32_t ___Style_5;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonText::OverrideAnchor
	bool ___OverrideAnchor_6;
	// UnityEngine.TextAnchor HoloToolkit.Unity.Buttons.CompoundButtonText::Anchor
	int32_t ___Anchor_7;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonText::OverrideSize
	bool ___OverrideSize_8;
	// System.Int32 HoloToolkit.Unity.Buttons.CompoundButtonText::Size
	int32_t ___Size_9;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonText::OverrideOffset
	bool ___OverrideOffset_10;
	// System.Single HoloToolkit.Unity.Buttons.CompoundButtonText::alpha
	float ___alpha_11;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonText::disableText
	bool ___disableText_12;

public:
	inline static int32_t get_offset_of_TextMesh_3() { return static_cast<int32_t>(offsetof(CompoundButtonText_t2169052765, ___TextMesh_3)); }
	inline TextMesh_t1536577757 * get_TextMesh_3() const { return ___TextMesh_3; }
	inline TextMesh_t1536577757 ** get_address_of_TextMesh_3() { return &___TextMesh_3; }
	inline void set_TextMesh_3(TextMesh_t1536577757 * value)
	{
		___TextMesh_3 = value;
		Il2CppCodeGenWriteBarrier((&___TextMesh_3), value);
	}

	inline static int32_t get_offset_of_OverrideFontStyle_4() { return static_cast<int32_t>(offsetof(CompoundButtonText_t2169052765, ___OverrideFontStyle_4)); }
	inline bool get_OverrideFontStyle_4() const { return ___OverrideFontStyle_4; }
	inline bool* get_address_of_OverrideFontStyle_4() { return &___OverrideFontStyle_4; }
	inline void set_OverrideFontStyle_4(bool value)
	{
		___OverrideFontStyle_4 = value;
	}

	inline static int32_t get_offset_of_Style_5() { return static_cast<int32_t>(offsetof(CompoundButtonText_t2169052765, ___Style_5)); }
	inline int32_t get_Style_5() const { return ___Style_5; }
	inline int32_t* get_address_of_Style_5() { return &___Style_5; }
	inline void set_Style_5(int32_t value)
	{
		___Style_5 = value;
	}

	inline static int32_t get_offset_of_OverrideAnchor_6() { return static_cast<int32_t>(offsetof(CompoundButtonText_t2169052765, ___OverrideAnchor_6)); }
	inline bool get_OverrideAnchor_6() const { return ___OverrideAnchor_6; }
	inline bool* get_address_of_OverrideAnchor_6() { return &___OverrideAnchor_6; }
	inline void set_OverrideAnchor_6(bool value)
	{
		___OverrideAnchor_6 = value;
	}

	inline static int32_t get_offset_of_Anchor_7() { return static_cast<int32_t>(offsetof(CompoundButtonText_t2169052765, ___Anchor_7)); }
	inline int32_t get_Anchor_7() const { return ___Anchor_7; }
	inline int32_t* get_address_of_Anchor_7() { return &___Anchor_7; }
	inline void set_Anchor_7(int32_t value)
	{
		___Anchor_7 = value;
	}

	inline static int32_t get_offset_of_OverrideSize_8() { return static_cast<int32_t>(offsetof(CompoundButtonText_t2169052765, ___OverrideSize_8)); }
	inline bool get_OverrideSize_8() const { return ___OverrideSize_8; }
	inline bool* get_address_of_OverrideSize_8() { return &___OverrideSize_8; }
	inline void set_OverrideSize_8(bool value)
	{
		___OverrideSize_8 = value;
	}

	inline static int32_t get_offset_of_Size_9() { return static_cast<int32_t>(offsetof(CompoundButtonText_t2169052765, ___Size_9)); }
	inline int32_t get_Size_9() const { return ___Size_9; }
	inline int32_t* get_address_of_Size_9() { return &___Size_9; }
	inline void set_Size_9(int32_t value)
	{
		___Size_9 = value;
	}

	inline static int32_t get_offset_of_OverrideOffset_10() { return static_cast<int32_t>(offsetof(CompoundButtonText_t2169052765, ___OverrideOffset_10)); }
	inline bool get_OverrideOffset_10() const { return ___OverrideOffset_10; }
	inline bool* get_address_of_OverrideOffset_10() { return &___OverrideOffset_10; }
	inline void set_OverrideOffset_10(bool value)
	{
		___OverrideOffset_10 = value;
	}

	inline static int32_t get_offset_of_alpha_11() { return static_cast<int32_t>(offsetof(CompoundButtonText_t2169052765, ___alpha_11)); }
	inline float get_alpha_11() const { return ___alpha_11; }
	inline float* get_address_of_alpha_11() { return &___alpha_11; }
	inline void set_alpha_11(float value)
	{
		___alpha_11 = value;
	}

	inline static int32_t get_offset_of_disableText_12() { return static_cast<int32_t>(offsetof(CompoundButtonText_t2169052765, ___disableText_12)); }
	inline bool get_disableText_12() const { return ___disableText_12; }
	inline bool* get_address_of_disableText_12() { return &___disableText_12; }
	inline void set_disableText_12(bool value)
	{
		___disableText_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTONTEXT_T2169052765_H
#ifndef COMPOUNDBUTTONSOUNDS_T1404604836_H
#define COMPOUNDBUTTONSOUNDS_T1404604836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonSounds
struct  CompoundButtonSounds_t1404604836  : public ProfileButtonBase_1_t3422457020
{
public:
	// UnityEngine.AudioSource HoloToolkit.Unity.Buttons.CompoundButtonSounds::audioSource
	AudioSource_t3935305588 * ___audioSource_4;
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.CompoundButtonSounds::lastState
	int32_t ___lastState_7;

public:
	inline static int32_t get_offset_of_audioSource_4() { return static_cast<int32_t>(offsetof(CompoundButtonSounds_t1404604836, ___audioSource_4)); }
	inline AudioSource_t3935305588 * get_audioSource_4() const { return ___audioSource_4; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_4() { return &___audioSource_4; }
	inline void set_audioSource_4(AudioSource_t3935305588 * value)
	{
		___audioSource_4 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_4), value);
	}

	inline static int32_t get_offset_of_lastState_7() { return static_cast<int32_t>(offsetof(CompoundButtonSounds_t1404604836, ___lastState_7)); }
	inline int32_t get_lastState_7() const { return ___lastState_7; }
	inline int32_t* get_address_of_lastState_7() { return &___lastState_7; }
	inline void set_lastState_7(int32_t value)
	{
		___lastState_7 = value;
	}
};

struct CompoundButtonSounds_t1404604836_StaticFields
{
public:
	// System.String HoloToolkit.Unity.Buttons.CompoundButtonSounds::lastClipName
	String_t* ___lastClipName_5;
	// System.Single HoloToolkit.Unity.Buttons.CompoundButtonSounds::lastClipTime
	float ___lastClipTime_6;

public:
	inline static int32_t get_offset_of_lastClipName_5() { return static_cast<int32_t>(offsetof(CompoundButtonSounds_t1404604836_StaticFields, ___lastClipName_5)); }
	inline String_t* get_lastClipName_5() const { return ___lastClipName_5; }
	inline String_t** get_address_of_lastClipName_5() { return &___lastClipName_5; }
	inline void set_lastClipName_5(String_t* value)
	{
		___lastClipName_5 = value;
		Il2CppCodeGenWriteBarrier((&___lastClipName_5), value);
	}

	inline static int32_t get_offset_of_lastClipTime_6() { return static_cast<int32_t>(offsetof(CompoundButtonSounds_t1404604836_StaticFields, ___lastClipTime_6)); }
	inline float get_lastClipTime_6() const { return ___lastClipTime_6; }
	inline float* get_address_of_lastClipTime_6() { return &___lastClipTime_6; }
	inline void set_lastClipTime_6(float value)
	{
		___lastClipTime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTONSOUNDS_T1404604836_H
#ifndef COMPOUNDBUTTONMESH_T1797344947_H
#define COMPOUNDBUTTONMESH_T1797344947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonMesh
struct  CompoundButtonMesh_t1797344947  : public ProfileButtonBase_1_t413189496
{
public:
	// UnityEngine.Transform HoloToolkit.Unity.Buttons.CompoundButtonMesh::TargetTransform
	Transform_t3600365921 * ___TargetTransform_4;
	// UnityEngine.MeshRenderer HoloToolkit.Unity.Buttons.CompoundButtonMesh::Renderer
	MeshRenderer_t587009260 * ___Renderer_5;
	// HoloToolkit.Unity.Buttons.CompoundButtonMesh/MeshButtonDatum HoloToolkit.Unity.Buttons.CompoundButtonMesh::currentDatum
	MeshButtonDatum_t3924860415 * ___currentDatum_6;
	// UnityEngine.Material HoloToolkit.Unity.Buttons.CompoundButtonMesh::instantiatedMaterial
	Material_t340375123 * ___instantiatedMaterial_7;
	// UnityEngine.Material HoloToolkit.Unity.Buttons.CompoundButtonMesh::sharedMaterial
	Material_t340375123 * ___sharedMaterial_8;
	// System.Single HoloToolkit.Unity.Buttons.CompoundButtonMesh::lastTimePressed
	float ___lastTimePressed_9;

public:
	inline static int32_t get_offset_of_TargetTransform_4() { return static_cast<int32_t>(offsetof(CompoundButtonMesh_t1797344947, ___TargetTransform_4)); }
	inline Transform_t3600365921 * get_TargetTransform_4() const { return ___TargetTransform_4; }
	inline Transform_t3600365921 ** get_address_of_TargetTransform_4() { return &___TargetTransform_4; }
	inline void set_TargetTransform_4(Transform_t3600365921 * value)
	{
		___TargetTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___TargetTransform_4), value);
	}

	inline static int32_t get_offset_of_Renderer_5() { return static_cast<int32_t>(offsetof(CompoundButtonMesh_t1797344947, ___Renderer_5)); }
	inline MeshRenderer_t587009260 * get_Renderer_5() const { return ___Renderer_5; }
	inline MeshRenderer_t587009260 ** get_address_of_Renderer_5() { return &___Renderer_5; }
	inline void set_Renderer_5(MeshRenderer_t587009260 * value)
	{
		___Renderer_5 = value;
		Il2CppCodeGenWriteBarrier((&___Renderer_5), value);
	}

	inline static int32_t get_offset_of_currentDatum_6() { return static_cast<int32_t>(offsetof(CompoundButtonMesh_t1797344947, ___currentDatum_6)); }
	inline MeshButtonDatum_t3924860415 * get_currentDatum_6() const { return ___currentDatum_6; }
	inline MeshButtonDatum_t3924860415 ** get_address_of_currentDatum_6() { return &___currentDatum_6; }
	inline void set_currentDatum_6(MeshButtonDatum_t3924860415 * value)
	{
		___currentDatum_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentDatum_6), value);
	}

	inline static int32_t get_offset_of_instantiatedMaterial_7() { return static_cast<int32_t>(offsetof(CompoundButtonMesh_t1797344947, ___instantiatedMaterial_7)); }
	inline Material_t340375123 * get_instantiatedMaterial_7() const { return ___instantiatedMaterial_7; }
	inline Material_t340375123 ** get_address_of_instantiatedMaterial_7() { return &___instantiatedMaterial_7; }
	inline void set_instantiatedMaterial_7(Material_t340375123 * value)
	{
		___instantiatedMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___instantiatedMaterial_7), value);
	}

	inline static int32_t get_offset_of_sharedMaterial_8() { return static_cast<int32_t>(offsetof(CompoundButtonMesh_t1797344947, ___sharedMaterial_8)); }
	inline Material_t340375123 * get_sharedMaterial_8() const { return ___sharedMaterial_8; }
	inline Material_t340375123 ** get_address_of_sharedMaterial_8() { return &___sharedMaterial_8; }
	inline void set_sharedMaterial_8(Material_t340375123 * value)
	{
		___sharedMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___sharedMaterial_8), value);
	}

	inline static int32_t get_offset_of_lastTimePressed_9() { return static_cast<int32_t>(offsetof(CompoundButtonMesh_t1797344947, ___lastTimePressed_9)); }
	inline float get_lastTimePressed_9() const { return ___lastTimePressed_9; }
	inline float* get_address_of_lastTimePressed_9() { return &___lastTimePressed_9; }
	inline void set_lastTimePressed_9(float value)
	{
		___lastTimePressed_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTONMESH_T1797344947_H
#ifndef LINEMESHES_T1824619843_H
#define LINEMESHES_T1824619843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.LineMeshes
struct  LineMeshes_t1824619843  : public LineRendererBase_t210867989
{
public:
	// System.String HoloToolkit.Unity.UX.LineMeshes::InvisibleShaderName
	String_t* ___InvisibleShaderName_16;
	// UnityEngine.Mesh HoloToolkit.Unity.UX.LineMeshes::LineMesh
	Mesh_t3648964284 * ___LineMesh_17;
	// UnityEngine.Material HoloToolkit.Unity.UX.LineMeshes::LineMaterial
	Material_t340375123 * ___LineMaterial_18;
	// System.String HoloToolkit.Unity.UX.LineMeshes::ColorProp
	String_t* ___ColorProp_19;
	// UnityEngine.MaterialPropertyBlock HoloToolkit.Unity.UX.LineMeshes::linePropertyBlock
	MaterialPropertyBlock_t3213117958 * ___linePropertyBlock_20;
	// System.Int32 HoloToolkit.Unity.UX.LineMeshes::colorID
	int32_t ___colorID_21;
	// UnityEngine.Matrix4x4[] HoloToolkit.Unity.UX.LineMeshes::meshTransforms
	Matrix4x4U5BU5D_t2302988098* ___meshTransforms_22;
	// UnityEngine.Vector4[] HoloToolkit.Unity.UX.LineMeshes::colorValues
	Vector4U5BU5D_t934056436* ___colorValues_23;
	// System.Boolean HoloToolkit.Unity.UX.LineMeshes::executeCommandBuffer
	bool ___executeCommandBuffer_24;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Rendering.CommandBuffer> HoloToolkit.Unity.UX.LineMeshes::cameras
	Dictionary_2_t2419791724 * ___cameras_25;
	// UnityEngine.MeshRenderer HoloToolkit.Unity.UX.LineMeshes::onWillRenderHelper
	MeshRenderer_t587009260 * ___onWillRenderHelper_26;
	// UnityEngine.Mesh HoloToolkit.Unity.UX.LineMeshes::onWillRenderMesh
	Mesh_t3648964284 * ___onWillRenderMesh_27;
	// UnityEngine.Material HoloToolkit.Unity.UX.LineMeshes::onWillRenderMat
	Material_t340375123 * ___onWillRenderMat_28;
	// UnityEngine.Vector3[] HoloToolkit.Unity.UX.LineMeshes::meshVertices
	Vector3U5BU5D_t1718750761* ___meshVertices_29;

public:
	inline static int32_t get_offset_of_InvisibleShaderName_16() { return static_cast<int32_t>(offsetof(LineMeshes_t1824619843, ___InvisibleShaderName_16)); }
	inline String_t* get_InvisibleShaderName_16() const { return ___InvisibleShaderName_16; }
	inline String_t** get_address_of_InvisibleShaderName_16() { return &___InvisibleShaderName_16; }
	inline void set_InvisibleShaderName_16(String_t* value)
	{
		___InvisibleShaderName_16 = value;
		Il2CppCodeGenWriteBarrier((&___InvisibleShaderName_16), value);
	}

	inline static int32_t get_offset_of_LineMesh_17() { return static_cast<int32_t>(offsetof(LineMeshes_t1824619843, ___LineMesh_17)); }
	inline Mesh_t3648964284 * get_LineMesh_17() const { return ___LineMesh_17; }
	inline Mesh_t3648964284 ** get_address_of_LineMesh_17() { return &___LineMesh_17; }
	inline void set_LineMesh_17(Mesh_t3648964284 * value)
	{
		___LineMesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___LineMesh_17), value);
	}

	inline static int32_t get_offset_of_LineMaterial_18() { return static_cast<int32_t>(offsetof(LineMeshes_t1824619843, ___LineMaterial_18)); }
	inline Material_t340375123 * get_LineMaterial_18() const { return ___LineMaterial_18; }
	inline Material_t340375123 ** get_address_of_LineMaterial_18() { return &___LineMaterial_18; }
	inline void set_LineMaterial_18(Material_t340375123 * value)
	{
		___LineMaterial_18 = value;
		Il2CppCodeGenWriteBarrier((&___LineMaterial_18), value);
	}

	inline static int32_t get_offset_of_ColorProp_19() { return static_cast<int32_t>(offsetof(LineMeshes_t1824619843, ___ColorProp_19)); }
	inline String_t* get_ColorProp_19() const { return ___ColorProp_19; }
	inline String_t** get_address_of_ColorProp_19() { return &___ColorProp_19; }
	inline void set_ColorProp_19(String_t* value)
	{
		___ColorProp_19 = value;
		Il2CppCodeGenWriteBarrier((&___ColorProp_19), value);
	}

	inline static int32_t get_offset_of_linePropertyBlock_20() { return static_cast<int32_t>(offsetof(LineMeshes_t1824619843, ___linePropertyBlock_20)); }
	inline MaterialPropertyBlock_t3213117958 * get_linePropertyBlock_20() const { return ___linePropertyBlock_20; }
	inline MaterialPropertyBlock_t3213117958 ** get_address_of_linePropertyBlock_20() { return &___linePropertyBlock_20; }
	inline void set_linePropertyBlock_20(MaterialPropertyBlock_t3213117958 * value)
	{
		___linePropertyBlock_20 = value;
		Il2CppCodeGenWriteBarrier((&___linePropertyBlock_20), value);
	}

	inline static int32_t get_offset_of_colorID_21() { return static_cast<int32_t>(offsetof(LineMeshes_t1824619843, ___colorID_21)); }
	inline int32_t get_colorID_21() const { return ___colorID_21; }
	inline int32_t* get_address_of_colorID_21() { return &___colorID_21; }
	inline void set_colorID_21(int32_t value)
	{
		___colorID_21 = value;
	}

	inline static int32_t get_offset_of_meshTransforms_22() { return static_cast<int32_t>(offsetof(LineMeshes_t1824619843, ___meshTransforms_22)); }
	inline Matrix4x4U5BU5D_t2302988098* get_meshTransforms_22() const { return ___meshTransforms_22; }
	inline Matrix4x4U5BU5D_t2302988098** get_address_of_meshTransforms_22() { return &___meshTransforms_22; }
	inline void set_meshTransforms_22(Matrix4x4U5BU5D_t2302988098* value)
	{
		___meshTransforms_22 = value;
		Il2CppCodeGenWriteBarrier((&___meshTransforms_22), value);
	}

	inline static int32_t get_offset_of_colorValues_23() { return static_cast<int32_t>(offsetof(LineMeshes_t1824619843, ___colorValues_23)); }
	inline Vector4U5BU5D_t934056436* get_colorValues_23() const { return ___colorValues_23; }
	inline Vector4U5BU5D_t934056436** get_address_of_colorValues_23() { return &___colorValues_23; }
	inline void set_colorValues_23(Vector4U5BU5D_t934056436* value)
	{
		___colorValues_23 = value;
		Il2CppCodeGenWriteBarrier((&___colorValues_23), value);
	}

	inline static int32_t get_offset_of_executeCommandBuffer_24() { return static_cast<int32_t>(offsetof(LineMeshes_t1824619843, ___executeCommandBuffer_24)); }
	inline bool get_executeCommandBuffer_24() const { return ___executeCommandBuffer_24; }
	inline bool* get_address_of_executeCommandBuffer_24() { return &___executeCommandBuffer_24; }
	inline void set_executeCommandBuffer_24(bool value)
	{
		___executeCommandBuffer_24 = value;
	}

	inline static int32_t get_offset_of_cameras_25() { return static_cast<int32_t>(offsetof(LineMeshes_t1824619843, ___cameras_25)); }
	inline Dictionary_2_t2419791724 * get_cameras_25() const { return ___cameras_25; }
	inline Dictionary_2_t2419791724 ** get_address_of_cameras_25() { return &___cameras_25; }
	inline void set_cameras_25(Dictionary_2_t2419791724 * value)
	{
		___cameras_25 = value;
		Il2CppCodeGenWriteBarrier((&___cameras_25), value);
	}

	inline static int32_t get_offset_of_onWillRenderHelper_26() { return static_cast<int32_t>(offsetof(LineMeshes_t1824619843, ___onWillRenderHelper_26)); }
	inline MeshRenderer_t587009260 * get_onWillRenderHelper_26() const { return ___onWillRenderHelper_26; }
	inline MeshRenderer_t587009260 ** get_address_of_onWillRenderHelper_26() { return &___onWillRenderHelper_26; }
	inline void set_onWillRenderHelper_26(MeshRenderer_t587009260 * value)
	{
		___onWillRenderHelper_26 = value;
		Il2CppCodeGenWriteBarrier((&___onWillRenderHelper_26), value);
	}

	inline static int32_t get_offset_of_onWillRenderMesh_27() { return static_cast<int32_t>(offsetof(LineMeshes_t1824619843, ___onWillRenderMesh_27)); }
	inline Mesh_t3648964284 * get_onWillRenderMesh_27() const { return ___onWillRenderMesh_27; }
	inline Mesh_t3648964284 ** get_address_of_onWillRenderMesh_27() { return &___onWillRenderMesh_27; }
	inline void set_onWillRenderMesh_27(Mesh_t3648964284 * value)
	{
		___onWillRenderMesh_27 = value;
		Il2CppCodeGenWriteBarrier((&___onWillRenderMesh_27), value);
	}

	inline static int32_t get_offset_of_onWillRenderMat_28() { return static_cast<int32_t>(offsetof(LineMeshes_t1824619843, ___onWillRenderMat_28)); }
	inline Material_t340375123 * get_onWillRenderMat_28() const { return ___onWillRenderMat_28; }
	inline Material_t340375123 ** get_address_of_onWillRenderMat_28() { return &___onWillRenderMat_28; }
	inline void set_onWillRenderMat_28(Material_t340375123 * value)
	{
		___onWillRenderMat_28 = value;
		Il2CppCodeGenWriteBarrier((&___onWillRenderMat_28), value);
	}

	inline static int32_t get_offset_of_meshVertices_29() { return static_cast<int32_t>(offsetof(LineMeshes_t1824619843, ___meshVertices_29)); }
	inline Vector3U5BU5D_t1718750761* get_meshVertices_29() const { return ___meshVertices_29; }
	inline Vector3U5BU5D_t1718750761** get_address_of_meshVertices_29() { return &___meshVertices_29; }
	inline void set_meshVertices_29(Vector3U5BU5D_t1718750761* value)
	{
		___meshVertices_29 = value;
		Il2CppCodeGenWriteBarrier((&___meshVertices_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEMESHES_T1824619843_H
#ifndef BEZIER_T2227656807_H
#define BEZIER_T2227656807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Bezier
struct  Bezier_t2227656807  : public LineBase_t3631573702
{
public:
	// HoloToolkit.Unity.UX.Bezier/PointSet HoloToolkit.Unity.UX.Bezier::points
	PointSet_t452872835  ___points_17;

public:
	inline static int32_t get_offset_of_points_17() { return static_cast<int32_t>(offsetof(Bezier_t2227656807, ___points_17)); }
	inline PointSet_t452872835  get_points_17() const { return ___points_17; }
	inline PointSet_t452872835 * get_address_of_points_17() { return &___points_17; }
	inline void set_points_17(PointSet_t452872835  value)
	{
		___points_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEZIER_T2227656807_H
#ifndef ELLIPSE_T1737437017_H
#define ELLIPSE_T1737437017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Ellipse
struct  Ellipse_t1737437017  : public LineBase_t3631573702
{
public:
	// System.Int32 HoloToolkit.Unity.UX.Ellipse::Resolution
	int32_t ___Resolution_18;
	// UnityEngine.Vector2 HoloToolkit.Unity.UX.Ellipse::Radius
	Vector2_t2156229523  ___Radius_19;

public:
	inline static int32_t get_offset_of_Resolution_18() { return static_cast<int32_t>(offsetof(Ellipse_t1737437017, ___Resolution_18)); }
	inline int32_t get_Resolution_18() const { return ___Resolution_18; }
	inline int32_t* get_address_of_Resolution_18() { return &___Resolution_18; }
	inline void set_Resolution_18(int32_t value)
	{
		___Resolution_18 = value;
	}

	inline static int32_t get_offset_of_Radius_19() { return static_cast<int32_t>(offsetof(Ellipse_t1737437017, ___Radius_19)); }
	inline Vector2_t2156229523  get_Radius_19() const { return ___Radius_19; }
	inline Vector2_t2156229523 * get_address_of_Radius_19() { return &___Radius_19; }
	inline void set_Radius_19(Vector2_t2156229523  value)
	{
		___Radius_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELLIPSE_T1737437017_H
#ifndef TOGGLEACTIVERECEIVER_T2322331634_H
#define TOGGLEACTIVERECEIVER_T2322331634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Receivers.ToggleActiveReceiver
struct  ToggleActiveReceiver_t2322331634  : public InteractionReceiver_t3572119366
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEACTIVERECEIVER_T2322331634_H
#ifndef COMPOUNDBUTTON_T147810562_H
#define COMPOUNDBUTTON_T147810562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButton
struct  CompoundButton_t147810562  : public Button_t2162584723
{
public:
	// UnityEngine.Collider HoloToolkit.Unity.Buttons.CompoundButton::MainCollider
	Collider_t1773347010 * ___MainCollider_16;
	// UnityEngine.MeshRenderer HoloToolkit.Unity.Buttons.CompoundButton::MainRenderer
	MeshRenderer_t587009260 * ___MainRenderer_17;

public:
	inline static int32_t get_offset_of_MainCollider_16() { return static_cast<int32_t>(offsetof(CompoundButton_t147810562, ___MainCollider_16)); }
	inline Collider_t1773347010 * get_MainCollider_16() const { return ___MainCollider_16; }
	inline Collider_t1773347010 ** get_address_of_MainCollider_16() { return &___MainCollider_16; }
	inline void set_MainCollider_16(Collider_t1773347010 * value)
	{
		___MainCollider_16 = value;
		Il2CppCodeGenWriteBarrier((&___MainCollider_16), value);
	}

	inline static int32_t get_offset_of_MainRenderer_17() { return static_cast<int32_t>(offsetof(CompoundButton_t147810562, ___MainRenderer_17)); }
	inline MeshRenderer_t587009260 * get_MainRenderer_17() const { return ___MainRenderer_17; }
	inline MeshRenderer_t587009260 ** get_address_of_MainRenderer_17() { return &___MainRenderer_17; }
	inline void set_MainRenderer_17(MeshRenderer_t587009260 * value)
	{
		___MainRenderer_17 = value;
		Il2CppCodeGenWriteBarrier((&___MainRenderer_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTON_T147810562_H
#ifndef ANIMCONTROLLERBUTTON_T4085404257_H
#define ANIMCONTROLLERBUTTON_T4085404257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.AnimControllerButton
struct  AnimControllerButton_t4085404257  : public Button_t2162584723
{
public:
	// HoloToolkit.Unity.Buttons.AnimatorControllerAction[] HoloToolkit.Unity.Buttons.AnimControllerButton::AnimActions
	AnimatorControllerActionU5BU5D_t4171275305* ___AnimActions_16;
	// UnityEngine.Animator HoloToolkit.Unity.Buttons.AnimControllerButton::_animator
	Animator_t434523843 * ____animator_17;

public:
	inline static int32_t get_offset_of_AnimActions_16() { return static_cast<int32_t>(offsetof(AnimControllerButton_t4085404257, ___AnimActions_16)); }
	inline AnimatorControllerActionU5BU5D_t4171275305* get_AnimActions_16() const { return ___AnimActions_16; }
	inline AnimatorControllerActionU5BU5D_t4171275305** get_address_of_AnimActions_16() { return &___AnimActions_16; }
	inline void set_AnimActions_16(AnimatorControllerActionU5BU5D_t4171275305* value)
	{
		___AnimActions_16 = value;
		Il2CppCodeGenWriteBarrier((&___AnimActions_16), value);
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(AnimControllerButton_t4085404257, ____animator_17)); }
	inline Animator_t434523843 * get__animator_17() const { return ____animator_17; }
	inline Animator_t434523843 ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_t434523843 * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier((&____animator_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMCONTROLLERBUTTON_T4085404257_H
#ifndef COMPOUNDBUTTONICON_T3845426619_H
#define COMPOUNDBUTTONICON_T3845426619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonIcon
struct  CompoundButtonIcon_t3845426619  : public ProfileButtonBase_1_t2022168353
{
public:
	// UnityEngine.MeshRenderer HoloToolkit.Unity.Buttons.CompoundButtonIcon::targetIconRenderer
	MeshRenderer_t587009260 * ___targetIconRenderer_3;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonIcon::DisableIcon
	bool ___DisableIcon_4;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonIcon::OverrideIcon
	bool ___OverrideIcon_5;
	// System.String HoloToolkit.Unity.Buttons.CompoundButtonIcon::iconName
	String_t* ___iconName_6;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.CompoundButtonIcon::iconOverride
	Texture2D_t3840446185 * ___iconOverride_7;
	// System.Single HoloToolkit.Unity.Buttons.CompoundButtonIcon::alpha
	float ___alpha_8;
	// UnityEngine.Material HoloToolkit.Unity.Buttons.CompoundButtonIcon::instantiatedMaterial
	Material_t340375123 * ___instantiatedMaterial_9;
	// UnityEngine.Mesh HoloToolkit.Unity.Buttons.CompoundButtonIcon::instantiatedMesh
	Mesh_t3648964284 * ___instantiatedMesh_10;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonIcon::updatingAlpha
	bool ___updatingAlpha_11;
	// System.Single HoloToolkit.Unity.Buttons.CompoundButtonIcon::alphaTarget
	float ___alphaTarget_12;

public:
	inline static int32_t get_offset_of_targetIconRenderer_3() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_t3845426619, ___targetIconRenderer_3)); }
	inline MeshRenderer_t587009260 * get_targetIconRenderer_3() const { return ___targetIconRenderer_3; }
	inline MeshRenderer_t587009260 ** get_address_of_targetIconRenderer_3() { return &___targetIconRenderer_3; }
	inline void set_targetIconRenderer_3(MeshRenderer_t587009260 * value)
	{
		___targetIconRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___targetIconRenderer_3), value);
	}

	inline static int32_t get_offset_of_DisableIcon_4() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_t3845426619, ___DisableIcon_4)); }
	inline bool get_DisableIcon_4() const { return ___DisableIcon_4; }
	inline bool* get_address_of_DisableIcon_4() { return &___DisableIcon_4; }
	inline void set_DisableIcon_4(bool value)
	{
		___DisableIcon_4 = value;
	}

	inline static int32_t get_offset_of_OverrideIcon_5() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_t3845426619, ___OverrideIcon_5)); }
	inline bool get_OverrideIcon_5() const { return ___OverrideIcon_5; }
	inline bool* get_address_of_OverrideIcon_5() { return &___OverrideIcon_5; }
	inline void set_OverrideIcon_5(bool value)
	{
		___OverrideIcon_5 = value;
	}

	inline static int32_t get_offset_of_iconName_6() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_t3845426619, ___iconName_6)); }
	inline String_t* get_iconName_6() const { return ___iconName_6; }
	inline String_t** get_address_of_iconName_6() { return &___iconName_6; }
	inline void set_iconName_6(String_t* value)
	{
		___iconName_6 = value;
		Il2CppCodeGenWriteBarrier((&___iconName_6), value);
	}

	inline static int32_t get_offset_of_iconOverride_7() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_t3845426619, ___iconOverride_7)); }
	inline Texture2D_t3840446185 * get_iconOverride_7() const { return ___iconOverride_7; }
	inline Texture2D_t3840446185 ** get_address_of_iconOverride_7() { return &___iconOverride_7; }
	inline void set_iconOverride_7(Texture2D_t3840446185 * value)
	{
		___iconOverride_7 = value;
		Il2CppCodeGenWriteBarrier((&___iconOverride_7), value);
	}

	inline static int32_t get_offset_of_alpha_8() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_t3845426619, ___alpha_8)); }
	inline float get_alpha_8() const { return ___alpha_8; }
	inline float* get_address_of_alpha_8() { return &___alpha_8; }
	inline void set_alpha_8(float value)
	{
		___alpha_8 = value;
	}

	inline static int32_t get_offset_of_instantiatedMaterial_9() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_t3845426619, ___instantiatedMaterial_9)); }
	inline Material_t340375123 * get_instantiatedMaterial_9() const { return ___instantiatedMaterial_9; }
	inline Material_t340375123 ** get_address_of_instantiatedMaterial_9() { return &___instantiatedMaterial_9; }
	inline void set_instantiatedMaterial_9(Material_t340375123 * value)
	{
		___instantiatedMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___instantiatedMaterial_9), value);
	}

	inline static int32_t get_offset_of_instantiatedMesh_10() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_t3845426619, ___instantiatedMesh_10)); }
	inline Mesh_t3648964284 * get_instantiatedMesh_10() const { return ___instantiatedMesh_10; }
	inline Mesh_t3648964284 ** get_address_of_instantiatedMesh_10() { return &___instantiatedMesh_10; }
	inline void set_instantiatedMesh_10(Mesh_t3648964284 * value)
	{
		___instantiatedMesh_10 = value;
		Il2CppCodeGenWriteBarrier((&___instantiatedMesh_10), value);
	}

	inline static int32_t get_offset_of_updatingAlpha_11() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_t3845426619, ___updatingAlpha_11)); }
	inline bool get_updatingAlpha_11() const { return ___updatingAlpha_11; }
	inline bool* get_address_of_updatingAlpha_11() { return &___updatingAlpha_11; }
	inline void set_updatingAlpha_11(bool value)
	{
		___updatingAlpha_11 = value;
	}

	inline static int32_t get_offset_of_alphaTarget_12() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_t3845426619, ___alphaTarget_12)); }
	inline float get_alphaTarget_12() const { return ___alphaTarget_12; }
	inline float* get_address_of_alphaTarget_12() { return &___alphaTarget_12; }
	inline void set_alphaTarget_12(float value)
	{
		___alphaTarget_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTONICON_T3845426619_H
#ifndef DISTORTERBULGE_T4206281855_H
#define DISTORTERBULGE_T4206281855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.DistorterBulge
struct  DistorterBulge_t4206281855  : public Distorter_t3784015034
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterBulge::bulgeCenter
	Vector3_t3722313464  ___bulgeCenter_4;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.DistorterBulge::bulgeFalloff
	AnimationCurve_t3046754366 * ___bulgeFalloff_5;
	// System.Single HoloToolkit.Unity.UX.DistorterBulge::bulgeRadius
	float ___bulgeRadius_6;
	// System.Single HoloToolkit.Unity.UX.DistorterBulge::scaleDistort
	float ___scaleDistort_7;
	// System.Single HoloToolkit.Unity.UX.DistorterBulge::bulgeStrength
	float ___bulgeStrength_8;

public:
	inline static int32_t get_offset_of_bulgeCenter_4() { return static_cast<int32_t>(offsetof(DistorterBulge_t4206281855, ___bulgeCenter_4)); }
	inline Vector3_t3722313464  get_bulgeCenter_4() const { return ___bulgeCenter_4; }
	inline Vector3_t3722313464 * get_address_of_bulgeCenter_4() { return &___bulgeCenter_4; }
	inline void set_bulgeCenter_4(Vector3_t3722313464  value)
	{
		___bulgeCenter_4 = value;
	}

	inline static int32_t get_offset_of_bulgeFalloff_5() { return static_cast<int32_t>(offsetof(DistorterBulge_t4206281855, ___bulgeFalloff_5)); }
	inline AnimationCurve_t3046754366 * get_bulgeFalloff_5() const { return ___bulgeFalloff_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_bulgeFalloff_5() { return &___bulgeFalloff_5; }
	inline void set_bulgeFalloff_5(AnimationCurve_t3046754366 * value)
	{
		___bulgeFalloff_5 = value;
		Il2CppCodeGenWriteBarrier((&___bulgeFalloff_5), value);
	}

	inline static int32_t get_offset_of_bulgeRadius_6() { return static_cast<int32_t>(offsetof(DistorterBulge_t4206281855, ___bulgeRadius_6)); }
	inline float get_bulgeRadius_6() const { return ___bulgeRadius_6; }
	inline float* get_address_of_bulgeRadius_6() { return &___bulgeRadius_6; }
	inline void set_bulgeRadius_6(float value)
	{
		___bulgeRadius_6 = value;
	}

	inline static int32_t get_offset_of_scaleDistort_7() { return static_cast<int32_t>(offsetof(DistorterBulge_t4206281855, ___scaleDistort_7)); }
	inline float get_scaleDistort_7() const { return ___scaleDistort_7; }
	inline float* get_address_of_scaleDistort_7() { return &___scaleDistort_7; }
	inline void set_scaleDistort_7(float value)
	{
		___scaleDistort_7 = value;
	}

	inline static int32_t get_offset_of_bulgeStrength_8() { return static_cast<int32_t>(offsetof(DistorterBulge_t4206281855, ___bulgeStrength_8)); }
	inline float get_bulgeStrength_8() const { return ___bulgeStrength_8; }
	inline float* get_address_of_bulgeStrength_8() { return &___bulgeStrength_8; }
	inline void set_bulgeStrength_8(float value)
	{
		___bulgeStrength_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTERBULGE_T4206281855_H
#ifndef TIMERSCHEDULER_T541408523_H
#define TIMERSCHEDULER_T541408523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TimerScheduler
struct  TimerScheduler_t541408523  : public Singleton_1_t941632977
{
public:
	// HoloToolkit.Unity.PriorityQueue`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData> HoloToolkit.Unity.TimerScheduler::timers
	PriorityQueue_2_t107015337 * ___timers_4;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.TimerScheduler/TimerData> HoloToolkit.Unity.TimerScheduler::deferredTimers
	List_1_t1233822936 * ___deferredTimers_5;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.TimerScheduler/TimerIdPair> HoloToolkit.Unity.TimerScheduler::activeTimers
	List_1_t2393398647 * ___activeTimers_6;
	// System.Int32 HoloToolkit.Unity.TimerScheduler::nextTimerId
	int32_t ___nextTimerId_7;

public:
	inline static int32_t get_offset_of_timers_4() { return static_cast<int32_t>(offsetof(TimerScheduler_t541408523, ___timers_4)); }
	inline PriorityQueue_2_t107015337 * get_timers_4() const { return ___timers_4; }
	inline PriorityQueue_2_t107015337 ** get_address_of_timers_4() { return &___timers_4; }
	inline void set_timers_4(PriorityQueue_2_t107015337 * value)
	{
		___timers_4 = value;
		Il2CppCodeGenWriteBarrier((&___timers_4), value);
	}

	inline static int32_t get_offset_of_deferredTimers_5() { return static_cast<int32_t>(offsetof(TimerScheduler_t541408523, ___deferredTimers_5)); }
	inline List_1_t1233822936 * get_deferredTimers_5() const { return ___deferredTimers_5; }
	inline List_1_t1233822936 ** get_address_of_deferredTimers_5() { return &___deferredTimers_5; }
	inline void set_deferredTimers_5(List_1_t1233822936 * value)
	{
		___deferredTimers_5 = value;
		Il2CppCodeGenWriteBarrier((&___deferredTimers_5), value);
	}

	inline static int32_t get_offset_of_activeTimers_6() { return static_cast<int32_t>(offsetof(TimerScheduler_t541408523, ___activeTimers_6)); }
	inline List_1_t2393398647 * get_activeTimers_6() const { return ___activeTimers_6; }
	inline List_1_t2393398647 ** get_address_of_activeTimers_6() { return &___activeTimers_6; }
	inline void set_activeTimers_6(List_1_t2393398647 * value)
	{
		___activeTimers_6 = value;
		Il2CppCodeGenWriteBarrier((&___activeTimers_6), value);
	}

	inline static int32_t get_offset_of_nextTimerId_7() { return static_cast<int32_t>(offsetof(TimerScheduler_t541408523, ___nextTimerId_7)); }
	inline int32_t get_nextTimerId_7() const { return ___nextTimerId_7; }
	inline int32_t* get_address_of_nextTimerId_7() { return &___nextTimerId_7; }
	inline void set_nextTimerId_7(int32_t value)
	{
		___nextTimerId_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERSCHEDULER_T541408523_H
#ifndef STABILIZATIONPLANEMODIFIER_T1238920175_H
#define STABILIZATIONPLANEMODIFIER_T1238920175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.StabilizationPlaneModifier
struct  StabilizationPlaneModifier_t1238920175  : public Singleton_1_t1639144629
{
public:
	// System.Boolean HoloToolkit.Unity.StabilizationPlaneModifier::SetStabilizationPlane
	bool ___SetStabilizationPlane_4;
	// System.Boolean HoloToolkit.Unity.StabilizationPlaneModifier::UseUnscaledTime
	bool ___UseUnscaledTime_5;
	// System.Single HoloToolkit.Unity.StabilizationPlaneModifier::LerpStabilizationPlanePowerCloser
	float ___LerpStabilizationPlanePowerCloser_6;
	// System.Single HoloToolkit.Unity.StabilizationPlaneModifier::LerpStabilizationPlanePowerFarther
	float ___LerpStabilizationPlanePowerFarther_7;
	// UnityEngine.Transform HoloToolkit.Unity.StabilizationPlaneModifier::targetOverride
	Transform_t3600365921 * ___targetOverride_8;
	// System.Boolean HoloToolkit.Unity.StabilizationPlaneModifier::trackVelocity
	bool ___trackVelocity_9;
	// System.Boolean HoloToolkit.Unity.StabilizationPlaneModifier::UseGazeManager
	bool ___UseGazeManager_10;
	// System.Single HoloToolkit.Unity.StabilizationPlaneModifier::DefaultPlaneDistance
	float ___DefaultPlaneDistance_11;
	// System.Boolean HoloToolkit.Unity.StabilizationPlaneModifier::DrawGizmos
	bool ___DrawGizmos_12;
	// UnityEngine.Vector3 HoloToolkit.Unity.StabilizationPlaneModifier::planePosition
	Vector3_t3722313464  ___planePosition_13;
	// System.Single HoloToolkit.Unity.StabilizationPlaneModifier::currentPlaneDistance
	float ___currentPlaneDistance_14;
	// UnityEngine.Vector3 HoloToolkit.Unity.StabilizationPlaneModifier::targetOverridePreviousPosition
	Vector3_t3722313464  ___targetOverridePreviousPosition_15;

public:
	inline static int32_t get_offset_of_SetStabilizationPlane_4() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t1238920175, ___SetStabilizationPlane_4)); }
	inline bool get_SetStabilizationPlane_4() const { return ___SetStabilizationPlane_4; }
	inline bool* get_address_of_SetStabilizationPlane_4() { return &___SetStabilizationPlane_4; }
	inline void set_SetStabilizationPlane_4(bool value)
	{
		___SetStabilizationPlane_4 = value;
	}

	inline static int32_t get_offset_of_UseUnscaledTime_5() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t1238920175, ___UseUnscaledTime_5)); }
	inline bool get_UseUnscaledTime_5() const { return ___UseUnscaledTime_5; }
	inline bool* get_address_of_UseUnscaledTime_5() { return &___UseUnscaledTime_5; }
	inline void set_UseUnscaledTime_5(bool value)
	{
		___UseUnscaledTime_5 = value;
	}

	inline static int32_t get_offset_of_LerpStabilizationPlanePowerCloser_6() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t1238920175, ___LerpStabilizationPlanePowerCloser_6)); }
	inline float get_LerpStabilizationPlanePowerCloser_6() const { return ___LerpStabilizationPlanePowerCloser_6; }
	inline float* get_address_of_LerpStabilizationPlanePowerCloser_6() { return &___LerpStabilizationPlanePowerCloser_6; }
	inline void set_LerpStabilizationPlanePowerCloser_6(float value)
	{
		___LerpStabilizationPlanePowerCloser_6 = value;
	}

	inline static int32_t get_offset_of_LerpStabilizationPlanePowerFarther_7() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t1238920175, ___LerpStabilizationPlanePowerFarther_7)); }
	inline float get_LerpStabilizationPlanePowerFarther_7() const { return ___LerpStabilizationPlanePowerFarther_7; }
	inline float* get_address_of_LerpStabilizationPlanePowerFarther_7() { return &___LerpStabilizationPlanePowerFarther_7; }
	inline void set_LerpStabilizationPlanePowerFarther_7(float value)
	{
		___LerpStabilizationPlanePowerFarther_7 = value;
	}

	inline static int32_t get_offset_of_targetOverride_8() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t1238920175, ___targetOverride_8)); }
	inline Transform_t3600365921 * get_targetOverride_8() const { return ___targetOverride_8; }
	inline Transform_t3600365921 ** get_address_of_targetOverride_8() { return &___targetOverride_8; }
	inline void set_targetOverride_8(Transform_t3600365921 * value)
	{
		___targetOverride_8 = value;
		Il2CppCodeGenWriteBarrier((&___targetOverride_8), value);
	}

	inline static int32_t get_offset_of_trackVelocity_9() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t1238920175, ___trackVelocity_9)); }
	inline bool get_trackVelocity_9() const { return ___trackVelocity_9; }
	inline bool* get_address_of_trackVelocity_9() { return &___trackVelocity_9; }
	inline void set_trackVelocity_9(bool value)
	{
		___trackVelocity_9 = value;
	}

	inline static int32_t get_offset_of_UseGazeManager_10() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t1238920175, ___UseGazeManager_10)); }
	inline bool get_UseGazeManager_10() const { return ___UseGazeManager_10; }
	inline bool* get_address_of_UseGazeManager_10() { return &___UseGazeManager_10; }
	inline void set_UseGazeManager_10(bool value)
	{
		___UseGazeManager_10 = value;
	}

	inline static int32_t get_offset_of_DefaultPlaneDistance_11() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t1238920175, ___DefaultPlaneDistance_11)); }
	inline float get_DefaultPlaneDistance_11() const { return ___DefaultPlaneDistance_11; }
	inline float* get_address_of_DefaultPlaneDistance_11() { return &___DefaultPlaneDistance_11; }
	inline void set_DefaultPlaneDistance_11(float value)
	{
		___DefaultPlaneDistance_11 = value;
	}

	inline static int32_t get_offset_of_DrawGizmos_12() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t1238920175, ___DrawGizmos_12)); }
	inline bool get_DrawGizmos_12() const { return ___DrawGizmos_12; }
	inline bool* get_address_of_DrawGizmos_12() { return &___DrawGizmos_12; }
	inline void set_DrawGizmos_12(bool value)
	{
		___DrawGizmos_12 = value;
	}

	inline static int32_t get_offset_of_planePosition_13() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t1238920175, ___planePosition_13)); }
	inline Vector3_t3722313464  get_planePosition_13() const { return ___planePosition_13; }
	inline Vector3_t3722313464 * get_address_of_planePosition_13() { return &___planePosition_13; }
	inline void set_planePosition_13(Vector3_t3722313464  value)
	{
		___planePosition_13 = value;
	}

	inline static int32_t get_offset_of_currentPlaneDistance_14() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t1238920175, ___currentPlaneDistance_14)); }
	inline float get_currentPlaneDistance_14() const { return ___currentPlaneDistance_14; }
	inline float* get_address_of_currentPlaneDistance_14() { return &___currentPlaneDistance_14; }
	inline void set_currentPlaneDistance_14(float value)
	{
		___currentPlaneDistance_14 = value;
	}

	inline static int32_t get_offset_of_targetOverridePreviousPosition_15() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t1238920175, ___targetOverridePreviousPosition_15)); }
	inline Vector3_t3722313464  get_targetOverridePreviousPosition_15() const { return ___targetOverridePreviousPosition_15; }
	inline Vector3_t3722313464 * get_address_of_targetOverridePreviousPosition_15() { return &___targetOverridePreviousPosition_15; }
	inline void set_targetOverridePreviousPosition_15(Vector3_t3722313464  value)
	{
		___targetOverridePreviousPosition_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STABILIZATIONPLANEMODIFIER_T1238920175_H
#ifndef TAGALONG_T697166241_H
#define TAGALONG_T697166241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Tagalong
struct  Tagalong_t697166241  : public SimpleTagalong_t3886944103
{
public:
	// System.Single HoloToolkit.Unity.Tagalong::MinimumHorizontalOverlap
	float ___MinimumHorizontalOverlap_14;
	// System.Single HoloToolkit.Unity.Tagalong::TargetHorizontalOverlap
	float ___TargetHorizontalOverlap_15;
	// System.Single HoloToolkit.Unity.Tagalong::MinimumVerticalOverlap
	float ___MinimumVerticalOverlap_16;
	// System.Single HoloToolkit.Unity.Tagalong::TargetVerticalOverlap
	float ___TargetVerticalOverlap_17;
	// System.Int32 HoloToolkit.Unity.Tagalong::HorizontalRayCount
	int32_t ___HorizontalRayCount_18;
	// System.Int32 HoloToolkit.Unity.Tagalong::VerticalRayCount
	int32_t ___VerticalRayCount_19;
	// System.Single HoloToolkit.Unity.Tagalong::MinimumTagalongDistance
	float ___MinimumTagalongDistance_20;
	// System.Boolean HoloToolkit.Unity.Tagalong::MaintainFixedSize
	bool ___MaintainFixedSize_21;
	// System.Single HoloToolkit.Unity.Tagalong::DepthUpdateSpeed
	float ___DepthUpdateSpeed_22;
	// System.Single HoloToolkit.Unity.Tagalong::defaultTagalongDistance
	float ___defaultTagalongDistance_23;
	// System.Boolean HoloToolkit.Unity.Tagalong::DebugDrawLines
	bool ___DebugDrawLines_24;
	// UnityEngine.Light HoloToolkit.Unity.Tagalong::DebugPointLight
	Light_t3756812086 * ___DebugPointLight_25;

public:
	inline static int32_t get_offset_of_MinimumHorizontalOverlap_14() { return static_cast<int32_t>(offsetof(Tagalong_t697166241, ___MinimumHorizontalOverlap_14)); }
	inline float get_MinimumHorizontalOverlap_14() const { return ___MinimumHorizontalOverlap_14; }
	inline float* get_address_of_MinimumHorizontalOverlap_14() { return &___MinimumHorizontalOverlap_14; }
	inline void set_MinimumHorizontalOverlap_14(float value)
	{
		___MinimumHorizontalOverlap_14 = value;
	}

	inline static int32_t get_offset_of_TargetHorizontalOverlap_15() { return static_cast<int32_t>(offsetof(Tagalong_t697166241, ___TargetHorizontalOverlap_15)); }
	inline float get_TargetHorizontalOverlap_15() const { return ___TargetHorizontalOverlap_15; }
	inline float* get_address_of_TargetHorizontalOverlap_15() { return &___TargetHorizontalOverlap_15; }
	inline void set_TargetHorizontalOverlap_15(float value)
	{
		___TargetHorizontalOverlap_15 = value;
	}

	inline static int32_t get_offset_of_MinimumVerticalOverlap_16() { return static_cast<int32_t>(offsetof(Tagalong_t697166241, ___MinimumVerticalOverlap_16)); }
	inline float get_MinimumVerticalOverlap_16() const { return ___MinimumVerticalOverlap_16; }
	inline float* get_address_of_MinimumVerticalOverlap_16() { return &___MinimumVerticalOverlap_16; }
	inline void set_MinimumVerticalOverlap_16(float value)
	{
		___MinimumVerticalOverlap_16 = value;
	}

	inline static int32_t get_offset_of_TargetVerticalOverlap_17() { return static_cast<int32_t>(offsetof(Tagalong_t697166241, ___TargetVerticalOverlap_17)); }
	inline float get_TargetVerticalOverlap_17() const { return ___TargetVerticalOverlap_17; }
	inline float* get_address_of_TargetVerticalOverlap_17() { return &___TargetVerticalOverlap_17; }
	inline void set_TargetVerticalOverlap_17(float value)
	{
		___TargetVerticalOverlap_17 = value;
	}

	inline static int32_t get_offset_of_HorizontalRayCount_18() { return static_cast<int32_t>(offsetof(Tagalong_t697166241, ___HorizontalRayCount_18)); }
	inline int32_t get_HorizontalRayCount_18() const { return ___HorizontalRayCount_18; }
	inline int32_t* get_address_of_HorizontalRayCount_18() { return &___HorizontalRayCount_18; }
	inline void set_HorizontalRayCount_18(int32_t value)
	{
		___HorizontalRayCount_18 = value;
	}

	inline static int32_t get_offset_of_VerticalRayCount_19() { return static_cast<int32_t>(offsetof(Tagalong_t697166241, ___VerticalRayCount_19)); }
	inline int32_t get_VerticalRayCount_19() const { return ___VerticalRayCount_19; }
	inline int32_t* get_address_of_VerticalRayCount_19() { return &___VerticalRayCount_19; }
	inline void set_VerticalRayCount_19(int32_t value)
	{
		___VerticalRayCount_19 = value;
	}

	inline static int32_t get_offset_of_MinimumTagalongDistance_20() { return static_cast<int32_t>(offsetof(Tagalong_t697166241, ___MinimumTagalongDistance_20)); }
	inline float get_MinimumTagalongDistance_20() const { return ___MinimumTagalongDistance_20; }
	inline float* get_address_of_MinimumTagalongDistance_20() { return &___MinimumTagalongDistance_20; }
	inline void set_MinimumTagalongDistance_20(float value)
	{
		___MinimumTagalongDistance_20 = value;
	}

	inline static int32_t get_offset_of_MaintainFixedSize_21() { return static_cast<int32_t>(offsetof(Tagalong_t697166241, ___MaintainFixedSize_21)); }
	inline bool get_MaintainFixedSize_21() const { return ___MaintainFixedSize_21; }
	inline bool* get_address_of_MaintainFixedSize_21() { return &___MaintainFixedSize_21; }
	inline void set_MaintainFixedSize_21(bool value)
	{
		___MaintainFixedSize_21 = value;
	}

	inline static int32_t get_offset_of_DepthUpdateSpeed_22() { return static_cast<int32_t>(offsetof(Tagalong_t697166241, ___DepthUpdateSpeed_22)); }
	inline float get_DepthUpdateSpeed_22() const { return ___DepthUpdateSpeed_22; }
	inline float* get_address_of_DepthUpdateSpeed_22() { return &___DepthUpdateSpeed_22; }
	inline void set_DepthUpdateSpeed_22(float value)
	{
		___DepthUpdateSpeed_22 = value;
	}

	inline static int32_t get_offset_of_defaultTagalongDistance_23() { return static_cast<int32_t>(offsetof(Tagalong_t697166241, ___defaultTagalongDistance_23)); }
	inline float get_defaultTagalongDistance_23() const { return ___defaultTagalongDistance_23; }
	inline float* get_address_of_defaultTagalongDistance_23() { return &___defaultTagalongDistance_23; }
	inline void set_defaultTagalongDistance_23(float value)
	{
		___defaultTagalongDistance_23 = value;
	}

	inline static int32_t get_offset_of_DebugDrawLines_24() { return static_cast<int32_t>(offsetof(Tagalong_t697166241, ___DebugDrawLines_24)); }
	inline bool get_DebugDrawLines_24() const { return ___DebugDrawLines_24; }
	inline bool* get_address_of_DebugDrawLines_24() { return &___DebugDrawLines_24; }
	inline void set_DebugDrawLines_24(bool value)
	{
		___DebugDrawLines_24 = value;
	}

	inline static int32_t get_offset_of_DebugPointLight_25() { return static_cast<int32_t>(offsetof(Tagalong_t697166241, ___DebugPointLight_25)); }
	inline Light_t3756812086 * get_DebugPointLight_25() const { return ___DebugPointLight_25; }
	inline Light_t3756812086 ** get_address_of_DebugPointLight_25() { return &___DebugPointLight_25; }
	inline void set_DebugPointLight_25(Light_t3756812086 * value)
	{
		___DebugPointLight_25 = value;
		Il2CppCodeGenWriteBarrier((&___DebugPointLight_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGALONG_T697166241_H
#ifndef DISTORTERSPHERE_T1346256769_H
#define DISTORTERSPHERE_T1346256769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.DistorterSphere
struct  DistorterSphere_t1346256769  : public Distorter_t3784015034
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterSphere::sphereCenter
	Vector3_t3722313464  ___sphereCenter_4;
	// System.Single HoloToolkit.Unity.UX.DistorterSphere::radius
	float ___radius_5;

public:
	inline static int32_t get_offset_of_sphereCenter_4() { return static_cast<int32_t>(offsetof(DistorterSphere_t1346256769, ___sphereCenter_4)); }
	inline Vector3_t3722313464  get_sphereCenter_4() const { return ___sphereCenter_4; }
	inline Vector3_t3722313464 * get_address_of_sphereCenter_4() { return &___sphereCenter_4; }
	inline void set_sphereCenter_4(Vector3_t3722313464  value)
	{
		___sphereCenter_4 = value;
	}

	inline static int32_t get_offset_of_radius_5() { return static_cast<int32_t>(offsetof(DistorterSphere_t1346256769, ___radius_5)); }
	inline float get_radius_5() const { return ___radius_5; }
	inline float* get_address_of_radius_5() { return &___radius_5; }
	inline void set_radius_5(float value)
	{
		___radius_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTERSPHERE_T1346256769_H
#ifndef DISTORTERWIGGLY_T1153356134_H
#define DISTORTERWIGGLY_T1153356134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.DistorterWiggly
struct  DistorterWiggly_t1153356134  : public Distorter_t3784015034
{
public:
	// System.Single HoloToolkit.Unity.UX.DistorterWiggly::ScaleMultiplier
	float ___ScaleMultiplier_11;
	// System.Single HoloToolkit.Unity.UX.DistorterWiggly::SpeedMultiplier
	float ___SpeedMultiplier_12;
	// System.Single HoloToolkit.Unity.UX.DistorterWiggly::StrengthMultiplier
	float ___StrengthMultiplier_13;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterWiggly::AxisStrength
	Vector3_t3722313464  ___AxisStrength_14;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterWiggly::AxisSpeed
	Vector3_t3722313464  ___AxisSpeed_15;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterWiggly::AxisOffset
	Vector3_t3722313464  ___AxisOffset_16;

public:
	inline static int32_t get_offset_of_ScaleMultiplier_11() { return static_cast<int32_t>(offsetof(DistorterWiggly_t1153356134, ___ScaleMultiplier_11)); }
	inline float get_ScaleMultiplier_11() const { return ___ScaleMultiplier_11; }
	inline float* get_address_of_ScaleMultiplier_11() { return &___ScaleMultiplier_11; }
	inline void set_ScaleMultiplier_11(float value)
	{
		___ScaleMultiplier_11 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_12() { return static_cast<int32_t>(offsetof(DistorterWiggly_t1153356134, ___SpeedMultiplier_12)); }
	inline float get_SpeedMultiplier_12() const { return ___SpeedMultiplier_12; }
	inline float* get_address_of_SpeedMultiplier_12() { return &___SpeedMultiplier_12; }
	inline void set_SpeedMultiplier_12(float value)
	{
		___SpeedMultiplier_12 = value;
	}

	inline static int32_t get_offset_of_StrengthMultiplier_13() { return static_cast<int32_t>(offsetof(DistorterWiggly_t1153356134, ___StrengthMultiplier_13)); }
	inline float get_StrengthMultiplier_13() const { return ___StrengthMultiplier_13; }
	inline float* get_address_of_StrengthMultiplier_13() { return &___StrengthMultiplier_13; }
	inline void set_StrengthMultiplier_13(float value)
	{
		___StrengthMultiplier_13 = value;
	}

	inline static int32_t get_offset_of_AxisStrength_14() { return static_cast<int32_t>(offsetof(DistorterWiggly_t1153356134, ___AxisStrength_14)); }
	inline Vector3_t3722313464  get_AxisStrength_14() const { return ___AxisStrength_14; }
	inline Vector3_t3722313464 * get_address_of_AxisStrength_14() { return &___AxisStrength_14; }
	inline void set_AxisStrength_14(Vector3_t3722313464  value)
	{
		___AxisStrength_14 = value;
	}

	inline static int32_t get_offset_of_AxisSpeed_15() { return static_cast<int32_t>(offsetof(DistorterWiggly_t1153356134, ___AxisSpeed_15)); }
	inline Vector3_t3722313464  get_AxisSpeed_15() const { return ___AxisSpeed_15; }
	inline Vector3_t3722313464 * get_address_of_AxisSpeed_15() { return &___AxisSpeed_15; }
	inline void set_AxisSpeed_15(Vector3_t3722313464  value)
	{
		___AxisSpeed_15 = value;
	}

	inline static int32_t get_offset_of_AxisOffset_16() { return static_cast<int32_t>(offsetof(DistorterWiggly_t1153356134, ___AxisOffset_16)); }
	inline Vector3_t3722313464  get_AxisOffset_16() const { return ___AxisOffset_16; }
	inline Vector3_t3722313464 * get_address_of_AxisOffset_16() { return &___AxisOffset_16; }
	inline void set_AxisOffset_16(Vector3_t3722313464  value)
	{
		___AxisOffset_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTERWIGGLY_T1153356134_H
#ifndef DISTORTERGRAVITY_T129825312_H
#define DISTORTERGRAVITY_T129825312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.DistorterGravity
struct  DistorterGravity_t129825312  : public Distorter_t3784015034
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterGravity::LocalCenterOfGravity
	Vector3_t3722313464  ___LocalCenterOfGravity_4;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterGravity::AxisStrength
	Vector3_t3722313464  ___AxisStrength_5;
	// System.Single HoloToolkit.Unity.UX.DistorterGravity::Radius
	float ___Radius_6;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.DistorterGravity::GravityStrength
	AnimationCurve_t3046754366 * ___GravityStrength_7;

public:
	inline static int32_t get_offset_of_LocalCenterOfGravity_4() { return static_cast<int32_t>(offsetof(DistorterGravity_t129825312, ___LocalCenterOfGravity_4)); }
	inline Vector3_t3722313464  get_LocalCenterOfGravity_4() const { return ___LocalCenterOfGravity_4; }
	inline Vector3_t3722313464 * get_address_of_LocalCenterOfGravity_4() { return &___LocalCenterOfGravity_4; }
	inline void set_LocalCenterOfGravity_4(Vector3_t3722313464  value)
	{
		___LocalCenterOfGravity_4 = value;
	}

	inline static int32_t get_offset_of_AxisStrength_5() { return static_cast<int32_t>(offsetof(DistorterGravity_t129825312, ___AxisStrength_5)); }
	inline Vector3_t3722313464  get_AxisStrength_5() const { return ___AxisStrength_5; }
	inline Vector3_t3722313464 * get_address_of_AxisStrength_5() { return &___AxisStrength_5; }
	inline void set_AxisStrength_5(Vector3_t3722313464  value)
	{
		___AxisStrength_5 = value;
	}

	inline static int32_t get_offset_of_Radius_6() { return static_cast<int32_t>(offsetof(DistorterGravity_t129825312, ___Radius_6)); }
	inline float get_Radius_6() const { return ___Radius_6; }
	inline float* get_address_of_Radius_6() { return &___Radius_6; }
	inline void set_Radius_6(float value)
	{
		___Radius_6 = value;
	}

	inline static int32_t get_offset_of_GravityStrength_7() { return static_cast<int32_t>(offsetof(DistorterGravity_t129825312, ___GravityStrength_7)); }
	inline AnimationCurve_t3046754366 * get_GravityStrength_7() const { return ___GravityStrength_7; }
	inline AnimationCurve_t3046754366 ** get_address_of_GravityStrength_7() { return &___GravityStrength_7; }
	inline void set_GravityStrength_7(AnimationCurve_t3046754366 * value)
	{
		___GravityStrength_7 = value;
		Il2CppCodeGenWriteBarrier((&___GravityStrength_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTERGRAVITY_T129825312_H
#ifndef DISTORTERSIMPLEX_T576167952_H
#define DISTORTERSIMPLEX_T576167952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.DistorterSimplex
struct  DistorterSimplex_t576167952  : public Distorter_t3784015034
{
public:
	// System.Single HoloToolkit.Unity.UX.DistorterSimplex::ScaleMultiplier
	float ___ScaleMultiplier_4;
	// System.Single HoloToolkit.Unity.UX.DistorterSimplex::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single HoloToolkit.Unity.UX.DistorterSimplex::StrengthMultiplier
	float ___StrengthMultiplier_6;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterSimplex::AxisStrength
	Vector3_t3722313464  ___AxisStrength_7;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterSimplex::AxisSpeed
	Vector3_t3722313464  ___AxisSpeed_8;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterSimplex::AxisOffset
	Vector3_t3722313464  ___AxisOffset_9;
	// System.Single HoloToolkit.Unity.UX.DistorterSimplex::ScaleDistort
	float ___ScaleDistort_10;
	// System.Boolean HoloToolkit.Unity.UX.DistorterSimplex::UniformScaleDistort
	bool ___UniformScaleDistort_11;
	// FastSimplexNoise HoloToolkit.Unity.UX.DistorterSimplex::noise
	FastSimplexNoise_t743524795 * ___noise_12;

public:
	inline static int32_t get_offset_of_ScaleMultiplier_4() { return static_cast<int32_t>(offsetof(DistorterSimplex_t576167952, ___ScaleMultiplier_4)); }
	inline float get_ScaleMultiplier_4() const { return ___ScaleMultiplier_4; }
	inline float* get_address_of_ScaleMultiplier_4() { return &___ScaleMultiplier_4; }
	inline void set_ScaleMultiplier_4(float value)
	{
		___ScaleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(DistorterSimplex_t576167952, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_StrengthMultiplier_6() { return static_cast<int32_t>(offsetof(DistorterSimplex_t576167952, ___StrengthMultiplier_6)); }
	inline float get_StrengthMultiplier_6() const { return ___StrengthMultiplier_6; }
	inline float* get_address_of_StrengthMultiplier_6() { return &___StrengthMultiplier_6; }
	inline void set_StrengthMultiplier_6(float value)
	{
		___StrengthMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_AxisStrength_7() { return static_cast<int32_t>(offsetof(DistorterSimplex_t576167952, ___AxisStrength_7)); }
	inline Vector3_t3722313464  get_AxisStrength_7() const { return ___AxisStrength_7; }
	inline Vector3_t3722313464 * get_address_of_AxisStrength_7() { return &___AxisStrength_7; }
	inline void set_AxisStrength_7(Vector3_t3722313464  value)
	{
		___AxisStrength_7 = value;
	}

	inline static int32_t get_offset_of_AxisSpeed_8() { return static_cast<int32_t>(offsetof(DistorterSimplex_t576167952, ___AxisSpeed_8)); }
	inline Vector3_t3722313464  get_AxisSpeed_8() const { return ___AxisSpeed_8; }
	inline Vector3_t3722313464 * get_address_of_AxisSpeed_8() { return &___AxisSpeed_8; }
	inline void set_AxisSpeed_8(Vector3_t3722313464  value)
	{
		___AxisSpeed_8 = value;
	}

	inline static int32_t get_offset_of_AxisOffset_9() { return static_cast<int32_t>(offsetof(DistorterSimplex_t576167952, ___AxisOffset_9)); }
	inline Vector3_t3722313464  get_AxisOffset_9() const { return ___AxisOffset_9; }
	inline Vector3_t3722313464 * get_address_of_AxisOffset_9() { return &___AxisOffset_9; }
	inline void set_AxisOffset_9(Vector3_t3722313464  value)
	{
		___AxisOffset_9 = value;
	}

	inline static int32_t get_offset_of_ScaleDistort_10() { return static_cast<int32_t>(offsetof(DistorterSimplex_t576167952, ___ScaleDistort_10)); }
	inline float get_ScaleDistort_10() const { return ___ScaleDistort_10; }
	inline float* get_address_of_ScaleDistort_10() { return &___ScaleDistort_10; }
	inline void set_ScaleDistort_10(float value)
	{
		___ScaleDistort_10 = value;
	}

	inline static int32_t get_offset_of_UniformScaleDistort_11() { return static_cast<int32_t>(offsetof(DistorterSimplex_t576167952, ___UniformScaleDistort_11)); }
	inline bool get_UniformScaleDistort_11() const { return ___UniformScaleDistort_11; }
	inline bool* get_address_of_UniformScaleDistort_11() { return &___UniformScaleDistort_11; }
	inline void set_UniformScaleDistort_11(bool value)
	{
		___UniformScaleDistort_11 = value;
	}

	inline static int32_t get_offset_of_noise_12() { return static_cast<int32_t>(offsetof(DistorterSimplex_t576167952, ___noise_12)); }
	inline FastSimplexNoise_t743524795 * get_noise_12() const { return ___noise_12; }
	inline FastSimplexNoise_t743524795 ** get_address_of_noise_12() { return &___noise_12; }
	inline void set_noise_12(FastSimplexNoise_t743524795 * value)
	{
		___noise_12 = value;
		Il2CppCodeGenWriteBarrier((&___noise_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTERSIMPLEX_T576167952_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5700 = { sizeof (TrackedObjectToReferenceEnum_t2659894601)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5700[4] = 
{
	TrackedObjectToReferenceEnum_t2659894601::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5701 = { sizeof (Vector3Smoothed_t107253641)+ sizeof (RuntimeObject), sizeof(Vector3Smoothed_t107253641 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5701[3] = 
{
	Vector3Smoothed_t107253641::get_offset_of_U3CCurrentU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3Smoothed_t107253641::get_offset_of_U3CGoalU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3Smoothed_t107253641::get_offset_of_U3CSmoothTimeU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5702 = { sizeof (QuaternionSmoothed_t1703835205)+ sizeof (RuntimeObject), sizeof(QuaternionSmoothed_t1703835205 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5702[3] = 
{
	QuaternionSmoothed_t1703835205::get_offset_of_U3CCurrentU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QuaternionSmoothed_t1703835205::get_offset_of_U3CGoalU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QuaternionSmoothed_t1703835205::get_offset_of_U3CSmoothTimeU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5703 = { sizeof (SolverMomentumizer_t3553753120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5703[6] = 
{
	SolverMomentumizer_t3553753120::get_offset_of_resistance_14(),
	SolverMomentumizer_t3553753120::get_offset_of_resistanceVelPower_15(),
	SolverMomentumizer_t3553753120::get_offset_of_accelRate_16(),
	SolverMomentumizer_t3553753120::get_offset_of_springiness_17(),
	SolverMomentumizer_t3553753120::get_offset_of_SnapZ_18(),
	SolverMomentumizer_t3553753120::get_offset_of_velocity_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5704 = { sizeof (SolverRadialView_t454380380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5704[9] = 
{
	SolverRadialView_t454380380::get_offset_of_ReferenceDirection_14(),
	SolverRadialView_t454380380::get_offset_of_MinDistance_15(),
	SolverRadialView_t454380380::get_offset_of_MaxDistance_16(),
	SolverRadialView_t454380380::get_offset_of_MinViewDegrees_17(),
	SolverRadialView_t454380380::get_offset_of_MaxViewDegrees_18(),
	SolverRadialView_t454380380::get_offset_of_AspectV_19(),
	SolverRadialView_t454380380::get_offset_of_IgnoreAngleClamp_20(),
	SolverRadialView_t454380380::get_offset_of_IgnoreDistanceClamp_21(),
	SolverRadialView_t454380380::get_offset_of_OrientToRefDir_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5705 = { sizeof (ReferenceDirectionEnum_t1005305064)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5705[5] = 
{
	ReferenceDirectionEnum_t1005305064::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5706 = { sizeof (SolverSurfaceMagnetism_t615223073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5706[19] = 
{
	SolverSurfaceMagnetism_t615223073::get_offset_of_MagneticSurface_14(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_MaxDistance_15(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_CloseDistance_16(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_SurfaceNormalOffset_17(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_SurfaceRayOffset_18(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_raycastMode_19(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_BoxRaysPerEdge_20(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_OrthoBoxCast_21(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_MaximumNormalVariance_22(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_SphereSize_23(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_VolumeCastSizeOverride_24(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_UseLinkedAltScaleOverride_25(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_UseTexCoordNormals_26(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_raycastDirection_27(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_orientationMode_28(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_OrientBlend_29(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_OnSurface_30(),
	SolverSurfaceMagnetism_t615223073::get_offset_of_m_BoxCollider_31(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5707 = { sizeof (RaycastDirectionEnum_t1407303881)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5707[4] = 
{
	RaycastDirectionEnum_t1407303881::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5708 = { sizeof (RaycastModeEnum_t2024436208)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5708[4] = 
{
	RaycastModeEnum_t2024436208::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5709 = { sizeof (OrientModeEnum_t4291832292)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5709[5] = 
{
	OrientModeEnum_t4291832292::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5710 = { sizeof (SortingLayerOverride_t4116415771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5710[3] = 
{
	SortingLayerOverride_t4116415771::get_offset_of_UseLastLayer_2(),
	SortingLayerOverride_t4116415771::get_offset_of_TargetSortingLayerName_3(),
	SortingLayerOverride_t4116415771::get_offset_of_renderers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5711 = { sizeof (SphereBasedTagalong_t2880395387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5711[9] = 
{
	SphereBasedTagalong_t2880395387::get_offset_of_SphereRadius_2(),
	SphereBasedTagalong_t2880395387::get_offset_of_MoveSpeed_3(),
	SphereBasedTagalong_t2880395387::get_offset_of_useUnscaledTime_4(),
	SphereBasedTagalong_t2880395387::get_offset_of_hideOnStart_5(),
	SphereBasedTagalong_t2880395387::get_offset_of_debugDisplaySphere_6(),
	SphereBasedTagalong_t2880395387::get_offset_of_debugDisplayTargetPosition_7(),
	SphereBasedTagalong_t2880395387::get_offset_of_targetPosition_8(),
	SphereBasedTagalong_t2880395387::get_offset_of_optimalPosition_9(),
	SphereBasedTagalong_t2880395387::get_offset_of_initialDistanceToCamera_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5712 = { sizeof (StabilizationPlaneModifier_t1238920175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5712[12] = 
{
	StabilizationPlaneModifier_t1238920175::get_offset_of_SetStabilizationPlane_4(),
	StabilizationPlaneModifier_t1238920175::get_offset_of_UseUnscaledTime_5(),
	StabilizationPlaneModifier_t1238920175::get_offset_of_LerpStabilizationPlanePowerCloser_6(),
	StabilizationPlaneModifier_t1238920175::get_offset_of_LerpStabilizationPlanePowerFarther_7(),
	StabilizationPlaneModifier_t1238920175::get_offset_of_targetOverride_8(),
	StabilizationPlaneModifier_t1238920175::get_offset_of_trackVelocity_9(),
	StabilizationPlaneModifier_t1238920175::get_offset_of_UseGazeManager_10(),
	StabilizationPlaneModifier_t1238920175::get_offset_of_DefaultPlaneDistance_11(),
	StabilizationPlaneModifier_t1238920175::get_offset_of_DrawGizmos_12(),
	StabilizationPlaneModifier_t1238920175::get_offset_of_planePosition_13(),
	StabilizationPlaneModifier_t1238920175::get_offset_of_currentPlaneDistance_14(),
	StabilizationPlaneModifier_t1238920175::get_offset_of_targetOverridePreviousPosition_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5713 = { sizeof (Tagalong_t697166241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5713[12] = 
{
	Tagalong_t697166241::get_offset_of_MinimumHorizontalOverlap_14(),
	Tagalong_t697166241::get_offset_of_TargetHorizontalOverlap_15(),
	Tagalong_t697166241::get_offset_of_MinimumVerticalOverlap_16(),
	Tagalong_t697166241::get_offset_of_TargetVerticalOverlap_17(),
	Tagalong_t697166241::get_offset_of_HorizontalRayCount_18(),
	Tagalong_t697166241::get_offset_of_VerticalRayCount_19(),
	Tagalong_t697166241::get_offset_of_MinimumTagalongDistance_20(),
	Tagalong_t697166241::get_offset_of_MaintainFixedSize_21(),
	Tagalong_t697166241::get_offset_of_DepthUpdateSpeed_22(),
	Tagalong_t697166241::get_offset_of_defaultTagalongDistance_23(),
	Tagalong_t697166241::get_offset_of_DebugDrawLines_24(),
	Tagalong_t697166241::get_offset_of_DebugPointLight_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5714 = { sizeof (TextToSpeechVoice_t443523131)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5714[5] = 
{
	TextToSpeechVoice_t443523131::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5715 = { sizeof (TextToSpeech_t421734152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5715[5] = 
{
	TextToSpeech_t421734152::get_offset_of_audioSource_2(),
	TextToSpeech_t421734152::get_offset_of_voice_3(),
	TextToSpeech_t421734152::get_offset_of_synthesizer_4(),
	TextToSpeech_t421734152::get_offset_of_voiceInfo_5(),
	TextToSpeech_t421734152::get_offset_of_speechTextInQueue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5716 = { sizeof (U3CU3Ec__DisplayClass15_0_t1766518704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5716[2] = 
{
	U3CU3Ec__DisplayClass15_0_t1766518704::get_offset_of_speakFunc_0(),
	U3CU3Ec__DisplayClass15_0_t1766518704::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5717 = { sizeof (U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5717[13] = 
{
	U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489::get_offset_of_U3CU3E1__state_0(),
	U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489::get_offset_of_U3CU3Et__builder_1(),
	U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489::get_offset_of_U3CU3E4__this_2(),
	U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489::get_offset_of_U3CU3E8__1_3(),
	U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489::get_offset_of_U3CspeechStreamU3E5__2_4(),
	U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489::get_offset_of_U3CsizeU3E5__3_5(),
	U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489::get_offset_of_U3CbufferU3E5__4_6(),
	U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489::get_offset_of_U3CU3E8__5_7(),
	U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489::get_offset_of_U3CU3Es__6_8(),
	U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489::get_offset_of_U3CinputStreamU3E5__7_9(),
	U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489::get_offset_of_U3CdataReaderU3E5__8_10(),
	U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489::get_offset_of_U3CU3Eu__1_11(),
	U3CU3CPlaySpeechU3Eb__0U3Ed_t1000717489::get_offset_of_U3CU3Eu__2_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5718 = { sizeof (U3CU3Ec__DisplayClass15_1_t4105170864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5718[1] = 
{
	U3CU3Ec__DisplayClass15_1_t4105170864::get_offset_of_voiceName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5719 = { sizeof (U3CU3Ec__DisplayClass15_2_t1384181680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5719[4] = 
{
	U3CU3Ec__DisplayClass15_2_t1384181680::get_offset_of_unityData_0(),
	U3CU3Ec__DisplayClass15_2_t1384181680::get_offset_of_sampleCount_1(),
	U3CU3Ec__DisplayClass15_2_t1384181680::get_offset_of_frequency_2(),
	U3CU3Ec__DisplayClass15_2_t1384181680::get_offset_of_CSU24U3CU3E8__locals1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5720 = { sizeof (U3CU3Ec__DisplayClass17_0_t1766387632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5720[2] = 
{
	U3CU3Ec__DisplayClass17_0_t1766387632::get_offset_of_ssml_0(),
	U3CU3Ec__DisplayClass17_0_t1766387632::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5721 = { sizeof (U3CU3Ec__DisplayClass18_0_t1765666736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5721[2] = 
{
	U3CU3Ec__DisplayClass18_0_t1765666736::get_offset_of_text_0(),
	U3CU3Ec__DisplayClass18_0_t1765666736::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5722 = { sizeof (Timer_t3129043708)+ sizeof (RuntimeObject), sizeof(Timer_t3129043708 ), sizeof(Timer_t3129043708_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5722[2] = 
{
	Timer_t3129043708::get_offset_of_Id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Timer_t3129043708_StaticFields::get_offset_of_Invalid_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5723 = { sizeof (TimerScheduler_t541408523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5723[4] = 
{
	TimerScheduler_t541408523::get_offset_of_timers_4(),
	TimerScheduler_t541408523::get_offset_of_deferredTimers_5(),
	TimerScheduler_t541408523::get_offset_of_activeTimers_6(),
	TimerScheduler_t541408523::get_offset_of_nextTimerId_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5724 = { sizeof (TimerData_t4056715490)+ sizeof (RuntimeObject), sizeof(TimerData_t4056715490_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5724[4] = 
{
	TimerData_t4056715490::get_offset_of_Callback_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TimerData_t4056715490::get_offset_of_Duration_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TimerData_t4056715490::get_offset_of_Loop_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TimerData_t4056715490::get_offset_of_Id_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5725 = { sizeof (TimerIdPair_t921323905)+ sizeof (RuntimeObject), sizeof(TimerIdPair_t921323905 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5725[2] = 
{
	TimerIdPair_t921323905::get_offset_of_Id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TimerIdPair_t921323905::get_offset_of_KeyTime_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5726 = { sizeof (Callback_t2663646540), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5727 = { sizeof (U3CU3Ec__DisplayClass17_0_t898832891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5727[1] = 
{
	U3CU3Ec__DisplayClass17_0_t898832891::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5728 = { sizeof (ToggleDebugWindow_t2216991934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5728[2] = 
{
	ToggleDebugWindow_t2216991934::get_offset_of_debugEnabled_2(),
	ToggleDebugWindow_t2216991934::get_offset_of_DebugWindow_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5729 = { sizeof (Utils_t85740694), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5730 = { sizeof (ProfileBase_t3960377668), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5731 = { sizeof (UsabilityScaler_t2506837711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5731[1] = 
{
	UsabilityScaler_t2506837711::get_offset_of_baseScale_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5732 = { sizeof (UsabilityUtilities_t4181744403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5733 = { sizeof (InteractionReceiver_t3572119366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5733[4] = 
{
	InteractionReceiver_t3572119366::get_offset_of_interactables_2(),
	InteractionReceiver_t3572119366::get_offset_of_Targets_3(),
	InteractionReceiver_t3572119366::get_offset_of_lockFocus_4(),
	InteractionReceiver_t3572119366::get_offset_of__selectingFocuser_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5734 = { sizeof (ToggleActiveReceiver_t2322331634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5735 = { sizeof (LayoutTypeEnum_t86934664)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5735[3] = 
{
	LayoutTypeEnum_t86934664::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5736 = { sizeof (OrientTypeEnum_t3115570328)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5736[6] = 
{
	OrientTypeEnum_t3115570328::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5737 = { sizeof (SortTypeEnum_t439642796)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5737[6] = 
{
	SortTypeEnum_t439642796::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5738 = { sizeof (SurfaceTypeEnum_t1788140069)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5738[5] = 
{
	SurfaceTypeEnum_t1788140069::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5739 = { sizeof (CollectionNode_t2333109746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5739[4] = 
{
	CollectionNode_t2333109746::get_offset_of_Name_0(),
	CollectionNode_t2333109746::get_offset_of_Offset_1(),
	CollectionNode_t2333109746::get_offset_of_Radius_2(),
	CollectionNode_t2333109746::get_offset_of_transform_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5740 = { sizeof (ObjectCollection_t2218954654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5740[18] = 
{
	ObjectCollection_t2218954654::get_offset_of_OnCollectionUpdated_2(),
	ObjectCollection_t2218954654::get_offset_of_NodeList_3(),
	ObjectCollection_t2218954654::get_offset_of_SurfaceType_4(),
	ObjectCollection_t2218954654::get_offset_of_SortType_5(),
	ObjectCollection_t2218954654::get_offset_of_OrientType_6(),
	ObjectCollection_t2218954654::get_offset_of_LayoutType_7(),
	ObjectCollection_t2218954654::get_offset_of_IgnoreInactiveTransforms_8(),
	ObjectCollection_t2218954654::get_offset_of_Radius_9(),
	ObjectCollection_t2218954654::get_offset_of_Rows_10(),
	ObjectCollection_t2218954654::get_offset_of_CellWidth_11(),
	ObjectCollection_t2218954654::get_offset_of_CellHeight_12(),
	ObjectCollection_t2218954654::get_offset_of_SphereMesh_13(),
	ObjectCollection_t2218954654::get_offset_of_CylinderMesh_14(),
	ObjectCollection_t2218954654::get_offset_of__columns_15(),
	ObjectCollection_t2218954654::get_offset_of__width_16(),
	ObjectCollection_t2218954654::get_offset_of__height_17(),
	ObjectCollection_t2218954654::get_offset_of__circumference_18(),
	ObjectCollection_t2218954654::get_offset_of__halfCell_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5741 = { sizeof (U3CU3Ec_t33098766), -1, sizeof(U3CU3Ec_t33098766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5741[6] = 
{
	U3CU3Ec_t33098766_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t33098766_StaticFields::get_offset_of_U3CU3E9__22_0_1(),
	U3CU3Ec_t33098766_StaticFields::get_offset_of_U3CU3E9__22_1_2(),
	U3CU3Ec_t33098766_StaticFields::get_offset_of_U3CU3E9__22_2_3(),
	U3CU3Ec_t33098766_StaticFields::get_offset_of_U3CU3E9__22_3_4(),
	U3CU3Ec_t33098766_StaticFields::get_offset_of_U3CU3E9__28_0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5742 = { sizeof (ObjectCollectionDynamic_t3678455523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5742[3] = 
{
	ObjectCollectionDynamic_t3678455523::get_offset_of_Behavior_2(),
	ObjectCollectionDynamic_t3678455523::get_offset_of_DynamicNodeList_3(),
	ObjectCollectionDynamic_t3678455523::get_offset_of_collection_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5743 = { sizeof (BehaviorEnum_t1724453858)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5743[4] = 
{
	BehaviorEnum_t1724453858::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5744 = { sizeof (CollectionNodeDynamic_t4288956070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5744[2] = 
{
	CollectionNodeDynamic_t4288956070::get_offset_of_localPositionOnStartup_4(),
	CollectionNodeDynamic_t4288956070::get_offset_of_localEulerAnglesOnStartup_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5745 = { sizeof (AnimatorControllerAction_t150210808)+ sizeof (RuntimeObject), sizeof(AnimatorControllerAction_t150210808_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5745[7] = 
{
	AnimatorControllerAction_t150210808::get_offset_of_ButtonState_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimatorControllerAction_t150210808::get_offset_of_ParamName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimatorControllerAction_t150210808::get_offset_of_ParamType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimatorControllerAction_t150210808::get_offset_of_BoolValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimatorControllerAction_t150210808::get_offset_of_IntValue_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimatorControllerAction_t150210808::get_offset_of_FloatValue_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimatorControllerAction_t150210808::get_offset_of_InvalidParam_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5746 = { sizeof (AnimButton_t3218175740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5746[1] = 
{
	AnimButton_t3218175740::get_offset_of__animator_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5747 = { sizeof (AnimControllerButton_t4085404257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5747[2] = 
{
	AnimControllerButton_t4085404257::get_offset_of_AnimActions_16(),
	AnimControllerButton_t4085404257::get_offset_of__animator_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5748 = { sizeof (Button_t2162584723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5748[14] = 
{
	Button_t2162584723::get_offset_of_ButtonState_2(),
	Button_t2162584723::get_offset_of_ButtonPressFilter_3(),
	Button_t2162584723::get_offset_of_RequireGaze_4(),
	Button_t2162584723::get_offset_of_StateChange_5(),
	Button_t2162584723::get_offset_of_OnButtonPressed_6(),
	Button_t2162584723::get_offset_of_OnButtonReleased_7(),
	Button_t2162584723::get_offset_of_OnButtonClicked_8(),
	Button_t2162584723::get_offset_of_OnButtonHeld_9(),
	Button_t2162584723::get_offset_of_OnButtonCanceled_10(),
	Button_t2162584723::get_offset_of__GizmoIcon_11(),
	Button_t2162584723::get_offset_of__bLastHandVisible_12(),
	Button_t2162584723::get_offset_of__bHandVisible_13(),
	Button_t2162584723::get_offset_of__bFocused_14(),
	Button_t2162584723::get_offset_of__handCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5749 = { sizeof (U3CDelayedReleaseU3Ed__41_t1082381697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5749[4] = 
{
	U3CDelayedReleaseU3Ed__41_t1082381697::get_offset_of_U3CU3E1__state_0(),
	U3CDelayedReleaseU3Ed__41_t1082381697::get_offset_of_U3CU3E2__current_1(),
	U3CDelayedReleaseU3Ed__41_t1082381697::get_offset_of_delay_2(),
	U3CDelayedReleaseU3Ed__41_t1082381697::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5750 = { sizeof (MeshButtonDatum_t1736360998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5750[5] = 
{
	MeshButtonDatum_t1736360998::get_offset_of_Name_0(),
	MeshButtonDatum_t1736360998::get_offset_of_ActiveState_1(),
	MeshButtonDatum_t1736360998::get_offset_of_StateColor_2(),
	MeshButtonDatum_t1736360998::get_offset_of_Offset_3(),
	MeshButtonDatum_t1736360998::get_offset_of_Scale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5751 = { sizeof (SpriteButtonDatum_t2537539945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5751[5] = 
{
	SpriteButtonDatum_t2537539945::get_offset_of_Name_0(),
	SpriteButtonDatum_t2537539945::get_offset_of_ActiveState_1(),
	SpriteButtonDatum_t2537539945::get_offset_of_ButtonSprite_2(),
	SpriteButtonDatum_t2537539945::get_offset_of_SpriteColor_3(),
	SpriteButtonDatum_t2537539945::get_offset_of_Scale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5752 = { sizeof (ButtonStateEnum_t3287638195)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5752[7] = 
{
	ButtonStateEnum_t3287638195::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5753 = { sizeof (ToggleBehaviorEnum_t2809601438)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5753[4] = 
{
	ToggleBehaviorEnum_t2809601438::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5754 = { sizeof (ButtonIconProfile_t2026164329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5754[5] = 
{
	ButtonIconProfile_t2026164329::get_offset_of__IconNotFound_2(),
	ButtonIconProfile_t2026164329::get_offset_of_AlphaTransitionSpeed_3(),
	ButtonIconProfile_t2026164329::get_offset_of_IconMaterial_4(),
	ButtonIconProfile_t2026164329::get_offset_of_IconMesh_5(),
	ButtonIconProfile_t2026164329::get_offset_of_AlphaColorProperty_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5755 = { sizeof (ButtonIconProfileTexture_t3085289291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5755[38] = 
{
	ButtonIconProfileTexture_t3085289291::get_offset_of_GlobalNavButton_7(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_ChevronUp_8(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_ChevronDown_9(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_ChevronLeft_10(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_ChevronRight_11(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Forward_12(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Back_13(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_PageLeft_14(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_PageRight_15(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Add_16(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Remove_17(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Clear_18(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Cancel_19(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Zoom_20(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Refresh_21(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Lock_22(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Accept_23(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_OpenInNewWindow_24(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Completed_25(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Error_26(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Contact_27(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Volume_28(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_KeyboardClassic_29(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Camera_30(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Video_31(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Microphone_32(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Ready_33(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_AirTap_34(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_PressHold_35(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_Drag_36(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_TapToPlaceArt_37(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_AdjustWithHand_38(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_AdjustHologram_39(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_RemoveHologram_40(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_CustomIcons_41(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_initialized_42(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_iconKeys_43(),
	ButtonIconProfileTexture_t3085289291::get_offset_of_iconLookup_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5756 = { sizeof (ButtonLocalizedText_t3494258600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5756[1] = 
{
	ButtonLocalizedText_t3494258600::get_offset_of_TextMesh_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5757 = { sizeof (ButtonMeshProfile_t417185472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5757[7] = 
{
	ButtonMeshProfile_t417185472::get_offset_of_ColorPropertyName_2(),
	ButtonMeshProfile_t417185472::get_offset_of_ValuePropertyName_3(),
	ButtonMeshProfile_t417185472::get_offset_of_SmoothStateChanges_4(),
	ButtonMeshProfile_t417185472::get_offset_of_StickyPressedEvents_5(),
	ButtonMeshProfile_t417185472::get_offset_of_StickyPressedTime_6(),
	ButtonMeshProfile_t417185472::get_offset_of_AnimationSpeed_7(),
	ButtonMeshProfile_t417185472::get_offset_of_ButtonStates_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5758 = { sizeof (ButtonProfile_t1729449105), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5759 = { sizeof (ButtonSoundProfile_t3426452996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5759[14] = 
{
	ButtonSoundProfile_t3426452996::get_offset_of_ButtonCanceled_2(),
	ButtonSoundProfile_t3426452996::get_offset_of_ButtonHeld_3(),
	ButtonSoundProfile_t3426452996::get_offset_of_ButtonPressed_4(),
	ButtonSoundProfile_t3426452996::get_offset_of_ButtonReleased_5(),
	ButtonSoundProfile_t3426452996::get_offset_of_ButtonObservation_6(),
	ButtonSoundProfile_t3426452996::get_offset_of_ButtonObservationTargeted_7(),
	ButtonSoundProfile_t3426452996::get_offset_of_ButtonTargeted_8(),
	ButtonSoundProfile_t3426452996::get_offset_of_ButtonCanceledVolume_9(),
	ButtonSoundProfile_t3426452996::get_offset_of_ButtonHeldVolume_10(),
	ButtonSoundProfile_t3426452996::get_offset_of_ButtonPressedVolume_11(),
	ButtonSoundProfile_t3426452996::get_offset_of_ButtonReleasedVolume_12(),
	ButtonSoundProfile_t3426452996::get_offset_of_ButtonObservationVolume_13(),
	ButtonSoundProfile_t3426452996::get_offset_of_ButtonObservationTargetedVolume_14(),
	ButtonSoundProfile_t3426452996::get_offset_of_ButtonTargetedVolume_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5760 = { sizeof (ButtonSounds_t3557837633), -1, sizeof(ButtonSounds_t3557837633_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5760[11] = 
{
	0,
	ButtonSounds_t3557837633::get_offset_of_ButtonCanceled_3(),
	ButtonSounds_t3557837633::get_offset_of_ButtonHeld_4(),
	ButtonSounds_t3557837633::get_offset_of_ButtonPressed_5(),
	ButtonSounds_t3557837633::get_offset_of_ButtonReleased_6(),
	ButtonSounds_t3557837633::get_offset_of_ButtonObservation_7(),
	ButtonSounds_t3557837633::get_offset_of_ButtonObservationTargeted_8(),
	ButtonSounds_t3557837633::get_offset_of_ButtonTargeted_9(),
	ButtonSounds_t3557837633::get_offset_of_audioSource_10(),
	ButtonSounds_t3557837633_StaticFields::get_offset_of_lastClipName_11(),
	ButtonSounds_t3557837633_StaticFields::get_offset_of_lastClipTime_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5761 = { sizeof (ButtonTextProfile_t3615608975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5761[15] = 
{
	ButtonTextProfile_t3615608975::get_offset_of_Alignment_2(),
	ButtonTextProfile_t3615608975::get_offset_of_Anchor_3(),
	ButtonTextProfile_t3615608975::get_offset_of_Style_4(),
	ButtonTextProfile_t3615608975::get_offset_of_Size_5(),
	ButtonTextProfile_t3615608975::get_offset_of_Color_6(),
	ButtonTextProfile_t3615608975::get_offset_of_Font_7(),
	ButtonTextProfile_t3615608975::get_offset_of_AnchorLowerCenterOffset_8(),
	ButtonTextProfile_t3615608975::get_offset_of_AnchorLowerLeftOffset_9(),
	ButtonTextProfile_t3615608975::get_offset_of_AnchorLowerRightOffset_10(),
	ButtonTextProfile_t3615608975::get_offset_of_AnchorMiddleCenterOffset_11(),
	ButtonTextProfile_t3615608975::get_offset_of_AnchorMiddleLeftOffset_12(),
	ButtonTextProfile_t3615608975::get_offset_of_AnchorMiddleRightOffset_13(),
	ButtonTextProfile_t3615608975::get_offset_of_AnchorUpperCenterOffset_14(),
	ButtonTextProfile_t3615608975::get_offset_of_AnchorUpperLeftOffset_15(),
	ButtonTextProfile_t3615608975::get_offset_of_AnchorUpperRightOffset_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5762 = { sizeof (CompoundButton_t147810562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5762[2] = 
{
	CompoundButton_t147810562::get_offset_of_MainCollider_16(),
	CompoundButton_t147810562::get_offset_of_MainRenderer_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5763 = { sizeof (CompoundButtonAnim_t700402005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5763[2] = 
{
	CompoundButtonAnim_t700402005::get_offset_of_TargetAnimator_2(),
	CompoundButtonAnim_t700402005::get_offset_of_AnimActions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5764 = { sizeof (CompoundButtonIcon_t3845426619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5764[11] = 
{
	CompoundButtonIcon_t3845426619::get_offset_of_targetIconRenderer_3(),
	CompoundButtonIcon_t3845426619::get_offset_of_DisableIcon_4(),
	CompoundButtonIcon_t3845426619::get_offset_of_OverrideIcon_5(),
	CompoundButtonIcon_t3845426619::get_offset_of_iconName_6(),
	CompoundButtonIcon_t3845426619::get_offset_of_iconOverride_7(),
	CompoundButtonIcon_t3845426619::get_offset_of_alpha_8(),
	CompoundButtonIcon_t3845426619::get_offset_of_instantiatedMaterial_9(),
	CompoundButtonIcon_t3845426619::get_offset_of_instantiatedMesh_10(),
	CompoundButtonIcon_t3845426619::get_offset_of_updatingAlpha_11(),
	CompoundButtonIcon_t3845426619::get_offset_of_alphaTarget_12(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5765 = { sizeof (U3CUpdateAlphaU3Ed__25_t3458770013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5765[6] = 
{
	U3CUpdateAlphaU3Ed__25_t3458770013::get_offset_of_U3CU3E1__state_0(),
	U3CUpdateAlphaU3Ed__25_t3458770013::get_offset_of_U3CU3E2__current_1(),
	U3CUpdateAlphaU3Ed__25_t3458770013::get_offset_of_U3CU3E4__this_2(),
	U3CUpdateAlphaU3Ed__25_t3458770013::get_offset_of_U3CstartTimeU3E5__1_3(),
	U3CUpdateAlphaU3Ed__25_t3458770013::get_offset_of_U3CcolorU3E5__2_4(),
	U3CUpdateAlphaU3Ed__25_t3458770013::get_offset_of_U3CalphaColorPropertyU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5766 = { sizeof (CompoundButtonMesh_t1797344947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5766[7] = 
{
	0,
	CompoundButtonMesh_t1797344947::get_offset_of_TargetTransform_4(),
	CompoundButtonMesh_t1797344947::get_offset_of_Renderer_5(),
	CompoundButtonMesh_t1797344947::get_offset_of_currentDatum_6(),
	CompoundButtonMesh_t1797344947::get_offset_of_instantiatedMaterial_7(),
	CompoundButtonMesh_t1797344947::get_offset_of_sharedMaterial_8(),
	CompoundButtonMesh_t1797344947::get_offset_of_lastTimePressed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5767 = { sizeof (MeshButtonDatum_t3924860415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5767[6] = 
{
	MeshButtonDatum_t3924860415::get_offset_of_Name_0(),
	MeshButtonDatum_t3924860415::get_offset_of_ActiveState_1(),
	MeshButtonDatum_t3924860415::get_offset_of_StateColor_2(),
	MeshButtonDatum_t3924860415::get_offset_of_StateValue_3(),
	MeshButtonDatum_t3924860415::get_offset_of_Offset_4(),
	MeshButtonDatum_t3924860415::get_offset_of_Scale_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5768 = { sizeof (CompoundButtonSounds_t1404604836), -1, sizeof(CompoundButtonSounds_t1404604836_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5768[5] = 
{
	0,
	CompoundButtonSounds_t1404604836::get_offset_of_audioSource_4(),
	CompoundButtonSounds_t1404604836_StaticFields::get_offset_of_lastClipName_5(),
	CompoundButtonSounds_t1404604836_StaticFields::get_offset_of_lastClipTime_6(),
	CompoundButtonSounds_t1404604836::get_offset_of_lastState_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5769 = { sizeof (CompoundButtonSpeech_t1131825139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5769[6] = 
{
	CompoundButtonSpeech_t1131825139::get_offset_of_KeywordSource_2(),
	CompoundButtonSpeech_t1131825139::get_offset_of_Keyword_3(),
	CompoundButtonSpeech_t1131825139::get_offset_of_prevButtonText_4(),
	CompoundButtonSpeech_t1131825139::get_offset_of_keyWord_5(),
	CompoundButtonSpeech_t1131825139::get_offset_of_m_button_6(),
	CompoundButtonSpeech_t1131825139::get_offset_of_m_button_text_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5770 = { sizeof (KeywordSourceEnum_t377777876)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5770[4] = 
{
	KeywordSourceEnum_t377777876::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5771 = { sizeof (CompoundButtonText_t2169052765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5771[10] = 
{
	CompoundButtonText_t2169052765::get_offset_of_TextMesh_3(),
	CompoundButtonText_t2169052765::get_offset_of_OverrideFontStyle_4(),
	CompoundButtonText_t2169052765::get_offset_of_Style_5(),
	CompoundButtonText_t2169052765::get_offset_of_OverrideAnchor_6(),
	CompoundButtonText_t2169052765::get_offset_of_Anchor_7(),
	CompoundButtonText_t2169052765::get_offset_of_OverrideSize_8(),
	CompoundButtonText_t2169052765::get_offset_of_Size_9(),
	CompoundButtonText_t2169052765::get_offset_of_OverrideOffset_10(),
	CompoundButtonText_t2169052765::get_offset_of_alpha_11(),
	CompoundButtonText_t2169052765::get_offset_of_disableText_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5772 = { sizeof (CompoundButtonToggle_t340090535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5772[6] = 
{
	CompoundButtonToggle_t340090535::get_offset_of_Behavior_2(),
	CompoundButtonToggle_t340090535::get_offset_of_OnProfile_3(),
	CompoundButtonToggle_t340090535::get_offset_of_OffProfile_4(),
	CompoundButtonToggle_t340090535::get_offset_of_Target_5(),
	CompoundButtonToggle_t340090535::get_offset_of_m_compButton_6(),
	CompoundButtonToggle_t340090535::get_offset_of_state_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5773 = { sizeof (MeshButton_t4168117946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5773[5] = 
{
	MeshButton_t4168117946::get_offset_of_UseAnimator_16(),
	MeshButton_t4168117946::get_offset_of_ButtonStates_17(),
	MeshButton_t4168117946::get_offset_of__renderer_18(),
	MeshButton_t4168117946::get_offset_of__meshFilter_19(),
	MeshButton_t4168117946::get_offset_of__animator_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5774 = { sizeof (ObjectButton_t1512479393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5774[1] = 
{
	ObjectButton_t1512479393::get_offset_of_ButtonStates_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5775 = { sizeof (ObjectButtonDatum_t759934215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5775[6] = 
{
	ObjectButtonDatum_t759934215::get_offset_of_Name_0(),
	ObjectButtonDatum_t759934215::get_offset_of_ActiveState_1(),
	ObjectButtonDatum_t759934215::get_offset_of_Prefab_2(),
	ObjectButtonDatum_t759934215::get_offset_of_Instance_3(),
	ObjectButtonDatum_t759934215::get_offset_of_Offset_4(),
	ObjectButtonDatum_t759934215::get_offset_of_Scale_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5776 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5776[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5777 = { sizeof (SpriteButton_t167139624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5777[2] = 
{
	SpriteButton_t167139624::get_offset_of_ButtonStates_16(),
	SpriteButton_t167139624::get_offset_of__renderer_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5778 = { sizeof (BoundingBox_t212658891), -1, sizeof(BoundingBox_t212658891_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5778[17] = 
{
	BoundingBox_t212658891::get_offset_of_OnFlattenedAxisChange_2(),
	BoundingBox_t212658891::get_offset_of_target_3(),
	BoundingBox_t212658891::get_offset_of_scaleTransform_4(),
	BoundingBox_t212658891::get_offset_of_flattenPreference_5(),
	BoundingBox_t212658891::get_offset_of_flattenAxisThreshold_6(),
	BoundingBox_t212658891::get_offset_of_flattenedAxisThickness_7(),
	BoundingBox_t212658891::get_offset_of_scalePadding_8(),
	BoundingBox_t212658891::get_offset_of_flattenedScalePadding_9(),
	BoundingBox_t212658891::get_offset_of_boundsCalculationMethod_10(),
	BoundingBox_t212658891::get_offset_of_ignoreLayers_11(),
	BoundingBox_t212658891::get_offset_of_targetBoundsWorldCenter_12(),
	BoundingBox_t212658891::get_offset_of_targetBoundsLocalScale_13(),
	BoundingBox_t212658891::get_offset_of_localTargetBounds_14(),
	BoundingBox_t212658891::get_offset_of_boundsPoints_15(),
	BoundingBox_t212658891::get_offset_of_flattenedAxis_16(),
	BoundingBox_t212658891_StaticFields::get_offset_of_corners_17(),
	BoundingBox_t212658891_StaticFields::get_offset_of_rectTransformCorners_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5779 = { sizeof (BoundsCalculationMethodEnum_t2485099182)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5779[5] = 
{
	BoundsCalculationMethodEnum_t2485099182::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5780 = { sizeof (FlattenModeEnum_t2920636453)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5780[6] = 
{
	FlattenModeEnum_t2920636453::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5781 = { sizeof (BoundingBoxGizmo_t584836069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5781[1] = 
{
	BoundingBoxGizmo_t584836069::get_offset_of_boundingBox_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5782 = { sizeof (BoundingBoxGizmoShell_t63524259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5782[9] = 
{
	BoundingBoxGizmoShell_t63524259::get_offset_of_DisplayEdgesWhenSelected_3(),
	BoundingBoxGizmoShell_t63524259::get_offset_of_ClampHandleScale_4(),
	BoundingBoxGizmoShell_t63524259::get_offset_of_HandleScaleMin_5(),
	BoundingBoxGizmoShell_t63524259::get_offset_of_HandleScaleMax_6(),
	BoundingBoxGizmoShell_t63524259::get_offset_of_edgeRenderer_7(),
	BoundingBoxGizmoShell_t63524259::get_offset_of_xyzHandlesObject_8(),
	BoundingBoxGizmoShell_t63524259::get_offset_of_xyHandlesObject_9(),
	BoundingBoxGizmoShell_t63524259::get_offset_of_xzHandlesObject_10(),
	BoundingBoxGizmoShell_t63524259::get_offset_of_zyHandlesObject_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5783 = { sizeof (Duplicator_t4067483021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5783[20] = 
{
	Duplicator_t4067483021::get_offset_of_OnTargetIdle_2(),
	Duplicator_t4067483021::get_offset_of_OnTargetActive_3(),
	Duplicator_t4067483021::get_offset_of_OnTargetEmpty_4(),
	Duplicator_t4067483021::get_offset_of_OnTargetDuplicateStart_5(),
	Duplicator_t4067483021::get_offset_of_OnTargetDuplicateEnd_6(),
	Duplicator_t4067483021::get_offset_of_activateMode_7(),
	Duplicator_t4067483021::get_offset_of_autoActivateRadius_8(),
	Duplicator_t4067483021::get_offset_of_removeRadius_9(),
	Duplicator_t4067483021::get_offset_of_restorePosSpeed_10(),
	Duplicator_t4067483021::get_offset_of_activeTimeout_11(),
	Duplicator_t4067483021::get_offset_of_restoreCurve_12(),
	Duplicator_t4067483021::get_offset_of_limitDuplicates_13(),
	Duplicator_t4067483021::get_offset_of_maxDuplicates_14(),
	Duplicator_t4067483021::get_offset_of_duplicateSpeed_15(),
	Duplicator_t4067483021::get_offset_of_duplicateGrowCurve_16(),
	Duplicator_t4067483021::get_offset_of_state_17(),
	Duplicator_t4067483021::get_offset_of_targetPos_18(),
	Duplicator_t4067483021::get_offset_of_targetScale_19(),
	Duplicator_t4067483021::get_offset_of_targetRot_20(),
	Duplicator_t4067483021::get_offset_of_numDuplicates_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5784 = { sizeof (StateEnum_t2014529335)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5784[5] = 
{
	StateEnum_t2014529335::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5785 = { sizeof (ActivateModeEnum_t1579891057)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5785[3] = 
{
	ActivateModeEnum_t1579891057::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5786 = { sizeof (U3CUpdateTargetU3Ed__30_t3559022823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5786[9] = 
{
	U3CUpdateTargetU3Ed__30_t3559022823::get_offset_of_U3CU3E1__state_0(),
	U3CUpdateTargetU3Ed__30_t3559022823::get_offset_of_U3CU3E2__current_1(),
	U3CUpdateTargetU3Ed__30_t3559022823::get_offset_of_U3CU3E4__this_2(),
	U3CUpdateTargetU3Ed__30_t3559022823::get_offset_of_U3CnormalizedTimeU3E5__1_3(),
	U3CUpdateTargetU3Ed__30_t3559022823::get_offset_of_U3CidleStartTimeU3E5__2_4(),
	U3CUpdateTargetU3Ed__30_t3559022823::get_offset_of_U3ClastTimeChangedU3E5__3_5(),
	U3CUpdateTargetU3Ed__30_t3559022823::get_offset_of_U3CtimedOutTimeU3E5__4_6(),
	U3CUpdateTargetU3Ed__30_t3559022823::get_offset_of_U3CoriginalTargetU3E5__5_7(),
	U3CUpdateTargetU3Ed__30_t3559022823::get_offset_of_U3CduplicateStartTimeU3E5__6_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5787 = { sizeof (Distorter_t3784015034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5787[2] = 
{
	Distorter_t3784015034::get_offset_of_distortOrder_2(),
	Distorter_t3784015034::get_offset_of_distortStrength_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5788 = { sizeof (DistorterBulge_t4206281855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5788[5] = 
{
	DistorterBulge_t4206281855::get_offset_of_bulgeCenter_4(),
	DistorterBulge_t4206281855::get_offset_of_bulgeFalloff_5(),
	DistorterBulge_t4206281855::get_offset_of_bulgeRadius_6(),
	DistorterBulge_t4206281855::get_offset_of_scaleDistort_7(),
	DistorterBulge_t4206281855::get_offset_of_bulgeStrength_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5789 = { sizeof (DistorterGravity_t129825312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5789[4] = 
{
	DistorterGravity_t129825312::get_offset_of_LocalCenterOfGravity_4(),
	DistorterGravity_t129825312::get_offset_of_AxisStrength_5(),
	DistorterGravity_t129825312::get_offset_of_Radius_6(),
	DistorterGravity_t129825312::get_offset_of_GravityStrength_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5790 = { sizeof (DistorterSimplex_t576167952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5790[9] = 
{
	DistorterSimplex_t576167952::get_offset_of_ScaleMultiplier_4(),
	DistorterSimplex_t576167952::get_offset_of_SpeedMultiplier_5(),
	DistorterSimplex_t576167952::get_offset_of_StrengthMultiplier_6(),
	DistorterSimplex_t576167952::get_offset_of_AxisStrength_7(),
	DistorterSimplex_t576167952::get_offset_of_AxisSpeed_8(),
	DistorterSimplex_t576167952::get_offset_of_AxisOffset_9(),
	DistorterSimplex_t576167952::get_offset_of_ScaleDistort_10(),
	DistorterSimplex_t576167952::get_offset_of_UniformScaleDistort_11(),
	DistorterSimplex_t576167952::get_offset_of_noise_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5791 = { sizeof (DistorterSphere_t1346256769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5791[2] = 
{
	DistorterSphere_t1346256769::get_offset_of_sphereCenter_4(),
	DistorterSphere_t1346256769::get_offset_of_radius_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5792 = { sizeof (DistorterWiggly_t1153356134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5792[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	DistorterWiggly_t1153356134::get_offset_of_ScaleMultiplier_11(),
	DistorterWiggly_t1153356134::get_offset_of_SpeedMultiplier_12(),
	DistorterWiggly_t1153356134::get_offset_of_StrengthMultiplier_13(),
	DistorterWiggly_t1153356134::get_offset_of_AxisStrength_14(),
	DistorterWiggly_t1153356134::get_offset_of_AxisSpeed_15(),
	DistorterWiggly_t1153356134::get_offset_of_AxisOffset_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5793 = { sizeof (Bezier_t2227656807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5793[1] = 
{
	Bezier_t2227656807::get_offset_of_points_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5794 = { sizeof (PointSet_t452872835)+ sizeof (RuntimeObject), sizeof(PointSet_t452872835 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5794[4] = 
{
	PointSet_t452872835::get_offset_of_Point1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PointSet_t452872835::get_offset_of_Point2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PointSet_t452872835::get_offset_of_Point3_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PointSet_t452872835::get_offset_of_Point4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5795 = { sizeof (Ellipse_t1737437017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5795[3] = 
{
	0,
	Ellipse_t1737437017::get_offset_of_Resolution_18(),
	Ellipse_t1737437017::get_offset_of_Radius_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5796 = { sizeof (Line_t2631947846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5796[2] = 
{
	Line_t2631947846::get_offset_of_Start_17(),
	Line_t2631947846::get_offset_of_End_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5797 = { sizeof (LineBase_t3631573702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5797[15] = 
{
	0,
	LineBase_t3631573702::get_offset_of_LineStartClamp_3(),
	LineBase_t3631573702::get_offset_of_LineEndClamp_4(),
	LineBase_t3631573702::get_offset_of_RotationType_5(),
	LineBase_t3631573702::get_offset_of_FlipUpVector_6(),
	LineBase_t3631573702::get_offset_of_OriginOffset_7(),
	LineBase_t3631573702::get_offset_of_ManualUpVectorBlend_8(),
	LineBase_t3631573702::get_offset_of_ManualUpVectors_9(),
	LineBase_t3631573702::get_offset_of_VelocitySearchRange_10(),
	LineBase_t3631573702::get_offset_of_VelocityBlend_11(),
	LineBase_t3631573702::get_offset_of_DistortionType_12(),
	LineBase_t3631573702::get_offset_of_DistortionStrength_13(),
	LineBase_t3631573702::get_offset_of_UniformDistortionStrength_14(),
	LineBase_t3631573702::get_offset_of_distorters_15(),
	LineBase_t3631573702::get_offset_of_loops_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5798 = { sizeof (LineMeshes_t1824619843), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5798[14] = 
{
	LineMeshes_t1824619843::get_offset_of_InvisibleShaderName_16(),
	LineMeshes_t1824619843::get_offset_of_LineMesh_17(),
	LineMeshes_t1824619843::get_offset_of_LineMaterial_18(),
	LineMeshes_t1824619843::get_offset_of_ColorProp_19(),
	LineMeshes_t1824619843::get_offset_of_linePropertyBlock_20(),
	LineMeshes_t1824619843::get_offset_of_colorID_21(),
	LineMeshes_t1824619843::get_offset_of_meshTransforms_22(),
	LineMeshes_t1824619843::get_offset_of_colorValues_23(),
	LineMeshes_t1824619843::get_offset_of_executeCommandBuffer_24(),
	LineMeshes_t1824619843::get_offset_of_cameras_25(),
	LineMeshes_t1824619843::get_offset_of_onWillRenderHelper_26(),
	LineMeshes_t1824619843::get_offset_of_onWillRenderMesh_27(),
	LineMeshes_t1824619843::get_offset_of_onWillRenderMat_28(),
	LineMeshes_t1824619843::get_offset_of_meshVertices_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5799 = { sizeof (LineObjectCollection_t2519879353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5799[16] = 
{
	LineObjectCollection_t2519879353::get_offset_of_Objects_2(),
	LineObjectCollection_t2519879353::get_offset_of_DistributionOffset_3(),
	LineObjectCollection_t2519879353::get_offset_of_LengthOffset_4(),
	LineObjectCollection_t2519879353::get_offset_of_ScaleOffset_5(),
	LineObjectCollection_t2519879353::get_offset_of_ScaleMultiplier_6(),
	LineObjectCollection_t2519879353::get_offset_of_PositionMultiplier_7(),
	LineObjectCollection_t2519879353::get_offset_of_ObjectScale_8(),
	LineObjectCollection_t2519879353::get_offset_of_ObjectPosition_9(),
	LineObjectCollection_t2519879353::get_offset_of_FlipRotation_10(),
	LineObjectCollection_t2519879353::get_offset_of_RotationOffset_11(),
	LineObjectCollection_t2519879353::get_offset_of_PositionOffset_12(),
	LineObjectCollection_t2519879353::get_offset_of_RotationTypeOverride_13(),
	LineObjectCollection_t2519879353::get_offset_of_DistributionType_14(),
	LineObjectCollection_t2519879353::get_offset_of_StepMode_15(),
	LineObjectCollection_t2519879353::get_offset_of_source_16(),
	LineObjectCollection_t2519879353::get_offset_of_transformHelper_17(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
