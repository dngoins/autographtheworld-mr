﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.IO.Stream>
struct ConditionalWeakTable_2_t1774956035;
// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.IO.Stream,System.IO.NetFxToWinRtStreamAdapter>
struct ConditionalWeakTable_2_t3962727071;
// System.IO.Stream
struct Stream_t1273022909;
// Windows.Storage.Streams.IBuffer
struct IBuffer_t541192229;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Runtime.CompilerServices.ConditionalWeakTable`2/CreateValueCallback<System.Object,System.IO.Stream>
struct CreateValueCallback_t3597102618;
// System.Threading.CancellationTokenSource
struct CancellationTokenSource_t540272775;
// System.Exception
struct Exception_t1436737249;
// Windows.Foundation.AsyncActionCompletedHandler
struct AsyncActionCompletedHandler_t4142409509;
// System.VoidReferenceTypeParameter
struct VoidReferenceTypeParameter_t1476249554;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t2326897723;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t2750080073;
// System.Random
struct Random_t108471755;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t2342208608;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// System.Action`2<System.Threading.Tasks.Task,System.Object>
struct Action_2_t1194737946;
// System.String
struct String_t;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t61518632;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t156472862;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2974092902;
// System.Threading.Tasks.Task
struct Task_t3187275312;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Threading.Tasks.Task`1<Windows.Storage.Streams.IBuffer>
struct Task_1_t1946732404;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_t923100567;
// System.Action
struct Action_t1264377477;
// System.Void
struct Void_t1185182177;
// Windows.Foundation.Metadata.IApiInformationStatics
struct IApiInformationStatics_t1368946868;
// System.Threading.Tasks.Task`1<System.Boolean>
struct Task_1_t1502828140;
// System.Threading.Tasks.Task`1<System.UInt32>
struct Task_1_t3965602153;
// System.Runtime.InteropServices.IMarshal
struct IMarshal_t3913494478;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Windows.Foundation.IAsyncInfo
struct IAsyncInfo_t2425795444;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;
// System.Runtime.ExceptionServices.ExceptionDispatchInfo
struct ExceptionDispatchInfo_t3750997369;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IO.StreamOperationsImplementation/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t3604542706;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_t472374824;
// System.IProgress`1<System.UInt32>
struct IProgress_1_t3233463983;
// System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t875659351;
// System.Runtime.InteropServices.SafeBuffer
struct SafeBuffer_t3564637124;
// System.Byte
struct Byte_t1134296376;
// System.IO.WinRtToNetFxStreamAdapter
struct WinRtToNetFxStreamAdapter_t1366777568;
// Windows.Foundation.IAsyncAction
struct IAsyncAction_t3072713919;

struct Guid_t ;
struct IAsyncActionCompletedHandler_t4142409509_ComCallableWrapper;
struct DateTime_t1679451545 ;
struct TimeSpan_t881159249 ;
struct Point_t4164953539 ;
struct Size_t550917638 ;
struct Rect_t2695113487 ;
struct IAsyncAction_t3072713919;



#ifndef U3CMODULEU3E_T692745579_H
#define U3CMODULEU3E_T692745579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745579 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745579_H
#ifndef U3CMODULEU3E_T692745580_H
#define U3CMODULEU3E_T692745580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745580 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745580_H
// System.Runtime.InteropServices.IAgileObject
struct NOVTABLE IAgileObject_t981452505 : Il2CppIUnknown
{
	static const Il2CppGuid IID;
};
// System.Runtime.InteropServices.WindowsRuntime.IBufferByteAccess
struct NOVTABLE IBufferByteAccess_t4245463285 : Il2CppIUnknown
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IBufferByteAccess_GetBuffer_m3254137481(intptr_t* comReturnValue) = 0;
};
// System.Runtime.InteropServices.IMarshal
struct NOVTABLE IMarshal_t3913494478 : Il2CppIUnknown
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMarshal_GetUnmarshalClass_m1100404162(Guid_t * ___riid0, intptr_t ___pv1, uint32_t ___dwDestContext2, intptr_t ___pvDestContext3, uint32_t ___mshlFlags4, Guid_t * ___pCid5) = 0;
	virtual il2cpp_hresult_t STDCALL IMarshal_GetMarshalSizeMax_m3676534994(Guid_t * ___riid0, intptr_t ___pv1, uint32_t ___dwDestContext2, intptr_t ___pvDestContext3, uint32_t ___mshlflags4, uint32_t* ___pSize5) = 0;
	virtual il2cpp_hresult_t STDCALL IMarshal_MarshalInterface_m735038959(intptr_t ___pStm0, Guid_t * ___riid1, intptr_t ___pv2, uint32_t ___dwDestContext3, intptr_t ___pvDestContext4, uint32_t ___mshlflags5) = 0;
	virtual il2cpp_hresult_t STDCALL IMarshal_UnmarshalInterface_m291083946(intptr_t ___pStm0, Guid_t * ___riid1, intptr_t* ___ppv2) = 0;
	virtual il2cpp_hresult_t STDCALL IMarshal_ReleaseMarshalData_m2968386882(intptr_t ___pStm0) = 0;
	virtual il2cpp_hresult_t STDCALL IMarshal_DisconnectObject_m119848861(uint32_t ___dwReserved0) = 0;
};
// Windows.Foundation.IClosable
struct NOVTABLE IClosable_t326290202 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IClosable_Close_m649191502() = 0;
};
// Windows.Foundation.Metadata.IApiInformationStatics
struct NOVTABLE IApiInformationStatics_t1368946868 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IApiInformationStatics_U24__Stripped0_IsTypePresent_m2300622423() = 0;
	virtual il2cpp_hresult_t STDCALL IApiInformationStatics_U24__Stripped1_IsMethodPresent_m93280429() = 0;
	virtual il2cpp_hresult_t STDCALL IApiInformationStatics_U24__Stripped2_IsMethodPresent_m1995582324() = 0;
	virtual il2cpp_hresult_t STDCALL IApiInformationStatics_U24__Stripped3_IsEventPresent_m3065413644() = 0;
	virtual il2cpp_hresult_t STDCALL IApiInformationStatics_U24__Stripped4_IsPropertyPresent_m1181033700() = 0;
	virtual il2cpp_hresult_t STDCALL IApiInformationStatics_U24__Stripped5_IsReadOnlyPropertyPresent_m2334894360() = 0;
	virtual il2cpp_hresult_t STDCALL IApiInformationStatics_U24__Stripped6_IsWriteablePropertyPresent_m2319983192() = 0;
	virtual il2cpp_hresult_t STDCALL IApiInformationStatics_U24__Stripped7_IsEnumNamedValuePresent_m2322865717() = 0;
	virtual il2cpp_hresult_t STDCALL IApiInformationStatics_IsApiContractPresent_m681551510(Il2CppHString ___contractName0, uint16_t ___majorVersion1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IApiInformationStatics_U24__Stripped8_IsApiContractPresent_m284209059() = 0;
};
// Windows.Foundation.IAsyncAction
struct NOVTABLE IAsyncAction_t3072713919 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IAsyncAction_put_Completed_m1173868328(IAsyncActionCompletedHandler_t4142409509_ComCallableWrapper* ___handler0) = 0;
	virtual il2cpp_hresult_t STDCALL IAsyncAction_get_Completed_m1835005524(IAsyncActionCompletedHandler_t4142409509_ComCallableWrapper** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IAsyncAction_GetResults_m1540137877() = 0;
};
// Windows.Foundation.IStringable
struct NOVTABLE IStringable_t1634798504 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IStringable_ToString_m698137009(Il2CppHString* comReturnValue) = 0;
};
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef WINDOWSRUNTIMESTREAMEXTENSIONS_T4179015579_H
#define WINDOWSRUNTIMESTREAMEXTENSIONS_T4179015579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.WindowsRuntimeStreamExtensions
struct  WindowsRuntimeStreamExtensions_t4179015579  : public RuntimeObject
{
public:

public:
};

struct WindowsRuntimeStreamExtensions_t4179015579_StaticFields
{
public:
	// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.IO.Stream> System.IO.WindowsRuntimeStreamExtensions::s_winRtToNetFxAdapterMap
	ConditionalWeakTable_2_t1774956035 * ___s_winRtToNetFxAdapterMap_0;
	// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.IO.Stream,System.IO.NetFxToWinRtStreamAdapter> System.IO.WindowsRuntimeStreamExtensions::s_netFxToWinRtAdapterMap
	ConditionalWeakTable_2_t3962727071 * ___s_netFxToWinRtAdapterMap_1;

public:
	inline static int32_t get_offset_of_s_winRtToNetFxAdapterMap_0() { return static_cast<int32_t>(offsetof(WindowsRuntimeStreamExtensions_t4179015579_StaticFields, ___s_winRtToNetFxAdapterMap_0)); }
	inline ConditionalWeakTable_2_t1774956035 * get_s_winRtToNetFxAdapterMap_0() const { return ___s_winRtToNetFxAdapterMap_0; }
	inline ConditionalWeakTable_2_t1774956035 ** get_address_of_s_winRtToNetFxAdapterMap_0() { return &___s_winRtToNetFxAdapterMap_0; }
	inline void set_s_winRtToNetFxAdapterMap_0(ConditionalWeakTable_2_t1774956035 * value)
	{
		___s_winRtToNetFxAdapterMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_winRtToNetFxAdapterMap_0), value);
	}

	inline static int32_t get_offset_of_s_netFxToWinRtAdapterMap_1() { return static_cast<int32_t>(offsetof(WindowsRuntimeStreamExtensions_t4179015579_StaticFields, ___s_netFxToWinRtAdapterMap_1)); }
	inline ConditionalWeakTable_2_t3962727071 * get_s_netFxToWinRtAdapterMap_1() const { return ___s_netFxToWinRtAdapterMap_1; }
	inline ConditionalWeakTable_2_t3962727071 ** get_address_of_s_netFxToWinRtAdapterMap_1() { return &___s_netFxToWinRtAdapterMap_1; }
	inline void set_s_netFxToWinRtAdapterMap_1(ConditionalWeakTable_2_t3962727071 * value)
	{
		___s_netFxToWinRtAdapterMap_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_netFxToWinRtAdapterMap_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWSRUNTIMESTREAMEXTENSIONS_T4179015579_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T3604542706_H
#define U3CU3EC__DISPLAYCLASS3_0_T3604542706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamOperationsImplementation/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t3604542706  : public RuntimeObject
{
public:
	// System.IO.Stream System.IO.StreamOperationsImplementation/<>c__DisplayClass3_0::stream
	Stream_t1273022909 * ___stream_0;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t3604542706, ___stream_0)); }
	inline Stream_t1273022909 * get_stream_0() const { return ___stream_0; }
	inline Stream_t1273022909 ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Stream_t1273022909 * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___stream_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T3604542706_H
#ifndef STREAMOPERATIONSIMPLEMENTATION_T1346754833_H
#define STREAMOPERATIONSIMPLEMENTATION_T1346754833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamOperationsImplementation
struct  StreamOperationsImplementation_t1346754833  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMOPERATIONSIMPLEMENTATION_T1346754833_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T875659351_H
#define U3CU3EC__DISPLAYCLASS2_0_T875659351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t875659351  : public RuntimeObject
{
public:
	// Windows.Storage.Streams.IBuffer System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0::buffer
	RuntimeObject* ___buffer_0;
	// System.IO.Stream System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0::stream
	Stream_t1273022909 * ___stream_1;
	// System.Byte[] System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0::data
	ByteU5BU5D_t4116647657* ___data_2;
	// System.Int32 System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0::offset
	int32_t ___offset_3;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t875659351, ___buffer_0)); }
	inline RuntimeObject* get_buffer_0() const { return ___buffer_0; }
	inline RuntimeObject** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(RuntimeObject* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t875659351, ___stream_1)); }
	inline Stream_t1273022909 * get_stream_1() const { return ___stream_1; }
	inline Stream_t1273022909 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_t1273022909 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___stream_1), value);
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t875659351, ___data_2)); }
	inline ByteU5BU5D_t4116647657* get_data_2() const { return ___data_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(ByteU5BU5D_t4116647657* value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t875659351, ___offset_3)); }
	inline int32_t get_offset_3() const { return ___offset_3; }
	inline int32_t* get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(int32_t value)
	{
		___offset_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T875659351_H
#ifndef U3CU3EC_T2303284929_H
#define U3CU3EC_T2303284929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.WindowsRuntimeStreamExtensions/<>c
struct  U3CU3Ec_t2303284929  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2303284929_StaticFields
{
public:
	// System.IO.WindowsRuntimeStreamExtensions/<>c System.IO.WindowsRuntimeStreamExtensions/<>c::<>9
	U3CU3Ec_t2303284929 * ___U3CU3E9_0;
	// System.Runtime.CompilerServices.ConditionalWeakTable`2/CreateValueCallback<System.Object,System.IO.Stream> System.IO.WindowsRuntimeStreamExtensions/<>c::<>9__11_0
	CreateValueCallback_t3597102618 * ___U3CU3E9__11_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2303284929_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2303284929 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2303284929 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2303284929 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2303284929_StaticFields, ___U3CU3E9__11_0_1)); }
	inline CreateValueCallback_t3597102618 * get_U3CU3E9__11_0_1() const { return ___U3CU3E9__11_0_1; }
	inline CreateValueCallback_t3597102618 ** get_address_of_U3CU3E9__11_0_1() { return &___U3CU3E9__11_0_1; }
	inline void set_U3CU3E9__11_0_1(CreateValueCallback_t3597102618 * value)
	{
		___U3CU3E9__11_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__11_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2303284929_H
#ifndef TASKTOASYNCINFOADAPTER_4_T1408863619_H
#define TASKTOASYNCINFOADAPTER_4_T1408863619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.TaskToAsyncInfoAdapter`4<Windows.Foundation.AsyncActionCompletedHandler,System.VoidReferenceTypeParameter,System.VoidValueTypeParameter,System.VoidValueTypeParameter>
struct  TaskToAsyncInfoAdapter_4_t1408863619  : public RuntimeObject
{
public:
	// System.Threading.CancellationTokenSource System.Threading.Tasks.TaskToAsyncInfoAdapter`4::_cancelTokenSource
	CancellationTokenSource_t540272775 * ____cancelTokenSource_0;
	// System.UInt32 System.Threading.Tasks.TaskToAsyncInfoAdapter`4::_id
	uint32_t ____id_1;
	// System.Exception System.Threading.Tasks.TaskToAsyncInfoAdapter`4::_error
	Exception_t1436737249 * ____error_2;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.TaskToAsyncInfoAdapter`4::_state
	int32_t ____state_3;
	// System.Object System.Threading.Tasks.TaskToAsyncInfoAdapter`4::_dataContainer
	RuntimeObject * ____dataContainer_4;
	// TCompletedHandler System.Threading.Tasks.TaskToAsyncInfoAdapter`4::_completedHandler
	AsyncActionCompletedHandler_t4142409509 * ____completedHandler_5;
	// TProgressHandler System.Threading.Tasks.TaskToAsyncInfoAdapter`4::_progressHandler
	VoidReferenceTypeParameter_t1476249554 * ____progressHandler_6;
	// System.Threading.SynchronizationContext System.Threading.Tasks.TaskToAsyncInfoAdapter`4::_startingContext
	SynchronizationContext_t2326897723 * ____startingContext_7;

public:
	inline static int32_t get_offset_of__cancelTokenSource_0() { return static_cast<int32_t>(offsetof(TaskToAsyncInfoAdapter_4_t1408863619, ____cancelTokenSource_0)); }
	inline CancellationTokenSource_t540272775 * get__cancelTokenSource_0() const { return ____cancelTokenSource_0; }
	inline CancellationTokenSource_t540272775 ** get_address_of__cancelTokenSource_0() { return &____cancelTokenSource_0; }
	inline void set__cancelTokenSource_0(CancellationTokenSource_t540272775 * value)
	{
		____cancelTokenSource_0 = value;
		Il2CppCodeGenWriteBarrier((&____cancelTokenSource_0), value);
	}

	inline static int32_t get_offset_of__id_1() { return static_cast<int32_t>(offsetof(TaskToAsyncInfoAdapter_4_t1408863619, ____id_1)); }
	inline uint32_t get__id_1() const { return ____id_1; }
	inline uint32_t* get_address_of__id_1() { return &____id_1; }
	inline void set__id_1(uint32_t value)
	{
		____id_1 = value;
	}

	inline static int32_t get_offset_of__error_2() { return static_cast<int32_t>(offsetof(TaskToAsyncInfoAdapter_4_t1408863619, ____error_2)); }
	inline Exception_t1436737249 * get__error_2() const { return ____error_2; }
	inline Exception_t1436737249 ** get_address_of__error_2() { return &____error_2; }
	inline void set__error_2(Exception_t1436737249 * value)
	{
		____error_2 = value;
		Il2CppCodeGenWriteBarrier((&____error_2), value);
	}

	inline static int32_t get_offset_of__state_3() { return static_cast<int32_t>(offsetof(TaskToAsyncInfoAdapter_4_t1408863619, ____state_3)); }
	inline int32_t get__state_3() const { return ____state_3; }
	inline int32_t* get_address_of__state_3() { return &____state_3; }
	inline void set__state_3(int32_t value)
	{
		____state_3 = value;
	}

	inline static int32_t get_offset_of__dataContainer_4() { return static_cast<int32_t>(offsetof(TaskToAsyncInfoAdapter_4_t1408863619, ____dataContainer_4)); }
	inline RuntimeObject * get__dataContainer_4() const { return ____dataContainer_4; }
	inline RuntimeObject ** get_address_of__dataContainer_4() { return &____dataContainer_4; }
	inline void set__dataContainer_4(RuntimeObject * value)
	{
		____dataContainer_4 = value;
		Il2CppCodeGenWriteBarrier((&____dataContainer_4), value);
	}

	inline static int32_t get_offset_of__completedHandler_5() { return static_cast<int32_t>(offsetof(TaskToAsyncInfoAdapter_4_t1408863619, ____completedHandler_5)); }
	inline AsyncActionCompletedHandler_t4142409509 * get__completedHandler_5() const { return ____completedHandler_5; }
	inline AsyncActionCompletedHandler_t4142409509 ** get_address_of__completedHandler_5() { return &____completedHandler_5; }
	inline void set__completedHandler_5(AsyncActionCompletedHandler_t4142409509 * value)
	{
		____completedHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____completedHandler_5), value);
	}

	inline static int32_t get_offset_of__progressHandler_6() { return static_cast<int32_t>(offsetof(TaskToAsyncInfoAdapter_4_t1408863619, ____progressHandler_6)); }
	inline VoidReferenceTypeParameter_t1476249554 * get__progressHandler_6() const { return ____progressHandler_6; }
	inline VoidReferenceTypeParameter_t1476249554 ** get_address_of__progressHandler_6() { return &____progressHandler_6; }
	inline void set__progressHandler_6(VoidReferenceTypeParameter_t1476249554 * value)
	{
		____progressHandler_6 = value;
		Il2CppCodeGenWriteBarrier((&____progressHandler_6), value);
	}

	inline static int32_t get_offset_of__startingContext_7() { return static_cast<int32_t>(offsetof(TaskToAsyncInfoAdapter_4_t1408863619, ____startingContext_7)); }
	inline SynchronizationContext_t2326897723 * get__startingContext_7() const { return ____startingContext_7; }
	inline SynchronizationContext_t2326897723 ** get_address_of__startingContext_7() { return &____startingContext_7; }
	inline void set__startingContext_7(SynchronizationContext_t2326897723 * value)
	{
		____startingContext_7 = value;
		Il2CppCodeGenWriteBarrier((&____startingContext_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKTOASYNCINFOADAPTER_4_T1408863619_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T3069626030_H
#define U3CU3EC__DISPLAYCLASS12_0_T3069626030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.WindowsRuntimeStreamExtensions/<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t3069626030  : public RuntimeObject
{
public:
	// System.Int32 System.IO.WindowsRuntimeStreamExtensions/<>c__DisplayClass12_0::bufferSize
	int32_t ___bufferSize_0;

public:
	inline static int32_t get_offset_of_bufferSize_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t3069626030, ___bufferSize_0)); }
	inline int32_t get_bufferSize_0() const { return ___bufferSize_0; }
	inline int32_t* get_address_of_bufferSize_0() { return &___bufferSize_0; }
	inline void set_bufferSize_0(int32_t value)
	{
		___bufferSize_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T3069626030_H
#ifndef WINRTIOHELPER_T4133879084_H
#define WINRTIOHELPER_T4133879084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.WinRtIOHelper
struct  WinRtIOHelper_t4133879084  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINRTIOHELPER_T4133879084_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef ASYNCINFO_T1262122865_H
#define ASYNCINFO_T1262122865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.AsyncInfo
struct  AsyncInfo_t1262122865  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCINFO_T1262122865_H
#ifndef IL2CPPCOMOBJECT_H
#define IL2CPPCOMOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.__Il2CppComObject

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IL2CPPCOMOBJECT_H
#ifndef TOKENIZERHELPER_T3391915057_H
#define TOKENIZERHELPER_T3391915057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.TokenizerHelper
struct  TokenizerHelper_t3391915057  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENIZERHELPER_T3391915057_H
#ifndef WINDOWSRUNTIMEBUFFEREXTENSIONS_T1627451895_H
#define WINDOWSRUNTIMEBUFFEREXTENSIONS_T1627451895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeBufferExtensions
struct  WindowsRuntimeBufferExtensions_t1627451895  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWSRUNTIMEBUFFEREXTENSIONS_T1627451895_H
#ifndef U3CU3EC_T3914568053_H
#define U3CU3EC_T3914568053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.ExceptionDispatchHelper/<>c
struct  U3CU3Ec_t3914568053  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3914568053_StaticFields
{
public:
	// System.Threading.Tasks.ExceptionDispatchHelper/<>c System.Threading.Tasks.ExceptionDispatchHelper/<>c::<>9
	U3CU3Ec_t3914568053 * ___U3CU3E9_0;
	// System.Threading.SendOrPostCallback System.Threading.Tasks.ExceptionDispatchHelper/<>c::<>9__0_0
	SendOrPostCallback_t2750080073 * ___U3CU3E9__0_0_1;
	// System.Threading.SendOrPostCallback System.Threading.Tasks.ExceptionDispatchHelper/<>c::<>9__0_1
	SendOrPostCallback_t2750080073 * ___U3CU3E9__0_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3914568053_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3914568053 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3914568053 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3914568053 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3914568053_StaticFields, ___U3CU3E9__0_0_1)); }
	inline SendOrPostCallback_t2750080073 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline SendOrPostCallback_t2750080073 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(SendOrPostCallback_t2750080073 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3914568053_StaticFields, ___U3CU3E9__0_1_2)); }
	inline SendOrPostCallback_t2750080073 * get_U3CU3E9__0_1_2() const { return ___U3CU3E9__0_1_2; }
	inline SendOrPostCallback_t2750080073 ** get_address_of_U3CU3E9__0_1_2() { return &___U3CU3E9__0_1_2; }
	inline void set_U3CU3E9__0_1_2(SendOrPostCallback_t2750080073 * value)
	{
		___U3CU3E9__0_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3914568053_H
#ifndef WINDOWSRUNTIMESYSTEMEXTENSIONS_T757232145_H
#define WINDOWSRUNTIMESYSTEMEXTENSIONS_T757232145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.WindowsRuntimeSystemExtensions
struct  WindowsRuntimeSystemExtensions_t757232145  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWSRUNTIMESYSTEMEXTENSIONS_T757232145_H
#ifndef VOIDREFERENCETYPEPARAMETER_T1476249554_H
#define VOIDREFERENCETYPEPARAMETER_T1476249554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.VoidReferenceTypeParameter
struct  VoidReferenceTypeParameter_t1476249554  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOIDREFERENCETYPEPARAMETER_T1476249554_H
#ifndef EXCEPTIONDISPATCHHELPER_T757867092_H
#define EXCEPTIONDISPATCHHELPER_T757867092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.ExceptionDispatchHelper
struct  ExceptionDispatchHelper_t757867092  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONDISPATCHHELPER_T757867092_H
#ifndef ASYNCINFOIDGENERATOR_T957218259_H
#define ASYNCINFOIDGENERATOR_T957218259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.AsyncInfoIdGenerator
struct  AsyncInfoIdGenerator_t957218259  : public RuntimeObject
{
public:

public:
};

struct AsyncInfoIdGenerator_t957218259_StaticFields
{
public:
	// System.Random System.Threading.Tasks.AsyncInfoIdGenerator::s_idGenerator
	Random_t108471755 * ___s_idGenerator_0;

public:
	inline static int32_t get_offset_of_s_idGenerator_0() { return static_cast<int32_t>(offsetof(AsyncInfoIdGenerator_t957218259_StaticFields, ___s_idGenerator_0)); }
	inline Random_t108471755 * get_s_idGenerator_0() const { return ___s_idGenerator_0; }
	inline Random_t108471755 ** get_address_of_s_idGenerator_0() { return &___s_idGenerator_0; }
	inline void set_s_idGenerator_0(Random_t108471755 * value)
	{
		___s_idGenerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_idGenerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCINFOIDGENERATOR_T957218259_H
#ifndef MARSHALBYREFOBJECT_T2760389100_H
#define MARSHALBYREFOBJECT_T2760389100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t2760389100  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t2342208608 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t2760389100, ____identity_0)); }
	inline ServerIdentity_t2342208608 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t2342208608 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t2342208608 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_pinvoke
{
	ServerIdentity_t2342208608 * ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_com
{
	ServerIdentity_t2342208608 * ____identity_0;
};
#endif // MARSHALBYREFOBJECT_T2760389100_H
#ifndef U3CU3EC_T2427350286_H
#define U3CU3EC_T2427350286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.WindowsRuntimeSystemExtensions/<>c
struct  U3CU3Ec_t2427350286  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2427350286_StaticFields
{
public:
	// System.WindowsRuntimeSystemExtensions/<>c System.WindowsRuntimeSystemExtensions/<>c::<>9
	U3CU3Ec_t2427350286 * ___U3CU3E9_0;
	// System.Action`1<System.Object> System.WindowsRuntimeSystemExtensions/<>c::<>9__0_0
	Action_1_t3252573759 * ___U3CU3E9__0_0_1;
	// System.Action`2<System.Threading.Tasks.Task,System.Object> System.WindowsRuntimeSystemExtensions/<>c::<>9__0_1
	Action_2_t1194737946 * ___U3CU3E9__0_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2427350286_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2427350286 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2427350286 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2427350286 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2427350286_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Action_1_t3252573759 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Action_1_t3252573759 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2427350286_StaticFields, ___U3CU3E9__0_1_2)); }
	inline Action_2_t1194737946 * get_U3CU3E9__0_1_2() const { return ___U3CU3E9__0_1_2; }
	inline Action_2_t1194737946 ** get_address_of_U3CU3E9__0_1_2() { return &___U3CU3E9__0_1_2; }
	inline void set_U3CU3E9__0_1_2(Action_2_t1194737946 * value)
	{
		___U3CU3E9__0_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2427350286_H
#ifndef EXCLUSIVETOATTRIBUTE_T3124418018_H
#define EXCLUSIVETOATTRIBUTE_T3124418018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.ExclusiveToAttribute
struct  ExclusiveToAttribute_t3124418018  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCLUSIVETOATTRIBUTE_T3124418018_H
#ifndef COMPOSABLEATTRIBUTE_T2299833422_H
#define COMPOSABLEATTRIBUTE_T2299833422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.ComposableAttribute
struct  ComposableAttribute_t2299833422  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOSABLEATTRIBUTE_T2299833422_H
#ifndef OVERLOADATTRIBUTE_T2178303703_H
#define OVERLOADATTRIBUTE_T2178303703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.OverloadAttribute
struct  OverloadAttribute_t2178303703  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERLOADATTRIBUTE_T2178303703_H
#ifndef LENGTHISATTRIBUTE_T613149204_H
#define LENGTHISATTRIBUTE_T613149204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.LengthIsAttribute
struct  LengthIsAttribute_t613149204  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENGTHISATTRIBUTE_T613149204_H
#ifndef EVENTREGISTRATIONTOKEN_T3152277946_H
#define EVENTREGISTRATIONTOKEN_T3152277946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.EventRegistrationToken
struct  EventRegistrationToken_t3152277946 
{
public:
	// System.Int64 Windows.Foundation.EventRegistrationToken::Value
	int64_t ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(EventRegistrationToken_t3152277946, ___Value_0)); }
	inline int64_t get_Value_0() const { return ___Value_0; }
	inline int64_t* get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(int64_t value)
	{
		___Value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTREGISTRATIONTOKEN_T3152277946_H
#ifndef GUIDATTRIBUTE_T2682955351_H
#define GUIDATTRIBUTE_T2682955351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.GuidAttribute
struct  GuidAttribute_t2682955351  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIDATTRIBUTE_T2682955351_H
#ifndef MARSHALINGBEHAVIORATTRIBUTE_T570737848_H
#define MARSHALINGBEHAVIORATTRIBUTE_T570737848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.MarshalingBehaviorAttribute
struct  MarshalingBehaviorAttribute_t570737848  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALINGBEHAVIORATTRIBUTE_T570737848_H
#ifndef PROTECTEDATTRIBUTE_T3736820420_H
#define PROTECTEDATTRIBUTE_T3736820420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.ProtectedAttribute
struct  ProtectedAttribute_t3736820420  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTEDATTRIBUTE_T3736820420_H
#ifndef THREADINGATTRIBUTE_T685453408_H
#define THREADINGATTRIBUTE_T685453408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.ThreadingAttribute
struct  ThreadingAttribute_t685453408  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADINGATTRIBUTE_T685453408_H
#ifndef OVERRIDABLEATTRIBUTE_T203117044_H
#define OVERRIDABLEATTRIBUTE_T203117044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.OverridableAttribute
struct  OverridableAttribute_t203117044  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERRIDABLEATTRIBUTE_T203117044_H
#ifndef APICONTRACTATTRIBUTE_T3160257459_H
#define APICONTRACTATTRIBUTE_T3160257459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.ApiContractAttribute
struct  ApiContractAttribute_t3160257459  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APICONTRACTATTRIBUTE_T3160257459_H
#ifndef DEPRECATEDATTRIBUTE_T3646164936_H
#define DEPRECATEDATTRIBUTE_T3646164936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.DeprecatedAttribute
struct  DeprecatedAttribute_t3646164936  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPRECATEDATTRIBUTE_T3646164936_H
#ifndef STATICATTRIBUTE_T2174868556_H
#define STATICATTRIBUTE_T2174868556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.StaticAttribute
struct  StaticAttribute_t2174868556  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICATTRIBUTE_T2174868556_H
#ifndef HRESULT_T3073183193_H
#define HRESULT_T3073183193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.HResult
struct  HResult_t3073183193 
{
public:
	// System.Int32 Windows.Foundation.HResult::Value
	int32_t ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(HResult_t3073183193, ___Value_0)); }
	inline int32_t get_Value_0() const { return ___Value_0; }
	inline int32_t* get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(int32_t value)
	{
		___Value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HRESULT_T3073183193_H
#ifndef CANCELLATIONTOKEN_T784455623_H
#define CANCELLATIONTOKEN_T784455623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.CancellationToken
struct  CancellationToken_t784455623 
{
public:
	// System.Threading.CancellationTokenSource System.Threading.CancellationToken::m_source
	CancellationTokenSource_t540272775 * ___m_source_0;

public:
	inline static int32_t get_offset_of_m_source_0() { return static_cast<int32_t>(offsetof(CancellationToken_t784455623, ___m_source_0)); }
	inline CancellationTokenSource_t540272775 * get_m_source_0() const { return ___m_source_0; }
	inline CancellationTokenSource_t540272775 ** get_address_of_m_source_0() { return &___m_source_0; }
	inline void set_m_source_0(CancellationTokenSource_t540272775 * value)
	{
		___m_source_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_source_0), value);
	}
};

struct CancellationToken_t784455623_StaticFields
{
public:
	// System.Action`1<System.Object> System.Threading.CancellationToken::s_ActionToActionObjShunt
	Action_1_t3252573759 * ___s_ActionToActionObjShunt_1;

public:
	inline static int32_t get_offset_of_s_ActionToActionObjShunt_1() { return static_cast<int32_t>(offsetof(CancellationToken_t784455623_StaticFields, ___s_ActionToActionObjShunt_1)); }
	inline Action_1_t3252573759 * get_s_ActionToActionObjShunt_1() const { return ___s_ActionToActionObjShunt_1; }
	inline Action_1_t3252573759 ** get_address_of_s_ActionToActionObjShunt_1() { return &___s_ActionToActionObjShunt_1; }
	inline void set_s_ActionToActionObjShunt_1(Action_1_t3252573759 * value)
	{
		___s_ActionToActionObjShunt_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_ActionToActionObjShunt_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Threading.CancellationToken
struct CancellationToken_t784455623_marshaled_pinvoke
{
	CancellationTokenSource_t540272775 * ___m_source_0;
};
// Native definition for COM marshalling of System.Threading.CancellationToken
struct CancellationToken_t784455623_marshaled_com
{
	CancellationTokenSource_t540272775 * ___m_source_0;
};
#endif // CANCELLATIONTOKEN_T784455623_H
#ifndef POINT_T4164953539_H
#define POINT_T4164953539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Point
struct  Point_t4164953539 
{
public:
	// System.Single Windows.Foundation.Point::_x
	float ____x_0;
	// System.Single Windows.Foundation.Point::_y
	float ____y_1;

public:
	inline static int32_t get_offset_of__x_0() { return static_cast<int32_t>(offsetof(Point_t4164953539, ____x_0)); }
	inline float get__x_0() const { return ____x_0; }
	inline float* get_address_of__x_0() { return &____x_0; }
	inline void set__x_0(float value)
	{
		____x_0 = value;
	}

	inline static int32_t get_offset_of__y_1() { return static_cast<int32_t>(offsetof(Point_t4164953539, ____y_1)); }
	inline float get__y_1() const { return ____y_1; }
	inline float* get_address_of__y_1() { return &____y_1; }
	inline void set__y_1(float value)
	{
		____y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT_T4164953539_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef CONFIGUREDTASKAWAITER_T4273446738_H
#define CONFIGUREDTASKAWAITER_T4273446738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32>
struct  ConfiguredTaskAwaiter_t4273446738 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_task
	Task_1_t61518632 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t4273446738, ___m_task_0)); }
	inline Task_1_t61518632 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t61518632 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t61518632 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t4273446738, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGUREDTASKAWAITER_T4273446738_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public MarshalByRefObject_t2760389100
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t156472862 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2974092902 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_t1273022909, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_t156472862 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_t156472862 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_t156472862 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_t1273022909, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2974092902 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2974092902 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2974092902 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_1)); }
	inline Stream_t1273022909 * get_Null_1() const { return ___Null_1; }
	inline Stream_t1273022909 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t1273022909 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef CONFIGUREDTASKAWAITER_T555647845_H
#define CONFIGUREDTASKAWAITER_T555647845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter
struct  ConfiguredTaskAwaiter_t555647845 
{
public:
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter::m_task
	Task_t3187275312 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t555647845, ___m_task_0)); }
	inline Task_t3187275312 * get_m_task_0() const { return ___m_task_0; }
	inline Task_t3187275312 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_t3187275312 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t555647845, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter
struct ConfiguredTaskAwaiter_t555647845_marshaled_pinvoke
{
	Task_t3187275312 * ___m_task_0;
	int32_t ___m_continueOnCapturedContext_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter
struct ConfiguredTaskAwaiter_t555647845_marshaled_com
{
	Task_t3187275312 * ___m_task_0;
	int32_t ___m_continueOnCapturedContext_1;
};
#endif // CONFIGUREDTASKAWAITER_T555647845_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_4;
	// System.Int16 System.Guid::_b
	int16_t ____b_5;
	// System.Int16 System.Guid::_c
	int16_t ____c_6;
	// System.Byte System.Guid::_d
	uint8_t ____d_7;
	// System.Byte System.Guid::_e
	uint8_t ____e_8;
	// System.Byte System.Guid::_f
	uint8_t ____f_9;
	// System.Byte System.Guid::_g
	uint8_t ____g_10;
	// System.Byte System.Guid::_h
	uint8_t ____h_11;
	// System.Byte System.Guid::_i
	uint8_t ____i_12;
	// System.Byte System.Guid::_j
	uint8_t ____j_13;
	// System.Byte System.Guid::_k
	uint8_t ____k_14;

public:
	inline static int32_t get_offset_of__a_4() { return static_cast<int32_t>(offsetof(Guid_t, ____a_4)); }
	inline int32_t get__a_4() const { return ____a_4; }
	inline int32_t* get_address_of__a_4() { return &____a_4; }
	inline void set__a_4(int32_t value)
	{
		____a_4 = value;
	}

	inline static int32_t get_offset_of__b_5() { return static_cast<int32_t>(offsetof(Guid_t, ____b_5)); }
	inline int16_t get__b_5() const { return ____b_5; }
	inline int16_t* get_address_of__b_5() { return &____b_5; }
	inline void set__b_5(int16_t value)
	{
		____b_5 = value;
	}

	inline static int32_t get_offset_of__c_6() { return static_cast<int32_t>(offsetof(Guid_t, ____c_6)); }
	inline int16_t get__c_6() const { return ____c_6; }
	inline int16_t* get_address_of__c_6() { return &____c_6; }
	inline void set__c_6(int16_t value)
	{
		____c_6 = value;
	}

	inline static int32_t get_offset_of__d_7() { return static_cast<int32_t>(offsetof(Guid_t, ____d_7)); }
	inline uint8_t get__d_7() const { return ____d_7; }
	inline uint8_t* get_address_of__d_7() { return &____d_7; }
	inline void set__d_7(uint8_t value)
	{
		____d_7 = value;
	}

	inline static int32_t get_offset_of__e_8() { return static_cast<int32_t>(offsetof(Guid_t, ____e_8)); }
	inline uint8_t get__e_8() const { return ____e_8; }
	inline uint8_t* get_address_of__e_8() { return &____e_8; }
	inline void set__e_8(uint8_t value)
	{
		____e_8 = value;
	}

	inline static int32_t get_offset_of__f_9() { return static_cast<int32_t>(offsetof(Guid_t, ____f_9)); }
	inline uint8_t get__f_9() const { return ____f_9; }
	inline uint8_t* get_address_of__f_9() { return &____f_9; }
	inline void set__f_9(uint8_t value)
	{
		____f_9 = value;
	}

	inline static int32_t get_offset_of__g_10() { return static_cast<int32_t>(offsetof(Guid_t, ____g_10)); }
	inline uint8_t get__g_10() const { return ____g_10; }
	inline uint8_t* get_address_of__g_10() { return &____g_10; }
	inline void set__g_10(uint8_t value)
	{
		____g_10 = value;
	}

	inline static int32_t get_offset_of__h_11() { return static_cast<int32_t>(offsetof(Guid_t, ____h_11)); }
	inline uint8_t get__h_11() const { return ____h_11; }
	inline uint8_t* get_address_of__h_11() { return &____h_11; }
	inline void set__h_11(uint8_t value)
	{
		____h_11 = value;
	}

	inline static int32_t get_offset_of__i_12() { return static_cast<int32_t>(offsetof(Guid_t, ____i_12)); }
	inline uint8_t get__i_12() const { return ____i_12; }
	inline uint8_t* get_address_of__i_12() { return &____i_12; }
	inline void set__i_12(uint8_t value)
	{
		____i_12 = value;
	}

	inline static int32_t get_offset_of__j_13() { return static_cast<int32_t>(offsetof(Guid_t, ____j_13)); }
	inline uint8_t get__j_13() const { return ____j_13; }
	inline uint8_t* get_address_of__j_13() { return &____j_13; }
	inline void set__j_13(uint8_t value)
	{
		____j_13 = value;
	}

	inline static int32_t get_offset_of__k_14() { return static_cast<int32_t>(offsetof(Guid_t, ____k_14)); }
	inline uint8_t get__k_14() const { return ____k_14; }
	inline uint8_t* get_address_of__k_14() { return &____k_14; }
	inline void set__k_14(uint8_t value)
	{
		____k_14 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_0;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_1;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_2;
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_3;

public:
	inline static int32_t get_offset_of__rngAccess_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_0)); }
	inline RuntimeObject * get__rngAccess_0() const { return ____rngAccess_0; }
	inline RuntimeObject ** get_address_of__rngAccess_0() { return &____rngAccess_0; }
	inline void set__rngAccess_0(RuntimeObject * value)
	{
		____rngAccess_0 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_0), value);
	}

	inline static int32_t get_offset_of__rng_1() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_1)); }
	inline RandomNumberGenerator_t386037858 * get__rng_1() const { return ____rng_1; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_1() { return &____rng_1; }
	inline void set__rng_1(RandomNumberGenerator_t386037858 * value)
	{
		____rng_1 = value;
		Il2CppCodeGenWriteBarrier((&____rng_1), value);
	}

	inline static int32_t get_offset_of__fastRng_2() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_2)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_2() const { return ____fastRng_2; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_2() { return &____fastRng_2; }
	inline void set__fastRng_2(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_2 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_2), value);
	}

	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_3)); }
	inline Guid_t  get_Empty_3() const { return ___Empty_3; }
	inline Guid_t * get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(Guid_t  value)
	{
		___Empty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef CONFIGUREDTASKAWAITER_T1863693214_H
#define CONFIGUREDTASKAWAITER_T1863693214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<Windows.Storage.Streams.IBuffer>
struct  ConfiguredTaskAwaiter_t1863693214 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_task
	Task_1_t1946732404 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t1863693214, ___m_task_0)); }
	inline Task_1_t1946732404 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t1946732404 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t1946732404 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t1863693214, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGUREDTASKAWAITER_T1863693214_H
#ifndef ASYNCMETHODBUILDERCORE_T2955600131_H
#define ASYNCMETHODBUILDERCORE_T2955600131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t2955600131 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t1264377477 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2955600131, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_stateMachine_0), value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2955600131, ___m_defaultContextAction_1)); }
	inline Action_t1264377477 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t1264377477 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t1264377477 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultContextAction_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2955600131_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2955600131_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
#endif // ASYNCMETHODBUILDERCORE_T2955600131_H
#ifndef REMOTEASYNCATTRIBUTE_T33115854_H
#define REMOTEASYNCATTRIBUTE_T33115854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.RemoteAsyncAttribute
struct  RemoteAsyncAttribute_t33115854  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTEASYNCATTRIBUTE_T33115854_H
#ifndef GCHANDLE_T3351438187_H
#define GCHANDLE_T3351438187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t3351438187 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t3351438187, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T3351438187_H
#ifndef RECT_T2695113487_H
#define RECT_T2695113487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Rect
struct  Rect_t2695113487 
{
public:
	// System.Single Windows.Foundation.Rect::_x
	float ____x_0;
	// System.Single Windows.Foundation.Rect::_y
	float ____y_1;
	// System.Single Windows.Foundation.Rect::_width
	float ____width_2;
	// System.Single Windows.Foundation.Rect::_height
	float ____height_3;

public:
	inline static int32_t get_offset_of__x_0() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____x_0)); }
	inline float get__x_0() const { return ____x_0; }
	inline float* get_address_of__x_0() { return &____x_0; }
	inline void set__x_0(float value)
	{
		____x_0 = value;
	}

	inline static int32_t get_offset_of__y_1() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____y_1)); }
	inline float get__y_1() const { return ____y_1; }
	inline float* get_address_of__y_1() { return &____y_1; }
	inline void set__y_1(float value)
	{
		____y_1 = value;
	}

	inline static int32_t get_offset_of__width_2() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____width_2)); }
	inline float get__width_2() const { return ____width_2; }
	inline float* get_address_of__width_2() { return &____width_2; }
	inline void set__width_2(float value)
	{
		____width_2 = value;
	}

	inline static int32_t get_offset_of__height_3() { return static_cast<int32_t>(offsetof(Rect_t2695113487, ____height_3)); }
	inline float get__height_3() const { return ____height_3; }
	inline float* get_address_of__height_3() { return &____height_3; }
	inline void set__height_3(float value)
	{
		____height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2695113487_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef DEFAULTATTRIBUTE_T1509828399_H
#define DEFAULTATTRIBUTE_T1509828399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.DefaultAttribute
struct  DefaultAttribute_t1509828399  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTATTRIBUTE_T1509828399_H
#ifndef DATETIME_T1679451545_H
#define DATETIME_T1679451545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.DateTime
struct  DateTime_t1679451545 
{
public:
	// System.Int64 Windows.Foundation.DateTime::UniversalTime
	int64_t ___UniversalTime_0;

public:
	inline static int32_t get_offset_of_UniversalTime_0() { return static_cast<int32_t>(offsetof(DateTime_t1679451545, ___UniversalTime_0)); }
	inline int64_t get_UniversalTime_0() const { return ___UniversalTime_0; }
	inline int64_t* get_address_of_UniversalTime_0() { return &___UniversalTime_0; }
	inline void set_UniversalTime_0(int64_t value)
	{
		___UniversalTime_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T1679451545_H
#ifndef RECT_T2695113488_H
#define RECT_T2695113488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Rect
struct  Rect_t2695113488 
{
public:
	// System.Single Windows.Foundation.Rect::X
	float ___X_0;
	// System.Single Windows.Foundation.Rect::Y
	float ___Y_1;
	// System.Single Windows.Foundation.Rect::Width
	float ___Width_2;
	// System.Single Windows.Foundation.Rect::Height
	float ___Height_3;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(Rect_t2695113488, ___X_0)); }
	inline float get_X_0() const { return ___X_0; }
	inline float* get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(float value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(Rect_t2695113488, ___Y_1)); }
	inline float get_Y_1() const { return ___Y_1; }
	inline float* get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(float value)
	{
		___Y_1 = value;
	}

	inline static int32_t get_offset_of_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2695113488, ___Width_2)); }
	inline float get_Width_2() const { return ___Width_2; }
	inline float* get_address_of_Width_2() { return &___Width_2; }
	inline void set_Width_2(float value)
	{
		___Width_2 = value;
	}

	inline static int32_t get_offset_of_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2695113488, ___Height_3)); }
	inline float get_Height_3() const { return ___Height_3; }
	inline float* get_address_of_Height_3() { return &___Height_3; }
	inline void set_Height_3(float value)
	{
		___Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2695113488_H
#ifndef TASKTOASYNCACTIONADAPTER_T900468660_H
#define TASKTOASYNCACTIONADAPTER_T900468660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.TaskToAsyncActionAdapter
struct  TaskToAsyncActionAdapter_t900468660  : public TaskToAsyncInfoAdapter_4_t1408863619
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKTOASYNCACTIONADAPTER_T900468660_H
#ifndef TIMESPAN_T1755640982_H
#define TIMESPAN_T1755640982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.TimeSpan
struct  TimeSpan_t1755640982 
{
public:
	// System.Int64 Windows.Foundation.TimeSpan::Duration
	int64_t ___Duration_0;

public:
	inline static int32_t get_offset_of_Duration_0() { return static_cast<int32_t>(offsetof(TimeSpan_t1755640982, ___Duration_0)); }
	inline int64_t get_Duration_0() const { return ___Duration_0; }
	inline int64_t* get_address_of_Duration_0() { return &___Duration_0; }
	inline void set_Duration_0(int64_t value)
	{
		___Duration_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T1755640982_H
#ifndef WEBHOSTHIDDENATTRIBUTE_T2021537596_H
#define WEBHOSTHIDDENATTRIBUTE_T2021537596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.WebHostHiddenAttribute
struct  WebHostHiddenAttribute_t2021537596  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBHOSTHIDDENATTRIBUTE_T2021537596_H
#ifndef DUALAPIPARTITIONATTRIBUTE_T1301304994_H
#define DUALAPIPARTITIONATTRIBUTE_T1301304994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.DualApiPartitionAttribute
struct  DualApiPartitionAttribute_t1301304994  : public Attribute_t861562559
{
public:
	// System.UInt32 Windows.Foundation.Metadata.DualApiPartitionAttribute::version
	uint32_t ___version_0;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(DualApiPartitionAttribute_t1301304994, ___version_0)); }
	inline uint32_t get_version_0() const { return ___version_0; }
	inline uint32_t* get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(uint32_t value)
	{
		___version_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUALAPIPARTITIONATTRIBUTE_T1301304994_H
#ifndef MUSEATTRIBUTE_T3401286167_H
#define MUSEATTRIBUTE_T3401286167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.MuseAttribute
struct  MuseAttribute_t3401286167  : public Attribute_t861562559
{
public:
	// System.UInt32 Windows.Foundation.Metadata.MuseAttribute::Version
	uint32_t ___Version_0;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(MuseAttribute_t3401286167, ___Version_0)); }
	inline uint32_t get_Version_0() const { return ___Version_0; }
	inline uint32_t* get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(uint32_t value)
	{
		___Version_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MUSEATTRIBUTE_T3401286167_H
#ifndef SIZE_T550917639_H
#define SIZE_T550917639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Size
struct  Size_t550917639 
{
public:
	// System.Single Windows.Foundation.Size::Width
	float ___Width_0;
	// System.Single Windows.Foundation.Size::Height
	float ___Height_1;

public:
	inline static int32_t get_offset_of_Width_0() { return static_cast<int32_t>(offsetof(Size_t550917639, ___Width_0)); }
	inline float get_Width_0() const { return ___Width_0; }
	inline float* get_address_of_Width_0() { return &___Width_0; }
	inline void set_Width_0(float value)
	{
		___Width_0 = value;
	}

	inline static int32_t get_offset_of_Height_1() { return static_cast<int32_t>(offsetof(Size_t550917639, ___Height_1)); }
	inline float get_Height_1() const { return ___Height_1; }
	inline float* get_address_of_Height_1() { return &___Height_1; }
	inline void set_Height_1(float value)
	{
		___Height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIZE_T550917639_H
#ifndef POINT_T4164953540_H
#define POINT_T4164953540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Point
struct  Point_t4164953540 
{
public:
	// System.Single Windows.Foundation.Point::X
	float ___X_0;
	// System.Single Windows.Foundation.Point::Y
	float ___Y_1;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(Point_t4164953540, ___X_0)); }
	inline float get_X_0() const { return ___X_0; }
	inline float* get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(float value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(Point_t4164953540, ___Y_1)); }
	inline float get_Y_1() const { return ___Y_1; }
	inline float* get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(float value)
	{
		___Y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT_T4164953540_H
#ifndef ALLOWMULTIPLEATTRIBUTE_T3314144467_H
#define ALLOWMULTIPLEATTRIBUTE_T3314144467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.AllowMultipleAttribute
struct  AllowMultipleAttribute_t3314144467  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLOWMULTIPLEATTRIBUTE_T3314144467_H
#ifndef CONTRACTVERSIONATTRIBUTE_T1666784187_H
#define CONTRACTVERSIONATTRIBUTE_T1666784187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.ContractVersionAttribute
struct  ContractVersionAttribute_t1666784187  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRACTVERSIONATTRIBUTE_T1666784187_H
#ifndef ATTRIBUTEUSAGEATTRIBUTE_T1422441135_H
#define ATTRIBUTEUSAGEATTRIBUTE_T1422441135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.AttributeUsageAttribute
struct  AttributeUsageAttribute_t1422441135  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEUSAGEATTRIBUTE_T1422441135_H
#ifndef SIZE_T550917638_H
#define SIZE_T550917638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Size
struct  Size_t550917638 
{
public:
	// System.Single Windows.Foundation.Size::_width
	float ____width_0;
	// System.Single Windows.Foundation.Size::_height
	float ____height_1;

public:
	inline static int32_t get_offset_of__width_0() { return static_cast<int32_t>(offsetof(Size_t550917638, ____width_0)); }
	inline float get__width_0() const { return ____width_0; }
	inline float* get_address_of__width_0() { return &____width_0; }
	inline void set__width_0(float value)
	{
		____width_0 = value;
	}

	inline static int32_t get_offset_of__height_1() { return static_cast<int32_t>(offsetof(Size_t550917638, ____height_1)); }
	inline float get__height_1() const { return ____height_1; }
	inline float* get_address_of__height_1() { return &____height_1; }
	inline void set__height_1(float value)
	{
		____height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIZE_T550917638_H
#ifndef VOIDVALUETYPEPARAMETER_T130952355_H
#define VOIDVALUETYPEPARAMETER_T130952355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.VoidValueTypeParameter
struct  VoidValueTypeParameter_t130952355 
{
public:
	union
	{
		struct
		{
		};
		uint8_t VoidValueTypeParameter_t130952355__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOIDVALUETYPEPARAMETER_T130952355_H
#ifndef DEFAULTOVERLOADATTRIBUTE_T2513707992_H
#define DEFAULTOVERLOADATTRIBUTE_T2513707992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.DefaultOverloadAttribute
struct  DefaultOverloadAttribute_t2513707992  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTOVERLOADATTRIBUTE_T2513707992_H
#ifndef ACTIVATABLEATTRIBUTE_T4098936231_H
#define ACTIVATABLEATTRIBUTE_T4098936231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.ActivatableAttribute
struct  ActivatableAttribute_t4098936231  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATABLEATTRIBUTE_T4098936231_H
#ifndef APIINFORMATION_T812843232_H
#define APIINFORMATION_T812843232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.ApiInformation
struct  ApiInformation_t812843232  : public Il2CppComObject
{
public:

public:
};

struct ApiInformation_t812843232_StaticFields
{
public:
	// Cached pointer to IActivationFactory
	Il2CppIActivationFactory* activationFactory;
	// Cached pointer to Windows.Foundation.Metadata.IApiInformationStatics
	IApiInformationStatics_t1368946868* ____iapiInformationStatics_t1368946868;

public:
	inline Il2CppIActivationFactory* get_activationFactory()
	{
		Il2CppIActivationFactory* returnValue = activationFactory;
		if (returnValue == NULL)
		{
			il2cpp::utils::StringView<Il2CppNativeChar> className(IL2CPP_NATIVE_STRING("Windows.Foundation.Metadata.ApiInformation"));
			returnValue = il2cpp_codegen_windows_runtime_get_activation_factory(className);

			if (il2cpp_codegen_atomic_compare_exchange_pointer<Il2CppIActivationFactory>((&activationFactory), returnValue, NULL) != NULL)
			{
				returnValue->Release();
				returnValue = activationFactory;
			}
		}
		return returnValue;
	}

	inline IApiInformationStatics_t1368946868* get_____iapiInformationStatics_t1368946868()
	{
		IApiInformationStatics_t1368946868* returnValue = ____iapiInformationStatics_t1368946868;
		if (returnValue == NULL)
		{
			const il2cpp_hresult_t hr = get_activationFactory()->QueryInterface(IApiInformationStatics_t1368946868::IID, reinterpret_cast<void**>(&returnValue));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);

			if (il2cpp_codegen_atomic_compare_exchange_pointer<IApiInformationStatics_t1368946868>((&____iapiInformationStatics_t1368946868), returnValue, NULL) != NULL)
			{
				returnValue->Release();
				returnValue = ____iapiInformationStatics_t1368946868;
			}
		}
		return returnValue;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIINFORMATION_T812843232_H
#ifndef ASYNCTASKMETHODBUILDER_1_T2862166739_H
#define ASYNCTASKMETHODBUILDER_1_T2862166739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<Windows.Storage.Streams.IBuffer>
struct  AsyncTaskMethodBuilder_1_t2862166739 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2955600131  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t1946732404 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2862166739, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2955600131  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2955600131 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2955600131  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2862166739, ___m_task_2)); }
	inline Task_1_t1946732404 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t1946732404 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t1946732404 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t2862166739_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t1946732404 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2862166739_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t1946732404 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t1946732404 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t1946732404 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T2862166739_H
#ifndef INPUTSTREAMOPTIONS_T2901738979_H
#define INPUTSTREAMOPTIONS_T2901738979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Storage.Streams.InputStreamOptions
struct  InputStreamOptions_t2901738979 
{
public:
	// System.UInt32 Windows.Storage.Streams.InputStreamOptions::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputStreamOptions_t2901738979, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSTREAMOPTIONS_T2901738979_H
#ifndef ASYNCTASKMETHODBUILDER_1_T2418262475_H
#define ASYNCTASKMETHODBUILDER_1_T2418262475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>
struct  AsyncTaskMethodBuilder_1_t2418262475 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2955600131  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t1502828140 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2418262475, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2955600131  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2955600131 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2955600131  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2418262475, ___m_task_2)); }
	inline Task_1_t1502828140 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t1502828140 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t1502828140 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t2418262475_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t1502828140 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2418262475_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t1502828140 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t1502828140 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t1502828140 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T2418262475_H
#ifndef ASYNCTASKMETHODBUILDER_1_T586069192_H
#define ASYNCTASKMETHODBUILDER_1_T586069192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.UInt32>
struct  AsyncTaskMethodBuilder_1_t586069192 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2955600131  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t3965602153 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t586069192, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2955600131  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2955600131 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2955600131  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t586069192, ___m_task_2)); }
	inline Task_1_t3965602153 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t3965602153 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t3965602153 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t586069192_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t3965602153 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t586069192_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t3965602153 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t3965602153 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t3965602153 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T586069192_H
#ifndef WINDOWSRUNTIMEBUFFER_T3806548453_H
#define WINDOWSRUNTIMEBUFFER_T3806548453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeBuffer
struct  WindowsRuntimeBuffer_t3806548453  : public RuntimeObject
{
public:
	// System.Byte[] System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeBuffer::_data
	ByteU5BU5D_t4116647657* ____data_1;
	// System.Int32 System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeBuffer::_dataStartOffs
	int32_t ____dataStartOffs_2;
	// System.Int32 System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeBuffer::_usefulDataLength
	int32_t ____usefulDataLength_3;
	// System.Int32 System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeBuffer::_maxDataCapacity
	int32_t ____maxDataCapacity_4;
	// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeBuffer::_pinHandle
	GCHandle_t3351438187  ____pinHandle_5;
	// System.IntPtr System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeBuffer::_dataPtr
	intptr_t ____dataPtr_6;

public:
	inline static int32_t get_offset_of__data_1() { return static_cast<int32_t>(offsetof(WindowsRuntimeBuffer_t3806548453, ____data_1)); }
	inline ByteU5BU5D_t4116647657* get__data_1() const { return ____data_1; }
	inline ByteU5BU5D_t4116647657** get_address_of__data_1() { return &____data_1; }
	inline void set__data_1(ByteU5BU5D_t4116647657* value)
	{
		____data_1 = value;
		Il2CppCodeGenWriteBarrier((&____data_1), value);
	}

	inline static int32_t get_offset_of__dataStartOffs_2() { return static_cast<int32_t>(offsetof(WindowsRuntimeBuffer_t3806548453, ____dataStartOffs_2)); }
	inline int32_t get__dataStartOffs_2() const { return ____dataStartOffs_2; }
	inline int32_t* get_address_of__dataStartOffs_2() { return &____dataStartOffs_2; }
	inline void set__dataStartOffs_2(int32_t value)
	{
		____dataStartOffs_2 = value;
	}

	inline static int32_t get_offset_of__usefulDataLength_3() { return static_cast<int32_t>(offsetof(WindowsRuntimeBuffer_t3806548453, ____usefulDataLength_3)); }
	inline int32_t get__usefulDataLength_3() const { return ____usefulDataLength_3; }
	inline int32_t* get_address_of__usefulDataLength_3() { return &____usefulDataLength_3; }
	inline void set__usefulDataLength_3(int32_t value)
	{
		____usefulDataLength_3 = value;
	}

	inline static int32_t get_offset_of__maxDataCapacity_4() { return static_cast<int32_t>(offsetof(WindowsRuntimeBuffer_t3806548453, ____maxDataCapacity_4)); }
	inline int32_t get__maxDataCapacity_4() const { return ____maxDataCapacity_4; }
	inline int32_t* get_address_of__maxDataCapacity_4() { return &____maxDataCapacity_4; }
	inline void set__maxDataCapacity_4(int32_t value)
	{
		____maxDataCapacity_4 = value;
	}

	inline static int32_t get_offset_of__pinHandle_5() { return static_cast<int32_t>(offsetof(WindowsRuntimeBuffer_t3806548453, ____pinHandle_5)); }
	inline GCHandle_t3351438187  get__pinHandle_5() const { return ____pinHandle_5; }
	inline GCHandle_t3351438187 * get_address_of__pinHandle_5() { return &____pinHandle_5; }
	inline void set__pinHandle_5(GCHandle_t3351438187  value)
	{
		____pinHandle_5 = value;
	}

	inline static int32_t get_offset_of__dataPtr_6() { return static_cast<int32_t>(offsetof(WindowsRuntimeBuffer_t3806548453, ____dataPtr_6)); }
	inline intptr_t get__dataPtr_6() const { return ____dataPtr_6; }
	inline intptr_t* get_address_of__dataPtr_6() { return &____dataPtr_6; }
	inline void set__dataPtr_6(intptr_t value)
	{
		____dataPtr_6 = value;
	}
};

struct WindowsRuntimeBuffer_t3806548453_ThreadStaticFields
{
public:
	// System.Runtime.InteropServices.IMarshal System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeBuffer::s_winRtMarshalProxy
	RuntimeObject* ___s_winRtMarshalProxy_0;

public:
	inline static int32_t get_offset_of_s_winRtMarshalProxy_0() { return static_cast<int32_t>(offsetof(WindowsRuntimeBuffer_t3806548453_ThreadStaticFields, ___s_winRtMarshalProxy_0)); }
	inline RuntimeObject* get_s_winRtMarshalProxy_0() const { return ___s_winRtMarshalProxy_0; }
	inline RuntimeObject** get_address_of_s_winRtMarshalProxy_0() { return &___s_winRtMarshalProxy_0; }
	inline void set_s_winRtMarshalProxy_0(RuntimeObject* value)
	{
		___s_winRtMarshalProxy_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_winRtMarshalProxy_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWSRUNTIMEBUFFER_T3806548453_H
#ifndef ATTRIBUTETARGETS_T1784037988_H
#define ATTRIBUTETARGETS_T1784037988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AttributeTargets
struct  AttributeTargets_t1784037988 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t1784037988, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTETARGETS_T1784037988_H
#ifndef FILEACCESS_T1659085276_H
#define FILEACCESS_T1659085276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAccess
struct  FileAccess_t1659085276 
{
public:
	// System.Int32 System.IO.FileAccess::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAccess_t1659085276, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEACCESS_T1659085276_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_19)); }
	inline TimeSpan_t881159249  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_t881159249 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_t881159249  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_t881159249  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_t881159249  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_21)); }
	inline TimeSpan_t881159249  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_t881159249  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef ASYNCTASKMETHODBUILDER_1_T976952967_H
#define ASYNCTASKMETHODBUILDER_1_T976952967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Int32>
struct  AsyncTaskMethodBuilder_1_t976952967 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2955600131  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t61518632 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t976952967, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2955600131  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2955600131 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2955600131  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t976952967, ___m_task_2)); }
	inline Task_1_t61518632 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t61518632 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t61518632 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t976952967_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t61518632 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t976952967_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t61518632 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t61518632 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t61518632 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T976952967_H
#ifndef STREAMOPERATIONASYNCRESULT_T2370793485_H
#define STREAMOPERATIONASYNCRESULT_T2370793485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamOperationAsyncResult
struct  StreamOperationAsyncResult_t2370793485  : public RuntimeObject
{
public:
	// System.AsyncCallback System.IO.StreamOperationAsyncResult::_userCompletionCallback
	AsyncCallback_t3962456242 * ____userCompletionCallback_0;
	// System.Object System.IO.StreamOperationAsyncResult::_userAsyncStateInfo
	RuntimeObject * ____userAsyncStateInfo_1;
	// Windows.Foundation.IAsyncInfo System.IO.StreamOperationAsyncResult::_asyncStreamOperation
	RuntimeObject* ____asyncStreamOperation_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.IO.StreamOperationAsyncResult::_completed
	bool ____completed_3;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.IO.StreamOperationAsyncResult::_callbackInvoked
	bool ____callbackInvoked_4;
	// System.Threading.ManualResetEvent modreq(System.Runtime.CompilerServices.IsVolatile) System.IO.StreamOperationAsyncResult::_waitHandle
	ManualResetEvent_t451242010 * ____waitHandle_5;
	// System.Int64 System.IO.StreamOperationAsyncResult::_bytesCompleted
	int64_t ____bytesCompleted_6;
	// System.Runtime.ExceptionServices.ExceptionDispatchInfo System.IO.StreamOperationAsyncResult::_errorInfo
	ExceptionDispatchInfo_t3750997369 * ____errorInfo_7;
	// System.Boolean System.IO.StreamOperationAsyncResult::_processCompletedOperationInCallback
	bool ____processCompletedOperationInCallback_8;
	// Windows.Foundation.IAsyncInfo System.IO.StreamOperationAsyncResult::_completedOperation
	RuntimeObject* ____completedOperation_9;

public:
	inline static int32_t get_offset_of__userCompletionCallback_0() { return static_cast<int32_t>(offsetof(StreamOperationAsyncResult_t2370793485, ____userCompletionCallback_0)); }
	inline AsyncCallback_t3962456242 * get__userCompletionCallback_0() const { return ____userCompletionCallback_0; }
	inline AsyncCallback_t3962456242 ** get_address_of__userCompletionCallback_0() { return &____userCompletionCallback_0; }
	inline void set__userCompletionCallback_0(AsyncCallback_t3962456242 * value)
	{
		____userCompletionCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&____userCompletionCallback_0), value);
	}

	inline static int32_t get_offset_of__userAsyncStateInfo_1() { return static_cast<int32_t>(offsetof(StreamOperationAsyncResult_t2370793485, ____userAsyncStateInfo_1)); }
	inline RuntimeObject * get__userAsyncStateInfo_1() const { return ____userAsyncStateInfo_1; }
	inline RuntimeObject ** get_address_of__userAsyncStateInfo_1() { return &____userAsyncStateInfo_1; }
	inline void set__userAsyncStateInfo_1(RuntimeObject * value)
	{
		____userAsyncStateInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&____userAsyncStateInfo_1), value);
	}

	inline static int32_t get_offset_of__asyncStreamOperation_2() { return static_cast<int32_t>(offsetof(StreamOperationAsyncResult_t2370793485, ____asyncStreamOperation_2)); }
	inline RuntimeObject* get__asyncStreamOperation_2() const { return ____asyncStreamOperation_2; }
	inline RuntimeObject** get_address_of__asyncStreamOperation_2() { return &____asyncStreamOperation_2; }
	inline void set__asyncStreamOperation_2(RuntimeObject* value)
	{
		____asyncStreamOperation_2 = value;
		Il2CppCodeGenWriteBarrier((&____asyncStreamOperation_2), value);
	}

	inline static int32_t get_offset_of__completed_3() { return static_cast<int32_t>(offsetof(StreamOperationAsyncResult_t2370793485, ____completed_3)); }
	inline bool get__completed_3() const { return ____completed_3; }
	inline bool* get_address_of__completed_3() { return &____completed_3; }
	inline void set__completed_3(bool value)
	{
		____completed_3 = value;
	}

	inline static int32_t get_offset_of__callbackInvoked_4() { return static_cast<int32_t>(offsetof(StreamOperationAsyncResult_t2370793485, ____callbackInvoked_4)); }
	inline bool get__callbackInvoked_4() const { return ____callbackInvoked_4; }
	inline bool* get_address_of__callbackInvoked_4() { return &____callbackInvoked_4; }
	inline void set__callbackInvoked_4(bool value)
	{
		____callbackInvoked_4 = value;
	}

	inline static int32_t get_offset_of__waitHandle_5() { return static_cast<int32_t>(offsetof(StreamOperationAsyncResult_t2370793485, ____waitHandle_5)); }
	inline ManualResetEvent_t451242010 * get__waitHandle_5() const { return ____waitHandle_5; }
	inline ManualResetEvent_t451242010 ** get_address_of__waitHandle_5() { return &____waitHandle_5; }
	inline void set__waitHandle_5(ManualResetEvent_t451242010 * value)
	{
		____waitHandle_5 = value;
		Il2CppCodeGenWriteBarrier((&____waitHandle_5), value);
	}

	inline static int32_t get_offset_of__bytesCompleted_6() { return static_cast<int32_t>(offsetof(StreamOperationAsyncResult_t2370793485, ____bytesCompleted_6)); }
	inline int64_t get__bytesCompleted_6() const { return ____bytesCompleted_6; }
	inline int64_t* get_address_of__bytesCompleted_6() { return &____bytesCompleted_6; }
	inline void set__bytesCompleted_6(int64_t value)
	{
		____bytesCompleted_6 = value;
	}

	inline static int32_t get_offset_of__errorInfo_7() { return static_cast<int32_t>(offsetof(StreamOperationAsyncResult_t2370793485, ____errorInfo_7)); }
	inline ExceptionDispatchInfo_t3750997369 * get__errorInfo_7() const { return ____errorInfo_7; }
	inline ExceptionDispatchInfo_t3750997369 ** get_address_of__errorInfo_7() { return &____errorInfo_7; }
	inline void set__errorInfo_7(ExceptionDispatchInfo_t3750997369 * value)
	{
		____errorInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&____errorInfo_7), value);
	}

	inline static int32_t get_offset_of__processCompletedOperationInCallback_8() { return static_cast<int32_t>(offsetof(StreamOperationAsyncResult_t2370793485, ____processCompletedOperationInCallback_8)); }
	inline bool get__processCompletedOperationInCallback_8() const { return ____processCompletedOperationInCallback_8; }
	inline bool* get_address_of__processCompletedOperationInCallback_8() { return &____processCompletedOperationInCallback_8; }
	inline void set__processCompletedOperationInCallback_8(bool value)
	{
		____processCompletedOperationInCallback_8 = value;
	}

	inline static int32_t get_offset_of__completedOperation_9() { return static_cast<int32_t>(offsetof(StreamOperationAsyncResult_t2370793485, ____completedOperation_9)); }
	inline RuntimeObject* get__completedOperation_9() const { return ____completedOperation_9; }
	inline RuntimeObject** get_address_of__completedOperation_9() { return &____completedOperation_9; }
	inline void set__completedOperation_9(RuntimeObject* value)
	{
		____completedOperation_9 = value;
		Il2CppCodeGenWriteBarrier((&____completedOperation_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMOPERATIONASYNCRESULT_T2370793485_H
#ifndef STREAMREADOPERATIONOPTIMIZATION_T12377751_H
#define STREAMREADOPERATIONOPTIMIZATION_T12377751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.NetFxToWinRtStreamAdapter/StreamReadOperationOptimization
struct  StreamReadOperationOptimization_t12377751 
{
public:
	// System.Int32 System.IO.NetFxToWinRtStreamAdapter/StreamReadOperationOptimization::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamReadOperationOptimization_t12377751, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMREADOPERATIONOPTIMIZATION_T12377751_H
#ifndef WINRTTONETFXSTREAMADAPTER_T1366777568_H
#define WINRTTONETFXSTREAMADAPTER_T1366777568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.WinRtToNetFxStreamAdapter
struct  WinRtToNetFxStreamAdapter_t1366777568  : public Stream_t1273022909
{
public:
	// System.Byte[] System.IO.WinRtToNetFxStreamAdapter::_oneByteBuffer
	ByteU5BU5D_t4116647657* ____oneByteBuffer_5;
	// System.Boolean System.IO.WinRtToNetFxStreamAdapter::_leaveUnderlyingStreamOpen
	bool ____leaveUnderlyingStreamOpen_6;
	// System.Object System.IO.WinRtToNetFxStreamAdapter::_winRtStream
	RuntimeObject * ____winRtStream_7;
	// System.Boolean System.IO.WinRtToNetFxStreamAdapter::_canRead
	bool ____canRead_8;
	// System.Boolean System.IO.WinRtToNetFxStreamAdapter::_canWrite
	bool ____canWrite_9;
	// System.Boolean System.IO.WinRtToNetFxStreamAdapter::_canSeek
	bool ____canSeek_10;

public:
	inline static int32_t get_offset_of__oneByteBuffer_5() { return static_cast<int32_t>(offsetof(WinRtToNetFxStreamAdapter_t1366777568, ____oneByteBuffer_5)); }
	inline ByteU5BU5D_t4116647657* get__oneByteBuffer_5() const { return ____oneByteBuffer_5; }
	inline ByteU5BU5D_t4116647657** get_address_of__oneByteBuffer_5() { return &____oneByteBuffer_5; }
	inline void set__oneByteBuffer_5(ByteU5BU5D_t4116647657* value)
	{
		____oneByteBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&____oneByteBuffer_5), value);
	}

	inline static int32_t get_offset_of__leaveUnderlyingStreamOpen_6() { return static_cast<int32_t>(offsetof(WinRtToNetFxStreamAdapter_t1366777568, ____leaveUnderlyingStreamOpen_6)); }
	inline bool get__leaveUnderlyingStreamOpen_6() const { return ____leaveUnderlyingStreamOpen_6; }
	inline bool* get_address_of__leaveUnderlyingStreamOpen_6() { return &____leaveUnderlyingStreamOpen_6; }
	inline void set__leaveUnderlyingStreamOpen_6(bool value)
	{
		____leaveUnderlyingStreamOpen_6 = value;
	}

	inline static int32_t get_offset_of__winRtStream_7() { return static_cast<int32_t>(offsetof(WinRtToNetFxStreamAdapter_t1366777568, ____winRtStream_7)); }
	inline RuntimeObject * get__winRtStream_7() const { return ____winRtStream_7; }
	inline RuntimeObject ** get_address_of__winRtStream_7() { return &____winRtStream_7; }
	inline void set__winRtStream_7(RuntimeObject * value)
	{
		____winRtStream_7 = value;
		Il2CppCodeGenWriteBarrier((&____winRtStream_7), value);
	}

	inline static int32_t get_offset_of__canRead_8() { return static_cast<int32_t>(offsetof(WinRtToNetFxStreamAdapter_t1366777568, ____canRead_8)); }
	inline bool get__canRead_8() const { return ____canRead_8; }
	inline bool* get_address_of__canRead_8() { return &____canRead_8; }
	inline void set__canRead_8(bool value)
	{
		____canRead_8 = value;
	}

	inline static int32_t get_offset_of__canWrite_9() { return static_cast<int32_t>(offsetof(WinRtToNetFxStreamAdapter_t1366777568, ____canWrite_9)); }
	inline bool get__canWrite_9() const { return ____canWrite_9; }
	inline bool* get_address_of__canWrite_9() { return &____canWrite_9; }
	inline void set__canWrite_9(bool value)
	{
		____canWrite_9 = value;
	}

	inline static int32_t get_offset_of__canSeek_10() { return static_cast<int32_t>(offsetof(WinRtToNetFxStreamAdapter_t1366777568, ____canSeek_10)); }
	inline bool get__canSeek_10() const { return ____canSeek_10; }
	inline bool* get_address_of__canSeek_10() { return &____canSeek_10; }
	inline void set__canSeek_10(bool value)
	{
		____canSeek_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINRTTONETFXSTREAMADAPTER_T1366777568_H
#ifndef THREADINGMODEL_T2838021473_H
#define THREADINGMODEL_T2838021473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.ThreadingModel
struct  ThreadingModel_t2838021473 
{
public:
	// System.Int32 Windows.Foundation.Metadata.ThreadingModel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ThreadingModel_t2838021473, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADINGMODEL_T2838021473_H
#ifndef MARSHALINGTYPE_T3556196393_H
#define MARSHALINGTYPE_T3556196393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.MarshalingType
struct  MarshalingType_t3556196393 
{
public:
	// System.Int32 Windows.Foundation.Metadata.MarshalingType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MarshalingType_t3556196393, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALINGTYPE_T3556196393_H
#ifndef ASYNCSTATUS_T2150485874_H
#define ASYNCSTATUS_T2150485874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.AsyncStatus
struct  AsyncStatus_t2150485874 
{
public:
	// System.Int32 Windows.Foundation.AsyncStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AsyncStatus_t2150485874, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCSTATUS_T2150485874_H
#ifndef DEPRECATIONTYPE_T4182238482_H
#define DEPRECATIONTYPE_T4182238482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.DeprecationType
struct  DeprecationType_t4182238482 
{
public:
	// System.Int32 Windows.Foundation.Metadata.DeprecationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeprecationType_t4182238482, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPRECATIONTYPE_T4182238482_H
#ifndef PROPERTYTYPE_T3031428354_H
#define PROPERTYTYPE_T3031428354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.PropertyType
struct  PropertyType_t3031428354 
{
public:
	// System.Int32 Windows.Foundation.PropertyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PropertyType_t3031428354, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPE_T3031428354_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef COMPOSITIONTYPE_T2620517455_H
#define COMPOSITIONTYPE_T2620517455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.CompositionType
struct  CompositionType_t2620517455 
{
public:
	// System.Int32 Windows.Foundation.Metadata.CompositionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompositionType_t2620517455, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOSITIONTYPE_T2620517455_H
#ifndef U3CU3CFLUSHASYNC_ABSTRACTSTREAMU3EB__0U3ED_T1213545130_H
#define U3CU3CFLUSHASYNC_ABSTRACTSTREAMU3EB__0U3ED_T1213545130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamOperationsImplementation/<>c__DisplayClass3_0/<<FlushAsync_AbstractStream>b__0>d
struct  U3CU3CFlushAsync_AbstractStreamU3Eb__0U3Ed_t1213545130 
{
public:
	// System.Int32 System.IO.StreamOperationsImplementation/<>c__DisplayClass3_0/<<FlushAsync_AbstractStream>b__0>d::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean> System.IO.StreamOperationsImplementation/<>c__DisplayClass3_0/<<FlushAsync_AbstractStream>b__0>d::<>t__builder
	AsyncTaskMethodBuilder_1_t2418262475  ___U3CU3Et__builder_1;
	// System.Threading.CancellationToken System.IO.StreamOperationsImplementation/<>c__DisplayClass3_0/<<FlushAsync_AbstractStream>b__0>d::cancelToken
	CancellationToken_t784455623  ___cancelToken_2;
	// System.IO.StreamOperationsImplementation/<>c__DisplayClass3_0 System.IO.StreamOperationsImplementation/<>c__DisplayClass3_0/<<FlushAsync_AbstractStream>b__0>d::<>4__this
	U3CU3Ec__DisplayClass3_0_t3604542706 * ___U3CU3E4__this_3;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter System.IO.StreamOperationsImplementation/<>c__DisplayClass3_0/<<FlushAsync_AbstractStream>b__0>d::<>u__1
	ConfiguredTaskAwaiter_t555647845  ___U3CU3Eu__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CU3CFlushAsync_AbstractStreamU3Eb__0U3Ed_t1213545130, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CU3CFlushAsync_AbstractStreamU3Eb__0U3Ed_t1213545130, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t2418262475  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t2418262475 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t2418262475  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_cancelToken_2() { return static_cast<int32_t>(offsetof(U3CU3CFlushAsync_AbstractStreamU3Eb__0U3Ed_t1213545130, ___cancelToken_2)); }
	inline CancellationToken_t784455623  get_cancelToken_2() const { return ___cancelToken_2; }
	inline CancellationToken_t784455623 * get_address_of_cancelToken_2() { return &___cancelToken_2; }
	inline void set_cancelToken_2(CancellationToken_t784455623  value)
	{
		___cancelToken_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CU3CFlushAsync_AbstractStreamU3Eb__0U3Ed_t1213545130, ___U3CU3E4__this_3)); }
	inline U3CU3Ec__DisplayClass3_0_t3604542706 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline U3CU3Ec__DisplayClass3_0_t3604542706 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(U3CU3Ec__DisplayClass3_0_t3604542706 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_4() { return static_cast<int32_t>(offsetof(U3CU3CFlushAsync_AbstractStreamU3Eb__0U3Ed_t1213545130, ___U3CU3Eu__1_4)); }
	inline ConfiguredTaskAwaiter_t555647845  get_U3CU3Eu__1_4() const { return ___U3CU3Eu__1_4; }
	inline ConfiguredTaskAwaiter_t555647845 * get_address_of_U3CU3Eu__1_4() { return &___U3CU3Eu__1_4; }
	inline void set_U3CU3Eu__1_4(ConfiguredTaskAwaiter_t555647845  value)
	{
		___U3CU3Eu__1_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3CFLUSHASYNC_ABSTRACTSTREAMU3EB__0U3ED_T1213545130_H
#ifndef ATTRIBUTETARGETS_T2460853228_H
#define ATTRIBUTETARGETS_T2460853228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.Metadata.AttributeTargets
struct  AttributeTargets_t2460853228 
{
public:
	// System.UInt32 Windows.Foundation.Metadata.AttributeTargets::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t2460853228, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTETARGETS_T2460853228_H
#ifndef NETFXTOWINRTSTREAMADAPTER_T2688706982_H
#define NETFXTOWINRTSTREAMADAPTER_T2688706982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.NetFxToWinRtStreamAdapter
struct  NetFxToWinRtStreamAdapter_t2688706982  : public RuntimeObject
{
public:
	// System.IO.Stream System.IO.NetFxToWinRtStreamAdapter::_managedStream
	Stream_t1273022909 * ____managedStream_0;
	// System.Boolean System.IO.NetFxToWinRtStreamAdapter::_leaveUnderlyingStreamOpen
	bool ____leaveUnderlyingStreamOpen_1;
	// System.IO.NetFxToWinRtStreamAdapter/StreamReadOperationOptimization System.IO.NetFxToWinRtStreamAdapter::_readOptimization
	int32_t ____readOptimization_2;

public:
	inline static int32_t get_offset_of__managedStream_0() { return static_cast<int32_t>(offsetof(NetFxToWinRtStreamAdapter_t2688706982, ____managedStream_0)); }
	inline Stream_t1273022909 * get__managedStream_0() const { return ____managedStream_0; }
	inline Stream_t1273022909 ** get_address_of__managedStream_0() { return &____managedStream_0; }
	inline void set__managedStream_0(Stream_t1273022909 * value)
	{
		____managedStream_0 = value;
		Il2CppCodeGenWriteBarrier((&____managedStream_0), value);
	}

	inline static int32_t get_offset_of__leaveUnderlyingStreamOpen_1() { return static_cast<int32_t>(offsetof(NetFxToWinRtStreamAdapter_t2688706982, ____leaveUnderlyingStreamOpen_1)); }
	inline bool get__leaveUnderlyingStreamOpen_1() const { return ____leaveUnderlyingStreamOpen_1; }
	inline bool* get_address_of__leaveUnderlyingStreamOpen_1() { return &____leaveUnderlyingStreamOpen_1; }
	inline void set__leaveUnderlyingStreamOpen_1(bool value)
	{
		____leaveUnderlyingStreamOpen_1 = value;
	}

	inline static int32_t get_offset_of__readOptimization_2() { return static_cast<int32_t>(offsetof(NetFxToWinRtStreamAdapter_t2688706982, ____readOptimization_2)); }
	inline int32_t get__readOptimization_2() const { return ____readOptimization_2; }
	inline int32_t* get_address_of__readOptimization_2() { return &____readOptimization_2; }
	inline void set__readOptimization_2(int32_t value)
	{
		____readOptimization_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETFXTOWINRTSTREAMADAPTER_T2688706982_H
// Windows.Foundation.IPropertyValue
struct NOVTABLE IPropertyValue_t3169763779 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_get_Type_m1307170434(int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_get_IsNumericScalar_m907101182(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetUInt8_m2421114344(uint8_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetInt16_m4200981392(int16_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetUInt16_m4219279933(uint16_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetInt32_m408686053(int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetUInt32_m3159865478(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetInt64_m4226135288(int64_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetUInt64_m3309170715(uint64_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetSingle_m4273073505(float* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetDouble_m1492351774(double* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetChar16_m3066416085(Il2CppChar* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetBoolean_m3854624799(bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetString_m2457071959(Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetGuid_m2989795186(Guid_t * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetDateTime_m4147484372(DateTime_t1679451545 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetTimeSpan_m3208245299(TimeSpan_t881159249 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetPoint_m2770216139(Point_t4164953539 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetSize_m1871300471(Size_t550917638 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetRect_m3170973200(Rect_t2695113487 * comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetUInt8Array_m1198332942(uint32_t* ___value0ArraySize, uint8_t** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetInt16Array_m99541304(uint32_t* ___value0ArraySize, int16_t** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetUInt16Array_m1856918684(uint32_t* ___value0ArraySize, uint16_t** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetInt32Array_m3011037886(uint32_t* ___value0ArraySize, int32_t** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetUInt32Array_m3306404924(uint32_t* ___value0ArraySize, uint32_t** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetInt64Array_m16819376(uint32_t* ___value0ArraySize, int64_t** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetUInt64Array_m966722754(uint32_t* ___value0ArraySize, uint64_t** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetSingleArray_m4042538522(uint32_t* ___value0ArraySize, float** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetDoubleArray_m2983062062(uint32_t* ___value0ArraySize, double** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetChar16Array_m3043815358(uint32_t* ___value0ArraySize, Il2CppChar** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetBooleanArray_m2519679727(uint32_t* ___value0ArraySize, bool** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetStringArray_m31925364(uint32_t* ___value0ArraySize, Il2CppHString** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetInspectableArray_m2438646989(uint32_t* ___value0ArraySize, Il2CppIInspectable*** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetGuidArray_m3262947336(uint32_t* ___value0ArraySize, Guid_t ** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetDateTimeArray_m2328955250(uint32_t* ___value0ArraySize, DateTime_t1679451545 ** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetTimeSpanArray_m3967818403(uint32_t* ___value0ArraySize, TimeSpan_t881159249 ** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetPointArray_m2823350989(uint32_t* ___value0ArraySize, Point_t4164953539 ** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetSizeArray_m1001766604(uint32_t* ___value0ArraySize, Size_t550917638 ** ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IPropertyValue_GetRectArray_m3897171591(uint32_t* ___value0ArraySize, Rect_t2695113487 ** ___value0) = 0;
};
#ifndef MULTICASTDELEGATE_T157516450_H
#define MULTICASTDELEGATE_T157516450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t157516450  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t157516450, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t157516450_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T157516450_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T472374824_H
#define U3CU3EC__DISPLAYCLASS1_0_T472374824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t472374824  : public RuntimeObject
{
public:
	// Windows.Storage.Streams.IBuffer System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0::dataBuffer
	RuntimeObject* ___dataBuffer_0;
	// System.IO.Stream System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0::stream
	Stream_t1273022909 * ___stream_1;
	// System.Int32 System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0::bytesRequested
	int32_t ___bytesRequested_2;
	// Windows.Storage.Streams.InputStreamOptions System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0::options
	uint32_t ___options_3;

public:
	inline static int32_t get_offset_of_dataBuffer_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t472374824, ___dataBuffer_0)); }
	inline RuntimeObject* get_dataBuffer_0() const { return ___dataBuffer_0; }
	inline RuntimeObject** get_address_of_dataBuffer_0() { return &___dataBuffer_0; }
	inline void set_dataBuffer_0(RuntimeObject* value)
	{
		___dataBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataBuffer_0), value);
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t472374824, ___stream_1)); }
	inline Stream_t1273022909 * get_stream_1() const { return ___stream_1; }
	inline Stream_t1273022909 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_t1273022909 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___stream_1), value);
	}

	inline static int32_t get_offset_of_bytesRequested_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t472374824, ___bytesRequested_2)); }
	inline int32_t get_bytesRequested_2() const { return ___bytesRequested_2; }
	inline int32_t* get_address_of_bytesRequested_2() { return &___bytesRequested_2; }
	inline void set_bytesRequested_2(int32_t value)
	{
		___bytesRequested_2 = value;
	}

	inline static int32_t get_offset_of_options_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t472374824, ___options_3)); }
	inline uint32_t get_options_3() const { return ___options_3; }
	inline uint32_t* get_address_of_options_3() { return &___options_3; }
	inline void set_options_3(uint32_t value)
	{
		___options_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T472374824_H
#ifndef STREAMFLUSHASYNCRESULT_T3395384147_H
#define STREAMFLUSHASYNCRESULT_T3395384147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamFlushAsyncResult
struct  StreamFlushAsyncResult_t3395384147  : public StreamOperationAsyncResult_t2370793485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMFLUSHASYNCRESULT_T3395384147_H
#ifndef U3CU3CREADASYNC_ABSTRACTSTREAMU3EB__0U3ED_T2037578343_H
#define U3CU3CREADASYNC_ABSTRACTSTREAMU3EB__0U3ED_T2037578343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0/<<ReadAsync_AbstractStream>b__0>d
struct  U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343 
{
public:
	// System.Int32 System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0/<<ReadAsync_AbstractStream>b__0>d::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<Windows.Storage.Streams.IBuffer> System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0/<<ReadAsync_AbstractStream>b__0>d::<>t__builder
	AsyncTaskMethodBuilder_1_t2862166739  ___U3CU3Et__builder_1;
	// System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0 System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0/<<ReadAsync_AbstractStream>b__0>d::<>4__this
	U3CU3Ec__DisplayClass1_0_t472374824 * ___U3CU3E4__this_2;
	// System.Threading.CancellationToken System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0/<<ReadAsync_AbstractStream>b__0>d::cancelToken
	CancellationToken_t784455623  ___cancelToken_3;
	// System.Byte[] System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0/<<ReadAsync_AbstractStream>b__0>d::<data>5__1
	ByteU5BU5D_t4116647657* ___U3CdataU3E5__1_4;
	// System.Int32 System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0/<<ReadAsync_AbstractStream>b__0>d::<offset>5__2
	int32_t ___U3CoffsetU3E5__2_5;
	// System.Int32 System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0/<<ReadAsync_AbstractStream>b__0>d::<bytesCompleted>5__3
	int32_t ___U3CbytesCompletedU3E5__3_6;
	// System.Int32 System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0/<<ReadAsync_AbstractStream>b__0>d::<bytesRead>5__4
	int32_t ___U3CbytesReadU3E5__4_7;
	// System.IProgress`1<System.UInt32> System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0/<<ReadAsync_AbstractStream>b__0>d::progressListener
	RuntimeObject* ___progressListener_8;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32> System.IO.StreamOperationsImplementation/<>c__DisplayClass1_0/<<ReadAsync_AbstractStream>b__0>d::<>u__1
	ConfiguredTaskAwaiter_t4273446738  ___U3CU3Eu__1_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t2862166739  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t2862166739 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t2862166739  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343, ___U3CU3E4__this_2)); }
	inline U3CU3Ec__DisplayClass1_0_t472374824 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline U3CU3Ec__DisplayClass1_0_t472374824 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(U3CU3Ec__DisplayClass1_0_t472374824 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_cancelToken_3() { return static_cast<int32_t>(offsetof(U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343, ___cancelToken_3)); }
	inline CancellationToken_t784455623  get_cancelToken_3() const { return ___cancelToken_3; }
	inline CancellationToken_t784455623 * get_address_of_cancelToken_3() { return &___cancelToken_3; }
	inline void set_cancelToken_3(CancellationToken_t784455623  value)
	{
		___cancelToken_3 = value;
	}

	inline static int32_t get_offset_of_U3CdataU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343, ___U3CdataU3E5__1_4)); }
	inline ByteU5BU5D_t4116647657* get_U3CdataU3E5__1_4() const { return ___U3CdataU3E5__1_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CdataU3E5__1_4() { return &___U3CdataU3E5__1_4; }
	inline void set_U3CdataU3E5__1_4(ByteU5BU5D_t4116647657* value)
	{
		___U3CdataU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3E5__1_4), value);
	}

	inline static int32_t get_offset_of_U3CoffsetU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343, ___U3CoffsetU3E5__2_5)); }
	inline int32_t get_U3CoffsetU3E5__2_5() const { return ___U3CoffsetU3E5__2_5; }
	inline int32_t* get_address_of_U3CoffsetU3E5__2_5() { return &___U3CoffsetU3E5__2_5; }
	inline void set_U3CoffsetU3E5__2_5(int32_t value)
	{
		___U3CoffsetU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CbytesCompletedU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343, ___U3CbytesCompletedU3E5__3_6)); }
	inline int32_t get_U3CbytesCompletedU3E5__3_6() const { return ___U3CbytesCompletedU3E5__3_6; }
	inline int32_t* get_address_of_U3CbytesCompletedU3E5__3_6() { return &___U3CbytesCompletedU3E5__3_6; }
	inline void set_U3CbytesCompletedU3E5__3_6(int32_t value)
	{
		___U3CbytesCompletedU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CbytesReadU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343, ___U3CbytesReadU3E5__4_7)); }
	inline int32_t get_U3CbytesReadU3E5__4_7() const { return ___U3CbytesReadU3E5__4_7; }
	inline int32_t* get_address_of_U3CbytesReadU3E5__4_7() { return &___U3CbytesReadU3E5__4_7; }
	inline void set_U3CbytesReadU3E5__4_7(int32_t value)
	{
		___U3CbytesReadU3E5__4_7 = value;
	}

	inline static int32_t get_offset_of_progressListener_8() { return static_cast<int32_t>(offsetof(U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343, ___progressListener_8)); }
	inline RuntimeObject* get_progressListener_8() const { return ___progressListener_8; }
	inline RuntimeObject** get_address_of_progressListener_8() { return &___progressListener_8; }
	inline void set_progressListener_8(RuntimeObject* value)
	{
		___progressListener_8 = value;
		Il2CppCodeGenWriteBarrier((&___progressListener_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_9() { return static_cast<int32_t>(offsetof(U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343, ___U3CU3Eu__1_9)); }
	inline ConfiguredTaskAwaiter_t4273446738  get_U3CU3Eu__1_9() const { return ___U3CU3Eu__1_9; }
	inline ConfiguredTaskAwaiter_t4273446738 * get_address_of_U3CU3Eu__1_9() { return &___U3CU3Eu__1_9; }
	inline void set_U3CU3Eu__1_9(ConfiguredTaskAwaiter_t4273446738  value)
	{
		___U3CU3Eu__1_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3CREADASYNC_ABSTRACTSTREAMU3EB__0U3ED_T2037578343_H
#ifndef U3CU3CWRITEASYNC_ABSTRACTSTREAMU3EB__0U3ED_T2737451479_H
#define U3CU3CWRITEASYNC_ABSTRACTSTREAMU3EB__0U3ED_T2737451479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__0>d
struct  U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479 
{
public:
	// System.Int32 System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__0>d::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.UInt32> System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__0>d::<>t__builder
	AsyncTaskMethodBuilder_1_t586069192  ___U3CU3Et__builder_1;
	// System.Threading.CancellationToken System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__0>d::cancelToken
	CancellationToken_t784455623  ___cancelToken_2;
	// System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0 System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__0>d::<>4__this
	U3CU3Ec__DisplayClass2_0_t875659351 * ___U3CU3E4__this_3;
	// System.IProgress`1<System.UInt32> System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__0>d::progressListener
	RuntimeObject* ___progressListener_4;
	// System.Int32 System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__0>d::<bytesToWrite>5__1
	int32_t ___U3CbytesToWriteU3E5__1_5;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__0>d::<>u__1
	ConfiguredTaskAwaiter_t555647845  ___U3CU3Eu__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t586069192  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t586069192 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t586069192  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_cancelToken_2() { return static_cast<int32_t>(offsetof(U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479, ___cancelToken_2)); }
	inline CancellationToken_t784455623  get_cancelToken_2() const { return ___cancelToken_2; }
	inline CancellationToken_t784455623 * get_address_of_cancelToken_2() { return &___cancelToken_2; }
	inline void set_cancelToken_2(CancellationToken_t784455623  value)
	{
		___cancelToken_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479, ___U3CU3E4__this_3)); }
	inline U3CU3Ec__DisplayClass2_0_t875659351 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline U3CU3Ec__DisplayClass2_0_t875659351 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(U3CU3Ec__DisplayClass2_0_t875659351 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_progressListener_4() { return static_cast<int32_t>(offsetof(U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479, ___progressListener_4)); }
	inline RuntimeObject* get_progressListener_4() const { return ___progressListener_4; }
	inline RuntimeObject** get_address_of_progressListener_4() { return &___progressListener_4; }
	inline void set_progressListener_4(RuntimeObject* value)
	{
		___progressListener_4 = value;
		Il2CppCodeGenWriteBarrier((&___progressListener_4), value);
	}

	inline static int32_t get_offset_of_U3CbytesToWriteU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479, ___U3CbytesToWriteU3E5__1_5)); }
	inline int32_t get_U3CbytesToWriteU3E5__1_5() const { return ___U3CbytesToWriteU3E5__1_5; }
	inline int32_t* get_address_of_U3CbytesToWriteU3E5__1_5() { return &___U3CbytesToWriteU3E5__1_5; }
	inline void set_U3CbytesToWriteU3E5__1_5(int32_t value)
	{
		___U3CbytesToWriteU3E5__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_6() { return static_cast<int32_t>(offsetof(U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479, ___U3CU3Eu__1_6)); }
	inline ConfiguredTaskAwaiter_t555647845  get_U3CU3Eu__1_6() const { return ___U3CU3Eu__1_6; }
	inline ConfiguredTaskAwaiter_t555647845 * get_address_of_U3CU3Eu__1_6() { return &___U3CU3Eu__1_6; }
	inline void set_U3CU3Eu__1_6(ConfiguredTaskAwaiter_t555647845  value)
	{
		___U3CU3Eu__1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3CWRITEASYNC_ABSTRACTSTREAMU3EB__0U3ED_T2737451479_H
#ifndef UNMANAGEDMEMORYSTREAM_T4234117669_H
#define UNMANAGEDMEMORYSTREAM_T4234117669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.UnmanagedMemoryStream
struct  UnmanagedMemoryStream_t4234117669  : public Stream_t1273022909
{
public:
	// System.Runtime.InteropServices.SafeBuffer System.IO.UnmanagedMemoryStream::_buffer
	SafeBuffer_t3564637124 * ____buffer_5;
	// System.Byte* System.IO.UnmanagedMemoryStream::_mem
	uint8_t* ____mem_6;
	// System.Int64 System.IO.UnmanagedMemoryStream::_length
	int64_t ____length_7;
	// System.Int64 System.IO.UnmanagedMemoryStream::_capacity
	int64_t ____capacity_8;
	// System.Int64 System.IO.UnmanagedMemoryStream::_position
	int64_t ____position_9;
	// System.Int64 System.IO.UnmanagedMemoryStream::_offset
	int64_t ____offset_10;
	// System.IO.FileAccess System.IO.UnmanagedMemoryStream::_access
	int32_t ____access_11;
	// System.Boolean System.IO.UnmanagedMemoryStream::_isOpen
	bool ____isOpen_12;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.UnmanagedMemoryStream::_lastReadTask
	Task_1_t61518632 * ____lastReadTask_13;

public:
	inline static int32_t get_offset_of__buffer_5() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t4234117669, ____buffer_5)); }
	inline SafeBuffer_t3564637124 * get__buffer_5() const { return ____buffer_5; }
	inline SafeBuffer_t3564637124 ** get_address_of__buffer_5() { return &____buffer_5; }
	inline void set__buffer_5(SafeBuffer_t3564637124 * value)
	{
		____buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_5), value);
	}

	inline static int32_t get_offset_of__mem_6() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t4234117669, ____mem_6)); }
	inline uint8_t* get__mem_6() const { return ____mem_6; }
	inline uint8_t** get_address_of__mem_6() { return &____mem_6; }
	inline void set__mem_6(uint8_t* value)
	{
		____mem_6 = value;
	}

	inline static int32_t get_offset_of__length_7() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t4234117669, ____length_7)); }
	inline int64_t get__length_7() const { return ____length_7; }
	inline int64_t* get_address_of__length_7() { return &____length_7; }
	inline void set__length_7(int64_t value)
	{
		____length_7 = value;
	}

	inline static int32_t get_offset_of__capacity_8() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t4234117669, ____capacity_8)); }
	inline int64_t get__capacity_8() const { return ____capacity_8; }
	inline int64_t* get_address_of__capacity_8() { return &____capacity_8; }
	inline void set__capacity_8(int64_t value)
	{
		____capacity_8 = value;
	}

	inline static int32_t get_offset_of__position_9() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t4234117669, ____position_9)); }
	inline int64_t get__position_9() const { return ____position_9; }
	inline int64_t* get_address_of__position_9() { return &____position_9; }
	inline void set__position_9(int64_t value)
	{
		____position_9 = value;
	}

	inline static int32_t get_offset_of__offset_10() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t4234117669, ____offset_10)); }
	inline int64_t get__offset_10() const { return ____offset_10; }
	inline int64_t* get_address_of__offset_10() { return &____offset_10; }
	inline void set__offset_10(int64_t value)
	{
		____offset_10 = value;
	}

	inline static int32_t get_offset_of__access_11() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t4234117669, ____access_11)); }
	inline int32_t get__access_11() const { return ____access_11; }
	inline int32_t* get_address_of__access_11() { return &____access_11; }
	inline void set__access_11(int32_t value)
	{
		____access_11 = value;
	}

	inline static int32_t get_offset_of__isOpen_12() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t4234117669, ____isOpen_12)); }
	inline bool get__isOpen_12() const { return ____isOpen_12; }
	inline bool* get_address_of__isOpen_12() { return &____isOpen_12; }
	inline void set__isOpen_12(bool value)
	{
		____isOpen_12 = value;
	}

	inline static int32_t get_offset_of__lastReadTask_13() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t4234117669, ____lastReadTask_13)); }
	inline Task_1_t61518632 * get__lastReadTask_13() const { return ____lastReadTask_13; }
	inline Task_1_t61518632 ** get_address_of__lastReadTask_13() { return &____lastReadTask_13; }
	inline void set__lastReadTask_13(Task_1_t61518632 * value)
	{
		____lastReadTask_13 = value;
		Il2CppCodeGenWriteBarrier((&____lastReadTask_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMANAGEDMEMORYSTREAM_T4234117669_H
#ifndef U3CREADASYNCINTERNALU3ED__43_T3130315676_H
#define U3CREADASYNCINTERNALU3ED__43_T3130315676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.WinRtToNetFxStreamAdapter/<ReadAsyncInternal>d__43
struct  U3CReadAsyncInternalU3Ed__43_t3130315676 
{
public:
	// System.Int32 System.IO.WinRtToNetFxStreamAdapter/<ReadAsyncInternal>d__43::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Int32> System.IO.WinRtToNetFxStreamAdapter/<ReadAsyncInternal>d__43::<>t__builder
	AsyncTaskMethodBuilder_1_t976952967  ___U3CU3Et__builder_1;
	// System.IO.WinRtToNetFxStreamAdapter System.IO.WinRtToNetFxStreamAdapter/<ReadAsyncInternal>d__43::<>4__this
	WinRtToNetFxStreamAdapter_t1366777568 * ___U3CU3E4__this_2;
	// System.Byte[] System.IO.WinRtToNetFxStreamAdapter/<ReadAsyncInternal>d__43::buffer
	ByteU5BU5D_t4116647657* ___buffer_3;
	// System.Int32 System.IO.WinRtToNetFxStreamAdapter/<ReadAsyncInternal>d__43::offset
	int32_t ___offset_4;
	// System.Int32 System.IO.WinRtToNetFxStreamAdapter/<ReadAsyncInternal>d__43::count
	int32_t ___count_5;
	// System.Threading.CancellationToken System.IO.WinRtToNetFxStreamAdapter/<ReadAsyncInternal>d__43::cancellationToken
	CancellationToken_t784455623  ___cancellationToken_6;
	// Windows.Storage.Streams.IBuffer System.IO.WinRtToNetFxStreamAdapter/<ReadAsyncInternal>d__43::<userBuffer>5__1
	RuntimeObject* ___U3CuserBufferU3E5__1_7;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<Windows.Storage.Streams.IBuffer> System.IO.WinRtToNetFxStreamAdapter/<ReadAsyncInternal>d__43::<>u__1
	ConfiguredTaskAwaiter_t1863693214  ___U3CU3Eu__1_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CReadAsyncInternalU3Ed__43_t3130315676, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CReadAsyncInternalU3Ed__43_t3130315676, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t976952967  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t976952967 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t976952967  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CReadAsyncInternalU3Ed__43_t3130315676, ___U3CU3E4__this_2)); }
	inline WinRtToNetFxStreamAdapter_t1366777568 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WinRtToNetFxStreamAdapter_t1366777568 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WinRtToNetFxStreamAdapter_t1366777568 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_buffer_3() { return static_cast<int32_t>(offsetof(U3CReadAsyncInternalU3Ed__43_t3130315676, ___buffer_3)); }
	inline ByteU5BU5D_t4116647657* get_buffer_3() const { return ___buffer_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_3() { return &___buffer_3; }
	inline void set_buffer_3(ByteU5BU5D_t4116647657* value)
	{
		___buffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_3), value);
	}

	inline static int32_t get_offset_of_offset_4() { return static_cast<int32_t>(offsetof(U3CReadAsyncInternalU3Ed__43_t3130315676, ___offset_4)); }
	inline int32_t get_offset_4() const { return ___offset_4; }
	inline int32_t* get_address_of_offset_4() { return &___offset_4; }
	inline void set_offset_4(int32_t value)
	{
		___offset_4 = value;
	}

	inline static int32_t get_offset_of_count_5() { return static_cast<int32_t>(offsetof(U3CReadAsyncInternalU3Ed__43_t3130315676, ___count_5)); }
	inline int32_t get_count_5() const { return ___count_5; }
	inline int32_t* get_address_of_count_5() { return &___count_5; }
	inline void set_count_5(int32_t value)
	{
		___count_5 = value;
	}

	inline static int32_t get_offset_of_cancellationToken_6() { return static_cast<int32_t>(offsetof(U3CReadAsyncInternalU3Ed__43_t3130315676, ___cancellationToken_6)); }
	inline CancellationToken_t784455623  get_cancellationToken_6() const { return ___cancellationToken_6; }
	inline CancellationToken_t784455623 * get_address_of_cancellationToken_6() { return &___cancellationToken_6; }
	inline void set_cancellationToken_6(CancellationToken_t784455623  value)
	{
		___cancellationToken_6 = value;
	}

	inline static int32_t get_offset_of_U3CuserBufferU3E5__1_7() { return static_cast<int32_t>(offsetof(U3CReadAsyncInternalU3Ed__43_t3130315676, ___U3CuserBufferU3E5__1_7)); }
	inline RuntimeObject* get_U3CuserBufferU3E5__1_7() const { return ___U3CuserBufferU3E5__1_7; }
	inline RuntimeObject** get_address_of_U3CuserBufferU3E5__1_7() { return &___U3CuserBufferU3E5__1_7; }
	inline void set_U3CuserBufferU3E5__1_7(RuntimeObject* value)
	{
		___U3CuserBufferU3E5__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuserBufferU3E5__1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_8() { return static_cast<int32_t>(offsetof(U3CReadAsyncInternalU3Ed__43_t3130315676, ___U3CU3Eu__1_8)); }
	inline ConfiguredTaskAwaiter_t1863693214  get_U3CU3Eu__1_8() const { return ___U3CU3Eu__1_8; }
	inline ConfiguredTaskAwaiter_t1863693214 * get_address_of_U3CU3Eu__1_8() { return &___U3CU3Eu__1_8; }
	inline void set_U3CU3Eu__1_8(ConfiguredTaskAwaiter_t1863693214  value)
	{
		___U3CU3Eu__1_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADASYNCINTERNALU3ED__43_T3130315676_H
// Windows.Foundation.IAsyncInfo
struct NOVTABLE IAsyncInfo_t2425795444 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IAsyncInfo_get_Id_m488151110(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IAsyncInfo_get_Status_m774826579(int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IAsyncInfo_get_ErrorCode_m1014534200(int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IAsyncInfo_Cancel_m959568370() = 0;
	virtual il2cpp_hresult_t STDCALL IAsyncInfo_Close_m1968429586() = 0;
};
#ifndef STREAMREADASYNCRESULT_T2363479406_H
#define STREAMREADASYNCRESULT_T2363479406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamReadAsyncResult
struct  StreamReadAsyncResult_t2363479406  : public StreamOperationAsyncResult_t2370793485
{
public:
	// Windows.Storage.Streams.IBuffer System.IO.StreamReadAsyncResult::_userBuffer
	RuntimeObject* ____userBuffer_10;

public:
	inline static int32_t get_offset_of__userBuffer_10() { return static_cast<int32_t>(offsetof(StreamReadAsyncResult_t2363479406, ____userBuffer_10)); }
	inline RuntimeObject* get__userBuffer_10() const { return ____userBuffer_10; }
	inline RuntimeObject** get_address_of__userBuffer_10() { return &____userBuffer_10; }
	inline void set__userBuffer_10(RuntimeObject* value)
	{
		____userBuffer_10 = value;
		Il2CppCodeGenWriteBarrier((&____userBuffer_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMREADASYNCRESULT_T2363479406_H
#ifndef U3CU3CWRITEASYNC_ABSTRACTSTREAMU3EB__1U3ED_T2737385943_H
#define U3CU3CWRITEASYNC_ABSTRACTSTREAMU3EB__1U3ED_T2737385943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__1>d
struct  U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943 
{
public:
	// System.Int32 System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__1>d::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.UInt32> System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__1>d::<>t__builder
	AsyncTaskMethodBuilder_1_t586069192  ___U3CU3Et__builder_1;
	// System.Threading.CancellationToken System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__1>d::cancelToken
	CancellationToken_t784455623  ___cancelToken_2;
	// System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0 System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__1>d::<>4__this
	U3CU3Ec__DisplayClass2_0_t875659351 * ___U3CU3E4__this_3;
	// System.IProgress`1<System.UInt32> System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__1>d::progressListener
	RuntimeObject* ___progressListener_4;
	// System.UInt32 System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__1>d::<bytesToWrite>5__1
	uint32_t ___U3CbytesToWriteU3E5__1_5;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter System.IO.StreamOperationsImplementation/<>c__DisplayClass2_0/<<WriteAsync_AbstractStream>b__1>d::<>u__1
	ConfiguredTaskAwaiter_t555647845  ___U3CU3Eu__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t586069192  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t586069192 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t586069192  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_cancelToken_2() { return static_cast<int32_t>(offsetof(U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943, ___cancelToken_2)); }
	inline CancellationToken_t784455623  get_cancelToken_2() const { return ___cancelToken_2; }
	inline CancellationToken_t784455623 * get_address_of_cancelToken_2() { return &___cancelToken_2; }
	inline void set_cancelToken_2(CancellationToken_t784455623  value)
	{
		___cancelToken_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943, ___U3CU3E4__this_3)); }
	inline U3CU3Ec__DisplayClass2_0_t875659351 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline U3CU3Ec__DisplayClass2_0_t875659351 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(U3CU3Ec__DisplayClass2_0_t875659351 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_progressListener_4() { return static_cast<int32_t>(offsetof(U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943, ___progressListener_4)); }
	inline RuntimeObject* get_progressListener_4() const { return ___progressListener_4; }
	inline RuntimeObject** get_address_of_progressListener_4() { return &___progressListener_4; }
	inline void set_progressListener_4(RuntimeObject* value)
	{
		___progressListener_4 = value;
		Il2CppCodeGenWriteBarrier((&___progressListener_4), value);
	}

	inline static int32_t get_offset_of_U3CbytesToWriteU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943, ___U3CbytesToWriteU3E5__1_5)); }
	inline uint32_t get_U3CbytesToWriteU3E5__1_5() const { return ___U3CbytesToWriteU3E5__1_5; }
	inline uint32_t* get_address_of_U3CbytesToWriteU3E5__1_5() { return &___U3CbytesToWriteU3E5__1_5; }
	inline void set_U3CbytesToWriteU3E5__1_5(uint32_t value)
	{
		___U3CbytesToWriteU3E5__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_6() { return static_cast<int32_t>(offsetof(U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943, ___U3CU3Eu__1_6)); }
	inline ConfiguredTaskAwaiter_t555647845  get_U3CU3Eu__1_6() const { return ___U3CU3Eu__1_6; }
	inline ConfiguredTaskAwaiter_t555647845 * get_address_of_U3CU3Eu__1_6() { return &___U3CU3Eu__1_6; }
	inline void set_U3CU3Eu__1_6(ConfiguredTaskAwaiter_t555647845  value)
	{
		___U3CU3Eu__1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3CWRITEASYNC_ABSTRACTSTREAMU3EB__1U3ED_T2737385943_H
#ifndef STREAMWRITEASYNCRESULT_T1545417764_H
#define STREAMWRITEASYNCRESULT_T1545417764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamWriteAsyncResult
struct  StreamWriteAsyncResult_t1545417764  : public StreamOperationAsyncResult_t2370793485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMWRITEASYNCRESULT_T1545417764_H
#ifndef ASYNCACTIONCOMPLETEDHANDLER_T4142409509_H
#define ASYNCACTIONCOMPLETEDHANDLER_T4142409509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Windows.Foundation.AsyncActionCompletedHandler
struct  AsyncActionCompletedHandler_t4142409509  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// COM Callable Wrapper interface definition for Windows.Foundation.AsyncActionCompletedHandler
struct IAsyncActionCompletedHandler_t4142409509_ComCallableWrapper : Il2CppIUnknown
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL Invoke(IAsyncAction_t3072713919* ___asyncInfo0, int32_t ___asyncStatus1) = 0;
};

#endif // ASYNCACTIONCOMPLETEDHANDLER_T4142409509_H
#ifndef WINDOWSRUNTIMEBUFFERUNMANAGEDMEMORYSTREAM_T4037845366_H
#define WINDOWSRUNTIMEBUFFERUNMANAGEDMEMORYSTREAM_T4037845366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeBufferExtensions/WindowsRuntimeBufferUnmanagedMemoryStream
struct  WindowsRuntimeBufferUnmanagedMemoryStream_t4037845366  : public UnmanagedMemoryStream_t4234117669
{
public:
	// Windows.Storage.Streams.IBuffer System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeBufferExtensions/WindowsRuntimeBufferUnmanagedMemoryStream::_sourceBuffer
	RuntimeObject* ____sourceBuffer_14;

public:
	inline static int32_t get_offset_of__sourceBuffer_14() { return static_cast<int32_t>(offsetof(WindowsRuntimeBufferUnmanagedMemoryStream_t4037845366, ____sourceBuffer_14)); }
	inline RuntimeObject* get__sourceBuffer_14() const { return ____sourceBuffer_14; }
	inline RuntimeObject** get_address_of__sourceBuffer_14() { return &____sourceBuffer_14; }
	inline void set__sourceBuffer_14(RuntimeObject* value)
	{
		____sourceBuffer_14 = value;
		Il2CppCodeGenWriteBarrier((&____sourceBuffer_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWSRUNTIMEBUFFERUNMANAGEDMEMORYSTREAM_T4037845366_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4400 = { sizeof (Size_t550917638)+ sizeof (RuntimeObject), sizeof(Size_t550917638 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4400[2] = 
{
	Size_t550917638::get_offset_of__width_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Size_t550917638::get_offset_of__height_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4401 = { sizeof (TokenizerHelper_t3391915057), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4402 = { sizeof (VoidValueTypeParameter_t130952355)+ sizeof (RuntimeObject), sizeof(VoidValueTypeParameter_t130952355 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4403 = { sizeof (VoidReferenceTypeParameter_t1476249554), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4404 = { sizeof (WindowsRuntimeSystemExtensions_t757232145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4405 = { sizeof (U3CU3Ec_t2427350286), -1, sizeof(U3CU3Ec_t2427350286_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4405[3] = 
{
	U3CU3Ec_t2427350286_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2427350286_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
	U3CU3Ec_t2427350286_StaticFields::get_offset_of_U3CU3E9__0_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4406 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4406[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4407 = { sizeof (AsyncInfoIdGenerator_t957218259), -1, sizeof(AsyncInfoIdGenerator_t957218259_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4407[1] = 
{
	AsyncInfoIdGenerator_t957218259_StaticFields::get_offset_of_s_idGenerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4408 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4408[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4409 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4409[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4410 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4410[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4411 = { sizeof (ExceptionDispatchHelper_t757867092), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4412 = { sizeof (U3CU3Ec_t3914568053), -1, sizeof(U3CU3Ec_t3914568053_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4412[3] = 
{
	U3CU3Ec_t3914568053_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3914568053_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
	U3CU3Ec_t3914568053_StaticFields::get_offset_of_U3CU3E9__0_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4413 = { sizeof (TaskToAsyncActionAdapter_t900468660), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4414 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4414[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4415 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4415[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4416 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4417 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4418 = { 0, sizeof(IAgileObject_t981452505*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4419 = { 0, sizeof(IMarshal_t3913494478*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4420 = { sizeof (AsyncInfo_t1262122865), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4421 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4421[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4422 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4422[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4423 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4423[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4424 = { 0, sizeof(IBufferByteAccess_t4245463285*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4425 = { sizeof (WindowsRuntimeBuffer_t3806548453), -1, 0, sizeof(WindowsRuntimeBuffer_t3806548453_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable4425[7] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	WindowsRuntimeBuffer_t3806548453::get_offset_of__data_1(),
	WindowsRuntimeBuffer_t3806548453::get_offset_of__dataStartOffs_2(),
	WindowsRuntimeBuffer_t3806548453::get_offset_of__usefulDataLength_3(),
	WindowsRuntimeBuffer_t3806548453::get_offset_of__maxDataCapacity_4(),
	WindowsRuntimeBuffer_t3806548453::get_offset_of__pinHandle_5(),
	WindowsRuntimeBuffer_t3806548453::get_offset_of__dataPtr_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4426 = { sizeof (WindowsRuntimeBufferExtensions_t1627451895), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4427 = { sizeof (WindowsRuntimeBufferUnmanagedMemoryStream_t4037845366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4427[1] = 
{
	WindowsRuntimeBufferUnmanagedMemoryStream_t4037845366::get_offset_of__sourceBuffer_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4428 = { sizeof (NetFxToWinRtStreamAdapter_t2688706982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4428[3] = 
{
	NetFxToWinRtStreamAdapter_t2688706982::get_offset_of__managedStream_0(),
	NetFxToWinRtStreamAdapter_t2688706982::get_offset_of__leaveUnderlyingStreamOpen_1(),
	NetFxToWinRtStreamAdapter_t2688706982::get_offset_of__readOptimization_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4429 = { sizeof (StreamReadOperationOptimization_t12377751)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4429[3] = 
{
	StreamReadOperationOptimization_t12377751::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4430 = { sizeof (StreamOperationAsyncResult_t2370793485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4430[10] = 
{
	StreamOperationAsyncResult_t2370793485::get_offset_of__userCompletionCallback_0(),
	StreamOperationAsyncResult_t2370793485::get_offset_of__userAsyncStateInfo_1(),
	StreamOperationAsyncResult_t2370793485::get_offset_of__asyncStreamOperation_2(),
	StreamOperationAsyncResult_t2370793485::get_offset_of__completed_3(),
	StreamOperationAsyncResult_t2370793485::get_offset_of__callbackInvoked_4(),
	StreamOperationAsyncResult_t2370793485::get_offset_of__waitHandle_5(),
	StreamOperationAsyncResult_t2370793485::get_offset_of__bytesCompleted_6(),
	StreamOperationAsyncResult_t2370793485::get_offset_of__errorInfo_7(),
	StreamOperationAsyncResult_t2370793485::get_offset_of__processCompletedOperationInCallback_8(),
	StreamOperationAsyncResult_t2370793485::get_offset_of__completedOperation_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4431 = { sizeof (StreamReadAsyncResult_t2363479406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4431[1] = 
{
	StreamReadAsyncResult_t2363479406::get_offset_of__userBuffer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4432 = { sizeof (StreamWriteAsyncResult_t1545417764), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4433 = { sizeof (StreamFlushAsyncResult_t3395384147), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4434 = { sizeof (StreamOperationsImplementation_t1346754833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4435 = { sizeof (U3CU3Ec__DisplayClass1_0_t472374824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4435[4] = 
{
	U3CU3Ec__DisplayClass1_0_t472374824::get_offset_of_dataBuffer_0(),
	U3CU3Ec__DisplayClass1_0_t472374824::get_offset_of_stream_1(),
	U3CU3Ec__DisplayClass1_0_t472374824::get_offset_of_bytesRequested_2(),
	U3CU3Ec__DisplayClass1_0_t472374824::get_offset_of_options_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4436 = { sizeof (U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4436[10] = 
{
	U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343::get_offset_of_cancelToken_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343::get_offset_of_U3CdataU3E5__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343::get_offset_of_U3CoffsetU3E5__2_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343::get_offset_of_U3CbytesCompletedU3E5__3_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343::get_offset_of_U3CbytesReadU3E5__4_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343::get_offset_of_progressListener_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CReadAsync_AbstractStreamU3Eb__0U3Ed_t2037578343::get_offset_of_U3CU3Eu__1_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4437 = { sizeof (U3CU3Ec__DisplayClass2_0_t875659351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4437[4] = 
{
	U3CU3Ec__DisplayClass2_0_t875659351::get_offset_of_buffer_0(),
	U3CU3Ec__DisplayClass2_0_t875659351::get_offset_of_stream_1(),
	U3CU3Ec__DisplayClass2_0_t875659351::get_offset_of_data_2(),
	U3CU3Ec__DisplayClass2_0_t875659351::get_offset_of_offset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4438 = { sizeof (U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4438[7] = 
{
	U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479::get_offset_of_cancelToken_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479::get_offset_of_U3CU3E4__this_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479::get_offset_of_progressListener_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479::get_offset_of_U3CbytesToWriteU3E5__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CWriteAsync_AbstractStreamU3Eb__0U3Ed_t2737451479::get_offset_of_U3CU3Eu__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4439 = { sizeof (U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4439[7] = 
{
	U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943::get_offset_of_cancelToken_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943::get_offset_of_U3CU3E4__this_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943::get_offset_of_progressListener_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943::get_offset_of_U3CbytesToWriteU3E5__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CWriteAsync_AbstractStreamU3Eb__1U3Ed_t2737385943::get_offset_of_U3CU3Eu__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4440 = { sizeof (U3CU3Ec__DisplayClass3_0_t3604542706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4440[1] = 
{
	U3CU3Ec__DisplayClass3_0_t3604542706::get_offset_of_stream_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4441 = { sizeof (U3CU3CFlushAsync_AbstractStreamU3Eb__0U3Ed_t1213545130)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4441[5] = 
{
	U3CU3CFlushAsync_AbstractStreamU3Eb__0U3Ed_t1213545130::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CFlushAsync_AbstractStreamU3Eb__0U3Ed_t1213545130::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CFlushAsync_AbstractStreamU3Eb__0U3Ed_t1213545130::get_offset_of_cancelToken_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CFlushAsync_AbstractStreamU3Eb__0U3Ed_t1213545130::get_offset_of_U3CU3E4__this_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CU3CFlushAsync_AbstractStreamU3Eb__0U3Ed_t1213545130::get_offset_of_U3CU3Eu__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4442 = { sizeof (WindowsRuntimeStreamExtensions_t4179015579), -1, sizeof(WindowsRuntimeStreamExtensions_t4179015579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4442[2] = 
{
	WindowsRuntimeStreamExtensions_t4179015579_StaticFields::get_offset_of_s_winRtToNetFxAdapterMap_0(),
	WindowsRuntimeStreamExtensions_t4179015579_StaticFields::get_offset_of_s_netFxToWinRtAdapterMap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4443 = { sizeof (U3CU3Ec_t2303284929), -1, sizeof(U3CU3Ec_t2303284929_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4443[2] = 
{
	U3CU3Ec_t2303284929_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2303284929_StaticFields::get_offset_of_U3CU3E9__11_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4444 = { sizeof (U3CU3Ec__DisplayClass12_0_t3069626030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4444[1] = 
{
	U3CU3Ec__DisplayClass12_0_t3069626030::get_offset_of_bufferSize_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4445 = { sizeof (WinRtIOHelper_t4133879084), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4446 = { sizeof (WinRtToNetFxStreamAdapter_t1366777568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4446[6] = 
{
	WinRtToNetFxStreamAdapter_t1366777568::get_offset_of__oneByteBuffer_5(),
	WinRtToNetFxStreamAdapter_t1366777568::get_offset_of__leaveUnderlyingStreamOpen_6(),
	WinRtToNetFxStreamAdapter_t1366777568::get_offset_of__winRtStream_7(),
	WinRtToNetFxStreamAdapter_t1366777568::get_offset_of__canRead_8(),
	WinRtToNetFxStreamAdapter_t1366777568::get_offset_of__canWrite_9(),
	WinRtToNetFxStreamAdapter_t1366777568::get_offset_of__canSeek_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4447 = { sizeof (U3CReadAsyncInternalU3Ed__43_t3130315676)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4447[9] = 
{
	U3CReadAsyncInternalU3Ed__43_t3130315676::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CReadAsyncInternalU3Ed__43_t3130315676::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CReadAsyncInternalU3Ed__43_t3130315676::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CReadAsyncInternalU3Ed__43_t3130315676::get_offset_of_buffer_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CReadAsyncInternalU3Ed__43_t3130315676::get_offset_of_offset_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CReadAsyncInternalU3Ed__43_t3130315676::get_offset_of_count_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CReadAsyncInternalU3Ed__43_t3130315676::get_offset_of_cancellationToken_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CReadAsyncInternalU3Ed__43_t3130315676::get_offset_of_U3CuserBufferU3E5__1_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CReadAsyncInternalU3Ed__43_t3130315676::get_offset_of_U3CU3Eu__1_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4448 = { sizeof (U3CModuleU3E_t692745579), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4449 = { sizeof (U3CModuleU3E_t692745580), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4450 = { 0, sizeof(IClosable_t326290202*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4451 = { sizeof (WebHostHiddenAttribute_t2021537596), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4452 = { sizeof (DualApiPartitionAttribute_t1301304994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4452[1] = 
{
	DualApiPartitionAttribute_t1301304994::get_offset_of_version_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4453 = { sizeof (MuseAttribute_t3401286167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4453[1] = 
{
	MuseAttribute_t3401286167::get_offset_of_Version_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4454 = { sizeof (PropertyType_t3031428354)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4454[42] = 
{
	PropertyType_t3031428354::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4455 = { sizeof (Point_t4164953540)+ sizeof (RuntimeObject), sizeof(Point_t4164953540 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4455[2] = 
{
	Point_t4164953540::get_offset_of_X_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Point_t4164953540::get_offset_of_Y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4456 = { sizeof (Size_t550917639)+ sizeof (RuntimeObject), sizeof(Size_t550917639 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4456[2] = 
{
	Size_t550917639::get_offset_of_Width_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Size_t550917639::get_offset_of_Height_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4457 = { sizeof (Rect_t2695113488)+ sizeof (RuntimeObject), sizeof(Rect_t2695113488 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4457[4] = 
{
	Rect_t2695113488::get_offset_of_X_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Rect_t2695113488::get_offset_of_Y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Rect_t2695113488::get_offset_of_Width_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Rect_t2695113488::get_offset_of_Height_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4458 = { sizeof (DateTime_t1679451545)+ sizeof (RuntimeObject), sizeof(DateTime_t1679451545 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4458[1] = 
{
	DateTime_t1679451545::get_offset_of_UniversalTime_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4459 = { sizeof (TimeSpan_t1755640982)+ sizeof (RuntimeObject), sizeof(TimeSpan_t1755640982 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4459[1] = 
{
	TimeSpan_t1755640982::get_offset_of_Duration_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4460 = { 0, sizeof(IPropertyValue_t3169763779*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4461 = { 0, sizeof(IStringable_t1634798504*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4462 = { sizeof (AsyncActionCompletedHandler_t4142409509), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4463 = { 0, sizeof(IApiInformationStatics_t1368946868*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4464 = { sizeof (ApiInformation_t812843232), -1, sizeof(ApiInformation_t812843232_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4465 = { sizeof (ActivatableAttribute_t4098936231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4466 = { sizeof (ContractVersionAttribute_t1666784187), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4467 = { sizeof (AllowMultipleAttribute_t3314144467), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4468 = { sizeof (AttributeUsageAttribute_t1422441135), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4469 = { sizeof (AttributeTargets_t2460853228)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4469[14] = 
{
	AttributeTargets_t2460853228::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4470 = { sizeof (DefaultOverloadAttribute_t2513707992), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4471 = { sizeof (DefaultAttribute_t1509828399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4472 = { sizeof (GuidAttribute_t2682955351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4473 = { sizeof (ComposableAttribute_t2299833422), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4474 = { sizeof (CompositionType_t2620517455)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4474[3] = 
{
	CompositionType_t2620517455::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4475 = { sizeof (OverloadAttribute_t2178303703), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4476 = { sizeof (ApiContractAttribute_t3160257459), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4477 = { sizeof (StaticAttribute_t2174868556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4478 = { sizeof (OverridableAttribute_t203117044), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4479 = { sizeof (ProtectedAttribute_t3736820420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4480 = { sizeof (ThreadingAttribute_t685453408), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4481 = { sizeof (ThreadingModel_t2838021473)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4481[5] = 
{
	ThreadingModel_t2838021473::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4482 = { sizeof (MarshalingBehaviorAttribute_t570737848), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4483 = { sizeof (MarshalingType_t3556196393)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4483[5] = 
{
	MarshalingType_t3556196393::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4484 = { sizeof (DeprecatedAttribute_t3646164936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4485 = { sizeof (DeprecationType_t4182238482)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4485[3] = 
{
	DeprecationType_t4182238482::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4486 = { sizeof (ExclusiveToAttribute_t3124418018), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4487 = { sizeof (LengthIsAttribute_t613149204), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4488 = { sizeof (AsyncStatus_t2150485874)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4488[5] = 
{
	AsyncStatus_t2150485874::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4489 = { sizeof (EventRegistrationToken_t3152277946)+ sizeof (RuntimeObject), sizeof(EventRegistrationToken_t3152277946 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4489[1] = 
{
	EventRegistrationToken_t3152277946::get_offset_of_Value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4490 = { sizeof (HResult_t3073183193)+ sizeof (RuntimeObject), sizeof(HResult_t3073183193 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4490[1] = 
{
	HResult_t3073183193::get_offset_of_Value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4491 = { sizeof (RemoteAsyncAttribute_t33115854), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4492 = { 0, sizeof(IAsyncInfo_t2425795444*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4493 = { 0, sizeof(IAsyncAction_t3072713919*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4494 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4495 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4496 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4497 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4498 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4499 = { 0, 0, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
