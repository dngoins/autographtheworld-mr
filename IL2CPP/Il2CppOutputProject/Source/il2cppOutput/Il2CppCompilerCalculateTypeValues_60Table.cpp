﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// HoloToolkit.Unity.Boundary.SceneContentAdjuster
struct SceneContentAdjuster_t1258964661;
// System.Comparison`1<HoloToolkit.Unity.Design.IDistorter>
struct Comparison_1_t2691982459;
// HoloToolkit.Unity.Design.LineUnity
struct LineUnity_t4238357500;
// HoloToolkit.Unity.ControllerExamples.Eraser
struct Eraser_t1049249012;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// HoloToolkit.Unity.ControllerExamples.ObjectSpawner
struct ObjectSpawner_t3925816580;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;
// UnityEngine.Collider
struct Collider_t1773347010;
// HoloToolkit.Unity.ControllerExamples.BrushController
struct BrushController_t3766631826;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// HoloToolkit.Unity.ControllerExamples.Brush
struct Brush_t1548626474;
// HoloToolkit.Unity.InputModule.MotionControllerVisualizer
struct MotionControllerVisualizer_t392243420;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// Windows.Foundation.IAsyncOperation`1<Windows.Storage.Streams.IRandomAccessStreamWithContentType>
struct IAsyncOperation_1_t1918115372;
// Windows.Storage.Streams.IRandomAccessStreamWithContentType
struct IRandomAccessStreamWithContentType_t3296927652;
// UnityGLTF.GLTFComponent
struct GLTFComponent_t238219215;
// Windows.Storage.Streams.DataReader
struct DataReader_t2381813649;
// Windows.Storage.Streams.DataReaderLoadOperation
struct DataReaderLoadOperation_t4248924386;
// UnityEngine.Transform
struct Transform_t3600365921;
// HoloToolkit.Unity.Controllers.PhysicsPointer
struct PhysicsPointer_t2887364969;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t3210418286;
// UnityEngine.Gradient
struct Gradient_t3067099924;
// HoloToolkit.Unity.InputModule.MotionControllerInfo
struct MotionControllerInfo_t938152223;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// HoloToolkit.Unity.ControllerExamples.ColorPickerWheel
struct ColorPickerWheel_t2235220782;
// HoloToolkit.Unity.Design.LineBase
struct LineBase_t717918686;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// HoloToolkit.Unity.Design.IDistorter[]
struct IDistorterU5BU5D_t4037927985;
// HoloToolkit.Unity.Boundary.BoundaryManager
struct BoundaryManager_t2608055002;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// UnityEngine.Material
struct Material_t340375123;
// System.Collections.Generic.Dictionary`2<System.String,HoloToolkit.Unity.InputModule.MotionControllerInfo>
struct Dictionary_2_t723408522;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Action`1<HoloToolkit.Unity.InputModule.MotionControllerInfo>
struct Action_1_t1110619818;
// System.Collections.Generic.Queue`1<UnityEngine.LineRenderer>
struct Queue_1_t3000609764;
// HoloToolkit.Unity.Design.Line
struct Line_t549989228;
// HoloToolkit.Unity.Design.LineRenderer[]
struct LineRendererU5BU5D_t2548926732;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Animator
struct Animator_t434523843;
// HoloToolkit.Unity.Design.LineObjectCollection
struct LineObjectCollection_t2817952665;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.Mesh[]
struct MeshU5BU5D_t3972987605;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CSETCONTENTHEIGHTU3ED__8_T405582793_H
#define U3CSETCONTENTHEIGHTU3ED__8_T405582793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Boundary.SceneContentAdjuster/<SetContentHeight>d__8
struct  U3CSetContentHeightU3Ed__8_t405582793  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.Boundary.SceneContentAdjuster/<SetContentHeight>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.Boundary.SceneContentAdjuster/<SetContentHeight>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.Boundary.SceneContentAdjuster HoloToolkit.Unity.Boundary.SceneContentAdjuster/<SetContentHeight>d__8::<>4__this
	SceneContentAdjuster_t1258964661 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSetContentHeightU3Ed__8_t405582793, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSetContentHeightU3Ed__8_t405582793, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSetContentHeightU3Ed__8_t405582793, ___U3CU3E4__this_2)); }
	inline SceneContentAdjuster_t1258964661 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SceneContentAdjuster_t1258964661 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SceneContentAdjuster_t1258964661 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETCONTENTHEIGHTU3ED__8_T405582793_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CU3EC_T3579773046_H
#define U3CU3EC_T3579773046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.LineBase/<>c
struct  U3CU3Ec_t3579773046  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3579773046_StaticFields
{
public:
	// HoloToolkit.Unity.Design.LineBase/<>c HoloToolkit.Unity.Design.LineBase/<>c::<>9
	U3CU3Ec_t3579773046 * ___U3CU3E9_0;
	// System.Comparison`1<HoloToolkit.Unity.Design.IDistorter> HoloToolkit.Unity.Design.LineBase/<>c::<>9__38_0
	Comparison_1_t2691982459 * ___U3CU3E9__38_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3579773046_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3579773046 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3579773046 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3579773046 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__38_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3579773046_StaticFields, ___U3CU3E9__38_0_1)); }
	inline Comparison_1_t2691982459 * get_U3CU3E9__38_0_1() const { return ___U3CU3E9__38_0_1; }
	inline Comparison_1_t2691982459 ** get_address_of_U3CU3E9__38_0_1() { return &___U3CU3E9__38_0_1; }
	inline void set_U3CU3E9__38_0_1(Comparison_1_t2691982459 * value)
	{
		___U3CU3E9__38_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__38_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3579773046_H
#ifndef U3CUPDATELINEUNITYU3ED__8_T3720518906_H
#define U3CUPDATELINEUNITYU3ED__8_T3720518906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.LineUnity/<UpdateLineUnity>d__8
struct  U3CUpdateLineUnityU3Ed__8_t3720518906  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.Design.LineUnity/<UpdateLineUnity>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.Design.LineUnity/<UpdateLineUnity>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.Design.LineUnity HoloToolkit.Unity.Design.LineUnity/<UpdateLineUnity>d__8::<>4__this
	LineUnity_t4238357500 * ___U3CU3E4__this_2;
	// System.Int32 HoloToolkit.Unity.Design.LineUnity/<UpdateLineUnity>d__8::<i>5__1
	int32_t ___U3CiU3E5__1_3;
	// System.Int32 HoloToolkit.Unity.Design.LineUnity/<UpdateLineUnity>d__8::<i>5__2
	int32_t ___U3CiU3E5__2_4;
	// System.Single HoloToolkit.Unity.Design.LineUnity/<UpdateLineUnity>d__8::<normalizedDistance>5__3
	float ___U3CnormalizedDistanceU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateLineUnityU3Ed__8_t3720518906, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateLineUnityU3Ed__8_t3720518906, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateLineUnityU3Ed__8_t3720518906, ___U3CU3E4__this_2)); }
	inline LineUnity_t4238357500 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LineUnity_t4238357500 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LineUnity_t4238357500 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CUpdateLineUnityU3Ed__8_t3720518906, ___U3CiU3E5__1_3)); }
	inline int32_t get_U3CiU3E5__1_3() const { return ___U3CiU3E5__1_3; }
	inline int32_t* get_address_of_U3CiU3E5__1_3() { return &___U3CiU3E5__1_3; }
	inline void set_U3CiU3E5__1_3(int32_t value)
	{
		___U3CiU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CUpdateLineUnityU3Ed__8_t3720518906, ___U3CiU3E5__2_4)); }
	inline int32_t get_U3CiU3E5__2_4() const { return ___U3CiU3E5__2_4; }
	inline int32_t* get_address_of_U3CiU3E5__2_4() { return &___U3CiU3E5__2_4; }
	inline void set_U3CiU3E5__2_4(int32_t value)
	{
		___U3CiU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CnormalizedDistanceU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CUpdateLineUnityU3Ed__8_t3720518906, ___U3CnormalizedDistanceU3E5__3_5)); }
	inline float get_U3CnormalizedDistanceU3E5__3_5() const { return ___U3CnormalizedDistanceU3E5__3_5; }
	inline float* get_address_of_U3CnormalizedDistanceU3E5__3_5() { return &___U3CnormalizedDistanceU3E5__3_5; }
	inline void set_U3CnormalizedDistanceU3E5__3_5(float value)
	{
		___U3CnormalizedDistanceU3E5__3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATELINEUNITYU3ED__8_T3720518906_H
#ifndef U3CDRAWOVERTIMEU3ED__4_T2030358129_H
#define U3CDRAWOVERTIMEU3ED__4_T2030358129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.Eraser/<DrawOverTime>d__4
struct  U3CDrawOverTimeU3Ed__4_t2030358129  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.ControllerExamples.Eraser/<DrawOverTime>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.ControllerExamples.Eraser/<DrawOverTime>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.ControllerExamples.Eraser HoloToolkit.Unity.ControllerExamples.Eraser/<DrawOverTime>d__4::<>4__this
	Eraser_t1049249012 * ___U3CU3E4__this_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HoloToolkit.Unity.ControllerExamples.Eraser/<DrawOverTime>d__4::<brushStrokes>5__1
	List_1_t2585711361 * ___U3CbrushStrokesU3E5__1_3;
	// System.Int32 HoloToolkit.Unity.ControllerExamples.Eraser/<DrawOverTime>d__4::<i>5__2
	int32_t ___U3CiU3E5__2_4;
	// UnityEngine.LineRenderer HoloToolkit.Unity.ControllerExamples.Eraser/<DrawOverTime>d__4::<lineRenderer>5__3
	LineRenderer_t3154350270 * ___U3ClineRendererU3E5__3_5;
	// UnityEngine.Vector3[] HoloToolkit.Unity.ControllerExamples.Eraser/<DrawOverTime>d__4::<positions>5__4
	Vector3U5BU5D_t1718750761* ___U3CpositionsU3E5__4_6;
	// System.Int32 HoloToolkit.Unity.ControllerExamples.Eraser/<DrawOverTime>d__4::<j>5__5
	int32_t ___U3CjU3E5__5_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__4_t2030358129, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__4_t2030358129, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__4_t2030358129, ___U3CU3E4__this_2)); }
	inline Eraser_t1049249012 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Eraser_t1049249012 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Eraser_t1049249012 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CbrushStrokesU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__4_t2030358129, ___U3CbrushStrokesU3E5__1_3)); }
	inline List_1_t2585711361 * get_U3CbrushStrokesU3E5__1_3() const { return ___U3CbrushStrokesU3E5__1_3; }
	inline List_1_t2585711361 ** get_address_of_U3CbrushStrokesU3E5__1_3() { return &___U3CbrushStrokesU3E5__1_3; }
	inline void set_U3CbrushStrokesU3E5__1_3(List_1_t2585711361 * value)
	{
		___U3CbrushStrokesU3E5__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbrushStrokesU3E5__1_3), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__4_t2030358129, ___U3CiU3E5__2_4)); }
	inline int32_t get_U3CiU3E5__2_4() const { return ___U3CiU3E5__2_4; }
	inline int32_t* get_address_of_U3CiU3E5__2_4() { return &___U3CiU3E5__2_4; }
	inline void set_U3CiU3E5__2_4(int32_t value)
	{
		___U3CiU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3ClineRendererU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__4_t2030358129, ___U3ClineRendererU3E5__3_5)); }
	inline LineRenderer_t3154350270 * get_U3ClineRendererU3E5__3_5() const { return ___U3ClineRendererU3E5__3_5; }
	inline LineRenderer_t3154350270 ** get_address_of_U3ClineRendererU3E5__3_5() { return &___U3ClineRendererU3E5__3_5; }
	inline void set_U3ClineRendererU3E5__3_5(LineRenderer_t3154350270 * value)
	{
		___U3ClineRendererU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClineRendererU3E5__3_5), value);
	}

	inline static int32_t get_offset_of_U3CpositionsU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__4_t2030358129, ___U3CpositionsU3E5__4_6)); }
	inline Vector3U5BU5D_t1718750761* get_U3CpositionsU3E5__4_6() const { return ___U3CpositionsU3E5__4_6; }
	inline Vector3U5BU5D_t1718750761** get_address_of_U3CpositionsU3E5__4_6() { return &___U3CpositionsU3E5__4_6; }
	inline void set_U3CpositionsU3E5__4_6(Vector3U5BU5D_t1718750761* value)
	{
		___U3CpositionsU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpositionsU3E5__4_6), value);
	}

	inline static int32_t get_offset_of_U3CjU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__4_t2030358129, ___U3CjU3E5__5_7)); }
	inline int32_t get_U3CjU3E5__5_7() const { return ___U3CjU3E5__5_7; }
	inline int32_t* get_address_of_U3CjU3E5__5_7() { return &___U3CjU3E5__5_7; }
	inline void set_U3CjU3E5__5_7(int32_t value)
	{
		___U3CjU3E5__5_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDRAWOVERTIMEU3ED__4_T2030358129_H
#ifndef U3CSWITCHOVERTIMEU3ED__25_T91139974_H
#define U3CSWITCHOVERTIMEU3ED__25_T91139974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.ObjectSpawner/<SwitchOverTime>d__25
struct  U3CSwitchOverTimeU3Ed__25_t91139974  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.ControllerExamples.ObjectSpawner/<SwitchOverTime>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.ControllerExamples.ObjectSpawner/<SwitchOverTime>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.ControllerExamples.ObjectSpawner HoloToolkit.Unity.ControllerExamples.ObjectSpawner/<SwitchOverTime>d__25::<>4__this
	ObjectSpawner_t3925816580 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSwitchOverTimeU3Ed__25_t91139974, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSwitchOverTimeU3Ed__25_t91139974, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSwitchOverTimeU3Ed__25_t91139974, ___U3CU3E4__this_2)); }
	inline ObjectSpawner_t3925816580 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObjectSpawner_t3925816580 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObjectSpawner_t3925816580 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSWITCHOVERTIMEU3ED__25_T91139974_H
#ifndef __STATICARRAYINITTYPESIZEU3D2048_T1070431641_H
#define __STATICARRAYINITTYPESIZEU3D2048_T1070431641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=2048
struct  __StaticArrayInitTypeSizeU3D2048_t1070431641 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D2048_t1070431641__padding[2048];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D2048_T1070431641_H
#ifndef __STATICARRAYINITTYPESIZEU3D864_T1736813345_H
#define __STATICARRAYINITTYPESIZEU3D864_T1736813345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=864
struct  __StaticArrayInitTypeSizeU3D864_t1736813345 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D864_t1736813345__padding[864];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D864_T1736813345_H
#ifndef __STATICARRAYINITTYPESIZEU3D5120_T1474365685_H
#define __STATICARRAYINITTYPESIZEU3D5120_T1474365685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=5120
struct  __StaticArrayInitTypeSizeU3D5120_t1474365685 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D5120_t1474365685__padding[5120];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D5120_T1474365685_H
#ifndef __STATICARRAYINITTYPESIZEU3D4480_T3791498020_H
#define __STATICARRAYINITTYPESIZEU3D4480_T3791498020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=4480
struct  __StaticArrayInitTypeSizeU3D4480_t3791498020 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D4480_t3791498020__padding[4480];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D4480_T3791498020_H
#ifndef __STATICARRAYINITTYPESIZEU3D36_T385526563_H
#define __STATICARRAYINITTYPESIZEU3D36_T385526563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36
struct  __StaticArrayInitTypeSizeU3D36_t385526563 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D36_t385526563__padding[36];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D36_T385526563_H
#ifndef __STATICARRAYINITTYPESIZEU3D96_T385919777_H
#define __STATICARRAYINITTYPESIZEU3D96_T385919777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=96
struct  __StaticArrayInitTypeSizeU3D96_t385919777 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D96_t385919777__padding[96];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D96_T385919777_H
#ifndef __STATICARRAYINITTYPESIZEU3D128_T531529104_H
#define __STATICARRAYINITTYPESIZEU3D128_T531529104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128
struct  __StaticArrayInitTypeSizeU3D128_t531529104 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D128_t531529104__padding[128];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D128_T531529104_H
#ifndef __STATICARRAYINITTYPESIZEU3D100_T2134348887_H
#define __STATICARRAYINITTYPESIZEU3D100_T2134348887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=100
struct  __StaticArrayInitTypeSizeU3D100_t2134348887 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D100_t2134348887__padding[100];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D100_T2134348887_H
#ifndef __STATICARRAYINITTYPESIZEU3D576_T2919708294_H
#define __STATICARRAYINITTYPESIZEU3D576_T2919708294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=576
struct  __StaticArrayInitTypeSizeU3D576_t2919708294 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D576_t2919708294__padding[576];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D576_T2919708294_H
#ifndef __STATICARRAYINITTYPESIZEU3D200_T2134545495_H
#define __STATICARRAYINITTYPESIZEU3D200_T2134545495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=200
struct  __StaticArrayInitTypeSizeU3D200_t2134545495 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D200_t2134545495__padding[200];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D200_T2134545495_H
#ifndef __STATICARRAYINITTYPESIZEU3D64_T3517497838_H
#define __STATICARRAYINITTYPESIZEU3D64_T3517497838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64
struct  __StaticArrayInitTypeSizeU3D64_t3517497838 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D64_t3517497838__padding[64];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D64_T3517497838_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef STATEENUM_T2160764486_H
#define STATEENUM_T2160764486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.ObjectSpawner/StateEnum
struct  StateEnum_t2160764486 
{
public:
	// System.Int32 HoloToolkit.Unity.ControllerExamples.ObjectSpawner/StateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StateEnum_t2160764486, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEENUM_T2160764486_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255376_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255376  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128 <PrivateImplementationDetails>::013E068C5CF13ADD6FADCBE20DC57D04D9748467
	__StaticArrayInitTypeSizeU3D128_t531529104  ___013E068C5CF13ADD6FADCBE20DC57D04D9748467_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=864 <PrivateImplementationDetails>::0C334FA5B28CF955C37AF8CEFB7F11ED8156B371
	__StaticArrayInitTypeSizeU3D864_t1736813345  ___0C334FA5B28CF955C37AF8CEFB7F11ED8156B371_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=200 <PrivateImplementationDetails>::0F9516F3B2268DEFAD0122DF5B7EB338BD7C6F20
	__StaticArrayInitTypeSizeU3D200_t2134545495  ___0F9516F3B2268DEFAD0122DF5B7EB338BD7C6F20_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=2048 <PrivateImplementationDetails>::1D1DF6626546FCE866E0F077A81E3F15D2FDAA02
	__StaticArrayInitTypeSizeU3D2048_t1070431641  ___1D1DF6626546FCE866E0F077A81E3F15D2FDAA02_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::326FF6F77BB5C244576A14A56AB1CB5F79950ED5
	__StaticArrayInitTypeSizeU3D36_t385526563  ___326FF6F77BB5C244576A14A56AB1CB5F79950ED5_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=5120 <PrivateImplementationDetails>::39E2F60BD85399DDD5728E8DD2951FB4457ABEC4
	__StaticArrayInitTypeSizeU3D5120_t1474365685  ___39E2F60BD85399DDD5728E8DD2951FB4457ABEC4_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::5D55BEE8A63F25A4C9ADC5776E15D1077A336940
	__StaticArrayInitTypeSizeU3D64_t3517497838  ___5D55BEE8A63F25A4C9ADC5776E15D1077A336940_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=96 <PrivateImplementationDetails>::62E39AF6A4EDE5B98ACA51428D9AF0E37629FB4F
	__StaticArrayInitTypeSizeU3D96_t385919777  ___62E39AF6A4EDE5B98ACA51428D9AF0E37629FB4F_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=100 <PrivateImplementationDetails>::6497EB3FE2DD456D24F706237E5A882D2BA9996F
	__StaticArrayInitTypeSizeU3D100_t2134348887  ___6497EB3FE2DD456D24F706237E5A882D2BA9996F_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=96 <PrivateImplementationDetails>::77500517B2CAA87B3925D20EEAA1428A8CD11B9C
	__StaticArrayInitTypeSizeU3D96_t385919777  ___77500517B2CAA87B3925D20EEAA1428A8CD11B9C_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::7C864787BEF4C9722095E7546132ACBC5C8FD940
	__StaticArrayInitTypeSizeU3D36_t385526563  ___7C864787BEF4C9722095E7546132ACBC5C8FD940_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::8634E53F81A91ED972DCFB19F840F53AC39A67F9
	__StaticArrayInitTypeSizeU3D64_t3517497838  ___8634E53F81A91ED972DCFB19F840F53AC39A67F9_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=576 <PrivateImplementationDetails>::888C635713ED1BDB91D86A17C2F3F4647CF4A55F
	__StaticArrayInitTypeSizeU3D576_t2919708294  ___888C635713ED1BDB91D86A17C2F3F4647CF4A55F_12;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=576 <PrivateImplementationDetails>::8CFF0276DAC339BBEAD6BFC55B53ED9492217C08
	__StaticArrayInitTypeSizeU3D576_t2919708294  ___8CFF0276DAC339BBEAD6BFC55B53ED9492217C08_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=4480 <PrivateImplementationDetails>::9AE3FD136714B4C4439E72CA48C36314C843CA0E
	__StaticArrayInitTypeSizeU3D4480_t3791498020  ___9AE3FD136714B4C4439E72CA48C36314C843CA0E_14;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=200 <PrivateImplementationDetails>::A714705DEAA10EE10D925E684E64E7CC81896007
	__StaticArrayInitTypeSizeU3D200_t2134545495  ___A714705DEAA10EE10D925E684E64E7CC81896007_15;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=100 <PrivateImplementationDetails>::C962AD6D5D1773668C4D37330DA336E1C53D07DE
	__StaticArrayInitTypeSizeU3D100_t2134348887  ___C962AD6D5D1773668C4D37330DA336E1C53D07DE_16;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=96 <PrivateImplementationDetails>::F84A5DF36B7884A8F76FAEED14A7E066C2DAA4DC
	__StaticArrayInitTypeSizeU3D96_t385919777  ___F84A5DF36B7884A8F76FAEED14A7E066C2DAA4DC_17;

public:
	inline static int32_t get_offset_of_U3013E068C5CF13ADD6FADCBE20DC57D04D9748467_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___013E068C5CF13ADD6FADCBE20DC57D04D9748467_0)); }
	inline __StaticArrayInitTypeSizeU3D128_t531529104  get_U3013E068C5CF13ADD6FADCBE20DC57D04D9748467_0() const { return ___013E068C5CF13ADD6FADCBE20DC57D04D9748467_0; }
	inline __StaticArrayInitTypeSizeU3D128_t531529104 * get_address_of_U3013E068C5CF13ADD6FADCBE20DC57D04D9748467_0() { return &___013E068C5CF13ADD6FADCBE20DC57D04D9748467_0; }
	inline void set_U3013E068C5CF13ADD6FADCBE20DC57D04D9748467_0(__StaticArrayInitTypeSizeU3D128_t531529104  value)
	{
		___013E068C5CF13ADD6FADCBE20DC57D04D9748467_0 = value;
	}

	inline static int32_t get_offset_of_U30C334FA5B28CF955C37AF8CEFB7F11ED8156B371_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___0C334FA5B28CF955C37AF8CEFB7F11ED8156B371_1)); }
	inline __StaticArrayInitTypeSizeU3D864_t1736813345  get_U30C334FA5B28CF955C37AF8CEFB7F11ED8156B371_1() const { return ___0C334FA5B28CF955C37AF8CEFB7F11ED8156B371_1; }
	inline __StaticArrayInitTypeSizeU3D864_t1736813345 * get_address_of_U30C334FA5B28CF955C37AF8CEFB7F11ED8156B371_1() { return &___0C334FA5B28CF955C37AF8CEFB7F11ED8156B371_1; }
	inline void set_U30C334FA5B28CF955C37AF8CEFB7F11ED8156B371_1(__StaticArrayInitTypeSizeU3D864_t1736813345  value)
	{
		___0C334FA5B28CF955C37AF8CEFB7F11ED8156B371_1 = value;
	}

	inline static int32_t get_offset_of_U30F9516F3B2268DEFAD0122DF5B7EB338BD7C6F20_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___0F9516F3B2268DEFAD0122DF5B7EB338BD7C6F20_2)); }
	inline __StaticArrayInitTypeSizeU3D200_t2134545495  get_U30F9516F3B2268DEFAD0122DF5B7EB338BD7C6F20_2() const { return ___0F9516F3B2268DEFAD0122DF5B7EB338BD7C6F20_2; }
	inline __StaticArrayInitTypeSizeU3D200_t2134545495 * get_address_of_U30F9516F3B2268DEFAD0122DF5B7EB338BD7C6F20_2() { return &___0F9516F3B2268DEFAD0122DF5B7EB338BD7C6F20_2; }
	inline void set_U30F9516F3B2268DEFAD0122DF5B7EB338BD7C6F20_2(__StaticArrayInitTypeSizeU3D200_t2134545495  value)
	{
		___0F9516F3B2268DEFAD0122DF5B7EB338BD7C6F20_2 = value;
	}

	inline static int32_t get_offset_of_U31D1DF6626546FCE866E0F077A81E3F15D2FDAA02_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___1D1DF6626546FCE866E0F077A81E3F15D2FDAA02_3)); }
	inline __StaticArrayInitTypeSizeU3D2048_t1070431641  get_U31D1DF6626546FCE866E0F077A81E3F15D2FDAA02_3() const { return ___1D1DF6626546FCE866E0F077A81E3F15D2FDAA02_3; }
	inline __StaticArrayInitTypeSizeU3D2048_t1070431641 * get_address_of_U31D1DF6626546FCE866E0F077A81E3F15D2FDAA02_3() { return &___1D1DF6626546FCE866E0F077A81E3F15D2FDAA02_3; }
	inline void set_U31D1DF6626546FCE866E0F077A81E3F15D2FDAA02_3(__StaticArrayInitTypeSizeU3D2048_t1070431641  value)
	{
		___1D1DF6626546FCE866E0F077A81E3F15D2FDAA02_3 = value;
	}

	inline static int32_t get_offset_of_U3326FF6F77BB5C244576A14A56AB1CB5F79950ED5_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___326FF6F77BB5C244576A14A56AB1CB5F79950ED5_4)); }
	inline __StaticArrayInitTypeSizeU3D36_t385526563  get_U3326FF6F77BB5C244576A14A56AB1CB5F79950ED5_4() const { return ___326FF6F77BB5C244576A14A56AB1CB5F79950ED5_4; }
	inline __StaticArrayInitTypeSizeU3D36_t385526563 * get_address_of_U3326FF6F77BB5C244576A14A56AB1CB5F79950ED5_4() { return &___326FF6F77BB5C244576A14A56AB1CB5F79950ED5_4; }
	inline void set_U3326FF6F77BB5C244576A14A56AB1CB5F79950ED5_4(__StaticArrayInitTypeSizeU3D36_t385526563  value)
	{
		___326FF6F77BB5C244576A14A56AB1CB5F79950ED5_4 = value;
	}

	inline static int32_t get_offset_of_U339E2F60BD85399DDD5728E8DD2951FB4457ABEC4_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___39E2F60BD85399DDD5728E8DD2951FB4457ABEC4_5)); }
	inline __StaticArrayInitTypeSizeU3D5120_t1474365685  get_U339E2F60BD85399DDD5728E8DD2951FB4457ABEC4_5() const { return ___39E2F60BD85399DDD5728E8DD2951FB4457ABEC4_5; }
	inline __StaticArrayInitTypeSizeU3D5120_t1474365685 * get_address_of_U339E2F60BD85399DDD5728E8DD2951FB4457ABEC4_5() { return &___39E2F60BD85399DDD5728E8DD2951FB4457ABEC4_5; }
	inline void set_U339E2F60BD85399DDD5728E8DD2951FB4457ABEC4_5(__StaticArrayInitTypeSizeU3D5120_t1474365685  value)
	{
		___39E2F60BD85399DDD5728E8DD2951FB4457ABEC4_5 = value;
	}

	inline static int32_t get_offset_of_U35D55BEE8A63F25A4C9ADC5776E15D1077A336940_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___5D55BEE8A63F25A4C9ADC5776E15D1077A336940_6)); }
	inline __StaticArrayInitTypeSizeU3D64_t3517497838  get_U35D55BEE8A63F25A4C9ADC5776E15D1077A336940_6() const { return ___5D55BEE8A63F25A4C9ADC5776E15D1077A336940_6; }
	inline __StaticArrayInitTypeSizeU3D64_t3517497838 * get_address_of_U35D55BEE8A63F25A4C9ADC5776E15D1077A336940_6() { return &___5D55BEE8A63F25A4C9ADC5776E15D1077A336940_6; }
	inline void set_U35D55BEE8A63F25A4C9ADC5776E15D1077A336940_6(__StaticArrayInitTypeSizeU3D64_t3517497838  value)
	{
		___5D55BEE8A63F25A4C9ADC5776E15D1077A336940_6 = value;
	}

	inline static int32_t get_offset_of_U362E39AF6A4EDE5B98ACA51428D9AF0E37629FB4F_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___62E39AF6A4EDE5B98ACA51428D9AF0E37629FB4F_7)); }
	inline __StaticArrayInitTypeSizeU3D96_t385919777  get_U362E39AF6A4EDE5B98ACA51428D9AF0E37629FB4F_7() const { return ___62E39AF6A4EDE5B98ACA51428D9AF0E37629FB4F_7; }
	inline __StaticArrayInitTypeSizeU3D96_t385919777 * get_address_of_U362E39AF6A4EDE5B98ACA51428D9AF0E37629FB4F_7() { return &___62E39AF6A4EDE5B98ACA51428D9AF0E37629FB4F_7; }
	inline void set_U362E39AF6A4EDE5B98ACA51428D9AF0E37629FB4F_7(__StaticArrayInitTypeSizeU3D96_t385919777  value)
	{
		___62E39AF6A4EDE5B98ACA51428D9AF0E37629FB4F_7 = value;
	}

	inline static int32_t get_offset_of_U36497EB3FE2DD456D24F706237E5A882D2BA9996F_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___6497EB3FE2DD456D24F706237E5A882D2BA9996F_8)); }
	inline __StaticArrayInitTypeSizeU3D100_t2134348887  get_U36497EB3FE2DD456D24F706237E5A882D2BA9996F_8() const { return ___6497EB3FE2DD456D24F706237E5A882D2BA9996F_8; }
	inline __StaticArrayInitTypeSizeU3D100_t2134348887 * get_address_of_U36497EB3FE2DD456D24F706237E5A882D2BA9996F_8() { return &___6497EB3FE2DD456D24F706237E5A882D2BA9996F_8; }
	inline void set_U36497EB3FE2DD456D24F706237E5A882D2BA9996F_8(__StaticArrayInitTypeSizeU3D100_t2134348887  value)
	{
		___6497EB3FE2DD456D24F706237E5A882D2BA9996F_8 = value;
	}

	inline static int32_t get_offset_of_U377500517B2CAA87B3925D20EEAA1428A8CD11B9C_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___77500517B2CAA87B3925D20EEAA1428A8CD11B9C_9)); }
	inline __StaticArrayInitTypeSizeU3D96_t385919777  get_U377500517B2CAA87B3925D20EEAA1428A8CD11B9C_9() const { return ___77500517B2CAA87B3925D20EEAA1428A8CD11B9C_9; }
	inline __StaticArrayInitTypeSizeU3D96_t385919777 * get_address_of_U377500517B2CAA87B3925D20EEAA1428A8CD11B9C_9() { return &___77500517B2CAA87B3925D20EEAA1428A8CD11B9C_9; }
	inline void set_U377500517B2CAA87B3925D20EEAA1428A8CD11B9C_9(__StaticArrayInitTypeSizeU3D96_t385919777  value)
	{
		___77500517B2CAA87B3925D20EEAA1428A8CD11B9C_9 = value;
	}

	inline static int32_t get_offset_of_U37C864787BEF4C9722095E7546132ACBC5C8FD940_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___7C864787BEF4C9722095E7546132ACBC5C8FD940_10)); }
	inline __StaticArrayInitTypeSizeU3D36_t385526563  get_U37C864787BEF4C9722095E7546132ACBC5C8FD940_10() const { return ___7C864787BEF4C9722095E7546132ACBC5C8FD940_10; }
	inline __StaticArrayInitTypeSizeU3D36_t385526563 * get_address_of_U37C864787BEF4C9722095E7546132ACBC5C8FD940_10() { return &___7C864787BEF4C9722095E7546132ACBC5C8FD940_10; }
	inline void set_U37C864787BEF4C9722095E7546132ACBC5C8FD940_10(__StaticArrayInitTypeSizeU3D36_t385526563  value)
	{
		___7C864787BEF4C9722095E7546132ACBC5C8FD940_10 = value;
	}

	inline static int32_t get_offset_of_U38634E53F81A91ED972DCFB19F840F53AC39A67F9_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___8634E53F81A91ED972DCFB19F840F53AC39A67F9_11)); }
	inline __StaticArrayInitTypeSizeU3D64_t3517497838  get_U38634E53F81A91ED972DCFB19F840F53AC39A67F9_11() const { return ___8634E53F81A91ED972DCFB19F840F53AC39A67F9_11; }
	inline __StaticArrayInitTypeSizeU3D64_t3517497838 * get_address_of_U38634E53F81A91ED972DCFB19F840F53AC39A67F9_11() { return &___8634E53F81A91ED972DCFB19F840F53AC39A67F9_11; }
	inline void set_U38634E53F81A91ED972DCFB19F840F53AC39A67F9_11(__StaticArrayInitTypeSizeU3D64_t3517497838  value)
	{
		___8634E53F81A91ED972DCFB19F840F53AC39A67F9_11 = value;
	}

	inline static int32_t get_offset_of_U3888C635713ED1BDB91D86A17C2F3F4647CF4A55F_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___888C635713ED1BDB91D86A17C2F3F4647CF4A55F_12)); }
	inline __StaticArrayInitTypeSizeU3D576_t2919708294  get_U3888C635713ED1BDB91D86A17C2F3F4647CF4A55F_12() const { return ___888C635713ED1BDB91D86A17C2F3F4647CF4A55F_12; }
	inline __StaticArrayInitTypeSizeU3D576_t2919708294 * get_address_of_U3888C635713ED1BDB91D86A17C2F3F4647CF4A55F_12() { return &___888C635713ED1BDB91D86A17C2F3F4647CF4A55F_12; }
	inline void set_U3888C635713ED1BDB91D86A17C2F3F4647CF4A55F_12(__StaticArrayInitTypeSizeU3D576_t2919708294  value)
	{
		___888C635713ED1BDB91D86A17C2F3F4647CF4A55F_12 = value;
	}

	inline static int32_t get_offset_of_U38CFF0276DAC339BBEAD6BFC55B53ED9492217C08_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___8CFF0276DAC339BBEAD6BFC55B53ED9492217C08_13)); }
	inline __StaticArrayInitTypeSizeU3D576_t2919708294  get_U38CFF0276DAC339BBEAD6BFC55B53ED9492217C08_13() const { return ___8CFF0276DAC339BBEAD6BFC55B53ED9492217C08_13; }
	inline __StaticArrayInitTypeSizeU3D576_t2919708294 * get_address_of_U38CFF0276DAC339BBEAD6BFC55B53ED9492217C08_13() { return &___8CFF0276DAC339BBEAD6BFC55B53ED9492217C08_13; }
	inline void set_U38CFF0276DAC339BBEAD6BFC55B53ED9492217C08_13(__StaticArrayInitTypeSizeU3D576_t2919708294  value)
	{
		___8CFF0276DAC339BBEAD6BFC55B53ED9492217C08_13 = value;
	}

	inline static int32_t get_offset_of_U39AE3FD136714B4C4439E72CA48C36314C843CA0E_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___9AE3FD136714B4C4439E72CA48C36314C843CA0E_14)); }
	inline __StaticArrayInitTypeSizeU3D4480_t3791498020  get_U39AE3FD136714B4C4439E72CA48C36314C843CA0E_14() const { return ___9AE3FD136714B4C4439E72CA48C36314C843CA0E_14; }
	inline __StaticArrayInitTypeSizeU3D4480_t3791498020 * get_address_of_U39AE3FD136714B4C4439E72CA48C36314C843CA0E_14() { return &___9AE3FD136714B4C4439E72CA48C36314C843CA0E_14; }
	inline void set_U39AE3FD136714B4C4439E72CA48C36314C843CA0E_14(__StaticArrayInitTypeSizeU3D4480_t3791498020  value)
	{
		___9AE3FD136714B4C4439E72CA48C36314C843CA0E_14 = value;
	}

	inline static int32_t get_offset_of_A714705DEAA10EE10D925E684E64E7CC81896007_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___A714705DEAA10EE10D925E684E64E7CC81896007_15)); }
	inline __StaticArrayInitTypeSizeU3D200_t2134545495  get_A714705DEAA10EE10D925E684E64E7CC81896007_15() const { return ___A714705DEAA10EE10D925E684E64E7CC81896007_15; }
	inline __StaticArrayInitTypeSizeU3D200_t2134545495 * get_address_of_A714705DEAA10EE10D925E684E64E7CC81896007_15() { return &___A714705DEAA10EE10D925E684E64E7CC81896007_15; }
	inline void set_A714705DEAA10EE10D925E684E64E7CC81896007_15(__StaticArrayInitTypeSizeU3D200_t2134545495  value)
	{
		___A714705DEAA10EE10D925E684E64E7CC81896007_15 = value;
	}

	inline static int32_t get_offset_of_C962AD6D5D1773668C4D37330DA336E1C53D07DE_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___C962AD6D5D1773668C4D37330DA336E1C53D07DE_16)); }
	inline __StaticArrayInitTypeSizeU3D100_t2134348887  get_C962AD6D5D1773668C4D37330DA336E1C53D07DE_16() const { return ___C962AD6D5D1773668C4D37330DA336E1C53D07DE_16; }
	inline __StaticArrayInitTypeSizeU3D100_t2134348887 * get_address_of_C962AD6D5D1773668C4D37330DA336E1C53D07DE_16() { return &___C962AD6D5D1773668C4D37330DA336E1C53D07DE_16; }
	inline void set_C962AD6D5D1773668C4D37330DA336E1C53D07DE_16(__StaticArrayInitTypeSizeU3D100_t2134348887  value)
	{
		___C962AD6D5D1773668C4D37330DA336E1C53D07DE_16 = value;
	}

	inline static int32_t get_offset_of_F84A5DF36B7884A8F76FAEED14A7E066C2DAA4DC_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___F84A5DF36B7884A8F76FAEED14A7E066C2DAA4DC_17)); }
	inline __StaticArrayInitTypeSizeU3D96_t385919777  get_F84A5DF36B7884A8F76FAEED14A7E066C2DAA4DC_17() const { return ___F84A5DF36B7884A8F76FAEED14A7E066C2DAA4DC_17; }
	inline __StaticArrayInitTypeSizeU3D96_t385919777 * get_address_of_F84A5DF36B7884A8F76FAEED14A7E066C2DAA4DC_17() { return &___F84A5DF36B7884A8F76FAEED14A7E066C2DAA4DC_17; }
	inline void set_F84A5DF36B7884A8F76FAEED14A7E066C2DAA4DC_17(__StaticArrayInitTypeSizeU3D96_t385919777  value)
	{
		___F84A5DF36B7884A8F76FAEED14A7E066C2DAA4DC_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255376_H
#ifndef CONTROLLERELEMENTENUM_T4108764020_H
#define CONTROLLERELEMENTENUM_T4108764020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.MotionControllerInfo/ControllerElementEnum
struct  ControllerElementEnum_t4108764020 
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.MotionControllerInfo/ControllerElementEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerElementEnum_t4108764020, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERELEMENTENUM_T4108764020_H
#ifndef SWIPEENUM_T3123030255_H
#define SWIPEENUM_T3123030255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.BrushSelector/SwipeEnum
struct  SwipeEnum_t3123030255 
{
public:
	// System.Int32 HoloToolkit.Unity.ControllerExamples.BrushSelector/SwipeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SwipeEnum_t3123030255, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEENUM_T3123030255_H
#ifndef U3CERASESTROKESOVERTIMEU3ED__5_T963038218_H
#define U3CERASESTROKESOVERTIMEU3ED__5_T963038218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.Eraser/<EraseStrokesOverTime>d__5
struct  U3CEraseStrokesOverTimeU3Ed__5_t963038218  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.ControllerExamples.Eraser/<EraseStrokesOverTime>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.ControllerExamples.Eraser/<EraseStrokesOverTime>d__5::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.ControllerExamples.Eraser HoloToolkit.Unity.ControllerExamples.Eraser/<EraseStrokesOverTime>d__5::<>4__this
	Eraser_t1049249012 * ___U3CU3E4__this_2;
	// UnityEngine.LineRenderer HoloToolkit.Unity.ControllerExamples.Eraser/<EraseStrokesOverTime>d__5::<lineRenderer>5__1
	LineRenderer_t3154350270 * ___U3ClineRendererU3E5__1_3;
	// System.Single HoloToolkit.Unity.ControllerExamples.Eraser/<EraseStrokesOverTime>d__5::<startTime>5__2
	float ___U3CstartTimeU3E5__2_4;
	// System.Single HoloToolkit.Unity.ControllerExamples.Eraser/<EraseStrokesOverTime>d__5::<startWidth>5__3
	float ___U3CstartWidthU3E5__3_5;
	// UnityEngine.Vector3[] HoloToolkit.Unity.ControllerExamples.Eraser/<EraseStrokesOverTime>d__5::<startPositions>5__4
	Vector3U5BU5D_t1718750761* ___U3CstartPositionsU3E5__4_6;
	// UnityEngine.Vector3[] HoloToolkit.Unity.ControllerExamples.Eraser/<EraseStrokesOverTime>d__5::<endPositions>5__5
	Vector3U5BU5D_t1718750761* ___U3CendPositionsU3E5__5_7;
	// System.Single HoloToolkit.Unity.ControllerExamples.Eraser/<EraseStrokesOverTime>d__5::<normalizedTime>5__6
	float ___U3CnormalizedTimeU3E5__6_8;
	// System.Int32 HoloToolkit.Unity.ControllerExamples.Eraser/<EraseStrokesOverTime>d__5::<i>5__7
	int32_t ___U3CiU3E5__7_9;
	// UnityEngine.Vector3 HoloToolkit.Unity.ControllerExamples.Eraser/<EraseStrokesOverTime>d__5::<randomNoise>5__8
	Vector3_t3722313464  ___U3CrandomNoiseU3E5__8_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CEraseStrokesOverTimeU3Ed__5_t963038218, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CEraseStrokesOverTimeU3Ed__5_t963038218, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CEraseStrokesOverTimeU3Ed__5_t963038218, ___U3CU3E4__this_2)); }
	inline Eraser_t1049249012 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Eraser_t1049249012 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Eraser_t1049249012 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3ClineRendererU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CEraseStrokesOverTimeU3Ed__5_t963038218, ___U3ClineRendererU3E5__1_3)); }
	inline LineRenderer_t3154350270 * get_U3ClineRendererU3E5__1_3() const { return ___U3ClineRendererU3E5__1_3; }
	inline LineRenderer_t3154350270 ** get_address_of_U3ClineRendererU3E5__1_3() { return &___U3ClineRendererU3E5__1_3; }
	inline void set_U3ClineRendererU3E5__1_3(LineRenderer_t3154350270 * value)
	{
		___U3ClineRendererU3E5__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClineRendererU3E5__1_3), value);
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CEraseStrokesOverTimeU3Ed__5_t963038218, ___U3CstartTimeU3E5__2_4)); }
	inline float get_U3CstartTimeU3E5__2_4() const { return ___U3CstartTimeU3E5__2_4; }
	inline float* get_address_of_U3CstartTimeU3E5__2_4() { return &___U3CstartTimeU3E5__2_4; }
	inline void set_U3CstartTimeU3E5__2_4(float value)
	{
		___U3CstartTimeU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CstartWidthU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CEraseStrokesOverTimeU3Ed__5_t963038218, ___U3CstartWidthU3E5__3_5)); }
	inline float get_U3CstartWidthU3E5__3_5() const { return ___U3CstartWidthU3E5__3_5; }
	inline float* get_address_of_U3CstartWidthU3E5__3_5() { return &___U3CstartWidthU3E5__3_5; }
	inline void set_U3CstartWidthU3E5__3_5(float value)
	{
		___U3CstartWidthU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CstartPositionsU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CEraseStrokesOverTimeU3Ed__5_t963038218, ___U3CstartPositionsU3E5__4_6)); }
	inline Vector3U5BU5D_t1718750761* get_U3CstartPositionsU3E5__4_6() const { return ___U3CstartPositionsU3E5__4_6; }
	inline Vector3U5BU5D_t1718750761** get_address_of_U3CstartPositionsU3E5__4_6() { return &___U3CstartPositionsU3E5__4_6; }
	inline void set_U3CstartPositionsU3E5__4_6(Vector3U5BU5D_t1718750761* value)
	{
		___U3CstartPositionsU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstartPositionsU3E5__4_6), value);
	}

	inline static int32_t get_offset_of_U3CendPositionsU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CEraseStrokesOverTimeU3Ed__5_t963038218, ___U3CendPositionsU3E5__5_7)); }
	inline Vector3U5BU5D_t1718750761* get_U3CendPositionsU3E5__5_7() const { return ___U3CendPositionsU3E5__5_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of_U3CendPositionsU3E5__5_7() { return &___U3CendPositionsU3E5__5_7; }
	inline void set_U3CendPositionsU3E5__5_7(Vector3U5BU5D_t1718750761* value)
	{
		___U3CendPositionsU3E5__5_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CendPositionsU3E5__5_7), value);
	}

	inline static int32_t get_offset_of_U3CnormalizedTimeU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CEraseStrokesOverTimeU3Ed__5_t963038218, ___U3CnormalizedTimeU3E5__6_8)); }
	inline float get_U3CnormalizedTimeU3E5__6_8() const { return ___U3CnormalizedTimeU3E5__6_8; }
	inline float* get_address_of_U3CnormalizedTimeU3E5__6_8() { return &___U3CnormalizedTimeU3E5__6_8; }
	inline void set_U3CnormalizedTimeU3E5__6_8(float value)
	{
		___U3CnormalizedTimeU3E5__6_8 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__7_9() { return static_cast<int32_t>(offsetof(U3CEraseStrokesOverTimeU3Ed__5_t963038218, ___U3CiU3E5__7_9)); }
	inline int32_t get_U3CiU3E5__7_9() const { return ___U3CiU3E5__7_9; }
	inline int32_t* get_address_of_U3CiU3E5__7_9() { return &___U3CiU3E5__7_9; }
	inline void set_U3CiU3E5__7_9(int32_t value)
	{
		___U3CiU3E5__7_9 = value;
	}

	inline static int32_t get_offset_of_U3CrandomNoiseU3E5__8_10() { return static_cast<int32_t>(offsetof(U3CEraseStrokesOverTimeU3Ed__5_t963038218, ___U3CrandomNoiseU3E5__8_10)); }
	inline Vector3_t3722313464  get_U3CrandomNoiseU3E5__8_10() const { return ___U3CrandomNoiseU3E5__8_10; }
	inline Vector3_t3722313464 * get_address_of_U3CrandomNoiseU3E5__8_10() { return &___U3CrandomNoiseU3E5__8_10; }
	inline void set_U3CrandomNoiseU3E5__8_10(Vector3_t3722313464  value)
	{
		___U3CrandomNoiseU3E5__8_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CERASESTROKESOVERTIMEU3ED__5_T963038218_H
#ifndef TRACKINGSPACETYPE_T4214487160_H
#define TRACKINGSPACETYPE_T4214487160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.TrackingSpaceType
struct  TrackingSpaceType_t4214487160 
{
public:
	// System.Int32 UnityEngine.XR.TrackingSpaceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingSpaceType_t4214487160, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSPACETYPE_T4214487160_H
#ifndef INTERACTIONSOURCEPRESSTYPE_T876504354_H
#define INTERACTIONSOURCEPRESSTYPE_T876504354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.WSA.Input.InteractionSourcePressType
struct  InteractionSourcePressType_t876504354 
{
public:
	// System.Int32 UnityEngine.XR.WSA.Input.InteractionSourcePressType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InteractionSourcePressType_t876504354, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSOURCEPRESSTYPE_T876504354_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef INTERACTIONSOURCEFLAGS_T1798650303_H
#define INTERACTIONSOURCEFLAGS_T1798650303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.WSA.Input.InteractionSourceFlags
struct  InteractionSourceFlags_t1798650303 
{
public:
	// System.Int32 UnityEngine.XR.WSA.Input.InteractionSourceFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InteractionSourceFlags_t1798650303, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSOURCEFLAGS_T1798650303_H
#ifndef INTERACTIONSOURCEHANDEDNESS_T3096408347_H
#define INTERACTIONSOURCEHANDEDNESS_T3096408347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.WSA.Input.InteractionSourceHandedness
struct  InteractionSourceHandedness_t3096408347 
{
public:
	// System.Int32 UnityEngine.XR.WSA.Input.InteractionSourceHandedness::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InteractionSourceHandedness_t3096408347, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSOURCEHANDEDNESS_T3096408347_H
#ifndef INTERACTIONSOURCEKIND_T3005082353_H
#define INTERACTIONSOURCEKIND_T3005082353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.WSA.Input.InteractionSourceKind
struct  InteractionSourceKind_t3005082353 
{
public:
	// System.Int32 UnityEngine.XR.WSA.Input.InteractionSourceKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InteractionSourceKind_t3005082353, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSOURCEKIND_T3005082353_H
#ifndef U3CDRAWOVERTIMEU3ED__24_T1141827424_H
#define U3CDRAWOVERTIMEU3ED__24_T1141827424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.BrushController/<DrawOverTime>d__24
struct  U3CDrawOverTimeU3Ed__24_t1141827424  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.ControllerExamples.BrushController/<DrawOverTime>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.ControllerExamples.BrushController/<DrawOverTime>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.ControllerExamples.BrushController HoloToolkit.Unity.ControllerExamples.BrushController/<DrawOverTime>d__24::<>4__this
	BrushController_t3766631826 * ___U3CU3E4__this_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.ControllerExamples.BrushController/<DrawOverTime>d__24::<lastPointPosition>5__1
	Vector3_t3722313464  ___U3ClastPointPositionU3E5__1_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.ControllerExamples.BrushController/<DrawOverTime>d__24::<startPosition>5__2
	Vector3_t3722313464  ___U3CstartPositionU3E5__2_4;
	// UnityEngine.GameObject HoloToolkit.Unity.ControllerExamples.BrushController/<DrawOverTime>d__24::<newStroke>5__3
	GameObject_t1113636619 * ___U3CnewStrokeU3E5__3_5;
	// UnityEngine.LineRenderer HoloToolkit.Unity.ControllerExamples.BrushController/<DrawOverTime>d__24::<line>5__4
	LineRenderer_t3154350270 * ___U3ClineU3E5__4_6;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushController/<DrawOverTime>d__24::<initialWidth>5__5
	float ___U3CinitialWidthU3E5__5_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__24_t1141827424, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__24_t1141827424, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__24_t1141827424, ___U3CU3E4__this_2)); }
	inline BrushController_t3766631826 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BrushController_t3766631826 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BrushController_t3766631826 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3ClastPointPositionU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__24_t1141827424, ___U3ClastPointPositionU3E5__1_3)); }
	inline Vector3_t3722313464  get_U3ClastPointPositionU3E5__1_3() const { return ___U3ClastPointPositionU3E5__1_3; }
	inline Vector3_t3722313464 * get_address_of_U3ClastPointPositionU3E5__1_3() { return &___U3ClastPointPositionU3E5__1_3; }
	inline void set_U3ClastPointPositionU3E5__1_3(Vector3_t3722313464  value)
	{
		___U3ClastPointPositionU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CstartPositionU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__24_t1141827424, ___U3CstartPositionU3E5__2_4)); }
	inline Vector3_t3722313464  get_U3CstartPositionU3E5__2_4() const { return ___U3CstartPositionU3E5__2_4; }
	inline Vector3_t3722313464 * get_address_of_U3CstartPositionU3E5__2_4() { return &___U3CstartPositionU3E5__2_4; }
	inline void set_U3CstartPositionU3E5__2_4(Vector3_t3722313464  value)
	{
		___U3CstartPositionU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CnewStrokeU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__24_t1141827424, ___U3CnewStrokeU3E5__3_5)); }
	inline GameObject_t1113636619 * get_U3CnewStrokeU3E5__3_5() const { return ___U3CnewStrokeU3E5__3_5; }
	inline GameObject_t1113636619 ** get_address_of_U3CnewStrokeU3E5__3_5() { return &___U3CnewStrokeU3E5__3_5; }
	inline void set_U3CnewStrokeU3E5__3_5(GameObject_t1113636619 * value)
	{
		___U3CnewStrokeU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnewStrokeU3E5__3_5), value);
	}

	inline static int32_t get_offset_of_U3ClineU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__24_t1141827424, ___U3ClineU3E5__4_6)); }
	inline LineRenderer_t3154350270 * get_U3ClineU3E5__4_6() const { return ___U3ClineU3E5__4_6; }
	inline LineRenderer_t3154350270 ** get_address_of_U3ClineU3E5__4_6() { return &___U3ClineU3E5__4_6; }
	inline void set_U3ClineU3E5__4_6(LineRenderer_t3154350270 * value)
	{
		___U3ClineU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClineU3E5__4_6), value);
	}

	inline static int32_t get_offset_of_U3CinitialWidthU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__24_t1141827424, ___U3CinitialWidthU3E5__5_7)); }
	inline float get_U3CinitialWidthU3E5__5_7() const { return ___U3CinitialWidthU3E5__5_7; }
	inline float* get_address_of_U3CinitialWidthU3E5__5_7() { return &___U3CinitialWidthU3E5__5_7; }
	inline void set_U3CinitialWidthU3E5__5_7(float value)
	{
		___U3CinitialWidthU3E5__5_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDRAWOVERTIMEU3ED__24_T1141827424_H
#ifndef POINTDISTRIBUTIONTYPEENUM_T4198058532_H
#define POINTDISTRIBUTIONTYPEENUM_T4198058532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.LineUtils/PointDistributionTypeEnum
struct  PointDistributionTypeEnum_t4198058532 
{
public:
	// System.Int32 HoloToolkit.Unity.Design.LineUtils/PointDistributionTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PointDistributionTypeEnum_t4198058532, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTDISTRIBUTIONTYPEENUM_T4198058532_H
#ifndef ROTATIONTYPEENUM_T2752505697_H
#define ROTATIONTYPEENUM_T2752505697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.LineUtils/RotationTypeEnum
struct  RotationTypeEnum_t2752505697 
{
public:
	// System.Int32 HoloToolkit.Unity.Design.LineUtils/RotationTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotationTypeEnum_t2752505697, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONTYPEENUM_T2752505697_H
#ifndef INTERPOLATIONMODEENUM_T4207999760_H
#define INTERPOLATIONMODEENUM_T4207999760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.LineUtils/InterpolationModeEnum
struct  InterpolationModeEnum_t4207999760 
{
public:
	// System.Int32 HoloToolkit.Unity.Design.LineUtils/InterpolationModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InterpolationModeEnum_t4207999760, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATIONMODEENUM_T4207999760_H
#ifndef STEPMODEENUM_T777535071_H
#define STEPMODEENUM_T777535071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.LineUtils/StepModeEnum
struct  StepModeEnum_t777535071 
{
public:
	// System.Int32 HoloToolkit.Unity.Design.LineUtils/StepModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StepModeEnum_t777535071, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEPMODEENUM_T777535071_H
#ifndef SPACEENUM_T2918055180_H
#define SPACEENUM_T2918055180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.LineUtils/SpaceEnum
struct  SpaceEnum_t2918055180 
{
public:
	// System.Int32 HoloToolkit.Unity.Design.LineUtils/SpaceEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpaceEnum_t2918055180, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACEENUM_T2918055180_H
#ifndef SPLINEPOINT_T3315310755_H
#define SPLINEPOINT_T3315310755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.SplinePoint
struct  SplinePoint_t3315310755 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.Design.SplinePoint::Point
	Vector3_t3722313464  ___Point_0;
	// UnityEngine.Quaternion HoloToolkit.Unity.Design.SplinePoint::Rotation
	Quaternion_t2301928331  ___Rotation_1;

public:
	inline static int32_t get_offset_of_Point_0() { return static_cast<int32_t>(offsetof(SplinePoint_t3315310755, ___Point_0)); }
	inline Vector3_t3722313464  get_Point_0() const { return ___Point_0; }
	inline Vector3_t3722313464 * get_address_of_Point_0() { return &___Point_0; }
	inline void set_Point_0(Vector3_t3722313464  value)
	{
		___Point_0 = value;
	}

	inline static int32_t get_offset_of_Rotation_1() { return static_cast<int32_t>(offsetof(SplinePoint_t3315310755, ___Rotation_1)); }
	inline Quaternion_t2301928331  get_Rotation_1() const { return ___Rotation_1; }
	inline Quaternion_t2301928331 * get_address_of_Rotation_1() { return &___Rotation_1; }
	inline void set_Rotation_1(Quaternion_t2301928331  value)
	{
		___Rotation_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEPOINT_T3315310755_H
#ifndef POINTERSURFACERESULTENUM_T86002505_H
#define POINTERSURFACERESULTENUM_T86002505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Controllers.PointerSurfaceResultEnum
struct  PointerSurfaceResultEnum_t86002505 
{
public:
	// System.Int32 HoloToolkit.Unity.Controllers.PointerSurfaceResultEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PointerSurfaceResultEnum_t86002505, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERSURFACERESULTENUM_T86002505_H
#ifndef INTERPOLATIONENUM_T2040439712_H
#define INTERPOLATIONENUM_T2040439712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.LineUtils/InterpolationEnum
struct  InterpolationEnum_t2040439712 
{
public:
	// System.Int32 HoloToolkit.Unity.Design.LineUtils/InterpolationEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InterpolationEnum_t2040439712, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATIONENUM_T2040439712_H
#ifndef LINEUTILS_T4143855974_H
#define LINEUTILS_T4143855974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.LineUtils
struct  LineUtils_t4143855974  : public RuntimeObject
{
public:

public:
};

struct LineUtils_t4143855974_StaticFields
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.Design.LineUtils::DefaultUpVector
	Vector3_t3722313464  ___DefaultUpVector_0;

public:
	inline static int32_t get_offset_of_DefaultUpVector_0() { return static_cast<int32_t>(offsetof(LineUtils_t4143855974_StaticFields, ___DefaultUpVector_0)); }
	inline Vector3_t3722313464  get_DefaultUpVector_0() const { return ___DefaultUpVector_0; }
	inline Vector3_t3722313464 * get_address_of_DefaultUpVector_0() { return &___DefaultUpVector_0; }
	inline void set_DefaultUpVector_0(Vector3_t3722313464  value)
	{
		___DefaultUpVector_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEUTILS_T4143855974_H
#ifndef DISPLAYMODEENUM_T2111840609_H
#define DISPLAYMODEENUM_T2111840609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.BrushController/DisplayModeEnum
struct  DisplayModeEnum_t2111840609 
{
public:
	// System.Int32 HoloToolkit.Unity.ControllerExamples.BrushController/DisplayModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DisplayModeEnum_t2111840609, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYMODEENUM_T2111840609_H
#ifndef U3CDRAWOVERTIMEU3ED__31_T814362136_H
#define U3CDRAWOVERTIMEU3ED__31_T814362136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.Brush/<DrawOverTime>d__31
struct  U3CDrawOverTimeU3Ed__31_t814362136  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.ControllerExamples.Brush/<DrawOverTime>d__31::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.ControllerExamples.Brush/<DrawOverTime>d__31::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.ControllerExamples.Brush HoloToolkit.Unity.ControllerExamples.Brush/<DrawOverTime>d__31::<>4__this
	Brush_t1548626474 * ___U3CU3E4__this_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.ControllerExamples.Brush/<DrawOverTime>d__31::<lastPointPosition>5__1
	Vector3_t3722313464  ___U3ClastPointPositionU3E5__1_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.ControllerExamples.Brush/<DrawOverTime>d__31::<startPosition>5__2
	Vector3_t3722313464  ___U3CstartPositionU3E5__2_4;
	// UnityEngine.GameObject HoloToolkit.Unity.ControllerExamples.Brush/<DrawOverTime>d__31::<newStroke>5__3
	GameObject_t1113636619 * ___U3CnewStrokeU3E5__3_5;
	// UnityEngine.LineRenderer HoloToolkit.Unity.ControllerExamples.Brush/<DrawOverTime>d__31::<line>5__4
	LineRenderer_t3154350270 * ___U3ClineU3E5__4_6;
	// System.Single HoloToolkit.Unity.ControllerExamples.Brush/<DrawOverTime>d__31::<initialWidth>5__5
	float ___U3CinitialWidthU3E5__5_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__31_t814362136, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__31_t814362136, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__31_t814362136, ___U3CU3E4__this_2)); }
	inline Brush_t1548626474 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Brush_t1548626474 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Brush_t1548626474 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3ClastPointPositionU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__31_t814362136, ___U3ClastPointPositionU3E5__1_3)); }
	inline Vector3_t3722313464  get_U3ClastPointPositionU3E5__1_3() const { return ___U3ClastPointPositionU3E5__1_3; }
	inline Vector3_t3722313464 * get_address_of_U3ClastPointPositionU3E5__1_3() { return &___U3ClastPointPositionU3E5__1_3; }
	inline void set_U3ClastPointPositionU3E5__1_3(Vector3_t3722313464  value)
	{
		___U3ClastPointPositionU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CstartPositionU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__31_t814362136, ___U3CstartPositionU3E5__2_4)); }
	inline Vector3_t3722313464  get_U3CstartPositionU3E5__2_4() const { return ___U3CstartPositionU3E5__2_4; }
	inline Vector3_t3722313464 * get_address_of_U3CstartPositionU3E5__2_4() { return &___U3CstartPositionU3E5__2_4; }
	inline void set_U3CstartPositionU3E5__2_4(Vector3_t3722313464  value)
	{
		___U3CstartPositionU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CnewStrokeU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__31_t814362136, ___U3CnewStrokeU3E5__3_5)); }
	inline GameObject_t1113636619 * get_U3CnewStrokeU3E5__3_5() const { return ___U3CnewStrokeU3E5__3_5; }
	inline GameObject_t1113636619 ** get_address_of_U3CnewStrokeU3E5__3_5() { return &___U3CnewStrokeU3E5__3_5; }
	inline void set_U3CnewStrokeU3E5__3_5(GameObject_t1113636619 * value)
	{
		___U3CnewStrokeU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnewStrokeU3E5__3_5), value);
	}

	inline static int32_t get_offset_of_U3ClineU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__31_t814362136, ___U3ClineU3E5__4_6)); }
	inline LineRenderer_t3154350270 * get_U3ClineU3E5__4_6() const { return ___U3ClineU3E5__4_6; }
	inline LineRenderer_t3154350270 ** get_address_of_U3ClineU3E5__4_6() { return &___U3ClineU3E5__4_6; }
	inline void set_U3ClineU3E5__4_6(LineRenderer_t3154350270 * value)
	{
		___U3ClineU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClineU3E5__4_6), value);
	}

	inline static int32_t get_offset_of_U3CinitialWidthU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CDrawOverTimeU3Ed__31_t814362136, ___U3CinitialWidthU3E5__5_7)); }
	inline float get_U3CinitialWidthU3E5__5_7() const { return ___U3CinitialWidthU3E5__5_7; }
	inline float* get_address_of_U3CinitialWidthU3E5__5_7() { return &___U3CinitialWidthU3E5__5_7; }
	inline void set_U3CinitialWidthU3E5__5_7(float value)
	{
		___U3CinitialWidthU3E5__5_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDRAWOVERTIMEU3ED__31_T814362136_H
#ifndef ALIGNMENTTYPE_T4238599496_H
#define ALIGNMENTTYPE_T4238599496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Boundary.SceneContentAdjuster/AlignmentType
struct  AlignmentType_t4238599496 
{
public:
	// System.Int32 HoloToolkit.Unity.Boundary.SceneContentAdjuster/AlignmentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AlignmentType_t4238599496, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNMENTTYPE_T4238599496_H
#ifndef DISPLAYMODEENUM_T2177039354_H
#define DISPLAYMODEENUM_T2177039354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.Brush/DisplayModeEnum
struct  DisplayModeEnum_t2177039354 
{
public:
	// System.Int32 HoloToolkit.Unity.ControllerExamples.Brush/DisplayModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DisplayModeEnum_t2177039354, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYMODEENUM_T2177039354_H
#ifndef INTERACTIONSOURCE_T872619030_H
#define INTERACTIONSOURCE_T872619030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.WSA.Input.InteractionSource
struct  InteractionSource_t872619030 
{
public:
	// System.UInt32 UnityEngine.XR.WSA.Input.InteractionSource::m_Id
	uint32_t ___m_Id_0;
	// UnityEngine.XR.WSA.Input.InteractionSourceKind UnityEngine.XR.WSA.Input.InteractionSource::m_SourceKind
	int32_t ___m_SourceKind_1;
	// UnityEngine.XR.WSA.Input.InteractionSourceHandedness UnityEngine.XR.WSA.Input.InteractionSource::m_Handedness
	int32_t ___m_Handedness_2;
	// UnityEngine.XR.WSA.Input.InteractionSourceFlags UnityEngine.XR.WSA.Input.InteractionSource::m_Flags
	int32_t ___m_Flags_3;
	// System.UInt16 UnityEngine.XR.WSA.Input.InteractionSource::m_VendorId
	uint16_t ___m_VendorId_4;
	// System.UInt16 UnityEngine.XR.WSA.Input.InteractionSource::m_ProductId
	uint16_t ___m_ProductId_5;
	// System.UInt16 UnityEngine.XR.WSA.Input.InteractionSource::m_ProductVersion
	uint16_t ___m_ProductVersion_6;

public:
	inline static int32_t get_offset_of_m_Id_0() { return static_cast<int32_t>(offsetof(InteractionSource_t872619030, ___m_Id_0)); }
	inline uint32_t get_m_Id_0() const { return ___m_Id_0; }
	inline uint32_t* get_address_of_m_Id_0() { return &___m_Id_0; }
	inline void set_m_Id_0(uint32_t value)
	{
		___m_Id_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceKind_1() { return static_cast<int32_t>(offsetof(InteractionSource_t872619030, ___m_SourceKind_1)); }
	inline int32_t get_m_SourceKind_1() const { return ___m_SourceKind_1; }
	inline int32_t* get_address_of_m_SourceKind_1() { return &___m_SourceKind_1; }
	inline void set_m_SourceKind_1(int32_t value)
	{
		___m_SourceKind_1 = value;
	}

	inline static int32_t get_offset_of_m_Handedness_2() { return static_cast<int32_t>(offsetof(InteractionSource_t872619030, ___m_Handedness_2)); }
	inline int32_t get_m_Handedness_2() const { return ___m_Handedness_2; }
	inline int32_t* get_address_of_m_Handedness_2() { return &___m_Handedness_2; }
	inline void set_m_Handedness_2(int32_t value)
	{
		___m_Handedness_2 = value;
	}

	inline static int32_t get_offset_of_m_Flags_3() { return static_cast<int32_t>(offsetof(InteractionSource_t872619030, ___m_Flags_3)); }
	inline int32_t get_m_Flags_3() const { return ___m_Flags_3; }
	inline int32_t* get_address_of_m_Flags_3() { return &___m_Flags_3; }
	inline void set_m_Flags_3(int32_t value)
	{
		___m_Flags_3 = value;
	}

	inline static int32_t get_offset_of_m_VendorId_4() { return static_cast<int32_t>(offsetof(InteractionSource_t872619030, ___m_VendorId_4)); }
	inline uint16_t get_m_VendorId_4() const { return ___m_VendorId_4; }
	inline uint16_t* get_address_of_m_VendorId_4() { return &___m_VendorId_4; }
	inline void set_m_VendorId_4(uint16_t value)
	{
		___m_VendorId_4 = value;
	}

	inline static int32_t get_offset_of_m_ProductId_5() { return static_cast<int32_t>(offsetof(InteractionSource_t872619030, ___m_ProductId_5)); }
	inline uint16_t get_m_ProductId_5() const { return ___m_ProductId_5; }
	inline uint16_t* get_address_of_m_ProductId_5() { return &___m_ProductId_5; }
	inline void set_m_ProductId_5(uint16_t value)
	{
		___m_ProductId_5 = value;
	}

	inline static int32_t get_offset_of_m_ProductVersion_6() { return static_cast<int32_t>(offsetof(InteractionSource_t872619030, ___m_ProductVersion_6)); }
	inline uint16_t get_m_ProductVersion_6() const { return ___m_ProductVersion_6; }
	inline uint16_t* get_address_of_m_ProductVersion_6() { return &___m_ProductVersion_6; }
	inline void set_m_ProductVersion_6(uint16_t value)
	{
		___m_ProductVersion_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSOURCE_T872619030_H
#ifndef U3CUPDATEDISPLAYMODEU3ED__32_T268247815_H
#define U3CUPDATEDISPLAYMODEU3ED__32_T268247815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.Brush/<UpdateDisplayMode>d__32
struct  U3CUpdateDisplayModeU3Ed__32_t268247815  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.ControllerExamples.Brush/<UpdateDisplayMode>d__32::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.ControllerExamples.Brush/<UpdateDisplayMode>d__32::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.ControllerExamples.Brush HoloToolkit.Unity.ControllerExamples.Brush/<UpdateDisplayMode>d__32::<>4__this
	Brush_t1548626474 * ___U3CU3E4__this_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.ControllerExamples.Brush/<UpdateDisplayMode>d__32::<targetPosition>5__1
	Vector3_t3722313464  ___U3CtargetPositionU3E5__1_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.ControllerExamples.Brush/<UpdateDisplayMode>d__32::<targetScale>5__2
	Vector3_t3722313464  ___U3CtargetScaleU3E5__2_4;
	// UnityEngine.Quaternion HoloToolkit.Unity.ControllerExamples.Brush/<UpdateDisplayMode>d__32::<targetRotation>5__3
	Quaternion_t2301928331  ___U3CtargetRotationU3E5__3_5;
	// UnityEngine.Vector3 HoloToolkit.Unity.ControllerExamples.Brush/<UpdateDisplayMode>d__32::<startPosition>5__4
	Vector3_t3722313464  ___U3CstartPositionU3E5__4_6;
	// UnityEngine.Vector3 HoloToolkit.Unity.ControllerExamples.Brush/<UpdateDisplayMode>d__32::<startScale>5__5
	Vector3_t3722313464  ___U3CstartScaleU3E5__5_7;
	// UnityEngine.Quaternion HoloToolkit.Unity.ControllerExamples.Brush/<UpdateDisplayMode>d__32::<startRotation>5__6
	Quaternion_t2301928331  ___U3CstartRotationU3E5__6_8;
	// HoloToolkit.Unity.ControllerExamples.Brush/DisplayModeEnum HoloToolkit.Unity.ControllerExamples.Brush/<UpdateDisplayMode>d__32::<lastDisplayMode>5__7
	int32_t ___U3ClastDisplayModeU3E5__7_9;
	// System.Single HoloToolkit.Unity.ControllerExamples.Brush/<UpdateDisplayMode>d__32::<startTime>5__8
	float ___U3CstartTimeU3E5__8_10;
	// System.Single HoloToolkit.Unity.ControllerExamples.Brush/<UpdateDisplayMode>d__32::<normalizedTime>5__9
	float ___U3CnormalizedTimeU3E5__9_11;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateDisplayModeU3Ed__32_t268247815, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateDisplayModeU3Ed__32_t268247815, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateDisplayModeU3Ed__32_t268247815, ___U3CU3E4__this_2)); }
	inline Brush_t1548626474 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Brush_t1548626474 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Brush_t1548626474 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtargetPositionU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CUpdateDisplayModeU3Ed__32_t268247815, ___U3CtargetPositionU3E5__1_3)); }
	inline Vector3_t3722313464  get_U3CtargetPositionU3E5__1_3() const { return ___U3CtargetPositionU3E5__1_3; }
	inline Vector3_t3722313464 * get_address_of_U3CtargetPositionU3E5__1_3() { return &___U3CtargetPositionU3E5__1_3; }
	inline void set_U3CtargetPositionU3E5__1_3(Vector3_t3722313464  value)
	{
		___U3CtargetPositionU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CtargetScaleU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CUpdateDisplayModeU3Ed__32_t268247815, ___U3CtargetScaleU3E5__2_4)); }
	inline Vector3_t3722313464  get_U3CtargetScaleU3E5__2_4() const { return ___U3CtargetScaleU3E5__2_4; }
	inline Vector3_t3722313464 * get_address_of_U3CtargetScaleU3E5__2_4() { return &___U3CtargetScaleU3E5__2_4; }
	inline void set_U3CtargetScaleU3E5__2_4(Vector3_t3722313464  value)
	{
		___U3CtargetScaleU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CtargetRotationU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CUpdateDisplayModeU3Ed__32_t268247815, ___U3CtargetRotationU3E5__3_5)); }
	inline Quaternion_t2301928331  get_U3CtargetRotationU3E5__3_5() const { return ___U3CtargetRotationU3E5__3_5; }
	inline Quaternion_t2301928331 * get_address_of_U3CtargetRotationU3E5__3_5() { return &___U3CtargetRotationU3E5__3_5; }
	inline void set_U3CtargetRotationU3E5__3_5(Quaternion_t2301928331  value)
	{
		___U3CtargetRotationU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CstartPositionU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CUpdateDisplayModeU3Ed__32_t268247815, ___U3CstartPositionU3E5__4_6)); }
	inline Vector3_t3722313464  get_U3CstartPositionU3E5__4_6() const { return ___U3CstartPositionU3E5__4_6; }
	inline Vector3_t3722313464 * get_address_of_U3CstartPositionU3E5__4_6() { return &___U3CstartPositionU3E5__4_6; }
	inline void set_U3CstartPositionU3E5__4_6(Vector3_t3722313464  value)
	{
		___U3CstartPositionU3E5__4_6 = value;
	}

	inline static int32_t get_offset_of_U3CstartScaleU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CUpdateDisplayModeU3Ed__32_t268247815, ___U3CstartScaleU3E5__5_7)); }
	inline Vector3_t3722313464  get_U3CstartScaleU3E5__5_7() const { return ___U3CstartScaleU3E5__5_7; }
	inline Vector3_t3722313464 * get_address_of_U3CstartScaleU3E5__5_7() { return &___U3CstartScaleU3E5__5_7; }
	inline void set_U3CstartScaleU3E5__5_7(Vector3_t3722313464  value)
	{
		___U3CstartScaleU3E5__5_7 = value;
	}

	inline static int32_t get_offset_of_U3CstartRotationU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CUpdateDisplayModeU3Ed__32_t268247815, ___U3CstartRotationU3E5__6_8)); }
	inline Quaternion_t2301928331  get_U3CstartRotationU3E5__6_8() const { return ___U3CstartRotationU3E5__6_8; }
	inline Quaternion_t2301928331 * get_address_of_U3CstartRotationU3E5__6_8() { return &___U3CstartRotationU3E5__6_8; }
	inline void set_U3CstartRotationU3E5__6_8(Quaternion_t2301928331  value)
	{
		___U3CstartRotationU3E5__6_8 = value;
	}

	inline static int32_t get_offset_of_U3ClastDisplayModeU3E5__7_9() { return static_cast<int32_t>(offsetof(U3CUpdateDisplayModeU3Ed__32_t268247815, ___U3ClastDisplayModeU3E5__7_9)); }
	inline int32_t get_U3ClastDisplayModeU3E5__7_9() const { return ___U3ClastDisplayModeU3E5__7_9; }
	inline int32_t* get_address_of_U3ClastDisplayModeU3E5__7_9() { return &___U3ClastDisplayModeU3E5__7_9; }
	inline void set_U3ClastDisplayModeU3E5__7_9(int32_t value)
	{
		___U3ClastDisplayModeU3E5__7_9 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__8_10() { return static_cast<int32_t>(offsetof(U3CUpdateDisplayModeU3Ed__32_t268247815, ___U3CstartTimeU3E5__8_10)); }
	inline float get_U3CstartTimeU3E5__8_10() const { return ___U3CstartTimeU3E5__8_10; }
	inline float* get_address_of_U3CstartTimeU3E5__8_10() { return &___U3CstartTimeU3E5__8_10; }
	inline void set_U3CstartTimeU3E5__8_10(float value)
	{
		___U3CstartTimeU3E5__8_10 = value;
	}

	inline static int32_t get_offset_of_U3CnormalizedTimeU3E5__9_11() { return static_cast<int32_t>(offsetof(U3CUpdateDisplayModeU3Ed__32_t268247815, ___U3CnormalizedTimeU3E5__9_11)); }
	inline float get_U3CnormalizedTimeU3E5__9_11() const { return ___U3CnormalizedTimeU3E5__9_11; }
	inline float* get_address_of_U3CnormalizedTimeU3E5__9_11() { return &___U3CnormalizedTimeU3E5__9_11; }
	inline void set_U3CnormalizedTimeU3E5__9_11(float value)
	{
		___U3CnormalizedTimeU3E5__9_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEDISPLAYMODEU3ED__32_T268247815_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef U3CLOADCONTROLLERMODELU3ED__27_T1961351108_H
#define U3CLOADCONTROLLERMODELU3ED__27_T1961351108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadControllerModel>d__27
struct  U3CLoadControllerModelU3Ed__27_t1961351108  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadControllerModel>d__27::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadControllerModel>d__27::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.XR.WSA.Input.InteractionSource HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadControllerModel>d__27::source
	InteractionSource_t872619030  ___source_2;
	// HoloToolkit.Unity.InputModule.MotionControllerVisualizer HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadControllerModel>d__27::<>4__this
	MotionControllerVisualizer_t392243420 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadControllerModelU3Ed__27_t1961351108, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadControllerModelU3Ed__27_t1961351108, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(U3CLoadControllerModelU3Ed__27_t1961351108, ___source_2)); }
	inline InteractionSource_t872619030  get_source_2() const { return ___source_2; }
	inline InteractionSource_t872619030 * get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(InteractionSource_t872619030  value)
	{
		___source_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadControllerModelU3Ed__27_t1961351108, ___U3CU3E4__this_3)); }
	inline MotionControllerVisualizer_t392243420 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline MotionControllerVisualizer_t392243420 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(MotionControllerVisualizer_t392243420 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCONTROLLERMODELU3ED__27_T1961351108_H
#ifndef U3CLOADSOURCECONTROLLERMODELU3ED__28_T2257991569_H
#define U3CLOADSOURCECONTROLLERMODELU3ED__28_T2257991569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadSourceControllerModel>d__28
struct  U3CLoadSourceControllerModelU3Ed__28_t2257991569  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadSourceControllerModel>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadSourceControllerModel>d__28::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.XR.WSA.Input.InteractionSource HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadSourceControllerModel>d__28::source
	InteractionSource_t872619030  ___source_2;
	// HoloToolkit.Unity.InputModule.MotionControllerVisualizer HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadSourceControllerModel>d__28::<>4__this
	MotionControllerVisualizer_t392243420 * ___U3CU3E4__this_3;
	// System.Byte[] HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadSourceControllerModel>d__28::<fileBytes>5__1
	ByteU5BU5D_t4116647657* ___U3CfileBytesU3E5__1_4;
	// UnityEngine.GameObject HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadSourceControllerModel>d__28::<controllerModelGameObject>5__2
	GameObject_t1113636619 * ___U3CcontrollerModelGameObjectU3E5__2_5;
	// Windows.Foundation.IAsyncOperation`1<Windows.Storage.Streams.IRandomAccessStreamWithContentType> HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadSourceControllerModel>d__28::<modelTask>5__3
	RuntimeObject* ___U3CmodelTaskU3E5__3_6;
	// Windows.Storage.Streams.IRandomAccessStreamWithContentType HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadSourceControllerModel>d__28::<modelStream>5__4
	RuntimeObject* ___U3CmodelStreamU3E5__4_7;
	// UnityGLTF.GLTFComponent HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadSourceControllerModel>d__28::<gltfScript>5__5
	GLTFComponent_t238219215 * ___U3CgltfScriptU3E5__5_8;
	// Windows.Storage.Streams.DataReader HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadSourceControllerModel>d__28::<reader>5__6
	DataReader_t2381813649 * ___U3CreaderU3E5__6_9;
	// Windows.Storage.Streams.DataReaderLoadOperation HoloToolkit.Unity.InputModule.MotionControllerVisualizer/<LoadSourceControllerModel>d__28::<loadModelOp>5__7
	DataReaderLoadOperation_t4248924386 * ___U3CloadModelOpU3E5__7_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadSourceControllerModelU3Ed__28_t2257991569, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadSourceControllerModelU3Ed__28_t2257991569, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(U3CLoadSourceControllerModelU3Ed__28_t2257991569, ___source_2)); }
	inline InteractionSource_t872619030  get_source_2() const { return ___source_2; }
	inline InteractionSource_t872619030 * get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(InteractionSource_t872619030  value)
	{
		___source_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadSourceControllerModelU3Ed__28_t2257991569, ___U3CU3E4__this_3)); }
	inline MotionControllerVisualizer_t392243420 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline MotionControllerVisualizer_t392243420 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(MotionControllerVisualizer_t392243420 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CfileBytesU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CLoadSourceControllerModelU3Ed__28_t2257991569, ___U3CfileBytesU3E5__1_4)); }
	inline ByteU5BU5D_t4116647657* get_U3CfileBytesU3E5__1_4() const { return ___U3CfileBytesU3E5__1_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CfileBytesU3E5__1_4() { return &___U3CfileBytesU3E5__1_4; }
	inline void set_U3CfileBytesU3E5__1_4(ByteU5BU5D_t4116647657* value)
	{
		___U3CfileBytesU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileBytesU3E5__1_4), value);
	}

	inline static int32_t get_offset_of_U3CcontrollerModelGameObjectU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CLoadSourceControllerModelU3Ed__28_t2257991569, ___U3CcontrollerModelGameObjectU3E5__2_5)); }
	inline GameObject_t1113636619 * get_U3CcontrollerModelGameObjectU3E5__2_5() const { return ___U3CcontrollerModelGameObjectU3E5__2_5; }
	inline GameObject_t1113636619 ** get_address_of_U3CcontrollerModelGameObjectU3E5__2_5() { return &___U3CcontrollerModelGameObjectU3E5__2_5; }
	inline void set_U3CcontrollerModelGameObjectU3E5__2_5(GameObject_t1113636619 * value)
	{
		___U3CcontrollerModelGameObjectU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontrollerModelGameObjectU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3CmodelTaskU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CLoadSourceControllerModelU3Ed__28_t2257991569, ___U3CmodelTaskU3E5__3_6)); }
	inline RuntimeObject* get_U3CmodelTaskU3E5__3_6() const { return ___U3CmodelTaskU3E5__3_6; }
	inline RuntimeObject** get_address_of_U3CmodelTaskU3E5__3_6() { return &___U3CmodelTaskU3E5__3_6; }
	inline void set_U3CmodelTaskU3E5__3_6(RuntimeObject* value)
	{
		___U3CmodelTaskU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelTaskU3E5__3_6), value);
	}

	inline static int32_t get_offset_of_U3CmodelStreamU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CLoadSourceControllerModelU3Ed__28_t2257991569, ___U3CmodelStreamU3E5__4_7)); }
	inline RuntimeObject* get_U3CmodelStreamU3E5__4_7() const { return ___U3CmodelStreamU3E5__4_7; }
	inline RuntimeObject** get_address_of_U3CmodelStreamU3E5__4_7() { return &___U3CmodelStreamU3E5__4_7; }
	inline void set_U3CmodelStreamU3E5__4_7(RuntimeObject* value)
	{
		___U3CmodelStreamU3E5__4_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelStreamU3E5__4_7), value);
	}

	inline static int32_t get_offset_of_U3CgltfScriptU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CLoadSourceControllerModelU3Ed__28_t2257991569, ___U3CgltfScriptU3E5__5_8)); }
	inline GLTFComponent_t238219215 * get_U3CgltfScriptU3E5__5_8() const { return ___U3CgltfScriptU3E5__5_8; }
	inline GLTFComponent_t238219215 ** get_address_of_U3CgltfScriptU3E5__5_8() { return &___U3CgltfScriptU3E5__5_8; }
	inline void set_U3CgltfScriptU3E5__5_8(GLTFComponent_t238219215 * value)
	{
		___U3CgltfScriptU3E5__5_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgltfScriptU3E5__5_8), value);
	}

	inline static int32_t get_offset_of_U3CreaderU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CLoadSourceControllerModelU3Ed__28_t2257991569, ___U3CreaderU3E5__6_9)); }
	inline DataReader_t2381813649 * get_U3CreaderU3E5__6_9() const { return ___U3CreaderU3E5__6_9; }
	inline DataReader_t2381813649 ** get_address_of_U3CreaderU3E5__6_9() { return &___U3CreaderU3E5__6_9; }
	inline void set_U3CreaderU3E5__6_9(DataReader_t2381813649 * value)
	{
		___U3CreaderU3E5__6_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreaderU3E5__6_9), value);
	}

	inline static int32_t get_offset_of_U3CloadModelOpU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CLoadSourceControllerModelU3Ed__28_t2257991569, ___U3CloadModelOpU3E5__7_10)); }
	inline DataReaderLoadOperation_t4248924386 * get_U3CloadModelOpU3E5__7_10() const { return ___U3CloadModelOpU3E5__7_10; }
	inline DataReaderLoadOperation_t4248924386 ** get_address_of_U3CloadModelOpU3E5__7_10() { return &___U3CloadModelOpU3E5__7_10; }
	inline void set_U3CloadModelOpU3E5__7_10(DataReaderLoadOperation_t4248924386 * value)
	{
		___U3CloadModelOpU3E5__7_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadModelOpU3E5__7_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSOURCECONTROLLERMODELU3ED__28_T2257991569_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DISTORTER_T3335802222_H
#define DISTORTER_T3335802222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.Distorter
struct  Distorter_t3335802222  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 HoloToolkit.Unity.Design.Distorter::distortOrder
	int32_t ___distortOrder_2;

public:
	inline static int32_t get_offset_of_distortOrder_2() { return static_cast<int32_t>(offsetof(Distorter_t3335802222, ___distortOrder_2)); }
	inline int32_t get_distortOrder_2() const { return ___distortOrder_2; }
	inline int32_t* get_address_of_distortOrder_2() { return &___distortOrder_2; }
	inline void set_distortOrder_2(int32_t value)
	{
		___distortOrder_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTER_T3335802222_H
#ifndef NAVIGATIONHOTSPOT_T405511513_H
#define NAVIGATIONHOTSPOT_T405511513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Controllers.NavigationHotSpot
struct  NavigationHotSpot_t405511513  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVIGATIONHOTSPOT_T405511513_H
#ifndef SINGLETON_1_T792467874_H
#define SINGLETON_1_T792467874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.InputModule.MotionControllerVisualizer>
struct  Singleton_1_t792467874  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t792467874_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	MotionControllerVisualizer_t392243420 * ___instance_2;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t792467874_StaticFields, ___instance_2)); }
	inline MotionControllerVisualizer_t392243420 * get_instance_2() const { return ___instance_2; }
	inline MotionControllerVisualizer_t392243420 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(MotionControllerVisualizer_t392243420 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_searchForInstance_3() { return static_cast<int32_t>(offsetof(Singleton_1_t792467874_StaticFields, ___searchForInstance_3)); }
	inline bool get_searchForInstance_3() const { return ___searchForInstance_3; }
	inline bool* get_address_of_searchForInstance_3() { return &___searchForInstance_3; }
	inline void set_searchForInstance_3(bool value)
	{
		___searchForInstance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T792467874_H
#ifndef SCENECONTENTADJUSTER_T1258964661_H
#define SCENECONTENTADJUSTER_T1258964661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Boundary.SceneContentAdjuster
struct  SceneContentAdjuster_t1258964661  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform HoloToolkit.Unity.Boundary.SceneContentAdjuster::containerObject
	Transform_t3600365921 * ___containerObject_2;
	// HoloToolkit.Unity.Boundary.SceneContentAdjuster/AlignmentType HoloToolkit.Unity.Boundary.SceneContentAdjuster::alignmentType
	int32_t ___alignmentType_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.Boundary.SceneContentAdjuster::stationarySpaceTypePosition
	Vector3_t3722313464  ___stationarySpaceTypePosition_4;
	// UnityEngine.Vector3 HoloToolkit.Unity.Boundary.SceneContentAdjuster::roomScaleSpaceTypePosition
	Vector3_t3722313464  ___roomScaleSpaceTypePosition_5;
	// UnityEngine.Vector3 HoloToolkit.Unity.Boundary.SceneContentAdjuster::contentPosition
	Vector3_t3722313464  ___contentPosition_6;
	// System.Int32 HoloToolkit.Unity.Boundary.SceneContentAdjuster::frameWaitHack
	int32_t ___frameWaitHack_7;

public:
	inline static int32_t get_offset_of_containerObject_2() { return static_cast<int32_t>(offsetof(SceneContentAdjuster_t1258964661, ___containerObject_2)); }
	inline Transform_t3600365921 * get_containerObject_2() const { return ___containerObject_2; }
	inline Transform_t3600365921 ** get_address_of_containerObject_2() { return &___containerObject_2; }
	inline void set_containerObject_2(Transform_t3600365921 * value)
	{
		___containerObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___containerObject_2), value);
	}

	inline static int32_t get_offset_of_alignmentType_3() { return static_cast<int32_t>(offsetof(SceneContentAdjuster_t1258964661, ___alignmentType_3)); }
	inline int32_t get_alignmentType_3() const { return ___alignmentType_3; }
	inline int32_t* get_address_of_alignmentType_3() { return &___alignmentType_3; }
	inline void set_alignmentType_3(int32_t value)
	{
		___alignmentType_3 = value;
	}

	inline static int32_t get_offset_of_stationarySpaceTypePosition_4() { return static_cast<int32_t>(offsetof(SceneContentAdjuster_t1258964661, ___stationarySpaceTypePosition_4)); }
	inline Vector3_t3722313464  get_stationarySpaceTypePosition_4() const { return ___stationarySpaceTypePosition_4; }
	inline Vector3_t3722313464 * get_address_of_stationarySpaceTypePosition_4() { return &___stationarySpaceTypePosition_4; }
	inline void set_stationarySpaceTypePosition_4(Vector3_t3722313464  value)
	{
		___stationarySpaceTypePosition_4 = value;
	}

	inline static int32_t get_offset_of_roomScaleSpaceTypePosition_5() { return static_cast<int32_t>(offsetof(SceneContentAdjuster_t1258964661, ___roomScaleSpaceTypePosition_5)); }
	inline Vector3_t3722313464  get_roomScaleSpaceTypePosition_5() const { return ___roomScaleSpaceTypePosition_5; }
	inline Vector3_t3722313464 * get_address_of_roomScaleSpaceTypePosition_5() { return &___roomScaleSpaceTypePosition_5; }
	inline void set_roomScaleSpaceTypePosition_5(Vector3_t3722313464  value)
	{
		___roomScaleSpaceTypePosition_5 = value;
	}

	inline static int32_t get_offset_of_contentPosition_6() { return static_cast<int32_t>(offsetof(SceneContentAdjuster_t1258964661, ___contentPosition_6)); }
	inline Vector3_t3722313464  get_contentPosition_6() const { return ___contentPosition_6; }
	inline Vector3_t3722313464 * get_address_of_contentPosition_6() { return &___contentPosition_6; }
	inline void set_contentPosition_6(Vector3_t3722313464  value)
	{
		___contentPosition_6 = value;
	}

	inline static int32_t get_offset_of_frameWaitHack_7() { return static_cast<int32_t>(offsetof(SceneContentAdjuster_t1258964661, ___frameWaitHack_7)); }
	inline int32_t get_frameWaitHack_7() const { return ___frameWaitHack_7; }
	inline int32_t* get_address_of_frameWaitHack_7() { return &___frameWaitHack_7; }
	inline void set_frameWaitHack_7(int32_t value)
	{
		___frameWaitHack_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENECONTENTADJUSTER_T1258964661_H
#ifndef SETGLOBALLISTENER_T2628677908_H
#define SETGLOBALLISTENER_T2628677908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.SetGlobalListener
struct  SetGlobalListener_t2628677908  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGLOBALLISTENER_T2628677908_H
#ifndef POINTERCURSOR_T2821356970_H
#define POINTERCURSOR_T2821356970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Controllers.PointerCursor
struct  PointerCursor_t2821356970  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Unity.Controllers.PhysicsPointer HoloToolkit.Unity.Controllers.PointerCursor::pointer
	PhysicsPointer_t2887364969 * ___pointer_2;
	// System.Single HoloToolkit.Unity.Controllers.PointerCursor::sizeOnScreen
	float ___sizeOnScreen_3;
	// System.Single HoloToolkit.Unity.Controllers.PointerCursor::scaleAdjustTime
	float ___scaleAdjustTime_4;
	// UnityEngine.Renderer[] HoloToolkit.Unity.Controllers.PointerCursor::renderers
	RendererU5BU5D_t3210418286* ___renderers_5;
	// UnityEngine.Transform HoloToolkit.Unity.Controllers.PointerCursor::pointerTransform
	Transform_t3600365921 * ___pointerTransform_6;

public:
	inline static int32_t get_offset_of_pointer_2() { return static_cast<int32_t>(offsetof(PointerCursor_t2821356970, ___pointer_2)); }
	inline PhysicsPointer_t2887364969 * get_pointer_2() const { return ___pointer_2; }
	inline PhysicsPointer_t2887364969 ** get_address_of_pointer_2() { return &___pointer_2; }
	inline void set_pointer_2(PhysicsPointer_t2887364969 * value)
	{
		___pointer_2 = value;
		Il2CppCodeGenWriteBarrier((&___pointer_2), value);
	}

	inline static int32_t get_offset_of_sizeOnScreen_3() { return static_cast<int32_t>(offsetof(PointerCursor_t2821356970, ___sizeOnScreen_3)); }
	inline float get_sizeOnScreen_3() const { return ___sizeOnScreen_3; }
	inline float* get_address_of_sizeOnScreen_3() { return &___sizeOnScreen_3; }
	inline void set_sizeOnScreen_3(float value)
	{
		___sizeOnScreen_3 = value;
	}

	inline static int32_t get_offset_of_scaleAdjustTime_4() { return static_cast<int32_t>(offsetof(PointerCursor_t2821356970, ___scaleAdjustTime_4)); }
	inline float get_scaleAdjustTime_4() const { return ___scaleAdjustTime_4; }
	inline float* get_address_of_scaleAdjustTime_4() { return &___scaleAdjustTime_4; }
	inline void set_scaleAdjustTime_4(float value)
	{
		___scaleAdjustTime_4 = value;
	}

	inline static int32_t get_offset_of_renderers_5() { return static_cast<int32_t>(offsetof(PointerCursor_t2821356970, ___renderers_5)); }
	inline RendererU5BU5D_t3210418286* get_renderers_5() const { return ___renderers_5; }
	inline RendererU5BU5D_t3210418286** get_address_of_renderers_5() { return &___renderers_5; }
	inline void set_renderers_5(RendererU5BU5D_t3210418286* value)
	{
		___renderers_5 = value;
		Il2CppCodeGenWriteBarrier((&___renderers_5), value);
	}

	inline static int32_t get_offset_of_pointerTransform_6() { return static_cast<int32_t>(offsetof(PointerCursor_t2821356970, ___pointerTransform_6)); }
	inline Transform_t3600365921 * get_pointerTransform_6() const { return ___pointerTransform_6; }
	inline Transform_t3600365921 ** get_address_of_pointerTransform_6() { return &___pointerTransform_6; }
	inline void set_pointerTransform_6(Transform_t3600365921 * value)
	{
		___pointerTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___pointerTransform_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERCURSOR_T2821356970_H
#ifndef PHYSICSPOINTER_T2887364969_H
#define PHYSICSPOINTER_T2887364969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Controllers.PhysicsPointer
struct  PhysicsPointer_t2887364969  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HoloToolkit.Unity.Controllers.PhysicsPointer::<TotalLength>k__BackingField
	float ___U3CTotalLengthU3Ek__BackingField_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.Controllers.PhysicsPointer::<StartPoint>k__BackingField
	Vector3_t3722313464  ___U3CStartPointU3Ek__BackingField_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.Controllers.PhysicsPointer::<TargetPoint>k__BackingField
	Vector3_t3722313464  ___U3CTargetPointU3Ek__BackingField_4;
	// UnityEngine.Vector3 HoloToolkit.Unity.Controllers.PhysicsPointer::<StartPointNormal>k__BackingField
	Vector3_t3722313464  ___U3CStartPointNormalU3Ek__BackingField_5;
	// UnityEngine.Vector3 HoloToolkit.Unity.Controllers.PhysicsPointer::<TargetPointNormal>k__BackingField
	Vector3_t3722313464  ___U3CTargetPointNormalU3Ek__BackingField_6;
	// UnityEngine.Vector3 HoloToolkit.Unity.Controllers.PhysicsPointer::<PointerForward>k__BackingField
	Vector3_t3722313464  ___U3CPointerForwardU3Ek__BackingField_7;
	// HoloToolkit.Unity.Controllers.PointerSurfaceResultEnum HoloToolkit.Unity.Controllers.PhysicsPointer::<TargetResult>k__BackingField
	int32_t ___U3CTargetResultU3Ek__BackingField_8;
	// System.Single HoloToolkit.Unity.Controllers.PhysicsPointer::<TargetPointOrientation>k__BackingField
	float ___U3CTargetPointOrientationU3Ek__BackingField_9;
	// UnityEngine.Gradient HoloToolkit.Unity.Controllers.PhysicsPointer::lineColorValid
	Gradient_t3067099924 * ___lineColorValid_10;
	// UnityEngine.Gradient HoloToolkit.Unity.Controllers.PhysicsPointer::lineColorInvalid
	Gradient_t3067099924 * ___lineColorInvalid_11;
	// UnityEngine.Gradient HoloToolkit.Unity.Controllers.PhysicsPointer::lineColorHotSpot
	Gradient_t3067099924 * ___lineColorHotSpot_12;
	// UnityEngine.Gradient HoloToolkit.Unity.Controllers.PhysicsPointer::lineColorNoTarget
	Gradient_t3067099924 * ___lineColorNoTarget_13;
	// UnityEngine.LayerMask HoloToolkit.Unity.Controllers.PhysicsPointer::validLayers
	LayerMask_t3493934918  ___validLayers_14;
	// UnityEngine.LayerMask HoloToolkit.Unity.Controllers.PhysicsPointer::invalidLayers
	LayerMask_t3493934918  ___invalidLayers_15;
	// System.Boolean HoloToolkit.Unity.Controllers.PhysicsPointer::detectTriggers
	bool ___detectTriggers_16;
	// System.Boolean HoloToolkit.Unity.Controllers.PhysicsPointer::active
	bool ___active_17;
	// UnityEngine.Transform HoloToolkit.Unity.Controllers.PhysicsPointer::raycastOrigin
	Transform_t3600365921 * ___raycastOrigin_18;
	// UnityEngine.RaycastHit HoloToolkit.Unity.Controllers.PhysicsPointer::targetHit
	RaycastHit_t1056001966  ___targetHit_19;

public:
	inline static int32_t get_offset_of_U3CTotalLengthU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___U3CTotalLengthU3Ek__BackingField_2)); }
	inline float get_U3CTotalLengthU3Ek__BackingField_2() const { return ___U3CTotalLengthU3Ek__BackingField_2; }
	inline float* get_address_of_U3CTotalLengthU3Ek__BackingField_2() { return &___U3CTotalLengthU3Ek__BackingField_2; }
	inline void set_U3CTotalLengthU3Ek__BackingField_2(float value)
	{
		___U3CTotalLengthU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CStartPointU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___U3CStartPointU3Ek__BackingField_3)); }
	inline Vector3_t3722313464  get_U3CStartPointU3Ek__BackingField_3() const { return ___U3CStartPointU3Ek__BackingField_3; }
	inline Vector3_t3722313464 * get_address_of_U3CStartPointU3Ek__BackingField_3() { return &___U3CStartPointU3Ek__BackingField_3; }
	inline void set_U3CStartPointU3Ek__BackingField_3(Vector3_t3722313464  value)
	{
		___U3CStartPointU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CTargetPointU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___U3CTargetPointU3Ek__BackingField_4)); }
	inline Vector3_t3722313464  get_U3CTargetPointU3Ek__BackingField_4() const { return ___U3CTargetPointU3Ek__BackingField_4; }
	inline Vector3_t3722313464 * get_address_of_U3CTargetPointU3Ek__BackingField_4() { return &___U3CTargetPointU3Ek__BackingField_4; }
	inline void set_U3CTargetPointU3Ek__BackingField_4(Vector3_t3722313464  value)
	{
		___U3CTargetPointU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CStartPointNormalU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___U3CStartPointNormalU3Ek__BackingField_5)); }
	inline Vector3_t3722313464  get_U3CStartPointNormalU3Ek__BackingField_5() const { return ___U3CStartPointNormalU3Ek__BackingField_5; }
	inline Vector3_t3722313464 * get_address_of_U3CStartPointNormalU3Ek__BackingField_5() { return &___U3CStartPointNormalU3Ek__BackingField_5; }
	inline void set_U3CStartPointNormalU3Ek__BackingField_5(Vector3_t3722313464  value)
	{
		___U3CStartPointNormalU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CTargetPointNormalU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___U3CTargetPointNormalU3Ek__BackingField_6)); }
	inline Vector3_t3722313464  get_U3CTargetPointNormalU3Ek__BackingField_6() const { return ___U3CTargetPointNormalU3Ek__BackingField_6; }
	inline Vector3_t3722313464 * get_address_of_U3CTargetPointNormalU3Ek__BackingField_6() { return &___U3CTargetPointNormalU3Ek__BackingField_6; }
	inline void set_U3CTargetPointNormalU3Ek__BackingField_6(Vector3_t3722313464  value)
	{
		___U3CTargetPointNormalU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CPointerForwardU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___U3CPointerForwardU3Ek__BackingField_7)); }
	inline Vector3_t3722313464  get_U3CPointerForwardU3Ek__BackingField_7() const { return ___U3CPointerForwardU3Ek__BackingField_7; }
	inline Vector3_t3722313464 * get_address_of_U3CPointerForwardU3Ek__BackingField_7() { return &___U3CPointerForwardU3Ek__BackingField_7; }
	inline void set_U3CPointerForwardU3Ek__BackingField_7(Vector3_t3722313464  value)
	{
		___U3CPointerForwardU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CTargetResultU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___U3CTargetResultU3Ek__BackingField_8)); }
	inline int32_t get_U3CTargetResultU3Ek__BackingField_8() const { return ___U3CTargetResultU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CTargetResultU3Ek__BackingField_8() { return &___U3CTargetResultU3Ek__BackingField_8; }
	inline void set_U3CTargetResultU3Ek__BackingField_8(int32_t value)
	{
		___U3CTargetResultU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CTargetPointOrientationU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___U3CTargetPointOrientationU3Ek__BackingField_9)); }
	inline float get_U3CTargetPointOrientationU3Ek__BackingField_9() const { return ___U3CTargetPointOrientationU3Ek__BackingField_9; }
	inline float* get_address_of_U3CTargetPointOrientationU3Ek__BackingField_9() { return &___U3CTargetPointOrientationU3Ek__BackingField_9; }
	inline void set_U3CTargetPointOrientationU3Ek__BackingField_9(float value)
	{
		___U3CTargetPointOrientationU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_lineColorValid_10() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___lineColorValid_10)); }
	inline Gradient_t3067099924 * get_lineColorValid_10() const { return ___lineColorValid_10; }
	inline Gradient_t3067099924 ** get_address_of_lineColorValid_10() { return &___lineColorValid_10; }
	inline void set_lineColorValid_10(Gradient_t3067099924 * value)
	{
		___lineColorValid_10 = value;
		Il2CppCodeGenWriteBarrier((&___lineColorValid_10), value);
	}

	inline static int32_t get_offset_of_lineColorInvalid_11() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___lineColorInvalid_11)); }
	inline Gradient_t3067099924 * get_lineColorInvalid_11() const { return ___lineColorInvalid_11; }
	inline Gradient_t3067099924 ** get_address_of_lineColorInvalid_11() { return &___lineColorInvalid_11; }
	inline void set_lineColorInvalid_11(Gradient_t3067099924 * value)
	{
		___lineColorInvalid_11 = value;
		Il2CppCodeGenWriteBarrier((&___lineColorInvalid_11), value);
	}

	inline static int32_t get_offset_of_lineColorHotSpot_12() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___lineColorHotSpot_12)); }
	inline Gradient_t3067099924 * get_lineColorHotSpot_12() const { return ___lineColorHotSpot_12; }
	inline Gradient_t3067099924 ** get_address_of_lineColorHotSpot_12() { return &___lineColorHotSpot_12; }
	inline void set_lineColorHotSpot_12(Gradient_t3067099924 * value)
	{
		___lineColorHotSpot_12 = value;
		Il2CppCodeGenWriteBarrier((&___lineColorHotSpot_12), value);
	}

	inline static int32_t get_offset_of_lineColorNoTarget_13() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___lineColorNoTarget_13)); }
	inline Gradient_t3067099924 * get_lineColorNoTarget_13() const { return ___lineColorNoTarget_13; }
	inline Gradient_t3067099924 ** get_address_of_lineColorNoTarget_13() { return &___lineColorNoTarget_13; }
	inline void set_lineColorNoTarget_13(Gradient_t3067099924 * value)
	{
		___lineColorNoTarget_13 = value;
		Il2CppCodeGenWriteBarrier((&___lineColorNoTarget_13), value);
	}

	inline static int32_t get_offset_of_validLayers_14() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___validLayers_14)); }
	inline LayerMask_t3493934918  get_validLayers_14() const { return ___validLayers_14; }
	inline LayerMask_t3493934918 * get_address_of_validLayers_14() { return &___validLayers_14; }
	inline void set_validLayers_14(LayerMask_t3493934918  value)
	{
		___validLayers_14 = value;
	}

	inline static int32_t get_offset_of_invalidLayers_15() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___invalidLayers_15)); }
	inline LayerMask_t3493934918  get_invalidLayers_15() const { return ___invalidLayers_15; }
	inline LayerMask_t3493934918 * get_address_of_invalidLayers_15() { return &___invalidLayers_15; }
	inline void set_invalidLayers_15(LayerMask_t3493934918  value)
	{
		___invalidLayers_15 = value;
	}

	inline static int32_t get_offset_of_detectTriggers_16() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___detectTriggers_16)); }
	inline bool get_detectTriggers_16() const { return ___detectTriggers_16; }
	inline bool* get_address_of_detectTriggers_16() { return &___detectTriggers_16; }
	inline void set_detectTriggers_16(bool value)
	{
		___detectTriggers_16 = value;
	}

	inline static int32_t get_offset_of_active_17() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___active_17)); }
	inline bool get_active_17() const { return ___active_17; }
	inline bool* get_address_of_active_17() { return &___active_17; }
	inline void set_active_17(bool value)
	{
		___active_17 = value;
	}

	inline static int32_t get_offset_of_raycastOrigin_18() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___raycastOrigin_18)); }
	inline Transform_t3600365921 * get_raycastOrigin_18() const { return ___raycastOrigin_18; }
	inline Transform_t3600365921 ** get_address_of_raycastOrigin_18() { return &___raycastOrigin_18; }
	inline void set_raycastOrigin_18(Transform_t3600365921 * value)
	{
		___raycastOrigin_18 = value;
		Il2CppCodeGenWriteBarrier((&___raycastOrigin_18), value);
	}

	inline static int32_t get_offset_of_targetHit_19() { return static_cast<int32_t>(offsetof(PhysicsPointer_t2887364969, ___targetHit_19)); }
	inline RaycastHit_t1056001966  get_targetHit_19() const { return ___targetHit_19; }
	inline RaycastHit_t1056001966 * get_address_of_targetHit_19() { return &___targetHit_19; }
	inline void set_targetHit_19(RaycastHit_t1056001966  value)
	{
		___targetHit_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICSPOINTER_T2887364969_H
#ifndef CONTROLLERFINDER_T3151550888_H
#define CONTROLLERFINDER_T3151550888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.ControllerFinder
struct  ControllerFinder_t3151550888  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Unity.InputModule.MotionControllerInfo/ControllerElementEnum HoloToolkit.Unity.InputModule.ControllerFinder::element
	int32_t ___element_2;
	// UnityEngine.XR.WSA.Input.InteractionSourceHandedness HoloToolkit.Unity.InputModule.ControllerFinder::handedness
	int32_t ___handedness_3;
	// UnityEngine.Transform HoloToolkit.Unity.InputModule.ControllerFinder::elementTransform
	Transform_t3600365921 * ___elementTransform_4;
	// HoloToolkit.Unity.InputModule.MotionControllerInfo HoloToolkit.Unity.InputModule.ControllerFinder::ControllerInfo
	MotionControllerInfo_t938152223 * ___ControllerInfo_5;

public:
	inline static int32_t get_offset_of_element_2() { return static_cast<int32_t>(offsetof(ControllerFinder_t3151550888, ___element_2)); }
	inline int32_t get_element_2() const { return ___element_2; }
	inline int32_t* get_address_of_element_2() { return &___element_2; }
	inline void set_element_2(int32_t value)
	{
		___element_2 = value;
	}

	inline static int32_t get_offset_of_handedness_3() { return static_cast<int32_t>(offsetof(ControllerFinder_t3151550888, ___handedness_3)); }
	inline int32_t get_handedness_3() const { return ___handedness_3; }
	inline int32_t* get_address_of_handedness_3() { return &___handedness_3; }
	inline void set_handedness_3(int32_t value)
	{
		___handedness_3 = value;
	}

	inline static int32_t get_offset_of_elementTransform_4() { return static_cast<int32_t>(offsetof(ControllerFinder_t3151550888, ___elementTransform_4)); }
	inline Transform_t3600365921 * get_elementTransform_4() const { return ___elementTransform_4; }
	inline Transform_t3600365921 ** get_address_of_elementTransform_4() { return &___elementTransform_4; }
	inline void set_elementTransform_4(Transform_t3600365921 * value)
	{
		___elementTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___elementTransform_4), value);
	}

	inline static int32_t get_offset_of_ControllerInfo_5() { return static_cast<int32_t>(offsetof(ControllerFinder_t3151550888, ___ControllerInfo_5)); }
	inline MotionControllerInfo_t938152223 * get_ControllerInfo_5() const { return ___ControllerInfo_5; }
	inline MotionControllerInfo_t938152223 ** get_address_of_ControllerInfo_5() { return &___ControllerInfo_5; }
	inline void set_ControllerInfo_5(MotionControllerInfo_t938152223 * value)
	{
		___ControllerInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___ControllerInfo_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERFINDER_T3151550888_H
#ifndef BRUSH_T1548626474_H
#define BRUSH_T1548626474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.Brush
struct  Brush_t1548626474  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HoloToolkit.Unity.ControllerExamples.Brush::minColorDelta
	float ___minColorDelta_2;
	// System.Single HoloToolkit.Unity.ControllerExamples.Brush::minPositionDelta
	float ___minPositionDelta_3;
	// System.Single HoloToolkit.Unity.ControllerExamples.Brush::maxTimeDelta
	float ___maxTimeDelta_4;
	// UnityEngine.Transform HoloToolkit.Unity.ControllerExamples.Brush::tip
	Transform_t3600365921 * ___tip_5;
	// UnityEngine.GameObject HoloToolkit.Unity.ControllerExamples.Brush::strokePrefab
	GameObject_t1113636619 * ___strokePrefab_6;
	// UnityEngine.Transform HoloToolkit.Unity.ControllerExamples.Brush::brushObjectTransform
	Transform_t3600365921 * ___brushObjectTransform_7;
	// UnityEngine.Renderer HoloToolkit.Unity.ControllerExamples.Brush::brushRenderer
	Renderer_t2627027031 * ___brushRenderer_8;
	// System.Boolean HoloToolkit.Unity.ControllerExamples.Brush::draw
	bool ___draw_9;
	// System.Single HoloToolkit.Unity.ControllerExamples.Brush::width
	float ___width_10;
	// UnityEngine.Vector3 HoloToolkit.Unity.ControllerExamples.Brush::inMenuPosition
	Vector3_t3722313464  ___inMenuPosition_11;
	// UnityEngine.Vector3 HoloToolkit.Unity.ControllerExamples.Brush::inMenuRotation
	Vector3_t3722313464  ___inMenuRotation_12;
	// UnityEngine.Vector3 HoloToolkit.Unity.ControllerExamples.Brush::inHandPosition
	Vector3_t3722313464  ___inHandPosition_13;
	// UnityEngine.Vector3 HoloToolkit.Unity.ControllerExamples.Brush::inHandRotation
	Vector3_t3722313464  ___inHandRotation_14;
	// HoloToolkit.Unity.ControllerExamples.Brush/DisplayModeEnum HoloToolkit.Unity.ControllerExamples.Brush::displayMode
	int32_t ___displayMode_15;
	// System.Single HoloToolkit.Unity.ControllerExamples.Brush::transitionDuration
	float ___transitionDuration_16;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.ControllerExamples.Brush::transitionCurve
	AnimationCurve_t3046754366 * ___transitionCurve_17;
	// UnityEngine.Color HoloToolkit.Unity.ControllerExamples.Brush::currentStrokeColor
	Color_t2555686324  ___currentStrokeColor_18;
	// System.Single HoloToolkit.Unity.ControllerExamples.Brush::lastPointAddedTime
	float ___lastPointAddedTime_19;

public:
	inline static int32_t get_offset_of_minColorDelta_2() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___minColorDelta_2)); }
	inline float get_minColorDelta_2() const { return ___minColorDelta_2; }
	inline float* get_address_of_minColorDelta_2() { return &___minColorDelta_2; }
	inline void set_minColorDelta_2(float value)
	{
		___minColorDelta_2 = value;
	}

	inline static int32_t get_offset_of_minPositionDelta_3() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___minPositionDelta_3)); }
	inline float get_minPositionDelta_3() const { return ___minPositionDelta_3; }
	inline float* get_address_of_minPositionDelta_3() { return &___minPositionDelta_3; }
	inline void set_minPositionDelta_3(float value)
	{
		___minPositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_maxTimeDelta_4() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___maxTimeDelta_4)); }
	inline float get_maxTimeDelta_4() const { return ___maxTimeDelta_4; }
	inline float* get_address_of_maxTimeDelta_4() { return &___maxTimeDelta_4; }
	inline void set_maxTimeDelta_4(float value)
	{
		___maxTimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_tip_5() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___tip_5)); }
	inline Transform_t3600365921 * get_tip_5() const { return ___tip_5; }
	inline Transform_t3600365921 ** get_address_of_tip_5() { return &___tip_5; }
	inline void set_tip_5(Transform_t3600365921 * value)
	{
		___tip_5 = value;
		Il2CppCodeGenWriteBarrier((&___tip_5), value);
	}

	inline static int32_t get_offset_of_strokePrefab_6() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___strokePrefab_6)); }
	inline GameObject_t1113636619 * get_strokePrefab_6() const { return ___strokePrefab_6; }
	inline GameObject_t1113636619 ** get_address_of_strokePrefab_6() { return &___strokePrefab_6; }
	inline void set_strokePrefab_6(GameObject_t1113636619 * value)
	{
		___strokePrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___strokePrefab_6), value);
	}

	inline static int32_t get_offset_of_brushObjectTransform_7() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___brushObjectTransform_7)); }
	inline Transform_t3600365921 * get_brushObjectTransform_7() const { return ___brushObjectTransform_7; }
	inline Transform_t3600365921 ** get_address_of_brushObjectTransform_7() { return &___brushObjectTransform_7; }
	inline void set_brushObjectTransform_7(Transform_t3600365921 * value)
	{
		___brushObjectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___brushObjectTransform_7), value);
	}

	inline static int32_t get_offset_of_brushRenderer_8() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___brushRenderer_8)); }
	inline Renderer_t2627027031 * get_brushRenderer_8() const { return ___brushRenderer_8; }
	inline Renderer_t2627027031 ** get_address_of_brushRenderer_8() { return &___brushRenderer_8; }
	inline void set_brushRenderer_8(Renderer_t2627027031 * value)
	{
		___brushRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___brushRenderer_8), value);
	}

	inline static int32_t get_offset_of_draw_9() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___draw_9)); }
	inline bool get_draw_9() const { return ___draw_9; }
	inline bool* get_address_of_draw_9() { return &___draw_9; }
	inline void set_draw_9(bool value)
	{
		___draw_9 = value;
	}

	inline static int32_t get_offset_of_width_10() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___width_10)); }
	inline float get_width_10() const { return ___width_10; }
	inline float* get_address_of_width_10() { return &___width_10; }
	inline void set_width_10(float value)
	{
		___width_10 = value;
	}

	inline static int32_t get_offset_of_inMenuPosition_11() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___inMenuPosition_11)); }
	inline Vector3_t3722313464  get_inMenuPosition_11() const { return ___inMenuPosition_11; }
	inline Vector3_t3722313464 * get_address_of_inMenuPosition_11() { return &___inMenuPosition_11; }
	inline void set_inMenuPosition_11(Vector3_t3722313464  value)
	{
		___inMenuPosition_11 = value;
	}

	inline static int32_t get_offset_of_inMenuRotation_12() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___inMenuRotation_12)); }
	inline Vector3_t3722313464  get_inMenuRotation_12() const { return ___inMenuRotation_12; }
	inline Vector3_t3722313464 * get_address_of_inMenuRotation_12() { return &___inMenuRotation_12; }
	inline void set_inMenuRotation_12(Vector3_t3722313464  value)
	{
		___inMenuRotation_12 = value;
	}

	inline static int32_t get_offset_of_inHandPosition_13() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___inHandPosition_13)); }
	inline Vector3_t3722313464  get_inHandPosition_13() const { return ___inHandPosition_13; }
	inline Vector3_t3722313464 * get_address_of_inHandPosition_13() { return &___inHandPosition_13; }
	inline void set_inHandPosition_13(Vector3_t3722313464  value)
	{
		___inHandPosition_13 = value;
	}

	inline static int32_t get_offset_of_inHandRotation_14() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___inHandRotation_14)); }
	inline Vector3_t3722313464  get_inHandRotation_14() const { return ___inHandRotation_14; }
	inline Vector3_t3722313464 * get_address_of_inHandRotation_14() { return &___inHandRotation_14; }
	inline void set_inHandRotation_14(Vector3_t3722313464  value)
	{
		___inHandRotation_14 = value;
	}

	inline static int32_t get_offset_of_displayMode_15() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___displayMode_15)); }
	inline int32_t get_displayMode_15() const { return ___displayMode_15; }
	inline int32_t* get_address_of_displayMode_15() { return &___displayMode_15; }
	inline void set_displayMode_15(int32_t value)
	{
		___displayMode_15 = value;
	}

	inline static int32_t get_offset_of_transitionDuration_16() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___transitionDuration_16)); }
	inline float get_transitionDuration_16() const { return ___transitionDuration_16; }
	inline float* get_address_of_transitionDuration_16() { return &___transitionDuration_16; }
	inline void set_transitionDuration_16(float value)
	{
		___transitionDuration_16 = value;
	}

	inline static int32_t get_offset_of_transitionCurve_17() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___transitionCurve_17)); }
	inline AnimationCurve_t3046754366 * get_transitionCurve_17() const { return ___transitionCurve_17; }
	inline AnimationCurve_t3046754366 ** get_address_of_transitionCurve_17() { return &___transitionCurve_17; }
	inline void set_transitionCurve_17(AnimationCurve_t3046754366 * value)
	{
		___transitionCurve_17 = value;
		Il2CppCodeGenWriteBarrier((&___transitionCurve_17), value);
	}

	inline static int32_t get_offset_of_currentStrokeColor_18() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___currentStrokeColor_18)); }
	inline Color_t2555686324  get_currentStrokeColor_18() const { return ___currentStrokeColor_18; }
	inline Color_t2555686324 * get_address_of_currentStrokeColor_18() { return &___currentStrokeColor_18; }
	inline void set_currentStrokeColor_18(Color_t2555686324  value)
	{
		___currentStrokeColor_18 = value;
	}

	inline static int32_t get_offset_of_lastPointAddedTime_19() { return static_cast<int32_t>(offsetof(Brush_t1548626474, ___lastPointAddedTime_19)); }
	inline float get_lastPointAddedTime_19() const { return ___lastPointAddedTime_19; }
	inline float* get_address_of_lastPointAddedTime_19() { return &___lastPointAddedTime_19; }
	inline void set_lastPointAddedTime_19(float value)
	{
		___lastPointAddedTime_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRUSH_T1548626474_H
#ifndef BRUSHCONTROLLER_T3766631826_H
#define BRUSHCONTROLLER_T3766631826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.BrushController
struct  BrushController_t3766631826  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushController::minColorDelta
	float ___minColorDelta_2;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushController::minPositionDelta
	float ___minPositionDelta_3;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushController::maxTimeDelta
	float ___maxTimeDelta_4;
	// UnityEngine.Transform HoloToolkit.Unity.ControllerExamples.BrushController::tip
	Transform_t3600365921 * ___tip_5;
	// UnityEngine.GameObject HoloToolkit.Unity.ControllerExamples.BrushController::strokePrefab
	GameObject_t1113636619 * ___strokePrefab_6;
	// UnityEngine.Transform HoloToolkit.Unity.ControllerExamples.BrushController::brushObjectTransform
	Transform_t3600365921 * ___brushObjectTransform_7;
	// UnityEngine.Renderer HoloToolkit.Unity.ControllerExamples.BrushController::brushRenderer
	Renderer_t2627027031 * ___brushRenderer_8;
	// HoloToolkit.Unity.ControllerExamples.ColorPickerWheel HoloToolkit.Unity.ControllerExamples.BrushController::colorPicker
	ColorPickerWheel_t2235220782 * ___colorPicker_9;
	// UnityEngine.Color HoloToolkit.Unity.ControllerExamples.BrushController::currentStrokeColor
	Color_t2555686324  ___currentStrokeColor_10;
	// System.Boolean HoloToolkit.Unity.ControllerExamples.BrushController::draw
	bool ___draw_11;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushController::width
	float ___width_12;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushController::lastPointAddedTime
	float ___lastPointAddedTime_13;

public:
	inline static int32_t get_offset_of_minColorDelta_2() { return static_cast<int32_t>(offsetof(BrushController_t3766631826, ___minColorDelta_2)); }
	inline float get_minColorDelta_2() const { return ___minColorDelta_2; }
	inline float* get_address_of_minColorDelta_2() { return &___minColorDelta_2; }
	inline void set_minColorDelta_2(float value)
	{
		___minColorDelta_2 = value;
	}

	inline static int32_t get_offset_of_minPositionDelta_3() { return static_cast<int32_t>(offsetof(BrushController_t3766631826, ___minPositionDelta_3)); }
	inline float get_minPositionDelta_3() const { return ___minPositionDelta_3; }
	inline float* get_address_of_minPositionDelta_3() { return &___minPositionDelta_3; }
	inline void set_minPositionDelta_3(float value)
	{
		___minPositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_maxTimeDelta_4() { return static_cast<int32_t>(offsetof(BrushController_t3766631826, ___maxTimeDelta_4)); }
	inline float get_maxTimeDelta_4() const { return ___maxTimeDelta_4; }
	inline float* get_address_of_maxTimeDelta_4() { return &___maxTimeDelta_4; }
	inline void set_maxTimeDelta_4(float value)
	{
		___maxTimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_tip_5() { return static_cast<int32_t>(offsetof(BrushController_t3766631826, ___tip_5)); }
	inline Transform_t3600365921 * get_tip_5() const { return ___tip_5; }
	inline Transform_t3600365921 ** get_address_of_tip_5() { return &___tip_5; }
	inline void set_tip_5(Transform_t3600365921 * value)
	{
		___tip_5 = value;
		Il2CppCodeGenWriteBarrier((&___tip_5), value);
	}

	inline static int32_t get_offset_of_strokePrefab_6() { return static_cast<int32_t>(offsetof(BrushController_t3766631826, ___strokePrefab_6)); }
	inline GameObject_t1113636619 * get_strokePrefab_6() const { return ___strokePrefab_6; }
	inline GameObject_t1113636619 ** get_address_of_strokePrefab_6() { return &___strokePrefab_6; }
	inline void set_strokePrefab_6(GameObject_t1113636619 * value)
	{
		___strokePrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___strokePrefab_6), value);
	}

	inline static int32_t get_offset_of_brushObjectTransform_7() { return static_cast<int32_t>(offsetof(BrushController_t3766631826, ___brushObjectTransform_7)); }
	inline Transform_t3600365921 * get_brushObjectTransform_7() const { return ___brushObjectTransform_7; }
	inline Transform_t3600365921 ** get_address_of_brushObjectTransform_7() { return &___brushObjectTransform_7; }
	inline void set_brushObjectTransform_7(Transform_t3600365921 * value)
	{
		___brushObjectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___brushObjectTransform_7), value);
	}

	inline static int32_t get_offset_of_brushRenderer_8() { return static_cast<int32_t>(offsetof(BrushController_t3766631826, ___brushRenderer_8)); }
	inline Renderer_t2627027031 * get_brushRenderer_8() const { return ___brushRenderer_8; }
	inline Renderer_t2627027031 ** get_address_of_brushRenderer_8() { return &___brushRenderer_8; }
	inline void set_brushRenderer_8(Renderer_t2627027031 * value)
	{
		___brushRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___brushRenderer_8), value);
	}

	inline static int32_t get_offset_of_colorPicker_9() { return static_cast<int32_t>(offsetof(BrushController_t3766631826, ___colorPicker_9)); }
	inline ColorPickerWheel_t2235220782 * get_colorPicker_9() const { return ___colorPicker_9; }
	inline ColorPickerWheel_t2235220782 ** get_address_of_colorPicker_9() { return &___colorPicker_9; }
	inline void set_colorPicker_9(ColorPickerWheel_t2235220782 * value)
	{
		___colorPicker_9 = value;
		Il2CppCodeGenWriteBarrier((&___colorPicker_9), value);
	}

	inline static int32_t get_offset_of_currentStrokeColor_10() { return static_cast<int32_t>(offsetof(BrushController_t3766631826, ___currentStrokeColor_10)); }
	inline Color_t2555686324  get_currentStrokeColor_10() const { return ___currentStrokeColor_10; }
	inline Color_t2555686324 * get_address_of_currentStrokeColor_10() { return &___currentStrokeColor_10; }
	inline void set_currentStrokeColor_10(Color_t2555686324  value)
	{
		___currentStrokeColor_10 = value;
	}

	inline static int32_t get_offset_of_draw_11() { return static_cast<int32_t>(offsetof(BrushController_t3766631826, ___draw_11)); }
	inline bool get_draw_11() const { return ___draw_11; }
	inline bool* get_address_of_draw_11() { return &___draw_11; }
	inline void set_draw_11(bool value)
	{
		___draw_11 = value;
	}

	inline static int32_t get_offset_of_width_12() { return static_cast<int32_t>(offsetof(BrushController_t3766631826, ___width_12)); }
	inline float get_width_12() const { return ___width_12; }
	inline float* get_address_of_width_12() { return &___width_12; }
	inline void set_width_12(float value)
	{
		___width_12 = value;
	}

	inline static int32_t get_offset_of_lastPointAddedTime_13() { return static_cast<int32_t>(offsetof(BrushController_t3766631826, ___lastPointAddedTime_13)); }
	inline float get_lastPointAddedTime_13() const { return ___lastPointAddedTime_13; }
	inline float* get_address_of_lastPointAddedTime_13() { return &___lastPointAddedTime_13; }
	inline void set_lastPointAddedTime_13(float value)
	{
		___lastPointAddedTime_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRUSHCONTROLLER_T3766631826_H
#ifndef LINERENDERER_T3025162641_H
#define LINERENDERER_T3025162641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.LineRenderer
struct  LineRenderer_t3025162641  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Gradient HoloToolkit.Unity.Design.LineRenderer::LineColor
	Gradient_t3067099924 * ___LineColor_2;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.Design.LineRenderer::LineWidth
	AnimationCurve_t3046754366 * ___LineWidth_3;
	// System.Single HoloToolkit.Unity.Design.LineRenderer::WidthMultiplier
	float ___WidthMultiplier_4;
	// System.Single HoloToolkit.Unity.Design.LineRenderer::ColorOffset
	float ___ColorOffset_5;
	// System.Single HoloToolkit.Unity.Design.LineRenderer::WidthOffset
	float ___WidthOffset_6;
	// System.Single HoloToolkit.Unity.Design.LineRenderer::RotationOffset
	float ___RotationOffset_7;
	// HoloToolkit.Unity.Design.LineUtils/StepModeEnum HoloToolkit.Unity.Design.LineRenderer::StepMode
	int32_t ___StepMode_8;
	// HoloToolkit.Unity.Design.LineUtils/InterpolationModeEnum HoloToolkit.Unity.Design.LineRenderer::InterpolationMode
	int32_t ___InterpolationMode_9;
	// System.Int32 HoloToolkit.Unity.Design.LineRenderer::NumLineSteps
	int32_t ___NumLineSteps_10;
	// System.Single HoloToolkit.Unity.Design.LineRenderer::StepLength
	float ___StepLength_11;
	// System.Int32 HoloToolkit.Unity.Design.LineRenderer::MaxLineSteps
	int32_t ___MaxLineSteps_12;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.Design.LineRenderer::StepLengthCurve
	AnimationCurve_t3046754366 * ___StepLengthCurve_13;
	// HoloToolkit.Unity.Design.LineBase HoloToolkit.Unity.Design.LineRenderer::source
	LineBase_t717918686 * ___source_14;
	// System.Single[] HoloToolkit.Unity.Design.LineRenderer::normalizedLengths
	SingleU5BU5D_t1444911251* ___normalizedLengths_15;

public:
	inline static int32_t get_offset_of_LineColor_2() { return static_cast<int32_t>(offsetof(LineRenderer_t3025162641, ___LineColor_2)); }
	inline Gradient_t3067099924 * get_LineColor_2() const { return ___LineColor_2; }
	inline Gradient_t3067099924 ** get_address_of_LineColor_2() { return &___LineColor_2; }
	inline void set_LineColor_2(Gradient_t3067099924 * value)
	{
		___LineColor_2 = value;
		Il2CppCodeGenWriteBarrier((&___LineColor_2), value);
	}

	inline static int32_t get_offset_of_LineWidth_3() { return static_cast<int32_t>(offsetof(LineRenderer_t3025162641, ___LineWidth_3)); }
	inline AnimationCurve_t3046754366 * get_LineWidth_3() const { return ___LineWidth_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_LineWidth_3() { return &___LineWidth_3; }
	inline void set_LineWidth_3(AnimationCurve_t3046754366 * value)
	{
		___LineWidth_3 = value;
		Il2CppCodeGenWriteBarrier((&___LineWidth_3), value);
	}

	inline static int32_t get_offset_of_WidthMultiplier_4() { return static_cast<int32_t>(offsetof(LineRenderer_t3025162641, ___WidthMultiplier_4)); }
	inline float get_WidthMultiplier_4() const { return ___WidthMultiplier_4; }
	inline float* get_address_of_WidthMultiplier_4() { return &___WidthMultiplier_4; }
	inline void set_WidthMultiplier_4(float value)
	{
		___WidthMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_ColorOffset_5() { return static_cast<int32_t>(offsetof(LineRenderer_t3025162641, ___ColorOffset_5)); }
	inline float get_ColorOffset_5() const { return ___ColorOffset_5; }
	inline float* get_address_of_ColorOffset_5() { return &___ColorOffset_5; }
	inline void set_ColorOffset_5(float value)
	{
		___ColorOffset_5 = value;
	}

	inline static int32_t get_offset_of_WidthOffset_6() { return static_cast<int32_t>(offsetof(LineRenderer_t3025162641, ___WidthOffset_6)); }
	inline float get_WidthOffset_6() const { return ___WidthOffset_6; }
	inline float* get_address_of_WidthOffset_6() { return &___WidthOffset_6; }
	inline void set_WidthOffset_6(float value)
	{
		___WidthOffset_6 = value;
	}

	inline static int32_t get_offset_of_RotationOffset_7() { return static_cast<int32_t>(offsetof(LineRenderer_t3025162641, ___RotationOffset_7)); }
	inline float get_RotationOffset_7() const { return ___RotationOffset_7; }
	inline float* get_address_of_RotationOffset_7() { return &___RotationOffset_7; }
	inline void set_RotationOffset_7(float value)
	{
		___RotationOffset_7 = value;
	}

	inline static int32_t get_offset_of_StepMode_8() { return static_cast<int32_t>(offsetof(LineRenderer_t3025162641, ___StepMode_8)); }
	inline int32_t get_StepMode_8() const { return ___StepMode_8; }
	inline int32_t* get_address_of_StepMode_8() { return &___StepMode_8; }
	inline void set_StepMode_8(int32_t value)
	{
		___StepMode_8 = value;
	}

	inline static int32_t get_offset_of_InterpolationMode_9() { return static_cast<int32_t>(offsetof(LineRenderer_t3025162641, ___InterpolationMode_9)); }
	inline int32_t get_InterpolationMode_9() const { return ___InterpolationMode_9; }
	inline int32_t* get_address_of_InterpolationMode_9() { return &___InterpolationMode_9; }
	inline void set_InterpolationMode_9(int32_t value)
	{
		___InterpolationMode_9 = value;
	}

	inline static int32_t get_offset_of_NumLineSteps_10() { return static_cast<int32_t>(offsetof(LineRenderer_t3025162641, ___NumLineSteps_10)); }
	inline int32_t get_NumLineSteps_10() const { return ___NumLineSteps_10; }
	inline int32_t* get_address_of_NumLineSteps_10() { return &___NumLineSteps_10; }
	inline void set_NumLineSteps_10(int32_t value)
	{
		___NumLineSteps_10 = value;
	}

	inline static int32_t get_offset_of_StepLength_11() { return static_cast<int32_t>(offsetof(LineRenderer_t3025162641, ___StepLength_11)); }
	inline float get_StepLength_11() const { return ___StepLength_11; }
	inline float* get_address_of_StepLength_11() { return &___StepLength_11; }
	inline void set_StepLength_11(float value)
	{
		___StepLength_11 = value;
	}

	inline static int32_t get_offset_of_MaxLineSteps_12() { return static_cast<int32_t>(offsetof(LineRenderer_t3025162641, ___MaxLineSteps_12)); }
	inline int32_t get_MaxLineSteps_12() const { return ___MaxLineSteps_12; }
	inline int32_t* get_address_of_MaxLineSteps_12() { return &___MaxLineSteps_12; }
	inline void set_MaxLineSteps_12(int32_t value)
	{
		___MaxLineSteps_12 = value;
	}

	inline static int32_t get_offset_of_StepLengthCurve_13() { return static_cast<int32_t>(offsetof(LineRenderer_t3025162641, ___StepLengthCurve_13)); }
	inline AnimationCurve_t3046754366 * get_StepLengthCurve_13() const { return ___StepLengthCurve_13; }
	inline AnimationCurve_t3046754366 ** get_address_of_StepLengthCurve_13() { return &___StepLengthCurve_13; }
	inline void set_StepLengthCurve_13(AnimationCurve_t3046754366 * value)
	{
		___StepLengthCurve_13 = value;
		Il2CppCodeGenWriteBarrier((&___StepLengthCurve_13), value);
	}

	inline static int32_t get_offset_of_source_14() { return static_cast<int32_t>(offsetof(LineRenderer_t3025162641, ___source_14)); }
	inline LineBase_t717918686 * get_source_14() const { return ___source_14; }
	inline LineBase_t717918686 ** get_address_of_source_14() { return &___source_14; }
	inline void set_source_14(LineBase_t717918686 * value)
	{
		___source_14 = value;
		Il2CppCodeGenWriteBarrier((&___source_14), value);
	}

	inline static int32_t get_offset_of_normalizedLengths_15() { return static_cast<int32_t>(offsetof(LineRenderer_t3025162641, ___normalizedLengths_15)); }
	inline SingleU5BU5D_t1444911251* get_normalizedLengths_15() const { return ___normalizedLengths_15; }
	inline SingleU5BU5D_t1444911251** get_address_of_normalizedLengths_15() { return &___normalizedLengths_15; }
	inline void set_normalizedLengths_15(SingleU5BU5D_t1444911251* value)
	{
		___normalizedLengths_15 = value;
		Il2CppCodeGenWriteBarrier((&___normalizedLengths_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINERENDERER_T3025162641_H
#ifndef LINEBASE_T717918686_H
#define LINEBASE_T717918686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.LineBase
struct  LineBase_t717918686  : public MonoBehaviour_t3962482529
{
public:
	// HoloToolkit.Unity.Design.LineUtils/SpaceEnum HoloToolkit.Unity.Design.LineBase::Space
	int32_t ___Space_3;
	// System.Single HoloToolkit.Unity.Design.LineBase::LineStartClamp
	float ___LineStartClamp_4;
	// System.Single HoloToolkit.Unity.Design.LineBase::LineEndClamp
	float ___LineEndClamp_5;
	// HoloToolkit.Unity.Design.LineUtils/RotationTypeEnum HoloToolkit.Unity.Design.LineBase::RotationType
	int32_t ___RotationType_6;
	// System.Boolean HoloToolkit.Unity.Design.LineBase::FlipUpVector
	bool ___FlipUpVector_7;
	// UnityEngine.Vector3 HoloToolkit.Unity.Design.LineBase::OriginOffset
	Vector3_t3722313464  ___OriginOffset_8;
	// System.Single HoloToolkit.Unity.Design.LineBase::ManualUpVectorBlend
	float ___ManualUpVectorBlend_9;
	// UnityEngine.Vector3[] HoloToolkit.Unity.Design.LineBase::ManualUpVectors
	Vector3U5BU5D_t1718750761* ___ManualUpVectors_10;
	// System.Single HoloToolkit.Unity.Design.LineBase::VelocitySearchRange
	float ___VelocitySearchRange_11;
	// System.Single HoloToolkit.Unity.Design.LineBase::VelocityBlend
	float ___VelocityBlend_12;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.Design.LineBase::DistortionStrength
	AnimationCurve_t3046754366 * ___DistortionStrength_13;
	// HoloToolkit.Unity.Design.IDistorter[] HoloToolkit.Unity.Design.LineBase::distorters
	IDistorterU5BU5D_t4037927985* ___distorters_14;
	// System.Boolean HoloToolkit.Unity.Design.LineBase::loops
	bool ___loops_15;

public:
	inline static int32_t get_offset_of_Space_3() { return static_cast<int32_t>(offsetof(LineBase_t717918686, ___Space_3)); }
	inline int32_t get_Space_3() const { return ___Space_3; }
	inline int32_t* get_address_of_Space_3() { return &___Space_3; }
	inline void set_Space_3(int32_t value)
	{
		___Space_3 = value;
	}

	inline static int32_t get_offset_of_LineStartClamp_4() { return static_cast<int32_t>(offsetof(LineBase_t717918686, ___LineStartClamp_4)); }
	inline float get_LineStartClamp_4() const { return ___LineStartClamp_4; }
	inline float* get_address_of_LineStartClamp_4() { return &___LineStartClamp_4; }
	inline void set_LineStartClamp_4(float value)
	{
		___LineStartClamp_4 = value;
	}

	inline static int32_t get_offset_of_LineEndClamp_5() { return static_cast<int32_t>(offsetof(LineBase_t717918686, ___LineEndClamp_5)); }
	inline float get_LineEndClamp_5() const { return ___LineEndClamp_5; }
	inline float* get_address_of_LineEndClamp_5() { return &___LineEndClamp_5; }
	inline void set_LineEndClamp_5(float value)
	{
		___LineEndClamp_5 = value;
	}

	inline static int32_t get_offset_of_RotationType_6() { return static_cast<int32_t>(offsetof(LineBase_t717918686, ___RotationType_6)); }
	inline int32_t get_RotationType_6() const { return ___RotationType_6; }
	inline int32_t* get_address_of_RotationType_6() { return &___RotationType_6; }
	inline void set_RotationType_6(int32_t value)
	{
		___RotationType_6 = value;
	}

	inline static int32_t get_offset_of_FlipUpVector_7() { return static_cast<int32_t>(offsetof(LineBase_t717918686, ___FlipUpVector_7)); }
	inline bool get_FlipUpVector_7() const { return ___FlipUpVector_7; }
	inline bool* get_address_of_FlipUpVector_7() { return &___FlipUpVector_7; }
	inline void set_FlipUpVector_7(bool value)
	{
		___FlipUpVector_7 = value;
	}

	inline static int32_t get_offset_of_OriginOffset_8() { return static_cast<int32_t>(offsetof(LineBase_t717918686, ___OriginOffset_8)); }
	inline Vector3_t3722313464  get_OriginOffset_8() const { return ___OriginOffset_8; }
	inline Vector3_t3722313464 * get_address_of_OriginOffset_8() { return &___OriginOffset_8; }
	inline void set_OriginOffset_8(Vector3_t3722313464  value)
	{
		___OriginOffset_8 = value;
	}

	inline static int32_t get_offset_of_ManualUpVectorBlend_9() { return static_cast<int32_t>(offsetof(LineBase_t717918686, ___ManualUpVectorBlend_9)); }
	inline float get_ManualUpVectorBlend_9() const { return ___ManualUpVectorBlend_9; }
	inline float* get_address_of_ManualUpVectorBlend_9() { return &___ManualUpVectorBlend_9; }
	inline void set_ManualUpVectorBlend_9(float value)
	{
		___ManualUpVectorBlend_9 = value;
	}

	inline static int32_t get_offset_of_ManualUpVectors_10() { return static_cast<int32_t>(offsetof(LineBase_t717918686, ___ManualUpVectors_10)); }
	inline Vector3U5BU5D_t1718750761* get_ManualUpVectors_10() const { return ___ManualUpVectors_10; }
	inline Vector3U5BU5D_t1718750761** get_address_of_ManualUpVectors_10() { return &___ManualUpVectors_10; }
	inline void set_ManualUpVectors_10(Vector3U5BU5D_t1718750761* value)
	{
		___ManualUpVectors_10 = value;
		Il2CppCodeGenWriteBarrier((&___ManualUpVectors_10), value);
	}

	inline static int32_t get_offset_of_VelocitySearchRange_11() { return static_cast<int32_t>(offsetof(LineBase_t717918686, ___VelocitySearchRange_11)); }
	inline float get_VelocitySearchRange_11() const { return ___VelocitySearchRange_11; }
	inline float* get_address_of_VelocitySearchRange_11() { return &___VelocitySearchRange_11; }
	inline void set_VelocitySearchRange_11(float value)
	{
		___VelocitySearchRange_11 = value;
	}

	inline static int32_t get_offset_of_VelocityBlend_12() { return static_cast<int32_t>(offsetof(LineBase_t717918686, ___VelocityBlend_12)); }
	inline float get_VelocityBlend_12() const { return ___VelocityBlend_12; }
	inline float* get_address_of_VelocityBlend_12() { return &___VelocityBlend_12; }
	inline void set_VelocityBlend_12(float value)
	{
		___VelocityBlend_12 = value;
	}

	inline static int32_t get_offset_of_DistortionStrength_13() { return static_cast<int32_t>(offsetof(LineBase_t717918686, ___DistortionStrength_13)); }
	inline AnimationCurve_t3046754366 * get_DistortionStrength_13() const { return ___DistortionStrength_13; }
	inline AnimationCurve_t3046754366 ** get_address_of_DistortionStrength_13() { return &___DistortionStrength_13; }
	inline void set_DistortionStrength_13(AnimationCurve_t3046754366 * value)
	{
		___DistortionStrength_13 = value;
		Il2CppCodeGenWriteBarrier((&___DistortionStrength_13), value);
	}

	inline static int32_t get_offset_of_distorters_14() { return static_cast<int32_t>(offsetof(LineBase_t717918686, ___distorters_14)); }
	inline IDistorterU5BU5D_t4037927985* get_distorters_14() const { return ___distorters_14; }
	inline IDistorterU5BU5D_t4037927985** get_address_of_distorters_14() { return &___distorters_14; }
	inline void set_distorters_14(IDistorterU5BU5D_t4037927985* value)
	{
		___distorters_14 = value;
		Il2CppCodeGenWriteBarrier((&___distorters_14), value);
	}

	inline static int32_t get_offset_of_loops_15() { return static_cast<int32_t>(offsetof(LineBase_t717918686, ___loops_15)); }
	inline bool get_loops_15() const { return ___loops_15; }
	inline bool* get_address_of_loops_15() { return &___loops_15; }
	inline void set_loops_15(bool value)
	{
		___loops_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEBASE_T717918686_H
#ifndef SINGLETON_1_T3008279456_H
#define SINGLETON_1_T3008279456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.Boundary.BoundaryManager>
struct  Singleton_1_t3008279456  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t3008279456_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	BoundaryManager_t2608055002 * ___instance_2;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t3008279456_StaticFields, ___instance_2)); }
	inline BoundaryManager_t2608055002 * get_instance_2() const { return ___instance_2; }
	inline BoundaryManager_t2608055002 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(BoundaryManager_t2608055002 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_searchForInstance_3() { return static_cast<int32_t>(offsetof(Singleton_1_t3008279456_StaticFields, ___searchForInstance_3)); }
	inline bool get_searchForInstance_3() const { return ___searchForInstance_3; }
	inline bool* get_address_of_searchForInstance_3() { return &___searchForInstance_3; }
	inline void set_searchForInstance_3(bool value)
	{
		___searchForInstance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T3008279456_H
#ifndef LINEOBJECTCOLLECTION_T2817952665_H
#define LINEOBJECTCOLLECTION_T2817952665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.LineObjectCollection
struct  LineObjectCollection_t2817952665  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Transform> HoloToolkit.Unity.Design.LineObjectCollection::Objects
	List_1_t777473367 * ___Objects_2;
	// System.Single HoloToolkit.Unity.Design.LineObjectCollection::DistributionOffset
	float ___DistributionOffset_3;
	// System.Single HoloToolkit.Unity.Design.LineObjectCollection::LengthOffset
	float ___LengthOffset_4;
	// System.Single HoloToolkit.Unity.Design.LineObjectCollection::ScaleOffset
	float ___ScaleOffset_5;
	// System.Single HoloToolkit.Unity.Design.LineObjectCollection::ScaleMultiplier
	float ___ScaleMultiplier_6;
	// System.Single HoloToolkit.Unity.Design.LineObjectCollection::PositionMultiplier
	float ___PositionMultiplier_7;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.Design.LineObjectCollection::ObjectScale
	AnimationCurve_t3046754366 * ___ObjectScale_8;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.Design.LineObjectCollection::ObjectPosition
	AnimationCurve_t3046754366 * ___ObjectPosition_9;
	// System.Boolean HoloToolkit.Unity.Design.LineObjectCollection::FlipRotation
	bool ___FlipRotation_10;
	// UnityEngine.Vector3 HoloToolkit.Unity.Design.LineObjectCollection::RotationOffset
	Vector3_t3722313464  ___RotationOffset_11;
	// UnityEngine.Vector3 HoloToolkit.Unity.Design.LineObjectCollection::PositionOffset
	Vector3_t3722313464  ___PositionOffset_12;
	// HoloToolkit.Unity.Design.LineUtils/RotationTypeEnum HoloToolkit.Unity.Design.LineObjectCollection::RotationTypeOverride
	int32_t ___RotationTypeOverride_13;
	// HoloToolkit.Unity.Design.LineUtils/PointDistributionTypeEnum HoloToolkit.Unity.Design.LineObjectCollection::DistributionType
	int32_t ___DistributionType_14;
	// HoloToolkit.Unity.Design.LineUtils/StepModeEnum HoloToolkit.Unity.Design.LineObjectCollection::StepMode
	int32_t ___StepMode_15;
	// HoloToolkit.Unity.Design.LineBase HoloToolkit.Unity.Design.LineObjectCollection::source
	LineBase_t717918686 * ___source_16;
	// UnityEngine.Transform HoloToolkit.Unity.Design.LineObjectCollection::transformHelper
	Transform_t3600365921 * ___transformHelper_17;

public:
	inline static int32_t get_offset_of_Objects_2() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___Objects_2)); }
	inline List_1_t777473367 * get_Objects_2() const { return ___Objects_2; }
	inline List_1_t777473367 ** get_address_of_Objects_2() { return &___Objects_2; }
	inline void set_Objects_2(List_1_t777473367 * value)
	{
		___Objects_2 = value;
		Il2CppCodeGenWriteBarrier((&___Objects_2), value);
	}

	inline static int32_t get_offset_of_DistributionOffset_3() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___DistributionOffset_3)); }
	inline float get_DistributionOffset_3() const { return ___DistributionOffset_3; }
	inline float* get_address_of_DistributionOffset_3() { return &___DistributionOffset_3; }
	inline void set_DistributionOffset_3(float value)
	{
		___DistributionOffset_3 = value;
	}

	inline static int32_t get_offset_of_LengthOffset_4() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___LengthOffset_4)); }
	inline float get_LengthOffset_4() const { return ___LengthOffset_4; }
	inline float* get_address_of_LengthOffset_4() { return &___LengthOffset_4; }
	inline void set_LengthOffset_4(float value)
	{
		___LengthOffset_4 = value;
	}

	inline static int32_t get_offset_of_ScaleOffset_5() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___ScaleOffset_5)); }
	inline float get_ScaleOffset_5() const { return ___ScaleOffset_5; }
	inline float* get_address_of_ScaleOffset_5() { return &___ScaleOffset_5; }
	inline void set_ScaleOffset_5(float value)
	{
		___ScaleOffset_5 = value;
	}

	inline static int32_t get_offset_of_ScaleMultiplier_6() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___ScaleMultiplier_6)); }
	inline float get_ScaleMultiplier_6() const { return ___ScaleMultiplier_6; }
	inline float* get_address_of_ScaleMultiplier_6() { return &___ScaleMultiplier_6; }
	inline void set_ScaleMultiplier_6(float value)
	{
		___ScaleMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_PositionMultiplier_7() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___PositionMultiplier_7)); }
	inline float get_PositionMultiplier_7() const { return ___PositionMultiplier_7; }
	inline float* get_address_of_PositionMultiplier_7() { return &___PositionMultiplier_7; }
	inline void set_PositionMultiplier_7(float value)
	{
		___PositionMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_ObjectScale_8() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___ObjectScale_8)); }
	inline AnimationCurve_t3046754366 * get_ObjectScale_8() const { return ___ObjectScale_8; }
	inline AnimationCurve_t3046754366 ** get_address_of_ObjectScale_8() { return &___ObjectScale_8; }
	inline void set_ObjectScale_8(AnimationCurve_t3046754366 * value)
	{
		___ObjectScale_8 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectScale_8), value);
	}

	inline static int32_t get_offset_of_ObjectPosition_9() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___ObjectPosition_9)); }
	inline AnimationCurve_t3046754366 * get_ObjectPosition_9() const { return ___ObjectPosition_9; }
	inline AnimationCurve_t3046754366 ** get_address_of_ObjectPosition_9() { return &___ObjectPosition_9; }
	inline void set_ObjectPosition_9(AnimationCurve_t3046754366 * value)
	{
		___ObjectPosition_9 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectPosition_9), value);
	}

	inline static int32_t get_offset_of_FlipRotation_10() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___FlipRotation_10)); }
	inline bool get_FlipRotation_10() const { return ___FlipRotation_10; }
	inline bool* get_address_of_FlipRotation_10() { return &___FlipRotation_10; }
	inline void set_FlipRotation_10(bool value)
	{
		___FlipRotation_10 = value;
	}

	inline static int32_t get_offset_of_RotationOffset_11() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___RotationOffset_11)); }
	inline Vector3_t3722313464  get_RotationOffset_11() const { return ___RotationOffset_11; }
	inline Vector3_t3722313464 * get_address_of_RotationOffset_11() { return &___RotationOffset_11; }
	inline void set_RotationOffset_11(Vector3_t3722313464  value)
	{
		___RotationOffset_11 = value;
	}

	inline static int32_t get_offset_of_PositionOffset_12() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___PositionOffset_12)); }
	inline Vector3_t3722313464  get_PositionOffset_12() const { return ___PositionOffset_12; }
	inline Vector3_t3722313464 * get_address_of_PositionOffset_12() { return &___PositionOffset_12; }
	inline void set_PositionOffset_12(Vector3_t3722313464  value)
	{
		___PositionOffset_12 = value;
	}

	inline static int32_t get_offset_of_RotationTypeOverride_13() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___RotationTypeOverride_13)); }
	inline int32_t get_RotationTypeOverride_13() const { return ___RotationTypeOverride_13; }
	inline int32_t* get_address_of_RotationTypeOverride_13() { return &___RotationTypeOverride_13; }
	inline void set_RotationTypeOverride_13(int32_t value)
	{
		___RotationTypeOverride_13 = value;
	}

	inline static int32_t get_offset_of_DistributionType_14() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___DistributionType_14)); }
	inline int32_t get_DistributionType_14() const { return ___DistributionType_14; }
	inline int32_t* get_address_of_DistributionType_14() { return &___DistributionType_14; }
	inline void set_DistributionType_14(int32_t value)
	{
		___DistributionType_14 = value;
	}

	inline static int32_t get_offset_of_StepMode_15() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___StepMode_15)); }
	inline int32_t get_StepMode_15() const { return ___StepMode_15; }
	inline int32_t* get_address_of_StepMode_15() { return &___StepMode_15; }
	inline void set_StepMode_15(int32_t value)
	{
		___StepMode_15 = value;
	}

	inline static int32_t get_offset_of_source_16() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___source_16)); }
	inline LineBase_t717918686 * get_source_16() const { return ___source_16; }
	inline LineBase_t717918686 ** get_address_of_source_16() { return &___source_16; }
	inline void set_source_16(LineBase_t717918686 * value)
	{
		___source_16 = value;
		Il2CppCodeGenWriteBarrier((&___source_16), value);
	}

	inline static int32_t get_offset_of_transformHelper_17() { return static_cast<int32_t>(offsetof(LineObjectCollection_t2817952665, ___transformHelper_17)); }
	inline Transform_t3600365921 * get_transformHelper_17() const { return ___transformHelper_17; }
	inline Transform_t3600365921 ** get_address_of_transformHelper_17() { return &___transformHelper_17; }
	inline void set_transformHelper_17(Transform_t3600365921 * value)
	{
		___transformHelper_17 = value;
		Il2CppCodeGenWriteBarrier((&___transformHelper_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEOBJECTCOLLECTION_T2817952665_H
#ifndef MOTIONCONTROLLERVISUALIZER_T392243420_H
#define MOTIONCONTROLLERVISUALIZER_T392243420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.MotionControllerVisualizer
struct  MotionControllerVisualizer_t392243420  : public Singleton_1_t792467874
{
public:
	// System.Boolean HoloToolkit.Unity.InputModule.MotionControllerVisualizer::AnimateControllerModel
	bool ___AnimateControllerModel_4;
	// System.Boolean HoloToolkit.Unity.InputModule.MotionControllerVisualizer::AlwaysUseAlternateLeftModel
	bool ___AlwaysUseAlternateLeftModel_5;
	// System.Boolean HoloToolkit.Unity.InputModule.MotionControllerVisualizer::AlwaysUseAlternateRightModel
	bool ___AlwaysUseAlternateRightModel_6;
	// UnityEngine.GameObject HoloToolkit.Unity.InputModule.MotionControllerVisualizer::AlternateLeftController
	GameObject_t1113636619 * ___AlternateLeftController_7;
	// UnityEngine.GameObject HoloToolkit.Unity.InputModule.MotionControllerVisualizer::AlternateRightController
	GameObject_t1113636619 * ___AlternateRightController_8;
	// UnityEngine.GameObject HoloToolkit.Unity.InputModule.MotionControllerVisualizer::TouchpadTouchedOverride
	GameObject_t1113636619 * ___TouchpadTouchedOverride_9;
	// UnityEngine.Material HoloToolkit.Unity.InputModule.MotionControllerVisualizer::GLTFMaterial
	Material_t340375123 * ___GLTFMaterial_10;
	// System.Collections.Generic.Dictionary`2<System.String,HoloToolkit.Unity.InputModule.MotionControllerInfo> HoloToolkit.Unity.InputModule.MotionControllerVisualizer::controllerDictionary
	Dictionary_2_t723408522 * ___controllerDictionary_11;
	// System.Collections.Generic.List`1<System.String> HoloToolkit.Unity.InputModule.MotionControllerVisualizer::loadingControllers
	List_1_t3319525431 * ___loadingControllers_12;
	// HoloToolkit.Unity.InputModule.MotionControllerInfo HoloToolkit.Unity.InputModule.MotionControllerVisualizer::leftControllerModel
	MotionControllerInfo_t938152223 * ___leftControllerModel_13;
	// HoloToolkit.Unity.InputModule.MotionControllerInfo HoloToolkit.Unity.InputModule.MotionControllerVisualizer::rightControllerModel
	MotionControllerInfo_t938152223 * ___rightControllerModel_14;
	// System.Action`1<HoloToolkit.Unity.InputModule.MotionControllerInfo> HoloToolkit.Unity.InputModule.MotionControllerVisualizer::OnControllerModelLoaded
	Action_1_t1110619818 * ___OnControllerModelLoaded_15;
	// System.Action`1<HoloToolkit.Unity.InputModule.MotionControllerInfo> HoloToolkit.Unity.InputModule.MotionControllerVisualizer::OnControllerModelUnloaded
	Action_1_t1110619818 * ___OnControllerModelUnloaded_16;

public:
	inline static int32_t get_offset_of_AnimateControllerModel_4() { return static_cast<int32_t>(offsetof(MotionControllerVisualizer_t392243420, ___AnimateControllerModel_4)); }
	inline bool get_AnimateControllerModel_4() const { return ___AnimateControllerModel_4; }
	inline bool* get_address_of_AnimateControllerModel_4() { return &___AnimateControllerModel_4; }
	inline void set_AnimateControllerModel_4(bool value)
	{
		___AnimateControllerModel_4 = value;
	}

	inline static int32_t get_offset_of_AlwaysUseAlternateLeftModel_5() { return static_cast<int32_t>(offsetof(MotionControllerVisualizer_t392243420, ___AlwaysUseAlternateLeftModel_5)); }
	inline bool get_AlwaysUseAlternateLeftModel_5() const { return ___AlwaysUseAlternateLeftModel_5; }
	inline bool* get_address_of_AlwaysUseAlternateLeftModel_5() { return &___AlwaysUseAlternateLeftModel_5; }
	inline void set_AlwaysUseAlternateLeftModel_5(bool value)
	{
		___AlwaysUseAlternateLeftModel_5 = value;
	}

	inline static int32_t get_offset_of_AlwaysUseAlternateRightModel_6() { return static_cast<int32_t>(offsetof(MotionControllerVisualizer_t392243420, ___AlwaysUseAlternateRightModel_6)); }
	inline bool get_AlwaysUseAlternateRightModel_6() const { return ___AlwaysUseAlternateRightModel_6; }
	inline bool* get_address_of_AlwaysUseAlternateRightModel_6() { return &___AlwaysUseAlternateRightModel_6; }
	inline void set_AlwaysUseAlternateRightModel_6(bool value)
	{
		___AlwaysUseAlternateRightModel_6 = value;
	}

	inline static int32_t get_offset_of_AlternateLeftController_7() { return static_cast<int32_t>(offsetof(MotionControllerVisualizer_t392243420, ___AlternateLeftController_7)); }
	inline GameObject_t1113636619 * get_AlternateLeftController_7() const { return ___AlternateLeftController_7; }
	inline GameObject_t1113636619 ** get_address_of_AlternateLeftController_7() { return &___AlternateLeftController_7; }
	inline void set_AlternateLeftController_7(GameObject_t1113636619 * value)
	{
		___AlternateLeftController_7 = value;
		Il2CppCodeGenWriteBarrier((&___AlternateLeftController_7), value);
	}

	inline static int32_t get_offset_of_AlternateRightController_8() { return static_cast<int32_t>(offsetof(MotionControllerVisualizer_t392243420, ___AlternateRightController_8)); }
	inline GameObject_t1113636619 * get_AlternateRightController_8() const { return ___AlternateRightController_8; }
	inline GameObject_t1113636619 ** get_address_of_AlternateRightController_8() { return &___AlternateRightController_8; }
	inline void set_AlternateRightController_8(GameObject_t1113636619 * value)
	{
		___AlternateRightController_8 = value;
		Il2CppCodeGenWriteBarrier((&___AlternateRightController_8), value);
	}

	inline static int32_t get_offset_of_TouchpadTouchedOverride_9() { return static_cast<int32_t>(offsetof(MotionControllerVisualizer_t392243420, ___TouchpadTouchedOverride_9)); }
	inline GameObject_t1113636619 * get_TouchpadTouchedOverride_9() const { return ___TouchpadTouchedOverride_9; }
	inline GameObject_t1113636619 ** get_address_of_TouchpadTouchedOverride_9() { return &___TouchpadTouchedOverride_9; }
	inline void set_TouchpadTouchedOverride_9(GameObject_t1113636619 * value)
	{
		___TouchpadTouchedOverride_9 = value;
		Il2CppCodeGenWriteBarrier((&___TouchpadTouchedOverride_9), value);
	}

	inline static int32_t get_offset_of_GLTFMaterial_10() { return static_cast<int32_t>(offsetof(MotionControllerVisualizer_t392243420, ___GLTFMaterial_10)); }
	inline Material_t340375123 * get_GLTFMaterial_10() const { return ___GLTFMaterial_10; }
	inline Material_t340375123 ** get_address_of_GLTFMaterial_10() { return &___GLTFMaterial_10; }
	inline void set_GLTFMaterial_10(Material_t340375123 * value)
	{
		___GLTFMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&___GLTFMaterial_10), value);
	}

	inline static int32_t get_offset_of_controllerDictionary_11() { return static_cast<int32_t>(offsetof(MotionControllerVisualizer_t392243420, ___controllerDictionary_11)); }
	inline Dictionary_2_t723408522 * get_controllerDictionary_11() const { return ___controllerDictionary_11; }
	inline Dictionary_2_t723408522 ** get_address_of_controllerDictionary_11() { return &___controllerDictionary_11; }
	inline void set_controllerDictionary_11(Dictionary_2_t723408522 * value)
	{
		___controllerDictionary_11 = value;
		Il2CppCodeGenWriteBarrier((&___controllerDictionary_11), value);
	}

	inline static int32_t get_offset_of_loadingControllers_12() { return static_cast<int32_t>(offsetof(MotionControllerVisualizer_t392243420, ___loadingControllers_12)); }
	inline List_1_t3319525431 * get_loadingControllers_12() const { return ___loadingControllers_12; }
	inline List_1_t3319525431 ** get_address_of_loadingControllers_12() { return &___loadingControllers_12; }
	inline void set_loadingControllers_12(List_1_t3319525431 * value)
	{
		___loadingControllers_12 = value;
		Il2CppCodeGenWriteBarrier((&___loadingControllers_12), value);
	}

	inline static int32_t get_offset_of_leftControllerModel_13() { return static_cast<int32_t>(offsetof(MotionControllerVisualizer_t392243420, ___leftControllerModel_13)); }
	inline MotionControllerInfo_t938152223 * get_leftControllerModel_13() const { return ___leftControllerModel_13; }
	inline MotionControllerInfo_t938152223 ** get_address_of_leftControllerModel_13() { return &___leftControllerModel_13; }
	inline void set_leftControllerModel_13(MotionControllerInfo_t938152223 * value)
	{
		___leftControllerModel_13 = value;
		Il2CppCodeGenWriteBarrier((&___leftControllerModel_13), value);
	}

	inline static int32_t get_offset_of_rightControllerModel_14() { return static_cast<int32_t>(offsetof(MotionControllerVisualizer_t392243420, ___rightControllerModel_14)); }
	inline MotionControllerInfo_t938152223 * get_rightControllerModel_14() const { return ___rightControllerModel_14; }
	inline MotionControllerInfo_t938152223 ** get_address_of_rightControllerModel_14() { return &___rightControllerModel_14; }
	inline void set_rightControllerModel_14(MotionControllerInfo_t938152223 * value)
	{
		___rightControllerModel_14 = value;
		Il2CppCodeGenWriteBarrier((&___rightControllerModel_14), value);
	}

	inline static int32_t get_offset_of_OnControllerModelLoaded_15() { return static_cast<int32_t>(offsetof(MotionControllerVisualizer_t392243420, ___OnControllerModelLoaded_15)); }
	inline Action_1_t1110619818 * get_OnControllerModelLoaded_15() const { return ___OnControllerModelLoaded_15; }
	inline Action_1_t1110619818 ** get_address_of_OnControllerModelLoaded_15() { return &___OnControllerModelLoaded_15; }
	inline void set_OnControllerModelLoaded_15(Action_1_t1110619818 * value)
	{
		___OnControllerModelLoaded_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnControllerModelLoaded_15), value);
	}

	inline static int32_t get_offset_of_OnControllerModelUnloaded_16() { return static_cast<int32_t>(offsetof(MotionControllerVisualizer_t392243420, ___OnControllerModelUnloaded_16)); }
	inline Action_1_t1110619818 * get_OnControllerModelUnloaded_16() const { return ___OnControllerModelUnloaded_16; }
	inline Action_1_t1110619818 ** get_address_of_OnControllerModelUnloaded_16() { return &___OnControllerModelUnloaded_16; }
	inline void set_OnControllerModelUnloaded_16(Action_1_t1110619818 * value)
	{
		___OnControllerModelUnloaded_16 = value;
		Il2CppCodeGenWriteBarrier((&___OnControllerModelUnloaded_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONCONTROLLERVISUALIZER_T392243420_H
#ifndef BOUNDARYMANAGER_T2608055002_H
#define BOUNDARYMANAGER_T2608055002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Boundary.BoundaryManager
struct  BoundaryManager_t2608055002  : public Singleton_1_t3008279456
{
public:
	// UnityEngine.GameObject HoloToolkit.Unity.Boundary.BoundaryManager::FloorQuad
	GameObject_t1113636619 * ___FloorQuad_4;
	// UnityEngine.GameObject HoloToolkit.Unity.Boundary.BoundaryManager::floorQuadInstance
	GameObject_t1113636619 * ___floorQuadInstance_5;
	// System.Single HoloToolkit.Unity.Boundary.BoundaryManager::boundaryHeight
	float ___boundaryHeight_6;
	// UnityEngine.Bounds HoloToolkit.Unity.Boundary.BoundaryManager::boundaryBounds
	Bounds_t2266837910  ___boundaryBounds_7;
	// UnityEngine.XR.TrackingSpaceType HoloToolkit.Unity.Boundary.BoundaryManager::opaqueTrackingSpaceType
	int32_t ___opaqueTrackingSpaceType_8;
	// UnityEngine.Vector3 HoloToolkit.Unity.Boundary.BoundaryManager::floorPositionInEditor
	Vector3_t3722313464  ___floorPositionInEditor_9;
	// System.Boolean HoloToolkit.Unity.Boundary.BoundaryManager::renderFloor
	bool ___renderFloor_10;
	// System.Boolean HoloToolkit.Unity.Boundary.BoundaryManager::renderBoundary
	bool ___renderBoundary_11;

public:
	inline static int32_t get_offset_of_FloorQuad_4() { return static_cast<int32_t>(offsetof(BoundaryManager_t2608055002, ___FloorQuad_4)); }
	inline GameObject_t1113636619 * get_FloorQuad_4() const { return ___FloorQuad_4; }
	inline GameObject_t1113636619 ** get_address_of_FloorQuad_4() { return &___FloorQuad_4; }
	inline void set_FloorQuad_4(GameObject_t1113636619 * value)
	{
		___FloorQuad_4 = value;
		Il2CppCodeGenWriteBarrier((&___FloorQuad_4), value);
	}

	inline static int32_t get_offset_of_floorQuadInstance_5() { return static_cast<int32_t>(offsetof(BoundaryManager_t2608055002, ___floorQuadInstance_5)); }
	inline GameObject_t1113636619 * get_floorQuadInstance_5() const { return ___floorQuadInstance_5; }
	inline GameObject_t1113636619 ** get_address_of_floorQuadInstance_5() { return &___floorQuadInstance_5; }
	inline void set_floorQuadInstance_5(GameObject_t1113636619 * value)
	{
		___floorQuadInstance_5 = value;
		Il2CppCodeGenWriteBarrier((&___floorQuadInstance_5), value);
	}

	inline static int32_t get_offset_of_boundaryHeight_6() { return static_cast<int32_t>(offsetof(BoundaryManager_t2608055002, ___boundaryHeight_6)); }
	inline float get_boundaryHeight_6() const { return ___boundaryHeight_6; }
	inline float* get_address_of_boundaryHeight_6() { return &___boundaryHeight_6; }
	inline void set_boundaryHeight_6(float value)
	{
		___boundaryHeight_6 = value;
	}

	inline static int32_t get_offset_of_boundaryBounds_7() { return static_cast<int32_t>(offsetof(BoundaryManager_t2608055002, ___boundaryBounds_7)); }
	inline Bounds_t2266837910  get_boundaryBounds_7() const { return ___boundaryBounds_7; }
	inline Bounds_t2266837910 * get_address_of_boundaryBounds_7() { return &___boundaryBounds_7; }
	inline void set_boundaryBounds_7(Bounds_t2266837910  value)
	{
		___boundaryBounds_7 = value;
	}

	inline static int32_t get_offset_of_opaqueTrackingSpaceType_8() { return static_cast<int32_t>(offsetof(BoundaryManager_t2608055002, ___opaqueTrackingSpaceType_8)); }
	inline int32_t get_opaqueTrackingSpaceType_8() const { return ___opaqueTrackingSpaceType_8; }
	inline int32_t* get_address_of_opaqueTrackingSpaceType_8() { return &___opaqueTrackingSpaceType_8; }
	inline void set_opaqueTrackingSpaceType_8(int32_t value)
	{
		___opaqueTrackingSpaceType_8 = value;
	}

	inline static int32_t get_offset_of_floorPositionInEditor_9() { return static_cast<int32_t>(offsetof(BoundaryManager_t2608055002, ___floorPositionInEditor_9)); }
	inline Vector3_t3722313464  get_floorPositionInEditor_9() const { return ___floorPositionInEditor_9; }
	inline Vector3_t3722313464 * get_address_of_floorPositionInEditor_9() { return &___floorPositionInEditor_9; }
	inline void set_floorPositionInEditor_9(Vector3_t3722313464  value)
	{
		___floorPositionInEditor_9 = value;
	}

	inline static int32_t get_offset_of_renderFloor_10() { return static_cast<int32_t>(offsetof(BoundaryManager_t2608055002, ___renderFloor_10)); }
	inline bool get_renderFloor_10() const { return ___renderFloor_10; }
	inline bool* get_address_of_renderFloor_10() { return &___renderFloor_10; }
	inline void set_renderFloor_10(bool value)
	{
		___renderFloor_10 = value;
	}

	inline static int32_t get_offset_of_renderBoundary_11() { return static_cast<int32_t>(offsetof(BoundaryManager_t2608055002, ___renderBoundary_11)); }
	inline bool get_renderBoundary_11() const { return ___renderBoundary_11; }
	inline bool* get_address_of_renderBoundary_11() { return &___renderBoundary_11; }
	inline void set_renderBoundary_11(bool value)
	{
		___renderBoundary_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDARYMANAGER_T2608055002_H
#ifndef ERASER_T1049249012_H
#define ERASER_T1049249012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.Eraser
struct  Eraser_t1049249012  : public Brush_t1548626474
{
public:
	// System.Single HoloToolkit.Unity.ControllerExamples.Eraser::eraseRange
	float ___eraseRange_20;
	// System.Single HoloToolkit.Unity.ControllerExamples.Eraser::eraseTime
	float ___eraseTime_21;
	// System.Boolean HoloToolkit.Unity.ControllerExamples.Eraser::erasingStrokes
	bool ___erasingStrokes_22;
	// System.Collections.Generic.Queue`1<UnityEngine.LineRenderer> HoloToolkit.Unity.ControllerExamples.Eraser::erasedStrokes
	Queue_1_t3000609764 * ___erasedStrokes_23;

public:
	inline static int32_t get_offset_of_eraseRange_20() { return static_cast<int32_t>(offsetof(Eraser_t1049249012, ___eraseRange_20)); }
	inline float get_eraseRange_20() const { return ___eraseRange_20; }
	inline float* get_address_of_eraseRange_20() { return &___eraseRange_20; }
	inline void set_eraseRange_20(float value)
	{
		___eraseRange_20 = value;
	}

	inline static int32_t get_offset_of_eraseTime_21() { return static_cast<int32_t>(offsetof(Eraser_t1049249012, ___eraseTime_21)); }
	inline float get_eraseTime_21() const { return ___eraseTime_21; }
	inline float* get_address_of_eraseTime_21() { return &___eraseTime_21; }
	inline void set_eraseTime_21(float value)
	{
		___eraseTime_21 = value;
	}

	inline static int32_t get_offset_of_erasingStrokes_22() { return static_cast<int32_t>(offsetof(Eraser_t1049249012, ___erasingStrokes_22)); }
	inline bool get_erasingStrokes_22() const { return ___erasingStrokes_22; }
	inline bool* get_address_of_erasingStrokes_22() { return &___erasingStrokes_22; }
	inline void set_erasingStrokes_22(bool value)
	{
		___erasingStrokes_22 = value;
	}

	inline static int32_t get_offset_of_erasedStrokes_23() { return static_cast<int32_t>(offsetof(Eraser_t1049249012, ___erasedStrokes_23)); }
	inline Queue_1_t3000609764 * get_erasedStrokes_23() const { return ___erasedStrokes_23; }
	inline Queue_1_t3000609764 ** get_address_of_erasedStrokes_23() { return &___erasedStrokes_23; }
	inline void set_erasedStrokes_23(Queue_1_t3000609764 * value)
	{
		___erasedStrokes_23 = value;
		Il2CppCodeGenWriteBarrier((&___erasedStrokes_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERASER_T1049249012_H
#ifndef LINE_T549989228_H
#define LINE_T549989228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.Line
struct  Line_t549989228  : public LineBase_t717918686
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.Design.Line::Start
	Vector3_t3722313464  ___Start_16;
	// UnityEngine.Vector3 HoloToolkit.Unity.Design.Line::End
	Vector3_t3722313464  ___End_17;

public:
	inline static int32_t get_offset_of_Start_16() { return static_cast<int32_t>(offsetof(Line_t549989228, ___Start_16)); }
	inline Vector3_t3722313464  get_Start_16() const { return ___Start_16; }
	inline Vector3_t3722313464 * get_address_of_Start_16() { return &___Start_16; }
	inline void set_Start_16(Vector3_t3722313464  value)
	{
		___Start_16 = value;
	}

	inline static int32_t get_offset_of_End_17() { return static_cast<int32_t>(offsetof(Line_t549989228, ___End_17)); }
	inline Vector3_t3722313464  get_End_17() const { return ___End_17; }
	inline Vector3_t3722313464 * get_address_of_End_17() { return &___End_17; }
	inline void set_End_17(Vector3_t3722313464  value)
	{
		___End_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINE_T549989228_H
#ifndef LINEUNITY_T4238357500_H
#define LINEUNITY_T4238357500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.LineUnity
struct  LineUnity_t4238357500  : public LineRenderer_t3025162641
{
public:
	// UnityEngine.Material HoloToolkit.Unity.Design.LineUnity::LineMaterial
	Material_t340375123 * ___LineMaterial_18;
	// System.Boolean HoloToolkit.Unity.Design.LineUnity::RoundedEdges
	bool ___RoundedEdges_19;
	// System.Boolean HoloToolkit.Unity.Design.LineUnity::RoundedCaps
	bool ___RoundedCaps_20;
	// UnityEngine.LineRenderer HoloToolkit.Unity.Design.LineUnity::lineRenderer
	LineRenderer_t3154350270 * ___lineRenderer_21;
	// UnityEngine.Vector3[] HoloToolkit.Unity.Design.LineUnity::positions
	Vector3U5BU5D_t1718750761* ___positions_22;

public:
	inline static int32_t get_offset_of_LineMaterial_18() { return static_cast<int32_t>(offsetof(LineUnity_t4238357500, ___LineMaterial_18)); }
	inline Material_t340375123 * get_LineMaterial_18() const { return ___LineMaterial_18; }
	inline Material_t340375123 ** get_address_of_LineMaterial_18() { return &___LineMaterial_18; }
	inline void set_LineMaterial_18(Material_t340375123 * value)
	{
		___LineMaterial_18 = value;
		Il2CppCodeGenWriteBarrier((&___LineMaterial_18), value);
	}

	inline static int32_t get_offset_of_RoundedEdges_19() { return static_cast<int32_t>(offsetof(LineUnity_t4238357500, ___RoundedEdges_19)); }
	inline bool get_RoundedEdges_19() const { return ___RoundedEdges_19; }
	inline bool* get_address_of_RoundedEdges_19() { return &___RoundedEdges_19; }
	inline void set_RoundedEdges_19(bool value)
	{
		___RoundedEdges_19 = value;
	}

	inline static int32_t get_offset_of_RoundedCaps_20() { return static_cast<int32_t>(offsetof(LineUnity_t4238357500, ___RoundedCaps_20)); }
	inline bool get_RoundedCaps_20() const { return ___RoundedCaps_20; }
	inline bool* get_address_of_RoundedCaps_20() { return &___RoundedCaps_20; }
	inline void set_RoundedCaps_20(bool value)
	{
		___RoundedCaps_20 = value;
	}

	inline static int32_t get_offset_of_lineRenderer_21() { return static_cast<int32_t>(offsetof(LineUnity_t4238357500, ___lineRenderer_21)); }
	inline LineRenderer_t3154350270 * get_lineRenderer_21() const { return ___lineRenderer_21; }
	inline LineRenderer_t3154350270 ** get_address_of_lineRenderer_21() { return &___lineRenderer_21; }
	inline void set_lineRenderer_21(LineRenderer_t3154350270 * value)
	{
		___lineRenderer_21 = value;
		Il2CppCodeGenWriteBarrier((&___lineRenderer_21), value);
	}

	inline static int32_t get_offset_of_positions_22() { return static_cast<int32_t>(offsetof(LineUnity_t4238357500, ___positions_22)); }
	inline Vector3U5BU5D_t1718750761* get_positions_22() const { return ___positions_22; }
	inline Vector3U5BU5D_t1718750761** get_address_of_positions_22() { return &___positions_22; }
	inline void set_positions_22(Vector3U5BU5D_t1718750761* value)
	{
		___positions_22 = value;
		Il2CppCodeGenWriteBarrier((&___positions_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEUNITY_T4238357500_H
#ifndef ELLIPSE_T4234545539_H
#define ELLIPSE_T4234545539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Design.Ellipse
struct  Ellipse_t4234545539  : public LineBase_t717918686
{
public:
	// System.Int32 HoloToolkit.Unity.Design.Ellipse::Resolution
	int32_t ___Resolution_17;
	// UnityEngine.Vector2 HoloToolkit.Unity.Design.Ellipse::Radius
	Vector2_t2156229523  ___Radius_18;

public:
	inline static int32_t get_offset_of_Resolution_17() { return static_cast<int32_t>(offsetof(Ellipse_t4234545539, ___Resolution_17)); }
	inline int32_t get_Resolution_17() const { return ___Resolution_17; }
	inline int32_t* get_address_of_Resolution_17() { return &___Resolution_17; }
	inline void set_Resolution_17(int32_t value)
	{
		___Resolution_17 = value;
	}

	inline static int32_t get_offset_of_Radius_18() { return static_cast<int32_t>(offsetof(Ellipse_t4234545539, ___Radius_18)); }
	inline Vector2_t2156229523  get_Radius_18() const { return ___Radius_18; }
	inline Vector2_t2156229523 * get_address_of_Radius_18() { return &___Radius_18; }
	inline void set_Radius_18(Vector2_t2156229523  value)
	{
		___Radius_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELLIPSE_T4234545539_H
#ifndef ATTACHTOCONTROLLER_T1213746185_H
#define ATTACHTOCONTROLLER_T1213746185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.AttachToController
struct  AttachToController_t1213746185  : public ControllerFinder_t3151550888
{
public:
	// System.Boolean HoloToolkit.Unity.InputModule.AttachToController::SetChildrenInactiveWhenDetached
	bool ___SetChildrenInactiveWhenDetached_6;
	// UnityEngine.Vector3 HoloToolkit.Unity.InputModule.AttachToController::PositionOffset
	Vector3_t3722313464  ___PositionOffset_7;
	// UnityEngine.Vector3 HoloToolkit.Unity.InputModule.AttachToController::RotationOffset
	Vector3_t3722313464  ___RotationOffset_8;
	// UnityEngine.Vector3 HoloToolkit.Unity.InputModule.AttachToController::ScaleOffset
	Vector3_t3722313464  ___ScaleOffset_9;
	// System.Boolean HoloToolkit.Unity.InputModule.AttachToController::SetScaleOnAttach
	bool ___SetScaleOnAttach_10;
	// System.Boolean HoloToolkit.Unity.InputModule.AttachToController::<IsAttached>k__BackingField
	bool ___U3CIsAttachedU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_SetChildrenInactiveWhenDetached_6() { return static_cast<int32_t>(offsetof(AttachToController_t1213746185, ___SetChildrenInactiveWhenDetached_6)); }
	inline bool get_SetChildrenInactiveWhenDetached_6() const { return ___SetChildrenInactiveWhenDetached_6; }
	inline bool* get_address_of_SetChildrenInactiveWhenDetached_6() { return &___SetChildrenInactiveWhenDetached_6; }
	inline void set_SetChildrenInactiveWhenDetached_6(bool value)
	{
		___SetChildrenInactiveWhenDetached_6 = value;
	}

	inline static int32_t get_offset_of_PositionOffset_7() { return static_cast<int32_t>(offsetof(AttachToController_t1213746185, ___PositionOffset_7)); }
	inline Vector3_t3722313464  get_PositionOffset_7() const { return ___PositionOffset_7; }
	inline Vector3_t3722313464 * get_address_of_PositionOffset_7() { return &___PositionOffset_7; }
	inline void set_PositionOffset_7(Vector3_t3722313464  value)
	{
		___PositionOffset_7 = value;
	}

	inline static int32_t get_offset_of_RotationOffset_8() { return static_cast<int32_t>(offsetof(AttachToController_t1213746185, ___RotationOffset_8)); }
	inline Vector3_t3722313464  get_RotationOffset_8() const { return ___RotationOffset_8; }
	inline Vector3_t3722313464 * get_address_of_RotationOffset_8() { return &___RotationOffset_8; }
	inline void set_RotationOffset_8(Vector3_t3722313464  value)
	{
		___RotationOffset_8 = value;
	}

	inline static int32_t get_offset_of_ScaleOffset_9() { return static_cast<int32_t>(offsetof(AttachToController_t1213746185, ___ScaleOffset_9)); }
	inline Vector3_t3722313464  get_ScaleOffset_9() const { return ___ScaleOffset_9; }
	inline Vector3_t3722313464 * get_address_of_ScaleOffset_9() { return &___ScaleOffset_9; }
	inline void set_ScaleOffset_9(Vector3_t3722313464  value)
	{
		___ScaleOffset_9 = value;
	}

	inline static int32_t get_offset_of_SetScaleOnAttach_10() { return static_cast<int32_t>(offsetof(AttachToController_t1213746185, ___SetScaleOnAttach_10)); }
	inline bool get_SetScaleOnAttach_10() const { return ___SetScaleOnAttach_10; }
	inline bool* get_address_of_SetScaleOnAttach_10() { return &___SetScaleOnAttach_10; }
	inline void set_SetScaleOnAttach_10(bool value)
	{
		___SetScaleOnAttach_10 = value;
	}

	inline static int32_t get_offset_of_U3CIsAttachedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AttachToController_t1213746185, ___U3CIsAttachedU3Ek__BackingField_11)); }
	inline bool get_U3CIsAttachedU3Ek__BackingField_11() const { return ___U3CIsAttachedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsAttachedU3Ek__BackingField_11() { return &___U3CIsAttachedU3Ek__BackingField_11; }
	inline void set_U3CIsAttachedU3Ek__BackingField_11(bool value)
	{
		___U3CIsAttachedU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHTOCONTROLLER_T1213746185_H
#ifndef LINEPOINTER_T114985340_H
#define LINEPOINTER_T114985340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Controllers.LinePointer
struct  LinePointer_t114985340  : public PhysicsPointer_t2887364969
{
public:
	// HoloToolkit.Unity.Design.Line HoloToolkit.Unity.Controllers.LinePointer::line
	Line_t549989228 * ___line_20;
	// HoloToolkit.Unity.Design.LineRenderer[] HoloToolkit.Unity.Controllers.LinePointer::renderers
	LineRendererU5BU5D_t2548926732* ___renderers_21;
	// System.Single HoloToolkit.Unity.Controllers.LinePointer::maxDistance
	float ___maxDistance_22;

public:
	inline static int32_t get_offset_of_line_20() { return static_cast<int32_t>(offsetof(LinePointer_t114985340, ___line_20)); }
	inline Line_t549989228 * get_line_20() const { return ___line_20; }
	inline Line_t549989228 ** get_address_of_line_20() { return &___line_20; }
	inline void set_line_20(Line_t549989228 * value)
	{
		___line_20 = value;
		Il2CppCodeGenWriteBarrier((&___line_20), value);
	}

	inline static int32_t get_offset_of_renderers_21() { return static_cast<int32_t>(offsetof(LinePointer_t114985340, ___renderers_21)); }
	inline LineRendererU5BU5D_t2548926732* get_renderers_21() const { return ___renderers_21; }
	inline LineRendererU5BU5D_t2548926732** get_address_of_renderers_21() { return &___renderers_21; }
	inline void set_renderers_21(LineRendererU5BU5D_t2548926732* value)
	{
		___renderers_21 = value;
		Il2CppCodeGenWriteBarrier((&___renderers_21), value);
	}

	inline static int32_t get_offset_of_maxDistance_22() { return static_cast<int32_t>(offsetof(LinePointer_t114985340, ___maxDistance_22)); }
	inline float get_maxDistance_22() const { return ___maxDistance_22; }
	inline float* get_address_of_maxDistance_22() { return &___maxDistance_22; }
	inline void set_maxDistance_22(float value)
	{
		___maxDistance_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEPOINTER_T114985340_H
#ifndef COLORPICKERWHEEL_T2235220782_H
#define COLORPICKERWHEEL_T2235220782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.ColorPickerWheel
struct  ColorPickerWheel_t2235220782  : public AttachToController_t1213746185
{
public:
	// System.Boolean HoloToolkit.Unity.ControllerExamples.ColorPickerWheel::visible
	bool ___visible_12;
	// UnityEngine.Transform HoloToolkit.Unity.ControllerExamples.ColorPickerWheel::selectorTransform
	Transform_t3600365921 * ___selectorTransform_13;
	// UnityEngine.Renderer HoloToolkit.Unity.ControllerExamples.ColorPickerWheel::selectorRenderer
	Renderer_t2627027031 * ___selectorRenderer_14;
	// System.Single HoloToolkit.Unity.ControllerExamples.ColorPickerWheel::inputScale
	float ___inputScale_15;
	// UnityEngine.Color HoloToolkit.Unity.ControllerExamples.ColorPickerWheel::selectedColor
	Color_t2555686324  ___selectedColor_16;
	// UnityEngine.Texture2D HoloToolkit.Unity.ControllerExamples.ColorPickerWheel::colorWheelTexture
	Texture2D_t3840446185 * ___colorWheelTexture_17;
	// UnityEngine.GameObject HoloToolkit.Unity.ControllerExamples.ColorPickerWheel::colorWheelObject
	GameObject_t1113636619 * ___colorWheelObject_18;
	// UnityEngine.Animator HoloToolkit.Unity.ControllerExamples.ColorPickerWheel::animator
	Animator_t434523843 * ___animator_19;
	// System.Single HoloToolkit.Unity.ControllerExamples.ColorPickerWheel::timeout
	float ___timeout_20;
	// UnityEngine.Vector2 HoloToolkit.Unity.ControllerExamples.ColorPickerWheel::selectorPosition
	Vector2_t2156229523  ___selectorPosition_21;
	// System.Single HoloToolkit.Unity.ControllerExamples.ColorPickerWheel::lastTimeVisible
	float ___lastTimeVisible_22;
	// System.Boolean HoloToolkit.Unity.ControllerExamples.ColorPickerWheel::visibleLastFrame
	bool ___visibleLastFrame_23;

public:
	inline static int32_t get_offset_of_visible_12() { return static_cast<int32_t>(offsetof(ColorPickerWheel_t2235220782, ___visible_12)); }
	inline bool get_visible_12() const { return ___visible_12; }
	inline bool* get_address_of_visible_12() { return &___visible_12; }
	inline void set_visible_12(bool value)
	{
		___visible_12 = value;
	}

	inline static int32_t get_offset_of_selectorTransform_13() { return static_cast<int32_t>(offsetof(ColorPickerWheel_t2235220782, ___selectorTransform_13)); }
	inline Transform_t3600365921 * get_selectorTransform_13() const { return ___selectorTransform_13; }
	inline Transform_t3600365921 ** get_address_of_selectorTransform_13() { return &___selectorTransform_13; }
	inline void set_selectorTransform_13(Transform_t3600365921 * value)
	{
		___selectorTransform_13 = value;
		Il2CppCodeGenWriteBarrier((&___selectorTransform_13), value);
	}

	inline static int32_t get_offset_of_selectorRenderer_14() { return static_cast<int32_t>(offsetof(ColorPickerWheel_t2235220782, ___selectorRenderer_14)); }
	inline Renderer_t2627027031 * get_selectorRenderer_14() const { return ___selectorRenderer_14; }
	inline Renderer_t2627027031 ** get_address_of_selectorRenderer_14() { return &___selectorRenderer_14; }
	inline void set_selectorRenderer_14(Renderer_t2627027031 * value)
	{
		___selectorRenderer_14 = value;
		Il2CppCodeGenWriteBarrier((&___selectorRenderer_14), value);
	}

	inline static int32_t get_offset_of_inputScale_15() { return static_cast<int32_t>(offsetof(ColorPickerWheel_t2235220782, ___inputScale_15)); }
	inline float get_inputScale_15() const { return ___inputScale_15; }
	inline float* get_address_of_inputScale_15() { return &___inputScale_15; }
	inline void set_inputScale_15(float value)
	{
		___inputScale_15 = value;
	}

	inline static int32_t get_offset_of_selectedColor_16() { return static_cast<int32_t>(offsetof(ColorPickerWheel_t2235220782, ___selectedColor_16)); }
	inline Color_t2555686324  get_selectedColor_16() const { return ___selectedColor_16; }
	inline Color_t2555686324 * get_address_of_selectedColor_16() { return &___selectedColor_16; }
	inline void set_selectedColor_16(Color_t2555686324  value)
	{
		___selectedColor_16 = value;
	}

	inline static int32_t get_offset_of_colorWheelTexture_17() { return static_cast<int32_t>(offsetof(ColorPickerWheel_t2235220782, ___colorWheelTexture_17)); }
	inline Texture2D_t3840446185 * get_colorWheelTexture_17() const { return ___colorWheelTexture_17; }
	inline Texture2D_t3840446185 ** get_address_of_colorWheelTexture_17() { return &___colorWheelTexture_17; }
	inline void set_colorWheelTexture_17(Texture2D_t3840446185 * value)
	{
		___colorWheelTexture_17 = value;
		Il2CppCodeGenWriteBarrier((&___colorWheelTexture_17), value);
	}

	inline static int32_t get_offset_of_colorWheelObject_18() { return static_cast<int32_t>(offsetof(ColorPickerWheel_t2235220782, ___colorWheelObject_18)); }
	inline GameObject_t1113636619 * get_colorWheelObject_18() const { return ___colorWheelObject_18; }
	inline GameObject_t1113636619 ** get_address_of_colorWheelObject_18() { return &___colorWheelObject_18; }
	inline void set_colorWheelObject_18(GameObject_t1113636619 * value)
	{
		___colorWheelObject_18 = value;
		Il2CppCodeGenWriteBarrier((&___colorWheelObject_18), value);
	}

	inline static int32_t get_offset_of_animator_19() { return static_cast<int32_t>(offsetof(ColorPickerWheel_t2235220782, ___animator_19)); }
	inline Animator_t434523843 * get_animator_19() const { return ___animator_19; }
	inline Animator_t434523843 ** get_address_of_animator_19() { return &___animator_19; }
	inline void set_animator_19(Animator_t434523843 * value)
	{
		___animator_19 = value;
		Il2CppCodeGenWriteBarrier((&___animator_19), value);
	}

	inline static int32_t get_offset_of_timeout_20() { return static_cast<int32_t>(offsetof(ColorPickerWheel_t2235220782, ___timeout_20)); }
	inline float get_timeout_20() const { return ___timeout_20; }
	inline float* get_address_of_timeout_20() { return &___timeout_20; }
	inline void set_timeout_20(float value)
	{
		___timeout_20 = value;
	}

	inline static int32_t get_offset_of_selectorPosition_21() { return static_cast<int32_t>(offsetof(ColorPickerWheel_t2235220782, ___selectorPosition_21)); }
	inline Vector2_t2156229523  get_selectorPosition_21() const { return ___selectorPosition_21; }
	inline Vector2_t2156229523 * get_address_of_selectorPosition_21() { return &___selectorPosition_21; }
	inline void set_selectorPosition_21(Vector2_t2156229523  value)
	{
		___selectorPosition_21 = value;
	}

	inline static int32_t get_offset_of_lastTimeVisible_22() { return static_cast<int32_t>(offsetof(ColorPickerWheel_t2235220782, ___lastTimeVisible_22)); }
	inline float get_lastTimeVisible_22() const { return ___lastTimeVisible_22; }
	inline float* get_address_of_lastTimeVisible_22() { return &___lastTimeVisible_22; }
	inline void set_lastTimeVisible_22(float value)
	{
		___lastTimeVisible_22 = value;
	}

	inline static int32_t get_offset_of_visibleLastFrame_23() { return static_cast<int32_t>(offsetof(ColorPickerWheel_t2235220782, ___visibleLastFrame_23)); }
	inline bool get_visibleLastFrame_23() const { return ___visibleLastFrame_23; }
	inline bool* get_address_of_visibleLastFrame_23() { return &___visibleLastFrame_23; }
	inline void set_visibleLastFrame_23(bool value)
	{
		___visibleLastFrame_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKERWHEEL_T2235220782_H
#ifndef BRUSHSELECTOR_T2086558127_H
#define BRUSHSELECTOR_T2086558127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.BrushSelector
struct  BrushSelector_t2086558127  : public AttachToController_t1213746185
{
public:
	// HoloToolkit.Unity.Design.LineObjectCollection HoloToolkit.Unity.ControllerExamples.BrushSelector::brushCollection
	LineObjectCollection_t2817952665 * ___brushCollection_12;
	// HoloToolkit.Unity.ControllerExamples.BrushSelector/SwipeEnum HoloToolkit.Unity.ControllerExamples.BrushSelector::currentAction
	int32_t ___currentAction_13;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.ControllerExamples.BrushSelector::swipeCurve
	AnimationCurve_t3046754366 * ___swipeCurve_14;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushSelector::swipeDuration
	float ___swipeDuration_15;
	// System.Int32 HoloToolkit.Unity.ControllerExamples.BrushSelector::displayBrushindex
	int32_t ___displayBrushindex_16;
	// System.Int32 HoloToolkit.Unity.ControllerExamples.BrushSelector::activeBrushindex
	int32_t ___activeBrushindex_17;
	// HoloToolkit.Unity.ControllerExamples.ColorPickerWheel HoloToolkit.Unity.ControllerExamples.BrushSelector::colorPicker
	ColorPickerWheel_t2235220782 * ___colorPicker_18;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushSelector::menuTimeout
	float ___menuTimeout_19;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushSelector::selectPressedDrawThreshold
	float ___selectPressedDrawThreshold_20;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushSelector::selectPressedStartValue
	float ___selectPressedStartValue_21;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.ControllerExamples.BrushSelector::selectWidthCurve
	AnimationCurve_t3046754366 * ___selectWidthCurve_22;
	// UnityEngine.Material HoloToolkit.Unity.ControllerExamples.BrushSelector::touchpadMaterial
	Material_t340375123 * ___touchpadMaterial_23;
	// UnityEngine.Gradient HoloToolkit.Unity.ControllerExamples.BrushSelector::touchpadColor
	Gradient_t3067099924 * ___touchpadColor_24;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushSelector::touchpadGlowLossTime
	float ___touchpadGlowLossTime_25;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushSelector::touchpadTouchTime
	float ___touchpadTouchTime_26;
	// UnityEngine.MeshRenderer HoloToolkit.Unity.ControllerExamples.BrushSelector::touchpadRenderer
	MeshRenderer_t587009260 * ___touchpadRenderer_27;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushSelector::menuOpenTime
	float ___menuOpenTime_28;
	// System.Boolean HoloToolkit.Unity.ControllerExamples.BrushSelector::menuOpen
	bool ___menuOpen_29;
	// System.Boolean HoloToolkit.Unity.ControllerExamples.BrushSelector::switchingBrush
	bool ___switchingBrush_30;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushSelector::startOffset
	float ___startOffset_31;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushSelector::targetOffset
	float ___targetOffset_32;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushSelector::startTime
	float ___startTime_33;
	// System.Boolean HoloToolkit.Unity.ControllerExamples.BrushSelector::resetInput
	bool ___resetInput_34;
	// System.Single HoloToolkit.Unity.ControllerExamples.BrushSelector::selectPressedSmooth
	float ___selectPressedSmooth_35;
	// HoloToolkit.Unity.ControllerExamples.Brush HoloToolkit.Unity.ControllerExamples.BrushSelector::activeBrush
	Brush_t1548626474 * ___activeBrush_36;
	// UnityEngine.Material HoloToolkit.Unity.ControllerExamples.BrushSelector::originalTouchpadMaterial
	Material_t340375123 * ___originalTouchpadMaterial_37;

public:
	inline static int32_t get_offset_of_brushCollection_12() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___brushCollection_12)); }
	inline LineObjectCollection_t2817952665 * get_brushCollection_12() const { return ___brushCollection_12; }
	inline LineObjectCollection_t2817952665 ** get_address_of_brushCollection_12() { return &___brushCollection_12; }
	inline void set_brushCollection_12(LineObjectCollection_t2817952665 * value)
	{
		___brushCollection_12 = value;
		Il2CppCodeGenWriteBarrier((&___brushCollection_12), value);
	}

	inline static int32_t get_offset_of_currentAction_13() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___currentAction_13)); }
	inline int32_t get_currentAction_13() const { return ___currentAction_13; }
	inline int32_t* get_address_of_currentAction_13() { return &___currentAction_13; }
	inline void set_currentAction_13(int32_t value)
	{
		___currentAction_13 = value;
	}

	inline static int32_t get_offset_of_swipeCurve_14() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___swipeCurve_14)); }
	inline AnimationCurve_t3046754366 * get_swipeCurve_14() const { return ___swipeCurve_14; }
	inline AnimationCurve_t3046754366 ** get_address_of_swipeCurve_14() { return &___swipeCurve_14; }
	inline void set_swipeCurve_14(AnimationCurve_t3046754366 * value)
	{
		___swipeCurve_14 = value;
		Il2CppCodeGenWriteBarrier((&___swipeCurve_14), value);
	}

	inline static int32_t get_offset_of_swipeDuration_15() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___swipeDuration_15)); }
	inline float get_swipeDuration_15() const { return ___swipeDuration_15; }
	inline float* get_address_of_swipeDuration_15() { return &___swipeDuration_15; }
	inline void set_swipeDuration_15(float value)
	{
		___swipeDuration_15 = value;
	}

	inline static int32_t get_offset_of_displayBrushindex_16() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___displayBrushindex_16)); }
	inline int32_t get_displayBrushindex_16() const { return ___displayBrushindex_16; }
	inline int32_t* get_address_of_displayBrushindex_16() { return &___displayBrushindex_16; }
	inline void set_displayBrushindex_16(int32_t value)
	{
		___displayBrushindex_16 = value;
	}

	inline static int32_t get_offset_of_activeBrushindex_17() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___activeBrushindex_17)); }
	inline int32_t get_activeBrushindex_17() const { return ___activeBrushindex_17; }
	inline int32_t* get_address_of_activeBrushindex_17() { return &___activeBrushindex_17; }
	inline void set_activeBrushindex_17(int32_t value)
	{
		___activeBrushindex_17 = value;
	}

	inline static int32_t get_offset_of_colorPicker_18() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___colorPicker_18)); }
	inline ColorPickerWheel_t2235220782 * get_colorPicker_18() const { return ___colorPicker_18; }
	inline ColorPickerWheel_t2235220782 ** get_address_of_colorPicker_18() { return &___colorPicker_18; }
	inline void set_colorPicker_18(ColorPickerWheel_t2235220782 * value)
	{
		___colorPicker_18 = value;
		Il2CppCodeGenWriteBarrier((&___colorPicker_18), value);
	}

	inline static int32_t get_offset_of_menuTimeout_19() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___menuTimeout_19)); }
	inline float get_menuTimeout_19() const { return ___menuTimeout_19; }
	inline float* get_address_of_menuTimeout_19() { return &___menuTimeout_19; }
	inline void set_menuTimeout_19(float value)
	{
		___menuTimeout_19 = value;
	}

	inline static int32_t get_offset_of_selectPressedDrawThreshold_20() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___selectPressedDrawThreshold_20)); }
	inline float get_selectPressedDrawThreshold_20() const { return ___selectPressedDrawThreshold_20; }
	inline float* get_address_of_selectPressedDrawThreshold_20() { return &___selectPressedDrawThreshold_20; }
	inline void set_selectPressedDrawThreshold_20(float value)
	{
		___selectPressedDrawThreshold_20 = value;
	}

	inline static int32_t get_offset_of_selectPressedStartValue_21() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___selectPressedStartValue_21)); }
	inline float get_selectPressedStartValue_21() const { return ___selectPressedStartValue_21; }
	inline float* get_address_of_selectPressedStartValue_21() { return &___selectPressedStartValue_21; }
	inline void set_selectPressedStartValue_21(float value)
	{
		___selectPressedStartValue_21 = value;
	}

	inline static int32_t get_offset_of_selectWidthCurve_22() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___selectWidthCurve_22)); }
	inline AnimationCurve_t3046754366 * get_selectWidthCurve_22() const { return ___selectWidthCurve_22; }
	inline AnimationCurve_t3046754366 ** get_address_of_selectWidthCurve_22() { return &___selectWidthCurve_22; }
	inline void set_selectWidthCurve_22(AnimationCurve_t3046754366 * value)
	{
		___selectWidthCurve_22 = value;
		Il2CppCodeGenWriteBarrier((&___selectWidthCurve_22), value);
	}

	inline static int32_t get_offset_of_touchpadMaterial_23() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___touchpadMaterial_23)); }
	inline Material_t340375123 * get_touchpadMaterial_23() const { return ___touchpadMaterial_23; }
	inline Material_t340375123 ** get_address_of_touchpadMaterial_23() { return &___touchpadMaterial_23; }
	inline void set_touchpadMaterial_23(Material_t340375123 * value)
	{
		___touchpadMaterial_23 = value;
		Il2CppCodeGenWriteBarrier((&___touchpadMaterial_23), value);
	}

	inline static int32_t get_offset_of_touchpadColor_24() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___touchpadColor_24)); }
	inline Gradient_t3067099924 * get_touchpadColor_24() const { return ___touchpadColor_24; }
	inline Gradient_t3067099924 ** get_address_of_touchpadColor_24() { return &___touchpadColor_24; }
	inline void set_touchpadColor_24(Gradient_t3067099924 * value)
	{
		___touchpadColor_24 = value;
		Il2CppCodeGenWriteBarrier((&___touchpadColor_24), value);
	}

	inline static int32_t get_offset_of_touchpadGlowLossTime_25() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___touchpadGlowLossTime_25)); }
	inline float get_touchpadGlowLossTime_25() const { return ___touchpadGlowLossTime_25; }
	inline float* get_address_of_touchpadGlowLossTime_25() { return &___touchpadGlowLossTime_25; }
	inline void set_touchpadGlowLossTime_25(float value)
	{
		___touchpadGlowLossTime_25 = value;
	}

	inline static int32_t get_offset_of_touchpadTouchTime_26() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___touchpadTouchTime_26)); }
	inline float get_touchpadTouchTime_26() const { return ___touchpadTouchTime_26; }
	inline float* get_address_of_touchpadTouchTime_26() { return &___touchpadTouchTime_26; }
	inline void set_touchpadTouchTime_26(float value)
	{
		___touchpadTouchTime_26 = value;
	}

	inline static int32_t get_offset_of_touchpadRenderer_27() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___touchpadRenderer_27)); }
	inline MeshRenderer_t587009260 * get_touchpadRenderer_27() const { return ___touchpadRenderer_27; }
	inline MeshRenderer_t587009260 ** get_address_of_touchpadRenderer_27() { return &___touchpadRenderer_27; }
	inline void set_touchpadRenderer_27(MeshRenderer_t587009260 * value)
	{
		___touchpadRenderer_27 = value;
		Il2CppCodeGenWriteBarrier((&___touchpadRenderer_27), value);
	}

	inline static int32_t get_offset_of_menuOpenTime_28() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___menuOpenTime_28)); }
	inline float get_menuOpenTime_28() const { return ___menuOpenTime_28; }
	inline float* get_address_of_menuOpenTime_28() { return &___menuOpenTime_28; }
	inline void set_menuOpenTime_28(float value)
	{
		___menuOpenTime_28 = value;
	}

	inline static int32_t get_offset_of_menuOpen_29() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___menuOpen_29)); }
	inline bool get_menuOpen_29() const { return ___menuOpen_29; }
	inline bool* get_address_of_menuOpen_29() { return &___menuOpen_29; }
	inline void set_menuOpen_29(bool value)
	{
		___menuOpen_29 = value;
	}

	inline static int32_t get_offset_of_switchingBrush_30() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___switchingBrush_30)); }
	inline bool get_switchingBrush_30() const { return ___switchingBrush_30; }
	inline bool* get_address_of_switchingBrush_30() { return &___switchingBrush_30; }
	inline void set_switchingBrush_30(bool value)
	{
		___switchingBrush_30 = value;
	}

	inline static int32_t get_offset_of_startOffset_31() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___startOffset_31)); }
	inline float get_startOffset_31() const { return ___startOffset_31; }
	inline float* get_address_of_startOffset_31() { return &___startOffset_31; }
	inline void set_startOffset_31(float value)
	{
		___startOffset_31 = value;
	}

	inline static int32_t get_offset_of_targetOffset_32() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___targetOffset_32)); }
	inline float get_targetOffset_32() const { return ___targetOffset_32; }
	inline float* get_address_of_targetOffset_32() { return &___targetOffset_32; }
	inline void set_targetOffset_32(float value)
	{
		___targetOffset_32 = value;
	}

	inline static int32_t get_offset_of_startTime_33() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___startTime_33)); }
	inline float get_startTime_33() const { return ___startTime_33; }
	inline float* get_address_of_startTime_33() { return &___startTime_33; }
	inline void set_startTime_33(float value)
	{
		___startTime_33 = value;
	}

	inline static int32_t get_offset_of_resetInput_34() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___resetInput_34)); }
	inline bool get_resetInput_34() const { return ___resetInput_34; }
	inline bool* get_address_of_resetInput_34() { return &___resetInput_34; }
	inline void set_resetInput_34(bool value)
	{
		___resetInput_34 = value;
	}

	inline static int32_t get_offset_of_selectPressedSmooth_35() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___selectPressedSmooth_35)); }
	inline float get_selectPressedSmooth_35() const { return ___selectPressedSmooth_35; }
	inline float* get_address_of_selectPressedSmooth_35() { return &___selectPressedSmooth_35; }
	inline void set_selectPressedSmooth_35(float value)
	{
		___selectPressedSmooth_35 = value;
	}

	inline static int32_t get_offset_of_activeBrush_36() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___activeBrush_36)); }
	inline Brush_t1548626474 * get_activeBrush_36() const { return ___activeBrush_36; }
	inline Brush_t1548626474 ** get_address_of_activeBrush_36() { return &___activeBrush_36; }
	inline void set_activeBrush_36(Brush_t1548626474 * value)
	{
		___activeBrush_36 = value;
		Il2CppCodeGenWriteBarrier((&___activeBrush_36), value);
	}

	inline static int32_t get_offset_of_originalTouchpadMaterial_37() { return static_cast<int32_t>(offsetof(BrushSelector_t2086558127, ___originalTouchpadMaterial_37)); }
	inline Material_t340375123 * get_originalTouchpadMaterial_37() const { return ___originalTouchpadMaterial_37; }
	inline Material_t340375123 ** get_address_of_originalTouchpadMaterial_37() { return &___originalTouchpadMaterial_37; }
	inline void set_originalTouchpadMaterial_37(Material_t340375123 * value)
	{
		___originalTouchpadMaterial_37 = value;
		Il2CppCodeGenWriteBarrier((&___originalTouchpadMaterial_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRUSHSELECTOR_T2086558127_H
#ifndef POINTERINPUT_T2201074199_H
#define POINTERINPUT_T2201074199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Controllers.PointerInput
struct  PointerInput_t2201074199  : public AttachToController_t1213746185
{
public:
	// HoloToolkit.Unity.Controllers.PhysicsPointer HoloToolkit.Unity.Controllers.PointerInput::pointer
	PhysicsPointer_t2887364969 * ___pointer_12;
	// UnityEngine.XR.WSA.Input.InteractionSourcePressType HoloToolkit.Unity.Controllers.PointerInput::activePressType
	int32_t ___activePressType_13;

public:
	inline static int32_t get_offset_of_pointer_12() { return static_cast<int32_t>(offsetof(PointerInput_t2201074199, ___pointer_12)); }
	inline PhysicsPointer_t2887364969 * get_pointer_12() const { return ___pointer_12; }
	inline PhysicsPointer_t2887364969 ** get_address_of_pointer_12() { return &___pointer_12; }
	inline void set_pointer_12(PhysicsPointer_t2887364969 * value)
	{
		___pointer_12 = value;
		Il2CppCodeGenWriteBarrier((&___pointer_12), value);
	}

	inline static int32_t get_offset_of_activePressType_13() { return static_cast<int32_t>(offsetof(PointerInput_t2201074199, ___activePressType_13)); }
	inline int32_t get_activePressType_13() const { return ___activePressType_13; }
	inline int32_t* get_address_of_activePressType_13() { return &___activePressType_13; }
	inline void set_activePressType_13(int32_t value)
	{
		___activePressType_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERINPUT_T2201074199_H
#ifndef OBJECTSPAWNER_T3925816580_H
#define OBJECTSPAWNER_T3925816580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ControllerExamples.ObjectSpawner
struct  ObjectSpawner_t3925816580  : public AttachToController_t1213746185
{
public:
	// UnityEngine.Transform HoloToolkit.Unity.ControllerExamples.ObjectSpawner::displayParent
	Transform_t3600365921 * ___displayParent_12;
	// UnityEngine.Transform HoloToolkit.Unity.ControllerExamples.ObjectSpawner::scaleParent
	Transform_t3600365921 * ___scaleParent_13;
	// UnityEngine.Transform HoloToolkit.Unity.ControllerExamples.ObjectSpawner::spawnParent
	Transform_t3600365921 * ___spawnParent_14;
	// UnityEngine.MeshFilter HoloToolkit.Unity.ControllerExamples.ObjectSpawner::displayObject
	MeshFilter_t3523625662 * ___displayObject_15;
	// UnityEngine.Material HoloToolkit.Unity.ControllerExamples.ObjectSpawner::objectMaterial
	Material_t340375123 * ___objectMaterial_16;
	// HoloToolkit.Unity.ControllerExamples.ColorPickerWheel HoloToolkit.Unity.ControllerExamples.ObjectSpawner::colorSource
	ColorPickerWheel_t2235220782 * ___colorSource_17;
	// UnityEngine.Mesh[] HoloToolkit.Unity.ControllerExamples.ObjectSpawner::availableMeshes
	MeshU5BU5D_t3972987605* ___availableMeshes_18;
	// UnityEngine.Animator HoloToolkit.Unity.ControllerExamples.ObjectSpawner::animator
	Animator_t434523843 * ___animator_19;
	// System.Int32 HoloToolkit.Unity.ControllerExamples.ObjectSpawner::meshIndex
	int32_t ___meshIndex_20;
	// HoloToolkit.Unity.ControllerExamples.ObjectSpawner/StateEnum HoloToolkit.Unity.ControllerExamples.ObjectSpawner::state
	int32_t ___state_21;
	// UnityEngine.Material HoloToolkit.Unity.ControllerExamples.ObjectSpawner::instantiatedMaterial
	Material_t340375123 * ___instantiatedMaterial_22;
	// System.Boolean HoloToolkit.Unity.ControllerExamples.ObjectSpawner::MicOn
	bool ___MicOn_23;

public:
	inline static int32_t get_offset_of_displayParent_12() { return static_cast<int32_t>(offsetof(ObjectSpawner_t3925816580, ___displayParent_12)); }
	inline Transform_t3600365921 * get_displayParent_12() const { return ___displayParent_12; }
	inline Transform_t3600365921 ** get_address_of_displayParent_12() { return &___displayParent_12; }
	inline void set_displayParent_12(Transform_t3600365921 * value)
	{
		___displayParent_12 = value;
		Il2CppCodeGenWriteBarrier((&___displayParent_12), value);
	}

	inline static int32_t get_offset_of_scaleParent_13() { return static_cast<int32_t>(offsetof(ObjectSpawner_t3925816580, ___scaleParent_13)); }
	inline Transform_t3600365921 * get_scaleParent_13() const { return ___scaleParent_13; }
	inline Transform_t3600365921 ** get_address_of_scaleParent_13() { return &___scaleParent_13; }
	inline void set_scaleParent_13(Transform_t3600365921 * value)
	{
		___scaleParent_13 = value;
		Il2CppCodeGenWriteBarrier((&___scaleParent_13), value);
	}

	inline static int32_t get_offset_of_spawnParent_14() { return static_cast<int32_t>(offsetof(ObjectSpawner_t3925816580, ___spawnParent_14)); }
	inline Transform_t3600365921 * get_spawnParent_14() const { return ___spawnParent_14; }
	inline Transform_t3600365921 ** get_address_of_spawnParent_14() { return &___spawnParent_14; }
	inline void set_spawnParent_14(Transform_t3600365921 * value)
	{
		___spawnParent_14 = value;
		Il2CppCodeGenWriteBarrier((&___spawnParent_14), value);
	}

	inline static int32_t get_offset_of_displayObject_15() { return static_cast<int32_t>(offsetof(ObjectSpawner_t3925816580, ___displayObject_15)); }
	inline MeshFilter_t3523625662 * get_displayObject_15() const { return ___displayObject_15; }
	inline MeshFilter_t3523625662 ** get_address_of_displayObject_15() { return &___displayObject_15; }
	inline void set_displayObject_15(MeshFilter_t3523625662 * value)
	{
		___displayObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___displayObject_15), value);
	}

	inline static int32_t get_offset_of_objectMaterial_16() { return static_cast<int32_t>(offsetof(ObjectSpawner_t3925816580, ___objectMaterial_16)); }
	inline Material_t340375123 * get_objectMaterial_16() const { return ___objectMaterial_16; }
	inline Material_t340375123 ** get_address_of_objectMaterial_16() { return &___objectMaterial_16; }
	inline void set_objectMaterial_16(Material_t340375123 * value)
	{
		___objectMaterial_16 = value;
		Il2CppCodeGenWriteBarrier((&___objectMaterial_16), value);
	}

	inline static int32_t get_offset_of_colorSource_17() { return static_cast<int32_t>(offsetof(ObjectSpawner_t3925816580, ___colorSource_17)); }
	inline ColorPickerWheel_t2235220782 * get_colorSource_17() const { return ___colorSource_17; }
	inline ColorPickerWheel_t2235220782 ** get_address_of_colorSource_17() { return &___colorSource_17; }
	inline void set_colorSource_17(ColorPickerWheel_t2235220782 * value)
	{
		___colorSource_17 = value;
		Il2CppCodeGenWriteBarrier((&___colorSource_17), value);
	}

	inline static int32_t get_offset_of_availableMeshes_18() { return static_cast<int32_t>(offsetof(ObjectSpawner_t3925816580, ___availableMeshes_18)); }
	inline MeshU5BU5D_t3972987605* get_availableMeshes_18() const { return ___availableMeshes_18; }
	inline MeshU5BU5D_t3972987605** get_address_of_availableMeshes_18() { return &___availableMeshes_18; }
	inline void set_availableMeshes_18(MeshU5BU5D_t3972987605* value)
	{
		___availableMeshes_18 = value;
		Il2CppCodeGenWriteBarrier((&___availableMeshes_18), value);
	}

	inline static int32_t get_offset_of_animator_19() { return static_cast<int32_t>(offsetof(ObjectSpawner_t3925816580, ___animator_19)); }
	inline Animator_t434523843 * get_animator_19() const { return ___animator_19; }
	inline Animator_t434523843 ** get_address_of_animator_19() { return &___animator_19; }
	inline void set_animator_19(Animator_t434523843 * value)
	{
		___animator_19 = value;
		Il2CppCodeGenWriteBarrier((&___animator_19), value);
	}

	inline static int32_t get_offset_of_meshIndex_20() { return static_cast<int32_t>(offsetof(ObjectSpawner_t3925816580, ___meshIndex_20)); }
	inline int32_t get_meshIndex_20() const { return ___meshIndex_20; }
	inline int32_t* get_address_of_meshIndex_20() { return &___meshIndex_20; }
	inline void set_meshIndex_20(int32_t value)
	{
		___meshIndex_20 = value;
	}

	inline static int32_t get_offset_of_state_21() { return static_cast<int32_t>(offsetof(ObjectSpawner_t3925816580, ___state_21)); }
	inline int32_t get_state_21() const { return ___state_21; }
	inline int32_t* get_address_of_state_21() { return &___state_21; }
	inline void set_state_21(int32_t value)
	{
		___state_21 = value;
	}

	inline static int32_t get_offset_of_instantiatedMaterial_22() { return static_cast<int32_t>(offsetof(ObjectSpawner_t3925816580, ___instantiatedMaterial_22)); }
	inline Material_t340375123 * get_instantiatedMaterial_22() const { return ___instantiatedMaterial_22; }
	inline Material_t340375123 ** get_address_of_instantiatedMaterial_22() { return &___instantiatedMaterial_22; }
	inline void set_instantiatedMaterial_22(Material_t340375123 * value)
	{
		___instantiatedMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___instantiatedMaterial_22), value);
	}

	inline static int32_t get_offset_of_MicOn_23() { return static_cast<int32_t>(offsetof(ObjectSpawner_t3925816580, ___MicOn_23)); }
	inline bool get_MicOn_23() const { return ___MicOn_23; }
	inline bool* get_address_of_MicOn_23() { return &___MicOn_23; }
	inline void set_MicOn_23(bool value)
	{
		___MicOn_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSPAWNER_T3925816580_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6000 = { sizeof (ControllerElementEnum_t4108764020)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6000[8] = 
{
	ControllerElementEnum_t4108764020::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6001 = { sizeof (MotionControllerVisualizer_t392243420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6001[13] = 
{
	MotionControllerVisualizer_t392243420::get_offset_of_AnimateControllerModel_4(),
	MotionControllerVisualizer_t392243420::get_offset_of_AlwaysUseAlternateLeftModel_5(),
	MotionControllerVisualizer_t392243420::get_offset_of_AlwaysUseAlternateRightModel_6(),
	MotionControllerVisualizer_t392243420::get_offset_of_AlternateLeftController_7(),
	MotionControllerVisualizer_t392243420::get_offset_of_AlternateRightController_8(),
	MotionControllerVisualizer_t392243420::get_offset_of_TouchpadTouchedOverride_9(),
	MotionControllerVisualizer_t392243420::get_offset_of_GLTFMaterial_10(),
	MotionControllerVisualizer_t392243420::get_offset_of_controllerDictionary_11(),
	MotionControllerVisualizer_t392243420::get_offset_of_loadingControllers_12(),
	MotionControllerVisualizer_t392243420::get_offset_of_leftControllerModel_13(),
	MotionControllerVisualizer_t392243420::get_offset_of_rightControllerModel_14(),
	MotionControllerVisualizer_t392243420::get_offset_of_OnControllerModelLoaded_15(),
	MotionControllerVisualizer_t392243420::get_offset_of_OnControllerModelUnloaded_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6002 = { sizeof (U3CLoadControllerModelU3Ed__27_t1961351108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6002[4] = 
{
	U3CLoadControllerModelU3Ed__27_t1961351108::get_offset_of_U3CU3E1__state_0(),
	U3CLoadControllerModelU3Ed__27_t1961351108::get_offset_of_U3CU3E2__current_1(),
	U3CLoadControllerModelU3Ed__27_t1961351108::get_offset_of_source_2(),
	U3CLoadControllerModelU3Ed__27_t1961351108::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6003 = { sizeof (U3CLoadSourceControllerModelU3Ed__28_t2257991569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6003[11] = 
{
	U3CLoadSourceControllerModelU3Ed__28_t2257991569::get_offset_of_U3CU3E1__state_0(),
	U3CLoadSourceControllerModelU3Ed__28_t2257991569::get_offset_of_U3CU3E2__current_1(),
	U3CLoadSourceControllerModelU3Ed__28_t2257991569::get_offset_of_source_2(),
	U3CLoadSourceControllerModelU3Ed__28_t2257991569::get_offset_of_U3CU3E4__this_3(),
	U3CLoadSourceControllerModelU3Ed__28_t2257991569::get_offset_of_U3CfileBytesU3E5__1_4(),
	U3CLoadSourceControllerModelU3Ed__28_t2257991569::get_offset_of_U3CcontrollerModelGameObjectU3E5__2_5(),
	U3CLoadSourceControllerModelU3Ed__28_t2257991569::get_offset_of_U3CmodelTaskU3E5__3_6(),
	U3CLoadSourceControllerModelU3Ed__28_t2257991569::get_offset_of_U3CmodelStreamU3E5__4_7(),
	U3CLoadSourceControllerModelU3Ed__28_t2257991569::get_offset_of_U3CgltfScriptU3E5__5_8(),
	U3CLoadSourceControllerModelU3Ed__28_t2257991569::get_offset_of_U3CreaderU3E5__6_9(),
	U3CLoadSourceControllerModelU3Ed__28_t2257991569::get_offset_of_U3CloadModelOpU3E5__7_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6004 = { sizeof (SetGlobalListener_t2628677908), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6005 = { sizeof (BoundaryManager_t2608055002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6005[8] = 
{
	BoundaryManager_t2608055002::get_offset_of_FloorQuad_4(),
	BoundaryManager_t2608055002::get_offset_of_floorQuadInstance_5(),
	BoundaryManager_t2608055002::get_offset_of_boundaryHeight_6(),
	BoundaryManager_t2608055002::get_offset_of_boundaryBounds_7(),
	BoundaryManager_t2608055002::get_offset_of_opaqueTrackingSpaceType_8(),
	BoundaryManager_t2608055002::get_offset_of_floorPositionInEditor_9(),
	BoundaryManager_t2608055002::get_offset_of_renderFloor_10(),
	BoundaryManager_t2608055002::get_offset_of_renderBoundary_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6006 = { sizeof (SceneContentAdjuster_t1258964661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6006[6] = 
{
	SceneContentAdjuster_t1258964661::get_offset_of_containerObject_2(),
	SceneContentAdjuster_t1258964661::get_offset_of_alignmentType_3(),
	SceneContentAdjuster_t1258964661::get_offset_of_stationarySpaceTypePosition_4(),
	SceneContentAdjuster_t1258964661::get_offset_of_roomScaleSpaceTypePosition_5(),
	SceneContentAdjuster_t1258964661::get_offset_of_contentPosition_6(),
	SceneContentAdjuster_t1258964661::get_offset_of_frameWaitHack_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6007 = { sizeof (AlignmentType_t4238599496)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6007[4] = 
{
	AlignmentType_t4238599496::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6008 = { sizeof (U3CSetContentHeightU3Ed__8_t405582793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6008[3] = 
{
	U3CSetContentHeightU3Ed__8_t405582793::get_offset_of_U3CU3E1__state_0(),
	U3CSetContentHeightU3Ed__8_t405582793::get_offset_of_U3CU3E2__current_1(),
	U3CSetContentHeightU3Ed__8_t405582793::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6009 = { sizeof (LinePointer_t114985340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6009[3] = 
{
	LinePointer_t114985340::get_offset_of_line_20(),
	LinePointer_t114985340::get_offset_of_renderers_21(),
	LinePointer_t114985340::get_offset_of_maxDistance_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6010 = { sizeof (NavigationHotSpot_t405511513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6011 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6012 = { sizeof (PointerSurfaceResultEnum_t86002505)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6012[5] = 
{
	PointerSurfaceResultEnum_t86002505::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6013 = { sizeof (PhysicsPointer_t2887364969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6013[18] = 
{
	PhysicsPointer_t2887364969::get_offset_of_U3CTotalLengthU3Ek__BackingField_2(),
	PhysicsPointer_t2887364969::get_offset_of_U3CStartPointU3Ek__BackingField_3(),
	PhysicsPointer_t2887364969::get_offset_of_U3CTargetPointU3Ek__BackingField_4(),
	PhysicsPointer_t2887364969::get_offset_of_U3CStartPointNormalU3Ek__BackingField_5(),
	PhysicsPointer_t2887364969::get_offset_of_U3CTargetPointNormalU3Ek__BackingField_6(),
	PhysicsPointer_t2887364969::get_offset_of_U3CPointerForwardU3Ek__BackingField_7(),
	PhysicsPointer_t2887364969::get_offset_of_U3CTargetResultU3Ek__BackingField_8(),
	PhysicsPointer_t2887364969::get_offset_of_U3CTargetPointOrientationU3Ek__BackingField_9(),
	PhysicsPointer_t2887364969::get_offset_of_lineColorValid_10(),
	PhysicsPointer_t2887364969::get_offset_of_lineColorInvalid_11(),
	PhysicsPointer_t2887364969::get_offset_of_lineColorHotSpot_12(),
	PhysicsPointer_t2887364969::get_offset_of_lineColorNoTarget_13(),
	PhysicsPointer_t2887364969::get_offset_of_validLayers_14(),
	PhysicsPointer_t2887364969::get_offset_of_invalidLayers_15(),
	PhysicsPointer_t2887364969::get_offset_of_detectTriggers_16(),
	PhysicsPointer_t2887364969::get_offset_of_active_17(),
	PhysicsPointer_t2887364969::get_offset_of_raycastOrigin_18(),
	PhysicsPointer_t2887364969::get_offset_of_targetHit_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6014 = { sizeof (PointerCursor_t2821356970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6014[5] = 
{
	PointerCursor_t2821356970::get_offset_of_pointer_2(),
	PointerCursor_t2821356970::get_offset_of_sizeOnScreen_3(),
	PointerCursor_t2821356970::get_offset_of_scaleAdjustTime_4(),
	PointerCursor_t2821356970::get_offset_of_renderers_5(),
	PointerCursor_t2821356970::get_offset_of_pointerTransform_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6015 = { sizeof (PointerInput_t2201074199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6015[2] = 
{
	PointerInput_t2201074199::get_offset_of_pointer_12(),
	PointerInput_t2201074199::get_offset_of_activePressType_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6016 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6017 = { sizeof (Distorter_t3335802222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6017[1] = 
{
	Distorter_t3335802222::get_offset_of_distortOrder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6018 = { sizeof (Ellipse_t4234545539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6018[3] = 
{
	0,
	Ellipse_t4234545539::get_offset_of_Resolution_17(),
	Ellipse_t4234545539::get_offset_of_Radius_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6019 = { sizeof (Line_t549989228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6019[2] = 
{
	Line_t549989228::get_offset_of_Start_16(),
	Line_t549989228::get_offset_of_End_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6020 = { sizeof (LineBase_t717918686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6020[14] = 
{
	0,
	LineBase_t717918686::get_offset_of_Space_3(),
	LineBase_t717918686::get_offset_of_LineStartClamp_4(),
	LineBase_t717918686::get_offset_of_LineEndClamp_5(),
	LineBase_t717918686::get_offset_of_RotationType_6(),
	LineBase_t717918686::get_offset_of_FlipUpVector_7(),
	LineBase_t717918686::get_offset_of_OriginOffset_8(),
	LineBase_t717918686::get_offset_of_ManualUpVectorBlend_9(),
	LineBase_t717918686::get_offset_of_ManualUpVectors_10(),
	LineBase_t717918686::get_offset_of_VelocitySearchRange_11(),
	LineBase_t717918686::get_offset_of_VelocityBlend_12(),
	LineBase_t717918686::get_offset_of_DistortionStrength_13(),
	LineBase_t717918686::get_offset_of_distorters_14(),
	LineBase_t717918686::get_offset_of_loops_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6021 = { sizeof (U3CU3Ec_t3579773046), -1, sizeof(U3CU3Ec_t3579773046_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6021[2] = 
{
	U3CU3Ec_t3579773046_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3579773046_StaticFields::get_offset_of_U3CU3E9__38_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6022 = { sizeof (LineObjectCollection_t2817952665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6022[16] = 
{
	LineObjectCollection_t2817952665::get_offset_of_Objects_2(),
	LineObjectCollection_t2817952665::get_offset_of_DistributionOffset_3(),
	LineObjectCollection_t2817952665::get_offset_of_LengthOffset_4(),
	LineObjectCollection_t2817952665::get_offset_of_ScaleOffset_5(),
	LineObjectCollection_t2817952665::get_offset_of_ScaleMultiplier_6(),
	LineObjectCollection_t2817952665::get_offset_of_PositionMultiplier_7(),
	LineObjectCollection_t2817952665::get_offset_of_ObjectScale_8(),
	LineObjectCollection_t2817952665::get_offset_of_ObjectPosition_9(),
	LineObjectCollection_t2817952665::get_offset_of_FlipRotation_10(),
	LineObjectCollection_t2817952665::get_offset_of_RotationOffset_11(),
	LineObjectCollection_t2817952665::get_offset_of_PositionOffset_12(),
	LineObjectCollection_t2817952665::get_offset_of_RotationTypeOverride_13(),
	LineObjectCollection_t2817952665::get_offset_of_DistributionType_14(),
	LineObjectCollection_t2817952665::get_offset_of_StepMode_15(),
	LineObjectCollection_t2817952665::get_offset_of_source_16(),
	LineObjectCollection_t2817952665::get_offset_of_transformHelper_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6023 = { sizeof (LineRenderer_t3025162641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6023[14] = 
{
	LineRenderer_t3025162641::get_offset_of_LineColor_2(),
	LineRenderer_t3025162641::get_offset_of_LineWidth_3(),
	LineRenderer_t3025162641::get_offset_of_WidthMultiplier_4(),
	LineRenderer_t3025162641::get_offset_of_ColorOffset_5(),
	LineRenderer_t3025162641::get_offset_of_WidthOffset_6(),
	LineRenderer_t3025162641::get_offset_of_RotationOffset_7(),
	LineRenderer_t3025162641::get_offset_of_StepMode_8(),
	LineRenderer_t3025162641::get_offset_of_InterpolationMode_9(),
	LineRenderer_t3025162641::get_offset_of_NumLineSteps_10(),
	LineRenderer_t3025162641::get_offset_of_StepLength_11(),
	LineRenderer_t3025162641::get_offset_of_MaxLineSteps_12(),
	LineRenderer_t3025162641::get_offset_of_StepLengthCurve_13(),
	LineRenderer_t3025162641::get_offset_of_source_14(),
	LineRenderer_t3025162641::get_offset_of_normalizedLengths_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6024 = { sizeof (LineUnity_t4238357500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6024[7] = 
{
	0,
	0,
	LineUnity_t4238357500::get_offset_of_LineMaterial_18(),
	LineUnity_t4238357500::get_offset_of_RoundedEdges_19(),
	LineUnity_t4238357500::get_offset_of_RoundedCaps_20(),
	LineUnity_t4238357500::get_offset_of_lineRenderer_21(),
	LineUnity_t4238357500::get_offset_of_positions_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6025 = { sizeof (U3CUpdateLineUnityU3Ed__8_t3720518906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6025[6] = 
{
	U3CUpdateLineUnityU3Ed__8_t3720518906::get_offset_of_U3CU3E1__state_0(),
	U3CUpdateLineUnityU3Ed__8_t3720518906::get_offset_of_U3CU3E2__current_1(),
	U3CUpdateLineUnityU3Ed__8_t3720518906::get_offset_of_U3CU3E4__this_2(),
	U3CUpdateLineUnityU3Ed__8_t3720518906::get_offset_of_U3CiU3E5__1_3(),
	U3CUpdateLineUnityU3Ed__8_t3720518906::get_offset_of_U3CiU3E5__2_4(),
	U3CUpdateLineUnityU3Ed__8_t3720518906::get_offset_of_U3CnormalizedDistanceU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6026 = { sizeof (SplinePoint_t3315310755)+ sizeof (RuntimeObject), sizeof(SplinePoint_t3315310755 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6026[2] = 
{
	SplinePoint_t3315310755::get_offset_of_Point_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplinePoint_t3315310755::get_offset_of_Rotation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6027 = { sizeof (LineUtils_t4143855974), -1, sizeof(LineUtils_t4143855974_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6027[1] = 
{
	LineUtils_t4143855974_StaticFields::get_offset_of_DefaultUpVector_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6028 = { sizeof (InterpolationEnum_t2040439712)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6028[4] = 
{
	InterpolationEnum_t2040439712::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6029 = { sizeof (SpaceEnum_t2918055180)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6029[3] = 
{
	SpaceEnum_t2918055180::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6030 = { sizeof (RotationTypeEnum_t2752505697)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6030[4] = 
{
	RotationTypeEnum_t2752505697::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6031 = { sizeof (PointDistributionTypeEnum_t4198058532)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6031[5] = 
{
	PointDistributionTypeEnum_t4198058532::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6032 = { sizeof (StepModeEnum_t777535071)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6032[3] = 
{
	StepModeEnum_t777535071::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6033 = { sizeof (InterpolationModeEnum_t4207999760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6033[4] = 
{
	InterpolationModeEnum_t4207999760::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6034 = { sizeof (Brush_t1548626474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6034[18] = 
{
	Brush_t1548626474::get_offset_of_minColorDelta_2(),
	Brush_t1548626474::get_offset_of_minPositionDelta_3(),
	Brush_t1548626474::get_offset_of_maxTimeDelta_4(),
	Brush_t1548626474::get_offset_of_tip_5(),
	Brush_t1548626474::get_offset_of_strokePrefab_6(),
	Brush_t1548626474::get_offset_of_brushObjectTransform_7(),
	Brush_t1548626474::get_offset_of_brushRenderer_8(),
	Brush_t1548626474::get_offset_of_draw_9(),
	Brush_t1548626474::get_offset_of_width_10(),
	Brush_t1548626474::get_offset_of_inMenuPosition_11(),
	Brush_t1548626474::get_offset_of_inMenuRotation_12(),
	Brush_t1548626474::get_offset_of_inHandPosition_13(),
	Brush_t1548626474::get_offset_of_inHandRotation_14(),
	Brush_t1548626474::get_offset_of_displayMode_15(),
	Brush_t1548626474::get_offset_of_transitionDuration_16(),
	Brush_t1548626474::get_offset_of_transitionCurve_17(),
	Brush_t1548626474::get_offset_of_currentStrokeColor_18(),
	Brush_t1548626474::get_offset_of_lastPointAddedTime_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6035 = { sizeof (DisplayModeEnum_t2177039354)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6035[4] = 
{
	DisplayModeEnum_t2177039354::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6036 = { sizeof (U3CDrawOverTimeU3Ed__31_t814362136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6036[8] = 
{
	U3CDrawOverTimeU3Ed__31_t814362136::get_offset_of_U3CU3E1__state_0(),
	U3CDrawOverTimeU3Ed__31_t814362136::get_offset_of_U3CU3E2__current_1(),
	U3CDrawOverTimeU3Ed__31_t814362136::get_offset_of_U3CU3E4__this_2(),
	U3CDrawOverTimeU3Ed__31_t814362136::get_offset_of_U3ClastPointPositionU3E5__1_3(),
	U3CDrawOverTimeU3Ed__31_t814362136::get_offset_of_U3CstartPositionU3E5__2_4(),
	U3CDrawOverTimeU3Ed__31_t814362136::get_offset_of_U3CnewStrokeU3E5__3_5(),
	U3CDrawOverTimeU3Ed__31_t814362136::get_offset_of_U3ClineU3E5__4_6(),
	U3CDrawOverTimeU3Ed__31_t814362136::get_offset_of_U3CinitialWidthU3E5__5_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6037 = { sizeof (U3CUpdateDisplayModeU3Ed__32_t268247815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6037[12] = 
{
	U3CUpdateDisplayModeU3Ed__32_t268247815::get_offset_of_U3CU3E1__state_0(),
	U3CUpdateDisplayModeU3Ed__32_t268247815::get_offset_of_U3CU3E2__current_1(),
	U3CUpdateDisplayModeU3Ed__32_t268247815::get_offset_of_U3CU3E4__this_2(),
	U3CUpdateDisplayModeU3Ed__32_t268247815::get_offset_of_U3CtargetPositionU3E5__1_3(),
	U3CUpdateDisplayModeU3Ed__32_t268247815::get_offset_of_U3CtargetScaleU3E5__2_4(),
	U3CUpdateDisplayModeU3Ed__32_t268247815::get_offset_of_U3CtargetRotationU3E5__3_5(),
	U3CUpdateDisplayModeU3Ed__32_t268247815::get_offset_of_U3CstartPositionU3E5__4_6(),
	U3CUpdateDisplayModeU3Ed__32_t268247815::get_offset_of_U3CstartScaleU3E5__5_7(),
	U3CUpdateDisplayModeU3Ed__32_t268247815::get_offset_of_U3CstartRotationU3E5__6_8(),
	U3CUpdateDisplayModeU3Ed__32_t268247815::get_offset_of_U3ClastDisplayModeU3E5__7_9(),
	U3CUpdateDisplayModeU3Ed__32_t268247815::get_offset_of_U3CstartTimeU3E5__8_10(),
	U3CUpdateDisplayModeU3Ed__32_t268247815::get_offset_of_U3CnormalizedTimeU3E5__9_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6038 = { sizeof (BrushController_t3766631826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6038[12] = 
{
	BrushController_t3766631826::get_offset_of_minColorDelta_2(),
	BrushController_t3766631826::get_offset_of_minPositionDelta_3(),
	BrushController_t3766631826::get_offset_of_maxTimeDelta_4(),
	BrushController_t3766631826::get_offset_of_tip_5(),
	BrushController_t3766631826::get_offset_of_strokePrefab_6(),
	BrushController_t3766631826::get_offset_of_brushObjectTransform_7(),
	BrushController_t3766631826::get_offset_of_brushRenderer_8(),
	BrushController_t3766631826::get_offset_of_colorPicker_9(),
	BrushController_t3766631826::get_offset_of_currentStrokeColor_10(),
	BrushController_t3766631826::get_offset_of_draw_11(),
	BrushController_t3766631826::get_offset_of_width_12(),
	BrushController_t3766631826::get_offset_of_lastPointAddedTime_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6039 = { sizeof (DisplayModeEnum_t2111840609)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6039[4] = 
{
	DisplayModeEnum_t2111840609::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6040 = { sizeof (U3CDrawOverTimeU3Ed__24_t1141827424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6040[8] = 
{
	U3CDrawOverTimeU3Ed__24_t1141827424::get_offset_of_U3CU3E1__state_0(),
	U3CDrawOverTimeU3Ed__24_t1141827424::get_offset_of_U3CU3E2__current_1(),
	U3CDrawOverTimeU3Ed__24_t1141827424::get_offset_of_U3CU3E4__this_2(),
	U3CDrawOverTimeU3Ed__24_t1141827424::get_offset_of_U3ClastPointPositionU3E5__1_3(),
	U3CDrawOverTimeU3Ed__24_t1141827424::get_offset_of_U3CstartPositionU3E5__2_4(),
	U3CDrawOverTimeU3Ed__24_t1141827424::get_offset_of_U3CnewStrokeU3E5__3_5(),
	U3CDrawOverTimeU3Ed__24_t1141827424::get_offset_of_U3ClineU3E5__4_6(),
	U3CDrawOverTimeU3Ed__24_t1141827424::get_offset_of_U3CinitialWidthU3E5__5_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6041 = { sizeof (BrushSelector_t2086558127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6041[26] = 
{
	BrushSelector_t2086558127::get_offset_of_brushCollection_12(),
	BrushSelector_t2086558127::get_offset_of_currentAction_13(),
	BrushSelector_t2086558127::get_offset_of_swipeCurve_14(),
	BrushSelector_t2086558127::get_offset_of_swipeDuration_15(),
	BrushSelector_t2086558127::get_offset_of_displayBrushindex_16(),
	BrushSelector_t2086558127::get_offset_of_activeBrushindex_17(),
	BrushSelector_t2086558127::get_offset_of_colorPicker_18(),
	BrushSelector_t2086558127::get_offset_of_menuTimeout_19(),
	BrushSelector_t2086558127::get_offset_of_selectPressedDrawThreshold_20(),
	BrushSelector_t2086558127::get_offset_of_selectPressedStartValue_21(),
	BrushSelector_t2086558127::get_offset_of_selectWidthCurve_22(),
	BrushSelector_t2086558127::get_offset_of_touchpadMaterial_23(),
	BrushSelector_t2086558127::get_offset_of_touchpadColor_24(),
	BrushSelector_t2086558127::get_offset_of_touchpadGlowLossTime_25(),
	BrushSelector_t2086558127::get_offset_of_touchpadTouchTime_26(),
	BrushSelector_t2086558127::get_offset_of_touchpadRenderer_27(),
	BrushSelector_t2086558127::get_offset_of_menuOpenTime_28(),
	BrushSelector_t2086558127::get_offset_of_menuOpen_29(),
	BrushSelector_t2086558127::get_offset_of_switchingBrush_30(),
	BrushSelector_t2086558127::get_offset_of_startOffset_31(),
	BrushSelector_t2086558127::get_offset_of_targetOffset_32(),
	BrushSelector_t2086558127::get_offset_of_startTime_33(),
	BrushSelector_t2086558127::get_offset_of_resetInput_34(),
	BrushSelector_t2086558127::get_offset_of_selectPressedSmooth_35(),
	BrushSelector_t2086558127::get_offset_of_activeBrush_36(),
	BrushSelector_t2086558127::get_offset_of_originalTouchpadMaterial_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6042 = { sizeof (SwipeEnum_t3123030255)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6042[4] = 
{
	SwipeEnum_t3123030255::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6043 = { sizeof (ColorPickerWheel_t2235220782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6043[12] = 
{
	ColorPickerWheel_t2235220782::get_offset_of_visible_12(),
	ColorPickerWheel_t2235220782::get_offset_of_selectorTransform_13(),
	ColorPickerWheel_t2235220782::get_offset_of_selectorRenderer_14(),
	ColorPickerWheel_t2235220782::get_offset_of_inputScale_15(),
	ColorPickerWheel_t2235220782::get_offset_of_selectedColor_16(),
	ColorPickerWheel_t2235220782::get_offset_of_colorWheelTexture_17(),
	ColorPickerWheel_t2235220782::get_offset_of_colorWheelObject_18(),
	ColorPickerWheel_t2235220782::get_offset_of_animator_19(),
	ColorPickerWheel_t2235220782::get_offset_of_timeout_20(),
	ColorPickerWheel_t2235220782::get_offset_of_selectorPosition_21(),
	ColorPickerWheel_t2235220782::get_offset_of_lastTimeVisible_22(),
	ColorPickerWheel_t2235220782::get_offset_of_visibleLastFrame_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6044 = { sizeof (Eraser_t1049249012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6044[4] = 
{
	Eraser_t1049249012::get_offset_of_eraseRange_20(),
	Eraser_t1049249012::get_offset_of_eraseTime_21(),
	Eraser_t1049249012::get_offset_of_erasingStrokes_22(),
	Eraser_t1049249012::get_offset_of_erasedStrokes_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6045 = { sizeof (U3CDrawOverTimeU3Ed__4_t2030358129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6045[8] = 
{
	U3CDrawOverTimeU3Ed__4_t2030358129::get_offset_of_U3CU3E1__state_0(),
	U3CDrawOverTimeU3Ed__4_t2030358129::get_offset_of_U3CU3E2__current_1(),
	U3CDrawOverTimeU3Ed__4_t2030358129::get_offset_of_U3CU3E4__this_2(),
	U3CDrawOverTimeU3Ed__4_t2030358129::get_offset_of_U3CbrushStrokesU3E5__1_3(),
	U3CDrawOverTimeU3Ed__4_t2030358129::get_offset_of_U3CiU3E5__2_4(),
	U3CDrawOverTimeU3Ed__4_t2030358129::get_offset_of_U3ClineRendererU3E5__3_5(),
	U3CDrawOverTimeU3Ed__4_t2030358129::get_offset_of_U3CpositionsU3E5__4_6(),
	U3CDrawOverTimeU3Ed__4_t2030358129::get_offset_of_U3CjU3E5__5_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6046 = { sizeof (U3CEraseStrokesOverTimeU3Ed__5_t963038218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6046[11] = 
{
	U3CEraseStrokesOverTimeU3Ed__5_t963038218::get_offset_of_U3CU3E1__state_0(),
	U3CEraseStrokesOverTimeU3Ed__5_t963038218::get_offset_of_U3CU3E2__current_1(),
	U3CEraseStrokesOverTimeU3Ed__5_t963038218::get_offset_of_U3CU3E4__this_2(),
	U3CEraseStrokesOverTimeU3Ed__5_t963038218::get_offset_of_U3ClineRendererU3E5__1_3(),
	U3CEraseStrokesOverTimeU3Ed__5_t963038218::get_offset_of_U3CstartTimeU3E5__2_4(),
	U3CEraseStrokesOverTimeU3Ed__5_t963038218::get_offset_of_U3CstartWidthU3E5__3_5(),
	U3CEraseStrokesOverTimeU3Ed__5_t963038218::get_offset_of_U3CstartPositionsU3E5__4_6(),
	U3CEraseStrokesOverTimeU3Ed__5_t963038218::get_offset_of_U3CendPositionsU3E5__5_7(),
	U3CEraseStrokesOverTimeU3Ed__5_t963038218::get_offset_of_U3CnormalizedTimeU3E5__6_8(),
	U3CEraseStrokesOverTimeU3Ed__5_t963038218::get_offset_of_U3CiU3E5__7_9(),
	U3CEraseStrokesOverTimeU3Ed__5_t963038218::get_offset_of_U3CrandomNoiseU3E5__8_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6047 = { sizeof (ObjectSpawner_t3925816580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6047[12] = 
{
	ObjectSpawner_t3925816580::get_offset_of_displayParent_12(),
	ObjectSpawner_t3925816580::get_offset_of_scaleParent_13(),
	ObjectSpawner_t3925816580::get_offset_of_spawnParent_14(),
	ObjectSpawner_t3925816580::get_offset_of_displayObject_15(),
	ObjectSpawner_t3925816580::get_offset_of_objectMaterial_16(),
	ObjectSpawner_t3925816580::get_offset_of_colorSource_17(),
	ObjectSpawner_t3925816580::get_offset_of_availableMeshes_18(),
	ObjectSpawner_t3925816580::get_offset_of_animator_19(),
	ObjectSpawner_t3925816580::get_offset_of_meshIndex_20(),
	ObjectSpawner_t3925816580::get_offset_of_state_21(),
	ObjectSpawner_t3925816580::get_offset_of_instantiatedMaterial_22(),
	ObjectSpawner_t3925816580::get_offset_of_MicOn_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6048 = { sizeof (StateEnum_t2160764486)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6048[5] = 
{
	StateEnum_t2160764486::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6049 = { sizeof (U3CSwitchOverTimeU3Ed__25_t91139974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6049[3] = 
{
	U3CSwitchOverTimeU3Ed__25_t91139974::get_offset_of_U3CU3E1__state_0(),
	U3CSwitchOverTimeU3Ed__25_t91139974::get_offset_of_U3CU3E2__current_1(),
	U3CSwitchOverTimeU3Ed__25_t91139974::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6050 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255376), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6050[18] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U3013E068C5CF13ADD6FADCBE20DC57D04D9748467_0(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U30C334FA5B28CF955C37AF8CEFB7F11ED8156B371_1(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U30F9516F3B2268DEFAD0122DF5B7EB338BD7C6F20_2(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U31D1DF6626546FCE866E0F077A81E3F15D2FDAA02_3(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U3326FF6F77BB5C244576A14A56AB1CB5F79950ED5_4(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U339E2F60BD85399DDD5728E8DD2951FB4457ABEC4_5(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U35D55BEE8A63F25A4C9ADC5776E15D1077A336940_6(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U362E39AF6A4EDE5B98ACA51428D9AF0E37629FB4F_7(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U36497EB3FE2DD456D24F706237E5A882D2BA9996F_8(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U377500517B2CAA87B3925D20EEAA1428A8CD11B9C_9(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U37C864787BEF4C9722095E7546132ACBC5C8FD940_10(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U38634E53F81A91ED972DCFB19F840F53AC39A67F9_11(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U3888C635713ED1BDB91D86A17C2F3F4647CF4A55F_12(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U38CFF0276DAC339BBEAD6BFC55B53ED9492217C08_13(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U39AE3FD136714B4C4439E72CA48C36314C843CA0E_14(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_A714705DEAA10EE10D925E684E64E7CC81896007_15(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_C962AD6D5D1773668C4D37330DA336E1C53D07DE_16(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_F84A5DF36B7884A8F76FAEED14A7E066C2DAA4DC_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6051 = { sizeof (__StaticArrayInitTypeSizeU3D36_t385526563)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D36_t385526563 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6052 = { sizeof (__StaticArrayInitTypeSizeU3D64_t3517497838)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D64_t3517497838 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6053 = { sizeof (__StaticArrayInitTypeSizeU3D96_t385919777)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D96_t385919777 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6054 = { sizeof (__StaticArrayInitTypeSizeU3D100_t2134348887)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D100_t2134348887 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6055 = { sizeof (__StaticArrayInitTypeSizeU3D128_t531529104)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D128_t531529104 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6056 = { sizeof (__StaticArrayInitTypeSizeU3D200_t2134545495)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D200_t2134545495 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6057 = { sizeof (__StaticArrayInitTypeSizeU3D576_t2919708294)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D576_t2919708294 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6058 = { sizeof (__StaticArrayInitTypeSizeU3D864_t1736813345)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D864_t1736813345 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6059 = { sizeof (__StaticArrayInitTypeSizeU3D2048_t1070431641)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D2048_t1070431641 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6060 = { sizeof (__StaticArrayInitTypeSizeU3D4480_t3791498020)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D4480_t3791498020 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6061 = { sizeof (__StaticArrayInitTypeSizeU3D5120_t1474365685)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D5120_t1474365685 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
