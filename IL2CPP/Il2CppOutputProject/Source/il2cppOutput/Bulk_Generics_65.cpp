﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Generic.IReadOnlyDictionary`2<System.Object,System.Object>
struct IReadOnlyDictionary_2_t548514548;
// System.String
struct String_t;
// System.Collections.Generic.IReadOnlyDictionary`2<System.Object,System.String>
struct IReadOnlyDictionary_2_t3610826369;
// System.Collections.Generic.IReadOnlyDictionary`2<System.String,System.Boolean>
struct IReadOnlyDictionary_2_t298513660;
// System.Collections.Generic.IReadOnlyDictionary`2<System.String,System.Int32>
struct IReadOnlyDictionary_2_t3152171448;
// System.Collections.Generic.IReadOnlyDictionary`2<System.String,System.Object>
struct IReadOnlyDictionary_2_t3281331859;
// System.Collections.Generic.IReadOnlyDictionary`2<System.String,System.String>
struct IReadOnlyDictionary_2_t2048676384;
// System.Type
struct Type_t;
// System.Collections.Generic.IReadOnlyDictionary`2<System.String,System.Type>
struct IReadOnlyDictionary_2_t2685170455;
// System.Collections.Generic.IReadOnlyDictionary`2<System.Type,System.Type>
struct IReadOnlyDictionary_2_t1049293924;
// System.Collections.Generic.IReadOnlyDictionary`2<System.UInt32,System.Object>
struct IReadOnlyDictionary_2_t475375926;
// System.Collections.Generic.IReadOnlyDictionary`2<System.Guid,System.Object>
struct IReadOnlyDictionary_2_t2545093093;
// System.Collections.Generic.IReadOnlyDictionary`2<System.Guid,System.Type>
struct IReadOnlyDictionary_2_t1948931689;
// System.Collections.Generic.IReadOnlyDictionary`2<System.Int32,System.Object>
struct IReadOnlyDictionary_2_t2384788891;
// System.Collections.Generic.IReadOnlyDictionary`2<System.Int64,System.Object>
struct IReadOnlyDictionary_2_t263748048;
// System.Collections.Generic.IReadOnlyDictionary`2<System.Object,System.Boolean>
struct IReadOnlyDictionary_2_t1860663645;
// System.Collections.Generic.IReadOnlyDictionary`2<System.Object,System.Int32>
struct IReadOnlyDictionary_2_t419354137;
// System.Collections.Generic.IReadOnlyList`1<System.Boolean>
struct IReadOnlyList_1_t661710871;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Collections.Generic.IReadOnlyList`1<System.Byte>
struct IReadOnlyList_1_t1698719282;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.Generic.IReadOnlyList`1<System.Char>
struct IReadOnlyList_1_t4198883376;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Reflection.Binder
struct Binder_t2999457153;

extern RuntimeClass* Il2CppComObject_il2cpp_TypeInfo_var;
extern const uint32_t IMap_2_Lookup_m1731177585_MetadataUsageId;
extern const uint32_t IMap_2_GetView_m2883253156_MetadataUsageId;
extern const uint32_t IMap_2_GetView_m2274488383_MetadataUsageId;
extern const uint32_t IMap_2_GetView_m1023461368_MetadataUsageId;
extern const uint32_t IMap_2_GetView_m3605659067_MetadataUsageId;
extern const uint32_t IMap_2_Lookup_m3451048385_MetadataUsageId;
extern const uint32_t IMap_2_GetView_m3764174551_MetadataUsageId;
extern const uint32_t IMap_2_GetView_m1648969448_MetadataUsageId;
extern const uint32_t IMap_2_GetView_m160940274_MetadataUsageId;
extern const uint32_t IMap_2_GetView_m3149330040_MetadataUsageId;
extern const uint32_t IMap_2_Lookup_m2631401358_MetadataUsageId;
extern const uint32_t IMap_2_GetView_m1344022084_MetadataUsageId;
extern const uint32_t IMapView_2_Lookup_m3331997343_MetadataUsageId;
extern const uint32_t IMapView_2_Split_m3394162196_MetadataUsageId;
extern const uint32_t IMapView_2_Split_m2592840147_MetadataUsageId;
extern const uint32_t IMapView_2_Lookup_m3742065343_MetadataUsageId;
extern const uint32_t IMapView_2_Split_m842968636_MetadataUsageId;
extern const uint32_t IMapView_2_Lookup_m2833276555_MetadataUsageId;
extern const uint32_t IMapView_2_Split_m2875932391_MetadataUsageId;
extern const uint32_t IMapView_2_Split_m2647546983_MetadataUsageId;
extern const uint32_t IMapView_2_Split_m4061037733_MetadataUsageId;
extern const uint32_t IMapView_2_Lookup_m354916185_MetadataUsageId;
extern const uint32_t IMapView_2_Split_m3017881906_MetadataUsageId;
extern const uint32_t IMapView_2_Split_m1609902508_MetadataUsageId;
extern const uint32_t IMapView_2_Split_m22494317_MetadataUsageId;
extern const uint32_t IMapView_2_Split_m96786832_MetadataUsageId;
extern const uint32_t IMapView_2_Lookup_m2993729319_MetadataUsageId;
extern const uint32_t IMapView_2_Split_m1626200424_MetadataUsageId;
extern const uint32_t IMapView_2_Split_m978754527_MetadataUsageId;
extern const uint32_t IMapView_2_Split_m3605358933_MetadataUsageId;
extern const uint32_t IMapView_2_Split_m964547936_MetadataUsageId;
extern const uint32_t IMapView_2_Lookup_m3822737838_MetadataUsageId;
extern const uint32_t IMapView_2_Split_m4049158937_MetadataUsageId;
extern const uint32_t IVector_1_GetView_m3248688482_MetadataUsageId;
extern const uint32_t IVector_1_GetView_m2759398957_MetadataUsageId;
extern const uint32_t IVector_1_GetView_m3718260831_MetadataUsageId;
struct IVectorView_1_t473161638;
struct IMapView_2_t4018177723;
struct IMapView_2_t2382301192;
struct IMapView_2_t319371831;
struct IMapView_2_t3381683652;
struct IMapView_2_t3193670913;
struct IMapView_2_t3717796159;
struct IMapView_2_t1596755316;
struct IMapView_2_t1752361405;
struct IMapView_2_t1808383194;
struct IVectorView_1_t3731120523;
struct IMapView_2_t1881521816;
struct IMapView_2_t648866341;
struct IVectorView_1_t2973325732;
struct IMapView_2_t190211420;
struct IMapView_2_t1631520928;
struct IMapView_2_t3281938957;
struct IMapView_2_t3878100361;

struct BooleanU5BU5D_t2897418192;
struct ByteU5BU5D_t4116647657;
struct CharU5BU5D_t3528271667;


// Windows.Foundation.Collections.IVector`1<System.Byte>
struct NOVTABLE IVector_1_t1874918097 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m156571538(uint32_t ___index0, uint8_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m216665452(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m2759398957(IVectorView_1_t473161638** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m2293818318(uint8_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m3049348516(uint32_t ___index0, uint8_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m1632374023(uint32_t ___index0, uint8_t ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m313767260(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m1600833624(uint8_t ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m2280259536() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m1881849540() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m3108887717(uint32_t ___startIndex0, uint32_t ___items1ArraySize, uint8_t* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m3769885758(uint32_t ___items0ArraySize, uint8_t* ___items0) = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.String,System.Type>
struct NOVTABLE IMapView_2_t4018177723 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m594002932(Il2CppHString ___key0, Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m3065552575(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m1694676420(Il2CppHString ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m3605358933(IMapView_2_t4018177723** ___first0, IMapView_2_t4018177723** ___second1) = 0;
};
// Windows.Foundation.Collections.IMap`2<System.Type,System.Type>
struct NOVTABLE IMap_2_t3577928644 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMap_2_Lookup_m4154434452(Type_t * ___key0, Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_get_Size_m1325865584(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_HasKey_m2230142782(Type_t * ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_GetView_m3149330040(IMapView_2_t2382301192** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Insert_m4189730454(Type_t * ___key0, Type_t * ___value1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Remove_m1267624818(Type_t * ___key0) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Clear_m2235974657() = 0;
};
// Windows.Foundation.Collections.IMap`2<System.String,System.Type>
struct NOVTABLE IMap_2_t918837879 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMap_2_Lookup_m3058979306(Il2CppHString ___key0, Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_get_Size_m732615070(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_HasKey_m3062070663(Il2CppHString ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_GetView_m160940274(IMapView_2_t4018177723** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Insert_m610774430(Il2CppHString ___key0, Type_t * ___value1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Remove_m3871291260(Il2CppHString ___key0) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Clear_m2457687352() = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.String,System.Object>
struct NOVTABLE IMapView_2_t319371831 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m2993729319(Il2CppHString ___key0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m2008534305(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m3205845981(Il2CppHString ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m1626200424(IMapView_2_t319371831** ___first0, IMapView_2_t319371831** ___second1) = 0;
};
// Windows.Foundation.Collections.IMap`2<System.String,System.String>
struct NOVTABLE IMap_2_t282343808 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMap_2_Lookup_m3419416962(Il2CppHString ___key0, Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_get_Size_m3747204495(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_HasKey_m1327307936(Il2CppHString ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_GetView_m1648969448(IMapView_2_t3381683652** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Insert_m3135696435(Il2CppHString ___key0, Il2CppHString ___value1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Remove_m3800098948(Il2CppHString ___key0) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Clear_m3867596926() = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.String,System.String>
struct NOVTABLE IMapView_2_t3381683652 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m1783676987(Il2CppHString ___key0, Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m3316105878(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m1003035185(Il2CppHString ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m978754527(IMapView_2_t3381683652** ___first0, IMapView_2_t3381683652** ___second1) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Boolean>
struct NOVTABLE IVectorView_1_t3731120523 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m2313715349(uint32_t ___index0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1684260838(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m2033007002(bool ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m684281448(uint32_t ___startIndex0, uint32_t ___items1ArraySize, bool* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.Object,System.Boolean>
struct NOVTABLE IMapView_2_t3193670913 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m4048973909(Il2CppIInspectable* ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m516432122(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m1413942596(Il2CppIInspectable* ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m2647546983(IMapView_2_t3193670913** ___first0, IMapView_2_t3193670913** ___second1) = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.Int32,System.Object>
struct NOVTABLE IMapView_2_t3717796159 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m3742065343(int32_t ___key0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m1359240774(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m973232472(int32_t ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m842968636(IMapView_2_t3717796159** ___first0, IMapView_2_t3717796159** ___second1) = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.Int64,System.Object>
struct NOVTABLE IMapView_2_t1596755316 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m2833276555(int64_t ___key0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m2657859418(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m1129223030(int64_t ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m2875932391(IMapView_2_t1596755316** ___first0, IMapView_2_t1596755316** ___second1) = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.Object,System.Int32>
struct NOVTABLE IMapView_2_t1752361405 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m877167602(Il2CppIInspectable* ___key0, int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m3871500701(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m1872447125(Il2CppIInspectable* ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m4061037733(IMapView_2_t1752361405** ___first0, IMapView_2_t1752361405** ___second1) = 0;
};
// Windows.Foundation.Collections.IMap`2<System.UInt32,System.Object>
struct NOVTABLE IMap_2_t3004010646 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMap_2_Lookup_m2631401358(uint32_t ___key0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_get_Size_m3107625399(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_HasKey_m1140347487(uint32_t ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_GetView_m1344022084(IMapView_2_t1808383194** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Insert_m3454051178(uint32_t ___key0, Il2CppIInspectable* ___value1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Remove_m1413378520(uint32_t ___key0) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Clear_m3255529802() = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.UInt32,System.Object>
struct NOVTABLE IMapView_2_t1808383194 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m3822737838(uint32_t ___key0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m142448920(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m2107050039(uint32_t ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m4049158937(IMapView_2_t1808383194** ___first0, IMapView_2_t1808383194** ___second1) = 0;
};
// Windows.Foundation.Collections.IVector`1<System.Boolean>
struct NOVTABLE IVector_1_t837909686 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m673046332(uint32_t ___index0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m1880168052(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m3248688482(IVectorView_1_t3731120523** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m2065486410(bool ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m4191429877(uint32_t ___index0, bool ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m2658525135(uint32_t ___index0, bool ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m3887137363(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m2261849140(bool ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m2083066132() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m1306498240() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m673289840(uint32_t ___startIndex0, uint32_t ___items1ArraySize, bool* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m2346663782(uint32_t ___items0ArraySize, bool* ___items0) = 0;
};
// Windows.Foundation.Collections.IMap`2<System.String,System.Object>
struct NOVTABLE IMap_2_t1514999283 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMap_2_Lookup_m3451048385(Il2CppHString ___key0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_get_Size_m2831091936(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_HasKey_m1506256555(Il2CppHString ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_GetView_m3764174551(IMapView_2_t319371831** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Insert_m3883043034(Il2CppHString ___key0, Il2CppIInspectable* ___value1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Remove_m970902889(Il2CppHString ___key0) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Clear_m3357622563() = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.Object,System.Object>
struct NOVTABLE IMapView_2_t1881521816 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m354916185(Il2CppIInspectable* ___key0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m1307357424(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m1226002878(Il2CppIInspectable* ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m3017881906(IMapView_2_t1881521816** ___first0, IMapView_2_t1881521816** ___second1) = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Char>
struct NOVTABLE IVectorView_1_t2973325732 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m1110858976(uint32_t ___index0, Il2CppChar* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m1535793255(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m613537994(Il2CppChar ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3420541689(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Il2CppChar* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IMap`2<System.Object,System.String>
struct NOVTABLE IMap_2_t1844493793 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMap_2_Lookup_m1366224756(Il2CppIInspectable* ___key0, Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_get_Size_m141312652(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_HasKey_m1806344367(Il2CppIInspectable* ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_GetView_m2274488383(IMapView_2_t648866341** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Insert_m728467539(Il2CppIInspectable* ___key0, Il2CppHString ___value1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Remove_m2528516460(Il2CppIInspectable* ___key0) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Clear_m467056858() = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.Type,System.Type>
struct NOVTABLE IMapView_2_t2382301192 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m580708534(Type_t * ___key0, Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m2678681203(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m1363070300(Type_t * ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m964547936(IMapView_2_t2382301192** ___first0, IMapView_2_t2382301192** ___second1) = 0;
};
// Windows.Foundation.Collections.IMap`2<System.Object,System.Object>
struct NOVTABLE IMap_2_t3077149268 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMap_2_Lookup_m1731177585(Il2CppIInspectable* ___key0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_get_Size_m3704454125(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_HasKey_m2433635512(Il2CppIInspectable* ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_GetView_m2883253156(IMapView_2_t1881521816** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Insert_m335884934(Il2CppIInspectable* ___key0, Il2CppIInspectable* ___value1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Remove_m3274472395(Il2CppIInspectable* ___key0) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Clear_m112521936() = 0;
};
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
// Windows.Foundation.Collections.IVector`1<System.Char>
struct NOVTABLE IVector_1_t80114895 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetAt_m3761631649(uint32_t ___index0, Il2CppChar* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_get_Size_m729810301(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetView_m3718260831(IVectorView_1_t2973325732** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_IndexOf_m1347992874(Il2CppChar ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_SetAt_m38195806(uint32_t ___index0, Il2CppChar ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_InsertAt_m843679895(uint32_t ___index0, Il2CppChar ___value1) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAt_m1420036444(uint32_t ___index0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Append_m3292248582(Il2CppChar ___value0) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_RemoveAtEnd_m3410828052() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_Clear_m4210028140() = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_GetMany_m1663454247(uint32_t ___startIndex0, uint32_t ___items1ArraySize, Il2CppChar* ___items1, uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVector_1_ReplaceAll_m968700710(uint32_t ___items0ArraySize, Il2CppChar* ___items0) = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.String,System.Int32>
struct NOVTABLE IMapView_2_t190211420 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m1298530599(Il2CppHString ___key0, int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m546192505(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m3322124696(Il2CppHString ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m96786832(IMapView_2_t190211420** ___first0, IMapView_2_t190211420** ___second1) = 0;
};
// Windows.Foundation.Collections.IMap`2<System.String,System.Int32>
struct NOVTABLE IMap_2_t1385838872 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMap_2_Lookup_m2400282917(Il2CppHString ___key0, int32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_get_Size_m3320579976(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_HasKey_m3055190088(Il2CppHString ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_GetView_m3605659067(IMapView_2_t190211420** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Insert_m48626576(Il2CppHString ___key0, int32_t ___value1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Remove_m2564332326(Il2CppHString ___key0) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Clear_m1499398795() = 0;
};
// Windows.Foundation.Collections.IVectorView`1<System.Byte>
struct NOVTABLE IVectorView_1_t473161638 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetAt_m54195267(uint32_t ___index0, uint8_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_get_Size_m473387791(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_IndexOf_m3353801494(uint8_t ___value0, uint32_t* ___index1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IVectorView_1_GetMany_m3912635590(uint32_t ___startIndex0, uint32_t ___items1ArraySize, uint8_t* ___items1, uint32_t* comReturnValue) = 0;
};
// Windows.Foundation.Collections.IMap`2<System.String,System.Boolean>
struct NOVTABLE IMap_2_t2827148380 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMap_2_Lookup_m356884552(Il2CppHString ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_get_Size_m2116548387(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_HasKey_m650644540(Il2CppHString ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_GetView_m1023461368(IMapView_2_t1631520928** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Insert_m2357915180(Il2CppHString ___key0, bool ___value1, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Remove_m2388588850(Il2CppHString ___key0) = 0;
	virtual il2cpp_hresult_t STDCALL IMap_2_Clear_m3448822397() = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.Object,System.String>
struct NOVTABLE IMapView_2_t648866341 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m1181259780(Il2CppIInspectable* ___key0, Il2CppHString* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m3995749853(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m2611237161(Il2CppIInspectable* ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m1609902508(IMapView_2_t648866341** ___first0, IMapView_2_t648866341** ___second1) = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.String,System.Boolean>
struct NOVTABLE IMapView_2_t1631520928 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m818349798(Il2CppHString ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m1396842905(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m2200864154(Il2CppHString ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m22494317(IMapView_2_t1631520928** ___first0, IMapView_2_t1631520928** ___second1) = 0;
};
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_t4116647657* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_t4116647657* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_t4116647657* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((&___categoryForLatin1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_4;
	// System.Int16 System.Guid::_b
	int16_t ____b_5;
	// System.Int16 System.Guid::_c
	int16_t ____c_6;
	// System.Byte System.Guid::_d
	uint8_t ____d_7;
	// System.Byte System.Guid::_e
	uint8_t ____e_8;
	// System.Byte System.Guid::_f
	uint8_t ____f_9;
	// System.Byte System.Guid::_g
	uint8_t ____g_10;
	// System.Byte System.Guid::_h
	uint8_t ____h_11;
	// System.Byte System.Guid::_i
	uint8_t ____i_12;
	// System.Byte System.Guid::_j
	uint8_t ____j_13;
	// System.Byte System.Guid::_k
	uint8_t ____k_14;

public:
	inline static int32_t get_offset_of__a_4() { return static_cast<int32_t>(offsetof(Guid_t, ____a_4)); }
	inline int32_t get__a_4() const { return ____a_4; }
	inline int32_t* get_address_of__a_4() { return &____a_4; }
	inline void set__a_4(int32_t value)
	{
		____a_4 = value;
	}

	inline static int32_t get_offset_of__b_5() { return static_cast<int32_t>(offsetof(Guid_t, ____b_5)); }
	inline int16_t get__b_5() const { return ____b_5; }
	inline int16_t* get_address_of__b_5() { return &____b_5; }
	inline void set__b_5(int16_t value)
	{
		____b_5 = value;
	}

	inline static int32_t get_offset_of__c_6() { return static_cast<int32_t>(offsetof(Guid_t, ____c_6)); }
	inline int16_t get__c_6() const { return ____c_6; }
	inline int16_t* get_address_of__c_6() { return &____c_6; }
	inline void set__c_6(int16_t value)
	{
		____c_6 = value;
	}

	inline static int32_t get_offset_of__d_7() { return static_cast<int32_t>(offsetof(Guid_t, ____d_7)); }
	inline uint8_t get__d_7() const { return ____d_7; }
	inline uint8_t* get_address_of__d_7() { return &____d_7; }
	inline void set__d_7(uint8_t value)
	{
		____d_7 = value;
	}

	inline static int32_t get_offset_of__e_8() { return static_cast<int32_t>(offsetof(Guid_t, ____e_8)); }
	inline uint8_t get__e_8() const { return ____e_8; }
	inline uint8_t* get_address_of__e_8() { return &____e_8; }
	inline void set__e_8(uint8_t value)
	{
		____e_8 = value;
	}

	inline static int32_t get_offset_of__f_9() { return static_cast<int32_t>(offsetof(Guid_t, ____f_9)); }
	inline uint8_t get__f_9() const { return ____f_9; }
	inline uint8_t* get_address_of__f_9() { return &____f_9; }
	inline void set__f_9(uint8_t value)
	{
		____f_9 = value;
	}

	inline static int32_t get_offset_of__g_10() { return static_cast<int32_t>(offsetof(Guid_t, ____g_10)); }
	inline uint8_t get__g_10() const { return ____g_10; }
	inline uint8_t* get_address_of__g_10() { return &____g_10; }
	inline void set__g_10(uint8_t value)
	{
		____g_10 = value;
	}

	inline static int32_t get_offset_of__h_11() { return static_cast<int32_t>(offsetof(Guid_t, ____h_11)); }
	inline uint8_t get__h_11() const { return ____h_11; }
	inline uint8_t* get_address_of__h_11() { return &____h_11; }
	inline void set__h_11(uint8_t value)
	{
		____h_11 = value;
	}

	inline static int32_t get_offset_of__i_12() { return static_cast<int32_t>(offsetof(Guid_t, ____i_12)); }
	inline uint8_t get__i_12() const { return ____i_12; }
	inline uint8_t* get_address_of__i_12() { return &____i_12; }
	inline void set__i_12(uint8_t value)
	{
		____i_12 = value;
	}

	inline static int32_t get_offset_of__j_13() { return static_cast<int32_t>(offsetof(Guid_t, ____j_13)); }
	inline uint8_t get__j_13() const { return ____j_13; }
	inline uint8_t* get_address_of__j_13() { return &____j_13; }
	inline void set__j_13(uint8_t value)
	{
		____j_13 = value;
	}

	inline static int32_t get_offset_of__k_14() { return static_cast<int32_t>(offsetof(Guid_t, ____k_14)); }
	inline uint8_t get__k_14() const { return ____k_14; }
	inline uint8_t* get_address_of__k_14() { return &____k_14; }
	inline void set__k_14(uint8_t value)
	{
		____k_14 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_0;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_1;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_2;
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_3;

public:
	inline static int32_t get_offset_of__rngAccess_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_0)); }
	inline RuntimeObject * get__rngAccess_0() const { return ____rngAccess_0; }
	inline RuntimeObject ** get_address_of__rngAccess_0() { return &____rngAccess_0; }
	inline void set__rngAccess_0(RuntimeObject * value)
	{
		____rngAccess_0 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_0), value);
	}

	inline static int32_t get_offset_of__rng_1() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_1)); }
	inline RandomNumberGenerator_t386037858 * get__rng_1() const { return ____rng_1; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_1() { return &____rng_1; }
	inline void set__rng_1(RandomNumberGenerator_t386037858 * value)
	{
		____rng_1 = value;
		Il2CppCodeGenWriteBarrier((&____rng_1), value);
	}

	inline static int32_t get_offset_of__fastRng_2() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_2)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_2() const { return ____fastRng_2; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_2() { return &____fastRng_2; }
	inline void set__fastRng_2(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_2 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_2), value);
	}

	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_3)); }
	inline Guid_t  get_Empty_3() const { return ___Empty_3; }
	inline Guid_t * get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(Guid_t  value)
	{
		___Empty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
// Windows.Foundation.Collections.IMapView`2<System.Guid,System.Type>
struct NOVTABLE IMapView_2_t3281938957 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m3896662699(Guid_t  ___key0, Type_t ** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m2349127300(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m1382000797(Guid_t  ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m2592840147(IMapView_2_t3281938957** ___first0, IMapView_2_t3281938957** ___second1) = 0;
};
// Windows.Foundation.Collections.IMapView`2<System.Guid,System.Object>
struct NOVTABLE IMapView_2_t3878100361 : Il2CppIInspectable
{
	static const Il2CppGuid IID;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Lookup_m3331997343(Guid_t  ___key0, Il2CppIInspectable** comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_get_Size_m3858358881(uint32_t* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_HasKey_m3111850129(Guid_t  ___key0, bool* comReturnValue) = 0;
	virtual il2cpp_hresult_t STDCALL IMapView_2_Split_m3394162196(IMapView_2_t3878100361** ___first0, IMapView_2_t3878100361** ___second1) = 0;
};
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_0;

public:
	inline static int32_t get_offset_of__impl_0() { return static_cast<int32_t>(offsetof(Type_t, ____impl_0)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_0() const { return ____impl_0; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_0() { return &____impl_0; }
	inline void set__impl_0(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_0 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_1;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_2;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_3;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_4;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_5;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_6;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2999457153 * ___defaultBinder_7;

public:
	inline static int32_t get_offset_of_FilterAttribute_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_1)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_1() const { return ___FilterAttribute_1; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_1() { return &___FilterAttribute_1; }
	inline void set_FilterAttribute_1(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_1), value);
	}

	inline static int32_t get_offset_of_FilterName_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_2)); }
	inline MemberFilter_t426314064 * get_FilterName_2() const { return ___FilterName_2; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_2() { return &___FilterName_2; }
	inline void set_FilterName_2(MemberFilter_t426314064 * value)
	{
		___FilterName_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_2), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_3)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_3() const { return ___FilterNameIgnoreCase_3; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_3() { return &___FilterNameIgnoreCase_3; }
	inline void set_FilterNameIgnoreCase_3(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_3 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_3), value);
	}

	inline static int32_t get_offset_of_Missing_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_4)); }
	inline RuntimeObject * get_Missing_4() const { return ___Missing_4; }
	inline RuntimeObject ** get_address_of_Missing_4() { return &___Missing_4; }
	inline void set_Missing_4(RuntimeObject * value)
	{
		___Missing_4 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_4), value);
	}

	inline static int32_t get_offset_of_Delimiter_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_5)); }
	inline Il2CppChar get_Delimiter_5() const { return ___Delimiter_5; }
	inline Il2CppChar* get_address_of_Delimiter_5() { return &___Delimiter_5; }
	inline void set_Delimiter_5(Il2CppChar value)
	{
		___Delimiter_5 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_6)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_6() const { return ___EmptyTypes_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_6() { return &___EmptyTypes_6; }
	inline void set_EmptyTypes_6(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_6 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_6), value);
	}

	inline static int32_t get_offset_of_defaultBinder_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_7)); }
	inline Binder_t2999457153 * get_defaultBinder_7() const { return ___defaultBinder_7; }
	inline Binder_t2999457153 ** get_address_of_defaultBinder_7() { return &___defaultBinder_7; }
	inline void set_defaultBinder_7(Binder_t2999457153 * value)
	{
		___defaultBinder_7 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
// System.Boolean[]
struct BooleanU5BU5D_t2897418192  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) bool m_Items[1];

public:
	inline bool GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline bool* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline bool GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline bool* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Char[]
struct CharU5BU5D_t3528271667  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// V Windows.Foundation.Collections.IMap`2<System.Object,System.Object>::Lookup(K)
extern "C"  RuntimeObject * IMap_2_Lookup_m1731177585 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_Lookup_m1731177585_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t3077149268* ____imap_2_t3077149268 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3077149268::IID, reinterpret_cast<void**>(&____imap_2_t3077149268));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____imap_2_t3077149268->IMap_2_Lookup_m1731177585(____key0_marshaled, &returnValue);
	____imap_2_t3077149268->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMap`2<System.Object,System.Object>::get_Size()
extern "C"  uint32_t IMap_2_get_Size_m3704454125 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t3077149268* ____imap_2_t3077149268 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3077149268::IID, reinterpret_cast<void**>(&____imap_2_t3077149268));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imap_2_t3077149268->IMap_2_get_Size_m3704454125(&returnValue);
	____imap_2_t3077149268->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Object,System.Object>::HasKey(K)
extern "C"  bool IMap_2_HasKey_m2433635512 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMap_2_t3077149268* ____imap_2_t3077149268 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3077149268::IID, reinterpret_cast<void**>(&____imap_2_t3077149268));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t3077149268->IMap_2_HasKey_m2433635512(____key0_marshaled, &returnValue);
	____imap_2_t3077149268->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return returnValue;
}
// System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2<System.Object,System.Object>::GetView()
extern "C"  RuntimeObject* IMap_2_GetView_m2883253156 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_GetView_m2883253156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t3077149268* ____imap_2_t3077149268 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3077149268::IID, reinterpret_cast<void**>(&____imap_2_t3077149268));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IMapView_2_t1881521816* returnValue = NULL;
	hr = ____imap_2_t3077149268->IMap_2_GetView_m2883253156(&returnValue);
	____imap_2_t3077149268->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Object,System.Object>::Insert(K,V)
extern "C"  bool IMap_2_Insert_m335884934 (RuntimeObject* __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	IMap_2_t3077149268* ____imap_2_t3077149268 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3077149268::IID, reinterpret_cast<void**>(&____imap_2_t3077149268));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Marshaling of parameter '___value1' to native representation
	Il2CppIInspectable* ____value1_marshaled = NULL;
	if (___value1 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value1))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value1)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____value1_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value1_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___value1);
		}
	}
	else
	{
		____value1_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t3077149268->IMap_2_Insert_m335884934(____key0_marshaled, ____value1_marshaled, &returnValue);
	____imap_2_t3077149268->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___value1' native representation
	if (____value1_marshaled != NULL)
	{
		(____value1_marshaled)->Release();
		____value1_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMap`2<System.Object,System.Object>::Remove(K)
extern "C"  void IMap_2_Remove_m3274472395 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMap_2_t3077149268* ____imap_2_t3077149268 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3077149268::IID, reinterpret_cast<void**>(&____imap_2_t3077149268));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	hr = ____imap_2_t3077149268->IMap_2_Remove_m3274472395(____key0_marshaled);
	____imap_2_t3077149268->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

}
// System.Void Windows.Foundation.Collections.IMap`2<System.Object,System.Object>::Clear()
extern "C"  void IMap_2_Clear_m112521936 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t3077149268* ____imap_2_t3077149268 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3077149268::IID, reinterpret_cast<void**>(&____imap_2_t3077149268));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t3077149268->IMap_2_Clear_m112521936();
	____imap_2_t3077149268->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// V Windows.Foundation.Collections.IMap`2<System.Object,System.String>::Lookup(K)
extern "C"  String_t* IMap_2_Lookup_m1366224756 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMap_2_t1844493793* ____imap_2_t1844493793 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1844493793::IID, reinterpret_cast<void**>(&____imap_2_t1844493793));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	Il2CppHString returnValue = NULL;
	hr = ____imap_2_t1844493793->IMap_2_Lookup_m1366224756(____key0_marshaled, &returnValue);
	____imap_2_t1844493793->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_hstring_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free_hstring(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMap`2<System.Object,System.String>::get_Size()
extern "C"  uint32_t IMap_2_get_Size_m141312652 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t1844493793* ____imap_2_t1844493793 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1844493793::IID, reinterpret_cast<void**>(&____imap_2_t1844493793));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imap_2_t1844493793->IMap_2_get_Size_m141312652(&returnValue);
	____imap_2_t1844493793->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Object,System.String>::HasKey(K)
extern "C"  bool IMap_2_HasKey_m1806344367 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMap_2_t1844493793* ____imap_2_t1844493793 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1844493793::IID, reinterpret_cast<void**>(&____imap_2_t1844493793));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t1844493793->IMap_2_HasKey_m1806344367(____key0_marshaled, &returnValue);
	____imap_2_t1844493793->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return returnValue;
}
// System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2<System.Object,System.String>::GetView()
extern "C"  RuntimeObject* IMap_2_GetView_m2274488383 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_GetView_m2274488383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t1844493793* ____imap_2_t1844493793 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1844493793::IID, reinterpret_cast<void**>(&____imap_2_t1844493793));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IMapView_2_t648866341* returnValue = NULL;
	hr = ____imap_2_t1844493793->IMap_2_GetView_m2274488383(&returnValue);
	____imap_2_t1844493793->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Object,System.String>::Insert(K,V)
extern "C"  bool IMap_2_Insert_m728467539 (RuntimeObject* __this, RuntimeObject * ___key0, String_t* ___value1, const RuntimeMethod* method)
{
	IMap_2_t1844493793* ____imap_2_t1844493793 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1844493793::IID, reinterpret_cast<void**>(&____imap_2_t1844493793));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Marshaling of parameter '___value1' to native representation
	Il2CppHString ____value1_marshaled = NULL;
	if (___value1 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("value"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___value1NativeView, ___value1);
	il2cpp::utils::Il2CppHStringReference ___value1HStringReference(___value1NativeView);
	____value1_marshaled = ___value1HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t1844493793->IMap_2_Insert_m728467539(____key0_marshaled, ____value1_marshaled, &returnValue);
	____imap_2_t1844493793->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMap`2<System.Object,System.String>::Remove(K)
extern "C"  void IMap_2_Remove_m2528516460 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMap_2_t1844493793* ____imap_2_t1844493793 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1844493793::IID, reinterpret_cast<void**>(&____imap_2_t1844493793));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	hr = ____imap_2_t1844493793->IMap_2_Remove_m2528516460(____key0_marshaled);
	____imap_2_t1844493793->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

}
// System.Void Windows.Foundation.Collections.IMap`2<System.Object,System.String>::Clear()
extern "C"  void IMap_2_Clear_m467056858 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t1844493793* ____imap_2_t1844493793 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1844493793::IID, reinterpret_cast<void**>(&____imap_2_t1844493793));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t1844493793->IMap_2_Clear_m467056858();
	____imap_2_t1844493793->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// V Windows.Foundation.Collections.IMap`2<System.String,System.Boolean>::Lookup(K)
extern "C"  bool IMap_2_Lookup_m356884552 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMap_2_t2827148380* ____imap_2_t2827148380 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2827148380::IID, reinterpret_cast<void**>(&____imap_2_t2827148380));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t2827148380->IMap_2_Lookup_m356884552(____key0_marshaled, &returnValue);
	____imap_2_t2827148380->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IMap`2<System.String,System.Boolean>::get_Size()
extern "C"  uint32_t IMap_2_get_Size_m2116548387 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t2827148380* ____imap_2_t2827148380 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2827148380::IID, reinterpret_cast<void**>(&____imap_2_t2827148380));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imap_2_t2827148380->IMap_2_get_Size_m2116548387(&returnValue);
	____imap_2_t2827148380->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.String,System.Boolean>::HasKey(K)
extern "C"  bool IMap_2_HasKey_m650644540 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMap_2_t2827148380* ____imap_2_t2827148380 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2827148380::IID, reinterpret_cast<void**>(&____imap_2_t2827148380));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t2827148380->IMap_2_HasKey_m650644540(____key0_marshaled, &returnValue);
	____imap_2_t2827148380->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2<System.String,System.Boolean>::GetView()
extern "C"  RuntimeObject* IMap_2_GetView_m1023461368 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_GetView_m1023461368_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t2827148380* ____imap_2_t2827148380 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2827148380::IID, reinterpret_cast<void**>(&____imap_2_t2827148380));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IMapView_2_t1631520928* returnValue = NULL;
	hr = ____imap_2_t2827148380->IMap_2_GetView_m1023461368(&returnValue);
	____imap_2_t2827148380->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.String,System.Boolean>::Insert(K,V)
extern "C"  bool IMap_2_Insert_m2357915180 (RuntimeObject* __this, String_t* ___key0, bool ___value1, const RuntimeMethod* method)
{
	IMap_2_t2827148380* ____imap_2_t2827148380 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2827148380::IID, reinterpret_cast<void**>(&____imap_2_t2827148380));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t2827148380->IMap_2_Insert_m2357915180(____key0_marshaled, ___value1, &returnValue);
	____imap_2_t2827148380->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMap`2<System.String,System.Boolean>::Remove(K)
extern "C"  void IMap_2_Remove_m2388588850 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMap_2_t2827148380* ____imap_2_t2827148380 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2827148380::IID, reinterpret_cast<void**>(&____imap_2_t2827148380));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	hr = ____imap_2_t2827148380->IMap_2_Remove_m2388588850(____key0_marshaled);
	____imap_2_t2827148380->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IMap`2<System.String,System.Boolean>::Clear()
extern "C"  void IMap_2_Clear_m3448822397 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t2827148380* ____imap_2_t2827148380 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t2827148380::IID, reinterpret_cast<void**>(&____imap_2_t2827148380));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t2827148380->IMap_2_Clear_m3448822397();
	____imap_2_t2827148380->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// V Windows.Foundation.Collections.IMap`2<System.String,System.Int32>::Lookup(K)
extern "C"  int32_t IMap_2_Lookup_m2400282917 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMap_2_t1385838872* ____imap_2_t1385838872 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1385838872::IID, reinterpret_cast<void**>(&____imap_2_t1385838872));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	int32_t returnValue = 0;
	hr = ____imap_2_t1385838872->IMap_2_Lookup_m2400282917(____key0_marshaled, &returnValue);
	____imap_2_t1385838872->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IMap`2<System.String,System.Int32>::get_Size()
extern "C"  uint32_t IMap_2_get_Size_m3320579976 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t1385838872* ____imap_2_t1385838872 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1385838872::IID, reinterpret_cast<void**>(&____imap_2_t1385838872));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imap_2_t1385838872->IMap_2_get_Size_m3320579976(&returnValue);
	____imap_2_t1385838872->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.String,System.Int32>::HasKey(K)
extern "C"  bool IMap_2_HasKey_m3055190088 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMap_2_t1385838872* ____imap_2_t1385838872 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1385838872::IID, reinterpret_cast<void**>(&____imap_2_t1385838872));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t1385838872->IMap_2_HasKey_m3055190088(____key0_marshaled, &returnValue);
	____imap_2_t1385838872->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2<System.String,System.Int32>::GetView()
extern "C"  RuntimeObject* IMap_2_GetView_m3605659067 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_GetView_m3605659067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t1385838872* ____imap_2_t1385838872 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1385838872::IID, reinterpret_cast<void**>(&____imap_2_t1385838872));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IMapView_2_t190211420* returnValue = NULL;
	hr = ____imap_2_t1385838872->IMap_2_GetView_m3605659067(&returnValue);
	____imap_2_t1385838872->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.String,System.Int32>::Insert(K,V)
extern "C"  bool IMap_2_Insert_m48626576 (RuntimeObject* __this, String_t* ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	IMap_2_t1385838872* ____imap_2_t1385838872 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1385838872::IID, reinterpret_cast<void**>(&____imap_2_t1385838872));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t1385838872->IMap_2_Insert_m48626576(____key0_marshaled, ___value1, &returnValue);
	____imap_2_t1385838872->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMap`2<System.String,System.Int32>::Remove(K)
extern "C"  void IMap_2_Remove_m2564332326 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMap_2_t1385838872* ____imap_2_t1385838872 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1385838872::IID, reinterpret_cast<void**>(&____imap_2_t1385838872));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	hr = ____imap_2_t1385838872->IMap_2_Remove_m2564332326(____key0_marshaled);
	____imap_2_t1385838872->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IMap`2<System.String,System.Int32>::Clear()
extern "C"  void IMap_2_Clear_m1499398795 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t1385838872* ____imap_2_t1385838872 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1385838872::IID, reinterpret_cast<void**>(&____imap_2_t1385838872));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t1385838872->IMap_2_Clear_m1499398795();
	____imap_2_t1385838872->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// V Windows.Foundation.Collections.IMap`2<System.String,System.Object>::Lookup(K)
extern "C"  RuntimeObject * IMap_2_Lookup_m3451048385 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_Lookup_m3451048385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t1514999283* ____imap_2_t1514999283 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1514999283::IID, reinterpret_cast<void**>(&____imap_2_t1514999283));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____imap_2_t1514999283->IMap_2_Lookup_m3451048385(____key0_marshaled, &returnValue);
	____imap_2_t1514999283->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMap`2<System.String,System.Object>::get_Size()
extern "C"  uint32_t IMap_2_get_Size_m2831091936 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t1514999283* ____imap_2_t1514999283 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1514999283::IID, reinterpret_cast<void**>(&____imap_2_t1514999283));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imap_2_t1514999283->IMap_2_get_Size_m2831091936(&returnValue);
	____imap_2_t1514999283->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.String,System.Object>::HasKey(K)
extern "C"  bool IMap_2_HasKey_m1506256555 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMap_2_t1514999283* ____imap_2_t1514999283 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1514999283::IID, reinterpret_cast<void**>(&____imap_2_t1514999283));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t1514999283->IMap_2_HasKey_m1506256555(____key0_marshaled, &returnValue);
	____imap_2_t1514999283->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2<System.String,System.Object>::GetView()
extern "C"  RuntimeObject* IMap_2_GetView_m3764174551 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_GetView_m3764174551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t1514999283* ____imap_2_t1514999283 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1514999283::IID, reinterpret_cast<void**>(&____imap_2_t1514999283));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IMapView_2_t319371831* returnValue = NULL;
	hr = ____imap_2_t1514999283->IMap_2_GetView_m3764174551(&returnValue);
	____imap_2_t1514999283->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.String,System.Object>::Insert(K,V)
extern "C"  bool IMap_2_Insert_m3883043034 (RuntimeObject* __this, String_t* ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	IMap_2_t1514999283* ____imap_2_t1514999283 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1514999283::IID, reinterpret_cast<void**>(&____imap_2_t1514999283));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Marshaling of parameter '___value1' to native representation
	Il2CppIInspectable* ____value1_marshaled = NULL;
	if (___value1 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value1))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value1)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____value1_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value1_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___value1);
		}
	}
	else
	{
		____value1_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t1514999283->IMap_2_Insert_m3883043034(____key0_marshaled, ____value1_marshaled, &returnValue);
	____imap_2_t1514999283->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value1' native representation
	if (____value1_marshaled != NULL)
	{
		(____value1_marshaled)->Release();
		____value1_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMap`2<System.String,System.Object>::Remove(K)
extern "C"  void IMap_2_Remove_m970902889 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMap_2_t1514999283* ____imap_2_t1514999283 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1514999283::IID, reinterpret_cast<void**>(&____imap_2_t1514999283));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	hr = ____imap_2_t1514999283->IMap_2_Remove_m970902889(____key0_marshaled);
	____imap_2_t1514999283->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IMap`2<System.String,System.Object>::Clear()
extern "C"  void IMap_2_Clear_m3357622563 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t1514999283* ____imap_2_t1514999283 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t1514999283::IID, reinterpret_cast<void**>(&____imap_2_t1514999283));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t1514999283->IMap_2_Clear_m3357622563();
	____imap_2_t1514999283->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// V Windows.Foundation.Collections.IMap`2<System.String,System.String>::Lookup(K)
extern "C"  String_t* IMap_2_Lookup_m3419416962 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMap_2_t282343808* ____imap_2_t282343808 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t282343808::IID, reinterpret_cast<void**>(&____imap_2_t282343808));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	Il2CppHString returnValue = NULL;
	hr = ____imap_2_t282343808->IMap_2_Lookup_m3419416962(____key0_marshaled, &returnValue);
	____imap_2_t282343808->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_hstring_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free_hstring(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMap`2<System.String,System.String>::get_Size()
extern "C"  uint32_t IMap_2_get_Size_m3747204495 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t282343808* ____imap_2_t282343808 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t282343808::IID, reinterpret_cast<void**>(&____imap_2_t282343808));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imap_2_t282343808->IMap_2_get_Size_m3747204495(&returnValue);
	____imap_2_t282343808->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.String,System.String>::HasKey(K)
extern "C"  bool IMap_2_HasKey_m1327307936 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMap_2_t282343808* ____imap_2_t282343808 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t282343808::IID, reinterpret_cast<void**>(&____imap_2_t282343808));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t282343808->IMap_2_HasKey_m1327307936(____key0_marshaled, &returnValue);
	____imap_2_t282343808->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2<System.String,System.String>::GetView()
extern "C"  RuntimeObject* IMap_2_GetView_m1648969448 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_GetView_m1648969448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t282343808* ____imap_2_t282343808 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t282343808::IID, reinterpret_cast<void**>(&____imap_2_t282343808));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IMapView_2_t3381683652* returnValue = NULL;
	hr = ____imap_2_t282343808->IMap_2_GetView_m1648969448(&returnValue);
	____imap_2_t282343808->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.String,System.String>::Insert(K,V)
extern "C"  bool IMap_2_Insert_m3135696435 (RuntimeObject* __this, String_t* ___key0, String_t* ___value1, const RuntimeMethod* method)
{
	IMap_2_t282343808* ____imap_2_t282343808 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t282343808::IID, reinterpret_cast<void**>(&____imap_2_t282343808));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Marshaling of parameter '___value1' to native representation
	Il2CppHString ____value1_marshaled = NULL;
	if (___value1 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("value"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___value1NativeView, ___value1);
	il2cpp::utils::Il2CppHStringReference ___value1HStringReference(___value1NativeView);
	____value1_marshaled = ___value1HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t282343808->IMap_2_Insert_m3135696435(____key0_marshaled, ____value1_marshaled, &returnValue);
	____imap_2_t282343808->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMap`2<System.String,System.String>::Remove(K)
extern "C"  void IMap_2_Remove_m3800098948 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMap_2_t282343808* ____imap_2_t282343808 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t282343808::IID, reinterpret_cast<void**>(&____imap_2_t282343808));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	hr = ____imap_2_t282343808->IMap_2_Remove_m3800098948(____key0_marshaled);
	____imap_2_t282343808->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IMap`2<System.String,System.String>::Clear()
extern "C"  void IMap_2_Clear_m3867596926 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t282343808* ____imap_2_t282343808 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t282343808::IID, reinterpret_cast<void**>(&____imap_2_t282343808));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t282343808->IMap_2_Clear_m3867596926();
	____imap_2_t282343808->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// V Windows.Foundation.Collections.IMap`2<System.String,System.Type>::Lookup(K)
extern "C"  Type_t * IMap_2_Lookup_m3058979306 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.UInt32 Windows.Foundation.Collections.IMap`2<System.String,System.Type>::get_Size()
extern "C"  uint32_t IMap_2_get_Size_m732615070 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t918837879* ____imap_2_t918837879 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t918837879::IID, reinterpret_cast<void**>(&____imap_2_t918837879));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imap_2_t918837879->IMap_2_get_Size_m732615070(&returnValue);
	____imap_2_t918837879->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.String,System.Type>::HasKey(K)
extern "C"  bool IMap_2_HasKey_m3062070663 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMap_2_t918837879* ____imap_2_t918837879 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t918837879::IID, reinterpret_cast<void**>(&____imap_2_t918837879));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t918837879->IMap_2_HasKey_m3062070663(____key0_marshaled, &returnValue);
	____imap_2_t918837879->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2<System.String,System.Type>::GetView()
extern "C"  RuntimeObject* IMap_2_GetView_m160940274 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_GetView_m160940274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t918837879* ____imap_2_t918837879 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t918837879::IID, reinterpret_cast<void**>(&____imap_2_t918837879));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IMapView_2_t4018177723* returnValue = NULL;
	hr = ____imap_2_t918837879->IMap_2_GetView_m160940274(&returnValue);
	____imap_2_t918837879->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.String,System.Type>::Insert(K,V)
extern "C"  bool IMap_2_Insert_m610774430 (RuntimeObject* __this, String_t* ___key0, Type_t * ___value1, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.Void Windows.Foundation.Collections.IMap`2<System.String,System.Type>::Remove(K)
extern "C"  void IMap_2_Remove_m3871291260 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMap_2_t918837879* ____imap_2_t918837879 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t918837879::IID, reinterpret_cast<void**>(&____imap_2_t918837879));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	hr = ____imap_2_t918837879->IMap_2_Remove_m3871291260(____key0_marshaled);
	____imap_2_t918837879->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IMap`2<System.String,System.Type>::Clear()
extern "C"  void IMap_2_Clear_m2457687352 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t918837879* ____imap_2_t918837879 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t918837879::IID, reinterpret_cast<void**>(&____imap_2_t918837879));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t918837879->IMap_2_Clear_m2457687352();
	____imap_2_t918837879->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// V Windows.Foundation.Collections.IMap`2<System.Type,System.Type>::Lookup(K)
extern "C"  Type_t * IMap_2_Lookup_m4154434452 (RuntimeObject* __this, Type_t * ___key0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.UInt32 Windows.Foundation.Collections.IMap`2<System.Type,System.Type>::get_Size()
extern "C"  uint32_t IMap_2_get_Size_m1325865584 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t3577928644* ____imap_2_t3577928644 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3577928644::IID, reinterpret_cast<void**>(&____imap_2_t3577928644));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imap_2_t3577928644->IMap_2_get_Size_m1325865584(&returnValue);
	____imap_2_t3577928644->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Type,System.Type>::HasKey(K)
extern "C"  bool IMap_2_HasKey_m2230142782 (RuntimeObject* __this, Type_t * ___key0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2<System.Type,System.Type>::GetView()
extern "C"  RuntimeObject* IMap_2_GetView_m3149330040 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_GetView_m3149330040_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t3577928644* ____imap_2_t3577928644 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3577928644::IID, reinterpret_cast<void**>(&____imap_2_t3577928644));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IMapView_2_t2382301192* returnValue = NULL;
	hr = ____imap_2_t3577928644->IMap_2_GetView_m3149330040(&returnValue);
	____imap_2_t3577928644->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.Type,System.Type>::Insert(K,V)
extern "C"  bool IMap_2_Insert_m4189730454 (RuntimeObject* __this, Type_t * ___key0, Type_t * ___value1, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.Void Windows.Foundation.Collections.IMap`2<System.Type,System.Type>::Remove(K)
extern "C"  void IMap_2_Remove_m1267624818 (RuntimeObject* __this, Type_t * ___key0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.Void Windows.Foundation.Collections.IMap`2<System.Type,System.Type>::Clear()
extern "C"  void IMap_2_Clear_m2235974657 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t3577928644* ____imap_2_t3577928644 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3577928644::IID, reinterpret_cast<void**>(&____imap_2_t3577928644));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t3577928644->IMap_2_Clear_m2235974657();
	____imap_2_t3577928644->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// V Windows.Foundation.Collections.IMap`2<System.UInt32,System.Object>::Lookup(K)
extern "C"  RuntimeObject * IMap_2_Lookup_m2631401358 (RuntimeObject* __this, uint32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_Lookup_m2631401358_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t3004010646* ____imap_2_t3004010646 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3004010646::IID, reinterpret_cast<void**>(&____imap_2_t3004010646));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____imap_2_t3004010646->IMap_2_Lookup_m2631401358(___key0, &returnValue);
	____imap_2_t3004010646->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMap`2<System.UInt32,System.Object>::get_Size()
extern "C"  uint32_t IMap_2_get_Size_m3107625399 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t3004010646* ____imap_2_t3004010646 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3004010646::IID, reinterpret_cast<void**>(&____imap_2_t3004010646));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imap_2_t3004010646->IMap_2_get_Size_m3107625399(&returnValue);
	____imap_2_t3004010646->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.UInt32,System.Object>::HasKey(K)
extern "C"  bool IMap_2_HasKey_m1140347487 (RuntimeObject* __this, uint32_t ___key0, const RuntimeMethod* method)
{
	IMap_2_t3004010646* ____imap_2_t3004010646 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3004010646::IID, reinterpret_cast<void**>(&____imap_2_t3004010646));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t3004010646->IMap_2_HasKey_m1140347487(___key0, &returnValue);
	____imap_2_t3004010646->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2<System.UInt32,System.Object>::GetView()
extern "C"  RuntimeObject* IMap_2_GetView_m1344022084 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMap_2_GetView_m1344022084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMap_2_t3004010646* ____imap_2_t3004010646 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3004010646::IID, reinterpret_cast<void**>(&____imap_2_t3004010646));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IMapView_2_t1808383194* returnValue = NULL;
	hr = ____imap_2_t3004010646->IMap_2_GetView_m1344022084(&returnValue);
	____imap_2_t3004010646->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IMap`2<System.UInt32,System.Object>::Insert(K,V)
extern "C"  bool IMap_2_Insert_m3454051178 (RuntimeObject* __this, uint32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	IMap_2_t3004010646* ____imap_2_t3004010646 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3004010646::IID, reinterpret_cast<void**>(&____imap_2_t3004010646));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___value1' to native representation
	Il2CppIInspectable* ____value1_marshaled = NULL;
	if (___value1 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___value1))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___value1)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____value1_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____value1_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___value1);
		}
	}
	else
	{
		____value1_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imap_2_t3004010646->IMap_2_Insert_m3454051178(___key0, ____value1_marshaled, &returnValue);
	____imap_2_t3004010646->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___value1' native representation
	if (____value1_marshaled != NULL)
	{
		(____value1_marshaled)->Release();
		____value1_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMap`2<System.UInt32,System.Object>::Remove(K)
extern "C"  void IMap_2_Remove_m1413378520 (RuntimeObject* __this, uint32_t ___key0, const RuntimeMethod* method)
{
	IMap_2_t3004010646* ____imap_2_t3004010646 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3004010646::IID, reinterpret_cast<void**>(&____imap_2_t3004010646));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t3004010646->IMap_2_Remove_m1413378520(___key0);
	____imap_2_t3004010646->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IMap`2<System.UInt32,System.Object>::Clear()
extern "C"  void IMap_2_Clear_m3255529802 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMap_2_t3004010646* ____imap_2_t3004010646 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMap_2_t3004010646::IID, reinterpret_cast<void**>(&____imap_2_t3004010646));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____imap_2_t3004010646->IMap_2_Clear_m3255529802();
	____imap_2_t3004010646->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// V Windows.Foundation.Collections.IMapView`2<System.Guid,System.Object>::Lookup(K)
extern "C"  RuntimeObject * IMapView_2_Lookup_m3331997343 (RuntimeObject* __this, Guid_t  ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Lookup_m3331997343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t3878100361* ____imapView_2_t3878100361 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3878100361::IID, reinterpret_cast<void**>(&____imapView_2_t3878100361));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____imapView_2_t3878100361->IMapView_2_Lookup_m3331997343(___key0, &returnValue);
	____imapView_2_t3878100361->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMapView`2<System.Guid,System.Object>::get_Size()
extern "C"  uint32_t IMapView_2_get_Size_m3858358881 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMapView_2_t3878100361* ____imapView_2_t3878100361 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3878100361::IID, reinterpret_cast<void**>(&____imapView_2_t3878100361));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imapView_2_t3878100361->IMapView_2_get_Size_m3858358881(&returnValue);
	____imapView_2_t3878100361->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMapView`2<System.Guid,System.Object>::HasKey(K)
extern "C"  bool IMapView_2_HasKey_m3111850129 (RuntimeObject* __this, Guid_t  ___key0, const RuntimeMethod* method)
{
	IMapView_2_t3878100361* ____imapView_2_t3878100361 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3878100361::IID, reinterpret_cast<void**>(&____imapView_2_t3878100361));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t3878100361->IMapView_2_HasKey_m3111850129(___key0, &returnValue);
	____imapView_2_t3878100361->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMapView`2<System.Guid,System.Object>::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
extern "C"  void IMapView_2_Split_m3394162196 (RuntimeObject* __this, RuntimeObject** ___first0, RuntimeObject** ___second1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Split_m3394162196_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t3878100361* ____imapView_2_t3878100361 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3878100361::IID, reinterpret_cast<void**>(&____imapView_2_t3878100361));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' to native representation
	IMapView_2_t3878100361* ____first0_empty = NULL;
	IMapView_2_t3878100361** ____first0_marshaled = &____first0_empty;

	// Marshaling of parameter '___second1' to native representation
	IMapView_2_t3878100361* ____second1_empty = NULL;
	IMapView_2_t3878100361** ____second1_marshaled = &____second1_empty;

	// Native function invocation
	hr = ____imapView_2_t3878100361->IMapView_2_Split_m3394162196(____first0_marshaled, ____second1_marshaled);
	____imapView_2_t3878100361->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' back from native representation
	RuntimeObject* _____first0_marshaled_unmarshaled_dereferenced = NULL;
	if (*____first0_marshaled != NULL)
	{
		_____first0_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____first0_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____first0_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___first0 = _____first0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___first0' native representation
	if (*____first0_marshaled != NULL)
	{
		(*____first0_marshaled)->Release();
		*____first0_marshaled = NULL;
	}

	// Marshaling of parameter '___second1' back from native representation
	RuntimeObject* _____second1_marshaled_unmarshaled_dereferenced = NULL;
	if (*____second1_marshaled != NULL)
	{
		_____second1_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____second1_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____second1_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___second1 = _____second1_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___second1' native representation
	if (*____second1_marshaled != NULL)
	{
		(*____second1_marshaled)->Release();
		*____second1_marshaled = NULL;
	}

}
// V Windows.Foundation.Collections.IMapView`2<System.Guid,System.Type>::Lookup(K)
extern "C"  Type_t * IMapView_2_Lookup_m3896662699 (RuntimeObject* __this, Guid_t  ___key0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.UInt32 Windows.Foundation.Collections.IMapView`2<System.Guid,System.Type>::get_Size()
extern "C"  uint32_t IMapView_2_get_Size_m2349127300 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMapView_2_t3281938957* ____imapView_2_t3281938957 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3281938957::IID, reinterpret_cast<void**>(&____imapView_2_t3281938957));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imapView_2_t3281938957->IMapView_2_get_Size_m2349127300(&returnValue);
	____imapView_2_t3281938957->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMapView`2<System.Guid,System.Type>::HasKey(K)
extern "C"  bool IMapView_2_HasKey_m1382000797 (RuntimeObject* __this, Guid_t  ___key0, const RuntimeMethod* method)
{
	IMapView_2_t3281938957* ____imapView_2_t3281938957 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3281938957::IID, reinterpret_cast<void**>(&____imapView_2_t3281938957));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t3281938957->IMapView_2_HasKey_m1382000797(___key0, &returnValue);
	____imapView_2_t3281938957->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMapView`2<System.Guid,System.Type>::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
extern "C"  void IMapView_2_Split_m2592840147 (RuntimeObject* __this, RuntimeObject** ___first0, RuntimeObject** ___second1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Split_m2592840147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t3281938957* ____imapView_2_t3281938957 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3281938957::IID, reinterpret_cast<void**>(&____imapView_2_t3281938957));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' to native representation
	IMapView_2_t3281938957* ____first0_empty = NULL;
	IMapView_2_t3281938957** ____first0_marshaled = &____first0_empty;

	// Marshaling of parameter '___second1' to native representation
	IMapView_2_t3281938957* ____second1_empty = NULL;
	IMapView_2_t3281938957** ____second1_marshaled = &____second1_empty;

	// Native function invocation
	hr = ____imapView_2_t3281938957->IMapView_2_Split_m2592840147(____first0_marshaled, ____second1_marshaled);
	____imapView_2_t3281938957->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' back from native representation
	RuntimeObject* _____first0_marshaled_unmarshaled_dereferenced = NULL;
	if (*____first0_marshaled != NULL)
	{
		_____first0_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____first0_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____first0_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___first0 = _____first0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___first0' native representation
	if (*____first0_marshaled != NULL)
	{
		(*____first0_marshaled)->Release();
		*____first0_marshaled = NULL;
	}

	// Marshaling of parameter '___second1' back from native representation
	RuntimeObject* _____second1_marshaled_unmarshaled_dereferenced = NULL;
	if (*____second1_marshaled != NULL)
	{
		_____second1_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____second1_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____second1_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___second1 = _____second1_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___second1' native representation
	if (*____second1_marshaled != NULL)
	{
		(*____second1_marshaled)->Release();
		*____second1_marshaled = NULL;
	}

}
// V Windows.Foundation.Collections.IMapView`2<System.Int32,System.Object>::Lookup(K)
extern "C"  RuntimeObject * IMapView_2_Lookup_m3742065343 (RuntimeObject* __this, int32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Lookup_m3742065343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t3717796159* ____imapView_2_t3717796159 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3717796159::IID, reinterpret_cast<void**>(&____imapView_2_t3717796159));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____imapView_2_t3717796159->IMapView_2_Lookup_m3742065343(___key0, &returnValue);
	____imapView_2_t3717796159->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMapView`2<System.Int32,System.Object>::get_Size()
extern "C"  uint32_t IMapView_2_get_Size_m1359240774 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMapView_2_t3717796159* ____imapView_2_t3717796159 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3717796159::IID, reinterpret_cast<void**>(&____imapView_2_t3717796159));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imapView_2_t3717796159->IMapView_2_get_Size_m1359240774(&returnValue);
	____imapView_2_t3717796159->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMapView`2<System.Int32,System.Object>::HasKey(K)
extern "C"  bool IMapView_2_HasKey_m973232472 (RuntimeObject* __this, int32_t ___key0, const RuntimeMethod* method)
{
	IMapView_2_t3717796159* ____imapView_2_t3717796159 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3717796159::IID, reinterpret_cast<void**>(&____imapView_2_t3717796159));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t3717796159->IMapView_2_HasKey_m973232472(___key0, &returnValue);
	____imapView_2_t3717796159->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMapView`2<System.Int32,System.Object>::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
extern "C"  void IMapView_2_Split_m842968636 (RuntimeObject* __this, RuntimeObject** ___first0, RuntimeObject** ___second1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Split_m842968636_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t3717796159* ____imapView_2_t3717796159 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3717796159::IID, reinterpret_cast<void**>(&____imapView_2_t3717796159));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' to native representation
	IMapView_2_t3717796159* ____first0_empty = NULL;
	IMapView_2_t3717796159** ____first0_marshaled = &____first0_empty;

	// Marshaling of parameter '___second1' to native representation
	IMapView_2_t3717796159* ____second1_empty = NULL;
	IMapView_2_t3717796159** ____second1_marshaled = &____second1_empty;

	// Native function invocation
	hr = ____imapView_2_t3717796159->IMapView_2_Split_m842968636(____first0_marshaled, ____second1_marshaled);
	____imapView_2_t3717796159->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' back from native representation
	RuntimeObject* _____first0_marshaled_unmarshaled_dereferenced = NULL;
	if (*____first0_marshaled != NULL)
	{
		_____first0_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____first0_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____first0_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___first0 = _____first0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___first0' native representation
	if (*____first0_marshaled != NULL)
	{
		(*____first0_marshaled)->Release();
		*____first0_marshaled = NULL;
	}

	// Marshaling of parameter '___second1' back from native representation
	RuntimeObject* _____second1_marshaled_unmarshaled_dereferenced = NULL;
	if (*____second1_marshaled != NULL)
	{
		_____second1_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____second1_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____second1_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___second1 = _____second1_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___second1' native representation
	if (*____second1_marshaled != NULL)
	{
		(*____second1_marshaled)->Release();
		*____second1_marshaled = NULL;
	}

}
// V Windows.Foundation.Collections.IMapView`2<System.Int64,System.Object>::Lookup(K)
extern "C"  RuntimeObject * IMapView_2_Lookup_m2833276555 (RuntimeObject* __this, int64_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Lookup_m2833276555_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t1596755316* ____imapView_2_t1596755316 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1596755316::IID, reinterpret_cast<void**>(&____imapView_2_t1596755316));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____imapView_2_t1596755316->IMapView_2_Lookup_m2833276555(___key0, &returnValue);
	____imapView_2_t1596755316->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMapView`2<System.Int64,System.Object>::get_Size()
extern "C"  uint32_t IMapView_2_get_Size_m2657859418 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMapView_2_t1596755316* ____imapView_2_t1596755316 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1596755316::IID, reinterpret_cast<void**>(&____imapView_2_t1596755316));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imapView_2_t1596755316->IMapView_2_get_Size_m2657859418(&returnValue);
	____imapView_2_t1596755316->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMapView`2<System.Int64,System.Object>::HasKey(K)
extern "C"  bool IMapView_2_HasKey_m1129223030 (RuntimeObject* __this, int64_t ___key0, const RuntimeMethod* method)
{
	IMapView_2_t1596755316* ____imapView_2_t1596755316 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1596755316::IID, reinterpret_cast<void**>(&____imapView_2_t1596755316));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t1596755316->IMapView_2_HasKey_m1129223030(___key0, &returnValue);
	____imapView_2_t1596755316->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMapView`2<System.Int64,System.Object>::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
extern "C"  void IMapView_2_Split_m2875932391 (RuntimeObject* __this, RuntimeObject** ___first0, RuntimeObject** ___second1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Split_m2875932391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t1596755316* ____imapView_2_t1596755316 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1596755316::IID, reinterpret_cast<void**>(&____imapView_2_t1596755316));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' to native representation
	IMapView_2_t1596755316* ____first0_empty = NULL;
	IMapView_2_t1596755316** ____first0_marshaled = &____first0_empty;

	// Marshaling of parameter '___second1' to native representation
	IMapView_2_t1596755316* ____second1_empty = NULL;
	IMapView_2_t1596755316** ____second1_marshaled = &____second1_empty;

	// Native function invocation
	hr = ____imapView_2_t1596755316->IMapView_2_Split_m2875932391(____first0_marshaled, ____second1_marshaled);
	____imapView_2_t1596755316->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' back from native representation
	RuntimeObject* _____first0_marshaled_unmarshaled_dereferenced = NULL;
	if (*____first0_marshaled != NULL)
	{
		_____first0_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____first0_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____first0_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___first0 = _____first0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___first0' native representation
	if (*____first0_marshaled != NULL)
	{
		(*____first0_marshaled)->Release();
		*____first0_marshaled = NULL;
	}

	// Marshaling of parameter '___second1' back from native representation
	RuntimeObject* _____second1_marshaled_unmarshaled_dereferenced = NULL;
	if (*____second1_marshaled != NULL)
	{
		_____second1_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____second1_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____second1_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___second1 = _____second1_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___second1' native representation
	if (*____second1_marshaled != NULL)
	{
		(*____second1_marshaled)->Release();
		*____second1_marshaled = NULL;
	}

}
// V Windows.Foundation.Collections.IMapView`2<System.Object,System.Boolean>::Lookup(K)
extern "C"  bool IMapView_2_Lookup_m4048973909 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMapView_2_t3193670913* ____imapView_2_t3193670913 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3193670913::IID, reinterpret_cast<void**>(&____imapView_2_t3193670913));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t3193670913->IMapView_2_Lookup_m4048973909(____key0_marshaled, &returnValue);
	____imapView_2_t3193670913->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IMapView`2<System.Object,System.Boolean>::get_Size()
extern "C"  uint32_t IMapView_2_get_Size_m516432122 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMapView_2_t3193670913* ____imapView_2_t3193670913 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3193670913::IID, reinterpret_cast<void**>(&____imapView_2_t3193670913));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imapView_2_t3193670913->IMapView_2_get_Size_m516432122(&returnValue);
	____imapView_2_t3193670913->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMapView`2<System.Object,System.Boolean>::HasKey(K)
extern "C"  bool IMapView_2_HasKey_m1413942596 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMapView_2_t3193670913* ____imapView_2_t3193670913 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3193670913::IID, reinterpret_cast<void**>(&____imapView_2_t3193670913));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t3193670913->IMapView_2_HasKey_m1413942596(____key0_marshaled, &returnValue);
	____imapView_2_t3193670913->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMapView`2<System.Object,System.Boolean>::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
extern "C"  void IMapView_2_Split_m2647546983 (RuntimeObject* __this, RuntimeObject** ___first0, RuntimeObject** ___second1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Split_m2647546983_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t3193670913* ____imapView_2_t3193670913 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3193670913::IID, reinterpret_cast<void**>(&____imapView_2_t3193670913));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' to native representation
	IMapView_2_t3193670913* ____first0_empty = NULL;
	IMapView_2_t3193670913** ____first0_marshaled = &____first0_empty;

	// Marshaling of parameter '___second1' to native representation
	IMapView_2_t3193670913* ____second1_empty = NULL;
	IMapView_2_t3193670913** ____second1_marshaled = &____second1_empty;

	// Native function invocation
	hr = ____imapView_2_t3193670913->IMapView_2_Split_m2647546983(____first0_marshaled, ____second1_marshaled);
	____imapView_2_t3193670913->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' back from native representation
	RuntimeObject* _____first0_marshaled_unmarshaled_dereferenced = NULL;
	if (*____first0_marshaled != NULL)
	{
		_____first0_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____first0_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____first0_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___first0 = _____first0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___first0' native representation
	if (*____first0_marshaled != NULL)
	{
		(*____first0_marshaled)->Release();
		*____first0_marshaled = NULL;
	}

	// Marshaling of parameter '___second1' back from native representation
	RuntimeObject* _____second1_marshaled_unmarshaled_dereferenced = NULL;
	if (*____second1_marshaled != NULL)
	{
		_____second1_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____second1_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____second1_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___second1 = _____second1_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___second1' native representation
	if (*____second1_marshaled != NULL)
	{
		(*____second1_marshaled)->Release();
		*____second1_marshaled = NULL;
	}

}
// V Windows.Foundation.Collections.IMapView`2<System.Object,System.Int32>::Lookup(K)
extern "C"  int32_t IMapView_2_Lookup_m877167602 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMapView_2_t1752361405* ____imapView_2_t1752361405 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1752361405::IID, reinterpret_cast<void**>(&____imapView_2_t1752361405));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	int32_t returnValue = 0;
	hr = ____imapView_2_t1752361405->IMapView_2_Lookup_m877167602(____key0_marshaled, &returnValue);
	____imapView_2_t1752361405->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IMapView`2<System.Object,System.Int32>::get_Size()
extern "C"  uint32_t IMapView_2_get_Size_m3871500701 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMapView_2_t1752361405* ____imapView_2_t1752361405 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1752361405::IID, reinterpret_cast<void**>(&____imapView_2_t1752361405));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imapView_2_t1752361405->IMapView_2_get_Size_m3871500701(&returnValue);
	____imapView_2_t1752361405->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMapView`2<System.Object,System.Int32>::HasKey(K)
extern "C"  bool IMapView_2_HasKey_m1872447125 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMapView_2_t1752361405* ____imapView_2_t1752361405 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1752361405::IID, reinterpret_cast<void**>(&____imapView_2_t1752361405));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t1752361405->IMapView_2_HasKey_m1872447125(____key0_marshaled, &returnValue);
	____imapView_2_t1752361405->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMapView`2<System.Object,System.Int32>::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
extern "C"  void IMapView_2_Split_m4061037733 (RuntimeObject* __this, RuntimeObject** ___first0, RuntimeObject** ___second1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Split_m4061037733_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t1752361405* ____imapView_2_t1752361405 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1752361405::IID, reinterpret_cast<void**>(&____imapView_2_t1752361405));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' to native representation
	IMapView_2_t1752361405* ____first0_empty = NULL;
	IMapView_2_t1752361405** ____first0_marshaled = &____first0_empty;

	// Marshaling of parameter '___second1' to native representation
	IMapView_2_t1752361405* ____second1_empty = NULL;
	IMapView_2_t1752361405** ____second1_marshaled = &____second1_empty;

	// Native function invocation
	hr = ____imapView_2_t1752361405->IMapView_2_Split_m4061037733(____first0_marshaled, ____second1_marshaled);
	____imapView_2_t1752361405->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' back from native representation
	RuntimeObject* _____first0_marshaled_unmarshaled_dereferenced = NULL;
	if (*____first0_marshaled != NULL)
	{
		_____first0_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____first0_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____first0_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___first0 = _____first0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___first0' native representation
	if (*____first0_marshaled != NULL)
	{
		(*____first0_marshaled)->Release();
		*____first0_marshaled = NULL;
	}

	// Marshaling of parameter '___second1' back from native representation
	RuntimeObject* _____second1_marshaled_unmarshaled_dereferenced = NULL;
	if (*____second1_marshaled != NULL)
	{
		_____second1_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____second1_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____second1_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___second1 = _____second1_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___second1' native representation
	if (*____second1_marshaled != NULL)
	{
		(*____second1_marshaled)->Release();
		*____second1_marshaled = NULL;
	}

}
// V Windows.Foundation.Collections.IMapView`2<System.Object,System.Object>::Lookup(K)
extern "C"  RuntimeObject * IMapView_2_Lookup_m354916185 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Lookup_m354916185_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t1881521816* ____imapView_2_t1881521816 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1881521816::IID, reinterpret_cast<void**>(&____imapView_2_t1881521816));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____imapView_2_t1881521816->IMapView_2_Lookup_m354916185(____key0_marshaled, &returnValue);
	____imapView_2_t1881521816->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMapView`2<System.Object,System.Object>::get_Size()
extern "C"  uint32_t IMapView_2_get_Size_m1307357424 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMapView_2_t1881521816* ____imapView_2_t1881521816 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1881521816::IID, reinterpret_cast<void**>(&____imapView_2_t1881521816));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imapView_2_t1881521816->IMapView_2_get_Size_m1307357424(&returnValue);
	____imapView_2_t1881521816->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMapView`2<System.Object,System.Object>::HasKey(K)
extern "C"  bool IMapView_2_HasKey_m1226002878 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMapView_2_t1881521816* ____imapView_2_t1881521816 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1881521816::IID, reinterpret_cast<void**>(&____imapView_2_t1881521816));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t1881521816->IMapView_2_HasKey_m1226002878(____key0_marshaled, &returnValue);
	____imapView_2_t1881521816->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMapView`2<System.Object,System.Object>::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
extern "C"  void IMapView_2_Split_m3017881906 (RuntimeObject* __this, RuntimeObject** ___first0, RuntimeObject** ___second1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Split_m3017881906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t1881521816* ____imapView_2_t1881521816 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1881521816::IID, reinterpret_cast<void**>(&____imapView_2_t1881521816));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' to native representation
	IMapView_2_t1881521816* ____first0_empty = NULL;
	IMapView_2_t1881521816** ____first0_marshaled = &____first0_empty;

	// Marshaling of parameter '___second1' to native representation
	IMapView_2_t1881521816* ____second1_empty = NULL;
	IMapView_2_t1881521816** ____second1_marshaled = &____second1_empty;

	// Native function invocation
	hr = ____imapView_2_t1881521816->IMapView_2_Split_m3017881906(____first0_marshaled, ____second1_marshaled);
	____imapView_2_t1881521816->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' back from native representation
	RuntimeObject* _____first0_marshaled_unmarshaled_dereferenced = NULL;
	if (*____first0_marshaled != NULL)
	{
		_____first0_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____first0_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____first0_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___first0 = _____first0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___first0' native representation
	if (*____first0_marshaled != NULL)
	{
		(*____first0_marshaled)->Release();
		*____first0_marshaled = NULL;
	}

	// Marshaling of parameter '___second1' back from native representation
	RuntimeObject* _____second1_marshaled_unmarshaled_dereferenced = NULL;
	if (*____second1_marshaled != NULL)
	{
		_____second1_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____second1_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____second1_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___second1 = _____second1_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___second1' native representation
	if (*____second1_marshaled != NULL)
	{
		(*____second1_marshaled)->Release();
		*____second1_marshaled = NULL;
	}

}
// V Windows.Foundation.Collections.IMapView`2<System.Object,System.String>::Lookup(K)
extern "C"  String_t* IMapView_2_Lookup_m1181259780 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMapView_2_t648866341* ____imapView_2_t648866341 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t648866341::IID, reinterpret_cast<void**>(&____imapView_2_t648866341));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	Il2CppHString returnValue = NULL;
	hr = ____imapView_2_t648866341->IMapView_2_Lookup_m1181259780(____key0_marshaled, &returnValue);
	____imapView_2_t648866341->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_hstring_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free_hstring(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMapView`2<System.Object,System.String>::get_Size()
extern "C"  uint32_t IMapView_2_get_Size_m3995749853 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMapView_2_t648866341* ____imapView_2_t648866341 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t648866341::IID, reinterpret_cast<void**>(&____imapView_2_t648866341));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imapView_2_t648866341->IMapView_2_get_Size_m3995749853(&returnValue);
	____imapView_2_t648866341->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMapView`2<System.Object,System.String>::HasKey(K)
extern "C"  bool IMapView_2_HasKey_m2611237161 (RuntimeObject* __this, RuntimeObject * ___key0, const RuntimeMethod* method)
{
	IMapView_2_t648866341* ____imapView_2_t648866341 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t648866341::IID, reinterpret_cast<void**>(&____imapView_2_t648866341));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppIInspectable* ____key0_marshaled = NULL;
	if (___key0 != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(___key0))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)___key0)->identity->QueryInterface(Il2CppIInspectable::IID, reinterpret_cast<void**>(&____key0_marshaled));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			____key0_marshaled = il2cpp_codegen_com_get_or_create_ccw<Il2CppIInspectable>(___key0);
		}
	}
	else
	{
		____key0_marshaled = NULL;
	}

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t648866341->IMapView_2_HasKey_m2611237161(____key0_marshaled, &returnValue);
	____imapView_2_t648866341->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling cleanup of parameter '___key0' native representation
	if (____key0_marshaled != NULL)
	{
		(____key0_marshaled)->Release();
		____key0_marshaled = NULL;
	}

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMapView`2<System.Object,System.String>::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
extern "C"  void IMapView_2_Split_m1609902508 (RuntimeObject* __this, RuntimeObject** ___first0, RuntimeObject** ___second1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Split_m1609902508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t648866341* ____imapView_2_t648866341 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t648866341::IID, reinterpret_cast<void**>(&____imapView_2_t648866341));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' to native representation
	IMapView_2_t648866341* ____first0_empty = NULL;
	IMapView_2_t648866341** ____first0_marshaled = &____first0_empty;

	// Marshaling of parameter '___second1' to native representation
	IMapView_2_t648866341* ____second1_empty = NULL;
	IMapView_2_t648866341** ____second1_marshaled = &____second1_empty;

	// Native function invocation
	hr = ____imapView_2_t648866341->IMapView_2_Split_m1609902508(____first0_marshaled, ____second1_marshaled);
	____imapView_2_t648866341->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' back from native representation
	RuntimeObject* _____first0_marshaled_unmarshaled_dereferenced = NULL;
	if (*____first0_marshaled != NULL)
	{
		_____first0_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____first0_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____first0_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___first0 = _____first0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___first0' native representation
	if (*____first0_marshaled != NULL)
	{
		(*____first0_marshaled)->Release();
		*____first0_marshaled = NULL;
	}

	// Marshaling of parameter '___second1' back from native representation
	RuntimeObject* _____second1_marshaled_unmarshaled_dereferenced = NULL;
	if (*____second1_marshaled != NULL)
	{
		_____second1_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____second1_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____second1_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___second1 = _____second1_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___second1' native representation
	if (*____second1_marshaled != NULL)
	{
		(*____second1_marshaled)->Release();
		*____second1_marshaled = NULL;
	}

}
// V Windows.Foundation.Collections.IMapView`2<System.String,System.Boolean>::Lookup(K)
extern "C"  bool IMapView_2_Lookup_m818349798 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMapView_2_t1631520928* ____imapView_2_t1631520928 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1631520928::IID, reinterpret_cast<void**>(&____imapView_2_t1631520928));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t1631520928->IMapView_2_Lookup_m818349798(____key0_marshaled, &returnValue);
	____imapView_2_t1631520928->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IMapView`2<System.String,System.Boolean>::get_Size()
extern "C"  uint32_t IMapView_2_get_Size_m1396842905 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMapView_2_t1631520928* ____imapView_2_t1631520928 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1631520928::IID, reinterpret_cast<void**>(&____imapView_2_t1631520928));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imapView_2_t1631520928->IMapView_2_get_Size_m1396842905(&returnValue);
	____imapView_2_t1631520928->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMapView`2<System.String,System.Boolean>::HasKey(K)
extern "C"  bool IMapView_2_HasKey_m2200864154 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMapView_2_t1631520928* ____imapView_2_t1631520928 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1631520928::IID, reinterpret_cast<void**>(&____imapView_2_t1631520928));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t1631520928->IMapView_2_HasKey_m2200864154(____key0_marshaled, &returnValue);
	____imapView_2_t1631520928->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMapView`2<System.String,System.Boolean>::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
extern "C"  void IMapView_2_Split_m22494317 (RuntimeObject* __this, RuntimeObject** ___first0, RuntimeObject** ___second1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Split_m22494317_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t1631520928* ____imapView_2_t1631520928 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1631520928::IID, reinterpret_cast<void**>(&____imapView_2_t1631520928));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' to native representation
	IMapView_2_t1631520928* ____first0_empty = NULL;
	IMapView_2_t1631520928** ____first0_marshaled = &____first0_empty;

	// Marshaling of parameter '___second1' to native representation
	IMapView_2_t1631520928* ____second1_empty = NULL;
	IMapView_2_t1631520928** ____second1_marshaled = &____second1_empty;

	// Native function invocation
	hr = ____imapView_2_t1631520928->IMapView_2_Split_m22494317(____first0_marshaled, ____second1_marshaled);
	____imapView_2_t1631520928->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' back from native representation
	RuntimeObject* _____first0_marshaled_unmarshaled_dereferenced = NULL;
	if (*____first0_marshaled != NULL)
	{
		_____first0_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____first0_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____first0_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___first0 = _____first0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___first0' native representation
	if (*____first0_marshaled != NULL)
	{
		(*____first0_marshaled)->Release();
		*____first0_marshaled = NULL;
	}

	// Marshaling of parameter '___second1' back from native representation
	RuntimeObject* _____second1_marshaled_unmarshaled_dereferenced = NULL;
	if (*____second1_marshaled != NULL)
	{
		_____second1_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____second1_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____second1_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___second1 = _____second1_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___second1' native representation
	if (*____second1_marshaled != NULL)
	{
		(*____second1_marshaled)->Release();
		*____second1_marshaled = NULL;
	}

}
// V Windows.Foundation.Collections.IMapView`2<System.String,System.Int32>::Lookup(K)
extern "C"  int32_t IMapView_2_Lookup_m1298530599 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMapView_2_t190211420* ____imapView_2_t190211420 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t190211420::IID, reinterpret_cast<void**>(&____imapView_2_t190211420));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	int32_t returnValue = 0;
	hr = ____imapView_2_t190211420->IMapView_2_Lookup_m1298530599(____key0_marshaled, &returnValue);
	____imapView_2_t190211420->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IMapView`2<System.String,System.Int32>::get_Size()
extern "C"  uint32_t IMapView_2_get_Size_m546192505 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMapView_2_t190211420* ____imapView_2_t190211420 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t190211420::IID, reinterpret_cast<void**>(&____imapView_2_t190211420));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imapView_2_t190211420->IMapView_2_get_Size_m546192505(&returnValue);
	____imapView_2_t190211420->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMapView`2<System.String,System.Int32>::HasKey(K)
extern "C"  bool IMapView_2_HasKey_m3322124696 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMapView_2_t190211420* ____imapView_2_t190211420 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t190211420::IID, reinterpret_cast<void**>(&____imapView_2_t190211420));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t190211420->IMapView_2_HasKey_m3322124696(____key0_marshaled, &returnValue);
	____imapView_2_t190211420->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMapView`2<System.String,System.Int32>::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
extern "C"  void IMapView_2_Split_m96786832 (RuntimeObject* __this, RuntimeObject** ___first0, RuntimeObject** ___second1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Split_m96786832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t190211420* ____imapView_2_t190211420 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t190211420::IID, reinterpret_cast<void**>(&____imapView_2_t190211420));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' to native representation
	IMapView_2_t190211420* ____first0_empty = NULL;
	IMapView_2_t190211420** ____first0_marshaled = &____first0_empty;

	// Marshaling of parameter '___second1' to native representation
	IMapView_2_t190211420* ____second1_empty = NULL;
	IMapView_2_t190211420** ____second1_marshaled = &____second1_empty;

	// Native function invocation
	hr = ____imapView_2_t190211420->IMapView_2_Split_m96786832(____first0_marshaled, ____second1_marshaled);
	____imapView_2_t190211420->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' back from native representation
	RuntimeObject* _____first0_marshaled_unmarshaled_dereferenced = NULL;
	if (*____first0_marshaled != NULL)
	{
		_____first0_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____first0_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____first0_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___first0 = _____first0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___first0' native representation
	if (*____first0_marshaled != NULL)
	{
		(*____first0_marshaled)->Release();
		*____first0_marshaled = NULL;
	}

	// Marshaling of parameter '___second1' back from native representation
	RuntimeObject* _____second1_marshaled_unmarshaled_dereferenced = NULL;
	if (*____second1_marshaled != NULL)
	{
		_____second1_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____second1_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____second1_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___second1 = _____second1_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___second1' native representation
	if (*____second1_marshaled != NULL)
	{
		(*____second1_marshaled)->Release();
		*____second1_marshaled = NULL;
	}

}
// V Windows.Foundation.Collections.IMapView`2<System.String,System.Object>::Lookup(K)
extern "C"  RuntimeObject * IMapView_2_Lookup_m2993729319 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Lookup_m2993729319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t319371831* ____imapView_2_t319371831 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t319371831::IID, reinterpret_cast<void**>(&____imapView_2_t319371831));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____imapView_2_t319371831->IMapView_2_Lookup_m2993729319(____key0_marshaled, &returnValue);
	____imapView_2_t319371831->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMapView`2<System.String,System.Object>::get_Size()
extern "C"  uint32_t IMapView_2_get_Size_m2008534305 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMapView_2_t319371831* ____imapView_2_t319371831 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t319371831::IID, reinterpret_cast<void**>(&____imapView_2_t319371831));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imapView_2_t319371831->IMapView_2_get_Size_m2008534305(&returnValue);
	____imapView_2_t319371831->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMapView`2<System.String,System.Object>::HasKey(K)
extern "C"  bool IMapView_2_HasKey_m3205845981 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMapView_2_t319371831* ____imapView_2_t319371831 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t319371831::IID, reinterpret_cast<void**>(&____imapView_2_t319371831));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t319371831->IMapView_2_HasKey_m3205845981(____key0_marshaled, &returnValue);
	____imapView_2_t319371831->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMapView`2<System.String,System.Object>::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
extern "C"  void IMapView_2_Split_m1626200424 (RuntimeObject* __this, RuntimeObject** ___first0, RuntimeObject** ___second1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Split_m1626200424_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t319371831* ____imapView_2_t319371831 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t319371831::IID, reinterpret_cast<void**>(&____imapView_2_t319371831));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' to native representation
	IMapView_2_t319371831* ____first0_empty = NULL;
	IMapView_2_t319371831** ____first0_marshaled = &____first0_empty;

	// Marshaling of parameter '___second1' to native representation
	IMapView_2_t319371831* ____second1_empty = NULL;
	IMapView_2_t319371831** ____second1_marshaled = &____second1_empty;

	// Native function invocation
	hr = ____imapView_2_t319371831->IMapView_2_Split_m1626200424(____first0_marshaled, ____second1_marshaled);
	____imapView_2_t319371831->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' back from native representation
	RuntimeObject* _____first0_marshaled_unmarshaled_dereferenced = NULL;
	if (*____first0_marshaled != NULL)
	{
		_____first0_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____first0_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____first0_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___first0 = _____first0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___first0' native representation
	if (*____first0_marshaled != NULL)
	{
		(*____first0_marshaled)->Release();
		*____first0_marshaled = NULL;
	}

	// Marshaling of parameter '___second1' back from native representation
	RuntimeObject* _____second1_marshaled_unmarshaled_dereferenced = NULL;
	if (*____second1_marshaled != NULL)
	{
		_____second1_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____second1_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____second1_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___second1 = _____second1_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___second1' native representation
	if (*____second1_marshaled != NULL)
	{
		(*____second1_marshaled)->Release();
		*____second1_marshaled = NULL;
	}

}
// V Windows.Foundation.Collections.IMapView`2<System.String,System.String>::Lookup(K)
extern "C"  String_t* IMapView_2_Lookup_m1783676987 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMapView_2_t3381683652* ____imapView_2_t3381683652 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3381683652::IID, reinterpret_cast<void**>(&____imapView_2_t3381683652));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	Il2CppHString returnValue = NULL;
	hr = ____imapView_2_t3381683652->IMapView_2_Lookup_m1783676987(____key0_marshaled, &returnValue);
	____imapView_2_t3381683652->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_hstring_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free_hstring(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMapView`2<System.String,System.String>::get_Size()
extern "C"  uint32_t IMapView_2_get_Size_m3316105878 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMapView_2_t3381683652* ____imapView_2_t3381683652 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3381683652::IID, reinterpret_cast<void**>(&____imapView_2_t3381683652));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imapView_2_t3381683652->IMapView_2_get_Size_m3316105878(&returnValue);
	____imapView_2_t3381683652->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMapView`2<System.String,System.String>::HasKey(K)
extern "C"  bool IMapView_2_HasKey_m1003035185 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMapView_2_t3381683652* ____imapView_2_t3381683652 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3381683652::IID, reinterpret_cast<void**>(&____imapView_2_t3381683652));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t3381683652->IMapView_2_HasKey_m1003035185(____key0_marshaled, &returnValue);
	____imapView_2_t3381683652->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMapView`2<System.String,System.String>::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
extern "C"  void IMapView_2_Split_m978754527 (RuntimeObject* __this, RuntimeObject** ___first0, RuntimeObject** ___second1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Split_m978754527_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t3381683652* ____imapView_2_t3381683652 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t3381683652::IID, reinterpret_cast<void**>(&____imapView_2_t3381683652));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' to native representation
	IMapView_2_t3381683652* ____first0_empty = NULL;
	IMapView_2_t3381683652** ____first0_marshaled = &____first0_empty;

	// Marshaling of parameter '___second1' to native representation
	IMapView_2_t3381683652* ____second1_empty = NULL;
	IMapView_2_t3381683652** ____second1_marshaled = &____second1_empty;

	// Native function invocation
	hr = ____imapView_2_t3381683652->IMapView_2_Split_m978754527(____first0_marshaled, ____second1_marshaled);
	____imapView_2_t3381683652->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' back from native representation
	RuntimeObject* _____first0_marshaled_unmarshaled_dereferenced = NULL;
	if (*____first0_marshaled != NULL)
	{
		_____first0_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____first0_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____first0_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___first0 = _____first0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___first0' native representation
	if (*____first0_marshaled != NULL)
	{
		(*____first0_marshaled)->Release();
		*____first0_marshaled = NULL;
	}

	// Marshaling of parameter '___second1' back from native representation
	RuntimeObject* _____second1_marshaled_unmarshaled_dereferenced = NULL;
	if (*____second1_marshaled != NULL)
	{
		_____second1_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____second1_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____second1_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___second1 = _____second1_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___second1' native representation
	if (*____second1_marshaled != NULL)
	{
		(*____second1_marshaled)->Release();
		*____second1_marshaled = NULL;
	}

}
// V Windows.Foundation.Collections.IMapView`2<System.String,System.Type>::Lookup(K)
extern "C"  Type_t * IMapView_2_Lookup_m594002932 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.UInt32 Windows.Foundation.Collections.IMapView`2<System.String,System.Type>::get_Size()
extern "C"  uint32_t IMapView_2_get_Size_m3065552575 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMapView_2_t4018177723* ____imapView_2_t4018177723 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t4018177723::IID, reinterpret_cast<void**>(&____imapView_2_t4018177723));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imapView_2_t4018177723->IMapView_2_get_Size_m3065552575(&returnValue);
	____imapView_2_t4018177723->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMapView`2<System.String,System.Type>::HasKey(K)
extern "C"  bool IMapView_2_HasKey_m1694676420 (RuntimeObject* __this, String_t* ___key0, const RuntimeMethod* method)
{
	IMapView_2_t4018177723* ____imapView_2_t4018177723 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t4018177723::IID, reinterpret_cast<void**>(&____imapView_2_t4018177723));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___key0' to native representation
	Il2CppHString ____key0_marshaled = NULL;
	if (___key0 == NULL)
	{
		IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("key"));
	}

	DECLARE_IL2CPP_STRING_AS_STRING_VIEW_OF_NATIVE_CHARS(___key0NativeView, ___key0);
	il2cpp::utils::Il2CppHStringReference ___key0HStringReference(___key0NativeView);
	____key0_marshaled = ___key0HStringReference;

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t4018177723->IMapView_2_HasKey_m1694676420(____key0_marshaled, &returnValue);
	____imapView_2_t4018177723->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMapView`2<System.String,System.Type>::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
extern "C"  void IMapView_2_Split_m3605358933 (RuntimeObject* __this, RuntimeObject** ___first0, RuntimeObject** ___second1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Split_m3605358933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t4018177723* ____imapView_2_t4018177723 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t4018177723::IID, reinterpret_cast<void**>(&____imapView_2_t4018177723));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' to native representation
	IMapView_2_t4018177723* ____first0_empty = NULL;
	IMapView_2_t4018177723** ____first0_marshaled = &____first0_empty;

	// Marshaling of parameter '___second1' to native representation
	IMapView_2_t4018177723* ____second1_empty = NULL;
	IMapView_2_t4018177723** ____second1_marshaled = &____second1_empty;

	// Native function invocation
	hr = ____imapView_2_t4018177723->IMapView_2_Split_m3605358933(____first0_marshaled, ____second1_marshaled);
	____imapView_2_t4018177723->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' back from native representation
	RuntimeObject* _____first0_marshaled_unmarshaled_dereferenced = NULL;
	if (*____first0_marshaled != NULL)
	{
		_____first0_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____first0_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____first0_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___first0 = _____first0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___first0' native representation
	if (*____first0_marshaled != NULL)
	{
		(*____first0_marshaled)->Release();
		*____first0_marshaled = NULL;
	}

	// Marshaling of parameter '___second1' back from native representation
	RuntimeObject* _____second1_marshaled_unmarshaled_dereferenced = NULL;
	if (*____second1_marshaled != NULL)
	{
		_____second1_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____second1_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____second1_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___second1 = _____second1_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___second1' native representation
	if (*____second1_marshaled != NULL)
	{
		(*____second1_marshaled)->Release();
		*____second1_marshaled = NULL;
	}

}
// V Windows.Foundation.Collections.IMapView`2<System.Type,System.Type>::Lookup(K)
extern "C"  Type_t * IMapView_2_Lookup_m580708534 (RuntimeObject* __this, Type_t * ___key0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.UInt32 Windows.Foundation.Collections.IMapView`2<System.Type,System.Type>::get_Size()
extern "C"  uint32_t IMapView_2_get_Size_m2678681203 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMapView_2_t2382301192* ____imapView_2_t2382301192 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t2382301192::IID, reinterpret_cast<void**>(&____imapView_2_t2382301192));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imapView_2_t2382301192->IMapView_2_get_Size_m2678681203(&returnValue);
	____imapView_2_t2382301192->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMapView`2<System.Type,System.Type>::HasKey(K)
extern "C"  bool IMapView_2_HasKey_m1363070300 (RuntimeObject* __this, Type_t * ___key0, const RuntimeMethod* method)
{
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_marshal_directive_exception("Cannot marshal type 'System.Type'."));
}
// System.Void Windows.Foundation.Collections.IMapView`2<System.Type,System.Type>::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
extern "C"  void IMapView_2_Split_m964547936 (RuntimeObject* __this, RuntimeObject** ___first0, RuntimeObject** ___second1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Split_m964547936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t2382301192* ____imapView_2_t2382301192 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t2382301192::IID, reinterpret_cast<void**>(&____imapView_2_t2382301192));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' to native representation
	IMapView_2_t2382301192* ____first0_empty = NULL;
	IMapView_2_t2382301192** ____first0_marshaled = &____first0_empty;

	// Marshaling of parameter '___second1' to native representation
	IMapView_2_t2382301192* ____second1_empty = NULL;
	IMapView_2_t2382301192** ____second1_marshaled = &____second1_empty;

	// Native function invocation
	hr = ____imapView_2_t2382301192->IMapView_2_Split_m964547936(____first0_marshaled, ____second1_marshaled);
	____imapView_2_t2382301192->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' back from native representation
	RuntimeObject* _____first0_marshaled_unmarshaled_dereferenced = NULL;
	if (*____first0_marshaled != NULL)
	{
		_____first0_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____first0_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____first0_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___first0 = _____first0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___first0' native representation
	if (*____first0_marshaled != NULL)
	{
		(*____first0_marshaled)->Release();
		*____first0_marshaled = NULL;
	}

	// Marshaling of parameter '___second1' back from native representation
	RuntimeObject* _____second1_marshaled_unmarshaled_dereferenced = NULL;
	if (*____second1_marshaled != NULL)
	{
		_____second1_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____second1_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____second1_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___second1 = _____second1_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___second1' native representation
	if (*____second1_marshaled != NULL)
	{
		(*____second1_marshaled)->Release();
		*____second1_marshaled = NULL;
	}

}
// V Windows.Foundation.Collections.IMapView`2<System.UInt32,System.Object>::Lookup(K)
extern "C"  RuntimeObject * IMapView_2_Lookup_m3822737838 (RuntimeObject* __this, uint32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Lookup_m3822737838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t1808383194* ____imapView_2_t1808383194 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1808383194::IID, reinterpret_cast<void**>(&____imapView_2_t1808383194));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppIInspectable* returnValue = NULL;
	hr = ____imapView_2_t1808383194->IMapView_2_Lookup_m3822737838(___key0, &returnValue);
	____imapView_2_t1808383194->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject * _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.UInt32 Windows.Foundation.Collections.IMapView`2<System.UInt32,System.Object>::get_Size()
extern "C"  uint32_t IMapView_2_get_Size_m142448920 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IMapView_2_t1808383194* ____imapView_2_t1808383194 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1808383194::IID, reinterpret_cast<void**>(&____imapView_2_t1808383194));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____imapView_2_t1808383194->IMapView_2_get_Size_m142448920(&returnValue);
	____imapView_2_t1808383194->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Boolean Windows.Foundation.Collections.IMapView`2<System.UInt32,System.Object>::HasKey(K)
extern "C"  bool IMapView_2_HasKey_m2107050039 (RuntimeObject* __this, uint32_t ___key0, const RuntimeMethod* method)
{
	IMapView_2_t1808383194* ____imapView_2_t1808383194 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1808383194::IID, reinterpret_cast<void**>(&____imapView_2_t1808383194));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____imapView_2_t1808383194->IMapView_2_HasKey_m2107050039(___key0, &returnValue);
	____imapView_2_t1808383194->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IMapView`2<System.UInt32,System.Object>::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
extern "C"  void IMapView_2_Split_m4049158937 (RuntimeObject* __this, RuntimeObject** ___first0, RuntimeObject** ___second1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IMapView_2_Split_m4049158937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IMapView_2_t1808383194* ____imapView_2_t1808383194 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IMapView_2_t1808383194::IID, reinterpret_cast<void**>(&____imapView_2_t1808383194));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' to native representation
	IMapView_2_t1808383194* ____first0_empty = NULL;
	IMapView_2_t1808383194** ____first0_marshaled = &____first0_empty;

	// Marshaling of parameter '___second1' to native representation
	IMapView_2_t1808383194* ____second1_empty = NULL;
	IMapView_2_t1808383194** ____second1_marshaled = &____second1_empty;

	// Native function invocation
	hr = ____imapView_2_t1808383194->IMapView_2_Split_m4049158937(____first0_marshaled, ____second1_marshaled);
	____imapView_2_t1808383194->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___first0' back from native representation
	RuntimeObject* _____first0_marshaled_unmarshaled_dereferenced = NULL;
	if (*____first0_marshaled != NULL)
	{
		_____first0_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____first0_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____first0_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___first0 = _____first0_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___first0' native representation
	if (*____first0_marshaled != NULL)
	{
		(*____first0_marshaled)->Release();
		*____first0_marshaled = NULL;
	}

	// Marshaling of parameter '___second1' back from native representation
	RuntimeObject* _____second1_marshaled_unmarshaled_dereferenced = NULL;
	if (*____second1_marshaled != NULL)
	{
		_____second1_marshaled_unmarshaled_dereferenced = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(*____second1_marshaled, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_____second1_marshaled_unmarshaled_dereferenced = NULL;
	}
	*___second1 = _____second1_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___second1' native representation
	if (*____second1_marshaled != NULL)
	{
		(*____second1_marshaled)->Release();
		*____second1_marshaled = NULL;
	}

}
// T Windows.Foundation.Collections.IVector`1<System.Boolean>::GetAt(System.UInt32)
extern "C"  bool IVector_1_GetAt_m673046332 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t837909686* ____ivector_1_t837909686 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t837909686::IID, reinterpret_cast<void**>(&____ivector_1_t837909686));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t837909686->IVector_1_GetAt_m673046332(___index0, &returnValue);
	____ivector_1_t837909686->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Boolean>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m1880168052 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t837909686* ____ivector_1_t837909686 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t837909686::IID, reinterpret_cast<void**>(&____ivector_1_t837909686));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t837909686->IVector_1_get_Size_m1880168052(&returnValue);
	____ivector_1_t837909686->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.Boolean>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m3248688482 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m3248688482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t837909686* ____ivector_1_t837909686 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t837909686::IID, reinterpret_cast<void**>(&____ivector_1_t837909686));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t3731120523* returnValue = NULL;
	hr = ____ivector_1_t837909686->IVector_1_GetView_m3248688482(&returnValue);
	____ivector_1_t837909686->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.Boolean>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m2065486410 (RuntimeObject* __this, bool ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVector_1_t837909686* ____ivector_1_t837909686 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t837909686::IID, reinterpret_cast<void**>(&____ivector_1_t837909686));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t837909686->IVector_1_IndexOf_m2065486410(___value0, (&____index1_empty), &returnValue);
	____ivector_1_t837909686->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Boolean>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m4191429877 (RuntimeObject* __this, uint32_t ___index0, bool ___value1, const RuntimeMethod* method)
{
	IVector_1_t837909686* ____ivector_1_t837909686 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t837909686::IID, reinterpret_cast<void**>(&____ivector_1_t837909686));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t837909686->IVector_1_SetAt_m4191429877(___index0, ___value1);
	____ivector_1_t837909686->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Boolean>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m2658525135 (RuntimeObject* __this, uint32_t ___index0, bool ___value1, const RuntimeMethod* method)
{
	IVector_1_t837909686* ____ivector_1_t837909686 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t837909686::IID, reinterpret_cast<void**>(&____ivector_1_t837909686));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t837909686->IVector_1_InsertAt_m2658525135(___index0, ___value1);
	____ivector_1_t837909686->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Boolean>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m3887137363 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t837909686* ____ivector_1_t837909686 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t837909686::IID, reinterpret_cast<void**>(&____ivector_1_t837909686));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t837909686->IVector_1_RemoveAt_m3887137363(___index0);
	____ivector_1_t837909686->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Boolean>::Append(T)
extern "C"  void IVector_1_Append_m2261849140 (RuntimeObject* __this, bool ___value0, const RuntimeMethod* method)
{
	IVector_1_t837909686* ____ivector_1_t837909686 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t837909686::IID, reinterpret_cast<void**>(&____ivector_1_t837909686));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t837909686->IVector_1_Append_m2261849140(___value0);
	____ivector_1_t837909686->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Boolean>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m2083066132 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t837909686* ____ivector_1_t837909686 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t837909686::IID, reinterpret_cast<void**>(&____ivector_1_t837909686));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t837909686->IVector_1_RemoveAtEnd_m2083066132();
	____ivector_1_t837909686->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Boolean>::Clear()
extern "C"  void IVector_1_Clear_m1306498240 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t837909686* ____ivector_1_t837909686 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t837909686::IID, reinterpret_cast<void**>(&____ivector_1_t837909686));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t837909686->IVector_1_Clear_m1306498240();
	____ivector_1_t837909686->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Boolean>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m673289840 (RuntimeObject* __this, uint32_t ___startIndex0, BooleanU5BU5D_t2897418192* ___items1, const RuntimeMethod* method)
{
	IVector_1_t837909686* ____ivector_1_t837909686 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t837909686::IID, reinterpret_cast<void**>(&____ivector_1_t837909686));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	bool* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<bool>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(bool));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t837909686->IVector_1_GetMany_m673289840(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivector_1_t837909686->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Boolean>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m2346663782 (RuntimeObject* __this, BooleanU5BU5D_t2897418192* ___items0, const RuntimeMethod* method)
{
	IVector_1_t837909686* ____ivector_1_t837909686 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t837909686::IID, reinterpret_cast<void**>(&____ivector_1_t837909686));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	bool* ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = reinterpret_cast<bool*>((___items0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	hr = ____ivector_1_t837909686->IVector_1_ReplaceAll_m2346663782(____items0_marshaledArraySize, ____items0_marshaled);
	____ivector_1_t837909686->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// T Windows.Foundation.Collections.IVector`1<System.Byte>::GetAt(System.UInt32)
extern "C"  uint8_t IVector_1_GetAt_m156571538 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t1874918097* ____ivector_1_t1874918097 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1874918097::IID, reinterpret_cast<void**>(&____ivector_1_t1874918097));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint8_t returnValue = 0;
	hr = ____ivector_1_t1874918097->IVector_1_GetAt_m156571538(___index0, &returnValue);
	____ivector_1_t1874918097->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Byte>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m216665452 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t1874918097* ____ivector_1_t1874918097 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1874918097::IID, reinterpret_cast<void**>(&____ivector_1_t1874918097));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t1874918097->IVector_1_get_Size_m216665452(&returnValue);
	____ivector_1_t1874918097->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.Byte>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m2759398957 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m2759398957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t1874918097* ____ivector_1_t1874918097 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1874918097::IID, reinterpret_cast<void**>(&____ivector_1_t1874918097));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t473161638* returnValue = NULL;
	hr = ____ivector_1_t1874918097->IVector_1_GetView_m2759398957(&returnValue);
	____ivector_1_t1874918097->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.Byte>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m2293818318 (RuntimeObject* __this, uint8_t ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVector_1_t1874918097* ____ivector_1_t1874918097 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1874918097::IID, reinterpret_cast<void**>(&____ivector_1_t1874918097));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t1874918097->IVector_1_IndexOf_m2293818318(___value0, (&____index1_empty), &returnValue);
	____ivector_1_t1874918097->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Byte>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m3049348516 (RuntimeObject* __this, uint32_t ___index0, uint8_t ___value1, const RuntimeMethod* method)
{
	IVector_1_t1874918097* ____ivector_1_t1874918097 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1874918097::IID, reinterpret_cast<void**>(&____ivector_1_t1874918097));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1874918097->IVector_1_SetAt_m3049348516(___index0, ___value1);
	____ivector_1_t1874918097->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Byte>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m1632374023 (RuntimeObject* __this, uint32_t ___index0, uint8_t ___value1, const RuntimeMethod* method)
{
	IVector_1_t1874918097* ____ivector_1_t1874918097 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1874918097::IID, reinterpret_cast<void**>(&____ivector_1_t1874918097));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1874918097->IVector_1_InsertAt_m1632374023(___index0, ___value1);
	____ivector_1_t1874918097->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Byte>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m313767260 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t1874918097* ____ivector_1_t1874918097 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1874918097::IID, reinterpret_cast<void**>(&____ivector_1_t1874918097));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1874918097->IVector_1_RemoveAt_m313767260(___index0);
	____ivector_1_t1874918097->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Byte>::Append(T)
extern "C"  void IVector_1_Append_m1600833624 (RuntimeObject* __this, uint8_t ___value0, const RuntimeMethod* method)
{
	IVector_1_t1874918097* ____ivector_1_t1874918097 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1874918097::IID, reinterpret_cast<void**>(&____ivector_1_t1874918097));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1874918097->IVector_1_Append_m1600833624(___value0);
	____ivector_1_t1874918097->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Byte>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m2280259536 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t1874918097* ____ivector_1_t1874918097 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1874918097::IID, reinterpret_cast<void**>(&____ivector_1_t1874918097));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1874918097->IVector_1_RemoveAtEnd_m2280259536();
	____ivector_1_t1874918097->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Byte>::Clear()
extern "C"  void IVector_1_Clear_m1881849540 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t1874918097* ____ivector_1_t1874918097 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1874918097::IID, reinterpret_cast<void**>(&____ivector_1_t1874918097));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t1874918097->IVector_1_Clear_m1881849540();
	____ivector_1_t1874918097->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Byte>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m3108887717 (RuntimeObject* __this, uint32_t ___startIndex0, ByteU5BU5D_t4116647657* ___items1, const RuntimeMethod* method)
{
	IVector_1_t1874918097* ____ivector_1_t1874918097 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1874918097::IID, reinterpret_cast<void**>(&____ivector_1_t1874918097));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	uint8_t* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<uint8_t>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(uint8_t));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t1874918097->IVector_1_GetMany_m3108887717(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivector_1_t1874918097->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Byte>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m3769885758 (RuntimeObject* __this, ByteU5BU5D_t4116647657* ___items0, const RuntimeMethod* method)
{
	IVector_1_t1874918097* ____ivector_1_t1874918097 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t1874918097::IID, reinterpret_cast<void**>(&____ivector_1_t1874918097));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	uint8_t* ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = reinterpret_cast<uint8_t*>((___items0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	hr = ____ivector_1_t1874918097->IVector_1_ReplaceAll_m3769885758(____items0_marshaledArraySize, ____items0_marshaled);
	____ivector_1_t1874918097->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// T Windows.Foundation.Collections.IVector`1<System.Char>::GetAt(System.UInt32)
extern "C"  Il2CppChar IVector_1_GetAt_m3761631649 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t80114895* ____ivector_1_t80114895 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t80114895::IID, reinterpret_cast<void**>(&____ivector_1_t80114895));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	Il2CppChar returnValue = 0;
	hr = ____ivector_1_t80114895->IVector_1_GetAt_m3761631649(___index0, &returnValue);
	____ivector_1_t80114895->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Char>::get_Size()
extern "C"  uint32_t IVector_1_get_Size_m729810301 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t80114895* ____ivector_1_t80114895 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t80114895::IID, reinterpret_cast<void**>(&____ivector_1_t80114895));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t80114895->IVector_1_get_Size_m729810301(&returnValue);
	____ivector_1_t80114895->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	return returnValue;
}
// System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1<System.Char>::GetView()
extern "C"  RuntimeObject* IVector_1_GetView_m3718260831 (RuntimeObject* __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IVector_1_GetView_m3718260831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IVector_1_t80114895* ____ivector_1_t80114895 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t80114895::IID, reinterpret_cast<void**>(&____ivector_1_t80114895));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	IVectorView_1_t2973325732* returnValue = NULL;
	hr = ____ivector_1_t80114895->IVector_1_GetView_m3718260831(&returnValue);
	____ivector_1_t80114895->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of return value back from native representation
	RuntimeObject* _returnValue_unmarshaled = NULL;
	if (returnValue != NULL)
	{
		_returnValue_unmarshaled = il2cpp_codegen_com_get_or_create_rcw_from_iinspectable<RuntimeObject>(returnValue, Il2CppComObject_il2cpp_TypeInfo_var);
	}
	else
	{
		_returnValue_unmarshaled = NULL;
	}

	// Marshaling cleanup of return value native representation
	if (returnValue != NULL)
	{
		(returnValue)->Release();
		returnValue = NULL;
	}

	return _returnValue_unmarshaled;
}
// System.Boolean Windows.Foundation.Collections.IVector`1<System.Char>::IndexOf(T,System.UInt32&)
extern "C"  bool IVector_1_IndexOf_m1347992874 (RuntimeObject* __this, Il2CppChar ___value0, uint32_t* ___index1, const RuntimeMethod* method)
{
	IVector_1_t80114895* ____ivector_1_t80114895 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t80114895::IID, reinterpret_cast<void**>(&____ivector_1_t80114895));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' to native representation
	uint32_t ____index1_empty = 0;

	// Native function invocation
	bool returnValue = 0;
	hr = ____ivector_1_t80114895->IVector_1_IndexOf_m1347992874(___value0, (&____index1_empty), &returnValue);
	____ivector_1_t80114895->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___index1' back from native representation
	*___index1 = ____index1_empty;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Char>::SetAt(System.UInt32,T)
extern "C"  void IVector_1_SetAt_m38195806 (RuntimeObject* __this, uint32_t ___index0, Il2CppChar ___value1, const RuntimeMethod* method)
{
	IVector_1_t80114895* ____ivector_1_t80114895 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t80114895::IID, reinterpret_cast<void**>(&____ivector_1_t80114895));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t80114895->IVector_1_SetAt_m38195806(___index0, ___value1);
	____ivector_1_t80114895->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Char>::InsertAt(System.UInt32,T)
extern "C"  void IVector_1_InsertAt_m843679895 (RuntimeObject* __this, uint32_t ___index0, Il2CppChar ___value1, const RuntimeMethod* method)
{
	IVector_1_t80114895* ____ivector_1_t80114895 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t80114895::IID, reinterpret_cast<void**>(&____ivector_1_t80114895));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t80114895->IVector_1_InsertAt_m843679895(___index0, ___value1);
	____ivector_1_t80114895->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Char>::RemoveAt(System.UInt32)
extern "C"  void IVector_1_RemoveAt_m1420036444 (RuntimeObject* __this, uint32_t ___index0, const RuntimeMethod* method)
{
	IVector_1_t80114895* ____ivector_1_t80114895 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t80114895::IID, reinterpret_cast<void**>(&____ivector_1_t80114895));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t80114895->IVector_1_RemoveAt_m1420036444(___index0);
	____ivector_1_t80114895->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Char>::Append(T)
extern "C"  void IVector_1_Append_m3292248582 (RuntimeObject* __this, Il2CppChar ___value0, const RuntimeMethod* method)
{
	IVector_1_t80114895* ____ivector_1_t80114895 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t80114895::IID, reinterpret_cast<void**>(&____ivector_1_t80114895));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t80114895->IVector_1_Append_m3292248582(___value0);
	____ivector_1_t80114895->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Char>::RemoveAtEnd()
extern "C"  void IVector_1_RemoveAtEnd_m3410828052 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t80114895* ____ivector_1_t80114895 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t80114895::IID, reinterpret_cast<void**>(&____ivector_1_t80114895));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t80114895->IVector_1_RemoveAtEnd_m3410828052();
	____ivector_1_t80114895->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.Void Windows.Foundation.Collections.IVector`1<System.Char>::Clear()
extern "C"  void IVector_1_Clear_m4210028140 (RuntimeObject* __this, const RuntimeMethod* method)
{
	IVector_1_t80114895* ____ivector_1_t80114895 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t80114895::IID, reinterpret_cast<void**>(&____ivector_1_t80114895));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Native function invocation
	hr = ____ivector_1_t80114895->IVector_1_Clear_m4210028140();
	____ivector_1_t80114895->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
// System.UInt32 Windows.Foundation.Collections.IVector`1<System.Char>::GetMany(System.UInt32,T[])
extern "C"  uint32_t IVector_1_GetMany_m1663454247 (RuntimeObject* __this, uint32_t ___startIndex0, CharU5BU5D_t3528271667* ___items1, const RuntimeMethod* method)
{
	IVector_1_t80114895* ____ivector_1_t80114895 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t80114895::IID, reinterpret_cast<void**>(&____ivector_1_t80114895));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' to native representation
	uint32_t ____items1_marshaledArraySize = 0;
	Il2CppChar* ____items1_marshaled = NULL;
	if (___items1 != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		____items1_marshaled = il2cpp_codegen_marshal_allocate_array<Il2CppChar>(static_cast<int32_t>(____items1_marshaledArraySize));
		memset(____items1_marshaled, 0, static_cast<int32_t>(____items1_marshaledArraySize) * sizeof(Il2CppChar));
	}

	// Native function invocation
	uint32_t returnValue = 0;
	hr = ____ivector_1_t80114895->IVector_1_GetMany_m1663454247(___startIndex0, ____items1_marshaledArraySize, ____items1_marshaled, &returnValue);
	____ivector_1_t80114895->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items1' back from native representation
	if (____items1_marshaled != NULL)
	{
		____items1_marshaledArraySize = static_cast<uint32_t>((___items1)->max_length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(static_cast<int32_t>(____items1_marshaledArraySize)); i++)
		{
			(___items1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (____items1_marshaled)[i]);
		}
	}

	// Marshaling cleanup of parameter '___items1' native representation
	il2cpp_codegen_marshal_free(____items1_marshaled);
	____items1_marshaled = NULL;

	return returnValue;
}
// System.Void Windows.Foundation.Collections.IVector`1<System.Char>::ReplaceAll(T[])
extern "C"  void IVector_1_ReplaceAll_m968700710 (RuntimeObject* __this, CharU5BU5D_t3528271667* ___items0, const RuntimeMethod* method)
{
	IVector_1_t80114895* ____ivector_1_t80114895 = NULL;
	il2cpp_hresult_t hr = static_cast<Il2CppComObject *>(__this)->identity->QueryInterface(IVector_1_t80114895::IID, reinterpret_cast<void**>(&____ivector_1_t80114895));
	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

	// Marshaling of parameter '___items0' to native representation
	uint32_t ____items0_marshaledArraySize = 0;
	Il2CppChar* ____items0_marshaled = NULL;
	if (___items0 != NULL)
	{
		____items0_marshaledArraySize = static_cast<uint32_t>((___items0)->max_length);
		____items0_marshaled = reinterpret_cast<Il2CppChar*>((___items0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	hr = ____ivector_1_t80114895->IVector_1_ReplaceAll_m968700710(____items0_marshaledArraySize, ____items0_marshaled);
	____ivector_1_t80114895->Release();

	il2cpp_codegen_com_raise_exception_if_failed(hr, false);

}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
